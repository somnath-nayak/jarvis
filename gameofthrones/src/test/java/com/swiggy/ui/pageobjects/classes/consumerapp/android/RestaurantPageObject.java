package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 9/22/17.
 */
public class RestaurantPageObject {

    public By restby = By.id("tv_restaurant_name");

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='QA_MobileTest_Restaurant1']")
    public MobileElement restaurant;

    @AndroidFindBy(id = "add_to_cart_item_add_text")
    public MobileElement add;

    @AndroidFindBy(id = "add_item")
    public MobileElement add_custom_item;

    @AndroidFindBy(id = "tv_checkout")
    public MobileElement checkout;

    @AndroidFindBy(id = "in.swiggy.android:id/item_menu_recommended_item_first_item_layout")
    public MobileElement menu;

    @AndroidFindBy(id = "in.swiggy.android:id/toggle_indicator_off")
    public MobileElement veg_toggle;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_restaurant_name")
    public MobileElement restaurant_name;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_restaurant_cuisine_info")
    public MobileElement cuisine;

    @AndroidFindBy(id = "in.swiggy.android:id/add_to_cart_customisation_text")
    public MobileElement customization;

    @AndroidFindBy(id = "in.swiggy.android:id/item_menu_top_header_restaurant_swiggy_assured_image")
    public MobileElement swiggy_assured;
}
