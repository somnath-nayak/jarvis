package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class LoginPageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/item_menu_top_header_restaurant_name3")
    public MobileElement NewUser;

    @AndroidFindBy(id = "loginCheckPhoneNumberEditText")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"Swiggy\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeTextField")
    public MobileElement MobileNumber;

    @AndroidFindBy(id = "loginCheckButton")
    @iOSFindBy(accessibility = "CONTINUE")
    public MobileElement Continue;

    @AndroidFindBy(id = "loginPasswordEditText")
    @iOSFindBy(xpath = "//XCUIElementTypeApplication[@name=\"Swiggy\"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeSecureTextField")
    public MobileElement Password;

    @AndroidFindBy(id = "loginButton")
    public MobileElement LoginButton;

    @AndroidFindBy(id = "google_place_search_title_text")
    public MobileElement Account;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    @iOSFindBy(accessibility = "Allow")
    public MobileElement Permission;

    @iOSFindBy(accessibility = "Don’t Allow")
    public MobileElement Dont_allow;
}
