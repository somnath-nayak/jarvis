package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class AddressPageObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@index='0' and @text='HOME']")
    public MobileElement address;

    @AndroidFindBy(xpath = "//android.widget.LinearLayout[@index='4']")
    public MobileElement addAddress;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Home']")
    public MobileElement home;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Work']")
    public MobileElement work;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Other']")
    public MobileElement other;

    @AndroidFindBy(id = "in.swiggy.android:id/item_category_name")
    public MobileElement view_more;

    @AndroidFindBy(id = "in.swiggy.android:id/google_place_search_title_text")
    public MobileElement type_of_address;

    @AndroidFindBy(id = "in.swiggy.android:id/google_place_search_subtitle_text")
    public MobileElement complete_address;
}
