package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class CheckoutPageObject {

    @AndroidFindBy(id = "dialog_neutral_layout_text")
    public MobileElement ok;

    @AndroidFindBy(id = "cartProceedToPayText")
    public MobileElement proceed;

    @AndroidFindBy(id = "in.swiggy.android:id/cartMultipleLayoutAddAddressBtn")
    public MobileElement add_address;

    @AndroidFindBy(id = "in.swiggy.android:id/cartMultipleLayoutSelectAddressBtn")
    public MobileElement select_address;
}
