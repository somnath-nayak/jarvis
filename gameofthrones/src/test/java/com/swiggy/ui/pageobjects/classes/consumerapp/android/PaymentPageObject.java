package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 9/22/17.
 */
public class PaymentPageObject {

    public static String paytm_text = "PayTM";
    public static String mobikwik_text = "Mobikwik";
    public static String freecharge_text = "Freecharge";

    @AndroidFindBy(id = "in.swiggy.android:id/cod_text")////android.widget.TextView[@text='By Cash']
    public MobileElement cod;

    @AndroidFindBy(id = "in.swiggy.android:id/view_swiggy_money_statement_button")
    public MobileElement view_statement;

    @AndroidFindBy(id = "in.swiggy.android:id/paytm_money_container_body")
    public MobileElement paytm;

    @AndroidFindBy(id = "in.swiggy.android:id/mobikwik_wallet_container")
    public MobileElement mobikwik;

    @AndroidFindBy(id = "in.swiggy.android:id/freecharge_wallet_container")
    public MobileElement freecharge;

    public By codby = By.id("in.swiggy.android:id/cod_text");

    @AndroidFindBy(id = "in.swiggy.android:id/cod_pay_button")
    public MobileElement pay;

    @AndroidFindBy(id = "tv_toolbar_title")
    public MobileElement header;
}
