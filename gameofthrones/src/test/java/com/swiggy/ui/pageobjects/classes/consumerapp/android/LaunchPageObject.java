package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class LaunchPageObject {

    public static final String app = InitializeUI.getApp_package();

    @AndroidFindBy(id = "item_menu_top_header_restaurant_name3")
    @iOSFindBy(accessibility = "Login")
    public MobileElement login;

    @AndroidFindBy(id = "in.swiggy.android:id/set_location_text")
    @iOSFindBy(accessibility = "SET DELIVERY LOCATION")
    public MobileElement set_location;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    @iOSFindBy(accessibility = "Allow")
    public MobileElement permission;

    @AndroidFindBy(id = "in.swiggy.android:id/cartLoginToProceedLayoutButton")
    public MobileElement continue_button;

    @iOSFindBy(accessibility = "Don’t Allow")
    public  MobileElement dont_allow;
}
