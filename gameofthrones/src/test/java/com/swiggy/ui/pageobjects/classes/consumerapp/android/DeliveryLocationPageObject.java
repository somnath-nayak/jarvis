package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class DeliveryLocationPageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/address_street_address_edit_text1")
    public MobileElement house_no;

    @AndroidFindBy(id = "in.swiggy.android:id/address_street_address_edit_text2")
    public MobileElement landmark;

    @AndroidFindBy(id = "in.swiggy.android:id/address_annotation_home_tv")
    public MobileElement home;

    @AndroidFindBy(id = "in.swiggy.android:id/address_annotation_home_tv1")
    public MobileElement work;

    @AndroidFindBy(id = "in.swiggy.android:id/address_annotation_home_tv2")
    public MobileElement other;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='SAVE and PROCEED']")
    public MobileElement save_button;

    @AndroidFindBy(id = "in.swiggy.android:id/address_street_address_edit_text3")
    public MobileElement area;
}
