package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class HomePageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/bottom_bar_account_icon")
    @iOSFindBy(accessibility = "Account")
    public MobileElement account;

    @AndroidFindBy(id = "in.swiggy.android:id/bottom_bar_cart_icon")
    @iOSFindBy(accessibility = "Cart")
    public MobileElement cart;

    @AndroidFindBy(id = "in.swiggy.android:id/bottom_bar_explore_icon")
    @iOSFindBy(accessibility = "Explore")
    public MobileElement explore;

    @AndroidFindBy(id = "in.swiggy.android:id/bottom_bar_restaurant_icon")
    public MobileElement restaurant_icon;

    @AndroidFindBy(id = "in.swiggy.android:id/flter_annotation")
    @iOSFindBy(xpath = "//XCUIElementTypeButton[@name=\"FILTER \"]")
    public MobileElement filter;

    @AndroidFindBy(id = "location")
    @iOSFindBy(xpath = "//XCUIElementTypeNavigationBar[@name=\"Near You\"]/XCUIElementTypeOther")
    public MobileElement address;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='QA_MobileTest_Restaurant1']")
    public MobileElement restaurant;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_restaurant_name")
    public MobileElement restaurant_name;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_restaurant_cuisine_info")
    public MobileElement cuisine;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_estimated_sla")
    public MobileElement sla;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_cft")
    public MobileElement cost_for_two;

    @AndroidFindBy(id = "in.swiggy.android:id/iv_restaurant_tag")
    public MobileElement restaurant_tag;

    @AndroidFindBy(id = "in.swiggy.android:id/tv_rating")
    public MobileElement rating;

    @AndroidFindBy(id = "in.swiggy.android:id/item_menu_top_header_restaurant_swiggy_assured_image")
    public MobileElement swiggy_assured;
}
