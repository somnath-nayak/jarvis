package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class FilterPageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/flter_annotation")
    public MobileElement filter;

    @AndroidFindBy(id = "in.swiggy.android:id/filters_fragment_item_text")
    public MobileElement filter_select;

    @AndroidFindBy(id = "in.swiggy.android:id/fragment_filter__submit_button")
    public MobileElement apply_filter;
}
