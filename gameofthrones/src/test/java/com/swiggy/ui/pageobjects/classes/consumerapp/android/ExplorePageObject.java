package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class ExplorePageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/et_search")
    public MobileElement search_text;
}
