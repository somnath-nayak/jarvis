package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class AccountPageObject {
    @AndroidFindBy(id = "in.swiggy.android:id/profile__name")
    public MobileElement user_name;

    @AndroidFindBy(id = "in.swiggy.android:id/profile__phone_number")
    public MobileElement phone_number;

    @AndroidFindBy(id = "in.swiggy.android:id/profile__email_address")
    public MobileElement email_id;

    @AndroidFindBy(id = "in.swiggy.android:id/profile__edit_profile_button")
    public MobileElement edit_button;

    @AndroidFindBy(id = "in.swiggy.android:id/account_controller_my_account_header")
    public MobileElement my_account;

    @AndroidFindBy(id = "in.swiggy.android:id/root_view")
    public MobileElement help;

    @AndroidFindBy(id = "in.swiggy.android:id/logoutButton")
    public MobileElement logout;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Manage Address']")
    public MobileElement manage_address;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Payment']")
    public MobileElement payment;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Favorites']")
    public MobileElement favourites;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Referrals']")
    public MobileElement referrals;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Offers']")
    public MobileElement offers;

    @AndroidFindBy(xpath = "in.swiggy.android.prod:id/tv_toolbar_title")
    public MobileElement offers_heading;
}
