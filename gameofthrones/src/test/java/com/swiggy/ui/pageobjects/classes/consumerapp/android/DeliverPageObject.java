package com.swiggy.ui.pageobjects.classes.consumerapp.android;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 9/22/17.
 */
public class DeliverPageObject {

    @AndroidFindBy(id = "in.swiggy.android:id/dish_detail_title")
    public MobileElement order_id;

    @AndroidFindBy(id = "in.swiggy.android:id/test2")
    public MobileElement order_status;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Order Received']")
    public MobileElement order_received;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Order Confirmed']")
    public MobileElement order_confirmed;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Order Picked Up']")
    public MobileElement order_picked_up;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Awaiting restaurant confirmation. delivery Executive will be assigned shortly']")
    public MobileElement order_received_message;

    @AndroidFindBy(xpath = "//android.widget.TextView[conatins(@text,'is on his way to the restaurant to confirm your order')]")
    public MobileElement assigned_message;

    @AndroidFindBy(xpath = "//android.widget.TextView[conatins(@text,'has arrived at the restaurant and will pick up your order soon')]")
    public MobileElement arrived_message;

    @AndroidFindBy(xpath = "//android.widget.TextView[conatins(@text,'has picked up your order. Tasty food is en route!')]")
    public MobileElement picked_up;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Your order has been delivered. Bon Appetit!']")
    public MobileElement order_delivered;

    @AndroidFindBy(id = "in.swiggy.android:id/google_place_search_title_text1")
    public MobileElement ok;
}
