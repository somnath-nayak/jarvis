package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

public class LeftNavDERN extends AndroidUtil {


    public IElement txt_deName;
    public IElement txt_profileSubHeading;
    public IElement btn_earning;
    public IElement btn_floating;
    public IElement btn_loginHistory;
    public IElement btn_order;
    public IElement btn_slot;
    public IElement btn_invite;
    public IElement btn_help;

    public LeftNavDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public void openProfileDetail(){
        txt_deName.click();
    }
    public void openEarningAndIncentives(){
        btn_earning.click();
    }

    public void openFloatingCash(){
        btn_floating.click();
    }
    public void openLoginHistory(){
        btn_loginHistory.click();
    }
    public void openOrderHistory(){
        btn_order.click();
    }
    public void openSlot(){
        btn_slot.click();
    }
    public void openInviteFriend(){
        btn_invite.click();
    }
    public void openHelp(){
        btn_help.click();
    }
    public String getDEName(){
        return txt_deName.getText().toString();
    }
    public String getSubHeading(){
        return txt_profileSubHeading.getText().toString();
    }
    public boolean isDENameDisplayed(){
        return txt_deName.isDisplayed();
    }

    public boolean isInviteFriendOptionDisplay(){
        return btn_invite.isDisplayed();
    }
    public boolean isEarningOptionDisplay(){
        return btn_earning.isDisplayed();
    }


}
