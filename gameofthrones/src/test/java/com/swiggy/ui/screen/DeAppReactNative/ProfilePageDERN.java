package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

public class ProfilePageDERN extends AndroidUtil {

    public IElement txt_DEName;
    public IElement txt_DEZoneName;
    public IElement btn_bonusPoint;
    public IElement btn_personalInformation;
    public IElement btn_BankInformation;
    public IElement txt_DEId;
    public IElement txt_joiningDate;
    public IElement txt_deServiceZone;
    public IElement txt_fleetManager;
    public IElement btn_editEmergencyContact;
    public IElement txt_deBlookGroup;
    public IElement btn_downloadInsurance;
    public IElement btn_shareInsurance;
    public IElement txt_bankAccountName;
    public IElement txt_bankAccountNumber;
    public IElement txt_bankName;
    public IElement txt_branchIfscCode;
    public IElement txt_dePanDetail;
    public IElement txt_deUpiId;




    public ProfilePageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }
    public String getDEName(){
        return txt_DEName.getText().toString();
    }
    public String getDEZone(){
        return txt_DEZoneName.getText().toString();
    }
    public void openBonusPointScreen(){
        btn_bonusPoint.click();
    }
    public void openDEPersonalInformation(){
        btn_personalInformation.click();
    }
    public void openDEBankInfo(){
        btn_BankInformation.click();
    }

    public String getDEId(){
        return txt_DEId.getText().toString();
    }
    public String getDEJoiningDate(){
        return txt_joiningDate.getText().toString();
    }
    public String getDEServiceZone(){
        return txt_deServiceZone.getText().toString();
    }
    public String getFleetManageEmailId(){
        return txt_fleetManager.getText().toString();
    }
    public String getDEBlookDetail(){
        return txt_deBlookGroup.getText().toString();
    }
    public void downloadDEInsurance(){
        btn_downloadInsurance.click();
    }
    public void shareDEInsurance(){
        btn_shareInsurance.click();
    }
    public void clickOnEditEmergencyContact(){
        btn_editEmergencyContact.click();
    }

    public String getAccountHolderName(){
        return txt_bankAccountName.getText().toString();
    }
    public String getAccountNumber(){
        return txt_bankAccountNumber.getText().toString();
    }
    public String getbankName(){
        return txt_bankName.getText().toString();
    }
    public String getIfscCode(){
        return txt_branchIfscCode.getText().toString();
    }
    public String getPandNumber(){
        return txt_dePanDetail.getText().toString();
    }
    public String getUpiId(){
        return txt_deUpiId.getText().toString();
    }





}
