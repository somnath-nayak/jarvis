package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class HomePage extends AndroidUtil {


	public List<IElement> listRestaurant;
	public List<IElement> allText;
	public List<IElement> selectFilter;
	public IElement btn_applyFilter;
	public IElement btn_sortRestaurant;
	public IElement explore;
	public IElement swiggyPop;
	public IElement chainRestaurant;
	public IElement selectOffersAndMore;
	public List<IElement> selectFilterNew;
	public IElement newApplyFilter;
	public IElement sortSLA;

	public List<IElement> listChainRestaurant;
	public IElement btn_accountTab;
	public IElement btn_NearMeTab;
	public List<IElement> image_carousel;
	public IElement txt_collectionName;
	public IElement txt_toolTipNoThanks;
	public IElement btn_cartTab;
	public IElement btn_openFilter;
	public List<IElement> image_SwiggyAssuredTag;
	public IElement txt_sortNudge;
	public IElement FrameFilter;




	public IElement btn_ratingSort;
	public IElement btn_deliveryTimeSort;
	public IElement btn_costOfTwoSort;
	public IElement btn_relevanceSort;
	public IElement txt_selectedSort;
	public List<IElement> collectionlist;
	public List<IElement> txt_offerDiscount;
	public IElement btn_popHomePage;
	private AppiumDriver driver;

	//dd
	public List<IElement> dd_listPageTitle;
	public List<IElement> dd_listPageSubTitle;
	public List<IElement> dd_listPageImage;

	//Preorder
	public IElement btn_TryNow_PreorderToolTip;
	public IElement btn_toolTipClose;
	public IElement txt_preorderToolTipTitle;
	public IElement txt_preordertoolTipMessage;
	public IElement btn_filterClear;
	public IElement btn_Now;

	public List<IElement> txt_tabOnSelectTimerLayout;
	public IElement txt_preOrderHeader;
	public IElement btn_setDeliveryTime;
	public IElement txt_selectedPreorderDateOnHeader;
	public IElement selectFilter_New;
	public IElement timeText_Preorder;
	public IElement textDescription;
	public IElement textOffer;
	public List<IElement> txt_selectDeliveryTime;
	public List<IElement> txt_chainoutlets;
	public List<IElement> txt_chainRestaurantPopup;
	public IElement applyFilterDot;
	public IElement btn_filterReset;
	public List<IElement> checkbox_filter;
	public IElement txt_notServicableLocation;
	public IElement btn_changeLocation;
	public IElement txt_deliveryCrountonMessage;
	public IElement txt_deliveryCrouuntonStatus;
	public IElement swiggysocial_Crouton;
	public IElement txt_preOrderOnbarding;


	public IElement btn_editLocation;
	public IElement btn_oKGotIt;
	public IElement btn_searchForarea;
	public IElement selectfirstaddress;
	public IElement btn_confirmLocation;

	public IElement txt_enterLocation;
	public IElement btn_selectSearchResult;
	public IElement btn_additem;
	public IElement txt_customizationhalfscreendisplayed;
	public IElement btn_customizationAdditem;
	public IElement btn_ViewCart_ConfirmationScreen;
	public IElement btn_closeFilter;
	public IElement listRestaurant1;
	public IElement btn_explore;

	public IElement selectDominosRestaurant;

	//explore page search result
	public List<IElement> listSearchResult;
	public IElement listFirstSearchResult;
	public IElement arw_removeRainMode;
	public List<IElement> selectFilterForDominos;
	public IElement selecctCuisines;
	public IElement locationConfirmationPopup;
	public IElement removeFlatDelivaryNudge;
	public IElement btn_showRestaurantsSubFilter;
	public IElement btn_showRestaurantsSubFilter2;
	public IElement btn_popTab;
	public IElement btn_noThanks;
	public IElement txt_sorttooltip;



    public IElement deeplink_openButton;

	//Hygiene area


    //POP FF
    public IElement txt_blackzonearea;

    public IElement txt_hygiene_area;

	//CategoryBar
	public List<IElement> section_CategoryBar;
	public IElement btn_SuperfastDelivery;
	public IElement btn_OffersNearYou;
	public IElement btn_FreeDelivery;
	public IElement btn_VegOnly;
	public IElement btn_PocketFriendly;
	public IElement btn_TrendingNow;
	public IElement btn_NewlyLaunched;

	public IElement txt_SuperfastDelivery;
	public IElement txt_OffersNearYou;
	public IElement txt_FreeDelivery;
	public IElement txt_VegOnly;
	public IElement txt_PocketFriendly;
	public IElement txt_TrendingNow;
	public IElement txt_NewlyLaunched;
	public IElement btn_back;
	public IElement Assertlistingbysortbtn;

    public IElement verifyCategoryBarOnListing;
    public IElement btn_firstCategoryBar;
    public IElement txt_firstCategoryBarName;

	//Filter Offers&More

    public IElement btn_30MinsOrFree;
    public IElement btn_myFavourites1;
    public IElement btn_Offers;
    public IElement btn_PureVeg;
    public IElement txt_SortFilter;
    public IElement btn_Offers_More;

    public HomePage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        driver = init.getAppiumDriver();
    }


    public void selectRestaurant(String restaurantName, int pos) {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            if (restaurantName != null) {
                ArrayList<String> al = getAllText(listRestaurant);
                System.out.println(al);
                if (al.contains(restaurantName)) {
                    listRestaurant.get(al.indexOf(restaurantName)).click();

                }


            } else {
                listRestaurant.get(pos).click();
            }
            if (checkElementDisplay(chainRestaurant)) {
                listChainRestaurant.get(0).click();
            }
        }
        //IOS Select restaurant
        else {

            if (restaurantName != null) {
                ArrayList<String> al = getAllText(listRestaurant);
                System.out.println(al);
                if (al.contains(restaurantName)) {
                    listRestaurant.get(al.indexOf(restaurantName)).click();

                }

            } else {

                listRestaurant.get(pos).click();
            }

            if (checkElementDisplay(chainRestaurant)) {
                listChainRestaurant.get(1).click();

            }
            //Search from explore page
            else if (checkElementDisplay(listFirstSearchResult)) {
                listSearchResult.get(2).click();
                if (checkElementDisplay(chainRestaurant)) {
                    listChainRestaurant.get(1).click();

                }
            }


        }


    }

    //Select dishtab from explore page
    public void selectDishTab() {
        if (checkElementDisplay(listFirstSearchResult)) {
            listSearchResult.get(1).click();
        }
    }

	public void selectSecondRestaurantios() {
		if(checkElementDisplay(listRestaurant1)){

			listRestaurant1.click();
		}
	}



	public void selectCollection(String collectionName, int pos) {
		if (collectionName != null) {
			ArrayList<String> al = getAllText(collectionlist);
			if (al.contains(collectionName)) {
				collectionlist.get(al.indexOf(collectionName)).click();
			}


        } else {
            collectionlist.get(pos).click();
        }

    }


    public void selectRestaurantNew(String restaurantName, int pos) {
        TouchAction touchAction = new TouchAction(driver);
        boolean checkrestaurantDisplay = false;
        while (!checkrestaurantDisplay) {
            for (int i = 0; i < listRestaurant.size(); i++) {
                System.out.println(listRestaurant.get(i).getText());
                if (listRestaurant.get(i).getText().toString().equalsIgnoreCase(restaurantName)) {
                    listRestaurant.get(i).click();
                    checkrestaurantDisplay = true;
                    break;
                }

            }
            if (!checkrestaurantDisplay) {
                touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            }
        }


		if (checkElementDisplay(chainRestaurant)) {
			listChainRestaurant.get(0).click();
		}

	}

    public void selectDominosRestaurant(String restaurantWith) {

		selectDominosRestaurant.click();
	}
	public String openRestaurantFromList(String restaurantName, int pos) {
		String name = null;
		TouchAction touchAction = new TouchAction(driver);
		boolean checkrestaurantDisplay = false;
		int size=listRestaurant.size();
		System.out.println("Total numbe rof restaurants: "+size);
		int k=1;
		if (inti.platformnew.equalsIgnoreCase("Android")) {

            while (!checkrestaurantDisplay) {
                for (int i = 0; i < listRestaurant.size(); i++) {
                    System.out.println(listRestaurant.get(i).getText());
                    if (listRestaurant.get(i).getText().toString().equalsIgnoreCase(restaurantName)) {
                        name = getTextNumberOfOutletsFromHomePage(0);
                        txt_chainoutlets.get(0).click();
                        checkrestaurantDisplay = true;
                        break;
                    }

                }
                if (!checkrestaurantDisplay) {
                    touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
                }
            }
        }
        //IOS chain restaurant test
        else {
            while (k <= 5 && !checkrestaurantDisplay) {
                for (int i = 0; i < listRestaurant.size(); i++) {
                    System.out.println(listRestaurant.get(i).getText());
                    if (listRestaurant.get(i).getText().toString().equalsIgnoreCase(restaurantName)) {
                        AndroidUtil.sleep(5000);
                        listRestaurant.get(i).click();
                        name = txt_chainoutlets.get(0).getText();
                        System.out.println("Number of Outlets: " + name);
                        checkrestaurantDisplay = true;
                        break;
                    } else {
                        System.out.println("Restaurant did not match");

                    }
                }
                AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
                k++;

			}
		}
		return name;
	}
	public String openRestaurantFromList1(String restaurantName, int pos) {
		String name = null;
		int size=listRestaurant.size();
		System.out.println("Total numbe rof restaurants: "+size);
		boolean checkrestaurantDisplay = false;
		int k=1;
		while (k<=5 && !checkrestaurantDisplay) {
			for (int i = 0; i < listRestaurant.size(); i++) {
				System.out.println(listRestaurant.get(i).getText());
				if (listRestaurant.get(i).getText().toString().equalsIgnoreCase(restaurantName)) {
					AndroidUtil.sleep(5000);
					listRestaurant.get(i).click();
					name = txt_chainoutlets.get(0).getText();
					System.out.println("Number of Outlets: "+name);
					checkrestaurantDisplay = true;
					break;
				} else {
					System.out.println("Restaurant did not match");

                }

            }
            AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
            k++;

        }

        return name;
    }




	public String openRestaurantFromListNew(String restaurantName, int pos) {


        String name = null;
        TouchAction touchAction = new TouchAction(driver);
        boolean checkrestaurantDisplay = false;

        while (!checkrestaurantDisplay) {
            for (int i = 0; i < listRestaurant.size(); i++) {
                System.out.println(listRestaurant.get(i).getText());
                if (listRestaurant.get(i).getText().toString().equalsIgnoreCase(restaurantName)) {
                    name = getTextNumberOfOutletsFromHomePage(0);
                    listRestaurant.get(i).click();
                    checkrestaurantDisplay = true;
                    break;
                }

            }
            if (!checkrestaurantDisplay) {
                touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            }
        }
        return name;


    }

    public String getRestaurantNameFromList(int pos) {
        return listRestaurant.get(pos).getText().toString();
    }

    public boolean getFavRestList(String pos) {
        return listRestaurant.toString().contains(pos);
    }


    public void clickOnCarousel(int pos) {
        image_carousel.get(0).click();
    }

    public void clickOnCarouselAndOpenRestaurantFromCollection(int pos) {
        clickOnCarousel(pos);
        /*if(checkElementDisplay(txt_collectionName)){
			selectRestaurant(null,0);
		}*/

    }

	/*public boolean isHomePage() {
		return this.isElementPresent(homepageobject.filter);
	}

	public void gotoAccountPage() {
		this.click(homepageobject.account);
	}

	public void gotoExplorePage() {
		this.click(homepageobject.explore);
	}

	public void gotoAddressPage() {
		this.click(homepageobject.address);
	}

	public void selectRestaurant() {
		try{
		click(homepageobject.restaurant);
		Thread.sleep(5000);
		clickBackButton();
		click(homepageobject.restaurant);
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void listrest() {
		List<MobileElement> rest = driver.findElementsById("in.swiggy.android:id/tv_restaurant_name");
		for(MobileElement r: rest) {
			System.out.println(r.getText());
		}
	}

	public String getRating() {
		return this.getText(homepageobject.rating);
	}

	public String getSla() {
		return this.getText(homepageobject.sla);
	}

	public String getCostForTwo() {
		return this.getText(homepageobject.cost_for_two);
	}

	public void clickOnFilter() {
		 this.click(homepageobject.filter);
	}

	public boolean isSwiggyAssured() {
		return this.isElementPresent(homepageobject.swiggy_assured);
	}*/

    public ArrayList<String> getAllText(List<IElement> listRestaurant) {
        ArrayList<String> al = new ArrayList<>();
        for (int i = 0; i < listRestaurant.size(); i++) {
            System.out.println(listRestaurant.get(i).getText());
            al.add(listRestaurant.get(i).getText());

        }
        return al;

    }

    public void openFilter() {

        if (inti.platformnew.equalsIgnoreCase("Android")) {
            if (checkElementDisplay(btn_openFilter)) {
                btn_openFilter.click();

			} else {
				System.out.println("NEw filter");
				if (checkElementDisplay(selectFilter_New)) {
					System.out.println("jdhjhjd");
					selectFilter_New.click();
					selecctCuisines.click();
					System.out.println("jhdjh");
				}
			}
		}
		//Ios filter
			else{
					if (checkElementDisplay(btn_openFilter)) {
						btn_openFilter.click();

					} else {
						System.out.println("NEw filter");
						if (checkElementDisplay(selectFilter_New)) {
							System.out.println("jdhjhjd");
							selectFilter_New.click();
							//selecctCuisines.click();
							System.out.println("jhdjh");
						}
					}
				}
			}

				public void clickonSortRestaurantBySLA() {
					sortSLA.click();
				}

				public void offersAndMore(String offers) {

					selectOffersAndMore.click();
					FrameFilter.click();
					ArrayList<String> al = getAllText(selectFilterNew);
					if (al.contains(offers)) {
						selectFilterNew.get(al.indexOf(offers)).click();
					}
				}

    public void clickonClearFilter() {
        if (checkElementDisplay(btn_filterClear)) {
            btn_filterClear.click();
        } else {
            clickonText("Clear All");


        }
    }

    public void clickonApplyForNewFilter() {
        newApplyFilter.click();
    }

    public void filterRestaurantWith(String restaurantWith) {
        AndroidUtil.sleep();
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            TouchAction touchAction = new TouchAction(driver);
            boolean checkFilter = false;
            while (!checkFilter) {
                ArrayList<String> al = getAllText(selectFilter);
                for (int i = 0; i < al.size(); i++) {
                    if (al.get(i).equalsIgnoreCase(restaurantWith)) {
                        selectFilter.get(i).click();
                        checkFilter = true;
                        break;
                    }
                }
                if (!checkFilter) {
                    touchAction.longPress(50, 600).moveTo(50, 100).release().perform();
                }

            }
            btn_applyFilter.click();
        } else {
            tapItemByTextForIos(restaurantWith);
            btn_applyFilter.click();
        }


    }

    public void filterRestaurantForDominos(String restaurantWith) {
        AndroidUtil.sleep();
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            TouchAction touchAction = new TouchAction(driver);
            boolean checkFilter = false;
            while (!checkFilter) {
                ArrayList<String> al = getAllText(selectFilterForDominos);
                for (int i = 0; i < al.size(); i++) {
                    if (al.get(i).equalsIgnoreCase(restaurantWith)) {
                        selectFilterForDominos.get(i).click();
                        checkFilter = true;
                        break;
                    }
                }
                if (!checkFilter) {
                    touchAction.longPress(50, 600).moveTo(50, 100).release().perform();
                }

            }
            btn_applyFilter.click();
        } else {
            tapItemByTextForIos(restaurantWith);
            btn_applyFilter.click();
        }


    }

    public void filterRestauranByPosition(int pos, String c) {
        TouchAction touchAction = new TouchAction(driver);
        boolean checkFilter = false;
        selectFilter.get(pos).click();
        filterRestaurantByCuisines(c);


        btn_applyFilter.click();


    }


    public void filterRestaurantByCuisines(String cuisinesName) {
        ArrayList<String> al = getAllText(selectFilter);
        if (al.contains(cuisinesName)) {

            selectFilter.get(al.indexOf(cuisinesName)).click();
        }
    }

    public void filterRestaurantByPosition(int pos) {
        selectFilter.get(pos).click();
    }

    public void clickonSortRestaurant() {
        btn_sortRestaurant.click();
    }

    public void selectSortRestaurant(String type) {
        ArrayList<String> al = getAllText(allText);
        if (al.contains(type)) {
            allText.get(al.indexOf(type)).click();
        }
    }

    public void openAccountTab() {

        if (inti.platformnew.equalsIgnoreCase("Android")) {
            btn_accountTab.click();
        }
        //iosacount tab
        else {
            if (checkElementDisplay(btn_accountTab)) {

					btn_accountTab.click();

            } else {
                btn_cartTab.click();
            }
        }


    }

    public void openNearMeTab() {
        btn_NearMeTab.click();
    }

	public void openExploreTab() {
			explore.click();
	}

    public void removeSwiggyPop() {


        if (checkElementDisplay(swiggyPop)) {
            swiggyPop.click();
            AndroidUtil.sleep(3000);
            openNearMeTab();
        }
        removePreOrderToolTip();

    }

    public void openSwiggyPop() {

        if (inti.platformnew.equalsIgnoreCase("Android")) {
            if (checkElementDisplay(swiggyPop)) {
                swiggyPop.click();

            }
            if (checkElementDisplay(btn_popHomePage)) {
                btn_popHomePage.click();
            }
            TouchAction touchAction = new TouchAction(driver);
            touchAction.longPress(50, 100).moveTo(50, 1000).release().perform();
            touchAction.longPress(50, 100).moveTo(50, 1000).release().perform();
            touchAction.longPress(50, 100).moveTo(50, 1000).release().perform();
        } else {
            if (checkElementDisplay(swiggyPop)) {
                swiggyPop.click();
            } else if (checkElementDisplay(btn_popTab)) {
                btn_popTab.click();
            }

        }
    }

    public void removePreOrderToolTip() {
        if (checkElementDisplay(timeText_Preorder)) {

            TouchAction touchAction = new TouchAction(driver);
            touchAction.longPress(50, 300).moveTo(50, 600).release().perform();

            //clickOnASAPOptionInPreorderNux();
            //selectPreorderDeliveryTimeText(0);
            //clickOnSetDeliveryTimeButtonForPreorder();
        }
        AndroidUtil.sleep();
        if (checkElementDisplay(txt_preorderToolTipTitle)) {
            btn_toolTipClose.click();
        }

    }

    public void removeSwiggyPopForPreorder() {
        if (checkElementDisplay(swiggyPop)) {
            swiggyPop.click();
            openNearMeTab();
        }
    }

    public void removeSwiggyPOPForReorder() {
        if (checkElementDisplay(swiggyPop)) {
            swiggyPop.click();
            openAccountTab();

        }
    }


    public boolean checkElementDisplay(IElement element) {
        try {
            if (element.isDisplayed()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }

    public void removeToolTip() {
        if (checkElementDisplay(txt_toolTipNoThanks)) {
            txt_toolTipNoThanks.click();
        }
    }

    public void openCartTab() {
        btn_cartTab.click();
    }

	public boolean checkSwiggyAssuredIcon(int pos) {
		return checkElementDisplay(image_SwiggyAssuredTag.get(0));

	}

    public void selectSorting(String sortingtype) {
        switch (sortingtype) {
            case Constants.RELEVANCE_SORT:
                btn_relevanceSort.click();
                break;
            case Constants.COSTOFTWO_SORT:
                btn_costOfTwoSort.click();
                break;
            case Constants.DELIVERYTIME_SORT:
                btn_deliveryTimeSort.click();
                break;
            case Constants.RATING_SORT:
                btn_ratingSort.click();
                break;
        }
    }

    public void clickOnFilterOffersAndMore(){
        btn_Offers_More.click();
    }


    public void setSelectOffersAndMore(String filterOffers){
        switch (filterOffers){
            case Constants.SHOW_RESTAURANT_WITH_30_MINS_OR_FREE:
                btn_30MinsOrFree.click();
                break;
            case Constants.SHOW_RESTAURANT_WITH_MY_FAVOURITES:
                btn_myFavourites1.click();
                break;
            case Constants.SHOW_RESTAURANT_WITH_OFFERS:
                btn_Offers.click();
                break;
            case Constants.SHOW_RESTAURANT_WITH_PURE_VEG:
                btn_PureVeg.click();
                break;

                //Default statement
            default:
                System.out.println("Couldn't find any filters");
        }

    }


    public String getFilterHeaderText(){
        return txt_SortFilter.getText();
    }

    public void clickonApplySortAndFilterButton() {
        btn_applyFilter.click();

    }

    public void scrolltill() {
        TouchAction touchAction = new TouchAction(driver);
        while (!checkElementDisplay(dd_listPageTitle)) {
            touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        }
        //touchAction.longPress(50, 600).moveTo(50, 300).release().perform();

    }

    public void scrollInIos() {

    }

    public String getStoryCollectionHeader() {
        return dd_listPageTitle.get(0).getText();
    }

    public String getStoryCollectionSubtitle() {
        return dd_listPageSubTitle.get(0).getText();
    }

    public String getStoryCollectionItem(int pos) {
        return dd_listPageTitle.get(pos).getText();
    }

    public String getStoryCollectionItemSubTitle(int pos) {
        return dd_listPageSubTitle.get(pos).getText();
    }

    public void openStoryCollection(int pos) {
        if (inti.platformnew.equalsIgnoreCase("ios")) {
            dd_listPageTitle.get(pos).click();
        } else {
            dd_listPageTitle.get(pos + 1).click();
        }
    }

    public boolean checkElementDisplay(List<IElement> element) {
        try {
            if (element.size() > 1) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }


    public String getOfferDiscount(int pos) {
        return txt_offerDiscount.get(pos).getText().toString();
    }

    public boolean isStoryCardDisplay() {
        return checkElementDisplay(dd_listPageTitle);
    }

    public void verticalScroll() {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            TouchAction touchAction = new TouchAction(driver);
            while (!checkElementDisplay(dd_listPageTitle)) {
                touchAction.longPress(50, 600).moveTo(50, 50).release().perform();
            }
        } else {
            JavascriptExecutor js = driver;
            HashMap<String, String> scrollObject = new HashMap<String, String>();
            scrollObject.put("direction", "down");
            js.executeScript("mobile: scroll", scrollObject);
        }
    }

    public void validateToolTipData() {
        Assert.assertEquals(txt_preorderToolTipTitle.getText().toLowerCase(), Constants.PREORDER_TOOLTIP_TITLE.toLowerCase());
        Assert.assertEquals(txt_preordertoolTipMessage.getText().toLowerCase(), Constants.PREORDER_TOOLTIP_MESSAGE.toLowerCase());

    }
	public String getPreorderOnboardingText(){
		return txt_preOrderOnbarding.getText();
	}

	public void clickonNowPreorderTime(){
        btn_Now.click();
    }

	public void clickOnSetDeliveryTimeButtonForPreorder() {
		AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
		btn_setDeliveryTime.click();
	}


    public void clickonTryNowPreorderButtonFromToolTip() {
        btn_TryNow_PreorderToolTip.click();
    }

    public String getHeaderOfSelectTimerForPreorder() {
        return txt_preOrderHeader.getText().toString();
    }


    public void clickonpreorderDialog() {
        textOffer.click();
    }

    public String getPreorderDeliveryTimeText(int pos) {
        if (txt_selectDeliveryTime.size() > pos) {
            return txt_selectDeliveryTime.get(pos).getText().toString();
        } else {
            return txt_selectDeliveryTime.get(pos).getText().toString();
        }
    }

    public void selectPreorderDeliveryTimeText(int pos) {
        AndroidUtil.sleep();
        if (txt_selectDeliveryTime.size() > pos) {
            txt_selectDeliveryTime.get(pos).click();
        } else {
            txt_selectDeliveryTime.get(0).click();
        }
    }

    public String getPreorderSlotTime() {
        return txt_selectedPreorderDateOnHeader.getText().toString();
    }

    public void clickOnPreorderSlot() {
        txt_selectedPreorderDateOnHeader.click();
    }

    public void selectDateTabForPreorder(int pos) {
        txt_tabOnSelectTimerLayout.get(pos).click();
    }

    public ArrayList<String> getDayForPreorder() {
        ArrayList<String> al = new ArrayList<>();
        for (int i = 0; i < txt_tabOnSelectTimerLayout.size(); i++) {
            al.add(txt_tabOnSelectTimerLayout.get(i).getText());
        }
        return al;
    }

    public void validateDaysStringForPreorder(List<Menulist> al) {
        ArrayList<String> days = getDayForPreorder();
        for (int i = 0; i < days.size(); i++) {
            Date expiry = new Date(al.get(i).getPreorderdate().get(i));
            //Assert.assertEquals(days.get(i).toLowerCase(),expiry.toString().split(" ")[0]);
            System.out.println(days.get(i));
            System.out.println(expiry.toString().split(" ")[i]);

            Assert.assertTrue(days.get(i).toLowerCase().contains(expiry.toString().split(" ")[0].toLowerCase()));
        }
    }

    public void scrolltillLast() {
        int leftX = txt_selectDeliveryTime.get(0).getLocation().getX();
        int rightX = leftX + txt_selectDeliveryTime.get(0).getSize().getWidth();
        int middleX = (rightX + leftX) / 2;
        int upperY = txt_selectDeliveryTime.get(0).getLocation().getY();
        int lowerY = upperY + txt_selectDeliveryTime.get(0).getSize().getHeight();
        int middleY = (upperY + lowerY) / 2;

        System.out.println(leftX);
        System.out.println(leftX);
        System.out.println(rightX);
        System.out.println(middleX);
        System.out.println(upperY);
        System.out.println(lowerY);
        System.out.println(middleY);

        TouchAction touchAction = new TouchAction(driver);
        for (int i = 0; i < 20; i++) {
            touchAction.longPress(leftX, txt_selectDeliveryTime.get(txt_selectDeliveryTime.size() - 1).getLocation().getY()).moveTo(leftX, txt_selectDeliveryTime.get(0).getLocation().getX()).release().perform();
        }


    }

    public void clickOnASAPOptionInPreorderNux() {
        timeText_Preorder.click();
    }

    public void clickOnClosePreorderToolTip() {
        if (checkElementDisplay(btn_toolTipClose)) {
            btn_toolTipClose.click();
        }
    }

    public String getTextNumberOfOutletsFromHomePage(int pos) {
        if (checkElementDisplay(txt_chainoutlets)) {
            return txt_chainoutlets.get(pos).getText();
        } else {
            TouchAction touchAction = new TouchAction(driver);

            touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            return txt_chainoutlets.get(pos).getText();
        }
    }

    public String getChainRestaurantPopupHeader() {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            return txt_chainRestaurantPopup.get(0).getText().toString();
        } else {
            return txt_chainRestaurantPopup.get(1).getText().toString();
        }
    }

    public String getChainRestaurantNumberOfOutletText() {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            return txt_chainRestaurantPopup.get(1).getText().toString();
        }
        return txt_chainRestaurantPopup.get(0).getText().toString();
    }

    public ArrayList<String> getChainRestaurantList() {
        ArrayList<String> al = new ArrayList<>();
        int s = listChainRestaurant.size();
        System.out.println(s);
        for (int i = 0; i < s; i++) {
            al.add(listChainRestaurant.get(i).getText());
        }
        return al;
    }

    public boolean checkFilterApplied() {
        return checkElementDisplay(applyFilterDot);
    }

    public void clickonResetFilter() {
        if (checkElementDisplay(btn_filterReset)) {
            btn_filterReset.click();
        } else {
            clickonText("Reset");
        }
    }

    public void clickonText(String textName) {
        for (int i = 0; i < allText.size(); i++) {
            if (allText.get(i).getText().equalsIgnoreCase(textName)) {
                allText.get(i).click();
                break;
            }
        }
    }


    public String isFilterCheckboxSelected(int pos) {
        int i = 10;
        while (!checkElementDisplay(checkbox_filter) && i > 0) {
            i--;
            continue;
        }
        return checkbox_filter.get(pos).getAttribute("checked");
    }

    public void clickonApplyFilterButton() {
        btn_applyFilter.click();
        scrollDownOnPopPage();

    }



    public void scrollDownOnPopPage() {



		TouchAction touchAction = new TouchAction(driver);
		touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
		touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
		touchAction.longPress(50, 600).moveTo(50, 300).release().perform();


    }

    public String getNotServicableLocationText() {
        return txt_notServicableLocation.getText().toString();
    }

    public void clickOnEditLocationButton() {
        btn_changeLocation.click();
    }

    public String getSelectedSort() {
        return txt_selectedSort.getText().toString();
    }

    public String getCollectionName() {
        return txt_collectionName.getText().toString();
    }

    public void clickonSwiggySocialCrouton() {

        swiggysocial_Crouton.click();
    }


    public String getOrderTrackStatusFromHomePageCrouton() {
        if (checkElementDisplay(txt_deliveryCrountonMessage)) {
            return txt_deliveryCrouuntonStatus.getText().toString();
        } else {
            return null;
        }
    }

    public String getOrderStatusFromHomePageCrouton() {
        if (checkElementDisplay(txt_deliveryCrountonMessage)) {
            return txt_deliveryCrountonMessage.getText().toString();
        } else {
            return null;
        }
    }

    public void clickOnOrderPlacedToolTip() {
        txt_deliveryCrountonMessage.click();
    }

    public void clickOnSortNudge() {
        if (checkElementDisplay(txt_sortNudge)) {
            txt_sortNudge.click();
        }
    }

    public void tapItemByTextForIos(String text) {
        System.out.println("   tapItemByDescription(): " + text);

        // scroll to item
        JavascriptExecutor js = driver;
        HashMap scrollObject = new HashMap<>();
        scrollObject.put("predicateString", "value == '" + text + "'");
        js.executeScript("mobile: scroll", scrollObject);

        // tap item
        WebElement el = ((IOSDriver) driver).findElementByIosNsPredicate("value = '" + text + "'");
        el.click();
    }

    public void clickOnEditLocation() {
        if (checkElementDisplay(btn_editLocation)) {
            AndroidUtil.sleep();
            btn_editLocation.click();
            txt_enterLocation.click();
            txt_enterLocation.sendKeys("IBC");
            AndroidUtil.sleep(3000);
            btn_selectSearchResult.click();
            btn_confirmLocation.click();
        }
    }


    public void clickOnEditLocationforaddaddresscase() {
        AndroidUtil.sleep();
        btn_editLocation.click();
        txt_enterLocation.click();
        txt_enterLocation.sendKeys("Koramangala");
        AndroidUtil.sleep(3000);
        btn_selectSearchResult.click();
        btn_confirmLocation.click();
    }




	public void selectRestaurantios(String restaurantName, int pos) {

        if (checkElementDisplay(listRestaurant)) {
            listRestaurant.get(pos).click();
            scrollInIos();
        }


    }


    public void additemtoCart() {
        //btn_addfirstitem.click();
        btn_additem.click();
        Boolean b = checkElementDisplay(txt_customizationhalfscreendisplayed);
        System.out.println(b);
        Boolean c = checkElementDisplay(btn_ViewCart_ConfirmationScreen);
        System.out.println(c);
		/*if (checkElementDisplay(txt_customizationhalfscreendisplayed)) {
			btn_customizationAdditem.click();
			btn_ViewCart_ConfirmationScreen.click();
		} else {
			btn_ViewCart_ConfirmationScreen.click();
			AndroidUtil.sleep(3000);

		}*/



}

	public void closeFilter()
	{
		btn_closeFilter.click();
	}

    public void removePOPRainMode() {
        if (checkElementDisplay(arw_removeRainMode)) {
            arw_removeRainMode.click();
        }
    }

    public void taponsorttooltip(){
        if (checkElementDisplay(txt_sorttooltip)){

            txt_sorttooltip.click();
        }else{
            System.out.println("Tooltip not found");}
        }

	public void locationConfirmationPopup() {
		if (checkElementDisplay(locationConfirmationPopup)) {
			locationConfirmationPopup.click();
		}
	}

    public void removeFlatDelivaryFeeNudge() {
        if (checkElementDisplay(removeFlatDelivaryNudge)) {
            removeFlatDelivaryNudge.click();
        }
    }

    public void selectShowRestaurantsWith() {
        if (checkElementDisplay(btn_showRestaurantsSubFilter)) {
            btn_showRestaurantsSubFilter.click();
        }
    }


	public void removeChangeLocationPopup() {
		if (checkElementDisplay(btn_noThanks)) {
			btn_noThanks.click();
		}
	}
	public void deeplink_openButton() {
		if (checkElementDisplay(deeplink_openButton)) {
			deeplink_openButton.click();
		}
	}

    public void open_cusinesSubFilter() {
        if (checkElementDisplay(btn_showRestaurantsSubFilter2)) {
            btn_showRestaurantsSubFilter2.click();
        }


}


	//Hygienerating area
	public void Hygieneratingarea() {
		if (checkElementDisplay(btn_editLocation)) {
			AndroidUtil.sleep();
			btn_editLocation.click();
			txt_enterLocation.click();
			txt_enterLocation.sendKeys("DLF phase 4");
			AndroidUtil.sleep(3000);
			txt_hygiene_area.click();
			btn_confirmLocation.click();
		}
	}//POP FF
    public void blackzonearea() {
        if (checkElementDisplay(btn_editLocation)) {
            AndroidUtil.sleep();
            btn_editLocation.click();
            txt_enterLocation.click();
            txt_enterLocation.sendKeys("patna");
            AndroidUtil.sleep(3000);
            txt_blackzonearea.click();
            btn_confirmLocation.click();
        }
    }

	//CategoryBar
	public boolean verifyCategoryBar() {
		boolean result = checkElementDisplay(btn_SuperfastDelivery);
		return result;
	}
    public boolean getSuperFastDeliveryDetailScreen() {
        boolean result=checkElementDisplay(txt_SuperfastDelivery);
        return result;
    }

	public void verifySuperFastDelivery() {
		if (checkElementDisplay(btn_SuperfastDelivery)) {
			btn_SuperfastDelivery.click();

		} else {
			System.out.println("No SuperDelivery option ");
		}
	}



	public void CategoryBarBack(){
		btn_back.click();
	}

    public boolean assertlisting(){
        return checkElementDisplay(Assertlistingbysortbtn);
    }


	public void verifyVegOnly() {
		if (checkElementDisplay(btn_VegOnly)) {
			btn_VegOnly.click();

		} else {
			System.out.println("No Veg only option ");
		}
	}

	public boolean getVegOnlyText() {
		boolean result=checkElementDisplay(txt_VegOnly);
		return result;
	}

	// Category Bar

           public void verifyCategoryBarOnListing() {
              if (checkElementDisplay(verifyCategoryBarOnListing)) {

                               System.out.println("Category bar is enabled on listing");
                   } else {
                       System.out.println("Category bar is disabled on listing");
                   }
    }
           public String getFirstCategoryBarName() {
               if (checkElementDisplay(txt_firstCategoryBarName)) {

                   return txt_firstCategoryBarName.getText();
                  }
           else{
                   return null;
               }
    }

   public void clickOnFirstCategoryBar(){
          btn_firstCategoryBar.click();
           }
}