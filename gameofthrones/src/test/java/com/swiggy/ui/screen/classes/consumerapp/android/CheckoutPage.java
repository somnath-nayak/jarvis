package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pojoClasses.EDVOMeal;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.ITestContext;

import java.util.HashMap;
import java.util.List;

import static com.swiggy.ui.utils.iOSUtils.Direction.UP;

public class CheckoutPage extends AndroidUtil {
    public IElement btn_ok;
    public IElement btn_proceedToPay;
    public IElement btn_addAddress;
    public IElement cartRestaurantName;
    public List<IElement> cartItemName;
    public IElement txt_itemCount;
    public IElement txt_savedAddressHeader;
    public IElement txt_totalBill;
    public IElement layout_cartLoginToProceedLayout;
    public IElement btn_continue;
    public IElement cartSelectAddress;
    public IElement btn_cartNewAddress;
    public List<IElement> btn_increaseItemCount;
    public List<IElement> btn_decreaseItemCount;
    public IElement btn_applyCoupon;
    public IElement input_enterCoupon;
    public IElement btn_apply;
    public IElement txt_couponAppliedDialogTitle;
    public IElement txt_couponAppliedDialogMessage;
    public IElement btn_proceedToPayNew;

    public IElement btn_okCouponApplied;
    public IElement btn_applyCouponnew;
    public IElement btn_viewDetailBill;

    public IElement btn_ChangeAddress;
    public List<IElement> txt_selectCartAddressFromList;
    public IElement txt_totalBillNew;
    public List<IElement> txt_mealPrice;
    public IElement txt_mealNameOnCustomizationPopUp;
    public IElement txt_mealItemOnCustomizationPopUp;
    public IElement txt_priceOnCustomizationPopUp;
    public IElement btn_iWillCustomise;
    public IElement btn_repeatcustomization;

    public List<IElement> txt_priceInCart;
    public List<IElement> txt_priceInfo;
    public IElement txt_finalPrice;
    public IElement txt_preorderSLA;
    public IElement btn_removeUnavailable;
    public IElement txt_emptyCartText;
    public IElement txt_cartCountOnBadge;
    public IElement clickOnSelectHomeAddress;


    public IElement btn_clearAppliedCoupon;

    public IElement btn_removeguest;
    public IElement btn_yes;
    public IElement txt_pastOrder;
    public List<IElement> btn_applyIos;
    public List<IElement>txt_AddressNameList;
    public IElement btn_applyfirstavailableCoupon;
    private AppiumDriver<MobileElement> driver;

    //Cafe elements
    public IElement txt_pickupatcounter;
    public IElement btn_howitworks_cart;
    public IElement btn_Okgotit;
    public IElement btn_cafeapplycoupon;
    public IElement btn_cafeapplycouponnew;


    AddressPage addressPage;

    public CheckoutPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        addressPage = new AddressPage(init);
        driver = init.getAppiumDriver();
    }


    public void clickOk() {
        try {
            if (btn_ok.isDisplayed()) {
                btn_ok.click();
            }
        } catch (Exception e) {

        }


    }

    public void clickOnProceedToPay() {

        if (checkElementDisplay(btn_proceedToPay)) {
            btn_proceedToPay.click();
        } else {
            btn_proceedToPayNew.click();
        }
    }

    public void addNewAddress() {

        btn_addAddress.click();
        /*try{
        this.click(checkoutpageobject.select_address);
		System.out.println(driver.findElements(add));
		List<MobileElement> list = driver.findElements(add);
		for(MobileElement e : list){
			System.out.println(e.getText());
			if(e.getText().equalsIgnoreCase("home"))
				this.click(e);
		}
		} catch(Exception e) {
			System.out.println(add);
		}
		return this;*/

	}

	public String getRestaurantNameFromCart(){
		return cartRestaurantName.getText().toString();
	}
	public String getItemNameFromCart(int  pos){
		waitForElementToDisplay(cartItemName);
		if(checkElementDisplay(cartItemName)) {
			return cartItemName.get(pos).getText().toString();
		}
		else{
			return null;
		}
	}
	public String getDefaultAddressFromCart(){
		return txt_savedAddressHeader.getText().toString();
	}
	public String getTotalBillInCart(){
		String str=txt_totalBillNew.getText().toString();
		String numberOnly= str.replaceAll("[^.0-9]", "");
		return numberOnly;

	}
	public boolean validateLoginToProceedLayoutDisplayInCart(){
		try {
			return layout_cartLoginToProceedLayout.isDisplayed();
		}
		catch(Exception e){
			return false;
		}
	}
	public void clickOnProccedWithPhoneNumber(){

			layout_cartLoginToProceedLayout.click();
		}

	public boolean validateAddAddressButtonDisplay(){
		return checkElementDisplay(btn_cartNewAddress);
	}
	public void proceedToPay(){
		if(validateAddAddressButtonDisplay() && false){
			btn_cartNewAddress.click();
			addressPage.enterNewAddress("WORK",null);
		}

			clickOnProceedToPay();

    }


	public void increaseItemCount(){
		btn_increaseItemCount.get(0).click();
	}
	public void decreaseItemCount(){
		btn_decreaseItemCount.get(0).click();
	}
	public void changeAddresFromCart(){
		btn_ChangeAddress.click();
		txt_selectCartAddressFromList.get(0).click();

	}
	public void clickonApplyCoupon(){
		if(checkElementDisplay(btn_clearAppliedCoupon)){
			btn_clearAppliedCoupon.click();
		}
		if(checkElementDisplay(btn_applyCouponnew)){
			btn_applyCouponnew.click();
		}
		else {
			btn_applyCoupon.click();
		}
	}
	public void enterCoupon(String coupon){
		input_enterCoupon.click();
		input_enterCoupon.sendKeys(coupon);
	}
	public void clickonApplyCouponButton(){
		if(inti.platformnew.equalsIgnoreCase("Android")) {
			btn_apply.click();
		}
		//list of apply buttons in ios
		else{
			btn_apply.click();
			//btn_applyIos.get(0).click();
		}
	}

    public void clickonApplyCouponNormalCart() {

        if (checkElementDisplay(btn_clearAppliedCoupon)) {
            btn_clearAppliedCoupon.click();
        }
        if (checkElementDisplay(btn_applyCouponnew)) {
            btn_applyCouponnew.click();
        }
    }



    public void applyCoupon(String couponName){
        clickonApplyCouponNormalCart();
        enterCoupon(couponName);
        clickonApplyCouponButton();
    }
    public void selectfirstapplicableCoupon(){
        btn_applyCouponnew.click();
        btn_applyfirstavailableCoupon.click();




    }public String getCouponAppliedDialogTitle(){
        return txt_couponAppliedDialogTitle.getText().toString();
    }
    public void clickOnOK(){
        if(checkElementDisplay(btn_okCouponApplied)) {
            btn_okCouponApplied.click();
        }
    }
    public String getCouponAppliedDialogMessage(){
        return txt_couponAppliedDialogMessage.getText().toString();}

    public void clickOnContinoueButton(){
        btn_continue.click();
    }

    public void cartSelectAddress() {
        cartSelectAddress.click();
    }

    public void selecHomeAddress() {
        clickOnSelectHomeAddress.click();
    }


    public boolean checkElementDisplay(IElement element) {
        try {
            if (element.isDisplayed()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }

    public boolean checkElementDisplay(List<IElement> element) {
        try {
            if (element.size() > 0) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }

    public void waitForElementToDisplay(List<IElement> elements) {
        int count = 5;
        while (!checkElementDisplay(elements) && count > 0) {
            AndroidUtil.sleep();
            count--;
        }
    }

    public void clickonViewDetailBill() {
        btn_viewDetailBill.click();
    }


    public void validateCartForMeal(EDVOMeal edvoMeal, ITestContext context) {
        AndroidUtil.sleep(5000);
        Assert.assertEquals(getRestaurantNameFromCart().toLowerCase(), "Domino's Pizza".toLowerCase());
        Assert.assertEquals(getItemNameFromCart(0).toLowerCase(), edvoMeal.getMealName().toLowerCase());
        Assert.assertEquals(getItemNameFromCart(1).toLowerCase(), context.getAttribute("itemName_1").toString().toLowerCase());
        Assert.assertEquals(getItemNameFromCart(2).toLowerCase(), context.getAttribute("itemName_2").toString().toLowerCase());
        Assert.assertEquals(getItemNameFromCart(2).toLowerCase(), context.getAttribute("itemName_2").toString().toLowerCase());


    }

    public String getMealPrice(int pos) {
        String str = txt_mealPrice.get(pos).getText().toString();
        String price = str.replaceAll("[^.0-9]", "");
        return price;
    }

    public void selectRepeatForIncreaseItemCount() {
        btn_repeatcustomization.click();
    }

    public String getMealNameOnCustomizationPopup() {
        return txt_mealNameOnCustomizationPopUp.getText().toString();
    }

    public String getPriceOnCustomizationPopup() {
        String str = txt_priceOnCustomizationPopUp.getText().toString();
        String price = str.replaceAll("[^.0-9]", "");
        return price;
    }

    public String getMealItemsOnCustomizationPopup() {
        String str = txt_mealItemOnCustomizationPopUp.getText().toString();
        return str;
    }

    public void clickonIwillCustomise() {
        btn_iWillCustomise.click();
    }

    public HashMap<String, Double> getRestaurantBillInfo() {
        HashMap<String, Double> hm = new HashMap<>();
        for (int i = 0; i < txt_priceInCart.size(); i++) {
            String str = txt_priceInCart.get(i).getText().toString();
            System.out.println("Hello = " + str);
            double price = Double.parseDouble(str.replaceAll("[^.0-9]", ""));
            hm.put(txt_priceInfo.get(i).getText().toString(), price);
            System.out.println("Hello PriceINfor= " + txt_priceInfo.get(i).getText().toString());

        }
        return hm;

    }

    public Double getRestaurantFinalBill() {
        String str = txt_finalPrice.getText().toString();
        double price = Double.parseDouble(str.replaceAll("[^.0-9]", ""));
        return price;
    }

    public int getItemCount() {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            //System.out.println(driver.findElement(By.id("add_to_cart_item_count_text")).getAttribute("content-desc").toString());
            System.out.println(driver.findElement(By.id("add_to_cart_item_count_text")).getAttribute("name").toString());

            return Integer.parseInt(driver.findElement(By.id("add_to_cart_item_count_text")).getAttribute("name").toString());
        } else {
            return Integer.parseInt(txt_itemCount.getText());
        }

    }

    public String getPreorderSla() {
        return txt_preorderSLA.getText().toString();
    }

    public void clickonPreorderSla() {
        txt_preorderSLA.click();
    }

    public boolean checkRemoveUnavailableButtonDisplay() {
        return checkElementDisplay(btn_removeUnavailable);
    }

    public void clickOnRemoveUnavailableButton() {
        btn_removeUnavailable.click();
    }

    public String getEmptyCartTitle() {
        return txt_emptyCartText.getText().toString();
    }

    public void ValidateCheckoutPageFromdeeplink(String restaurantName) {
        AndroidUtil.sleep(5000);
        Assert.assertEquals(getRestaurantNameFromCart(), restaurantName);
    }

    public String getCartItemCountOnBadge() {
        return txt_cartCountOnBadge.getText().toString();
    }

    public boolean isRepeatCustomizeButtonDisplay() {
        return checkElementDisplay(btn_repeatcustomization);
    }


    public void removeGuestFromCart() {
        btn_removeguest.click();
        AndroidUtil.sleep(3000);
        btn_yes.click();
    }

    public void clickOnAddressFromMultipleAddress(String addressName) {
        for (int i = 0; i < txt_AddressNameList.size(); i++) {
            if (txt_AddressNameList.get(i).getText().toString().toLowerCase().equalsIgnoreCase(addressName)) {
                txt_AddressNameList.get(i).click();
            }
        }
    }

    public String checkPastordertxtDisplayed() {
        return txt_pastOrder.getText();
    }

    public void validatecafecartwithtext() {

        scroll(Direction.UP);
        String Actual = "Pickup at Counter";
        String Expected = txt_pickupatcounter.getText();
        System.out.println("Expected text is :" + Expected);
        Assert.assertEquals(Actual, Expected);
        System.out.println("Success");
    }

    public void tapOnHowitworks() {
        if (checkElementDisplay(btn_howitworks_cart)) {
            btn_howitworks_cart.click();
        } else {
            System.out.println("How it works button not found");

        }
    }
    public void taponokgotit(){
        if(checkElementDisplay(btn_Okgotit))
        {
            btn_Okgotit.click();
        }else{
            System.out.println("Ok got it button not find");
        }
    }
    //TBD
    public boolean verifyhowitworksscree(){
        boolean b = checkElementDisplay(btn_Okgotit);
        return b;
    }


    //Cafe cases 
    public void cafescrollup(){
        scroll(Direction.UP);
        scroll(Direction.UP);
    }

    public void cafescrollDown(){
        scroll(Direction.DOWN);
        scroll(Direction.DOWN);
    }

    public void ioscafeapplycoupon(){
        if(checkElementDisplay(btn_cafeapplycoupon)) {
            btn_cafeapplycoupon.click();
        }else{
            btn_cafeapplycouponnew.click();
        }
    }
}


