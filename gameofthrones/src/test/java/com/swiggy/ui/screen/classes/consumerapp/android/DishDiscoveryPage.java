package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.JavascriptExecutor;

import java.util.HashMap;
import java.util.List;

public class DishDiscoveryPage extends AndroidUtil {


	public IElement storyTitle;
	public IElement descriptionTextView;
	public IElement btn_viewFullMenu;
	public List<IElement> btn_add;
	public List<IElement> txt_itemName;
	public IElement txt_Offer;
	public IElement txt_restaurantAddress;
	public IElement txt_restaurantName;
	public IElement btn_closeCollection;
	public IElement txt_TitleBar;
	public IElement txt_headerStoryEndPoint;
	public IElement txt_descriptionStoryEndPoint;

    public List<IElement> txt_countMessageLayout;




	private AppiumDriver<MobileElement> driver;




	public DishDiscoveryPage(InitializeUI  init) {
		super(init);
		initElements(init.getAppiumDriver(),this,init);
		driver=init.getAppiumDriver();
	}

	public void scrollHorizontal(){
		scrollHorizontal_new(driver);


	}public void scrollHorizontal(int time){
		scrollHorizontal_new(driver,time);
	}
	public void scrollHorizontalRight(){
		JavascriptExecutor js = driver;
		HashMap<String, String> scrollObject = new HashMap<String, String>();
		scrollObject.put("direction", "left");
		js.executeScript("mobile: scroll", scrollObject);


	}
	
	public String getCollectionCount()
	{
		return txt_countMessageLayout.get(2).getText();
	}
	
	public String getStoryTitle(){
		return storyTitle.getText().toString();
	}
	public String getStoryDescription(){
		return descriptionTextView.getText().toString();
	}

	public void clickonViewFullMenu(){
		btn_viewFullMenu.click();
	}
	public void clickonAddToCartButton(int pos){
		btn_add.get(pos).click();
	}
	public String getRestaurantName(){
		return txt_restaurantName.getText().toString();
	}
	public String getRestaurantAddress(){
		return txt_restaurantAddress.getText().toString();
	}
	public void clickonCloseCollectionbutton(){
		btn_closeCollection.click();
	}
	public String getOfferText(){
		return txt_Offer.getText().toString();
	}
	public String getTitleBar(){
		return txt_TitleBar.getText().toString();
	}
	public String getItemName(int pos){
		return txt_itemName.get(pos).getText();
	}
	public String getStoryEndPointHeader(){
		return txt_headerStoryEndPoint.getText();
	}
	public String getStoryEndPointDescription(){
		return txt_descriptionStoryEndPoint.getText();
	}
	public boolean checkElementDisplay(IElement element){
		try {
            return element.isDisplayed();
		}
		catch(Exception e){
			return false;
		}
	}

	public void validateDishDiscoveryFirstPage(){
		//ddStoryBoard=consumerApiHelper.getStoryBoardForDishDicovery("9886472081","qwerty",dishDiscoveryObject.getCollectionId().get(0),Double.toString(Constants.lat1),Double.toString(Constants.lng1),dishDiscoveryObject.getParentCollectionId().get(0));

		//home.openStoryCollection(1);
		AndroidUtil.sleep(10000);

		//System.out.println(dishDiscoveryPage.getStoryTitle());
		//System.out.println(dishDiscoveryPage.getStoryDescription());
		//Assert.assertEquals(dishDiscoveryPage.getStoryTitle(),context.getAttribute("storyTitle"));
		//Assert.assertEquals(dishDiscoveryPage.getStoryDescription().toLowerCase(),ddStoryBoard.getDescriptionStoryStartCard().toLowerCase());


	}






}
