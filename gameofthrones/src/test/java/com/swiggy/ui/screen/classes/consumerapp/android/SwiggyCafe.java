package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.junit.Assert;
import java.util.ArrayList;
import java.util.List;

public class SwiggyCafe extends AndroidUtil {
    private AppiumDriver driver;

    public IElement cafebarinlisting;
    public IElement btn_startorder_intro;
    public IElement btn_taponcorporate;
    public IElement enterpassword_corp;
    public IElement btn_unlock_corp;
    public List<IElement> restlisting_cafelisting;
    public IElement txt_cafescreen1;
    public IElement txt_cafescreen2;
    public IElement btn_change;
    public IElement btn_back_intro;
    public IElement btn_back_list;
    public IElement txt_foodcourt;
    public IElement txt_unlockscreen;
    public IElement image_lock_corp;
    public IElement oksorry;
    public IElement cross;
    public IElement btn_taponcorporatenew;
    public IElement listingelement;
    public SwiggyCafe(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        driver = init.getAppiumDriver();
    }


    public void tapOnCafebar()

    {
        if (checkElementDisplay(cafebarinlisting)) {
            cafebarinlisting.click();
        } else {
            System.out.println("Cafe bar on listing is not found");

        }
    }

    public void tapOnStartOrdering() {
        if (checkElementDisplay(btn_startorder_intro)) {
            btn_startorder_intro.click();
        } else {
            System.out.println("start ordering button is not found");
        }
    }


    public void tapOnCooperate() {
        if (checkElementDisplay(btn_taponcorporate)) {
            btn_taponcorporate.click();
        } else {
            btn_taponcorporatenew.click();
            System.out.println("Cooperates are not found in one of the device");
        }
    }






    public void enterPasscode() {
        if(checkElementDisplay(enterpassword_corp)) {
            enterpassword_corp.click();
            enterpassword_corp.sendKeys("SWIG18");
            btn_unlock_corp.click();
        }else{
            System.out.println("Passcode field  not found");
        }
    }

    public void enterinvalidPasscode() {
        if(checkElementDisplay(enterpassword_corp)) {
            enterpassword_corp.click();
            enterpassword_corp.sendKeys("SWIG1");
            btn_unlock_corp.click();
            oksorry.click();
        }else{
            System.out.println("Passcode field  not found");
        }
    }


    public ArrayList<String> getAllText(List<IElement> listRestaurant) {
        ArrayList<String> al = new ArrayList<>();
        for (int i = 0; i < listRestaurant.size(); i++) {
            System.out.println(listRestaurant.get(i).getText());
            al.add(listRestaurant.get(i).getText());

        }
        return al;

    }
    public void tapOnfirstCafeoutlet(String restName, int pos) {


        if (restName != null) {
            ArrayList<String> al = getAllText(restlisting_cafelisting);
            System.out.println(al);
            if (al.contains(restName)) {
                restlisting_cafelisting.get(al.indexOf(restName)).click();
            }
        }
                else {
            restlisting_cafelisting.get(pos).click();



        }
    }

    public void validatestartorderingpage() {
        String Actual = "Order online from eateries in your office or campus - without waiting!";
        String Expected = txt_cafescreen1.getText();
        System.out.println("Expected value:" +Expected);
        Assert.assertEquals(Actual, Expected);
        System.out.println("Success");
    }

    public void validateworkspacepage(){
        String Actual = "Order online from eateries in your office or campus - without waiting!";
        String Expected = txt_cafescreen2.getText();
        System.out.println("Expected value:" +Expected);
        Assert.assertEquals(Actual, Expected);
        System.out.println("Success");

    }
    public void tapOnChange() {
        if (checkElementDisplay(btn_change)) {
            btn_change.click();
            cross.click();
        } else {
            System.out.println(" Change button not found");
        }
    }
        public void taponbackbutton(){
            if(checkElementDisplay(btn_back_intro)){
                btn_back_intro.click();
            }else{
                System.out.println("Back button not found");
            }
    }
    public  void taponbacklist(){
        if(checkElementDisplay(btn_back_list)){
            btn_back_list.click();
        }else {
            System.out.println("Backbutton not found");
        }
    }

    public void validatecafelistpage(){
        String Actual = "SWIGGY FOOD COURT";
        String Expected = txt_foodcourt.getText();
        System.out.println("Expected value:" +Expected);
        Assert.assertEquals(Actual, Expected);
        System.out.println("Success");

    }

    public void validateunlockscreen(){
        String Actual = "UNLOCK USING PASSCODE";
        String Expected = txt_unlockscreen.getText();
        System.out.println("Expected value:" +Expected);
        Assert.assertEquals(Actual, Expected);
        System.out.println("Success");

    }

    public boolean getUnlockPasscodestatus(){

      return checkElementDisplay(txt_unlockscreen);

    }

public Boolean verifylockimagestatus()
    {
       return checkElementDisplay(image_lock_corp);

    }

    public Boolean verifylockimage()
    {
        return checkElementDisplay(image_lock_corp);


    }

    public Boolean listingelementstatus()
    {
       return checkElementDisplay(listingelement);

    }



    }
