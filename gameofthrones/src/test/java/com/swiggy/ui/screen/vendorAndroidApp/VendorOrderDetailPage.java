package com.swiggy.ui.screen.vendorAndroidApp;

import com.swiggy.ui.page.classes.vendor.dashboard.VmsHelper;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;

import java.util.HashMap;
import java.util.List;

public class VendorOrderDetailPage extends AndroidUtil {

    public IElement btn_confirmOrder;
    public IElement btn_done;
    public IElement btn_plus;
    public IElement btn_minus;
    public IElement txt_timeOnDialog;
    public IElement txt_firstPartOrderNumber;
    public IElement txt_SecondPartOrderNumber;
    public IElement txt_prepTimeLeft;
    public IElement txt_orderStatus;
    public List<IElement> alltext;
    public IElement txt_ItemName;
    public IElement txt_deliveryExecutive;
    public IElement btn_call;
    public IElement txt_deName;
    public IElement txt_deStatus;
    public IElement txt_dialogTitle;
    public IElement txt_dialogSubTitle;
    public IElement btn_OK;
    public IElement btn_cancelOutOfStock;
    public IElement btn_markOOS;
    public IElement btn_oos;
    public List<IElement> listAlternativesForOos;
    public IElement btn_submitAlternates;
    public IElement btn_doneOos;
    public IElement txt_orders;
    public IElement txt_awaitingcall;
    public IElement btn_help;
    public IElement btn_yes;
    public IElement btn_no;
    public IElement txt_global_preptime;
    public IElement txt_quantity;
    public IElement txt_itemprice;
    public IElement map_google;


    AppiumDriver driver;








    public VendorOrderDetailPage(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
        driver=inti.getAppiumDriver();
    }

    public void clickOnConfirmOrderButton(){
        if(checkElementDisplay(btn_confirmOrder)){
            btn_confirmOrder.click();
        }


    }
    public String getPreprationTime(){
       return  txt_timeOnDialog.getText().toString();
    }

    public void increasePreprationTimeToMax(){
        for(int i=0;i<30;i++){
            btn_plus.click();
            AndroidUtil.sleep(500);
        }
    }
    public int increasePreprationTime(int noOfTime){
        for(int i=0;i<noOfTime;i++){
            btn_plus.click();
            AndroidUtil.sleep(500);
        }
//        return Integer.parseInt(txt_timeOnDialog.getText().toString().split(" ")[0]);
        return 0;
    }
    public void decreasePreprationTimeToMin(){
        for(int i=0;i<30;i++){
            btn_minus.click();
            AndroidUtil.sleep(500);
        }

    }


    public boolean checkElementDisplay(IElement element){
        try{
            if(element.isDisplayed()){
                return true;
            }
        }
        catch(Exception e){
            return false;
        }
        return false;

    }

    public void clickOnDoneButton(){
        if(checkElementDisplay(btn_done)){
            btn_done.click();
        }
    }

    public String getOrderNumber(){
        String text=txt_firstPartOrderNumber.getText()+txt_SecondPartOrderNumber.getText();
        return text.replaceAll("[^.0-9]", "");
    }
    public String getPrepTimeLeft(){
       return txt_prepTimeLeft.getText().toString();
    }
    public String getOrderStatus(){
        return txt_orderStatus.getText().toString();
    }
    public String getItemName(){
        return txt_ItemName.getText().toString();
    }
    public String getTotalBill(){
        for(int i=0;i<alltext.size();i++){
            System.out.println(alltext.get(i).getText());
            if(alltext.get(i).getText().contains("Bill Total")){
                return alltext.get(i+1).getText().replaceAll("[^.0-9]", "");
            }
        }
        return null;
    }

    public void scrollTillDeliveryExecutive(){
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 800).moveTo(50, 300).release().perform();
        int i=2;

        while(i>0 && !checkElementDisplay(txt_deliveryExecutive)){
            touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            i--;
        }
    }
    public void scrollTillCallButton(){
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 800).moveTo(50, 300).release().perform();
        int i=2;

        while(i>0 && !checkElementDisplay(btn_call)){
            touchAction.longPress(50, 1000).moveTo(50, 500).release().perform();
            i--;
        }
    }
    public boolean isDeliveryExecutiveDisplay(){
        return checkElementDisplay(txt_deliveryExecutive);
    }

    public String getDeliveryExecutiveName(){
        return txt_deName.getText().toString();
    }
    public String getDeliveryExecutiveStatus(){
        return txt_deStatus.getText().toString();
    }

    public String getDialogTitle(){
        return txt_dialogTitle.getText().toString();
    }
    public String getDialogSubTitle(){
        return txt_dialogSubTitle.getText().toString();
    }

    public void clickOnOkButton(){
        btn_OK.click();
    }


    public void clickOnMarkOOS(){
        btn_markOOS.click();
    }

    public void clickOnButtonCancelForMarkOOS(){
        btn_cancelOutOfStock.click();
    }

    public void selectAlternativesForOOS(){
        listAlternativesForOos.get(0).click();
    }
    public void clickOnButtonDoneForOOS(){
        btn_doneOos.click();
    }
    public void clickOnSubmitAlternates(){
        btn_submitAlternates.click();
    }
    public void clickOnButtonOOS(){
        btn_oos.click();
    }


    public void returnToHomePage() {
        int counter = 5;
        while(!checkElementDisplay(txt_orders) && counter > 0) {
            AndroidUtil.pressBackKey(driver);
            counter--;
        }
    }

    public boolean isAwaitingCallDisplayed() {
        return checkElementDisplay(txt_awaitingcall);
    }

    public VendorOrderDetailPage tapOnHelp() {
        btn_help.click();
        return this;
    }

    public VendorOrderDetailPage acceptCallRequest() {
        btn_yes.click();
        return this;
    }

    public VendorOrderDetailPage dismissCallRequest() {
        btn_no.click();
        return this;
    }

    public String getGlobalPrepTime() {
        return txt_global_preptime.getText().replaceAll("[^.0-9]", "").replace("0","");
    }

    public boolean verifyDetails(String order_id) {
        HashMap<String, Object> map = new HashMap<>();
        VmsHelper vmsHelper = new VmsHelper();
        map = vmsHelper.orderDetails(order_id);
        System.out.println("item_name is"+txt_ItemName.getText());
        System.out.println("item_price is "+ txt_itemprice.getText());
        System.out.println(getTotalBill());
        return map.get("item_name").toString().equalsIgnoreCase(txt_ItemName.getText()) &&
                map.get("total").toString().equalsIgnoreCase(getTotalBill()) &&
                map.get("item_price").toString().equalsIgnoreCase(txt_itemprice.getText());
    }

    public boolean isMapDisplayed() {
        return checkElementDisplay(map_google);
    }


}


