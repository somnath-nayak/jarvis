package com.swiggy.ui.screen.vendorAndroidApp;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class VendorLoginPage extends AndroidUtil{

    public IElement input_username;
    public IElement input_password;
    public IElement btn_login;
    public IElement btn_continue;
    private AppiumDriver<MobileElement> driver;
    //public LaunchPageObject launhpageobject = new LaunchPageObject();

    public VendorLoginPage(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
        driver=inti.getAppiumDriver();
    }

    public void clickonLoginButton(){
        btn_login.click();
    }

    public void performLogin(String username,String password){
        input_username.sendKeys(username);
        AndroidUtil.hideKeyboard(driver);
        btn_continue.click();
        input_password.sendKeys(password);
        AndroidUtil.hideKeyboard(driver);

    }



}
