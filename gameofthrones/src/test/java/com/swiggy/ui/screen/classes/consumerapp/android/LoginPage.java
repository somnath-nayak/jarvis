package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;

public class LoginPage extends AndroidUtil {

	public IElement MobileNumber;
	public IElement NewUser;
	public IElement Continue;
	public IElement Password;
	public IElement LoginButton;
	public IElement edit_signupEmail;
	public IElement Permission;
	public IElement allowLocationAccess;
	public IElement Account;
	public IElement Dont_allow;
	public IElement edit_signupName;
	public IElement edit_signupPassword;
	public IElement checkbox_ReferralCode;
	public IElement edit_enterReferralCode;
	public IElement btn_signup;
	public IElement txt_enterMobileNumber;
	public IElement txt_enterPassword;
	public IElement edit_signupMobilenumberios;
	public IElement edit_signupEmailAddresios;
	public IElement edit_signupNameios;
	public IElement edit_signupPasswordios;
	public IElement checkbox_ReferralCodeios;
	public IElement edit_enterReferralCodeios;

	AppiumDriver driver;
	HomePage homepage;
	LaunchPage launchPage;








	public LoginPage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(),this,init);
		homepage=new HomePage(init);
		driver=init.getAppiumDriver();
		launchPage=new LaunchPage(inti);

	}
	
	public void EnterMobileNo(String mobileno) {
		MobileNumber.click();
		MobileNumber.sendKeys(mobileno);


	}
	
	public boolean isLoggedIn()
	{
		return Account.isDisplayed();
	}
	
	public void TapOnLogin() {
		LoginButton.click();
	}
	
	public void EnterPassword(String password) {
		Password.click();
		Password.sendKeys(password);

	}
	
	public void LogintoApp(String mobileno, String password) {
		if (checkElementDisplay(MobileNumber)) {
			this.EnterMobileNo(mobileno);
			Continue.click();
			this.EnterPassword(password);
			this.TapOnLogin();
			launchPage.clickonAllowButton();
			try {
				if (Permission.isDisplayed()) {
					Permission.click();
				}
			} catch (Exception e) {

			}
			//homepage.removeSwiggyPop();

		}
		//Loginfromcartios
		else {
			//txt_enterMobileNumber.click();
			txt_enterMobileNumber.sendKeys(mobileno);
			Continue.click();
			//txt_enterPassword.click();
			txt_enterPassword.sendKeys(password);
			Continue.click();
			launchPage.clickonAllowButton();
			try {
				if (Permission.isDisplayed()) {
					Permission.click();
				}
			} catch (Exception e) {

			}
			//homepage.removeSwiggyPop();

		}
	}
	public void clickonAllowButton(){
		if(checkElementDisplay(allowLocationAccess)){
			allowLocationAccess.click();
		}
	}




	public void TapOnContinue() {
		Continue.click();

	}

	public void AllowPermission() {
		Permission.click();

    }
    public void enterName(){
		edit_signupName.click();
		edit_signupName.sendKeys("test");
		driver.hideKeyboard();

	}
	public void enterEmailId(){
    	edit_signupEmail.clear();

		edit_signupEmail.sendKeys("test55555@gmail.com");

	}
	public void enterPassword(){
		edit_signupPassword.click();
		edit_signupPassword.sendKeys("qwertyuiop");
		driver.hideKeyboard();
		}

		public void singupWithoutReferral(){
			if(inti.platformnew.equalsIgnoreCase("Android"))

			 {
			 	EnterMobileNo("1231231212");
				Continue.click();
				clickonAllowButton();
				enterName();
				enterPassword();
				enterEmailId();



				 btn_signup.click();
			}
			else
			{
				EnterMobileNo("1231231212");
				Continue.click();
				//edit_signupNameios.click();
				edit_signupNameios.sendKeys("test");
				driver.hideKeyboard();
				edit_signupPasswordios.click();
				edit_signupPasswordios.sendKeys("qwerty");

				driver.hideKeyboard();
				edit_signupEmailAddresios.click();
				edit_signupEmailAddresios.sendKeys("test@gmail.com");
				driver.hideKeyboard();
				btn_signup.click();

			}



		}
		public void singupWithReferral(){
			if(inti.platformnew.equalsIgnoreCase("Android")) {
				EnterMobileNo("1231231212");
				Continue.click();
				clickonAllowButton();
				enterEmailId();
				enterName();
				enterPassword();
				enterEmailId();
				btn_signup.click();
				checkbox_ReferralCode.click();
				edit_signupName.click();
			}
			else {
				EnterMobileNo("1231231212");
				Continue.click();
				edit_signupNameios.click();
				edit_signupNameios.sendKeys("test");
				driver.hideKeyboard();
				edit_signupPasswordios.click();
				edit_signupPasswordios.sendKeys("qwerty");

				driver.hideKeyboard();
				edit_signupEmailAddresios.click();
				edit_signupEmailAddresios.sendKeys("test@gmail.com");
				driver.hideKeyboard();

				checkbox_ReferralCodeios.click();
				edit_signupNameios.click();



			}

	}
	public boolean checkElementDisplay(IElement element) {
		try {
            return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}



	
}
