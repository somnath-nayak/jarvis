package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExplorePage extends AndroidUtil {

	public IElement input_search;
	public List<IElement> txt_tab;
	public List<IElement> allText;
	public IElement disheshSuggestionList;

	HomePage homePage;
	RestaurantPage restaurantPage;
	AddressPage addressPage;
	CheckoutPage checkoutPage;
	public List<IElement> test;
	public IElement btn_addCustomItem;
	public List<IElement> addtoCart;
	public List<IElement> list_dishesh;//android.support.v7.widget.RecyclerView/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView
	AppiumDriver driver;
	public IElement btn_fullMenu;
	public IElement restaurantName;
	public IElement dd_titleText;
	public IElement btn_crossSearch;
	public IElement btn_dishesTab;
	public IElement tab_searchResult;
    public IElement btn_DD_viewMore;



	public ExplorePage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(), this, init);
		homePage = new HomePage(init);
		restaurantPage = new RestaurantPage(init);
		driver = init.getAppiumDriver();
		addressPage = new AddressPage(init);
		checkoutPage = new CheckoutPage(init);
	}

	public void doSearch(String name) {
		input_search.click();
		input_search.sendKeys(name);
	}

	public void selectTabonExplorePage(String tabName) {
		if (inti.platformnew.equalsIgnoreCase("Android")) {
			clickonText(null, txt_tab, tabName);
		} else {
			clickOnTab(tabName);
		}
	}

	public boolean validateSearchText(String searchText) {
		ArrayList<String> al = new ArrayList<>();
        return al.contains("Related to \"" + searchText + "\"");
	}

	public void clickonText(IElement element, List<IElement> elements, String text) {

		if (elements == null) {
			if (element.getText().equalsIgnoreCase(text)) {
				element.click();
			}
		} else {
			for (int i = 0; i < elements.size(); i++) {
				System.out.println("Prod Name " + elements.get(i).getText() + " searched name " + text);

				if (elements.get(i).getText().equalsIgnoreCase(text)) {
					System.out.println("Foundout");
					elements.get(i).click();
					break;
				}
			}
		}
	}

	public ArrayList<String> getAllText(List<IElement> listRestaurant) {
		ArrayList<String> al = new ArrayList<>();
		for (int i = 0; i < listRestaurant.size(); i++) {
			al.add(listRestaurant.get(i).getText());

		}
		return al;

	}

	public void openRestaurantfromExplore() {

		//TODO - homepage.selectRestaurant method
		homePage.selectRestaurant("", 0);
	}

	public void addToCartFromfromExplore() {


		addItemtest();

		//TODO - restaurantPage.addtoCart
	}

	public void selectDishes(String name, int pos) {
		if (name == null || name.isEmpty()) {
			list_dishesh.get(pos).click();

		} else {
			//List<WebElement> temp=list_dishesh
			for (int i = 0; i < list_dishesh.size(); i++) {
				if (list_dishesh.get(pos).getText().toString().equalsIgnoreCase(name)) {
					list_dishesh.get(pos).click();
					break;
				}
			}
		}




        /*System.out.println(allText.size());
        for(int i=0;i<allText.size();i++){
            System.out.println(allText.get(i).getText().toString());
            if(allText.get(i).getText().toString().contains("Punjabi")){
                allText.get(i).click();
                break;
            }
        }*/


		/*List<WebElement> elements=disheshSuggestionList.findElements(By.className("android.widget.LinearLayout"));
		if(name==null){
			disheshSuggestionList.
		}
		else{
			for(int i=0;i<elements.size();i++){
			    System.out.println(elements.get(i).findElement(By.className("android.widget.LinearLayout")).findElement(By.className("android.widget.TextView")).getText().toString());
				//if(elements.get(i).getText().toString().equalsIgnoreCase(name)){
				//	elements.get(i).click();
				//}
			}
		}*/
	}

	public String searchDishesAndAddToCart(String dishName) {

			homePage.openExploreTab();
			doSearch(dishName);
			AndroidUtil.hideKeyboard(driver);
			selectTabonExplorePage("Dishes");
			//AndroidUtil.sleep(5000)

			selectDishes(null, 0);
			AndroidUtil.sleep(5000);
			String restaurantName = getRestaurantNameFromSearchDishes();
			addToCartFromfromExplore();
			return restaurantName;

	}





	public void openRestaurantAndAddTocCart(int pos){
		homePage.selectRestaurant(null,pos);
		restaurantPage.removeCoachmark();
		restaurantPage.filterMenu(0);
		restaurantPage.addItemtest();
	}
	public void openRestaurantFromSearchScreen(int pos){
		homePage.selectRestaurant(null,pos);

	}

	public void searchRestaurant(String restaurantName){
		homePage.openExploreTab();
		doSearch(restaurantName);
		addressPage.hideKeyboard();

	}
	public void searchRestaurantios(String restaurantName){
		homePage.openExploreTab();
		doSearch(restaurantName);
		addressPage.hideKeyboard();
		//tab_searchResult.click();


	}
	public void addItemtest() {
		//restaurantPage.addItemtest();
		if(restaurantPage.checkElementDisplay(restaurantPage.addtoCartNew)){
			restaurantPage.clickonAddTo();}


        /*WebElement element=test.get(0).findElement(By.id("add_to_cart_customisation_text"));
        if(element.getText().toString().equals("Customizable")){
            addtoCart.get(0).click();
            btn_addCustomItem.click();
        }
        else{
            addtoCart.get(0).click();
        }*/

	}

	public void clickonFullMenu(){
		if(inti.platformnew.equalsIgnoreCase("Android")) {
			scrollTillElementVisible(btn_fullMenu);
		}
		if(inti.platformnew.equalsIgnoreCase("ios")){
			//scrollTillElementInIos("FULL MENU");
		}

		btn_fullMenu.click();
	}

	public void scrollTillElementVisible(IElement element){
		int count=0;
		while(!checkElementDisplay(element) && count<3){
			TouchAction touchAction = new TouchAction(driver);
			touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
			count++;
		}
	}
	public boolean checkElementDisplay(IElement element){
		try{
			if(element.isDisplayed()){
				return true;
			}
		}
		catch(Exception e){
			return false;
		}
		return false;

	}
	public String getRestaurantNameFromSearchDishes(){
		return restaurantName.getText().toString();
	}

	public String getCollectionTitleText(){
		return dd_titleText.getText().toString();
	}
	public void clearSearch(){
		btn_crossSearch.click();
	}


	public void validateExploreFromDeeplink(String searchKey) {
		searchRestaurant(searchKey);
		String restaurantName=homePage.getRestaurantNameFromList(0);
		homePage.selectRestaurant(null,0);
		restaurantPage.removeCoachmark();
		AndroidUtil.sleep();
		//Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),restaurantName.toLowerCase());
		restaurantPage.addItemtestForDeeplink();
		restaurantPage.openCartFromRestaurantPage();
		//Continue code should be there to go to cart
		checkoutPage.btn_continue.click();
		checkoutPage.clickOnProceedToPay();

	}
	public void validateExploreFromDeeplinkios(String searchKey) {

		doSearch(searchKey);
		//String restaurantName=homePage.getRestaurantNameFromList(0);
		//homePage.selectRestaurant(null,0);


	}

	public void validateExploreFromDeeplinkForNonLoggedIn(String searchKey) {
		doSearch(searchKey);
		addressPage.hideKeyboard();
		String restaurantName=homePage.getRestaurantNameFromList(0);
		/*homePage.selectRestaurant(null,0);
		restaurantPage.removeCoachmark();
		AndroidUtil.sleep();
		//Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),restaurantName.toLowerCase());
		restaurantPage.filterMenu(0);
		restaurantPage.addItemtestForDeeplink();
		restaurantPage.openCartFromRestaurantPage();
		Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());*/

		//Continue code should be there to go to cart


	}

	public void clickOnTab(String tabName){
		if(tabName.equalsIgnoreCase("Dishes")){
			btn_dishesTab.click();
		}
	}


	public void scrollTillElementInIos(String text) {
		System.out.println("   tapItemByDescription(): " + text);

		// scroll to item
		JavascriptExecutor js = driver;
		HashMap scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "value == '" + text + "'");
		js.executeScript("mobile: scroll", scrollObject);

		// tap item
		WebElement el = ((IOSDriver) driver).findElementByIosNsPredicate("value = '" + text + "'");

	}
	public void clickDDfastforward(){

			if(checkElementDisplay(btn_DD_viewMore)){
				btn_DD_viewMore.click();
			}
			else {
				System.out.print("viewMore option is not displayed");
			}

	}


}
