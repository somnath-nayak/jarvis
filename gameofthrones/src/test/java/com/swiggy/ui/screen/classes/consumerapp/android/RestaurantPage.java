package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pojoClasses.EDVOMeal;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.swiggy.ui.utils.AndroidUtil.Direction.UP;
import static org.testng.Assert.assertTrue;


public class RestaurantPage extends AndroidUtil {

    public IElement menuPopUpFirstTime;
    public List<IElement> addtoCart;
    public List<IElement> addtoCartNew;
    public List<IElement> allText;
    public IElement btn_Menu;
    public IElement btn_checkout;
    public List<IElement> test;
    public IElement btn_addCustomItem;
    public List<IElement> txt_categoryName;
    public List<IElement> txt_categoryItemCount;
    public List<IElement> btn_exapandArrow;
    public IElement btn_toggleVeg;
    public IElement txt_ItemCategoryName;
    public List<IElement> txt_ItemName;
    public IElement btn_repeatcustomization;
    public IElement txt_restaurantName;
    public IElement txt_rating;
    public IElement preOrderOnboarding;
    public IElement set_deliveryTime;
    public IElement txt_priceofTwo;
    public IElement icon_swiggyAssured;
    public IElement fav_icon;
    public IElement icon_pureVeg;
    public IElement icon_search;
    public IElement input_searchbox;
    public IElement txt_ReplaceCartItemdialogTitle;
    public IElement txt_ReplaceCartItemdialogMessage;
    public IElement btn_ReplaceCartItemdialog_No;
    public IElement btn_ReplaceCartItemdialog_Yes;
    public IElement txt_ItemNameOnRepeatDialog;
    public List<IElement> txt_mandotryCust;
    //EDVO

    public IElement txt_edvoHeader;
    public IElement txt_edvoSubHeader;
    public IElement image_firstEDVO;
    public IElement image_SecondEDVO;
    public IElement txt_priceSubHeaderFirstEDVO;
    public IElement txt_priceSubHeaderSecondEDVO;
    public IElement txt_EDVOlandingPage_header;
    public IElement txt_EDVOlandingPage_mealDescription;
    public IElement txt_EDVOlandingPage_mealSubText;
    public List<IElement> txtEDVOlandingPage_groupName;
    public List<IElement> txtEDVOlandingPage_groupDescp;
    public IElement btn_back;
    public IElement btn_Continue;
    public IElement txt_mealDescription_SelectPizzaScreen;
    public IElement txt_mealTitle_SelectPizzaScreen;
    public List<IElement> txt_groupName_SelectPizzaScreen;
    public IElement btn_Continue_addOnScreen;
    public IElement btn_AddItem_addOnScreen;
    public List<IElement> checkBox_addOnScreen;
    public IElement btn_Continue_SelectPizzaScreen;
    public List<IElement> icon_mealSelected;
    public IElement txt_saved_ConfirmationScreen;
    public IElement btn_ViewCart_ConfirmationScreen;
    public IElement btn_AddMoreItem_ConfimationScreen;
    public IElement txt_MaxAllowedSnackBar;
    public IElement btn_okyaGotSnackBar;
    public IElement image_brandLogo;
    public IElement btn_searchCancel;
    public IElement btn_iWillChoose;
    public List<IElement> radioBtn_addCus;
    public List<IElement> checkbox_addCus;
    public IElement btn_discardcart;
    public IElement btn_keepcart;
    public IElement txt_groupordercreate;
    public IElement grouporder_icon1;
    public IElement txt_POPCartcleartMessage;
    public IElement txt_POPCartcleartTitle;
    public List<IElement> btn_select;
    public IElement btn_ReviewOrder;
    public IElement selecttimeSlot;
    public IElement btn_back_cart;
    public IElement btn_Now;


    //dd
    public IElement txt_dishdiscoveryTitle;
    public IElement btn_dishdiscoveryBackButton;

    //Group_order
    public IElement grouporder_icon;

    public IElement grouporder_created_text;
    public IElement txt_deliveryTime;
    public IElement txt_guestjoinedgrouporder;

    //Hygiene
    public IElement image_hygiene;
    public IElement btn_back_HR;
    public IElement txt1_rating;
    public IElement btn_feedback;
    public IElement btn_cancel;
    public IElement btn_deletedraft;
    public IElement btn_continueordering;
    public IElement txt_deliverytime;

    //FSSAI
    public IElement fssai_status;
    public IElement fssai_section;
    public IElement fixed_fee_cruton;
    public IElement fssaiImage;
    public IElement txt_menurestnameintitle;



    CheckoutPage checkoutPage;
    AppiumDriver driver;


    public RestaurantPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        checkoutPage = new CheckoutPage(init);
        driver = init.getAppiumDriver();
    }

    public void scrollDownOnPopPage() {


        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();

    }

    public void removeCoachmark() {
        //ArrayList<String> al=new ArrayList<>();
        //al=getAllText();
        AndroidUtil.sleep();

        if (inti.platformnew.equalsIgnoreCase("Android")) {
            clickonText(null, allText, "Exploring the food menu has become ever so easier");
        } else {

        }
        /*if(al.contains("Exploring the food menu has become ever so easier")){
            allText.get(al.indexOf("Exploring the food menu has become ever so easier")).click();
		}*/

    }


    public ArrayList<String> getAllText() {
        ArrayList<String> al = new ArrayList<>();
        for (int i = 0; i < allText.size(); i++) {
            System.out.println(allText.get(i).getText());
            al.add(allText.get(i).getText());

        }
        return al;
    }

    public void addItem() {
        addtoCart.get(0).click();

    }

    public void changeTimeSlotfromMenu() {
        AndroidUtil.sleep(1000);
        selecttimeSlot.click();

    }

    public String addItemtest() {

        if (inti.platformnew.equalsIgnoreCase("Android")) {
            if (checkElementDisplay(addtoCart) || checkElementDisplay(addtoCartNew)) {
                if (getCategoryName().equalsIgnoreCase("recommendation")) {
                    TouchAction touchAction = new TouchAction(driver);
                    touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
                }
                scrollDownOnPopPage();
                clickonAddTo();
            } else {
                if (checkElementDisplay(btn_exapandArrow)) {
                    btn_exapandArrow.get(0).click();
                    clickonAddTo();
                }
            }
        } else {
            addItemToCartForIOs();
        }
        //return getItemName(0);
        return null;
    }

    public String addItemtest(int pos) {


        if (checkElementDisplay(addtoCart)) {
            if (getCategoryName().equalsIgnoreCase("recommendation")) {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            }
            clickonAddTo(pos);
        } else {
            if (checkElementDisplay(btn_exapandArrow)) {
                btn_exapandArrow.get(0).click();
                clickonAddTo(pos);
            }
        }
        //return getItemName(0);
        return null;
    }

		/*addtoCart.get(0).click();
        if(btn_addCustomItem.isDisplayed()){
			btn_addCustomItem.click();
		}


		for(int i=0;i<test.size();i++){
			if(test.get(i).isDisplayed()){
				System.out.println(test.get(i).getText().toString());
			}
		}

		WebElement element=test.get(0).findElement(By.id("add_to_cart_customisation_text"));
		if(element.getText().toString().equals("Customizable") && element.isDisplayed()){
			addtoCart.get(2).click();
			btn_addCustomItem.click();
		}
		else{
			addtoCart.get(2).click();
		}*/

    public String addItemtestForDeeplink() {
        if (checkElementDisplay(addtoCart) || checkElementDisplay(addtoCartNew)) {
            if (getCategoryName().equalsIgnoreCase("recommendation")) {
                TouchAction touchAction = new TouchAction(driver);
                touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            }
            clickonAddToForDeeplink();
        } else {
            if (checkElementDisplay(btn_exapandArrow)) {
                btn_exapandArrow.get(0).click();
                clickonAddToForDeeplink();
            }


        }

        return null;
    }

    public void clickonAddToForDeeplink() {
        if (checkElementDisplay(addtoCart)) {
            addtoCart.get(0).click();
        } else {
            addtoCartNew.get(0).click();
        }


        if (checkElementDisplay(btn_ReplaceCartItemdialog_Yes)) {
            clickonReplaceCartItemDialogYes();
        }

        while (!checkElementDisplay(btn_addCustomItem)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            } else {
                System.out.println("Hello inside elese");
                if (checkElementDisplay(btn_addCustomItem)) {
                    btn_addCustomItem.click();
                }
                break;
            }

        }
        if (checkElementDisplay(btn_addCustomItem)) {
            btn_addCustomItem.click();
        }
    }


    public void clickonAddTo() {
        if (checkElementDisplay(addtoCart)) {
            addtoCart.get(0).click();
        } else {
            addtoCartNew.get(0).click();
        }

        while (!checkElementDisplay(btn_addCustomItem)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            } else {
                System.out.println("Hello inside elese");
                if (checkElementDisplay(btn_addCustomItem)) {
                    if (checkMandotryCustRequire()) {
                        selectMandotryCust();
                    }
                    btn_addCustomItem.click();
                } else {
                    if (checkElementDisplay(radioBtn_addCus)) {
                        chooseAllCust();
                    }
                }

                break;
            }

        }
        if (checkElementDisplay(btn_addCustomItem)) {
            if (checkMandotryCustRequire()) {
                selectMandotryCust();
            }
            if (checkElementDisplay(radioBtn_addCus)) {
                chooseAllCust();
            }
            btn_addCustomItem.click();
        }
    }

    public void clickonCustomButtonForAddTo() {
        while (!checkElementDisplay(btn_addCustomItem)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            } else {
                System.out.println("Hello inside elese");
                if (checkElementDisplay(btn_addCustomItem)) {
                    if (checkMandotryCustRequire()) {
                        selectMandotryCust();
                    }
                    btn_addCustomItem.click();
                } else {
                    if (checkElementDisplay(radioBtn_addCus)) {
                        chooseAllCust();
                    }
                }

                break;
            }

        }
        if (checkElementDisplay(btn_addCustomItem)) {
            if (checkMandotryCustRequire()) {
                selectMandotryCust();
            }
            if (checkElementDisplay(radioBtn_addCus)) {
                chooseAllCust();
            }
            btn_addCustomItem.click();
        }
    }


    public void clickonAddTo(int pos) {
        addtoCart.get(pos).click();

        while (!checkElementDisplay(btn_addCustomItem)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            } else {
                System.out.println("Hello inside elese");
                if (checkElementDisplay(btn_addCustomItem)) {
                    btn_addCustomItem.click();
                }
                break;
            }

        }
        if (checkElementDisplay(btn_addCustomItem)) {
            btn_addCustomItem.click();
        }
    }

    public void clickOnExpandArrow(int pos) {
        if (checkElementDisplay(btn_exapandArrow)) {
            btn_exapandArrow.get(pos).click();
        }
    }

    public void clickonAddToForIncreaseItemCount(int pos) {

        addtoCart.get(0).click();
        while (!checkElementDisplay(btn_addCustomItem)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            } else {
                if (checkElementDisplay(btn_addCustomItem)) {
                    btn_addCustomItem.click();
                }
                break;
            }

        }
    }

    public void clickOnAddItemCustimization() {
        if (checkElementDisplay(btn_addCustomItem)) {
            btn_addCustomItem.click();
        }
    }

    public String getItemName(int pos) {
        try {
            String name = txt_ItemName.get(pos).getText().toString();
            System.out.println("ItemName = " + name);
            return name;
        } catch (Exception e) {
            System.out.println("Item not found");
            return null;
        }

    }

    public void clickMenu() {
        btn_Menu.click();
    }

    public void openCartFromRestaurantPage() {
        sleep();
        if (checkElementDisplay(btn_checkout)) {
            System.out.println("Button display");
            btn_checkout.click();

        } else {
            System.out.println("Button not display");
            TouchAction touchAction = new TouchAction(driver);
            touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
            touchAction.longPress(50, 100).moveTo(50, 600).release().perform();
            touchAction.longPress(50, 100).moveTo(50, 600).release().perform();

            if (checkElementDisplay(btn_checkout)) {
                System.out.println("Button second display");
                btn_checkout.click();
            }
        }
        checkoutPage.clickOk();


    }

    public void filterMenu(String menuName) {
        btn_Menu.click();
        for (int i = 0; i < txt_categoryName.size(); i++) {
            if (txt_categoryName.get(i).getText().equalsIgnoreCase(menuName)) {
                txt_categoryName.get(i).click();
                break;
            }
        }
        TouchAction touchAction = new TouchAction(driver);
        //touchAction.longPress(50, 600).moveTo(50, 500).release().perform();

    }

    public void filterMenu(int pos) {
        btn_Menu.click();
        txt_categoryName.get(pos).click();
    }

    public void toggleToVegOnly() {
        btn_toggleVeg.click();
    }

    public String getCategoryName() {
        return txt_ItemCategoryName.getText();
    }

    public void clickonText(IElement element, List<IElement> elements, String text) {
        elements.get(0).click();
        /*if (elements == null) {
			if (element.getText().equalsIgnoreCase(text)) {
				element.click();
			}
		} else {
			for (int i = 0; i < elements.size(); i++) {
				System.out.println("Prod Name " + elements.get(i).getText() + " searched name " + text);

				if (elements.get(i).getText().equalsIgnoreCase(text)) {
					System.out.println("Foundout");
					elements.get(i).click();
					break;
				}
			}
		}*/
    }

    public boolean checkElementDisplay(IElement element) {
        try {
            if (element.isDisplayed()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }

    public boolean checkElementDisplay(List<IElement> element) {
        for (int i = 0; i < 10; i++) {
            try {
                if (element.size() > 0) {
                    return true;
                }
            } catch (Exception e) {
                return false;
            }
        }
        return false;

    }

    public void increaseItemCountFromRestaurantPage() {
        checkoutPage.increaseItemCount();
        if (checkElementDisplay(btn_repeatcustomization)) {
            btn_repeatcustomization.click();
        }
    }

    public void clickOnRepeatCustomization() {
        btn_repeatcustomization.click();
    }

    public String getRestaurantRating() {
        return txt_rating.getText().toString();
    }

    public String getDeliveryTime() {
        return txt_deliveryTime.getText().toString();
    }

    public int getCostOfTwo() {
        String str = txt_priceofTwo.getText().toString();
        String amount = str.replaceAll("[^.0-9]", "");
        System.out.println(amount);
        String finalAmount = null;
        if (amount.contains(".")) {
            finalAmount = amount.substring(0, amount.indexOf("."));
            return Integer.parseInt(finalAmount);
        } else {
            return Integer.parseInt(amount);
        }

    }

    public String getRestaurantName() {
        return txt_restaurantName.getText().toString();
    }

    public boolean isSwiggyAssuredIconDisplay() {
        return checkElementDisplay(icon_swiggyAssured);
    }

    public void clickonFavIcon() {
        fav_icon.click();
    }


    public boolean isPureVegIconDisplay() {
        return checkElementDisplay(icon_pureVeg);
    }

    public void openSearch() {
        icon_search.click();
    }

    public void searchInRestaurant(String keyword) {
        input_searchbox.clear();
        input_searchbox.sendKeys(keyword);

    }

    public String getReplaceCartItemDialogTitle() {
        return txt_ReplaceCartItemdialogTitle.getText();
    }

    public String getReplaceCartItemDialogMessage() {
        return txt_ReplaceCartItemdialogMessage.getText();
    }

    public String getPOPCartcleartTitle() {
        return txt_POPCartcleartTitle.getText();
    }

    public String getPOPCartcleartMessage() {
        return txt_POPCartcleartMessage.getText();
    }

    public void clickonReplaceCartItemDialogYes() {
        btn_ReplaceCartItemdialog_Yes.click();
    }

    public void clickonReplaceCartItemDialogNO() {
        btn_ReplaceCartItemdialog_No.click();
    }


    public void openEDVOMeal(int pos) {

        image_firstEDVO.click();
    }

    public void openEDVOMealiOS(int pos) {
        scroll(Direction.DOWN);
        image_firstEDVO.click();
    }

    public void clickOnContinueButtonFromLandingScreen() {
        btn_Continue.click();
    }

    public void clickOnIncreaseItemCount() {
        checkoutPage.increaseItemCount();
    }

    public void selectFirstPizza() {
        btn_select.get(0).click();
        btn_Continue_addOnScreen.click();
        btn_Continue_addOnScreen.click();
        AndroidUtil.sleep();
        btn_AddItem_addOnScreen.click();
    }

    public void selectFirstPizza(int pos) {
        btn_select.get(pos).click();
        btn_Continue_addOnScreen.click();
        btn_Continue_addOnScreen.click();
        AndroidUtil.sleep();
        btn_AddItem_addOnScreen.click();
    }


    public void clickOnViewCart() {
        if (checkElementDisplay(btn_ViewCart_ConfirmationScreen)) {
            btn_ViewCart_ConfirmationScreen.click();
        } else {
            System.out.println("View cart not found");
        }
    }


    public void clickOnReviewOrder() {

        btn_ReviewOrder.click();
    }

    public void clickOnAddMoreItem() {
        btn_AddMoreItem_ConfimationScreen.click();
    }

    public void clickOnContinueButtonOnAddPizzaScreen() {
        btn_Continue_SelectPizzaScreen.click();
    }




	
	/*public boolean isCustomisableItem() {

		return this.isElementPresent(restaurantpageobject.customization);
	}
	
	public RestaurantPage addCustomItem() {
		if(this.isElementPresent(restaurantpageobject.add_custom_item))
		this.click(restaurantpageobject.add_custom_item);
		return this;
	}
	
	public RestaurantPage selectVeg() {
		this.click(restaurantpageobject.veg_toggle);
		return this;
	}
	
	public boolean isSwiggyAssured() {
		return this.isElementPresent(restaurantpageobject.swiggy_assured);
	}*/

    public void validateEDVOLandingScreen(EDVOMeal edvoObject) {
        Assert.assertEquals(txt_EDVOlandingPage_header.getText().toLowerCase(), edvoObject.getTagText().toLowerCase());
        Assert.assertEquals(txt_EDVOlandingPage_mealDescription.getText().toLowerCase(), edvoObject.getMainText().toLowerCase());
        Assert.assertEquals(txt_EDVOlandingPage_mealSubText.getText().toLowerCase(), edvoObject.getSubText().toLowerCase());
        Assert.assertEquals(txtEDVOlandingPage_groupName.get(0).getText().toLowerCase(), edvoObject.getScreens_title().get(0).toLowerCase());
        Assert.assertEquals(txtEDVOlandingPage_groupName.get(1).getText().toLowerCase(), edvoObject.getScreens_title().get(1).toLowerCase());


    }

    public void validatePizzaScreen(EDVOMeal edvoObject, int pos) {
        Assert.assertEquals(txt_mealDescription_SelectPizzaScreen.getText().toLowerCase(), edvoObject.getMainText().toLowerCase());
        Assert.assertEquals(txt_mealTitle_SelectPizzaScreen.getText().toLowerCase(), edvoObject.getScreens_description().get(pos).toLowerCase());
        Assert.assertEquals(txt_groupName_SelectPizzaScreen.get(pos).getText().toLowerCase(), edvoObject.getScreens_title().get(pos).toLowerCase());
    }

    public void validateMealConfirmationScreen(EDVOMeal edvoObject) {
        Assert.assertEquals(txt_mealDescription_SelectPizzaScreen.getText().toLowerCase(), edvoObject.getExitPage_MainText().toLowerCase());
        //Assert.assertEquals(txt_mealDescription_SelectPizzaScreen.getText().toLowerCase(), edvoObject.getExitPage_MainText().toLowerCase());
        assertTrue(txt_saved_ConfirmationScreen.getText().toString().contains("212"));

    }

    public void validateEDVOOnRestaurantePage(Restaurantmenu restaurantmenu) {
        Assert.assertEquals(txt_edvoHeader.getText().toLowerCase(), restaurantmenu.getEdvoName().toLowerCase());
        Assert.assertEquals(txt_edvoSubHeader.getText().toLowerCase(), restaurantmenu.getSubTitle().toLowerCase());
        Assert.assertEquals(txt_edvoSubHeader.getText().toLowerCase(), restaurantmenu.getSubTitle().toLowerCase());
        assertTrue(image_brandLogo.isDisplayed());

    }

    public boolean checkContinueButtonDisplay() {
        return btn_Continue_SelectPizzaScreen.isDisplayed();
    }

    public String getSnackBarText() {
        return txt_MaxAllowedSnackBar.getText().toString();
    }

    public void clickOnButtonOkayGotIt() {
        btn_okyaGotSnackBar.click();
    }

    public boolean isMealSelectedItemDisplay(int pos) {
        if (icon_mealSelected.size() > 0) {
            return icon_mealSelected.get(pos).isDisplayed();
        } else {
            return false;
        }
    }

    public boolean isAddMoreItemButtonDisplay() {
        return btn_AddMoreItem_ConfimationScreen.isDisplayed();
    }

    public void clickonAddMoreItemButtonOnMealConfirmationPage() {
        btn_AddMoreItem_ConfimationScreen.click();
    }

    public String getDishDiscoveryTitle() {
        return txt_dishdiscoveryTitle.getText().toString();
    }

    public void clickOnBackButtonForDishDiscovery() {
        btn_dishdiscoveryBackButton.click();
    }

    public void pressBackFromSearchScreen() {
        if (inti.platformnew.equalsIgnoreCase("Android")) {
            AndroidDriver drivertemp = (AndroidDriver) driver;
            drivertemp.pressKeyCode(AndroidKeyCode.BACK);
        } else {
            btn_searchCancel.click();
        }
    }

    public void clickonPreOrderTimeSlot() {
        preOrderOnboarding.click();
    }

    public void setPreOrderTime() {
        set_deliveryTime.click();
    }

    public void clickOnPreorderNowTimeslot(){
        btn_Now.click();
    }

    public String getItemNameFromRepeatDialog() {
        return txt_ItemNameOnRepeatDialog.getText().toString();
    }

    public void clickOnIwillChosseButton() {
        btn_iWillChoose.click();
    }

    public void chooseCustimization(int pos) {
        if (radioBtn_addCus.size() > pos) {
            radioBtn_addCus.get(pos).click();
        } else {
            if (checkbox_addCus.size() > pos) {
                checkbox_addCus.get(pos).click();
            }
        }
    }

    public String getGrouporderCreatedText() {

        String Get_grouporder_createdtext_Actual;
        if (checkElementDisplay(grouporder_created_text)) {

            Get_grouporder_createdtext_Actual = grouporder_created_text.getText();

        } else {
            AndroidUtil.pressBackKey(driver);
            Get_grouporder_createdtext_Actual = grouporder_created_text.getText();
        }


        return Get_grouporder_createdtext_Actual;
    }


    public void clickonGrouporderIcon() {
        if (checkElementDisplay(grouporder_icon)) {
            grouporder_icon.click();
        } else
            grouporder_icon1.click();
    }

    public void discardGroupCart() {
        btn_discardcart.click();
    }

    public void keepGroupCart() {
        btn_keepcart.click();
    }

    public void chooseAllCust() {
        for (int i = 0; i < radioBtn_addCus.size(); i++) {
            radioBtn_addCus.get(i).click();
        }
    }

    public boolean checkMandotryCustRequire() {
        return txt_mandotryCust.size() > 2;
    }

    public void selectMandotryCust() {
        for (int i = 0; i < checkbox_addCus.size(); i++) {
            checkbox_addCus.get(i).click();
        }
    }

    public void clickonAddToCartForReplaceItem() {
        if (checkElementDisplay(addtoCart)) {
            addtoCart.get(0).click();
        } else {
            if (checkElementDisplay(addtoCartNew)) {
                addtoCartNew.get(0).click();
            }
        }
    }


    public void clickOnAddToForDominos() {
        if (checkElementDisplay(addtoCart) || checkElementDisplay(addtoCartNew)) {
            if (checkElementDisplay(addtoCart)) {
                addtoCart.get(0).click();
            } else {
                addtoCartNew.get(0).click();
            }
            while (!checkElementDisplay(btn_AddItem_addOnScreen)) {
                if (checkElementDisplay(btn_Continue_addOnScreen)) {
                    btn_Continue_addOnScreen.click();
                }
            }

            btn_AddItem_addOnScreen.click();
        }
    }

    public void custForDominos() {
        while (!checkElementDisplay(btn_AddItem_addOnScreen)) {
            if (checkElementDisplay(btn_Continue_addOnScreen)) {
                btn_Continue_addOnScreen.click();
            }
        }

        btn_AddItem_addOnScreen.click();
    }

    public void addItemToCartForIOs() {
        AndroidUtil.sleep(10000);
        if (checkElementDisplay(addtoCart)) {
            addtoCart.get(0).click();
        } else {
            if (checkElementDisplay(btn_exapandArrow)) {
                btn_exapandArrow.get(0).click();
                addtoCart.get(0).click();
            }
        }
        if (checkElementDisplay(btn_addCustomItem)) {
            btn_addCustomItem.click();
        }
    }


    public void clickOnBackButton() {
        btn_back.click();
    }

    public void clickOnBackButtonFromCart() {
        btn_back_cart.click();
    }


    public String verifyGuestJoinedGroupOrderByText() {
        return txt_guestjoinedgrouporder.getText();
    }


    public boolean validatePureVegIconDisplay() {

        System.out.println("Pre Veg text" + icon_pureVeg.getText());
        return icon_pureVeg.getText() != "PURE VEG";
    }

    //Hygiene rating
    public void verifyHygieneimage() {
        if (checkElementDisplay(image_hygiene)) {
            System.out.println("image is present " );
        } else {

            System.out.println("image is not present " );


        }

    }

    public void tapOnHygienerating()
        {
            if (checkElementDisplay(image_hygiene)) {
                image_hygiene.click();
            }
            else{
                 System.out.println("Cant tap on hygiene rating as its not enabled for this restaurant");
            }
        }


    public void verifyHRback() {
        if (checkElementDisplay(btn_back_HR)) {
            btn_back_HR.click();
        }
        else{
            System.out.println("No Hygiene rating back key");
        }
    }

    public void ScrollToEndScreenMenuFssai() {
        for (int i = 0; i < 11; i++) {

            JavascriptExecutor js = driver;
            HashMap scrollObject = new HashMap();
            scrollObject.put("direction", "down");
            js.executeScript("mobile: scroll", scrollObject);

        }
    }

    public void verifyFeedback() {
        ScrollToEndScreenMenuFssai();
        if (checkElementDisplay(btn_feedback)) {
            btn_feedback.click();
            btn_cancel.click();
            btn_deletedraft.click();
        } else {
            System.out.println("Feedback button not found");
        }

    }

    public void verifyContinueOrdering() {
        if(checkElementDisplay(btn_continueordering))
        {
        btn_continueordering.click();
        }
        else
        {
            System.out.println("No continue ordering button");
        }
    }

    public String getHygieneratingtext() {
        return txt1_rating.getText().toString();
    }

    public boolean getDeliverytimetext()
    {
        boolean b = checkElementDisplay(txt_deliverytime);
        return b;
    }

    //FSSAI methods

    public void verifyFssaiLicence() {
        ScrollToEndScreenMenuFssai();
        ScrollToEndScreenMenuFssai();
        scrollios(driver, UP);
        sleep(20000);
        //scrollToElement(driver, UP, fssai_section);
        if (fssai_section.isDisplayed()) {
            fssai_section.click();
            System.out.println("Restaurant has Approved/Applied fssai license");
        } else {
            System.out.println("Restaurant do not have  Approved/Applied fssai license");
        }
    }

    public void verifyLicenseStatus() {
        System.out.println("Inside method");
        if(checkElementDisplay(fixed_fee_cruton))
        {
            fixed_fee_cruton.click();
        }
        ScrollToEndScreenMenuFssai();
        ScrollToEndScreenMenuFssai();
        scrollios(driver, UP);
        sleep(20000);
        //scrollToElement(driver, UP, fssai_section);
        if (fssai_section.isDisplayed()) {
            String license_status = fssai_status.getText();

            if (license_status.contains("Applied for license")) {
                System.out.println("License status is :: " + license_status);
            } else {
                System.out.println("Restaurant has license number");
            }
        }
    }


    public String getfssaiimage(){
     return fssaiImage.getText();
    }

    public boolean verifymenurestnameintitle(){
        return checkElementDisplay(txt_menurestnameintitle);
    }

}