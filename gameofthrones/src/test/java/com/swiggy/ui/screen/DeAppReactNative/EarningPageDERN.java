package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.ArrayList;
import java.util.List;

public class EarningPageDERN extends AndroidUtil {

    public List<IElement> txt_cardWeekHeader;
    public List<IElement> cardDateRange ;
    public List<IElement> txt_cardTransferDateHeader ;
    public List<IElement> txt_transferDate ;
    public List<IElement> txt_headerCardStatus ;
    public List<IElement> txt_status ;
    public List<IElement> txt_weekTotal ;
    public List<IElement> txt_tabPrice ;
    public List<IElement> txt_tabHeader ;
    public List<IElement> txt_earningCardHeader ;
    public List<IElement> txt_earningCardTotalPrice ;
    public List<IElement> txt_earningCardName ;
    public List<IElement> txt_earningCardTimeStamp ;
    public List<IElement> txt_earningCardViewAmount ;
    public List<IElement> txt_earningCardViewOrderStatus ;
    public IElement view_cardView;
    public IElement txt_weekDateRangeDetailView;
    public IElement view_detailCardView;

    public EarningPageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }


    public ArrayList<String> getHeaderOfCard(){
       return  getList(txt_cardWeekHeader);
    }
    public ArrayList<String> getDateRangeOnCard(){
        return  getList(cardDateRange);
    }
    public ArrayList<String> getCardTransferDateHeader(){
        return  getList(txt_cardTransferDateHeader);
    }
    public ArrayList<String> getTransferDate(){
        return  getList(txt_transferDate);
    }
    public ArrayList<String> getStatusHeaderOnCard(){
        return  getList(txt_headerCardStatus);
    }
    public ArrayList<String> getStatus(){
        return  getList(txt_status);
    }
    public ArrayList<String> getWeekTotal(){
        return  getList(txt_weekTotal);
    }
    public ArrayList<String> getPriceOnTab(){
        return  getList(txt_tabPrice);
    }
    public ArrayList<String> getTabHeader(){
        return  getList(txt_tabHeader);
    }
    public ArrayList<String> getEarningCardHeader(){
        return  getList(txt_earningCardHeader);
    }
    public ArrayList<String> getEarningCardTotalPrice(){
        return  getList(txt_earningCardTotalPrice);
    }
    public ArrayList<String> getRestaurantNameOnEarningCard(){
        return  getList(txt_earningCardName);
    }
    public ArrayList<String> getTimeStampOnEaringCard(){
        return  getList(txt_earningCardTimeStamp);
    }
    public ArrayList<String> getAmountOnEarningCard(){
        return  getList(txt_earningCardViewAmount);
    }
    public ArrayList<String> getOrderStatusOnEarningCard(){
        return  getList(txt_earningCardViewOrderStatus);
    }

    public boolean isCardViewDisplay(){
        return view_cardView.isDisplayed();
    }
    public void clickOnCard(int index){
        txt_earningCardHeader.get(index).click();
    }

    public String getTotalPriceOnTab(){
        return txt_tabPrice.get(0).getText().toString();
    }
    public String getOrderPriceOnTab(){
        return txt_tabPrice.get(1).getText().toString();
    }
    public String getIncentivePriceOntab(){
        return txt_tabPrice.get(2).getText().toString();
    }
    public String getWeekDateRange(){
        return txt_weekDateRangeDetailView.getText().toString();
    }
    public boolean isOrderDetailCardDisplayForADay(){
        return view_detailCardView.isDisplayed();
    }
    public boolean isRestaurantNameDisplayOnCard(){
        return txt_earningCardName.size() > 0;
    }
    public void clickOnCardForCollpase(int index){
        txt_earningCardHeader.get(index).click();
    }















    public ArrayList<String> getList(List<IElement> list){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<list.size();i++){
            al.add(list.get(i).getText().toString());
        }
        return al;
    }
}
