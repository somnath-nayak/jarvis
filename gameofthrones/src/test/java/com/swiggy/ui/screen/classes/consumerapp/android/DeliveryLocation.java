package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pageobjects.classes.consumerapp.android.DeliveryLocationPageObject;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class DeliveryLocation extends AndroidUtil {

	private AppiumDriver<MobileElement> driver = null;
	public DeliveryLocationPageObject deliverylocationpageobject = new DeliveryLocationPageObject();
	
	public DeliveryLocation(InitializeUI init) {
		super(init);
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(init.getAndroidDriver()), deliverylocationpageobject);
	}
	

	
	public DeliveryLocation enterArea(String text) {
		if(this.isElementPresent(deliverylocationpageobject.area))
			this.sendKeys(deliverylocationpageobject.area, text);
		return this;
	}
	
	public DeliveryLocation enterHouseNo(String text) {
		this.sendKeys(deliverylocationpageobject.house_no, text);
		return this;
	}
	
	public DeliveryLocation enterLandMark(String text) {
		this.sendKeys(deliverylocationpageobject.landmark, text);
		return this;
	}
	
	public DeliveryLocation selectLocationAsHome() {
		this.click(deliverylocationpageobject.home);
		return this;
	}
	
	public DeliveryLocation selectLocationAsWork() {
		this.click(deliverylocationpageobject.work);
		return this;
	}
	
	public DeliveryLocation selectAsOther() {
		this.click(deliverylocationpageobject.other);
		return this;
	}
	
	public void save() {
		this.click(deliverylocationpageobject.save_button);
	}
}
