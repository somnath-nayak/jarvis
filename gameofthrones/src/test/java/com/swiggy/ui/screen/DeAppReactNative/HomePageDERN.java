package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

public class HomePageDERN  extends AndroidUtil{

    public IElement btn_openleftNav; //android.widget.ImageButton

    public HomePageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public void openLeftNav(){
        btn_openleftNav.click();
    }
    public boolean isLeftNavButtonDisplay(){
        return btn_openleftNav.isDisplayed();
    }

    public void openHomePage(){
        for(int i=0;i<3;i++){
            if(checkElementDisplay(btn_openleftNav)){
                break;
            }

        }
    }

    public boolean checkElementDisplay(IElement element){
        try{
            if(element.isDisplayed()){
                return true;
            }

        }
        catch(Exception e){
            return  false;
        }
        return false;
    }

}
