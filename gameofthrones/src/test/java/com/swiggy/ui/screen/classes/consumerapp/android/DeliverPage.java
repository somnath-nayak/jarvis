package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pageobjects.classes.consumerapp.android.DeliverPageObject;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;

public class DeliverPage extends AndroidUtil {
	
	private AppiumDriver<MobileElement> driver = null;
	public DeliverPageObject deliverpageobject = new DeliverPageObject();
	InitializeUI init;

	public DeliverPage(InitializeUI init) {
		super(init);
		this.init=init;
		this.driver = driver;

		PageFactory.initElements(new AppiumFieldDecorator(init.getAndroidDriver()), deliverpageobject);
	}

	public String getOrderId() {
		return this.getText(deliverpageobject.order_id);
	}
	
	public boolean isOrderPlaced() {
		return this.isElementPresent(deliverpageobject.order_received);
	}
	
	public boolean isOrderConfirmed() {
		return this.isElementPresent(deliverpageobject.order_confirmed);
	}
	
	public boolean isOrderAssigned() {
		return this.isElementPresent(deliverpageobject.assigned_message);
	}
	
	public boolean isOrderArrived() {
		return this.isElementPresent(deliverpageobject.arrived_message);
	}
	
	public boolean isOrderPickedUp() {
		return this.isElementPresent(deliverpageobject.picked_up);
	}
	
	public boolean isOrderDelivered() {
		return this.isElementPresent(deliverpageobject.order_delivered);
	}
	
	public boolean checkCAPRD() {
		boolean success = false;
		try{
			success = this.isOrderPlaced();
			Thread.sleep(20000);
			success = this.isOrderAssigned();
			Thread.sleep(20000);
			success = this.isOrderConfirmed();
			Thread.sleep(20000);
			success = this.isOrderArrived();
			Thread.sleep(20000);
			success = this.isOrderPickedUp();
			Thread.sleep(20000);
			success = this.isOrderDelivered();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return success;
	}
	
	public HomePage clickOnOk() {
		this.click(deliverpageobject.ok);
		return new HomePage(init);
	}
}
