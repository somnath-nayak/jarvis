package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class GrouporderPage extends AndroidUtil {

    public IElement create_group_order;
    public IElement home_back_button;
    public IElement copy_linktext;
    public IElement btn_moreOptionstoShareLink;
    String str;
    //public IElement btn_discardcart;
    //public IElement btn_keepcart;



    private AppiumDriver<MobileElement> driver;


    public GrouporderPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);

        driver=init.getAppiumDriver();

    }

    public String grouporderlinkCopy_Adbcommand() {
         str = AndroidUtil.runShellCommand("adb -s "+System.getenv("deviceId")+" shell am broadcast -a clipper.get").toString();
        System.out.println(str);
        return str;
    }

    public void Copygrouporderlinktext() {
        copy_linktext.click();
    }

    public void clickonCreateGrouporderButton()
    {
        create_group_order.click();
    }

    public void closeGroupOrderPage()
    {
        home_back_button.click();
    }

    public void moreOptionsToSharelink() { btn_moreOptionstoShareLink.click(); }

    //public void yes_ProceedWithoutGuestConfirmation() { btn_keepcart.click(); }

    //public void no_WaitGuestConfirmation() { btn_discardcart.click(); }

}
