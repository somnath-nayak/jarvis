package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.HashMap;


public class PaymentPage extends AndroidUtil {

	public IElement txt_paymentHeader;
	public IElement txt_paymentdisplayonPayment;
	public IElement btn_cod;
	public IElement btn_payWithCash;
	public IElement btn_backtocart;
	public IElement txt_walletHeader;

	//Cafe
	public IElement txt_COD;


	AppiumDriver driver;
	ExplorePage explorePage;

	public PaymentPage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(),this,init);
		driver=init.getAppiumDriver();
		explorePage=new ExplorePage(init);
	}

	public String getPaymentHeader(){
		return txt_paymentHeader.getText().toString();
	}
	//This method to get total payment and Quantity from payment page
	public String getSubHeaderFromPaytmenPage(){
		return txt_paymentdisplayonPayment.getText().toString();
	}

	public String getWalletHeadet(){
		//	This method will return wallets header text
		return txt_walletHeader.getText();
	}
	public void clickonCOD(){
		//explorePage.scrollTillElementVisible(btn_cod);
		if(inti.platformnew.equalsIgnoreCase("Android")) {
			while (!checkElementDisplay(btn_cod)) {
				TouchAction touchAction = new TouchAction(driver);
				touchAction.longPress(50, 800).moveTo(50, 300).release().perform();
				touchAction.longPress(50, 800).moveTo(50, 300).release().perform();
			}
			btn_cod.click();

			while (!checkElementDisplay(btn_payWithCash)) {
				TouchAction touchAction = new TouchAction(driver);
				touchAction.longPress(50, 400).moveTo(50, 300).release().perform();


			}
			btn_payWithCash.click();
		}
		else{
			tapItemByDescription("Cash");
			btn_payWithCash.click();
		}

	}
	public boolean checkElementDisplay(IElement element){
		try{
			if(element.isDisplayed()){
				return true;
			}
		}
		catch(Exception e){
			return false;
		}
		return false;

	}
	 public void ValidatePaymentPageFromdeeplink(String P1)
	 {
		 String PaymentHeader= getPaymentHeader();
		 System.out.println(PaymentHeader);
		 Assert.assertEquals(P1,PaymentHeader);
	 }

	public void tapItemByDescription(String text) {
		AndroidUtil.sleep(10000);
		System.out.println("   tapItemByDescription(): " + text);

		// scroll to item
		JavascriptExecutor js = driver;
		HashMap scrollObject = new HashMap<>();
		scrollObject.put("predicateString", "value == '" + text + "'");
		js.executeScript("mobile: scroll", scrollObject);

		// tap item
		WebElement el = ((IOSDriver) driver).findElementByIosNsPredicate("value = '" + text + "'");
		el.click();
	}


	 public void ClickOnbackToCart()
	 {
	 	btn_backtocart.click();
	 }

public void validateCODmsg(){

	scroll(Direction.UP);
	scroll(Direction.UP);
	scroll(Direction.UP);
	String Actual="COD isn't available for Food Court orders";
	String  Expected=txt_COD.getText();
	System.out.println("Expected value " +Expected);
	Assert.assertEquals(Actual,Expected);
	System.out.println("Success");
}


}
