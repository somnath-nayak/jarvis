package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
public class LaunchPage extends AndroidUtil {
	public IElement login;
	public IElement setCurrentLocation;
	public IElement allowLocationAccess;
	public IElement btn_setDeliveryLocation;
	public IElement denyLocationAccess;
	public IElement permission;
	public IElement txt_smartLockNoneOfAbove;
	public IElement btn_alwaysForDeeplink;
	private AppiumDriver<MobileElement> driver;
	//public LaunchPageObject launhpageobject = new LaunchPageObject();

	public LaunchPage(InitializeUI inti) {
		super(inti);
		initElements(inti.getAppiumDriver(),this,inti);
	}
	public void tapOnLogin() {
		login.click();
		if(cancelSmartLock()){
			txt_smartLockNoneOfAbove.click();
		}
	}
	public void clickonAllowButton(){
		if(checkElementDisplay(allowLocationAccess)){
			allowLocationAccess.click();}
	}


	public void setOrder_location(){
		if(checkElementDisplay(setCurrentLocation)){
			setCurrentLocation.click();
		}
	}
	public void clickOnSetDeliveryLocation() {
		clickonAllowButton();
		if (checkElementDisplay(btn_setDeliveryLocation)) {
			btn_setDeliveryLocation.click();
		}
		if (checkElementDisplay(allowLocationAccess)) {
			allowLocationAccess.click();
		}
	}
	public void clickonDenyLocationAccess(){
		if(checkElementDisplay(denyLocationAccess)) {
			denyLocationAccess.click();
		}
	}
	public boolean checkElementDisplay(IElement element){
		try{
			if(element.isDisplayed()){
				return true;
			}
		}
		catch(Exception e){
			return false;
		}
		return false;
	}
	public boolean cancelSmartLock(){
		return checkElementDisplay(txt_smartLockNoneOfAbove);
	}
	/*public boolean isLaunchPage() {
        return isElementPresent();
    }


    public LaunchPage allowPermission() {
        this.click(launhpageobject.permission);
        return this;
    }

    public void addLocation() {
        this.click(launhpageobject.set_location);
    }
    public boolean isContinueDisplayed() {
        return this.isElementPresent(launhpageobject.continue_button);
    }

    public void tapOnContinue() {
        this.click(launhpageobject.continue_button);
    }*/
	public void clickonButtonAlways(){
		btn_alwaysForDeeplink.click();
	}
}

