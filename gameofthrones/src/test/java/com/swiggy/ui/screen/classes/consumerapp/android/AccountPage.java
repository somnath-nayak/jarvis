package com.swiggy.ui.screen.classes.consumerapp.android;


import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.ios.IOSDriver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.HashMap;
import java.util.List;
public class AccountPage extends AndroidUtil {


	public IElement txt_mobileNumber;
	public IElement txt_profileName;
	public IElement txt_profileEmailId;

	public IElement btn_editProfile;
	public IElement input_editPhoneNumber;
	public IElement input_editEmailAddress;
	public IElement btn_update;
	public IElement txt_otpScreen;
	public IElement txt_toastForEmailUpdate;
	public IElement btn_toastForEmailUpdate;
	public IElement btn_myAccount;
	public IElement btn_manageAddress;
	public IElement btn_addNewAddress;
	public IElement getAllText;
	public IElement btn_managePayment;
	public IElement txt_swiggyMoneyBalance;
	public IElement btn_linkPaytm;
	public IElement btn_linkMobikwik;
	public IElement btn_linkFreecharge;
	public IElement btn_linkProceedConfirmaiton;
	public IElement txt_walletLinkHeader;
	public IElement txt_walletLinksubHeader;
	public List<IElement> btn_trackOrderFromAccount;
	public IElement viewActiveOrder;
	public IElement viewPastOrder;
	public List<IElement> txt_restaurantNameInMyOrder;//profile_fragment__order_detail
	public List<IElement> txt_priceInMyOrder;
	public List<IElement> btn_reorder;
	public IElement btn_Favorities;
	public IElement btn_referrals;
	public IElement btn_offers;
	public IElement btn_inviteAndEarn;
	public List<IElement> list_inviteshare;
	public IElement btn_logout;
	public IElement btn_login;
	public IElement offers_heading;
	public IElement help_heading;

	public IElement btn_back;
	public IElement txt_myAccount;

	public IElement txt_profile;
	public IElement help_Orderheading;

	public List<IElement> list_inviteshareNEW;
	public IElement txt_logoutConfirmationMsg;
	public IElement btn_dialogLogout;
	public IElement btn_dialogCancel;
	public IElement btn_reorder2;


	private AppiumDriver driver;



	AddressPage addressPage;

	public AccountPage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(), this, init);
		addressPage = new AddressPage(init);
		driver = init.getAppiumDriver();
	}

	public void getTextMobileNumber() {
		String veg = txt_mobileNumber.getText();
		System.out.println(veg);
	}

	public String getRegisteredMobileNumber() {
		try {
			if (txt_mobileNumber.isDisplayed()) {
				return txt_mobileNumber.getText().toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public boolean ValidateRegisteredMobileNumberios(String username) {
		try {
			if (txt_mobileNumber.isDisplayed()) {
				String mobile_num = txt_mobileNumber.getText().toString();
				mobile_num.contains(username);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public void updateMobileNumber(String newNumber) {
		clickOnEditProfile();
		input_editPhoneNumber.clear();
		input_editPhoneNumber.sendKeys(newNumber);
		btn_update.click();
	}

	public void updateEmailAddress(String newEmail) {
		clickOnEditProfile();
		input_editEmailAddress.clear();
		input_editEmailAddress.sendKeys(newEmail);
		btn_update.click();
	}

	public void clickOnEditProfile() {
		btn_editProfile.click();
	}

	public boolean validateOtpScreen() {
		try {
			return txt_otpScreen.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}

	public boolean validateEmailUpdateMessage() {
        return txt_toastForEmailUpdate.isDisplayed();
	}

	public void openManageAddress() {
		btn_myAccount.click();
		btn_manageAddress.click();

	}

	public void clickOnBtnAddNewAddress() {
		btn_addNewAddress.click();
	}

	public String getSwiggyMoneyBalance() {
		String str = txt_swiggyMoneyBalance.getText().toString();
		String balance = str.replaceAll("[^.0-9]", "");
		return balance;
	}

	public void linkWallet(String walletName) {

		switch (walletName) {
			case "paytm":
				btn_linkPaytm.click();
				break;
			case "freecharge":
				btn_linkFreecharge.click();
				break;
			case "mobikwik":
				btn_linkMobikwik.click();
				break;

		}
		btn_linkProceedConfirmaiton.click();
	}

	public String getWalledLinkHeaderText() {
		return txt_walletLinkHeader.getText().toString();

	}

	public String getMobileNumberFromWalledLink() {
		String str = txt_walletLinksubHeader.getText().toString();
		String mobileNo = str.replaceAll("[^0-9]", "");
		return mobileNo;

	}



	public void openManagePayment() {
		btn_myAccount.click();
		btn_managePayment.click();

	}

	public void openFavorities() {
		AndroidUtil.sleep(1000);
		btn_myAccount.click();
		AndroidUtil.sleep(2000);
		btn_Favorities.click();

	}

	public void clickonTrackOrder(int pos) {
		btn_trackOrderFromAccount.get(pos).click();
	}

	public boolean checkAnyActiveOrderPresent() {
		return checkElementDisplay(viewActiveOrder);

	}

	public boolean checkAnyPastOrderPresent() {
		return checkElementDisplay(viewPastOrder);
	}

	public void clickOnReorder(int pos) {
		if (inti.platformnew.equalsIgnoreCase("Android")) {

			TouchAction touchAction = new TouchAction(driver);
			touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
			btn_reorder.get(pos).click();

		} else {
			scrollios(driver, Direction.UP);
			AndroidUtil.sleep();
			btn_reorder.get(pos).click();
		}
	}


	public boolean checkElementDisplay(IElement element) {
		try {
			if (element.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;

	}

	public String getRestaurantNameFromMyOrder(int pos) {

		return txt_restaurantNameInMyOrder.get(pos).getText().toString();

	}

	public void openViewOrderDetail(int pos) {
		txt_restaurantNameInMyOrder.get(pos).click();

	}

	public void openReferral() {
		//btn_myAccount.click();
		btn_referrals.click();
	}

	public void clickonInviteButton() {
		btn_inviteAndEarn.click();
	}

	public void clickOncopytoClipboard() {


			for (int i = 0; i < list_inviteshare.size(); i++) {
				if (list_inviteshare.get(i).getText().toString().equalsIgnoreCase("Copy to clipboard")) {
					list_inviteshare.get(i).click();
				}


			}


		}


	public void clickOncopytoClipboardGroupOrder() {
		for (int i = 0; i < list_inviteshareNEW.size(); i++) {
			if (list_inviteshareNEW.get(i).getText().toString().equalsIgnoreCase("Copy to clipboard")) {
				list_inviteshareNEW.get(i).click();
			}
		}
	}


	public void openOffers() {
		if(checkElementDisplay(btn_myAccount))
		{
		btn_myAccount.click();}
		btn_offers.click();
	}

	public void ValidateAccountsPageFromdeeplink(String mobileNumber) {
		Assert.assertEquals(getRegisteredMobileNumber(),mobileNumber);
	}


	public void clickonLogout(){
		if(inti.platformnew.equalsIgnoreCase("Android")){
			TouchAction touchAction = new TouchAction(driver);
			while(!checkElementDisplay(btn_logout)){
				touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
			}
			touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
			btn_logout.click();
		}
		else if(!inti.platformnew.equalsIgnoreCase("ios")) {
			String text="LOGOUT";
			System.out.println("   tapItemByDescription(): " + text);

			// scroll to item
			JavascriptExecutor js = driver;
			HashMap scrollObject = new HashMap<>();
			scrollObject.put("predicateString", "value == '" + text + "'");
			js.executeScript("mobile: scroll", scrollObject);

			// tap item
			WebElement el = ((IOSDriver) driver).findElementByIosNsPredicate("value = '" + text + "'");
			el.click();
		}
		if(inti.platformnew.equalsIgnoreCase("ios"))
		{
			scrollToElement(driver, Direction.UP, btn_logout);
			btn_logout.click();
		}


	}



	public boolean checkLogoutButtonDisplay(){
		return checkElementDisplay(btn_logout);
	}
	public boolean checkLoginButtonDisplay(){
		return checkElementDisplay(btn_login);
	}
	public void clickOnLoginFromAccount(){
		btn_login.click();
	}
	public String getProfileName(){
		return txt_profileName.getText();
	}
	public String getProfileEmailId(){
		return txt_profileEmailId.getText();
	}



	public void validateInviteFromDeeplink()
	{
		clickonInviteButton();


	}

	public String getOffersHeader(){
		return offers_heading.getText().toString();

	}

	public void validateoffersFromDeeplink(String Exp)
	{
		String Act= getOffersHeader();
		System.out.println("String Fetched:"+Act);
		Assert.assertEquals(Act,Exp);
	}

	public String gethelpHeader() {
		return help_heading.getText().toString();

	}

	public void validateHelpFromDeeplink(String Exp)
	{
		String Act= gethelpHeader();
		System.out.println("String Fetched:"+Act);
		Assert.assertEquals(Act,Exp);
	}



	public void validateProfileFromDeeplinkIos()
	{
		String Act= "Help";
		String Exp= txt_myAccount.getText();
		System.out.println("Expected value:"+Exp);
		Assert.assertEquals(Act,Exp);
	}

	public void pressBackKeyFromLinkWalltetPage(){
		btn_back.click();
	}

	public String gethelpOrderHeader() {
		return help_Orderheading.getText().toString();

	}



	public String getRegisteredName() {
		try {
			if (txt_profileName.isDisplayed()) {
				return txt_profileName.getText();
			} else {
				return null;
			}
		} catch (Exception e) {
			return null;
		}
	}

	public String fetchFirstName()
	{
		String Full_name=getRegisteredName();

		String[] arrayist = Full_name.split(" ");
		String temp=arrayist[0];
		return temp;

	}

	public String Grouporder_createdtext()
	{
		String first_name=fetchFirstName();
		String Expected_Grouporder_createdtext= "Swiggy Social order by "+ first_name;
		return Expected_Grouporder_createdtext;


	}

	public String getLogoutConfirmationMessage(){
		return txt_logoutConfirmationMsg.getText().toString();
	}
	public void clickLogoutOnDialog(){
		btn_dialogLogout.click();
	}
	public void clickCancelOnLogoutDialog(){
		btn_dialogCancel.click();

	}

	public void clickReorderbutton(){
		btn_reorder2.click();
	}

}


