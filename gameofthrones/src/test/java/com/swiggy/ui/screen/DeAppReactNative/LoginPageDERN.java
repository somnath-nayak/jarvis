package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

public class LoginPageDERN extends AndroidUtil {

    public IElement input_deId;//   android.widget.EditText
    public IElement btn_continue; //android.widget.TextView[@text='CONTINUE']
    //public IElement txt_otpsentText;
    public IElement input_otp ;//android.view.ViewGroup[@index=3]/android.view.ViewGroup/android.widget.EditText
    public IElement btn_login;
    public IElement btn_otpSendAgain;
    public IElement allowLocationAccess;
    public IElement denyLocationAccess;
    public IElement txt_deIDShortView;
    public IElement txt_didnotreceive;


    public LoginPageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public void enterDEId(String id){
        input_deId.sendKeys(id);
    }
    public void clickOnContinueButton(){
        btn_continue.click();
    }
    public void enterOtp(String otp){
        input_otp.sendKeys(otp);
    }
    public void clickOnLoginButton(){
        btn_login.click();
    }

    public void clickOnAllowPermission(){
        if(checkElementDisplay(allowLocationAccess)){
            allowLocationAccess.click();
        }
    }
    public void clickOnDenyPermission(){
        if(checkElementDisplay(denyLocationAccess)){
            denyLocationAccess.click();
        }
    }
    public boolean checkElementDisplay(IElement element) {
        try {
            return element.isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }


}
