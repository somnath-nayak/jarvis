package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.HashMap;
import java.util.List;

public class InvitePageDERN extends AndroidUtil {


    public IElement txt_referralTitle ;
    public IElement txt_referralSubTitle;
   /* public IElement btn_seeStatement;
    public List<IElement> txt_sectionTitle;
    public List<IElement> txt_depositTitle ;
    public List<IElement> txt_depositSubTitle ;
    public List<IElement> image_depositImage;
    public List<IElement> btn_arrow ;
    public List<IElement> txt_titleDetailView;
    public List<IElement> txt_subTitleDetailView ;
    public IElement txt_depositCode;*/


    public IElement btn_inviteNow;
    public IElement input_name;
    public IElement input_contactNumber;
    public IElement btn_inviteFriend;
    public IElement btn_contactlist;
    public IElement txt_cityName;
    public List<IElement> listCityName;
    public IElement txt_sectionTitle;
    public List<IElement> txt_rulesTitle;
    public List<IElement> txt_rulesSubTitle;
    public List<IElement> txt_applicantName;
    public List<IElement> txt_applicantMobileNumber;
    public List<IElement> txt_invitedAt;
    public List<IElement> txt_invitationStatus;

    public IElement txt_detailViewApplicantName;
    public IElement txt_detailViewApplicantMobile;
    public IElement txt_detailViewInvitedAt;
    public IElement txt_invitationId;
    public IElement txt_detailViewInvitationstatus;
    public IElement txt_referralText;
    public IElement txt_detailViewApplicantCity;










    public InvitePageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public String getReferralScreenTitle(){
        return txt_referralTitle.getText().toString();
    }
    /*public void clickOnSeeStatement(){
        btn_seeStatement.click();
    }
    public String getsectionTitles(int pos){
        return txt_sectionTitle.get(pos).getText().toString();
    }
    public String getDepositTitle(int pos){
        return txt_depositTitle.get(pos).getText().toString();
    }
    public String getDepositSubTitle(int pos){
        return txt_depositSubTitle.get(pos).getText().toString();
    }
    public void openDepositDetail(int pos){
         btn_arrow.get(pos).click();
    }
    public String getTitleDetailView(int pos){
        return txt_titleDetailView.get(pos).toString();
    }
    public String getSubTitleDetailView(int pos){
        return txt_subTitleDetailView.get(pos).toString();
    }
    public String getDepositCode(){
        return txt_depositCode.getText().toString();
    }*/
    public String getReferralScreenSubTitle(){
        return txt_referralSubTitle.getText().toString();
    }
    public void clickOnInviteNow(){
        btn_inviteNow.click();
    }
    public void enterUserName(String text){
        input_name.sendKeys(text);
    }

    public void enterContactNumber(String text){
        input_contactNumber.sendKeys(text);
    }
    public void clickOnInviteButton(){
        btn_inviteFriend.click();
    }
    public void clickOnContactListButton(){
        btn_contactlist.click();
    }
    public String getSelectedCity(){
        return txt_cityName.getText().toString();
    }
    public void  selectCity(int index){
        txt_cityName.click();
        listCityName.get(index).click();
    }
    public boolean isInviteNowButtonDisplaying(){
       return btn_inviteNow.isDisplayed();
    }
    public String getRulesSectionTitles(){
        return txt_sectionTitle.getText().toString();
    }
    public HashMap<String,String> getRules(){
        HashMap<String,String> hm=new HashMap<>();
        for(int i=0;i<txt_rulesTitle.size();i++){
            hm.put(txt_rulesTitle.get(i).getText(),txt_rulesSubTitle.get(i).getText());
        }
        return hm;
    }
    public String getFirstReferralApplicantName(){
        return txt_applicantName.get(0).getText().toString();
    }
    public String getFirstReferralApplicantMobileNumber(){
        return txt_applicantMobileNumber.get(0).getText().toString();
    }
    public String getFirstReferralApplicantInvitationTime(){
        return txt_invitedAt.get(0).getText().toString();
    }
    public String getFirstReferralApplicantInvitationStatus(){
        return txt_invitationStatus.get(0).getText().toString();
    }
    public void clickOnFirstReferral(){
        txt_applicantName.get(0).click();
    }
    public String getApplicantMobileNumberFromDetaiLView(){
        return txt_detailViewApplicantMobile.getText().toString();
    }
    public String getApplicantNameFromDetaiLView(){
        return txt_detailViewApplicantName.getText().toString();
    }
    public String getApplicantCityIdDetaiLView(){
        return txt_detailViewApplicantCity.getText().toString();
    }
    public String getApplicantInvitationId(){
        return txt_invitationId.getText().toString();
    }
    public String getInvitationStatusFromDetailView(){
        return txt_detailViewInvitationstatus.getText().toString();
    }
    public String getReferralText(){
        return txt_referralText.getText().toString();
    }







}
