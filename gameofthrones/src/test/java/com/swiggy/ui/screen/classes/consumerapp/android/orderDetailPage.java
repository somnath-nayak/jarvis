package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

public class orderDetailPage extends AndroidUtil {

	public IElement pageTitle;
	public IElement pageSubTitle;
	public IElement btn_help;




	public orderDetailPage(InitializeUI  init) {
		super(init);
		initElements(init.getAppiumDriver(),this,init);
	}

	public String getpageTitle(){
		return pageTitle.getText().toString();
	}
	public String getPageSubTitle(){
		return pageSubTitle.getText().toString();
	}
	public void clickOnHelp(){
		btn_help.click();
	}


}
