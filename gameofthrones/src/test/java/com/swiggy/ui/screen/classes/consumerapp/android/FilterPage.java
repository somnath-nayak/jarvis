package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.pageobjects.classes.consumerapp.android.FilterPageObject;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class FilterPage extends AndroidUtil {

	private AppiumDriver<MobileElement> driver = null;
	public FilterPageObject filterpageobject = new FilterPageObject();
	
	public FilterPage(InitializeUI init) {
		super(init);
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(init.getAndroidDriver()), filterpageobject);
	}
	
	By filterselect = By.id("in.swiggy.android:id/filters_fragment_item_text");
	
	public FilterPage selectFilter(String filter_text) {
		try{
		List<MobileElement> filters = driver.findElements(filterselect);
		for(MobileElement e: filters) 
			if(e.getText().equalsIgnoreCase(filter_text)){
				this.click(e);
				break;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public void applyFilter() {
		this.click(filterpageobject.apply_filter);
	}
//tests
}
