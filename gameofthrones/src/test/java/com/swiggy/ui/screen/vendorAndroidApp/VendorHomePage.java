package com.swiggy.ui.screen.vendorAndroidApp;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;

import java.util.List;

public class VendorHomePage extends AndroidUtil {

    public IElement btn_GoToSetting;
    public IElement btn_All;
    public IElement btn_preparing;
    public IElement btn_pending;
    public IElement btn_pop;
    public List<IElement> btn_search;
    public IElement txt_newOrder;
    //public IElement txt_bannerPreparing;
    public List<IElement> txt_orderId;
    public List<IElement> txt_ItemDetail;
    public List<IElement> txt_itemPrice;
    public IElement txt_restaurantName;
    public IElement btn_preparationTime;
    public IElement txt_defaultPreparationTime;
    public IElement txt_restaurantNameNEw;
    public IElement txt_search;
    public IElement txt_orders;
    public IElement tag_preparing;
    public IElement btn_food_ready;
    public IElement txt_awaitingcall;
    public IElement tag_ready;
    public IElement btn_track_order;
    public IElement btn_call_order;
    public IElement tab_past_orders;
    public IElement txt_global_preptime;
    public IElement btn_cross;
    public List<IElement> txt_order_id;
    public IElement btn_search_clear;
    public IElement btn_search_back;

    AppiumDriver driver;



    //public IElement input_searchKey;
   // public IElement btn_timer;


    public VendorHomePage(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
        driver=inti.getAppiumDriver();
    }

    public void clickOnGoToSetting(){
        if(checkElementDisplay(btn_GoToSetting)){
            btn_GoToSetting.click();
        }

    }

    public boolean isGoToSettingDisplay(){
        return checkElementDisplay(btn_GoToSetting);
    }
    public void selectOrdersTabAll(){
       btn_All.click();

    }
    public void selectOrdersTabPreparing(){
        btn_preparing.click();

    }
    public void selectOrdersTabPending(){
        btn_pending.click();

    }
    public void selectOrdersTabPOP(){
        btn_pop.click();

    }
    public VendorHomePage openSearch(){
        btn_search.get(btn_search.size()-1).click();
        return this;

    }
    public VendorHomePage searchOrder(String searchKey){
        txt_search.sendKeys(searchKey);
        return this;
    }
    public void clickOnNewOrder(){
        txt_newOrder.click();
    }

    public String getOrderId(int pos){
        return txt_orderId.get(pos).getText();
    }
    public String getItemDetail(int pos){
        return txt_ItemDetail.get(pos).getText();
    }
    public String getItemPrice(int pos){
        return txt_itemPrice.get(pos).getText();
    }
    public void clickOnFirstOrder(int pos){
        txt_orderId.get(pos).click();
    }
    public String getRestaurantAddress(){
        if(checkElementDisplay(txt_restaurantNameNEw)){
            return txt_restaurantNameNEw.getText().toString();
        }
        else {
            return txt_restaurantName.getText().toString();
        }
    }
    public void clickOnPreparationTimeButton(){
        btn_preparationTime.click();
    }
    public boolean isNewOrderPopUpDisplay(){
        return checkElementDisplay(txt_newOrder);
    }
    public String getDefaultPreparationTime(){
        return txt_defaultPreparationTime.getText();
    }

    public boolean checkElementDisplay(IElement element){
        try{
            if(element.isDisplayed()){
                return true;
            }
        }
        catch(Exception e){
            return false;
        }
        return false;

    }

    public boolean checkElementDisplay(List<IElement> element){
        try{
            if(element.size()>0){
                return true;
            }
        }
        catch(Exception e){
            return false;
        }
        return false;

    }

    public boolean isOrderDisplayAfterPick(){
        return checkElementDisplay(txt_orderId);
    }

    public String getOrderStatusFromHomeScreen(){
        return "";
    }

    public String getNewOrderText() {
        String text = txt_newOrder.getText();
        return text;
    }

    public boolean isNewOrderReceived() {
        return txt_newOrder.isDisplayed();
    }

    public boolean isHomePage() {
        return txt_orders.isDisplayed();
    }

    public boolean isPreparingOrder() {
        return checkElementDisplay(tag_preparing);
    }

    public boolean isFoodReady() {
        return checkElementDisplay(btn_food_ready);
    }

    public VendorHomePage makeOrderReady() {
        while(checkElementDisplay(btn_food_ready)) {
            btn_food_ready.click();
        }
        return this;
    }

    public boolean isAwaitingCallDisplayed() {
        return checkElementDisplay(txt_awaitingcall);
    }

    public boolean isOrderReady() {
        return checkElementDisplay(tag_ready);
    }

    public boolean trackDE() {
        return checkElementDisplay(btn_call_order) && checkElementDisplay(btn_track_order);
    }

    public VendorHomePage goToPastOrders() {
        tab_past_orders.click();
        return this;
    }

    public String getGlobalPrepTime() {
        return txt_global_preptime.getText().replaceAll("[^.0-9]", "").replaceAll("^0+","");
    }

    public boolean clickOnNewOrder(String order_id){
        boolean flag = false;
        txt_newOrder.click();
        AndroidUtil.pressBackKey(driver);
        System.out.println("order id is"+ order_id.substring(Math.max(0, order_id.length() - 4)));
        if(checkElementDisplay(btn_cross)) {
            for (int i = 0; i < txt_order_id.size(); i++) {
                if ((txt_order_id.get(i).getText().toString().replace("#","")).equalsIgnoreCase(order_id.substring(Math.max(0, order_id.length() - 4)))) {
                    txt_order_id.get(i).click();
                    flag = true;
                    break;
                }
            }
        }
        if(checkElementDisplay(txt_newOrder))
            txt_newOrder.click();
        return flag;
    }

    public VendorHomePage dismissOrderView() {
        if(checkElementDisplay(btn_cross))
            btn_cross.click();
        return this;
    }

    public VendorHomePage clearSearch() {
        if(checkElementDisplay(btn_search_clear))
            btn_search_clear.click();
        return this;
    }

    public VendorHomePage pressBackSearch() {
        if(checkElementDisplay(btn_search_back))
            btn_search_back.click();
        return this;
    }

}


