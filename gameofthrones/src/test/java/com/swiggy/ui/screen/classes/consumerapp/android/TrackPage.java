package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.List;

public class TrackPage extends AndroidUtil {

	public IElement txt_trackScreenTitle;
	public IElement txt_trackScreenSubTitle;
	public IElement btn_help;
	public IElement btn_pullUp;
	public List<IElement> orderStatus;
	public IElement txt_orderCancel;
	public IElement btn_okGotIt;
	public IElement preOrderDetails;
	public IElement cancelPreOrder;
	public IElement modifyOrder;


	public TrackPage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(), this, init);
	}

	public String getOrderId() {
		if (txt_trackScreenTitle.isDisplayed()) {
			return txt_trackScreenTitle.getText().toString().split("#")[1];
		} else {
			return null;
		}
	}

	public String getOrderAmount() {
		if (txt_trackScreenSubTitle.isDisplayed()) {
			String str = txt_trackScreenSubTitle.getText().toString().split(",")[1];
			String numberOnly = str.replaceAll("[^.0-9]", "");
			return numberOnly;
		} else {
			return null;
		}
	}

	public void getAllText() {
		for (int i = 0; i < orderStatus.size(); i++) {
			System.out.println(orderStatus.get(i).getText());
		}
	}
	public String getOrdercancelText(){
		return txt_orderCancel.getText();
	}
	public String getOrderStatus(){
		return txt_orderCancel.getText();
	}

	public void validateTrackPageFromDeeplink() {
		System.out.println("Track Page Navigation is Successfull.Started Validation..");
		System.out.println("Validating Track page navigation:");
		String Order_id = getOrderId();
		System.out.println("Order_id: " + Order_id);
		if (Order_id != null)

			System.out.println("Track page Navigation and Validation successfull");
		else
			System.out.println("Track page Navigation and Validation is not successfull");
	}
	public void clickOnOkGotItbutton(){
		btn_okGotIt.click();
	}


	public void clickOnHelp(){
		btn_help.click();
	}
	public void clickOncancelPreOrder(){
		cancelPreOrder.click();
	}

	public void clickOnModifyOrder(){
		modifyOrder.click();
	}

	public void viewOrderDetails(){
		preOrderDetails.click();
	}





}