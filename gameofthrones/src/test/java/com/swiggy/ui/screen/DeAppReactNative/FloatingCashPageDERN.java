package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.ArrayList;
import java.util.List;

public class FloatingCashPageDERN extends AndroidUtil {

    public IElement txt_currentBalance;
    public IElement txt_stringCurrentBalance;
    public IElement btn_seeStatement;
    public IElement txt_sectionTitle;
    public List<IElement> txt_depositTitle;
    public List<IElement> txt_depositSubTitle;
    public IElement image_depositImage;
    public IElement btn_arrow;
    public List<IElement> txt_titleDetailView;
    public List<IElement> txt_subTitleDetailView;
    public IElement txt_depositCode;




    public FloatingCashPageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public String getCurrentBalance(){
        return txt_currentBalance.getText().toString();
    }
    public void openViewStatement(){
        btn_seeStatement.click();
    }
    public String getSectionTitle(){
       return txt_sectionTitle.getText().toString();
    }
    public ArrayList<String> getDepositsTitle(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_depositTitle.size();i++){
            al.add(txt_depositTitle.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getDepositsSubTitle(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_depositSubTitle.size();i++){
            al.add(txt_depositSubTitle.get(i).getText().toString());
        }
        return al;
    }
    public String getFloatingCashTitle(){
        return txt_stringCurrentBalance.getText().toString();
    }

    public void openDepositSection(int index){
        txt_depositTitle.get(index).click();
    }

    public ArrayList<String> getTitleOnDetailView(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_titleDetailView.size();i++){
            al.add(txt_titleDetailView.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getSubTitleOnDetailView(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_subTitleDetailView.size();i++){
            al.add(txt_subTitleDetailView.get(i).getText().toString());
        }
        return al;
    }
    public String getDepositCode(){
        return txt_depositCode.getText().toString();
    }
    public boolean isImageDisplaying(){
        return true;
    }










}
