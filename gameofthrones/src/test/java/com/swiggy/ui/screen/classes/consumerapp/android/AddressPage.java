package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;

import java.util.ArrayList;
import java.util.List;

public class AddressPage extends AndroidUtil {


	public IElement address;
	public IElement addAddress;
	public IElement home;
	public IElement work;
	public IElement other;
	public IElement view_more;
	public IElement complete_address;
	public List<IElement> savedAddress;
	public IElement btn_currentLocation;
	public IElement btn_confirmLocation;
	public IElement btn_addMoreDetail;
	public IElement input_addressLine1;
	public IElement input_addressLine2;
	public IElement btn_SaveAsWork;
	public IElement btn_save;
	public IElement btn_skip;
	public IElement input_enterAddress;
	public IElement taponLocation;
	public IElement txt_otherName;
	public IElement btn_saveAsOther;
	public List<IElement> btn_editAddress;
	public List<IElement> btn_deleteAddress;
	public IElement btn_deleteAddressConfirmationYes;
	public IElement btn_deleteAddressConfirmationNo;
	public IElement input_area;
	public IElement setCurrentLocation;







	HomePage homePage;
	AppiumDriver driver;


	public AddressPage(InitializeUI init) {
		super(init);
		initElements(init.getAppiumDriver(),this,init);
		homePage=new HomePage(init);
		driver=init.getAppiumDriver();
	}
	

	
	public void tapOnLocation() {
		address.click();

	}
	
	public void selectSavedAddress(String titleOfSavedAddress) {

		for(int i=0;i<savedAddress.size();i++){
			if(savedAddress.get(i).getText().toString().equals(titleOfSavedAddress)){
				savedAddress.get(i).click();
			}
		}
		//homePage.removeSwiggyPop();

		/*if(al.contains(titleOfSavedAddress)){
			savedAddress.get(al.indexOf(titleOfSavedAddress)).click();
		}*/


	}
	
	public void goToAddressPage() {
		address.click();
	}
	
	public void selectLocationAsHome()
	{
		home.click();
	}
	
	public void selectLocationAsWork()
	{
		work.click();
	}
	
	public void selectLocationAsOther()
	{
		other.click();
	}
	
	public void viewMore()
	{
		view_more.click();
	}

	public ArrayList<String> getListofSavedAddress(){
		ArrayList<String> al=new ArrayList<>();
		if(savedAddress.get(0).isDisplayed()){
			for(int i=0;i<savedAddress.size();i++){
				al.add(savedAddress.get(i).getText().toString());
			}
		}
		return al;
	}

	public void selectUserCurrentLocation(){
		btn_currentLocation.click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		btn_confirmLocation.click();

	}

	public void enterNewAddress(String saveAsName,String OthersName){
		input_addressLine1.clear();
		input_addressLine2.clear();
		AndroidUtil.sleep();
		input_addressLine1.sendKeys("Test Order Flat no" );
		input_addressLine2.sendKeys("Test Order");
		if(checkElementDisplay(input_area)){
			input_area.clear();
			input_area.sendKeys("Test Area");
		}
		hideKeyboard();
		if(saveAsName.equalsIgnoreCase("work")){
			btn_SaveAsWork.click();
		}
		if(saveAsName.equalsIgnoreCase("other")){
			btn_saveAsOther.click();
			txt_otherName.sendKeys(OthersName);
			hideKeyboard();

		}


		/*for(int i=0;i<btn_SaveAs.size();i++){
			if(btn_SaveAs.get(i).getText().toString().equalsIgnoreCase(saveAsName)){
				btn_SaveAs.get(i).click();
				break;
			}
		}

		if(saveAsName.equalsIgnoreCase("Other")){
		}*/


		clickonSaveAddress();
	}

	public void clickonSaveAddress(){
		hideKeyboard();
		btn_save.click();
	}

	public void clickonSkipAddress(){
		if(checkElementDisplay(btn_skip)){
			btn_skip.click();
		}
		else{
			if(checkElementDisplay(btn_addMoreDetail)){
				btn_addMoreDetail.click();
			}
			btn_skip.click();
		}

	}
	public void clickonButtonAddMoreDetail(){

		int count=10;
		//System.out.println(btn_addMoreDetail.isEnabled());
		while(!checkElementDisplay(btn_addMoreDetail) && count>0) {
			AndroidUtil.sleep();
			count--;
		}
		if(checkElementDisplay(btn_addMoreDetail)){
			btn_addMoreDetail.click();}
	}
	public void searchLocation(String location){
		input_enterAddress.click();
		input_enterAddress.sendKeys(location);
		savedAddress.get(0).click();

	}
	public void tapOnLocationToSearchManually(){
		//System.out.println(taponLocation.getText());
		taponLocation.click();
	}
	public void clickonBtnConfirmLocation(){
		btn_confirmLocation.click();
	}
	public void enterOthersName(String name){
		txt_otherName.sendKeys(name);
	}
	public void clickonEditAddress(int pos){
		btn_editAddress.get(pos).click();
	}
	public void clickOnDeleteAddress(int pos){
		btn_deleteAddress.get(pos).click();
	}
	
	public void clickOnDeleteAddressConfirmationNo(){
		btn_deleteAddressConfirmationNo.click();
	}
	
	public void clickOnDeleteAddressConfirmationYes(){
		btn_deleteAddressConfirmationYes.click();
	}

	
	public void editEnteredAddress(){
		AndroidUtil.sleep();
		int size=btn_editAddress.size();
		clickonEditAddress(size-1);

	}
	public void deleteEnteredAddress(){
		AndroidUtil.sleep();
		System.out.println("failing here");
		int size=btn_deleteAddress.size();
		System.out.println(size);
		clickOnDeleteAddress(size-1);
		btn_deleteAddressConfirmationYes.click();

	}


	public boolean checkElementDisplay(IElement element){
		try{
			if(element.isDisplayed()){
				return true;
			}
		}
		catch(Exception e){
			return false;
		}
		return false;

	}
	public void hideKeyboard(){
		try{
			driver.hideKeyboard();
		}
		catch(Exception e){

		}
	}

	public void setCurrentLocation()
	{
		setCurrentLocation.click();
	}




}
