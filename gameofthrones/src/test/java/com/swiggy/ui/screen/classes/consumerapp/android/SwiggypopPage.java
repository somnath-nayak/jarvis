package com.swiggy.ui.screen.classes.consumerapp.android;

import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import junit.framework.Assert;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static com.swiggy.ui.utils.AndroidUtil.Direction.DOWN;
import static com.swiggy.ui.utils.AndroidUtil.Direction.UP;

public class SwiggypopPage extends AndroidUtil {

    public IElement txt_popHeader;
    public IElement image_firstItem;
    public List<IElement> pointText;
    public IElement txt_firstItemPrice;
    public IElement txt_detailViewItemName;
    public IElement txt_detailViewItemDetail;
    public IElement txt_detailViewItemPrice;
    public IElement txt_addToCartCustomisationText;
    public IElement txt_confirmAddressAndProceed;
    public IElement btn_proceedToPaymentNoPreferred;
    public IElement txt_NoPreferredPayment;
    public IElement btn_addAddressAndProceed;
    public IElement txt_cartAddressHeader;
    public IElement btn_backButton;
    public IElement txt_nextAvailableMessage;
    public List<IElement> pop_items;
    public IElement btn_okGotIt;
    public IElement selectAddress;
    public IElement clickOnSelectHomeAddress;
    public IElement clickonChangePayment;

    //POP filter
    public IElement section_POPfilter;
    public IElement button_veg;
    public IElement buttonnon_veg;
    public IElement button_under99;
    public IElement firstpopfilteritem;
    public IElement txt_popfiltertitle;
    public IElement button_checkbacklater;
    public IElement txt_fornextbatchin;


    private AppiumDriver<MobileElement> driver;


    public SwiggypopPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        driver = init.getAppiumDriver();

    }

    public String getpopHeader() {
        return txt_popHeader.getText().toString();
    }

    public void clickOnFirstPOPItem() {
        scrolltillElement(image_firstItem);
        image_firstItem.click();
    }

    public ArrayList<String> getPointText() {
        ArrayList<String> al = new ArrayList<>();
        for (int i = 0; i < pointText.size(); i++) {
            al.add(pointText.get(i).getText());
        }
        return al;

    }

    public String getFirstItemPriceFromPOPList() {
        String str = txt_firstItemPrice.getText().toString();
        String numberOnly = str.replaceAll("[^.0-9]", "");
        return numberOnly;
    }

    public String getItemNameFromDetailView() {
        return txt_detailViewItemName.getText().toString().split("  ")[1].trim();
    }

    public String getItemDetailFromDetailView() {
        return txt_detailViewItemDetail.getText().toString();
    }

    public void clickonConfirmAddressAndProceed() {
        txt_confirmAddressAndProceed.click();
    }

    public void scrolltillElement(IElement element) {
        TouchAction touchAction = new TouchAction(driver);
        while (!checkElementDisplay(element, null)) {
            touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        }
    }

    public boolean checkElementDisplay(IElement element, List<IElement> elements) {
        try {
            if (elements == null) {
                if (element.isDisplayed()) {
                    return true;
                }
            } else {
                if (elements.size() > 1) {
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }

    public void clickonProceedToPay() {
        /*if(txt_NoPreferredPayment.getText().equalsIgnoreCase("No preferred payment method yet")){
			btn_proceedToPaymentNoPreferred.click();
		}*/
        if (checkElementDisplay(txt_NoPreferredPayment, null)) {
            btn_proceedToPaymentNoPreferred.click();
        }


    }

    public void clickonAddAddressAndProceed() {
        btn_addAddressAndProceed.click();
    }

    public void changePaymentMethod() {
        clickonChangePayment.click();
        scrollDownOnPopPage();
    }


    public void clickonSelectAddress() {
        selectAddress.click();

    }

    public void selecHomeAddress() {
        clickOnSelectHomeAddress.click();
    }

    public String getCartAddressHeader() {
        return txt_cartAddressHeader.getText().toString();
    }

    public void clickonBackButton() {
        btn_backButton.click();
    }

    public boolean isNextAvailableTextDisplay() {
        return checkElementDisplay(txt_nextAvailableMessage, null);
    }

    public void clickOnNextAvailableButton() {
        txt_nextAvailableMessage.click();
    }

    public void clickOnFirstPOPImageIos() {
        //scrollDownOnPopPage();
        pop_items.get(1).click();
        System.out.println("Item added");
        AndroidUtil.sleep(3000);
    }


    public void clickOnFirstPOPImageAND() {
        List<MobileElement> list = driver.findElements(By.xpath("//XCUIElementTypeOther/XCUIElementTypeImage"));
        list.get(4).click();
        pop_items.get(1).click();
        System.out.println("Item added");
        AndroidUtil.sleep(3000);
    }

    public void scrollDownOnPopPage() {


        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();

    }

    public void clickOnOkGotIt() {
        btn_okGotIt.click();
    }

    //POP FF

    public void swipedowntopoponboardingpage() {
        scrollios(driver, DOWN);
    }

    public void swipedontopoplisting() {
        scrollios(driver, UP);
    }

    public boolean verifyPOPfilersection() {
        boolean result = checkElementDisplay(section_POPfilter);
        return result;
    }

    public void clickOnvegfilter() {
        if (checkElementDisplay(button_veg))
            button_veg.click();
    }

    public void clickOnnonvegfilter() {
        if (checkElementDisplay(buttonnon_veg))
            buttonnon_veg.click();
    }

    public void clickOnUnder99filter() {
        if (checkElementDisplay(button_under99))
            button_under99.click();
    }

    public void verifyMultipleFilterVeg() {
        if (checkElementDisplay(button_veg)) {
            button_veg.click();
        }
        if (checkElementDisplay(button_under99)) {
            button_under99.click();
        } else {
            System.out.println("No under99 button and cant apply multiple filter");
        }

    }

    public void verifyMultipleFilterNonVeg() {
        if (checkElementDisplay(buttonnon_veg)) {
            buttonnon_veg.click();
        }
        if (checkElementDisplay(button_under99)) {
            button_under99.click();
        } else {
            System.out.println("No under99 button and cant apply multiple filter");
        }

    }


    public boolean validateVegfilter() {
        boolean result = checkElementDisplay(buttonnon_veg);
        return result;
    }

    public boolean validateNonvegfilter() {
        boolean result = checkElementDisplay(button_veg);
        return result;
    }

    public boolean validateunder99filter() {
        boolean result = checkElementDisplay(buttonnon_veg);
        return result;
    }

    public void clickonfirstitemafterapplyingfilter() {
        firstpopfilteritem.click();
    }

    public void ValidatingPOPfiltertitle() {
        String Expected = txt_popfiltertitle.getText();
        String Actual = "Single Serve Meals. Free Delivery. No Extra Charges.";
        Assert.assertEquals(Actual, Expected);

    }

    public boolean validatepopfilterinnonserviceablearea() {
        boolean result = checkElementDisplay(txt_popfiltertitle);
        return result;


    }

    public void validatepopfiltercards() {
        if (checkElementDisplay(txt_fornextbatchin)) {
            AndroidUtil.scrollios(driver, UP);
            if (checkElementDisplay(txt_popfiltertitle)) {

                String A = txt_popfiltertitle.getText();
                Assert.assertEquals(A, Constants.txt_popfiltertitle);
            } else {
                System.out.println("No filter section which is expected");
            }
        } else {
            System.out.println("No batching");
        }

    }
}
