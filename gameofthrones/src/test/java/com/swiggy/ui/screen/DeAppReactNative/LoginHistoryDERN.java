package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.ArrayList;
import java.util.List;

public class LoginHistoryDERN extends AndroidUtil {

    public IElement txt_titleCurrentBalance;
    public IElement txt_timeInHours;
    public IElement txt_timeInMinute;
    public IElement txt_shiftLogin;
    public List<IElement> txt_pastLoginInformationTitle;
    public List<IElement> txt_pastLoginInformationSubTitle;
    public List<IElement> txt_cardHeaderOnDetailView;
    public List<IElement> txt_loginTimeOnCardHeaderDetailView;
    public List<IElement> txt_loginTimeOnCardDetailLeftText;
    public List<IElement> txt_loginTimeOnCardDetailRightText;

    public LoginHistoryDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public String getTitleCurrentBalance(){
        return txt_titleCurrentBalance.getText().toString();
    }
    public String getTimeInHours(){
        return txt_timeInHours.getText().toString();
    }
    public String getTimeInMinute(){
        return txt_timeInMinute.getText().toString();
    }
    public String getShiftLoginTime(){
        return txt_shiftLogin.getText().toString();
    }
    public void clickOnShiftLoginTime(){
        txt_shiftLogin.click();
    }
    public ArrayList<String> getPastLoginInformationCardTitle(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_pastLoginInformationTitle.size();i++){
            al.add(txt_pastLoginInformationTitle.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getPastLoginInformationCardSubTitle(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_pastLoginInformationSubTitle.size();i++){
            al.add(txt_pastLoginInformationSubTitle.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getCardViewHeader(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_cardHeaderOnDetailView.size();i++){
            al.add(txt_cardHeaderOnDetailView.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getTotalLoginTimeOnCardHeaderDetailView(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_loginTimeOnCardHeaderDetailView.size();i++){
            al.add(txt_loginTimeOnCardHeaderDetailView.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getLoginTimeOnCardDetailLeftText(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_loginTimeOnCardDetailLeftText.size();i++){
            al.add(txt_loginTimeOnCardDetailLeftText.get(i).getText().toString());
        }
        return al;
    }
    public ArrayList<String> getLoginTimeOnCardDetailRightText(){
        ArrayList<String> al=new ArrayList<>();
        for(int i=0;i<txt_loginTimeOnCardDetailRightText.size();i++){
            al.add(txt_loginTimeOnCardDetailRightText.get(i).getText().toString());
        }
        return al;
    }





}
