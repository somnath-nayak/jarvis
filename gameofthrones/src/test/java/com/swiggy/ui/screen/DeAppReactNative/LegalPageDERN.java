package com.swiggy.ui.screen.DeAppReactNative;

import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import java.util.List;

public class LegalPageDERN extends AndroidUtil {

    public List<IElement> txt_legalTitle;
    public List<IElement> txt_legalSubTitle;
    public List<IElement> btn_readAndAccept;
    public IElement btn_accept;



    public LegalPageDERN(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(),this,inti);
    }

    public String getLegalTitle(int pos){
        return txt_legalTitle.get(pos).toString();
    }
    public String getLegalSubTitle(int pos){
        return txt_legalSubTitle.get(pos).toString();
    }
    public void clickOnReadAndAcceptButton(int pos){
        btn_readAndAccept.get(pos).click();
    }
    public void clickOnAcceptButton(){
        btn_accept.click();
    }





}
