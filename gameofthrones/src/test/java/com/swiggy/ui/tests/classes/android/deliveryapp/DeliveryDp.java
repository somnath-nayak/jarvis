package com.swiggy.ui.tests.classes.android.deliveryapp;

import com.swiggy.ui.api.ApiConstants;
import com.swiggy.ui.api.CreateOrder;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import org.testng.annotations.DataProvider;
import com.swiggy.ui.pageclass.delivery.DeConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;

/**
 * Created by kiran.j on 1/8/18.
 */
public class DeliveryDp {

    @DataProvider(name = "masterotplogin")
    public Object[][] masterotplogin() {
        return new Object[][]{{DeConstants.de_id3, DeConstants.master_otp}};
    }

    @DataProvider(name = "login")
    public Object[][] login() {
        return new Object[][]{{DeConstants.de_id, DeConstants.app_version}};
    }

    @DataProvider(name = "invalidotplogin")
    public Object[][] invalidlogin() {
        return new Object[][]{{DeConstants.de_id, DeConstants.incorrect_otp}};
    }

    @DataProvider(name = "invalidDelogin")
    public Object[][] invalidDelogin() {
        return new Object[][]{{DeConstants.suspended_deid}};
    }

    @DataProvider(name = "createorder")
    public Object[][] createorder() {
        CreateOrder createOrder = new CreateOrder();
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
       String order_id = null;
       
        try{
        	 order_id = createOrder.createAndAssignOrder();
           }
      catch(Exception e)
       {
    	   DeliveryDataHelper deliveryDataHelper= new DeliveryDataHelper();
    	   order_id=deliveryDataHelper.getOrderIdForCAPRDFF("9990","12.923359", "77.587080" );
    	   try {
    		   
    		  
			deliveryServiceHelper.assignOrder(order_id);
			
		}
    	   catch (Exception ie) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	   
        }
        return new Object[][] {{DeConstants.de_id, DeConstants.app_version, order_id}};
    }

    @DataProvider(name = "batchorder")
    public Object[][] batchorder() {
    	String order_id1=null;
    	String order_id2=null;
    	
    	try{
        CreateOrder createOrder = new CreateOrder();
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
         order_id1 = createOrder.createOrder(ApiConstants.user2_Mobileno, ApiConstants.user2_password);
        Thread.sleep(10000);
         order_id2 = createOrder.createOrder(ApiConstants.user2_Mobileno,ApiConstants.user2_password);
         deliveryServiceHelper.mergeorder(order_id1, order_id2);
         createOrder.assignOrder(order_id1);
        }catch(Exception e)
    	{e.printStackTrace();}
    	return new Object[][] {{DeConstants.de_id, DeConstants.master_otp, order_id1, order_id2}};
    }
    
    @DataProvider(name = "profile")
    public Object[][] profile() {
        return new Object[][]{{1026,  DeConstants.app_version,DeConstants.profile}};
    }

    @DataProvider(name = "bankdetails")
    public Object[][] bankdetails() {
        return new Object[][]{{DeConstants.de_id, DeConstants.app_version,DeConstants.bank_details}};
    }
    
    @DataProvider(name = "hambuger")
    public Object[][] hamber() {
        return new Object[][]{{DeConstants.de_id, DeConstants.app_version}};
    }
    
    
    @DataProvider(name = "feedback")
    public Object[][] feedback() {
        return new Object[][]{{DeConstants.de_id,  DeConstants.app_version, DeConstants.Feedback}};
 
    }
    
    
    @DataProvider(name = "Ticketing")
    public Object[][] ticket() {
        return new Object[][]{{"216",  DeConstants.app_version,  DeConstants.LongdistanceOrder,}};
    }
    
    @DataProvider(name = "orderpayout")
    public Object[][] orderpayout() {
        return new Object[][]{{"216",  DeConstants.app_version,  DeConstants.OrderPayoutIncorrect,}};
    }
    
    
    @DataProvider(name = "manager")
    public Object[][] manager() {
        return new Object[][]{{"216",  DeConstants.app_version,  DeConstants.Managerrelatedissue,DeConstants.ManageDoseNotPickCalls}};
    }
    
    
    @DataProvider(name = "weeklyRelated")
    public Object[][] weeklyrelated() {
        return new Object[][]{{"216", DeConstants.app_version,  DeConstants.RecivedLessPayout}};
    }
    @DataProvider(name = "general")
    public Object[][] general() {
        return new Object[][]{{"216",  DeConstants.app_version,  DeConstants.GeneralIssues,DeConstants.NotRecivingorders}};
    }
    
    @DataProvider(name = "earninghistory")
    public Object[][] earninghistory() {
        return new Object[][]{{DeConstants.de_id, DeConstants.app_version}};
    }
    
    
}



