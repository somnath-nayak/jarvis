package com.swiggy.ui.tests.classes.android.deliveryapp;

import com.swiggy.ui.api.ApiConstants;
import com.swiggy.ui.api.CreateOrder;
import com.swiggy.ui.api.DeliveryHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryControllerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.ui.tests.classes.android.deliveryapp.DeliveryDp;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pageclass.delivery.*;
import com.swiggy.ui.pageobject.delivery.ProfilePageObject;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.AndroidUtil.Direction;

/**
 * Created by kiran.j on 1/8/18.
 */
public class LoginTest extends DeliveryDp {

	
	 private static final boolean fasle = false;
	public AndroidDriver driver;
	 DeliveryApplaunchPage launchPage ;
	 LoginPage loginPage;
	 LiveTasks liveTasks ;
	 LeftNavPage leftNavPage ;
	 DeliveryPage deliveryPage;
	 ProfilePage profilePage ;
	 BankdetailPage bankpage;
	 FeedsPage feedsPage;
	 OrderHistoryPage orderHistoryPage;
	 LegalPage legalPage ;
	 HambergurPage HambergurPage ;
	 FeedbackOptionPage FeedbackOptionPage;
	 SlotSignUpPage slotSignUpPage;
	 IssuesAndComplaintsPage IssuesAndComplaintsPage;
	 StopDutyPage StopDutyPage;
	 NoticePage noticePage ;
	 HomePage HomePage;
	 NewTaskPage NewTaskPage;
	 HelpPage HelpPage;
	 DeliveryServiceHelper deliveryServiceHelper;
	 DeliveryControllerHelper deliveryControllerHelper;
	 EarningHistory EarningHistory;
	 Loginhistory Loginhistory ;
	   
	   @BeforeClass
	    public void setUp(){
	   InitializeUI inti = new InitializeUI();
	   deliveryServiceHelper = new DeliveryServiceHelper();
	   deliveryControllerHelper = new DeliveryControllerHelper();
	  // AndroidUtil.runShellCommand("adb shell input swipe 300 400 500 100");
	   driver = inti.getAndroidDriver();
	   launchPage = new DeliveryApplaunchPage(inti);
	   loginPage=new LoginPage(inti);
	   liveTasks =new  LiveTasks(inti);
	   leftNavPage =new LeftNavPage(inti);
	   deliveryPage=new  DeliveryPage(inti);
	   profilePage = new ProfilePage(inti);
	   bankpage = new BankdetailPage(inti);
	   feedsPage = new FeedsPage(inti);
	   orderHistoryPage = new OrderHistoryPage(inti);
	   legalPage = new LegalPage(inti);
	   HambergurPage = new HambergurPage(inti);
	   noticePage = new NoticePage(inti);
	   FeedbackOptionPage=new FeedbackOptionPage(inti);
	   slotSignUpPage = new SlotSignUpPage(inti);
	   IssuesAndComplaintsPage=new IssuesAndComplaintsPage(inti);
	   HomePage=new HomePage(inti);
	   NewTaskPage=new NewTaskPage(inti);
	   HelpPage=new HelpPage(inti);
	   StopDutyPage=new StopDutyPage(inti);
	   EarningHistory=new EarningHistory(inti);
	  Loginhistory = new Loginhistory(inti);
	   }
	


    InitializeUI gameofthrones = new InitializeUI();
  //  AndroidDriver driver = gameofthrones.getAndroidDriver();

    public static String testorder_id = null;


    @BeforeMethod
    public void unassign() {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        try {
            if (testorder_id != null) {
                deliveryServiceHelper.unAssign(testorder_id, DeConstants.de_id);
                checkoutHelper.OrderCancel(ApiConstants.mobile_no, ApiConstants.password, testorder_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//working
 @Test(dataProvider = "login",priority=1,description ="verifyLogin",enabled=true)
    public void verifyLogin(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id,version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
        softAssert.assertAll();
    }

 //working
  @Test(dataProvider = "masterotplogin",priority=2,description ="Master otp",enabled=true)
    public void verifyMasterOtpLogin(String de_id, String master_otp) {
        SoftAssert softAssert = new SoftAssert();
        launchPage.allowAllPermission();
        loginPage.enterDeId(de_id);
        loginPage.submitLogin();
        loginPage.enterOtp(master_otp);
        loginPage.submitOtp();
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        softAssert.assertAll();
    }

  //working
   @Test(dataProvider = "invalidotplogin",priority=4,description ="Invalid otp",enabled= false)
    public void verifyInvalidOtpLogin(String de_id, String invalidotp) {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
       launchPage.allowAllPermission();
       loginPage.enterDeId(de_id);
       loginPage.submitLogin();
      loginPage.enterOtp(invalidotp);
       loginPage.submitOtp();
     //  softAssert.assertTrue(!liveTasks.isLiveTasksPage());
        softAssert.assertAll();
    }  
//working
     @Test(dataProvider = "invalidDelogin", priority=5,description ="tost message",enabled= true)
    public void verifytoast(String de_id) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.enterDeId(de_id);
        loginPage.submitLogin();
        Thread.sleep(1000);
        //softAssert.assertTrue(loginPage.isLoginPage());
        softAssert.assertAll();
    } 
//working 
   @Test(dataProvider = "createorder",priority=6,description ="create order",enabled= true )
    public void createordertest(String de_id, String version, String order_id) throws InterruptedException {
        testorder_id = order_id;
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id,version);
        Thread.sleep(1000);
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        liveTasks.dismissLeftNav();
        deliveryServiceHelper.makeDEFree(de_id);
        softAssert.assertAll();
    }  

   //working
  @Test(dataProvider = "login",priority=7,description ="login and navigate to profile page",enabled= true )
    public void test(String de_id,  String version) {
        SoftAssert softAssert = new SoftAssert();
         launchPage.allowAllPermission();
         loginPage.performLogin(de_id,version);
      // loginPage.enterDeId(de_id).submitLogin().enterOtp(master_otp).submitOtp();
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.profile);
        softAssert.assertAll();
    }
//working
  @Test(dataProvider = "createorder", priority=8,description ="Verify that de is not able to do stopduty in between the order process",enabled=true)
    public void stopDuty(String de_id, String version, String order_id) throws InterruptedException {
        testorder_id = order_id;
        SoftAssert softAssert = new SoftAssert();
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id,version);
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        Thread.sleep(1000);
       System.out.println("testing left nave");
        //liveTasks.selectFirstTask();
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.stop_duty);
        deliveryServiceHelper.makeDEFree(de_id);
        //softAssert.assertTrue(leftNavPage.isLeftNavOpen());
        softAssert.assertAll();
    } 
//working
  @Test(dataProvider = "createorder",priority=9,description ="loggout between order and verify the toast message",enabled= true)
    public void logout(String de_id, String version, String order_id) throws InterruptedException {
        testorder_id = order_id;
        SoftAssert softAssert = new SoftAssert();
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id,version);
        deliveryPage.confirmOrder();
        Thread.sleep(1000);
        deliveryPage.confirm();
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.logout);
        Thread.sleep(1000);
        deliveryServiceHelper.makeDEFree(de_id);
       // softAssert.assertTrue(leftNavPage.isLeftNavOpen());
        softAssert.assertAll();
    }
    
  
//working
   @Test(dataProvider = "createorder",priority=10,description ="unassign order",enabled= true)
    public void unassignOrder(String de_id, String version, String order_id) {
        try {
            testorder_id = order_id;
            SoftAssert softAssert = new SoftAssert();
           // DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
            launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
            Thread.sleep(1000);
            deliveryServiceHelper.unAssign(order_id, de_id);
            Thread.sleep(1000);
         // Assert.assertTrue(liveTasks.isOrderUnassigned(), "Order Unassignment Failed");
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    } 
//working
    @Test(dataProvider = "createorder",priority=11,description ="reassign order",enabled= true)
    public void reassignOrder(String de_id, String version, String order_id) {
        try {
            SoftAssert softAssert = new SoftAssert();
            testorder_id = order_id;
            DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
           // LaunchPage launchPage = new LaunchPage(driver);
             launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
            deliveryServiceHelper.reassign(order_id, DeConstants.reassign_status_code, DeConstants.city_code);
           // softAssert.assertTrue(liveTasks.isOrderUnassigned(), "Order Unassignment Failed");
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

//confirm order (CAPRD)
    @Test(dataProvider = "createorder",priority=12,description ="confirm order",enabled= false)
    public void confirmOrder(String de_id, String version, String order_id) {
        try {
            SoftAssert softAssert = new SoftAssert();
            testorder_id = order_id;
           launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
            // liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
            //softAssert.assertTrue(liveTasks.isTasksDisplayed());
            //softAssert.assertAll();
            deliveryServiceHelper.makeDEFree(de_id);
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
    
 
    @Test(dataProvider = "createorder",priority=13,description ="arrive order",enabled= true)
    public void arrivedOrder(String de_id, String version, String order_id) {
        try {
            SoftAssert softAssert = new SoftAssert();
            testorder_id = order_id;
           // LaunchPage launchPage = new LaunchPage(driver);
           launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
           // softAssert.assertTrue(liveTasks.isLiveTasksPage());

           // liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
         //   softAssert.assertTrue(liveTasks.isTasksDisplayed());
            liveTasks.selectFirstTask();
            
            deliveryPage.navigateToCustomerDetails();
            //Assert.assertTrue(!deliveryPage.isCallCustomerEnabled());
            deliveryPage.goBackToDeliveryPage();
            deliveryPage.markArrived();
            deliveryPage.confirm();
            deliveryServiceHelper.makeDEFree(de_id);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
    
    @Test(dataProvider = "createorder",priority=14,description ="arrive order",enabled=true)
    public void pickedupOrder(String de_id, String version, String order_id) {
        try {
            SoftAssert softAssert = new SoftAssert();
            testorder_id = order_id;
           // LaunchPage launchPage = new LaunchPage(driver);
           // DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
             launchPage.allowAllPermission();
             loginPage.performLogin(de_id, version);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
          // liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
           // softAssert.assertTrue(liveTasks.isTasksDisplayed());
            liveTasks.selectFirstTask();
            deliveryPage.markArrived();
            deliveryPage.confirm();
            //softAssert.assertTrue(liveTasks.isLiveTasksPage());
          //liveTasks.selectFirstTask();
            String bill = deliveryPage.getTotalBill();
            deliveryPage.markPickedUp();
            deliveryPage.confirm();
            deliveryPage.enterBill(bill);
            deliveryControllerHelper.orderPickedupFromController(order_id);
            deliveryServiceHelper.makeDEFree(de_id);
           
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

   
    
    @Test(dataProvider = "profile",priority=15,description ="profile",enabled= true)
    public void VerifyprofileDetail(String de_id, String version, String profile) {
    	
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        System.out.println("deid is "+de_id );
        loginPage.performLogin(de_id, version);
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(profile);
        profilePage.scrollprofile();
       // runShellCommand(String command)
        AndroidUtil.runShellCommand("adb shell input swipe 300 400 500 100");
        AndroidUtil.runShellCommand("adb shell input swipe 300 400 500 100");
        AndroidUtil.runShellCommand("adb shell input swipe 300 400 500 100");
        AndroidUtil.runShellCommand("adb shell input swipe 300 400 500 100");

        profilePage.clickOn_DownloadInsur();
        //softAssert.assertTrue(profilePage.isProfilePage());
        softAssert.assertAll();
    }   
    
    
    @Test(dataProvider="bankdetails",priority=16,description ="bankdetail",enabled= true)
    public void Verifybankdetails(String de_id, String version, String bankdetail) throws InterruptedException
    {
    	SoftAssert softAssert = new SoftAssert();
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(bankdetail);
         Thread.sleep(1000);
         bankpage.clickon_backbutton();
       // bankpage.clickOn_Confirmcheckbox();
        //bankpage.ckickOn_savebutton();
        //bankpage.clickon_backbutton();
       // softAssert.assertAll();
    	
    }
    
   @Test(dataProvider = "createorder",priority=17,description ="rejectorder",enabled= true)
    public void rejectOrder(String de_id, String version, String order_id) {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id;
        launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        DeliveryPage deliveryPage = liveTasks.selectFirstTask();
       // DeliveryPage deliveryPage = new DeliveryPage(driver);
         deliveryPage.rejectOrder();
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        softAssert.assertAll();
    }

    @Test(dataProvider = "createorder",priority=18,description =" stopDutyAfterPickedupOrder",enabled= true)
    public void stopDutyAfterPickedupOrder(String de_id, String version, String order_id) throws InterruptedException {
            SoftAssert softAssert = new SoftAssert();
            testorder_id = order_id;
            //LaunchPage launchPage = new LaunchPage(driver);
          //  DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
            launchPage.allowAllPermission();
             loginPage.performLogin(de_id, version);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
             liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            
           deliveryPage.confirm();
            softAssert.assertTrue(liveTasks.isTasksDisplayed());
             liveTasks.selectFirstTask();
            deliveryPage.markArrived().confirm();
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
          liveTasks.selectFirstTask();
            String bill = deliveryPage.getTotalBill();
            deliveryPage.markPickedUp().confirm();
            deliveryPage.enterBill(bill);
            deliveryControllerHelper.orderPickedupFromController(order_id);
             liveTasks.selectLeftNav();
            leftNavPage.stopDuty();
            deliveryServiceHelper.makeDEFree(de_id);
            softAssert.assertTrue(leftNavPage.verifyStartDuty());
            softAssert.assertAll();
    }

    @Test(dataProvider = "createorder",priority=19,description ="reachedOrder",enabled= true)
    public void reachedOrder(String de_id, String version, String order_id) throws InterruptedException {
            SoftAssert softAssert = new SoftAssert();
            //LaunchPage launchPage = new LaunchPage(driver);
            testorder_id = order_id;
         //   DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
             launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
//            liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
           // softAssert.assertTrue(liveTasks.isTasksDisplayed());
             liveTasks.selectFirstTask();
            deliveryPage.markArrived().confirm();
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
          //  liveTasks.selectFirstTask();
            String bill = deliveryPage.getTotalBill();
            deliveryPage.markPickedUp().confirm();
            deliveryPage.enterBill(bill);
            deliveryControllerHelper.orderPickedupFromController(order_id);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            Thread.sleep(1000);
            liveTasks.selectFirstTask();
            deliveryPage.reached();
            deliveryServiceHelper.makeDEFree(de_id);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
    } 
    
    

    @Test(dataProvider = "createorder",priority=20,description ="deliveredOrder",enabled=true)
    public void deliveredOrder(String de_id, String version, String order_id) throws InterruptedException  {
            SoftAssert softAssert = new SoftAssert();
           // LaunchPage launchPage = new LaunchPage(driver);
            testorder_id = order_id;
           // DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
            launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            //liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
           // softAssert.assertTrue(liveTasks.isTasksDisplayed());
            liveTasks.selectFirstTask();
            deliveryPage.markArrived();
            deliveryPage.confirm();
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            //liveTasks.selectFirstTask();
            String bill = deliveryPage.getTotalBill();
            deliveryPage.markPickedUp().confirm();
            deliveryPage.enterBill(bill);
            deliveryControllerHelper.orderPickedupFromController(order_id);
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            Thread.sleep(1000);
          liveTasks.selectFirstTask();
            deliveryPage.reached();
//            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            //liveTasks.selectFirstTask();
            deliveryPage.deliverd();
           // softAssert.assertTrue(feedbackPage.isFeedBackPage());
            softAssert.assertAll();
    }

  

    @Test(dataProvider = "login",priority=21,description ="verifyFeedsPage",enabled= true)
    public void verifyFeedsPage(String de_id, String version) {
            SoftAssert softAssert = new SoftAssert();
            //LaunchPage launchPage = new LaunchPage(driver);
             launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
           // softAssert.assertTrue(liveTasks.isLiveTasksPage());
           liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.feeds);
            //FeedsPage feedsPage = new FeedsPage(driver);
           // softAssert.assertTrue(feedsPage.isFeedsPage());
            feedsPage.selectFirstFeed();
            //Assert.assertTrue(feedsPage.isFeedsPage());
            feedsPage.scrollThroughFeeds();
            softAssert.assertAll();
    }
    
    
   

    @Test(dataProvider = "login",priority=22,description ="Noticeboard",enabled=true)
    public void verifyNoticeBoard(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
             launchPage.allowAllPermission();
             loginPage.performLogin(de_id, version);
              liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.notice_board);
           // NoticePage noticePage = new NoticePage(driver);
            //softAssert.assertTrue(noticePage.isNoticePage());
            Assert.assertTrue(noticePage.isNoticeListDisplayed());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }


  
    @Test(dataProvider = "login",priority=23,description ="orderhistory",enabled= true)
    public void orderHistory(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
      
        DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
       launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        DeliveryPage deliveryPage = liveTasks.selectFirstTask();
//        deliveryPage.confirmOrder().confirm();
//        softAssert.assertTrue(liveTasks.isTasksDisplayed());
//        deliveryPage = liveTasks.selectFirstTask();
//        deliveryPage.markArrived().confirm();
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        deliveryPage = liveTasks.selectFirstTask();
//        String bill = deliveryPage.getTotalBill();
//        deliveryPage.markPickedUp().confirm();
//        deliveryPage.enterBill(bill);
//        deliveryControllerHelper.orderPickedupFromController(order_id);
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        deliveryPage = liveTasks.selectFirstTask();
//        deliveryPage.reached();
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        deliveryPage = liveTasks.selectFirstTask();
//        FeedbackPage feedbackPage = deliveryPage.deliverd();
//        softAssert.assertTrue(feedbackPage.isFeedBackPage());
        //TODO
        softAssert.assertAll();
    }

    @Test(dataProvider = "login",priority=24, description ="legle check",enabled= true)
    public void verifyLegalCheck(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
             launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
            //Assert.assertTrue(liveTasks.isLiveTasksPage());
             liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.legal);
            Assert.assertTrue(legalPage.isLegalPage());
            legalPage.navigateToGeneralTerms();
            Assert.assertTrue(legalPage.isGeneralTerms());
            Assert.assertTrue(legalPage.isTnCDisplayed());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
 
    @Test(dataProvider = "login",priority=25,description ="slotsignup",enabled= true)
    public void verifySlotSignUp(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
            launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
           liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.slot_sign_up);
            Assert.assertTrue(slotSignUpPage.isSlotSignUpPage());
            slotSignUpPage.update();
            Thread.sleep(1000);
          // Assert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

   

    @Test(dataProvider = "login",priority=26,description ="slotsignup",enabled= false)
    public void verifySlotSignUpToggle(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
           launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
            //softAssert.assertTrue(liveTasks.isLiveTasksPage());
             liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.slot_sign_up);
           // Assert.assertTrue(slotSignUpPage.isSlotSignUpPage());
            slotSignUpPage.toggleAvailableOff();
           // Assert.assertTrue(slotSignUpPage.isNoRadioEnabled());
            slotSignUpPage.toggleAvailableOn();
          //  Assert.assertTrue(slotSignUpPage.isYesRadioEnabled());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

   
    
   
    @Test(dataProvider = "login",priority=27,description ="logout",enabled= true)
    public void verifyLogout(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
            launchPage.allowAllPermission();
           loginPage.performLogin(de_id, version);
           // Assert.assertTrue(liveTasks.isLiveTasksPage());
            liveTasks.selectLeftNav();
            leftNavPage.logout();
            Assert.assertTrue(loginPage.isLoginPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

   
    @Test(dataProvider = "createorder",priority=28,description ="Deassistance",enabled=true)
    public void verifyDeAssistanceUnassign(String de_id, String version, String order_id) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id;
       launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);
//        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
        // liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
       // softAssert.assertTrue(liveTasks.isTasksDisplayed());
        liveTasks.selectFirstTask();
        // deliveryPage.navigateToHelp().selectReasonsFromString(DeConstants.unassign_order).selectFirstReason().unassign();
        deliveryPage.navigateToHelp();
        HelpPage.selectReasonsFromString(DeConstants.unassign_order);
        HelpPage.unassign();
        //deliveryServiceHelper.unAssign(order_id, de_id);
        
       // softAssert.assertTrue(liveTasks.isOrderUnassigned());
        softAssert.assertAll();
    }

    @Test(dataProvider = "createorder",priority=28,description ="verify call fm assistance",enabled=true)
    public void verifyDeAssistanceCallFm(String de_id, String version, String order_id) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id;
        launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
       liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
       // softAssert.assertTrue(liveTasks.isTasksDisplayed());
        liveTasks.selectFirstTask();
      deliveryPage.navigateToHelp();
      HelpPage.selectReasonsFromString(DeConstants.locate_rest);
      HelpPage.isCallFmDisplayed();
      deliveryServiceHelper.makeDEFree(de_id);
        softAssert.assertAll();
    }
    
   

    @Test(dataProvider = "login",priority=29,description ="order history",enabled= true)
    public void verifyOrderHistory(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.order_history);
        //OrderHistoryPage orderHistoryPage = new OrderHistoryPage(driver);
        //softAssert.assertTrue(orderHistoryPage.isOrderHistoryPage());
        //softAssert.assertTrue(orderHistoryPage.verifyOrderHistory());
        softAssert.assertAll();
    }

    @Test(dataProvider = "createorder",priority=30,description ="verifyLogin",enabled= true)
    public void verifyNextOrder(String de_id, String version, String order_id) throws InterruptedException {
        //SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id;
        launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
       //liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        CreateOrder createorder=new CreateOrder();
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
       String  orderid2=createorder.createOrder();
       deliveryServiceHelper.nextorder(orderid2, de_id);
//        softAssert.assertTrue(liveTasks.isTasksDisplayed());
//        deliveryPage = liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        Thread.sleep(1000);
        deliveryServiceHelper.makeDEFree(de_id);
       Thread.sleep(1000);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
    }

    @Test(dataProvider = "batchorder",priority=31,description ="batch order",enabled=false)
    public void verifyBatchOrder(String de_id, String version, String order_id1,String order_id2) throws InterruptedException {
        //SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id1;
        //DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
         launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
        
      // liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();

        //softAssert.assertTrue(liveTasks.isTasksDisplayed());
        liveTasks.selectFirstTask();
        deliveryPage.markArrived().confirm();

        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
       // liveTasks.selectFirstTask();
        String bill = deliveryPage.getTotalBill();
        deliveryPage.markPickedUp().confirm();
        deliveryPage.enterBill(bill);
        deliveryControllerHelper.orderPickedupFromController(order_id1);
          Thread.sleep(1000);
     //   softAssert.assertTrue(liveTasks.isLiveTasksPage());
      liveTasks.selectNthTask(2);
      Thread.sleep(100);
      deliveryPage.markArrived().confirm();
      String bill2 = deliveryPage.getTotalBill();
        deliveryPage.markPickedUp().confirm();
        deliveryPage.enterBill(bill2);
        deliveryControllerHelper.orderPickedupFromController(order_id2);
      Thread.sleep(100);
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectFirstTask();
        deliveryPage.reached();

       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectNthTask(2);
        deliveryPage.reached();

//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectFirstTask();
        deliveryPage.deliverd();

  //      softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectNthTask(2);
       deliveryPage.deliverd();
       deliveryServiceHelper.makeDEFree(de_id);
        //softAssert.assertTrue(feedbackPage.isFeedBackPage());
        //TODO
       // softAssert.assertAll();
    } 
    
    
  @Test(dataProvider = "hambuger",priority=32,description ="Hamburger",enabled= true)
    public void Verifyhamberger(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
      loginPage.performLogin(de_id, version);
      //  softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
      //  LeftNavPage left=new LeftNavPage(driver);
       // LeftNavPage.verifyitemsopt();
        leftNavPage.verifyitemsopt();
       // HambergurPage HambergurPage = new HambergurPage(driver);
        HambergurPage.clickOn_Hambergur();
        HambergurPage.clickOn_background();
        softAssert.assertAll();
    }
    

    @Test(dataProvider = "login",priority=33,description ="signup",enabled=true)
    public void verifySignUpslot(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
           // LaunchPage launchPage = new LaunchPage(driver);
            launchPage.allowAllPermission();
          loginPage.performLogin(de_id, version);
            softAssert.assertTrue(liveTasks.isLiveTasksPage());
             liveTasks.selectLeftNav();
             leftNavPage.selectItem(DeConstants.slot_sign_up);
            //SlotSignUpPage slotSignUpPage = new SlotSignUpPage(driver);
            softAssert.assertTrue(slotSignUpPage.isSlotSignUpPage());
             slotSignUpPage.clickon_date();
             slotSignUpPage.clickon_yes();
             slotSignUpPage.clickon_no();
             slotSignUpPage.clickon_maybe();
         slotSignUpPage.update();
            //softAssert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
    
    @Test(dataProvider = "login",priority=34,description ="signup slot",enabled= true)
    public void verifySignUpslot1(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
            //LaunchPage launchPage = new LaunchPage(driver);
            launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
            softAssert.assertTrue(liveTasks.isLiveTasksPage());
          liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.slot_sign_up);
            //SlotSignUpPage slotSignUpPage = new SlotSignUpPage(driver);
            softAssert.assertTrue(slotSignUpPage.isSlotSignUpPage());
             softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
   
    
    @Test(dataProvider = "login",priority=35,description ="order history",enabled= true)
    public void verifyOrderHistory1(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
         launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.order_history);
      //  OrderHistoryPage orderHistoryPage = new OrderHistoryPage(driver);
        //softAssert.assertTrue(orderHistoryPage.isOrderHistoryPage());
        //softAssert.assertTrue(orderHistoryPage.verifyOrderHistory());
        orderHistoryPage.Click_OnOrders();
        orderHistoryPage.scrollorderdetailpage();
        softAssert.assertAll();
    }
     

    @Test(dataProvider = "login",priority=36,description ="order history",enabled=false)
    public void verifyOrderHistory2(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
      launchPage.allowAllPermission();
     loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
    liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.order_history);
        //OrderHistoryPage orderHistoryPage = new OrderHistoryPage(driver);
        //softAssert.assertTrue(orderHistoryPage.isOrderHistoryPage());
        //softAssert.assertTrue(orderHistoryPage.verifyOrderHistory());
        orderHistoryPage.Click_OnOrders();
        orderHistoryPage.Click_OnHelp();
        softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "login",priority=37,description ="order history",enabled= true)
    public void verifyOrderHistory3(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
      loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
       liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.order_history);
        //OrderHistoryPage orderHistoryPage = new OrderHistoryPage(driver);
//        Assert.assertTrue(orderHistoryPage.isOrderHistoryPage());
//        Assert.assertTrue(orderHistoryPage.verifyOrderHistory());
        orderHistoryPage.Click_changedate();
       // orderHistoryPage.Click_changyear();
        orderHistoryPage.Click_onOkbutton();
        orderHistoryPage.Click_OnOrders();
        orderHistoryPage.scrollorderdetailpage();
        softAssert.assertAll();
    }
   
    @Test(dataProvider = "login",priority=38,description ="order history",enabled=true)
    public void verifyLegalCheck1(String de_id, String version) {
        try {
            SoftAssert softAssert = new SoftAssert();
           
            launchPage.allowAllPermission();
            loginPage.performLogin(de_id, version);
            
            liveTasks.selectLeftNav();
            leftNavPage.selectItem(DeConstants.legal);

          
            legalPage.navigateToGeneralTerms();
            //softAssert.assertTrue(legalPage.isTnCDisplayed());
            //softAssert.assertTrue(legalPage.isGeneralTerms(), "general");
            legalPage.navigateBack();
            
            //softAssert.assertTrue(legalPage.isLegalPage(),"legal");
            legalPage.navigateToCodeOfContact();
//            softAssert.assertTrue(legalPage.isTnCDisplayed());
//            softAssert.assertTrue(legalPage.isCodeOfContact(),"code");
            legalPage.navigateBack();
            
            //softAssert.assertTrue(legalPage.isLegalPage(),"legal");
            legalPage.navigateToPayoutst();
//            softAssert.assertTrue(legalPage.isTnCDisplayed());
//            softAssert.assertTrue(legalPage.isPayouts(),"payout");
            legalPage.navigateBack();
           // softAssert.assertTrue(legalPage.isLegalPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
    
    
    @Test(dataProvider = "login",priority=39,description ="Noticeboard",enabled=true)
    public void stopORstartDuty(String de_id, String version) {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);
        softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
       // StopDutyPage StopDutyPage=new StopDutyPage(driver);
        leftNavPage.selectItem(DeConstants.stop_duty);
      softAssert.assertTrue(StopDutyPage.isStopdutypopup());
        StopDutyPage.Click_yesbutton();
        leftNavPage.selectItem(DeConstants.legal);
        //LegalPage legal = new LegalPage(driver);
        legalPage.navigateBack();
        softAssert.assertTrue(liveTasks.isLiveTasksPage());
       StopDutyPage.Click_Startdutybutton();
        StopDutyPage.Click_yesbutton();
       softAssert.assertAll();
    }
    
    @Test(dataProvider = "feedback",priority=40,description ="Noticeboard",enabled=true)
    public void Verifyfeedback(String de_id, String version, String feedback) {
        SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
         launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
        softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.feed_back);
       // FeedbackOptionPage FeedbackOptionPage=new FeedbackOptionPage(driver);
        softAssert.assertTrue(FeedbackOptionPage.isFeedbackPage());
        FeedbackOptionPage.enterfeedback(feedback);
        FeedbackOptionPage.click_onSubmitbutton();
        softAssert.assertTrue(FeedbackOptionPage.isFeedbackPage());
        FeedbackOptionPage.enterfeedback(feedback);
        FeedbackOptionPage.click_onResetbutton();
        softAssert.assertAll();
 
       }
    
    @Test(dataProvider = "createorder",priority=41,description ="Noticeboard",enabled=true)
    public void arrivedOrder1(String de_id, String version, String order_id) {
        try {
            SoftAssert softAssert = new SoftAssert();
           // LaunchPage launchPage = new LaunchPage(driver);
             launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
            softAssert.assertTrue(liveTasks.isLiveTasksPage());
            liveTasks.selectFirstTask();
            deliveryPage.confirmOrder();
            deliveryPage.confirm();
            softAssert.assertTrue(liveTasks.isTasksDisplayed());
           liveTasks.selectFirstTask();
            deliveryPage.markArrived().confirm();
            deliveryPage.navigateToCustomerDetails();
            softAssert.assertTrue(deliveryPage.isCallCustomerEnabled());
            deliveryPage.goBackToDeliveryPage();
            softAssert.assertTrue(liveTasks.isTasksDisplayed());
            //softAssert.assertTrue(liveTasks.isLiveTasksPage());
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }
    
    
    
    @Test(dataProvider = "createorder",priority=42,description ="Noticeboard",enabled=true)
    public void verifyDeAssistanceUnassign1(String de_id, String version, String order_id) {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
         launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
        softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        softAssert.assertTrue(liveTasks.isTasksDisplayed());
         liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        softAssert.assertTrue(liveTasks.isTasksDisplayed());
         liveTasks.selectFirstTask();
        deliveryPage.markArrived().confirm();
   // deliveryPage.navigateToHelp()selectReasonsFromString(DeConstants.unassign_order).selectFirstReason().unassign();
        deliveryPage.navigateToHelp();
        HelpPage.selectReasonsFromString(DeConstants.unassign_order);
        HelpPage.unassign();
        
        softAssert.assertTrue(liveTasks.isOrderUnassigned());
        softAssert.assertAll();
    }
    
    

    @Test(dataProvider = "Ticketing",priority=43,description ="order related issue",enabled=true)
    public void Verifyissuesorderrelated(String de_id, String version, String issue) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.issues_complaints);
      //  IssuesAndComplaintsPage IssuesAndComplaintsPage=new IssuesAndComplaintsPage(driver);
       // softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesAndComplaintspage());
         IssuesAndComplaintsPage.Click_OnRaiseNewIssue();
        IssuesAndComplaintsPage.Click_OnOrderRelatedIssue();
        IssuesAndComplaintsPage .Click_OnLongdistanceorder();
        IssuesAndComplaintsPage.enterComplaints(issue);
        IssuesAndComplaintsPage.Click_OnConfirmbutton();
     //   softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesdetailpsge());
        liveTasks.clickBackButton();
         softAssert.assertAll();
 
       }
  
    @Test(dataProvider = "orderpayout",priority=44,description ="orderpayout",enabled= true)
    public void Verifyissuespayout(String de_id, String version, String issue) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
       launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);
      //  softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.issues_complaints);
        //IssuesAndComplaintsPage IssuesAndComplaintsPage=new IssuesAndComplaintsPage(driver);
        softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesAndComplaintspage());
         IssuesAndComplaintsPage.Click_OnRaiseNewIssue();
        IssuesAndComplaintsPage.Click_OnOrderRelatedIssue();
        IssuesAndComplaintsPage.Click_Orderpayout();
        IssuesAndComplaintsPage.enterComplaints(issue);
        IssuesAndComplaintsPage.Click_OnConfirmbutton();
        //softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesdetailpsge());
        liveTasks.clickBackButton();
         softAssert.assertAll();
 
       } 
    
    
    @Test(dataProvider = "manager",priority=45,description ="manager related issue",enabled= true)
    public void Managerrelatedissue(String de_id, String version, String issue,String issue1) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
       launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.issues_complaints);
       // IssuesAndComplaintsPage IssuesAndComplaintsPage=new IssuesAndComplaintsPage(driver);
       // softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesAndComplaintspage());
         IssuesAndComplaintsPage.Click_OnRaiseNewIssue();
        IssuesAndComplaintsPage.Click_Managerrelatedissue();
        IssuesAndComplaintsPage.enterComplaints(issue);
        IssuesAndComplaintsPage.Click_onNext();
       // OrderHistoryPage orderHistoryPage = new OrderHistoryPage(driver);
        orderHistoryPage.Click_OnOrders();
         IssuesAndComplaintsPage.Click_managerdoespickcall();
         IssuesAndComplaintsPage.enterComplaints(issue1);
         IssuesAndComplaintsPage.Click_OnConfirmbutton();
       // softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesdetailpsge());
        liveTasks.clickBackButton();
         softAssert.assertAll();
   }
    
    @Test(dataProvider = "weeklyRelated",priority=46,description ="weekly related issue",enabled= true)
    public void Weeklyrelatedissue(String de_id, String version, String issue) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
         launchPage.allowAllPermission();
         loginPage.performLogin(de_id, version);
      //  softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.issues_complaints);
       // IssuesAndComplaintsPage IssuesAndComplaintsPage=new IssuesAndComplaintsPage(driver);
       // softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesAndComplaintspage());
         IssuesAndComplaintsPage.Click_OnRaiseNewIssue();
        IssuesAndComplaintsPage.Click_weeklyrelated();
        IssuesAndComplaintsPage.Click_Recivedlesspayout();
        IssuesAndComplaintsPage. enterComplaints(issue);
        IssuesAndComplaintsPage.Click_OnConfirmbutton();
      //  softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesdetailpsge());
        liveTasks.clickBackButton();
         softAssert.assertAll();
 
   } 
    
    @Test(dataProvider = "general",priority=47,description ="general issue",enabled= true)
    public void generalissue(String de_id, String version, String issue,String issue1) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
       //softAssert.assertTrue(liveTasks.isLiveTasksPage());
       liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.issues_complaints);
        //IssuesAndComplaintsPage IssuesAndComplaintsPage=new IssuesAndComplaintsPage(driver);
        //softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesAndComplaintspage());
         IssuesAndComplaintsPage.Click_OnRaiseNewIssue();
     //  IssuesAndComplaintsPage.Click_generalissue().enterComplaints(issue).Click_onNext();
         IssuesAndComplaintsPage.Click_generalissue();
       IssuesAndComplaintsPage.enterComplaints(issue);
       IssuesAndComplaintsPage.Click_onNext();
       // IssuesAndComplaintsPage.Click_notrecivingorders().enterComplaints(issue1).Click_OnConfirmbutton();
       IssuesAndComplaintsPage.Click_notrecivingorders();
       IssuesAndComplaintsPage.enterComplaints(issue1);
       IssuesAndComplaintsPage.Click_OnConfirmbutton();
        softAssert.assertTrue(IssuesAndComplaintsPage.isIssuesdetailpsge());
        liveTasks.clickBackButton();
         softAssert.assertAll();
 
   } 
    
    @Test(dataProvider = "createorder",priority=48,description ="next order",enabled=true)
    public void verifyNextOrder1(String de_id, String version, String order_id) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
       // LaunchPage launchPage = new LaunchPage(driver);
        testorder_id = order_id;
        launchPage.allowAllPermission();
       loginPage.performLogin(de_id, version);     
      // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        //liveTasks.selectFirstTask();
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
        //softAssert.assertTrue(liveTasks.isTasksDisplayed());
      liveTasks.selectFirstTask();
        deliveryPage.markArrived().confirm();
        //DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        
//        softAssert.assertTrue(liveTasks.isTasksDisplayed());
//        deliveryPage = liveTasks.selectFirstTask();
        
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
//        softAssert.assertTrue(liveTasks.isTasksDisplayed());
//        deliveryPage = liveTasks.selectFirstTask();
        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
         //liveTasks.selectFirstTask();
        String bill = deliveryPage.getTotalBill();
        deliveryPage.markPickedUp().confirm();
        deliveryPage.enterBill(bill);
       // DeliveryControllerHelper deliveryControllerHelper = new DeliveryControllerHelper();
        deliveryControllerHelper.orderPickedupFromController(order_id);
        Thread.sleep(1000);
      CreateOrder createorder=new CreateOrder();
        String  order_id2 =createorder.createOrder();
        deliveryServiceHelper.nextorder(order_id2, de_id);
        deliveryPage.confirmOrder();
        deliveryPage.confirm();
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectFirstTask();
        deliveryPage.reached();
       // liveTasks.selectFirstTask();
        deliveryPage.deliverd();
        //softAssert.assertTrue(feedbackPage.isFeedBackPage());
        

        //softAssert.assertTrue(liveTasks.isLiveTasksPage());
        
     liveTasks.selectFirstTask();
        deliveryPage.markArrived().confirm();
        
        String bill2 = deliveryPage.getTotalBill();
        deliveryPage.markPickedUp().confirm();
        deliveryPage.enterBill(bill2);
        deliveryControllerHelper.orderPickedupFromController(order_id2);
        Thread.sleep(1000);
        liveTasks.selectFirstTask();
        deliveryPage.reached();
//        softAssert.assertTrue(liveTasks.isLiveTasksPage());
         liveTasks.selectFirstTask();
       deliveryPage.deliverd();
        //softAssert.assertTrue(feedbackPage1.isFeedBackPage());

        
    }
    
    
    @Test(dataProvider = "earninghistory",priority=47,description =" earnings",enabled= true)
    public void earnings(String de_id, String version) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
       //softAssert.assertTrue(liveTasks.isLiveTasksPage());
       liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.earning_history);
        EarningHistory.ClickOn_thisweek();
    }
    
    
    
    
    @Test(dataProvider = "login",priority=48,description =" loginhistory",enabled= true)
    public void loginhistory(String de_id, String version) throws InterruptedException {
       // SoftAssert softAssert = new SoftAssert();
      //  LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        loginPage.performLogin(de_id, version);
       //softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(DeConstants.loginhistory);
        Loginhistory.clickonTodaysofor();
    }
    
    
    @Test(dataProvider = "profile",priority=15,description ="profile",enabled= true)   
 public void Depositecash(String de_id, String version, String profile) {
    	
        SoftAssert softAssert = new SoftAssert();
        //LaunchPage launchPage = new LaunchPage(driver);
        launchPage.allowAllPermission();
        System.out.println("deid is "+de_id );
        loginPage.performLogin(de_id, version);
       // softAssert.assertTrue(liveTasks.isLiveTasksPage());
        liveTasks.selectLeftNav();
        leftNavPage.selectItem(profile);
       // profilePage.scrollprofile();
        profilePage.Click_OnDepositecash();
        profilePage.Click_Onicici();
        profilePage.clickon_backbutton();
        profilePage.Click_OnNovopay();
        profilePage.clickon_backbutton();
        profilePage.Click_OnSwiggyhub();
        profilePage.clickon_backbutton();
        profilePage.Click_onUpi();
        profilePage.clickon_backbutton();
       
        softAssert.assertAll();
    }   
    

    
    @AfterMethod
    public void clearAndLaunch() {
       this.driver.launchApp();
    

    }
}

