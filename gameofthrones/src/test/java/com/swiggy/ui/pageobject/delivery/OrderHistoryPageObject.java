package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/18/18.
 */
public class OrderHistoryPageObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Order History']")
    public MobileElement order_title;

    @AndroidFindBy(id = "date_edit")
    public MobileElement change_datebutton;

    @AndroidFindBy(id = "history_banner")
    public MobileElement history_title;

    @AndroidFindBy(id = "order_list")
    public MobileElement order_list;
    
    @AndroidFindBy(id = "title")
    public MobileElement orders;
    
    @AndroidFindBy(id = "in.swiggy.delivery:id/label")
    public MobileElement orderdetails;
    
    @AndroidFindBy(id = "in.swiggy.delivery:id/action_help")
    public MobileElement Help;
    
    
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='2018']")
    public MobileElement changeyear;
    
    @AndroidFindBy(xpath = "android:id/date_picker_month")
    public MobileElement changemonth;
    
    @AndroidFindBy(xpath="//android.view.View[@content-desc='05 February 2018']")
    public MobileElement changedate;
    
    @AndroidFindBy(id = "android:id/button1")
    public MobileElement okbutton;
    
}


