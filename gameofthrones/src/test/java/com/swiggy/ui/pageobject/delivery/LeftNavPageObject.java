package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 1/10/18.
 */
public class LeftNavPageObject {

    @AndroidFindBy(id = "design_menu_item_text")
    public MobileElement item_text;

    @AndroidFindBy(id = "alertTitle")
    public MobileElement alert_text;

    @AndroidFindBy(id = "android:id/button2")
    public MobileElement no;

    @AndroidFindBy(id = "android:id/button1")
    public MobileElement yes;

    public By by_item_text = By.id("design_menu_item_text");

    @AndroidFindBy(id = "button2")
    public MobileElement no_button;

    @AndroidFindBy(id = "button1")
    public MobileElement yes_button;
}
