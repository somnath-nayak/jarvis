package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class ProfilePageObject {
	
	@AndroidFindBy(id = "profile_image")
    public MobileElement profile;

	@AndroidFindBy(id = "//android.widget.CheckedTextView[@text= 'Profile']")
    public MobileElement profileIcon;
	
    @AndroidFindBy(id = "textView54")
    public MobileElement download_insurance;
    
    @AndroidFindBy(id= "service_details_label")
    public MobileElement ServiceDetail;
    
    @AndroidFindBy(id= "in.swiggy.delivery:id/emergency_contact_label")
    public MobileElement EmergencyInfo;
    
    @AndroidFindBy(id= "in.swiggy.delivery:id/bank_details_label")
    public MobileElement bankDetails;

}
