package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 1/9/18.
 */
public class LiveTasksPageObject {

    @AndroidFindBy(id = "title")
    public MobileElement task_title;

    @AndroidFindBy(xpath = "//android.widget.ImageButton")
    public MobileElement left_nav;

    @AndroidFindBy(id = "navigate_arrow")
    public MobileElement navigate_arrow;

    @AndroidFindBy(id = "toolbar")
    public MobileElement toolbar;

    @AndroidFindBy(id = "android:id/button1")
    public MobileElement ok_button;

    By toast = By.xpath("//android.widget.Toast[1]");
    public By by_task_title = By.id("title");
    public By tasks = By.id("navigate_arrow");

    @AndroidFindBy(id = "android:id/alertTitle")
    public MobileElement alert_title;

}
