package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/10/18.
 */
public class FeedbackPageObject {

    @AndroidFindBy(id = "deliveryExpRatingBar")
    public MobileElement rating_bar;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Feedback']")
    public MobileElement feedback;

    @AndroidFindBy(id = "orderID")
    public MobileElement order_id;

    @AndroidFindBy(id = "leaveComment")
    public MobileElement leave_comment;

    @AndroidFindBy(id = "feedbackSubmit")
    public MobileElement submit;

    @AndroidFindBy(id = "distance_submit_button")
    public MobileElement distance_submit;
}
