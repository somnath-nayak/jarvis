package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/8/18.
 */
public class LaunchPageObject {

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_deny_button")
    public MobileElement deny_button;

    @AndroidFindBy(id = "com.android.packageinstaller:id/permission_allow_button")
    public MobileElement allow_button;

    @AndroidFindBy(id = "com.android.packageinstaller:id/current_page_text")
    public MobileElement current_page;
}
