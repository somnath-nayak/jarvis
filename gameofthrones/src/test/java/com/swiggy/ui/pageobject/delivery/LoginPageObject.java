package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/8/18.
 */
public class LoginPageObject {

    @AndroidFindBy(id = "employeeLoginID")
    public MobileElement login_id;

    @AndroidFindBy(id = "employeeLoginButton")
    public MobileElement submit_button;

    @AndroidFindBy(id = "otpEditText")
    public MobileElement otp_text;

    @AndroidFindBy(id = "submitOTP")
    public MobileElement submit_otp;

    @AndroidFindBy(id = "resendOTP")
    public MobileElement resend_otp;
}
