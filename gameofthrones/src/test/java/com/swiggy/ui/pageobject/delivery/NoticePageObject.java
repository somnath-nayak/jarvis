package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/17/18.
 */
public class NoticePageObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Notice Board']")
    public MobileElement noticeboard_title;

    @AndroidFindBy(id = "notice_board_list")
    public MobileElement notice_list;
}
