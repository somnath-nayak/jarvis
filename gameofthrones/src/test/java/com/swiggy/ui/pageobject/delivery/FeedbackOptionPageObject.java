package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class FeedbackOptionPageObject {
	
	@AndroidFindBy(id = "in.swiggy.delivery:id/feedback_text")
    public MobileElement FeedbackTextbox;
	
    @AndroidFindBy(id = "in.swiggy.delivery:id/submit_feedback")
    public MobileElement Submitbutton;
	
    @AndroidFindBy(id = "in.swiggy.delivery:id/cancel_feedback")
    public MobileElement Resetbutton;
    
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    public MobileElement backbutton;	

    @AndroidFindBy(xpath = "//android.widget.TextView [@text ='Feedback']")
    public MobileElement feedbackPage;

}




