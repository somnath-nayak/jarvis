package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class IssuesAndComplaintsPageObject {
	
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text = 'Issues and Complaints']")
	    public MobileElement IssuesAndComplaintspage1 ;

	 @AndroidFindBy(id= "in.swiggy.delivery:id/create_ticket")
	    public MobileElement RaiseNewIssuebutton ;


	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Order Related Issues']")
	    public MobileElement OrderRelatedIssues ;
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Long Distance Order']")
	    public MobileElement LongDistanceOrder ;
	 
	 @AndroidFindBy(id= "in.swiggy.delivery:id/inputText")
	    public MobileElement EnterComplaints ;

	 @AndroidFindBy(xpath= "//android.widget.Button[2][@text='Confirm Issue']") 
	    public MobileElement confirmissuebutton ;
	 
	 @AndroidFindBy(id= "in.swiggy.delivery:id/issue_details_view")
	    public MobileElement Issuedetailpage ;

	 @AndroidFindBy(xpath="//android.widget.TextView[@text='Order Payout Incorrect']")
	    public MobileElement OrderPayoutIncorrect ;

	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Weekly Payout Related Issues']")
	    public MobileElement WeeklyPayoutRelatedIssues ;
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Received Less Payout']")
	    public MobileElement RecivedlessPayout ;
	 

	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Did Not Receive Payout']")
	    public MobileElement DidNotRecivedPayout ;
	 
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Manager Related Issues']")
	    public MobileElement ManagerRelatedIssues ;
	 
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Manager does not pick calls']")
	    public MobileElement ManagerDosenotpickcall ;
	 
	 @AndroidFindBy(xpath= "//android.widget.Button[2][@text='Next']")
	    public MobileElement Nextbutton;
	 
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='General Issue']")
	    public MobileElement GeneralIssue ;
	 
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Not Receiving Orders']")
	    public MobileElement Notrecivingorders ;
	 
	
	 @AndroidFindBy(xpath= "//android.widget.TextView[@text='Not Receiving Orders']")
	    public MobileElement others ;
	 

	 
}
