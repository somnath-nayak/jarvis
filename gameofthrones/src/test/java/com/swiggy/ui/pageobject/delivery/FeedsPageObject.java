package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/17/18.
 */
public class FeedsPageObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Feeds']")
    public MobileElement feeds;

    @AndroidFindBy(id = "typeContainer")
    public MobileElement first_feed;
}
