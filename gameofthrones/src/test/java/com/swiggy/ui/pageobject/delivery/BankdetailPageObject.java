package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class BankdetailPageObject {
	
	@AndroidFindBy(id = "in.swiggy.delivery:id/bank_details_checkbox")
    public MobileElement bankdetailCheckbox;

	@AndroidFindBy(id = "in.swiggy.delivery:id/save_bank_details")
    public MobileElement bankSavebutton;
	
	@AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up'] ")
	public MobileElement navigateup;

}
