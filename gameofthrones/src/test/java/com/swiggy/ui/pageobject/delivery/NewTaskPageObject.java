package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/11/18.
 */
public class NewTaskPageObject {

    @AndroidFindBy(id = "reject")
    public MobileElement reject_button;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CONFIRM ORDER']")
    public MobileElement confirm_order;

    @AndroidFindBy(id = "back")
    public MobileElement back_button;

    @AndroidFindBy(id = "button1")
    public  MobileElement confirm_button;

    @AndroidFindBy(id = "button2")
    public MobileElement cancel_button;
}
