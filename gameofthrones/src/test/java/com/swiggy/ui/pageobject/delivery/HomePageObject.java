package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/8/18.
 */
public class HomePageObject {

    @AndroidFindBy(id = "toolbar")
    public MobileElement toolbar;

}
