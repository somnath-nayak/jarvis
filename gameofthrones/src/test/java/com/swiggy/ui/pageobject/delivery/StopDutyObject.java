package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class StopDutyObject {
	
	  @AndroidFindBy(id = "android:id/button1")
	    public MobileElement yesbutton;	

	  
	  @AndroidFindBy(id = "in.swiggy.delivery:id/startOrderButton")
	    public MobileElement startdutybutton;	
	  
	  @AndroidFindBy(xpath = "//android.widget.TextView[@text ='Start duty']")
	    public MobileElement startdutypopup;	

	  @AndroidFindBy(xpath= "//android.widget.TextView[@text ='Stop duty']")
	    public MobileElement stopdutypopup;	
	  
	  @AndroidFindBy(xpath= "//android.widget.RelativeLayout")
	    public MobileElement taponbackground;	
	  
	// @AndroidFindBy(id = "//android.widget.CheckedTextView[@text = 'Start Duty']")
	  // public MobileElement startdutybutton;	
	  
	  
}


