package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 1/18/18.
 */
public class HelpPageObject {

    @AndroidFindBy(id = "title")
    public MobileElement title;

    public By by_title = By.id("title");

    @AndroidFindBy(xpath = "//android.widget.Button[@text='UNASSIGN']")
    public MobileElement unassign_button;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CANCEL']")
    public MobileElement cancel_button;

    @AndroidFindBy(xpath = "//android.widget.Button[@text='CALL_FM']")
    public MobileElement call_fm;
}
