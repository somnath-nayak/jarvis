package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/10/18.
 */
public class DeliveryPageObject {

    @AndroidFindBy(id = "action_messages")
    public MobileElement messages;

    @AndroidFindBy(id = "action_customer")
    public MobileElement action_customer;

    @AndroidFindBy(id = "action_help")
    public MobileElement help;

    @AndroidFindBy(id = "actionButton")
    public MobileElement action_button;

    @AndroidFindBy(id = "android:id/button2")
    public MobileElement cancel_button;

    @AndroidFindBy(id = "android:id/button1")
    public MobileElement confirm_button;

    @AndroidFindBy(id = "itemPickedUp")
    public MobileElement item_pickedup;

    @AndroidFindBy(id = "tf_total")
    public MobileElement bill_value;

    @AndroidFindBy(id = "confirm_bill")
    public MobileElement confirm_bill_value;

    @AndroidFindBy(id = "tf_paid")
    public MobileElement paid_value;

    @AndroidFindBy(id = "com.android.camera2:id/shutter_button")
    public MobileElement shutter_button;

    @AndroidFindBy(id = "com.android.camera2:id/done_button")
    public MobileElement done_button;

    @AndroidFindBy(id = "com.android.camera2:id/cancel_button")
    public MobileElement cancel_camera_button;

    @AndroidFindBy(id = "tv_collected")
    public MobileElement delivered_collect;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CONFIRM ORDER']")
    public MobileElement confirm_order;

    @AndroidFindBy(id = "totalBill")
    public MobileElement total_bill;

    @AndroidFindBy(id = "reject")
    public MobileElement reject_button;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='YES - REJECT ORDER']")
    public MobileElement reject_confirm;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Long distance']")
    public MobileElement long_distance_option;

    @AndroidFindBy(id = "callCustomer")
    public MobileElement call_customer;


    @AndroidFindBy(xpath = "//android.widget.ImageButton[1][@NAF = 'true']")
    public MobileElement call_customerenable;

}

