package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;

/**
 * Created by kiran.j on 1/17/18.
 */
public class SlotSignUpPageObject {

    @AndroidFindBy(id = "sign_up_available")
    public MobileElement available;

    @AndroidFindBy(id = "slot_sign_up_update")
    public MobileElement update;

    @AndroidFindBy(id = "attendance_no_radio")
    public MobileElement no_radio;

    @AndroidFindBy(id = "attendance_yes_radio")
    public MobileElement yes_radio;
    
    @AndroidFindBy(id="attendance_default_radio")
    public MobileElement maybe;

    public By byyes_radio = By.id("attendance_yes_radio");
    public By byno_radio = By.id("attendance_no_radio");

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Slot Sign-up System']")
    public MobileElement slot_title;

    

    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.RadioGroup/android.widget.RadioButton[2]")
    public MobileElement date;
    
  
}
