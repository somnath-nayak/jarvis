package com.swiggy.ui.pageobject.delivery;

import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

/**
 * Created by kiran.j on 1/17/18.
 */
public class LegalPageObject {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Legal']")
    public MobileElement legal_title;

    @AndroidFindBy(id = "general_terms")
    public MobileElement general_terms;

    @AndroidFindBy(id = "code_of_conduct")
    public MobileElement code_of_conduct;

    @AndroidFindBy(id = "payouts")
    public MobileElement payouts;

    @AndroidFindBy(id = "legal_terms_checkbox")
    public MobileElement legal_checkbox;

    @AndroidFindBy(id = "save_legal")
    public MobileElement save_button;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='General terms']")
    public MobileElement general_terms_title;
    
    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc='Navigate up']")
    public MobileElement Navigateback;
    
    
}
