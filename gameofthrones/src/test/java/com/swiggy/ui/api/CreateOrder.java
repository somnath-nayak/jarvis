package com.swiggy.ui.api;

import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;

import framework.gameofthrones.JonSnow.Processor;

/**
 * Created by kiran.j on 1/8/18.
 */
public class CreateOrder {

    public String createAndAssignOrder()  {
        String order_id =null;
        try {
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
            Processor processor = checkoutHelper.placeOrder(ApiConstants.user2_Mobileno, ApiConstants.user2_password, ApiConstants.item_id1, ApiConstants.quantity1, ApiConstants.rest_id1);
            order_id=processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_id");
            System.out.println("Order_id :"+order_id);
            deliveryServiceHelper.orderAssign(String.valueOf(order_id), ApiConstants.de_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(order_id);
    }
    
    public String createOrder() {
    	 String order_id = "";
         try {
             CheckoutHelper checkoutHelper = new CheckoutHelper();
             DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
             Processor processor = checkoutHelper.placeOrder(ApiConstants.user2_Mobileno, ApiConstants.user2_password, ApiConstants.item_id1, ApiConstants.quantity1, ApiConstants.rest_id1);
             order_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_id"));
         } catch(Exception e) {
        	 e.printStackTrace();
         }
         return order_id;

    }

    public String createOrder(String mobile, String password) {
   	 String order_id = "";
        try {
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
            Processor processor = checkoutHelper.placeOrder(mobile,password, ApiConstants.item_id, ApiConstants.quantity, ApiConstants.rest_id);
            order_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("data.order_id"));
        } catch(Exception e) {
       	 e.printStackTrace();
        }
        return order_id;

   }
    
    
    
    public void assignOrder(String order_id)
    {
    try
    {
    	 DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    	 deliveryServiceHelper.orderAssign(order_id, ApiConstants.de_id);
        }catch(Exception e){
    	e.printStackTrace();
    }
}


    public  static void main(String[] args){
        CreateOrder c=new CreateOrder();
        c.createAndAssignOrder();
    }
}
