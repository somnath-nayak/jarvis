package com.swiggy.ui.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.Delivery.DeappApi.AppapiHelper;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DashDeliveryHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.JonSnow.Processor;
import io.gatling.core.json.Json;
import org.apache.tomcat.jni.Proc;
import com.swiggy.ui.pojoClasses.superapp.Earninghistory;
import com.swiggy.ui.pojoClasses.superapp.FloatingCash;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by kiran.j on 1/8/18.
 */
public class DeliveryHelper {

    AppapiHelper helper=new AppapiHelper();
    DashDeliveryHelper dashhelper = new DashDeliveryHelper();


    public String getOtp(String de_id, String version) {
        AutoassignHelper autoassignHelper = new AutoassignHelper();
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
//        Processor processor = autoassignHelper.delogin(de_id, version);
        return String.valueOf(deliveryServiceHelper.getOtp(de_id).ResponseValidator.GetNodeValueAsInt("data"));
    }

    public String getAuth(String deId){
        DashDeliveryHelper dash = new DashDeliveryHelper();
        dash.delogin("216","1.8");
        //String otp=getOtp("216","1.8");
        String otp="547698";
        Processor processor=dash.validateOtp("216",otp);
        String response=processor.ResponseValidator.GetBodyAsText();
        String auth=JsonPath.read(response,"$.data.Authorization");
        return auth;


    }


    public String performLogin(String deId,String version){

        return null;

    }



    public FloatingCash getFloatingCashDepostiDetails(String deId){
        Processor processor;
        String response;
        FloatingCash f=new FloatingCash();
        String auth=getAuth("216");
        //String auth="Basic MjE2OjJjMDFkZGQ5LTdlZTQtNDhhMS1iYWNiLTU3NmMyZDhkZDM3ZA==";
        System.out.println(auth);
        processor=dashhelper.getAllChannelOptions(auth);
        response=processor.ResponseValidator.GetBodyAsText();
        ArrayList<String> channelLable=JsonPath.read(response,"$.data.channel[*].channel_label");
        ArrayList<String> availableChannelList=JsonPath.read(response,"$.data.channel[0].available_channel_list[*]");
        f.setChannelLabel(channelLable);
        f.setChannellist(availableChannelList);
        HashMap<Integer,ArrayList<String>> hm=new HashMap<>();
        HashMap<Integer,ArrayList<String>> hm_code=new HashMap<>();
        for(int i=1;i<=availableChannelList.size()+1;i++){
            processor=dashhelper.getSpecificChannelDetail(auth,Integer.toString(i));
            response=processor.ResponseValidator.GetBodyAsText();
            hm.put(i,JsonPath.read(response,"$.data.steps[*].step_instructions"));
            hm_code.put(i, JsonPath.read(response,"$.data.steps[*].dynamic_code"));
        }
        f.setChannelDetail(hm);
        f.setDepositCode(hm_code);

        return f;
    }


    public Earninghistory getEarningPageDetails(String deId) throws Exception{
        Processor processor;
        String response;
        FloatingCash f=new FloatingCash();
        String auth=getAuth("216");
        //String auth="Basic MjE2OjJjMDFkZGQ5LTdlZTQtNDhhMS1iYWNiLTU3NmMyZDhkZDM3ZA==";
        System.out.println(auth);
        processor=dashhelper.getEarningHistory("216",auth);
        ObjectMapper mapper = new ObjectMapper();
        Earninghistory earninghistory=mapper.readValue(processor.ResponseValidator.GetBodyAsText().toString(),Earninghistory.class);
        return earninghistory;
    }
    public void getEarningPageDetailByWeek(String deId){

    }

}
