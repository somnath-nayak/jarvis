package com.swiggy.ui.api;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.EndToEndHelp;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.projectX.grouporder.helper.GroupOrderHelper;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.ui.pojoClasses.*;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.swiggy.ui.utils.AndroidUtil.sleep;


public class ConsumerApiHelper extends RedisHelper {


    public GetAddressPojo getAllAddresS(String userName, String password) {
        GetAddressPojo object_address = null;

        try {
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            Processor loginResponse = SnDHelper.consumerLogin(userName, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
            Processor getAllAddressResp = checkoutHelper.invokegetAllAddressAPI(tid, token);
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            object_address = mapper.readValue(getAllAddressResp.ResponseValidator.GetBodyAsText(), GetAddressPojo.class);


        } catch (Exception e) {
            System.out.println(e);
        }
        return object_address;


    }

    public void createNewAddress(String username, String password, String lat, String lng, String annotation) {
        AddressHelper addressHelper = new AddressHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor newAddress = addressHelper.NewAddress(tid, token, "name", password, "near budhwal", "234", "area", lat, lng, "223", "ggn", annotation);
        newAddress.ResponseValidator.GetBodyAsText();

    }

    public void deleteAddress(String username, String password, String addressId) {
        AddressHelper addressHelper = new AddressHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        addressHelper.DeleteAddress(tid, token, addressId);
    }

    public void deleteAddressBasedOnAnnotation(String username, String password, String Annotation) {
        GetAddressPojo addressObject = getAllAddresS(username, password);
        ArrayList<Addresses> lAddress = addressObject.getData().getAddresses();
        String addressId = null;
        for (int i = 0; i < lAddress.size(); i++) {

            if (lAddress.get(i).getAnnotation().equalsIgnoreCase(Annotation)) {
                addressId = lAddress.get(i).getId();
                break;

            }
        }
        if (addressId != null) {
            deleteAddress(username, password, addressId);
        }
    }

    public void deleteAllAddressExceptHomeANDWORK(String username, String password) {
        GetAddressPojo addressObject = getAllAddresS(username, password);
        ArrayList<Addresses> lAddress = addressObject.getData().getAddresses();
        String addressId = null;
        for (int i = 0; i < lAddress.size(); i++) {

            if (!(lAddress.get(i).getAnnotation().equalsIgnoreCase("HOME") || lAddress.get(i).getAnnotation().equalsIgnoreCase("WORK"))) {
                addressId = lAddress.get(i).getId();
                deleteAddress(username, password, addressId);
            }
        }

    }

    public void deleteAllAddress(String username, String password) {
        GetAddressPojo addressObject = getAllAddresS(username, password);
        ArrayList<Addresses> lAddress = addressObject.getData().getAddresses();
        String addressId = null;
        for (int i = 0; i < lAddress.size(); i++) {
            //System.out.println(addressId);
            deleteAddress(username, password, lAddress.get(i).getId());

        }

    }

    public void clearCart(String username, String password) {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor deleteCart = checkoutHelper.DeleteCart(tid, token);
        deleteCart.ResponseValidator.GetBodyAsText();
    }

    public List<Menulist> getRestaurantList(String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocation(lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                lMenulist.add(temp);
            }


        }

        return lMenulist;
    }

    public List<Menulist> getFavList(String tid, String token, String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getFavoritesList(tid, token, lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                lMenulist.add(temp);
            }


        }

        return lMenulist;
    }

    public List<Menulist> getRestaurantList(String lat, String lng, String collectionId) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocationFromCollection(lat, lng, "0", collectionId, Constants.ANDROID_USER_AGENT);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                lMenulist.add(temp);
            }


        }
        System.out.println(lMenulist.get(0).getRestaurantName());
        return lMenulist;
    }

    public List<Menulist> getRestaurantListWithFilter(String lat, String lng, String filter) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocationWithFilter(lat, lng, "0", filter);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                int chainRestaurantValue = ctx.read("$.data.cards[" + i + "].data.data.chain.length()");
                temp.setChainCount(chainRestaurantValue);
                lMenulist.add(temp);
            }


        }
        return lMenulist;
    }

    public List<Menulist> getRestaurantListWithPreorder(String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocation(lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                int preorderlength = ctx.read("$.data.preorderSlots.length()", Integer.class);
                if (preorderlength > 0) {
                    temp.setPreorderdate(ctx.read("$.data.preorderSlots[*].date"));

                }
                lMenulist.add(temp);
            }


        }


        return lMenulist;
    }

    public List<Menulist> getCarouselList(String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocation(lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("carousel")) {
                int carouselLenght = ctx.read("$.data.cards[" + i + "].data.data.cards.length()", Integer.class);
                for (int j = 0; j < carouselLenght; j++) {
                    Menulist temp = new Menulist();
                    temp.setType(ctx.read("$.data.cards[" + i + "].data.data.cards[" + j + "].data.type"));
                    temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.cards[" + j + "].data.link"));
                    //temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.cards[" + j + "].data.restaurantUuid"));
                    lMenulist.add(temp);
                }
                break;
            }


        }
        System.out.println(lMenulist.get(0).getRestaurantId());
        return lMenulist;


    }

    public Restaurantmenu getRestaurantMenu(String restaurantId, String uuid, String lat, String lng, String username, String password) {

        SnDHelper snDHelper = new SnDHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor pMenuV3 = SnDHelper.menuV3(restaurantId, lat, lng, tid, token);
        String response = pMenuV3.ResponseValidator.GetBodyAsText();
        Restaurantmenu RestaurantmenuObject = new Restaurantmenu();
        ReadContext ctx = JsonPath.parse(response);
        RestaurantmenuObject.setId(ctx.read("$.data.id"));
        RestaurantmenuObject.setAvgRating(ctx.read("$.data.avgRatingString"));
        RestaurantmenuObject.setTotalRatingsString(ctx.read("$.data.totalRatingsString"));
        RestaurantmenuObject.setCostForTwoMsg(ctx.read("$.data.costForTwo"));
        RestaurantmenuObject.setCuisines(ctx.read("$.data.cuisines[*]"));
        RestaurantmenuObject.setOpened(ctx.read("$.data.opened", Integer.class));
        RestaurantmenuObject.setVeg(ctx.read("$.data.isVeg", Boolean.class));
        //RestaurantmenuObject.setTradeCampaignHeaders(ctx.read("$.data.tradeCampaignHeaders[0].description", String.class));
        RestaurantmenuObject.setSlaString(ctx.read("$.data.sla.slaString", String.class));
        RestaurantmenuObject.setLongDistance(ctx.read("$.data.sla.longDistance", String.class));
        RestaurantmenuObject.setCollection(ctx.read("$.data.menu.collections[*].name"));
        RestaurantmenuObject.setName(ctx.read("$.data.name"));
        RestaurantmenuObject.setItems(ctx.read("$.data.menu.items.*.name"));
        RestaurantmenuObject.setIsVegMenu(ctx.read("$.data.menu.items.*.isVeg"));
        System.out.println(RestaurantmenuObject.getCollection().get(0));
        System.out.println(RestaurantmenuObject.getItems());
        return RestaurantmenuObject;
    }

    public Restaurantmenu getRestaurantMenuV4(String restaurantId, String uuid, String lat, String lng, String username, String password, String client, String version_code) {
        Restaurantmenu RestaurantmenuObject = new Restaurantmenu();

        try {
            SnDHelper snDHelper = new SnDHelper();
            Processor loginResponse = SnDHelper.consumerLogin(username, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
            HashMap<String, String> headers = new HashMap<>();
            headers.put(Constants.APP_VERSION_CODE, version_code);
            headers.put(Constants.USER_AGENT, client);
            Processor pMenuV4 = snDHelper.menuV4(restaurantId, lat, lng, tid, token, headers);
            String response = pMenuV4.ResponseValidator.GetBodyAsText();
            System.out.println("=====response==="+response);
            ReadContext ctx = JsonPath.parse(response);
            RestaurantmenuObject.setId(ctx.read("$.data.id"));
            RestaurantmenuObject.setAvgRating(ctx.read("$.data.avgRatingString"));
            RestaurantmenuObject.setTotalRatingsString(ctx.read("$.data.totalRatingsString"));
            RestaurantmenuObject.setCostForTwoMsg(ctx.read("$.data.costForTwo"));
            RestaurantmenuObject.setCuisines(ctx.read("$.data.cuisines[*]"));
            RestaurantmenuObject.setOpened(ctx.read("$.data.opened", Integer.class));
            RestaurantmenuObject.setVeg(ctx.read("$.data.isVeg", Boolean.class));
            //RestaurantmenuObject.setTradeCampaignHeaders(ctx.read("$.data.tradeCampaignHeaders[0].description", String.class));
            // RestaurantmenuObject.setSlaString(ctx.read("$.data.sla.slaString", String.class));
            //  RestaurantmenuObject.setLongDistance(ctx.read("$.data.sla.longDistance", String.class));
            RestaurantmenuObject.setCollection(ctx.read("$.data.menu.widgets[*].name"));
            RestaurantmenuObject.setName(ctx.read("$.data.name"));
            RestaurantmenuObject.setItems(ctx.read("$.data.menu.items.*.name"));
            RestaurantmenuObject.setIsVegMenu(ctx.read("$.data.menu.items.*.isVeg"));
            RestaurantmenuObject.setImageId(ctx.read("$.data.restaurantLicenses.*.imageId").toString().replace("[","").replace("]","").replace("\"",""));
            RestaurantmenuObject.setText(ctx.read("$.data.restaurantLicenses.*.text").toString().replace("[","").replace("]","").replace("\"",""));
            if (ctx.read("$.data.menu.widgets[0].name").toString().equalsIgnoreCase("Everyday Value Offers")) {
                RestaurantmenuObject.setEdvoName(ctx.read("$.data.menu.widgets[0].name"));
                RestaurantmenuObject.setSubTitle(ctx.read("$.data.menu.widgets[0].subTitle"));
                RestaurantmenuObject.setEdvoOfferId(ctx.read("$.data.menu.widgets[0].entities[*].id"));
                RestaurantmenuObject.setEdvoOfferText(ctx.read("$.data.menu.widgets[0].entities[*].text"));
                RestaurantmenuObject.setEdvoOfferSubText(ctx.read("$.data.menu.widgets[0].entities[*].subText"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return RestaurantmenuObject;

    }


    public void cancelOrder(String username, String password, String orderId) {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        checkoutHelper.OrderCancel(username, password, orderId);
    }

    public void delinWallet(String userName, String password, String walletName) {
        CheckoutHelper checkoutHelper = new CheckoutHelper();
        Processor loginResponse = SnDHelper.consumerLogin(userName, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        switch (walletName) {
            case "mobikwik":
                checkoutHelper.mobiKwikDelinkWallet(tid, token);
                break;
            case "paytm":
                checkoutHelper.paytmDelinkWallet(tid, token);
                break;
            case "freecharge":


        }


    }

    public void searchDishesh(String lat, String lng, String keyword, String page) {
        SANDHelper sandHelper = new SANDHelper();
        sandHelper.searchV2(lat, lng, keyword, page);
    }

    public void removeFavs(String username, String password, String lat, String lng) {
        SANDHelper sandHelper = new SANDHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        List<Menulist> al = getFavList(tid, token, lat, lng);
        for (int i = 0; i < al.size(); i++) {
            sandHelper.setFavorite(username, password, new String[]{"0", "1", al.get(i).getRestaurantId()});
        }

    }

    public String getFirstNonVegItem(Restaurantmenu RestaurantmenuObject) {
        String menuName = null;
        for (int i = 0; i < RestaurantmenuObject.getItems().size(); i++) {
            if (RestaurantmenuObject.getIsVegMenu().get(i) == 0) {
                menuName = RestaurantmenuObject.getItems().get(i).toString();
                break;
            }
        }
        return menuName;

    }

    public List<Menulist> getRestaurantListBasedOnSort(String userName, String password, String lat, String lng, String sortName) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor loginResponse = SnDHelper.consumerLogin(userName, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor processor = rngHelper.getSortProcessor(sortName, lat, lng, "0", tid, token);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            //System.out.println(cardType);
            if (cardType.equalsIgnoreCase("restaurant")) {
                Menulist temp = new Menulist();
                temp.setCardType(cardType);
                temp.setCostForTwoString(ctx.read("$.data.cards[" + i + "].data.data.costForTwoString", String.class));
                //temp.setCuisines(ctx.read("$.data.cards[\"+i+\"].data.data..cuisines"));
                temp.setDeliveryTime(ctx.read("$.data.cards[" + i + "].data.data.slaString"));
                temp.setRestaurantName(ctx.read("$.data.cards[" + i + "].data.data.name"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.type"));
                temp.setUuid(ctx.read("$.data.cards[" + i + "].data.data.uuid"));
                temp.setRestaurantId(ctx.read("$.data.cards[" + i + "].data.data.id"));
                lMenulist.add(temp);
            }


        }
        System.out.println(lMenulist.get(0).getRestaurantName());
        return lMenulist;
    }

    public EDVOMeal getEDVOMealObject(String restaurantId, String mealId) {
        EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
        Processor processor = edvoCartHelper.getMeals(mealId, restaurantId);
        String response = processor.ResponseValidator.GetBodyAsText();
        EDVOMeal edvoMeal = new EDVOMeal();
        ReadContext ctx = JsonPath.parse(response);
        edvoMeal.setMealName(ctx.read("data.name"));
        edvoMeal.setMainText(ctx.read("data.launchPage.mainText"));
        edvoMeal.setSubText(ctx.read("data.launchPage.subText"));
        edvoMeal.setCommunicationText(ctx.read("data.launchPage.communicationText"));
        edvoMeal.setTagText(ctx.read("data.launchPage.tagText"));
        edvoMeal.setExitPage_MainText(ctx.read("data.exitPage.mainText"));
        edvoMeal.setExitPage_subText(ctx.read("data.exitPage.subText"));
        edvoMeal.setScreens_title(ctx.read("data.screens[*].title"));
        edvoMeal.setScreens_description(ctx.read("data.screens[*].description"));
        edvoMeal.setName(ctx.read("data.screens[*].group.items[*].name"));
        edvoMeal.setPizzaDescription(ctx.read("data.screens[*].group.items[*].description"));

        System.out.println(edvoMeal.getScreens_description().get(0));
        System.out.println(edvoMeal.getName().get(0));

        return edvoMeal;


    }


    public DishDiscovery getdd(String userName, String password, String lat1, String lng1) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor loginResponse = SnDHelper.consumerLogin(userName, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor processor = rngHelper.getListing(lat1, lng1, "0", tid, token);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        DishDiscovery temp = new DishDiscovery();
        int value = ctx.read("$.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String cardType = ctx.read("$.data.cards[" + i + "].cardType");
            System.out.println(cardType);
            if (cardType.equalsIgnoreCase("storyCollection")) {
                temp.setHeader(ctx.read("$.data.cards[" + i + "].data.data.header"));
                temp.setDescription(ctx.read("$.data.cards[" + i + "].data.data.description"));
                temp.setCollectionName(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.header"));
                temp.setCollectionSubName(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.storyDepth"));
                temp.setCollectionId(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.id"));
                temp.setMenuletCount(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.menuletCount"));
                temp.setType(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.type"));
                temp.setParentCollectionId(ctx.read("$.data.cards[" + i + "].data.data.cards[*].data.parentCollectionId"));

                break;
            }


        }

        return temp;


    }

    public DisDiscoveryStoryBoard getStoryBoardForDishDicovery(String userName, String password, String collectionId, String lat1, String lng1, String parentCollectionId) {
        SnDHelper snDHelper = new SnDHelper();
        List rids = new ArrayList<>();
        Processor loginResponse = SnDHelper.consumerLogin(userName, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        Processor processor = SnDHelper.storyBoard(tid, token, collectionId, lat1, lng1, "RESTAURANT_ITEM_GROUP", parentCollectionId);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        System.out.println(ctx);
        DisDiscoveryStoryBoard ddStoryBoard = new DisDiscoveryStoryBoard();
        ddStoryBoard.setHeaderStoryStartCard(ctx.read("$.data.cards[" + 0 + "].data.data.header"));

        int value = ctx.read("$.data.cards[0].data.data.cards.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            if (ctx.read("$.data.cards[0].data.data.cards[" + i + "].type").toString().equalsIgnoreCase("storyStartCard")) {
                ddStoryBoard.setDepthOfStory(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.depthOfStory"));
                ddStoryBoard.setDescriptionStoryStartCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.description"));
                ddStoryBoard.setSwipeTextStoryStartCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.swipeText"));
                ddStoryBoard.setTermsAndConditionsTextStoryStartCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.termsAndConditionsText"));
            }
            if (ctx.read("$.data.cards[0].data.data.cards[" + i + "].type").toString().equalsIgnoreCase("storyEndCard")) {
                ddStoryBoard.setHeaderStoryEndCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.title"));
                ddStoryBoard.setDescriptionStoryEndCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.description"));
                //ddStoryBoard.setCardsHeaderStoryEndCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.cards[*].data.header"));
                //ddStoryBoard.setCardsstoryDepthStoryEndCard(ctx.read("$.data.cards[0].data.data.cards[" + i + "].data.cards[*].data.storyDepth"));

            }
            if (ctx.read("$.data.cards[0].data.data.cards[" + i + "].type").toString().equalsIgnoreCase("restaurantItemCard")) {
                ddStoryBoard.setRestaurantName(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.name"));
                ddStoryBoard.setLocality(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.locality"));
                ddStoryBoard.setArea(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.area"));
                ddStoryBoard.setTd(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.tradeCampaignHeaders[0].description"));
                ddStoryBoard.setAvgRating(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.avgRating"));
                ddStoryBoard.setFirstItem(ctx.read("$.data.cards[0].data.data.cards[*].data.restaurant.avgRating"));
            }


        }
        System.out.println(ddStoryBoard.getRestaurantName().get(0));
        return ddStoryBoard;
    }

    public List<SwiggyPop> swiggyPopListing(String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getSwiggyPopListing(lat, lng);
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        List<SwiggyPop> popList = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        int value = ctx.read("$.data.cards.length()", Integer.class);
        int count = 0;
        for (int i = 0; i < value; i++) {
            if (ctx.read("data.cards[" + i + "].cardType").toString().equalsIgnoreCase("popItem")) {
                SwiggyPop popListTemp = new SwiggyPop();
                System.out.println(ctx.read("data.cards[" + i + "].data.data.name").toString());
                popListTemp.setName(ctx.read("data.cards[" + i + "].data.data.name"));
                popListTemp.setDescription(ctx.read("data.cards[" + i + "].data.data.description"));
                popListTemp.setRestaurantName(ctx.read("data.cards[" + i + "].data.data.restaurantName"));
                popListTemp.setRestaurantId(ctx.read("data.cards[" + i + "].data.data.restaurantId"));
                popListTemp.setPrice(ctx.read("data.cards[" + i + "].data.data.price"));
                popListTemp.setOpened(ctx.read("data.cards[" + i + "].data.data.availability.opened"));
                popList.add(popListTemp);
            }
        }
        System.out.println(popList.get(0).getName());
        return popList;


    }

    public ArrayList<String> getAllCusFilter(String lat, String lng, String filterTitle) {
        ArrayList<String> al = new ArrayList<>();
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocation(lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        List<Menulist> lMenulist = new ArrayList<>();
        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        int value = ctx.read("$.data.filters.length()", Integer.class);
        for (int i = 0; i < value; i++) {
            String filterType = ctx.read("$.data.filters[" + i + "].key");
            if (filterType.equals(filterTitle)) {
                al = ctx.read("$.data.filters[" + i + "].options[*].option");
            }


        }
        System.out.println(al.get(0));
        return al;

    }

    public Menulist getRestaurantTimers(String lat, String lng) {
        RngHelper rngHelper = new RngHelper();
        List rids = new ArrayList<>();
        Processor processor = rngHelper.getRestaurantListOnLocation(lat, lng, "0");
        String listingResponse = processor.ResponseValidator.GetBodyAsText();
        String pages = JsonPath.read(listingResponse, "$.data.pages").toString();

        String response = processor.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);

        int preorderlength = ctx.read("$.data.preorderSlots.length()", Integer.class);
        Menulist temp = new Menulist();

        temp.setPreorderdate(ctx.read("$.data.preorderSlots[*].date"));
        for (int i = 0; i < preorderlength; i++) {
            //System.out.println(cardType);
            if (preorderlength > 0) {
                Long date = ctx.read("$.data.preorderSlots[" + i + "].date");

                int slotlength = ctx.read("$.data.preorderSlots[" + i + "].slots.length()", Integer.class);
                ArrayList<PreorderTimer> al = new ArrayList<>();
                for (int j = 0; j < slotlength; j++) {
                    PreorderTimer temp1 = new PreorderTimer();
                    temp1.setEndTime(ctx.read("$.data.preorderSlots[" + i + "].slots[" + j + "].endTime"));
                    temp1.setStartTime(ctx.read("$.data.preorderSlots[" + i + "].slots[" + j + "].startTime"));
                    al.add(temp1);
                }
                HashMap<Long, ArrayList<PreorderTimer>> hm = new HashMap<>();
                hm.put(date, al);
                temp.setHm_preorderTimers(hm);

            }


        }


        return temp;
    }

    public List<RestaurantPageEDVO> getEDVOListForRestaurantPage(String username, String password, String restaurantId, String lat, String lng) {
        SnDHelper snDHelper = new SnDHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        HashMap<String, String> headers = new HashMap<>();
        headers.put(Constants.APP_VERSION_CODE, "300");
        headers.put(Constants.USER_AGENT, Constants.ANDROID_USER_AGENT);

        Processor pMenuV3 = SnDHelper.menuV4(restaurantId, lat, lng, tid, token, headers);
        String response = pMenuV3.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        List<RestaurantPageEDVO> edvoList = new ArrayList<>();
        int widgetsLenght = ctx.read("$.data.menu.widgets.length()", Integer.class);

        for (int i = 0; i < widgetsLenght; i++) {
            if (ctx.read("$.data.menu.widgets[" + i + "].name").toString().equalsIgnoreCase("Everyday Value Offers")) {
                int edvolength = ctx.read("$.data.menu.widgets[" + i + "].entities.length()", Integer.class);
                for (int j = 0; j < edvolength; j++) {
                    RestaurantPageEDVO temp = new RestaurantPageEDVO();
                    temp.setId(ctx.read("$.data.menu.widgets[" + i + "].entities[" + j + "].id"));
                    temp.setName(ctx.read("$.data.menu.widgets[" + i + "].entities[" + j + "].name"));
                    temp.setText(ctx.read("$.data.menu.widgets[" + i + "].entities[" + j + "].text"));
                    temp.setSubText(ctx.read("$.data.menu.widgets[" + i + "].entities[" + j + "].subText"));
                    edvoList.add(temp);
                    System.out.println(temp.getText());
                }
            }
//            System.out.println(edvoList.get(6));

        }
        System.out.println(edvoList.size());
        System.out.println(edvoList.get(0).getText());
        System.out.println(edvoList.get(1).getText());
        for (int i = 0; i < edvoList.size(); i++) {
            System.out.println("Hello " + edvoList.get(i).getText());
        }


        return edvoList;


    }

    public void joinGroupOrder(String username, String password, String parentKey) {
        try {
            /*Processor loginResponse = SnDHelper.consumerLogin(username, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            System.out.println("Tid"+tid);
            String sid=loginResponse.ResponseValidator.GetNodeValue("sid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");*/

            GroupOrderHelper groupOrderHelper = new GroupOrderHelper();
            groupOrderHelper.loginUsers(username, password);
            Processor processor = groupOrderHelper.joinCart(parentKey, 0);

            // Processor processor1 = groupOrderHelper.joinCart(parentKey,tid_user,sid_user,token_user);
            String body1 = processor.ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body1, "$.statusCode");
            System.out.println(status);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addItemForGroupOrder(String username, String password, String parentKey, String restaurantId) {
        try {
            Processor loginResponse = SnDHelper.consumerLogin(username, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            System.out.println("Tid" + tid);
            String sid = loginResponse.ResponseValidator.GetNodeValue("sid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");

            GroupOrderHelper groupOrderHelper = new GroupOrderHelper();
            Processor cartMapping = groupOrderHelper.userCartMapping(parentKey, 0);
            String response = cartMapping.ResponseValidator.GetBodyAsText();
            ReadContext ctx = JsonPath.parse(response);
            System.out.println(ctx);
            String clientCartKey = ctx.read("$.data.userCartList[0].cartKey");

            Processor processor = groupOrderHelper.updateCart(tid, sid, token, parentKey, clientCartKey, restaurantId, 1);

            // Processor processor1 = groupOrderHelper.joinCart(parentKey,tid_user,sid_user,token_user);
            String body1 = processor.ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body1, "$.statusCode");
            System.out.println(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeCart(String username, String password, String parentKey, String restaurantId) {
        try {
            Processor loginResponse = SnDHelper.consumerLogin(username, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            System.out.println("Tid" + tid);
            String sid = loginResponse.ResponseValidator.GetNodeValue("sid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");

            GroupOrderHelper groupOrderHelper = new GroupOrderHelper();
            Processor cartMapping = groupOrderHelper.userCartMapping(parentKey, 0);
            String response = cartMapping.ResponseValidator.GetBodyAsText();
            ReadContext ctx = JsonPath.parse(response);
            System.out.println(ctx);
            String clientCartKey = ctx.read("$.data.userCartList[0].cartKey");

            Processor processor = groupOrderHelper.closeCart(tid, sid, token, parentKey, clientCartKey);

            // Processor processor1 = groupOrderHelper.joinCart(parentKey,tid_user,sid_user,token_user);
            String body1 = processor.ResponseValidator.GetBodyAsText();
            int status = JsonPath.read(body1, "$.statusCode");
            System.out.println(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public String  placeOrder(String username,String password,String restId){
        String orderId = null;
        CheckoutHelper checkoutHelper = new CheckoutHelper();

        try {
            Processor loginResponse = SnDHelper.consumerLogin(username, password);
            String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
            System.out.println("Tid" + tid);
            String sid = loginResponse.ResponseValidator.GetNodeValue("sid");
            String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
            OrderPlace orderPlace = new OrderPlace();
            EndToEndHelp endToEndHelp = new EndToEndHelp();
            List<Cart> cart = new ArrayList<>();

            //cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("683"), 1));
            cart.add(new Cart(null, null, orderPlace.getRestaurantMenu(restId,tid,token), 1));

            CreateMenuEntry createOrder = new CreateOrderBuilder()
                    .password(password)
                    .mobile(Long.parseLong(username))
                    .cart(cart)
                    .restaurant(Integer.parseInt(restId))
                    .paymentMethod("Cash")
                    .orderComments("Test-Order")
                    .buildAll();
            orderId = orderPlace.createOrder(createOrder);
            System.out.println(orderId);
            DeliveryServiceHelper delhelp = new DeliveryServiceHelper();
            delhelp.processOrderInDeliveryProd(orderId, "placed", "216");





            /*endToEndHelp.getRestaurantMenu("9990");
            Processor processor=checkoutHelper.placeOrder("7899772315","swiggy","8428163","1","9990");
            String body = processor.ResponseValidator.GetBodyAsText();

                    String orderId = JsonPath.read(body, "$.data.order_id").toString();
            System.out.println("orderId" + orderId);*/


            // OMSHelper omsHelper = new OMSHelper();
            //omsHelper.verifyOrderPROD(orderId, "111");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orderId;

    }

    public void changeOrderStatus(String orderId, String status) {
        try {
            DeliveryServiceHelper delhelp = new DeliveryServiceHelper();
            switch (status) {
                case OrderStatus.ARRIVED:
                    delhelp.zipDialArrived(orderId);
                    break;
                case OrderStatus.ASSIGNED:
                    delhelp.orderAssignProd(orderId, "216");
                    break;
                case OrderStatus.CONFIRMED:
                    delhelp.zipDialConfirm(orderId);
                    break;
                case OrderStatus.PICKEDUP:
                    delhelp.zipDialPickedUp(orderId);
                    break;
                case OrderStatus.REACHED:
                    delhelp.zipDialReached(orderId);
                    break;
                case OrderStatus.DELIVERED:
                    delhelp.zipDialDelivered(orderId);
            }

            // delhelp.orderAssignProd(orderId, "216");
            //delhelp.zipDialConfirm(orderId);
            //delhelp.processOrderInDeliveryProd(orderId, OrderStatus.ASSIGNED, "216");
            //delhelp.zipDialPickedUp(orderId);
        } catch (Exception e) {

        }
    }
    public String getRestaurantAddress(String username, String password, String restaurantId, String lat, String lng) {
        SnDHelper snDHelper = new SnDHelper();
        Processor loginResponse = SnDHelper.consumerLogin(username, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        HashMap<String, String> headers = new HashMap<>();
        headers.put(Constants.APP_VERSION_CODE, "300");
        headers.put(Constants.USER_AGENT, "Swiggy-Android");

        Processor pMenuV3 = SnDHelper.menuV4(restaurantId, lat, lng, tid, token, headers);
        String response = pMenuV3.ResponseValidator.GetBodyAsText();
        ReadContext ctx = JsonPath.parse(response);
        return ctx.read("data.locality") + ", " + ctx.read("data.area");
    }


    //Appupdate api helpers

    public boolean setWp_Options(String option_name, String option_value, String autoload) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        boolean success = sqlTemplate.update(CmsConstants.update_wp_options + option_value + CmsConstants.update_wp_options2 + autoload + CmsConstants.update_wp_options3 + option_name + CmsConstants.update_wp_options_end) > 0;
        return success;
    }

    public void deleteKey( final String name, int database, final String key ) {
        System.out.println(key);
        RedisTemplate rTemplate = getRedisTemplate(name, database);
        rTemplate.delete(key);
    }

    public void clearwpoptionsredis(String name, String database, String key){
//here wp option key is static so mention this as static in your constants.
        deleteKey("sandredisstage",2,Constants.wp_options);
        sleep(50000);

    }


}












