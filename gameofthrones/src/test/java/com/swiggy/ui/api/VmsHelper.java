package com.swiggy.ui.api;

import com.swiggy.api.erp.vms.helper.PrepTimeServiceHelper;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsConstants;

/**
 * Created by kiran.j on 6/7/18.
 */
public class VmsHelper {

    public void deletePrepTimes() {
        PrepTimeServiceHelper prepTimeServiceHelper = new PrepTimeServiceHelper();
        try {
            String[] slots = prepTimeServiceHelper.fetchSlotWiseId(VmsConstants.username);
            for (String slot : slots) {
                if (slot != null && slot != "")
                    prepTimeServiceHelper.delPreptime(slot);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
