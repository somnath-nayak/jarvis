package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;
import java.util.List;

import static com.swiggy.ui.utils.AndroidUtil.*;
import static com.swiggy.ui.utils.AndroidUtil.Direction.UP;
import static com.swiggy.ui.utils.Constants.lat1;
import static com.swiggy.ui.utils.Constants.lat5;
import static com.swiggy.ui.utils.Constants.lng5;


public class iOS_RegressionSuite_Newfeatures {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    PaymentPage paymentPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    RestaurantPage restaurantPage;
    SwiggyCafe cafepage;
    CheckoutPage checkoutPage;
    ExplorePage explorepage;
    AccountPage accountPage;
    orderDetailPage orderDetailPage;
    TrackPage trackPage;



    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        paymentPage = new PaymentPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorepage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        cafepage = new SwiggyCafe(inti);
    }
//POP FF
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify POPfilter section,TitleBar,Veg,Nonveg flter")
    public void verifyPOPtitleNONvegVegFilter(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openSwiggyPop();
        sleep(1000);
        Assert.assertTrue(swiggypopPage.verifyPOPfilersection());
        sleep(1000);
        swiggypopPage.ValidatingPOPfiltertitle();
        swiggypopPage.clickOnvegfilter();
        Assert.assertFalse(swiggypopPage.validateVegfilter());
        homePage.openAccountTab();
        homePage.openSwiggyPop();
        swiggypopPage.clickOnnonvegfilter();
        sleep(1000);
        Assert.assertFalse(swiggypopPage.validateNonvegfilter());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Under99 filter,Multiplefilter,add itema afterapplying filter")
    public void verifyUnder99AdditemMultiplefilters(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openSwiggyPop();
        sleep(1000);
        swiggypopPage.clickOnUnder99filter();
        swiggypopPage.clickonfirstitemafterapplyingfilter();
        swiggypopPage.clickonBackButton();
        swiggypopPage.verifyMultipleFilterVeg();
        swiggypopPage.clickonfirstitemafterapplyingfilter();
        swiggypopPage.clickonBackButton();

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify filter in balckzone,nextbatch or any card case")
    public void verifyblackzoneandNextbatchcard(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.openSwiggyPop();
        AndroidUtil.scrollios(driver, UP);
        sleep(1000);
        swiggypopPage.validatepopfiltercards();
        homePage.openNearMeTab();
        homePage.blackzonearea();
        homePage.openSwiggyPop();
        sleep(1000);
        Assert.assertFalse(swiggypopPage.validatepopfilterinnonserviceablearea());
    }

    //FSSAI
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify whether license is present in the menu or not")
    public void verifyLicense(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat5), Double.toString(lng5));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenuV4(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat5),Double.toString(lng5),username,password,Constants.IOS_USER_AGENT,"238");
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Truffles");
        homePage.selectRestaurant("Truffles",0);
        sleep(2000);
        System.out.println(restaurantMenuObject.getImageId());
        System.out.println(Constants.fssaiimage);
        //System.out.println(restaurantMenuObject.getImageId().contains(Constants.fssaiimage)); just to check whether condition returns true
        Assert.assertTrue(restaurantPage.verifymenurestnameintitle());
        Assert.assertEquals(Constants.fssaiimage,restaurantMenuObject.getImageId());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify the license status")
    public void verifythelicensestatus(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat5), Double.toString(lng5));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenuV4(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat5),Double.toString(lng5),username,password,Constants.IOS_USER_AGENT,"238");
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Truffles");
        homePage.selectRestaurant("Truffles",0);
        Assert.assertTrue(restaurantPage.verifymenurestnameintitle());
        Assert.assertEquals(Constants.fssailicenceno,restaurantMenuObject.getText());
    }
//Hygiene rating
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Hygiene Rating(image and click), Back button, Menu which doesnt have HR")
    public void verifyHygieneRatingandClickandNoHRmenu(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Mughal");
        homePage.selectRestaurant("Mughal Sarai", 0);
        restaurantPage.verifyHygieneimage();
        restaurantPage.tapOnHygienerating();
        Assert.assertEquals(restaurantPage.getHygieneratingtext(), Constants.Hygiene_txt);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Feed back and CTA")
    public void verifyFeedbackandCTA(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Mughal");
        homePage.selectRestaurant("mughal sarai",0);
        restaurantPage.tapOnHygienerating();
        restaurantPage.verifyFeedback();
        restaurantPage.verifyContinueOrdering();
        Assert.assertTrue(restaurantPage.getDeliverytimetext());
    }

//Category bar
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Hygiene Rating(image and click), Back button, Menu which doesnt have HR")
    public void verifyCategoryBarDisplay(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        sleep(2000);
        Assert.assertTrue(homePage.verifyCategoryBar());
    }
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Category Bar")
    public void verifyBarOptions(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        System.out.println("ios driver" + driver);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.removeToolTip();
        homePage.clickOnSortNudge();
        homePage.verifyCategoryBarOnListing();
        Assert.assertTrue(homePage.checkElementDisplay(homePage.verifyCategoryBarOnListing), "Category Bar is disabled on IBC area");
        homePage.getFirstCategoryBarName();
        Assert.assertTrue(homePage.checkElementDisplay(homePage.txt_firstCategoryBarName), "First Category Bar name is missing");
        homePage.clickOnFirstCategoryBar();
        sleep(2000);
        homePage.CategoryBarBack();
    }


    //Preorder

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify PreOrder for Non-Logged in user!")
    public void verifyPreOrderFlow(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(3000);
        restaurantPage.clickOnPreorderNowTimeslot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(0);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));


    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify PreOrder for Logged in user!")
    public void verifyPreOrderFlowForLoggedInUser(String userName2, String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        AndroidUtil.sleep(3000);
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(1);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));


    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Preorder for Favourite restaurant")
    public void verifyPreOrderForFavouriteRestaurant (String userName2, String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        AndroidUtil.sleep(3000);
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(0);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        Assert.assertTrue(restaurantPage.checkElementDisplay(restaurantPage.fav_icon),"Favourite icon is not displayed");
        restaurantPage.clickonFavIcon();
        String restaurantName = restaurantPage.getRestaurantName();
        restaurantPage.clickOnBackButton();
        homePage.openFilter();
        Assert.assertTrue(homePage.checkElementDisplay(homePage.txt_SortFilter),"Wrong filter header displayed");
        homePage.clickOnFilterOffersAndMore();
        homePage.setSelectOffersAndMore(Constants.SHOW_RESTAURANT_WITH_MY_FAVOURITES);
        homePage.clickonApplySortAndFilterButton();
        AndroidUtil.sleep(1000);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));

    }

    @Test (dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Preorder for location not supporting Preorder")
    public void verifyPreOrderForLocationNotSupportingPreorder (String userName2, String password2) {
        Location location = new Location(Constants.lat2, Constants.lng2, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertFalse(homePage.checkElementDisplay(homePage.txt_preOrderOnbarding));

    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }

    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
