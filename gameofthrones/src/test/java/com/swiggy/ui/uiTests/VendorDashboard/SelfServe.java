package com.swiggy.ui.uiTests.VendorDashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.vendor.dashboard.HomePage;
import com.swiggy.ui.page.classes.vendor.dashboard.SelfServeMenu;
import com.swiggy.ui.page.classes.vendor.dashboard.SettingsPage;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsConstants;
import com.swiggy.ui.uiTests.dashboard.dp.VendorDp;

public class SelfServe extends VendorDp {

	private WebDriver driver;
	private static String URL = VmsConstants.dashboard_url;
	By toggle_orders = By.xpath("//span[@class='bg']");

    @BeforeMethod
	public void setUp() throws InterruptedException{
		//Thread.sleep(5000);
        InitializeUI gameofthrones = new InitializeUI();
        driver = gameofthrones.getWebdriver();
		driver.navigate().to(URL);
		HomePage homePage = new HomePage(driver);
		SettingsPage settingsPage = new SettingsPage(driver);
//		homePage.login();
//		homePage.showOrders();
//		settingsPage.toggleShowOrders();
//		homePage.tapOnOrders();
		//driver.manage().window().maximize();
	}

    
    @Test(dataProvider = "createSelfServeMenuTicket", groups = {"sanity","smoke","regression"},description="Add Self Serve Menu New Item")
   	public void createSelfServeMenuTicket(String itemName, int itemPrice, int packaging) {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
    	    homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.clickAddItem();
   			selfServeMenu.EnterItemDetails();
   			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
   			packaging = Integer.parseInt(selfServeMenu.getPackaging());
   			int cgst = Integer.parseInt(selfServeMenu.getCGST());
   			int sgst = Integer.parseInt(selfServeMenu.getSGST());
   			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
   			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
   			selfServeMenu.submit();
   			Thread.sleep(2000);	
   			selfServeMenu.uploadMenu();
   			Assert.assertTrue(selfServeMenu.verifyUploadMenu());
   			selfServeMenu.submitApproval();
   			Thread.sleep(2000);
   			String approve = selfServeMenu.getapproveMessage();
   			Assert.assertEquals(approve, "Submitted");
   			selfServeMenu.pendingTickets();
   			selfServeMenu.pendingTicketView();
   			Assert.assertEquals(selfServeMenu.getViewItemName(), VmsConstants.itemName);
            
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
    }
    

    @Test(dataProvider = "createSelfServeMenuTicket", groups = {"sanity","smoke","regression"},description="Cancel pending ticket")
   	public void cancelPendingcreateTicket(String itemName, int itemPrice, int packaging) {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.clickAddItem();
   			selfServeMenu.EnterItemDetails();
   			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
   			packaging = Integer.parseInt(selfServeMenu.getPackaging());
   			int cgst = Integer.parseInt(selfServeMenu.getCGST());
   			int sgst = Integer.parseInt(selfServeMenu.getSGST());
   			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
   			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
   			selfServeMenu.submit();
   			Thread.sleep(2000);	
   			selfServeMenu.uploadMenu();
   			Assert.assertTrue(selfServeMenu.verifyUploadMenu());
   			selfServeMenu.submitApproval();
   			Thread.sleep(2000);	
   			selfServeMenu.pendingTickets();
   			selfServeMenu.pendingTicketView();
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
    }


    @Test(dataProvider = "createSelfServeMenuTicket", groups = {"sanity","smoke","regression"},description="Cancel waiting ticket")
   	public void cancelWaitingCreateTicket(String itemName, int itemPrice, int packaging) {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
    	   	homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.clickAddItem();
   			selfServeMenu.EnterItemDetails();
   			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
   			packaging = Integer.parseInt(selfServeMenu.getPackaging());
   			int cgst = Integer.parseInt(selfServeMenu.getCGST());
   			int sgst = Integer.parseInt(selfServeMenu.getSGST());
   			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
   			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
   			selfServeMenu.submit();
   			Thread.sleep(2000);	
   			selfServeMenu.viewWaitingTicket();
   			Thread.sleep(1000);
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
    }
    
    
    @Test(dataProvider = "editItem", groups = {"sanity","smoke","regression"},description="Edit Self Serve Menu Item")
 	public void editItem(int itemPrice, int packaging) {
    try{
  	    HomePage homePage = new HomePage(driver);
  	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
  	   	homePage.login();
 			Assert.assertTrue(homePage.logoverify());
 			selfServeMenu.clickMenu();
 			selfServeMenu.editItem();
 			selfServeMenu.editPrice();
 			selfServeMenu.editPackaging();
 			
 			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
 			packaging = Integer.parseInt(selfServeMenu.getPackaging());
 			int cgst = Integer.parseInt(selfServeMenu.getCGST());
 			int sgst = Integer.parseInt(selfServeMenu.getSGST());
 			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
 			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
 			selfServeMenu.submit();
   			Thread.sleep(2000);	
   			selfServeMenu.uploadMenu();
   			Assert.assertTrue(selfServeMenu.verifyUploadMenu());
   			selfServeMenu.submitApproval();
   			String approve = selfServeMenu.getapproveMessage();
   			Assert.assertEquals(approve, "Submitted");
 			
 		} catch(Exception e) {
 			Assert.assertTrue(false);
 			e.printStackTrace();
 		}

  }    

    @Test(dataProvider = "editItem", groups = {"sanity","smoke","regression"},description="Edit Self Serve Menu Pending Item")
 	public void cancelPendingEditItem(int itemPrice, int packaging) {
    try{
  	    HomePage homePage = new HomePage(driver);
  	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
  	   	homePage.login();
 			Assert.assertTrue(homePage.logoverify());
 			selfServeMenu.clickMenu();
 			selfServeMenu.editItem();
 			selfServeMenu.editPrice();
 			selfServeMenu.editPackaging();
 			
 			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
 			packaging = Integer.parseInt(selfServeMenu.getPackaging());
 			int cgst = Integer.parseInt(selfServeMenu.getCGST());
 			int sgst = Integer.parseInt(selfServeMenu.getSGST());
 			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
 			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
 			selfServeMenu.submit();
   			Thread.sleep(2000);	
   			selfServeMenu.uploadMenu();
   			Assert.assertTrue(selfServeMenu.verifyUploadMenu());
   			selfServeMenu.submitApproval();
   			Thread.sleep(2000);	
   			selfServeMenu.pendingTickets();
   			selfServeMenu.pendingTicketView();
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
 			
 		} catch(Exception e) {
 			Assert.assertTrue(false);
 			e.printStackTrace();
 		}

  }    

    @Test(dataProvider = "editItem", groups = {"sanity","smoke","regression"},description="Cancel waiting edit Item")
 	public void cancelWaitingEditItem(int itemPrice, int packaging) {
    try{
  	    HomePage homePage = new HomePage(driver);
  	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
  	   	homePage.login();
 			Assert.assertTrue(homePage.logoverify());
 			selfServeMenu.clickMenu();
 			selfServeMenu.editItem();
 			selfServeMenu.editPrice();
 			selfServeMenu.editPackaging();
 			
 			itemPrice = Integer.parseInt(selfServeMenu.getItemPrice());
 			packaging = Integer.parseInt(selfServeMenu.getPackaging());
 			int cgst = Integer.parseInt(selfServeMenu.getCGST());
 			int sgst = Integer.parseInt(selfServeMenu.getSGST());
 			int actualTotalPrice = Integer.parseInt(selfServeMenu.getTotalPrice());
 			int expectedTotalPrice = itemPrice+packaging+cgst+sgst;
            Assert.assertEquals(actualTotalPrice, expectedTotalPrice);
 			selfServeMenu.submit();
   			Thread.sleep(2000); 			Thread.sleep(2000);	
   			selfServeMenu.viewWaitingTicket();
   			Thread.sleep(1000);
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
   			
 		} catch(Exception e) {
 			Assert.assertTrue(false);
 			e.printStackTrace();
 		}

  }    

    
    @Test(groups = {"sanity","smoke"},description="Delete Item")
   	public void deleteItem() {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
    	    homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.deleteItem();	
   			Thread.sleep(1000);
            selfServeMenu.submitApproval();
            Thread.sleep(1000);
            selfServeMenu.pendingTickets();
            selfServeMenu.pendingTicketView();
            Assert.assertEquals(selfServeMenu.getDeleteLabel(), "Delete Item requested");
            
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
}


    @Test(groups = {"sanity","smoke"},description="Edit Self Serve Menu New Item")
   	public void cancelPendingDeleteItemTicket() {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
    	    homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.deleteItem();
   			Thread.sleep(1000);
   			selfServeMenu.submitApproval();
   			Thread.sleep(1000);
   			selfServeMenu.pendingTickets();
   			selfServeMenu.pendingTicketView();
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
}

    @Test(groups = {"sanity","smoke"},description="Edit Self Serve Menu New Item")
   	public void cancelWaitingDeleteItemTicket() {
      try{
    	    HomePage homePage = new HomePage(driver);
    	    SelfServeMenu selfServeMenu = new SelfServeMenu(driver);
    	    homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			selfServeMenu.clickMenu();
   			selfServeMenu.deleteItem();
   			Thread.sleep(1000);
   			selfServeMenu.viewWaitingTicket();
   			Thread.sleep(1000);
   			selfServeMenu.cancelTicket();
   			String cancelMessage = selfServeMenu.getCancelMessage();
   			Assert.assertEquals(cancelMessage, "Your ticket has been cancelled successfully");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
}
    
   @AfterMethod
	public void tearDown() {
		driver.close();
		driver.quit();
	}	
	
}
