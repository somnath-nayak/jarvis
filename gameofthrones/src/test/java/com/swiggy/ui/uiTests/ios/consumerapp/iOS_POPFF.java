package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.elasticsearch.search.Scroll;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;

import static com.swiggy.ui.utils.AndroidUtil.Direction.DOWN;
import static com.swiggy.ui.utils.AndroidUtil.Direction.UP;
import static com.swiggy.ui.utils.AndroidUtil.sleep;

public class iOS_POPFF {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    PaymentPage paymentPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    SwiggyCafe cafepage;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        paymentPage = new PaymentPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify POPfilter section,TitleBar,Veg,Nonveg flter")
    public void verifyPOPtitleNONvegVegFilter(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openSwiggyPop();
        sleep(1000);
        Assert.assertTrue(swiggypopPage.verifyPOPfilersection());
        sleep(1000);
        swiggypopPage.ValidatingPOPfiltertitle();
        swiggypopPage.clickOnvegfilter();
        Assert.assertFalse(swiggypopPage.validateVegfilter());
        homePage.openAccountTab();
        homePage.openSwiggyPop();
        swiggypopPage.clickOnnonvegfilter();
        sleep(1000);
        Assert.assertFalse(swiggypopPage.validateNonvegfilter());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Under99 filter,Multiplefilter,add itema afterapplying filter")
    public void verifyUnder99AdditemMultiplefilters(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openSwiggyPop();
        sleep(1000);
        swiggypopPage.clickOnUnder99filter();
        swiggypopPage.clickonfirstitemafterapplyingfilter();
        swiggypopPage.clickonBackButton();
        swiggypopPage.verifyMultipleFilterVeg();
        swiggypopPage.clickonfirstitemafterapplyingfilter();
        swiggypopPage.clickonBackButton();

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify filter in balckzone,nextbatch or any card case")
    public void verifyblackzoneandNextbatchcard(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.openSwiggyPop();
        AndroidUtil.scrollios(driver, UP);
        sleep(1000);
        swiggypopPage.validatepopfiltercards();
        homePage.openNearMeTab();
        homePage.blackzonearea();
        homePage.openSwiggyPop();
        sleep(1000);
        Assert.assertFalse(swiggypopPage.validatepopfilterinnonserviceablearea());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }

    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
