package com.swiggy.ui.uiTests.Group_order_iOS;

import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;

    public class Group_order_iOS {

        public AppiumDriver driver;
        InitializeUI inti;
        LaunchPage launch;
        HomePage home;
        LoginPage login;
        AddressPage addressPage;
        RestaurantPage restaurantPage;
        CheckoutPage checkoutPage;
        ExplorePage explorePage;
        AccountPage accountPage;
        PaymentPage paymentPage;
        orderDetailPage orderDetailPage;
        ConsumerApiHelper consumerApiHelper;
        SwiggypopPage swiggypopPage;
        TrackPage trackPage;
        GrouporderPage grouporderPage;
        Double lat1=12.932815;
        Double lng1=77.603578;

        Double lat2=27.834105;
        Double lng2=76.171977;

        Double lat3=19.229388800000002;
        Double lng3=72.8569977;



        @DataProvider(name = "LoginData")
        public Object[][] getdata() {
            return new Object[][]{{"8073093486","asdfghjkl"}};
        }

        @BeforeClass
        public void setUptest() throws UnknownHostException {
            //Proxyserver mobProxyTest=new Proxyserver(9091);
            //mobProxyTest.startServer();


            consumerApiHelper=new ConsumerApiHelper();


            inti= new InitializeUI();

            driver = inti.getAppiumDriver();
            launch = new LaunchPage(inti);
            home = new HomePage(inti);
            login=new LoginPage(inti);
            addressPage=new AddressPage(inti);
            restaurantPage=new RestaurantPage(inti);
            checkoutPage=new CheckoutPage(inti);
            explorePage=new ExplorePage(inti);
            accountPage=new AccountPage(inti);
            paymentPage=new PaymentPage(inti);
            orderDetailPage=new orderDetailPage(inti);
            trackPage=new TrackPage(inti);
            swiggypopPage=new SwiggypopPage(inti);
            accountPage=new AccountPage(inti);
            grouporderPage=new GrouporderPage(inti);

        }

       /*//Create group_order Link
        @Test(dataProvider = "LoginData",priority = 0)
        public void Create_Group(String username,String password){
            launch.clickonAllowButton();
            launch.tapOnLogin();
            login.LogintoApp(username, password);
            home.removeSwiggyPop();
            home.openAccountTab();
            accountPage.getRegisteredName();
            home.openNearMeTab();
            home.selectRestaurant(null,0);
            restaurantPage.removeCoachmark();
            //Group order Icon
            restaurantPage.ClickongrouporderIcon();
            System.out.println("social icon clicked");
            AndroidUtil.sleep(5000);

            grouporderPage.ClickonCreategrouporderButton();
            grouporderPage.Copygrouporderlinktext();

            AndroidUtil.sleep(5000);
            grouporderPage.CloseGroupOrderPage();

            System.out.println(restaurantPage.Get_grouporder_Created_text());
            //Assert.assertEquals(restaurantPage.Get_grouporder_Created_text(),accountPage.Grouporder_createdtext());
            Assert.assertEquals(restaurantPage.Get_grouporder_Created_text(),"Swiggy Social order by Mukesh");


        }
        //Complete  order placing scenario with group order and track with order id
        @Test(priority = 1)
        public void VerifyOrderPlacingFlowGroupOrder(){


            restaurantPage.addItemtest();
            restaurantPage.openCartFromRestaurantPage();
            checkoutPage.proceedToPay();
            paymentPage.ClickOnbackToCart();
            AndroidUtil.pressBackKey(driver);



        }

        //Keep Group Cart
        @Test(priority = 2)
        public void KeepGroupCart()
        {
            AndroidUtil.pressBackKey(driver);
            restaurantPage.KeepGroupCart();
            Assert.assertEquals(restaurantPage.Get_grouporder_Created_text(),"Swiggy Social order by Mukesh");


        }

        //Discard Group Cart
        @Test(priority = 3)
        public void DiscardGroupCart()
        {
            AndroidUtil.pressBackKey(driver);
            restaurantPage.DiscardGroupCart();

        }



        //Share Link with Friends
        @Test()
        public void Share_link(){

        }

        public void loginInToApp(Double lat,Double lng){

            consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
            consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat),Double.toString(lng),"WORK");
            Location location = new Location(lat, lng, 0);
//            driver.setLocation(location);
            launch.tapOnLogin();
            login.LogintoApp(Constants.userName, Constants.password);
        }
        public void addItemToCart(){
            home.removeSwiggyPop();
            home.selectRestaurant(null,0);
            restaurantPage.removeCoachmark();
            //restaurantPage.clickonFavIcon();
            //restaurantName=restaurantPage.getRestaurantName();
            restaurantPage.filterMenu(0);
            restaurantPage.addItemtest();
            restaurantPage.openCartFromRestaurantPage();
        }*/


    }



