package com.swiggy.ui.uiTests.crmAgentWorkBench;

import java.util.concurrent.Callable;

/**
 * Created by sumit.m on 24/04/18.
 */
public class WorkerThread implements Callable {

    private String command;

    public WorkerThread(String s) {
        this.command = s;
    }

    @Override
    public Object call() {
        System.out.println(Thread.currentThread().getName() + " Login. Agent = " + command);
        processCommand();
        System.out.println(Thread.currentThread().getName() + " Agent Logged in.");
        return 0;
    }

    private void processCommand() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
