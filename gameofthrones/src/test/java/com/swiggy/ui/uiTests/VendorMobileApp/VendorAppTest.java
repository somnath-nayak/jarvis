package com.swiggy.ui.uiTests.VendorMobileApp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.screen.classes.consumerapp.android.*;

import com.swiggy.ui.screen.vendorAndroidApp.VendorHomePage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorLoginPage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorSettingPage;
import com.swiggy.ui.screen.vendorAndroidApp.*;
import com.swiggy.ui.screen.vendorAndroidApp.VendorSettingPage;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import com.swiggy.ui.utils.Vendorappconstant;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

public class VendorAppTest {


    public AppiumDriver driver;
    InitializeUI inti;
    VendorHomePage vendorHomePage;
    VendorLoginPage vendorLoginPage;
    VendorSettingPage vendorSettingPage;


    VendorOrderDetailPage vendorOrderDetailPage;
    ConsumerApiHelper consumerApiHelper;

    TrackPage trackPage;


    @DataProvider(name = "restaurantLoginData")
    public Object[][] restaurantLoginData() {
        return new Object[][]{{Vendorappconstant.RESTAURANTID, Vendorappconstant.PASSWORD}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper=new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        vendorHomePage = new VendorHomePage(inti);
        vendorLoginPage = new VendorLoginPage(inti);
        vendorSettingPage = new VendorSettingPage(inti);
        vendorOrderDetailPage=new VendorOrderDetailPage(inti);
    }


    @Test(dataProvider = "restaurantLoginData", priority = 1, enabled = true, groups = {"regression"}, description = "Verify login on vendor app")
    public void verifyLogin(String restId, String password, ITestContext iTestContext) {
        vendorLoginPage.performLogin(restId,password);

        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);
        String restaurantAddress=consumerApiHelper.getRestaurantAddress(Vendorappconstant.CONSUMER_USERNAME,Vendorappconstant.CONSUMER_PASSWORD,restId,Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        Assert.assertEquals(vendorHomePage.getRestaurantAddress(),restaurantAddress);
        iTestContext.setAttribute("defaultPrepTime",vendorHomePage.getDefaultPreparationTime());
    }
    @Test(dataProvider = "restaurantLoginData", priority = 2, enabled = true, groups = {"regression"}, description = "Verify new order")
    public void verifyNewOrder(String restId, String password,ITestContext iTestContext) {
        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        String orderId=consumerApiHelper.placeOrder(Vendorappconstant.CONSUMER_USERNAME,Vendorappconstant.CONSUMER_PASSWORD,restId);
        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderPopUpDisplay());

    }


    @Test(dataProvider = "restaurantLoginData", priority = 3, enabled = true, groups = {"regression"}, description = "verify confirm order from order detail page")
    public void verifyConfirmOrderFromOrderDetailPage(String username, String password,ITestContext iTestContext) {
        vendorHomePage.clickOnNewOrder();
        String orderId=vendorOrderDetailPage.getOrderNumber();
        iTestContext.setAttribute("OrderNumber",orderId);
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        //Assert.assertEquals(vendorOrderDetailPage.getPreprationTime(),iTestContext.getAttribute("defaultPrepTime"));
        vendorOrderDetailPage.increasePreprationTimeToMax();
        Assert.assertEquals(vendorOrderDetailPage.getPreprationTime(), Vendorappconstant.MAX_PREP_TIME);
        vendorOrderDetailPage.decreasePreprationTimeToMin();
        Assert.assertEquals(vendorOrderDetailPage.getPreprationTime(),Vendorappconstant.MIN_PREP_TIME);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        AndroidUtil.sleep();
    }
    @Test(dataProvider = "restaurantLoginData", priority = 4, enabled = true, groups = {"regression"}, description = "verify order detail from home page")
    public void verifyOrderDetailOnHomePage(String username, String password,ITestContext iTestContext) {
        AndroidUtil.sleep(10000);
        Assert.assertEquals(vendorHomePage.getOrderId(0).replaceAll("[^.0-9]", ""),iTestContext.getAttribute("OrderNumber").toString().substring(iTestContext.getAttribute("OrderNumber").toString().length()-4));
        vendorHomePage.clickOnFirstOrder(0);
//        Assert.assertEquals(vendorOrderDetailPage.getPrepTimeLeft().toLowerCase(),iTestContext.getAttribute("prepTime").toString().toLowerCase());
        //System.out.println(vendorOrderDetailPage.getItemName());
        System.out.println(vendorOrderDetailPage.getOrderStatus());
        System.out.println(vendorOrderDetailPage.getTotalBill());
    }
    @Test(dataProvider = "restaurantLoginData", priority = 5, enabled = true, groups = {"regression"}, description = "verify DE status Confirmed")
    public void verifyDeliveryExecuteStatusConfirmed(String username, String password,ITestContext iTestContext) {
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(),OrderStatus.ASSIGNED);
        AndroidUtil.sleep(20000);
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(),OrderStatus.CONFIRMED);
        //consumerApiHelper.changeOrderStatus("1177235784",OrderStatus.ARRIVED);
        driver.closeApp();
        driver.launchApp();
        vendorLoginPage.performLogin(username,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);
        vendorHomePage.clickOnGoToSetting();
        AndroidUtil.sleep();    
        vendorSettingPage.clickOnDisplayOrderSetting();
        AndroidUtil.pressBackKey(driver);


        //AndroidUtil.pressBackKey(driver);
        vendorHomePage.clickOnFirstOrder(0);
        vendorOrderDetailPage.scrollTillDeliveryExecutive();
        if(vendorOrderDetailPage.isDeliveryExecutiveDisplay()){
            vendorOrderDetailPage.scrollTillCallButton();
            Assert.assertEquals(vendorOrderDetailPage.getDeliveryExecutiveName().toString().toLowerCase(),"Test-vaishakh".toLowerCase());
            Assert.assertEquals(vendorOrderDetailPage.getDeliveryExecutiveStatus().toString().toLowerCase(),Vendorappconstant.ORDERSTATUS_CONFIRMED.toLowerCase());
            System.out.println(vendorOrderDetailPage.getDeliveryExecutiveName());

        }
    }
    @Test(dataProvider = "restaurantLoginData", priority = 6, enabled = true, groups = {"regression"}, description = "verify DE status Arrived")
    public void verifyDeliveryExecuteStatusARRIVED(String username, String password,ITestContext iTestContext) {
        AndroidUtil.pressBackKey(driver);
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(),OrderStatus.ARRIVED);
        vendorHomePage.clickOnFirstOrder(0);
        if(vendorOrderDetailPage.isDeliveryExecutiveDisplay()){
            vendorOrderDetailPage.scrollTillCallButton();
            Assert.assertEquals(vendorOrderDetailPage.getDeliveryExecutiveName().toString().toLowerCase(),"Test-vaishakh".toLowerCase());
            Assert.assertEquals(vendorOrderDetailPage.getDeliveryExecutiveStatus().toString().toLowerCase(),Vendorappconstant.ORDERSTATUS_ARRIVED.toLowerCase());
            System.out.println(vendorOrderDetailPage.getDeliveryExecutiveName());

        }
    }
    @Test(dataProvider = "restaurantLoginData", priority = 7, enabled = true, groups = {"regression"}, description = "verify DE status PICKEDUP")
    public void verifyDeliveryExecuteStatusPICKEDUP(String username, String password,ITestContext iTestContext) {
        AndroidUtil.pressBackKey(driver);
        vendorHomePage.clickOnFirstOrder(0);
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(),OrderStatus.PICKEDUP);
        AndroidUtil.sleep();
        Assert.assertEquals(vendorOrderDetailPage.getDialogTitle().toLowerCase(),Vendorappconstant.TITLE_ORDERPICKEDUP.toLowerCase());
        Assert.assertEquals(vendorOrderDetailPage.getDialogSubTitle().toLowerCase(),Vendorappconstant.SUBTITLE_ORDERPICKEDUP.toLowerCase());
        vendorOrderDetailPage.clickOnOkButton();
        Assert.assertFalse(vendorHomePage.isOrderDisplayAfterPick());
        }
    @Test(dataProvider = "restaurantLoginData", priority = 8, enabled = true, groups = {"regression"}, description = "verify mark order OOS")
    public void verifymarkOrderOOS(String restId, String password,ITestContext iTestContext) {
        driver.closeApp();
        driver.launchApp();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);
        vendorHomePage.clickOnGoToSetting();
        AndroidUtil.sleep();
        vendorSettingPage.clickOnDisplayOrderSetting();
        AndroidUtil.pressBackKey(driver);
        String orderId=consumerApiHelper.placeOrder(Vendorappconstant.CONSUMER_USERNAME,Vendorappconstant.CONSUMER_PASSWORD,restId);
        iTestContext.setAttribute("orderIdoos",orderId);
        vendorHomePage.clickOnNewOrder();
        vendorOrderDetailPage.clickOnMarkOOS();
        vendorOrderDetailPage.clickOnButtonOOS();
        vendorOrderDetailPage.selectAlternativesForOOS();
        vendorOrderDetailPage.clickOnButtonDoneForOOS();
        vendorOrderDetailPage.clickOnSubmitAlternates();
        AndroidUtil.pressBackKey(driver);
        Assert.assertEquals(vendorHomePage.getOrderStatusFromHomeScreen(),Vendorappconstant.ORDERSTATUS_PENDING);









    }
   
  



}
