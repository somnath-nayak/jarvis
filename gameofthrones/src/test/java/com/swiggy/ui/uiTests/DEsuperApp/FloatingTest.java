package com.swiggy.ui.uiTests.DEsuperApp;

import com.swiggy.ui.api.DeliveryHelper;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.pojoClasses.superapp.FloatingCash;
import com.swiggy.ui.screen.DeAppReactNative.*;
import com.swiggy.ui.utils.AndroidUtil;

public class FloatingTest {
    public AppiumDriver driver;
    InitializeUI inti;
    LoginPageDERN loginPage;
    HomePageDERN homePage;
        LeftNavDERN leftNav;
        ProfilePageDERN profilePage;
        InvitePageDERN invitePageDERN;
        AutoassignHelper hepauto;
        EarningPageDERN earningPageDERN;
        FloatingCashPageDERN floatingCashPageDERN;
        DeliveryServiceHelper helpdel = new DeliveryServiceHelper();
        DeliveryHelper de=new DeliveryHelper();
    FloatingCash fdetail;

        @DataProvider(name = "LoginData")
        public Object[][] getdata() {
            return new Object[][]{{"216"}};
        }

        @BeforeClass
        public void setUptest() {
            fdetail=de.getFloatingCashDepostiDetails("216");
            System.out.println(fdetail.getDepositCode().get(4).get(0).toString());
            System.out.println(fdetail.getDepositCode().get(1).get(0).toString());
            inti = new InitializeUI();
            loginPage = new LoginPageDERN(inti);
            homePage = new HomePageDERN(inti);
            leftNav = new LeftNavDERN(inti);
            profilePage = new ProfilePageDERN(inti);
            invitePageDERN=new InvitePageDERN(inti);
            earningPageDERN=new EarningPageDERN(inti);
            floatingCashPageDERN=new FloatingCashPageDERN(inti);
            driver = inti.getAppiumDriver();
        }
        @Test(dataProvider = "LoginData",priority = 1)
        public void verifyFloatingCashInLeftNav(String deId){
         String deotp=null;
        loginPage.clickOnAllowPermission();
        loginPage.clickOnAllowPermission();
        loginPage.enterDEId(deId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        loginPage.clickOnContinueButton();
        /*try {
                deotp= (new JSONObject(helpdel.getOtp("216").ResponseValidator.GetBodyAsText())).get("data").toString();
                System.out.println(deotp);
            }
            catch (Exception e){

            }*/
        deotp="547698";
        loginPage.enterOtp(deotp);
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        loginPage.clickOnLoginButton();
        AndroidUtil.sleep(10000);
        homePage.openLeftNav();
        Assert.assertTrue(leftNav.isEarningOptionDisplay());
    }
    @Test(dataProvider = "LoginData",priority = 2)
    public void verifyOpenFloatingTab(String deId){
        leftNav.openFloatingCash();
    }
    @Test(dataProvider = "LoginData",priority = 3)
    public void verifyFloatingCashAvailable(String deId){
        Assert.assertEquals("Current Balance",floatingCashPageDERN.getFloatingCashTitle());
        //Assert.assertEquals(0,floatingCashPageDERN.getCurrentBalance());
        }
    @Test(dataProvider = "LoginData",priority = 4,enabled = true)
    public void verifyOpenSeeStatement(String deId){
            floatingCashPageDERN.openViewStatement();
            Assert.assertFalse(floatingCashPageDERN.btn_seeStatement.isDisplayed());

    }

    @Test(dataProvider = "LoginData",priority = 5,enabled = false)
    public void verifySectionTitle(String deId){
        Assert.assertEquals("DEPOSIT CASH",floatingCashPageDERN.getSectionTitle());
    }
    @Test(dataProvider = "LoginData",priority = 6,enabled = true)
    public void verifyCashDepositSections(String deId){
       for(int i=0;i<2;i++){
            System.out.println(floatingCashPageDERN.getDepositsTitle().get(i).toString());
           System.out.println(floatingCashPageDERN.getDepositsSubTitle().get(i).toString());
       }
    }
    @Test(dataProvider = "LoginData",priority = 7,enabled = true)
    public void verifyICICIDepositSection(String deId){
        floatingCashPageDERN.openDepositSection(0);
        for(int i=0;i<floatingCashPageDERN.getTitleOnDetailView().size();i++){
            System.out.println(floatingCashPageDERN.getTitleOnDetailView().get(i).toString());
            System.out.println(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString());
            Assert.assertEquals(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString().toLowerCase(),fdetail.getChannelDetail().get(1).get(i).toLowerCase());
        }
        System.out.println(floatingCashPageDERN.getDepositCode());
        Assert.assertEquals(floatingCashPageDERN.getDepositCode(),fdetail.getDepositCode().get(1).get(0).toString());
        AndroidUtil.pressBackKey(driver);
    }

    @Test(dataProvider = "LoginData",priority = 8,enabled = true)
    public void verifyNovoPayDepositSection(String deId){
        floatingCashPageDERN.openDepositSection(1);
        AndroidUtil.sleep();
        for(int i=0;i<floatingCashPageDERN.getTitleOnDetailView().size();i++){
            System.out.println(floatingCashPageDERN.getTitleOnDetailView().get(i).toString());
            System.out.println(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString());
            Assert.assertEquals(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString().toLowerCase(),fdetail.getChannelDetail().get(2).get(i).toLowerCase());
        }
        System.out.println(floatingCashPageDERN.getDepositCode());
        Assert.assertEquals(floatingCashPageDERN.getDepositCode(),fdetail.getDepositCode().get(2).get(0).toString());
        AndroidUtil.pressBackKey(driver);
    }

    @Test(dataProvider = "LoginData",priority = 9,enabled = true)
    public void verifySwiggyDepositSection(String deId){
        floatingCashPageDERN.openDepositSection(2);
        for(int i=0;i<floatingCashPageDERN.getTitleOnDetailView().size();i++){
            System.out.println(floatingCashPageDERN.getTitleOnDetailView().get(i).toString());
            System.out.println(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString());
            Assert.assertEquals(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString().toLowerCase(),fdetail.getChannelDetail().get(3).get(i).toLowerCase());
        }
        System.out.println(floatingCashPageDERN.getDepositCode());
        Assert.assertEquals(floatingCashPageDERN.getDepositCode(),fdetail.getDepositCode().get(3).get(0).toString());
        AndroidUtil.pressBackKey(driver);
    }
    @Test(dataProvider = "LoginData",priority = 10,enabled = true)
    public void verifyUPIsection(String deId){
        floatingCashPageDERN.openDepositSection(3);
        for(int i=0;i<floatingCashPageDERN.getTitleOnDetailView().size();i++){
            System.out.println(floatingCashPageDERN.getTitleOnDetailView().get(i).toString());
            System.out.println(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString());
            //Assert.assertEquals(floatingCashPageDERN.getSubTitleOnDetailView().get(i).toString().toLowerCase(),fdetail.getChannelDetail().get(4).get(i).toLowerCase());
        }
        System.out.println(floatingCashPageDERN.getDepositCode());
        Assert.assertEquals(floatingCashPageDERN.getDepositCode(),fdetail.getDepositCode().get(4).get(0).toString());
        AndroidUtil.pressBackKey(driver);
    }








}
