package com.swiggy.ui.uiTests.android.regressionSuite;

import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.aspectj.weaver.ast.And;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.net.UnknownHostException;
import java.util.List;

public class AndroidAppUpgradeTest {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    List<Menulist> al;



    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9108457497","qwerty"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        AndroidUtil.unInstallApp(System.getenv("packageName"));
        AndroidUtil.unInstallApp(   "in.swiggy.android.prod");
        consumerApiHelper=new ConsumerApiHelper();
       /* consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"WORK");
        */
        al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        inti= new InitializeUI();
        driver = inti.getAppiumDriver();
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        intilizeAllclasess();
    }



    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"regression"},description="E2E flow ")
    public void LoginFlowTest(String userName2, String password2, ITestContext iTestContext) {
        launch.tapOnLogin();
        login.LogintoApp(Constants.userName2, Constants.password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        iTestContext.setAttribute("oldAppVersion",AndroidUtil.getAppVersion().trim());
        System.out.println(AndroidUtil.getAppVersion().trim());
    }
    @Test(dataProvider = "LoginData", priority = 2, enabled = true,groups={"regression"},description="E2E flow ")
    public void addItemToCart(String userName2, String password2, ITestContext iTestContext) {
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.clickonFavIcon();
        String restaurantName=restaurantPage.getRestaurantName();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        iTestContext.setAttribute("restaurantName",restaurantName);
        AndroidUtil.pressBackKey(driver,2);

    }
    @Test(dataProvider = "LoginData", priority = 3, enabled = true,groups={"regression"},description="E2E flow ")
    public void openExplore(String userName2, String password2, ITestContext iTestContext) {
        home.openExploreTab();
        explorePage.searchRestaurant(al.get(0).getRestaurantName());
        AndroidUtil.hideKeyboard(driver );
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        AndroidUtil.pressBackKey(driver,4);
    }

    @Test(dataProvider = "LoginData", priority = 4, enabled = true,groups={"regression"},description="E2E flow ")
    public void upgradetest(String userName2,String password2,ITestContext iTestContext) {
        AndroidUtil.upgrade("/Users/nidhi.singh/Documents/APPUPDATES/swiggy1.apk");
        driver.quit();
        inti.createSessionForUpgrade();
        intilizeAllclasess();
        driver=inti.getAppiumDriver();
        driver.launchApp();
        System.out.println(AndroidUtil.getAppVersion().trim());
        System.out.println(iTestContext.getAttribute("oldAppVersion"));
        Assert.assertNotEquals(iTestContext.getAttribute("oldAppVersion"),AndroidUtil.getAppVersion().trim());
    }

    @Test(dataProvider = "LoginData", priority = 5, enabled = true,groups={"regression"},description="E2E flow ")
    public void openCartAndValidateData(String userName2,String password2,ITestContext iTestContext) {
        home.removeSwiggyPop();
        home.openCartTab();
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(),iTestContext.getAttribute("restaurantName"));
        home.openNearMeTab();
        System.out.println("Hello"+checkoutPage.getCartItemCountOnBadge());
    }

    @Test(dataProvider = "LoginData", priority = 6, enabled = true,groups={"regression"},description="E2E flow ")
    public void validateLogin(String userName2,String password2,ITestContext iTestContext) {
        home.openAccountTab();
        Assert.assertEquals(accountPage.getRegisteredMobileNumber(),Constants.userName2);
    }
    @Test(dataProvider = "LoginData", priority = 7, enabled = true,groups={"regression"},description="E2E flow ")
    public void validateSearchQuery(String userName2,String password2,ITestContext iTestContext) {
        home.openExploreTab();
        AndroidUtil.sleep(5000);

    }


   /* @Test(dataProvider = "LoginData", priority = 8, enabled = true,groups={"regression"},description="E2E flow ")
    public void validateFevRest(String userName2,String password2,ITestContext iTestContext) {
        home.openAccountTab();
        accountPage.openFavorities();
        System.out.println(home.getRestaurantNameFromList(0));
        String restaurantName = restaurantPage.getRestaurantName();
        Assert.assertEquals(home.getRestaurantNameFromList(0),restaurantName);
    }*/

    @AfterTest
    public void tearDown(){
        driver.closeApp();
        driver.quit();
    }


    public void intilizeAllclasess(){
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login=new LoginPage(inti);
        addressPage=new AddressPage(inti);
        restaurantPage=new RestaurantPage(inti);
        checkoutPage=new CheckoutPage(inti);
        explorePage=new ExplorePage(inti);
        accountPage=new AccountPage(inti);
        paymentPage=new PaymentPage(inti);
        orderDetailPage=new orderDetailPage(inti);
        trackPage=new TrackPage(inti);
        swiggypopPage=new SwiggypopPage(inti);
    }


}
