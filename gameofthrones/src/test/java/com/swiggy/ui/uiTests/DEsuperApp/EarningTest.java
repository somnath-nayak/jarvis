package com.swiggy.ui.uiTests.DEsuperApp;

import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.screen.DeAppReactNative.*;
import com.swiggy.ui.utils.AndroidUtil;

public class EarningTest{
    public AppiumDriver driver;
    InitializeUI inti;
    LoginPageDERN loginPage;
    HomePageDERN homePage;
        LeftNavDERN leftNav;
        ProfilePageDERN profilePage;
        InvitePageDERN invitePageDERN;
        AutoassignHelper hepauto;
        EarningPageDERN earningPageDERN;
        DeliveryServiceHelper helpdel = new DeliveryServiceHelper();


        @DataProvider(name = "LoginData")
        public Object[][] getdata() {
            return new Object[][]{{"216"}};
        }

        @BeforeClass
        public void setUptest() {
            inti = new InitializeUI();
            loginPage = new LoginPageDERN(inti);
            homePage = new HomePageDERN(inti);
            leftNav = new LeftNavDERN(inti);
            profilePage = new ProfilePageDERN(inti);
            invitePageDERN=new InvitePageDERN(inti);
            earningPageDERN=new EarningPageDERN(inti);
            driver = inti.getAppiumDriver();
        }
        @Test(dataProvider = "LoginData",priority = 1)
        public void verifyEarningOptionInLeftNav(String deId){
         String deotp=null;
        loginPage.clickOnAllowPermission();
        loginPage.clickOnAllowPermission();
        loginPage.enterDEId(deId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        loginPage.clickOnContinueButton();
            try {
                deotp= (new JSONObject(helpdel.getOtp("216").ResponseValidator.GetBodyAsText())).get("data").toString();
                System.out.println(deotp);
            }
            catch (Exception e){

            }
        loginPage.enterOtp(deotp);
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        loginPage.clickOnLoginButton();
        AndroidUtil.sleep(10000);
        homePage.openLeftNav();
        Assert.assertTrue(leftNav.isEarningOptionDisplay());
    }
    @Test(dataProvider = "LoginData",priority = 2)
    public void verifyOpenEarningTab(String deId){
        leftNav.openEarningAndIncentives();
        Assert.assertTrue(earningPageDERN.isCardViewDisplay());
    }
    @Test(dataProvider = "LoginData",priority = 3)
    public void verifyCardHeader(String deId){
        for(int i=0;i<earningPageDERN.getHeaderOfCard().size();i++){
            Assert.assertEquals(getCardName(i),earningPageDERN.getHeaderOfCard().get(i));
        }
    }
    @Test(dataProvider = "LoginData",priority = 4)
    public void verifyCardDateRange(String deId){
        for(int i=0;i<earningPageDERN.getDateRangeOnCard().size();i++){
            //Assert.assertEquals(getCardName(i),earningPageDERN.getHeaderOfCard().get(i));
            System.out.println(earningPageDERN.getDateRangeOnCard().get(i));
        }
    }
    @Test(dataProvider = "LoginData",priority = 5)
    public void verifyEarningPerWeek(String deId){
        for(int i=0;i<earningPageDERN.getWeekTotal().size();i++){
            //Assert.assertEquals(getCardName(i),earningPageDERN.getHeaderOfCard().get(i));
            System.out.println(earningPageDERN.getWeekTotal().get(i));
        }
    }
    @Test(dataProvider = "LoginData",priority = 6)
    public void openCurrentWeek(String deId){
       String currentWeekTotal=earningPageDERN.getWeekTotal().get(0).toString().replaceAll("[^.0-9]", "");
       String currentWeekDateRange=earningPageDERN.getDateRangeOnCard().get(0).toString();
       AndroidUtil.sleep();
       earningPageDERN.clickOnCard(0);
       Assert.assertEquals(currentWeekDateRange,earningPageDERN.getWeekDateRange());
       Assert.assertEquals(currentWeekTotal,earningPageDERN.getTotalPriceOnTab().replaceAll("[^.0-9]", ""));
        }
    @Test(dataProvider = "LoginData",priority = 7)
    public void verifyAllTabsValue(String deId){
            System.out.println(earningPageDERN.getIncentivePriceOntab());
            System.out.println(earningPageDERN.getOrderPriceOnTab());
        System.out.println(earningPageDERN.getTotalPriceOnTab());
       // Assert.assertEquals("0",earningPageDERN.getTotalPriceOnTab());
       // Assert.assertEquals("0",earningPageDERN.getOrderPriceOnTab());
       // Assert.assertEquals("0",earningPageDERN.getIncentivePriceOntab());
    }
    @Test(dataProvider = "LoginData",priority = 8)
    public void verifyWhenUserNotGotAnyOrder(String deId){
        if(true){
            Assert.assertTrue(earningPageDERN.isOrderDetailCardDisplayForADay());
        }
        else{

        }
    }
    @Test(dataProvider = "LoginData",priority = 9)
    public void verifyFirstEarningCardHeaderDetail(String deId){
        System.out.println(earningPageDERN.getEarningCardHeader().get(0));
        System.out.println(earningPageDERN.getEarningCardTotalPrice().get(0));
    }
    @Test(dataProvider = "LoginData",priority = 10)
    public void verifyFirstCardDisplayAsActive(String deId){
       Assert.assertTrue(earningPageDERN.isRestaurantNameDisplayOnCard());
        earningPageDERN.clickOnCard(0);
        Assert.assertFalse(earningPageDERN.isRestaurantNameDisplayOnCard());

    }
    @Test(dataProvider = "LoginData",priority = 11)
    public void verifyRestaurantDetailOnCard(String deId){
        earningPageDERN.clickOnCard(0);
        for(int i=0;i<earningPageDERN.getRestaurantNameOnEarningCard().size();i++){
            System.out.println(earningPageDERN.getRestaurantNameOnEarningCard().get(i).toString());
            System.out.println(earningPageDERN.getTimeStampOnEaringCard().get(i).toString());
            System.out.println(earningPageDERN.getAmountOnEarningCard().get(i).toString());
            System.out.println(earningPageDERN.getOrderStatusOnEarningCard().get(i).toString());


        }

    }






    public String getCardName(int index){
            String str;
        switch (index){
            case 0 :
                str="This week";
                break;
            case 1 :
                str="Last week";
                break;
            default :
                str="Week "+index;
        }
        return str;
    }





}
