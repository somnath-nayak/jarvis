package com.swiggy.ui.uiTests.portal.web;

import org.apache.maven.wagon.providers.ssh.knownhost.UnknownHostException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.portalweb.HomePage;
import com.swiggy.ui.page.classes.portalweb.ListingPage;
import com.swiggy.ui.page.classes.portalweb.LoginPage;
import com.swiggy.ui.page.classes.portalweb.PortalConstants;
import com.swiggy.ui.page.classes.portalweb.SearchPage;
import com.swiggy.ui.page.classes.portalweb.SignUpPage;

public class ListingPageTests 
{
	public WebDriver driver;
    InitializeUI inti;
    HomePage homepage;
    LoginPage loginpage;
    ListingPage listingpage;
    SignUpPage signUppage;
    SearchPage searchpage;
    
    @BeforeClass
    public void setUptest() throws UnknownHostException
    {
        inti = new InitializeUI();
        driver = inti.getWebdriver();
        driver.manage().window().maximize();
        driver.get(PortalConstants.portal_url);
        homepage = new HomePage(inti);
        loginpage = new LoginPage(inti);
        listingpage = new ListingPage(inti);
        searchpage = new SearchPage(inti);
        

    }
    
    
    /*Verify the header elements are present on the listing page*/
    @Test(groups={"regression","sanity"},description="Verify header of listing page",priority=0)
    public void verifyHeader() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
    	homepage.clickOnLogin();
    	loginpage.enterMobileNo(PortalConstants.validPhoneNo);
    	loginpage.enterPassword(PortalConstants.validpassword);
    	loginpage.clickOnLogin();
    	Thread.sleep(1000);    
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.image), "swiggy image missing");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.cart), "Cart is not present");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.myAccount), "my account is not present");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.help), "help is not present");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.search),"Search is not present");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.address), "Address is not present");
    	softAssertion.assertAll();
    	
    	
    }
    
    /*   Verify that user is able to click on help section */
    @Test(groups={"regression","sanity"},description="Verify user is able to click on the help",priority=1)
    public void verifyHelpIsClickable()
    {
    	SoftAssert softAssertion = new SoftAssert();
     	listingpage.clickOnAnElement(listingpage.help);
     	
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.helpPageUrl,"on click of help user not navigated to help page.");
    	softAssertion.assertEquals(listingpage.getText(listingpage.helpPageTitle),"HELP", "The title for help page is not proper.");
    	driver.navigate().back();
    	softAssertion.assertAll();
    	
    	
    	
    	
    	
    }
    
    /*  Verify that user is able to click on My Account section */
    @Test(groups={"regression"},description="Verify user is able to click on the My Account",priority=2)
    public void verifyMyAccountIsClickable()
    {
    	SoftAssert softAssertion = new SoftAssert();
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
    	listingpage.clickOnAnElement(listingpage.myAccount);
    	
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.AccountPageUrl,"on click of help user not navigated to my accounts page.");
    	softAssertion.assertEquals(listingpage.getText(listingpage.accountPageTitle),"MY ACCOUNT", "The title for Accounts page is not proper.");
    	driver.navigate().back();
    	softAssertion.assertAll();
    	
    	
    	
    }
    
    
    /*   Verify that user is able to click on Cart section */
    @Test(groups={"regression",},description="Verify user is able to click on the Cart",priority=3)
    public void verifyMyCartIsClickable() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
    	listingpage.clickOnAnElement(listingpage.cart);
    	
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.CartPageUrl,"on click of cart user not navigated to checkout page.");
    	softAssertion.assertEquals(listingpage.getText(listingpage.checkoutPageTitle),"SECURE CHECKOUT", "The title for cart page is not proper.");
    	driver.navigate().back();
    	softAssertion.assertAll();
    	
    	
    }
    
    /* Verify search is icon is click able */
    @Test(groups={"regression"},description="verify that user is able to click on the search ",priority=4)
    public void verifySearchIconIsClickable()
    {
    	SoftAssert softAssertion = new SoftAssert();
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
        listingpage.clickOnSearchIcon();
    	
     	softAssertion.assertEquals(listingpage.trimedUrl(driver), PortalConstants.searchUrl,"Search page is not loading");
       	softAssertion.assertTrue(searchpage.checkElementDisplay(searchpage.searchInputbox), "search page not loaded");
       	searchpage.clickOnEscButton();
       	softAssertion.assertEquals(listingpage.trimedUrl(driver), PortalConstants.listingpageUrl);
       	softAssertion.assertAll();   	
    }

    

    /*   Verify the Carousel  section is present */
    @Test(groups={"regression","sanity"},description="Verify header of listing page",priority=5)
    public void verifyCarousel() throws InterruptedException
    {
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
    	listingpage.getAllCarousalIntoView();
    	Assert.assertTrue(listingpage.carousalsPresent(), "No carousal present in UI");
    	
    	
    	
    }
    
    /*   Verify that scroll keys are present */
    @Test(groups={"regression"},description="Verify spy keys present for the carousal",priority=6)
    public void verifyCarouselScrollKeys() throws InterruptedException
    {
    	listingpage.refresh(driver);
    	listingpage.getAllCarousalIntoView();
    	Assert.assertTrue(listingpage.checkElementDisplay(listingpage.carousal_navback),"carousal navigation back button missing");
    	
    	
    	
    }
    
    /*    Verify that Open filter section is present */
    @Test(groups={"regression"},description="Verify Open filters are present",priority=7)
    public void openFilter() throws InterruptedException
    {
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	Assert.assertTrue(listingpage.openFilterPresent(),"Open filter not displaying on UI");
    	
    	
    }
    
/*  Verify the collections
    @Test(groups={"regression"},description="Verify collections",priority=8)
    public void verifyCollections() throws InterruptedException
    {
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	listingpage.getTypesOfCollections();
    	Assert.assertTrue(listingpage.verifyCollections(), "Collections are not correct or clickable");
    	
    	
    	
    }*/
    
    /* Verify that when user clicks on the address drop-down should open scroll overlay*/
    @Test(groups={"regression"},description="Verify that when user clicks on the address drop-down should open scroll overlay address bar",priority=9)
    public void verifyAddressDropDown()
    {
    	SoftAssert softAssertion = new SoftAssert();
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
    	
     	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.addressDropDown), "Address dropdown missing");
     	listingpage.clikOnAddressDropDown();
     	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.addressWindowCloseBtn), "close button missing on the select address window");
     	softAssertion.assertAll();
     	

     	
    }
    
   
    
    
    /* verify that filter are present when user click on see all restaurants */
    
    @Test(groups={"regression"},description="Verify filters are present when user clicks on see All restaurants ",priority=10)
    public void verifyFilters()
    {
    	SoftAssert softAssertion = new SoftAssert();
    	driver.navigate().to(listingpage.getCompleteURL(PortalConstants.listingpageUrl));
    	listingpage.refresh(driver);
    	listingpage.clickSeeAllRestaurants();
    	
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.filter), "filter is  missing");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.costForTwo), "cost for two filter missing");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.deliveryTime), "delivery time filter missing");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.rating), "rating filter missing");
    	softAssertion.assertTrue(listingpage.checkElementDisplay(listingpage.relevance), "relevance filter missing");
    	softAssertion.assertAll();
    	
    }
    
    /* verify that rating filter functionality */
    @Test(groups={"regression"},description="Verify rating filter fucntionality",priority=11)
    public void verifyRatingFilter() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	listingpage.clickRatingFilter();
    	Thread.sleep(1000);
    	
    	softAssertion.assertTrue(listingpage.verifyRatingOrder(), "Rating filter is not working properly");
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.ratingUrl,"Url is no proper");
    	softAssertion.assertAll();
    	
    	
    }
    
    /* verify that Delivery time filter functionality */
    @Test(groups={"regression"},description="Verify Delivery time filter fucntionality",priority=12)
    public void verifyDeliveryTimeFilter() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	listingpage.clickDeliveryFilter();
    	listingpage.verifyDeliveryFilter();
    	Thread.sleep(1000);
    	
    	softAssertion.assertTrue(listingpage.verifyDeliveryFilter(), "Delivery Time filter is not working properly");
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.deliveryTimeUrl,"Url is no proper");
    	softAssertion.assertAll();
    	
    	
    }
    
    /* verify that Cost For two filter functionality */
    @Test(groups={"regression"},description="Cost for two filter fucntionality",priority=13)
    public void verifyCostForTwoFilter() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	listingpage.clickCostForTwoFilter();
    	listingpage.verifyDeliveryFilter();
    	Thread.sleep(1000);
    	
    	softAssertion.assertTrue(listingpage.verifyCostForTwoFilter(), "Cost For Two filter is not working properly");
    	softAssertion.assertEquals(listingpage.trimedUrl(driver),PortalConstants.costForTwoUrl,"Url is no proper");
    	softAssertion.assertAll();
    	
    }
   
    
/*     verify that on click of the filter, sub filters are opening  
    @Test(groups={"regression",},description="Cost for two filter fucntionality",priority=9)
    public void verifyFilter() throws InterruptedException
    {
    	listingpage.refresh(driver);
    	Thread.sleep(1000);
    	listingpage.clickOnFilter();
    	Thread.sleep(1000);
    	softAssertion.assertTrue(listingpage.verifyCostForTwoFilter(), "Cost For Two filter is not working properly");
    	softAssertion.assertEquals(listingpage.getCurrentUlr(driver),PortalConstants.costForTwoUrl,"Url is no proper");
    	
    }*/
    
    
    
    

    @AfterClass
    public void teardown()
    {
    	
    	driver.close();
    	
    }
    

}