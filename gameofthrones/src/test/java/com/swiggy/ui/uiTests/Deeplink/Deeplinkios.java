package com.swiggy.ui.uiTests.Deeplink;

import com.swiggy.ui.DeepLink.DeeplinkDataProvider;
import com.swiggy.ui.DeepLink.DeeplinkHelper;
import com.swiggy.ui.DeepLink.Deeplinkconstant;
import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import org.testng.annotations.Test;
import java.util.Map;
import java.util.Set;



public class Deeplinkios extends DeeplinkDataProvider {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    LoginPage login;
    HomePage homePage;
    ConsumerApiHelper consumerApiHelper;
    DeeplinkHelper deeplinkHelper;
    AccountPage accountPage;
    AddressPage addressPage;
    SwiggypopPage swiggypopPage;
    PaymentPage paymentPage;
    RestaurantPage restaurantPage;
    ExplorePage explorePage;
    String Exp_profilename;
    String RestName;
    public boolean isLoginInApp;



    @DataProvider(name = "data")
    public Object[][] data() {
        return new Object[][]{{"9950611829", "qwerty123"}};
    }


    @BeforeClass()
    @Parameters("isLogin")
    public void setup(boolean isLogin) {

        InitializeUI.setDeeplink(true);
        inti = new InitializeUI();
        deeplinkHelper = new DeeplinkHelper();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        login =new LoginPage(inti);
        homePage=new HomePage(inti);
        accountPage=new AccountPage(inti);
        addressPage=new AddressPage(inti);
        deeplinkHelper=new DeeplinkHelper();
        swiggypopPage=new SwiggypopPage(inti);
        paymentPage=new PaymentPage(inti);
        restaurantPage=new RestaurantPage(inti);
        explorePage=new ExplorePage(inti);
        consumerApiHelper=new ConsumerApiHelper();
        consumerApiHelper.removeFavs(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        if(!isLogin){
            loginInToApp(Constants.lat1,Constants.lng1);

        }

        else{
            launch.clickonAllowButton();
            launch.clickOnSetDeliveryLocation();
            homePage.clickOnSortNudge();
            homePage.selectRestaurant(null,1);
            restaurantPage.addItemtest();
        }




    }


    @BeforeMethod
    @Parameters("isLogin")
    public void beforeMethod(boolean isLogin){
        isLoginInApp=isLogin;
    }

    @Test(dataProvider = "DeepLinkURL",priority = 2)
    public void test1(int n, String URL) {
        AndroidUtil.pressBackKey(driver, 5);
        AndroidUtil.sleep();
        String l2 = deeplinkHelper.ParseLink(n, URL, inti);
        System.out.println(l2);
        deeplinkHelper.openDeeplinkIos(URL);
        switch (l2) {

            case Deeplinkconstant.DEEPLINK_POP:

                Assert.assertEquals(swiggypopPage.getpopHeader(),"Introducing Swiggy Pop");
                //swiggypopPage.clickOnFirstPOPItem();


                break;

            case Deeplinkconstant.DEEPLINK_DISCOVERY:
                System.out.println("Discovery");

                explorePage.validateExploreFromDeeplinkios("Punjabi");


                break;

            case Deeplinkconstant.DEEPLINK_FILTER:

                Map<String, String> map=deeplinkHelper.getQueryMap(URL);
                Set<String> keys = map.keySet();

                for (String key : keys) {
                    if (key.contains("CUISINES")) {
                        Assert.assertTrue(homePage.checkFilterApplied());
                        homePage.openFilter();
                        Assert.assertEquals(homePage.isFilterCheckboxSelected(2), "true");
                        AndroidUtil.pressBackKey(driver);
                        homePage.closeFilter();

                    }
                    if (key.contains("SHOW_RESTAURANTS_WITH")) {
                        //Assert.assertTrue(home.checkFilterApplied());
                        homePage.openFilter();
                        // Assert.assertEquals(home.isFilterCheckboxSelected(0), "true");
                        AndroidUtil.pressBackKey(driver);
                        homePage.closeFilter();
                    }
                    System.out.println("Value=" + map.get(key));
                    if(key.contains("sortBy")){
                        Assert.assertEquals(homePage.getSelectedSort().toLowerCase(),map.get(key).replaceAll("_"," ").toLowerCase());
                    }
                }
                break;

            case Deeplinkconstant.DEEPLINK_PROFILE:
                if(!isLoginInApp)
                {
                    loginCheckForProfile();
                }else {
                    Assert.assertTrue(accountPage.ValidateRegisteredMobileNumberios(Constants.userName));
                }


                break;

            case Deeplinkconstant.DEEPLINK_CART:
                homePage.deeplink_openButton();

                break;

            case Deeplinkconstant.DEEPLINK_FAV:
                if(!isLoginInApp){
                    loginCheckForProfile();
                }else {

                    //String Fav=home.getRestaurantNameFromList(0);
                    //Assert.assertEquals(RestName,Fav);
                }


                break;

            case Deeplinkconstant.DEEPLINK_INVITE:
                if(!isLoginInApp){
                    loginCheckForProfile();
                }else {
                    accountPage.validateInviteFromDeeplink();
                }

                break;

            case Deeplinkconstant.DEEPLINK_PAYMENTS:
                if(!isLoginInApp){
                    loginCheckForProfile();
                }else {
                    paymentPage.ValidatePaymentPageFromdeeplink("PAYMENTS");
                }

                break;

            case Deeplinkconstant.DEEPLINK_OFFERS:
                String off=accountPage.getOffersHeader();
                System.out.println(off);


                break;

            case Deeplinkconstant.DEEPLINK_TRACK_ORDER:

                break;

            case Deeplinkconstant.DEEPLINK_SUPPORT:
                if(!isLoginInApp){
                    loginCheckForProfile();
                }else {

                    if (URL.contains("ordId")) {
                        Map<String, String> mapMenu = deeplinkHelper.getQueryMap(URL);
                        Assert.assertEquals(accountPage.gethelpOrderHeader().toLowerCase(), "HELP".toLowerCase());
                    } else {
                        Assert.assertEquals(accountPage.gethelpHeader().toLowerCase(), "HELP WITH YOUR ORDERS".toLowerCase());
                    }
                    AndroidUtil.pressBackKey(driver, 2);
                }

                break;


        }

    }

    @AfterClass
    public void stopServer() {
        System.out.println("Deep link Execution Stopped");


    }
    public void loginCheckForProfile() {
        Assert.assertTrue(accountPage.checkElementDisplay(accountPage.btn_login));
    }

    public void loginInToApp(Double lat,Double lng){

        //consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        //consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat),Double.toString(lng),"WORK");
        Location location = new Location(lat, lng, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        homePage.removeSwiggyPop();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(Constants.userName, Constants.password);
        homePage.openNearMeTab();
        homePage.selectRestaurant(null,1);
        restaurantPage.addItemtest();
        restaurantPage.clickonFavIcon();

    }

}