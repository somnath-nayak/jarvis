package com.swiggy.ui.uiTests.ios.consumerapp.App_update;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.AddressPage;
import com.swiggy.ui.screen.classes.consumerapp.android.HomePage;
import com.swiggy.ui.screen.classes.consumerapp.android.LaunchPage;
import com.swiggy.ui.screen.classes.consumerapp.android.RestaurantPage;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;

import static com.swiggy.ui.utils.AndroidUtil.sleep;

public class AppUpdateTest {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    ConsumerApiHelper consumerApiHelper;
    AppUpdatePage appupdatepage;

    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        appupdatepage = new AppUpdatePage(inti);
    }
// Use UAT01 build
    @Test(dataProvider = "LoginData",enabled = true, groups = {"regression"}, description = "verify Force Update")
    public void verifyForceUpdate(String username, String password)
    {
        System.out.println("ios driver" + driver);
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_force_update","238","yes");
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_optional_update","0","yes");
        consumerApiHelper.clearwpoptionsredis("sandredisstage","2",Constants.wp_options);
        sleep(40000);
        driver.launchApp();
        launch.clickonAllowButton();
        driver.launchApp();
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
         sleep(4000);
        String Expected=appupdatepage.getUpdatemessgae();
        Assert.assertEquals(appupdatepage.getActualmessage(),Expected);
        Assert.assertFalse(appupdatepage.checkifcancelisdisplayed());
        appupdatepage.tapOnUpdate();
    }

    @Test(dataProvider = "LoginData",enabled = true, groups = {"regression"}, description = "verify Optional Update")
    public void verifyOptionalUpdate(String username, String password)
    {
        System.out.println("ios driver" + driver);
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_force_update","0","yes");
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_optional_update","238","yes");
        consumerApiHelper.clearwpoptionsredis("sandredisstage","2",Constants.wp_options);
        sleep(40000);
        driver.launchApp();
        launch.clickonAllowButton();
        driver.launchApp();
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(4000);
        String Expected=appupdatepage.getUpdatemessgae();
        Assert.assertEquals(appupdatepage.getActualmessage(),Expected);
        Assert.assertTrue(appupdatepage.checkifcancelisdisplayed());
        appupdatepage.tapOnCancel();
        homePage.openAccountTab();
    }

    @Test(dataProvider = "LoginData",enabled = true, groups = {"regression"}, description = "verify Force Update Priority")
    public void verifyForceUpdatePriority(String username, String password)
    {
        System.out.println("ios driver" + driver);
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_force_update","238","yes");
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_optional_update","238","yes");
        consumerApiHelper.clearwpoptionsredis("sandredisstage","2",Constants.wp_options);
        sleep(40000);
        driver.launchApp();
        launch.clickonAllowButton();
        driver.launchApp();
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(4000);
        Assert.assertFalse(appupdatepage.checkifcancelisdisplayed());
    }

    @Test(dataProvider = "LoginData",enabled = true, groups = {"regression"}, description = "verify Force Update Disable")
    public void verifyUpdateDisable(String username, String password)
    {
        System.out.println("ios driver" + driver);
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_force_update","0","yes");
        consumerApiHelper.setWp_Options("v2_new_ios_consumer_optional_update","0","yes");
        consumerApiHelper.clearwpoptionsredis("sandredisstage","2",Constants.wp_options);
        sleep(40000);
        driver.launchApp();
        launch.clickonAllowButton();
        driver.launchApp();
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(4000);
        homePage.openAccountTab();
    }

    @AfterMethod
    public void tearDown() throws Exception {

        driver.resetApp();

    }
    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
