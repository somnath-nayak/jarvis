package com.swiggy.ui.uiTests.android.consumerapp;
import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.AppiumServerJava;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.TouchAction;

import org.aspectj.weaver.ast.And;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pojoClasses.GetAddressPojo;
import scopt.Check;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import java.io.IOException;
import java.util.*;

import framework.gameofthrones.JonSnow.Processor;
import com.swiggy.ui.pojoClasses.*;
import org.openqa.selenium.html5.*;
import java.text.MessageFormat;

public class EDVOFeatureSanity {


    public AndroidDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    TrackPage trackPage;
    EDVOMeal edvoMeal;

    String dominosRestaurantId;
    Restaurantmenu restaurantmenu;

    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{Constants.userName, Constants.password}};
    }

    @BeforeClass
    public void setUptest() {


        consumerApiHelper = new ConsumerApiHelper();
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "CUISINES:Pizzas");
        for(int i=0;i<al.size();i++){
            if(al.get(i).getRestaurantName().equalsIgnoreCase(Constants.dominosRestaurantName)){
                dominosRestaurantId=al.get(i).getRestaurantId();
            }
        }
        restaurantmenu=consumerApiHelper.getRestaurantMenuV4(dominosRestaurantId,"",Double.toString(Constants.lat1),Double.toString(Constants.lng1),Constants.userName, Constants.password,Constants.ANDROID_USER_AGENT,"300");
        inti = new InitializeUI();

        driver = inti.getAndroidDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);


    }

    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"EDVO"},description="validate EDVO OnRestaurantPage")
    public void verifyEDVOOnRestaurantPage(String username,String password,ITestContext context) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");

        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("Pizzas");
        AndroidUtil.sleep();
        home.clickonSortRestaurant();
        home.selectSorting(Constants.DELIVERYTIME_SORT);
        Assert.assertEquals(home.getOfferDiscount(0).toLowerCase(),"Every Day Value Offer".toLowerCase());
        home.selectRestaurantNew(Constants.dominosRestaurantName, 0);
        restaurantPage.removeCoachmark();
        String firstrestaurantName = restaurantPage.getRestaurantName();
        context.setAttribute("restaurantName",Constants.dominosRestaurantName);
        restaurantPage.validateEDVOOnRestaurantePage(restaurantmenu);


    }
    @Test(dataProvider = "LoginData", priority = 2, enabled = true,groups={"EDVO"},description="validate EDVO Landing screen")
    public void verifyLandingScreen(String username,String password) {
        restaurantPage.openEDVOMeal(0);
        edvoMeal=consumerApiHelper.getEDVOMealObject(dominosRestaurantId,Integer.toString(restaurantmenu.getEdvoOfferId().get(0)));
        restaurantPage.validateEDVOLandingScreen(edvoMeal);

    }
    @Test(dataProvider = "LoginData", priority = 3, enabled = true,groups={"EDVO"},description="validate EDVO first pizza screen")
    public void verifyPizzaScreen(String username,String password) {
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.validatePizzaScreen(edvoMeal,0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.validatePizzaScreen(edvoMeal,1);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
    }
    @Test(dataProvider = "LoginData", priority = 4, enabled = true,groups={"EDVO"},description="validate EDVO- Press back key from meal page ")
    public void verifyCancellationDialog(String username,String password) {
       driver.pressKeyCode(AndroidKeyCode.BACK);
       Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(),Constants.mealCancelPopMessage.toLowerCase());
       restaurantPage.clickonReplaceCartItemDialogYes();
       Assert.assertTrue(restaurantPage.txt_EDVOlandingPage_header.isDisplayed());
    }
    @Test(dataProvider = "LoginData", priority = 5, enabled = true,groups={"EDVO"},description="validate EDVO -Trying to add more than one item in a group")
    public void verifyTryingToAddMoreThanOneItem(String username,String password) {
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.selectFirstPizza();
        restaurantPage.clickOnIncreaseItemCount();
        Assert.assertEquals(restaurantPage.getSnackBarText().toLowerCase(),Constants.ERROR_INCREASEITEMCOUNT.toLowerCase());
        restaurantPage.clickOnButtonOkayGotIt();
        driver.pressKeyCode(AndroidKeyCode.BACK);
    }
    @Test(dataProvider = "LoginData", priority = 6, enabled = true,groups={"EDVO"},description="validate EDVO meal selection item")
    public void verifyMealSelectedIcon(String username,String password) {
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.isMealSelectedItemDisplay(0));
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.isMealSelectedItemDisplay(1));
        driver.pressKeyCode(AndroidKeyCode.BACK);
        }


    @Test(dataProvider = "LoginData", priority = 7, enabled = true,groups={"EDVO"},description="validate EDVO -meal confirmation page")
    public void verifyMealConfirmationPage(String username,String password,ITestContext context) {
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.selectFirstPizza();
        String itemName_1=restaurantPage.getItemName(0);
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.selectFirstPizza();
        String itemName_2=restaurantPage.getItemName(0);
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        AndroidUtil.sleep(5000);
        context.setAttribute("itemName_1",itemName_1);
        context.setAttribute("itemName_2",itemName_2);
        Assert.assertEquals(restaurantPage.getItemName(0).toLowerCase(),itemName_1.toLowerCase());
        Assert.assertEquals(restaurantPage.getItemName(1).toLowerCase(),itemName_2.toLowerCase());
        Assert.assertTrue(restaurantPage.isAddMoreItemButtonDisplay());

    }
    @Test(dataProvider = "LoginData", priority = 8, enabled = true,groups={"EDVO"},description="validate EDVO meal on cart page")
    public void verifyMealOnCartPage(String username,String password,ITestContext context) {
        restaurantPage.clickOnViewCart();
        checkoutPage.getRestaurantNameFromCart();
        checkoutPage.validateCartForMeal(edvoMeal,context);
        Assert.assertEquals(checkoutPage.getMealPrice(1),"398");
        Assert.assertEquals(checkoutPage.getMealPrice(0),"610");
        context.setAttribute("mealDiscountedPrice",Integer.parseInt(checkoutPage.getMealPrice(1)));
        context.setAttribute("mealOriginalPrice",Integer.parseInt(checkoutPage.getMealPrice(0)));



    }
    @Test(dataProvider = "LoginData", priority = 9, enabled = true,groups={"EDVO"},description="validate EDVO meal repeat from cart")
    public void verifyRepeat(String username,String password,ITestContext context) {
        scrollDownOnCheckoutPage();
        HashMap<String,Double> hm_beforeIncrease=checkoutPage.getRestaurantBillInfo();
        scrollUpOnCheckoutPage();
        checkoutPage.increaseItemCount();
        Assert.assertTrue(checkoutPage.getMealNameOnCustomizationPopup().toString().toLowerCase().contains(edvoMeal.getMealName().toLowerCase()));
        System.out.println(checkoutPage.getPriceOnCustomizationPopup());
        Assert.assertEquals(checkoutPage.getPriceOnCustomizationPopup().toString().toLowerCase(),"398");
        Assert.assertTrue(checkoutPage.getMealItemsOnCustomizationPopup().toString().toLowerCase().contains(context.getAttribute("itemName_1").toString().toLowerCase()));
        Assert.assertTrue(checkoutPage.getMealItemsOnCustomizationPopup().toString().toLowerCase().contains(context.getAttribute("itemName_2").toString().toLowerCase()));
        checkoutPage.selectRepeatForIncreaseItemCount();
        AndroidUtil.sleep(10000);
        Assert.assertEquals(Integer.parseInt(checkoutPage.getMealPrice(1)),((int) context.getAttribute("mealDiscountedPrice"))*2);
        Assert.assertEquals(Integer.parseInt(checkoutPage.getMealPrice(0)),((int)context.getAttribute("mealOriginalPrice"))*2);
        scrollDownOnCheckoutPage();
        HashMap<String,Double> hm_afterIncrease=new HashMap<>();
        hm_afterIncrease=checkoutPage.getRestaurantBillInfo();
        Set<String> set=hm_beforeIncrease.keySet();
        Iterator it=set.iterator();
        while(it.hasNext()){
            String key=it.next().toString();
            Double value=hm_afterIncrease.get(key);
            System.out.println(key);
            System.out.println(value);
            Assert.assertEquals(hm_afterIncrease.get(key),hm_beforeIncrease.get(key)*2);
        }


        System.out.println(checkoutPage.getRestaurantFinalBill());




    }
    @Test(dataProvider = "LoginData", priority = 10, enabled = true,groups={"EDVO"},description="validate EDVO meal customization form cart")
    public void verifySelectIWillCusti(String username,String password,ITestContext context) {
        checkoutPage.increaseItemCount();
        checkoutPage.clickonIwillCustomise();
        restaurantPage.validateEDVOLandingScreen(edvoMeal);
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.selectFirstPizza(1);
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.selectFirstPizza();
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        Assert.assertFalse(restaurantPage.checkElementDisplay(restaurantPage.btn_AddMoreItem_ConfimationScreen));
        restaurantPage.clickOnViewCart();
        AndroidUtil.sleep(5000);
        Assert.assertEquals(checkoutPage.getItemNameFromCart(3).toLowerCase(),edvoMeal.getMealName().toLowerCase());


    }



    @Test(dataProvider = "LoginData", priority = 11, enabled = true,groups={"EDVO"},description="validate Payment page for EDVO")
    public void verifyProceedToPay(String username,String password,ITestContext context) {
        checkoutPage.proceedToPay();
        String header=paymentPage.getPaymentHeader();
        Assert.assertEquals(header.toLowerCase(),Constants.PAYMENTPAGEHEADER.toLowerCase());

    }

    @Test(dataProvider = "LoginData", priority = 12, enabled = true,groups={"EDVO"},description="validate EDVO on search page")
    public void verifySearchDominosFromExplorer(String username,String password,ITestContext context) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        driver.closeApp();
        driver.launchApp();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openExploreTab();
        explorePage.searchRestaurant("Domino");
        AndroidUtil.sleep();
        Assert.assertEquals(home.getRestaurantNameFromList(0),Constants.dominosRestaurantName);
        Assert.assertEquals(home.getOfferDiscount(0),"Every Day Value Offer");
        home.selectRestaurantNew(Constants.dominosRestaurantName, 0);
        restaurantPage.removeCoachmark();

        restaurantPage.validateEDVOOnRestaurantePage(restaurantmenu);
        }
    @Test(dataProvider = "LoginData", priority = 13, enabled = true,groups={"EDVO"},description="validate AddMoreItemFromMealConfirmationScreen")
    public void verifyAddMoreItemFromMealConfirmationScreen(String username,String password,ITestContext context) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        driver.closeApp();
        driver.launchApp();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openExploreTab();
        explorePage.searchRestaurant("Domino");
        home.selectRestaurantNew(Constants.dominosRestaurantName, 0);
        restaurantPage.removeCoachmark();
        restaurantPage.openEDVOMeal(0);
        EDVOMeal edvoMeal=consumerApiHelper.getEDVOMealObject(dominosRestaurantId,Integer.toString(restaurantmenu.getEdvoOfferId().get(0)));
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.selectFirstPizza();
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.validatePizzaScreen(edvoMeal,1);
        restaurantPage.selectFirstPizza();
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.clickonAddMoreItemButtonOnMealConfirmationPage();
        AndroidUtil.sleep(5000);
        System.out.println(restaurantmenu);
        restaurantPage.filterMenu(restaurantmenu.getCollection().get(2).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();




    }


    public String getDominosRestaurantId(){
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "CUISINES:Pizzas");
        for(int i=0;i<al.size();i++){
            if(al.get(i).getRestaurantName().equalsIgnoreCase(Constants.dominosRestaurantName)){
                dominosRestaurantId=al.get(i).getRestaurantId();
            }
        }
        System.out.println(dominosRestaurantId);
        return dominosRestaurantId;
    }

    public void scrollUpOnCheckoutPage(){
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();

    }
    public void scrollDownOnCheckoutPage(){



        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();

    }




}
