package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import junit.framework.Assert;
import org.openqa.selenium.html5.Location;
import org.testng.annotations.*;

import java.net.UnknownHostException;

import static com.swiggy.ui.utils.AndroidUtil.Direction;
import static com.swiggy.ui.utils.AndroidUtil.sleep;


public class Cafe_iOS {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    SwiggyCafe cafepage;
    Double lat1 = 12.932815;
    Double lng1 = 77.603578;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
        cafepage = new SwiggyCafe(inti);


    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Cafe entry point")
    public void verifyTapOnCafeFromListing(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        sleep(20000);
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        //Assertion is inside the method
        cafepage.validatestartorderingpage();
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Startordering")
    public void verifyStartOrdering(String username, String password) {
        System.out.println("ios driver" + driver);
        // driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        //Assertion is inside the method
        cafepage.validateworkspacepage();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Tap on cooperate")
    public void verifyTapOnCooperate(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        //Assertion is inside the method
        cafepage.validateunlockscreen();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify valid passcode")
    public void verifyValidPasscode(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        //Assertion is inside the method
        cafepage.validatecafelistpage();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Change And Close button")
    public void verifyChangeAndClose(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        cafepage.tapOnChange();
        //Assertion is inside the method
        cafepage.validatecafelistpage();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Tap on cafe restaurant")
    public void verifyTapOnRestaurant(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        //Need to add assertion
        cafepage.tapOnfirstCafeoutlet(null, 0);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Add cafe item & tap on view cart")
    public void verifyAdditemAndViewCart(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.tapOnfirstCafeoutlet(null, 0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Cafe address and how it works in cart")
    public void verifyCafeAddressandHowItWorksinCart(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.tapOnfirstCafeoutlet(null, 0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        AndroidUtil.scrollios(driver, Direction.UP);
        //Assertion is indie the method
        checkoutPage.validatecafecartwithtext();
        sleep(20000);
        checkoutPage.tapOnHowitworks();
        checkoutPage.taponokgotit();
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify MakePayment and order place")
    public void VerifyMakePayment(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.tapOnfirstCafeoutlet(null, 0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        checkoutPage.clickOnProceedToPay();
        //paymentPage.clickonCOD();
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify COD not available")
    public void verifyCODmsg(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.tapOnfirstCafeoutlet(null, 0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        checkoutPage.clickOnProceedToPay();
        //Assertion is inside the method
        paymentPage.validateCODmsg();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Backbuttons")
    public void verifybackintroandcorp(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        cafepage.taponbackbutton();
        cafepage.taponbackbutton();
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify backbutton from list")
    public void verifybackbuttonlist(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.taponbacklist();
        Assert.assertTrue(cafepage.listingelementstatus());
    }

/*
    @Test(dataProvider = "LoginData", priority = 2, enabled = true, groups = {"regression"}, description = "verifytaponcafefromlisting")
    public void verifylockimage(String username, String password) {
        //driver.getPageSource();
        //Location location = new Location(lat1, lng1, 0);
        System.out.println("ios driver" + driver);
        // driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        sleep(20000);
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        Assert.assertTrue(cafepage.verifylockimage());

    } */

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify invalid passcode")
    public void verifyInvalidLockScreen(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterinvalidPasscode();
        Assert.assertTrue(cafepage.getUnlockPasscodestatus());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify lock for same section")
    public void validatedLockForSameSection(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        cafepage.taponbacklist();
        cafepage.taponbackbutton();
        cafepage.taponbackbutton();
        cafepage.tapOnCafebar();
        cafepage.tapOnCooperate();
        //Assertion is inside the method
        cafepage.validatecafelistpage();
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify increase item count and apply coupon for cafe order")
    public void verifyApplycouponincreaseitem(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation();
        homePage.taponsorttooltip();
        cafepage.tapOnCafebar();
        cafepage.tapOnStartOrdering();
        sleep(20000);
        cafepage.tapOnCooperate();
        cafepage.enterPasscode();
        sleep(20000);
        cafepage.tapOnfirstCafeoutlet(null, 0);
        restaurantPage.addItemtest();
        //restaurantPage.addItemToCartForIOs();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        sleep(20000);
        checkoutPage.cafescrollup();
        checkoutPage.ioscafeapplycoupon();
        //give any coupon name
        checkoutPage.enterCoupon("AMAZONP40");
        checkoutPage.clickonApplyCouponButton();
        checkoutPage.clickOnOK();
        sleep(20000);
        checkoutPage.cafescrollDown();
        checkoutPage.increaseItemCount();
        //test
    }



    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }


    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}