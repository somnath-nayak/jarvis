package com.swiggy.ui.uiTests.dashboard.dp;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import org.testng.annotations.DataProvider;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsConstants;

import java.io.IOException;

/**
 * Created by kiran.j on 12/7/17.
 */
public class VendorDp {
    
    OMSHelper omsHelper = new OMSHelper();
    LOSHelper losHelper = new LOSHelper();


    @DataProvider(name = "createorder")
    public Object[][] create() throws IOException, InterruptedException {
        String order_id = losHelper.getAnOrder("partner");
        omsHelper.verifyOrder(order_id, VmsConstants.order_confirm_code);
        return new Object[][]{{order_id}};
    }
    
    @DataProvider(name = "createDiscount")
    public Object[][] createDiscount() {
        return new Object[][]{
        	{"2", "10"}
        	};
    }
    
    @DataProvider(name = "createSelfServeMenuTicket")
    public Object[][] createSelfServeMenuTicket() {
        return new Object[][]{
        	{"AutomationItem", 10, 2}
        	};
    }
    
    @DataProvider(name = "editItem")
    public Object[][] editItem() {
        return new Object[][]{
        	{10, 2}
        	};
    }
    
}
