package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;

import static com.swiggy.ui.utils.AndroidUtil.sleep;

public class iOS_HygieneRating {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    ConsumerApiHelper consumerApiHelper;
    ExplorePage explorepage;

    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        explorepage = new ExplorePage(inti);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Hygiene Rating(image and click), Back button, Menu which doesnt have HR")
    public void verifyHygieneRatingandClickandNoHRmenu(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Mughal");
        homePage.selectRestaurant("Mughal Sarai", 0);
        restaurantPage.verifyHygieneimage();
        restaurantPage.tapOnHygienerating();
        Assert.assertEquals(restaurantPage.getHygieneratingtext(), Constants.Hygiene_txt);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Feed back and CTA")
    public void verifyFeedbackandCTA(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Mughal");
        homePage.selectRestaurant("mughal sarai",0);
        restaurantPage.tapOnHygienerating();
        restaurantPage.verifyFeedback();
        restaurantPage.verifyContinueOrdering();
        Assert.assertTrue(restaurantPage.getDeliverytimetext());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }


    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
