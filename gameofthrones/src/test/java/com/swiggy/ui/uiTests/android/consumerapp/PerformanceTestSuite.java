package com.swiggy.ui.uiTests.android.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.Viserion.android.DragonViserion;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
//import org.apache.http.HttpHost;
//import org.apache.http.client.config.RequestConfig;
//import org.apache.http.client.methods.CloseableHttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.client.CloseableHttpClient;
//import org.apache.http.impl.client.HttpClients;
//import org.apache.http.util.EntityUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.html5.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.*;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

//import java.io.IOException;
import java.util.List;

public class PerformanceTestSuite {

    AndroidUtil androidUtil = new AndroidUtil();
    DragonViserion viserion = new DragonViserion();
    private static Logger log = LoggerFactory.getLogger(PerformanceTestSuite.class);
    static final String appType = "ConsumerApp";
    public AppiumDriver driver;
    //public AndroidDriver driver;
    InitializeUI got;
    LaunchPage launch;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    PaymentPage paymentPage;
    HomePage home;
    TrackPage trackPage;
    AccountPage accountPage;
    ExplorePage explorePage;
    orderDetailPage orderDetailPage;

    ConsumerApiHelper consumerApiHelper;
    //Double lat2=27.834105;
    //Double lng2=76.171977;
    Double lat1 = 12.932709778627975;
    Double lng1 = 77.60356619954109;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9036976038", "huawei123"}};
    }

    @BeforeTest(alwaysRun = true)
    public void setupViserion() {
        //push apk size data; this solution till merged with CI-CD
        got = new InitializeUI();
        driver = got.getAppiumDriver();
        viserion.pushApkSize(appType);
        viserion.disableCharging();
        viserion.resetBatteryStats();
    }

    @BeforeClass(alwaysRun = true)
    public void setup() {

        consumerApiHelper = new ConsumerApiHelper();
        launch = new LaunchPage(got);
        login = new LoginPage(got);
        addressPage = new AddressPage(got);
        restaurantPage = new RestaurantPage(got);
        checkoutPage = new CheckoutPage(got);
        paymentPage = new PaymentPage(got);
        trackPage = new TrackPage(got);
        home = new HomePage(got);
        accountPage = new AccountPage(got);
        explorePage = new ExplorePage(got);
        //orderDetailPage = new orderDetailPage(got);
    }


    @Test(dataProvider = "LoginData", priority = 1, groups = {"AndroidPerformanceTest", "Viserion", "aviral"}, enabled = true)
    public void endToEndFlow(String mobile, String password) throws InterruptedException {
        viserion.fire("End To End Flow", appType);
        end2EndFlowHelper(mobile, password);
    }

    @Test(dataProvider = "LoginData", priority = 2, enabled = true,groups={"AndroidPerformanceTest", "Viserion"},description=" verifyForNonServicableLocation ")
    public void verifyForNonServicableLocation(String userName2,String password2) throws InterruptedException {
       viserion.fire("Verify Nonservicable Location", appType);
        Location location=new Location(Constants.lat_nonServicable,Constants.lng_nonServicable,0);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        addressPage.clickonSkipAddress();
        AndroidUtil.sleep(1000);
        home.removeSwiggyPop();
        // Assert.assertEquals(home.getNotServicableLocationText(),Constants.LOCATION_NOT_SERVICABLE_IOS);
        home.clickOnEditLocation();

    }


    @Test(dataProvider = "LoginData", priority = 3, groups = {"AndroidPerformanceTest", "Viserion", "aviral"}, enabled = true)
    public void scrollListingPage(String username, String password) throws InterruptedException {
        viserion.fire("Scroll Listing Page", appType);
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
       driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        //home.selectRestaurant(null,0);
        //System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        //scroll down and up 5 times
        for (int count = 1; count <= 5; count++) {
            for (int i = 0; i < 7; i++) {
                log.info("Testing Driver Scroll");
                Dimension dimensions = driver.manage().window().getSize();
                log.info("Size of Window= " + dimensions);
                int scrollStart = (int) (dimensions.getHeight() * 0.8);
                log.info("Size of scrollStart= " + scrollStart);
                int scrollEnd = (int) (dimensions.getHeight() * 0.60);
                log.info("Size of scrollEnd= " + scrollEnd);
                driver.swipe(0, scrollStart, 0, scrollEnd, 250);
                Thread.sleep(1000);
                log.info("Screen Scrolled down .... ");
            }
            for (int i = 0; i < 7; i++) {
                log.info("Testing Driver Scroll");
                Dimension dimensions = driver.manage().window().getSize();
                log.info("Size of Window= " + dimensions);
                int scrollStart = (int) (dimensions.getHeight() * 0.8);
                log.info("Size of scrollStart= " + scrollStart);
                int scrollEnd = (int) (dimensions.getHeight() * 0.60);
                log.info("Size of scrollEnd= " + scrollEnd);
                driver.swipe(0, scrollEnd, 0, scrollStart, 250);
                Thread.sleep(1000);
                log.info("Screen Scrolled up .... ");
            }
            log.info("Scrolled DOWN-UP :: " + count + " :: Times");
        }
    }

    @Test(dataProvider = "LoginData", priority = 4, groups = {"AndroidPerformanceTest", "Viserion", "aviral"}, enabled = true)
    public void e2eWithBackgroundApps(String mobile, String password) throws InterruptedException {
        /*
         * This will launch:
         * Facebook
         * Instagram
         * Amazon Prime
         * Youtube
         * Netflix
         * Gmail
         * Whatsapp
         * Firefox Browser
         * Flipkart
         * Super Mario Run
         * Then Runs E2E FLow
         */
        Thread.sleep(1000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch facebook
        viserion.startActivity("com.facebook.katana", ".LoginActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch instagram
        viserion.startActivity("com.instagram.android", ".activity.MainTabActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch Youtube
        viserion.startActivity("com.google.android.youtube", ".HomeActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch Netflix
        viserion.startActivity("com.netflix.mediaclient", ".ui.launch.NetflixComLaunchActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch Gmail
        viserion.startActivity("com.google.android.gm", ".ui.MailActivityGmail");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch flipkart
        viserion.startActivity("com.flipkart.android", ".activity.HomeFragmentHolderActivity");
        Thread.sleep(2000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch whatsapp
        viserion.startActivity("com.whatsapp", ".HomeActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch amazon prime
        viserion.startActivity("com.amazon.avod.thirdpartyclient", ".LauncherActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch firefox browser
        viserion.startActivity("org.mozilla.firefox", ".App");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);
        //launch Super Mario Run
        viserion.startActivity("com.nintendo.zara", "com.snslinkage.dena.snslinkageunityplugin.SnsLinkageSafeUnityPlayerNativeActivity");
        Thread.sleep(3000);
        androidUtil.clickHomeButtonOfDevice(driver);
        Thread.sleep(1000);

        //trigger Viserion
        viserion.fire("End2End With Multiple Background Apps", appType);
        //Now Run end to end flow
        driver.launchApp();
        end2EndFlowHelper(mobile, password);
        // Thread.sleep(5000);
    }

    @Test(priority = 5, groups = {"AndroidPerformanceTest", "Viserion", "aviral"}, enabled = true)
    public void appForegroundBackground() throws InterruptedException {
        viserion.fire("App Foreground-Background", appType);
        for (int i = 0; i < 20; i++) {
           driver.launchApp();
            Thread.sleep(5000);
            androidUtil.clickHomeButtonOfDevice(driver);
        }
    }


    @Test(dataProvider = "LoginData", priority = 6, groups = {"AndroidPerformanceTest", "Viserion", "Link/Delink Wallets"}, enabled = true)
    public void verifyLinkWallet(String username, String password) throws InterruptedException {
        viserion.fire("Link/Delink Wallets", appType);
        verifyLinkWalletHelper(username, password);
    }

    @Test(dataProvider = "LoginData", priority = 7, groups = {"AndroidPerformanceTest", "Viserion", "SearchAndPlaceOrder"}, enabled = true)
    public void searchAndPlaceOrder(String username, String password) throws InterruptedException {
            Location location = new Location(lat1, lng1, 0);
           consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
            consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat1),Double.toString(lng1),"WORK");
        String restaurantName;
        viserion.fire("Search And Place Order", appType);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        home.openExploreTab();
        Assert.assertEquals(home.checkElementDisplay(home.explore),"explore button is not showing");
        explorePage.doSearch("idli");
        Assert.assertEquals(explorePage.checkElementDisplay(explorePage.btn_dishesTab),"Dishesh tab is not showing");
        explorePage.clickOnTab("DISHES");
        explorePage.list_dishesh.get(0).click();
        //String restaurantname = explorePage.searchDishesAndAddToCart("Idli");
        //explorePage.clickonFullMenu();
        //restaurantPage.removeCoachmark();
        //Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantname.toLowerCase());
        //restaurantPage.openCartFromRestaurantPage();
        //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(), restaurantname.toLowerCase());


    }

    @Test(dataProvider = "LoginData", priority = 8, groups = {"AndroidPerformanceTest", "Viserion", "ReorderFlow"}, enabled = true)
    public void ReorderFlow(String username, String password) throws InterruptedException {

        String restaurantName;
        viserion.fire("Reorder Flow", appType);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openAccountTab();
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        if (accountPage.checkAnyActiveOrderPresent()) {
            accountPage.clickonTrackOrder(0);
        }
        if (accountPage.checkAnyPastOrderPresent()) {
            restaurantName = accountPage.getRestaurantNameFromMyOrder(0);
            accountPage.openViewOrderDetail(0);
            System.out.println(orderDetailPage.getpageTitle());
            System.out.println(orderDetailPage.getPageSubTitle());
            AndroidUtil.pressBackKey(driver);
            AndroidUtil.sleep();
            accountPage.clickOnReorder(0);
            checkoutPage.clickOk();
            System.out.println(checkoutPage.getRestaurantNameFromCart());
            System.out.println(restaurantName);
            Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(), restaurantName);


        }
    }


   /* @Test(dataProvider = "LoginData", priority = 5,groups = {"AndroidPerformanceTest", "Viserion", "aviral"}, enabled = true)
    public void e2eWithNetworkThrottle(String mobile, String password) throws InterruptedException, IOException {
       //setDeviceProxyHelper();
        throttleNetworkHelper();
        viserion.fire("End To End With Network Throttle", appType);
        end2EndFlowHelper(mobile, password);
        unsetDeviceProxyHelper();
    }*/


    @AfterMethod
    public void tearDown(){
        driver.closeApp();
        //driver.quit();
        driver.launchApp();
    }

    @AfterTest
    public void revertingStuff(){
        viserion.enableCharging();
    }

    /* * END TO END FLOW HELPER METHOD * */

    public void end2EndFlowHelper(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat2),Double.toString(Constants.lng2));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        AndroidUtil.sleep(1000);
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);


    }

    public void verifyLinkWalletHelper(String username, String password)
    {
       consumerApiHelper.delinWallet(username,password,"mobikwik");
       consumerApiHelper.delinWallet(username,password,"paytm");
       consumerApiHelper.delinWallet(username,password,"freecharge");
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openAccountTab();
        accountPage.openManagePayment();
        System.out.println(accountPage.getSwiggyMoneyBalance());
        accountPage.linkWallet("paytm");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        AndroidUtil.pressBackKey(driver);
        accountPage.linkWallet("freecharge");
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        System.out.println(accountPage.getWalledLinkHeaderText());
        AndroidUtil.pressBackKey(driver);
        accountPage.linkWallet("mobikwik");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        AndroidUtil.pressBackKey(driver);


    }




        /* * SETUP PROXY HELPER METHODS
     * USES ANDROIDPROXYSETTER & APACHE HTTPCLIENT */

   /* public void setDeviceProxyHelper() {

        String ip = viserion.execShellCommand("ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'").trim();
        log.info("IP ADDRESS ::: "+ip);
        final String SETANDROIDPROXY = "adb shell am start -n tk.elevenk.proxysetter/.MainActivity -e host "+ip+" -e port 8888 -e ssid \"Swiggy_Mobile\" -e key ilbtfY@2201";
        log.info("SETANDROIDPROXY ::: " + SETANDROIDPROXY );
        String out = viserion.execShellCommand(SETANDROIDPROXY);
    }

    public void unsetDeviceProxyHelper() {
        String ip = viserion.execShellCommand("ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'").trim();
        log.info("IP ADDRESS ::: "+ip);
        final String UNSETANDROIDPROXY = "adb shell am start -n tk.elevenk.proxysetter/.MainActivity -e ssid Swiggy_Mobile -e clear true";
        log.info("SETANDROIDPROXY ::: " + UNSETANDROIDPROXY );
        String out = viserion.execShellCommand(UNSETANDROIDPROXY);
    }

    public void throttleNetworkHelper () throws IOException {

        String ip = viserion.execShellCommand("ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}'").trim();
        //HTTP CLIENT
        CloseableHttpClient httpclient = HttpClients.createDefault();
        try {
            HttpHost target = new HttpHost("control.charles/throttling/activate?preset=3G", 8888, "http");
            HttpHost proxy = new HttpHost(ip, 8888, "http");

            RequestConfig config = RequestConfig.custom()
                    .setProxy(proxy)
                    .build();
            HttpGet request = new HttpGet("/");
            request.setConfig(config);

            log.info("Executing request " + request.getRequestLine() + " to " + target + " via " + proxy);

            CloseableHttpResponse response = httpclient.execute(target, request);
            try {
                log.info("----------------------------------------");
                log.info(response.getStatusLine());
                EntityUtils.consume(response.getEntity());
            } finally {
                response.close();
            }
        } finally {
            httpclient.close();
        }
    }*/

}
