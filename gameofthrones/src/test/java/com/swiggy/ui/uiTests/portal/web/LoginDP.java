package com.swiggy.ui.uiTests.portal.web;

import org.testng.annotations.DataProvider;

import com.swiggy.ui.page.classes.portalweb.PortalConstants;

public class LoginDP 
{
    @DataProvider(name = "validLogin")
    public Object[][] validLogin() {
        return new Object[][]{{PortalConstants.validusername, PortalConstants.validpassword}};
    }
    
    @DataProvider(name = "inValidPasswordLogin")
    public Object[][] inValidPasswordLogin() {
        return new Object[][]{{PortalConstants.validusername, PortalConstants.invalidPassword}};
    }
    
    @DataProvider(name = "inValidPhoneNo")
    public Object[][] inValidUserLogin() 
    {
        return new Object[][]{{PortalConstants.invalphno, PortalConstants.validpassword}};
    }
    
    
    


}
