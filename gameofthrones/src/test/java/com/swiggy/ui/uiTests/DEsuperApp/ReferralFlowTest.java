package com.swiggy.ui.uiTests.DEsuperApp;

import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.screen.DeAppReactNative.*;
import com.swiggy.ui.utils.AndroidUtil;

import java.util.HashMap;

public class ReferralFlowTest {

    public AppiumDriver driver;
    InitializeUI inti;
    LoginPageDERN loginPage;
    HomePageDERN homePage;
    LeftNavDERN leftNav;
    ProfilePageDERN profilePage;
    InvitePageDERN invitePageDERN;
    AutoassignHelper hepauto;
    DeliveryServiceHelper helpdel = new DeliveryServiceHelper();


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"216"}};
    }

    @BeforeClass
    public void setUptest() {
        inti = new InitializeUI();
        loginPage = new LoginPageDERN(inti);
        homePage = new HomePageDERN(inti);
        leftNav = new LeftNavDERN(inti);
        profilePage = new ProfilePageDERN(inti);
        invitePageDERN=new InvitePageDERN(inti);
        driver = inti.getAppiumDriver();
    }

    @Test(dataProvider = "LoginData",priority = 1)
    public void verifyInviteOptionInLeftNav(String deId){
        String deotp=null;
        loginPage.clickOnAllowPermission();
        loginPage.clickOnAllowPermission();
        loginPage.enterDEId(deId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        loginPage.clickOnContinueButton();
        loginPage.clickOnContinueButton();
        loginPage.enterOtp("547698");
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        loginPage.clickOnLoginButton();
        loginPage.clickOnLoginButton();
        AndroidUtil.sleep(5000);
        homePage.openLeftNav();
        Assert.assertTrue(leftNav.isInviteFriendOptionDisplay());
    }
    @Test(dataProvider = "LoginData",priority = 2,enabled = false)
    public void verifyOpenInviteFriend(String deId){
        String deotp=null;
        leftNav.openInviteFriend();
        Assert.assertEquals("Invite your friends to join Swiggy",invitePageDERN.getReferralScreenTitle());
        Assert.assertEquals("Test_city1",invitePageDERN.getReferralScreenSubTitle());
    }
    @Test(dataProvider = "LoginData",priority = 3,enabled = false)
    public void verifyInvite(String deId){
       invitePageDERN.clickOnInviteNow();
       invitePageDERN.enterUserName("test");
       invitePageDERN.enterContactNumber("ASWERWFSFSF");
       AndroidUtil.pressBackKey(driver);
       invitePageDERN.clickOnInviteNow();
       Assert.assertTrue(invitePageDERN.isInviteNowButtonDisplaying());
    }
    @Test(dataProvider = "LoginData",priority = 4,enabled = false)
    public void verifyInviteForOtherCity(String deId){
        AndroidUtil.pressBackKey(driver);
        invitePageDERN.clickOnInviteNow();
        invitePageDERN.enterUserName("test");
        invitePageDERN.enterContactNumber("ASWERWFSFSF");
        invitePageDERN.selectCity(1);
        invitePageDERN.clickOnInviteButton();
        Assert.assertTrue(invitePageDERN.isInviteNowButtonDisplaying());

    }
    @Test(dataProvider = "LoginData",priority = 5)
    public void verifyInviteScreenSelectContactDenyPermission(String deId){
        leftNav.openInviteFriend();
        invitePageDERN.clickOnInviteNow();
        invitePageDERN.enterUserName("test");
        AndroidUtil.sleep();
        invitePageDERN.clickOnContactListButton();
        AndroidUtil.sleep();
        loginPage.clickOnDenyPermission();

    }
    @Test(dataProvider = "LoginData",priority = 6)
    public void verifyInviteScreenSelectContactAllowPermission(String deId){
        AndroidUtil.pressBackKey(driver);
        invitePageDERN.clickOnInviteNow();
        invitePageDERN.enterUserName("test");
        invitePageDERN.clickOnContactListButton();
        loginPage.clickOnAllowPermission();
    }
    @Test(dataProvider = "LoginData",priority = 7)
    public void verifyInviteAlreadyInvitedFried(String deId){
        AndroidUtil.pressBackKey(driver);
        invitePageDERN.clickOnInviteNow();
        invitePageDERN.enterUserName("test");
        invitePageDERN.enterContactNumber("ASWERWFSFSF");
        AndroidUtil.pressBackKey(driver);
        invitePageDERN.clickOnInviteButton();

    }
    @Test(dataProvider = "LoginData",priority = 7)
    public void verifyRules(String deId){
        AndroidUtil.pressBackKey(driver);
        invitePageDERN.clickOnInviteNow();
        Assert.assertEquals("RULES AND CONDITIONS",invitePageDERN.getRulesSectionTitles());
        HashMap hm=invitePageDERN.getRules();
        Assert.assertEquals("You should be working with Swiggy at the time of referral payment",hm.get("Rule1"));
        Assert.assertEquals("Your friend should be a new lead, not among existing leads",hm.get("Rule2"));
    }
    @Test(dataProvider = "LoginData",priority = 8)
    public void referralDetails(String deId){
        AndroidUtil.pressBackKey(driver);
        Assert.assertEquals("test Referral",invitePageDERN.getFirstReferralApplicantName());
        Assert.assertEquals("PaNuWcKSpQkVs+Mgz4HrXJkVm2w==",invitePageDERN.getFirstReferralApplicantMobileNumber());
        Assert.assertEquals("2018-03-25 15:22:24",invitePageDERN.getFirstReferralApplicantInvitationTime());
        Assert.assertEquals("In-progress",invitePageDERN.getFirstReferralApplicantInvitationStatus());
    }
    @Test(dataProvider = "LoginData",priority = 9)
    public void verifyOpenReferralDetail(String deId) {
        String applicantName = invitePageDERN.getFirstReferralApplicantName();
        invitePageDERN.clickOnFirstReferral();
        Assert.assertEquals("test Referral",invitePageDERN.getApplicantNameFromDetaiLView());
        Assert.assertEquals("PaNuWcKSpQkVs+Mgz4HrXJkVm2w==",invitePageDERN.getApplicantMobileNumberFromDetaiLView());
        Assert.assertEquals("In-progress",invitePageDERN.getInvitationStatusFromDetailView());
        Assert.assertEquals("2",invitePageDERN.getApplicantInvitationId());
        System.out.println(invitePageDERN.getReferralText());

    }

}
