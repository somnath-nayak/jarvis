package com.swiggy.ui.uiTests.android.regressionSuite;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.rng.pojo.Menu;

import antlr.Utils;
import framework.gameofthrones.Aegon.AppiumServerJava;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.TouchAction;

import org.aspectj.weaver.ast.And;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pojoClasses.GetAddressPojo;
import scopt.Check;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.StfHelper;
import com.swiggy.ui.pojoClasses.*;

import org.openqa.selenium.By;
import org.openqa.selenium.html5.*;
import java.text.MessageFormat;

public class DD_Regression {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    StfHelper stfHelper;
    Double lat1=12.932815;
    Double lng1=77.603578;

    Double lat2=27.834105;
    Double lng2=76.171977;

    Double lat3=19.229388800000002;
    Double lng3=72.8569977;
    String deviceId;
    
    DishDiscoveryPage dishDiscoveryPage;
    DishDiscovery dishDiscoveryObject;
    DisDiscoveryStoryBoard ddStoryBoard;
    


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{Constants.userName,Constants.password}};
    }

    @BeforeClass
    public void setUptest() {


        consumerApiHelper = new ConsumerApiHelper();
      
        dishDiscoveryObject=consumerApiHelper.getdd(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1));
          System.out.println(dishDiscoveryObject.getCollectionName().get(0).toLowerCase());
         System.out.println(dishDiscoveryObject.getCollectionSubName().get(0).toLowerCase());
	 
        
        inti = new InitializeUI();

        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        dishDiscoveryPage=new DishDiscoveryPage(inti);
    }
    
   
    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"DishDiscovery"},description="validate DishDiscovery collection on home page")
    public void verifyDishDiscoveryCollectionOnList(String username,String password,ITestContext context) {
    	SoftAssert softAssert = new SoftAssert();
    
    	
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        
        home.verticalScroll();
        context.setAttribute("storyTitle",home.getStoryCollectionItem(0).toLowerCase());
        System.out.println(context.getAttribute("storyTitle").toString());
        softAssert.assertEquals(dishDiscoveryObject.getHeader().toLowerCase(),home.getStoryCollectionHeader().toLowerCase());
        softAssert.assertEquals(dishDiscoveryObject.getDescription().toLowerCase(),home.getStoryCollectionSubtitle().toLowerCase());
        softAssert.assertEquals(dishDiscoveryObject.getCollectionName().get(0).toLowerCase(),home.getStoryCollectionItem(1).toLowerCase());
        softAssert.assertEquals(dishDiscoveryObject.getCollectionSubName().get(0).toLowerCase(),home.getStoryCollectionItemSubTitle(1).toLowerCase());
        softAssert.assertAll();
    }
    @Test(dataProvider = "LoginData", priority = 2, enabled = true,groups={"DishDiscovery"},description="verify DishDiscovery collection landing page")
    public void verifyStoryCollectionLandingPage(String username,String password,ITestContext context) {
        ddStoryBoard=consumerApiHelper.getStoryBoardForDishDicovery("9886472081","qwerty",dishDiscoveryObject.getCollectionId().get(0),Double.toString(Constants.lat1),Double.toString(Constants.lng1),dishDiscoveryObject.getParentCollectionId().get(0));

        home.openStoryCollection(0);
        AndroidUtil.sleep(10000);

        System.out.println(dishDiscoveryPage.getStoryTitle());
        System.out.println(dishDiscoveryPage.getStoryDescription());
        Assert.assertEquals(dishDiscoveryPage.getStoryTitle(),context.getAttribute("storyTitle"));
        Assert.assertEquals(dishDiscoveryPage.getStoryDescription().toLowerCase(),ddStoryBoard.getDescriptionStoryStartCard().toLowerCase());



    }
    @Test(dataProvider = "LoginData", priority = 3, enabled = true,groups={"DishDiscovery"},description="verify first collection ")
    public void verifyFirstStoryCollectionPage(String username,String password,ITestContext context) {
        dishDiscoveryPage.scrollHorizontal();
        AndroidUtil.sleep();
        Assert.assertEquals(dishDiscoveryPage.getRestaurantName(), ddStoryBoard.getRestaurantName().get(0));
        Assert.assertEquals(dishDiscoveryPage.getTitleBar(), ddStoryBoard.getHeaderStoryStartCard());
        Assert.assertEquals(dishDiscoveryPage.getRestaurantAddress().toLowerCase(), ddStoryBoard.getLocality().get(0).toLowerCase()+", "+ddStoryBoard.getArea().get(0).toLowerCase());
        Assert.assertEquals(dishDiscoveryPage.getOfferText().toLowerCase(), ddStoryBoard.getTd().get(0).toLowerCase());
        Assert.assertEquals(dishDiscoveryPage.getItemName(0).toLowerCase(), ddStoryBoard.getFirstItem().get(0).toLowerCase());
        System.out.println(dishDiscoveryPage.getItemName(0));
        context.setAttribute("restaurantName",dishDiscoveryPage.getRestaurantName());
        context.setAttribute("itemName",dishDiscoveryPage.getItemName(0).toString().trim());
        dishDiscoveryPage.clickonAddToCartButton(0);

        //TODO -view cart button
    }
    @Test(dataProvider = "LoginData", priority = 4, enabled = true,groups={"DishDiscovery"},description="verifiy click on full menu from collection")
    public void clickonViewFullMenu(String username,String password,ITestContext context) {
       dishDiscoveryPage.clickonViewFullMenu();
       AndroidUtil.sleep(5000);
       restaurantPage.removeCoachmark();
       //Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),context.getAttribute("restaurantName").toString().toLowerCase());
       //Assert.assertEquals(restaurantPage.getDishDiscoveryTitle().toLowerCase(),context.getAttribute("storyTitle").toString().toLowerCase());
       restaurantPage.openSearch();
       String itemName=context.getAttribute("itemName").toString();
        restaurantPage.searchInRestaurant(itemName.substring(2,itemName.length()).toString().trim());
        AndroidUtil.hideKeyboard(driver);
        Assert.assertEquals(checkoutPage.getItemCount(),1);
        restaurantPage.pressBackFromSearchScreen();
        AndroidUtil.sleep();
        restaurantPage.clickOnBackButtonForDishDiscovery();
        Assert.assertEquals(dishDiscoveryPage.getRestaurantName(), context.getAttribute("restaurantName").toString());


    }
    @Test(dataProvider = "LoginData", priority = 5, enabled = true,groups={"DishDiscovery"},description="verify clear cart dialog on collection ")
    public void verifyClearCartDialog(String username,String password,ITestContext context) {
        String firstrestaurantName=context.getAttribute("restaurantName").toString();
        dishDiscoveryPage.scrollHorizontal();
        AndroidUtil.sleep(5000);
        String secondRestaurantname=dishDiscoveryPage.getRestaurantName();
        dishDiscoveryPage.clickonAddToCartButton(0);
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle().toLowerCase(),Constants.hm.get("REPLACECARTITME_HEADER_"+inti.platformnew.toString().toUpperCase()).toLowerCase());
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(), MessageFormat.format(Constants.hm.get("REPLACECARTITEM_SUB_"+inti.platformnew.toString().toUpperCase()),firstrestaurantName, secondRestaurantname).toLowerCase());
        restaurantPage.clickonReplaceCartItemDialogYes();
        restaurantPage.clickOnAddItemCustimization();
        }
    
    @Test(dataProvider = "LoginData", priority = 6, enabled = true,groups={"DishDiscovery"},description="validate close button ")
    public void validateCloseButton(String username,String password,ITestContext context) {
        dishDiscoveryPage.clickonCloseCollectionbutton();
        System.out.println(home.getStoryCollectionItem(0));

        Assert.assertEquals(home.getStoryCollectionItem(0).toLowerCase(),context.getAttribute("storyTitle").toString().toLowerCase());
        }
    @Test(dataProvider = "LoginData", priority = 7, enabled = true,groups={"DishDiscovery"},description="validate story end point page")
    public void verifyStoryEndPoint(String username,String password,ITestContext context) {
        home.openStoryCollection(0);
        AndroidUtil.sleep(5000);
        dishDiscoveryPage.scrollHorizontal(7);

        Assert.assertEquals(dishDiscoveryPage.getStoryEndPointHeader().toLowerCase(),ddStoryBoard.getHeaderStoryEndCard().toLowerCase());
        /*Assert.assertEquals(dishDiscoveryPage.getStoryEndPointHeader().toLowerCase(),ddStoryBoard.getDescriptionStoryEndCard().toLowerCase());
        Assert.assertEquals(home.getStoryCollectionItem(0).toLowerCase(),ddStoryBoard.getCardsHeaderStoryEndCard().get(0).toLowerCase());
        Assert.assertEquals(home.getStoryCollectionItemSubTitle(0).toLowerCase(),ddStoryBoard.getCardsstoryDepthStoryEndCard().get(0).toLowerCase());*/
        dishDiscoveryPage.clickonCloseCollectionbutton();

    }
    @Test(dataProvider = "LoginData", priority = 8, enabled = true,groups={"DishDiscovery"},description="validate place order from collection ")
    public void addItemAndOpenCart(String username,String password,ITestContext context) {
        home.openStoryCollection(0);
        AndroidUtil.sleep(5000);
        dishDiscoveryPage.scrollHorizontal();
        AndroidUtil.sleep(5000);
        String itemName=dishDiscoveryPage.getItemName(0);
        String restaurantName=dishDiscoveryPage.getRestaurantName().toString();
        dishDiscoveryPage.clickonAddToCartButton(0);
        restaurantPage.clickonReplaceCartItemDialogYes();
        restaurantPage.clickOnAddItemCustimization();
        restaurantPage.openCartFromRestaurantPage();
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toString().toLowerCase(),restaurantName.toLowerCase());
        Assert.assertEquals(checkoutPage.getItemNameFromCart(0).toString().toLowerCase(),itemName.toLowerCase().substring(2,itemName.length()).trim());
        checkoutPage.proceedToPay();
        paymentPage.clickonCOD();
        AndroidUtil.sleep();
        String orderId=trackPage.getOrderId();
        System.out.println(trackPage.getOrderAmount());
    //    consumerApiHelper.cancelOrder(username,password,orderId);
        trackPage.getAllText();
    }
    @Test(dataProvider = "LoginData", priority = 9, enabled = true,groups={"DishDiscovery"},description="verify collection on explore option")
    public void verifyCollectionOnExploreScreen(String username,String password,ITestContext context) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        driver.closeApp();
        driver.launchApp();
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        AndroidUtil.sleep(10000);
        home.openExploreTab();
        AndroidUtil.sleep(5000);
        AndroidUtil.hideKeyboard(driver);
        //Assert.assertEquals(explorePage.getCollectionTitleText().toLowerCase(),"Swiggy Food Guide".toLowerCase());

    }
    @Test(dataProvider = "LoginData", priority = 10, enabled = true,groups={"DishDiscovery"},description="verify open collection from explore screen")
    public void openCollectionFromExploreScreen(String username,String password,ITestContext context) {
        home.openStoryCollection(0);
        AndroidUtil.sleep(5000);
        dishDiscoveryPage.scrollHorizontal();
        AndroidUtil.sleep(5000);
        String itemName=dishDiscoveryPage.getItemName(0);
        String restaurantName=dishDiscoveryPage.getRestaurantName().toString();
        dishDiscoveryPage.clickonAddToCartButton(0);
        dishDiscoveryPage.clickonCloseCollectionbutton();
        
    }
    @Test(dataProvider = "LoginData", priority =11, enabled = true,groups={"DishDiscovery"},description="verify search on explore screen")
    public void searchOnExploreScreen(String username,String password,ITestContext context) {
        explorePage.doSearch("Test Restaurant Automation2");
        AndroidUtil.hideKeyboard(driver);
        AndroidUtil.sleep();
        System.out.println(explorePage.dd_titleText.getText());
        Assert.assertFalse(explorePage.checkElementDisplay(explorePage.dd_titleText));
        explorePage.clearSearch();
        Assert.assertTrue(explorePage.checkElementDisplay(explorePage.dd_titleText));




    }








}




