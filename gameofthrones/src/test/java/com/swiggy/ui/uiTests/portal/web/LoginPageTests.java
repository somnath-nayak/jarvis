package com.swiggy.ui.uiTests.portal.web;
import java.util.concurrent.TimeUnit;

import org.apache.maven.wagon.providers.ssh.knownhost.UnknownHostException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.portalweb.LoginPage;
import com.swiggy.ui.page.classes.portalweb.PortalConstants;
import com.swiggy.ui.page.classes.portalweb.SignUpPage;
import com.swiggy.ui.page.classes.portalweb.HomePage;
import com.swiggy.ui.uiTests.portal.web.LoginDP;
import com.swiggy.ui.page.classes.portalweb.ListingPage;

public class LoginPageTests extends LoginDP

{
	
    public WebDriver driver;
    InitializeUI inti;
    HomePage homepage;
    LoginPage loginpage;
    ListingPage listingpage;
    SignUpPage signUppage;
    
    
	
    @BeforeClass
    public void setUptest() throws UnknownHostException
    {
        inti = new InitializeUI();
        driver = inti.getWebdriver();
        driver.manage().window().maximize();
        driver.get(PortalConstants.portal_url);
        homepage = new HomePage(inti);
        loginpage = new LoginPage(inti);
        listingpage = new ListingPage(inti);
        signUppage = new SignUpPage(inti);
        

    }
    
    /*Verify that all elements are present on the  Login page*/
    
    @Test(groups={"sanity","regression"},description="Verify all elements are present on the login Page",priority=0)
    public void verifyCreateAccountLink() throws InterruptedException
    {	
    	SoftAssert softAssertion = new SoftAssert();
    	homepage.clickOnLogin();
    	
    	softAssertion.assertEquals(loginpage.getLoginPage_Text(), "Login","Page title is not proper");
    	softAssertion.assertTrue(loginpage.closeBtn_Present(),"Close button is not present On login page");
    	softAssertion.assertTrue(loginpage.createAccounttLink_Present(),"Create account link is not present On login page");
    	softAssertion.assertTrue(loginpage.mobileField_Present(),"Text phone field is not present On login page");
    	softAssertion.assertTrue(loginpage.passwordField_Present(),"Text password field is not present On login page");
    	softAssertion.assertTrue(loginpage.forgotPassword_Present(),"Login Button is not present On login page");
    	softAssertion.assertTrue(loginpage.submit_Present(),"Forgot password link is not present On login page");
    	softAssertion.assertAll();
		
    }
    
    /*Verify that create an account link is click able*/
    
    @Test(groups={"regression"},description="Verify that create an account link is click able",priority=1)
    public void verifySignUpLinkClickable()
    {
    	loginpage.clickOnCreateAccountLink();
    	Assert.assertEquals(signUppage.getSignUppage_Text(), "Sign up","Sign Up Page title is not proper");
		signUppage.clickOnClose();
    }
    
    
    /*Verify forget password link is click able*/
    
    @Test(groups={"sanity","regression"}, priority = 2,description="Verify forget password link is clickable")
    public void verifyForgotPasswordLink()
    {
      
      driver.navigate().to(PortalConstants.portal_url);
      homepage.clickOnLogin();
      loginpage.enterMobileNo(PortalConstants.validusername);
  	  loginpage.clickForgotPwd_link();
	  
	  Assert.assertEquals(loginpage.getForogotpasswordPageText(),"Forgot password","Not landing on forget password page");
	  
  		  
    }
    
 
    /*Verify that proper error message is coming when user enters wrong password*/
    
    @Test(dataProvider = "inValidPasswordLogin", groups={"regression"},description ="When user enters invalid password for valid user",priority = 3)
    public void verifyLoginWith_inValidPasswordLogin(String user, String password) throws InterruptedException
    {
      driver.navigate().to(PortalConstants.portal_url);
      homepage.clickOnLogin();
      loginpage.enterMobileNo(user);
  	  loginpage.enterPassword(password);
  	  loginpage.clickOnLogin();
  	  Thread.sleep(1000);
  	  Assert.assertEquals(loginpage.getErrorForInvalidPassword(), "Invalid Password","Error message not proper");
  	  
    }
	

    /*Verify that proper error message is coming when user enters invalid phone number*/
	
    
    @Test(dataProvider = "inValidPhoneNo",groups={"regression"}, priority = 4,description="When user enters invalid Phone no")
    public void verifyLoginWith_InvalidPhNo(String user, String password) throws InterruptedException
    {
    	
    	
      driver.navigate().to(PortalConstants.portal_url);
      homepage.clickOnLogin();
      loginpage.enterMobileNo(user);
  	  loginpage.enterPassword(password);
  	  loginpage.clickOnLogin();
  	  loginpage.wait(driver, 10, TimeUnit.SECONDS);
  	  Assert.assertEquals(loginpage.getErrorForInvalidPhNo(),"Enter your phone number","Error message not proper");
  	  
    }
    
    /*Verifying the error messages when user leaves input  field blank*/
    
    @Test(groups={"regression"}, priority = 5,description="Verify error msgs when user Not Entering any input into the input field")
    public void verifyErrosMsgs() throws InterruptedException
    {
      SoftAssert softAssertion = new SoftAssert();
      driver.navigate().to(PortalConstants.portal_url);
      homepage.clickOnLogin();
      loginpage.clearPhnNOField();
   	  loginpage.clearPwdField();
      loginpage.clickOnLogin();
      loginpage.wait(driver, 10, TimeUnit.SECONDS);
      
  	  softAssertion.assertEquals(loginpage.getErrorForInvalidPhNo(),"Enter your phone number","Error message not proper" );
  	  softAssertion.assertEquals(loginpage.getErrorForInvalidPassword(), "Password should be min 6 chars","Error message not proper");
  	  softAssertion.assertAll();	  
    }
    
    
    
    /*Verify login when user logins with valid credentials*/
    
	@Test(dataProvider = "validLogin",groups={"regression","sanity"}, priority = 6, enabled = true, description="Verfiy login when user logins with valid credentials")
	public void verifyLoginWithValidUser(String user, String password) throws InterruptedException
	{
	
	  SoftAssert softAssertion = new SoftAssert();
	  driver.navigate().to(PortalConstants.portal_url);
      homepage.clickOnLogin();
	  loginpage.enterMobileNo(user);
	  loginpage.enterPassword(password);
	  loginpage.clickOnLogin();
	  Thread.sleep(1000);
	  softAssertion.assertTrue(listingpage.checkCart_Present(), "User is not landing on listing page");
	  softAssertion.assertEquals(listingpage.getUserName(), PortalConstants.name,"User Name does not match with the loged in id");
	  softAssertion.assertAll();
	  
		  
	}
	


    
    @AfterClass
    public void teardown()	
    {
    	
    	driver.close();

    }
}
