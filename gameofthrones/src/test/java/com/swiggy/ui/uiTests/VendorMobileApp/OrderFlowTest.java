package com.swiggy.ui.uiTests.VendorMobileApp;

import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.vms.helper.PrepTimeServiceHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.page.classes.vendor.dashboard.VendorPastOrders;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.TrackPage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorHomePage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorLoginPage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorOrderDetailPage;
import com.swiggy.ui.screen.vendorAndroidApp.VendorSettingPage;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Vendorappconstant;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kiran.j on 6/4/18.
 */
public class OrderFlowTest extends OrderFlowDp {

    public AppiumDriver driver;
    InitializeUI inti;
    VendorHomePage vendorHomePage;
    VendorLoginPage vendorLoginPage;
    VendorSettingPage vendorSettingPage;
    VendorOrderDetailPage vendorOrderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    VendorPastOrders vendorPastOrders;
    TrackPage trackPage;
    VmsHelper vmsHelper = new VmsHelper();
    PrepTimeServiceHelper prepTimeServiceHelper = new PrepTimeServiceHelper();
    List<String> orderIds = new ArrayList<>();


    @DataProvider(name = "restaurantLoginData")
    public Object[][] restaurantLoginData() {
        return new Object[][]{{Vendorappconstant.RESTAURANTID, Vendorappconstant.PASSWORD}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper=new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        vendorHomePage = new VendorHomePage(inti);
        vendorLoginPage = new VendorLoginPage(inti);
        vendorSettingPage = new VendorSettingPage(inti);
        vendorOrderDetailPage=new VendorOrderDetailPage(inti);
        vendorPastOrders = new VendorPastOrders(inti);
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void verifyNewOrder(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        softAssert.assertTrue(vendorHomePage.isNewOrderReceived(), "Order Not Received");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void verifyOrderDetails(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        softAssert.assertTrue(vendorHomePage.isNewOrderReceived(), "Order Not Received");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void awaitingCall(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.tapOnHelp().acceptCallRequest();
        softAssert.assertTrue(vendorOrderDetailPage.isAwaitingCallDisplayed(),"Awaiting Call Action Failed");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void confirmOrder(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder(orderId);
        String order_id = vendorOrderDetailPage.getOrderNumber();
        System.out.println("order ids are actual:" + orderId +"confirmed order:" + order_id);
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.dismissOrderView().isHomePage(), "Unable to Confirm Order");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void trackDE(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }
        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        //ToDo: working on dynamic order verification
        vendorHomePage.clickOnNewOrder(orderId);
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(), OrderStatus.ASSIGNED);
        AndroidUtil.sleep(5000);
        consumerApiHelper.changeOrderStatus(iTestContext.getAttribute("orderId").toString(),OrderStatus.CONFIRMED);
        AndroidUtil.sleep(5000);
        vendorHomePage.openSearch();
        vendorHomePage.searchOrder(orderId);
        softAssert.assertTrue(vendorHomePage.trackDE(), "DE is Not Assigned");
        //go to order detail page
        vendorHomePage.clickOnFirstOrder(0);
        softAssert.assertTrue(vendorOrderDetailPage.isDeliveryExecutiveDisplay() && vendorOrderDetailPage.isMapDisplayed(), "DE is Not Assigned");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void isFoodReady(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderPopUpDisplay());
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder(orderId);
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        vendorHomePage.openSearch().searchOrder(orderId);
        softAssert.assertTrue(vendorHomePage.isPreparingOrder(), "Order Not Moved to Preparing Tab");
        softAssert.assertTrue(vendorHomePage.isFoodReady(), "Food Ready Button Not Displayed");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void markReady(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderPopUpDisplay());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder(orderId);
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        vendorOrderDetailPage.increasePreprationTime(5);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        vendorHomePage.dismissOrderView();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        vendorHomePage.openSearch().searchOrder(orderId);
        softAssert.assertTrue(vendorHomePage.isPreparingOrder(), "Order Not Moved to Preparing Tab");
        softAssert.assertTrue(vendorHomePage.isFoodReady(), "Food Ready Button Not Displayed");
        vendorHomePage.clearSearch().pressBackSearch();
        AndroidUtil.sleep(5000);
        vendorHomePage.openSearch().searchOrder(orderId).makeOrderReady();
        AndroidUtil.sleep(5000);
        vendorHomePage.clearSearch().pressBackSearch().openSearch().searchOrder(orderId);
        softAssert.assertTrue(vendorHomePage.isOrderReady(), "Unable to Mark Order Ready");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void deliverOrder(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderPopUpDisplay());
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder(orderId);
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        vendorHomePage.openSearch();
        vendorHomePage.searchOrder(orderId);
        softAssert.assertTrue(vendorHomePage.isPreparingOrder(), "Order Not Moved to Preparing Tab");
        softAssert.assertTrue(vendorHomePage.isFoodReady(), "Food Ready Button Not Displayed");
        vendorHomePage.clearSearch().pressBackSearch();
        vendorHomePage.openSearch().searchOrder(orderId).makeOrderReady();
        softAssert.assertTrue(vendorHomePage.isOrderReady(), "Unable to Mark Order Ready");
        consumerApiHelper.changeOrderStatus(orderId, OrderStatus.ASSIGNED);
        consumerApiHelper.changeOrderStatus(orderId, OrderStatus.CONFIRMED);
        consumerApiHelper.changeOrderStatus(orderId, OrderStatus.ARRIVED);
        consumerApiHelper.changeOrderStatus(orderId,OrderStatus.PICKEDUP);
        consumerApiHelper.changeOrderStatus(orderId,OrderStatus.REACHED);
        consumerApiHelper.changeOrderStatus(orderId,OrderStatus.DELIVERED);
        AndroidUtil.sleep(5000);
        vendorHomePage.goToPastOrders();
        softAssert.assertTrue(vendorPastOrders.isPastOrderPage());
        vendorPastOrders.selectFirstOrder();
        softAssert.assertEquals(vendorPastOrders.getOrderId(), orderId, "Order is Not Delivered");
        softAssert.assertTrue(vendorPastOrders.isDelivered(), "Order is Not Delivered");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void cancelOrder(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderPopUpDisplay());
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        //cancel order
        EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
        edvoCartHelper.cancelOrder(orderId);
        vendorHomePage.goToPastOrders();
        softAssert.assertTrue(vendorPastOrders.isPastOrderPage());
        vendorPastOrders.selectFirstOrder();
        softAssert.assertEquals(vendorPastOrders.getOrderId(), orderId, "This is");
        softAssert.assertTrue(vendorPastOrders.isCancelled(), "Order is Not Cancelled");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void verifyGlobalPrepTime(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        String global_prep = vendorHomePage.getGlobalPrepTime();
        System.out.println("prep time is"+global_prep);
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        String prep_time = vendorOrderDetailPage.getGlobalPrepTime();
        System.out.println("prep time is"+prep_time);
        Assert.assertEquals(global_prep,prep_time,"Did Not Pick Prep Time");
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void verifyPrepTime(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        com.swiggy.ui.api.VmsHelper vmsHelper2 = new com.swiggy.ui.api.VmsHelper();
        vmsHelper2.deletePrepTimes();
        prepTimeServiceHelper.createDefaultSlot(restId);
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        int global_prep = Integer.parseInt(vendorHomePage.getGlobalPrepTime());
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        int prep_time = Integer.parseInt(vendorOrderDetailPage.getGlobalPrepTime());
        Assert.assertEquals(global_prep,prep_time,"Did Not Pick Prep Time");
        vendorOrderDetailPage.increasePreprationTime(5);
        Assert.assertEquals(global_prep+5, Integer.parseInt(vendorOrderDetailPage.getGlobalPrepTime()),"Edit Increase Prep time Not Working");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void selectAlternative(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        vendorOrderDetailPage.clickOnMarkOOS();
        vendorOrderDetailPage.clickOnButtonOOS();
        vendorOrderDetailPage.selectAlternativesForOOS();
        vendorOrderDetailPage.clickOnButtonDoneForOOS();
        vendorOrderDetailPage.clickOnSubmitAlternates();
        AndroidUtil.pressBackKey(driver);
        Assert.assertEquals(vendorHomePage.getOrderStatusFromHomeScreen(),Vendorappconstant.ORDERSTATUS_PENDING);
        orderIds.add(orderId);
        softAssert.assertAll();
    }

   @Test(dataProvider = "createOrder", groups = {"regression"}, description = "Verify new order")
    public void verifyDetails(String restId, String password,String orderId, ITestContext iTestContext) {
        SoftAssert softAssert = new SoftAssert();
        vendorLoginPage.performLogin(restId,password);
        vendorLoginPage.clickonLoginButton();
        AndroidUtil.sleep(5000);

        if(vendorHomePage.isGoToSettingDisplay()) {
            vendorHomePage.clickOnGoToSetting();
            AndroidUtil.sleep();
            vendorSettingPage.clickOnDisplayOrderSetting();
            AndroidUtil.pressBackKey(driver);
        }

        iTestContext.setAttribute("orderId",orderId);
        AndroidUtil.sleep();
        System.out.println(vendorHomePage.getNewOrderText());
        Assert.assertTrue(vendorHomePage.isNewOrderReceived(),"Order Not Received");
        vendorHomePage.clickOnNewOrder();
        String order_id = vendorOrderDetailPage.getOrderNumber();
        softAssert.assertTrue(vendorOrderDetailPage.verifyDetails(orderId),"Order Details Verification Failed");
        vendorOrderDetailPage.clickOnConfirmOrderButton();
        System.out.println("order is"+order_id);
        int prepTime=vendorOrderDetailPage.increasePreprationTime(5);
        iTestContext.setAttribute("prepTime",prepTime);
        vendorOrderDetailPage.clickOnDoneButton();
        vendorOrderDetailPage.returnToHomePage();
        softAssert.assertTrue(vendorHomePage.isHomePage(), "Unable to Confirm Order");
        orderIds.add(orderId);
        softAssert.assertAll();
    }

    @AfterMethod
    public void tearDown() {
        driver.closeApp();
        driver.launchApp();
        LOSHelper losHelper = new LOSHelper();
        while(orderIds.size() > 0) {
            losHelper.cancelOrder(orderIds.get(0), "false");
            orderIds.remove(0);
        }
    }

}
