package com.swiggy.ui.uiTests.android.regressionSuite;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.gargoylesoftware.htmlunit.javascript.host.Touch;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Address;
import com.swiggy.api.sf.rng.pojo.Menu;
import framework.gameofthrones.Aegon.AppiumServerJava;
import framework.gameofthrones.Aegon.InitializeUI;
import framework.gameofthrones.Baelish.Proxyserver;
import framework.gameofthrones.Tyrion.StfHelper;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.TouchAction;

import org.aspectj.weaver.ast.And;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pojoClasses.GetAddressPojo;
import scopt.Check;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.awt.*;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;

import framework.gameofthrones.JonSnow.Processor;
import com.swiggy.ui.pojoClasses.*;
import org.openqa.selenium.html5.*;
import java.text.MessageFormat;
import java.util.List;

public class AndroidRegression {


    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    StfHelper stfHelper;
    Double lat1 = 12.932815;
    Double lng1 = 77.603578;

    Double lat2 = 27.834105;
    Double lng2 = 76.171977;

    Double lat3 = 19.229388800000002;
    Double lng3 = 72.8569977;
    String deviceId;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{Constants.userName2, Constants.password2}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
       // Proxyserver mobProxyTest=new Proxyserver(9091);
       // mobProxyTest.startServer();

       // stfHelper=new StfHelper(Constants.STF_AUTH_TOKEN);
       // deviceId=stfHelper.connectDevice();
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI("deviceId");
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);


    }


   @Test(dataProvider = "LoginData", priority = 19, enabled = true, groups = {"regression"}, description = "verifySwiggyPopFlow")
   public void verifySwiggyPopFlow(String userName2, String password2) {
       Location location = new Location(Constants.lat1, Constants.lng1, 0);
       List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
       consumerApiHelper.deleteAllAddress(Constants.userName2,Constants.password2);
       consumerApiHelper.createNewAddress(Constants.userName2,Constants.password2,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"Home");
       driver.setLocation(location);
       launch.tapOnLogin();
       login.LogintoApp(userName2, password2);
     //  launch.setOrder_location();
       addressPage.clickonButtonAddMoreDetail();
       AndroidUtil.sleep(5000);

       addressPage.clickonSkipAddress();
       restaurantPage.clickonPreOrderTimeSlot();
       restaurantPage.setPreOrderTime();
       home.openSwiggyPop();
       swiggypopPage.clickOnFirstPOPItem();
       if (!popObj.isEmpty() && popObj.get(0).isOpened()) {
           swiggypopPage.clickonBackButton();
           AndroidUtil.sleep();
           Assert.assertEquals(restaurantPage.getPOPCartcleartTitle(), Constants.SWIGGYPOP_PRESSBACK_TITTLE);
           Assert.assertEquals(restaurantPage. getPOPCartcleartMessage(), Constants.SWIGGYPOP_PRESSBACK_MESSAGE);
           AndroidUtil.sleep();

           swiggypopPage.clickOnOkGotIt();
       }
       else{
           Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
       }
       swiggypopPage.clickOnFirstPOPItem();
       AndroidUtil.sleep();
       swiggypopPage.clickonSelectAddress();
       swiggypopPage.selecHomeAddress();
       swiggypopPage.clickonConfirmAddressAndProceed();
       swiggypopPage.clickonProceedToPay();
       AndroidUtil.sleep(5000);
       swiggypopPage.changePaymentMethod();
       AndroidUtil.sleep(5000);
       paymentPage.clickonCOD();

   }




    @Test(dataProvider = "LoginData", priority = 20, enabled = true,groups={"regression"},description="E2E flow For Normal cart ")
    public void validatePlaceOrderInApp(String userName2,String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(userName2,password2);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"Home");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.openFilter();
        home.offersAndMore("offers");
        //Assert.assertTrue(home.checkFilterApplied());
        //Assert.assertTrue(home.checkElementDisplay(home.btn_filterReset));
        //home.clickonClearFilter();
        // Assert.assertTrue(home.newApplyFilter.isEnabled());

        home.clickonApplyForNewFilter();
        AndroidUtil.sleep(10000);
        home.openFilter();
        home.offersAndMore("offers");
        home.clickonClearFilter();
        home.clickonApplyForNewFilter();
        AndroidUtil.sleep(10000);

        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null, 0);
        System.out.println("RestaurantName = " + al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        restaurantPage.removeCoachmark();
        restaurantPage.toggleToVegOnly();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
       // Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(userName2, password2);

        checkoutPage.increaseItemCount();
        if (checkoutPage.isRepeatCustomizeButtonDisplay()) {
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        checkoutPage.increaseItemCount();
        if (checkoutPage.isRepeatCustomizeButtonDisplay()) {
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        AndroidUtil.sleep(1000);
        checkoutPage.proceedToPay();
        /*paymentPage.clickonCOD();
        AndroidUtil.sleep(5000);
        Assert.assertEquals(trackPage.getOrderStatus(),"Order Received");
        String orderId=trackPage.getOrderId();
        System.out.println(trackPage.getOrderAmount());

        // consumerApiHelper.cancelOrder(username,password,orderId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        //Assert.assertEquals(trackPage.getOrdercancelText(),"Order Cancelled");
        // Assert.assertEquals(home.getOrderStatusFromHomePageCrouton().toLowerCase(),"Order Received".toLowerCase(),"");
        Assert.assertEquals(home.getOrderTrackStatusFromHomePageCrouton().toLowerCase(),"STATUS".toLowerCase(),"STATUS not found");
        home.clickOnOrderPlacedToolTip();
        AndroidUtil.sleep();
        Assert.assertEquals(trackPage.getOrderId(),orderId);
        //  consumerApiHelper.cancelOrder(username,password,orderId);
        AndroidUtil.sleep(10000);
        Assert.assertEquals(trackPage.getOrdercancelText(),"Order Cancelled","Cancel text failure");
*/
        //trackPage.getAllText();
    }


    @Test(dataProvider = "LoginData", priority = 18, enabled = true, groups = {"regression"}, description = "PreOrderFlow for logged in User")
    public void PreOrderFlow(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        // List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
       // consumerApiHelper.deleteAllAddress(userName2, password2);

        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2,password2);
       // launch.setOrder_location();
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertTrue(restaurantPage.checkElementDisplay(home.txt_preOrderHeader));
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        Assert.assertEquals(home.getPreorderDeliveryTimeText(0).toLowerCase(),"Now".toLowerCase());
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(1);
        String selectedTime=home.getPreorderDeliveryTimeText(1);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        Assert.assertEquals(home.getPreorderSlotTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.changeTimeSlotfromMenu();
        restaurantPage.setPreOrderTime();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        //checkoutPage.cartSelectAddress();
       // checkoutPage.selecHomeAddress();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();


        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);

        checkoutPage.proceedToPay();
        //paymentPage.clickonCOD();
        //trackPage.getAllText();
        //trackPage.viewOrderDetails();
        //trackPage.clickOnHelp();
        //trackPage.clickOnModifyOrder();

        //trackPage.clickOncancelPreOrder();

    }




    @Test(dataProvider = "LoginData", priority = 17, enabled = true,groups={"regression"},description="verify add other restaurantItem")
    public void verifyAddOtherRestaurantItem(String userName2,String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null,0);
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        String firstrestaurantName=restaurantPage.getRestaurantName();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat1),Double.toString(lng1),userName2,password2);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        restaurantPage.addItemtest();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        home.selectRestaurant(null,1);
        System.out.println("RestaurantName = "+al.get(1).getRestaurantName());
        AndroidUtil.sleep(10000);
        home.selectRestaurant(al.get(1).getRestaurantName(),1);
        String secondRestaurantname=restaurantPage.getRestaurantName();
        restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(1).getRestaurantId(),al.get(1).getUuid(),Double.toString(lat1),Double.toString(lng1),userName2,password2);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(1).toString());
        restaurantPage.addItemtest();
        restaurantPage.clickonAddToCartForReplaceItem();
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle().toLowerCase(),"Replace cart item?".toLowerCase());
       // Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(),MessageFormat.format("Your cart contains dishes from {0}. Do you want to discard the selection and add dishes from {1}?",firstrestaurantName).toLowerCase());
        restaurantPage.clickonReplaceCartItemDialogYes();
        restaurantPage.clickonCustomButtonForAddTo();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(),secondRestaurantname.toLowerCase());
        checkoutPage.proceedToPay();

    }


    @Test(dataProvider = "LoginData", priority = 16, enabled = true,groups={"regression"},description="IncreaseItemCount")
    public void IncreaseItemCount(String userName2,String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null, 0);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.decreaseItemCount();


    }


    @Test(dataProvider = "LoginData", priority = 15, enabled = true, groups = {"regression"}, description = "Verify ReOrder Flow")
    public void VerifyReOrderFlow(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        String restaurantName;
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        home.openAccountTab();
        if (accountPage.checkAnyPastOrderPresent()) {
            restaurantName = accountPage.getRestaurantNameFromMyOrder(0);
            AndroidUtil.sleep();
            accountPage.openViewOrderDetail(0);
            home.scrollDownOnPopPage();

            accountPage.clickReorderbutton();
            System.out.println(checkoutPage.getRestaurantNameFromCart());
            System.out.println(restaurantName);
           // Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(), restaurantName);

        } else {
            System.out.println("There are no old orders for this user");
        }
    }


    @Test(dataProvider = "LoginData", priority = 14, enabled = true, groups = {"regression"}, description = "E2E flow select carousel ")
    public void verifyE2EFlowFromCarousel(String userName2, String password2) {
        String restaurantName = null;
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        //addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        List<Menulist> lcarousel = consumerApiHelper.getCarouselList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        List<Menulist> lrestaurantList = null;
        Restaurantmenu restaurantMenuObject;

        String firstCarouselType = lcarousel.get(0).getType();
        if (lcarousel.get(0).getType().equalsIgnoreCase("collection")) {
            lrestaurantList = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1), lcarousel.get(0).getRestaurantId());
        } else {
            lrestaurantList = lcarousel;
        }
        home.clickOnCarouselAndOpenRestaurantFromCollection(0);
        AndroidUtil.sleep();
        if (firstCarouselType.equalsIgnoreCase("collection")) {
            AndroidUtil.sleep(5000);
            restaurantName = home.getRestaurantNameFromList(0);
            //Assert.assertEquals(home.getRestaurantNameFromList(0),lrestaurantList.get(0).getRestaurantName());
            home.selectRestaurant(null, 0);
            restaurantPage.removeCoachmark();
            Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantName.toLowerCase());


        } else {
          //  restaurantMenuObject = consumerApiHelper.getRestaurantMenu(lrestaurantList.get(0).getRestaurantId(), lrestaurantList.get(0).getUuid(), Double.toString(Constants.lat1), Double.toString(Constants.lng1), userName2, password2);
            restaurantPage.removeCoachmark();
         //   Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantMenuObject.getName().toLowerCase());

        }
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.decreaseItemCount();
        checkoutPage.clickOk();
        checkoutPage.clickOnContinoueButton();

        login.LogintoApp(userName2, password2);
        checkoutPage.clickOnProceedToPay();


    }


    @Test(dataProvider = "LoginData", priority = 13, enabled = true,groups={"regression"},description="Validate veg only icon on restaurant page")
    public void verifyVegOnlyIconOnRestaurantPage(String userName2,String password2) {
        Location location=new Location(lat1,lng1,0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.openFilter();
        home.offersAndMore("Pure Veg");
        home.clickonApplyForNewFilter();
        AndroidUtil.sleep(5000);
        home.selectRestaurant(null,0);
        Assert.assertTrue(restaurantPage.isPureVegIconDisplay());

    }

    @Test(dataProvider = "LoginData", priority = 12, enabled = true, groups = {"regression"}, description = "verify mark favourite ")
    public void verifyMarkFavourite(String userName2, String password2) {
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(userName2, password2);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        consumerApiHelper.createNewAddress(userName2, password2, Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        consumerApiHelper.removeFavs(userName2, password2, Double.toString(lat1), Double.toString(lng1));
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        restaurantPage.scrollDownOnPopPage();
        //home.clickOnSortNudge();
        home.selectRestaurant(null,0);
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        AndroidUtil.sleep(10000);
        String restaurantName = restaurantPage.getRestaurantName();
        restaurantPage.clickonFavIcon();
        restaurantPage.clickOnBackButton();
        home.openAccountTab();
        accountPage.openFavorities();
        swiggypopPage.scrollDownOnPopPage();

        System.out.println(home.getRestaurantNameFromList(0));
        swiggypopPage.scrollDownOnPopPage();
        Assert.assertEquals(home.getRestaurantNameFromList(0),restaurantName);

    }


    @Test(dataProvider = "LoginData", priority = 11, enabled = true,groups={"regression"},description="Verify Login scenario from Account tab and Validate Loggedin User")
    public void verifyLoginFromAccountTab(String userName2,String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonSkipAddress();
        AndroidUtil.sleep(1000);
        home.removeSwiggyPop();
        AndroidUtil.sleep(1000);
        home.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(userName2, password2);
        accountPage.getTextMobileNumber();
    }



   /* @Test(dataProvider = "LoginData", priority = 10, enabled = true,groups={"regression"},description="verify EDVO")
    public void verifyEDVOMeal(String userName2, String password2) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress("9036976038", "huawei123");
        consumerApiHelper.createNewAddress("9036976038", "huawei123", Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat1), Double.toString(lng1),"CUISINES:Pizzas");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        //Add location Manually
        home.clickOnEditLocation();
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        home.scrollDownOnPopPage();
        home.openFilter();
        // home.filterRestaurantByCuisinesForDominos("Pizzas");
        home.selectRestaurantNew("Domino's Pizza", 0);
        // String firstrestaurantName = restaurantPage.getRestaurantName();
        restaurantPage.openEDVOMeal(0);
        EDVOMeal edvoMeal = consumerApiHelper.getEDVOMealObject("23798", "1");
        //restaurantPage.validateEDVOLandingScreen(edvoMeal);
        // restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.validatePizzaScreen(edvoMeal, 0);
        String itemName_1 = restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.validatePizzaScreen(edvoMeal, 1);
        String itemName_2 = restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.clickOnViewCart();
        //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toString().toLowerCase(),"Domino's Pizza".toLowerCase());
        Assert.assertEquals(checkoutPage.getItemNameFromCart(0).toLowerCase(), edvoMeal.getTagText().toLowerCase());


    }*/


    @Test(dataProvider = "LoginData", priority = 9, enabled = true, groups = {"regression"}, description = "Verify Logout and Validate Logout Confirmation message")
    public void verifyLogOut(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null,0);
        AndroidUtil.sleep(5000);
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        restaurantPage.clickOnBackButtonFromCart();
        restaurantPage.clickOnBackButton();
        home.openAccountTab();
        AndroidUtil.sleep(5000);
        accountPage.clickonLogout();
        Assert.assertEquals(accountPage.getLogoutConfirmationMessage().toLowerCase(),Constants.LOGOUT_MESSAGE.toLowerCase());
        accountPage.clickCancelOnLogoutDialog();
        Assert.assertTrue(accountPage.checkLogoutButtonDisplay());
        accountPage.clickonLogout();
        AndroidUtil.sleep(1000);
        accountPage.clickLogoutOnDialog();
        Assert.assertTrue(accountPage.checkLoginButtonDisplay());
        home.openCartTab();
        Assert.assertEquals(checkoutPage.getEmptyCartTitle(),Constants.EMPTYCART_TITLE);

    }


    @Test(dataProvider = "LoginData", priority = 8, enabled = true, groups = {"regression"}, description = "verify add new address,edit & delete")
    public void verifyAddNewAddress(String userName2, String password2) {
        consumerApiHelper.deleteAllAddressExceptHomeANDWORK(userName2, password2);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        home.openAccountTab();
       /* accountPage.updateMobileNumber("1123123122");
        driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.pressKeyCode(AndroidKeyCode.BACK);
        accountPage.updateEmailAddress("newEmail@gmail.com");
        driver.pressKeyCode(AndroidKeyCode.BACK);*/
        accountPage.openManageAddress();
        accountPage.clickOnBtnAddNewAddress();
        addressPage.enterNewAddress("other", "ATestOrder");
        GetAddressPojo addressObject = consumerApiHelper.getAllAddresS("9950611829", "qwerty123");
        System.out.println(addressObject.getData().getAddresses());
       // Assert.assertTrue(addressObject.getData().getAddresses().toString().contains("ATestOrder"));
        addressPage.editEnteredAddress();
        addressPage.clickonSaveAddress();
        addressPage.deleteEnteredAddress();
    }




    @Test(dataProvider = "LoginData", priority = 7, enabled = true, groups = {"regression"}, description = "verify apply coupon from cart")
    public void couponsVerification(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(userName2, password2);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        consumerApiHelper.createNewAddress(userName2, password2, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        AndroidUtil.sleep(10000);
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null, 0);
        restaurantPage.removeCoachmark();
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), userName2, password2);
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.increaseItemCount();
        if (checkoutPage.isRepeatCustomizeButtonDisplay()) {
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        checkoutPage.increaseItemCount();
        if (checkoutPage.isRepeatCustomizeButtonDisplay()) {
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        swiggypopPage.scrollDownOnPopPage();
        checkoutPage.applyCoupon("25HDFC");
       // Assert.assertEquals(checkoutPage.getCouponAppliedDialogTitle().toLowerCase(),"Coupon Applied".toLowerCase());
        checkoutPage.clickOnOK();
        checkoutPage.proceedToPay();
    }

    @Test(dataProvider = "LoginData", priority = 6, enabled = true,groups={"regression"},description="verify sort restaurant list based on Delivery time ")
    public void verifySorting(String userName2,String password2) {
        Location location=new Location(lat1,lng1,0);
        consumerApiHelper.deleteAllAddress(userName2,password2);
        consumerApiHelper.createNewAddress(userName2,password2,Double.toString(lat1),Double.toString(lng1),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantListBasedOnSort(userName2,password2,Double.toString(lat1),Double.toString(lng1),"DELIVERY_TIME");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();
        home.openFilter();
        home.clickonSortRestaurantBySLA();
        home.clickonApplyForNewFilter();
     //   Assert.assertEquals(home.getRestaurantNameFromList(0).toLowerCase(),al.get(0).getRestaurantName().toLowerCase());
    }

    @Test(dataProvider = "LoginData", priority = 5, enabled = true, groups = {"regression"}, description = "Validate Veg toggle option on restaurantPage")
    public void verifyVegToggleInRestaurantPage(String userName2, String password2) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), userName2, password2);
        String nonVegItem = consumerApiHelper.getFirstNonVegItem(restaurantMenuObject);
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(userName2, password2);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        consumerApiHelper.createNewAddress(userName2, password2, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        AndroidUtil.sleep(10000);
        home.removeSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();

        home.selectRestaurant(null, 0);
        restaurantPage.removeCoachmark();
        restaurantPage.toggleToVegOnly();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant(nonVegItem);
        Assert.assertNotEquals(restaurantPage.getRestaurantName(),nonVegItem);
    }


    @Test(dataProvider = "LoginData", priority = 4, enabled = true, groups = {"regression"}, description = "Validate search on Menu Page")
    public void verifySearchOnRestaurantPage(String userName2, String password2) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), userName2, password2);

        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        addressPage.clickonSkipAddress();
        AndroidUtil.sleep();
        swiggypopPage.scrollDownOnPopPage();
        home.selectRestaurant(null, 0);
        restaurantPage.removeCoachmark();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant(restaurantMenuObject.getItems().get(2));
       // Assert.assertEquals(restaurantPage.getItemName(0), restaurantMenuObject.getItems().get(2));
        // restaurantPage.searchInRestaurant(restaurantMenuObject.get);
        addressPage.hideKeyboard();
        restaurantPage.pressBackFromSearchScreen();
        AndroidUtil.sleep();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant("wrwr22342342#@$@#$@#$");
        Assert.assertNotEquals(restaurantPage.getRestaurantName(),"wrwr22342342#@$@#$@#$");

    }





    @Test(dataProvider = "LoginData", priority = 3, enabled = true,groups={"regression"},description="verifySwiggyPopWithoutLogin")
    public void verifySwiggyPopWithoutLogin(String userName2,String password2) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        consumerApiHelper.createNewAddress(userName2, password2, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        addressPage.clickonSkipAddress();
        home.openSwiggyPop();
        swiggypopPage.scrollDownOnPopPage();

        Assert.assertEquals(swiggypopPage.getpopHeader(),"Introducing Swiggy Pop");
        swiggypopPage.clickOnFirstPOPItem();
        if (popObj.get(0).isOpened()) {
            Assert.assertFalse(swiggypopPage.checkElementDisplay(swiggypopPage.txt_confirmAddressAndProceed, null));
            checkoutPage.clickOnContinoueButton();
            login.LogintoApp(userName2, password2);
           // Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),poplist.get(0).getName().trim());
            //Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+poplist.get(0).getRestaurantName());
            swiggypopPage.clickonConfirmAddressAndProceed();
            swiggypopPage.clickonProceedToPay();
        }
        else{
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }




    @Test(dataProvider = "LoginData", priority = 2, enabled = true,groups={"regression"},description=" verifyForNonServicableLocation ")
    public void verifyForNonServicableLocation(String userName2,String password2) {
        Location location=new Location(Constants.lat_nonServicable,Constants.lng_nonServicable,0);
        consumerApiHelper.deleteAllAddress(userName2, password2);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        addressPage.clickonSkipAddress();
        AndroidUtil.sleep(1000);
        home.removeSwiggyPop();
       // Assert.assertEquals(home.getNotServicableLocationText(),Constants.LOCATION_NOT_SERVICABLE_IOS);
        home.clickOnEditLocation();

    }


    @Test(dataProvider = "LoginData", priority = 1, enabled = true, groups = {"regression"}, description = "Sign up flow without referral")
    public void verifySignupWithoutReferral(String userName2, String password2) {
        launch.tapOnLogin();
        launch.clickonAllowButton();
        login.singupWithoutReferral();
    }
























    @AfterMethod
    public void tearDown() {
        driver.closeApp();
        driver.launchApp();
    }


    @AfterClass
    public void stopServer() {
        //inti.appiumServer.stopServer();
        stfHelper.disconnectDeviceFromSTF(deviceId);
    }
}