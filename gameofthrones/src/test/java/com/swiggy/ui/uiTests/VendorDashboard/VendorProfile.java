package com.swiggy.ui.uiTests.VendorDashboard;

import framework.gameofthrones.Aegon.InitializeUI;
import framework.gameofthrones.JonSnow.Processor;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.swiggy.api.erp.vms.helper.RMSHelper;

import com.swiggy.ui.page.classes.vendor.dashboard.*;
import com.swiggy.ui.uiTests.dashboard.dp.VendorDp;

public class VendorProfile extends VendorDp {

	private WebDriver driver;
	private static String URL = VmsConstants.dashboard_url;

	@BeforeMethod
	public void setUp() throws InterruptedException {
		// Thread.sleep(5000);
		InitializeUI gameofthrones = new InitializeUI();
		driver = gameofthrones.getWebdriver();
		driver.navigate().to(URL);

		HomePage homePage = new HomePage(driver);
		SettingsPage settingsPage = new SettingsPage(driver);
		Thread.sleep(2000);
		driver.navigate().refresh();
	}

	@Test(groups={"sanity","smoke","regression"},description="Account Tab Verification")
	public void accountVerifytab() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.accountClick();
		Assert.assertEquals(pro.getAccountName(), pro.expectedAccName());
		Assert.assertEquals(pro.getSwiggyId(), pro.expectedSwiggyId());
		Assert.assertEquals(pro.getOnboardingDate(), pro.expectedOnboardingDate());
		Assert.assertEquals(pro.getPhoneNum(), pro.expectedPhoneNum());
	}

	@Test(groups={"sanity","smoke","regression"},description="Finance Tab Verification")
	public void financeVerifytab() {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.financeClick();
		Assert.assertEquals(pro.getBankName(), pro.expectedBankName());
		Assert.assertEquals(pro.getAccNum(), pro.expectedAccNum());
		Assert.assertEquals(pro.getIfscCode(), pro.expectedIfscCode());
	}

	@Test(groups={"sanity","smoke","regression"},description="Profile Tab Verification")
	public void profileUsersClickVerify() {
		HomePage homePage = new HomePage(driver);
		SoftAssert softAssert = new SoftAssert();
		VmsHelper vmsHelper = new VmsHelper();
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		Assert.assertEquals(pro.manageUsersVerify(), "Manage Users");
		pro.inviteUserClick();
		Assert.assertEquals(pro.inviteUserPopUpVerify(), "Choose user role*");
	}

	@Test(groups={"sanity","smoke","regression"},description="Inviting existing User")
	public void profileExistingUsersVerify() {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.existingUser();
//		RMSHelper rmsHelper = new RMSHelper();
//		String accessToken = rmsHelper.getLoginToken(VmsConstants.restIdMulti, VmsConstants.prodPass);
//		rmsHelper.getRestUsers(accessToken, VmsConstants.selectOutlet);

	}

	@Test(groups={"sanity","smoke","regression"},description="Inviting new User as Owner")
	public void profileNewUsersAsOwner() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.inviteUserAsOwner();
		Thread.sleep(3000);
		/*Validating the Created User via API*/
		RMSHelper rmsHelper = new RMSHelper();
		String accessToken = rmsHelper.getLoginToken(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Processor processor = rmsHelper.getRestUsers(accessToken, VmsConstants.selectOutlet);
		Assert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.userDetails..[?(@.mobile_no==" + VmsConstants.newPhoneNum + ")]"));

	}
	@Test(groups={"sanity","smoke","regression"},description="Inviting new User as Restaurant Manager")
	public void profileNewUsersAsRM() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.inviteUserAsRestMgr();
		Thread.sleep(3000);
		/*Validating the Created User via API*/
		RMSHelper rmsHelper = new RMSHelper();
		String accessToken = rmsHelper.getLoginToken(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Processor processor = rmsHelper.getRestUsers(accessToken, VmsConstants.selectOutlet);
		Assert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.userDetails..[?(@.mobile_no==" + VmsConstants.newPhoneNum + ")]"));

	}
	@Test(groups={"sanity","smoke","regression"},description="Inviting new User as Order Manager")
	public void profileNewUsersAsOrderMgr() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.inviteUserAsOrderMgr();
		Thread.sleep(3000);
		/*Validating the Created User via API*/
		RMSHelper rmsHelper = new RMSHelper();
		String accessToken = rmsHelper.getLoginToken(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Processor processor = rmsHelper.getRestUsers(accessToken, VmsConstants.selectOutlet);
		Assert.assertNotNull(processor.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.userDetails..[?(@.mobile_no==" + VmsConstants.newPhoneNum + ")]"));

	}
	@Test(groups={"sanity","smoke","regression"},description="Editing the Profile for role")
	public void profileEditUserRole() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.editUserRole();
		
	}
	@Test(groups={"sanity","smoke","regression"},description="Editing the Profile for Outlets")
	public void profileEditUserOutlet() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.editUserOutlet();
		
	}
	
	@Test(groups={"sanity","smoke","regression"},description="Disabling User")
	public void profileDisable() {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.disableUser();
		
	}
	@Test(groups={"sanity","smoke","regression"},description="Enabling User")
	public void profileEnable() {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.restIdMulti, VmsConstants.prodPass);
		Assert.assertTrue(homePage.logoverify());
		Profile pro = new Profile(driver);
		pro.clickProfileIcon();
		pro.userClick();
		pro.enableUser();
		
	}
	

	@AfterMethod
	public void tearDown() {
		driver.close();
		driver.quit();
	}

}
