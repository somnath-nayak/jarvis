package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;
import java.util.List;

import static com.swiggy.ui.utils.AndroidUtil.sleep;
import static com.swiggy.ui.utils.Constants.lat5;
import static com.swiggy.ui.utils.Constants.lng5;

public class iOS_FSSAI {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    ConsumerApiHelper consumerApiHelper;
    ExplorePage explorepage;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwerty"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        explorepage = new ExplorePage(inti);
    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify whether license is present in the menu or not")
    public void verifyLicense(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat5), Double.toString(lng5));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenuV4(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat5),Double.toString(lng5),username,password,Constants.IOS_USER_AGENT,"238");
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Truffles");
        homePage.selectRestaurant("Truffles",0);
        sleep(2000);
        System.out.println(restaurantMenuObject.getImageId());
        System.out.println(Constants.fssaiimage);
        //System.out.println(restaurantMenuObject.getImageId().contains(Constants.fssaiimage)); just to check whether condition returns true
        Assert.assertTrue(restaurantPage.verifymenurestnameintitle());
        Assert.assertEquals(Constants.fssaiimage,restaurantMenuObject.getImageId());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify the license status")
    public void verifythelicensestatus(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat5), Double.toString(lng5));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenuV4(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat5),Double.toString(lng5),username,password,Constants.IOS_USER_AGENT,"238");
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        //homePage.clickOnEditLocation(); if device is not locating current location
        homePage.taponsorttooltip();
        homePage.openAccountTab();
        homePage.openExploreTab();
        sleep(2000);
        explorepage.searchRestaurantios("Truffles");
        homePage.selectRestaurant("Truffles",0);
        Assert.assertTrue(restaurantPage.verifymenurestnameintitle());
        Assert.assertEquals(Constants.fssailicenceno,restaurantMenuObject.getText());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }


    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
