package com.swiggy.ui.uiTests.ios.consumerapp.App_update;


import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;

public class AppUpdatePage extends AndroidUtil {

    private AppiumDriver driver;


    public IElement cancelupdate;
    public IElement update_msg;
    public IElement appupdate;


    public AppUpdatePage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        driver = init.getAppiumDriver();
    }


    public boolean checkifcancelisdisplayed() {
        try {

            return cancelupdate.isDisplayed();

        } catch (Exception e) {
            return false;
        }
    }


    public String getUpdatemessgae() {
        return update_msg.getText().toString();
    }

    public String getActualmessage(){
        return update_msg.getText().toString();
    }

    public void tapOnUpdate(){
        appupdate.click();
    }

    public void tapOnCancel(){
    cancelupdate.click();
}


    }


