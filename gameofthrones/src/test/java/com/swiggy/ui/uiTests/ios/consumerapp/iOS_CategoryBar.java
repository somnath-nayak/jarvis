package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;

import static com.swiggy.ui.utils.AndroidUtil.sleep;

public class iOS_CategoryBar {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    ConsumerApiHelper consumerApiHelper;
    ExplorePage explorepage;

    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9535896584", "qwertyu"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        explorepage = new ExplorePage(inti);
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Hygiene Rating(image and click), Back button, Menu which doesnt have HR")
    public void verifyCategoryBarDisplay(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        sleep(2000);
        Assert.assertTrue(homePage.verifyCategoryBar());
    }
    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify Hygiene Rating(image and click), Back button, Menu which doesnt have HR")
    public void verifyBarOptions(String username, String password) {
        System.out.println("ios driver" + driver);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        sleep(20000);
        homePage.Hygieneratingarea();
        homePage.removeToolTip();
        sleep(2000);
        homePage.verifySuperFastDelivery();
        Assert.assertTrue(homePage.getSuperFastDeliveryDetailScreen());
        homePage.CategoryBarBack();
        homePage.verifyVegOnly();
        Assert.assertTrue(homePage.getVegOnlyText());
        homePage.CategoryBarBack();
        Assert.assertTrue(homePage.assertlisting());
    }

    @AfterMethod
    public void tearDown() throws Exception {
        driver.resetApp();

    }


    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }
}
