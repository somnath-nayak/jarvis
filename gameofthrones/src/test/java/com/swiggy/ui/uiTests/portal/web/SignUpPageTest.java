package com.swiggy.ui.uiTests.portal.web;

import org.apache.maven.wagon.providers.ssh.knownhost.UnknownHostException;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.portalweb.HomePage;
import com.swiggy.ui.page.classes.portalweb.ListingPage;
import com.swiggy.ui.page.classes.portalweb.LoginPage;
import com.swiggy.ui.page.classes.portalweb.PortalConstants;
import com.swiggy.ui.page.classes.portalweb.SignUpPage;

public class SignUpPageTest extends SignUpPageDP
{
	
    public WebDriver driver;
    InitializeUI inti;
    HomePage homepage;
    LoginPage loginpage;
    ListingPage listingpage;
    SignUpPage signUppage;
     
    
	
    @BeforeClass
    public void setUptest() throws UnknownHostException
    {
        inti = new InitializeUI();
        driver = inti.getWebdriver();
        driver.manage().window().maximize();
        driver.get(PortalConstants.portal_url);
        homepage = new HomePage(inti);
        loginpage = new LoginPage(inti);
        listingpage = new ListingPage(inti);
        signUppage = new SignUpPage(inti);
        

    }
    
    
    /*Verify that all field are present on the sign up page*/
    
    @Test(groups={"sanity","regression"},description="Verify all elements are present on the signup Page",priority=0)
    public void verifyCreateAccountLink() throws InterruptedException
    {
    	SoftAssert softAssertion= new SoftAssert();
		homepage.clickOnSignup();
		softAssertion.assertEquals(signUppage.getSignUppage_Text(), "Sign up","Sign Up Page title is not proper");
		softAssertion.assertTrue(signUppage.closeBtn_Present(),"Close button is not present on Sign Up Page");
		softAssertion.assertTrue( signUppage.loginToAccountLink_Present(),"Create account link is not present  on Sign Up Page");
		softAssertion.assertTrue(signUppage.mobileField_Present(),"Text phone field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.nameField_present(),"Text phone field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.emailField_Present(),"Text password field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.passwordField_Present(),"Login Button is not present  on Sign Up Page");
		signUppage.click_Referal();
		softAssertion.assertTrue( signUppage.referalField_Present(),"Forgot password link is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.submit_Present(),"Forgot password link is not present  on Sign Up Page");
		
		
		
    }
    
    
    	/*Verify that proper validation message is displayed */
    
    @Test(dataProvider = "ErrorMessageValidation",groups={"regression"},description="Verify validation messages",priority=1)
    public void verifyValidationMessage(String phno, String name, String emailid, String password)
    {
    	SoftAssert softAssertion= new SoftAssert();
    	signUppage.sendKeys(phno, signUppage.mobile_no);
    	signUppage.sendKeys(name, signUppage.name);
    	signUppage.sendKeys(emailid, signUppage.email);
    	signUppage.sendKeys(password, signUppage.password);
    	signUppage.clickOnContinue();
		softAssertion.assertEquals(signUppage.getErrorMessage(signUppage.errorPhNo), "Enter your phone number","Error message not proper");
		softAssertion.assertEquals(signUppage.getErrorMessage(signUppage.errorName), "Enter your name","Error message not proper");
		softAssertion.assertEquals(signUppage.getErrorMessage(signUppage.errorEmail), "Invalid email address","Error message not proper");
		softAssertion.assertAll();
		
		
    }
    
    
   /*Verify that proper validation message is displayed when user try's to register with MobileNo already registered*/
    
    @Test(dataProvider = "RegisteredUser",groups={"sanity","regression"},description="Verify when user try to signup using already registered Phone number.",priority=3)
    public void verifyRegistrationOfAlreadyRegisteredUser(String phno, String name, String emailid, String password) throws InterruptedException
    {
    	SoftAssert softAssertion= new SoftAssert();
    	signUppage.sendKeys(phno, signUppage.mobile_no);
    	signUppage.sendKeys(name, signUppage.name);
    	signUppage.sendKeys(emailid, signUppage.email);
    	signUppage.sendKeys(password, signUppage.password);
    	signUppage.clickOnContinue();
    	Thread.sleep(1000);
    	softAssertion.assertEquals(signUppage.getErrorMessage(signUppage.errorPhNo), "Account exists, Please Login","validation message not proper");
		softAssertion.assertAll();
		
    }
    
    @AfterClass
    public void teardown()
    {
    	driver.close();

    }

}
