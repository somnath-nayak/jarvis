package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.DisDiscoveryStoryBoard;
import com.swiggy.ui.pojoClasses.DishDiscovery;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import net.sf.saxon.functions.ConstantFunction;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;

public class DD_FastForward {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    DishDiscoveryPage dishDiscoveryPage;



    DishDiscovery dishDiscoveryObject;
    DisDiscoveryStoryBoard ddStoryBoard;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
            return new Object[][]{{Constants.userName2, Constants.password2}};
        }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
        dishDiscoveryPage = new DishDiscoveryPage(inti);

    }


    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Dish Discovery FF")
    public void verifyDDfastForward(String username, String password) {
        Location location = new Location(com.swiggy.ui.utils.Constants.lat1, Constants.lng1, 0);
        System.out.println("ios driver" + driver);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openExploreTab();
        String collectionName = explorePage.getCollectionTitleText();
        AndroidUtil.sleep(3000);
        explorePage.doSearch("idli");
        explorePage.clickOnTab("DISHES");
        explorePage.list_dishesh.get(0).click();
        explorePage.clearSearch();
        explorePage.doSearch("vada");
        explorePage.clickOnTab("DISHES");
        explorePage.list_dishesh.get(0).click();
        explorePage.clearSearch();
        Assert.assertTrue(explorePage.checkElementDisplay(explorePage.btn_DD_viewMore),
                "View more button is not visible");
        explorePage.clickDDfastforward();
        Assert.assertEquals(explorePage.getCollectionTitleText(),collectionName,
                "The collection title has not matched");


    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "VerifyFF")
    public void verifyfastForward(String username, String password){

        Location location = new Location(com.swiggy.ui.utils.Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openExploreTab();
        String collectionName = explorePage.getCollectionTitleText();
        Assert.assertFalse(explorePage.checkElementDisplay(explorePage.btn_DD_viewMore),
                "View more button is not showing due to unavailability of search list");


}
    @AfterMethod
    public void tearDown () throws Exception {

        //driver.resetApp();
        driver.launchApp();

    }

    @AfterClass
    public void stopServer () {
        // inti.appiumServer.stopServer();
    }
}