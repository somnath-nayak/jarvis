package com.swiggy.ui.uiTests.Deeplink;




import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.ui.DeepLink.*;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class DeeplinkTest extends DeeplinkDataProvider {
    public AndroidDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login1;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    SwiggypopPage swiggypopPage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    TrackPage trackPage;
    String restaurantName;
    String Add_item;
    DeeplinkHelper deeplinkHelper;
    public boolean isLoginInApp;


    @BeforeClass
    @Parameters("isLogin")
        public void setUptest(boolean isLogin){
        deeplinkHelper = new DeeplinkHelper();
        consumerApiHelper=new ConsumerApiHelper();
        consumerApiHelper.removeFavs(Constants.userName,Constants.password,Double.toString(Constants.lat2),Double.toString(Constants.lng2));
        inti= new InitializeUI();
        driver = inti.getAndroidDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login1=new LoginPage(inti);
        addressPage=new AddressPage(inti);
        checkoutPage=new CheckoutPage(inti);
        accountPage=new AccountPage(inti);
        trackPage=new TrackPage(inti);
        explorePage=new ExplorePage(inti);
        restaurantPage=new RestaurantPage(inti);
        paymentPage=new PaymentPage(inti);
        swiggypopPage=new SwiggypopPage(inti);
        if(isLogin){
        loginInToApp(Constants.lat1,Constants.lng1);}
        else{
            Location location = new Location(Constants.lat1, Constants.lng1, 0);
            driver.setLocation(location);
            launch.clickonAllowButton();
            launch.clickOnSetDeliveryLocation();
            AndroidUtil.sleep();
            addressPage.clickonButtonAddMoreDetail();
            addressPage.clickonSkipAddress();
            home.removeSwiggyPop();
        }
        addItemToCart();
    }

    @BeforeMethod
    @Parameters("isLogin")
    public void beforeMethod(boolean isLogin){
        isLoginInApp=isLogin;
    }

    @Test(dataProvider = "DeepLinkURL")

    public void test1(int n, String URL) {
        AndroidUtil.pressBackKey(driver,5);
        AndroidUtil.sleep();
        String l2= deeplinkHelper.ParseLink(n, URL,inti);
        System.out.println(l2);
        deeplinkHelper.openDeeplink(URL);
        switch (l2) {
            case Deeplinkconstant.DEEPLINK_DISCOVERY:
                if(!isLoginInApp){
                    explorePage.validateExploreFromDeeplinkForNonLoggedIn("Punjabi");
                }
                else {
                    explorePage.validateExploreFromDeeplink("Punjabi");
                }
                break;

            case Deeplinkconstant.DEEPLINK_PROFILE:
                if(!isLoginInApp){
                    loginCheckForProfile();}
                else{
                    accountPage.ValidateAccountsPageFromdeeplink(Constants.userName);
                }
                break;

            case Deeplinkconstant.DEEPLINK_CART:
                checkoutPage.ValidateCheckoutPageFromdeeplink(restaurantName);
                break;

            case Deeplinkconstant.DEEPLINK_FAV:
                if(!isLoginInApp){
                    }
                else{
                    String Fav=home.getRestaurantNameFromList(0);
                    Assert.assertEquals(restaurantName,Fav);}
                break;

            case Deeplinkconstant.DEEPLINK_INVITE:
                if(!isLoginInApp){
                    }
                else {
                    accountPage.validateInviteFromDeeplink();}
                break;

            case Deeplinkconstant.DEEPLINK_PAYMENTS:
                if(!isLoginInApp){
                    }
                else{
                    paymentPage.ValidatePaymentPageFromdeeplink("Payments");}
                break;

            case Deeplinkconstant.DEEPLINK_OFFERS:

                    accountPage.validateoffersFromDeeplink("Offers");
                    break;

            case Deeplinkconstant.DEEPLINK_TRACK_ORDER:
                if(!isLoginInApp){
                    loginCheckForProfile();}
                else{
                    trackPage.validateTrackPageFromDeeplink();}
                break;

            case Deeplinkconstant.DEEPLINK_SUPPORT:
                if(!isLoginInApp){
                    //TODO list check
                   }
                else {
                    if (URL.contains("ordId")) {
                        Map<String, String> mapMenu = deeplinkHelper.getQueryMap(URL);
                        Assert.assertEquals(accountPage.gethelpHeader().toLowerCase(), "HELP AND SUPPORT".toLowerCase());
                    } else {
                        Assert.assertEquals(accountPage.gethelpHeader().toLowerCase(), "HELP".toLowerCase());
                    }
                    AndroidUtil.pressBackKey(driver, 2);
                }
                break;


            case Deeplinkconstant.DEEPLINK_MENU:
                Map<String, String> mapMenu=deeplinkHelper.getQueryMap(URL);
                Set<String> mapKeysMenu = mapMenu.keySet();
                Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(mapMenu.get("restaurant_id"),mapMenu.get("uuid"),Double.toString(Constants.lat1),Double.toString(Constants.lng1),Constants.userName,Constants.password);
                Assert.assertEquals(restaurantPage.getRestaurantName(),restaurantMenuObject.getName());
                restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0));
                restaurantPage.addItemtestForDeeplink();
                restaurantPage.openCartFromRestaurantPage();
                //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(),restaurantMenuObject.getName());
                break;

            case Deeplinkconstant.DEEPLINK_COLLECTION:
                System.out.println(home.getCollectionName());
                home.removeSwiggyPop();
                Map<String, String> mapCollections=deeplinkHelper.getQueryMap(URL);
                Set<String> mapKeys = mapCollections.keySet();
                List<Menulist> lrestaurantList = null;
                System.out.println(mapCollections.get("collection_id"));
                lrestaurantList=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1),mapCollections.get("collection_id"));
//                Assert.assertEquals(home.getRestaurantNameFromList(0),lrestaurantList.get(0).getRestaurantName());
                home.selectRestaurant(home.getRestaurantNameFromList(0),0);
                restaurantPage.removeCoachmark();
                restaurantPage.filterMenu(0);
                restaurantPage.addItemtestForDeeplink();
                restaurantPage.openCartFromRestaurantPage();
                //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(),lrestaurantList.get(0).getRestaurantName().toLowerCase());

                break;
            case Deeplinkconstant.DEEPLINK_FILTER:
                home.removeSwiggyPop();
                Map<String, String> map=deeplinkHelper.getQueryMap(URL);
                Set<String> keys = map.keySet();

                for (String key : keys) {
                    if (key.contains("CUISINES")) {
                        //Assert.assertTrue(home.checkFilterApplied());
                        home.openFilter();
                        Assert.assertEquals(home.isFilterCheckboxSelected(2), "true");
                        AndroidUtil.pressBackKey(driver);
                    }
                    if (key.contains("SHOW_RESTAURANTS_WITH")) {
                        //Assert.assertTrue(home.checkFilterApplied());
                        home.openFilter();
                        Assert.assertEquals(home.isFilterCheckboxSelected(0), "true");
                        AndroidUtil.pressBackKey(driver);
                    }
                    System.out.println("Value=" + map.get(key));
                    if(key.contains("sortBy")){
                        Assert.assertEquals(home.getSelectedSort().toLowerCase(),map.get(key).replaceAll("_"," ").toLowerCase());
                    }
                }
                break;
            case Deeplinkconstant.DEEPLINK_RESTAURANTLIST:
                home.removeSwiggyPop();
                System.out.println(home.getRestaurantNameFromList(0));

                break;
            case Deeplinkconstant.DEEPLINK_SORT:

                break;
            case Deeplinkconstant.DEEPLINK_SLUG:
                if(launch.checkElementDisplay(launch.btn_alwaysForDeeplink)){
                    launch.clickonButtonAlways();
                }
                String restaurantName=restaurantPage.getRestaurantName();
                System.out.println(restaurantName);
                restaurantPage.filterMenu(0);
                restaurantPage.addItemtestForDeeplink();
                restaurantPage.openCartFromRestaurantPage();
                //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(),restaurantName.toLowerCase());
                AndroidUtil.pressBackKey(driver);
                AndroidUtil.pressBackKey(driver);
                break;
            case Deeplinkconstant.DEEPLINK_TRACK_ORDERID:
                if(!isLoginInApp){
                    loginCheckForTrackOrder();}
                    else{
                String orderId=trackPage.getOrderId();
                System.out.println(trackPage.getOrderAmount());
                Map<String, String> mapTrack=deeplinkHelper.getQueryMap(URL);
                Assert.assertEquals(orderId,mapTrack.get("ordId"));}
                break;
            case Deeplinkconstant.DEEPLINK_POP:

                Assert.assertEquals(swiggypopPage.getpopHeader(),"Introducing Swiggy Pop");
                swiggypopPage.clickOnFirstPOPItem();
                if(!isLoginInApp){
                    Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());

                }
                else {
                    if (swiggypopPage.isNextAvailableTextDisplay()) {
                        swiggypopPage.clickOnNextAvailableButton();
                        //Assert.assertEquals();
                    } else {
                        swiggypopPage.clickonConfirmAddressAndProceed();
                        swiggypopPage.clickonProceedToPay();
                    }
                }
                break;
        }
    }
    @Test(dataProvider = "preOrder")
    @Parameters("isLogin")
    public void test2(int n, String URL) {
        if(!isLoginInApp){

        }
        else {
            loginInToApp(Constants.lat4, Constants.lng4);
            AndroidUtil.pressBackKey(driver, 2);
        }
        String l2 = deeplinkHelper.ParseLink(n, URL, inti);
        System.out.println(l2);
        deeplinkHelper.openDeeplink(URL);
        AndroidUtil.sleep(3000);
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        String selectedTime=home.getPreorderDeliveryTimeText(0);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        Map<String, String> map=deeplinkHelper.getQueryMap(URL);
        Date expiry = new Date(Long.parseLong(map.get("date")));
        String day=expiry.toString().split(" ")[0];
        System.out.println(day);
        Assert.assertEquals(home.getPreorderSlotTime(),day+","+selectedTime.split("-")[1]);
    }


    @AfterClass
    public void stopServer() {
        System.out.println("Deep link Execution Stopped");
    }


    public void loginInToApp(Double lat,Double lng){
        driver.closeApp();
        driver.launchApp();
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat),Double.toString(lng),"WORK");
        Location location = new Location(lat, lng, 0);
        driver.setLocation(location);
        launch.tapOnLogin();
        login1.LogintoApp(Constants.userName, Constants.password);
    }
    public void addItemToCart(){
        home.removeSwiggyPop();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        //restaurantPage.clickonFavIcon();
        restaurantName=restaurantPage.getRestaurantName();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
    }

    public void loginCheckForProfile() {
        Assert.assertTrue(accountPage.checkElementDisplay(accountPage.btn_login));
    }
    public void loginCheckForTrackOrder() {
        System.out.println(restaurantPage.getReplaceCartItemDialogTitle());
        System.out.println(restaurantPage.getReplaceCartItemDialogMessage());
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(),"Your session has expired. Please sign-in again.".toLowerCase());
        checkoutPage.clickOk();
        Assert.assertTrue(login1.checkElementDisplay(login1.MobileNumber));

    }

}




