package com.swiggy.ui.uiTests.portal.web;

import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.portalweb.HomePage;
import com.swiggy.ui.page.classes.portalweb.LoginPage;
import com.swiggy.ui.page.classes.portalweb.PortalConstants;

import com.swiggy.ui.page.classes.portalweb.ListingPage;
import com.swiggy.ui.page.classes.portalweb.SignUpPage;


public class HomePageTests {
    public WebDriver driver;
    InitializeUI inti;
    HomePage homepage;
    LoginPage loginpage;
    SignUpPage signUppage;
    ListingPage listingpage;
    
    
    
    

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        inti = new InitializeUI();
        driver = inti.getWebdriver();
        driver.manage().window().maximize();
        driver.navigate().to(PortalConstants.portal_url);
        homepage = new HomePage(inti);
        loginpage = new LoginPage(inti);
        listingpage = new ListingPage(inti);
        signUppage = new SignUpPage(inti);
        
       
        

    }
    
    /*Verifying that the user is landing on home page when user enter the url "swiggy.com" for the first time*/
		
	@Test(groups={"sanity","regression"},description="Verify the home page",priority=0 )
	public void verfyHomePage() 
	{
		SoftAssert softAssertion = new SoftAssert();
		softAssertion.assertTrue(homepage.checkloginButn_present(), "Login button missing on homepage");
		softAssertion.assertTrue(homepage.checksignupBtn_present(), "Signup button missing on homepage");
		softAssertion.assertTrue(homepage.checksignupBtn_present(),"Signup button missing on homepage");
		softAssertion.assertTrue(homepage.checklocation_present(),"Location field missing on homepage");
		softAssertion.assertFalse(homepage.checkClear_Present(),"clear field option is present in location text field");
		softAssertion.assertTrue( homepage.checkLocateme_Present(),"Locate me missing on home page in delivery location");
		softAssertion.assertTrue(homepage.checkfindButn_present(),"submit button missing on homepage");
		softAssertion.assertEquals(driver.getCurrentUrl(),PortalConstants.portal_url,"Home page URL not correct");
		
		softAssertion.assertAll();
		
	}
	
	
	
	/*Verifying that he user is landing on the login page once he clicks on login link from home page*/
	
	@Test(groups={"sanity","regression"},description="On click of login, user lands on login page",priority=1)
	public void verfyLoginPage() 
	{
		SoftAssert softAssertion = new SoftAssert();
		homepage.clickOnLogin();
				
		softAssertion.assertEquals(loginpage.getLoginPage_Text(),"Login", "Page title is not proper");
		softAssertion.assertTrue(loginpage.closeBtn_Present(),"Close button is not present On login page");
		softAssertion.assertTrue(loginpage.createAccounttLink_Present(),"Create account link is not present On login page");
		softAssertion.assertTrue(loginpage.mobileField_Present(),"Text phone field is not present On login page");
		softAssertion.assertTrue(loginpage.passwordField_Present(),"Text password field is not present On login page");
		softAssertion.assertTrue(loginpage.forgotPassword_Present(),"Login Button is not present On login page");
		softAssertion.assertTrue(loginpage.submit_Present(),"Forgot password link is not present On login page");
		loginpage.clickOnClose();
		
		softAssertion.assertAll();
		
	}
	
	/*Verifying that he user is landing on the SignUp page once he clicks on SignUp link from home page*/
	
	@Test(groups={"sanity","regression"},description="On click of Signup, user lands on Sign Up page",priority=2)
	public void verfySignupPage() 
	{
		SoftAssert softAssertion = new SoftAssert();
		driver.navigate().to(PortalConstants.portal_url);
		homepage.clickOnSignup();
		
		softAssertion.assertEquals(signUppage.getSignUppage_Text(),"Sign up","Sign Up Page title is not proper");
		softAssertion.assertTrue( signUppage.closeBtn_Present(),"Close button is not present on Sign Up Page");
		softAssertion.assertTrue( signUppage.loginToAccountLink_Present(),"Create account link is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.mobileField_Present(),"Text phone field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.nameField_present(),"Text name field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.emailField_Present(),"Text email field is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.passwordField_Present(),"password field is not present  on Sign Up Page");
		signUppage.click_Referal();
		softAssertion.assertTrue( signUppage.referalField_Present(),"Forgot password link is not present  on Sign Up Page");
		softAssertion.assertTrue( signUppage.submit_Present(),"Continue button is not present  on Sign Up Page");
		
		softAssertion.assertAll();
	}
	
	/*Verifying that the auto suggestions are populating for the given input*/
	
	@Test(groups={"sanity","regression"},description="Verify autosuggestions populating",priority=3)
	public void verfyMyDeliveryLocationAutosuggestionLoading() 
	{
		SoftAssert softAssertion = new SoftAssert();
		driver.navigate().to(PortalConstants.portal_url);
		homepage.searchForLocation(PortalConstants.autosuggestion);
		homepage.wait(driver, 10, TimeUnit.SECONDS);
		
		softAssertion.assertTrue( homepage.autosuggestionPopulating(),"Autosuggestion are not populating");
		softAssertion.assertTrue(homepage.checkClear_Present(),"clear button is not enabled");
		
		softAssertion.assertAll();
		
		
	}
	
	/*Verifying that user is taken to listing page when user selects serviceable location from the auto suggestions*/
	
	@Test(groups={"sanity","regression"},description="User selects location from autosuggestion",priority=4)
	public void verfyLandingPage_UserSelectslocFromAutoSugg() 
	{
		SoftAssert softAssertion = new SoftAssert();
		homepage.selectAddressFromAutosuggetion(PortalConstants.address);
		
		softAssertion.assertTrue( listingpage.checkCart_Present(),"Cart icon not present");
		softAssertion.assertTrue( listingpage.checksignIn_Present(),"Sign In icon not present");
		softAssertion.assertAll();
		
	
	}
	
	/*Verify that user is taken to the listing page when user selects the location using GPS, and without signing into account*/

	@Test(groups={"sanity","regression"},description="User selects location using gps",priority=6)
	public void verfyLoacateMe() 
	{
		SoftAssert softAssertion = new SoftAssert();
		/*homepage.allowLoationSharing(driver);*/
		homepage.deleteAllCookies(driver);
		driver.navigate().to(PortalConstants.portal_url);
		
		homepage.selectLocationGps(driver);
		
		
		softAssertion.assertTrue(listingpage.checkCart_Present(),"Cart icon not present");
		softAssertion.assertTrue(listingpage.checksignIn_Present(),"Sign In icon not present");
		
		softAssertion.assertAll();
		
	
	}
	
	/*Verify that the proper error message is shown to user when user selects non serviceable location*/
	

	@Test(groups={"regression"},description="User selects unserviceable location",priority=7)
	public void verfyLandingPage_UserSelectsunserviceableLocation() 
	{
		SoftAssert softAssertion = new SoftAssert();
		homepage.deleteAllCookies(driver);
		homepage.navigateTo(driver,PortalConstants.portal_url);
		homepage.searchForLocation(PortalConstants.unservicible_loc);
		homepage.wait(driver, 10, TimeUnit.SECONDS);
		homepage.selectAddressFromAutosuggetion(PortalConstants.unservicible_address);
		
		softAssertion.assertFalse(listingpage.checkCart_Present(),"Cart icon not present");
		softAssertion.assertFalse(listingpage.checksignIn_Present(),"Sign In icon not present");
		softAssertion.assertEquals(homepage.getErrorMessage(),"Sorry! We don't serve at your location currently.","The error message is not correct");
		homepage.clearLocationTextField();
		
		softAssertion.assertAll();
	
	}
	
	/*Verify that when user clicks on the serviceable city, all the popular areas in selected city should be displayed
	when user selects the popular area user should be taken listing page for that area*/
	
	@Test(groups={"regression"},description="Verify when user clicks on city link",priority=8)
	public void verfyServiciableCityLink() 
	{
		SoftAssert softAssertion = new SoftAssert();
		driver.navigate().to(PortalConstants.portal_url);
		homepage.clikOnServiciableCity();
		
		softAssertion.assertTrue( homepage.popularareas_Present(driver),"Cart icon not present");
		homepage.selectACityFromPopularCities();
		softAssertion.assertTrue(listingpage.checkCart_Present(),"user is not redirected to listing page");
		
		softAssertion.assertAll();
		
	}
	
	

	
/*	Verify that all links are present under the company link in static section */
	
	@Test(groups={"regression"},description="Verify that all links are present under the company link",priority=9)
	public void verifyAllLinkPresentUnderCompany()
	{
		
		homepage.navigateTo(driver, PortalConstants.portal_url);
		homepage.scrollIntoView(driver, homepage.company);
		Assert.assertTrue(homepage.verfyLlinksUnderStaticSection( homepage.linksunder_company,PortalConstants.companyLinks), "Links under company section in static part are not proper");
			
		
	}
	
	/*Verify that all links under company section are click-able*/
	@Test(groups={"regression"},description="Verify that all links are clickable under the company link",priority=10)
	public void verifyAllLinksAreClickableUnderCompany()
	{
		
		homepage.navigateTo(driver, PortalConstants.portal_url);
		Assert.assertTrue(homepage.verfyLlinksAreClickable(driver,homepage.linksunder_company,PortalConstants.companlyUrls,homepage.company), "Links under company section in static part are not proper");
			
		
	}
	
	/*Verify that user is taken to support and help  page when clicks on Help&Support link*/
	
	@Test(groups={"regression"},description="Verify that user is able to click on the Help&SupportLink ",priority=11)
	public void verifyHelpandSupportLink() 
	{
		homepage.navigateTo(driver, PortalConstants.portal_url);
		homepage.clickOnHelpandSupportLink(driver);
		Assert.assertEquals(homepage.getHelppagetitle(), "HELP","Support page is not clickable");
		
		
		
	}
	
	
	/*Verify that partner with us link*/
	@Test(groups={"regression"},description="Verify partner with us link",priority=12)
	public void verifypartnerWithUsLink()
	{
		
		homepage.navigateTo(driver, PortalConstants.portal_url);
		homepage.scrollIntoView(driver, homepage.partnerWithUs);
		Assert.assertTrue(homepage.verifyPartnerWithUs(driver), "Links under partner with us section in static part are not proper");
		
		
	}
	
	/*Verify that all links are present under the Legal Section*/ 
	
	@Test(groups={"regression"},description="Verify that all links are present under the Legal section",priority=13)
	public void verifyAllLinkPresentUnderLeagal()
	{
		
		homepage.navigateTo(driver, PortalConstants.portal_url);
		homepage.scrollIntoView(driver, homepage.legal);
		Assert.assertTrue(homepage.verfyLlinksUnderStaticSection(homepage.legal_links, PortalConstants.legalLinks), "Links under company section in static part are not proper");
			
		
	}
	
	/*Verify that all links are click able under Legal*/
	@Test(groups={"regression"},description="Verify that all links are clickable under the Legal link",priority=14)
	public void verifyAllLinksAreClickableUndeLeagal()
	{
		
		homepage.navigateTo(driver, PortalConstants.portal_url);
		Assert.assertTrue(homepage.verfyLlinksAreClickable(driver,homepage.legal_links,PortalConstants.legalUrls,homepage.legal), "Links under legal section in static part are not proper");
			
		
	}
	
    @AfterClass
    public void teardown()
    {
    	
    	driver.close();
    	
    }


	

}
