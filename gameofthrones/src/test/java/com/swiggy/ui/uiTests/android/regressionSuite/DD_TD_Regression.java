package com.swiggy.ui.uiTests.android.regressionSuite;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.rng.pojo.Menu;

import antlr.Utils;
import framework.gameofthrones.Aegon.AppiumServerJava;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.TouchAction;

import org.aspectj.weaver.ast.And;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pojoClasses.GetAddressPojo;
import scopt.Check;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.StfHelper;
import com.swiggy.ui.pojoClasses.*;

import org.openqa.selenium.By;
import org.openqa.selenium.html5.*;
import java.text.MessageFormat;

public class DD_TD_Regression {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    StfHelper stfHelper;
    Double lat1=12.932815;
    Double lng1=77.603578;

    Double lat2=27.834105;
    Double lng2=76.171977;

    Double lat3=19.229388800000002;
    Double lng3=72.8569977;
    String deviceId;
    
    DishDiscoveryPage dishDiscoveryPage;
    DishDiscovery dishDiscoveryObject;
    DisDiscoveryStoryBoard ddStoryBoard;
    


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{Constants.userName,Constants.password}};
    }

    @BeforeClass
    public void setUptest() {
        consumerApiHelper = new ConsumerApiHelper();
     
        inti = new InitializeUI();

        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        dishDiscoveryPage=new DishDiscoveryPage(inti);
    }
    
    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"DishDiscovery"},description="validate DishDiscovery collection on home page")
    public void verifyDishDiscoveryCollectionPresentOnList(String username,String password,ITestContext context) {
    	SoftAssert softAssert = new SoftAssert();
    	setUptest();
    	
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        
        home.verticalScroll();
        context.setAttribute("storyTitle",home.getStoryCollectionItem(0).toLowerCase()); 
        System.out.println(context.getAttribute("storyTitle").toString());
        Assert.assertNotNull(context.getAttribute("storyTitle"));
    }
    
    
    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"DishDiscovery"},description="validate DishDiscovery collection on home page")
    public void addDishDiscoveryItemFromExplore(String username,String password,ITestContext context) {
    	SoftAssert softAssert = new SoftAssert();
     	setUptest();
    	
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        
        home.openExploreTab();
        AndroidUtil.sleep(10000);

        explorePage.selectDishes("", 1);
        
        AndroidUtil.sleep(15000);
        
        String collectionCountMessage = dishDiscoveryPage.getCollectionCount();
        String count = collectionCountMessage.substring(0,2);
        
        System.out.println("Story Description..."+dishDiscoveryPage.getStoryDescription());
        System.out.println("Total Collections Message..."+ collectionCountMessage);
        
        System.out.println("Count = "+count);
        int countNew = Integer.parseInt(count);
        AndroidUtil.scrollHorizontal_new(driver,countNew+5);
        
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(10000);
        
        explorePage.selectDishes("", 1);
        AndroidUtil.sleep(15000);
        
       AndroidUtil.scrollHorizontal_new(driver,5);
       AndroidUtil.sleep(1000);
       AndroidUtil.scrollVertical_new(driver, 1);
       AndroidUtil.sleep(2000);
       
       dishDiscoveryPage.clickonAddToCartButton(1);
       
       dishDiscoveryPage.clickonViewFullMenu();
       dishDiscoveryPage.clickOnXY(100, 100);
       
       AndroidUtil.sleep(1000);
       restaurantPage.openCartFromRestaurantPage();
       
       AndroidUtil.pressBackKey(driver,2);
       AndroidUtil.pressBackKey(driver);
       
       addressPage.clickOnDeleteAddressConfirmationNo();
       AndroidUtil.pressBackKey(driver);
       addressPage.clickOnDeleteAddressConfirmationYes();
       AndroidUtil.pressBackKey(driver);
       
       softAssert.assertEquals(explorePage.getCollectionTitleText(),context.getAttribute("storyTitle").toString()); 
     
       softAssert.assertAll();
    }
    
    
    

    
    
}




