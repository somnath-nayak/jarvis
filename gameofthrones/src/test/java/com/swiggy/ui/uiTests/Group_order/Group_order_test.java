package com.swiggy.ui.uiTests.Group_order;


import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import java.util.regex.*;
import java.util.ArrayList;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Group_order_test {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    GrouporderPage grouporderPage;
    Double lat1 = 12.932815;
    Double lng1 = 77.603578;

    Double lat2 = 27.834105;
    Double lng2 = 76.171977;

    Double lat3 = 19.229388800000002;
    Double lng3 = 72.8569977;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9036976038", "huawei123"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {

        AndroidUtil.runShellCommand("adb -s "+System.getenv("deviceId")+"  shell am start -n ca.zgrs.clipper/.Main");
        AndroidUtil.runShellCommand("adb -s "+System.getenv("deviceId")+" shell input keyevent input 3");

        //Proxyserver mobProxyTest=new Proxyserver(9091);

        //mobProxyTest.startServer();

        consumerApiHelper = new ConsumerApiHelper();
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));


        inti = new InitializeUI();

        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
        accountPage = new AccountPage(inti);
        grouporderPage = new GrouporderPage(inti);


    }


    //Create group_order Link
    @Test(dataProvider = "LoginData", priority = 1,description = "Create Social group order link")
    public void verifyCreateGroupOrderandValidate(String username, String password, ITestContext iTestContext) {
        consumerApiHelper.deleteAllAddress("9036976038", "huawei123");
        consumerApiHelper.createNewAddress("9036976038", "huawei123", Double.toString(Constants.lat2), Double.toString(Constants.lng2), "WORK");
        loginInToApp(Constants.lat2, Constants.lng2);
        home.removeSwiggyPop();
        home.openNearMeTab();
        home.selectRestaurant(null, 0);
        restaurantPage.removeCoachmark();
        restaurantPage.clickonGrouporderIcon();
        System.out.println("social icon clicked");
        grouporderPage.clickonCreateGrouporderButton();
        AndroidUtil.sleep(5000);
        //click on more button
        grouporderPage.moreOptionsToSharelink();
        AndroidUtil.sleep(5000);
        accountPage.clickOncopytoClipboardGroupOrder();
        AndroidUtil.sleep(5000);
        String str = grouporderPage.grouporderlinkCopy_Adbcommand();
        System.out.println("link:" + str);
        String myUrl = parseUrl(str);
        System.out.println("Parsed link:" + myUrl);
        iTestContext.setAttribute("url", myUrl);
        AndroidUtil.sleep(5000);
        String str1 = restaurantPage.getGrouporderCreatedText();
        System.out.println(str1);
        Assert.assertEquals(restaurantPage.getGrouporderCreatedText(), "Swiggy Social order by Mukesh");


    }

    @Test(priority = 2,description = "Guest user joins group order using group order link")
    public void verifyGuestUserJoinAndValidate(ITestContext iTestContext) {
        try {
            String expandedUrl = expand(iTestContext.getAttribute("url").toString());
            Map<String, String> mapGroupOrderUrl = getQueryMap(expandedUrl);
            consumerApiHelper.joinGroupOrder(Constants.username1, Constants.password1, mapGroupOrderUrl.get("parentCartKey"));
            AndroidUtil.sleep(5900);
            String guest = restaurantPage.verifyGuestJoinedGroupOrderByText();
            System.out.println(guest);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(priority = 3,description = "Host adds items to cart after creating group order")
    public void verifyHostAddsItemToCart(ITestContext iTestContext) {

        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        restaurantPage.addItemtest();
        //restaurantPage.openCartFromRestaurantPage();

    }

    @Test(priority = 4,description = "Guest adds item after joining to group order")
    public void verifyGuestAddsItemToCart(ITestContext iTestContext) {
        try {
            String expandedUrl = expand(iTestContext.getAttribute("url").toString());
            Map<String, String> mapGroupOrderUrl = getQueryMap(expandedUrl);
            consumerApiHelper.addItemForGroupOrder(Constants.username1, Constants.password1, mapGroupOrderUrl.get("parentCartKey"), mapGroupOrderUrl.get("restId"));
            restaurantPage.openCartFromRestaurantPage();
            //checkoutPage.proceedToPay();
            AndroidUtil.pressBackKey(driver);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Test(priority = 6,description = "Keep group order and Go to Listing page")

    public void verifyKeepGroupOrderCartOption() {
        AndroidUtil.sleep(5000);
        AndroidUtil.pressBackKey(driver);
        restaurantPage.keepGroupCart();

    }

    @Test(priority = 7,description = "Discard group order cart")
    public void verifyDiscardGroupOrderCartOption() {

        home.clickonSwiggySocialCrouton();
        AndroidUtil.sleep(5000);
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        restaurantPage.discardGroupCart();

    }

    //Create Normal Cart, create group order with same cart
    @Test(priority = 8,description = "Create normal cart and create group order cart with existing cart")
    public void verifyNormalCartToGroupOrderCart(ITestContext iTestContext) {

        home.selectRestaurant(null, 0);
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        restaurantPage.addItemtest();
        AndroidUtil.sleep(5000);
        restaurantPage.clickonGrouporderIcon();
        System.out.println("social icon clicked");
        grouporderPage.clickonCreateGrouporderButton();
        AndroidUtil.sleep(5000);
        //click on more button
        grouporderPage.moreOptionsToSharelink();
        AndroidUtil.sleep(5000);
        accountPage.clickOncopytoClipboardGroupOrder();
        AndroidUtil.sleep(5000);
        String str = grouporderPage.grouporderlinkCopy_Adbcommand();
        System.out.println("link:" + str);
        String myUrl = parseUrl(str);
        System.out.println("Parsed link:" + myUrl);
        iTestContext.setAttribute("url", myUrl);
        AndroidUtil.sleep(5000);
        String str1 = restaurantPage.getGrouporderCreatedText();
        System.out.println(str1);
        verifyOtherUserJoinAndValidate1(iTestContext);
        AndroidUtil.sleep(5900);
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertEquals(restaurantPage.getGrouporderCreatedText(), "Swiggy Social order by Mukesh");

    }


    @Test(priority = 9,description = "Apply coupon to group order cart")
    public void verifyApplyCouponToGroupOrderCart() {

        restaurantPage.openCartFromRestaurantPage();
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        checkoutPage.applyCoupon("FCH50");
        Assert.assertEquals(checkoutPage.getCouponAppliedDialogTitle().toLowerCase(), "Coupon Applied".toLowerCase());
        checkoutPage.clickOnOK();

        //checkoutPage.proceedToPay();
    }

    //Remove guest from cart
    @Test(priority = 10,description = "Remove guest from group order cart")
    public void verifyHostRemovingGuestFromCart() {

        AndroidUtil.sleep(5000);
        checkoutPage.removeGuestFromCart();
        AndroidUtil.pressBackKey(driver, 2);
        restaurantPage.discardGroupCart();

    }

    @Test(priority = 11,description = "create group order flow non logged in user")
    public void verifyCreateGroupOrderFlowNonLoggedinUser(ITestContext iTestContext) {

        driver.closeApp();
        driver.launchApp();

        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.selectRestaurant(null, 0);
        restaurantPage.removeCoachmark();
        //restaurantPage.addItemtest();
        AndroidUtil.sleep(5000);
        restaurantPage.clickonGrouporderIcon();
        System.out.println("social icon clicked");
        grouporderPage.clickonCreateGrouporderButton();
        AndroidUtil.sleep(5000);
        login.LogintoApp(Constants.userName, Constants.password);
        grouporderPage.clickonCreateGrouporderButton();
        AndroidUtil.sleep(5000);
        //click on more button
        grouporderPage.moreOptionsToSharelink();
        AndroidUtil.sleep(5000);
        accountPage.clickOncopytoClipboardGroupOrder();
        AndroidUtil.sleep(5000);
        String str = grouporderPage.grouporderlinkCopy_Adbcommand();
        System.out.println("link:" + str);
        String myUrl = parseUrl(str);
        System.out.println("Parsed link:" + myUrl);
        iTestContext.setAttribute("url", myUrl);
        AndroidUtil.sleep(5000);
        String str1 = restaurantPage.getGrouporderCreatedText();
        System.out.println(str1);
        Assert.assertEquals(restaurantPage.getGrouporderCreatedText(), "Swiggy Social order by Mukesh");


    }


    public void loginInToApp(Double lat, Double lng) {
        driver.closeApp();
        driver.launchApp();
        consumerApiHelper.deleteAllAddress(Constants.userName, Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName, Constants.password, Double.toString(lat), Double.toString(lng), "WORK");
        Location location = new Location(lat, lng, 0);
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(Constants.userName, Constants.password);
    }

    public static String expand(String urle) throws IOException {
        String urleFinal = urle.substring(urle.lastIndexOf("=") + 1).replaceAll("^\"|\"$", "");
        String expandedUrl = null;

        URL url = new URL(urleFinal);
        // open connection
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();

        // stop following browser redirect
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.connect();
        // extract location header containing the actual destination URL
        expandedUrl = httpURLConnection.getHeaderField("location");
        httpURLConnection.disconnect();


        System.out.println("Hello: " + expandedUrl);
        return expandedUrl;
    }

    public Map<String, String> getQueryMap(String query) {
        Map<String, String> map = new HashMap<String, String>();
        try {

            URL aURL = new URL(query);
            System.out.println("query = " + aURL.getQuery());
            String[] params = aURL.getQuery().split("&");

            for (String param : params) {
                String name = param.split("=")[0];
                String value = param.split("=")[1];
                map.put(name, value);
                System.out.println(name);
                System.out.println(value);

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return map;
    }

    //Parse URL from Text Message

    public String parseUrl(String msg) {
        List<String> extractedUrls = extractUrls(msg);
        String myUrl = "";
        for (String url : extractedUrls) {
            myUrl = url;
            break;
        }
        return myUrl;
    }


    public static List<String> extractUrls(String text) {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);

        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }

        return containedUrls;
    }


    public void verifyOtherUserJoinAndValidate1(ITestContext iTestContext) {

        try {
            String expandedUrl = expand(iTestContext.getAttribute("url").toString());
            Map<String, String> mapGroupOrderUrl = getQueryMap(expandedUrl);
            consumerApiHelper.joinGroupOrder(Constants.username1, Constants.password1, mapGroupOrderUrl.get("parentCartKey"));
            consumerApiHelper.addItemForGroupOrder(Constants.username1, Constants.password1, mapGroupOrderUrl.get("parentCartKey"), mapGroupOrderUrl.get("restId"));
            AndroidUtil.sleep(5900);
            String guest = restaurantPage.verifyGuestJoinedGroupOrderByText();
            System.out.println(guest);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}










