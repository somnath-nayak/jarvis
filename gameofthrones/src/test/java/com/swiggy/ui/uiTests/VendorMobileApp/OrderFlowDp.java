package com.swiggy.ui.uiTests.VendorMobileApp;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsConstants;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsHelper;
import com.swiggy.ui.utils.Vendorappconstant;
import org.testng.annotations.DataProvider;

/**
 * Created by kiran.j on 6/4/18.
 */
public class OrderFlowDp {

    VmsHelper vmsHelper = new VmsHelper();
    LOSHelper losHelper = new LOSHelper();
    OMSHelper omsHelper = new OMSHelper();

    @DataProvider(name = "createOrder")
    public Object[][] createOrder() throws Exception {
        String order_id = losHelper.getAnOrder("partner");
        omsHelper.verifyOrder(order_id, VmsConstants.order_confirm_code);
        return new Object[][]{{Vendorappconstant.RESTAURANTID, Vendorappconstant.PASSWORD, order_id}};
    }
}
