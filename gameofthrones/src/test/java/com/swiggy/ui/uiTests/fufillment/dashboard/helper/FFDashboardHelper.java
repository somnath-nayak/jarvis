package com.swiggy.ui.uiTests.fufillment.dashboard.helper;

import org.openqa.selenium.WebDriver;

import com.swiggy.ui.page.classes.fulfillment.dashboard.HomePage;
import com.swiggy.ui.page.classes.fulfillment.dashboard.LoginPage;
import com.swiggy.ui.uiTests.fufillment.dashboard.constants.FFDashboardConstants;
import com.swiggy.ui.utils.WebUtil;

public class FFDashboardHelper implements FFDashboardConstants {
	
public WebDriver driver = null;
	
	public FFDashboardHelper(WebDriver driver) {
		this.driver = driver;
	}
	
	WebUtil util = new WebUtil();
	
	public HomePage loginAs(String oe){
		LoginPage login = new LoginPage(driver);
		if(oe.contains("L1")){
			HomePage home = login.enterUserName(l1User).enterPassword(l1Pass).submitLogin();
			return home;
		}else if (oe.contains("L2")) {
			HomePage home = login.enterUserName(l2User).enterPassword(l2Pass).submitLogin();
			return home;
		}else if (oe.contains("callDE")) {
			HomePage home = login.enterUserName(callDEUser).enterPassword(callDEPass).submitLogin();
			return home;
		}else if (oe.contains("verifier")) {
			HomePage home = login.enterUserName(verifierUser).enterPassword(verifierPass).submitLogin();
			return home;
		}else if (oe.contains("superuser")) {
			HomePage home = login.enterUserName(superuser).enterPassword(superpass).submitLogin();
			return home;
		}else {
			System.out.println("********** Please select an OE **********");
			return null;
		}
	}
	
	public void logoutFromDashboard() throws InterruptedException{
		HomePage home = new HomePage(driver);
		Thread.sleep(1000);
		home.clickOnProfileDropdown();
		Thread.sleep(2000);
		home.clickOnlogoutButton();
	}
	
	public void changeOrderStatusToPlaced(String oe){
		HomePage home = new HomePage(driver);
		if(!oe.equalsIgnoreCase("L1Placer")){
			home.clickOnOrderPane();
			home.clickOnTogglePlacingButons();
			home.clickOnPlacedButton();
			util.acceptAlert(driver);
		}else {
			home.clickOnOrderPane();
			home.clickOnPlacedButton();
			util.acceptAlert(driver);
		}
	}

	public void assignAnOrderToL2(String orderId) throws InterruptedException {
//		driver.navigate().to(FFDashboardConstants.urlStage1);
		loginAs("superuser");
		HomePage home = new HomePage(driver);
		home.enterSearchText(orderId);
		home.search();
		home.clickOnOrderPane();
		home.clickOnReassignPlacerButton();
		util.acceptAlert(driver);
		logoutFromDashboard();
	}
	
}
