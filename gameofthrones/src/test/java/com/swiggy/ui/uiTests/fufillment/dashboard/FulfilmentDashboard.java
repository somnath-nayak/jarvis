package com.swiggy.ui.uiTests.fufillment.dashboard;

import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.InitializeUI;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.page.classes.fulfillment.dashboard.*;

import java.util.ArrayList;
import java.util.List;

public class FulfilmentDashboard  {

	InitializeUI gameofthrones = new InitializeUI();
	private WebDriver driver = gameofthrones.getWebdriver();
	LOSHelper losHelper = new LOSHelper();
	DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();

	private static String URL = "http://oms-u-cuat-02.swiggyops.de/";
	
	@Test(enabled = true, priority = 0)
	public void test1() {
		try{
		Thread.sleep(5000);
		driver.navigate().to(URL);	
		} catch(Exception e) {
			e.getStackTrace();
		}
	}
	
	@DataProvider(name = "login")
	public Object[][] getdetails() {
		return new Object[][] {{"shashank","Shash@ank1","AutomationDEQ__DontUse_Atmn_runs( 1664.0 km )","AutomationDEO_DontUse_Atmn_runs( 4984.21 km )"}};
	}
	
	@Test(dataProvider = "login", priority = 1)
	public void test(String user, String pass, String de1, String de2) {
		try{
			driver.navigate().to(URL);
		LoginPage login = new LoginPage(driver);
		List<String> des = new ArrayList<String>();
		des.add(de1);
		des.add(de2);
		HomePage home = login.enterUserName(user).enterPassword(pass).submitLogin();
		String orderId = losHelper.getAnOrder("partner");
		CAPRDPage caprdpage = home.enterSearchText(orderId).search().clickOnOrdercaprd();
		Thread.sleep(3000);
		AssignDE assignde = home.proceed();
		deliveryServiceHelper.orderAssign(orderId, "216");
		driver.navigate().refresh();
		home.clickOnOrder();
//		assignde.select().assign(des);
//		home.clickOnOrder();
		Thread.sleep(3000);
		caprdpage.confirmOrder();
		Thread.sleep(3000);
		home.clickOnOrder();
		caprdpage.arrive();
		Thread.sleep(3000);
		home.clickOnOrder();
		caprdpage.pickupOrder();
		Thread.sleep(3000);
		home.clickOnOrder();
		caprdpage.reachOrder();
		Thread.sleep(3000);
		home.clickOnOrder();
		caprdpage.deliverOrder();
		Thread.sleep(3000);
		driver.quit();
//		CancelPage cancel = home.scrollToCancel();
//		cancel.cancel().selectReason();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@AfterClass
	public void tearDown() {
		//driver.close();
		driver.quit();
	}
	
}