package com.swiggy.ui.uiTests.android.regressionSuite;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.gargoylesoftware.htmlunit.javascript.host.Touch;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Address;
import com.swiggy.api.sf.rng.pojo.Menu;
import framework.gameofthrones.Aegon.AppiumServerJava;
import framework.gameofthrones.Aegon.InitializeUI;
import framework.gameofthrones.Baelish.Proxyserver;
import framework.gameofthrones.Tyrion.StfHelper;
import io.advantageous.boon.core.Sys;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.TouchAction;

import org.aspectj.weaver.ast.And;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;
import com.swiggy.ui.pojoClasses.GetAddressPojo;
import scopt.Check;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.awt.*;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;

import framework.gameofthrones.JonSnow.Processor;
import com.swiggy.ui.pojoClasses.*;
import org.openqa.selenium.html5.*;
import java.text.MessageFormat;
import java.util.List;

public class regression {


    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    StfHelper stfHelper;
    Double lat1=12.932815;
    Double lng1=77.603578;

    Double lat2=27.834105;
    Double lng2=76.171977;

    Double lat3=19.229388800000002;
    Double lng3=72.8569977;
    String deviceId;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{Constants.userName,Constants.password}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException{
        //Proxyserver mobProxyTest=new Proxyserver(9091);
        //mobProxyTest.startServer();

        //stfHelper=new StfHelper(Constants.STF_AUTH_TOKEN);
        //deviceId=stfHelper.connectDevice();
        consumerApiHelper=new ConsumerApiHelper();
        inti= new InitializeUI("0cf3f117f1645650");
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login=new LoginPage(inti);
        addressPage=new AddressPage(inti);
        restaurantPage=new RestaurantPage(inti);
        checkoutPage=new CheckoutPage(inti);
        explorePage=new ExplorePage(inti);
        accountPage=new AccountPage(inti);
        paymentPage=new PaymentPage(inti);
        orderDetailPage=new orderDetailPage(inti);
        trackPage=new TrackPage(inti);
        swiggypopPage=new SwiggypopPage(inti);


        }

    @Test(dataProvider = "LoginData", priority = 0, enabled = true,groups={"regression"},description="E2E flow ")
    public void validatePlaceOrderInApp1(String username,String password) {
        Location location = new Location(Constants.lat2, Constants.lng2, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);

        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();

        home.removeSwiggyPop();
        home.selectRestaurant(null, 0);
    }

    @Test(dataProvider = "LoginData", priority = 1, enabled = true,groups={"regression"},description="E2E flow ")
    public void validatePlaceOrderInApp(String username,String password){
        Location location=new Location(Constants.lat2,Constants.lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        //consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        //consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat2),Double.toString(Constants.lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat2),Double.toString(Constants.lng2));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.selectRestaurant(null,0);
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        //restaurantPage.toggleToVegOnly();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat2),Double.toString(lng2),username,password);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        Assert.assertEquals(restaurantPage.getCategoryName(),restaurantMenuObject.getCollection().get(0).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        checkoutPage.proceedToPay();
        paymentPage.clickonCOD();
        AndroidUtil.sleep(5000);
        Assert.assertEquals(trackPage.getOrderStatus(),"Order Received");
        String orderId=trackPage.getOrderId();
        System.out.println(trackPage.getOrderAmount());

       // consumerApiHelper.cancelOrder(username,password,orderId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        //Assert.assertEquals(trackPage.getOrdercancelText(),"Order Cancelled");
        /*Assert.assertEquals(home.getOrderStatusFromHomePageCrouton().toLowerCase(),"Order Received".toLowerCase(),"");
        Assert.assertEquals(home.getOrderTrackStatusFromHomePageCrouton().toLowerCase(),"STATUS".toLowerCase(),"STATUS not found");
        home.clickOnOrderPlacedToolTip();
        AndroidUtil.sleep();
        Assert.assertEquals(trackPage.getOrderId(),orderId);
        consumerApiHelper.cancelOrder(username,password,orderId);
        AndroidUtil.sleep(10000);
        Assert.assertEquals(trackPage.getOrdercancelText(),"Order Cancelled","Cancel text failure");

        //trackPage.getAllText();*/
        }

    @Test(dataProvider = "LoginData",priority = 40,enabled = true,groups = {"regression"},description = "Selecting single address from multiple addresses")
    public void verifySelectSingleAddressFromMultipleAddresses(String username,String password){
        Location location= new Location(lat2,lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);
        consumerApiHelper.deleteAllAddress(Constants.userName, Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName, Constants.password,Double.toString(lat2),Double.toString(lng2),"WORK");
        consumerApiHelper.createNewAddress(Constants.userName, Constants.password,Double.toString(lat2),Double.toString(lng2),"HOME");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat2),Double.toString(lng2));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(10000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        AndroidUtil.sleep(10000);
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat2),Double.toString(lng2),username,password);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        AndroidUtil.sleep(1000);
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        checkoutPage.cartSelectAddress();
        AndroidUtil.sleep(1000);
        checkoutPage.clickOnAddressFromMultipleAddress("Home");
        checkoutPage.proceedToPay();
        paymentPage.clickonCOD();
        AndroidUtil.sleep();
        String orderId=trackPage.getOrderId();
        System.out.println(trackPage.getOrderAmount());
        consumerApiHelper.cancelOrder(username,password,orderId);
        AndroidUtil.sleep();
        Assert.assertEquals(trackPage.getOrdercancelText(),"Order Cancelled");
        trackPage.clickOnOkGotItbutton();


    }











    @Test(dataProvider = "LoginData", priority = 2, enabled = false,groups={"regression"},description="E2E flow after apply filter ")
    public void validateE2EFlowAfterApplyFilter(String username,String password){
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        Location location=new Location(lat1,lng1,0);
        driver.setLocation(location);
        GetAddressPojo allAddress=consumerApiHelper.getAllAddresS(username,password);
        for(int i=0;i<allAddress.getData().getAddresses().size();i++){
            if(allAddress.getData().getAddresses().get(i).getAnnotation().equalsIgnoreCase("work")){
                consumerApiHelper.deleteAddress(username,password,allAddress.getData().getAddresses().get(i).getId());
            }
        }
        List<Menulist> al=consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat1),Double.toString(lng1),"SHOW_RESTAURANTS_WITH:Offers");
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.enterNewAddress("WORK",null);
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("Offers");
        AndroidUtil.sleep();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat1),Double.toString(lng1),username,password);
        AndroidUtil.sleep();
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        Assert.assertEquals(restaurantPage.getCategoryName(),restaurantMenuObject.getCollection().get(0).toString());
        //restaurantPage.clickonAddTo(1);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.increaseItemCount();
        if(checkoutPage.isRepeatCustomizeButtonDisplay()){
            checkoutPage.selectRepeatForIncreaseItemCount();
        }

        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        checkoutPage.proceedToPay();
        //paymentPage.clickonCOD();

    }
    @Test(dataProvider = "LoginData", priority = 3, enabled = false,groups={"regression"},description="E2E flow select carousel ")
    public void verifyE2EFlowFromCarousel(String username,String password){
        String restaurantName=null;
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"WORK");
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        List<Menulist> lcarousel=consumerApiHelper.getCarouselList(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        List<Menulist> lrestaurantList = null;
        Restaurantmenu restaurantMenuObject;
        String firstCarouselType=lcarousel.get(0).getType();
        if(lcarousel.get(0).getType().equalsIgnoreCase("collection")){
            lrestaurantList=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1),lcarousel.get(0).getRestaurantId());
        }
        else{
            lrestaurantList=lcarousel;
        }
        home.clickOnCarouselAndOpenRestaurantFromCollection(0);
        AndroidUtil.sleep();
        if(firstCarouselType.equalsIgnoreCase("collection")){
            AndroidUtil.sleep(5000);
            restaurantName=home.getRestaurantNameFromList(0);
            //Assert.assertEquals(home.getRestaurantNameFromList(0),lrestaurantList.get(0).getRestaurantName());
            home.selectRestaurant(null,0);
            restaurantPage.removeCoachmark();
            Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),restaurantName.toLowerCase());


        }
        else{
            restaurantMenuObject=consumerApiHelper.getRestaurantMenu(lrestaurantList.get(0).getRestaurantId(),lrestaurantList.get(0).getUuid(),Double.toString(Constants.lat1),Double.toString(Constants.lng1),username,password);
            restaurantPage.removeCoachmark();
            Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),restaurantMenuObject.getName().toLowerCase());

        }
        restaurantPage.filterMenu(0);
        //restaurantPage.clickonAddTo(1);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.decreaseItemCount();
        checkoutPage.clickOk();
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        //checkoutPage.changeAddresFromCart(); TODO - Need to add multiple address for one location using api
        checkoutPage.clickOnProceedToPay();

        //paymentPage.clickonCOD();

    }
    @Test(dataProvider = "LoginData", priority = 4, enabled = true,groups={"regression"},description="Sign up flow without referral")
    public void verifySignupWithoutReferral(String username,String password){
        launch.tapOnLogin();
        login.singupWithoutReferral();
        }

    @Test(dataProvider = "LoginData", priority = 5, enabled = true,groups={"regression"},description="Sign up flow with referral")
    public void verifySignupWithReferral(String username,String password){
        launch.tapOnLogin();
        login.singupWithReferral();
    }

    @Test(dataProvider = "LoginData", priority = 6, enabled = false,groups={"regression"},description="verify E2E flow With Filter SwiggyAssured")
    public void    verifyE2EflowWithFilterSwiggyAssured(String username,String password){
        String filterName="SHOW_RESTAURANTS_WITH:Swiggy%20Assured";
        Location location=new Location(lat3,lng3,0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat3),Double.toString(lng3),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat3),Double.toString(lng3),filterName);
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat3),Double.toString(lng3),username,password);

        launch.clickOnSetDeliveryLocation();
        launch.clickonDenyLocationAccess();
        addressPage.tapOnLocationToSearchManually();
        addressPage.searchLocation("Borivali");
        addressPage.clickonBtnConfirmLocation();
        home.removeToolTip();
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("Swiggy Assured");
        AndroidUtil.sleep();
        Assert.assertTrue(home.checkSwiggyAssuredIcon(0));
        home.selectRestaurant(null,0); //TODO- to check for swiggy assured restaurant
        restaurantPage.removeCoachmark();
        Assert.assertTrue(restaurantPage.isSwiggyAssuredIconDisplay());
        Assert.assertEquals(restaurantPage.getRestaurantName(),al.get(0).getRestaurantName());

        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(2).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        //checkoutPage.
        checkoutPage.proceedToPay();



    }

    @Test(dataProvider = "LoginData", priority = 7, enabled = true,groups={"regression"},description="verify add new address,edit & delete")
    public void verifyAddNewAddress(String username,String password){
        consumerApiHelper.deleteAllAddressExceptHomeANDWORK(username,password);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openAccountTab();
       /* accountPage.updateMobileNumber("1123123122");
        driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.pressKeyCode(AndroidKeyCode.BACK);
        accountPage.updateEmailAddress("newEmail@gmail.com");
        driver.pressKeyCode(AndroidKeyCode.BACK);*/
        accountPage.openManageAddress();
        accountPage.clickOnBtnAddNewAddress();
        addressPage.enterNewAddress("other","ATestOrder");
        GetAddressPojo addressObject=consumerApiHelper.getAllAddresS(Constants.userName,Constants.password);
        System.out.println(addressObject.getData().getAddresses());
        Assert.assertTrue(addressObject.getData().getAddresses().toString().contains("ATestOrder"));
        addressPage.editEnteredAddress();
        addressPage.clickonSaveAddress();
        addressPage.deleteEnteredAddress();
        }
    @Test(dataProvider = "LoginData", priority = 8, enabled = true,groups={"regression"},description="verify link wallet & delink wallet")
    public void verifyLinkWallet(String username,String password){
        consumerApiHelper.delinWallet(username,password,"mobikwik");
        consumerApiHelper.delinWallet(username,password,"paytm");
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openAccountTab();
        accountPage.openManagePayment();
        System.out.println(accountPage.getSwiggyMoneyBalance());
        accountPage.linkWallet("paytm");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(),username);

        AndroidUtil.pressBackKey(driver);
        accountPage.linkWallet("freecharge");
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(),username);

        System.out.println(accountPage.getWalledLinkHeaderText());
        AndroidUtil.pressBackKey(driver);
        accountPage.linkWallet("mobikwik");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(),username);
        AndroidUtil.pressBackKey(driver);

    }
    @Test(dataProvider = "LoginData", priority = 9, enabled = true,groups={"regression"},description="verify Older order present ")
    public void verifyOlderOrderInAccounts(String username,String password) {
        String restaurantName;
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openAccountTab();
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 600).moveTo(50, 300).release().perform();
        if (accountPage.checkAnyActiveOrderPresent()) {
            accountPage.clickonTrackOrder(0);
        }
        if (accountPage.checkAnyPastOrderPresent()) {
            restaurantName = accountPage.getRestaurantNameFromMyOrder(0);
            accountPage.openViewOrderDetail(0);
            System.out.println(orderDetailPage.getpageTitle());
            System.out.println(orderDetailPage.getPageSubTitle());
            AndroidUtil.pressBackKey(driver);
            AndroidUtil.sleep();
            accountPage.clickOnReorder(0);
            checkoutPage.clickOk();
            System.out.println(checkoutPage.getRestaurantNameFromCart());
            System.out.println(restaurantName);
            Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(),restaurantName);

        }
    }
    @Test(dataProvider = "LoginData", priority = 10, enabled = false,groups={"regression"},description="verify search dish from explore screen")
    public void verifySearchDishFromExploreScreen(String username,String password) {
        Location location=new Location(lat1,lng1,0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat1),Double.toString(lng1),"WORK");
        String restaurantName;
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openExploreTab();
        String restaurantname=explorePage.searchDishesAndAddToCart("Idli");
        explorePage.clickonFullMenu();
        restaurantPage.removeCoachmark();
        Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),restaurantname.toLowerCase());
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(),restaurantname.toLowerCase());
        checkoutPage.clickOnProceedToPay();
        }
    @Test(dataProvider = "LoginData", priority = 11, enabled = true,groups={"regression"},description="verify search restaurant from explore screen")
    public void verifySearchRestaurantFromExploreScreen(String username,String password) {
        consumerApiHelper.deleteAllAddress(username,password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat2),Double.toString(lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat2),Double.toString(lng2));
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openExploreTab();
        //explorePage.searchDishesAndAddToCart("Test Restaurant Automation2");
        explorePage.searchRestaurant(al.get(1).getRestaurantName());
        String restaurantName=home.getRestaurantNameFromList(0);
        explorePage.openRestaurantAndAddTocCart(0);
        TouchAction touchAction = new TouchAction(driver);
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();
        touchAction.longPress(50, 300).moveTo(50, 600).release().perform();
        Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(),al.get(1).getRestaurantName().toLowerCase());
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProceedToPay();
    }

    //Verify Apply coupon
    @Test(dataProvider = "LoginData", priority = 12, enabled = true,groups={"regression"},description="verify apply coupon from cart")
    public void verifyApplyCouponFromCart(String username,String password) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat1),Double.toString(Constants.lng1),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        //restaurantPage.toggleToVegOnly();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat2),Double.toString(lng2),username,password);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        Assert.assertEquals(restaurantPage.getCategoryName(),restaurantMenuObject.getCollection().get(0).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.increaseItemCount();
        if(checkoutPage.isRepeatCustomizeButtonDisplay()){
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        checkoutPage.increaseItemCount();
        if(checkoutPage.isRepeatCustomizeButtonDisplay()){
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        checkoutPage.applyCoupon("KTK10X");
        Assert.assertEquals(checkoutPage.getCouponAppliedDialogTitle().toLowerCase(),"Coupon Applied".toLowerCase());
        checkoutPage.clickOnOK();
        checkoutPage.proceedToPay();
    }

    //Mark Faviou
    @Test(dataProvider = "LoginData", priority = 13, enabled = false,groups={"regression"},description="verify mark favourite ")
    public void verifyMarkFavourite(String username,String password) {
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress(username,password);
        consumerApiHelper.createNewAddress(username,password,Double.toString(lat2),Double.toString(lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat2),Double.toString(lng2));
        consumerApiHelper.removeFavs(username,password,Double.toString(lat2),Double.toString(lng2));

        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        System.out.println("RestaurantName = "+al.get(0).getRestaurantName());
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        String restaurantName=restaurantPage.getRestaurantName();
        restaurantPage.clickonFavIcon();
        AndroidUtil.pressBackKey(driver);
        home.openAccountTab();
        accountPage.openFavorities();
        AndroidUtil.sleep(5000);
        System.out.println(home.getRestaurantNameFromList(0));
        Assert.assertEquals(home.getRestaurantNameFromList(0).toLowerCase(),restaurantName.toLowerCase());
    }

    //referral flow
    @Test(dataProvider = "LoginData", priority = 14, enabled = true,groups={"regression"},description="verify referral Flow")
    public void verifyReffralFlow(String username,String password) {
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat2),Double.toString(lng2),"WORK");
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.openAccountTab();
        accountPage.openReferral();
        accountPage.clickonInviteButton();
        AndroidUtil.sleep();
        accountPage.clickOncopytoClipboard();
        String str=AndroidUtil.runShellCommand("adb -s "+System.getenv("deviceId")+" shell am broadcast -a clipper.get").toString();
        String inviteString=str.substring(str.lastIndexOf("=")+1).replaceAll("^\"|\"$", "");
        System.out.println(inviteString);
        //TODO - Api to validate data and link



        }
        //Offer flow
    @Test(dataProvider = "LoginData", priority = 15, enabled = true,groups={"regression"},description="verify offerFlow")
    public void verifyOfferFlow(String username,String password) {
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat2),Double.toString(lng2),"WORK");
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.openAccountTab();
        accountPage.openOffers();
        }
        //Validate Veg toggle option on restaurantPage
        @Test(dataProvider = "LoginData", priority = 16, enabled = true,groups={"regression"},description="Validate Veg toggle option on restaurantPage")
    public void verifyVegToggleInRestaurantPage(String username,String password) {
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat2),Double.toString(Constants.lng2));
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(Constants.lat2),Double.toString(Constants.lng2),username,password);
        String nonVegItem=consumerApiHelper.getFirstNonVegItem(restaurantMenuObject);
        Location location=new Location(Constants.lat2,Constants.lng2,0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.toggleToVegOnly();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant(nonVegItem);
        Assert.assertNotEquals(restaurantPage.getRestaurantName(),nonVegItem);
    }
    //Validate Rating, time and price on two on ,TD restaurantpage ANd
    @Test(dataProvider = "LoginData", priority = 17, enabled = true,groups={"regression"},description="Validate Rating, time and price on two on ,TD restaurantpage")
    public void validateRatingAndSLAOnRestaurantPage(String username,String password) {
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat1),Double.toString(lng1));
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat1),Double.toString(lng1),username,password);
        Location location=new Location(lat1,lng1,0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(10000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        SoftAssert asserttest=new SoftAssert();
        Assert.assertEquals(restaurantPage.getRestaurantRating(),restaurantMenuObject.getAvgRating(),"Fail");
        Assert.assertEquals(restaurantPage.getDeliveryTime().toLowerCase(),restaurantMenuObject.getSlaString().toLowerCase(),"Fail1");
        Assert.assertEquals(restaurantPage.getCostOfTwo(),restaurantMenuObject.getCostForTwoMsg()/100,"Fail2");
        //asserttest.assertAll();
    }
    //Validate search on restaurant page
    @Test(dataProvider = "LoginData", priority = 18, enabled = true,groups={"regression"},description="Validate search on restaurant Page")
    public void verifySearchOnRestaurantPage(String username,String password) {
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat1),Double.toString(lng1));
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat1),Double.toString(lng1),username,password);
        Location location=new Location(lat1,lng1,0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        AndroidUtil.sleep();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant(restaurantMenuObject.getItems().get(2));
        AndroidUtil.sleep(5000);
        Assert.assertEquals(restaurantPage.getItemName(0),restaurantMenuObject.getItems().get(2));
       // restaurantPage.searchInRestaurant(restaurantMenuObject.get);
        addressPage.hideKeyboard();
        AndroidUtil.pressBackKey(driver);
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant("wrwr22342342#@$@#$@#$");
        Assert.assertNotEquals(restaurantPage.getRestaurantName(),"wrwr22342342#@$@#$@#$");




    }
    //Validate Veg only icon on restaurat page
    @Test(dataProvider = "LoginData", priority = 19, enabled = true,groups={"regression"},description="Validate veg only icon on restaurant page")
    public void verifyVegOnlyIconOnRestaurantPage(String username,String password) {
        Location location=new Location(lat1,lng1,0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("Pure Veg");
        AndroidUtil.sleep();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        Assert.assertTrue(restaurantPage.isPureVegIconDisplay());

    }

    // sort restaurant based
    @Test(dataProvider = "LoginData", priority = 20, enabled = true,groups={"regression"},description="verify sort restaurant list based on Delivery time ")
    public void verifySorting(String username,String password) {
        Location location=new Location(lat1,lng1,0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat1),Double.toString(lng1),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantListBasedOnSort(Constants.userName,Constants.password,Double.toString(lat1),Double.toString(lng1),"DELIVERY_TIME");
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.clickonSortRestaurant();
        home.selectSorting(Constants.DELIVERYTIME_SORT);
        AndroidUtil.sleep(5000);
        Assert.assertEquals(home.getRestaurantNameFromList(0).toLowerCase(),al.get(0).getRestaurantName().toLowerCase());

    }

    //add other restaurant item
    @Test(dataProvider = "LoginData", priority = 21, enabled = true,groups={"regression"},description="verify add other restaurantItem")
    public void verifyAddOtherRestaurantItem(String username,String password) {
        Location location=new Location(Constants.lat2,Constants.lng2,0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(Constants.lat2),Double.toString(Constants.lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(Constants.lat2),Double.toString(Constants.lng2));
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.selectRestaurant(al.get(0).getRestaurantName(),0);
        restaurantPage.removeCoachmark();
        String firstrestaurantName=restaurantPage.getRestaurantName();
        Restaurantmenu restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(),al.get(0).getUuid(),Double.toString(lat1),Double.toString(lng1),username,password);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        restaurantPage.addItemtest();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        home.selectRestaurant(al.get(1).getRestaurantName(),0);
        AndroidUtil.sleep(5000);
        String secondRestaurantname=restaurantPage.getRestaurantName();
        restaurantMenuObject=consumerApiHelper.getRestaurantMenu(al.get(1).getRestaurantId(),al.get(1).getUuid(),Double.toString(lat1),Double.toString(lng1),username,password);
        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(0).toString());
        restaurantPage.clickonAddToCartForReplaceItem();
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle().toLowerCase(),"Replace cart item?".toLowerCase());
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(),MessageFormat.format("Your cart contains dishes from {0}. Do you want to discard the selection and add dishes from {1}?",firstrestaurantName,secondRestaurantname).toLowerCase());
        restaurantPage.clickonReplaceCartItemDialogYes();
        restaurantPage.clickonCustomButtonForAddTo();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toLowerCase(),secondRestaurantname.toLowerCase());
        checkoutPage.proceedToPay();

        }


        //EVDO
     @Test(dataProvider = "LoginData", priority = 22, enabled = true,groups={"regression"},description="verify EDVO")
    public void verifyEDVOMeal(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1),"CUISINES:Pizzas");
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("Pizzas");
        home.selectRestaurantNew("Domino's Pizza", 0);
        restaurantPage.removeCoachmark();
        String firstrestaurantName = restaurantPage.getRestaurantName();
         List<RestaurantPageEDVO> ledvoOnRestaurantPage=consumerApiHelper.getEDVOListForRestaurantPage(Constants.userName,Constants.password,"23798",Double.toString(lat1),Double.toString(lng1));

         restaurantPage.openEDVOMeal(0);
        EDVOMeal edvoMeal=consumerApiHelper.getEDVOMealObject("23798",Integer.toString(ledvoOnRestaurantPage.get(0).getId()));
        restaurantPage.validateEDVOLandingScreen(edvoMeal);
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.validatePizzaScreen(edvoMeal,0);
        String itemName_1=restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.validatePizzaScreen(edvoMeal,1);
        String itemName_2=restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.clickOnViewCart();
        AndroidUtil.sleep(10000);
        //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toString().toLowerCase(),"Domino's Pizza".toLowerCase());
        Assert.assertEquals(checkoutPage.getItemNameFromCart(0).toLowerCase(),edvoMeal.getMealName().toLowerCase());


    }
    //preorder
    @Test(dataProvider = "LoginData", priority = 23, enabled = true,groups={"regression"},description="verify preorder without login ")
    public void verifyPreorderWithoutLogin(String username,String password) {
        List<Menulist> al=consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat1),Double.toString(Constants.lng1));
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        Date expiry = new Date( al.get(0).getPreorderdate().get(1) );
        System.out.println(expiry);
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPopForPreorder();
        //home.removeSwiggyPop();
        //home.clickonpreorderDialog();
        home.validateToolTipData();
        home.clickonTryNowPreorderButtonFromToolTip();
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        Assert.assertEquals(home.getPreorderDeliveryTimeText(0).toLowerCase(),"As Soon As Possible".toLowerCase());
        home.validateDaysStringForPreorder(al);
        home.selectPreorderDeliveryTimeText(0);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        Assert.assertEquals(home.getPreorderSlotTime().toLowerCase(),"ASAP".toLowerCase());
        home.clickOnPreorderSlot();
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
    }

    @Test(dataProvider = "LoginData", priority = 24, enabled = true,groups={"regression"},description="preorder with next day delivery time")
    public void verifyPreorderWithNextDayTime(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPopForPreorder();
        //home.removeSwiggyPop();
        //home.clickonpreorderDialog();
        home.clickonTryNowPreorderButtonFromToolTip();
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        Assert.assertEquals(home.getPreorderDeliveryTimeText(0).toLowerCase(),"As Soon As Possible".toLowerCase());
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(1);
        String selectedTime=home.getPreorderDeliveryTimeText(1);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        Assert.assertEquals(home.getPreorderSlotTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        home.selectRestaurant(null,0);
        Assert.assertEquals(restaurantPage.getDeliveryTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);
        checkoutPage.proceedToPay();



    }
    //preorder
    @Test(dataProvider = "LoginData", priority = 25, enabled = true,groups={"regression"},description="change time slot from restatuant page for preorder")
    public void verifyChangePreorderSlotFromRestaurantPage(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPopForPreorder();
        //home.clickonpreorderDialog();
        home.clickonTryNowPreorderButtonFromToolTip();
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(1);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        String slotTime=home.getPreorderSlotTime();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        home.selectDateTabForPreorder(0);
        home.selectPreorderDeliveryTimeText(0);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.sleep();
        Assert.assertNotEquals(restaurantPage.getDeliveryTime(),slotTime);

        restaurantPage.clickonPreOrderTimeSlot();
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(2);
        String selectedTime=home.getPreorderDeliveryTimeText(2);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.sleep();
        Assert.assertEquals(restaurantPage.getDeliveryTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);
        checkoutPage.proceedToPay();




    }
    //preorder
    @Test(dataProvider = "LoginData", priority = 26, enabled = true,groups={"regression"},description="change delivery slot from cart for preorder ")
    public void verifyChangeDeliverySlotFromCart(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPopForPreorder();
        //home.removeSwiggyPop();
        //home.clickonpreorderDialog();
        home.clickonTryNowPreorderButtonFromToolTip();
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(1);
        String selectedTime=home.getPreorderDeliveryTimeText(1);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        String slotTime=home.getPreorderSlotTime();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);
        checkoutPage.clickonPreorderSla();
        Assert.assertEquals(home.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());

        home.selectDateTabForPreorder(0);
        home.selectPreorderDeliveryTimeText(0);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        checkoutPage.clickOk();
        Assert.assertNotEquals(checkoutPage.getPreorderSla(),slotTime);
        checkoutPage.clickonPreorderSla();
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(0);
        selectedTime=home.getPreorderDeliveryTimeText(0);
        dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);
        checkoutPage.proceedToPay();
        }
    //preorder
    @Test(dataProvider = "LoginData", priority = 27, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyChangeDeliverySlotFromCartItemUnavailable(String username,String password) {
        Location location = new Location(Constants.lat4, Constants.lng4, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat4), Double.toString(Constants.lng4), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat4), Double.toString(Constants.lng4));
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPopForPreorder();
        //home.removeSwiggyPop();
        //home.clickonpreorderDialog();
        home.clickonTryNowPreorderButtonFromToolTip();
        home.selectDateTabForPreorder(0);
        home.selectPreorderDeliveryTimeText(0);
        String selectedTime=home.getPreorderDeliveryTimeText(0);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        String slotTime=home.getPreorderSlotTime();
        home.selectRestaurantNew("Sangeetha Veg Restaurant",0);
        restaurantPage.removeCoachmark();
        //restaurantPage.filterMenu("Breakfast");
        AndroidUtil.sleep();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant("Idli");
        restaurantPage.addItemtest();
        AndroidUtil.hideKeyboard(driver);
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        AndroidUtil.sleep();
        checkoutPage.clickonPreorderSla();
        home.selectDateTabForPreorder(1);
        home.scrolltillLast();
        home.selectPreorderDeliveryTimeText(0);
        home.clickOnSetDeliveryTimeButtonForPreorder();
        checkoutPage.clickOk();
        AndroidUtil.sleep();
        Assert.assertTrue(checkoutPage.checkRemoveUnavailableButtonDisplay());
        checkoutPage.clickOnRemoveUnavailableButton();
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getEmptyCartTitle(),Constants.EMPTYCART_TITLE);

        }
    @Test(dataProvider = "LoginData", priority = 28, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyPreorderNuxForLoginUser(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithPreorder(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPopForPreorder();
        home.clickOnASAPOptionInPreorderNux();
        home.selectDateTabForPreorder(1);
        home.selectPreorderDeliveryTimeText(0);
        String selectedTime=home.getPreorderDeliveryTimeText(0);
        ArrayList<String> dayOfPreorder=home.getDayForPreorder();
        home.clickOnSetDeliveryTimeButtonForPreorder();
        home.clickOnClosePreorderToolTip();
        Assert.assertEquals(home.getPreorderSlotTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        Assert.assertEquals(restaurantPage.getDeliveryTime(),dayOfPreorder.get(1).split(" ")[0]+","+selectedTime.split("-")[1]);
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        AndroidUtil.sleep();
        Assert.assertEquals(checkoutPage.getPreorderSla(),dayOfPreorder.get(1).split(" ")[0]+", "+selectedTime);
        checkoutPage.proceedToPay();
        }
    @Test(dataProvider = "LoginData", priority = 29, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyChainRestaurants(String username,String password) {
        String firstChainRestaurantName=null;
        int chainResturantCount=0;
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1),"CUISINES:American");
        HashMap<String,Integer> hm=new HashMap<>();
        for(int i=0;i<al.size();i++){
            if(al.get(i).getChainCount()>0){
                System.out.println(al.get(i).getRestaurantName());
                firstChainRestaurantName=al.get(i).getRestaurantName();
                chainResturantCount=al.get(i).getChainCount()+1;
                //hm.put(firstChainRestaurantName,chainResturantCount);
                break;
            }
        }
        System.out.println("Hello"+firstChainRestaurantName);
        System.out.println("Hello"+chainResturantCount);
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestaurantWith("American");
        String chainRestaurantText=home.openRestaurantFromList(firstChainRestaurantName,0);
        System.out.println(chainRestaurantText);
        AndroidUtil.sleep();
        Assert.assertEquals(chainRestaurantText,chainResturantCount+" outlets near you");
        Assert.assertEquals(home.getChainRestaurantPopupHeader(),"Choose \""+firstChainRestaurantName+"\" Outlet");
        Assert.assertEquals(home.getChainRestaurantNumberOfOutletText(),chainRestaurantText);
        Assert.assertEquals(home.getChainRestaurantList().size(),Integer.parseInt(home.getChainRestaurantNumberOfOutletText().split(" ")[0]));
    }
    @Test(dataProvider = "LoginData", priority = 30, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyLogOut(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.selectRestaurant(null,0);
        restaurantPage.removeCoachmark();
        restaurantPage.filterMenu(0);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        AndroidUtil.pressBackKey(driver,2);
        home.openAccountTab();
        accountPage.clickonLogout();
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage(),Constants.LOGOUT_MESSAGE);
        restaurantPage.clickonReplaceCartItemDialogNO();
        Assert.assertTrue(accountPage.checkLogoutButtonDisplay());
        accountPage.clickonLogout();
        restaurantPage.clickonReplaceCartItemDialogYes();
        Assert.assertTrue(accountPage.checkLoginButtonDisplay());
        home.openCartTab();
        Assert.assertEquals(checkoutPage.getEmptyCartTitle(),Constants.EMPTYCART_TITLE);
        }
    @Test(dataProvider = "LoginData", priority = 31, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyLoginFromAccountTab(String username,String password) {
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress(Constants.userName,Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName,Constants.password,Double.toString(lat2),Double.toString(lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat2),Double.toString(lng2));
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        home.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        Assert.assertEquals(accountPage.getRegisteredMobileNumber(),username);
    }
    @Test(dataProvider = "LoginData", priority = 32, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyResetFilter(String username,String password) {
        Location location=new Location(lat2,lng2,0);
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openFilter();
        home.filterRestauranByPosition(0, "Andhra");
        //Assert.assertTrue(home.checkFilterApplied());
        AndroidUtil.sleep(10000);
        home.openFilter();
        Assert.assertEquals(home.isFilterCheckboxSelected(0),"true");
        System.out.println(home.isFilterCheckboxSelected(0));
        Assert.assertFalse(home.btn_applyFilter.isEnabled());
        home.clickonResetFilter();
        Assert.assertEquals(home.isFilterCheckboxSelected(0),"false");
        Assert.assertTrue(home.btn_applyFilter.isEnabled());
        home.clickonApplyFilterButton();
        //Assert.assertFalse(home.checkFilterApplied());
    }
    @Test(dataProvider = "LoginData", priority = 33, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void IncreaseItemCount(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        driver.closeApp();
        driver.launchApp();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openExploreTab();
        explorePage.searchRestaurant("Domino");
        AndroidUtil.sleep();
        home.selectRestaurantNew(Constants.dominosRestaurantName, 0);
        restaurantPage.removeCoachmark();
        restaurantPage.filterMenu(0);
        restaurantPage.clickOnAddToForDominos();
        checkoutPage.increaseItemCount();
        restaurantPage.clickOnIwillChosseButton();
        restaurantPage.chooseCustimization(1);
        restaurantPage.custForDominos();
        //Assert.assertEquals(checkoutPage.getItemCount(), 2);
        checkoutPage.decreaseItemCount();
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle(), Constants.MULTIPLE_CUST_REMOVEITEM_DIALOG_TITLE);
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage(), Constants.MULTIPLE_CUST_REMOVEITEM_DIALOG_MESSAGE);
        restaurantPage.clickonReplaceCartItemDialogNO();
        //Assert.assertEquals(checkoutPage.getItemCount(), 2);
        checkoutPage.decreaseItemCount();
        restaurantPage.clickonReplaceCartItemDialogYes();
        checkoutPage.clickOk();
       // Assert.assertEquals(checkoutPage.cartItemName.size(), 2);

    }
    @Test(dataProvider = "LoginData", priority = 34, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifyForNonServicableLocation(String username,String password) {
        Location location=new Location(Constants.lat_nonServicable,Constants.lng_nonServicable,0);
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.removeSwiggyPop();
        Assert.assertEquals(home.getNotServicableLocationText().toLowerCase(),Constants.LOCATION_NOT_SERVICABLE.toLowerCase());
        home.clickOnEditLocationButton();
        addressPage.searchLocation("Borivali");
        addressPage.clickonBtnConfirmLocation();
        home.removeToolTip();




    }
    @Test(dataProvider = "LoginData", priority = 35, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPop(String username,String password) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.removeSwiggyPop();
        home.openSwiggyPop();
       // Assert.assertEquals(swiggypopPage.getpopHeader(), "Introducing Swiggy Pop");
        swiggypopPage.clickOnFirstPOPItem();

        if (popObj.get(0).isOpened()) {
            Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),popObj.get(0).getName().trim());
            Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+popObj.get(0).getRestaurantName());
            System.out.println(swiggypopPage.getItemNameFromDetailView());
            System.out.println(swiggypopPage.getItemDetailFromDetailView());
            swiggypopPage.clickonConfirmAddressAndProceed();
            swiggypopPage.clickonProceedToPay();
        } else {
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }

    @Test(dataProvider = "LoginData", priority = 36, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPopWithoutAddress(String username,String password) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(username, password);
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        addressPage.selectUserCurrentLocation();
        home.openSwiggyPop();
        //Assert.assertEquals(swiggypopPage.getpopHeader(),"Introducing Swiggy Pop");
        swiggypopPage.clickOnFirstPOPItem();
        //Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),poplist.get(0).getName().trim());
        //Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+poplist.get(0).getRestaurantName());
        if (popObj.get(0).isOpened()) {
            Assert.assertEquals(swiggypopPage.getCartAddressHeader(), "You seem to be in a new location!");
            swiggypopPage.clickonAddAddressAndProceed();
            addressPage.enterNewAddress("WORK", null);
            swiggypopPage.clickonConfirmAddressAndProceed();
            swiggypopPage.clickonProceedToPay();
        }
        else{
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }

    @Test(dataProvider = "LoginData", priority = 37, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPopWithoutLogin(String username,String password) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep();
        addressPage.clickonButtonAddMoreDetail();
        addressPage.clickonSkipAddress();
        home.openSwiggyPop();
        //Assert.assertEquals(swiggypopPage.getpopHeader(),"Introducing Swiggy Pop");
        swiggypopPage.clickOnFirstPOPItem();
        if (popObj.get(0).isOpened()) {
            Assert.assertFalse(swiggypopPage.checkElementDisplay(swiggypopPage.txt_confirmAddressAndProceed, null));
            checkoutPage.clickOnContinoueButton();
            login.LogintoApp(username, password);
            //Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),poplist.get(0).getName().trim());
            //Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+poplist.get(0).getRestaurantName());
            swiggypopPage.clickonConfirmAddressAndProceed();
            swiggypopPage.clickonProceedToPay();
        }
        else{
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
            }
    }
    @Test(dataProvider = "LoginData", priority = 38, enabled = true,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPopPressBackKey(String username,String password) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        //consumerApiHelper.deleteAllAddress(username, password);
        //consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.openSwiggyPop();
        swiggypopPage.clickOnFirstPOPItem();
        if (popObj.get(0).isOpened()) {
            Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),popObj.get(0).getName().trim());
            Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+popObj.get(0).getRestaurantName());
            swiggypopPage.clickonBackButton();
            AndroidUtil.sleep();
            Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle(), Constants.SWIGGYPOP_CLEARCART_DIALOG_TITLE);
            Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage(), Constants.SWIGGYPOP_CLEARCART_DIALOG_MESSAGE);
            restaurantPage.clickonReplaceCartItemDialogYes();
            Assert.assertTrue(swiggypopPage.checkElementDisplay(swiggypopPage.image_firstItem, null));
        }
        else{

        }
    }
    @Test(dataProvider = "LoginData", priority = 39, enabled = false,groups={"regression"},description="validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPopwithPreferredPayment(String username,String password) {

    }



    @AfterMethod
    public void tearDown(){
        driver.closeApp();
        driver.launchApp();
    }



    @AfterClass
    public void stopServer(){
        //inti.appiumServer.stopServer();
        stfHelper.disconnectDeviceFromSTF(deviceId);
    }












}
