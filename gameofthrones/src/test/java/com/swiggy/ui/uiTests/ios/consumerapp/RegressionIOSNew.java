package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.*;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import org.testng.asserts.SoftAssert;

import java.net.UnknownHostException;
import java.util.List;


public class RegressionIOSNew {
    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    DishDiscoveryPage dishDiscoveryPage;

    Double lat1 = 12.932815;
    Double lng1 = 77.603578;

    Double lat2 = 27.834105;
    Double lng2 = 76.171977;

    Double lat3 = 19.229388800000002;
    Double lng3 = 72.8569977;


    DishDiscovery dishDiscoveryObject;
    DisDiscoveryStoryBoard ddStoryBoard;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9950611829", "qwerty123"}};
    }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
        dishDiscoveryPage = new DishDiscoveryPage(inti);


    }

    //Login to App
    @Test(dataProvider = "LoginData", priority = 0, enabled = true, groups = {"regression"}, description = "Verify Login scenario from Account tab and Validate Loggedin User")
    public void verifyLoginFromAccountTab(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        System.out.println("ios driver" + driver);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(5000);
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        restaurantPage.clickOnBackButton();
        restaurantPage.clickOnBackButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        accountPage.getTextMobileNumber();
        Assert.assertTrue(accountPage.ValidateRegisteredMobileNumberios(username));
    }


    //Mark Faviourite
    @Test(dataProvider = "LoginData", priority = 1, enabled = true, groups = {"regression"}, description = "verify mark favourite ")
    public void verifyMarkFavourite(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        System.out.println("2nd test" + driver.getSessionId());
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat2), Double.toString(lng2), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat2), Double.toString(lng2));
        consumerApiHelper.removeFavs(username, password, Double.toString(lat2), Double.toString(lng2));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        homePage.openNearMeTab();
        homePage.selectRestaurant(null, 1);
        Assert.assertTrue(checkElementDisplay(restaurantPage.fav_icon),"Favourite Icon is not showing in restaurant Menu");
        restaurantPage.clickonFavIcon();
        String restaurantName = restaurantPage.getRestaurantName();
        restaurantPage.clickOnBackButton();
        homePage.openAccountTab();
        accountPage.openFavorities();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        System.out.println(homePage.getRestaurantNameFromList(0));
        Assert.assertEquals(homePage.getRestaurantNameFromList(0), restaurantName);

    }

    //Refferal flow
    @Test(dataProvider = "LoginData", priority = 2, enabled = true, groups = {"regression"}, description = "verify referral Flow")
    public void verifyOfferFlowAndRefferalFlow(String username, String password) {
        Location location = new Location(lat2, lng2, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat2), Double.toString(lng2), "WORK");
        launch.clickonAllowButton();
        homePage.openCartTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        //Verify Offers flow
        accountPage.openOffers();
        //OLD OFFERS PAGE
        accountPage.validateoffersFromDeeplink("Offers");
        restaurantPage.clickOnBackButton();
        //Verify Refferal flow
        accountPage.openReferral();
        accountPage.clickonInviteButton();


    }

    @Test(dataProvider = "LoginData", priority = 3, enabled = true, groups = {"regression"}, description = "Sign up flow without referral")
    public void verifySignupWithoutReferral(String username, String password) {
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.singupWithoutReferral();
    }

    //Validate Veg toggle option on restaurantPage
    @Test(dataProvider = "LoginData", priority = 4, enabled = true, groups = {"regression"}, description = "Validate Veg toggle option on restaurantPage")
    public void verifyVegToggleInRestaurantPage(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);
        String nonVegItem = consumerApiHelper.getFirstNonVegItem(restaurantMenuObject);
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        int counter = 1;
        while (restaurantPage.validatePureVegIconDisplay() == true && counter <= 5) {
            restaurantPage.clickOnBackButton();
            AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
            homePage.selectRestaurant(null, 1);
            counter++;
        }
        Assert.assertTrue(checkElementDisplay(restaurantPage.btn_toggleVeg), "Veg toggle is not displayed");
        restaurantPage.toggleToVegOnly();

    }


    @Test(dataProvider = "LoginData", priority = 4, enabled = true, groups = {"regression"}, description = "Verify ReOrder Flow")
    public void VerifyReOrderFlow(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        String restaurantName;
        launch.clickonAllowButton();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        AndroidUtil.sleep(5000);
        if (accountPage.checkAnyPastOrderPresent()) {
            restaurantName = accountPage.getRestaurantNameFromMyOrder(0);
            accountPage.openViewOrderDetail(0);
            accountPage.clickReorderbutton();
            System.out.println(checkoutPage.getRestaurantNameFromCart());
            System.out.println(restaurantName);
            Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(), restaurantName);
        } else {
            System.out.println("There are no old orders for this user");
        }
    }


    @Test(dataProvider = "LoginData", priority = 5, enabled = true, groups = {"regression"}, description = "verify link wallet & delink wallet")
    public void verifyLinkWallet(String username, String password) {
       consumerApiHelper.delinWallet(username, password, "mobikwik");
       consumerApiHelper.delinWallet(username, password, "paytm");
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        accountPage.openManagePayment();
        accountPage.linkWallet("paytm");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        accountPage.pressBackKeyFromLinkWalltetPage();
        accountPage.linkWallet("freecharge");
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        System.out.println(accountPage.getWalledLinkHeaderText());
        accountPage.pressBackKeyFromLinkWalltetPage();
        accountPage.linkWallet("mobikwik");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        accountPage.pressBackKeyFromLinkWalltetPage();
    }


    @Test(dataProvider = "LoginData", priority = 6, enabled = true, groups = {"regression"}, description = "verify add other restaurantItem")
    public void verifyAddOtherRestaurantItem(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        System.out.println("RestaurantName = " + al.get(0).getRestaurantName());
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        String firstrestaurantName = restaurantPage.getRestaurantName();
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);
        restaurantPage.filterMenu(1);
        restaurantPage.addItemtest();
        restaurantPage.clickOnBackButton();
        AndroidUtil.sleep(5000);
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        String secondRestaurantname = restaurantPage.getRestaurantName();
        restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(1).getRestaurantId(), al.get(1).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);
        restaurantPage.filterMenu(1);
        restaurantPage.addItemtest();
        Assert.assertEquals(restaurantPage.getReplaceCartItemDialogTitle().toLowerCase(), "items already in cart".toLowerCase());
        restaurantPage.clickonReplaceCartItemDialogYes();
        //Assert.assertEquals(restaurantPage.getReplaceCartItemDialogMessage().toLowerCase(),MessageFormat.format("your cart contains items from other restaurant. would you like to reset your cart before browsing this restaurant?",firstrestaurantName,secondRestaurantname).toLowerCase());

        //TODO- validation of clear cart*/
    }

    @Test(dataProvider = "LoginData", priority = 7, enabled = true, groups = {"regression"}, description = "verify Older order present")
    public void verifyOlderOrderInAccounts(String username, String password) {
        String restaurantName;
        launch.clickonAllowButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        AndroidUtil.sleep(5000);

        if (accountPage.checkAnyActiveOrderPresent()) {
            accountPage.clickonTrackOrder(0);
        }
        if (accountPage.checkAnyPastOrderPresent()) {
            restaurantName = accountPage.getRestaurantNameFromMyOrder(0);
            accountPage.openViewOrderDetail(0);
            System.out.println(orderDetailPage.getpageTitle());
            System.out.println(orderDetailPage.getPageSubTitle());
            AndroidUtil.pressBackKey(driver);
            accountPage.clickOnReorder(0);
            checkoutPage.clickOk();
            System.out.println(checkoutPage.getRestaurantNameFromCart());
            System.out.println(restaurantName);
            Assert.assertEquals(checkoutPage.getRestaurantNameFromCart(), restaurantName);

        }
    }

    //Validate search on restaurant page
    @Test(dataProvider = "LoginData", priority = 8, enabled = true, groups = {"regression"}, description = "Validate search on restaurant Page")
    public void verifySearchOnRestaurantPage(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);

        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        restaurantPage.removeCoachmark();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant(restaurantMenuObject.getItems().get(2));
        Assert.assertEquals(restaurantPage.getItemName(0), restaurantMenuObject.getItems().get(2));
        //restaurantPage.searchInRestaurant(restaurantMenuObject.get);
        addressPage.hideKeyboard();
        restaurantPage.pressBackFromSearchScreen();
        restaurantPage.openSearch();
        restaurantPage.searchInRestaurant("wrwr22342342#@$@#$@#$");
        Assert.assertNotEquals(restaurantPage.getRestaurantName(), "wrwr22342342#@$@#$@#$");


    }

    //Validate Rating, time and price on two on ,TD restaurantpage ANd
    @Test(dataProvider = "LoginData", priority = 9, enabled = true, groups = {"regression"}, description = "Validate Rating, time and price on two on ,TD restaurantpage")
    public void validateRatingAndSLAOnRestaurantPage(String username, String password) {
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat1), Double.toString(lng1));
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        restaurantPage.removeCoachmark();
        SoftAssert asserttest = new SoftAssert();
        Assert.assertEquals(restaurantPage.getRestaurantRating().split(" ")[1], restaurantMenuObject.getAvgRating(), "Fail");
        Assert.assertEquals(restaurantPage.getDeliveryTime().toLowerCase(), restaurantMenuObject.getSlaString().toLowerCase(), "Fail1");
        Assert.assertEquals(restaurantPage.getCostOfTwo(), restaurantMenuObject.getCostForTwoMsg() / 100, "Fail2");
        asserttest.assertAll();
    }

    @Test(dataProvider = "LoginData", priority = 10, enabled = true, groups = {"regression"}, description = "verifySwiggyPop")
    public void verifySwiggyPop(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(Constants.userName, Constants.password);
        homePage.openNearMeTab();
        homePage.openSwiggyPop();
        swiggypopPage.clickOnFirstPOPImageIos();
        if (!popObj.isEmpty() && popObj.get(0).isOpened()) {

            swiggypopPage.clickonConfirmAddressAndProceed();
            //swiggypopPage.clickonProceedToPay();

        } else {
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());

        }
    }

    @Test(dataProvider = "LoginData", priority = 11, enabled = true, groups = {"regression"}, description = "verify add other restaurantItem")
    public void validatePlaceOrderInApp(String username, String password) {
        Location location = new Location(lat2, lng2, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat2), Double.toString(lng2), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(lat2), Double.toString(lng2));
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        checkoutPage.proceedToPay();
        //COD disabled
        paymentPage.clickonCOD();

        /*AndroidUtil.sleep();
        String orderId = trackPage.getOrderId();
        System.out.println(trackPage.getOrderAmount());
        consumerApiHelper.cancelOrder(username, password, orderId);
        AndroidUtil.sleep();
        Assert.assertEquals(trackPage.getOrdercancelText(), "Order Cancelled");
        trackPage.clickOnOkGotItbutton();
        //trackPage.getAllText();*/
    }

    @Test(dataProvider = "LoginData", priority = 12, enabled = true, groups = {"regression"}, description = "verifySwiggyPopWithoutAddress")
    public void verifySwiggyPopWithoutAddress(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        consumerApiHelper.deleteAllAddress(username, password);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        homePage.openNearMeTab();
        homePage.openSwiggyPop();
        swiggypopPage.clickOnFirstPOPImageIos();
        if (!popObj.isEmpty() && popObj.get(0).isOpened()) {
            Assert.assertEquals(swiggypopPage.getCartAddressHeader(), "You seem to be in a new location!");
            swiggypopPage.clickonAddAddressAndProceed();
            addressPage.enterNewAddress("WORK", null);
            swiggypopPage.clickonConfirmAddressAndProceed();
            // swiggypopPage.clickonProceedToPay();
        } else {
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());

        }
    }


    @Test(dataProvider = "LoginData", priority = 13, enabled = true, groups = {"regression"}, description = "validate out of stock product in case of change delivert time from cart")
    public void verifySwiggyPopWithoutLogin(String username, String password) {
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.openSwiggyPop();
        Assert.assertEquals(swiggypopPage.getpopHeader(), "Introducing Swiggy Pop");
        swiggypopPage.clickOnFirstPOPImageIos();
        if (popObj.get(0).isOpened()) {
            Assert.assertFalse(swiggypopPage.checkElementDisplay(swiggypopPage.txt_confirmAddressAndProceed, null));
            checkoutPage.clickOnContinoueButton();
            login.LogintoApp(username, password);
            //Assert.assertEquals(swiggypopPage.getItemNameFromDetailView(),poplist.get(0).getName().trim());
            //Assert.assertEquals(swiggypopPage.getItemDetailFromDetailView(),"From: "+poplist.get(0).getRestaurantName());
            swiggypopPage.clickonConfirmAddressAndProceed();
            swiggypopPage.clickonProceedToPay();
        } else {
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }

    @Test(dataProvider = "LoginData", priority = 14, enabled = true, groups = {"regression"}, description = "verify add new address,edit & delete")
    public void verifyAddNewAddress(String username, String password) {
        consumerApiHelper.deleteAllAddressExceptHomeANDWORK(username, password);
        launch.clickonAllowButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        accountPage.updateMobileNumber("1123123122");
       /* driver.pressKeyCode(AndroidKeyCode.BACK);
        driver.pressKeyCode(AndroidKeyCode.BACK);
        accountPage.updateEmailAddress("newEmail@gmail.com");
        driver.pressKeyCode(AndroidKeyCode.BACK);*/
        accountPage.openManageAddress();
        accountPage.clickOnBtnAddNewAddress();
        addressPage.enterNewAddress("other", "ATestOrder");
        GetAddressPojo addressObject = consumerApiHelper.getAllAddresS("9950611829", "qwerty123");
        System.out.println(addressObject.getData().getAddresses());
        Assert.assertTrue(addressObject.getData().getAddresses().toString().contains("ATestOrder"));
        addressPage.editEnteredAddress();
        addressPage.clickonSaveAddress();
        //addressPage.deleteEnteredAddress();
    }

    @Test(dataProvider = "LoginData", priority = 15, enabled = true, groups = {"regression"}, description = "validate out of stock product in case of change delivert time from cart")
    public void IncreaseItemCount(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);

        AndroidUtil.sleep(5000);
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertEquals(checkoutPage.cartItemName.size(), 2);
        checkoutPage.decreaseItemCount();
        Assert.assertEquals(checkoutPage.cartItemName.size(), 1);


    }

    //Verify Apply coupon
    @Test(dataProvider = "LoginData", priority = 16, enabled = true, groups = {"regression"}, description = "verify apply coupon from cart")
    public void verifyApplyCouponFromCart(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        launch.clickonAllowButton();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        homePage.openNearMeTab();
        homePage.clickOnSortNudge();
        homePage.selectRestaurant(null, 1);
        restaurantPage.removeCoachmark();
        //Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat2), Double.toString(lng2), username, password);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();

        AndroidUtil.sleep(5000);

        checkoutPage.applyCoupon("ICICI25");
        Assert.assertEquals(checkoutPage.getCouponAppliedDialogTitle().toLowerCase(), "Woohoo!".toLowerCase());
        checkoutPage.clickOnOK();
    }


    @Test(dataProvider = "LoginData", priority = 17, enabled = true, groups = {"regression"}, description = "verify search dish from explore screen")
    public void verifySearchDishFromExploreScreen(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat1), Double.toString(lng1), "WORK");
        String restaurantName;
        driver.setLocation(location);
        launch.clickonAllowButton();
        AndroidUtil.sleep(10000);
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();

        String restaurantname = explorePage.searchDishesAndAddToCart("Gobi paratha");
        explorePage.clickonFullMenu();
        restaurantPage.removeCoachmark();
        Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantname.toLowerCase());
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
    }


    @Test(dataProvider = "LoginData", priority = 18, enabled = true, groups = {"regression"}, description = "verifySwiggyPopPressBackKey")
    public void verifySwiggyPopPressBackKey(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);

        homePage.openSwiggyPop();
        swiggypopPage.clickOnFirstPOPImageIos();
        if (!popObj.isEmpty() && popObj.get(0).isOpened()) {
            swiggypopPage.clickonBackButton();
            Assert.assertEquals(restaurantPage.getPOPCartcleartTitle(), Constants.SWIGGYPOP_PRESSBACK_TITTLE);
            Assert.assertEquals(restaurantPage.getPOPCartcleartMessage(), Constants.SWIGGYPOP_PRESSBACK_MESSAGE);
            swiggypopPage.clickOnOkGotIt();
        } else {
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }


    //Validate Veg only icon on restaurat page
    @Test(dataProvider = "LoginData", priority = 19, enabled = true, groups = {"filters"}, description = "Validate veg only icon on restaurant page")
    public void verifyVegOnlyIconOnRestaurantPage(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat1), Double.toString(lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openFilter();
        homePage.selectShowRestaurantsWith();
        homePage.filterRestaurantWith("Pure Veg");
        homePage.selectRestaurant(null, 1);
        Assert.assertTrue(restaurantPage.isPureVegIconDisplay());

    }

    @Test(dataProvider = "LoginData", priority = 20, enabled = false, groups = {"filters"}, description = "verify sort restaurant list based on Delivery time ")
    public void verifySorting(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListBasedOnSort(username, password, Double.toString(lat1), Double.toString(lng1), "DELIVERY_TIME");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        homePage.removeSwiggyPop();
        homePage.clickOnSortNudge();
        homePage.openFilter();
        //homePage.selectShowRestaurantsWith();
        homePage.selectSorting(Constants.DELIVERYTIME_SORT);
        //Assert.assertEquals(home.getRestaurantNameFromList(0).toLowerCase(),al.get(0).getRestaurantName().toLowerCase());
        // TODO - Need to validate
    }

    @Test(dataProvider = "LoginData", priority = 21, enabled = false, groups = {"filters"}, description = "Verify Filter Reset Case")
    public void verifyResetFilter(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        launch.clickOnSetDeliveryLocation();
        homePage.locationConfirmationPopup();
        homePage.removeSwiggyPop();
        homePage.clickOnSortNudge();
        homePage.openFilter();
        homePage.selectShowRestaurantsWith();
        //homePage.filterRestauranByPosition(1);
        //Assert.assertTrue(homePage.checkFilterApplied());
        //homePage.openFilter();
        AndroidUtil.sleep();
        Assert.assertTrue(homePage.checkElementDisplay(homePage.btn_filterReset));
        homePage.clickonResetFilter();
        Assert.assertTrue(homePage.btn_applyFilter.isEnabled());
        homePage.clickonApplyFilterButton();

    }

    @Test(dataProvider = "LoginData", priority = 22, enabled = true, groups = {"filters"}, description = "validate out of stock product in case of change delivert time from cart")
    public void verifyChainRestaurants(String username, String password) {
        String firstChainRestaurantName = null;
        int chainResturantCount = 0;
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat1), Double.toString(lng1), "CUISINES:American");
        for (int i = 1; i < al.size(); i++) {
            if (al.get(i).getChainCount() > 0) {
                System.out.println(al.get(i).getRestaurantName());
                firstChainRestaurantName = al.get(i).getRestaurantName();
                chainResturantCount = al.get(i).getChainCount() + 1;
                break;
            }
        }

        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        launch.clickonAllowButton();
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        homePage.openNearMeTab();
        homePage.openFilter();
        homePage.open_cusinesSubFilter();
        homePage.filterRestaurantWith("American");
        String rest1 = al.get(0).getRestaurantName();
        System.out.println(firstChainRestaurantName);
        String chainRestaurantText = homePage.openRestaurantFromList1(firstChainRestaurantName, 0);
        System.out.println(chainRestaurantText);
        Assert.assertEquals(chainRestaurantText, chainResturantCount + " outlets near you");
        Assert.assertEquals(homePage.getChainRestaurantPopupHeader(), "Choose \"" + firstChainRestaurantName + "\" Outlet");
        //Assert.assertEquals(homePage.getChainRestaurantNumberOfOutletText(), chainRestaurantText);
        //Assert.assertEquals(homePage.getChainRestaurantList().size(), Integer.parseInt(homePage.getChainRestaurantNumberOfOutletText().split(" ")[0]));*/
    }


    //Apply filter check again
    @Test(dataProvider = "LoginData", priority = 23, enabled = true, groups = {"filters"}, description = "E2E flow after apply filter ")
    public void validateE2EFlowAfterApplyFilter(String username, String password) {
        consumerApiHelper.deleteAllAddress(username, password);
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        GetAddressPojo allAddress = consumerApiHelper.getAllAddresS(username, password);
        for (int i = 0; i < allAddress.getData().getAddresses().size(); i++) {
            if (allAddress.getData().getAddresses().get(i).getAnnotation().equalsIgnoreCase("work")) {
                consumerApiHelper.deleteAddress(username, password, allAddress.getData().getAddresses().get(i).getId());
            }
        }
        consumerApiHelper.getRestaurantList(Double.toString(lat2), Double.toString(lng2));
        consumerApiHelper.createNewAddress("9950611829", "qwerty123", Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat1), Double.toString(lng1), "SHOW_RESTAURANTS_WITH:Offers");
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openFilter();
        homePage.selectShowRestaurantsWith();
        homePage.filterRestaurantWith("Offers");
        homePage.selectRestaurant(null, 1);
        restaurantPage.removeCoachmark();
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat1), Double.toString(lng1), username, password);
        restaurantPage.filterMenu(1);
        //Assert.assertEquals(restaurantPage.getCategoryName(),restaurantMenuObject.getCollection().get(1).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.increaseItemCount();
        if (checkoutPage.isRepeatCustomizeButtonDisplay()) {
            checkoutPage.selectRepeatForIncreaseItemCount();
        }
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
    }


    @Test(dataProvider = "LoginData", priority = 27, enabled = true, groups = {"filters"}, description = "verify E2E flow With Filter SwiggyAssured")
    public void verifyE2EflowWithFilterSwiggyAssured(String username, String password) {
        String filterName = "SHOW_RESTAURANTS_WITH:Swiggy%20Assured";
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress("9036976038", "huawei123");
        consumerApiHelper.createNewAddress("9036976038", "huawei123", Double.toString(lat3), Double.toString(lng3), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat3), Double.toString(lng3), filterName);
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat3), Double.toString(lng3), username, password);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openFilter();
        homePage.selectShowRestaurantsWith();
        homePage.filterRestaurantWith("30 Min or Free");
        Assert.assertTrue(homePage.checkSwiggyAssuredIcon(0));
        homePage.selectRestaurant(null, 1); //TODO- to check for swiggy assured restaurant
        restaurantPage.removeCoachmark();
        Assert.assertTrue(restaurantPage.isSwiggyAssuredIconDisplay());
        Assert.assertEquals(restaurantPage.getRestaurantName(), al.get(0).getRestaurantName());

        restaurantPage.filterMenu(restaurantMenuObject.getCollection().get(2).toString());
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        Assert.assertTrue(checkoutPage.validateLoginToProceedLayoutDisplayInCart());
        checkoutPage.clickOnContinoueButton();
        login.LogintoApp(username, password);
        //checkoutPage.proceedToPay();


    }

    @Test(dataProvider = "LoginData", priority = 24, enabled = true, groups = {"filters"}, description = "E2E flow select carousel ")
    public void verifyE2EFlowFromCarousel(String username, String password) {
        String restaurantName = null;
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(Constants.userName, Constants.password);
        consumerApiHelper.createNewAddress(Constants.userName, Constants.password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        List<Menulist> lcarousel = consumerApiHelper.getCarouselList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        List<Menulist> lrestaurantList = null;
        Restaurantmenu restaurantMenuObject;
        String firstCarouselType = lcarousel.get(0).getType();
        if (lcarousel.get(0).getType().equalsIgnoreCase("collection")) {
            lrestaurantList = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1), lcarousel.get(0).getRestaurantId());
        } else {
            lrestaurantList = lcarousel;
        }
        homePage.clickOnCarouselAndOpenRestaurantFromCollection(0);

        if (firstCarouselType.equalsIgnoreCase("collection")) {
            AndroidUtil.sleep(5000);
            restaurantName = homePage.getRestaurantNameFromList(0);
            Assert.assertEquals(homePage.getRestaurantNameFromList(0), lrestaurantList.get(0).getRestaurantName());
            homePage.selectRestaurant(null, 1);
            restaurantPage.removeCoachmark();
            Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantName.toLowerCase());


        } else {
            restaurantMenuObject = consumerApiHelper.getRestaurantMenu(lrestaurantList.get(0).getRestaurantId(), lrestaurantList.get(0).getUuid(), Double.toString(Constants.lat1), Double.toString(Constants.lng1), username, password);
            restaurantPage.removeCoachmark();
            Assert.assertEquals(restaurantPage.getRestaurantName().toLowerCase(), restaurantMenuObject.getName().toLowerCase());

        }

        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        restaurantPage.addItemtest();
        restaurantPage.increaseItemCountFromRestaurantPage();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.decreaseItemCount();
        checkoutPage.clickOk();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        //checkoutPage.changeAddresFromCart(); TODO - Need to add multiple address for one location using api
        //checkoutPage.clickOnProceedToPay();


    }


    @Test(dataProvider = "LoginData", priority = 25, enabled = false, groups = {"regression"}, description = "validate out of stock product in case of change delivert time from cart")
    public void verifyForNonServicableLocation1(String username, String password) {
        Location location = new Location(Constants.lat_nonServicable, Constants.lng_nonServicable, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.removeSwiggyPop();
        Assert.assertEquals(homePage.getNotServicableLocationText(), Constants.LOCATION_NOT_SERVICABLE_IOS);
        homePage.clickOnEditLocation();

    }

    //EVDO- Pending
    @Test(dataProvider = "LoginData", priority = 26, enabled = false, groups = {"regression"}, description = "verify EDVO")
    public void verifyEDVOMeal(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress("9036976038", "huawei123");
        consumerApiHelper.createNewAddress("9036976038", "huawei123", Double.toString(lat1), Double.toString(lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantListWithFilter(Double.toString(lat1), Double.toString(lng1), "CUISINES:Pizzas");
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        //Add location Manually
        homePage.clickOnEditLocation();
        homePage.removeSwiggyPop();
        homePage.openFilter();
        homePage.filterRestaurantWith("Pizzas");
        homePage.selectRestaurantNew("Domino's Pizza", 0);
        String firstrestaurantName = restaurantPage.getRestaurantName();
        restaurantPage.openEDVOMeal(0);
        EDVOMeal edvoMeal = consumerApiHelper.getEDVOMealObject("23798", "1");
        restaurantPage.validateEDVOLandingScreen(edvoMeal);
        restaurantPage.clickOnContinueButtonFromLandingScreen();
        restaurantPage.validatePizzaScreen(edvoMeal, 0);
        String itemName_1 = restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.validatePizzaScreen(edvoMeal, 1);
        String itemName_2 = restaurantPage.getItemName(0);
        restaurantPage.selectFirstPizza();
        Assert.assertTrue(restaurantPage.checkContinueButtonDisplay());
        restaurantPage.clickOnContinueButtonOnAddPizzaScreen();
        restaurantPage.clickOnViewCart();
        //Assert.assertEquals(checkoutPage.getRestaurantNameFromCart().toString().toLowerCase(),"Domino's Pizza".toLowerCase());
        Assert.assertEquals(checkoutPage.getItemNameFromCart(0).toLowerCase(), edvoMeal.getTagText().toLowerCase());

    }

    @Test(dataProvider = "LoginData", priority = 25, enabled = false, groups = {"regression"}, description = "validate out of stock product in case of change delivert time from cart")
    public void verifyForNonServicableLocation(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        consumerApiHelper.deleteAllAddress(username, password);
        driver.setLocation(location);
        launch.clickonAllowButton();
        homePage.removeSwiggyPop();
        Assert.assertEquals(homePage.getNotServicableLocationText(), Constants.LOCATION_NOT_SERVICABLE_IOS);
        homePage.clickOnEditLocation();

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "validate DishDiscovery collection on home page ")
    public void validateiOSDDCollectionOnHomePage(String username, String password, ITestContext context) {
        Location location = new Location(Constants.lat_nonServicable, Constants.lng_nonServicable, 0);
        SoftAssert softAssert = new SoftAssert();
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        homePage.verticalScroll();
        context.setAttribute("storyTitle", homePage.getStoryCollectionItem(0).toLowerCase());
        System.out.println(context.getAttribute("storyTitle").toString());
    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "validate DD collection on Explore")
    public void validateiOSDDCollectionOnExploreScreen(String username, String password, ITestContext context) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        SoftAssert softAssert = new SoftAssert();
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        homePage.openExploreTab();
        context.setAttribute("storyTitle", homePage.getStoryCollectionItem(0).toLowerCase());
        System.out.println(context.getAttribute("storyTitle").toString());

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify first DD collection on Listing")
    public void validateiOSfirstDDCollectionOnListing(String username, String password, ITestContext context) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        SoftAssert softAssert = new SoftAssert();
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        homePage.verticalScroll();
        context.setAttribute("storyTitle", homePage.getStoryCollectionItem(0).toLowerCase());
        System.out.println(context.getAttribute("storyTitle").toString());
        homePage.openStoryCollection(0);
        AndroidUtil.sleep();
        softAssert.assertEquals(dishDiscoveryObject.getHeader().toLowerCase(), homePage.getStoryCollectionHeader().toLowerCase());
        softAssert.assertEquals(dishDiscoveryObject.getDescription().toLowerCase(), homePage.getStoryCollectionSubtitle().toLowerCase());

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify first DD collection on explore")
    public void validateiOSDDfirstCollectionOnExplorePage(String username, String password, ITestContext context) {

        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        SoftAssert softAssert = new SoftAssert();
        //driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        restaurantPage.clickonPreOrderTimeSlot();
        restaurantPage.setPreOrderTime();
        AndroidUtil.sleep(10000);
        homePage.openExploreTab();
        AndroidUtil.sleep(10000);
        homePage.openStoryCollection(0);
        AndroidUtil.sleep(10000);
        AndroidUtil.scrollHorizontal_new(driver, 1);
        AndroidUtil.sleep(5000);
        restaurantPage.addItemToCartForIOs();
        AndroidUtil.sleep(2000);
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickOnProccedWithPhoneNumber();
        login.LogintoApp(username, password);
        checkoutPage.proceedToPay();

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify restaurant full menu on DD")
    public void validateiOSDDRestaurantfullmenu(String username, String password, ITestContext context) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        SoftAssert softAssert = new SoftAssert();
        driver.setLocation(location);
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        homePage.openExploreTab();
        AndroidUtil.sleep(10000);
        homePage.openStoryCollection(0);
        AndroidUtil.scrollHorizontal_new(driver, 1);
        dishDiscoveryPage.clickonViewFullMenu();
        restaurantPage.addItemToCartForIOs();
        restaurantPage.openCartFromRestaurantPage();

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "verify DD Close Collection button")
    public void validateiOSDDcloseCollection(String username, String password, ITestContext context) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        launch.clickOnSetDeliveryLocation();
        homePage.clickOnSortNudge();
        AndroidUtil.sleep(10000);
        homePage.openExploreTab();
        AndroidUtil.sleep(10000);
        homePage.openStoryCollection(0);
        AndroidUtil.sleep(10000);
        dishDiscoveryPage.clickonCloseCollectionbutton();

    }


    @AfterMethod
    public void tearDown() throws Exception {

        //driver.resetApp();
        driver.launchApp();


    }


    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }

    public boolean checkElementDisplay(IElement element) {
        try {
            if (element.isDisplayed()) {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
        return false;

    }

}








