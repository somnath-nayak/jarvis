package com.swiggy.ui.uiTests.portal.web;


import org.apache.maven.wagon.providers.ssh.knownhost.UnknownHostException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.portalweb.ForgotPasswordPage;
import com.swiggy.ui.page.classes.portalweb.HomePage;
import com.swiggy.ui.page.classes.portalweb.ListingPage;
import com.swiggy.ui.page.classes.portalweb.LoginPage;
import com.swiggy.ui.page.classes.portalweb.PortalConstants;


public class ForgetPasswordTests 
{
	
    public WebDriver driver;
    InitializeUI inti;
    HomePage homepage;
    LoginPage loginpage;
    ListingPage listingpage;
    ForgotPasswordPage forgotpwdpage;
    
	
    @BeforeClass
    public void setUptest() throws UnknownHostException
    {
        inti = new InitializeUI();
        driver = inti.getWebdriver();
        driver.manage().window().maximize();
        driver.get(PortalConstants.portal_url);
        homepage = new HomePage(inti);
        loginpage = new LoginPage(inti);
        forgotpwdpage = new ForgotPasswordPage(inti);
        
        

    }
	
    
    
    /*Verify that all field are present on the Forgot password Page*/
    
    @Test(groups={"sanity","regression"},description="Verify all elements are present on the forgot password Page",priority=0)
    public void verifyForgotPasswordFields() throws InterruptedException
    {
    	SoftAssert softAssertion = new SoftAssert();
        homepage.clickOnLogin();
        loginpage.enterMobileNo(PortalConstants.validusername);
    	loginpage.clickForgotPwd_link();
		softAssertion.assertEquals(forgotpwdpage.getTextOfPage(),"Forgot password","Forgot password Page title is not proper");
		softAssertion.assertTrue(forgotpwdpage.checkBackBtn_Present(),"Navigate back button is not present on forgot password Page");
		softAssertion.assertTrue(forgotpwdpage.checkLoginLink_Present(),"Login accont link is not present  on forgot password Page");
		softAssertion.assertTrue(forgotpwdpage.checkMobileNoField_Present(),"Phone text field is not present  on forgot password Page");
		softAssertion.assertTrue(forgotpwdpage.checkOtpField_Present(),"OTP text field is not present  on forgot password Page");
		softAssertion.assertTrue(forgotpwdpage.checkVerifyOtp_Present(),"Verify OTP button is not present");
		
		forgotpwdpage.navigateBack();
		softAssertion.assertAll();
		
	  	  
			
    }
    
    
    /*Verify that correct number is pre-populating on forgot password page */
    
    @Test(groups={"sanity","regression"},description="Verify password is populating on forgot password page",priority=1)
    public void verifyCorrectPhoneNoIsPopulating()
    {
    	
    	loginpage.enterMobileNo(PortalConstants.validusername);
		loginpage.clickForgotPwd_link();
		Assert.assertEquals(PortalConstants.expected_prepopulatedValue, forgotpwdpage.getText(),"Prepopulating value in mobile field is not correct");
			
    }
    

    /*Verify the error message when incorrect otp is entered */
    
    @Test(groups={"regression"},description="Verify error message when inccorrect otp is enterd",priority=2)
    public void validateErrorMessageForInvalidOtp()
    {
    	
    	forgotpwdpage.enterOTP(PortalConstants.invalid_OTP);
    	forgotpwdpage.clickVerifyOTP();
    	Assert.assertEquals(forgotpwdpage.validation_msg_otp(),"Enter valid OTP","Error message is not correct");
	    }
    
    
    

    
    @AfterClass
    public void teardown()
    {
    	
    	driver.close();

    }
	
	

}
