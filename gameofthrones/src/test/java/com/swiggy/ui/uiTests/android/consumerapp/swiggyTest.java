package com.swiggy.ui.uiTests.android.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.Constants;

import java.util.List;

public class swiggyTest {


    public AndroidDriver driver;
    LaunchPage launch;
    HomePage home;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    Double lat2=27.834105;
    Double lng2=76.171977;
    ConsumerApiHelper consumerApiHelper;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9036976038","huawei123"}};
    }

    @BeforeClass
    public void setUp(){
        consumerApiHelper=new ConsumerApiHelper();

        InitializeUI inti = new InitializeUI();
        driver = inti.getAndroidDriver();
        launch = new LaunchPage(inti);
        home = new HomePage(inti);
        login=new LoginPage(inti);
        addressPage=new AddressPage(inti);
        restaurantPage=new RestaurantPage(inti);
        checkoutPage=new CheckoutPage(inti);
        explorePage=new ExplorePage(inti);
        accountPage=new AccountPage(inti);
        paymentPage=new PaymentPage(inti);





    }
    @Test(dataProvider = "LoginData", priority = 0, enabled = true)
    public void verifyLogin(String username, String password){
        Location location=new Location(lat2,lng2,0);

        driver.setLocation(location);
        consumerApiHelper.clearCart(username,password);
        consumerApiHelper.deleteAllAddress("9036976038","huawei123");
        consumerApiHelper.createNewAddress("9036976038","huawei123",Double.toString(lat2),Double.toString(lng2),"WORK");
        List<Menulist> al=consumerApiHelper.getRestaurantList(Double.toString(lat2),Double.toString(lng2));
        launch.tapOnLogin();
        login.LogintoApp(username, password);
        home.openAccountTab();
        Assert.assertEquals(accountPage.getRegisteredMobileNumber(),username);
    }
    @Test(priority = 3, enabled = true)
    public void SelectAddressAsOther(ITestContext context){
        context.setAttribute("Address","Work");
        home.openNearMeTab();
        addressPage.goToAddressPage();
        addressPage.selectSavedAddress("Work");
        Assert.assertEquals(addressPage.address.getText().toString().toLowerCase(),"Work".toLowerCase());


    }
    @Test(priority = 2, enabled = false)
    public void selectCurrentLocation(){
        addressPage.goToAddressPage();
        addressPage.selectUserCurrentLocation();
        home.removeSwiggyPop();

    }
    @Test(priority = 4, enabled = true)
    public void openRestaurantFromHomeAndApplyMenuFilter(){
        String restaurantName=home.getRestaurantNameFromList(1);
        //home.selectRestaurant(null,1);
        home.selectRestaurant("Automation_QA_DO_NOT_TOUCH1",0);
        restaurantPage.removeCoachmark();
        restaurantPage.filterMenu(Constants.MENU_FILTER);
        Assert.assertEquals(restaurantPage.getCategoryName(),Constants.MENU_FILTER);
    }
    @Test(priority = 5, enabled = false)
    public void testToggleToVegOnly()
    {
       restaurantPage.toggleToVegOnly();
    }
    @Test(priority = 6, enabled = true)
    public void addItemToCart(){
        String itemName=restaurantPage.addItemtest();
        Assert.assertEquals(restaurantPage.checkElementDisplay(restaurantPage.btn_checkout),true);
        restaurantPage.openCartFromRestaurantPage();
        //Assert.assertEquals(checkoutPage.getItemNameFromCart(),itemName);

    }
    @Test(priority = 7, enabled = true)
    public void validateProceedToPay(ITestContext context){
        String Expectedaddress= (String) context.getAttribute("Address");
        Assert.assertTrue(checkoutPage.getDefaultAddressFromCart().contains(Expectedaddress));
        checkoutPage.clickonViewDetailBill();
        String totalBill=checkoutPage.getTotalBillInCart();
        checkoutPage.clickOnProceedToPay();
        String header=paymentPage.getPaymentHeader();
        Assert.assertEquals(header.toLowerCase(),"PAYMENT".toLowerCase());
        //Assert.assertTrue(paymentPage.getSubHeaderFromPaytmenPage().contains(totalBill.split(".")[0]));



    }



    @AfterMethod
    public void clearAndLaunch() {
        //driver.launchApp();
    }


    @AfterSuite
    public void close() {
        driver.quit();
    }

}
