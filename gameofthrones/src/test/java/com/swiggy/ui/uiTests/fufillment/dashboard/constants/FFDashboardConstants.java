package com.swiggy.ui.uiTests.fufillment.dashboard.constants;

public interface FFDashboardConstants {
	
	String l1User = "L1_Automation";
	String l1Pass = "Swiggy@321";
	String l2User = "L2_Automation";
	String l2Pass = "Swiggy@321";
	String callDEUser = "";
	String callDEPass = "";
	String verifierUser = "V1_Automation";
	String verifierPass = "Swiggy@123";
	String superuser = "shashank";
	String superpass = "Shash@ank1";
	
	String urlStage1 = "http://oms-u-cuat-01.swiggyops.de/";
	String urlStage2 = "http://oms-u-cuat-02.swiggyops.de/";
	String urlStage3 = "http://oms-u-cuat-03.swiggyops.de/";

}
