package com.swiggy.ui.uiTests.crmAgentWorkBench;

import framework.gameofthrones.Aegon.InitializeUI;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.openqa.selenium.chrome.ChromeOptions;

/**
 * Created by sumit.m on 24/04/18.
 */

public class SimpleThreadPool {

    private static String URL = "https://workbench.swiggy.com//login";
    private static String username="";
    private static String password="cinema";

    @Test(enabled = true, priority = 0)
    public void testPara() {
        int noOfThreads;
        noOfThreads = 1;
        ExecutorService executor = Executors.newFixedThreadPool(noOfThreads);
        int count=1;
        List<Future> executorsList=new ArrayList<Future>();
        for (int i = 1; i <= 1; i++) {
                InitializeUI gameofthrones = new InitializeUI();

            WebDriver driver = gameofthrones.getWebdriver();

                //   Thread.sleep(3000);
                driver.navigate().to(URL);
                //   Thread.sleep(3000);
                WebElement useremail = driver.findElement(By.id("email"));
                WebElement pwd = driver.findElement(By.id("password"));
                WebElement login = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/form/button"));
                username = "fcuk"+i+"@swiggy.in";

                useremail.sendKeys(username);
                pwd.sendKeys(password);
                login.click();
                //   Thread.sleep(500);
                //   driver.close();
                Callable worker = new WorkerThread("" + i);

                executorsList.add(executor.submit(worker));
                count = count + 1;
            }

        while (true){
            ListIterator<Future> it=executorsList.listIterator();
            while(it.hasNext()){
                if(it.next().isDone()){
                    Callable worker = new WorkerThread("" + count);
                    it.remove();
                    it.add(executor.submit(worker));
                    count++;
                }
                if(executorsList.size()<=0){
                    executor.shutdown();
                    break;
                }
            }
        }
    }
}


