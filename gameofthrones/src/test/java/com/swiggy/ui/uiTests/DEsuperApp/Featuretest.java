package com.swiggy.ui.uiTests.DEsuperApp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.api.erp.delivery.helper.AutoassignHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import framework.gameofthrones.Tyrion.StfHelper;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.aspectj.weaver.ast.And;
import org.json.JSONObject;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.pojoClasses.EDVOMeal;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.DeAppReactNative.HomePageDERN;
import com.swiggy.ui.screen.DeAppReactNative.LeftNavDERN;
import com.swiggy.ui.screen.DeAppReactNative.LoginPageDERN;
import com.swiggy.ui.screen.DeAppReactNative.ProfilePageDERN;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;

import java.util.List;

public class Featuretest {


    public AppiumDriver driver;
    InitializeUI inti;
    LoginPageDERN loginPage;
    HomePageDERN homePage;
    LeftNavDERN leftNav;
    ProfilePageDERN profilePage;
    AutoassignHelper hepauto;
    DeliveryServiceHelper helpdel = new DeliveryServiceHelper();


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"403"}};
    }

    @BeforeClass
    public void setUptest() {
        inti = new InitializeUI();
        loginPage = new LoginPageDERN(inti);
        homePage=new HomePageDERN(inti);
        leftNav=new LeftNavDERN(inti);
        profilePage=new ProfilePageDERN(inti);
        driver=inti.getAppiumDriver();
    }
    @Test(dataProvider = "LoginData",priority = 1)
    public void verifyLogin(String deId){
        String deotp=null;
        loginPage.clickOnAllowPermission();
        loginPage.clickOnAllowPermission();

        loginPage.enterDEId(deId);
        AndroidUtil.sleep();
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep(5000);
        loginPage.clickOnContinueButton();
        loginPage.clickOnContinueButton();
        try {
            deotp= (new JSONObject(helpdel.getOtp("403").ResponseValidator.GetBodyAsText())).get("data").toString();
            System.out.println(deotp);
        }
        catch (Exception e){

        }
        loginPage.enterOtp(deotp);
        AndroidUtil.pressBackKey(driver);
        AndroidUtil.sleep();
        loginPage.clickOnLoginButton();
        loginPage.clickOnLoginButton();
        Assert.assertTrue(homePage.isLeftNavButtonDisplay());
    }
    @Test(priority = 2)
    public void verifyLeftNav() {
        homePage.openLeftNav();
        AndroidUtil.sleep();
        Assert.assertTrue(leftNav.isDENameDisplayed());
    }
    @Test(priority = 3)
    public void verifyDeProfile(){
        Assert.assertEquals("Karunakar",leftNav.getDEName());
        Assert.assertEquals("2313 miles", leftNav.getSubHeading());
    }
    @Test(priority = 4)
    public void verifyProfileDetailPage(){
        leftNav.openProfileDetail();
        Assert.assertEquals("Karunakar",profilePage.getDEName());
        Assert.assertEquals("BLR_Kormangala",profilePage.getDEZone());

    }
    @Test(dataProvider = "LoginData",priority = 5)
    public void verifyPersonalInformation(String deId){
        profilePage.openDEPersonalInformation();
        Assert.assertEquals(deId,profilePage.getDEId());
        System.out.println(profilePage.getDEJoiningDate());
        System.out.println(profilePage.getDEServiceZone());
        System.out.println(profilePage.getFleetManageEmailId());
        System.out.println(profilePage.getDEBlookDetail());

    }
    @Test(dataProvider = "LoginData",priority = 6)
    public void verifyUpdateEmergencyDetail(String deId){
      profilePage.clickOnEditEmergencyContact();

    }
    @Test(dataProvider = "LoginData",priority = 7)
    public void verifyDownloadInsurance(String deId){
        profilePage.downloadDEInsurance();

    }
    @Test(dataProvider = "LoginData",priority = 8)
    public void verifyShareTheInsurance(String deId){
        profilePage.shareDEInsurance();
    }
    @Test(dataProvider = "LoginData",priority = 9)
    public void verifyDEBankInformation(String deId){
        homePage.openHomePage();
        homePage.openLeftNav();
        leftNav.openProfileDetail();
        profilePage.openDEBankInfo();
    }







}
