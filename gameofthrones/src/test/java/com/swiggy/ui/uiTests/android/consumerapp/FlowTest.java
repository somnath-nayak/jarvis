package com.swiggy.ui.uiTests.android.consumerapp;

import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.ui.screen.classes.consumerapp.android.*;

public class FlowTest {

	InitializeUI gameofthrones = new InitializeUI();
	/*public AndroidDriver driver = gameofthrones.getAndroidDriver();
	
	@DataProvider(name = "LoginData")
	public Object[][] getdata() {
		return new Object[][]{{"7338471120","123456"}};
	}
	
	@Test(dataProvider = "LoginData", priority = 0, enabled = true)
	public void Scenario1(String username, String password) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		CheckoutPage checkout = restaurant.selectRestaurant().addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
//		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
//		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
	}
	
	@Test(dataProvider = "LoginData", priority = 1, enabled = true)
	public void Scenario2(String username, String password) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		CheckoutPage checkout = restaurant.selectRestaurant().addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
	}
	
	@DataProvider(name = "testLoggedOutUser")
	public Object[][] test() {
		return new Object[][]{{"9900039746","Kiran123!","pauri","tower D","Christ College"}};
	}
	
	@Test(enabled = true, dataProvider = "testLoggedOutUser",priority = 2)
	public void Scenario3(String username, String password, String area, String houseno, String landmark) {
		try{
		LaunchPage launch = new LaunchPage(driver);
		DeliveryLocation location = launch.addLocation();
		launch.allowPermission();
		Thread.sleep(5000);
		HomePage home = location.enterArea(area).enterHouseNo(houseno).enterLandMark(landmark).selectLocationAsHome().save();
		Assert.assertTrue(home.isHomePage(), "Unsuccessful");
		RestaurantPage restaurant = new RestaurantPage(driver);
		restaurant.selectRestaurant().addItem().addCustomItem();
		CheckoutPage checkout = restaurant.checkout();
		checkout.clickOk();
		restaurant.checkout();
		if(launch.isContinueDisplayed()) {
		launch.tapOnContinue().LogintoApp(username, password);
		checkout.selectAddress();
		}
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@DataProvider(name = "LoginData4")
	public Object[][] getdata4() {
		return new Object[][]{{"7338471120","123456","Swiggy Assured"}};
	}
	
	@Test(dataProvider = "LoginData4", priority = 3, enabled = true)
	public void Scenario4(String username, String password,String filter_text) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		FilterPage filter = home.clickOnFilter();
		filter.selectFilter(filter_text);
		home = filter.applyFilter();
		restaurant.selectRestaurant();
		Assert.assertTrue(restaurant.isSwiggyAssured(), "Not a Swiggy Assured Resturant");
		CheckoutPage checkout = restaurant.addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		DeliverPage caprd = payment.payWithPaytm();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
	}
	
	@Test(dataProvider = "LoginData4", priority = 1, enabled = true)
	public void Scenario(String username, String password, String filter_text) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		FilterPage filter = home.clickOnFilter();
		filter.selectFilter(filter_text);
		home = filter.applyFilter();
		restaurant.selectRestaurant();
		Assert.assertTrue(restaurant.isSwiggyAssured(), "Not a Swiggy Assured Resturant");
		CheckoutPage checkout = restaurant.addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk(); 
//		Assert.assertTrue(home.isHomePage());
	}
	
	@Test(dataProvider = "LoginData4", priority = 4, enabled = true)
	public void Scenario5(String username, String password,String filter_text) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		FilterPage filter = home.clickOnFilter();
		filter.selectFilter(filter_text);
		home = filter.applyFilter();
		restaurant.selectRestaurant();
		Assert.assertTrue(restaurant.isSwiggyAssured(), "Not a Swiggy Assured Resturant");
		CheckoutPage checkout = restaurant.addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		DeliverPage caprd = payment.payWithFreecharge();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
	}
	
	@Test(dataProvider = "LoginData4", priority = 5, enabled = true)
	public void Scenario6(String username, String password,String filter_text) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		FilterPage filter = home.clickOnFilter();
		filter.selectFilter(filter_text);
		home = filter.applyFilter();
		restaurant.selectRestaurant();
		Assert.assertTrue(restaurant.isSwiggyAssured(), "Not a Swiggy Assured Resturant");
		CheckoutPage checkout = restaurant.addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		DeliverPage caprd = payment.payWithMobikwik();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
	}
	
	@Test(enabled = true, dataProvider = "testLoggedOutUser",priority = 6)
	public void Scenario7(String username, String password, String area, String houseno, String landmark) {
		try{
		LaunchPage launch = new LaunchPage(driver);
		DeliveryLocation location = launch.addLocation();
		launch.allowPermission();
		Thread.sleep(5000);
		HomePage home = location.enterArea(area).enterHouseNo(houseno).enterLandMark(landmark).selectLocationAsWork().save();
		Assert.assertTrue(home.isHomePage(), "Unsuccessful");
		home.listrest();
		RestaurantPage restaurant = new RestaurantPage(driver);
		restaurant.selectVeg().addItem().addCustomItem();
		CheckoutPage checkout = restaurant.checkout();
		checkout.clickOk();
		restaurant.checkout();
		if(launch.isContinueDisplayed()) {
		launch.tapOnContinue().LogintoApp(username, password);
		checkout.selectAddress();
		}
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(enabled = true, dataProvider = "testLoggedOutUser",priority = 7)
	public void Scenario8(String username, String password, String area, String houseno, String landmark) {
		try{
		LaunchPage launch = new LaunchPage(driver);
		DeliveryLocation location = launch.addLocation();
		launch.allowPermission();
		Thread.sleep(5000);
		HomePage home = location.enterArea(area).enterHouseNo(houseno).enterLandMark(landmark).selectAsOther().save();
		Assert.assertTrue(home.isHomePage(), "Unsuccessful");
		home.listrest();
		RestaurantPage restaurant = new RestaurantPage(driver);
		restaurant.selectRestaurant().selectVeg().addItem().addCustomItem();
		CheckoutPage checkout = restaurant.checkout();
		checkout.clickOk();
		restaurant.checkout();
		if(launch.isContinueDisplayed()) {
		launch.tapOnContinue().LogintoApp(username, password);
		checkout.selectAddress();
		}
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	@Test(enabled = true, dataProvider = "testLoggedOutUser",priority = 7)
	public void Scenario9(String username, String password, String area, String houseno, String landmark) {
		try{
		LaunchPage launch = new LaunchPage(driver);
		DeliveryLocation location = launch.addLocation();
		launch.allowPermission();
		Thread.sleep(5000);
		HomePage home = location.enterArea(area).enterHouseNo(houseno).enterLandMark(landmark).selectAsOther().save();
		Assert.assertTrue(home.isHomePage(), "Unsuccessful");
		home.listrest();
		RestaurantPage restaurant = new RestaurantPage(driver);
		restaurant.selectRestaurant().selectVeg().addItem().addCustomItem();
		CheckoutPage checkout = restaurant.checkout();
		checkout.clickOk();
		restaurant.checkout();
		if(launch.isContinueDisplayed()) {
		launch.tapOnContinue().LogintoApp(username, password);
		checkout.selectAddress();
		}
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(enabled = true, dataProvider = "testLoggedOutUser",priority = 6)
	public void Scenario10(String username, String password, String area, String houseno, String landmark) {
		try{
		LaunchPage launch = new LaunchPage(driver);
		DeliveryLocation location = launch.addLocation();
		launch.allowPermission();
		Thread.sleep(5000);
		HomePage home = location.enterArea(area).enterHouseNo(houseno).enterLandMark(landmark).selectAsOther().save();
		Assert.assertTrue(home.isHomePage(), "Unsuccessful");
		home.listrest();
		FilterPage filter = home.clickOnFilter();
		filter.selectFilter("Swiggy Assured");
		home = filter.applyFilter();
		RestaurantPage restaurant = new RestaurantPage(driver);
		restaurant.selectRestaurant().selectVeg().addItem().addCustomItem();
		CheckoutPage checkout = restaurant.checkout();
		checkout.clickOk();
		restaurant.checkout();
		if(launch.isContinueDisplayed()) {
		launch.tapOnContinue().LogintoApp(username, password);
		checkout.selectAddress();
		}
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.clickOnCOD();
//		DeliverPage caprd = payment.payNow();
//		caprd.checkCAPRD();
//		home = caprd.clickOnOk();
//		Assert.assertTrue(home.isHomePage());
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(dataProvider = "LoginData", priority = 4, enabled = true)
	public void Scenario11(String username, String password) {
		LaunchPage launch = new LaunchPage(driver);
		LoginPage login = launch.tapOnLogin();
		login.LogintoApp(username, password);
		HomePage home = new HomePage(driver);
		AddressPage location = home.gotoAddressPage();
		RestaurantPage restaurant = location.selectLocationAsOther();
		CheckoutPage checkout = restaurant.selectRestaurant().addItem().addCustomItem().checkout();
		checkout.clickOk();
		restaurant.checkout();
		PaymentPage payment = checkout.proccedToPay();
		Assert.assertTrue(payment.isPaymentPage(), "Not a Payment Page");
		payment.payWithFreecharge();
//		payment.payNow();
	}

	
	@AfterMethod
	public void clearAndLaunch() {
		driver.launchApp();
	}
	
	
	@AfterSuite
	public void close() {
		driver.quit();
	}*/

}
