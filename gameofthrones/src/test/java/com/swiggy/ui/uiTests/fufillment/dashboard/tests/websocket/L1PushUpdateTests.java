package com.swiggy.ui.uiTests.fufillment.dashboard.tests.websocket;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;

import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.fulfillment.dashboard.HomePage;
import com.swiggy.ui.uiTests.fufillment.dashboard.constants.FFDashboardConstants;
import com.swiggy.ui.uiTests.fufillment.dashboard.helper.FFDashboardHelper;

public class L1PushUpdateTests {
	
	InitializeUI gameofthrones = new InitializeUI();
	WebDriver driver = gameofthrones.getWebdriver();
	
	LOSHelper losHelper = new LOSHelper();
	OMSHelper omsHelper = new OMSHelper();
	FFDashboardHelper helper = new FFDashboardHelper(driver);
	String orderId;
	
	private static String URL = FFDashboardConstants.urlStage1;
	
	@BeforeTest
	public void startBrowser(){
		try{
			driver.manage().window().maximize();
			driver.navigate().to(URL);
			} catch(Exception e) {
				e.getStackTrace();
			}
	}
	
	@AfterTest
	public void quitDriver(){
		driver.close();
	}
	
	@BeforeClass
	public void print(){
		System.out.println("**********************************************************************");
		System.out.println("*     ____          _                                                *");
		System.out.println("*    /____|_      _(_) __ _  __ _ _   _                              *");
		System.out.println("*    \\___ \\ \\ /\\ / / |/ _` |/ _` | | | |                             *");
		System.out.println("*     ___) \\ V  V /| | (_| | (_| | |_| |                             *");
		System.out.println("*    |____/ \\_/\\_/ |_|\\__, |\\__, |\\__, |                             *");
		System.out.println("*                      |___/ |___/ |___/                             *");
		System.out.println("*                                                                    *");
		System.out.println("*  Starting Websocket push update tests for L1Placer                 *");
		System.out.println("**********************************************************************");
	}
	
	@BeforeMethod
	public void placeOrderAndLogin() throws IOException, InterruptedException{
		omsHelper.clearOrdersInQueue("L1Placer");
		orderId = losHelper.getAnOrder("manual");
		omsHelper.verifyOrder(orderId, "001");
		helper.loginAs("L1Placer");
	}
	
	@AfterMethod
	public void logout() throws InterruptedException{
		helper.logoutFromDashboard();
		omsHelper.forceLogout(FFDashboardConstants.l1User);
	}
	
	
	@Test()
	public void testdeliveryStatusUpdate() throws IOException, InterruptedException{
		System.out.println("***************************************** testdeliveryStatusUpdate started *****************************************");
		
		HomePage home = new HomePage(driver);
		home.clickOnOrderAlertButton();
		System.out.println(home.getOrderStatus());
//		Assert.assertEquals(home.getOrderId(), "#"+orderId);
		losHelper.statusUpdate(orderId, "1515751008 ", "assigned");
		Thread.sleep(2000);
		Assert.assertTrue(home.getOrderStatus().contains("Assigned"), "Status is not updated to assigned at L1Placer's dashboard");
		home.clickOnOrderPane();
		Thread.sleep(10000);
		losHelper.statusUpdate(orderId, "1515751008 ", "confirmed");
//		driver.navigate().refresh();
//		home.clickOnOrderAlertButton();
		Assert.assertTrue(home.getOrderStatus().contains("Confirmed"), "Status is not updated to confirmed at L1Placer's dashboard");
		Thread.sleep(2000);
		losHelper.statusUpdate(orderId, "1515751008 ", "arrived");
//		driver.navigate().refresh();
//		home.clickOnOrderAlertButton();
		Assert.assertTrue(home.getOrderStatus().contains("Arrived"), "Status is not updated to arrived at L1Placer's dashboard");
		driver.navigate().refresh();
		home.clickOnOrderAlertButton();
		losHelper.statusUpdate(orderId, "1515751008 ", "pickedup");
//		driver.navigate().refresh();
//		home.clickOnOrderAlertButton();
		Assert.assertTrue(home.getOrderStatus().contains("Pickedup"), "Status is not updated to pickedup at L1Placer's dashboard");
//		driver.navigate().refresh();
//		home.clickOnOrderAlertButton();
		losHelper.statusUpdate(orderId, "1515751008 ", "reached");
//		driver.navigate().refresh();
//		home.clickOnOrderAlertButton();
		Assert.assertTrue(home.getOrderStatus().contains("Reached"), "Status is not updated to reached at L1Placer's dashboard");
		Thread.sleep(2000);
		losHelper.statusUpdate(orderId, "1515751008 ", "delivered");
		
		System.out.println("######################################### testdeliveryStatusUpdate completed #########################################");
	}
	
//	@Test
	public void unplacedToPlacedTest() throws IOException{
		System.out.println("***************************************** unplacedToPlacedTest started *****************************************");
		
		HomePage home = new HomePage(driver);
		home.clickOnOrderAlertButton();
		helper.changeOrderStatusToPlaced("L1Placer");
		
		System.out.println("######################################### unplacedToPlacedTest completed #########################################");
	}
	
}
