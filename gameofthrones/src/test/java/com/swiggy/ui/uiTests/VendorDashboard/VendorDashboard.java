package com.swiggy.ui.uiTests.VendorDashboard;

//import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import framework.gameofthrones.Aegon.InitializeUI;
import static org.testng.Assert.assertEquals;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import antlr.ParserSharedInputState;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.page.classes.vendor.dashboard.HomePage;
import com.swiggy.ui.page.classes.vendor.dashboard.ManageOrders;
import com.swiggy.ui.page.classes.vendor.dashboard.PrepTimePage;
import com.swiggy.ui.page.classes.vendor.dashboard.Profile;
import com.swiggy.ui.page.classes.vendor.dashboard.SettingsPage;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsConstants;
import com.swiggy.ui.page.classes.vendor.dashboard.VmsHelper;
import com.swiggy.ui.page.classes.vendor.dashboard.*;
import com.swiggy.ui.uiTests.dashboard.dp.VendorDp;

public class VendorDashboard extends VendorDp{

	private WebDriver driver;
	private static String URL = VmsConstants.dashboard_url;
	By toggle_orders = By.xpath("//span[@class='bg']");

    @BeforeMethod
	public void setUp() throws InterruptedException{
		//Thread.sleep(5000);
        InitializeUI gameofthrones = new InitializeUI();
        driver = gameofthrones.getWebdriver();
		driver.navigate().to(URL);
//		HomePage homePage = new HomePage(driver);
//		SettingsPage settingsPage = new SettingsPage(driver);
//		homePage.login();
//		homePage.showOrders();
//		settingsPage.toggleShowOrders();
//		homePage.tapOnOrders();
		//driver.manage().window().maximize();
	}

//	@Test(groups={"sanity","smoke","regression"},description="Load the Browser",priority=0 )
//	public void loadHomePage() throws InterruptedException {
//			InitializeUI gameofthrones = new InitializeUI();
//			driver = gameofthrones.getWebdriver();
//			driver.navigate().to(URL);
//			HomePage homePage = new HomePage(driver);
//			Assert.assertTrue(homePage.verify());
//	}
	
	@Test(groups={"sanity","smoke","regression"},description="Login to the application")
	public void logintoApp() {

			HomePage homePage = new HomePage(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			homePage.showOrders();
			SettingsPage settingsPage = new SettingsPage(driver);
			settingsPage.toggleShowOrders();
			homePage.tapOnOrders();
			Assert.assertEquals(homePage.getTitle(), VmsConstants.orders_title, "Page Navigation Failed");
	}

	@Test(groups={"sanity","smoke","regression"},description="Multi Login to the application")
	public void multiOutletLogin() {
			HomePage homePage = new HomePage(driver);
			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			Assert.assertTrue(homePage.logoverify());
			homePage.showOrders();
			SettingsPage settingsPage = new SettingsPage(driver);
			settingsPage.toggleShowOrders();
			homePage.tapOnOrders();
			Assert.assertEquals(homePage.getTitle(), VmsConstants.orders_title, "Page Navigation Failed");
		}


	@Test(groups={"sanity","smoke","regression"},description="Set Global Prep Time")
    public void setGlobalPreptime() throws InterruptedException {
            HomePage homePage = new HomePage(driver);
            homePage.login();
            homePage.dismissPrepTime();
            PrepTimePage prepTimePage = new PrepTimePage(driver);
            Profile profiletab = new Profile(driver);
            prepTimePage.clickprep();
			prepTimePage.verifyFirstPage();
            prepTimePage.decreaseGlobalprep(VmsConstants.prepTimeCount);
            Thread.sleep(1000);
            int BeforeIncreasingPrepTime = profiletab.getPrepTime();
            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
            Thread.sleep(5000);
            int AfterIncreasingPrepTime = profiletab.getPrepTime();
            if (BeforeIncreasingPrepTime == 0)
            {
                int ActualPrepTime =  BeforeIncreasingPrepTime + 4 + VmsConstants.prepTimeCount;
                Assert.assertEquals(AfterIncreasingPrepTime, ActualPrepTime);
            }
            else
            {
            int ActualPrepTime = BeforeIncreasingPrepTime + VmsConstants.prepTimeCount;
            System.out.println("ActualPrepTime "+ActualPrepTime);
            Assert.assertEquals(AfterIncreasingPrepTime, ActualPrepTime);
            }
    }


    @Test(groups={"sanity","smoke","regression"},description="Update Global Prep Time")
    public void updateGlobalPreptime() throws InterruptedException {

            HomePage homePage = new HomePage(driver);
            homePage.login();
			homePage.dismissPrepTime();
            PrepTimePage prepTimePage = new PrepTimePage(driver);
            Profile profiletab = new Profile(driver);
            prepTimePage.clickprep();
            Thread.sleep(5000);
            int BeforeDecreasingPrepTime = profiletab.getPrepTime();
            prepTimePage.decreaseGlobalprep(VmsConstants.prepTimeCount);
            Thread.sleep(5000);
            int AfterDecreasingPrepTime = profiletab.getPrepTime();
            if (AfterDecreasingPrepTime == 5)
            {
             Assert.assertEquals(AfterDecreasingPrepTime, 5);
            }
            else
            {
            int ActualPrepTime = BeforeDecreasingPrepTime - VmsConstants.prepTimeCount ;
            System.out.println("ActualPrepTime "+ActualPrepTime);
            Assert.assertEquals(AfterDecreasingPrepTime, ActualPrepTime);
           }
    }

	@Test(groups={"sanity","smoke","regression"},description="Verify Profile Visibility")
	public void clickProfile() throws InterruptedException {
		HomePage homePage = new HomePage(driver);
		homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
		homePage.dismissPrepTime();
		Profile profiletab = new Profile(driver);
		PrepTimePage prepTimePage = new PrepTimePage(driver);
		profiletab.clickProfileIcon();
		int firstOutletPrepTime = profiletab.getPrepTime();
		Assert.assertTrue(profiletab.profileTab());
		profiletab.changeOutlet();
		Thread.sleep(5000);
		profiletab.selectOutlet(2);
		Thread.sleep(10000);
		prepTimePage.decreaseGlobalprep(VmsConstants.prepTimeCount);
		Thread.sleep(1000);
		int BeforeIncreasingPrepTime = profiletab.getPrepTime();
		profiletab.setGlobalPreptime(VmsConstants.prepTimeCount);
		Thread.sleep(5000);
		int AfterIncreasingPrepTime = profiletab.getPrepTime();
		if (BeforeIncreasingPrepTime == 0)
		{
			int ActualPrepTime = BeforeIncreasingPrepTime + 4 + VmsConstants.prepTimeCount;
			Assert.assertEquals(AfterIncreasingPrepTime, ActualPrepTime);
		}
		else {
			int ActualPrepTime = BeforeIncreasingPrepTime + VmsConstants.prepTimeCount;
			Assert.assertEquals(AfterIncreasingPrepTime, ActualPrepTime);
		}

		if (firstOutletPrepTime == AfterIncreasingPrepTime )
		{
			Assert.assertEquals(firstOutletPrepTime, AfterIncreasingPrepTime);
		}
		else if (firstOutletPrepTime == BeforeIncreasingPrepTime)
		{
			Assert.assertNotEquals(firstOutletPrepTime, AfterIncreasingPrepTime);
		}
		else if (firstOutletPrepTime != BeforeIncreasingPrepTime)
		{
			Assert.assertNotEquals(firstOutletPrepTime, AfterIncreasingPrepTime);
		}

	}

    @Test(groups={"sanity","smoke","regression"},description="Set Global prep time if  Global prep=0")
    public void setPrepTimeifZero() throws InterruptedException {
            HomePage homePage = new HomePage(driver);
            homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			homePage.dismissPrepTime();
            Profile profiletab = new Profile(driver);
            PrepTimePage prepTimePage = new PrepTimePage(driver);
            profiletab.clickProfileIcon();
            Assert.assertTrue(profiletab.profileTab());
            profiletab.changeOutlet();
            Thread.sleep(5000);
            profiletab.selectFirstOutlet();
            Thread.sleep(5000);
            int BeforeIncreasingPrepTime = profiletab.getPrepTime();
            if (BeforeIncreasingPrepTime == 0)
            {
                profiletab.setGlobalPreptime(VmsConstants.prepTimeCount);
            }
            else if (BeforeIncreasingPrepTime != 0)
            {
                System.out.println("Global PrepTime is already set");
            }
    }

	@SuppressWarnings("static-access")
	@Test(groups={"Sanity","Smoke","Regression"},description="Verify Add Time Slot Link")
	public void ClickOnAddTimeSlotLink() throws InterruptedException{
			HomePage homePage = new HomePage(driver);
			homePage.login();
			//homePage.login(VmsConstants.multiUsername, VmsConstants.multiPassowrd)
			homePage.dismissPrepTime();
			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Add More button is not working");

	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Cancel Added Time Slot")
	public void AddPreptimeCancel() throws InterruptedException{
			HomePage homePage = new HomePage(driver);
			homePage.login();
			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Cancel button is not working");
	}

	@Test(groups={"sanity","smoke","regression"},description="Add PrepTime Slot, Select All Days, Cancel it")
	public void AddPreptimeSlotSelectAllDaysCancel() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);
			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after Cancel :"+ PrepTimePage.totalDivCount);
			//System.out.println("Test" +prepTimepage.fetchSlotPreptime());
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Cancel button is not working");
	}


	@Test(groups={"Sanity","Smoke","Regression"},description="Select All DAYS,update slot time & increase slot time, Cancel it")
	public void AddPreptimeSlotSelectAllDaysIncreaseSlotTimeCancel() throws InterruptedException{
			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeCancelButton();
			Thread.sleep(5000);
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Cancel button is not working");

	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Select All DAYS,update slot time & decrease slot time, Cancel it")
	public void AddPreptimeSlotSelectAllDaysDecreaseSlotTimeCancel() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(1000);

			prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);
			Thread.sleep(1000);

			prepTimepage.clickPreptimeCancelButton();
			Thread.sleep(5000);
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Cancel button is not working");
	}

	@Test(groups={"sanity","smoke","regression"},description="Increase Slot-Wise Preptime, Cancel it")
	public void IncreaseSlotPreptimeCancel() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			int BeforeIncreasingSlotPrepTime = prepTimepage.fetchSlotPreptime();
			System.out.println("BeforeIncreasingSlotPrepTime : " +BeforeIncreasingSlotPrepTime);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);

			int AfterIncreasingSlotPrepTime = prepTimepage.fetchSlotPreptime();
			System.out.println("AfterIncreasingSlotPrepTime : " +AfterIncreasingSlotPrepTime);
			int ActualSlotPrepTime = BeforeIncreasingSlotPrepTime + VmsConstants.slotLevelPreptimeCount1;
			System.out.println("ActualPrepTime : "+ActualSlotPrepTime);
			Assert.assertEquals(AfterIncreasingSlotPrepTime, ActualSlotPrepTime);

			System.out.println("Preptime slots count after Cancel :"+ PrepTimePage.totalDivCount);
			prepTimepage.clickPreptimeCancelButton();
	}

	@Test(groups={"sanity","smoke","regression"},description="Decrease Slot-Wise Preptime, Cancel it")
	public void DecreaseSlotPreptimeCancel() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);

			int BeforeDecreasingSlotPrepTime = prepTimepage.fetchSlotPreptime();
			System.out.println("BeforeDecreasingSlotPrepTime : " +BeforeDecreasingSlotPrepTime);

			prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);
			Thread.sleep(5000);

			int AfterDecreasingSlotPrepTime = prepTimepage.fetchSlotPreptime();
			System.out.println("AfterDecreasingSlotPrepTime : " +AfterDecreasingSlotPrepTime);
			int ActualSlotPrepTime = BeforeDecreasingSlotPrepTime - VmsConstants.slotLevelPreptimeCount2;
			System.out.println("ActualPrepTime : "+ActualSlotPrepTime);
			Assert.assertEquals(AfterDecreasingSlotPrepTime, ActualSlotPrepTime);

			System.out.println("Decreased preptime slot  : "+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after Cancel :"+ PrepTimePage.totalDivCount);
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Cancel button is not working");
	}

	@Test(groups={"sanity","smoke","regression"},description="Default Day Time selection. Increase the prep time. Save it")
	public void DefaultDayTimeIncreasePreptimeSave() throws InterruptedException {
			HomePage homePage = new HomePage(driver);
			homePage.login();
			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
            Thread.sleep(5000);
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Preptime slots count after Save :"+ PrepTimePage.totalDivCount);
			Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Save button is not working");
	}

	@Test(groups={"sanity","smoke","regression"},description="Default Day Time selection. Decrease the prep time. Save it")
	public void DefaultDayTimeDecreasePreptimeSave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);

			prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);
			Thread.sleep(5000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Preptime slots count after Save :"+ PrepTimePage.totalDivCount);
			Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Save button is not working");

			}

	@Test(groups={"sanity","smoke","regression"},description="Increase Slot-Wise Preptime, Save it")
	public void IncreaseSlotPreptimeSave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Preptime slots count after Save :"+ PrepTimePage.totalDivCount);

			//System.out.println("Test" +prepTimepage.fetchSlotPreptime());
		Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Save button is not working");
	}

	@Test(groups={"sanity","smoke","regression"},description="Decrease Slot-Wise Preptime, Save it")
	public void DecreaseSlotPreptimeSave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			Thread.sleep(5000);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);

			prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);
			Thread.sleep(5000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Preptime slots count after Save :"+ PrepTimePage.totalDivCount);

			//			System.out.println("Test" +prepTimepage.fetchSlotPreptime());
			Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Save button is not working");

	}


	@Test(groups={"sanity","smoke","regression"},description="Update Slot-Wise Prep Time")
	public void UpdateSlotPreptime() {
		try{
			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			// Assert.assertTrue(prepTimepage.prepTimetab());
			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Saved preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);
			Assert.assertEquals(count+1, prepTimepage.getSlotCount(), "Save button is not working");
		} catch(Exception e) {
			e.getStackTrace();
		}

	}

	@Test(groups={"sanity","smoke","regression"},description="Update Slot Prep Time, Save, Increase Preptime and SAVE it ")
	public void UpdateSlotPreptimeUndo() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			//Assert.assertTrue(prepTimepage.prepTimetab());
			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Saved preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			System.out.println("Increased preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.clickPreptimeUndoButton();
			System.out.println("Undo preptime slot  : "+ PrepTimePage.totalDivCount);
			Thread.sleep(3000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Save button is not working");

	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Select particular DAYS, Save it and Verify it.")
	public void SelectDayPreptimeSlot() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
			prepTimepage.addGlobalIfNotExists();
			int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.selectMonday();
			prepTimepage.selectWednesday();

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelMins, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelMins, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelPM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelMins, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelMins, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			//prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Save button is not working");


	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Select All DAYS,update slot time & increase slot prep time, save, verify & delete ")
	public void SelectAllDayUpdateSlotimeIncreaseSlotPreptime() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Select All DAYS, Save it,Cancel delete action")
	public void SelectAllDaySaveDeleteNo() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
            prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
            int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);
			prepTimepage.clickPopUpYesButton();
            Thread.sleep(5000);

			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Delete Button is not working");
	}


	@Test(groups={"Sanity","Smoke","Regression"},description="Select All DAYS, Save it,confirm  delete action & Verify it")
	public void SelectAllDaySaveDeleteYes() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login();

			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
            prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
			int BeforeDeleteSlotCount = PrepTimePage.totalDivCount;
            int count = prepTimepage.getSlotCount();

            System.out.println("BeforeDeleteSlotCount : "+ BeforeDeleteSlotCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
			driver.navigate().refresh();

            Thread.sleep(5000);
			Assert.assertEquals(prepTimepage.getSlotCount(), count);
	}


	@Test(groups={"sanity","smoke","regression"},description="Multi Login with Increase Slot Time and Save it")
	public void MultiLoginIncreaseSlotTimeSave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
		    homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
            prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			System.out.println("Preptime slots count after Save :"+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
	}

	@Test(groups={"sanity","smoke","regression"},description="Multi Login, Update Day and Save it")
	public void MultiLoginUpdateDaySave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
		  	homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);

			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
            int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.selectMonday();
			prepTimepage.selectWednesday();

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			//		prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);
			Assert.assertEquals(count, prepTimepage.getSlotCount(), "Save Button is not working");

	}

	@Test(groups={"sanity","smoke","regression"},description="Multi Login, Update Slot Time and Save it")
	public void MultiLoginUpdateSlotTimeSave() throws InterruptedException {

			HomePage homePage = new HomePage(driver);
			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			//prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);
	}

	@Test(groups={"sanity","smoke","regression"},description="Multi Login, Update Slot Preptime and Save it")
	public void MultiLoginUpdatePrepTimeSave() throws InterruptedException {
			HomePage homePage = new HomePage(driver);
			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
			prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
            int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after clicking on ADD TIME SLOT:"+ PrepTimePage.totalDivCount);
			Thread.sleep(1000);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			//prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);
            Assert.assertEquals(count, prepTimepage.getSlotCount(), "Save Button is not working");
	}

	@Test(groups={"Sanity","Smoke","Regression"},description="Multi Login - Select All DAYS, Save it,Cancel delete action")
	public void MultiLoginSelectAllDaySaveDeleteNo() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			PrepTimePage prepTimepage = new PrepTimePage(driver);
            prepTimepage.deletePrepTimes();
            prepTimepage.clickprep();
            prepTimepage.addGlobalIfNotExists();
            int count = prepTimepage.getSlotCount();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
            Assert.assertEquals(count, prepTimepage.getSlotCount(), "Save Button is not working");
    }


	@Test(groups={"Sanity","Smoke","Regression"},description="Multi Login - Select All DAYS, Save it,confirm  delete action & Verify it")
	public void MultiLoginSelectAllDaySaveDeleteYes() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
  			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);
			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.clickPreptimeCancelButton();
			System.out.println("Preptime slots count after CANCEL :"+ PrepTimePage.totalDivCount);

			prepTimepage.clickAddTimeSlotlink();
			System.out.println("Preptime slots count after ADD TIME SLOT:"+ PrepTimePage.totalDivCount);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
	}


	@Test(groups={"Sanity","Smoke","Regression"},description="Add Preptime Slot, Select All Days, Update SlotTime, Saved it, Deleted With Cancel, Deleted with Yes")
	public void SelectAllDaysInPreptimeSlot() throws InterruptedException{

			HomePage homePage = new HomePage(driver);
			homePage.login(VmsConstants.multiUsername,VmsConstants.multiPassowrd);

			PrepTimePage prepTimepage = new PrepTimePage(driver);
			prepTimepage.clickprep();
			prepTimepage.clickAddTimeSlotlink();

			prepTimepage.selectAllDays();

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelMins, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeStart, VmsConstants.slotLevelMins, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelHours, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelPM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelHours, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelMins, VmsConstants.slotLevelIncrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.updateSlotTime(VmsConstants.slotLevelTimeEnd, VmsConstants.slotLevelMins, VmsConstants.slotLevelDecrease, VmsConstants.slotLevelAM, VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.increaseSlotprepTime(VmsConstants.slotLevelPreptimeCount1);
			prepTimepage.decreaseSlotprepTime(VmsConstants.slotLevelPreptimeCount2);

			prepTimepage.clickPreptimeSaveButton();
			Thread.sleep(5000);
			System.out.println("Test" +prepTimepage.fetchSlotPreptime());

			prepTimepage.clickPreptimeDeleteButton();
			Thread.sleep(5000);
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpCancelButton();

			Thread.sleep(5000);
			prepTimepage.clickPreptimeDeleteButton();
			System.out.println("Deleted preptime slot  : "+ PrepTimePage.totalDivCount);

			Thread.sleep(5000);
			prepTimepage.clickPopUpYesButton();
	}


	@Test(groups={"sanity","smoke","regression"},description="Confirming the Order")
	public void ConfimrOrder() {
		try{
			ManageOrders prepTimePage = new ManageOrders(driver);
			prepTimePage.Confirm_order_click();
//	Thread.sleep(1000);
	} catch(Exception e) {
        Assert.assertTrue(false);
		e.getStackTrace();
	}
}


	@Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Positive Live Order Flow")
	public void liveOrderFlow(String order_id) {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			ManageOrders manageOrders = new ManageOrders(driver);
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.Confirm_order_click();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.searchPreparingOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickFoodReady();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();
	}

	@Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Positive Live Order Flow for Single Restaurant")
	public void liveOrderFlowSingleRest(String order_id) {
		try {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
		    ManageOrders manageOrders = new ManageOrders(driver);
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            homePage.login(VmsConstants.username, VmsConstants.password);
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
		    manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();
		} catch (Exception e) {
            Assert.assertTrue(false);
			e.printStackTrace();
		}
	}


    @Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Positive Live Order Flow for Multi Restaurant")
    public void liveOrderFlowMultiRest(String order_id) {
        try {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login(VmsConstants.multiUsername, VmsConstants.multiPassowrd);
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

	@Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab", dataProvider = "createorder")
    public void outOfStockPreparingTab(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            PrepTimePage prepTimePage = new PrepTimePage(driver);
            prepTimePage.clickprep();
            softAssert.assertTrue(prepTimePage.prepTimetab());
            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab")
    public void requestCallBackNewTab(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            if(homePage.showOrders())
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.requestCallBack();
            softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab")
    public void requestCallBackNewTabVerify(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            if(homePage.showOrders())
                settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.requestCallBack();
            softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
            manageOrders.acceptRequestCallBack();
            softAssert.assertTrue(vmsHelper.iscallRequestOms(order_id));
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false);
            e.printStackTrace();
        }
    }



    @Test(groups = {"sanity","smoke","regression"},description="Verifies Global Prep Time Setting")
    public void GlobalPrep() {
        SoftAssert softAssert = new SoftAssert();
        HomePage homePage = new HomePage(driver);
        SettingsPage settingsPage = new SettingsPage(driver);
        ManageOrders manageOrders = new ManageOrders(driver);
        homePage.login();
        Assert.assertTrue(homePage.logoverify());
        if(homePage.showOrders())
            settingsPage.toggleShowOrders();
        manageOrders.clickOrdersPanel();
        PrepTimePage prepTimePage = new PrepTimePage(driver);
        prepTimePage.clickprep();
        softAssert.assertTrue(prepTimePage.prepTimetab());
        prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
        softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
        softAssert.assertAll();
    }

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab")
    public void printScreen(String order_id) {
		if(order_id == null)
			throw new SkipException("Unable to Create Order");
        SoftAssert softAssert = new SoftAssert();
        HomePage homePage = new HomePage(driver);
        SettingsPage settingsPage = new SettingsPage(driver);
        ManageOrders manageOrders = new ManageOrders(driver);
        homePage.login();
        Assert.assertTrue(homePage.logoverify());
        if(homePage.showOrders())
            settingsPage.toggleShowOrders();
        manageOrders.clickOrdersPanel();
//        PrepTimePage prepTimePage = new PrepTimePage(driver);
//        prepTimePage.clickprep();
//        softAssert.assertTrue(prepTimePage.prepTimetab());
//        prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//        softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
        manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
        manageOrders.searchNewOrders(order_id.trim());
        //softAssert.assertTrue(manageOrders.verifyOrderDetails());
        manageOrders.printInvoice();
        softAssert.assertTrue(manageOrders.checkPrintScreen());
		manageOrders.cancelOrder(order_id);
        softAssert.assertAll();
    }

    @Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Confirm Order in New Tab")
    public void newTabConfirm(String order_id) {
        try {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            ManageOrders manageOrders = new ManageOrders(driver);
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            homePage.login(VmsConstants.username, VmsConstants.password);
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Confirm Cart Details")
    public void verifyCartDetails(String order_id) {
        try {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            ManageOrders manageOrders = new ManageOrders(driver);
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            homePage.login(VmsConstants.username, VmsConstants.password);
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            softAssert.assertTrue(manageOrders.cartDetails());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(dataProvider = "createorder", groups={"sanity","smoke","regression"},description="Verifies Global Prep Time set for Order")
    public void verifyGlobalPrepTime(String order_id) {
        try {
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            ManageOrders manageOrders = new ManageOrders(driver);
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            homePage.login(VmsConstants.username, VmsConstants.password);
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            softAssert.assertTrue(manageOrders.cartDetails());
            softAssert.assertTrue(manageOrders.globalPrepTime());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch (Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in New Tab", dataProvider = "createorder")
    public void outOfStockNewTab(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.selectItems(2).outOfStockDone().sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Verifes Status in FF", dataProvider = "createorder")
    public void outOfStockOrderStatus(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Accepts Suggestion", dataProvider = "createorder")
    public void outOfStockPreparingTabAlternative(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Marks Item Delivered", dataProvider = "createorder")
    public void outOfStockPreparingTabDelivered(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            manageOrders.markDelivered(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Marks Item Cancelled", dataProvider = "createorder")
    public void outOfStockPreparingTabCancelled(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Accepts Suggestion", dataProvider = "createorder")
    public void outOfStockNewTabAlternative(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails(), "Out Of Stock not Updated in OMS");
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in New Tab & Marks Order Delivered", dataProvider = "createorder")
    public void outOfStockNewTabDelivered(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id), "Out Of Stock not Updated in OMS");

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            manageOrders.markDelivered(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    @Test(groups = {"sanity","smoke","regression"},description="Verifies Out Of Stock Flow in Preparing Tab & Cancels Order", dataProvider = "createorder")
    public void outOfStockNewTabCancelled(String order_id) {
        try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
            SoftAssert softAssert = new SoftAssert();
            VmsHelper vmsHelper = new VmsHelper();
            HomePage homePage = new HomePage(driver);
            SettingsPage settingsPage = new SettingsPage(driver);
            ManageOrders manageOrders = new ManageOrders(driver);
            homePage.login();
            Assert.assertTrue(homePage.logoverify());
            homePage.showOrders();
            settingsPage.toggleShowOrders();
            manageOrders.clickOrdersPanel();
//            PrepTimePage prepTimePage = new PrepTimePage(driver);
//            prepTimePage.clickprep();
//            softAssert.assertTrue(prepTimePage.prepTimetab());
//            prepTimePage.increaseGlobalprep(VmsConstants.prepTimeCount);
//            softAssert.assertTrue(prepTimePage.isPrepTimeSaved());
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.selectItems(2).sendSuggestions();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            softAssert.assertTrue(vmsHelper.isOutOfStock(order_id), "Out Of Stock not Updated in OMS");

            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
            manageOrders.searchNewOrders(order_id.trim());
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.Confirm_order_click().clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
            manageOrders.searchPreparingOrders(order_id);
            softAssert.assertTrue(manageOrders.verifyOrderDetails());
            manageOrders.clickFoodReady();
            manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
            manageOrders.cancelOrder(order_id);
            softAssert.assertAll();
        } catch(Exception e) {
            Assert.assertTrue(false, "Failed Due to Exception");
            e.printStackTrace();
        }
    }

    // --- Rachana --//


    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Request Callback Flow in Preparing Tab")
	public void requestCallBackPreparingTabCancelVerify(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.Confirm_order_click();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.searchPreparingOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.requestCallBack();
			softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
			manageOrders.rejectRequestCallBack();
			softAssert.assertTrue(!manageOrders.isRequestCallBackPopUp());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Request Callback Flow in Preparing Tab")
	public void requestCallBackReadyTabVerify(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.Confirm_order_click();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.searchPreparingOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickFoodReady();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.requestCallBack();
			softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
			manageOrders.acceptRequestCallBack();
			softAssert.assertTrue(vmsHelper.iscallRequestOms(order_id));
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();
		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Request Callback popup disapperas after clicking on Cancel from Request Callback popup - ready Tab")
	public void requestCallBackReadyTabCancelVerify(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			//System.out.println("Order iddddddddd : "+order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.Confirm_order_click();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			//manageOrders.searchPreparingOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.clickFoodReady();
			//manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.requestCallBack();
			softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
			manageOrders.rejectRequestCallBack();
			softAssert.assertTrue(!manageOrders.isRequestCallBackPopUp());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Request Callback Flow in Preparing Tab")
	public void requestCallBackReadyTabCancelOMSVerify(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);

			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);

			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.Confirm_order_click();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			//manageOrders.searchPreparingOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.clickFoodReady();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.READY);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.requestCallBack();
			softAssert.assertTrue(manageOrders.isRequestCallBackPopUp());
			manageOrders.rejectRequestCallBack();
			softAssert.assertTrue(vmsHelper.iscallRequestOms(order_id));
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies item out of stock in New Tab for the item without Addons/variants")
	public void itemOutOfStockDoneNewTab(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.NEW);
			manageOrders.selectItems(2);
			manageOrders.outOfStockDone();
			manageOrders.sendSuggetions();
			softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));
			//manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
			//manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Cancel Button for item out of stock in New Tab for the item without Addons/variants")
	public void itemOutOfStockCancelNewTab(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.NEW);
			manageOrders.selectItems(2);
			manageOrders.cancelOutOfStock();
			softAssert.assertTrue(!vmsHelper.isOutOfStock(order_id));
			//softAssert.assertTrue(manageOrders.verifyTab(VmsConstants.OrderStatusTab.NEW));
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Cancel Button functionality for item OOS in Preparing Tab for the item without Addons/variants")
	public void itemOutOfStockCancelPreparingTab(String order_id) {
    	try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.selectItems(2);
			manageOrders.cancelOutOfStock();
			softAssert.assertTrue(!vmsHelper.isOutOfStock(order_id));   //checks order status in OMS
			//softAssert.assertTrue(manageOrders.verifyTab(VmsConstants.OrderStatusTab.NEW)); //Checks order status in UI
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Cancel Button functionality for item OOS in Preparing Tab for the item without Addons/variants")
	public void itemOutOfStockDonePreparingTabPendingStatus(String order_id) {
    	try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.selectItems(2);
			manageOrders.outOfStockDone().sendSuggestions();
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.MORE);
            manageOrders.verifyOrderStatus(VmsConstants.OrderStatus.PENDING);
            //softAssert.assertTrue(vmsHelper.isOutOfStock(order_id), "Out Of Stock not Updated in OMS");
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies Cancel Button functionality for item OOS in Preparing Tab for the item without Addons/variants")
	public void itemOutOfStockDonePreparingTabOMSVerify(String order_id) {
    	try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.selectItems(2);
			manageOrders.outOfStockDone();
			softAssert.assertTrue(vmsHelper.isOutOfStock(order_id));   //checks order status in OMS
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies alternative items can not be selected more than 4 after marking item OOS in New Tab")
	public void selectMoreAlternativeItemsForOOSNewTab(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);

			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);

			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.NEW);

			manageOrders.selectItems(5);
			manageOrders.checkSelectedItems(5);
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies alternative items can not be selected more than 4 after marking item OOS in Preparing Tab")
   	public void selectMoreAlternativeItemsForOOSPreapringTab(String order_id) {
   		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
   			SoftAssert softAssert = new SoftAssert();
   			VmsHelper vmsHelper = new VmsHelper();
   			HomePage homePage = new HomePage(driver);

   			SettingsPage settingsPage = new SettingsPage(driver);
   			ManageOrders manageOrders = new ManageOrders(driver);

   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			if(homePage.showOrders())
   				settingsPage.toggleShowOrders();
   			manageOrders.clickOrdersPanel();
   			//TODO: set global prep time
   			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
   			manageOrders.searchNewOrders(order_id);
   			softAssert.assertTrue(manageOrders.verifyOrderDetails());
   			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
   			manageOrders.searchNewOrders(order_id);
   			softAssert.assertTrue(manageOrders.verifyOrderDetails());
   			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);

   			manageOrders.selectItems(5);
   			manageOrders.checkSelectedItems(5);
			manageOrders.cancelOrder(order_id);
   			softAssert.assertAll();

   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
   	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies that placed item can not be marked as OOS without selecting any alternative item - New tab")
	public void donotSelectAnyAlternativeItemsForOOSNewTab(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);

			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);

			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			manageOrders.searchNewOrders(order_id);
			softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.NEW);
			manageOrders.selectItems(0);
			softAssert.assertTrue(manageOrders.isDoneDisabled());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    @Test(dataProvider = "createorder", groups = {"sanity","smoke","regression"},description="Verifies that placed item can not be marked as OOS without selecting any alternative item - Preparing tab")
	public void donotSelectAnyAlternativeItemsForOOSPreparingTab(String order_id) {
		try{
			if(order_id == null)
				throw new SkipException("Unable to Create Order");
			SoftAssert softAssert = new SoftAssert();
			//VmsHelper vmsHelper = new VmsHelper();
			HomePage homePage = new HomePage(driver);
			SettingsPage settingsPage = new SettingsPage(driver);
			ManageOrders manageOrders = new ManageOrders(driver);
			homePage.login();
			Assert.assertTrue(homePage.logoverify());
			if(homePage.showOrders())
				settingsPage.toggleShowOrders();
			manageOrders.clickOrdersPanel();
			//TODO: set global prep time
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.NEW);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			manageOrders.clickOnTab(VmsConstants.OrderStatusTab.PREPARING);
			//manageOrders.searchNewOrders(order_id);
			//softAssert.assertTrue(manageOrders.verifyOrderDetails());
			//manageOrders.clickOutOfStock(VmsConstants.OrderStatusTab.PREPARING);
			manageOrders.selectItems(0);
			softAssert.assertTrue(manageOrders.isDoneDisabled());
			manageOrders.cancelOrder(order_id);
			softAssert.assertAll();

		} catch(Exception e) {
			Assert.assertTrue(false);
			e.printStackTrace();
		}
	}

    
    @Test(dataProvider = "createDiscount", groups = {"sanity","smoke","regression"},description="Create Trade Discount")
   	public void createDiscount(String discount, String minValue) {
      try{
    	    HomePage homePage = new HomePage(driver);
   			TradeDiscount tradeDiscount = new TradeDiscount(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			tradeDiscount.click();
   			Thread.sleep(2000);
   			tradeDiscount.selectOption();
   			tradeDiscount.enterDiscount(discount);
   			tradeDiscount.enterMinValue(minValue);
   			tradeDiscount.createDiscount();
   			softAssert.assertTrue(tradeDiscount.discountVisible());
   			   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 1")
   	public void createIssue1() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(1);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 2")
   	public void createIssue2() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(2);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 3")
   	public void createIssue3() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(3);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    
    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 4")
   	public void createIssue4() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(4);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    
    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 5")
   	public void createIssue5() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(5);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 6")
   	public void createIssue6() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(6);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 7")
   	public void createIssue7() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(7);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

   // @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 8 with subCategory1")
   	public void createIssue8() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory8();
   			contact.selectSubCategory(1);
   			contact.edit1();
   			contact.uploadMenu();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    //@Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 8 with subCategory2")
   	public void createIssue9() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory8();
   			contact.selectSubCategory(2);
   			contact.uploadMenu();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for single restaurant for Category 9")
   	public void createIssue10() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory9();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 1")
   	public void createIssue11() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(1);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 2")
   	public void createIssue12() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(2);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 3")
   	public void createIssue13() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(3);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 4")
   	public void createIssue14() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(4);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 5")
   	public void createIssue15() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(5);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 6")
   	public void createIssue16() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(6);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 7")
   	public void createIssue17() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory(7);
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    //@Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 8 with subCategory1")
   	public void createIssue18() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory8();
   			contact.selectSubCategory(1);
   			contact.uploadMenu();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    //@Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 8 with subCategory2")
   	public void createIssue19() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory8();
   			contact.selectSubCategory(2);
   			contact.uploadMenu();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    
    @Test(groups = {"sanity","smoke","regression"},description="Raise contact ticket for multiple restaurants for Category 9")
   	public void createIssue20() {
      try{
    	    HomePage homePage = new HomePage(driver);
   			Contact contact = new Contact(driver);
   			SoftAssert softAssert = new SoftAssert();
   			homePage.login();
   			Assert.assertTrue(homePage.logoverify());
   			contact.clickContact();
   			String contactTitle = contact.getContactTitle();
   			assertEquals(contactTitle, "CONTACT");
   			contact.selectRest();
   			contact.selectCategory9();
   			contact.enterSubject();
   			contact.enterComment();
   			contact.clickSubmit();
   			Thread.sleep(4000);
   			Assert.assertEquals(contact.successMessage(), "Submission Successful!");
   			
   		} catch(Exception e) {
   			Assert.assertTrue(false);
   			e.printStackTrace();
   		}
 
   	}

    
    @AfterMethod
	public void tearDown() {
		driver.close();
		driver.quit();
	}
}