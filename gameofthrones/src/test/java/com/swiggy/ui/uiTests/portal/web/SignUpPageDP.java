package com.swiggy.ui.uiTests.portal.web;

import org.testng.annotations.DataProvider;

import com.swiggy.ui.page.classes.portalweb.PortalConstants;

public class SignUpPageDP 
{
	
	
    @DataProvider(name = "ErrorMessageValidation")
    public Object[][] ErrorMessageValidation() {
        return new Object[][]{{PortalConstants.invalidPhoneNo, PortalConstants.invalidName,PortalConstants.invalidemail,PortalConstants.invalidPwd}};
    }
    
    @DataProvider(name = "NewUserRegistration")
    public Object[][] newUserCreationData() {
        return new Object[][]{{PortalConstants.validPhoneNo, PortalConstants.validName,PortalConstants.validEmail,PortalConstants.validPwd}};
    }
    
    
    @DataProvider(name = "RegisteredUser")
    public Object[][] inValidUserLogin() 
    {
        return new Object[][]{{PortalConstants.registeredPhoneNo, PortalConstants.validName,PortalConstants.validEmail,PortalConstants.validPwd}};
    }
    
    
        

}