package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;
import java.util.List;

public class preOrder_iOS {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;

    @DataProvider(name = "LoginData")
    public Object[][] getdata() {

        return new Object[][]{{Constants.userName2, Constants.password2}};

    }

    @BeforeClass

    public void setUptest() throws UnknownHostException {


        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);

    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify PreOrder for Non-Logged in user!")
    public void verifyPreOrderFlow(String userName2, String password2) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        AndroidUtil.sleep(3000);
        restaurantPage.clickOnPreorderNowTimeslot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(0);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));


    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify PreOrder for Logged in user!")
        public void verifyPreOrderFlowForLoggedInUser(String userName2, String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        AndroidUtil.sleep(3000);
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(1);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));


    }

    @Test(dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Preorder for Favourite restaurant")
    public void verifyPreOrderForFavouriteRestaurant (String userName2, String password2) {
        Location location=new Location(Constants.lat1,Constants.lng1,0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.tapOnLogin();
        login.LogintoApp(userName2, password2);
        AndroidUtil.sleep(3000);
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.selectDateTabForPreorder(0);
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        Assert.assertTrue(restaurantPage.checkElementDisplay(restaurantPage.fav_icon),"Favourite icon is not displayed");
        restaurantPage.clickonFavIcon();
        String restaurantName = restaurantPage.getRestaurantName();
        restaurantPage.clickOnBackButton();
        homePage.openFilter();
        Assert.assertTrue(homePage.checkElementDisplay(homePage.txt_SortFilter),"Wrong filter header displayed");
        homePage.clickOnFilterOffersAndMore();
        homePage.setSelectOffersAndMore(Constants.SHOW_RESTAURANT_WITH_MY_FAVOURITES);
        homePage.clickonApplySortAndFilterButton();
        AndroidUtil.sleep(1000);
        homePage.selectRestaurant(null, 1);
        restaurantPage.changeTimeSlotfromMenu();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        restaurantPage.setPreOrderTime();
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.clickonPreorderSla();
        restaurantPage.setPreOrderTime();
        checkoutPage.proceedToPay();
        Assert.assertEquals(paymentPage.getSubHeaderFromPaytmenPage().toLowerCase(),Constants.PAYMENT_SCREEN_HEADER.toLowerCase());
        Assert.assertTrue(paymentPage.checkElementDisplay(paymentPage.txt_walletHeader));

    }

    @Test (dataProvider = "LoginData", enabled = true, groups = {"regression"}, description = "Verify Preorder for location not supporting Preorder")
    public void verifyPreOrderForLocationNotSupportingPreorder (String userName2, String password2) {
        Location location = new Location(Constants.lat2, Constants.lng2, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertFalse(homePage.checkElementDisplay(homePage.txt_preOrderOnbarding));


    }
    @AfterMethod
    public void tearDown() {

        driver.launchApp();

    }


}
