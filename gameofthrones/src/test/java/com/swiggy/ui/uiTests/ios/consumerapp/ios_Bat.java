package com.swiggy.ui.uiTests.ios.consumerapp;

import com.swiggy.ui.api.ConsumerApiHelper;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.pojoClasses.Restaurantmenu;
import com.swiggy.ui.pojoClasses.SwiggyPop;
import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.Constants;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.html5.Location;
import org.testng.Assert;
import org.testng.annotations.*;

import java.net.UnknownHostException;
import java.util.List;

public class ios_Bat {

    public AppiumDriver driver;
    InitializeUI inti;
    LaunchPage launch;
    HomePage homePage;
    LoginPage login;
    AddressPage addressPage;
    RestaurantPage restaurantPage;
    CheckoutPage checkoutPage;
    ExplorePage explorePage;
    AccountPage accountPage;
    PaymentPage paymentPage;
    orderDetailPage orderDetailPage;
    ConsumerApiHelper consumerApiHelper;
    SwiggypopPage swiggypopPage;
    TrackPage trackPage;
    Double lat1 = 12.932815;
    Double lng1 = 77.603578;

    Double lat2 = 27.834105;
    Double lng2 = 76.171977;

    Double lat3 = 19.229388800000002;
    Double lng3 = 72.8569977;


    @DataProvider(name = "LoginData")
    public Object[][] getdata() {
        return new Object[][]{{"9950611829", "qwerty123"}};
    }

    @BeforeClass
    public void setUptest() throws UnknownHostException {
        consumerApiHelper = new ConsumerApiHelper();
        inti = new InitializeUI();
        driver = inti.getAppiumDriver();
        launch = new LaunchPage(inti);
        homePage = new HomePage(inti);
        login = new LoginPage(inti);
        addressPage = new AddressPage(inti);
        restaurantPage = new RestaurantPage(inti);
        checkoutPage = new CheckoutPage(inti);
        explorePage = new ExplorePage(inti);
        accountPage = new AccountPage(inti);
        paymentPage = new PaymentPage(inti);
        orderDetailPage = new orderDetailPage(inti);
        trackPage = new TrackPage(inti);
        swiggypopPage = new SwiggypopPage(inti);
    }

    @Test(dataProvider = "LoginData",enabled = true, groups = {"regression"}, description = "Sign up flow without referral")
    public void verifySignupWithoutReferral(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.singupWithoutReferral();
    }

    @Test(dataProvider = "LoginData", enabled = true, groups={"regression"},description="Verify Login scenario from Account")
    public void verifyLoginFromAccountTab(String username, String password) {
        Location location = new Location(lat1, lng1, 0);
        System.out.println("ios driver" + driver);
        driver.setLocation(location);
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        AndroidUtil.sleep(1000);
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username,password);
    }

    @Test(dataProvider = "LoginData", enabled = true,groups={"regression"},description="verifySwiggyPopFlow")
    public void verifySwiggyPopFlow(String username,String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        List<SwiggyPop> popObj = consumerApiHelper.swiggyPopListing(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.clickOnSortNudge();
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        homePage.openSwiggyPop();
        swiggypopPage.clickOnFirstPOPImageIos();
        if (!popObj.isEmpty() && popObj.get(0).isOpened()) {
            swiggypopPage.clickonBackButton();
            AndroidUtil.sleep();
            Assert.assertEquals(restaurantPage.getPOPCartcleartTitle(), Constants.SWIGGYPOP_PRESSBACK_TITTLE);
            Assert.assertEquals(restaurantPage. getPOPCartcleartMessage(), Constants.SWIGGYPOP_PRESSBACK_MESSAGE);
            swiggypopPage.clickOnOkGotIt();
        }else{
            Assert.assertTrue(swiggypopPage.isNextAvailableTextDisplay());
        }
    }
    @Test(dataProvider = "LoginData",  enabled = true, groups = {"regression"}, description = "verifyNormalOrderFlow")
    public void verifyNormalOrderFlow(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
        driver.setLocation(location);
        consumerApiHelper.clearCart(username, password);
        consumerApiHelper.deleteAllAddress(username, password);
        consumerApiHelper.createNewAddress(username, password, Double.toString(Constants.lat1), Double.toString(Constants.lng1), "WORK");
        List<Menulist> al = consumerApiHelper.getRestaurantList(Double.toString(Constants.lat1), Double.toString(Constants.lng1));
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.locationConfirmationPopup();
        homePage.clickOnSortNudge();
        AndroidUtil.scrollios(driver, AndroidUtil.Direction.UP);
        homePage.selectRestaurant(null, 1);
        Restaurantmenu restaurantMenuObject = consumerApiHelper.getRestaurantMenu(al.get(0).getRestaurantId(), al.get(0).getUuid(), Double.toString(lat2), Double.toString(lng2), username, password);
        restaurantPage.addItemtest();
        restaurantPage.openCartFromRestaurantPage();
        checkoutPage.increaseItemCount();

    }

    @Test(dataProvider = "LoginData",enabled = true,groups = {"regression"}, description = "link and delink wallet")

    public void verifyLinkWallet(String username, String password) {
        Location location = new Location(Constants.lat1, Constants.lng1, 0);
         driver.setLocation(location);
        consumerApiHelper.delinWallet(username, password, "mobikwik");
        consumerApiHelper.delinWallet(username, password, "paytm");
        launch.clickonAllowButton();
        launch.clickOnSetDeliveryLocation();
        Assert.assertEquals(homePage.getPreorderOnboardingText().toLowerCase(),Constants.PREORDER_ONBOARDING.toLowerCase());
        restaurantPage.clickonPreOrderTimeSlot();
        Assert.assertEquals(homePage.getHeaderOfSelectTimerForPreorder().toLowerCase(),Constants.PREORDER_SELECTTIMER_HEADER.toLowerCase());
        homePage.clickOnSetDeliveryTimeButtonForPreorder();
        homePage.clickOnSortNudge();
        homePage.removeSwiggyPop();
        AndroidUtil.sleep(1000);
        homePage.openAccountTab();
        accountPage.clickOnLoginFromAccount();
        login.LogintoApp(username, password);
        accountPage.openManagePayment();
        accountPage.linkWallet("paytm");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        accountPage.pressBackKeyFromLinkWalltetPage();
        accountPage.linkWallet("freecharge");
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        System.out.println(accountPage.getWalledLinkHeaderText());
        accountPage.pressBackKeyFromLinkWalltetPage();
        accountPage.linkWallet("mobikwik");
        System.out.println(accountPage.getWalledLinkHeaderText());
        Assert.assertEquals(accountPage.getMobileNumberFromWalledLink(), username);
        accountPage.pressBackKeyFromLinkWalltetPage();
    }

    @AfterMethod
    public void tearDown() {

        driver.launchApp();
        driver.resetApp();
    }
    @AfterClass
    public void stopServer() {
        // inti.appiumServer.stopServer();
    }

}





