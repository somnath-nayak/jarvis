package com.swiggy.ui.page.classes.vendor.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import framework.gameofthrones.Aegon.IElement;
import com.swiggy.ui.utils.WebUtil;

public class TradeDiscount extends WebUtil {

	By discountIcon = By.xpath("//*[@title='Discounts']");
	By discount = By.xpath("//*[@class='icon discounts-icon']");
	By createDiscount = By.xpath("//*[@class='create-trade-discount']//*[@ng-click='initCreateTradeDiscount()']");
	By selectOption = By.xpath("//*[@class='select-type ng-pristine ng-untouched ng-invalid ng-invalid-required']");
	By selectNet = By.xpath("//*[@label='Net']");
	By selectPercentage = By.xpath("//*[@label='Percentage']");
	By enterDiscount = By.xpath(".//*[@class='ng-pristine ng-invalid ng-invalid-required ng-valid-pattern ng-valid-maxlength ng-touched']");
	By enterMinOrderValue = By.xpath(".//*[@class='ng-pristine ng-untouched ng-valid ng-valid-pattern ng-valid-maxlength']");
	//By discountDate = By.id(".//*[@id='tradeDiscountDate']");
	By schedule = By.xpath(".//*[@ng-click='scheduleTradeDiscount()']");
	By discountVisible = By.xpath(".//*[@class='live-discount-head']");
	By viewLiveDiscount = By.xpath(".//*[@ng-click='toggleTradeDiscountLive(showliveDetails)']");
	
	
	private WebDriver driver;


	public TradeDiscount(WebDriver driver) {
		this.driver = driver;
	}

	public boolean verify() {
		return this.isVisible(driver, discountIcon);
	}

	public TradeDiscount click(){
		try{
			
			click(driver, discount );
		}
			catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}


	public TradeDiscount selectOption(){
		
		try{
			
			click(driver, selectOption);
			click(driver, selectNet);
		}
			catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public TradeDiscount enterDiscount (String discount){
		
		try{
			enterText(driver, enterDiscount, discount);
		}   
		    catch (Exception e) {
			e.printStackTrace();
		}
		return this;	
	}
	
  public TradeDiscount enterMinValue (String minValue){
		
		try{
			enterText(driver, enterMinOrderValue, minValue);
		}   
		    catch (Exception e) {
			e.printStackTrace();
		}
		return this;	
	}
	
  public TradeDiscount createDiscount(){
	  
	  try{
		  
		  click(driver, schedule);
		}   
	    catch (Exception e) {
		e.printStackTrace();
	}
    	return this;	
  }
 
  public boolean discountVisible(){
	  return this.isVisible(driver, discountVisible);
	}
	  
 }
  
