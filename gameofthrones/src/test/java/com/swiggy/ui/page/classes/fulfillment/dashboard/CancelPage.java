package com.swiggy.ui.page.classes.fulfillment.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.swiggy.ui.utils.WebUtil;


public class CancelPage extends WebUtil {
	
	WebDriver driver = null;
	
	By cancel = By.xpath("//*[@id='sample-table-2']/div/div[1]/accordion/div/div/div[2]/div/fieldset/div/div[2]/div[7]/div[2]/button");
	By selectReason = By.xpath("//*[@id='cancel-order-window']/div[2]/form/div[2]/div/select");
	By swiggy = By.xpath("//*[@id='cancel-order-window']//*[@value='swiggy']");
	By no = By.xpath("//*[@id='cancel-order-window']/div[2]/form/p[2]/label[2]/input");
	By cancelButton = By.xpath("//*[@id='cancel-order-window']//*[@type='submit']");
	By detailedreason = By.xpath("//*[@id='onSubDispositionSelect']");
	

	public CancelPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public CancelPage cancel() {
		this.scrollTo(driver, cancel);
		this.click(driver, cancel);
		return this;
	}

	public CancelPage selectReason() {
		try{
		this.click(driver, swiggy);
		Select select = new Select(driver.findElement(selectReason));
		select.selectByIndex(4);
		Select select2 = new Select(driver.findElement(detailedreason));
		select2.selectByIndex(1);
		Thread.sleep(3000);
//		this.click(driver, no);
		Assert.assertTrue(driver.findElement(cancelButton).isEnabled(), "cancel button is not enabled");
		this.click(driver, cancelButton);
		this.acceptAlert(driver);
		} catch(Exception e) {
			e.getStackTrace();
		}
		return this;
	}
}
