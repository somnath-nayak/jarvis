package com.swiggy.ui.page.classes.vendor.dashboard;

import com.swiggy.api.erp.vms.helper.PrepTimeServiceHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.swiggy.ui.utils.WebUtil;

import java.util.List;

public class PrepTimePage extends WebUtil {


	private WebDriver driver = null;
	//----------Global level Prep time Elements----------------
	//By clickPreptime = By.xpath("//*[@class='btn btn-warning'][@id='set-preptime-button']");
	By clickPreptime = By.id("set-preptime-button");
	//By prepTimetab=By.xpath("//*[@ng-class=\"{'active':prepTimeTabActive}\"]");
//	By increaseGlobalprep=By.xpath("//*[@ng-click=\"increaseGloablPrepTime(globals.choosenOutlet.rest_rid)\"]");
//	By decreaseGlobalprep=By.xpath("//*[@ng-click=\"decreaseGlobalPrepTime(globals.choosenOutlet.rest_rid)\"]");
	By increaseGlobalprep=By.id("increase-global-preptime-button");
	By decreaseGlobalprep=By.id("decrease-global-preptime-button");
	By increase = By.id("increase-global-preptime-button-intro");
	By decrease = By.id("decrease-global-preptime-button-intro");
    By fetchPreptime = By.xpath("//input[@class='form-control ng-pristine ng-valid-pattern ng-valid-minlength ng-valid-maxlength ng-valid-max ng-valid ng-valid-min ng-touched']");
    By message = By.xpath(".//*[@id='toast-wrap']/div");
    By prepTimetab=By.xpath("//*[@ng-class=\"{'active':prepTimeTabActive}\"]");
    By next_button = By.xpath("//button[contains(text(), 'Next')]");
    By done_button = By.xpath("//button[contains(text(),'Done')]");


	//----------Slot-Wise Prep time Elements----------------

	public static int totalDivCount;
	public static String strDynamicXP;
	List <WebElement> listofTotalSlots;    

	String fetchSlotpreptimeLink = "']/form/div[1]/div[3]/div/label/input";
	By slotXpath = By.xpath("//*[starts-with(@id,'preptime-slot-')]");    
	By clickSlotTimelink = By.xpath("//*[@id='add-preptime-slot-link']/h4/u");

	String strCommonPreptime = "//*[@id='preptime-slot-";

	String strIncreasePreptime = "']/form/div[1]/div[3]/div/button[2]";   
	String strDecreasePreptime = "']/form/div[1]/div[3]/div/button[1]";

	String strSavePreptime   = "']/form/div[2]/div/ul/li[2]/button";       
	String strDeletePreptime = "']/form/div[2]/div/ul/li[1]/button";    
	String strUndoPreptime   = "']/form/div[2]/div/ul/li[1]/button";    
	String strCancelPreptime = "']/form/div[2]/div/ul/li[1]/button";

	String popupYesButton = "//button[contains(text(),'Yes')]";
	String popupCancelButton = "//button[contains(text(),'Cancel')][@role='button']";    

	String strDays = "']/form/div[1]/div[1]/h5";
	String strDaysDownArrow = "']/form/div[1]/div[1]/div/a/div/div[2]/i";	
	String strMonday    = "']/form/div[1]/div[1]/div/div/div/ul/li[3]";
	String strWednesday = "']/form/div[1]/div[1]/div/div/div/ul/li[5]";		
	String strAlldays =  "']/form/div[1]/div[1]/div/div/div/ul/li[1]/label/input";
	
	String strTime = 	"']/form/div[1]/div[2]/h5";
	String strTimeDownArrow       = "']/form/div[1]/div[2]/div/a/div/div[2]/i";
	String strStartHoursUpArrow   = "']/form/div[1]/div[2]/div/div/div[1]/div[1]/label/span/a[1]/i";
	String strStartHoursDownArrow = "']/form/div[1]/div[2]/div/div/div[1]/div[1]/label/span/a[2]/i";   
	String strStartHoursVal       = "']/form/div[1]/div[2]/div/div/div[1]/div[1]/label/input";	

	String strStartMinUpArrow   = "']/form/div[1]/div[2]/div/div/div[1]/div[3]/label/span/a[1]/i";
	String strStartMinDownArrow = "']/form/div[1]/div[2]/div/div/div[1]/div[3]/label/span/a[2]/i";   
	String strStartMinVal       = "']/form/div[1]/div[2]/div/div/div[1]/div[3]/label/input";
	String strStartAmPm      = "']/form/div[1]/div[2]/div/div/div[1]/div[4]/label/input";
	
	String strEndHoursUpArrow   = "']/form/div[1]/div[2]/div/div/div[2]/div[1]/label/span/a[1]/i";
	String strEndHoursDownArrow = "']/form/div[1]/div[2]/div/div/div[2]/div[1]/label/span/a[2]/i";   
	String strEndHoursVal       = "']/form/div[1]/div[2]/div/div/div[2]/div[1]/label/input";    

	String strEndMinUpArrow   = "']/form/div[1]/div[2]/div/div/div[2]/div[3]/label/span/a[1]/i";
	String strEndMinDownArrow = "']/form/div[1]/div[2]/div/div/div[2]/div[3]/label/span/a[2]/i";   
	String strEndMinVal       = "']/form/div[1]/div[2]/div/div/div[2]/div[3]/label/input";
	String strEndAmPm      = "']/form/div[1]/div[2]/div/div/div[2]/div[4]/label/input";

	String strSlotTime   = "']/form/div[1]/div[2]/div/a";
	String strDoneButton = "']/form/div[1]/div[2]/div/div/div[3]/button";	
	
	
	public PrepTimePage(WebDriver driver) {
		this.driver = driver;
	}

	public PrepTimePage clickprep() {

		try {
			click(driver, clickPreptime);
			Thread.sleep(1500);
		} catch (Exception e) {
		}
		return this;
	}


	public PrepTimePage increaseGlobalprep(int count){
		for(int i =1; i <= count; i++){
			click(driver, increaseGlobalprep);
		}
		return this;
	}

	public PrepTimePage decreaseGlobalprep(int count){
		for(int i =count; i >= 1; i--){
			click(driver, decreaseGlobalprep);
		}
		return this;
	}


	 public PrepTimePage fetchPreptime(){
	    	getText(driver,fetchPreptime);
	    	return this;
	 }

	 public boolean isPrepTimeSaved() {
	 	return this.isVisible(driver, message);
	 }

	//=========================SLOT-WISE PREPTIME METHODS======================

	public PrepTimePage clickAddTimeSlotlink(){
		click(driver, clickSlotTimelink);
		getPrepTimeSlotCount();
		return this;		 
	}

	public PrepTimePage getPrepTimeSlotCount(){
		listofTotalSlots = driver.findElements(slotXpath);
		totalDivCount = listofTotalSlots.size();
		//System.out.println("Preptime slots inside getPrepTimeSlotCount() :"+ totalDivCount);			
		return this;
	}

	public int getSlotCount(){
		listofTotalSlots = driver.findElements(slotXpath);
		return listofTotalSlots.size();
	}

	public PrepTimePage setDynamicXPath(String xp){
		getPrepTimeSlotCount();
		strDynamicXP = strCommonPreptime + totalDivCount + xp;
		//System.out.println("Preptime dynamic slot string in setDynamicXPath() :"+ strDynamicXP);				
		return this;
	}

	public PrepTimePage labelDays(){
		getPrepTimeSlotCount();	
		strDynamicXP=null;
		setDynamicXPath(strDays);		
		//System.out.println("Preptime dynamic slot xpath of labelDays() :"+ strDays);
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();			
		return this;
	}

	public PrepTimePage selectDaysDownArrow(){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside selectDaysDownArrow() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strDaysDownArrow);	 	
		//System.out.println("Preptime dynamic slot xpath of selectDaysDownArrow() :"+ strDaysDownArrow);			
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();			
		return this;
	}
	
	public boolean isAllDaysSelected(){
	getPrepTimeSlotCount();	
	strDynamicXP=null;
	setDynamicXPath(strAlldays);		
	//System.out.println("Preptime dynamic slot xpath of isAllDaysSelected() :"+ driver.findElement(By.xpath(strDynamicXP)).isSelected());
	return driver.findElement(By.xpath(strDynamicXP)).isSelected();
	}
	
	public PrepTimePage selectAllDays(){
		getPrepTimeSlotCount();	
		labelDays();
		selectDaysDownArrow();
		//System.out.println("Preptime slots inside selectAllDays() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strAlldays);	 	
		//System.out.println("Preptime dynamic slot xpath of selectAllDays() :"+ strAlldays);			
		if (!isAllDaysSelected())
		{
			click(driver, By.xpath(strDynamicXP));
		}else{
			click(driver, By.xpath(strDynamicXP));				
			for (int i=3;i<=9;i++){
				String strDays=null;
				strDays = "']/form/div[1]/div[1]/div/div/div/ul/li[" +i+ "]";
				strDynamicXP=null;
				setDynamicXPath(strDays);	 	
				//System.out.println("Preptime dynamic slot xpath of selectAllDays() :"+ strDays);		
				click(driver, By.xpath(strDynamicXP));
			}
		}
		getPrepTimeSlotCount();			
		labelTime();	
		return this;
	}

	public PrepTimePage selectMonday(){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside selectMonday() :"+ totalDivCount);
		labelDays();
		selectDaysDownArrow();		
		strDynamicXP=null;
		if (isAllDaysSelected())
		{
		setDynamicXPath(strAlldays);	
		//System.out.println("Preptime dynamic slot xpath of selectMonday() - strMonday:"+ strDynamicXP);					
		click(driver, By.xpath(strDynamicXP));
		
		strDynamicXP=null;
		setDynamicXPath(strMonday);	 	
		//System.out.println("Preptime dynamic slot xpath of selectMonday() - strMonday:"+ strDynamicXP);					
		click(driver, By.xpath(strDynamicXP));
		} else 
		{
			strDynamicXP=null;
			setDynamicXPath(strMonday);	 	
			//System.out.println("Preptime dynamic slot xpath of selectMonday() - strMonday:"+ strDynamicXP);					
			click(driver, By.xpath(strDynamicXP));			
		}
		getPrepTimeSlotCount();		
		labelTime();		
		return this;
	}

	public PrepTimePage selectWednesday(){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside selectMonday() :"+ totalDivCount);
		labelDays();
		selectDaysDownArrow();		
		strDynamicXP=null;
		if (isAllDaysSelected())
		{
		setDynamicXPath(strAlldays);	
		//System.out.println("Preptime dynamic slot xpath of strWednesday() - strWednesday:"+ strDynamicXP);					
		click(driver, By.xpath(strDynamicXP));
		
		strDynamicXP=null;
		setDynamicXPath(strWednesday);	 	
		//System.out.println("Preptime dynamic slot xpath of strWednesday() - strWednesday:"+ strDynamicXP);					
		click(driver, By.xpath(strDynamicXP));
		} else 
		{
			strDynamicXP=null;
			setDynamicXPath(strWednesday);	 	
			//System.out.println("Preptime dynamic slot xpath of strWednesday() - strWednesday:"+ strDynamicXP);					
			click(driver, By.xpath(strDynamicXP));			
		}
		getPrepTimeSlotCount();		
		labelTime();		
		return this;
	}
	public PrepTimePage labelTime(){
		getPrepTimeSlotCount();	
		strDynamicXP=null;
		setDynamicXPath(strTime);		
		//System.out.println("Preptime dynamic slot xpath of labelTime() :"+ strTime);
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();			
		return this;
	}

	public PrepTimePage selectTimeDownArrow(){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside selectDaysDownArrow() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strTimeDownArrow);	 	
		//System.out.println("Preptime dynamic slot xpath of selectTimeDownArrow() :"+ strTimeDownArrow);			
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();			
		return this;
	}

	public boolean isSlotTimeClicked(){
		getPrepTimeSlotCount();	
		strDynamicXP=null;
		setDynamicXPath(strSlotTime);		
		//System.out.println("Preptime dynamic slot xpath of isSlotTimeClicked() :"+ driver.findElement(By.xpath(strDynamicXP)).isSelected());
		return driver.findElement(By.xpath(strDynamicXP)).isSelected();
	}
	
	public PrepTimePage updateSlotAMPM(String strGivenAmPm, String strPageAmPm){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside updateSlotAMPM() :"+ totalDivCount);
		setDynamicXPath(strPageAmPm);
		//System.out.println("Preptime dynamic slot xpath of updateSlotAMPM() - strPageAmPm :"+ strPageAmPm);
		
		WebElement weTemp = driver.findElement(By.xpath(strDynamicXP));
		String strTemp = weTemp.getAttribute("value").toString();
		
		if (!strGivenAmPm.equalsIgnoreCase(strTemp))
		{
			click(driver, By.xpath(strDynamicXP));	
		}
		//System.out.println("Preptime dynamic slot xpath of updateSlotAMPM() - strDynamicXP :"+ strDynamicXP);
		getPrepTimeSlotCount();			
		return this;
	}

	public PrepTimePage commitSlotChanges()
	{
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside commitSlotChanges() :"+ totalDivCount);	
		strDynamicXP=null;
		setDynamicXPath(strDoneButton);		
		click(driver, By.xpath(strDynamicXP));
		//System.out.println("Preptime dynamic slot xpath of commitSlotChanges() - strDoneButton :"+ strDynamicXP);
		return this;	
	}
		
	public PrepTimePage updateSlotTime(String strSlotVal, String strHrMn, String strAction, String strAmPm, int count) throws InterruptedException{
		getPrepTimeSlotCount();	
		labelTime();
		selectTimeDownArrow();
		strDynamicXP=null;
		if (strSlotVal.equalsIgnoreCase("START"))
		{
			updateSlotAMPM(strAmPm, strStartAmPm);

			strDynamicXP=null;			
			if (strHrMn.equalsIgnoreCase("HOUR"))
			{
				setDynamicXPath(strStartHoursVal);
				click(driver, By.xpath(strDynamicXP));
				//System.out.println("Preptime dynamic slot xpath of updateSlotTime() - strStartHoursVal :"+ strDynamicXP);				
				
				strDynamicXP=null;				
				if (strAction.equalsIgnoreCase("INCREASE"))
				{
					setDynamicXPath(strStartHoursUpArrow);
					//System.out.println("Preptime slots inside updateSlotTime() - strStartHoursUpArrow :"+ strDynamicXP);			
				} 
				else if (strAction.equalsIgnoreCase("DECREASE"))
				{
					setDynamicXPath(strStartHoursDownArrow);						
					//System.out.println("Preptime slots inside updateSlotTime() - strStartHoursDownArrow :"+ strDynamicXP);							
				}
			} 
			else if (strHrMn.equalsIgnoreCase("MIN"))
			{
				setDynamicXPath(strStartMinVal);
				click(driver, By.xpath(strDynamicXP));
				System.out.println("Preptime dynamic slot xpath of updateSlotTime() - strStartMinVal : "+ strDynamicXP);	
				
				strDynamicXP=null;								
				if (strAction.equalsIgnoreCase("INCREASE"))
				{
					setDynamicXPath(strStartMinUpArrow);	
					//System.out.println("Preptime slots inside updateSlotTime() - strStartMinUpArrow :"+ strDynamicXP);			
				} 
				else if (strAction.equalsIgnoreCase("DECREASE"))
				{
					setDynamicXPath(strStartMinDownArrow);	
					//System.out.println("Preptime slots inside updateSlotTime() - strStartMinDownArrow :"+ strDynamicXP);			
				}
			}
		}
		else if (strSlotVal.equalsIgnoreCase("END"))
		{
			updateSlotAMPM(strAmPm, strEndAmPm);

			strDynamicXP=null;			
			if (strHrMn.equalsIgnoreCase("HOUR"))
			{
				setDynamicXPath(strEndHoursVal);
				click(driver, By.xpath(strDynamicXP));
				//System.out.println("Preptime dynamic slot xpath of updateSlotTime() - strEndHoursVal :"+ strDynamicXP);	

				strDynamicXP=null;
				if (strAction.equalsIgnoreCase("INCREASE"))
				{
					setDynamicXPath(strEndHoursUpArrow);
					//System.out.println("Preptime slots inside updateSlotTime() - invisibleElement - strEndHoursUpArrow :"+ strDynamicXP);			
				} 
				else if (strAction.equalsIgnoreCase("DECREASE"))
				{
					setDynamicXPath(strEndHoursDownArrow);	
					//System.out.println("Preptime slots inside updateSlotTime() - invisibleElement - strEndHoursDownArrow :"+ strDynamicXP);								
				}
			} 
			else if (strHrMn.equalsIgnoreCase("MIN"))
			{
				setDynamicXPath(strEndMinVal);
				click(driver, By.xpath(strDynamicXP));
				//System.out.println("Preptime dynamic slot xpath of updateSlotTime() - strEndMinVal :"+ strDynamicXP);	
				
				strDynamicXP=null;								
				if (strAction.equalsIgnoreCase("INCREASE"))
				{
					setDynamicXPath(strEndMinUpArrow);
					//System.out.println("Preptime slots inside updateSlotTime() - strEndMinUpArrow :"+ strDynamicXP);								
				} 
				else if (strAction.equalsIgnoreCase("DECREASE"))
				{
					setDynamicXPath(strEndMinDownArrow);		
//				System.out.println("Preptime slots inside updateSlotTime() - strEndMinDownArrow :"+ strDynamicXP);								
				}
			}
		}



		//System.out.println("Preptime dynamic slot xpath of updateSlotTime() :"+ strDynamicXP);			
		for(int i =1; i <= count; i++){
			click(driver, By.xpath(strDynamicXP));
			Thread.sleep(1000);
//			System.out.println("Preptime dynamic slot xpath of updateSlotTime() :"+ strDynamicXP);	
		}

		getPrepTimeSlotCount();
		commitSlotChanges();

		return this;
	}

	
	public int fetchSlotPreptime(){
		getPrepTimeSlotCount();
		setDynamicXPath(fetchSlotpreptimeLink);
		WebElement fetchPreptimeVal = driver.findElement(By.xpath(strDynamicXP));
		System.out.println("Preptime dynamic slot xpath of fetchPreptimeVal.getAttribute :"+ fetchPreptimeVal.getAttribute("value"));		
		return Integer.parseInt(fetchPreptimeVal.getAttribute("value"));
	}

	public PrepTimePage increaseSlotprepTime(int count){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside increaseSlotprepTime() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strIncreasePreptime);	 	
		System.out.println("Preptime dynamic slot xpath of increaseSlotprepTime() :"+ strDynamicXP);			
		for(int i =1; i <= count; i++){
			click(driver, By.xpath(strDynamicXP));
		}
		getPrepTimeSlotCount();			
		return this;
	}

	public PrepTimePage decreaseSlotprepTime(int count){
		getPrepTimeSlotCount();	
		//System.out.println("Preptime slots inside decreaseSlotprepTime() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strDecreasePreptime);	
		System.out.println("Preptime dynamic slot xpath of decreaseSlotprepTime() :"+ strDynamicXP);			
		for(int i =1; i <= count; i++){
			click(driver, By.xpath(strDynamicXP));
		}
		getPrepTimeSlotCount();			
		return this;
	}	

	public PrepTimePage clickPreptimeSaveButton(){
		getPrepTimeSlotCount();
		//System.out.println("Preptime slots inside clickPreptimeSaveButton() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strSavePreptime);
		System.out.println("Preptime dynamic slot xpath of clickPreptimeSaveButton() :"+ strDynamicXP);				
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();
		return this;		 
	}

	public PrepTimePage clickPreptimeCancelButton(){
		getPrepTimeSlotCount();
		//System.out.println("Preptime slots inside clickPreptimeCancelButton() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strCancelPreptime);
		System.out.println("Preptime dynamic slot xpath of clickPreptimeCancelButton() :"+ strDynamicXP);				
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();
		return this;		 
	}

	public PrepTimePage clickPreptimeDeleteButton() throws InterruptedException{
		getPrepTimeSlotCount();
		//System.out.println("Preptime slots inside clickPreptimeDeleteButton() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strDeletePreptime);
		System.out.println("Preptime dynamic slot xpath of clickPreptimeDeleteButton() :"+ strDynamicXP);		
		click(driver, By.xpath(strDynamicXP));		
		getPrepTimeSlotCount();
		return this;		 
	}

	public PrepTimePage clickPreptimeUndoButton(){
		getPrepTimeSlotCount();
		//System.out.println("Preptime slots inside clickPreptimeUndoButton() :"+ totalDivCount);
		strDynamicXP=null;
		setDynamicXPath(strUndoPreptime);
		System.out.println("Preptime dynamic slot xpath of clickPreptimeUndoButton() :"+ strDynamicXP);				
		click(driver, By.xpath(strDynamicXP));
		getPrepTimeSlotCount();
		return this;		 
	}		

	public PrepTimePage clickPopUpCancelButton(){
		//System.out.println("Preptime slots inside clickPopUpCancelButton());
		System.out.println("Preptime dynamic slot xpath of clickPopUpCancelButton() :"+ popupCancelButton);				
		click(driver, By.xpath(popupCancelButton));
		System.out.println("Dismissed Alert - Preptime dynamic slot");				
		return this;		 
	}			 

	public PrepTimePage clickPopUpYesButton(){
		//System.out.println("Preptime slots inside clickPopUpYesButton());
		System.out.println("Preptime dynamic slot xpath of clickPopUpYesButton() :"+ popupYesButton);				
		click(driver, By.xpath(popupYesButton));
		System.out.println("Accepted Alert - Preptime dynamic slot");					
		return this;		 
	}	
	public  boolean prepTimetab(){
        return this.isVisible(driver, prepTimetab);
    }

    public boolean verifyFirstPage() {
		boolean first_page = false;
		if(this.isVisible(driver, next_button)) {
			while(this.isVisible(driver, next_button))
				this.click(driver, next_button);
			first_page = true;
		}
		return first_page;
	}

	public PrepTimePage increasePrep(int n) {
		while(n > 0) {
			this.click(driver, increase);
			n--;
		}
		return this;
	}

	public PrepTimePage decreasePrep(int n) {
		while(n > 0) {
			this.click(driver, decrease);
			n--;
		}
		return this;
	}

	public PrepTimePage saveGlobalPrepTime() {
		this.click(driver, done_button);
		return this;
	}

	public PrepTimePage addGlobalIfNotExists() throws InterruptedException {
		PrepTimePage prepTimePage = new PrepTimePage(driver);
		if(prepTimePage.verifyFirstPage()) {
			prepTimePage.increasePrep(4).saveGlobalPrepTime();
			Profile profiletab = new Profile(driver);
			profiletab.clickProfileIcon();
		}
		return this;
	}

	public void deletePrepTimes() {
		PrepTimeServiceHelper prepTimeServiceHelper = new PrepTimeServiceHelper();
		String[] slots = prepTimeServiceHelper.fetchSlotWiseId(VmsConstants.username);
		for(String slot : slots) {
			if(slot != null && slot != "")
			prepTimeServiceHelper.delPreptime(slot);
		}
	}

}