package com.swiggy.ui.page.classes.fulfillment.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.swiggy.ui.utils.WebUtil;

public class CAPRDPage extends WebUtil {
	
	WebDriver driver = null;
	HomePage home = new HomePage(driver);
	
	public CAPRDPage(WebDriver driver) {
		this.driver = driver;
		
	}
	
	By proceed = By.xpath("//*[@id='sample-table-2']/div/div/accordion/div/div/div[2]/div/fieldset/div/div[1]/div[4]/div[4]/label");
	By toggle = By.xpath("//*[@id='sample-table-2']/div/div/accordion/div/div/div[2]/div/fieldset/div/div[1]/label");
	By assigned = By.xpath("//*[@id='sample-table-2']/div/div[1]/accordion/div/div/div[2]/div/fieldset/div/div[1]/div[5]/div/label[4]");
	By confirmed = By.xpath("//label[contains(text(),'Confirmed')]");
	By arrived = By.xpath("//label[contains(text(),'Arrived')]");
	By pickedUp = By.xpath("//label[contains(text(),'PickedUp')]");
	By reached = By.xpath("//label[contains(text(),'Reached')]");
	By delivered = By.xpath("//label[contains(text(),'Delivered')]");
	
	public CAPRDPage proceedAllGood() {
		this.click(driver, proceed);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage confirmOrder() {
		if(!isVisible(driver, confirmed))
			this.togglePlacingButtons();
		this.click(driver, confirmed);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage arrive() {
		if(!isVisible(driver, arrived))
			this.togglePlacingButtons();
		this.click(driver, arrived);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage pickupOrder() {
		if(!isVisible(driver, pickedUp))
			this.togglePlacingButtons();
		this.click(driver, pickedUp);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage reachOrder() {
		if(!isVisible(driver, reached))
			this.togglePlacingButtons();
		this.click(driver, reached);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage deliverOrder() {
		if(!isVisible(driver, delivered))
			this.togglePlacingButtons();
		this.click(driver, delivered);
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public CAPRDPage togglePlacingButtons() {
		this.click(driver, toggle);
		return this;
	}

}
