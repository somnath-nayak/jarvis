package com.swiggy.ui.page.classes.vendor.dashboard;

import framework.gameofthrones.Aegon.IElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.swiggy.ui.utils.WebUtil;

public class SelfServeMenu extends WebUtil {

	By menu = By.xpath(".//*[@class='icon menu-icon']");
	By itemsVisible = By.xpath(".//*[@class='item__info__name ng-binding']");
	By addItem = By.xpath(".//*[@ng-click='addItem()']");
	By itemName = By.xpath(".//*[@name='itemname']");
	By veg = By.xpath(".//*[@id='inlineRadio2']");
	By itemPrice = By.xpath(".//*[@class='ng-pristine ng-scope ng-invalid ng-invalid-required ng-valid-pattern ng-valid-min ng-touched']");
	By packageCharge = By.xpath(".//*[@class='ng-pristine ng-scope ng-valid-pattern ng-valid-min ng-valid-max ng-valid ng-valid-packaging-charge ng-touched']");
	By gst = By.xpath(".//*[@class='col-xs-6 text-right']");
	By select5 = By.xpath(".//*[@ng-click='calculateGst(5)']");
	By select12 = By.xpath(".//*[@ng-click='calculateGst(12)']");
	By select18 = By.xpath(".//*[@ng-click='calculateGst(18)']");
	By saveSubmission = By.xpath(".//*[@class='btn btn-info ng-scope']");
	By cgst = By.xpath(".//*[@name='cgst']");
	By sgst = By.xpath(".//*[@name='sgst']");
	By totalPrice = By.xpath(".//*[@ng-show='!calculatingPrice']");
	By editItem = By.xpath(".//*[@ng-click='editItem(item, mainMenu.name, subCat.name, false)']");
	By editPrice = By.xpath(".//*[@class='ng-pristine ng-scope ng-valid-pattern ng-valid ng-valid-required ng-touched']");
	By editPackaging = By.xpath(".//*[@class='ng-pristine ng-scope ng-valid-pattern ng-valid-min ng-valid-max ng-valid ng-valid-packaging-charge ng-touched']");
	By deleteItem = By.xpath(".//*[@class='sw-btn--no-style delete-btn']");
	By yesDelete = By.xpath(".//*[@class='menu-delete-item__action__primary']");
	By cancelDelete = By.xpath("menu-delete-item__action__close");
	By uploadMenu = By.xpath(".//*[@class='btn btn-warning btn-lg btn-block upload-proof-btn ng-pristine ng-untouched ng-valid pulse-button']");
	By isUploaded = By.xpath(".//*[@ng-click='previewProof(proof)']");
	By submitApproval = By.xpath(".//*[@class='btn btn-x-lg btn-block btn-success submit-for-approval-btn pulse-button-green']");
	By approveMessage = By.xpath("//p[@class='margin-bottom-none padding-bottom-1x text-center text-green']");
	By pendingTickets = By.xpath(".//*[@class='panel revision-stats__panel revision-stats__panel--submitted ng-scope']");
	By resolvedTickets = By.xpath(".//*[@class='panel revision-stats__panel revision-stats__panel--resolved ng-scope']");
	By pendingTicketView = By.xpath(".//*[@class='text-orange text-underline']");
	By resolvedTicketView = By.xpath(".//*[@class='text-orange text-underline ng-scope']");
	By viewItemName = By.xpath(".//*[@class='filter-name ng-binding']");
	By deleteLabel = By.xpath("//*[@class='table-list clearfix ng-scope']");
	By cancelTicket = By.xpath(".//*[@class='cancel-ticket']");
	By yesCancel = By.xpath(".//*[@class='yes-button']");
	By cancelMessage = By.xpath("//div[@ng-show='editItemSuccess']/h6");
	By viewWaitingTicket = By.xpath(".//*[@class='text-orange text-underline']");
	
	String name= VmsConstants.itemName;
	String price =VmsConstants.price;
	String packaging=VmsConstants.packaging;
	String price1 =VmsConstants.price1;
	String packaging1=VmsConstants.packaging1;
	
	
	private WebDriver driver;

	public SelfServeMenu() {}

	public SelfServeMenu(WebDriver driver) {
		this.driver = driver;
		driver.manage().window().maximize();
	}


	public boolean verifyMenu() {
		return this.isVisible(driver, menu);
	}


	public SelfServeMenu clickMenu() {
		try {
			click(driver,menu );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu clickAddItem() {
		try {
			click(driver,addItem );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	
	public SelfServeMenu EnterItemDetails() {
		try {
			enterText(driver, itemName, name);
			click (driver, veg);
			enterText(driver, itemPrice, price);
			enterText(driver, packageCharge, packaging);
			click(driver, gst);
			click(driver, select5);
			Thread.sleep(5000);
			click(driver, saveSubmission);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

    public String getItemPrice() {
	    return this.getText(driver, itemName);	    
    }
    
    public String getPackaging() {
	    return this.getText(driver, packageCharge);	    
    }
    
    public String getCGST() {
	    return this.getText(driver, cgst);	    
    }
    
    public String getSGST() {
	    return this.getText(driver, sgst);	    
    }

    public String getTotalPrice() {
	    return this.getText(driver, totalPrice);	    
    }

    
	public SelfServeMenu submit() {
		try {
			Thread.sleep(5000);
			click(driver, saveSubmission);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu uploadMenu() {
		try {
			WebElement M1 = driver.findElement(uploadMenu);
			M1.sendKeys("C:\\Users\\Public\\Pictures\\Sample Pictures\\Tulips.jpg");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public boolean verifyUploadMenu() {
		return this.isVisible(driver, isUploaded);
	}

	public SelfServeMenu submitApproval() {
		try {
			click(driver, submitApproval);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public String getapproveMessage() {
	    return this.getText(driver, approveMessage);	    
    }
	
	public SelfServeMenu pendingTickets() {
		try {
			click(driver, pendingTickets);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public SelfServeMenu pendingTicketView() {
		try {
			click(driver, pendingTicketView);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	 public String getViewItemName() {
		    return this.getText(driver, viewItemName);	    
	    }
	
	public SelfServeMenu resolvedTickets() {
		try {
			click(driver, resolvedTickets);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu resolvedTicketView() {
		try {
			click(driver, resolvedTicketView);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	
	public SelfServeMenu editItem() {
		try {
			click(driver, editItem);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu editPrice() {
		try {
			enterText(driver, editPrice, price1);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu editPackaging() {
		try {
			enterText(driver, editPackaging, packaging1);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public SelfServeMenu deleteItem() {
		try {
			click(driver, deleteItem);
			click(driver, yesDelete);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

    public String getDeleteLabel() {
	    return this.getText(driver, deleteLabel);	    
    }

	public SelfServeMenu cancelTicket() {
		try {
			click(driver, cancelTicket);			
			click(driver, yesCancel);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public String getCancelMessage() {
	    return this.getText(driver, cancelMessage);	    
    }
	

	public SelfServeMenu viewWaitingTicket() {
		try {
			click(driver, viewWaitingTicket);			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

}




