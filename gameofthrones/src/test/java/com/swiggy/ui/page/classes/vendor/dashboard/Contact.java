package com.swiggy.ui.page.classes.vendor.dashboard;

import framework.gameofthrones.Aegon.IElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.swiggy.ui.utils.WebUtil;

public class Contact extends WebUtil {

	//By clickContact = By.xpath(".//*[@class='icon help-icon']");
	By clickContact = By.xpath("//span[@class='caption'][contains(text(),'Help')]");
	By verifyContactPage = By.xpath(".//*[@class='page__title']");
	By chooseRest = By.xpath(".//*[@ng-click='toggleCheckboxes( $event ); refreshSelectedItems(); refreshButton(); prepareGrouping; prepareIndex();']");
	By selectRest = By.xpath("(.//*[@ng-class='{disabled:itemIsDisabled( item )}'])[1]");
	By dummyClick = By.xpath("//h5[contains(text(),'Get in touch with us')]");
	By chooseCategory = By.xpath(".//*[@id='contactCategory']");
	By selectCategory1 = By.xpath(".//*[@ng-attr-id='Invoice Request or Payment Issue']");
	By selectCategory2 = By.xpath(".//*[@ng-attr-id='Delivery Executive Related Issue']");
	By selectCategory3 = By.xpath(".//*[@ng-attr-id='Partner App Issue']");
	By selectCategory4 = By.xpath(".//*[@ng-attr-id='Update Restaurant Tax/Packaging Charges']");
	By selectCategory5 = By.xpath(".//*[@ng-attr-id='Request for a Ledger Book']");
	By selectCategory6 = By.xpath(".//*[@ng-attr-id='Run a discount on Swiggy']");
	By selectCategory7 = By.xpath(".//*[@ng-attr-id='Contact Sales POC']");
	By selectCategory8 = By.xpath(".//*[@ng-attr-id='Menu Edit']");
	By clickSubCategory = By.xpath(".//*[@id='contactSubCategory']");
	By subCategory1 = By.xpath("//option[@value='More than 20']");
	By subCategory2 = By.xpath(".//*[@value='Less than 20']");
	By typeOfEdit = By.xpath("//*[@id='contactTypeEdit']");
	By edit1= By.xpath("//option[@value='item edit/addition/deletion']");
	By uploadMenu = By.xpath("//button[contains(text(),'Upload')]");
	By selectCategory9 = By.xpath(".//*[@ng-attr-id='Others']");
	By enterSubject = By.xpath(".//*[@placeholder='Enter the subject']");
//	By enterComment = By.xpath(".//*[@class='ng-pristine ng-invalid ng-invalid-required ng-touched']");
	By enterComment = By.xpath("//textarea[@name='message']");
	By clickSubmit = By.xpath(".//*[@ng-show='!contactSubmitBtnClkd']");
	By goBackToForm = By.xpath(".//*[@class='btn btn-default pull-right']");
	By successMessage = By.xpath("//h4[contains(text(),'Submission Successful!')]");
	By selectAllRest = By.xpath(".//*[@ng-if='helperStatus.all']");
	
	private WebDriver driver;

	public Contact() {}

	public Contact(WebDriver driver) {
		this.driver = driver;
		driver.manage().window().maximize();
	}



	public Contact clickContact() {
		try {
			click(driver, clickContact);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	 public String getContactTitle() {
		    return this.getText(driver, verifyContactPage);	    
	    }


	public Contact selectRest() {
		try {
			click(driver, chooseRest);
			click(driver, selectRest);
			click(driver, dummyClick);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact selectCategory(int category) {
		try {
			click(driver, chooseCategory);
		switch (category)
		{
		case 1 :
			click(driver, selectCategory1);
		case 2 :
			click(driver, selectCategory2);
		case 3 :
			click(driver, selectCategory3);
		case 4 :
			click(driver, selectCategory4);
		case 5 :
			click(driver, selectCategory5);
		case 6 :
			click(driver, selectCategory6);
		case 7 :
			click(driver, selectCategory7);
		case 9 :
			click(driver, selectCategory9);
		
		}
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact selectCategory8() {
		try {
			click(driver, chooseCategory);
			click(driver, selectCategory8);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public Contact selectSubCategory(int subCategory) {
		try {
			click(driver, clickSubCategory);
	switch (subCategory)
	{
	case 1 :
		click(driver, subCategory1);
	case 2 :
		click(driver, subCategory2);
	}		
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact edit1() {
		try {
			click(driver, typeOfEdit);
			click(driver, edit1);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact uploadMenu() {
		try {
			click(driver, uploadMenu);
			driver.switchTo()
            .activeElement()
            .sendKeys("C:\\Users\\Public\\Pictures\\Sample Pictures\\Tulips.jpg");
			//WebElement M1 = driver.findElement(uploadMenu);
			//M1.sendKeys("C:\\Users\\Public\\Pictures\\Sample Pictures\\Tulips.jpg");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}


	public Contact selectCategory9() {
		try {
			click(driver, chooseCategory);
			click(driver, selectCategory9);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	
	public Contact enterSubject() {
		try {
			enterText(driver, enterSubject, "Test Subject");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	
	public Contact enterComment() {
		try {
			isVisible(driver, enterComment);
			enterText(driver, enterComment, "Test Commnent");	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact clickSubmit() {
		try {
			click(driver, clickSubmit);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public Contact selectAllRest() {
		try {
			click(driver, selectAllRest);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	
	public String successMessage() {
//		return this.isVisible(driver, successMessage);
		isVisible(driver, successMessage);
		String s1 = getText(driver, successMessage);
		return s1;
	}
	
}
