package com.swiggy.ui.page.classes.fulfillment.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.swiggy.ui.utils.WebUtil;

public class LoginPage extends WebUtil {
	
	public WebDriver driver = null;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	By username = By.id("id_username");
	By password =  By.id("id_password");
	By submit = By.xpath("//input[@value='Log in']");
	
	public HomePage login(String user, String pass) {
		enterText(driver, username, user);
		enterText(driver, password, pass);
		click(driver, submit);
		return new HomePage(driver);
	}
	
	public LoginPage enterUserName(String user) {
		enterText(driver, username, user);
		return this;
	}
	
	public LoginPage enterPassword(String pass) {
		enterText(driver, password, pass);
		return this;
	}
	
	public HomePage submitLogin() {
		click(driver, submit);
		return new HomePage(driver);
	}
	
	

}
