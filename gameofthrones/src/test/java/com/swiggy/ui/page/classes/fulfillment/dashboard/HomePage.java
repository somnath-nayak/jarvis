package com.swiggy.ui.page.classes.fulfillment.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.swiggy.ui.utils.WebUtil;

public class HomePage extends WebUtil {
	
	private WebDriver driver = null;
	private static String mobileNumber = "78658341";

	public HomePage(WebDriver driver) {
		this.driver = driver;
	}
	
	By searchBox = By.xpath("//*[@id='myTab4']//*[@name='srch-term']");
	By searchButton = By.xpath("//*[@id='myTab4']//*[@type='button']");
	By order = By.xpath("//*[@id='sample-table-2']/div/div[1]/accordion/div/div/div[1]");
	By cancel = By.xpath("//*[@id='sample-table-2']/div/div[1]/accordion/div/div/div[2]/div/fieldset/div/div[2]/div[7]/div[2]/button");
	By proceed = By.xpath("//label[@class='btn btn-default label-success']");
	By orderStatus = By.xpath(".//*[@id='sample-table-2']/div/div/accordion/div/div/div[1]/h4/a/div/ul[1]/li[6]/span[1]/b");
	By orderId = By.xpath(".//*[@id='sample-table-2']/div/div/accordion/div/div/div[1]/h4/a/div/ul[1]/li[1]/span[1]");
	By orderAlertButton = By.xpath(".//*[@id='search_nav']/button");
	By profileDropdown = By.xpath(".//*[@id='navbar-container']/div[2]/ul/li[2]/a");
	By logoutButton = By.linkText("Logout");
	By orderPane = By.xpath(".//*[@id='sample-table-2']/div/div/accordion/div/div/div[1]");
	By togglePlacingButtons = By.xpath(".//*[@id='sample-table-2']//label[contains(text(),'Toggle Placing buttons')]");
	By placedButton = By.xpath(".//*[@id='sample-table-2']//label[contains(text(),'Placed')]");
	By reassignPlacer = By.xpath(".//*[@id='sample-table-2']//div[contains(text(),'Placer')]/button[contains(text(),'Re-Assign')]");


	public HomePage enterSearchText(String text) {
		this.waitForVisibility(driver, searchBox);
		this.click(driver, searchBox);
		this.enterText(driver, searchBox, text);
		return this;
	}
	
	public HomePage search() {
		this.click(driver, searchButton);
		return this;
	}
	
	public HomePage clickOnOrder() {
		this.click(driver, order);
		return this;
	}
	
	public CAPRDPage clickOnOrdercaprd() {
		this.click(driver, order);
		return new CAPRDPage(driver);
	}
	
	public CancelPage scrollToCancel() {
		this.scrollTo(driver, cancel);
		try{
		Thread.sleep(5000);
		} catch(Exception e) {
			e.getStackTrace();
		}
		return new CancelPage(driver);
	}

	int retry = 0;
	public AssignDE proceed() {
		while (retry !=3){
			if(this.isVisible(driver, proceed)) {
				this.click(driver, proceed);
				this.acceptAlert(driver);
				this.refresh(driver);
			}else {
				retry++;
				this.refresh(driver);
				clickOnOrdercaprd();
				proceed();
			}
		}
		return new AssignDE(driver);
	}
	
	public String getOrderStatus(){
		this.waitForVisibility(driver, orderStatus);
		return driver.findElement(orderStatus).getText();
	}
	
	public String getOrderId(){
		this.waitForVisibility(driver, orderId);
		return driver.findElement(orderId).getText();
	}
	
	public void clickOnOrderAlertButton(){
		this.waitForVisibility(driver, orderAlertButton);
		this.click(driver, orderAlertButton);
	}
	
	public void clickOnProfileDropdown(){
		this.waitForVisibility(driver, profileDropdown);
		this.click(driver, profileDropdown);
	}
	
	public void clickOnlogoutButton(){
		driver.switchTo().activeElement();
		this.waitForVisibility(driver, logoutButton);
		this.click(driver, logoutButton);
	}
	
	public void clickOnOrderPane(){
		this.waitForVisibility(driver, orderPane);
		this.click(driver, orderPane);
	}
	
	public void clickOnTogglePlacingButons(){
		this.waitForVisibility(driver, togglePlacingButtons);
		this.click(driver, togglePlacingButtons);
	}

	public void clickOnPlacedButton() {
		this.waitForVisibility(driver, placedButton);
		this.click(driver, placedButton);
	}
	
	public void clickOnReassignPlacerButton(){
		this.waitForVisibility(driver, reassignPlacer);
		this.click(driver, reassignPlacer);
	}
	
}

