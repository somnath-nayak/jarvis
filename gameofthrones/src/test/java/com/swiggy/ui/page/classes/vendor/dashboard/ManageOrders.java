package com.swiggy.ui.page.classes.vendor.dashboard;

import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.swiggy.ui.utils.WebUtil;

import java.util.List;

/**
 * Created by imran khan on 12/06/2017.
 */

public class ManageOrders extends WebUtil 
{
	private WebDriver driver = null;
	By Confirm_order_click = By.xpath("//button[contains(text(),'Confirm Order')]");
	By request_call_back = By.xpath("//*[@title='Request Callback']");
	By request_call_back_readytab = By.xpath("(//*[@title='Request Callback'])[3]");
	//TODO: Since ID is removed
	// By request_call_back = By.id("request-callback-link-new");
	By print = By.xpath("//*[@class='print-button']");
	By food_ready = By.xpath("//button[contains(text(),'Food Ready')]");
	By prep_time = By.xpath("//*[@class='display-table']//span[contains(text(),'min')]");
	By order_id = By.xpath("//span[contains(text(),'#')]");
	By out_of_stock = By.xpath("//button[contains(text(),'Out of Stock')]");
	By new_outofstock = By.xpath("id('new')//button[contains(text(),'Out of Stock')]");
	By preparing_outofstock = By.xpath("//id('preparing')//button[contains(text(),'Out of Stock')]");
	By preparing = By.xpath("//span[contains(text(),'preparing')]");
	By new_order = By.xpath("//span[contains(text(),'new')]");
	By yes_popup = By.xpath("//button[contains(text(),'Yes')]");
	By cancel_popup = By.xpath("//button[contains(text(),'Cancel')][@role='button']");
	By test = By.xpath("//span[@class='caption'][contains(text(),'Profile')]");
	By order_total = By.xpath("//*[@class='order-details__total']");
	By neworders_search = By.xpath("//*[@placeholder='Search' and @ng-model='newOrdersSearch']");
	By preparingorders_search = By.xpath("//*[@placeholder='Search' and @ng-model='preparingOrdersSearch']");
	By readyorders_search = By.xpath("//*[@placeholder='Search' and @ng-model='readyOrdersSearch']");
	By moreorders_search = By.xpath("//*[@placeholder='Search' and @ng-model='moreOrdersSearch']");
	By new_tab = By.xpath("//*[@href='#new' and @title='New']");
	By preparing_tab = By.xpath("//*[@href='#preparing' and @title='Preparing']");
	By ready_tab = By.xpath("//*[@href='#ready' and @title='Ready']");
	By more_tab = By.xpath("//*[@href='#more' and @title='More']");
	By order_date = By.xpath("//*[@class='text-muted ng-binding selectorgadget_selected']");
	By de_status = By.cssSelector(".text-uppercase.text-muted.ng-scope");
	By drop_down = By.xpath("id(\"mCSB_10_container\")//*[@class='fa fa-angle-down']");
	By special_instructions_text = By.xpath("//*[@class='text-orange ng-binding ng-scope']");
	By item_checkbox = By.xpath("//*[@type='checkbox']");
	By orders_panel = By.xpath("//*[@class='dropdown-toggle' and @href='/orders']");
	By order_status = By.xpath("adasds");
	By done_button = By.xpath("//button[contains(text(),'Done')]");
	By cancel_button = By.xpath("//button[contains(text(),'Cancel')]");
	By request_cancel = By.xpath("//button[contains(text(),'Cancel') and @role='button']");
	By request_yes = By.xpath("//button[contains(text(),'Yes')]");
	By outofstock_icon = By.xpath("//span[@class='icon']");
	By send_suggestions_button=By.xpath("//button[contains(text(),'Send Suggestions')]");
	By send_suggestions = By.cssSelector(".btn.btn-x-lg.btn-success.send-suggestions-button");
	By cancel_suggestions = By.cssSelector(".btn.btn-x-lg.btn-default.cancel-send-suggestions-button");
	//By test = By.xpath("//*[@href='/restaurant-info' and @title='Profile']");
	VmsHelper vmsHelper = new VmsHelper();

    public ManageOrders() {
    }

    public ManageOrders(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public ManageOrders Confirm_order_click() {
		System.out.println(this.isVisible(driver, Confirm_order_click));
		this.click(driver, Confirm_order_click);
		return this;
	}

	public String getOrderId() {
		return getText(driver, order_id).replace("#","").replace(" ","");
	}

	public ManageOrders clickOutOfStock(VmsConstants.OrderStatusTab tab) {
    	switch (tab) {
			case PREPARING: this.click(driver, preparing_outofstock);
			break;
			case NEW: this.click(driver, new_outofstock);
			break;
		}
		return this;
	}

	public ManageOrders requestCallBack() {
		this.click(driver, request_call_back);
		this.click(driver, request_call_back_readytab);
		return this;
	}

	public ManageOrders printInvoice() {
		this.click(driver, print);
		return this;
	}

	public ManageOrders clickFoodReady() {
		this.click(driver, food_ready);
		return this;
	}

	public boolean isNewOrder() {
		return this.isVisible(driver, new_order);
	}

	public boolean isPreparingOrder() {
		return this.isVisible(driver, preparing);
	}

	public ManageOrders acceptRequestCallBack() {
		this.click(driver, yes_popup);
		return this;
	}

	public ManageOrders rejectRequestCallBack() {
		this.click(driver, cancel_popup);
		return this;
	}

	public boolean isRequestCallBackPopUp() {
    	return this.isVisible(driver, yes_popup);
	}

	public String getPrepTime() {
		return this.getText(driver, prep_time);
	}

	public String getOrderTotal() {
		String s = this.getText(driver, order_total);
		String[] ss = s.split(" ");
		return ss[ss.length-1];
	}

	public boolean verifyNewOrder() {
		boolean success = false;
		success = isNewOrder();
		return success;
	}

	public ManageOrders searchNewOrders(String order_id) {
		if(this.isVisible(driver, neworders_search))
			this.enterText(driver, neworders_search, order_id);
		return this;
	}

	public ManageOrders searchPreparingOrders(String order_id) {
		if(this.isVisible(driver, preparingorders_search))
			this.enterText(driver, preparingorders_search, order_id);
		return this;
	}

	public ManageOrders searchReadyOrders(String order_id) {
		if(this.isVisible(driver, readyorders_search))
			this.enterText(driver, readyorders_search, order_id);
		return this;
	}

	public ManageOrders searchMoreOrders(String order_id) {
		if(this.isVisible(driver, moreorders_search))
			this.enterText(driver, moreorders_search, order_id);
		return this;
	}

	/*public boolean verifyOrderDetails() {
		VmsHelper vmsHelper = new VmsHelper();
		String OrderId="1127260629";
		return OrderId;
		//return (vmsHelper.getOrderId() == getOrderId()) && (vmsHelper.getOrderTotal() == getOrderTotal());
	}
*/
	
	public boolean verifyOrderDetails() {
	return (VmsHelper.getOrderId() == getOrderId()) && (VmsHelper.getOrderTotal() == getOrderTotal());
}

	public boolean verifyTab(VmsConstants.OrderStatusTab status){
		boolean flag=false;
		
		switch (status) {
		case NEW: this.isVisible(driver, new_tab);
		     flag = true;
			   	break;
		case PREPARING: this.isVisible(driver, preparing_tab);
		               flag = true;
						break;
		case READY: this.isVisible(driver, ready_tab);
		               flag = true;			
		               break;
		case MORE: this.isVisible(driver, more_tab);
		flag = true;			
		break;
	}
	
			return flag;
		
	}
	
	public ManageOrders clickOnTab(VmsConstants.OrderStatusTab status) {
		switch (status) {
			case NEW: this.click(driver, new_tab);
				      break;
			case PREPARING: this.click(driver, preparing_tab);
							break;
			case READY: this.click(driver, ready_tab);
						break;
			case MORE: this.click(driver, more_tab);
						break;
		}
		return this;
	}

	public String getOrderDate() {
        return this.getText(driver, order_date);
    }

    public String getDeStatus() {
		return this.getText(driver, de_status);
	}

	public String getItemName() {
	    return "";
    }

    public String getItemQuantity() {
	    return "";
    }

    public String getItemTotal() {
	    return "";
    }

    public List<WebElement> outOfStockItems() {
        return driver.findElements(drop_down);
    }

    public List<WebElement> getItemsList() {
        return driver.findElements(item_checkbox);
    }

    public ManageOrders selectItems(int n) {
        List<WebElement> categories = outOfStockItems();
        //categories = categories.subList(1,categories.size());
        List<WebElement> items = getItemsList();
         for(int i=0;i<n;i++) {
            if(!isVisible(driver, items.get(i)))
                categories.get(i).click();
            items.get(i).click();
        }
        return this;
    }
    
    public boolean checkSelectedItems(int n) {
    	boolean istrue = true;
        List<WebElement> categories = outOfStockItems();
        List<WebElement> items = getItemsList();
         for(int i=0;i<n;i++) 
        	 istrue &= items.get(i).isSelected();
        return !istrue;
    }
    
    

    public String getInstructionText() {
        return getText(driver, special_instructions_text);
    }

    public ManageOrders clickOrdersPanel() {
    	this.click(driver, orders_panel);
    	return this;
	}

	public boolean verifyOrderStatus(VmsConstants.OrderStatus status) {
    	return (this.getText(driver, order_status).equalsIgnoreCase(status.toString()));
	}

	public ManageOrders cancelOutOfStock() {
    	this.click(driver, cancel_button);
    	return this;
	}

	public ManageOrders outOfStockDone() {
    	this.click(driver, done_button);
    	return this;
	}

	public ManageOrders sendSuggetions(){
		this.click(driver, send_suggestions_button);
		return this;
	}
	public boolean verifyOutOfStock() {
    	return this.isVisible(driver, outofstock_icon);
	}

	public boolean checkPrintScreen() {
    	return driver.getWindowHandles().size() > 1;
	}

	public boolean cartDetails() {
    	//TODO: Will be Updated once ID's are available for Cart
		return true;
	}

	public boolean globalPrepTime() {
    	//TODO: need to get the text
		return true;
	}

	public ManageOrders cancelSuggestions() {
    	this.click(driver, cancel_suggestions);
    	return this;
	}

	public ManageOrders sendSuggestions() {
    	this.click(driver, send_suggestions);
    	return  this;
	}

	public ManageOrders markDelivered(String order_id) {
    	VmsHelper vmsHelper = new VmsHelper();
    	vmsHelper.assignOrder(order_id);
    	vmsHelper.markConfirm(order_id);
    	vmsHelper.markArrived(order_id);
    	vmsHelper.markPickedUp(order_id);
    	vmsHelper.markReached(order_id);
    	vmsHelper.markDelivered(order_id);
    	return this;
	}

	public ManageOrders cancelOrder(String order_id) {
		CheckoutHelper checkoutHelper = new CheckoutHelper();
		checkoutHelper.OrderCancel(VmsConstants.mobileno, VmsConstants.orderpos, order_id);
    	return this;
	}
	
	public boolean isDoneDisabled() {
		return !isEnabled(driver, done_button);
	}

}
