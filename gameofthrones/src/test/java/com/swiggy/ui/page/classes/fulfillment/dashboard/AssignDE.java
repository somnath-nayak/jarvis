package com.swiggy.ui.page.classes.fulfillment.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.swiggy.ui.utils.WebUtil;


public class AssignDE extends WebUtil {

	private WebDriver driver = null;

	public AssignDE(WebDriver driver) {
		this.driver = driver;
	}
	
	List<String> des = new ArrayList<String>();
	
	By assign = By.xpath("//*[@id='sample-table-2']/div/div/accordion/div/div/div[2]/div/fieldset/div/div[6]/div[2]/div/fieldset/a");
	By selectxpath = By.xpath("//*[@id='sample-table-2']/div/div/accordion/div/div/div[2]/div/fieldset/div/div[6]/div[2]/div/fieldset/form/div/select");
	By submit = By.xpath("//*[@id='sample-table-2']//*[@type='submit']");
	
	public AssignDE select() {
		this.click(driver, assign);
		return this;
	}
	
	public AssignDE assign(List<String> des) {
		try{
		Thread.sleep(1000);
		int index = 0;
		Select select = new Select(driver.findElement(selectxpath));
		List<WebElement> options = select.getOptions();
		System.out.println("size is "+ options.size());
		for(WebElement o: options)
			System.out.println(o.getText());
		for(WebElement opt: options) {
			index++;
			for(String e: des) {
				if(opt.getText().contains(e)) {
					System.out.println(index+ opt.getText());
					select.selectByVisibleText(e);
					break;
				}
			}
		}
		} catch(Exception e) {
			e.printStackTrace();
		}
		this.submit();
		this.acceptAlert(driver);
		this.refresh(driver);
		return this;
	}
	
	public AssignDE submit() {
		this.click(driver, submit);
		return this;
	}
}
