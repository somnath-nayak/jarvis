package com.swiggy.ui.page.classes.portalweb;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.xalan.xsltc.compiler.sym;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebDriver;


import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import com.swiggy.ui.utils.WebUtils;



public class HomePage extends WebUtils
{
	/*Elements for the Home page*/
	
	
	public IElement btn_login;
	public IElement btn_signup;
	public IElement location_textfield;
	public IElement locateme;
	public IElement btn_find;
	public IElement clear_locationField;
	public IElement error_unserviceable;
	public IElement serviceable_city;
	public IElement popularareas_city;
	public IElement helpandSupport;
	public IElement helppage;
	public IElement company;
	public IElement contactUs;
	public IElement legal;
	public IElement partnerWithUs;
	
	public List<IElement> legal_links;
	public List<IElement> contactUs_Link;
	public List<IElement> linksunder_company;
	public List<IElement> auto_suggestions;
	public List<IElement> listpopularareas_city;
	

    public  HomePage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
    
    /*Methods Home page*/
    
    public boolean checkloginButn_present()
    {
        return checkElementDisplay(btn_login);
    }
    
    public boolean checksignupBtn_present()
    {
        return checkElementDisplay(btn_signup);
    }
    
    public boolean checklocation_present()
    {
    	return checkElementDisplay(location_textfield);
    }
    
    public boolean checkLocateme_Present()
    {
    	return checkElementDisplay(locateme);
    }
    
    public boolean checkfindButn_present()
    {
    	return checkElementDisplay(btn_find);
    }
    
    public boolean checkClear_Present()
    {
    	return checkElementDisplay(clear_locationField);
    }
    
    public void clickOnLogin()
    {
    	btn_login.click(10);
  
    }
    
    public void clickOnSignup()
    {
    	btn_signup.click(20);
    }
    
    public void clearLocationTextField()
    {
    	location_textfield.clear(10);
    }
    public void searchForLocation(String searchfor)
    {
    	location_textfield.clear(10);
    	location_textfield.sendKeys(searchfor, 10);
    }
    
    
    /*Getting text for all the autosuggestion and storing in the Array list*/
    
    public ArrayList<String> autoSuggestion()
    {
    	ArrayList<String> al=new ArrayList<>();
    	
    	for(int i=0;i<auto_suggestions.size();i++) {
    		al.add(auto_suggestions.get(i).getText());
    	}
    	return al;
    }
    
    public int size(ArrayList<String> al)
    {
    	return al.size();
    	
    }
    
    /*Selecting  the address from the auto suggestion*/
    
    public void selectAddressFromAutosuggetion(String address)
    {
    	for(int i=0;i<auto_suggestions.size();i++) 
    	{
    		   		if((auto_suggestions.get(i).getText()).equalsIgnoreCase(address))
    				auto_suggestions.get(i).click(10);
    	}
    	
    }
/*    public  boolean isDialogPresent(WebDriver driver) {
        try {
            driver.getTitle();
            return false;
        } catch (UnhandledAlertException e) {
            // Modal dialog showed
            return true;
        }
    }*/
/*    public void handleAlert(WebDriver driver)
    {
    	if(isDialogPresent(driver))
    		driver.switchTo().alert().accept();
  
    }*/
    
    public boolean autosuggestionPopulating()
    {

        return auto_suggestions.size() > 0;
    }
    
    public void deleteAllCookies(WebDriver driver)
    {
    	driver.manage().deleteAllCookies();
    }
    public void selectLocationGps(WebDriver driver)
    {
    	locateme.click();
    	
    }
    
    public void navigateTo(WebDriver driver, String Url)
    {
    	driver.navigate().to(Url);
    }
    
    public String  getErrorMessage()
    {
    	return error_unserviceable.getText(10);
    	
    }
    public void clearTextField()
    {
    	location_textfield.clear(10);
    }
    
    public void removeSelectedLocation()
    {
    	clearTextField();
    }
    public void clikOnServiciableCity()
    {
    	serviceable_city.click(10);
    }
        
    public boolean popularareas_Present(WebDriver driver)
    {
    	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", popularareas_city.getWebElement());
    	return checkElementDisplay(popularareas_city);
    }
    
	public void selectACityFromPopularCities()
	{
		listpopularareas_city.get(0).click(10);
		
	}
    	
	public void clickOnHelpandSupportLink(WebDriver driver)
	{
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",helpandSupport.getWebElement());
		helpandSupport.click(10);
			
	}
	public String getHelppagetitle()
	{
		return helppage.getText(10);
	}
	


/*	public void allowLoationSharing(WebDriver driver) 
	{
		// TODO Auto-generated method stub
		 driver.navigate().to("chrome://settings/content");
		    driver.switchTo().frame("settings");
		    WebElement location= driver.findElement(By.xpath("//*[@name='location' and @value='allow']"));
		    try {
		        Thread.sleep(5000);
		    } catch (InterruptedException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
		    ((JavascriptExecutor) driver).executeScript("arguments[0].click();", location);
		    

		    driver.findElement(By.xpath("//*[@id='content-settings-overlay-confirm']")).click();

		    try {
		        Thread.sleep(5000);
		    } catch (InterruptedException e) {
		        // TODO Auto-generated catch block
		        e.printStackTrace();
		    }
	
	}*/
	
	public void scrollIntoView(WebDriver driver, IElement element)
	{
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",element.getWebElement());
	}
	
	public boolean verfyLlinksUnderStaticSection(List<IElement> list, String [] expectedVal)
	{
		
		
		boolean flag = true;
		for(int i=0;i<list.size();i++)
		{
			
			System.out.println((list.get(i).getText()+"actual"+"=========="+"expected"+expectedVal[i]));
			if (!(list.get(i).getText().equalsIgnoreCase(expectedVal[i])))
			{
				System.out.println(list.get(i).getText()+"=================================================================================");
				System.out.println(expectedVal[i]);
				flag = false;
				/*break;*/
				
			}
		}
		return flag;
			
			
			
		}
	
	
	
	public boolean verfyLlinksAreClickable(WebDriver driver,List<IElement> list, String [] pageTitle, IElement element)
	{
		
		scrollIntoView(driver, element);
		String winHandleBefore = driver.getWindowHandle();
		boolean flag = true;
		
			for(int i =0; i<list.size();i++)
			{
				
				list.get(i).click(10);
				Set<String> winHandle = driver.getWindowHandles();
				Object [] windows =  winHandle.toArray();
				driver.switchTo().window(windows[1].toString());
				String str = driver.getCurrentUrl();
				    
				    if(!(str.replaceAll(PortalConstants.portal_url, "").equals(pageTitle[i])))
				    {
				    	System.out.println("The not matching url"+str+"+++++++++++++++++++++++++++++++");
				    	flag = false;
					 
					}
				    
				    System.out.println((str.replaceAll(PortalConstants.portal_url, "")));
				    driver.close();
					driver.switchTo().window(winHandleBefore);   
					System.out.println(flag);
				}
				
		
			return flag;
		}

	
	public boolean verifyPartnerWithUs(WebDriver driver)
	{
		boolean flag = true;
		String winHandleBefore = driver.getWindowHandle();
		
		partnerWithUs.click(10);
		Set<String> winHandle = driver.getWindowHandles();
		Object [] windows =  winHandle.toArray();
		driver.switchTo().window(windows[1].toString());
		String str = driver.getCurrentUrl();
		if(!(str.equals(PortalConstants.partnerWithUs)))
		{
			System.out.println("The Current url is ========================="+str);
			flag = false;
		}
		driver.close();
		driver.switchTo().window(winHandleBefore);   
		System.out.println(flag);
		return flag;
		
		
		
	}
		
/*	catch(Exception e)
	{
		e.printStackTrace();
		return flag = false;
	}*/
}



	



