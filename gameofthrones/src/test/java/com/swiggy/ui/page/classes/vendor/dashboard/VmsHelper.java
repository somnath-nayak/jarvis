package com.swiggy.ui.page.classes.vendor.dashboard;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.JsonNode;

import java.util.*;

/**
 * Created by kiran.j on 12/6/17.
 */
public class VmsHelper {

    private static String orderId;
    private static String orderTotal;
    private static Processor orderJson;

    CheckoutHelper checkoutHelper = new CheckoutHelper();
    DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
    OMSHelper omsHelper = new OMSHelper();

    public static String getOrderId() {
        return orderId;
    }

    public static void setOrderId(String orderId) {
        VmsHelper.orderId = orderId;
    	//VmsHelper.orderId = "1127260629";
    }

    public static String getOrderTotal() {
        return orderTotal;
    }

    public static void setOrderTotal(String orderTotal) {
        VmsHelper.orderTotal = orderTotal;
    	//VmsHelper.orderTotal ="1127260629";
    }

    public static Processor getOrderJson() {
        return orderJson;
    }

    public static void setOrderJson(Processor orderJson) {
        VmsHelper.orderJson = orderJson;
    }

    List<ItemPojo> items = new ArrayList<>();

    public void createOrder(String mobileno, String password, String ItemId, String quantity, String restId) {
        try {
            Processor order_response = checkoutHelper.placeOrder(mobileno, password, ItemId, quantity, restId);
            setOrderJson(order_response);
            setOrderId(JsonPath.read(order_response.ResponseValidator.GetBodyAsText(), "$.data.order_id").toString().replace("[","").replace("]",""));
            setOrderTotal(JsonPath.read(order_response.ResponseValidator.GetBodyAsText(), "$.data.order_items[0].total").toString().replace("[","").replace("]",""));
            omsHelper.verifyOrder(getOrderId(), VmsConstants.order_confirm_code);
            //deliveryServiceHelper.assignOrder(orderId, VmsConstants.delivery_id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getRandomItem(String restId) {
        CMSHelper cmsHelper = new CMSHelper();
        CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
        List<Map<String,Object>> list = cmsHelper.getRegularItems(restId);
        return list.get(commonAPIHelper.getRandomNo(0, list.size())).get("item_id").toString();
//        return "5748291";
    }

    public void assignOrder(String order_id) {
        try {
            DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
            deliveryServiceHelper.assignOrder(order_id, VmsConstants.delivery_id);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void markConfirm(String order_id) {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryServiceHelper.zipDialConfirmDE(order_id);
    }

    public void markArrived(String order_id) {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryServiceHelper.zipDialArrivedDE(order_id);
    }

    public void markPickedUp(String order_id) {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryServiceHelper.zipDialPickedUpDE(order_id);
    }

    public void markReached(String order_id) {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryServiceHelper.zipDialReachedDE(order_id);
    }

    public void markDelivered(String order_id) {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        deliveryServiceHelper.zipDialDeliveredDE(order_id);
    }

    public boolean iscallRequestOms(String order_id) {
        OMSHelper omsHelper = new OMSHelper();
        String response = omsHelper.getOrderInfoFromOMS(order_id);
        String json =  JsonPath.read(response, "$.objects[0].status.call_partner_time");
        return json != null;
    }

    public String getOrderStatusFromDelivery() {
        DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
        return deliveryServiceHelper.getOrderStatus(getOrderId()).ResponseValidator.GetNodeValue("data.status").toString();
    }

    public List<ItemPojo> getItemDetails() {
        try {
            JsonNode json = CommonAPIHelper.convertStringtoJSON(getOrderJson().ResponseValidator.GetNodeValue("order_items"));
            Iterator<JsonNode> itr = json.getElements();
            while(itr.hasNext()) {
                JsonNode parse = itr.next();
                items.add(new ItemPojo(parse.get("item_id").toString(), parse.get("name").toString(), parse.get("quantity").toString(), parse.get("subtotal").toString()));
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return items;
    }

    public boolean verifyItemDetails() {
        ManageOrders manageOrders = new ManageOrders();
        boolean success = false;
        for(ItemPojo item: items) {
            success = (manageOrders.getItemName() == item.getName()) && (manageOrders.getItemQuantity() == item.getQuantity()) && (manageOrders.getItemTotal() == item.getTotal());
        }
        return success;
    }

    public boolean verifyStatus() {
        ManageOrders manageOrders = new ManageOrders();
        return getOrderStatusFromDelivery() == manageOrders.getDeStatus();
    }

    public boolean verifyInstructions() {
        ManageOrders manageOrders = new ManageOrders();
        return (getOrderJson().ResponseValidator.GetNodeValue("order_notes") == manageOrders.getInstructionText());
    }

    public boolean isOutOfStock(String order_id) {
        OMSHelper omsHelper = new OMSHelper();
        String response = omsHelper.getOrderInfoFromOMS(order_id);
        String json = JsonPath.read(response, "$.objects[0].status.status");
        return json == VmsConstants.outofstock;
    }

    public boolean acceptOOSSuggestion() {
        //TODO: Need to get Helper methods from OMS
        return true;
    }

    public HashMap<String, Object> orderDetails(String order_id){
        HashMap<String, Object> map = new HashMap<>();
        OMSHelper omsHelper = new OMSHelper();
        String json = omsHelper.getOrderInfoFromOMS(order_id);
        map.put("item_name", JsonPath.read(json,"$.objects..cart.items[0].name"));
        map.put("total", JsonPath.read(json,"$.objects.[0].original_total"));
        map.put("item_price", JsonPath.read(json,"$.objects..cart.items[0].sub_total"));
        return map;
    }

    public Long createOrder() {
        Long order_id = 0L;
        try {
            LOSHelper losHelper = new LOSHelper();
            String json = losHelper.getAnOrder("partner");
            order_id = Long.parseLong(json);
        } catch (Exception e ) {
            e.printStackTrace();
        }
        return order_id;
    }
}
