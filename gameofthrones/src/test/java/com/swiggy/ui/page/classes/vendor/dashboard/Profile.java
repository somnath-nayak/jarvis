package com.swiggy.ui.page.classes.vendor.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

//import groovy.util.CliBuilder;
import com.swiggy.ui.utils.WebUtil;

public class Profile extends WebUtil {

	private WebDriver driver = null;
	By profileIcon = By.xpath("//span[@class='caption'][contains(text(),'Profile')]");
	By changeOutlet = By.xpath("(//li[2]//a[1]//span[@class='ng-scope'])[3]");
	By selectFirstOutlet = By.xpath(
			"(//button[@class='btn btn-success ng-scope'][@ng-if='outlet.rest_rid === globals.choosenOutlet.rest_rid'])[3]");
	By selectSecondOutlet = By.xpath("(//button[@class='btn btn-disabled ng-scope']//i[@class='fa fa-circle'])[5]");
	By selectThirdOutlet = By.xpath("(//button[@class='btn btn-disabled ng-scope']//i[@class='fa fa-circle'])[6]");
	By setGlobalPreptime = By.xpath("//button[@id='increase-global-preptime-button']");
	By updateGlobalPreptime = By.xpath("//button[@id='decrease-global-preptime-button']");
	By outlets = By.xpath("//button[@class='btn btn-disabled ng-scope']");
	// AccountTab
	By Account = By.xpath("//li[contains(text(),'Account')]");
	By accName = By.xpath("//div[@ng-if='userRestProfileData.name']");
	By swiggyID = By.xpath("(//div[@class='details-holder brd-rt-0 ng-binding'])[1]");
	By onboardingDate = By.xpath("//div[@ng-if='userRestProfileData.onboardingDate']");
	By phoneNum = By.xpath("(//div[@class='details-holder brd-rt-0 ng-binding'])[2]");
	// FinanceTab
	By Finance = By.xpath("//li[contains(text(),'Finance')]");
	By bankName = By.xpath("//*[@ng-if='!userRestProfileData.bankName.length']");
	By accNum = By.xpath("//*[@ng-if='!userRestProfileData.accountNo.length']");
	By ifscCode = By.xpath("//*[@ng-if='!userRestProfileData.ifscCode.length']");
	/* Users */
	By Users = By.xpath("//li[contains(text(),'Users')]");
	By manageUser = By.xpath("//h4[contains(text(),'Manage Users')]");
	By inviteUserButton = By.xpath("//h4[contains(text(),'Manage Users')]/../button"); // (//button[@ng-click='inviteUserModal()'])[2]
	By inviteUserPopUp = By.xpath("//span[contains(text(),'Choose user role')]");
	By role = By.xpath(
			"//h6[contains(text(),'" + VmsConstants.userPhoneNum + "')]/../../following-sibling::div[1]/div/span");
	By chooseRoleAsOwner = By.xpath("//select[@name='userRoleChooseRole']/option[@value='1']");
	By chooseRoleAsRM = By.xpath("//select[@name='userRoleChooseRole']/option[@value='2']");
	By chooseRoleAsOrderManager = By.xpath("//select[@name='userRoleChooseRole']/option[@value='3']");
	By invitePhoneNum = By.xpath("//input[@name='mobile_no']");
	By inviteName = By.xpath("//input[@name='name']");
	By inviteEmail = By.xpath("//input[@name='email']");
	By inviteNextButton = By.xpath("//div[contains(text(),' Next >')]");
	By selectRestaurants = By.xpath("(//a[contains(text(),'Hyderabad')])[1]");
	By restIsMaximized = By.xpath("(//div[@class='user-list-holder ng-scope is-single-user profile-acc-active'])[2]");
	By selectOutlet = By.xpath("//label[@for='9990']/input");
	
	By summaryDetailPage = By.xpath("//span[contains(text(),'Summary')]");
	By inviteBtn = By.xpath("//span[contains(text(),'Invite')]");
	By errorMsg = By.xpath("//span[contains(text(),'Phone/email already exists')]");
	By editUser = By.xpath("//h6[contains(text(),'" + VmsConstants.userPhoneNum
			+ "')]/../../following-sibling::div[3]//span[contains(text(),'Edit')]");
	By actionButton = By.xpath("//h6[contains(text(),'" + VmsConstants.userPhoneNum
			+ "')]/../../following-sibling::div[3]//span[@ng-click='enableDisableProfileUser(user)']");
	By status = By.xpath(
			"//h6[contains(text(),'" + VmsConstants.userPhoneNum + "')]/../../following-sibling::div[2]/div/span");
	By updateBtn = By.xpath("//span[contains(text(),'Update')]");
	/* For Existing User */
	String existingPhoneNumber = VmsConstants.existingPhoneNum;
	String existingName = VmsConstants.existingName;
	String existingEmail = VmsConstants.existingEmail;
	String summaryDetailPage1 = VmsConstants.summaryDetailPage;
	String errorMessage = VmsConstants.errorMessage;
	/* For New user */
	String newPhoneNum = VmsConstants.newPhoneNum;
	String newName = VmsConstants.newName;
	String newEmail = VmsConstants.newEmail;
	

	public Profile() {
	}

	public Profile(WebDriver driver) {
		this.driver = driver;
	}

	public boolean profileTab() {
		return this.isVisible(driver, profileIcon);
	}

	public Profile clickProfileIcon() {
		isVisible(driver, profileIcon);
		click(driver, profileIcon);
		// click(driver,profileIcon);
		return this;
	}

	public Profile changeOutlet() {

		click(driver, changeOutlet);
		return this;
	}

	public Profile selectFirstOutlet() {

		click(driver, selectFirstOutlet);
		return this;
	}

	public Profile selectSecondOutlet() {

		click(driver, selectSecondOutlet);
		return this;
	}

	public Profile selectThirdOutlet() {

		click(driver, selectThirdOutlet);
		return this;
	}

	public Profile setGlobalPreptime(int multiCount) {

		WebElement fetchPreptime = driver.findElement(By.name("globalPrepTime"));

		int prep = Integer.parseInt(fetchPreptime.getAttribute("value"));

		if (prep >= 28) {
			for (int i = 1; i <= 2; i++) {
				click(driver, setGlobalPreptime);
			}
			return this;
		} else {

			for (int i = 1; i <= VmsConstants.prepTimeCount; i++) {
				click(driver, setGlobalPreptime);
			}
			return this;
		}
	}

	public Profile updateGlobalPreptime(int multiCount) {

		for (int i = multiCount; i >= 0; i--) {
			click(driver, updateGlobalPreptime);
		}
		return this;
	}

	public int getPrepTime() {
		WebElement fetchPreptime = driver.findElement(By.name("globalPrepTime"));

		return Integer.parseInt(fetchPreptime.getAttribute("value"));
	}

	public Profile selectOutlet(int outlet_no) {
		outlet_no += 3;
		By outlet = By
				.xpath("(//button[@class='btn btn-disabled ng-scope']//i[@class='fa fa-circle'])[" + outlet_no + "]");
		click(driver, outlet);
		return this;
	}

	public Profile addGlobalIfNotExists() {
		PrepTimePage prepTimePage = new PrepTimePage(driver);
		if (prepTimePage.verifyFirstPage())
			prepTimePage.increasePrep(4).saveGlobalPrepTime();
		return this;
	}

	/* Profile :: Account */

	public Profile accountClick() {
		isVisible(driver, Account);
		click(driver, Account);
		return this;
	}

	public String getAccountName() {
		String s1 = getText(driver, accName);
		return s1;
	}

	public String expectedAccName() {
		String s1 = VmsConstants.expectedAccountName;
		return s1;
	}

	public String getSwiggyId() {
		String s1 = getText(driver, swiggyID);
		return s1;
	}

	public String expectedSwiggyId() {
		String s1 = VmsConstants.expectedSwiggyID;
		return s1;
	}

	public String getOnboardingDate() {
		String s1 = getText(driver, onboardingDate);
		return s1;
	}

	public String expectedOnboardingDate() {
		String s1 = VmsConstants.expectedOnboardingDate;
		return s1;
	}

	public String getPhoneNum() {
		String s1 = getText(driver, phoneNum);
		return s1;
	}

	public String expectedPhoneNum() {
		String s1 = VmsConstants.expectedPhoneNumber;
		return s1;
	}

	/* Profile :: Finance */
	public Profile financeClick() {
		isVisible(driver, Finance);
		click(driver, Finance);
		return this;
	}

	public String getBankName() {
		String s1 = getText(driver, bankName);
		return s1;
	}

	public String expectedBankName() {
		String s1 = VmsConstants.expectedBankName;
		return s1;
	}

	public String getAccNum() {
		String s1 = getText(driver, accNum);
		return s1;
	}

	public String expectedAccNum() {
		String s1 = VmsConstants.expectedAccNum;
		return s1;
	}

	public String getIfscCode() {
		String s1 = getText(driver, ifscCode);
		return s1;
	}

	public String expectedIfscCode() {
		String s1 = VmsConstants.expectedIfscCode;
		return s1;
	}

	/* Profile :: Users */
	public Profile userClick() {
		isVisible(driver, Users);
		sleep(3000);
		click(driver, Users);
		return this;
	}

	public String manageUsersVerify() {
		String s1 = getText(driver, manageUser);
		return s1;
	}

	public Profile inviteUserClick() {
		isVisible(driver, inviteUserButton);
		click(driver, inviteUserButton);
		return this;
	}

	public String inviteUserPopUpVerify() {
		isVisible(driver, inviteUserPopUp);
		String s1 = getText(driver, inviteUserPopUp);
		return s1;
	}

	public Profile existingUser() {
		click(driver, inviteUserButton);
		click(driver, chooseRoleAsOwner);
		enterText(driver, invitePhoneNum, existingPhoneNumber);		
		enterText(driver, inviteName, existingName);
		enterText(driver, inviteEmail, existingEmail);
		click(driver, inviteNextButton);
		click(driver, selectRestaurants);
		click(driver, selectOutlet);
		click(driver, inviteNextButton);
		Assert.assertEquals(getText(driver, summaryDetailPage), summaryDetailPage1);
		click(driver, inviteBtn);
		isVisible(driver, errorMsg);
		Assert.assertEquals(getText(driver, errorMsg), errorMessage);
		return this;
	}

	public Profile inviteUserAsOwner() throws InterruptedException {
		isVisible(driver, inviteUserButton);
		Thread.sleep(3000);
		click(driver, inviteUserButton);
		isVisible(driver, chooseRoleAsOwner);
		click(driver, chooseRoleAsOwner);
		enterText(driver, invitePhoneNum, newPhoneNum);
		enterText(driver, inviteName, newName);
		click(driver, inviteNextButton);
		click(driver, selectRestaurants);
		click(driver, selectOutlet);
		click(driver, inviteNextButton);
		Assert.assertEquals(getText(driver, summaryDetailPage), summaryDetailPage1);
		click(driver, inviteBtn);
		return this;

	}

	public Profile inviteUserAsRestMgr() throws InterruptedException {
		isVisible(driver, inviteUserButton);
		Thread.sleep(3000);
		click(driver, inviteUserButton);
		isVisible(driver, chooseRoleAsRM);
		click(driver, chooseRoleAsRM);
		enterText(driver, invitePhoneNum, newPhoneNum);
		enterText(driver, inviteName, newName);
		click(driver, inviteNextButton);
		click(driver, selectRestaurants);
		click(driver, selectOutlet);
		click(driver, inviteNextButton);
		Assert.assertEquals(getText(driver, summaryDetailPage), summaryDetailPage1);
		click(driver, inviteBtn);
		return this;

	}

	public Profile inviteUserAsOrderMgr() throws InterruptedException {
		isVisible(driver, inviteUserButton);
		Thread.sleep(3000);
		click(driver, inviteUserButton);
		isVisible(driver, chooseRoleAsOrderManager);
		click(driver, chooseRoleAsOrderManager);
		enterText(driver, invitePhoneNum, newPhoneNum);
		enterText(driver, inviteName, newName);
		click(driver, inviteNextButton);
		click(driver, selectRestaurants);
		click(driver, selectOutlet);
		click(driver, inviteNextButton);
		Assert.assertEquals(getText(driver, summaryDetailPage), summaryDetailPage1);
		click(driver, inviteBtn);
		return this;

	}

	public Profile editUserRole() throws InterruptedException {
		if (isVisible(driver, editUser)) {
			click(driver, editUser);
			isVisible(driver, role);
			String s1 = getText(driver, role);
			System.out.println("Before Edit Role: " + s1);
			if (s1.equals("Owner")) {
				isVisible(driver, chooseRoleAsRM);
				click(driver, chooseRoleAsRM);

			} else if (s1.equals("Restaurant Manager")) {
				isVisible(driver, chooseRoleAsOrderManager);
				click(driver, chooseRoleAsOrderManager);

			} else if (s1.equals("Order Manager")) {
				isVisible(driver, chooseRoleAsOwner);
				click(driver, chooseRoleAsOwner);
			}
			click(driver, inviteNextButton);
			click(driver, inviteNextButton);
			click(driver, updateBtn);
			Thread.sleep(3000);
			isVisible(driver, role);
			String s2 = getText(driver, role);
			System.out.println("After Edit role: " + s2);
			Assert.assertNotEquals(s1, s2, "Role has not changed");
		} else {
			System.out.println("No Edit Option available for this User");
		}

		return this;
	}

	public Profile editUserOutlet() {
		if (isVisible(driver, editUser)) {
			click(driver, editUser);
			click(driver, inviteNextButton);
			if (isVisible(driver, restIsMaximized)) {
				System.out.println("Restaurant is maximised");
			}else {
				click(driver, selectRestaurants);
			}
			WebElement w1 = driver.findElement(selectOutlet);
			boolean b1 = w1.isSelected();
			click(driver, selectOutlet);
			click(driver, inviteNextButton);
			click(driver, updateBtn);
			sleep(3000);
			click(driver, editUser);
			click(driver, inviteNextButton);
			if (isVisible(driver, restIsMaximized)) {
				System.out.println("Restaurant is maximised");
			}else {
				click(driver, selectRestaurants);
			}
			WebElement w2 = driver.findElement(selectOutlet);
			boolean b2 = w2.isSelected();
			Assert.assertNotEquals(b1, b2, "Checkbox state is not changed");
			

		} else {
			System.out.println("No Edit Option available for this User");
		}
		return this;

	}

	public Profile disableUser() {
		if (isVisible(driver, actionButton)) {
			isVisible(driver, status);
			String s1 = getText(driver, status);
			System.out.println(s1);
			if (s1.equals("Active")) {
				click(driver, actionButton);
				String s2 = getText(driver, status);
				Assert.assertEquals(s2, "Disabled");

			} else {
				System.out.println("User is Already Disabled");
			}

		} else {
			System.out.println("No Enable/Disbale option for this User");

		}
		return this;
	}

	public Profile enableUser() {

		if (isVisible(driver, actionButton)) {
			isVisible(driver, status);
			String s1 = getText(driver, status);
			System.out.println(s1);
			if (s1.equals("Disabled")) {
				click(driver, actionButton);
				String s2 = getText(driver, status);
				Assert.assertEquals(s2, "Active");

			} else {
				System.out.println("User is Already Enabled");
			}

		} else {
			System.out.println("No Enable/Disbale option for this User");

		}
		return this;
	}

}