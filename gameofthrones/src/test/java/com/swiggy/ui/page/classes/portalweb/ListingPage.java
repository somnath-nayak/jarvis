package com.swiggy.ui.page.classes.portalweb;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;

import com.swiggy.ui.utils.WebUtils;

public class ListingPage extends WebUtils
{
	public IElement cart;
	public IElement usernotlogedin_signIn;
	public IElement help; 
	public IElement myAccount;
	public IElement	search;
	public IElement	address;
	public IElement loc;
	public IElement image;
	public IElement helpPageTitle;
	public IElement accountPageTitle;
	public IElement checkoutPageTitle;
	public IElement carousal_nav;
	public IElement carousal_navback;
	public IElement seeAll;
	public IElement addressDropDown;
	public IElement filter;
	public IElement relevance;
	public IElement costForTwo;
	public IElement deliveryTime;
	public IElement rating;
	public IElement addressInput;
	public IElement addressWindowCloseBtn;
	public List<IElement> collections;
	public List<IElement> restaurantRatings;
	public List<IElement> restaurantDeliveryFilter;
	public List<IElement> restaurantCostForTwoFilter;
	public List<IElement> carousals;
	public List<IElement> collection;
	
	
	
    public  ListingPage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
	
	
	public boolean checkCart_Present() 
	{
		return checkElementDisplay(cart);
		
	}
	public boolean checksignIn_Present()
	{
		return checkElementDisplay(usernotlogedin_signIn);
		
	}
	public boolean checkElementPresent(IElement element)
	{
		return checkElementDisplay(element);
	}
	
	public String getUserName()
	{
		return myAccount.getText(10);
	}

    
    public boolean carousalsPresent()
    {

        return carousals.size() > 0;
    }
    
    public void getAllCarousalIntoView()
    
    {
    	for (int i = 0; i < 20; i++) 
    	{
    		if(checkElementDisplay(carousal_nav))
    			carousal_nav.click(10);
    		else
    			break;
		}
    }
    

    
    public boolean openFilterPresent()
    {
    	
    	System.out.println(collection.size()+"#############################################");
        return collection.size() > 0;
    }
    
   public void clickSeeAllRestaurants()
   {
	   
	   seeAll.click(10);
	   
   }
   
   public void clickRatingFilter()
   {
	   rating.click(10);
   }
   public void clickDeliveryFilter()
   {
	   deliveryTime.click(10);
   }
   public void clickCostForTwoFilter()
   {
	   costForTwo.click(10);
   }
   public void clickOnFilter()
   {
	   filter.click(10);
   }
   public String trimedUrl(WebDriver driver)
   {
	   return removeParentUrl(driver.getCurrentUrl(), PortalConstants.portal_url);
   }
   public String getCompleteURL(String str)
   {
	   return (PortalConstants.portal_url+str);
   }
   public void clickOnAnElement(IElement element)
   {
	   element.click(10);
	   
   }
    
   public boolean verifyRatingOrder()
   {
	   ArrayList<String> al = getText(restaurantRatings);
	   System.out.println(al.size());
	   boolean sorted = true;
	   for (int i=1;i<al.size();i++)
	   {	
		   if(!(Float.valueOf(al.get(1-1))>=Float.valueOf(al.get(i))))
			   sorted = false;
	   }
	   return sorted;
   }
   
   public ArrayList<String> getText(List<IElement> element)
   {
   	ArrayList<String> al=new ArrayList<>();
   	
   	for(int i=0;i<element.size();i++) {
   		al.add((element.get(i).getText()));
   	}
   	return al;
   }
   
   public boolean verifyDeliveryFilter() 
   {
	   boolean sorted = true;
	   ArrayList<String> al = getText(restaurantDeliveryFilter);
	   for (int i=1;i<al.size();i++)
	   {	
		   System.out.println(Float.valueOf((al.get(i).replaceAll("\\s.*", ""))));
		   if(!(Float.valueOf((al.get(i-1).replaceAll("\\s.*", ""))) <= Float.valueOf((al.get(i).replaceAll("\\s.*", "")))))
			   sorted = false;
	   }
	   return sorted;
	   
   }
   
   public boolean verifyCostForTwoFilter()
   {
	   boolean sorted = true;
	   ArrayList<String> al = getText(restaurantCostForTwoFilter);
	   for (int i=1;i<al.size();i++)
	   {	
		   System.out.println(Float.valueOf((al.get(i).substring(1).replaceAll("\\s.*", ""))));
		   if(!(Float.valueOf((al.get(i-1).substring(1).replaceAll("\\s.*", ""))) <= Float.valueOf((al.get(i).substring(1).replaceAll("\\s.*", "")))))
			   sorted = false;
	   }
	   return sorted;
	   
   }
   public void clikOnAddressDropDown()
   {
	   addressDropDown.click(10);
   }
   public void clickOnSearchIcon()
   {
	   search.click(10);
	   
   }
   public String getText(IElement element)
   {
	   return element.getText(10);
   }
   public void hoverOverElementAndClick(IElement element)
   {
	   element.hoverAndClick(element, 10);
   }
/*   public boolean verifyCollection()
   {
	   
   }*/
   public ArrayList<String> getTypesOfCollections()
   {
	   ArrayList<String> collectionType = new ArrayList<String>();
	   for(int i=0;i<collection.size();i++)
	   {
		   System.out.println(collections.get(i).getText(10));
		   collectionType.add(collections.get(i).getText(10));
	   }
	   return collectionType;
   }
/*   public boolean verifyCollections()
   {
	   boolean collectionStaus = true;
	   ArrayList<String> collectionType = getTypesOfCollections();
	   for(int i=0;i<collections.size();i++)
	   {
		   collection.get(i).click(10);
		   
	   }*/
   }

   
	


