package com.swiggy.ui.page.classes.portalweb;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.utils.WebUtils;

public class SearchPage extends WebUtils
{
	
	
	public  SearchPage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
	
	public IElement searchInputbox;
	public IElement searchEscBtn;
	
	
	
	public void clickOnEscButton() 
	{
		searchEscBtn.click(10);
		
	}

}
