package com.swiggy.ui.page.classes.vendor.dashboard;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by kiran.j on 12/13/17.
 */
public class ItemPojo {
    String item_id;
    String name;
    String quantity;
    String total;

    public ItemPojo(String item_id, String name, String quantity, String total) {
        this.item_id = item_id;
        this.name = name;
        this.quantity = quantity;
        this.total = total;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("item_id", item_id).append("name", name).append("quantity", quantity).append("total", total).toString();
    }
}
