package com.swiggy.ui.page.classes.vendor.dashboard;

import framework.gameofthrones.Aegon.IElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.swiggy.ui.utils.WebUtil;

public class HomePage extends WebUtil {

	By homePage = By.id("sign-up-form");
	By userName=By.xpath(".//*[@id='sign-up-form']//*[@ng-model='mobNo']");
	By password=By.xpath(".//*[@id='sign-up-form']//*[@ng-model='otp']");
	By verifyLogo=By.xpath("//*[@class='logo'][@title='Swiggy']");
	By signIn=By.id("sign-up-btn");
	By settings = By.xpath("//*[@class='orders']//*[@href='/settings/orders']");
	By orders_tab = By.xpath("//*[@class='dropdown-toggle' and @href='/orders']");
	By prep_time = By.cssSelector(".swal2-close");
	By title = By.cssSelector(".page__title");
	String userName1= VmsConstants.username;
	String password1=VmsConstants.password;
	String multiUserName= VmsConstants.multiUsername;
	String multiPassword=VmsConstants.multiPassowrd;
	//public IElement homePage_new;
	private WebDriver driver;


	public HomePage() {}

	public HomePage(WebDriver driver) {
		this.driver = driver;
		driver.manage().window().maximize();
	}

//	public HomePage(InitializeUI init) {
//		super(init);
//		initElements(init.getWebdriver(),this,init);
//		driver = init.getWebdriver();
//	}

	public boolean verify() {
		return this.isVisible(driver, homePage);
	}

//	public boolean verify() {
//		//return this.isVisible(driver, homePage);
//		return checkElementDisplay(homePage_new);
//	}

	public boolean checkElementDisplay(IElement element){
		try{
			if(element.isDisplayed()){
				return true;
			}
		}
		catch(Exception e){
			return false;
		}
		return false;

	}

	public HomePage login() {
		try {
			enterText(driver, userName, userName1);
			enterText(driver, password, password1);
			Thread.sleep(5000);
			click(driver, signIn);
			Thread.sleep(5000);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}
	
	public HomePage login(String user, String pass) {
		try {
			Thread.sleep(5000);
			enterText(driver, userName, user);
			enterText(driver, password, pass);
			Thread.sleep(5000);
			click(driver, signIn);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this;
	}

	public boolean showOrders() {
		boolean success = false;
		try{
			Thread.sleep(5000);
			try {
				driver.switchTo().activeElement();
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(this.isVisible(driver, prep_time))
				this.click(driver, prep_time);
			driver.switchTo().defaultContent();
			Thread.sleep(5000);
			if(this.isVisible(driver, settings)) {
				this.click(driver, settings);
				success = true;
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return success;
	}

	public  boolean logoverify(){
		return this.isVisible(driver, verifyLogo);
	}

	public HomePage hoverOrders() {
		this.mouseHover(driver, driver.findElement(orders_tab));
		return this;
	}

	public HomePage tapOnOrders() {
	    this.click(driver, orders_tab);
	    return this;
    }

    public String getTitle() {
	    return this.getText(driver, title);
    }

    public HomePage dismissPrepTime() {

        if(this.isVisible(driver, prep_time))
            this.click(driver, prep_time);
        sleep();
	    return this;
    }

}

