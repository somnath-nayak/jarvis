package com.swiggy.ui.page.classes.portalweb;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.utils.WebUtils;

public class SignUpPage extends WebUtils
{
	public IElement text_SignupPage;
	public IElement linkTo_login;
	public IElement mobile_no;
	public IElement password;
	public IElement name;
	public IElement email;
	public IElement refLink;
	public IElement refcode_inpt;
	public IElement btn_submit;
	public IElement btn_close;
	public IElement errorPhNo;
	public IElement errorName;
	public IElement errorEmail;
	public IElement errorPwd;
	
    public  SignUpPage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
    
    
    public boolean closeBtn_Present()
    {
    	return checkElementDisplay(btn_close);
    }

    
    public String getSignUppage_Text()
    {
    	return text_SignupPage.getText(10);
    	
    }
    
    public boolean loginToAccountLink_Present()
    {
    	return checkElementDisplay(linkTo_login);
    }
    
    public boolean nameField_present()
    {
    	return checkElementDisplay(name);
    	
    }
    
    public boolean emailField_Present()
    {
    	return checkElementDisplay(email);
    }
    
    public boolean referalField_Present()
    {
    	return checkElementDisplay(refcode_inpt);
    }
    
    public boolean mobileField_Present()
    {
    	return checkElementDisplay(mobile_no);
    }
    
    public boolean passwordField_Present()
    {
    	return checkElementDisplay(password);
    }
    
    public boolean submit_Present()
    {
    	return checkElementDisplay(btn_submit);
    }
    public void clickOnClose()
    {
    	btn_close.click();
    }
    
    public void click_Referal()
    {
    	refLink.click(10);
    }
    
	public void clickOnContinue()
	{
		btn_submit.click(10);
	}
	public void sendKeys(String key, IElement element)
	{
		element.clear(10);
		element.sendKeys(key, 10);
	}
	public String getErrorMessage(IElement element)
	{
		return element.getText(10);
	}


}
