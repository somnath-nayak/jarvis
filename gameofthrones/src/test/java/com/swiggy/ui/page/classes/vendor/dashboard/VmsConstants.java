package com.swiggy.ui.page.classes.vendor.dashboard;

/**
 * Created by kiran.j on 12/4/17.
 */
public interface VmsConstants {

    enum OrderStatusTab{
        NEW,
        PREPARING,
        READY,
        MORE
    }


    enum OrderStatus {
        PENDING
    }

    String username = "9990";
    //String username = "30751";
    String password = "imran@123";
    String multiUsername = "9036285925";
    String multiPassowrd = "El3ment@ry";

    String dashboard_url = "http://vendor-u-cuat-02.swiggyops.de:7000/login";

   // String dashboard_url = "https://partner.swiggy.com/login";

    String delivery_id = "216";
    String mobileno = "9738948943";
    String orderpos = "swiggy";
    String mobileno2 = "9738913616";
    String restId = "5271";
    String restIdMulti = "9738948943";
    String prodPass = "imran@123";
    String quantity = "2";
    //String password = "imran1";
    
    String itemName = "autoTestItem";
    String price = "10";
    String packaging ="1";
    String price1 = "20";
    String packaging1 ="2";
    
    
    int prepTimeCount = 2;
    int multiPrepTimeCount = 2;
    int prepTime = 30;
    
//    ------------Slot Level Constants
    String slotLevelTimeStart = "START";
    String slotLevelTimeEnd = "END";
    String slotLevelIncrease = "INCREASE";
    String slotLevelDecrease = "DECREASE";
    String slotLevelHours = "HOUR";
    String slotLevelMins = "MIN";
    String slotLevelAM = "AM";
    String slotLevelPM = "PM";
    int slotLevelPreptimeCount1 = 5; //Needed for changing time in Slot wise time Automation
    int slotLevelPreptimeCount2 = 3; //Needed for changing time in  Slot wise time Automation
    //String password = "imran1";
    //String passowrd = "imran@1234";
    String order_confirm_code = "011";
    String outofstock = "$.objects[0].status.status";
    String orders_title = "MANAGE ORDERS";
    
    /*Profile Related Constants*/
    /*Existing User*/
    String existingPhoneNum = "9742471472";
    String existingName = "Ravi";
    String existingEmail = "ravi.brahmam@swiggy.in";
    String summaryDetailPage = "Summary";
    String errorMessage = "Phone/email already exists";
    String userPhoneNum = "7975633213";
    /*For New User*/
    String newPhoneNum = "1000000002";
    String newName = "test3";
    String newEmail = "ravi.brahmam@swiggy.in";
    String selectOutlet = "9990";
    /*Constants*/
    String expectedAccountName = "Cafe PeterDonut (Test)";
    String expectedSwiggyID = "2258";
    String expectedOnboardingDate = "15/07/15";
    String expectedPhoneNumber = "02065108709";
    String expectedBankName = "-";
    String expectedAccNum="-";
    String expectedIfscCode = "-";
    
}