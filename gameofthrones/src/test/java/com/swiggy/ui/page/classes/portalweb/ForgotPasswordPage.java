package com.swiggy.ui.page.classes.portalweb;

import org.openqa.selenium.WebDriver;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.utils.WebUtils;

public class ForgotPasswordPage extends WebUtils
{
	
	public IElement pageText;
	public IElement login_link;
	public IElement mb_no;
	public IElement otp;
	public IElement submit;
	public IElement newPwd;
	public IElement re_enterPwd;
	public IElement setPwd;
	public IElement validation_msg_newPwd;
	public IElement validation_msg_re_enterPwd;
	public IElement backBtn;
	public IElement verfyOtpText;
	public IElement validation_msg_otp;
	
	
    public  ForgotPasswordPage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
	
	
	public boolean checkLoginLink_Present()
	{
		return checkElementDisplay(login_link);
	}
	public boolean checkMobileNoField_Present()
	{
		return checkElementDisplay(mb_no);
	}
	public boolean checkOtpField_Present()
	{
		return checkElementDisplay(otp);
	}
	public boolean checkVerifyOtp_Present()
	{
		return checkElementDisplay(submit);
	}
	
	public boolean checknewPasswordField_Present()
	{
		return checkElementDisplay(newPwd);
	}
	public boolean checkReEnterPasswordField_Present()
	{
		return checkElementDisplay(re_enterPwd);
	}
	public boolean checkSetPasswordField_Present()
	{
		return checkElementDisplay(setPwd);
	}
	
	public boolean checkBackBtn_Present()
	{
		return checkElementDisplay(backBtn);
	}
	
	public String getTextOfPage()
	{
		if(checkElementDisplay(pageText))
		{
			return pageText.getText(10);
		}
		else
			return null;
		
	}
	
	public void  navigateBack()
	{
		backBtn.click(10);
	}
	
	public String getText()
	{
		return mb_no.getAttribute("value", 10);
	}
	public void enterOTP(String OTP)
	{
		otp.sendKeys(OTP, 10);
		
	}
	public void clickVerifyOTP()
	{
		submit.click(10);
	}
	
	public String  validation_msg_otp()
	{
		return validation_msg_otp.getText(10);
	}
    public void deleteAllCookies(WebDriver driver)
    {
    	driver.manage().deleteAllCookies();
    }
	

}
