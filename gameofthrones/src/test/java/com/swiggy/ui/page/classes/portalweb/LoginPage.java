package com.swiggy.ui.page.classes.portalweb;

import org.openqa.selenium.WebDriver;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.utils.WebUtils;



public class LoginPage extends WebUtils
{
	public IElement loginpage_text;
	public IElement createaccount_link;
	public IElement mobile_no;
	public IElement password;
	public IElement forget_link;
	public IElement btn_submit;
	public IElement error_text;
	public IElement btn_close;
	public IElement errormsg_invalidphno;
	public IElement userName;
	public IElement forgotpasswordpage_text;
	public IElement backicon;
	
	
    public  LoginPage(InitializeUI inti)
    {
        initElements(inti.getWebdriver(),this,inti);
    }
	
	
	public void enterMobileNo(String mobileNo)
	{
		mobile_no.clear(10);
		mobile_no.sendKeys(mobileNo, 10);
		
	}
	
	public void enterPassword(String pwd)
	{
		password.clear(10);
		password.sendKeys(pwd, 10);
		
	}
	public void clearPwdField()
	{
		password.clear(10);
		
	}
	public void clearPhnNOField()
	{
		mobile_no.clear(10);
		
	}
	public boolean checkUserNamePresent()
	{
		return checkElementDisplay(userName);
		
	}
	

	
	public void clickOnLogin()
	{
		btn_submit.click(10);
	}
	
	public String getErrorForInvalidPassword()
	{
		
		return error_text.getText(20);
	}
	
	public void clickOnCreateAccountLink()
	{
		createaccount_link.click(10);
	}
	
    public String getLoginPage_Text()
    {
    	return loginpage_text.getText(10);
    }
    
    public void clickForgotPwd_link()
    {
    	forget_link.click(10);
    }
    public void refreshBrowser(WebDriver driver)
    {
    	driver.navigate().refresh();
    }
    
    public boolean mobileField_Present()
    {
    	return checkElementDisplay(mobile_no);
    }
    
    public boolean passwordField_Present()
    {
    	return checkElementDisplay(password);
    }
    
    public boolean submit_Present()
    {
    	return checkElementDisplay(btn_submit);
    }
    
    public boolean forgotPassword_Present()
    {
    	return checkElementDisplay(forget_link);
    }
    
    public boolean createAccounttLink_Present()
    {
    	return checkElementDisplay(createaccount_link);
    }
    
    public boolean closeBtn_Present()
    {
    	return checkElementDisplay(btn_close);
    }

    
    public void clickOnClose()
    {
    	btn_close.click();
    }
    public String getErrorForInvalidPhNo()
    {
    	
    	return errormsg_invalidphno.getText(10);
    }
    
    public String getForogotpasswordPageText()
    {
    	
    	return forgotpasswordpage_text.getText(10);
    }
    public void navigateBack() 
    {
    	backicon.click(10);
    }
    public void deleteAllCookies(WebDriver driver)
    {
    	driver.manage().deleteAllCookies();
    }
    public void navigateTo(WebDriver driver, String Url)
    {
    	driver.navigate().to(Url);
    }
    


}
