package com.swiggy.ui.page.classes.vendor.dashboard;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 6/6/18.
 */
public class VendorPastOrders extends AndroidUtil {

    public IElement txt_past_orders;
    public IElement btn_past_order;
    public IElement txt_order_id;
    public IElement txt_delivered;
    public IElement txt_cancelled;

    public VendorPastOrders(InitializeUI inti) {
        super(inti);
        initElements(inti.getAppiumDriver(), this, inti);
    }


    public VendorPastOrders selectFirstOrder() {
        btn_past_order.click();
        return this;
    }

    public boolean isPastOrderPage() {
        return checkElementDisplay(txt_past_orders);
    }

    public boolean checkElementDisplay(IElement element){
        try{
            if(element.isDisplayed())
                return true;
        }
        catch(Exception e){
            return false;
        }
        return false;
    }

    public String getOrderId() {
        return txt_order_id.getText();
    }

    public boolean isDelivered() {
        return checkElementDisplay(txt_delivered);
    }

    public boolean isCancelled() {
        return checkElementDisplay(txt_cancelled);
    }

}
