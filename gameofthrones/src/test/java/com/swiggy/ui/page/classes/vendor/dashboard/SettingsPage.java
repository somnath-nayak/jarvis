package com.swiggy.ui.page.classes.vendor.dashboard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.swiggy.ui.utils.WebUtil;

/**
 * Created by kiran.j on 12/4/17.
 */
public class SettingsPage extends WebUtil {

    private WebDriver driver = null;
    By toggle_orders = By.xpath("//span[@class='bg']");

    public SettingsPage(WebDriver driver) {
        this.driver = driver;
    }

    public void toggleShowOrders() {
        if(this.isVisible(driver, toggle_orders))
            this.click(driver, toggle_orders);
    }

}
