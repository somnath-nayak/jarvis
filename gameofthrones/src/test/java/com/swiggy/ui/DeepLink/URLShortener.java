package com.swiggy.ui.DeepLink;

import java.util.HashMap;
import java.util.Random;

/*
 * URL Shortener
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

//Author - Mohamed Shimran
//Blog - <a class="vglnk" href="http://www.ultimateprogrammingtutorials.info" rel="nofollow"><span>http</span><span>://</span><span>www</span><span>.</span><span>ultimateprogrammingtutorials</span><span>.</span><span>info</span></a>

class URLShortener {

    public static String getShortenerUrl(String longUrl) {
        String Link=null;
        try {
            URL url = new URL("http://tinyurl.com/api-create.php?url=" + longUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                Link=output;
            }
        } catch (Exception e) {

        }
        return Link;


    }
}
