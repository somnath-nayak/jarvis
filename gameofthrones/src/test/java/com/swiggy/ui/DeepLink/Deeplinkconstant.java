package com.swiggy.ui.DeepLink;

public class Deeplinkconstant {

    public static final String DEEPLINK_DISCOVERY="discovery";
    public static final String DEEPLINK_PROFILE="profile";
    public static  final String DEEPLINK_CART="cart";
    public static final String DEEPLINK_FAV="favourites";
    public static final String DEEPLINK_INVITE="invite";
    public static final String DEEPLINK_PAYMENTS="payments";
    public static final String DEEPLINK_FILTER="filter";
    public static final String DEEPLINK_SORT="sort";
    public static final String DEEPLINK_OFFERS="offers";
    public static final String DEEPLINK_TRACK_ORDER="track-order";
    public static final String DEEPLINK_SUPPORT="support";
    public static final String DEEPLINK_COLLECTION="collection";
    public static final String DEEPLINK_MENU="menu";
    public static final String DEEPLINK_RESTAURANTLIST="restaurantList";
    public static final String DEEPLINK_SLUG="bangalore";
    public static final String DEEPLINK_TRACK_ORDERID="track";
    public static final String DEEPLINK_POP="pop";
    public static final String DEEPLINK_PREORDER="";
    public static final String DEEPLINKCOMMAND="adb -s "+System.getenv("deviceId")+" shell am start -a android.intent.action.VIEW ";


    public static final String DEEPLINKCOMMANDios="xcrun simctl openurl booted";

}
