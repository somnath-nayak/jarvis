package com.swiggy.ui.DeepLink;

import com.swiggy.ui.api.ConsumerApiHelper;
import org.testng.annotations.DataProvider;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.utils.Constants;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public class DeeplinkDataProvider {

    @DataProvider(name = "DeepLinkURL")
    public Object[][] deeplinkData() {


        ConsumerApiHelper c = new ConsumerApiHelper();
        ArrayList<String> al = c.getAllCusFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "CUISINES");


        return new Object[][]{
                {3, "swiggy://cart"},
                {3, "swiggy://profile"},
                {2, "swiggy://discovery"},
                {1, "swiggy://menu?restaurant_id=36972&uuid=599542a8-1e2e-4b10-8ef2-3b447bcd1afd"},
                {1, "swiggy://filter?CUISINES=" + c.getAllCusFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "CUISINES").get(0)},
                {9, "swiggy://support"},
                {9, "swiggy://support?ordId=1164633502"},
                {1, "swiggy://track?ordId=1154071710"},
                {1, "https://www.swiggy.com/bangalore/truffles-ice-spice-5th-block-koramangala"},
                {1, "swiggy://support?ordId=1159562898"},
                {1, "swiggy://filter?sortBy=" + "COST_FOR_TWO"},
                {1, "swiggy://filter?CUISINES=" + c.getAllCusFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "CUISINES").get(0)},
                {1, "swiggy://filter?SHOW_RESTAURANTS_WITH=" + c.getAllCusFilter(Double.toString(Constants.lat1), Double.toString(Constants.lng1), "SHOW_RESTAURANTS_WITH").get(0)},
                {4, "swiggy://favourites"},
                {5, "swiggy://invite"},
                {6, "swiggy://payments"},
                {7, "swiggy://offers"},
                {9, "swiggy://support"},
                {1, MessageFormat.format("swiggy://restaurantList?lat={0}&lng={1}", Constants.lat2, Constants.lng2)},
                {9, "swiggy://pop"},
                {15, "swiggy://collection?collection_id=1&collection_name=Trending"},
                {9, "swiggy://support?ordId=1177255578"}

        };
    }

    @DataProvider(name = "preOrder")
    public Object[][] preOrderDeeplink() {


        ConsumerApiHelper c = new ConsumerApiHelper();
        Menulist alPreorderList = c.getRestaurantTimers(Double.toString(Constants.lat4), Double.toString(Constants.lng4));
        long date = alPreorderList.getPreorderdate().get(1);
        long startTimer = alPreorderList.getHm_preorderTimers().get(alPreorderList.getPreorderdate().get(1)).get(0).getStartTime();
        long endTimer = alPreorderList.getHm_preorderTimers().get(alPreorderList.getPreorderdate().get(1)).get(0).getEndTime();
        return new Object[][]{
                {9, MessageFormat.format("swiggy://preorder?date={0}&startTime={1}&endTime={2}", Long.toString(date), Long.toString(startTimer), Long.toString(endTimer))},

        };


    }
}



