package com.swiggy.ui.DeepLink;
import com.swiggy.ui.api.ConsumerApiHelper;
import framework.gameofthrones.Aegon.InitializeUI;
import com.swiggy.ui.pojoClasses.Menulist;
import com.swiggy.ui.screen.classes.consumerapp.android.AddressPage;
import com.swiggy.ui.screen.classes.consumerapp.android.HomePage;
import com.swiggy.ui.screen.classes.consumerapp.android.RestaurantPage;
import com.swiggy.ui.utils.AndroidUtil;
import framework.gameofthrones.Aegon.InitializeUI;

import com.swiggy.ui.screen.classes.consumerapp.android.*;
import com.swiggy.ui.utils.Constants;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import java.net.*;
import java.util.concurrent.TimeUnit;

public class DeeplinkHelper {


    public String ParseLink(int a, String url,InitializeUI init) {
        String temp=null;
       try {
           if (url.contains("www.swiggy.com")) {
               URI uri = new URI(url);
               System.out.println(uri.getPath().split("/")[1]);
               return uri.getPath().split("/")[1];

           }
           else{
               String[] arrayist = url.split("//");
               if(arrayist[1].contains("?")){
                   temp=arrayist[1].split("\\?")[0].trim();
               }
               else{
                   temp=arrayist[1];
               }
               return temp;}
       }
       catch(Exception e){
            return null;
       }




    }

    public Map<String, String> getQueryMap(String query)
    {
        Map<String, String> map = new HashMap<String, String>();
        try {

            URL aURL = new URL(query.replaceAll("swiggy://","http://www.swiggy.com/"));
            System.out.println("query = " + aURL.getQuery());
            String[] params = aURL.getQuery().split("&");

            for (String param : params)
            {
                String name = param.split("=")[0];
                String value = param.split("=")[1];
                map.put(name, value);
                System.out.println(name);
                System.out.println(value);

            }
        }
        catch(Exception e){
            System.out.println(e);
        }
        return map;
    }

    public void openDeeplink(String url){
        String temp_url;
        if(url.contains("&")){
            temp_url=URLShortener.getShortenerUrl(url);
        }
        else{
            temp_url=url;
        }
        AndroidUtil.runShellCommand(Deeplinkconstant.DEEPLINKCOMMAND+temp_url);
    }

    public void openDeeplinkIos(String url){
        String temp_url;
        if(url.contains("&")){
            temp_url=URLShortener.getShortenerUrl(url);
        }
        else{
            temp_url=url;
        }
        AndroidUtil.runShellCommandios(temp_url);
    }

    public static void main(String[] args){

        ConsumerApiHelper c=new ConsumerApiHelper();


        long currentTime = System.currentTimeMillis();
        long oneHourLater = currentTime + TimeUnit.HOURS.toMillis(1l);
        System.out.println(oneHourLater);
        Date d = new Date(new Date().getTime() + 86400000);
        System.out.println(d.getTime());
        Calendar cal = Calendar.getInstance(); //current date and time
        cal.add(Calendar.DAY_OF_MONTH, 1); //add a day
        cal.set(Calendar.HOUR_OF_DAY, 23); //set hour to last hour
        cal.set(Calendar.MINUTE, 59); //set minutes to last minute
        cal.set(Calendar.SECOND, 59); //set seconds to last second
        cal.set(Calendar.MILLISECOND, 999); //set milliseconds to last millisecond
        long millis = cal.getTimeInMillis();

        System.out.println(cal.getTime());

        DeeplinkHelper deeplinkHelper=new DeeplinkHelper();
        deeplinkHelper.ParseLink(1,"https://www.swiggy.com/bangalore/truffles-ice-spice-5th-block-koramangala",null);
    }







}