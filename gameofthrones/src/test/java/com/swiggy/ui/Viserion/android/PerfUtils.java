package com.swiggy.ui.Viserion.android;

import java.io.*;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PerfUtils {

    public static String packageName = System.getenv("packageName").replace("\"","");
    public static String pathToApk = System.getenv("appPath");

    public static String execCmd(String exec_cmd) {
        StringBuffer output = new StringBuffer();
        Process p;
        String command;
        if (exec_cmd.contains("adb "))
            command = System.getenv("ANDROID_HOME")+"/"+exec_cmd;
        else
            command = exec_cmd;
        String[] cmd = {"/bin/sh", "-c", command};
        try {
            String line = "";
            p = Runtime.getRuntime().exec(cmd);
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader error = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((line = input.readLine()) != null) {
                output.append(line + "\n");
            }
            while ((line = error.readLine()) != null) {
                output.append("Error Occurred : \n" + line + "\n");
            }
        } catch (Exception e) {
            System.out.println("Unable to Execute Command " + command);
            e.printStackTrace();
        }
        if (output.toString().length() > 0)
            return output.toString().substring(0, output.lastIndexOf("\n"));
        else
            return output.toString();
    }

    /**
     *
     * @param runType
     * @return
     */
    public HashMap parseInputFileAndReturnMap(String runType) {
        // createDirectoryToSaveHeapFiles()();
        HashMap<String, String> keyValueMap = new HashMap<>();
        switch (runType.toUpperCase()) {
            case "MEMORY":
                return getMemoryMap();
            case "CPU":
                return getCpuMap();
            case "BATTERY":
                return getBatteryMap();
        }
        return null;
    }

    /**
     *
     * @param runType
     * @return
     */
    public HashMap<String, Object> parseInputFileAndReturnMapStrInt(String dirName, String app, String testcase, String runType) {
        HashMap<String, String> keyValueMap = new HashMap<>();
        switch (runType.toUpperCase()) {
            case "MEMORY":
                return getMemoryMapStrInt(dirName);
            case "CPU":
                return getCpuMapStrInt(dirName);
            case "BATTERY":
                return getBatteryMapStrInt();
            case "APKSIZE":
                return getApkSize();
            case "NETWORK":
                return getNetworkMapStrInt();
        }
        return null;
    }

    /**
     *
     * @param path
     * @return
     */
   /* public String getFileContent(String path) {
        File f = new File(path);
        if (f.exists()) {
            String line;
            StringBuffer sb = new StringBuffer();
            BufferedReader bf = null;
            try {
                bf = new BufferedReader(new FileReader(f));
                while ((line = bf.readLine()) != null) {
                    sb.append(line + "\n");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return sb.toString();
        } else {
            System.out.println("ERROR  : " + path + " file not found");
            return "";
        }
    }*/

    /**
     *
     * @return
     */
    private HashMap getMemoryMap() {
        HashMap<String, String> keyValueMap = new HashMap<>();
        String m_getmeminfo = PerfConstants.M_GETMEMINFO.replace("${PACKAGE}",packageName);
        String output = execCmd(m_getmeminfo);
        String path = writeStringAsFile(output, "memoryheap.txt");
        String command = PerfConstants.M_MEMLINUX_COMMAND.replace("${PATH}", path).replace("${PACKAGE}",packageName);
        for (String getType : PerfConstants.M_ARRAY) {
            String finalCommandToExecute = command.replace("${TYPE}", getType);
            String outputFromLinuxCommand = execCmd(finalCommandToExecute);
            if (outputFromLinuxCommand.length() > 0)
                keyValueMap.put(getType, outputFromLinuxCommand);
            else
                keyValueMap.put(getType, "0");
            System.out.println(getType + " : " + execCmd(finalCommandToExecute));
        }
        return keyValueMap;
    }

    /**
     *
     * @return
     */
    private HashMap getMemoryMapStrInt(String dirPath) {
        System.out.println("-----------------> Pushing Memory Stats into DB");
        HashMap<String, Integer> keyValueMap = new HashMap<>();
        String m_getmeminfo = PerfConstants.M_GETMEMINFO.replace("${PACKAGE}",packageName);
        String output = execCmd(m_getmeminfo);
        String path = writeStringAsFile(output, dirPath + File.separator + "memoryheap_" + getCurrentTimeStamp() + ".txt");
        String command = PerfConstants.M_MEMLINUX_COMMAND.replace("${PATH}", path).replace("${PACKAGE}",packageName);
        for (String getType : PerfConstants.M_ARRAY) {
            String finalCommandToExecute = command.replace("${TYPE}", getType).replace("${PACKAGE}",packageName);
            int outputFromLinuxCommand = setZeroIfEmpty(execCmd(finalCommandToExecute));
            keyValueMap.put(getType, outputFromLinuxCommand);
            // System.out.println(getType + " : " + execCmd(finalCommandToExecute));
        }
        return keyValueMap;
    }

    /**
     *
     * @return
     */
    private HashMap getCpuMap() {
        HashMap<String, String> keyValueMap = new HashMap<>();
        String[] valuesOfCpuStatsForProcess;
        String[] valuesOfCpuStatsForDevice;
        String c_getcpuinfo = PerfConstants.C_GETCPUINFO.replace("${PACKAGE}",packageName);
        String output = execCmd(c_getcpuinfo);
        String path = writeStringAsFile(output, "cpuheap.txt");
        String commandForProcessId = PerfConstants.C_PROCESS_ID.replace("${PATH}", path).replace("${PACKAGE}",packageName);
        String commandForCpuStatsProcess = PerfConstants.CP_CPU_STATS.replace("${PATH}", path).replace("${PACKAGE}",packageName);
        String commandForCpuStatsTotal = PerfConstants.CT_CPU_STATS.replace("${PATH}", path).replace("${PACKAGE}",packageName);

        // Add process id of "in.swiggy.android"
        keyValueMap.put("processID", execCmd(commandForProcessId));

        // Add all process stats into map
        String outputOfLinuxCommand = execCmd(commandForCpuStatsProcess);
        if (outputOfLinuxCommand.length() > 0)
            valuesOfCpuStatsForProcess = outputOfLinuxCommand.split(":");
        else
            valuesOfCpuStatsForProcess = new String[] {"0", "0", "0", "0", "0"};

        for (int i = 0; i < PerfConstants.CP_ARRAY.length; i++) {
            keyValueMap.put(PerfConstants.CP_ARRAY[i], valuesOfCpuStatsForProcess[i]);
            // System.out.println(PerfConstants.CP_ARRAY[i]+" : "+valuesOfCpuStatsForProcess[i]);
        }

        // Add overall device cpu stats into map
        String outputOfLinuxCommandTotal = execCmd(commandForCpuStatsTotal);
        if (outputOfLinuxCommandTotal.length() > 0)
            valuesOfCpuStatsForDevice = outputOfLinuxCommandTotal.split(":");
        else
            valuesOfCpuStatsForDevice = new String[] {"0", "0", "0", "0", "0"};

        // String[] valuesOfCpuStatsForDevice = execCmd(commandForCpuStatsTotal).split(":");
        for (int i = 0; i < PerfConstants.CT_ARRAY.length; i++) {
            keyValueMap.put(PerfConstants.CT_ARRAY[i], valuesOfCpuStatsForDevice[i]);
            System.out.println(PerfConstants.CT_ARRAY[i]+" : "+valuesOfCpuStatsForDevice[i]);
        }
        return keyValueMap;
    }

    /**
     *
     * @return
     */
    private HashMap getCpuMapStrInt(String dirPath) {
        System.out.println("-----------------> Pushing CPU Stats into DB");
        HashMap<String, Double> keyValueMap = new HashMap<>();
        String[] valuesOfCpuStatsForProcess = null;
        String[] valuesOfCpuStatsForDevice = null;
        String c_getcpuinfo = PerfConstants.C_GETCPUINFO.replace("${PACKAGE}",packageName);
        String output = execCmd(c_getcpuinfo);

        String path = writeStringAsFile(output, dirPath + File.separator + "cpu_" + getCurrentTimeStamp() + ".txt");
        System.out.println(path);
        String commandForCpuStatsProcess = PerfConstants.CP_CPU_STATS.replace("${PATH}", path).replace("${PACKAGE}",packageName);
        String commandForCpuStatsTotal = PerfConstants.CT_CPU_STATS.replace("${PATH}", path).replace("${PACKAGE}",packageName);

        // Add all process stats into map
        String outputOfLinuxCommand = execCmd(commandForCpuStatsProcess);
        if (outputOfLinuxCommand.length() > 0)
            valuesOfCpuStatsForProcess = outputOfLinuxCommand.split(":");
        else
            valuesOfCpuStatsForProcess = new String[] {"0.0", "0.0", "0.0", "0.0", "0.0"};

        for (int i = 0; i < PerfConstants.CP_ARRAY.length; i++) {
            String valueToBeInserted = (valuesOfCpuStatsForProcess[i].contains("-") && valuesOfCpuStatsForProcess[i].contains("%")) ? "0.0" : valuesOfCpuStatsForProcess[i].replace("%", "");
            keyValueMap.put(PerfConstants.CP_ARRAY[i], setZeroIfEmptyDouble(valueToBeInserted));
            // System.out.println(PerfConstants.CP_ARRAY[i]+" : "+valueToBeInserted);
        }

        // Add overall device cpu stats into map
        String outputOfLinuxCommandTotal = execCmd(commandForCpuStatsTotal);
        if (outputOfLinuxCommandTotal.length() > 0)
            valuesOfCpuStatsForDevice = outputOfLinuxCommandTotal.split(":");
        else
            valuesOfCpuStatsForDevice = new String[] {"0.0", "0.0", "0.0", "0.0", "0.0"};

        for (int i = 0; i < PerfConstants.CT_ARRAY.length; i++) {
            String valueToBeInserted = (valuesOfCpuStatsForDevice[i].contains("-") && valuesOfCpuStatsForDevice[i].contains("%")) ? "0" : valuesOfCpuStatsForDevice[i].replace("%", "");
            keyValueMap.put(PerfConstants.CT_ARRAY[i], setZeroIfEmptyDouble(valueToBeInserted));
            // System.out.println(PerfConstants.CT_ARRAY[i]+" : "+valueToBeInserted);
        }

        return keyValueMap;
    }

    /**
     * Method to call batterystats and get the memory map
     */
    private HashMap<String, String> getBatteryMap() {
        String command = execCmd(PerfConstants.GETUIDOFSWIGGYV2.replace("${PACKAGE}",packageName)).replace("\n", "");
        //debugger
        //System.out.println("------------------------------------>"+command);
        String commandToExecuted = PerfConstants.GETBATTERYCONSUMPTION.replace("${UID}", command).replace("${PACKAGE}",packageName);
        HashMap<String, String> keyValueMap = new HashMap<>();
        keyValueMap.put("battery_consumed", execCmd(commandToExecuted).replace("\n", ""));
        System.out.println("battery_consumed" + " : " + execCmd(commandToExecuted));
        return keyValueMap;
    }

    /**
     * Method to call batterystats and get the memory map
     */
    private HashMap getBatteryMapStrInt() {
        System.out.println("-----------------> Pushing Battery Stats into DB");
        String command = execCmd(PerfConstants.GETUIDOFSWIGGYV2.replace("${PACKAGE}",packageName));
        String commandToExecuted = PerfConstants.GETBATTERYCONSUMPTION.replace("${PACKAGE}",packageName).replace("${UID}", command);
        HashMap<String, Double> keyValueMap = new HashMap<>();
        //debugger
        //System.out.println("------------------------------------1.Command-> " + command);
        //System.out.println("-----------------Battery Command To Exec----------"+commandToExecuted);
        Double batteryLevel = setZeroIfEmptyDouble(getBatteryLevel());
        Double batteryConsumed = setZeroIfEmptyDouble(execCmd(commandToExecuted));
        System.out.println("Battery Consumed : " + batteryConsumed);
        keyValueMap.put("battery_level", batteryLevel);
        keyValueMap.put("battery_consumed", batteryConsumed);
        String[] batteryConsumptionBreakup = getBatteryConsumptionBreakup().trim().split("\\s");
        if (batteryConsumptionBreakup.length > 0) {
            for (int i = 0; i < batteryConsumptionBreakup.length; i++) {
                if (batteryConsumptionBreakup[i].trim().contains("=")) {
                    String[] keyValue = batteryConsumptionBreakup[i].trim().split("=");
                    if (keyValue.length != 0) {
                        String key = keyValue[0];
                        Double value = setZeroIfEmptyDouble(keyValue[1]);
                        keyValueMap.put(key, value);
                        System.out.println(key + " : " + value);
                    }
                }

            }
        }

        return keyValueMap;
    }

    private HashMap getNetworkMapStrInt() {
        double f_rx = 0, f_tx = 0, b_rx=0, b_tx=0 , total_rx =0, total_tx=0;
        HashMap<String, Double> hashmap = new HashMap<>();
        String uid = execCmd(PerfConstants.GETUIDOFAPACKAGE.replace("${PACKAGE}",packageName));
        System.out.println("uid --> "+uid);
        String get_qta_guid = execCmd((PerfConstants.GETSWIGGYNETWORKSTATS_FGBG.replace("${UID}",uid)));
        //debug
        //System.out.println(get_qta_guid);
        String[] tmp = get_qta_guid.split("\n");
        for (String t : tmp) {
            //debug
           //System.out.println("L: "+t);
            String[] fgbg_rx_tx = t.split(" ");
            if((Integer.parseInt(fgbg_rx_tx[0]))==0){
                total_rx = total_rx + Integer.parseInt(fgbg_rx_tx[1]);
                total_tx = total_tx + Integer.parseInt(fgbg_rx_tx[2]);
                b_rx = b_rx + Integer.parseInt(fgbg_rx_tx[1]);
                b_tx = b_tx + Integer.parseInt(fgbg_rx_tx[2]);
            }
            else {
                total_rx = total_rx + Integer.parseInt(fgbg_rx_tx[1]);
                total_tx = total_tx + Integer.parseInt(fgbg_rx_tx[2]);
                f_rx = f_rx + Integer.parseInt(fgbg_rx_tx[1]);
                f_tx = f_tx + Integer.parseInt(fgbg_rx_tx[2]);
            }
        }
        //convert to MB
        b_rx = b_rx/(1024*1024);
        b_tx = b_tx/(1024*1024);
        f_rx = f_rx/(1024*1024);
        f_tx = f_tx/(1024*1024);
        total_rx = total_rx/(1024*1024);
        total_tx = total_tx/(1024*1024);
        String[] value_holder = {
                String.format("%.3f", b_rx),
                String.format("%.3f", b_tx),
                String.format("%.3f", f_rx),
                String.format("%.3f", f_tx),
                String.format("%.3f", total_rx),
                String.format("%.3f", total_tx)
            };
        //debug
        System.out.println("b_rx --> "+ value_holder[0] + " b_tx --> "+ value_holder[1]);
        System.out.println("f_rx --> " + value_holder[2] + " f_tx --> " + value_holder[3]);
        System.out.println("total_rx --> "+ value_holder[4]+" total_tx -->"+ value_holder[5]);

        for (int i = 0; i < PerfConstants.NTWK_ARRAY.length; i++) {
            hashmap.put(PerfConstants.NTWK_ARRAY[i], setZeroIfEmptyDouble(value_holder[i]));
        }

        return hashmap;
    }

    /**
     *
     * @return battery consumption breakup
     */
    public static String getBatteryConsumptionBreakup() {
        return execCmd(PerfConstants.BATTERYCONSUMPTIONBREAKUP.replace("${UID}", execCmd(PerfConstants.GETUIDOFSWIGGYV2.replace("${PACKAGE}",packageName)))).trim();
    }

    /**
     * Disbale charging
     */
    public void disableCharging() {
        execCmd(PerfConstants.DISABLEBATTERY);
    }

    /**
     * Get Battery stats
     */
    public String getBatteryStats() {
        return execCmd(PerfConstants.GETBATTERYCONSUMPTION.replace("${UID}", execCmd(PerfConstants.GETUIDOFSWIGGYV2.replace("${PACKAGE}",packageName))));
    }

    /**
     * Enable charging
     */
    public void enableCharging() {
        execCmd(PerfConstants.ENABLEBATTERY);
    }

    /**
     * Reset Battery
     */
    public void resetBatteryStats() {
        execCmd(PerfConstants.RESETBATTERYSTATS);
    }

    /**
     * get battery level
     *
     * @return
     */
    public static String getBatteryLevel() {
        return execCmd(PerfConstants.GETBATTERYLEVEL);
    }

    /**
     *
     * @param filePath
     * @return
     */
    public boolean checkIfFileExists(String filePath) {
        return new File(filePath).exists();
    }

    /**
     *
     * @return swiggy app version
     */
    public String getSwiggyAppVersion() {
        String app_version = execCmd(PerfConstants.GETSWIGGYAPPVERSION.replace("${PACKAGE}",packageName));
        System.out.println("--> App Version ::: "+app_version);
        return app_version;
    }

    /**
     *
     * @return Device Android Version
     */
    public String getDeviceAndroidVersion() {
        return execCmd(PerfConstants.GETANDROIDVERSION);
    }

    /**
     *
     * @return device serial number
     */
    public String getDeviceSerialNumber() {
        return execCmd(PerfConstants.GETDEVICESERIALNUMBER);
    }

    /**
     *
     * @return Device sdk version
     */
    public String getDeviceAndroidSDKVersion() {
        return execCmd(PerfConstants.GETDEVICEANDROIDSDKVERSION);
    }

    /**
     *
     * @return device brand
     */
    public String getDeviceBrand() {
        return execCmd(PerfConstants.GETDEVICEBRAND);
    }

    /**
     *
     * @return device manufacturer
     */
    public String getDeviceManufacturer() {
        return execCmd(PerfConstants.GETDEVICEMANUFACTURER);
    }

    /**
     *
     * @return device model
     */
    public String getDeviceModel() {
        return execCmd(PerfConstants.GETDEVICEMODEL);
    }

    private int setZeroIfEmpty(String value) {
        if (value.trim().isEmpty())
            return 0;
        else {
            try {
                return Integer.parseInt(value.trim());
            }catch(Exception e) {
                System.out.println("Faulty Value for getting Integer ---> "+ value);
                e.printStackTrace();
                return Integer.parseInt(value.trim());
            }
        }
    }

    private double setZeroIfEmptyDouble(String value) {
        //System.out.println("setZeroIfEmptyDouble===="+value);
        if (value.trim().isEmpty()) {
            System.out.println(" after setZeroIfEmptyDouble====" + value);
            return 0.0;
        }
        else {
            System.out.println(" non-empty double value ::: "+value);
            try {
                return Double.parseDouble(value.trim().replace("+", ""));
            }
            catch(Exception e) {
                System.out.println("Faulty Value for getting Double ---> "+ value);
                return 0.0;
            }
        }

    }

    private String writeStringAsFile(String content, String fileName) {
        // String completeFilePath = System.getPxroperty("user.home")+File.separator+"Desktop"+File.separator+"PerfTempData"+File.separator+fileName;
        File f = new File(fileName);
        PrintWriter pw = null;
        boolean result = false;
        try {
            result = f.exists() ? f.delete() : f.createNewFile();
            if (result) {
                pw = new PrintWriter(new FileOutputStream(f));
                pw.write(content);
                return f.getAbsolutePath();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pw.close();
        }
        return null;
    }

    public String createDirectoryToSaveHeapFiles(String appVersion, String testcase, String appType, String buildEnv) {
        File dir = new File(System.getProperty("user.home") + File.separator + "Desktop" + File.separator + "PerfTempData" + File.separator + appType + File.separator + buildEnv + File.separator + appVersion + "_" + testcase.replaceAll(" ", "_") + "_"
                + getCurrentTimeStamp());
        boolean result = (dir.exists()) == true || dir.mkdirs();
        if (result)
            return dir.getAbsolutePath();
        else
            return dir.getAbsolutePath();
    }

    private String getCurrentTimeStamp() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return sdf.format(timestamp).toString();
    }

    private HashMap getApkSize() {
        File file = new File(pathToApk);
        String size;
        HashMap<String, Double> hashMap = new HashMap<>();
        if(file.exists()){
            double kilobytes = ((file.length()) / 1024);
            double megabytes = (kilobytes / 1024);
            //System.out.println(":::APK SIZE in KB == "+kilobytes);
            System.out.println(":::APK SIZE in MB == "+megabytes);
            size = String.format("%.3f",megabytes);
            System.out.println(":::Final APK SIZE (roundoff to %.3D) === "+ size+ " MB");
            hashMap.put("apksize",setZeroIfEmptyDouble(size));
        }
        else
            System.out.println("----APK file not found----");
        return hashMap;
    }

    public static String identifyBuildEnvironment() {
        String[] getEnvName = packageName.trim().split("\\.");
        int len = getEnvName.length;
        System.out.println("Package Name ====> "+packageName);
        if (getEnvName[len-1].equalsIgnoreCase("prod"))
            return "PROD";
        else if (getEnvName[len-1].equalsIgnoreCase("uat"))
            return "UAT";
        else
            return "RELEASE";
    }

    public void startActivity(String packageName, String activityName) {
        String command = PerfConstants.STARTACTIVITY.replace("${PACKAGE}",packageName).replace("${ACTIVITY}",activityName);
        System.out.println("Start Activity Command ::: "+command);
        execCmd(command);
    }

}
