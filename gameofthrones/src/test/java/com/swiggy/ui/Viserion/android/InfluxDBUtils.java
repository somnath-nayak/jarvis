package com.swiggy.ui.Viserion.android;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class InfluxDBUtils {
    static String dbNamePerf = "viserion";
    //static String dbNameBenchMark = "benchmarking";
    //static DBOperations db = new DBOperations();
    static PerfUtils util = new PerfUtils();

    public void pushData(String dirName, String appVersion, String deviceModel, String testCaseDescription, String appType, String buildEnv)
    {
        BatchPoints batchPoints = getBatchPoints(dbNamePerf, appVersion, deviceModel, testCaseDescription, appType, buildEnv);
        Point pointCpu = Point.measurement("cpu")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .fields(util.parseInputFileAndReturnMapStrInt(dirName, appVersion, testCaseDescription, PerfConstants.CPU)).build();

        Point pointMemory = Point.measurement("memory")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .fields(util.parseInputFileAndReturnMapStrInt(dirName, appVersion, testCaseDescription ,PerfConstants.MEMORY)).build();

        Point pointBattery = Point.measurement("battery")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .fields(util.parseInputFileAndReturnMapStrInt(dirName, appVersion, testCaseDescription, PerfConstants.BATTERY)).build();

        Point pointNetwork = Point.measurement("network")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .fields(util.parseInputFileAndReturnMapStrInt(dirName, appVersion, testCaseDescription, PerfConstants.NETWORK)).build();

        //Debug
        System.out.println("pointCpu========> "+pointCpu.toString());
        System.out.println("pointMemory=====> "+pointMemory.toString());
        System.out.println("pointBattery====> "+pointBattery.toString());
        System.out.println("pointNetwork====> "+pointNetwork.toString());
        batchPoints.point(pointCpu);
        batchPoints.point(pointMemory);
        batchPoints.point(pointBattery);
        batchPoints.point(pointNetwork);
        try {
            InfluxDBSingleton.getInstance().getConnection().write(batchPoints);
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }

    public void pushApkSizeData(String dirName, String appVersion, String deviceModel, String testCaseDescription, String appType, String buildEnv) {
        BatchPoints batchPoints = getBatchPoints(dbNamePerf, appVersion, deviceModel, testCaseDescription, appType, buildEnv);
        Point pointApkSize =  Point.measurement("apksize")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .fields(util.parseInputFileAndReturnMapStrInt(dirName, appVersion, testCaseDescription, PerfConstants.APKSIZE)).build();
        batchPoints.point(pointApkSize);
        System.out.println("pointApkSize====="+ pointApkSize.toString());
        try {
            InfluxDBSingleton.getInstance().getConnection().write(batchPoints);
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void queryDatabase(String appVersion,String testCaseDescription,String paramater, String appType, String buildEnv){
        Reporter.log("appversion---> "+appVersion,true);
        Reporter.log("testcasedesc----->"+testCaseDescription,true);
        Reporter.log("appType -----> "+appType, true);
        Reporter.log("buildEnv -----> "+buildEnv, true);

        if(paramater.equalsIgnoreCase("memory")) {
            String measurement_name = paramater.toLowerCase();
            //dalvic heap
            String queryStr=formatQuery(PerfConstants.SELECTMAX,"Dalvic Heap",measurement_name,"appversion="+appVersion);
            runDatabase(queryStr);
            //native heap
            queryStr=formatQuery(PerfConstants.SELECTMAX,"Native Heap",measurement_name,"appversion="+appVersion);
            runDatabase(queryStr);
        }
        if(paramater.equalsIgnoreCase("battery")) {
        }
        if(paramater.equalsIgnoreCase("cpu")){
        }

    }

    public String formatQuery(String Constquery,String field_key,String measurement_name,String stuff){
        String formattedQueryStr=Constquery.replace("<field_key>",field_key).replace("<measurement_name>",measurement_name)
                .replace("<stuff>",stuff);
        return formattedQueryStr;
    }

    public void runDatabase(String queryStr){
        Query query = new Query(queryStr, dbNamePerf);
        QueryResult queryResult =  InfluxDBSingleton.getInstance().getConnection().query(query);
        // iterate the results and print details
        for (QueryResult.Result result : queryResult.getResults()) {
            // print details of the entire result
            System.out.println(result.toString());
            // iterate the series within the result
            for (QueryResult.Series series : result.getSeries()) {
                System.out.println("series.getName() = " + series.getName());
                System.out.println("series.getColumns() = " + series.getColumns());
                System.out.println("series.getValues() = " + series.getValues());
                System.out.println("series.getTags() = " + series.getTags());
            }
        }
    }

    public BatchPoints getBatchPoints(String dbName, String version, String deviceModel, String description, String appType, String buildEnv){
        BatchPoints batchPoints = BatchPoints
                .database(dbName)
                .tag("appversion", version)
                .tag("deviceModel", deviceModel)
                .tag("description", description)
                .tag("appType", appType)
                .tag("buildEnv", buildEnv)
                .retentionPolicy("autogen")
                .consistency(InfluxDB.ConsistencyLevel.ALL)
                .build();
        return batchPoints;
    }

    public BatchPoints getBatchPoints(String dbName, String version, String deviceModel, String description, String percentile, String appType, String buildEnv){
        BatchPoints batchPoints = BatchPoints
                .database(dbName)
                .tag("appversion", version)
                .tag("deviceModel", deviceModel)
                .tag("description", description)
                .tag("percentile", percentile)
                .tag("appType", appType)
                .tag("buildEnv", buildEnv)
                .retentionPolicy("autogen")
                .consistency(InfluxDB.ConsistencyLevel.ALL)
                .build();
        return batchPoints;
    }

}
