package com.swiggy.ui.Viserion.android;

import java.util.ArrayList;

public class PerfConstants {

    // public static final String perfuser = AppiumSteps.userConstants;

    // public static final String DEVICEID = MultipleDevice.getDevices().get(perfuser).getProperty(Device.Property.UDID.toString());

    public static final String NA = "N/A";
    public static final String COMMA = ",";
    public static final String LINE_END = "\r\n";
    public static final String COLON = ":";

    public static final String BATTERY = "BATTERY";
    public static final String LATENCY = "LATENCY";
    public static final String NETWORK = "NETWORK";
    public static final String CPU = "CPU";
    public static final String MEMORY = "MEMORY";
    public static final String APKSIZE = "APKSIZE";

    public static final int OSTYPE_ANDROID = 1;
    public static final int OSTYPE_IOS = 2;

    //public static final String perfuser = AppiumSteps.userConstants;
    //public static final String DEVICEID = 

    //MEM INFO
    public static final String M_GETMEMINFO = "adb shell dumpsys meminfo ${PACKAGE}";
    public static final String M_MEMLINUX_COMMAND = "grep -m 1 \"${TYPE}\" ${PATH} | awk -F'[[:space:]][[:space:]][[:space:]]*' '{print $3}'";
    public static final String M_NATIVEHEAP = "Native Heap";
    public static final String M_DALVIKHEAP = "Dalvik Heap";
    public static final String M_DALVIKOTHER = "Dalvik Other";
    public static final String M_STACK = "Stack";
    public static final String M_ASHMEN = "Ashmem";
    public static final String M_OTHERDEV = "Other dev";
    public static final String M_SOMMAP = ".so mmap";
    public static final String M_APKMMAP = ".apk mmap";
    public static final String M_TTFMMAP = ".ttf mmap";
    public static final String M_DEXMMAP = ".dex mmap";
    public static final String M_OATMMAP = ".oat mmap";
    public static final String M_ARTMMAP = ".art mmap";
    public static final String M_OTHERMMAP = "Other mmap";
    public static final String M_UNKNOWN = "Unknown";
    public static final String M_TOTAL = "TOTAL";
    public static final String[] M_ARRAY = {
            M_NATIVEHEAP, M_DALVIKHEAP,M_DALVIKOTHER, M_STACK,
            M_ASHMEN, M_OTHERDEV, M_SOMMAP, M_APKMMAP,
            M_TTFMMAP, M_DEXMMAP, M_OATMMAP, M_ARTMMAP,
            M_OTHERMMAP, M_UNKNOWN, M_TOTAL
    };

    //CPU INFO
    public static final String C_GETCPUINFO = "adb shell dumpsys cpuinfo";
    public static final String C_PROCESS_ID = "grep \"${PACKAGE}:\"  ${PATH} | awk '{print $2}' | awk -F\"/\" '{print $1}'";
    //CP = CPU per process, output format = Total : User : kernel : Faults Minor : Faults Major
    public static final String CP_CPU_STATS = "grep \"${PACKAGE}:\"  ${PATH} | awk '{print $1,\":\",$3,\":\",$6,\":\",$10,\":\",$12}'";
    //CT = CPU Total, Output Format = Total : User : Kernel : IO : Softirq
    public static final String CT_CPU_STATS = "grep \"TOTAL\" ${PATH} | awk '{print $1,\":\",$3,\":\",$6,\":\",$9,\":\",$12}'";
    public static final String[] CP_ARRAY = {
            "p_total",
            "p_user",
            "p_kernel",
            "p_faults_minor",
            "p_faults_major"
    };

    public static final String[] CT_ARRAY = {
            "t_total",
            "t_user",
            "t_kernel",
            "t_io",
            "t_softirq"
    };

    //Battery commands
    public static final String GETBATTERYLEVEL = "adb shell dumpsys battery | grep -m 1 \"level\" | awk -F: '{print $2}'";
    public static final String DISABLEBATTERY = "adb shell dumpsys battery set usb 0";
    public static final String ENABLEBATTERY = "adb shell dumpsys battery reset";
    public static final String RESETBATTERYSTATS = "adb shell dumpsys batterystats --reset";

    //Get UID of a package
    public static final String GETUIDOFAPACKAGE = "adb shell dumpsys package \"${PACKAGE}\" | grep -m 1 \"uid\" | awk '{print $1}' | awk -F\"=\" '{print $2}'";

    //Get UID of SWIGGY
    public static final String GETUIDOFSWIGGY = "adb shell dumpsys batterystats | grep -i ${PACKAGE} | grep -m 1 -i \"proc\" | awk -F\"=\" '{print $2}' | awk -F\":\" '{print $1}'";
    public static final String GETUIDOFSWIGGYV2 = "adb shell ps | grep -i \"${PACKAGE}\" | awk '{print $1}' | sed  's/[_]//g'";

    //Get Battery consumption in mAh
    public static final String GETBATTERYCONSUMPTION = "adb shell dumpsys batterystats ${PACKAGE} | grep -m 1 -i \"${UID}\" | awk -F: '{print $2}' | awk -F\" \" '{print $1}'";

    public static final String BATTERYCONSUMPTIONBREAKUP = "adb shell dumpsys batterystats | grep -m 1 -i \"uid ${UID}\" | cut -d\"(\" -f2 | cut -d\")\" -f1";

    //Get SWIGGY App Version
    public static final String GETSWIGGYAPPVERSION = "adb shell dumpsys package \"${PACKAGE}\" | grep versionName | awk -F \"=\" '{print $2}'";

    //Get Android version
    public static final String GETANDROIDVERSION = "adb shell getprop ro.build.version.release";

    //Get Device Serial Number
    public static final String GETDEVICESERIALNUMBER = "adb shell getprop ro.boot.serialno";

    //Get Android SDK version
    public static final String GETDEVICEANDROIDSDKVERSION = "adb shell getprop ro.build.version.sdk";

    //Get Device brand
    public static final String GETDEVICEBRAND = "adb shell getprop ro.product.brand";

    //Get Device manufacturer
    public static final String GETDEVICEMANUFACTURER = "adb shell getprop ro.product.manufacturer";

    //Get Device model
    public static final String GETDEVICEMODEL = "adb  shell getprop ro.product.model";

    //Select Mean
    public static String SELECTMEAN="SELECT MEAN(<field_key>) FROM <measurement_name> WHERE <stuff>";

    //Select Median
    public static String SELECTMEDIAN="SELECT MEDIAN(<field_key>) FROM <measurement_name> WHERE <stuff>";

    //Select Mode
    public static String SELECTMODE="SELECT MEAN(<field_key>) FROM <measurement_name> WHERE <stuff>";

    //select Min
    public static String SELECTMIN="SELECT MIN(<field_key>) FROM <measurement_name> WHERE <stuff>";

    //select Max
    public static String SELECTMAX="SELECT MAX(<field_key>) FROM <measurement_name> WHERE <stuff>";

    //Select Percentile
    public static  String SELECTPERCENTILE="SELECT PERCENTILE(<field_key>, <N>) FROM <measurement_name> WHERE <stuff>";

    //Start Another Activity through ADB
    public static String STARTACTIVITY="adb shell am start -n ${PACKAGE}/${ACTIVITY}";

    //Network Related Stats
    //public static String GETSWIGGYNETWORKSTATS = "adb shell cat /proc/net/xt_qtaguid/stats | grep ${UID}";
    public static String GETSWIGGYNETWORKSTATS_FGBG = "adb shell cat /proc/net/xt_qtaguid/stats | grep ${UID} | awk '{print $5, $6, $8}'";
    public static  String[] NTWK_ARRAY = {
            "b_rx_mb",
            "b_tx_mb",
            "f_rx_mb",
            "f_tx_mb",
            "total_rx_mb",
            "total_tx_mb"
    };
}
