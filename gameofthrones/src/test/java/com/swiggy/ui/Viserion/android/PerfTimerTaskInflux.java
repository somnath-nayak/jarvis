package com.swiggy.ui.Viserion.android;

public class PerfTimerTaskInflux implements Runnable {
    String appVersion;
    String deviceModel;
    String testDescription;
    String dirName;
    String appType;
    String buildEnv;
    InfluxDBUtils influx = new InfluxDBUtils();

    public PerfTimerTaskInflux(String dirName, String appVersion, String deviceModel, String testDescription, String appType, String buildEnv) {
        this.dirName = dirName;
        this.appVersion = appVersion;
        this.testDescription = testDescription;
        this.deviceModel = deviceModel;
        this.appType = appType;
        this.buildEnv = buildEnv;
    }

    @Override
    public void run() {
        // TODO Auto-generated method stub
        try {
            influx.pushData(dirName, appVersion, deviceModel, testDescription, appType, buildEnv);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}