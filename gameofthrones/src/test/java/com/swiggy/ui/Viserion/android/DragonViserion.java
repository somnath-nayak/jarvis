package com.swiggy.ui.Viserion.android;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class DragonViserion {

    ScheduledThreadPoolExecutor schedulePool;
    PerfUtils perfUtility = new PerfUtils();
    int TestID = 1;

    public void fire(String testcase, String appType) throws InterruptedException {
        resetBatteryStats();
        String buildEnv = PerfUtils.identifyBuildEnvironment();
        System.out.println(" --- BUILD ENVIRONMENT OF APK --- " + buildEnv);
        String deviceModel = perfUtility.getDeviceModel();
        String appVersion = perfUtility.getSwiggyAppVersion();
        schedulePool = new ScheduledThreadPoolExecutor(2);
        String createAndGetDir = perfUtility.createDirectoryToSaveHeapFiles(appVersion, testcase, appType, buildEnv);
        PerfTimerTaskInflux perfTimerTaskInflux=new PerfTimerTaskInflux(createAndGetDir, appVersion, deviceModel, testcase,appType, buildEnv);
        Thread thread = new Thread(perfTimerTaskInflux);
        System.out.println("::Viserion Fires:: TestID --->" + TestID);
        //if (TestID == 0)
        thread.start();
        /*else {
            thread.sleep(30000);
            thread.start();
        }*/
        ++TestID;
        schedulePool.scheduleWithFixedDelay(new PerfTimerTaskInflux(createAndGetDir, appVersion, deviceModel, testcase, appType, buildEnv), 0, 500, TimeUnit.MILLISECONDS);
    }

    public void pushApkSize(String appType) {
        InfluxDBUtils influx = new InfluxDBUtils();
        String buildEnv = PerfUtils.identifyBuildEnvironment();
        System.out.println(" --- BUILD ENVIRONMENT OF APK --- " + buildEnv);
        String testcase = "APK Size in MB";
        String deviceModel = perfUtility.getDeviceModel();
        String appVersion = perfUtility.getSwiggyAppVersion();
        String createAndGetDir = perfUtility.createDirectoryToSaveHeapFiles(appVersion, testcase, appType,  buildEnv);
        try {
            influx.pushApkSizeData(createAndGetDir,appVersion, deviceModel, testcase, appType,  buildEnv);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public void disableCharging() {
        perfUtility.disableCharging();
        System.out.println(" ---- Disabled Charging ----");
    }

    public void enableCharging() {
        perfUtility.enableCharging();
        System.out.println(" ---- Enabled Charging ----");
    }

    public void resetBatteryStats() {
        perfUtility.resetBatteryStats();
        System.out.println(" ---- Reset BatteryStats ----");
    }

    public void startActivity(String packageName, String activityName) {
        perfUtility.startActivity(packageName,activityName);
    }

    public String execShellCommand(String command) {
        String output = PerfUtils.execCmd(command);
        return output;
    }
}
