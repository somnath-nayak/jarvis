package com.swiggy.ui.Viserion.android;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.concurrent.TimeUnit;

public class InfluxDBSingleton {

    private static InfluxDBSingleton dbInstance;
    private String url = "http://qa-u-gotinflux.swiggyops.de:8086";
    private String dbName = "viserion";
    private InfluxDB con;


    private InfluxDBSingleton() {
        // private constructor //
    }

    public static InfluxDBSingleton getInstance() {
        if (dbInstance == null) {
            dbInstance = new InfluxDBSingleton();
        }
        return dbInstance;
    }

    public InfluxDB getConnection() {
        if (con == null) {
            con = InfluxDBFactory.connect(url, "root", "root");
            con.enableBatch(2000, 30000, TimeUnit.MILLISECONDS);
            System.out.println("ping======="+con.ping());
        }
        return con;
    }
}