package com.swiggy.ui.utils;

import framework.gameofthrones.Aegon.*;
import io.appium.java_client.AppiumDriver;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import framework.gameofthrones.Aegon.ElementLocator;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import framework.gameofthrones.Aegon.LocatingElementHandler;
import framework.gameofthrones.Aegon.LocatingElementListHandler;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import java.util.List;
import java.util.NoSuchElementException;

public class WebUtil {
	
    public void initElements(WebDriver driverRef, Object page, InitializeUI init)
    {

        Class<?> proxyIn = page.getClass();
        Object value;
        InvocationHandler handler ;
        System.out.println("ProxyIn"+proxyIn);
        Field[] fields=proxyIn.getDeclaredFields();
        for (Field field : fields) {

            System.out.println("" + field.getName() + "" + field.getDeclaredAnnotations() + " = = " + field.getType());
            if(field.getType().toString().contains("interface")){
                if (IElement.class.isAssignableFrom(field.getType())) {
                    handler = new LocatingElementHandler(new ElementLocator(driverRef, field, init));
                    value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{IElement.class}, handler);
                } else {
                    handler = new LocatingElementListHandler(new ElementLocator(driverRef, field, init));
                    value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{List.class}, handler);
                }

                field.setAccessible(true);
                try {
                    field.set(page, value);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }


    }
	
		private static Logger log = Logger.getLogger(WebUtil.class);
		private static long waitTime = 10;
		private static WebDriver driver;
		public static InitializeUI init;

		public WebUtil(){}

		public WebUtil(InitializeUI init) {
			driver = init.getWebdriver();
            WebUtil.init =init;
		}
		
		public static WebElement waitUntilClickable(WebDriver driver, By by) {
			WebElement element = null;
			try {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			element = wait.until(ExpectedConditions.elementToBeClickable(by));
			} catch(Exception e) {
				e.printStackTrace();
			}
			return element;
		}
		
		public void click(WebDriver driver, By by) {
			waitUntilClickable(driver, by);
			driver.findElement(by).click();
		}

		public void click(WebDriver driver, WebElement element) {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.elementToBeClickable(element));
		}
		
		public WebElement waitForVisibility(WebDriver driver, By by)  {
			WebElement element = null;
			try {
			WebDriverWait wait = new WebDriverWait(driver, waitTime);
			element = wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			} catch(Exception e) {
				e.printStackTrace();
			}
			return element;
		}
		
		public void enterText(WebDriver driver, By by, String text) {
			try {
				waitForVisibility(driver, by);
				driver.findElement(by).sendKeys(text);
			} catch(NoSuchElementException e) { 
				log.info("Element Not Found Exception");
			}
			catch(Exception e) {
				log.info("Exception" + e.getStackTrace());
			}
		}
		
		public boolean isVisible(WebDriver driver, By by) {
			boolean visible = false;
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.presenceOfElementLocated(by));
				visible = driver.findElement(by).isDisplayed();
			} catch(Exception e) {
				e.printStackTrace();
			}
			return visible;
		}

		public boolean isVisible(WebDriver driver, WebElement element) {
			boolean visible = false;
			try {
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.visibilityOf(element));
				visible = element.isDisplayed();
			} catch(Exception e) {
				e.printStackTrace();
			}
			return visible;
		}
		
		public void scrollTo(WebDriver driver, By by) {
			WebElement element = driver.findElement(by);
			if(!isVisible(driver, by))
			((JavascriptExecutor)driver).executeScript("arguments[0].scrollIntoView();", element);
		}
		
		public void acceptAlert(WebDriver driver) {
			try{ 
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.accept();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		public void dismissAlert(WebDriver driver) {
			try{
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.alertIsPresent());
			Alert alert = driver.switchTo().alert();
			alert.dismiss();
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		public boolean isAlertPresent(WebDriver driver, By by) {
			boolean present = false;
			try{
				WebDriverWait wait = new WebDriverWait(driver, 10);
				wait.until(ExpectedConditions.alertIsPresent());
				present = true;
			} catch(Exception e) {
				present = false;
			}
			return present;
		}

		public void refresh(WebDriver driver) {
			driver.navigate().refresh();
		}

		public void mouseHover(WebDriver driver, WebElement element) {
		    try {
                if (isVisible(driver, element)) {
                    Actions action = new Actions(driver);
                    action.moveToElement(element).contextClick();
                    Thread.sleep(2000);
                }
            } catch(Exception e) {
		        e.printStackTrace();
            }
		}

		public String getText(WebDriver driver, By by) {
			System.out.println("Preptime dynamic slot xpath of fetchSlotPreptime() - getText() :"+ driver.findElement(by).getText());			
			return driver.findElement(by).getText();
		}
		
		public boolean isEnabled(WebDriver driver, By by) {
			return driver.findElement(by).isEnabled();
		}

//	public static void initElements(WebDriver driverRef, Object page, InitializeUI init)
//	{
//
//		Class<?> proxyIn = page.getClass();
//		Object value;
//		InvocationHandler handler ;
//		System.out.println("ProxyIn"+proxyIn);
//		Field[] fields=proxyIn.getDeclaredFields();
//		for (Field field : fields) {
//
//			System.out.println("" + field.getName() + "" + field.getDeclaredAnnotations() + " = = " + field.getType());
//			if(field.getType().toString().contains("interface")){
//				if (IElement.class.isAssignableFrom(field.getType())) {
//					handler = new LocatingElementHandler(new ElementLocator(driverRef, field, init));
//					value = (IElement) java.lang.reflect.Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{IElement.class}, handler);
//				} else {
//					handler = new LocatingElementListHandler(new ElementLocator(driverRef, field, init));
//					value = (List<IElement>) java.lang.reflect.Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{List.class}, handler);
//				}
//
//				field.setAccessible(true);
//				try {
//					field.set(page, value);
//				} catch (IllegalArgumentException e) {
//					e.printStackTrace();
//				} catch (IllegalAccessException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//
//
//	}

	public static void sleep(){
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void sleep(int i){
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
		
}
