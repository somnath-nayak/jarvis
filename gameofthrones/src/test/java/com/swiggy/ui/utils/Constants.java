package com.swiggy.ui.utils;

import java.text.MessageFormat;
import java.util.HashMap;

public class
Constants {

    //LatLong

    public static String DISHES_NAME = "punjabi";
    public static String MENU_FILTER = "Main Course";
    public static String RESTAURANT_NAME = "";

    public static String username1 = "";
    public static String password1 = "";

    //Flush db, updatemsgs
    public static String wp_options = "all_options";
    public static String updatemsg = "Looks like your version of the app is out of date. Enhance your experience and download the update from the App Store.";
    //Hygiene Rating
    public static String Hygiene_txt = "Audit Partner";
    //POP ff
    public static String txt_popfiltertitle = "Single Serve Meals. Free Delivery. No Extra Charges.";
    //fssai
    public static String fssaiimage="fssai_final_edss9i";
    public static String fssailicenceno="License No. 11215334000294";

    public static final String RELEVANCE_SORT = "relevance";
    public static final String COSTOFTWO_SORT = "costoftwo";
    public static final String DELIVERYTIME_SORT = "deliveryTime";
    public static final String RATING_SORT = "rating";
    public static final String APP_VERSION_CODE = "version-code";
    public static final String USER_AGENT = "User-Agent";
    public static final String ANDROID_USER_AGENT = "Swiggy-Android";
    public static final String IOS_USER_AGENT = "Swiggy-IOS";
    public static final String ERROR_INCREASEITEMCOUNT = "You’ve selected the maximum allowed number for this item";




    //Location IBC
    public static Double lat1 = 12.932709778627975;
    public static Double lng1 = 77.60356619954109;
    //Location Bhudwal
    public static Double lat2 = 27.834105;
    public static Double lng2 = 76.171977;
    //Location Kormangala
    public static Double lat3 = 19.229388800000002;
    public static Double lng3 = 72.8569977;

    //Anna university chennai
    public static Double lat4 = 13.008305355814347;
    public static Double lng4 = 80.23347347974777;

    public static Double lat_nonServicable = 25.4487677;
    public static Double lng_nonServicable = 82.8567959;

    public static Double lat_marathahalli = 12.956667;
    public static Double lng_marathahalli = 77.701213;

    public static Double lat5 =12.932815;
    public static Double lng5=77.603578;

    public static String userName = "9036976038";
    public static String password = "huawei123";


    public static String userName2 = "9108457497";
    public static String password2 = "qwerty";


    public static String androidAppVersion = "300";
    public static String dominosRestaurantName = "Domino's Pizza";
    public static String mealCancelPopMessage = "Going back will discard your selection.";
    public static String PAYMENTPAGEHEADER = "PAYMENT";
    public static String REPLACECARTITME_HEADER_IOS = "Items already in cart";
    public static String REPLACECARTITEM_SUB_IOS = "your cart contains items from a different restaurant. would you like to reset your cart before browsing this restaurant?";
    public static String REPLACECARTITME_HEADER_ANDROID = "Replace cart item?";
    public static String REPLACECARTITEM_SUB_ANDROID = "Your cart contains dishes from {0}. Do you want to discard the selection and add dishes from {1}?";
    public static HashMap<String, String> hm = createMap();

    private static HashMap<String, String> createMap() {
        HashMap<String, String> myMap = new HashMap<String, String>();
        myMap.put("REPLACECARTITME_HEADER_IOS", REPLACECARTITME_HEADER_IOS);
        myMap.put("REPLACECARTITEM_SUB_IOS", REPLACECARTITEM_SUB_IOS);
        myMap.put("REPLACECARTITME_HEADER_ANDROID", REPLACECARTITME_HEADER_ANDROID);
        myMap.put("REPLACECARTITEM_SUB_ANDROID", REPLACECARTITEM_SUB_ANDROID);
        return myMap;
    }


    public static String PREORDER_TOOLTIP_TITLE="tried order scheduling?";
    public static String PREORDER_TOOLTIP_MESSAGE="plan your meals in advance & get it delivered at your convenience.";
    public static String PREORDER_SELECTTIMER_HEADER="When do you want your delivery?";
    public static String PREORDER_ONBOARDING="Get your food delivered NOW or schedule it for later";
    public static String EMPTYCART_TITLE="GOOD FOOD IS ALWAYS COOKING";
    public static String LOGOUT_MESSAGE="Are you sure you want to logout?";
    public static String LOCATION_NOT_SERVICABLE="WE ARE NOT HERE YET!";
    public static String LOCATION_NOT_SERVICABLE_IOS="WISH WE WERE HERE!";

    public static String SWIGGYPOP_CLEARCART_DIALOG_TITLE="Remove item from cart";
    public static String SWIGGYPOP_CLEARCART_DIALOG_MESSAGE="Going back will clear your cart and you will have to start afresh.";
    public static String MULTIPLE_CUST_REMOVEITEM_DIALOG_TITLE="Remove item from cart";
    public static String MULTIPLE_CUST_REMOVEITEM_DIALOG_MESSAGE="This item has multiple customizations added. Proceed to cart to remove item?";
    public static String SWIGGYPOP_PRESSBACK_TITTLE="Cart will be cleared";
    public static String SWIGGYPOP_PRESSBACK_MESSAGE="Going back will clear your cart and you will have to start afresh.";
    public static String STF_AUTH_TOKEN="216ebe43ffaf40a9bec56e91710ce7b1076eaf62cec343bd98aceb27496e1325";
    public static String PAYMENT_SCREEN_HEADER="PAYMENT OPTIONS";


    //New Filter Offers&More

    public static  final String SHOW_RESTAURANT_WITH_30_MINS_OR_FREE="30 Mins or Free";
    public static final String SHOW_RESTAURANT_WITH_MY_FAVOURITES="My Favourites";
    public static final String SHOW_RESTAURANT_WITH_OFFERS="Offers";
    public static final String SHOW_RESTAURANT_WITH_PURE_VEG="Pure Veg";




    public static void main(String[] args)
    {
        System.out.println(MessageFormat.format(Constants.REPLACECARTITEM_SUB_ANDROID,"Hello","Test").toLowerCase());


}}
