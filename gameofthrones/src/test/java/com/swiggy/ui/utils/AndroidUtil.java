package com.swiggy.ui.utils;
import java.io.InputStreamReader;

import framework.gameofthrones.Aegon.ElementLocator;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.apache.log4j.Logger;
import org.aspectj.weaver.ast.And;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.SubnodeConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import framework.gameofthrones.Aegon.LocatingElementHandler;
import framework.gameofthrones.Aegon.LocatingElementListHandler;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
public class AndroidUtil {
	private AppiumDriver<MobileElement> driver;
	private static final String NATIVE_APP = "NATIVE_APP";
	private static Logger log = Logger.getLogger(AndroidUtil.class);
	public static InitializeUI inti;
	public enum Direction {
		LEFT,
		RIGHT,
		UP,
		DOWN
	}


    public AndroidUtil() {
		
	}
	public AndroidUtil(InitializeUI init) {

		this.driver = init.getAppiumDriver();
		inti=init;
	}
	
	public void click(By locator) {
		//ElementLocator e=new ElementLocator(driver,locator);
		//e.findElement(15,6);
		if(isElementPresent(locator))
			driver.findElement(locator).click();

		else {
			log.info("Element Not Found");
			throw new NoSuchElementException("Element Not Found" + locator);
		}
	}
	
	public void click(MobileElement locator) {


		/*if(isElementPresent(locator))
			locator.click();

		else {
			log.info("Element Not Found");
			throw new NoSuchElementException("Element Not Found" + locator);
		}*/
	}

	public void clickonList(By locator,int position){
		List<MobileElement> llist=driver.findElements(locator);
		llist.get(position).click();
	}
	public void clickBackButton() {
		driver.navigate().back();
	}
	
	public boolean isElementPresent(By locator) {
		if(locator == null)
			throw new IllegalArgumentException("Locator cannot be Null");
		
		try{
			driver.findElement(locator);
			log.info("Element Found");
			return true;
		} catch(NoSuchElementException e) {
			log.info("Element Not Found" + locator);
			return false;
		}
	}
	
	public boolean isElementPresent(MobileElement locator){
		if(locator == null) throw new IllegalArgumentException("Locator cannot be Null");
		try{
			WebDriverWait wait = new WebDriverWait(driver,10);
			//wait.until(ExpectedConditions.visibilityOf(locator));
			return locator.isDisplayed();
		} catch(Exception e) {
			log.info("Element Not Found" + locator);
			return false;
		}
	}
	
	public boolean isElementClickable(AppiumDriver<MobileElement> driver, MobileElement locator) {
//		fluentWait.until(ExpectedConditions.elementToBeClickable(locator));
		return locator.isDisplayed() && locator.isEnabled();
	}
	
	public String getText(MobileElement locator) {
		if(locator == null) 
			throw new IllegalArgumentException("Locator cannot be null");
		log.info("getText Locator text is " + locator.getText());
		return locator.getText();
	}


	
	public String getText(By locator) {
		validLocator(locator);
		log.info("getText Locator text is " + driver.findElement(locator).getText());
		return driver.findElement(locator).getText();
	}
	

	public void clearTextField(AppiumDriver<MobileElement> driver,MobileElement locator) {
		while(!locator.getText().isEmpty()) {
			pressDeleteKey(driver);
		}
	}
	
	public void clearTextField(AppiumDriver<MobileElement> driver,By locator) {
		log.info("Clearing the text in locator "+ locator);
		int counter = 100;
		while(!driver.findElement(locator).getText().isEmpty() && counter > 0) {
			pressDeleteKey(driver);
			counter--;
		}
	}
	
	public void clearTextFieldByCount(AppiumDriver<MobileElement> driver, MobileElement locator,int count) {
		log.info("Clearing the text in locator "+ locator);
		while(count-- > 0) {
			pressDeleteKey(driver);
		}
	}
	
	public  void clearTextFieldByCount(AppiumDriver<MobileElement> driver, By locator,int count) {
		log.info("Clearing the text in locator "+ locator);
		while(count-- > 0) {
			pressDeleteKey(driver);
		}
	}
	
	public void validLocator(By locator) {
		if(locator == null)
			throw new IllegalArgumentException("Locator cannot be Null");
	}
	
	
	public void pressDeleteKey(AppiumDriver<MobileElement> driver) {
		((AndroidDriver<MobileElement>)driver).pressKeyCode(AndroidKeyCode.DEL);
	}
	
	public  void pressEnterKey(AppiumDriver<MobileElement> driver) {
		((AndroidDriver<MobileElement>)driver).pressKeyCode(AndroidKeyCode.ENTER);
	}
	
	
	
	public void waitForElementPresent(AppiumDriver<MobileElement> driver, By locator) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//locator = (By) wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
	}
	
	public boolean waitForElementPresent(AppiumDriver<MobileElement> driver, MobileElement locator) {
		try {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//locator = (MobileElement) wait.until(ExpectedConditions.visibilityOf(locator));
		return true;
		} catch(Exception e) {
			log.info("Element Not Found even after waiting for some time" + locator);
			return false;
		}
	}
	
		
	public void sendKeys(AppiumDriver<MobileElement> driver, By locator, String text) {
		if(isElementPresent(locator)) {
			driver.findElement(locator).sendKeys(text);
		} else {
			log.info("Element Not Found" + locator);
		}
	}
	
	public void sendKeys(MobileElement locator, String text) {
		if(isElementPresent(locator)) {
			locator.sendKeys(text);
		} 
	}
	
	public String getAlertText() {
		try {
			Alert alert = this.driver.switchTo().alert();
			return alert.getText();
		} catch(NoAlertPresentException e) {
			throw new NoAlertPresentException();
		}
	}
	
	public void acceptAlert() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().accept();
	}
	
	public void dismissAlert() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//wait.until(ExpectedConditions.alertIsPresent());
		driver.switchTo().alert().dismiss();
	}
	
	public void openNotifications() {
		((AndroidDriver<MobileElement>)driver).openNotifications();
	}
	
	public void selectFromList(By element, String text) {
		try{
		List<MobileElement> list = driver.findElements(element);
		for(MobileElement e: list) {
			if(e.getText().equalsIgnoreCase(text))
				this.click(element);
		}
		} catch(Exception e){
			log.info(e.getMessage());
		}
	}
	
	public void swipe(AppiumDriver<MobileElement> driver, Direction direction) {
		driver.context(NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int startx = (int) (size.width * 0.7);
		int endx = (int) (size.width * 0.3);
		int starty = size.height / 2;
		switch(direction){
		case LEFT: driver.swipe(startx, starty, endx, starty, 2000);
					break;
		case RIGHT : driver.swipe(endx, starty, startx, starty, 2000);
					break;
		default : log.info("Invalid Direction for Swipe");
		}
	}
	
	public void scroll(Direction direction) {
		try {
		driver.context(NATIVE_APP);
		Dimension size = driver.manage().window().getSize();
		int starty = (int) (size.height * 0.7);
		int endy = (int) (size.height * 0.3);
		int startx = size.width / 2;
		switch(direction) {
		case UP : driver.swipe(startx, starty, startx, endy, 2000);
					break;
		case DOWN : driver.swipe(startx, endy, startx, starty, 2000);
					break;
		default : log.info("Invalid Direction for Scroll");
		}
		} catch(Exception e) {
			log.info(e.getMessage());
		}
	}
	
	public void scrollToElement(AppiumDriver<MobileElement> driver, Direction direction, MobileElement element) {
		try {
			driver.context(NATIVE_APP);
			Dimension size = driver.manage().window().getSize();
			int starty = (int) (size.height * 0.7);
			int endy = (int) (size.height * 0.3);
			int startx = size.width / 2;
			int counter = 0;
			switch(direction) {
			case UP : 	while(counter < 5 && !this.isElementPresent(element)) {
						driver.swipe(startx, starty, startx, endy, 2000);
						counter++;
						}
						break;
			case DOWN : while(counter < 5 && !this.isElementPresent(element)) {
						driver.swipe(startx, endy, startx, starty, 2000);
						counter++;
						}
						break;
			default : log.info("Invalid Direction for Scroll");
			}
			if(!this.isElementPresent(element))
			log.info("Element Not Found : "+ element);
			} catch(Exception e) {
				log.info(e.getMessage());
			}
	}


	
	
	/*public void selectElement(By by, String text) {
		List<MobileElement> list = driver.findElements(by);
		for(MobileElement m : list) {
			if(m.getText().equalsIgnoreCase(text)) {
				this.click(driver, m);
				break;
			}
		}
	}*/

	public void clickHomeButtonOfDevice(AppiumDriver<MobileElement> driver) {
		driver.closeApp();
	}

	public void clickOnXY(int x, int y) {
		TouchAction touchAction = new TouchAction(driver);
		touchAction.tap(x, y).perform();
	}
	public static void initElements(AppiumDriver driverRef, Object page,InitializeUI init)
	{

		Class<?> proxyIn = page.getClass();
		Object value;
		InvocationHandler handler ;
		System.out.println("ProxyIn"+proxyIn);
		Field[] fields=proxyIn.getDeclaredFields();
		for (Field field : fields) {

			System.out.println("" + field.getName() + "" + field.getDeclaredAnnotations() + " = = " + field.getType());
			if(field.getType().toString().contains("interface")){
				if (IElement.class.isAssignableFrom(field.getType())) {
					handler = new LocatingElementHandler(new ElementLocator(driverRef, field, init));
					value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{IElement.class}, handler);
				} else {
					handler = new LocatingElementListHandler(new ElementLocator(driverRef, field, init));
					value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{List.class}, handler);
				}

				field.setAccessible(true);
				try {
					field.set(page, value);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}


	}

	public static void sleep(){
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static void sleep(int time){
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public static String runShellCommand(String command) {

		String s = null;
		StringBuilder sb = new StringBuilder();
		try {

			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			String sep=File.separator;
			Process p = Runtime.getRuntime().exec(System.getenv("ANDROID_TOOLS")+sep+"platform-tools"+sep+command);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(
					p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			System.out.println("Here is the standard output of the command:\n"
					+ command + "\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
				sb.append(s);
			}

			System.out
					.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

		} catch (Exception e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}
		return sb.toString();
	}

	public static String runShellCommandios(String command) {

		String s = null;
		StringBuilder sb = new StringBuilder();

		try {

			// run the Unix "ps -ef" command
			// using the Runtime exec method:
			System.out.println(command);
			Process p = Runtime.getRuntime().exec("xcrun simctl openurl booted "+command);
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

			System.out.println("Here is the standard output of the command:\n" + command + "\n");
			while ((s = stdInput.readLine()) != null) {
				System.out.println(s);
				sb.append(s);
			}

			System.out.println("Here is the standard error of the command (if any):\n");
			while ((s = stdError.readLine()) != null) {
				System.out.println(s);
			}

		} catch (Exception e) {
			System.out.println("exception happened - here's what I know: ");
			e.printStackTrace();
			System.exit(-1);
		}
		return sb.toString();
	}



	public static void scrollHorizontal_new(AppiumDriver driver){
		if(System.getenv("platform").equalsIgnoreCase("Android")){
			TouchAction touchAction = new TouchAction(driver);
			touchAction.longPress(580, 500).moveTo(30, 500).release().perform();
		}
		else{
			JavascriptExecutor js = driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "right");
			js.executeScript("mobile: scroll", scrollObject);
		}

	}
	public static void scrollHorizontal_new(AppiumDriver driver,int iteration){
		for(int i=0;i<iteration;i++){
			scrollHorizontal_new(driver);
		}

	}
	
	public static void scrollVertical_new(AppiumDriver driver,int iteration){
		for(int i=0;i<iteration;i++){
			scrollvertical_new(driver);
		}

	}
	
	public static void scrollvertical_new(AppiumDriver driver) {
		
		if(System.getenv("platform").equalsIgnoreCase("Android")){
			TouchAction touchAction = new TouchAction(driver);
			touchAction.longPress(50, 600).moveTo(50, 50).release().perform();
		}
		else {
			JavascriptExecutor js = driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "down");
			js.executeScript("mobile: scroll", scrollObject);
		}
	}

	public static void hideKeyboard(AppiumDriver driver){
		try{
			driver.hideKeyboard();
		}
		catch (Exception e){

		}
	}
	public static void pressBackKey(AppiumDriver driver){
		if(inti.platformnew.equalsIgnoreCase("Android")){
			AndroidDriver drivernew=inti.getAndroidDriver();
			drivernew.pressKeyCode(AndroidKeyCode.BACK);
		}


	}
	public static void pressBackKey(AppiumDriver driver,int iteration ){
		while(iteration>0) {
			if (inti.platformnew.equalsIgnoreCase("Android")) {
				AndroidDriver drivernew = inti.getAndroidDriver();
				drivernew.pressKeyCode(AndroidKeyCode.BACK);
				sleep();
			}
			iteration--;
		}

	}
	public static void upgrade(String appname) {

		runShellCommand("adb -s "+System.getenv("deviceId")+"  install -r " + appname);
	}
	public static String getAppVersion(){
		String str = runShellCommand(MessageFormat.format("adb -s "+System.getenv("deviceId")+" shell dumpsys package {0} |grep versionName",System.getenv("packageName")));
		System.out.println(str);
		return str;
	}

	public static void unInstallApp(String pkgName){
		//runShellCommand(MessageFormat.format("adb uninstall",pkgName));
		runShellCommand("adb -s "+System.getenv("deviceId")+"  uninstall in.swiggy.android.prod");
	}

	public static void scrollios(AppiumDriver driver , Direction direction) {
		try {
			driver.context(NATIVE_APP);
			Dimension size = driver.manage().window().getSize();
			int starty = (int) (size.height * 0.7);
			int endy = (int) (size.height * 0.3);
			int startx = size.width / 2;
			switch (direction) {
				case UP:
					driver.swipe(startx, starty, startx, endy, 2000);
					break;
				case DOWN:
					driver.swipe(startx, endy, startx, starty, 2000);
					break;
				default:
					log.info("Invalid Direction for Scroll");
			}
		} catch (Exception e) {
			log.info(e.getMessage());
		}
	}


	public void scrollToElement(AppiumDriver<MobileElement> driver, Direction direction, IElement element) {
		try {
			driver.context(NATIVE_APP);
			Dimension size = driver.manage().window().getSize();
			int starty = (int) (size.height * 0.7);
			int endy = (int) (size.height * 0.3);
			int startx = size.width / 2;
			int counter = 0;
			switch(direction) {
				case UP : 	while(counter < 30 && !checkElementDisplay(element)) {
					driver.swipe(startx, starty, startx, endy, 2000);
					counter++;
				}
					break;
				case DOWN : while(counter < 30 && !checkElementDisplay(element)) {
					driver.swipe(startx, endy, startx, starty, 2000);
					counter++;
				}
					break;
				default : log.info("Invalid Direction for Scroll");
			}
			if(!checkElementDisplay(element))
				log.info("Element Not Found : "+ element);
		} catch(Exception e) {
			log.info(e.getMessage());
		}
	}

	public boolean checkElementDisplay(IElement element) {
		try {
            return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}


}