package com.swiggy.ui.utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by kiran.j on 10/6/17.
 */
public class iOSUtils {
    private AppiumDriver<MobileElement> driver;
    private static Logger log = Logger.getLogger(AndroidUtil.class);
    public enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }


    public iOSUtils() {

    }
    public iOSUtils(AppiumDriver<MobileElement> driver) {
        this.driver = driver;
    }

    public void click(AppiumDriver<MobileElement> driver, By locator) {
        if(isElementPresent(driver, locator))
            driver.findElement(locator).click();
        else {
            log.info("Element Not Found");
            throw new NoSuchElementException("Element Not Found" + locator);
        }
    }

    public void click(AppiumDriver<MobileElement> driver, MobileElement locator) {
        if(isElementPresent(driver, locator))
            locator.click();
        else {
            log.info("Element Not Found");
            throw new NoSuchElementException("Element Not Found" + locator);
        }
    }
    public void clickBackButton(AppiumDriver<MobileElement> driver) {
        driver.navigate().back();
    }

    public boolean isElementPresent(AppiumDriver<MobileElement> driver, By locator) {
        if(locator == null)
            throw new IllegalArgumentException("Locator cannot be Null");

        try{
            driver.findElement(locator);
            log.info("Element Found");
            return true;
        } catch(NoSuchElementException e) {
            log.info("Element Not Found" + locator);
            return false;
        }
    }

    public boolean isElementPresent(AppiumDriver<MobileElement> driver, MobileElement locator){
        if(locator == null) throw new IllegalArgumentException("Locator cannot be Null");
        try{
            WebDriverWait wait = new WebDriverWait(driver,10);
            wait.until(ExpectedConditions.visibilityOf(locator));
            return locator.isDisplayed();
        } catch(Exception e) {
            log.info("Element Not Found" + locator);
            return false;
        }
    }

    public boolean isElementClickable(AppiumDriver<MobileElement> driver, MobileElement locator) {
//		fluentWait.until(ExpectedConditions.elementToBeClickable(locator));
        return locator.isDisplayed() && locator.isEnabled();
    }

    public String getText(AppiumDriver<MobileElement> driver, MobileElement locator) {
        if(locator == null)
            throw new IllegalArgumentException("Locator cannot be null");
        log.info("getText Locator text is " + locator.getText());
        return locator.getText();
    }



    public String getText(AppiumDriver<MobileElement> driver, By locator) {
        validLocator(locator);
        log.info("getText Locator text is " + driver.findElement(locator).getText());
        return driver.findElement(locator).getText();
    }

    public void hideKeyboard(AppiumDriver<MobileElement> driver) {
        driver.hideKeyboard();
    }

    public void clearTextField(AppiumDriver<MobileElement> driver,MobileElement locator) {
        while(!locator.getText().isEmpty()) {
            pressDeleteKey(driver);
        }
    }

    public void clearTextField(AppiumDriver<MobileElement> driver,By locator) {
        log.info("Clearing the text in locator "+ locator);
        int counter = 100;
        while(!driver.findElement(locator).getText().isEmpty() && counter > 0) {
            pressDeleteKey(driver);
            counter--;
        }
    }

    public void clearTextFieldByCount(AppiumDriver<MobileElement> driver, MobileElement locator,int count) {
        log.info("Clearing the text in locator "+ locator);
        while(count-- > 0) {
            pressDeleteKey(driver);
        }
    }

    public  void clearTextFieldByCount(AppiumDriver<MobileElement> driver, By locator,int count) {
        log.info("Clearing the text in locator "+ locator);
        while(count-- > 0) {
            pressDeleteKey(driver);
        }
    }

    public void validLocator(By locator) {
        if(locator == null)
            throw new IllegalArgumentException("Locator cannot be Null");
    }


    public void pressDeleteKey(AppiumDriver<MobileElement> driver) {
        ((AndroidDriver<MobileElement>)driver).pressKeyCode(AndroidKeyCode.DEL);
    }

    public  void pressEnterKey(AppiumDriver<MobileElement> driver) {
        ((AndroidDriver<MobileElement>)driver).pressKeyCode(AndroidKeyCode.ENTER);
    }



    public void waitForElementPresent(AppiumDriver<MobileElement> driver, By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        locator = (By) wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(locator));
    }

    public boolean waitForElementPresent(AppiumDriver<MobileElement> driver, MobileElement locator) {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 10);
            locator = (MobileElement) wait.until(ExpectedConditions.visibilityOf(locator));
            return true;
        } catch(Exception e) {
            log.info("Element Not Found even after waiting for some time" + locator);
            return false;
        }
    }


    public void sendKeys(AppiumDriver<MobileElement> driver, By locator, String text) {
        if(isElementPresent(driver, locator)) {
            driver.findElement(locator).sendKeys(text);
        } else {
            log.info("Element Not Found" + locator);
        }
    }

    public void sendKeys(AppiumDriver<MobileElement> driver, MobileElement locator, String text) {
        if(isElementPresent(driver, locator)) {
            locator.sendKeys(text);
        }
    }

    public String getAlertText() {
        try {
            Alert alert = this.driver.switchTo().alert();
            return alert.getText();
        } catch(NoAlertPresentException e) {
            throw new NoAlertPresentException();
        }
    }

    public void acceptAlert() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
    }

    public void dismissAlert() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().dismiss();
    }

    public void openNotifications() {
        ((AndroidDriver<MobileElement>)driver).openNotifications();
    }

    public void selectFromList(By element, String text) {
        try{
            List<MobileElement> list = driver.findElements(element);
            for(MobileElement e: list) {
                if(e.getText().equalsIgnoreCase(text))
                    this.click(driver, element);
            }
        } catch(Exception e){
            log.info(e.getMessage());
        }
    }

    public void selectElement(AppiumDriver<MobileElement> driver,By by, String text) {
        List<MobileElement> list = driver.findElements(by);
        for(MobileElement m : list) {
            if(m.getText().equalsIgnoreCase(text)) {
                this.click(driver, m);
                break;
            }
        }
    }
}
