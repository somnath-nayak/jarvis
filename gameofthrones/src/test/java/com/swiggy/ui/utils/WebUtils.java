package com.swiggy.ui.utils;

import framework.gameofthrones.Aegon.*;
import io.appium.java_client.AppiumDriver;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
public class WebUtils {

    public void initElements(WebDriver driverRef, Object page, InitializeUI init)
    {

        Class<?> proxyIn = page.getClass();
        Object value;
        InvocationHandler handler ;
        System.out.println("ProxyIn"+proxyIn);
        Field[] fields=proxyIn.getDeclaredFields();
        for (Field field : fields) {

            System.out.println("" + field.getName() + "" + field.getDeclaredAnnotations() + " = = " + field.getType());
            if(field.getType().toString().contains("interface")){
                if (IElement.class.isAssignableFrom(field.getType())) {
                    handler = new LocatingElementHandler(new ElementLocator(driverRef, field, init));
                    value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{IElement.class}, handler);
                } else {
                    handler = new LocatingElementListHandler(new ElementLocator(driverRef, field, init));
                    value = Proxy.newProxyInstance(page.getClass().getClassLoader(), new Class[]{List.class}, handler);
                }

                field.setAccessible(true);
                try {
                    field.set(page, value);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }


    }
    
    
	public boolean checkElementDisplay(IElement element)
	{
		try
		{
			if(element.isDisplayed(10))
			{
				return true;
			}
		}
		catch(Exception e)
		{
			return false;
		}
		return false;

	}
	
	public void wait( WebDriver driver, int time, TimeUnit unit  ) 
	{
		driver.manage().timeouts().implicitlyWait(time, unit);
	}
	
	public void refresh(WebDriver driver)
	{
		driver.navigate().refresh();
	}
	
	public String removeParentUrl(String str,String parentUrl)
	{
		return str.replace(parentUrl,"");
	}
	

	



}

