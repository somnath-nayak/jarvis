package com.swiggy.ui.pageclass.delivery;

import java.util.ArrayList;
import java.util.List;

import io.appium.java_client.MobileElement;

/**
 * Created by kiran.j on 1/8/18.
 */
public interface DeConstants {

    String de_id = "216";
    String suspended_deid = "27166";
    String master_otp = "547698";
    String incorrect_otp = "123";
    String app_version = "1.8.6.0";
    String profile = "Profile";
    String slot_sign_up = "Slot Sign Up";
    String stop_duty = "Stop Duty";
    String start_duty = "Start_Duty";
    String logout = "Logout";
    String issues_complaints = "Issues and Complaints";
    String feeds = "Feeds";
    String notice_board = "Notice Board";
    String order_history = "Order History";
    String incentives = "Incentives";
    String bank_details = "Bank Details";
    String legal = "Legal";
    String earning_history="Earnings History";
    String feed_back = "Feedback";
    String loginhistory = "Login History";
    String order_unassigned = "Unassign Order ";
    String arrived_message = "Arrived at Restaurant";
    String pickedup_message = "Order picked up";
    String delivered_message = "Order Delivered";
    String city_code = "1";
    String reassign_status_code = "2";
    String reached_popupmessage = "Are you sure you reached customer location ?";
    String delivered_popupmessage = "Are you sure you delivered the order ?";
    String current_task = "CURRENT TASK";
    String next_task = "NEXT TASKS";
	String  de_id1 = "1026";
    String stop_duty_popup = "Stop duty";
    String unassign_order = "Unassign Order";
    String locate_rest = "Unable to locate Restaurant ";
    String Feedback= "TEST FEEDBACK";
    String LongdistanceOrder="I have to travel more than shown distance for an order";
    String OrderPayoutIncorrect="The payout for this order shown less then the actual payout";
    String RecivedLessPayout="I have Recived less payout for this week";
    String DidNotRecivedPayout="I did order but did not recive payout";
    String Managerrelatedissue ="My leaves or shift changes are not approved";
    String ManageDoseNotPickCalls="My Fm never picks calls";
    String GeneralIssues="GOT Automation";
    String NotRecivingorders="I am not reciving orders in my app";
    String de_id3 ="216";
    String opt[] = {"profile","slot_sign_up","feeds","notice_board","order_history","bank_details","legal","feed_back"};
   
    
}
