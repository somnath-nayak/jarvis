package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.NewTaskPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/11/18.
 */
public class NewTaskPage extends AndroidUtil {
	
	public IElement reject_button;
	public IElement confirm_order;
	public IElement back_button ;
	public IElement confirm_button;
	public IElement cancel_button;

    private AppiumDriver<MobileElement> driver = null;
    private NewTaskPageObject newTaskPageObject = new NewTaskPageObject();

    public NewTaskPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
        //PageFactory.initElements(new AppiumFieldDecorator(driver), newTaskPageObject);
    }

    public NewTaskPage confirmOrder() {
//        this.click(newTaskPageObject.confirm_order);
//        return this;
    	
     	this .confirm_order.click();
    	return this;
    	
    	
    }

    public NewTaskPage rejectOrder() {
//        this.click(newTaskPageObject.reject_button);
//        return this;
    	this.reject_button.click();
    	return this;
    }

    public void confirm() {
        //this.click(newTaskPageObject.confirm_button);
      this.confirm_button.click();
    }

    public NewTaskPage cancel() {
       // this.click(newTaskPageObject.cancel_button);
        //return this;
    
     this.cancel_button.click();
     return this;
    }
    


}
