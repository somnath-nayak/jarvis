package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.FeedsPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/17/18.
 */
public class FeedsPage extends AndroidUtil {
	
	public IElement first_feed;
	public IElement feeds;

    private AppiumDriver<MobileElement> driver = null;
    private FeedsPageObject feedsPageObject = new FeedsPageObject();

    public FeedsPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       // this.driver = driver;
       // PageFactory.initElements(new AppiumFieldDecorator(driver), feedsPageObject);
    }

    public boolean isFeedsPage() {
//        this.waitForElementPresent(driver, feedsPageObject.feeds);
//        return this.isElementPresent(feedsPageObject.feeds);
            try {
				this.feeds.wait(50000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

        return this.feeds.isDisplayed(1000);
           
    }

    public FeedsPage selectFirstFeed() {
//        if(this.isElementPresent(feedsPageObject.first_feed)) {
//            this.click(feedsPageObject.first_feed);
//            this.clickBackButton();
//        }
//        return this;
    
    if(this.first_feed.isDisplayed())
    {
    	this.first_feed.click();
    	this.clickBackButton();
    }
    return this;
     }
    
    

   public FeedsPage scrollThroughFeeds() {
	   this.scroll(Direction.UP);
       return this;
    
   
   }
}
