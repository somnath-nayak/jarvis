package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.OrderHistoryPageObject;
import com.swiggy.ui.utils.AndroidUtil;
import com.swiggy.ui.utils.AndroidUtil.Direction;

/**
 * Created by kiran.j on 1/18/18.
 */
public class OrderHistoryPage extends AndroidUtil {
	
	public IElement  order_title;
	public IElement  change_datebutton;
	public IElement  history_title;
	public IElement  order_list;
	public IElement  orders ;
	public IElement  orderdetails;
	public IElement  Help ;
	public IElement changeyear;
	public IElement changemonth;
	public IElement changedate;
	public IElement okbutton ;
	
//    private AppiumDriver<MobileElement> driver = null;
//    private OrderHistoryPageObject orderHistoryPageObject = new OrderHistoryPageObject();

    public OrderHistoryPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        //this.driver = init.getAndroidDriver();
        
        //PageFactory.initElements(new AppiumFieldDecorator(driver), orderHistoryPageObject);
    }

    public boolean isOrderHistoryPage() {
       // return this.isElementPresent(orderHistoryPageObject.history_title);
    	
    	return this.history_title.isDisplayed();
    }

    public boolean verifyOrderHistory() {
        boolean success = false;

//        success = this.isElementPresent(driver, orderHistoryPageObject.change_datebutton);
//        success &= this.isElementPresent(driver, orderHistoryPageObject.order_list);
//        success &= this.isElementPresent(driver, orderHistoryPageObject.order_title);
//
//        success = this.isElementPresent(orderHistoryPageObject.change_date);
//       
//        return success;
        
        
        success = this.change_datebutton.isDisplayed();
        success &= this.order_list.isDisplayed();
        success &= this.order_title.isDisplayed();
        
        success =this.changedate.isDisplayed();
        
        return success;
        
    }
    
    public OrderHistoryPage Click_OnOrders()
    {
    	
    	
//    	 this.click(driver,orderHistoryPageObject.orders );
//         return this;
    	
    	this.orders.click();
    	return this;
    	
    	
    }
    public OrderHistoryPage Click_OnHelp()
    {
//    	 this.click(driver,orderHistoryPageObject.Help );
//         return this;
    	
    	this.Help.click();
    	return this;
   	
    }
    
    public OrderHistoryPage Click_changedate()
    {
//    	 this.click(driver,orderHistoryPageObject.change_datebutton);
//         return this;
    	
    	this.change_datebutton.click();
    	return this;
   	
    }
    
    public OrderHistoryPage Click_changyear()
    {
//    	 this.click(driver,orderHistoryPageObject.changeyear);
//         return this;
    	this.changeyear.click();
    	return this;
   	
    }
    
    public OrderHistoryPage Click_changemonth()
    {
//    	 this.click(driver,orderHistoryPageObject.changemonth);
//         return this;
    	
    	this.changemonth.click();
    	return this;
   	
    }
    
    public OrderHistoryPage Click_changedateto()
    {
//    	 this.click(driver,orderHistoryPageObject.changedate);
//         return this;
    	
    	
    	this.changedate.click();
    	return this;
   	
    }
    public OrderHistoryPage Click_onOkbutton()
   {
//    	 this.click(driver,orderHistoryPageObject.okbutton);
//         return this;
   	
    	this.okbutton.click();
    	return this;
    }
    public OrderHistoryPage scrollorderdetailpage() {
    	//this.scrollToElement(driver, Direction.UP,orderHistoryPageObject.orderdetails );
    	this.scroll( Direction.UP);
		return this;
    	
    	}
    }

