package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.OrderHistoryPageObject;
import com.swiggy.ui.pageobject.delivery.StopDutyObject;
import com.swiggy.ui.utils.AndroidUtil;

public class StopDutyPage extends AndroidUtil {
	
	
	public IElement yesbutton ;
	public IElement startdutybutton;
	public IElement startdutypopup;
	public IElement stopdutypopup ;
	public IElement taponbackground ;
	//public IElement startdutybutton;	


 private AppiumDriver<MobileElement> driver = null;
	    private StopDutyObject StopDutyObject = new StopDutyObject();

	    public StopDutyPage(InitializeUI init) {
	        super(init);
	        initElements(init.getAppiumDriver(), this, init);
	       this.driver = init.getAndroidDriver();
	        //this.driver = driver;
	       // PageFactory.initElements(new AppiumFieldDecorator(driver), StopDutyObject);
	    }
	
	    public boolean isStopdutypopup() {
	      //  return this.isElementPresent(driver,StopDutyObject.stopdutypopup );
	      return this.startdutypopup.isDisplayed();
	    }

	    public boolean isStartdutypopup() {
	       // return this.isElementPresent(driver,StopDutyObject.startdutypopup );
	    	return this.startdutypopup.isDisplayed();
	    }

	    public StopDutyPage Click_yesbutton()
	    {
//	    	 this.click(driver,StopDutyObject.yesbutton );
//	         return this;
	    	
	    	this.yesbutton.click();
	    	return this;
	    }
	
	    
	    public StopDutyPage Click_backgroundbutton()
	    {
//	    	 this.click(driver,StopDutyObject.taponbackground );
//	         return this;
	    	this.taponbackground.click();
	    	return this;
	    	
	    	
	    }
	    public StopDutyPage Click_Startdutybutton()
	    {
//	    	 this.click(driver,StopDutyObject.startdutybutton );
//	         return this;	
	    	this.startdutybutton.click();
	    	return this;
	    }
	    
	public StopDutyPage click_swipeleft()
	{
		this.swipe(driver, Direction.LEFT);
		return this;
		
	}
	
}
