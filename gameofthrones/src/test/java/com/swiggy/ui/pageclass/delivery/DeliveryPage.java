package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.DeliveryPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/10/18.
 */
public class DeliveryPage extends AndroidUtil {
	
	public IElement messages;
	public IElement action_customer ;
	public IElement help;
	public IElement action_button;
	public IElement confirm_button;
	public IElement cancel_button;
	public IElement item_pickedup;
	public IElement bill_value;
	public IElement confirm_bill_value;
	public IElement paid_value;
	public IElement shutter_button;
	public IElement  done_button ;
	public IElement cancel_camera_button;
	public IElement delivered_collect;
	public IElement confirm_order;
	public IElement total_bill;
	public IElement reject_button;
	public IElement reject_confirm;
	public IElement long_distance_option;
	public IElement call_customer;
	public IElement call_customerenable;
    private AppiumDriver<MobileElement> driver = null;
    private DeliveryPageObject deliveryPageObject = new DeliveryPageObject();

    public DeliveryPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       
       // PageFactory.initElements(new AppiumFieldDecorator(driver), deliveryPageObject);
    }

    public DeliveryPage markArrived() {
//        if(this.getText(deliveryPageObject.action_button).trim().equalsIgnoreCase(DeConstants.arrived_message));
//            this.click(deliveryPageObject.action_button);
 
         if (this.action_button.getText().trim().equalsIgnoreCase(DeConstants.arrived_message))
        	 this.action_button.click(100);
             return this;
    }

    public void confirm() {
        //this.click(deliveryPageObject.confirm_button);
    	
      this.confirm_button.click(100);   
    }
    

    public DeliveryPage markPickedUp() {
//        this.click(deliveryPageObject.item_pickedup);
//        if(this.getText(deliveryPageObject.action_button).trim().equalsIgnoreCase(DeConstants.arrived_message));
//        this.click(deliveryPageObject.action_button);
    	  this.item_pickedup.click();
    	if(this.action_button.getText().trim().equalsIgnoreCase(DeConstants.arrived_message));
    	this.action_button.click();
         return this;
    	
    }

    public void confirmOrder() {
       // this.click(deliveryPageObject.confirm_order);
    	confirm_order.click(100);
        
    }

    public DeliveryPage reached() {
//        this.click(deliveryPageObject.action_button);
//        this.click(deliveryPageObject.confirm_button);
//        return this;
    	this.action_button.click();
    	this.confirm_button.click(100);
           return this;
           
    }

    public void deliverd() {
//        this.click(deliveryPageObject.action_button);
//        this.click(deliveryPageObject.confirm_button);
//        if(this.isElementPresent(deliveryPageObject.confirm_button))
//        this.click(deliveryPageObject.confirm_button);
    	this.action_button.click();
    	this.confirm_button.click();
    	if(this.confirm_button.isDisplayed())
    		this.cancel_button.click();
    	
    }

    public String getTotalBill() {
  
    	//  return this.getText(deliveryPageObject.total_bill);
    	return this.total_bill.getText();
    }

    public DeliveryPage enterBill(String bill_value) {
//        this.sendKeys(deliveryPageObject.bill_value, bill_value);
//        this.sendKeys(deliveryPageObject.confirm_bill_value, bill_value);
//        return this;
    	
    	this.bill_value.sendKeys(bill_value);
    	this.confirm_bill_value.sendKeys(bill_value);
    	return this;
    	
    }

    public void rejectOrder() {
//        this.click(deliveryPageObject.reject_button);
//        this.click(deliveryPageObject.reject_confirm);
//        this.click(deliveryPageObject.long_distance_option);
    	
    	reject_button.click(100);
    	reject_confirm.click(100);
    	long_distance_option.click(100);
    }

    public void navigateToCustomerDetails() {
//        this.click(deliveryPageObject.action_customer);
//        return this;
    	
    	action_customer.click();
    
    	
    }

    public boolean isCallCustomerEnabled() {
        //return deliveryPageObject.call_customer.isEnabled();
    	return call_customer.isEnabled();
    	
    }

    public DeliveryPage goBackToDeliveryPage() {
       this.clickBackButton();
        return this;
    	
    }

    public void navigateToHelp() {
        //this.click(deliveryPageObject.help);
    	this.help.click();
    }
}
