package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.FeedbackPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/10/18.
 */
public class FeedbackPage extends AndroidUtil {
	
	public IElement rating_bar;
	public IElement order_id;
	public IElement feedback;
	public IElement distance_submit;
	public IElement leave_comment;
	public IElement submit;
	

    private AppiumDriver<MobileElement> driver = null;
    private FeedbackPageObject feedbackPageObject = new FeedbackPageObject();

    public FeedbackPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), feedbackPageObject);
    }

    public boolean isFeedBackPage() {
       // return this.isElementPresent(feedbackPageObject.feedback);
     return this.feedback.isDisplayed();
    }
}
