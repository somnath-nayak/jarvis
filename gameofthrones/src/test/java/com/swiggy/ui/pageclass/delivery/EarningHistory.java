package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import com.swiggy.ui.utils.AndroidUtil;

public class EarningHistory extends AndroidUtil{
	
	public IElement this_week;
	public IElement total;
	public IElement orders;
	public IElement incentives ;
	public IElement adjustments;
	public IElement previous_week;
	public IElement week_3;
	public IElement week_4;
	
    private AppiumDriver<MobileElement> driver = null;

	
	 public EarningHistory(InitializeUI init) {
	        super( init);
	        this.driver = init.getAndroidDriver();
	       
	    }
	 public void ClickOn_thisweek()
	    {
	    	
	    	this.this_week.click();
	          
	    }
	 
	 

}
