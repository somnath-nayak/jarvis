package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.ProfilePageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class ProfilePage extends AndroidUtil{
	
	
	public IElement profile;
	public IElement profileIcon;
	public IElement download_insurance;
	public IElement ServiceDetail;
	public IElement EmergencyInfo;
	public IElement bankDetails;
	public IElement deposit_cash;
	public IElement icici;
	public IElement novopay;
	public IElement swiggyhub;
	public IElement upi;
	public IElement navigateup;
	
	
//	private AppiumDriver<MobileElement> driver = null;
//    private  ProfilePageObject profilePageObject = new  ProfilePageObject();

    public ProfilePage(InitializeUI init) {
        super( init);
        initElements(init.getAppiumDriver(), this, init);
        //this.driver = init.getAndroidDriver();
        //PageFactory.initElements(new AppiumFieldDecorator(driver), profilePageObject);
    }

    public ProfilePage clickOn_ProfileImage()
    {
//    	this.click(driver, profilePageObject.profile);
//        return this;
    	
    	this.profile.click();
    	  return this;
    }
    
    public void clickOn_DownloadInsur()
    {
    	
//    	this.click(driver, profilePageObject.download_insurance);
//        return this;
    	download_insurance.click(1000);
    	  
    }
    
    public ProfilePage clickOn_Servicedetails()
    {
    	
//    	this.click(driver, profilePageObject.ServiceDetail);
//        return this;
            this.ServiceDetail.click();
            return this;
    }
    
    
    public ProfilePage clickOn_emergencyInfo()
    {
   	
//    	this.click(driver, profilePageObject.EmergencyInfo);
//        return this;
    	
    	this.EmergencyInfo.click();
    	  return this;
    }
    
    
    public ProfilePage clickOn_bankDetails()
    {
    	
//    	this.click(driver, profilePageObject.bankDetails);
//        return this;
    	this.bankDetails.click();
    	  return this;
    }
    
    public boolean isProfilePage() {
//        if(this.isElementPresent(driver, profilePageObject.profile))
//            this.click(driver, profilePageObject.profile);
//        return this.isElementPresent(driver, profilePageObject.profile);
    	
    	
    	if (this.profile.isDisplayed())
    	
    		this.profile.click();
    		return this.profile.isDisplayed();
    }

    public ProfilePage scrollprofile() {
//    	this.scrollToElement(driver, Direction.UP, profilePageObject.bankDetails);
//		return this;
    	this.scroll(Direction.UP);
    	
    return this;
    
    	
    	  
    	}
    
    public void Click_OnDepositecash()
    {
    	
    	deposit_cash.click(100);
    }
    
    public void Click_Onicici()
    {
    	
    	icici.click(100);
    }
    
    public void Click_OnNovopay()
    {
    	
    	novopay.click(100);
    	
    }
    
    public void Click_OnSwiggyhub()
    {
    	
  swiggyhub.click(100);
    }
    
    
public void Click_onUpi()
{
	
	upi.click(100);
	
}
public void clickon_backbutton()
{

	navigateup.click(1000);


}
}

