package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.IssuesAndComplaintsPageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class IssuesAndComplaintsPage extends AndroidUtil {
	
	public IElement  IssuesAndComplaintspage1 ;
	public IElement  RaiseNewIssuebutton ;
	public IElement  OrderRelatedIssues ;
	public IElement  LongDistanceOrder ;
	public IElement  EnterComplaints ;
	public IElement  confirmissuebutton ;
	public IElement  Issuedetailpage ;
	public IElement   OrderPayoutIncorrect;
	public IElement  WeeklyPayoutRelatedIssues ;
	public IElement   RecivedlessPayout;
	public IElement  DidNotRecivedPayout ;
	public IElement  ManagerRelatedIssues ;
	public IElement   ManagerDosenotpickcall;
	public IElement  Nextbutton ;
	public IElement   GeneralIssue;
	public IElement  Notrecivingorders ;
	public IElement  others ;
	
	
	
	
	

    private AppiumDriver<MobileElement> driver = null;
    private IssuesAndComplaintsPageObject IssuesAndComplaintsPageObject = new IssuesAndComplaintsPageObject();
    InitializeUI initializeUI;
    public IssuesAndComplaintsPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
    }
    
    public boolean isIssuesAndComplaintspage() {
       // return this.isElementPresent(driver,IssuesAndComplaintsPageObject.IssuesAndComplaintspage1 );
    
    return this.IssuesAndComplaintspage1.isDisplayed();
    }
    
    public IssuesAndComplaintsPage Click_OnRaiseNewIssue()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.RaiseNewIssuebutton );
//         return this;	
    	
    	this.RaiseNewIssuebutton.click();
            return this;
    }
    
    public IssuesAndComplaintsPage Click_OnOrderRelatedIssue()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.OrderRelatedIssues );
//         return this;	
    
    this.OrderRelatedIssues.click();
    return this;
    }
    
    public IssuesAndComplaintsPage Click_OnLongdistanceorder()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.LongDistanceOrder );
//         return this;	
    	
    	this.LongDistanceOrder.click();
    	return this;
    }
    
    public IssuesAndComplaintsPage enterComplaints(String issue)
    {
//    	 this.sendKeys(driver,IssuesAndComplaintsPageObject.EnterComplaints , issue);
//         return this;
    	
    	this.EnterComplaints.sendKeys(issue);

    return this;}
    
    public IssuesAndComplaintsPage Click_Orderpayout()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.OrderPayoutIncorrect );
//         return this;	
    this.OrderPayoutIncorrect.click();
    return this;
    
    
    }
    public IssuesAndComplaintsPage Click_OnConfirmbutton()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.confirmissuebutton );
//         return this;	
    	
    	this.confirmissuebutton.click();
    	return this;
    }
    
    public boolean isIssuesdetailpsge() {
       // return this.isElementPresent(driver,IssuesAndComplaintsPageObject.Issuedetailpage );
    
   return  this.Issuedetailpage.isDisplayed();}
    
    
    public IssuesAndComplaintsPage Click_Managerrelatedissue()
   {
//    	 this.click(driver,IssuesAndComplaintsPageObject.ManagerRelatedIssues);
//         return this;	
     this.ManagerRelatedIssues.click();
    return this;
    }
    
    public IssuesAndComplaintsPage Click_managerdoespickcall()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.ManagerDosenotpickcall);
//         return this;	
    	this.ManagerDosenotpickcall.click();
    	return this;
    	
    }
    
    public IssuesAndComplaintsPage Click_onNext()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.Nextbutton);
//         return this;	
    	this.Nextbutton.click();
    	return this;
    	
    }
    
    public IssuesAndComplaintsPage Click_weeklyrelated()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.WeeklyPayoutRelatedIssues);
//         return this;	
    	
    	this.WeeklyPayoutRelatedIssues.click();
    	return this;
    }
    
    public IssuesAndComplaintsPage Click_Recivedlesspayout()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.RecivedlessPayout);
//         return this;
    	
    this.RecivedlessPayout.click();
    return this;
    }
    
    public IssuesAndComplaintsPage Click_generalissue()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.GeneralIssue);
//         return this;	
    	
    	this.GeneralIssue.click();
    	return this;
    }
    
    public IssuesAndComplaintsPage Click_notrecivingorders()
    {
//    	 this.click(driver,IssuesAndComplaintsPageObject.Notrecivingorders);
//         return this;	
    	this.Notrecivingorders.click();
    	return this;
    }
}
