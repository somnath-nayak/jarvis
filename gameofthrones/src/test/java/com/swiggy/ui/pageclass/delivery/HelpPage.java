package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.HelpPageObject;
import com.swiggy.ui.utils.AndroidUtil;

import java.util.List;

/**
 * Created by kiran.j on 1/18/18.
 */
public class HelpPage extends AndroidUtil {
	
	public IElement title;
	public List<IElement> by_title;
	public IElement unassign_button;
	public IElement cancel_button;
    public IElement call_fm;
	

    private AppiumDriver<MobileElement> driver = null;
    private HelpPageObject helpPageObject = new HelpPageObject();

    public HelpPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), helpPageObject);
    }

    public void selectReasonsFromString(String s) {
//        List<MobileElement> l = driver.findElements(helpPageObject.by_title);
//        for(MobileElement e: l) {
//            System.out.println(e.getText());
//            if (e.getText().trim().equalsIgnoreCase(s)) {
//                this.click(e);
//                break;
    	for(int i=0;i<by_title.size();i++)
    	{
    		System.out.println("Title value"+by_title.get(i).getText().toString());
    		    if (by_title.get(i).getText().toString().trim().equalsIgnoreCase(s))
    	        	{
    			this.by_title.get(i).click();
    			this.by_title.get(i).click();
    			
    			break;
    		           }
    		
    		
            }
     }
    

    public void selectFirstReason() {
//        List<MobileElement> l = driver.findElements(helpPageObject.by_title);
//        this.click(l.get(0));
        by_title.get(0).click();
    }
    

    public void unassign() {
//        this.click(helpPageObject.unassign_button);
//        this.clickBackButton();
    	
    	this.unassign_button.click();
        this.clickBackButton();
    	
    }

    public void cancel() {
        this.click(helpPageObject.cancel_button);
    }

    public boolean isCallFmDisplayed() {
       //return this.isElementPresent(helpPageObject.call_fm);
     return call_fm.isDisplayed();
    }
}
