package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.NoticePageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/17/18.
 */
public class NoticePage extends AndroidUtil{

	
	
	public IElement noticeboard_title;
	public IElement notice_list;
//    private AppiumDriver<MobileElement> driver = null;
//    private NoticePageObject noticePageObject = new NoticePageObject();

    public NoticePage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
      //  this.driver = init.getAndroidDriver();
        //PageFactory.initElements(new AppiumFieldDecorator(driver), noticePageObject);
    }

    public boolean isNoticePage() {
     //   return this.isElementPresent(noticePageObject.noticeboard_title);
   
    return this.noticeboard_title.isDisplayed();
    }

    public boolean isNoticeListDisplayed() {
      //  return this.isElementPresent(noticePageObject.notice_list);
    	
    	return this.notice_list.isDisplayed();
    }
}
