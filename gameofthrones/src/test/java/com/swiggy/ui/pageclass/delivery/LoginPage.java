package com.swiggy.ui.pageclass.delivery;

import com.swiggy.ui.api.DeliveryHelper;
import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.LoginPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/8/18.
 */
public class LoginPage extends AndroidUtil{
	
	public IElement login_id;
	public IElement submit_button;
	public IElement otp_text;
	public IElement submit_otp;
	public IElement resend_otp;

    public AndroidDriver driver;
    private LoginPageObject loginPageObject = new LoginPageObject();

    public LoginPage(InitializeUI init) {
        super(init);
        this.driver = init.getAndroidDriver();
        initElements(init.getAppiumDriver(), this, init);
       // PageFactory.initElements(new AppiumFieldDecorator(driver), loginPageObject);
    }

    public boolean isLoginPage() {
       // return this.isElementPresent(loginPageObject.login_id);
        return login_id.isDisplayed();
    }

    public void submitLogin() {
        //submit_button.click();
        driver.pressKeyCode(AndroidKeyCode.ENTER);
    }

    public void enterDeId(String de_id) {
       // this.sendKeys(loginPageObject.login_id, de_id);
        login_id.sendKeys(de_id);
    }

    public void enterOtp(String otp) {
        try {
            Thread.sleep(10000);
           // this.sendKeys(loginPageObject.otp_text, otp);
            otp_text.sendKeys(otp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void submitOtp() {
        //this.click(loginPageObject.submit_otp);
    	submit_otp.click();
    }

    public LoginPage resendOtp() {
       // this.click(loginPageObject.resend_otp);
    	resend_otp.click();
        return this;
    }

    public void performLogin(String de_id,String version){
        DeliveryHelper deliveryHelper = new DeliveryHelper();
        enterDeId(de_id);
        submitLogin();
        String otp = deliveryHelper.getOtp(de_id, version);
        enterOtp(otp);
        submitOtp();
    }
}
