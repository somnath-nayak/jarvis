package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.BankdetailPageObject;
import com.swiggy.ui.pageobject.delivery.ProfilePageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class BankdetailPage extends AndroidUtil {
	
	
	public IElement bankdetailCheckbox;
	public IElement bankSavebutton;
	public IElement navigateup ;
	
	private AppiumDriver<MobileElement> driver = null;
    private  BankdetailPageObject bankdetailpageObject = new  BankdetailPageObject();

    public BankdetailPage(InitializeUI init) {
        super( init);
        this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), bankdetailpageObject);
    }

    public BankdetailPage clickOn_Confirmcheckbox()
    {
//    	this.click(driver, bankdetailpageObject.bankdetailCheckbox);
//        return this;
    	
    	this.bankdetailCheckbox.click();
        return this;
    }
    
 public BankdetailPage ckickOn_savebutton()
 {
//	 this.click(driver,bankdetailpageObject.bankSavebutton);
//	return this;
	 
	 this.bankSavebutton.click();
     return this;
	 
 }
  
 
public void clickon_backbutton()
{
//	 this.click(driver,bankdetailpageObject.navigateup);
//	return this;
	
	navigateup.click(1000);


}
 
}
