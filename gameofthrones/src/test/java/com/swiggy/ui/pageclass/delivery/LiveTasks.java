package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.LiveTasksPageObject;
import com.swiggy.ui.utils.AndroidUtil;

import java.util.List;

/**
 * Created by kiran.j on 1/9/18.
 */
public class LiveTasks extends AndroidUtil {

	public IElement task_title;
	public IElement left_nav;
	public IElement navigate_arrow;
	public IElement toolbar;
	public IElement ok_button;
	public IElement toast;
	//public IElement by_task_title;
	public IElement tasks;
	public IElement alert_title;
	public By tasks_by = By.id("navigate_arrow");
	public By by_task_title = By.id("title");
	
  private AppiumDriver<MobileElement> driver = null;
 private LiveTasksPageObject liveLaunchPageObject = new LiveTasksPageObject();

    public LiveTasks(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), liveLaunchPageObject);
    }

    public void selectLeftNav() {
       // this.click(liveLaunchPageObject.left_nav);
//    	if(ok_button.isDisplayed())
//    		ok_button.click();
    	left_nav.click(1000);

    }

    public void clickOnToolBar() {
       // this.click(liveLaunchPageObject.toolbar);
    	this.toolbar.click();
    }

    public LiveTasks dismissLeftNav() {
        int height = driver.manage().window().getSize().getHeight() / 2;
        int width = (int) (driver.manage().window().getSize().getWidth() * 0.8);
        this.clickOnXY(width, height);
        return this;
    }

    public boolean isLiveTasksPage() {
//        if(this.isElementPresent(liveLaunchPageObject.ok_button))
//            this.click(liveLaunchPageObject.ok_button);
//        return this.isElementPresent(liveLaunchPageObject.left_nav);
       if(this.ok_button.isDisplayed())
    	this.ok_button.click();
      return this.left_nav.isDisplayed();
      
    }

    public boolean nextTaskVerify() {
        return true;
    }

    public boolean isOrderUnassigned() {
       // return (this.getText(liveLaunchPageObject.alert_title).trim().equalsIgnoreCase(DeConstants.order_unassigned));
    
    	return (alert_title.getText().trim().equalsIgnoreCase(DeConstants.order_unassigned));
    }

    public LiveTasks clickOk() {
//        this.click(liveLaunchPageObject.ok_button);
//        return this;
    	this .ok_button.click();
    	return this;
    }

    public void selectFirstTask() {
        //this.click(liveLaunchPageObject.navigate_arrow);
    	//if(this.navigate_arrow.isDisplayed())
    	if(driver.findElement(tasks_by).isDisplayed())
    	this.navigate_arrow.click();
    	
    }

    public void selectNthTask(int n) {
        int counter = 0;
       //List<MobileElement> ele = driver.findElements(liveLaunchPageObject.tasks);
        List<MobileElement> ele = driver.findElements(tasks_by);
        for(MobileElement e : ele) {
            if(counter == n) {
                this.click(e);
                break;
            }
            counter++;
        }
    }

    public boolean isTasksDisplayed() {
       // return driver.findElements(liveLaunchPageObject.by_task_title).size() >= 1;
   return driver.findElements(by_task_title).size() >=1;
      
    }

}
