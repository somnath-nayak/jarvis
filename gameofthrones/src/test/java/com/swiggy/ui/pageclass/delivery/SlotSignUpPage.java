package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.SlotSignUpPageObject;
import com.swiggy.ui.utils.AndroidUtil;

import java.util.HashMap;
import java.util.List;

/**
 * Created by kiran.j on 1/17/18.
 */
public class SlotSignUpPage extends AndroidUtil{
	
          public IElement available;
          public IElement update;
          public IElement no_radio;
          public IElement yes_radio;
          public IElement maybe;
          public List<IElement> byyes_radio;
          public List<IElement> byno_radio;
          public IElement slot_title;
          public IElement date ;

	
//	 private AppiumDriver<MobileElement> driver = null;
//    private SlotSignUpPageObject slotSignUpPageObject = new SlotSignUpPageObject();

    public SlotSignUpPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
      //  this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), slotSignUpPageObject);
    }

    public boolean isSlotSignUpPage() {
       // return this.isElementPresent(slotSignUpPageObject.slot_title);
    return this.slot_title.isDisplayed();
    
    }

    public SlotSignUpPage toggleAvailableOn() {
//        if(!slotSignUpPageObject.available.isSelected())
//            this.click(slotSignUpPageObject.available);
//        return this;
    	
    	if(!available.isSelected())
    		this.available.click();
    	return this;
    }

    public SlotSignUpPage toggleAvailableOff() {
//        if(slotSignUpPageObject.available.isSelected())
//            this.click(slotSignUpPageObject.available);
//        return this;
    	
    	if(available.isSelected())
    		this.available.click();
    	return this;
    }

    public void update() {
       // this.click(slotSignUpPageObject.update);
    	this.update.click();
    }

    public boolean isNoRadioEnabled() {
//        List<MobileElement> l = driver.findElements(slotSignUpPageObject.byno_radio);
//        for(MobileElement e: l)
//            if(!e.isEnabled())
//                return false;
//        return true;
        
         for (int i=0; i<byno_radio.size();i++)
    		
    	{
    		if(!byno_radio.get(i).isEnabled())
    			return false;
    		
    	}
    	
    	return true;
    }

    public boolean isYesRadioEnabled() {
//        List<MobileElement> l = driver.findElements(slotSignUpPageObject.byyes_radio);
//        for(MobileElement e: l)
//            if(!e.isEnabled())
//                return false;
//        return true;
    	
    	
    	for (int i=0; i<byyes_radio.size();i++)
    		
    	{
    		if(!byyes_radio.get(i).isEnabled())
    			return false;
    		
    	}
    	
    	return true;
    }

    public SlotSignUpPage clickon_date()
  {
//    	 this.click(driver, slotSignUpPageObject.date);
//         return this;
    	
    	this.date.click();
    	return this;
    	
    	
    }
    
    public SlotSignUpPage clickon_yes()
    {
//    	 this.click(driver, slotSignUpPageObject.yes_radio);
//         return this;
  	  this.yes_radio.click();
  	  return this;
    }
    
    public SlotSignUpPage clickon_no()
   {
//    	 this.click(driver, slotSignUpPageObject.no_radio);
//         return this;
    	
    	this.no_radio.click();
    	return this;
    	
    
    }
    public SlotSignUpPage clickon_maybe()
    {
//    	 this.click(driver, slotSignUpPageObject.maybe);
//         return this;
    	
    	this.maybe.click();
    	return this;
    }   	
    
    public HashMap<String, String> hour(){
    HashMap<String, String> hr = new HashMap<String, String>();
    hr.put(	"Breakfast", "7:00 -12:00");
    hr.put("Lunch", "13:00 - 15:00");
    hr.put("Snacks", "16:00 - 18:00");
    hr.put("Dinner", "19:00 - 11:00");
    
    return hr;
    
  }  	
  
    
}
