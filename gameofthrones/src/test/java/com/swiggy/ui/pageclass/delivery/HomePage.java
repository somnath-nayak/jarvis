package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.HomePageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/8/18.
 */
public class HomePage extends AndroidUtil{
	
	public IElement toolbar;

    private AppiumDriver<MobileElement> driver = null;
    private HomePageObject homePageObject = new HomePageObject();

    public HomePage(InitializeUI init) {
        super(init);
        this.driver = init.getAndroidDriver();
        initElements(init.getAppiumDriver(), this, init);
        //PageFactory.initElements(new AppiumFieldDecorator(driver), homePageObject);
    }

    public boolean isHomePage() {
      //  return this.isElementPresent(homePageObject.toolbar);
    	return toolbar.isDisplayed();
    }
}
