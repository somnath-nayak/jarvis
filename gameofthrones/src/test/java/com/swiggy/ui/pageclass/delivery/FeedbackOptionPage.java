package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.FeedbackOptionPageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class FeedbackOptionPage  extends AndroidUtil{
	 public IElement FeedbackTextbox;
	 public IElement Submitbutton;
	 public IElement Resetbutton;
	 public IElement backbutton;
	 public IElement feedbackPage;
	
	
	
	 private AppiumDriver<MobileElement> driver = null;
	    private FeedbackOptionPageObject FeedbackOptionPageObject =new FeedbackOptionPageObject();

	    public FeedbackOptionPage(InitializeUI init) {
	    	super(init);
	    	 initElements(init.getAppiumDriver(), this, init);
	        this.driver = init.getAndroidDriver();
	       
	        //PageFactory.initElements(new AppiumFieldDecorator(driver), FeedbackOptionPageObject);
	    }

	    
	    

	    public FeedbackOptionPage enterfeedback(String feedback) {
	    	
//	        this.sendKeys(driver, FeedbackOptionPageObject.FeedbackTextbox,feedback);
//	         return this;
           this.FeedbackTextbox.sendKeys(feedback);
           return this;
           
	 
	    }
	    
     public FeedbackOptionPage click_onSubmitbutton () {
//	    	
//	        this.click(driver, FeedbackOptionPageObject.Submitbutton);
//	         return this;
    	 
    	 this.Submitbutton.click();
    	 return this;
	 
	    }
     
	    
       public FeedbackOptionPage click_onResetbutton () {
	    	
//	        this.click(driver, FeedbackOptionPageObject.Resetbutton);
//	         return this;
    	   
    	   this.Resetbutton.click();
            return this;	 
	    }
       public FeedbackOptionPage click_onbackbutton () {
//	    	
//	        this.click(driver, FeedbackOptionPageObject.backbutton);
//	         return this;
    	   this.backbutton.click();
	       return this;
	    }
	    
       public boolean isFeedbackPage() {
	       // return this.isElementPresent(driver,FeedbackOptionPageObject.feedbackPage );
    	  return  this.feedbackPage.isDisplayed();
	    }

}
