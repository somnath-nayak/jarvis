package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.LaunchPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/8/18.
 */
public class DeliveryApplaunchPage extends AndroidUtil{
	
	public IElement deny_button;
	public IElement allow_button;
	public IElement current_page;
	

    private AppiumDriver<MobileElement> driver = null;
    private LaunchPageObject launchPageObject = new LaunchPageObject();

    public DeliveryApplaunchPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       
    }

    public void clicOnAllowPermissionButton() {

    	allow_button.click();
    	
    }

    public void clickonDenyPermission() {
    	deny_button.click();
    }

    public void allowAllPermission() {

    	while(checkElementDisplay(allow_button))
    		allow_button.click();
    	}

    public boolean checkElementDisplay(IElement element){
        try{
            return element.isDisplayed();

        }
        catch(Exception e){
            return false;
        }
    }
}
