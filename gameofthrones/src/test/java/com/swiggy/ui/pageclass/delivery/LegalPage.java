package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.LegalPageObject;
import com.swiggy.ui.utils.AndroidUtil;

/**
 * Created by kiran.j on 1/17/18.
 */
public class LegalPage extends AndroidUtil {
	
	
	public IElement legal_title ;
	public IElement general_terms;
	public IElement code_of_conduct;
	public IElement payouts;
	public IElement legal_checkbox ;
	public IElement save_button ;
	public IElement general_terms_title ;
	public IElement Navigateback;

    private AppiumDriver<MobileElement> driver = null;
    private LegalPageObject legalPageObject = new LegalPageObject();

    public LegalPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
       // PageFactory.initElements(new AppiumFieldDecorator(driver), legalPageObject);
    }

    public boolean isLegalPage() {
       // return this.isElementPresent(legalPageObject.legal_title);
    	return this.legal_title.isDisplayed();
    }

    public boolean isGeneralTerms() {
       // return this.isElementPresent(legalPageObject.general_terms_title);
    	return this.general_terms_title.isDisplayed();
    }

    public LegalPage navigateToGeneralTerms() {
//        this.click(legalPageObject.general_terms);
//        return this;
    	
    	this.general_terms.click();
    	 return this;
    }

    public boolean isCodeOfContact() {
       // return this.isElementPresent(driver, legalPageObject.code_of_conduct);
        return this.code_of_conduct.isDisplayed();
    }
    
    public LegalPage navigateToCodeOfContact() {
//        this.click(driver, legalPageObject.code_of_conduct);
//        return this;
    	
    	this.code_of_conduct.click();
    	 return this;
    }
    
    public boolean isPayouts() {
        //return this.isElementPresent(driver, legalPageObject.payouts);
    	return this.payouts.isDisplayed();
        
    }
    public LegalPage navigateToPayoutst() {
//        this.click(driver, legalPageObject.payouts);
//        return this;
    	
    	this.payouts.click();
    	 return this;
    }
    
    
    public LegalPage navigateBack() {
//        this.click(driver, legalPageObject.Navigateback);
//        return this;
    	
    	this.Navigateback.click();
    	 return this;
    }
    public boolean isTnCDisplayed() {
       // return this.isElementPresent(legalPageObject.legal_checkbox) && this.isElementPresent(legalPageObject.save_button);
   return this.legal_checkbox.isDisplayed() && this.save_button.isDisplayed();
    }


}
