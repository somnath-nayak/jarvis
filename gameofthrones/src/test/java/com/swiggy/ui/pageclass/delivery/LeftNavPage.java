package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import com.swiggy.ui.pageobject.delivery.LeftNavPageObject;
import com.swiggy.ui.utils.AndroidUtil;

import java.util.List;

/**
 * Created by kiran.j on 1/10/18.
 */
public class LeftNavPage extends AndroidUtil{

	public IElement item_text ;
	public IElement alert_text ;
    public IElement no ;
    public IElement yes ;
    public List<IElement> by_item_text ;
	public IElement no_button ;
	public IElement yes_button ;
 // public By by_item_text = By.id("design_menu_item_text");
	LiveTasks liveTasks;
	
    private AppiumDriver<MobileElement> driver = null;
    private LeftNavPageObject leftNavPage = new LeftNavPageObject();
    InitializeUI initializeUI;
    public LeftNavPage(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        initializeUI=init;
        this.driver = init.getAndroidDriver();
        liveTasks=new LiveTasks(init);
    }

    public LeftNavPage selectItem(String s) {
    	
    	for(int i=0;i<by_item_text.size();i++){
    		this.scroll(Direction.UP);
    		if(by_item_text.get(i).getText().toString().equalsIgnoreCase(s)){
    			by_item_text.get(i).click();
    		  break;	
    		}
    	}
    	return this;
    	
    	
    	
//        int count = 0, retry = 3;
//        while(count < 3) {
//            this.scroll(Direction.UP);
//            List<MobileElement> ele = driver.findElements(leftNavPage.by_item_text);
//            for (MobileElement e : ele) {
//                if (e.getText().trim().equalsIgnoreCase(s)) {
//                    this.click(e);
//                    break;
//                }
//            }
//            count++;
//        }
//        return this;
    }


    public boolean isLeftNavOpen() {
        return this.isElementPresent(leftNavPage.by_item_text);
    }

    public LeftNavPage stopDuty() {
        this.selectItem(DeConstants.stop_duty);
        this.click(leftNavPage.yes);
        return this;
    }

    public boolean verifyStartDuty() {
//        LiveTasks liveTasks = new LiveTasks(initializeUI);
//        liveTasks.dismissLeftNav().selectLeftNav();
//        this.scroll(Direction.UP);
//       List<MobileElement> ele = driver.findElements(leftNavPage.by_item_text);
//       for(MobileElement e: ele) {
//            if(e.getText().trim().equalsIgnoreCase(DeConstants.start_duty)) {
//                return true;
//            }
//        }
        
    	LiveTasks liveTasks = new LiveTasks(initializeUI);
        liveTasks.dismissLeftNav().selectLeftNav();
        this.scroll(Direction.UP);
         for(int i=0;i<by_item_text.size();i++){
    		if(by_item_text.get(i).getText().toString().equalsIgnoreCase(DeConstants.start_duty)){
    			return true;	
    		}
    	}
        return false;
    }

    public LeftNavPage tapOnYesButton() {
      //  this.click(leftNavPage.yes_button);
        this.yes_button.click();
    	return this;
    }

    public LeftNavPage tapOnNoButton() {
       // this.click(leftNavPage.no_button);
    this.no_button.click();
    	return this;
    }

    public void logout() {
        selectItem(DeConstants.logout);
        tapOnYesButton();
        tapOnNoButton();
    }
    
    
    public boolean verifyitemsopt() {
//        LiveTasks liveTasks = new LiveTasks(driver);
//        liveTasks.dismissLeftNav().selectLeftNav();
//        this.scroll(driver, Direction.UP);
//        List<MobileElement> ele = driver.findElements(leftNavPage.by_item_text);
//        String[] opt = DeConstants.opt;
//        for(MobileElement e: ele) {
//        	for (String lst: opt)
//        	{
//                if(e.getText().trim().equalsIgnoreCase(lst)) {
//                	return true;
//            }
//           }
//        }
//        return false;
    	
    	liveTasks.dismissLeftNav().selectLeftNav();
    	this.scroll(Direction.UP);
        String[] opt = DeConstants.opt;
    	for(IElement e: by_item_text)
    	{
    		for (String lst: opt)
            	{
                    if(e.getText().trim().equalsIgnoreCase(lst)) {
                   	return true;
                     }
              }
    	
        }
    
    	return false;

    }

}




