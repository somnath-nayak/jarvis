package com.swiggy.ui.pageclass.delivery;

import org.openqa.selenium.support.PageFactory;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.swiggy.ui.pageobject.delivery.HambergurPageObject;
import com.swiggy.ui.pageobject.delivery.LeftNavPageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class HambergurPage extends AndroidUtil
{

	public IElement left_nav ;
	public IElement background ;

    private AppiumDriver<MobileElement> driver = null;
    private HambergurPageObject HambergurPageObject = new HambergurPageObject();

    public HambergurPage(InitializeUI init) {
    	
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
        //PageFactory.initElements(new AppiumFieldDecorator(driver), HambergurPageObject);
    }

    public HambergurPage clickOn_Hambergur()
  {
//    	this.click(driver,HambergurPageObject.left_nav);
//        return this;
    this.left_nav.click();
    return this;   }
    
    public HambergurPage clickOn_background()
    {
//    	this.click(driver,HambergurPageObject.background);
//        return this;
    	
    	this.background.click();
    	return this;
    }
    
    
    
	
}
