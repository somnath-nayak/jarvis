package com.swiggy.ui.pageclass.delivery;

import framework.gameofthrones.Aegon.IElement;
import framework.gameofthrones.Aegon.InitializeUI;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import com.swiggy.ui.pageobject.delivery.NewTaskPageObject;
import com.swiggy.ui.utils.AndroidUtil;

public class Loginhistory extends AndroidUtil{
	
	
	public IElement navigate;
	public IElement thisweek;
	public IElement backbutton ;
	public IElement prev_week;
	public IElement week3;
	public IElement week4;

    private AppiumDriver<MobileElement> driver = null;
    
    public Loginhistory(InitializeUI init) {
        super(init);
        initElements(init.getAppiumDriver(), this, init);
        this.driver = init.getAndroidDriver();
      
         }
    

    public void clickonTodaysofor() {
    	navigate.click();
    }


}
