package com.swiggy.ui.pojoClasses.superapp;

public class Data
{
    private WeeklyEarningsData[] weeklyEarningsData;

    public WeeklyEarningsData[] getWeeklyEarningsData ()
    {
        return weeklyEarningsData;
    }

    public void setWeeklyEarningsData (WeeklyEarningsData[] weeklyEarningsData)
    {
        this.weeklyEarningsData = weeklyEarningsData;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [weeklyEarningsData = "+weeklyEarningsData+"]";
    }
}

