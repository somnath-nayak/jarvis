package com.swiggy.ui.pojoClasses;

public class Widget
{

    private String subTitle;

    private String imageId;

    private String collapsible;

    private String name;


    private String type;

    private Entities[] entities;



    public String getSubTitle ()
    {
        return subTitle;
    }

    public void setSubTitle (String subTitle)
    {
        this.subTitle = subTitle;
    }


    public String getCollapsible ()
    {
        return collapsible;
    }

    public void setCollapsible (String collapsible)
    {
        this.collapsible = collapsible;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }




    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public Entities[] getEntities ()
    {
        return entities;
    }

    public void setEntities (Entities[] entities)
    {
        this.entities = entities;
    }


}
