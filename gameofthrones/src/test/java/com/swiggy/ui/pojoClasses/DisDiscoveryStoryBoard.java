package com.swiggy.ui.pojoClasses;

import java.util.ArrayList;

public class DisDiscoveryStoryBoard {

    private String headerStoryStartCard;
    private String depthOfStory;
    private String descriptionStoryStartCard;
    private String termsAndConditionsTextStoryStartCard;
    private String swipeTextStoryStartCard;
    private ArrayList<String> restaurantName;
    private ArrayList<String> locality;
    private ArrayList<String> td;
    private ArrayList<String> avgRating;

    public ArrayList<String> getArea() {
        return area;
    }

    public void setArea(ArrayList<String> area) {
        this.area = area;
    }

    private ArrayList<String> area;

    public String getHeaderStoryStartCard() {
        return headerStoryStartCard;
    }

    public void setHeaderStoryStartCard(String headerStoryStartCard) {
        this.headerStoryStartCard = headerStoryStartCard;
    }

    public String getDepthOfStory() {
        return depthOfStory;
    }

    public void setDepthOfStory(String depthOfStory) {
        this.depthOfStory = depthOfStory;
    }

    public String getDescriptionStoryStartCard() {
        return descriptionStoryStartCard;
    }

    public void setDescriptionStoryStartCard(String descriptionStoryStartCard) {
        this.descriptionStoryStartCard = descriptionStoryStartCard;
    }

    public String getTermsAndConditionsTextStoryStartCard() {
        return termsAndConditionsTextStoryStartCard;
    }

    public void setTermsAndConditionsTextStoryStartCard(String termsAndConditionsTextStoryStartCard) {
        this.termsAndConditionsTextStoryStartCard = termsAndConditionsTextStoryStartCard;
    }

    public String getSwipeTextStoryStartCard() {
        return swipeTextStoryStartCard;
    }

    public void setSwipeTextStoryStartCard(String swipeTextStoryStartCard) {
        this.swipeTextStoryStartCard = swipeTextStoryStartCard;
    }

    public ArrayList<String> getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(ArrayList<String> restaurantName) {
        this.restaurantName = restaurantName;
    }

    public ArrayList<String> getLocality() {
        return locality;
    }

    public void setLocality(ArrayList<String> locality) {
        this.locality = locality;
    }

    public ArrayList<String> getTd() {
        return td;
    }

    public void setTd(ArrayList<String> td) {
        this.td = td;
    }

    public ArrayList<String> getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(ArrayList<String> avgRating) {
        this.avgRating = avgRating;
    }

    public ArrayList<String> getFirstItem() {
        return firstItem;
    }

    public void setFirstItem(ArrayList<String> firstItem) {
        this.firstItem = firstItem;
    }

    public String getHeaderStoryEndCard() {
        return headerStoryEndCard;
    }

    public void setHeaderStoryEndCard(String headerStoryEndCard) {
        this.headerStoryEndCard = headerStoryEndCard;
    }

    public String getDescriptionStoryEndCard() {
        return descriptionStoryEndCard;
    }

    public void setDescriptionStoryEndCard(String descriptionStoryEndCard) {
        this.descriptionStoryEndCard = descriptionStoryEndCard;
    }

    public ArrayList<String> getCardsHeaderStoryEndCard() {
        return cardsHeaderStoryEndCard;
    }

    public void setCardsHeaderStoryEndCard(ArrayList<String> cardsHeaderStoryEndCard) {
        this.cardsHeaderStoryEndCard = cardsHeaderStoryEndCard;
    }

    public ArrayList<String> getCardsstoryDepthStoryEndCard() {
        return cardsstoryDepthStoryEndCard;
    }

    public void setCardsstoryDepthStoryEndCard(ArrayList<String> cardsstoryDepthStoryEndCard) {
        this.cardsstoryDepthStoryEndCard = cardsstoryDepthStoryEndCard;
    }

    private ArrayList<String> firstItem;
    private String headerStoryEndCard;
    private String descriptionStoryEndCard;
    private ArrayList<String> cardsHeaderStoryEndCard;
    private ArrayList<String> cardsstoryDepthStoryEndCard;








}
