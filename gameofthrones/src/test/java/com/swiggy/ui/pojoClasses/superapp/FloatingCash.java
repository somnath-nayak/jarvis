package com.swiggy.ui.pojoClasses.superapp;

import com.tectonica.core.Hash;

import java.util.ArrayList;
import java.util.HashMap;

public class FloatingCash {

    private ArrayList<String> channelLabel;
    private ArrayList<String> channellist;
    private HashMap<Integer,ArrayList<String>> channelDetail;
    private HashMap<Integer,ArrayList<String>> depositCode;

    public HashMap<Integer, ArrayList<String>> getDepositCode() {
        return depositCode;
    }

    public void setDepositCode(HashMap<Integer, ArrayList<String>> depositCode) {
        this.depositCode = depositCode;
    }




    public ArrayList<String> getChannelLabel() {
        return channelLabel;
    }

    public void setChannelLabel(ArrayList<String> channelLabel) {
        this.channelLabel = channelLabel;
    }

    public ArrayList<String> getChannellist() {
        return channellist;
    }

    public void setChannellist(ArrayList<String> channellist) {
        this.channellist = channellist;
    }

    public HashMap<Integer, ArrayList<String>> getChannelDetail() {
        return channelDetail;
    }

    public void setChannelDetail(HashMap<Integer, ArrayList<String>> channelDetail) {
        this.channelDetail = channelDetail;
    }










}
