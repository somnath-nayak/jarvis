package com.swiggy.ui.pojoClasses;

import java.util.ArrayList;

public class EDVOMeal {

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    private String mealName;
    private String tagText;
    private String communicationText;
    private String mainText;
    private String subText;

    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public String getCommunicationText() {
        return communicationText;
    }

    public void setCommunicationText(String communicationText) {
        this.communicationText = communicationText;
    }

    public String getMainText() {
        return mainText;
    }

    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    public String getSubText() {
        return subText;
    }

    public void setSubText(String subText) {
        this.subText = subText;
    }

    public String getExitPage_MainText() {
        return exitPage_MainText;
    }

    public void setExitPage_MainText(String exitPage_MainText) {
        this.exitPage_MainText = exitPage_MainText;
    }

    public String getExitPage_subText() {
        return exitPage_subText;
    }

    public void setExitPage_subText(String exitPage_subText) {
        this.exitPage_subText = exitPage_subText;
    }

    public ArrayList<String> getScreens_title() {
        return screens_title;
    }

    public void setScreens_title(ArrayList<String> screens_title) {
        this.screens_title = screens_title;
    }

    public ArrayList<String> getName() {
        return name;
    }

    public void setName(ArrayList<String> name) {
        this.name = name;
    }

    private String exitPage_MainText;
    private String exitPage_subText;
    private ArrayList<String> screens_title;

    public ArrayList<String> getScreens_description() {
        return screens_description;
    }

    public void setScreens_description(ArrayList<String> screens_description) {
        this.screens_description = screens_description;
    }

    private ArrayList<String> screens_description;
    private ArrayList<String> name;

    public ArrayList<String> getPizzaDescription() {
        return pizzaDescription;
    }

    public void setPizzaDescription(ArrayList<String> pizzaDescription) {
        this.pizzaDescription = pizzaDescription;
    }

    private ArrayList<String> pizzaDescription;
    //private String screens_description;




}
