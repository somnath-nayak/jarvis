package com.swiggy.ui.pojoClasses;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EVDO {
    private String widgets;

    private String subTitle;

    private String imageId;

    private String collapsible;

    private String name;

    private String perRow;

    private String type;

    private Entities[] entities;

    public String getWidgets() {
        return widgets;
    }

    public void setWidgets(String widgets) {
        this.widgets = widgets;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getCollapsible() {
        return collapsible;
    }

    public void setCollapsible(String collapsible) {
        this.collapsible = collapsible;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPerRow() {
        return perRow;
    }

    public void setPerRow(String perRow) {
        this.perRow = perRow;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Entities[] getEntities() {
        return entities;
    }

    public void setEntities(Entities[] entities) {
        this.entities = entities;
    }

    @Override
    public String toString() {
        return "ClassPojo [widgets = " + widgets + ", subTitle = " + subTitle + ", imageId = " + imageId + ", collapsible = " + collapsible + ", name = " + name + ", perRow = " + perRow + ", type = " + type + ", entities = " + entities + "]";
    }
}