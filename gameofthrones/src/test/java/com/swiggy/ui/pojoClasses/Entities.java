package com.swiggy.ui.pojoClasses;

public class Entities
{
    private String id;

    private String text;

    private String imageId;

    private String subText;

    private String name;

    private String showRibbon;

    private String showImg;

    private String type;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getImageId ()
    {
        return imageId;
    }

    public void setImageId (String imageId)
    {
        this.imageId = imageId;
    }

    public String getSubText ()
    {
        return subText;
    }

    public void setSubText (String subText)
    {
        this.subText = subText;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getShowRibbon ()
    {
        return showRibbon;
    }

    public void setShowRibbon (String showRibbon)
    {
        this.showRibbon = showRibbon;
    }

    public String getShowImg ()
    {
        return showImg;
    }

    public void setShowImg (String showImg)
    {
        this.showImg = showImg;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", text = "+text+", imageId = "+imageId+", subText = "+subText+", name = "+name+", showRibbon = "+showRibbon+", showImg = "+showImg+", type = "+type+"]";
    }
}