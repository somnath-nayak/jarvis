package com.swiggy.ui.pojoClasses;

import java.util.ArrayList;

public class Restaurantmenu {
    private String id;
    private String name;
    private String avgRatingString;
    private String imageId;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String text;


    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvgRatingString() {
        return avgRatingString;
    }

    public void setAvgRatingString(String avgRatingString) {
        this.avgRatingString = avgRatingString;
    }

    public String getTotalRatingsString() {
        return totalRatingsString;
    }

    public void setTotalRatingsString(String totalRatingsString) {
        this.totalRatingsString = totalRatingsString;
    }

    public int getCostForTwoMsg() {
        return costForTwoMsg;
    }

    public void setCostForTwoMsg(int costForTwoMsg) {
        this.costForTwoMsg = costForTwoMsg;
    }

    public ArrayList<String> getCuisines() {
        return cuisines;
    }

    public void setCuisines(ArrayList<String> cuisines) {
        this.cuisines = cuisines;
    }

    public String getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int opened) {
        this.opened = opened;
    }

    public boolean isVeg() {
        return isVeg;
    }

    public void setVeg(boolean veg) {
        isVeg = veg;
    }

    public String getTradeCampaignHeaders() {
        return tradeCampaignHeaders;
    }

    public void setTradeCampaignHeaders(String tradeCampaignHeaders) {
        this.tradeCampaignHeaders = tradeCampaignHeaders;
    }

    public String getSlaString() {
        return slaString;
    }

    public void setSlaString(String slaString) {
        this.slaString = slaString;
    }

    public String getLongDistance() {
        return longDistance;
    }

    public void setLongDistance(String longDistance) {
        this.longDistance = longDistance;
    }

    public ArrayList<String> getCollection() {
        return collection;
    }

    public void setCollection(ArrayList<String> collection) {
        this.collection = collection;
    }

    private String totalRatingsString;
    private int costForTwoMsg;
    private ArrayList<String> cuisines;
    private String avgRating;
    private int opened;
    private boolean isVeg;
    private String tradeCampaignHeaders;
    private String slaString;
    private String longDistance;
    private ArrayList<String> collection;

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    private ArrayList<String> items;

    public ArrayList<Integer> getIsVegMenu() {
        return isVegMenu;
    }

    public void setIsVegMenu(ArrayList<Integer> isVegMenu) {
        this.isVegMenu = isVegMenu;
    }



    private ArrayList<Integer> isVegMenu;

    private String edvoName;
    private String subTitle;
    private ArrayList<Integer> edvoOfferId;

    public String getEdvoName() {
        return edvoName;
    }

    public void setEdvoName(String edvoName) {
        this.edvoName = edvoName;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public ArrayList<Integer> getEdvoOfferId() {
        return edvoOfferId;
    }

    public void setEdvoOfferId(ArrayList<Integer> edvoOfferId) {
        this.edvoOfferId = edvoOfferId;
    }

    public ArrayList<String> getEdvoOfferText() {
        return edvoOfferText;
    }

    public void setEdvoOfferText(ArrayList<String> edvoOfferText) {
        this.edvoOfferText = edvoOfferText;
    }

    public ArrayList<String> getEdvoOfferSubText() {
        return edvoOfferSubText;
    }

    public void setEdvoOfferSubText(ArrayList<String> edvoOfferSubText) {
        this.edvoOfferSubText = edvoOfferSubText;
    }

    private ArrayList<String> edvoOfferText;
    private ArrayList<String> edvoOfferSubText;











}
