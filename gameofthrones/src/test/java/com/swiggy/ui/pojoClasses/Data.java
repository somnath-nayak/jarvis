package com.swiggy.ui.pojoClasses;

import java.util.ArrayList;

public class Data
{
    private ArrayList<Addresses> addresses;

    public ArrayList<Addresses> getAddresses ()
    {
        return addresses;
    }

    public void setAddresses (ArrayList<Addresses> addresses)
    {
        this.addresses = addresses;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [addresses = "+addresses+"]";
    }
}
