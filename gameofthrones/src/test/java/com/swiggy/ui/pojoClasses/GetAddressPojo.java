package com.swiggy.ui.pojoClasses;

public class GetAddressPojo
{
    private String statusCode;

    private String sid;

    private Data data;

    private String tid;

    private String deviceId;

    private String statusMessage;

    public String getStatusCode ()
    {
        return statusCode;
    }

    public void setStatusCode (String statusCode)
    {
        this.statusCode = statusCode;
    }

    public String getSid ()
    {
        return sid;
    }

    public void setSid (String sid)
    {
        this.sid = sid;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    public String getTid ()
    {
        return tid;
    }

    public void setTid (String tid)
    {
        this.tid = tid;
    }

    public String getDeviceId ()
    {
        return deviceId;
    }

    public void setDeviceId (String deviceId)
    {
        this.deviceId = deviceId;
    }

    public String getStatusMessage ()
    {
        return statusMessage;
    }

    public void setStatusMessage (String statusMessage)
    {
        this.statusMessage = statusMessage;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [statusCode = "+statusCode+", sid = "+sid+", data = "+data+", tid = "+tid+", deviceId = "+deviceId+", statusMessage = "+statusMessage+"]";
    }
}