package com.swiggy.ui.pojoClasses;

import io.gatling.jms.request.ObjectJmsMessage;

import java.util.ArrayList;
import java.util.HashMap;

public class Menulist {

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ArrayList<String> getCuisines() {
        return cuisines;
    }

    public void setCuisines(ArrayList<String> cuisines) {
        this.cuisines = cuisines;
    }

    public String getCostForTwoString() {
        return costForTwoString;
    }

    public void setCostForTwoString(String costForTwoString) {
        this.costForTwoString = costForTwoString;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    private String cardType;
    private String type;
    private String restaurantId;
    private String restaurantName;
    private String uuid;
    private ArrayList<String> cuisines;
    private String costForTwoString;
    private String deliveryTime;
    public HashMap<String,String> carousel;

    public ArrayList<Long> getPreorderdate() {
        return preorderdate;
    }

    public void setPreorderdate(ArrayList<Long> preorderdate) {
        this.preorderdate = preorderdate;
    }

    public ArrayList<Long> getSlots() {
        return slots;
    }

    public void setSlots(ArrayList<Long> slots) {
        this.slots = slots;
    }

    public ArrayList<Long> preorderdate;
    public ArrayList<Long> slots;

    public int getChainCount() {
        return chainCount;
    }

    public void setChainCount(int chainCount) {
        this.chainCount = chainCount;
    }

    public int chainCount;
    ArrayList<PreorderTimer> preorderTimers;
    HashMap<Long,ArrayList<PreorderTimer>> hm_preorderTimers;


    public ArrayList<PreorderTimer> getPreorderTimers() {
        return preorderTimers;
    }

    public void setPreorderTimers(ArrayList<PreorderTimer> preorderTimers) {
        this.preorderTimers = preorderTimers;
    }

    public HashMap<Long, ArrayList<PreorderTimer>> getHm_preorderTimers() {
        return hm_preorderTimers;
    }

    public void setHm_preorderTimers(HashMap<Long, ArrayList<PreorderTimer>> hm_preorderTimers) {
        this.hm_preorderTimers = hm_preorderTimers;
    }










}
