package com.swiggy.ui.pojoClasses;

public class Addresses
{
    private String recalculation_required;

    private String created_on;

    private String rainMode;

    private String lng;

    private String is_edited;

    private String reverse_geo_code_failed;

    private String id;

    private String annotation;

    private String area;

    private String estimated_sla_min;

    private String image_url;

    private String name;

    private String updated_by;

    private String user_id;

    private String lat;

    private String estimated_sla;

    private String assured;

    private String landmark;

    private String estimated_sla_max;

    private String delivery_valid;

    private String is_deleted;

    private String default_value;

    private String address;

    private String updated_on;

    private String mobile;

    public String getFlat_no() {
        return flat_no;
    }

    public void setFlat_no(String flat_no) {
        this.flat_no = flat_no;
    }

    private String flat_no;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    private String city;

    public String getRecalculation_required ()
    {
        return recalculation_required;
    }

    public void setRecalculation_required (String recalculation_required)
    {
        this.recalculation_required = recalculation_required;
    }

    public String getCreated_on ()
    {
        return created_on;
    }

    public void setCreated_on (String created_on)
    {
        this.created_on = created_on;
    }

    public String getRainMode ()
    {
        return rainMode;
    }

    public void setRainMode (String rainMode)
    {
        this.rainMode = rainMode;
    }

    public String getLng ()
    {
        return lng;
    }

    public void setLng (String lng)
    {
        this.lng = lng;
    }

    public String getIs_edited ()
    {
        return is_edited;
    }

    public void setIs_edited (String is_edited)
    {
        this.is_edited = is_edited;
    }

    public String getReverse_geo_code_failed ()
    {
        return reverse_geo_code_failed;
    }

    public void setReverse_geo_code_failed (String reverse_geo_code_failed)
    {
        this.reverse_geo_code_failed = reverse_geo_code_failed;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAnnotation ()
    {
        return annotation;
    }

    public void setAnnotation (String annotation)
    {
        this.annotation = annotation;
    }

    public String getArea ()
    {
        return area;
    }

    public void setArea (String area)
    {
        this.area = area;
    }

    public String getEstimated_sla_min ()
    {
        return estimated_sla_min;
    }

    public void setEstimated_sla_min (String estimated_sla_min)
    {
        this.estimated_sla_min = estimated_sla_min;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUpdated_by ()
    {
        return updated_by;
    }

    public void setUpdated_by (String updated_by)
    {
        this.updated_by = updated_by;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getEstimated_sla ()
    {
        return estimated_sla;
    }

    public void setEstimated_sla (String estimated_sla)
    {
        this.estimated_sla = estimated_sla;
    }

    public String getAssured ()
    {
        return assured;
    }

    public void setAssured (String assured)
    {
        this.assured = assured;
    }

    public String getLandmark ()
    {
        return landmark;
    }

    public void setLandmark (String landmark)
    {
        this.landmark = landmark;
    }

    public String getEstimated_sla_max ()
    {
        return estimated_sla_max;
    }

    public void setEstimated_sla_max (String estimated_sla_max)
    {
        this.estimated_sla_max = estimated_sla_max;
    }

    public String getDelivery_valid ()
    {
        return delivery_valid;
    }

    public void setDelivery_valid (String delivery_valid)
    {
        this.delivery_valid = delivery_valid;
    }

    public String getIs_deleted ()
    {
        return is_deleted;
    }

    public void setIs_deleted (String is_deleted)
    {
        this.is_deleted = is_deleted;
    }

    public String getDefault ()
    {
        return default_value;
    }

    public void setDefault (String default_value)
    {
        this.default_value = default_value;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getUpdated_on ()
    {
        return updated_on;
    }

    public void setUpdated_on (String updated_on)
    {
        this.updated_on = updated_on;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [recalculation_required = "+recalculation_required+", created_on = "+created_on+", rainMode = "+rainMode+", lng = "+lng+", is_edited = "+is_edited+", reverse_geo_code_failed = "+reverse_geo_code_failed+", id = "+id+", annotation = "+annotation+", area = "+area+", estimated_sla_min = "+estimated_sla_min+", image_url = "+image_url+", name = "+name+", updated_by = "+updated_by+", user_id = "+user_id+", lat = "+lat+", estimated_sla = "+estimated_sla+", assured = "+assured+", landmark = "+landmark+", estimated_sla_max = "+estimated_sla_max+", delivery_valid = "+delivery_valid+", is_deleted = "+is_deleted+", default = "+default_value+", address = "+address+", updated_on = "+updated_on+", mobile = "+mobile+"]";
    }
}

