package com.swiggy.ui.pojoClasses;

public class SwiggyPop {
    private  String name;
    private  String restaurantName;
    private  String restaurantId;
    private  String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getNextOpenMessage() {
        return nextOpenMessage;
    }

    public void setNextOpenMessage(String nextOpenMessage) {
        this.nextOpenMessage = nextOpenMessage;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    private  int price;
    private  String  nextOpenMessage;
    private  boolean isOpened;
}
