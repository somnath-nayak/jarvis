package com.swiggy.ui.pojoClasses;

import java.util.ArrayList;

/**
 * Created by nidhi.singh on 3/14/18.
 */
public class DishDiscovery {
    private String Header;
    private String Description;
    private ArrayList<String> CollectionName;

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public ArrayList<String> getCollectionName() {
        return CollectionName;
    }

    public void setCollectionName(ArrayList<String> collectionName) {
        CollectionName = collectionName;
    }

    public ArrayList<String> getCollectionId() {
        return CollectionId;
    }

    public void setCollectionId(ArrayList<String> collectionId) {
        CollectionId = collectionId;
    }

    public ArrayList<String> getCollectionSubName() {
        return CollectionSubName;
    }

    public void setCollectionSubName(ArrayList<String> collectionSubName) {
        CollectionSubName = collectionSubName;
    }

    public ArrayList<String> getMenuletCount() {
        return MenuletCount;
    }

    public void setMenuletCount(ArrayList<String> menuletCount) {
        MenuletCount = menuletCount;
    }

    private ArrayList<String> CollectionId;
    private ArrayList<String> CollectionSubName;
    private ArrayList<String> MenuletCount ;
    private ArrayList<String> type ;

    public ArrayList<String> getType() {
        return type;
    }

    public void setType(ArrayList<String> type) {
        this.type = type;
    }

    public ArrayList<String> getParentCollectionId() {
        return parentCollectionId;
    }

    public void setParentCollectionId(ArrayList<String> parentCollectionId) {
        this.parentCollectionId = parentCollectionId;
    }

    private ArrayList<String> parentCollectionId;



}
