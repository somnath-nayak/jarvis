package com.swiggy.ui.pojoClasses.superapp;

public class WeeklyEarningsData
{
    private String week_total;

    private String processing_window;

    public String getWeek_total ()
    {
        return week_total;
    }

    public void setWeek_total (String week_total)
    {
        this.week_total = week_total;
    }

    public String getProcessing_window ()
    {
        return processing_window;
    }

    public void setProcessing_window (String processing_window)
    {
        this.processing_window = processing_window;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [week_total = "+week_total+", processing_window = "+processing_window+"]";
    }
}
