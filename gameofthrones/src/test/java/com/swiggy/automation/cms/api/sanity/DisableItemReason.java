package com.swiggy.automation.cms.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DisableItemReason extends RestTestHelper {

	@DataProvider(name = "disableItemReason")
	public static Object[][] get_restaurants_for_area()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("disableItemReason");
	}

	@Test(priority = 1, dataProvider = "disableItemReason", groups = "Sanity")
	public void disableItemReason(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData, new String[] { "status" });
			Assert.assertTrue(isSuccess, "disableItemReason");

		} catch (Exception e) {
			Assert.fail();
		}

	}

}
