package com.swiggy.automation.cms.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class GetRestaurantForArea extends RestTestHelper {

	@DataProvider(name = "restaurantForArea")
	public static Object[][] get_restaurants_for_area()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("getrestaurantsforarea");
	}

	@Test(priority = 1, dataProvider = "restaurantForArea", groups = "Sanity")
	public void restaurantForArea(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "restaurantForArea");

		} catch (Exception e) {
			Assert.fail();
		}

	}
}
