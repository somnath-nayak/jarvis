package com.swiggy.automation.cms.gst.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class FetchRestaurantGST extends RestTestHelper {
	@DataProvider(name = "Get_GST_Details_By_RestId") // name can be any thing
	// eg A
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Get_GST_Details_By_RestId",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "Get_GST_Details_By_RestId")
	public void testSearchGST_Rest(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed
		// Assert.assertTrue(status, "testSearchRestDetails" + smokeTest);
		// status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
		// actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testSearchRestDetails" + sanityTest);

	}

}
