package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class VariantServiceSanity extends RestTestHelper {
	String updatedName = null;
	int VariantId = 0;

	@DataProvider(name = "CREATE_VARIANT_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANT_SANITY", "SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_VARIANT_SANITY")
	public void testCreateVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest;

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "third_party_id",
				updatedRequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
		VariantId = actualData.get("data").get("id").asInt();
	}

	@DataProvider(name = "UPDATE_VARIANT_SANITY")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_VARIANT_SANITY", "SANITY");
	}

	@Test(priority = 2, dataProvider = "UPDATE_VARIANT_SANITY")
	public void testUpdateVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VariantId);

		System.out.println("variant_id" + VariantId);
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "requestbody", "name",
				updatedRequest.get("requestbody").get("name").asText()
						+ System.currentTimeMillis());

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "requestbody", "third_party_id",
				updatedRequest.get("requestbody").get("third_party_id").asText()
						+ System.currentTimeMillis());

		System.out.println(
				"updatedRequest updatedRequestupdatedRequestupdatedRequest"
						+ updatedRequest);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateVariant" + sanityTest);

	}

	@DataProvider(name = "GET_VARIANT_BYID_SANITY")
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_VARIANT_BYID_SANITY",
				"SANITY");
	}

	@Test(priority = 4, dataProvider = "GET_VARIANT_BYID_SANITY")
	public void testGetVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VariantId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetVariantByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetVariantByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_VARIANT_SANITY")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_VARIANT_SANITY", "SANITY");
	}

	@Test(priority = 5, dataProvider = "DELETE_VARIANT_SANITY")
	public void testDeleteVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VariantId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteVariant" + sanityTest);
	}

}
