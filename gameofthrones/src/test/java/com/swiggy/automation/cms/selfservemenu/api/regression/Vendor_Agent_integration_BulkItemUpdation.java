package com.swiggy.automation.cms.selfservemenu.api.regression;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.CMSDAO;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class Vendor_Agent_integration_BulkItemUpdation extends RestTestHelper {

	int id = 0;
	String update = "update";

	@DataProvider(name = "CMS_Vendor_Update_item_Generate_Ticket")
	public static Object[][] CreataeItemFromVendorAPI()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Vendor_Update_item_Generate_Ticket", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Vendor_Update_item_Generate_Ticket")
	public void CreataeItemFromVendorAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Update_item_to_generate_ticket" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"Update_item_to_generate_ticket" + sanityTest);

	}

	@DataProvider(name = "CMS_Vendor_item_Submit_Ticket")
	public static Object[][] SubmitTicketAPI()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Vendor_item_Submit_Ticket",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Vendor_item_Submit_Ticket")
	public void SubmitTicketAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		id = CMSDAO.getTicketCreatedByVendor("update");
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic dmVuZG9yLXN5c3RlbTpzZWxmc2VydmUtc3lzdGVt");

		int a1 = jsonRequestData.get("requestbody").get("tickets").get(0)
				.get("id").asInt();
		System.out.println("Hello :" + a1);

		System.out.println("ttttttttttttttttttttttttttttttttttttttt	" + a1);
		JSONObject a = new JSONObject(jsonRequestData.toString());

		JsonNode updatedrequest = RestTestUtil
				.updateRequestDataBeforeSend(jsonRequestData, "a1", id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Update_item_to_generate_ticket" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"Update_item_to_generate_ticket" + sanityTest);

	}
}
