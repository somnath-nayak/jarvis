package com.swiggy.automation.cms.selfservemenu.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class TicketingService extends RestTestHelper {

	@DataProvider(name = "CMS_TMS_Get_Ticket_CommentBy_Ticket_Id")
	public static Object[][] getComment_By_Ticket_ID()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_TMS_Get_Ticket_CommentBy_Ticket_Id", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_TMS_Get_Ticket_CommentBy_Ticket_Id")
	public void Test_getComment_By_Ticket_ID(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		// JsonNode actualData = RestTestUtil.sendDataAsPathParam(apiToExecute,
		// jsonRequestData);
		// JSONObject a = new JSONObject(jsonRequestData.toString());
		// JsonArray array=a.getJSONArray("data").getJSONArray(0);
		// boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
		// actualData);
		// Assert.assertTrue(status, "testTicketComment" + smokeTest);
		// status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
		// actualData);
		//
		// status = a.getJSONArray("data").getJSONObject(0)
		// .equals(expectedData.get(smokeTest));
		//
		// Assert.assertTrue(status, "testTicketComment" + sanityTest);
		//
		// JSONObject a = new JSONObject(jsonRequestData.toString());
		// a.getJSONArray("data").getJSONArray(0).equals(expectedData);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCommentByTicketID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCommentByTicketID" + sanityTest);

	}

	// @DataProvider(name = "CMS_TMS_Get_Ticket_CommentBy_Comment_Id")
	// public static Object[][] getTicket_By_TicketID()
	// throws SwiggyAPIAutomationException {
	// return APIDataReader.getDataSetData(
	// "CMS_TMS_Get_Ticket_CommentBy_Comment_Id", "REGRESSION");
	// }
	//
	// @Test(priority = 2, dataProvider =
	// "CMS_TMS_Get_Ticket_CommentBy_Comment_Id")
	// public void Test_Ticket_By_TicketID(String dataGroup, String
	// apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// JsonNode actualData = RestTestUtil.sendDataAsPathParam(apiToExecute,
	// jsonRequestData);
	//
	// System.out.println(actualData);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
	// actualData);
	//
	// Assert.assertTrue(status, "testCommentByCommentID" + smokeTest);
	// status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
	// actualData);
	// Assert.assertTrue(status, "testCommentByCommentID" + sanityTest);
	//
	// }

}
