package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ItemServiceRegressionTest extends RestTestHelper {
	public static int item_id1 = 0;

	int item_id = 0;

	// ------Start of CREATE_ITEM_SANITY
	@DataProvider(name = "CREATE_ITEM_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ITEM_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_ITEM_REGRESSION")
	public void testCreateItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedrequest, "third_party_id",
				updatedrequest.get("third_party_id").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + sanityTest);
		item_id = actualData.get("data").get("id").asInt();
	}

	// -------End of CREATE_ITEM_SANITY
	@DataProvider(name = "CREATE_ITEM_REGRESSION1")
	public static Object[][] getDataitem1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ITEM_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_ITEM_REGRESSION1")
	public void testCreateItem1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData;
		if (dataSetName != "INVALID_INPUT(name field blank)") {
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "name",
					jsonRequestData.get("name").asText()
							+ System.currentTimeMillis());

			actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, updatedrequest);
		} else
			actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + sanityTest);
		item_id1 = actualData.get("data").get("id").asInt();
	}

	// ------Start of UPDATE_ITEM_SANITY
	@DataProvider(name = "UPDATE_ITEM_REGRESSION")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ITEM_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "UPDATE_ITEM_REGRESSION")
	public void testUpdateitem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateitem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateitem" + sanityTest);
	}
	// -------End of UPDATE_ITEM_SANITY

	// ------Start of SEARCH_ITEM_SANITY
	@DataProvider(name = "Search_ITEM_SANITY") // name can be any thing eg A
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_ITEM_SANITY", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "Search_ITEM_SANITY")
	public void testSearchitem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed

		Assert.assertTrue(status, "testSearchitem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testSearchitem" + sanityTest);
	}
	// -------End of SEARCH_ITEM_SANITY

	@DataProvider(name = "GET_ITEM_BYID_SANITY") // name can be any thing eg A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ITEM_BYID_SANITY",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "GET_ITEM_BYID_SANITY")
	public void testGetItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed
		Assert.assertTrue(status, "testGetItemById" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetItemById" + sanityTest);
	}

	@DataProvider(name = "DELETE_ITEM") // name can be any thing eg A
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ITEM", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "DELETE_ITEM")
	public void testDeleteItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", item_id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed

		Assert.assertTrue(status, "testDeleteItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testDeleteItem" + sanityTest);
	}

}
