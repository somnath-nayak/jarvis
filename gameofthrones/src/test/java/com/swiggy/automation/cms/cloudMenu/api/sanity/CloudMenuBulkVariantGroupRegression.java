package com.swiggy.automation.cms.cloudMenu.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit Chaubey
 *
 */
public class CloudMenuBulkVariantGroupRegression extends RestTestHelper

{

	@DataProvider(name = "CloudMenu_Create_VariantGroup_Regression")
	public static Object[][] Create_VariantGroup_Regression()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CloudMenu_Create_VariantGroup_Regression", "REGRESSION");

	}

	@Test(priority = 1, dataProvider = "CloudMenu_Create_VariantGroup_Regression")
	public void Create_VariantGroup_Regression(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Create_VariantGroup_Regression" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"Create_VariantGroup_Regression" + sanityTest);

		System.out.println(actualData.get("data"));
	}

	@DataProvider(name = "CloudMenu_Update_VariantGroup_Regression")
	public static Object[][] Update_VariantGroup_Regression()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CloudMenu_Update_VariantGroup_Regression", "REGRESSION");

	}

	@Test(priority = 2, dataProvider = "Update_VariantGroup_Regression")
	public void Update_VariantGroup_Regression(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Update_VariantGroup_Regression" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"Update_VariantGroup_Regression" + sanityTest);

		System.out.println(actualData.get("data"));
	}

	@DataProvider(name = "CloudMenu_Create_Variant_Regression")
	public static Object[][] Create_Variant_Regression()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CloudMenu_Create_Variant_Regression", "REGRESSION");

	}

	@Test(priority = 3, dataProvider = "CloudMenu_Create_Variant_Regression")
	public void Create_Variant_Regression(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Create_Variant_Regression" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Create_Variant_Regression" + sanityTest);

		System.out.println(actualData.get("data"));
	}

	@DataProvider(name = "CloudMenu_Update_Variant_Regression")
	public static Object[][] Update_Variant_Regression()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CloudMenu_Update_Variant_Regression", "REGRESSION");

	}

	@Test(priority = 4, dataProvider = "CloudMenu_Update_Variant_Regression")
	public void Update_Variant_Regression(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Update_Variant_Regression" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Update_Variant_Regression" + sanityTest);

		System.out.println(actualData.get("data"));
	}

}