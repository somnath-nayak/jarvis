package com.swiggy.automation.cms.selfservemenu.api.regression;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.CMSDAO;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class SelfServeAgenSideUpdateTicketState extends RestTestHelper {

	@DataProvider(name = "CMS_SelfServe_Update_TicketState")
	public static Object[][] Test_Get_TicketDetails()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_SelfServe_Update_TicketState",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_SelfServe_Update_TicketState")
	public void Test_Get_TicketDetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		int id = 0;

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");
		id = CMSDAO.getTicketIdToUpdateTicketStatus();

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "placeholder1", id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "GetTicketDetailsBYTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetTicketDetailsBYTicketId" + sanityTest);

		System.out.println(actualData.get("data"));
	}

	//
	// @DataProvider(name = "CMS_SelfServe_Update_TicketState")
	// public static Object[][] Test_CreateAgent()
	// throws SwiggyAPIAutomationException {
	//
	// return APIDataReader.getDataSetData("CMS_SelfServe_Update_TicketState",
	// "REGRESSION");
	// }
	//
	//
	// @Test(priority = 2, dataProvider = "CMS_SelfServe_Update_TicketState")
	// public void Test_GetCatListByRestID(String dataGroup, String
	// apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// String authkey=CMSDAO.getAuthKeyOfAgent();
	//
	// HashMap<String, Object> header = new HashMap<String, Object>();
	// header.put("Authorization",
	// "Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");
	// header.put("auth-key",authkey);
	// JsonNode actualData = RestTestUtil.sendDataAsPathParam(apiToExecute,
	// jsonRequestData, header);
	// System.out.println(actualData);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
	// actualData);
	// Assert.assertTrue(status, "UpdateTicketState" + smokeTest);
	// status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
	// actualData);
	// Assert.assertTrue(status, "UpdateTicketState" + sanityTest);
	//
	// }

}
