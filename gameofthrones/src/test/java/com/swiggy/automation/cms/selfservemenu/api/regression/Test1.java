package com.swiggy.automation.cms.selfservemenu.api.regression;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class Test1 extends RestTestHelper {
	@DataProvider(name = "CMS_Agent_Get_CatDetail_by_RestId")
	public static Object[][] Test_GetCatListByRestID()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Agent_Get_CatDetail_by_RestId",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_CatDetail_by_RestId")
	public void Test_GetCatListByRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		JSONObject a = new JSONObject(expectedData.toString());
		JSONArray array = a.getJSONArray("data").getJSONArray(0);
		a.put("arrayName", array);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(a);
		JsonNode jsonNode = mapper.readTree(json);

		JSONObject a1 = new JSONObject(actualData.toString());
		JSONArray array1 = a.getJSONArray("data").getJSONArray(0);
		a1.put("arrayName", array1);

		String json1 = mapper.writeValueAsString(a1);
		JsonNode jsonNode1 = mapper.readTree(json1);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(jsonNode,
				jsonNode1);
		Assert.assertTrue(status, "GetCategoryByRestid" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetCategoryByRestid" + sanityTest);

	}
}
