package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class VariantGroupServiceSanity extends RestTestHelper {
	int VariantGroupId = 0;

	@DataProvider(name = "CREATE_VARIANTGROUP_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANTGROUP_SANITY",
				"SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_VARIANTGROUP_SANITY")
	public void testCreateVariantGroup(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updaterequest = RestTestUtil.updateRequestDataBeforeSend(updaterequest,
				"third_party_id", updaterequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualdata = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apitoExecute,
				updaterequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualdata);

		Assert.assertTrue(status, "testCreateVariantGroup" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testCreateVariantGroup" + sanityTest);
		VariantGroupId = actualdata.get("data").get("id").asInt();

	}

	@DataProvider(name = "GET_VARIANTGROUP_BYID_SANITY")
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_VARIANTGROUP_BYID_SANITY",
				"SANITY");
	}

	@Test(priority = 4, dataProvider = "GET_VARIANTGROUP_BYID_SANITY")
	public void testGetVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VariantGroupId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetVariantGroupByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetVariantGroupByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_VARIANTGROUP_SANITY")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_VARIANTGROUP_SANITY",
				"SANITY");
	}

	@Test(priority = 5, dataProvider = "DELETE_VARIANTGROUP_SANITY")
	public void testDeleteVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VariantGroupId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteVariantGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteVariantGroup" + sanityTest);
	}

}
