package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class VariantServiceRegressionTest extends RestTestHelper {
	String updatedName = null;
	int variant_id = 0;

	@DataProvider(name = "CREATE_VARIANT_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANT_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_VARIANT_REGRESSION")
	public void testCreateVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest;

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedrequest, "third_party_id",
				updatedrequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
		variant_id = actualData.get("data").get("id").asInt();

	}

	@DataProvider(name = "CREATE_VARIANT_REGRESSION1")
	public static Object[][] getDataREG1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANT_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_VARIANT_REGRESSION1")
	public void testCreateVariantREG1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest;

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);

	}

	@DataProvider(name = "CREATE_VARIANT_REGRESSION2")
	public static Object[][] getDataWEID() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANT_REGRESSION2",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CREATE_VARIANT_REGRESSION2")
	public void testCreateVariant1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
	}

	@DataProvider(name = "UPDATE_VARIANT_REGRESSION")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_VARIANT_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "UPDATE_VARIANT_REGRESSION")
	public void testUpdateVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateVariant" + sanityTest);

	}

	@DataProvider(name = "SEARCH_VARIANT_REGRESSION") // name can be any thing
	// eg A
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_VARIANT_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "SEARCH_VARIANT_REGRESSION")
	public void testSearchVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed

		Assert.assertTrue(status, "testSearchVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testSearchVariant" + sanityTest);
	}

	@DataProvider(name = "GET_VARIANT_BYID_REGRESSION") // name can be any thing
														// eg
	// A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_VARIANT_BYID_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "GET_VARIANT_BYID_REGRESSION")
	public void testGetVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed

		Assert.assertTrue(status, "testGetVariantByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetVariantByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_VARIANT_REGRESSION") // name can be any thing
														// eg A
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_VARIANT_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "DELETE_VARIANT_REGRESSION")
	public void testDeleteVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", variant_id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed

		Assert.assertTrue(status, "testDeleteVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testDeleteVariant" + sanityTest);
	}

	@DataProvider(name = "CREATE_VARIANT_REGRESSION3_BAD_REQUEST") // name can
																	// be any
																	// thing
	// eg A
	public static Object[][] getDataInvalidCase()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CREATE_VARIANT_REGRESSION3_BAD_REQUEST", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CREATE_VARIANT_REGRESSION3_BAD_REQUEST")
	public void testInvaidcase(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

}
