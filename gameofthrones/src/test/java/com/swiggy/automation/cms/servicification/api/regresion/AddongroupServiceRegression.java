package com.swiggy.automation.cms.servicification.api.regresion;

import java.util.ArrayList;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.junit.AfterClass;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AddongroupServiceRegression extends RestTestHelper {

	int addongroupid = 0, item_id1 = 0, countOfAddongroupId[];
	int count = 0;
	ArrayList<Integer> addonGroupList = null;

	@BeforeClass
	public void intialize() {
		addonGroupList = new ArrayList<Integer>();

		System.out.println("test111111111111111111111" + addonGroupList);
	}

	@DataProvider(name = "CREATE_ITEM_SANITY")
	public static Object[][] getDataItem() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ITEM_SANITY", "SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_ITEM_SANITY")
	public void testCreateItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedrequest, "third_party_id",
				updatedrequest.get("third_party_id").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testItemForAG" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testItemForAG" + sanityTest);
		if (item_id1 == 0) {
			item_id1 = actualData.get("data").get("id").asInt();
		}
		System.out.println("test22222222222222222222" + addonGroupList);
	}

	@DataProvider(name = "CREATE_ADDONGROUP_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDONGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CREATE_ADDONGROUP_REGRESSION")
	public void testCreateAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedrequest, "third_party_id",
				updatedrequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddonGroup" + sanityTest);
		System.out.println("id outside");

		if (addongroupid == 0) {

			System.out.println("id test");
			addongroupid = actualData.get("data").get("id").asInt();
			if (0 != addongroupid) {
				addonGroupList.add(addongroupid);
			}
		}

	}

	@DataProvider(name = "CREATE_ADDONGROUP_REGRESSION1")
	public static Object[][] getDataCAG() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDONGROUP_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CREATE_ADDONGROUP_REGRESSION1")
	public void testCreateAG1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG" + sanityTest);
	}

	@DataProvider(name = "CREATE_ADDONGROUP_REGRESSION2")
	public static Object[][] getDataCAG2Reg()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDONGROUP_REGRESSION2",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "CREATE_ADDONGROUP_REGRESSION2")
	public void testCreateAGReg(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "item_id", item_id1);

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG2" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG2" + sanityTest);
	}

	@DataProvider(name = "CREATE_ADDONGROUP_REGRESSION3")
	public static Object[][] getDataCAG2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDONGROUP_REGRESSION3",
				"REGRESSION");
	}

	@Test(priority = 5, dataProvider = "CREATE_ADDONGROUP_REGRESSION3")
	public void testCreateAG2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG2" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAG2" + sanityTest);
	}

	@DataProvider(name = "UPDATE_ADOONGROUP_SANITY")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ADOONGROUP_SANITY",
				"REGRESSION");

	}

	@Test(priority = 6, dataProvider = "UPDATE_ADOONGROUP_SANITY")
	public void testUpdateAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testUpdateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateAddonGroup" + sanityTest);

	}

	@DataProvider(name = "SEARCH_ADDONGROUP_REGRESSION") // name can be any
															// thing
	// eg A
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_ADDONGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 7, dataProvider = "SEARCH_ADDONGROUP_REGRESSION")
	public void testSearchAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed

		Assert.assertTrue(status, "testSearchAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data
		Assert.assertTrue(status, "testSearchAddonGroup" + sanityTest);
	}

	@DataProvider(name = "GET_ADDONGROUPBYID_SANITY") // name can be any
	// thing eg A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ADDONGROUPBYID_SANITY",
				"REGRESSION");
	}

	@Test(priority = 8, dataProvider = "GET_ADDONGROUPBYID_SANITY")
	public void testGetAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData;
		if (count == 0) {
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "pathvariable", addongroupid);

			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					updatedrequest);
			count++;
		} else {
			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					jsonRequestData);
		}
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed
		Assert.assertTrue(status, "testGetAddonGroupByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetAddonGroupByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_ADOONGROUP_REGRESSION")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ADOONGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 9, dataProvider = "DELETE_ADOONGROUP_REGRESSION")
	public void testDeleteAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		int count = 0;
		JsonNode actualData = null, updatedrequest = null;
		if (count == 0) {
			try {
				Iterator<Integer> itr = addonGroupList.listIterator();
				while (itr.hasNext()) {
					int a = itr.next();

					updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
							jsonRequestData, "pathvariable", a);

					actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
							updatedrequest);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			count++;
			Thread.sleep(3000);
		} else {
			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					jsonRequestData);
		}
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed

		Assert.assertTrue(status, "testDeleteAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testDeleteAddonGroup" + sanityTest);

		System.out.println("test22222222222222222222" + addonGroupList);

	}

	@DataProvider(name = "Badrequest_ADDONGROUP_REG3")
	public static Object[][] getDataCAG11()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Badrequest_ADDONGROUP_REG3",
				"REGRESSION");
	}

	@Test(priority = 10, dataProvider = "Badrequest_ADDONGROUP_REG3")
	public void testCreateAG11(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("rms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@AfterClass
	public void cleanUp() {

	}

}
