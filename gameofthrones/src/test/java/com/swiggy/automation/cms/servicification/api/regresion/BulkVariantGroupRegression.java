package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkVariantGroupRegression extends RestTestHelper {
	@DataProvider(name = "BULK_VARIANTGROUP_UPLOAD")
	public static Object[][] getDataVariantGroup()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_VARIANTGROUP_UPLOAD",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BULK_VARIANTGROUP_UPLOAD")
	public void testCreateVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData;
		JSONObject a = new JSONObject(jsonRequestData.toString());
		int length = a.getJSONArray("variant_groups").length();

		for (int i = 0; i < length; i++) {

			a.getJSONArray("variant_groups").getJSONObject(i).put("name",
					a.getJSONArray("variant_groups").getJSONObject(i)
							.get("name").toString() + System.currentTimeMillis()
							+ i);
			a.getJSONArray("variant_groups").getJSONObject(i).put(
					"third_party_id",
					a.getJSONArray("variant_groups").getJSONObject(i)
							.get("third_party_id").toString()
							+ System.currentTimeMillis() + i);
		}
		JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, updatedreq);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
	}

	@DataProvider(name = "BULK_VARIANTGROUP_UPLOAD_REG")
	public static Object[][] getDataVariantGroup1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_VARIANTGROUP_UPLOAD_REG",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "BULK_VARIANTGROUP_UPLOAD_REG")
	public void testCreateVariantGroup1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariantGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariantGroup" + sanityTest);
	}

}
