package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ItemServiceSanity extends RestTestHelper {

	int itemId = 0;

	@DataProvider(name = "CREATE_ITEM_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ITEM_SANITY", "SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_ITEM_SANITY")
	public void testCreateItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "third_party_id",
				updatedRequest.get("third_party_id").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + sanityTest);

		itemId = actualData.get("data").get("id").asInt();

	}

	@DataProvider(name = "GET_ITEM_BYID_SANITY")
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ITEM_BYID_SANITY", "SANITY");
	}

	@Test(priority = 3, dataProvider = "GET_ITEM_BYID_SANITY")
	public void testGetItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", itemId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetItemById" + smokeTest);
		System.out.println("actual" + actualData);
		System.out.println("expected" + expectedData);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetItemById" + sanityTest);
	}

	@DataProvider(name = "DELETE_ITEM")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ITEM", "SANITY");
	}

	@Test(priority = 4, dataProvider = "DELETE_ITEM")
	public void testDeleteItem(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", itemId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteItem" + sanityTest);
	}

}
