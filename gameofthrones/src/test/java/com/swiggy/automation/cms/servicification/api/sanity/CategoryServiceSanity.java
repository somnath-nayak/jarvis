package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CategoryServiceSanity extends RestTestHelper {

	int catId = 0;

	@DataProvider(name = "CREATE_CATEGORY_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CATEGORY_SANITY", "SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_CATEGORY_SANITY")
	public void testCreateCategory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest;

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateCategory" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateCategory" + sanityTest);
		catId = actualData.get("data").get("id").asInt();

	}

	@DataProvider(name = "UPDATE_CATEGORY_SANITY")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_CATEGORY_SANITY", "SANITY");
	}

	@Test(priority = 2, dataProvider = "UPDATE_CATEGORY_SANITY")
	public void testUpdateCategory(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualdata = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apitoExecute,
				jsonRequestData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testUpdateCategory" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testUpdateVariantCategory" + sanityTest);

	}

	@DataProvider(name = "GET_CATEGORY_BY_ID_SANITY") // name can be any
														// thing eg A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_CATEGORY_BY_ID_SANITY",
				"SANITY");
	}

	@Test(priority = 5, dataProvider = "GET_CATEGORY_BY_ID_SANITY")
	public void testGetCategory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", catId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetCategoryByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetCategoryByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_CatOrSubCat_SANITY")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_CatOrSubCat_SANITY",
				"SANITY");
	}

	@Test(priority = 6, dataProvider = "DELETE_CatOrSubCat_SANITY")
	public void testDeleteVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", catId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteCatOrSubcat" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteCatOrSubcat" + sanityTest);
	}

}
