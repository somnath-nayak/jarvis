package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkCategorySanity extends RestTestHelper {
	@DataProvider(name = "BULK_CATEGORY_UPLOAD")
	public static Object[][] getDataCat() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_CATEGORY_UPLOAD", "SANITY");
	}

	@Test(priority = 1, dataProvider = "BULK_CATEGORY_UPLOAD")
	public void testCreateCat(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData;
		JSONObject a = new JSONObject(jsonRequestData.toString());
		int length = a.getJSONArray("categories").length();

		for (int i = 0; i < length; i++) {

			a.getJSONArray("categories").getJSONObject(i)
					.put("name",
							a.getJSONArray("categories").getJSONObject(i)
									.get("name").toString()
									+ System.currentTimeMillis() + i);

		}
		JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute, updatedreq);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateItem" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateItem" + sanityTest);

	}

}
