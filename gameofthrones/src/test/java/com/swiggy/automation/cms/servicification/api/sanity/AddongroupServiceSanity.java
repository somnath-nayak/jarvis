package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AddongroupServiceSanity extends RestTestHelper {

	int addonGroupId = 0;

	@DataProvider(name = "CREATE_ADDONGROUP_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDONGROUP_SANITY",
				"SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_ADDONGROUP_SANITY")
	public void testCreateAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "third_party_id",
				updatedRequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddonGroup" + sanityTest);
		addonGroupId = actualData.get("data").get("id").asInt();

	}

	@DataProvider(name = "UPDATE_ADOONGROUP_SANITY")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ADOONGROUP_SANITY",
				"SANITY");

	}

	@Test(priority = 2, dataProvider = "UPDATE_ADOONGROUP_SANITY")
	public void testUpdateAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addonGroupId);

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "requestbody", "name",
				updatedRequest.get("requestbody").get("name").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testUpdateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateAddonGroup" + sanityTest);

	}

	@DataProvider(name = "GET_ADDONGROUPBYID_SANITY")
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ADDONGROUPBYID_SANITY",
				"SANITY");
	}

	@Test(priority = 4, dataProvider = "GET_ADDONGROUPBYID_SANITY")
	public void testGetAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addonGroupId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetAddonGroupByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetAddonGroupByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_ADOONGROUP_SANITY")
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ADOONGROUP_SANITY",
				"SANITY");
	}

	@Test(priority = 5, dataProvider = "DELETE_ADOONGROUP_SANITY")
	public void testDeleteAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addonGroupId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testDeleteAddonGroup" + sanityTest);
	}

}
