package com.swiggy.automation.cms.cloudMenu.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit Chaubey
 *
 */

public class CloudMenuAddonGroupRegression extends RestTestHelper {

	@DataProvider(name = "CMS_CloudMenu_Create_AddonGroup")
	public static Object[][] CmsCloudMenuCreateAddonGroup()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_CloudMenu_Create_AddonGroup",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Create_AddonGroup")
	public void CmsCloudMenuCreateAddonGroup(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData, header);

		System.out.println("actual value" + actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "CreateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "CreateAddonGroup" + sanityTest);

	}

	@DataProvider(name = "CMS_CloudMenu_Update_AddonGroup")
	public static Object[][] CmsCloudMenuUpdateAddonGroup()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_CloudMenu_Update_AddonGroup",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Update_AddonGroup")
	public void CmsCloudMenuUpdateAddonGroup(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get(""),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "UpdateAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "UpdateAddonGroup" + sanityTest);

	}

	@DataProvider(name = "CMS_CloudMenu_Create_AddonGroup_Bulk")
	public static Object[][] CmsCloudMenuCreateAddonGroupBulk()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_CloudMenu_Create_AddonGroup_Bulk", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Create_AddonGroup_Bulk")
	public void CmsCloudMenuCreateAddonGroupBulk(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "CreateAddonGroupBulk" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "CreateAddonGroupBulk" + sanityTest);

	}

	@DataProvider(name = "CMS_CloudMenu_Update_AddonGroup_Bulk")
	public static Object[][] CmsCloudMenuUpdateAddonGroupBulk()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_CloudMenu_Update_AddonGroup_Bulk", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Update_AddonGroup_Bulk")
	public void CmsCloudMenuUpdateAddonGroupBulk(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "UpdateAddonGroupBulk" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "UpdateAddonGroupBulk" + sanityTest);

	}

}
