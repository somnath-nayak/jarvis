package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class VariantGroupServiceRegressionTest extends RestTestHelper {

	int VG_id = 0;

	@DataProvider(name = "CREATE_VARIANTGROUP_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANTGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_VARIANTGROUP_REGRESSION")
	public void testCreateVariantGroup(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		updaterequest = RestTestUtil.updateRequestDataBeforeSend(updaterequest,
				"third_party_id", updaterequest.get("third_party_id").asText()
						+ System.currentTimeMillis());

		JsonNode actualdata = RestTestUtil.sendData(serviceNameMap.get("cms"),apitoExecute,
				updaterequest);
		System.out.println(actualdata);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualdata);

		Assert.assertTrue(status, "testCreateVariantGroup" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testCreateVariantGroup" + sanityTest);
		VG_id = actualdata.get("data").get("id").asInt();

	}

	@DataProvider(name = "CREATE_VARIANTGROUP_REGRESSION1")
	public static Object[][] getDataReg1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANTGROUP_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CREATE_VARIANTGROUP_REGRESSION1")
	public void testCreateVariantGroupReg1(String dataGroup,
			String apitoExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apitoExecute,
				updaterequest);

		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariantGroupReg1" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariantGroupReg1" + sanityTest);

	}

	@DataProvider(name = "CREATE_VARIANTGROUP_REGRESSION2")
	public static Object[][] getDataReg() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VARIANTGROUP_REGRESSION2",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CREATE_VARIANTGROUP_REGRESSION2")
	public void testCreateVariantGroup1(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apitoExecute,
				jsonRequestData);

		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariantGroup" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariantGroup" + sanityTest);

	}

	@DataProvider(name = "CREATE_VG_REGRESSION3_BAD_REQUEST")
	public static Object[][] getDataRegvariantGrp()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_VG_REGRESSION3_BAD_REQUEST",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CREATE_VG_REGRESSION3_BAD_REQUEST")
	public void testInvaidcase(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "UPDATE_VARIANTGROUP_REGRESSION")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_VARIANTGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "UPDATE_VARIANTGROUP_REGRESSION")
	public void testUpdateVariantGroup(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualdata = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apitoExecute,
				jsonRequestData);
		System.out.println(actualdata);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualdata);

		Assert.assertTrue(status, "testUpdateVariantGroup" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testUpdateVariantGroup" + sanityTest);

	}

	@DataProvider(name = "SEARCH_VARIANTGROUP_REGRESSION") // name can be any
															// thing
															// eg A
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_VARIANTGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "SEARCH_VARIANTGROUP_REGRESSION")
	public void testSearchVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed

		Assert.assertTrue(status, "testSearchVariantGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data
		Assert.assertTrue(status, "testSearchVariantGroup" + sanityTest);
	}

	@DataProvider(name = "GET_VARIANTGROUP_BYID_REGRESSION1") // name can be any
																// thing eg A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_VARIANTGROUP_BYID_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "GET_VARIANTGROUP_BYID_REGRESSION1")
	public void testGetVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed
		Assert.assertTrue(status, "testGetVariantGroupByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetVariantGroupByID" + sanityTest);
	}

	@DataProvider(name = "GET_VARIANTGROUP_BYID_REGRESSION") // name can be any
	// thing eg A
	public static Object[][] getData3GetVG()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_VARIANTGROUP_BYID_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 4, dataProvider = "GET_VARIANTGROUP_BYID_REGRESSION")
	public void testGetVG(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VG_id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed
		Assert.assertTrue(status, "testGetVariantGroupByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetVariantGroupByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_VARIANTGROUP_REGRESSION") // name can be any
															// thing
															// eg A
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_VARIANTGROUP_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 5, dataProvider = "DELETE_VARIANTGROUP_REGRESSION")
	public void testDeleteVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", VG_id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed

		Assert.assertTrue(status, "testDeleteVariantGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testDeleteVariantGroup" + sanityTest);
	}

}
