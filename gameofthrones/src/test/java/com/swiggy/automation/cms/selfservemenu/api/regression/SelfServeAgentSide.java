package com.swiggy.automation.cms.selfservemenu.api.regression;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class SelfServeAgentSide extends RestTestHelper {

	@DataProvider(name = "CMS_Agent_Get_CatDetail_by_RestId")
	public static Object[][] Test_GetCatListByRestID()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Agent_Get_CatDetail_by_RestId",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_CatDetail_by_RestId")
	public void Test_GetCatListByRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetCategoryByRestid" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetCategoryByRestid" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_CatDetail_by_RestId_Bad_request") // name
																			// can

	public static Object[][] getDataInvalidCase()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_CatDetail_by_RestId_Bad_request", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_CatDetail_by_RestId_Bad_request")
	public void testInvaidcase(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_SubCatDetail_by_CatId")
	public static Object[][] Test_GetSubCatListByCatId()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_SubCatDetail_by_CatId", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_SubCatDetail_by_CatId")
	public void Test_GetSubCatListByCatId(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetSubCategoryByCatid" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetSubCategoryByCatid" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_SubCatDetail_by_CatId_BadRequest")
	public static Object[][] getDataInvalidCase1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_SubCatDetail_by_CatId_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_SubCatDetail_by_CatId_BadRequest")
	public void testInvaidcase1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_Menu_Images_By_RestId")
	public static Object[][] Test_GetMenuByRestId()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_Menu_Images_By_RestId", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_Menu_Images_By_RestId")
	public void Test_GetMenuByRestId(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetMenuByRestId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetMenuByRestId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_Menu_Images_By_RestId_BadRequest")
	public static Object[][] getDataInvalidCase2()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_Menu_Images_By_RestId_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_Menu_Images_By_RestId_BadRequest")
	public void testInvaidcase2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_Comments_By_TicketId")
	public static Object[][] Test_GetComments_ByTicketId()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_Comments_By_TicketId", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_Comments_By_TicketId")
	public void Test_GetComments_ByTicketId(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetCommentsByTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetCommentsByTicketId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_Comments_By_TicketId_BadRequest")
	public static Object[][] getDataInvalidCase3()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_Comments_By_TicketId_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_Comments_By_TicketId_BadRequest")
	public void testInvaidcase3(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId")
	public static Object[][] Test_Get_itemDetails()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId")
	public void Test_Get_itemDetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId_BadRequest")
	public static Object[][] getDataInvalidCase4()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId_BadRequest",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_View_ItemDetails_ByTicketId_ItemId_BadRequest")
	public void testInvaidcase4(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_TicketDetails")
	public static Object[][] Test_Get_TicketDetails()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Agent_Get_TicketDetails",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_TicketDetails")
	public void Test_Get_TicketDetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetTicketDetailsBYTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "GetTicketDetailsBYTicketId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_TicketDetails_BadRequest")
	public static Object[][] getDataInvalidCase5()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_TicketDetails_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_TicketDetails_BadRequest")
	public void testInvaidcase5(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_RevisionTicket_Details_ByAgentId")
	public static Object[][] Test_Get_RevisionTicketsByAgentId()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_RevisionTicket_Details_ByAgentId", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_RevisionTicket_Details_ByAgentId")
	public void Test_Get_RevisionTicketsByAgentId(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_RevisionTicket_Details_ByAgentId_BadRequest")
	public static Object[][] getDataInvalidCase6()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_RevisionTicket_Details_ByAgentId_BadRequest",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Agent_Get_RevisionTicket_Details_ByAgentId_BadRequest")
	public void testInvaidcase6(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Agent_Get_Reason_List")
	public static Object[][] Test_Get_RessonList()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Agent_Get_Reason_List",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_Reason_List")
	public void Test_Get_RessonList(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + sanityTest);

	}

	@DataProvider(name = "CMS_Agent_Get_Open_tickets_for_An_Agent")
	public static Object[][] Test_Get_OpenTicketsForAnAgent()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_Agent_Get_Open_tickets_for_An_Agent", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_Agent_Get_Open_tickets_for_An_Agent")
	public void Test_Get_OpenTicketsForAnAgent(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("Authorization",
				"Basic YWdlbnQtZGFzaGJvYXJkOnNlbGZzZXJ2ZS1zeXN0ZW0=");

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status,
				"GetItemDetailsByItemIdandTicketId" + sanityTest);

	}

}
