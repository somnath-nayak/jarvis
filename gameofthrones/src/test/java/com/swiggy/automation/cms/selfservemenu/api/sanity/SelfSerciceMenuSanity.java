package com.swiggy.automation.cms.selfservemenu.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class SelfSerciceMenuSanity extends RestTestHelper{ 

	@DataProvider(name = "CMS_SelfServe_CategoryList_By_RestID")
	public static Object[][] getDataCAG() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_SelfServe_CategoryList_By_RestID", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_SelfServe_CategoryList_By_RestID")
	public void testCreateAG1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
	}

}
