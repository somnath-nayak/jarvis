package com.swiggy.automation.cms.cloudMenu.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit Chaubey
 *
 */

public class CloudMenuMainCategoryRegression extends RestTestHelper {

	@DataProvider(name = "CMS_CloudMenu_Create_Maincategory_Bulk")
	public static Object[][] CmsCloudMenuCreateCategory()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_CloudMenu_Create_Maincategory_Bulk", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Create_Maincategory_Bulk")
	public void CmsCloudMenuCreateCategory(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println("actual value" + actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "CreateBulkMainCategory" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "CreateBulkMainCategory" + sanityTest);

	}

	@DataProvider(name = "CMS_CloudMenu_Update_Maincategory_Bulk")
	public static Object[][] CmsCloudMenuUpdateManaCat()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_CloudMenu_Update_Maincategory_Bulk", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Update_Maincategory_Bulk")
	public void CmsCloudMenuUpdateManaCat(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println("actual value" + actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "UpdateBulkMainCategory" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "UpdateBulkMainCategory" + sanityTest);

	}

}
