package com.swiggy.automation.cms.selfservemenu.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.CMSDAO;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AgentService extends RestTestHelper {

	int id = 0;

	@DataProvider(name = "CMS_AgentService_CreateAgent")
	public static Object[][] Test_CreateAgent()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_AgentService_CreateAgent",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_AgentService_CreateAgent")
	public void Test_CreateAgent(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		id = CMSDAO.getAuthUserId();
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "auth_user_id", id);
		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "auth_key",
				jsonRequestData.get("auth_key").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		// System.out.println(actualData);
		Assert.assertTrue(status, "testCreateAgent" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAgent" + sanityTest);
	}

	@DataProvider(name = "CMS_AgentService_CreateAgent_INvalid")
	public static Object[][] Test_CreateAgent1()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData(
				"CMS_AgentService_CreateAgent_INvalid", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CMS_AgentService_CreateAgent_INvalid")
	public void Test_CreateAgent1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAgent" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAgent" + sanityTest);
	}

	@DataProvider(name = "CMS_AgentService_CreateAgent_BadRequest")
	public static Object[][] Test_CreateAgent2()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_AgentService_CreateAgent_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_AgentService_CreateAgent_BadRequest")
	public void Test_CreateAgent2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CMS_Update_Availibality_Of_Agent")
	public static Object[][] Update_Availibality_Of_Agent()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_Update_Availibality_Of_Agent",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_Update_Availibality_Of_Agent")
	public void Update_Availibality_Of_Agent(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		// System.out.println(actualData);
		Assert.assertTrue(status, "testCreateAgent" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAgent" + sanityTest);

	}

	@DataProvider(name = "CMS_AgentService_Availability_of_Agent_BadRequest")
	public static Object[][] Availability_of_Agent_BadRequest()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData(
				"CMS_AgentService_CreateAgent_BadRequest", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CMS_AgentService_Availability_of_Agent_BadRequest")
	public void Availability_of_Agent_BadRequest(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

}
