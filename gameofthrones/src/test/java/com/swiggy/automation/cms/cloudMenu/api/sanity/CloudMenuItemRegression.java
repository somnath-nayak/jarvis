package com.swiggy.automation.cms.cloudMenu.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit Chaubey
 *
 */

public class CloudMenuItemRegression extends RestTestHelper

{
	@DataProvider(name = "CloudMenu_Create_Item_Regresison")
	public static Object[][] Create_Item_Regresison()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CloudMenu_Create_Item_Regresison",
				"REGRESSION");

	}

	@Test(priority = 1, dataProvider = "CloudMenu_Create_Item_Regresison")
	public void Create_Item_Regresison(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Create_Item_Regresison" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Create_Item_Regresison" + sanityTest);

		System.out.println(actualData.get("data"));
	}

	@DataProvider(name = "CloudMenu_Update_Item_Regression")
	public static Object[][] Update_Item_Regression()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CloudMenu_Create_Item_Regresison",
				"REGRESSION");

	}

	@Test(priority = 2, dataProvider = "CloudMenu_Update_Item_Regression")
	public void Update_Item_Regression(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");
		System.out.println("Header sumit = " + header);
		// JsonNode updatedrequest = RestTestUtil
		// .updateRequestDataBeforeSend(jsonRequestData, header);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Update_Item_Regression" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Update_Item_Regression" + sanityTest);

		System.out.println(actualData.get("data"));
	}

}