package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AddonServiceRegressionTest extends RestTestHelper {

	int addon_id = 0;
	String addonname = null;
	String updatedName = null;
	int count = 0;

	@DataProvider(name = "CREATE_ADDON_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDON_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_ADDON_REGRESSION")
	public void testCreateAddon1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest;
		try {
			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "name",
					jsonRequestData.get("name").asText()
							+ System.currentTimeMillis());
			updatedName = updatedrequest.get("name").asText();

			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedrequest, "third_party_id",
					updatedrequest.get("third_party_id").asText()
							+ System.currentTimeMillis());

			// JsonNode actualData =
			// RestTestUtil.sendData(envMap.get("cms"),apiToExecute,jsonRequestData);

			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
					updatedrequest);
			// addon_id = actualData.get("data").get("id").asInt();

			System.out.println(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(status, "testCreateAddon" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(status, "testCreateAddon" + sanityTest);
			if (addon_id == 0)
				addon_id = actualData.get("data").get("id").asInt();
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "CREATE_ADDON_REGRESSION1")
	public static Object[][] getDataAddonReg1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDON_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_ADDON_REGRESSION1")
	public void testCreateAddonReg1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAddonReg" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddonReg" + sanityTest);

	}

	@DataProvider(name = "BADREQUEST_ADDON_REGRESSION")
	public static Object[][] getDataAddonRegression()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BADREQUEST_ADDON_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BADREQUEST_ADDON_REGRESSION")
	public void testCreateAddonRegession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("sand"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

	}

	@DataProvider(name = "CREATE_ADDON_WHICH_EXIST")
	public static Object[][] getDataAddonRegExistingAddon()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDON_WHICH_EXIST",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_ADDON_WHICH_EXIST")
	public void testCreateAddonRegExistingAddon(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "third_party_id",
				jsonRequestData.get("third_party_id").asText()
						+ System.currentTimeMillis());
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAddonReg" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddonReg" + sanityTest);

	}

	@DataProvider(name = "UPDATE_ADOON_REGRESSION")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ADOON_REGRESSION",
				"REGRESSION");

	}

	@Test(priority = 3, dataProvider = "UPDATE_ADOON_REGRESSION")
	public void testUpdateAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addon_id);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testUpdateAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateAddon" + sanityTest);

	}

	@DataProvider(name = "UPDATE_ADOON_REGRESSION1")
	public static Object[][] getData1update()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ADOON_REGRESSION1",
				"REGRESSION");

	}

	@Test(priority = 3, dataProvider = "UPDATE_ADOON_REGRESSION1")
	public void testUpdateAddonReg(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testUpdateAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateAddon" + sanityTest);

	}

	@DataProvider(name = "DELETE_ADOON_REGRESSION")
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ADOON_REGRESSION",
				"REGRESSION");

	}

	@Test(priority = 4, dataProvider = "DELETE_ADOON_REGRESSION")
	public void testDeleteAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData;
		if (count == 0) {
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "pathvariable", addon_id);

			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					updatedrequest);

			count++;
			Thread.sleep(3000);
		} else {
			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
					jsonRequestData);

		}
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteAddon" + sanityTest);

	}

	@DataProvider(name = "SEARCH_ADDON_REGRESSION") // name can be any thing
	// eg A
	public static Object[][] getDatasearchaddon()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_ADDON_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "SEARCH_ADDON_REGRESSION")
	public void testSearchAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
		// basic validation to proceed

		Assert.assertTrue(status, "testSearchAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data
		Assert.assertTrue(status, "testSearchAddon" + sanityTest);
	}

	@DataProvider(name = "GET_ADDON_BYID_REGRESSION") // name can be any
	// thing eg A
	public static Object[][] getDatagetaddon()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ADDON_BYID_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "GET_ADDON_BYID_REGRESSION")
	public void testGetAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);// high level validation eg: response code:200
		// basic validation to proceed
		Assert.assertTrue(status, "testGetAddonByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetAddonID" + sanityTest);
	}

}
