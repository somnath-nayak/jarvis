package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkAddonGroupRegression extends RestTestHelper {

	String third_party_id = null;

	@DataProvider(name = "Bulk_AddonGroup_Upload")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Bulk_AddonGroup_Upload",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "Bulk_AddonGroup_Upload")
	public void testCreateAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData;
		JSONObject a = new JSONObject(jsonRequestData.toString());
		int length = a.getJSONArray("addon_groups").length();

		for (int i = 0; i < length; i++) {

			a.getJSONArray("addon_groups").getJSONObject(i)
					.put("name",
							a.getJSONArray("addon_groups").getJSONObject(i)
									.get("name").toString()
									+ System.currentTimeMillis() + i);
			try {
				third_party_id = null;
				third_party_id = a.getJSONArray("addon_groups").getJSONObject(i)
						.get("third_party_id").toString();
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (third_party_id != null) {
				a.getJSONArray("addon_groups").getJSONObject(i).put(
						"third_party_id",
						a.getJSONArray("addon_groups").getJSONObject(i)
								.get("third_party_id").toString()
								+ System.currentTimeMillis() + i);

			}
		}
		JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, updatedreq);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testBulkAddonGroup" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testAddonGroup" + sanityTest);
	}

}