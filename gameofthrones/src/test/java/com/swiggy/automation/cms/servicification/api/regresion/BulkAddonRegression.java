package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkAddonRegression extends RestTestHelper {

	String third_party_id = null;

	@DataProvider(name = "BULK_ADDON_UPLOAD")
	public static Object[][] getDataAG() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_ADDON_UPLOAD", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BULK_ADDON_UPLOAD")
	public void testCreateAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData;
		JSONObject a = new JSONObject(jsonRequestData.toString());
		int length = a.getJSONArray("addons").length();

		for (int i = 0; i < length; i++) {

			a.getJSONArray("addons").getJSONObject(i)
					.put("name",
							a.getJSONArray("addons").getJSONObject(i)
									.get("name").toString()
									+ System.currentTimeMillis() + i);
			try {
				third_party_id = null;
				third_party_id = a.getJSONArray("addons").getJSONObject(i)
						.get("third_party_id").toString();
			} catch (Exception e) {
				// TODO: handle exception
			}

			if (third_party_id != null) {
				a.getJSONArray("addons").getJSONObject(i).put("third_party_id",
						a.getJSONArray("addons").getJSONObject(i)
								.get("third_party_id").toString()
								+ System.currentTimeMillis() + i);
			}
		}
		JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, updatedreq);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddon" + sanityTest);

	}

	@DataProvider(name = "BULK_ADDON_UPLOAD_Regression")
	public static Object[][] getDataAG1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_ADDON_UPLOAD_Regression",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "BULK_ADDON_UPLOAD_Regression")
	public void testCreateAddon1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateAddon" + sanityTest);

	}

}
