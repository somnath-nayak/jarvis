package com.swiggy.automation.cms.cloudMenu.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit Chaubey
 *
 * 
 */

public class CloudMenuFullItemRegression extends RestTestHelper {

	@DataProvider(name = "CMS_CloudMenu_Create_FullMenu")
	public static Object[][] CMS_CloudMenu_Create_FullMenu()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_CloudMenu_Create_FullMenu",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Create_FullMenu")
	public void CmsCloudMenuCreateCategory(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println("actual value" + actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "CMS_CloudMenu_Create_FullMenu" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "CMS_CloudMenu_Create_FullMenu" + sanityTest);

	}

	@DataProvider(name = "CMS_CloudMenu_Update_FullMenu")
	public static Object[][] CMS_CloudMenu_Update_FullMenu()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("CMS_CloudMenu_Update_FullMenu",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CMS_CloudMenu_Update_FullMenu")
	public void CMS_CloudMenu_Update_FullMenu(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		HashMap<String, Object> header = new HashMap<String, Object>();
		header.put("tokenid", "6dbad7c1113ff7d8");

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData, header);

		System.out.println("actual value" + actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "CMS_CloudMenu_Update_FullMenu" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "CMS_CloudMenu_Update_FullMenu" + sanityTest);

	}

}
