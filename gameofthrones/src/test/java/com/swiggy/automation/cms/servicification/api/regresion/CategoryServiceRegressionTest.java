package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CategoryServiceRegressionTest extends RestTestHelper {
	int cat_id = 0;

	@DataProvider(name = "CREATE_CATEGORY_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CATEGORY_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "CREATE_CATEGORY_REGRESSION")
	public void testCreateCategory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest;

		updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "name", jsonRequestData.get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateCategory" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateCategory" + sanityTest);
		cat_id = actualData.get("data").get("id").asInt();
	}

	@DataProvider(name = "CATEGORY_REGRESSION_BADREQUEST")
	public static Object[][] getDataWEID() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CATEGORY_REGRESSION_BADREQUEST",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CATEGORY_REGRESSION_BADREQUEST")
	public void testCreateVariant1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse res = RestTestUtil.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("cms"),
				apiToExecute, jsonRequestData);
		JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData, res);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
	}

	@DataProvider(name = "UPDATE_CATEGORY_REGRESSION")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_CATEGORY_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "UPDATE_CATEGORY_REGRESSION")
	public void testUpdateCategory(String dataGroup, String apitoExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualdata = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apitoExecute,
				jsonRequestData);
		System.out.println(actualdata);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testUpdateCategory" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualdata);
		Assert.assertTrue(status, "testUpdateVariantCategory" + sanityTest);

	}

	@DataProvider(name = "SEARCH_SUBCAT_BY_CAT_ID_SANITY")
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_SUBCAT_BY_CAT_ID_SANITY",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "SEARCH_SUBCAT_BY_CAT_ID_SANITY")
	public void testSearchSubcatByCatId(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testSearchSubCategoryByCatID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testSearchSubCategoryByCatID" + sanityTest);
	}

	@DataProvider(name = "SEARCH_CATEGORY_REG")
	public static Object[][] getDataSearchCat()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_CATEGORY_REG",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "SEARCH_CATEGORY_REG")
	public void testSearchCat(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testSearchCategory" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testSearchCategory" + sanityTest);
	}

	@DataProvider(name = "GET_CATEGORY_BY_ID_SANITY") // name can be any
														// thing eg A
	public static Object[][] getData3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_CATEGORY_BY_ID_SANITY",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "GET_CATEGORY_BY_ID_SANITY")
	public void testGetCategory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData); // high level validation eg: response code:200
								// basic validation to proceed
		Assert.assertTrue(status, "testGetCategoryByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData); // Detailed validation for each field or data

		Assert.assertTrue(status, "testGetCategoryByID" + sanityTest);
	}

	@DataProvider(name = "DELETE_CatOrSubCat_SANITY") // name can be any thing
	// eg A
	public static Object[][] getData4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_CatOrSubCat_SANITY",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "DELETE_CatOrSubCat_SANITY")
	public void testDeleteVariantGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", cat_id);
		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cms"),apiToExecute,
				updatedrequest);
		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteCatOrSubcat" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteCatOrSubcat" + sanityTest);
	}

}
