package com.swiggy.automation.cms.servicification.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AddonServiceSanity extends RestTestHelper {

	int addonId = 0, item_id = 0;
	String addonName = null;
	String updatedName = null;

	@DataProvider(name = "CREATE_ADDON_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_ADDON_SANITY", "SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_ADDON_SANITY")
	public void testCreateAddon1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest;
		try {
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "name",
					jsonRequestData.get("name").asText()
							+ System.currentTimeMillis());
			updatedName = updatedRequest.get("name").asText();

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "third_party_id",
					updatedRequest.get("third_party_id").asText()
							+ System.currentTimeMillis());

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "item_id", item_id);

			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cmsbaseservice"),apiToExecute,
					updatedRequest);
			addonId = actualData.get("data").get("id").asInt();

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(status, "testCreateAddon" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testCreateAddon" + sanityTest);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "UPDATE_ADOON_SANITY")
	public static Object[][] getData1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_ADOON_SANITY", "SANITY");

	}

	@Test(priority = 2, dataProvider = "UPDATE_ADOON_SANITY")
	public void testUpdateAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addonId);

		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "requestbody", "name",
				updatedRequest.get("requestbody").get("name").asText()
						+ System.currentTimeMillis());

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testUpdateAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testUpdateAddon" + sanityTest);

	}

	@DataProvider(name = "DELETE_ADOON_SANITY")
	public static Object[][] getData2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ADOON_SANITY", "SANITY");

	}

	@Test(priority = 5, dataProvider = "DELETE_ADOON_SANITY")
	public void testDeleteAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil
				.updateRequestDataBeforeSend(jsonRequestData, "id", addonId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testDeleteAddon" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testDeleteAddon" + sanityTest);

	}

	@DataProvider(name = "GET_ADDON_BYID_SANITY")
	public static Object[][] getDatagetaddon()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ADDON_BYID_SANITY", "SANITY");
	}

	@Test(priority = 4, dataProvider = "GET_ADDON_BYID_SANITY")
	public void testGetAddon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", addonId);

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("cmsbaseservice"),apiToExecute,
				updatedRequest);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testGetAddonByID" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetAddonID" + sanityTest);
	}

}
