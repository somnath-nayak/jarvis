package com.swiggy.automation.cms.servicification.api.regresion;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkVariantRegressionTest extends RestTestHelper {

	int addongroupid = 0;

	@DataProvider(name = "BULK_VARIANT_UPLOAD")
	public static Object[][] getDataVariant()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_VARIANT_UPLOAD",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BULK_VARIANT_UPLOAD")
	public void testCreateVariant(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JSONObject a = new JSONObject(jsonRequestData.toString());
		int length = a.getJSONArray("variants").length();
		JsonNode actualData;
		for (int i = 0; i < length; i++) {

			a.getJSONArray("variants").getJSONObject(i)
					.put("name",
							a.getJSONArray("variants").getJSONObject(i)
									.get("name").toString()
									+ System.currentTimeMillis() + i);
			a.getJSONArray("variants").getJSONObject(i).put("third_party_id",
					a.getJSONArray("variants").getJSONObject(i)
							.get("third_party_id").toString()
							+ System.currentTimeMillis() + i);
		}
		JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute, updatedreq);

		// JsonNode updatedreq = APIUtils.convertStringtoJSON(a.toString());
		// JsonNode actualData =
		// RestTestUtil.sendData(envMap.get("cms"),apiToExecute,updatedreq);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
	}

	@DataProvider(name = "BULK_VARIANT_UPLOAD_REG")
	public static Object[][] getDataVariantReg()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BULK_VARIANT_UPLOAD_REG",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BULK_VARIANT_UPLOAD_REG")
	public void testCreateVariantReg(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("cms"),apiToExecute,
				jsonRequestData);

		System.out.println(actualData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testCreateVariant" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "testCreateVariant" + sanityTest);
	}

}