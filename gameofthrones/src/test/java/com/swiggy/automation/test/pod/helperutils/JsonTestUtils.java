package com.swiggy.automation.test.pod.helperutils;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class JsonTestUtils {
	final static Logger log = Logger.getLogger(JsonTestUtils.class);

	public static Long getServiceableAddressId(JsonNode node)
			throws SwiggyAPIAutomationException {
		log.info("retrieving the serviceable address");
		int size = node.get("data").get("addresses").size();
		for (int i = 0; i < size; i++) {

			System.out.println(node.get("data").get("addresses").get(i)
					.get("delivery_valid").asInt());
			if (node.get("data").get("addresses").get(i).get("delivery_valid")
					.asInt() == 1) {
				Long a = node.get("data").get("addresses").get(i).get("id")
						.asLong();
				log.info("Found the Serviceable Address ,addressId" + a);
				System.out.println(
						"Found the Serviceable Address ,addressId :" + a);
				return a;
			}
		}

		log.error("unable to get the serviceable address");
		throw new SwiggyAPIAutomationException(
				"unable to get the serviceable address from GetAllAddress API");
	}

	/**
	 * use this function in getting Serviceable addressId and respective Object
	 *
	 * @param node
	 * @return
	 * @throws SwiggyAPIAutomationException
	 */
	public static HashMap<String, Object> getServiceableAddressObject(
			JsonNode node) throws SwiggyAPIAutomationException {
		HashMap<String, Object> data = new HashMap<String, Object>();
		log.info("retrieving the serviceable address");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"AddressObject List is Empty"));
			throw new SwiggyAPIAutomationException(
					"AddressObject List is Empty");
		}
		int size = node.size();
		for (int i = 0; i < size; i++) {
			if (node.get(i).get("delivery_valid").asInt() == 1) {
				data.put("id", node.get(i).get("id").asLong());
				data.put("object", node.get(i));
				return data;
			}
		}

		log.error("unable to get the serviceable address");
		throw new SwiggyAPIAutomationException(
				"unable to get the serviceable address from GetAllAddress API");
	}

	public static String getJsonObjectFromNode(JsonNode node,
			JsonNode nodeTreePath, int id) throws SwiggyAPIAutomationException {
		log.info("retrieving the serviceable address");
		int size = node.get("data").get("addresses").size();
		for (int i = 0; i < size; i++) {
			if (node.get("data").get("addresses").get(i).get("delivery_valid")
					.asInt() == 1) {
				String a = node.get("data").get("addresses").get(i).get("id")
						.asText();
				log.info("Found the Serviceable Address ,addressId" + a);
				System.out.println(
						"Found the Serviceable Address ,addressId :" + a);
				return a;
			}
		}

		log.error("unable to get the serviceable address");
		throw new SwiggyAPIAutomationException(
				"unable to get the serviceable address from GetAllAddress API");
	}

	public static JsonNode getRestaurantObjectUsingId(JsonNode node, int id)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();
		for (int i = 0; i < size; i++) {
			if (node.get(i).get("id").asInt() == id) {
				System.out.println(node.get(i));
				return node.get(i);

			}
		}

		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static <JSONObject> JsonNode getItemObjectUsingId(JsonNode itemList,
			int id) {

		for (int i = 0; i < itemList.size(); i++) {
			System.out.println(i);

			if (itemList.get(i).get("item_id").asText().toString()
					.equals(Integer.toString(id))) {
				return itemList.get(i);
			}
		}

		return null;
	}

	public static JsonNode getRestaurantObjectUsingIdForListing(JsonNode node,
			int id) throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();

		for (int i = 0; i < size; i++) {
			if (node.get(i).get("type").asText().equals("restaurant")) {
				if (node.get(i).get("data").get("id").asInt() == id) {
					System.out.println(node.get(i));
					return node.get(i);
				}
			}
		}
		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static JsonNode verifyRestaurantObjectMessageCount(JsonNode node,
			String type) throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();
		String message = "";
		for (int i = 0; i < size; i++) {
			if (type == "count") {
				if (node.get(i).get("type").asText().equals("messageCard")
						&& node.get(i).get("subtype").asText()
								.equals("sortedRestaurantCount")) {
					message = node.get(i).get("data").get("message").asText();
					if (message.matches(".*\\d.*")) {
						System.out.println(node);
						return node;
					}
				}
			} else if (type == "message") {
				if (node.get(i).get("type").asText().equals("messageCard")
						&& node.get(i).get("subtype").asText()
								.equals("welcome")) {
					if (!node.get(i).get("data").get("message").asText()
							.isEmpty()) {
						return node;
					}
				}
			}
		}
		log.error("No count or message is available for messagecard");
		throw new SwiggyAPIAutomationException("message");
	}

	public static JsonNode getCaraouselNode(JsonNode node, Integer carouselId)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();
		String message = "";
		for (int i = 0; i < size; i++) {
			if (node.get(i).get("type").asText().equals("carousel")) {

				for (int j = 0; j < node.get(i).get("data").get("cards")
						.size(); j++) {

					if (node.get(i).get("data").get("cards").get(j).get("data")
							.get("bannerId").asInt() == carouselId) {
						System.out.println(
								node.get(i).get("data").get("cards").get(j));
						return node.get(i).get("data").get("cards").get(j);
					}
				}

			}
		}
		log.error(message);
		throw new SwiggyAPIAutomationException("message");
	}

	public static boolean verifyOptionSelected(JsonNode node, String option)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();
		String message = "";
		for (int i = 0; i < size; i++) {
			if (node.get(i).get("key").asText()
					.equals("SHOW_RESTAURANTS_WITH")) {
				for (int j = 0; j < node.get(i).get("options").size(); j++) {
					if (node.get(i).get("options").get(j).get("option").asText()
							.equals(option)
							&& node.get(i).get("options").get(j).get("selected")
									.asInt() == 1) {
						System.out.println(node.get(i));
						return true;
					}
				}
			}
		}
		log.error(message);
		throw new SwiggyAPIAutomationException("message");
	}

	public static JsonNode getRestaurantObjectUsingElementForAggregator(
			JsonNode node, int id) throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();
		String restaurantParentKey = String.valueOf(id);
		for (int i = 0; i < size; i++) {

			if (node.get(restaurantParentKey).get("id").asInt() == id) {
				System.out.println(node.get(i));
				return node.get(restaurantParentKey);

			}
		}
		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static JsonNode getRestaurantObjectUsingElementForFees(JsonNode node)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();

		for (int i = 0; i < size; i++) {
			if (node.get(i).get("type").asText().equals("restaurant")) {
				if (node.get(i).get("data").get("feeDetails").get("totalFees")
						.asInt() != 0) {
					System.out.println(node.get(i));
					return node.get(i);
				}
			}
		}

		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static JsonNode getRestaurantObjectUsingElementForVeg(JsonNode node)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();

		for (int i = 0; i < size; i++) {
			if (node.get(i).get("type").asText().equals("restaurant")) {
				if (node.get(i).get("data").get("veg").asBoolean() == true) {
					System.out.println(node.get(i));
					return node.get(i);
				}
			}
		}
		/*
		 * if (node.get("feeDetails").get("totalFees").asInt()!=0) {
		 * System.out.println(node); return node;
		 * 
		 * }
		 */
		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static JsonNode getRestaurantObjectUsingElementForOffers(
			JsonNode node) throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		}
		int size = node.size();

		for (int i = 0; i < size; i++) {
			if (node.get(i).get("type").asText().equals("restaurant")) {
				if (node.get(i).get("data").get("type").asText().equals("D")) {
					System.out.println(node.get(i));
					return node.get(i);
				}
			}
		}

		log.error("unable to get the RestaurantList Is Empty");
		throw new SwiggyAPIAutomationException(
				"unable to get the RestaurantList Is Empty");
	}

	public static JsonNode getFirstObjectFromArray(JsonNode node)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		log.info("retrieving the RestaurantList");
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException(
					"RestaurantList Is Empty"));
			throw new SwiggyAPIAutomationException("RestaurantList Is Empty");
		} else {
			System.out.println(node.get(0));
			return node.get(0);
		}

	}
}