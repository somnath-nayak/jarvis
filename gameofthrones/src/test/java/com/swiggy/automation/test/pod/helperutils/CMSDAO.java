package com.swiggy.automation.test.pod.helperutils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.utils.APITestBase;

public class CMSDAO extends APITestBase {

	public static int getAuthUserId() {

		String query = "select  id from auth_user where is_active=1 and id not in(select auth_user_id from agents)";

		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		int id = 0;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));
			rs = DBUtil.executeQuery(connection, query);
			if (rs.equals(null)) {
				return -999;
			}
			while (rs.next()) {
				id = rs.getInt("id");
				break;
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return id;
	}

	public static int getCreateAuth_User() {
		String name = "test" + System.currentTimeMillis();
		String query = "insert into auth_user(password, is_superuser, username,first_name,last_name,date_joined,is_staff,is_active)VALUES ('abc',0, '"
				+ name + "','hello','hi','2012-02-02',1,1);";

		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		int id = 0;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));
			rs = DBUtil.executeQuery(connection, query);

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return id;
	}

	public static int getTicketCreatedByVendor(String type) {

		String query = "";

		if (type == "add") {
			query = "select * from tickets where state=\"NEW\" and parent_resc_id=1542";
		} else {
			query = "select * from tickets where state=\"NEW\" and parent_resc_id=1542 and resc_id=4391198";
		}
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		int id = 0;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));
			rs = DBUtil.executeQuery(connection, query);
			if (rs.equals(null)) {
				return -999;
			}
			while (rs.next()) {
				id = rs.getInt("id");
				System.out.println("test value" + id);
				break;
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return id;
	}

	public static ArrayList<Integer> getTicketCreatedByVendor1(String type) {

		ArrayList<Integer> data = new ArrayList<>();
		String query = "";
		int i = 0;
		if (type == "add") {
			query = "select * from tickets where state=\"NEW\" and parent_resc_id=1542";
		} else {
			query = "select * from tickets where state=\"NEW\" and parent_resc_id=1542 and resc_id=4391198";
		}
		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		int id[] = new int[5];

		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));

			System.out.println(query);
			rs = DBUtil.executeQuery(connection, query);

			while (rs.next()) {

				data.add(rs.getInt("id"));
				System.out.println(data);
				// System.out.println("test value" + id[i]);

			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return data;
	}

	public static String getAuthKeyOfAgent() {

		String query = "select auth_key from agents where id=(select assigned_to from tickets where state=\"ASSIGNED\" limit 1)";

		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		String auth_key = "";
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));
			rs = DBUtil.executeQuery(connection, query);

			while (rs.next()) {
				auth_key = rs.getString("auth_key");
				break;
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return auth_key;
	}

	public static int getTicketIdToUpdateTicketStatus() {

		String query = "";

		query = "select * from tickets where state=\"ASSIGNED\" and parent_resc_id=1542";

		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		int id = 0;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("cmsselfserve_db_host"),
					serviceNameMap.get("cmsselfserve_db_name"),
					serviceNameMap.get("cmsselfserve_db_user"),
					serviceNameMap.get("cmsselfserve_assword"));
			rs = DBUtil.executeQuery(connection, query);
			if (rs.equals(null)) {
				return -999;
			}
			while (rs.next()) {
				id = rs.getInt("id");
				System.out.println("test value" + id);
				break;
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return id;
	}

}
