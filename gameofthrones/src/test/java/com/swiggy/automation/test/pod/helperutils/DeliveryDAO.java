package com.swiggy.automation.test.pod.helperutils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.utils.APITestBase;

public class DeliveryDAO extends APITestBase {

	public static long getDeliveryBoyOTP(int deliveryBoyId) {

		String query = "select otp from delivery_boys where id="
				+ deliveryBoyId;

		ResultSet rs = null;
		Connection connection = null;
		Statement statement = null;
		long otp = -999;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("delivery_db_host"),
					serviceNameMap.get("delivery_db_name"),
					serviceNameMap.get("delivery_db_user"),
					serviceNameMap.get("delivery_db_password"));
			rs = DBUtil.executeQuery(connection, query);
			while (rs.next()) {
				otp = rs.getLong("otp");
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			DBUtil.close(rs);
			DBUtil.close(statement);
			DBUtil.close(connection);
		}
		return otp;
	}

	public static long getDeliveryBoyAssignedOrderId(int deliveryBoyId) {
		String query = "select order_id from  delivery_boys where id="
				+ deliveryBoyId;
		ResultSet rs = null;

		Connection connection = null;
		long n = -999;
		try {
			connection = DBUtil.getConnection(IDBDriverType.MYSQL,
					serviceNameMap.get("delivery_db_host"),
					serviceNameMap.get("delivery_db_name"),
					serviceNameMap.get("delivery_db_user"),
					serviceNameMap.get("delivery_db_password"));
			rs = DBUtil.executeQuery(connection, query);
			while (rs.next()) {
				n = rs.getLong("order_id");
			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			DBUtil.close(rs);
			DBUtil.close(connection);
		}
		return n;
	}

}