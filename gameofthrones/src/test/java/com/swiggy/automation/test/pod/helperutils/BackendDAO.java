package com.swiggy.automation.test.pod.helperutils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.common.utils.DBConnectionPool;

public class BackendDAO extends RestTestHelper {

	public static Map<Integer, HashMap<String, Integer>> getOpenAndCloseData(
			int[] cityIds) throws SQLException {

		HashMap<Integer, HashMap<String, Integer>> data = new HashMap<Integer, HashMap<String, Integer>>();

		HashMap<String, Integer> cityData = null;

		String query = "select id,open_time,close_time,is_open from city where id in (1,2,3,4,5,6,7,8)";
		System.out.println(query);
		ResultSet rs = null;
		Connection connection = null;
		PreparedStatement prepStmt = null;
		DBConnectionPool connectionPool = null;
		long otp = -999;
		try {
			connectionPool = new DBConnectionPool("com.mysql.jdbc.Driver",
					"jdbc:mysql://" + "52.76.68.4" + ":3306/" + "swiggy",
					"swiggy", "swiggy@2014", 2, 2, true);
			connection = connectionPool.getConnection();
			System.out
					.println("We have got connection from ConnectionPool class");
			prepStmt = connection.prepareStatement(query);
			rs = prepStmt.executeQuery(query);
			while (rs.next()) {
				System.out.println(rs.getInt("id"));
				System.out.println(rs.getInt("open_time"));
				System.out.println(rs.getInt("close_time"));
				cityData = new HashMap<String, Integer>();
				cityData.put("open_time", rs.getInt("open_time"));
				cityData.put("close_time", rs.getInt("close_time"));
				cityData.put("is_open", rs.getInt("is_open"));
				data.put(rs.getInt("id"), cityData);

				System.out.println(data);
				System.out.println(data.get(rs.getInt("id")));

			}

		} catch (Exception e) {

			e.printStackTrace();

		} finally {

			if (rs != null)
				rs.close(); // close resultSet
			if (prepStmt != null)
				prepStmt.close(); // close PreparedStatement

			connectionPool.freeUpConnection(connection);
			connectionPool.closeAllConnections();

		}
		System.out.println("myData :" + data);

		System.out.println(data.get(1));
		System.out.println(data.get(2));
		System.out.println(data.get(3));
		System.out.println(data.get(4));
		System.out.println(data.get(5));
		System.out.println(data.get(6));

		System.out
				.println("We have free/released connection to ConnectionPool class");

		return data;
	}
}
