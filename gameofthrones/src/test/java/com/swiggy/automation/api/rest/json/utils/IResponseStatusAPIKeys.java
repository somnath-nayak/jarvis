package com.swiggy.automation.api.rest.json.utils;

/**
 * 
 * @author mohammedramzi
 *
 */
public interface IResponseStatusAPIKeys {
	String STATUS_MESSAGE_KEY_BACKEND = "statusMessage";
	String STATUS_CODE_KEY_BACKEND = "statusCode";

	String STATUS_MESSAGE_KEY_OMS = "statusMessage";
	String STATUS_CODE_KEY_OMS = "statusCode";
	String STATUS_MESSAGE_KEY_OMS1 = "statusMessage";
	String STATUS_CODE_KEY_OMS1 = "statusCode";
	String STATUS_MESSAGE_KEY_OMS2 = "statusMessage";
	String STATUS_CODE_KEY_OMS2 = "statusCode";

	String STATUS_MESSAGE_KEY_DE = "statusMessage";
	String STATUS_CODE_KEY_DE = "statusCode";

	String STATUS_MESSAGE_KEY = "statusMessage";
	String STATUS_CODE_KEY = "statusCode";

}
