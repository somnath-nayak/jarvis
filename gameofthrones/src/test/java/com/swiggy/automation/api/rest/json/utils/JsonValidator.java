package com.swiggy.automation.api.rest.json.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.reporting.ExtentReporting;
import org.codehaus.jackson.JsonNode;

/**
 * 
 * @author mohammedramzi
 *
 */
public enum JsonValidator {

	DETAILED_LEVEL {

		/**
		 * validate Json detail
		 * 
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param JsonNode
		 *            actualData
		 * 
		 */

		@Override
		public boolean validateJson(JsonNode expectedData,
				JsonNode actualData) {
			boolean isSuccess = false;
			try {

				if (null == expectedData || null == actualData) {

					return false;
				}

				isSuccess = JsonHandler.getJsonHandlerObject()
						.handleObject(expectedData, actualData);
				if (!isSuccess) {
					JsonValidatorUtil.logItOnFailure(isSuccess, expectedData,
							actualData);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return isSuccess;
		}

		/**
		 * validate Json detail
		 * 
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param JsonNode
		 *            actualData
		 * 
		 * @param JsonNode
		 *            apiFieldData
		 * 
		 */

		@Override
		public boolean validateJson(JsonNode expectedData, JsonNode actualData,
				JsonNode apiFieldData) {
			boolean isSuccess = false;
			try {

				if (null == expectedData || null == actualData
						|| null == apiFieldData) {

					return false;
				}
				isSuccess = JsonHandler.getJsonHandlerObject()
						.handleObject(expectedData, actualData, apiFieldData);
				if (!isSuccess) {
					JsonValidatorUtil.logItOnFailure(isSuccess, expectedData,
							actualData, apiFieldData);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return isSuccess;
		}

		/**
		 * validate Json detail
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param ClientResponse
		 *            actualData
		 */

		@Override
		public boolean validateJson(JsonNode expectedData,
				ClientResponse actualData) {
			JsonNode jsonResponseObject = actualData.getEntity(JsonNode.class);
            return validateJson(expectedData, jsonResponseObject);
        }

		/**
		 * Not defined. check other APIs n options
		 * 
		 */

		@Override
		public boolean validateHttpResponse(JsonNode expectedData,
				ClientResponse respo) {
			System.out.println("Not defined...use other APIs");
			return false;
		}

		/**
		 * Not defined. check other APIs n options
		 * 
		 */

		@Override
		public boolean validateJsonWithCustomFields(JsonNode expectedData,
				JsonNode actualData, String[] fields) {
			System.out.println("Not defined...use other APIs");
			return false;
		}

	},
	HIGH_LEVEL {

		/**
		 * validate Json highlevel.Checks stausCode N Message defined as per
		 * IResponseStatusAPIKeys interface
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param JsonNode
		 *            actualData
		 * 
		 */

		@Override
		public boolean validateJson(JsonNode expectedData,
				JsonNode actualData) {
			return JsonHandler.getJsonHandlerObject()
					.validateHighLevel(expectedData, actualData);
		}

		/**
		 * validate Json highlevel with set of fields.
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param ClientResponse
		 *            respo
		 * 
		 */

		@Override
		public boolean validateJsonWithCustomFields(JsonNode expectedData,
				JsonNode actualData, String[] fields) {

			boolean status = false;
			try {

				for (String fieldName : fields) {
					if (expectedData.get(fieldName).toString()
							.equals(actualData.get(fieldName).toString())) {
						status = true;
						continue;
					} else {
						return false;
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			return status;
		}

		/**
		 * validate Json highlevel.Checks stausCode N Message defined as per
		 * IResponseStatusAPIKeys interface
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param ClientResponse
		 *            respo
		 * 
		 */

		@Override
		public boolean validateJson(JsonNode expectedData,
				ClientResponse actualData) {
			JsonNode jsonResponseObject = actualData.getEntity(JsonNode.class);
            return validateJson(expectedData, jsonResponseObject);
        }

		/**
		 * validate httpstatuscode highlevel.Checks httpstatuscode
		 * 
		 * @param JsonNode
		 *            expectedData
		 * 
		 * @param ClientResponse
		 *            response
		 * 
		 */
		@Override
		public boolean validateHttpResponse(JsonNode expectedData,
				ClientResponse respo) {
			try {
				ExtentReporting.getTest().log(LogStatus.INFO,
						"Expected Response/Status: " + expectedData + "<br />");
				ExtentReporting.getTest().log(LogStatus.INFO,
						"Acutal Response/Status: " + respo + "<br />");
				if (expectedData.get("httpstatuscode").asInt() == respo
						.getStatus()) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return false;
		}

		/**
		 * Not defined..check other APIs
		 * 
		 */

		@Override
		public boolean validateJson(JsonNode expectedData, JsonNode actualData,
				JsonNode apiFieldData) {
			System.out.println("Not defined!!!!!!!!!!!!!!!!!!!!!");
			return false;
		}

	};

	public abstract boolean validateJsonWithCustomFields(JsonNode expectedData,
			JsonNode actualData, String[] fields);

	public abstract boolean validateJson(JsonNode expectedData,
			JsonNode actualData);

	public abstract boolean validateJson(JsonNode expectedData,
			ClientResponse actualData);

	public abstract boolean validateHttpResponse(JsonNode expectedData,
			ClientResponse respo);

	public abstract boolean validateJson(JsonNode expectedData,
			JsonNode actualData, JsonNode apiFieldData);

}
