package com.swiggy.automation.api.rest;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import com.swiggy.services.client.SwiggyClient;
import com.swiggy.services.client.SwiggyClientException;
import com.swiggy.services.client.SwiggyClientRequest;
import com.swiggy.services.client.SwiggyClientWrapper;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONObject;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/***
 * 
 * @author mohammedramzi
 * 
 */

public final class RestTestUtil extends RestTestHelper {

	private static final int EEXPECTED_LENGTH = 2;
	private static final String STR_BASIC = "Basic ";
	public static final Logger log = Logger.getLogger(RestTestUtil.class);



	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiKey
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendData(String baseURL, String apiKey,
			String tid, String token) throws SwiggyClientException {
		return invokeClient(baseURL, apiKey, null, tid, token, null);
	}

	
	public static JsonNode sendData(String baseURL, String apiKey) throws SwiggyClientException {
		return invokeClient(baseURL, apiKey, null, null);
	}

	public static JsonNode sendData(String baseURL, String apiKey,
			String authString) throws SwiggyClientException {
		return invokeClient(baseURL, apiKey, null, null, null, authString);
	}


	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @return
	 * @throws SwiggyClientException
	 */
	public static JsonNode sendData(String baseURL,String apiToExecute,
			JsonNode jsonRequestData) throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime.");
		}

		return invokeClient(baseURL, apiToExecute,
				jsonRequestData, defaulTID, defaultTOKEN, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendData(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, String tid, String token)
			throws SwiggyClientException {

	
		return invokeClient(baseURL, apiToExecute,
				jsonRequestData, tid, token, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param authString
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendData(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, String authString)
			throws SwiggyClientException {

	

		return invokeClient(baseURL, apiToExecute,
				jsonRequestData, null, null, authString);
	}


	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendData(String baseURL,
			String apiToExecute, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {

		return invokeClient(baseURL, apiToExecute, jsonRequestData, headers);

	}
	
	
	


	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendDataAsPathParam(String baseURL,String apiToExecute,
			JsonNode jsonRequestData) throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime");
		}

	

		return invokeClientWithPathParam(baseURL, apiToExecute,
				jsonRequestData,null);
	}
	
	
	public static JsonNode sendDataAsPathParam(String baseURL,String apiToExecute,
			JsonNode jsonRequestData,String authString) throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime");
		}

	

		return invokeClientWithPathParam(baseURL, apiToExecute,
				jsonRequestData,null,null,authString);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */
	public static JsonNode sendDataAsPathParam(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, String tid, String token)
			throws SwiggyClientException {

	

		return invokeClientWithPathParam(baseURL, apiToExecute,
				jsonRequestData, tid, token,null);
	}


	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */

	public static JsonNode sendDataAsPathParam(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, Map<String, Object> headers)
			throws SwiggyClientException {

		

		return invokeClientWithPathParam(baseURL, apiToExecute,
				jsonRequestData, headers);
	}


	/**
	 * 
	 * @param apiKey
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiKey)
			throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime..");
		}

	

		return invokeClientForHttpStatusInfo(baseURL, apiKey, null,
				defaulTID, defaultTOKEN, null);
	}

	/**
	 * 
	 * @param apiKey
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiKey,
			String tid, String token) throws SwiggyClientException {

	
		return invokeClientForHttpStatusInfo(baseURL, apiKey, null,
				tid, token, null);
	}

	/**
	 * 
	 * @param apiKey
	 * @param authString
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiKey,
			String authString) throws SwiggyClientException {
	
		return invokeClientForHttpStatusInfo(baseURL, apiKey, null,
				null, null, authString);
	}








	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiKey
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */
	public static ClientResponse sendDataNGetHttpStatus(String baseURL, String apiKey, Map<String, Object> headers)
			throws SwiggyClientException {
		return invokeClientForHttpStatus(baseURL, apiKey, null, headers);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @return
	 * @throws SwiggyClientException
	 */
	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiToExecute,
			JsonNode jsonRequestData) throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime.");
		}


		return invokeClientForHttpStatusInfo(baseURL, apiToExecute,
				jsonRequestData, defaulTID, defaultTOKEN, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, String tid, String token)
			throws SwiggyClientException {


		return invokeClientForHttpStatusInfo(baseURL, apiToExecute,
				jsonRequestData, tid, token, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param authString
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, String authString)
			throws SwiggyClientException {


		return invokeClientForHttpStatusInfo(baseURL, apiToExecute,
				jsonRequestData, null, null, authString);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
//	 */
	public static ClientResponse sendDataNGetHttpStatus(String baseURL,String apiToExecute,
			JsonNode jsonRequestData, Map<String, Object> headers)
			throws SwiggyClientException {

	

		return invokeClientForHttpStatus(baseURL, apiToExecute,
				jsonRequestData, headers);
	}






	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param osrfsid
	 * @param osrftoken
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataWithOSRFTokenNGetHttpStatu(String host,
			Integer port, String apiToExecute, JsonNode jsonRequestData,
			String osrfsid, String osrftoken) throws SwiggyClientException {

		return invokeClientOMSNGetHttpStatus(host, port, apiToExecute,
				jsonRequestData, getRequestHeadersOms(osrftoken, osrfsid));
	}

	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataWithOSRFTokenNGetHttpStatu(String host,
			Integer port, String apiToExecute, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {

		return invokeClientOMSNGetHttpStatus(host, port, apiToExecute,
				jsonRequestData, headers);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataAsPathParamNGetHttpStatus(String baseURL,
			String apiToExecute, JsonNode jsonRequestData)
			throws SwiggyClientException {

		if (null == defaulTID || null == defaultTOKEN) {
			log.warn(
					"tid /token is null..please check.ensure tid and token variables initialized at runtime");
		}

	

		return invokeClientWithPathParamNGetHttpStatus(baseURL,
				apiToExecute, jsonRequestData, defaulTID, defaultTOKEN, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param tid
	 * @param token
	 * @return
	 * @throws SwiggyClientException
	 */
	public static ClientResponse sendDataAsPathParamNGetHttpStatus(String baseURL,
			String apiToExecute, JsonNode jsonRequestData, String tid,
			String token) throws SwiggyClientException {

	

		return invokeClientWithPathParamNGetHttpStatus(baseURL,
				apiToExecute, jsonRequestData, tid, token, null);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param authString
	 * @return
	 * @throws SwiggyClientException
	 */

	public static ClientResponse sendDataAsPathParamNGetHttpStatus(String baseURL,
			String apiToExecute, JsonNode jsonRequestData, String authString)
			throws SwiggyClientException {

	
		return invokeClientWithPathParamNGetHttpStatus(baseURL,
				apiToExecute, jsonRequestData, null, null, authString);
	}

	/**
	 * 
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */
//	public static ClientResponse sendDataAsPathParamNGetHttpStatus(
//			String apiToExecute, JsonNode jsonRequestData,
//			Map<String, Object> headers) throws SwiggyClientException {
//
//	
//
//		return invokeClientWithPathParamNGetHttpStatus(defaultHost, defaultPort,
//				apiToExecute, jsonRequestData, headers);
//	}




	/**
	 * 
	 * @param host
	 * @param port
	 * @param apiToExecute
	 * @param jsonRequestData
	 * @param headers
	 * @return
	 * @throws SwiggyClientException
	 */
//	public static ClientResponse sendDataAsPathParamNGetHttpStatus(String host,
//			Integer port, String apiToExecute, JsonNode jsonRequestData,
//			Map<String, Object> headers) throws SwiggyClientException {
//
//		return invokeClientWithPathParamNGetHttpStatus(host, port, apiToExecute,
//				jsonRequestData, headers);
//
//	}

	// ========================

	public static JsonNode updateRequestDataBeforeSend(JsonNode requestData,
			String key, Object value) {
		JsonNode updatedRequestedNodeObject = null;
		try {

			if (null == requestData || null == key) {
				return null;
			}
			ObjectMapper mapper = new ObjectMapper();

			LinkedHashMap<String, Object> requestMapObject = mapper
					.convertValue(requestData, LinkedHashMap.class);
			if (requestMapObject.containsKey(key)) {
				requestMapObject.put(key, value);
			}
			System.out.println("Updated Reqr " + requestMapObject);

			updatedRequestedNodeObject = mapper.convertValue(requestMapObject,
					JsonNode.class);
			log.info("update the RequestData");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatedRequestedNodeObject;
	}

	// To replace day object value with specific day.
	public static JsonNode updateRequestDataBeforeSendForGetCarouselDay(JsonNode requestData,
													   String key, Object value) {
		JsonNode updatedRequestedNodeObject = null;
		try {

			if (null == requestData || null == key) {
				return null;
			}
			JsonNode requestDt=requestData;
			ObjectMapper mapper = new ObjectMapper();

			if(requestData.size()==1){
					requestData=requestData.get(0);
			}
			LinkedHashMap<String, Object> requestMapObject = mapper
					.convertValue(requestData, LinkedHashMap.class);
			if (requestMapObject.containsKey(key)) {
				requestMapObject.put(key, value);
			}
			System.out.println(requestMapObject);

			updatedRequestedNodeObject = mapper.convertValue(requestMapObject,
					JsonNode.class);
			log.info("update the RequestData");
			if(requestDt.size()==1){
				StringBuilder builder = new StringBuilder("");
				builder.append("[");
				String updateJson=updatedRequestedNodeObject.toString();
				builder.append(updateJson);
				builder.append("]");

				updatedRequestedNodeObject=APIUtils.convertStringtoJSON(builder.toString());

			}


		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatedRequestedNodeObject;
	}

	// ======================================================================
	// Added method to update multiple values at the root level - Abhishek Garg

	public static JsonNode updateRequestDataBeforeSend(JsonNode requestData,
			Map<String, Object> updateData) {
		JsonNode updatedRequestedNodeObject = null;
		// Map<String, String> map = updateData;
		ObjectMapper mapper = new ObjectMapper();
		@SuppressWarnings(value = "unchecked")
		LinkedHashMap<String, Object> requestMapObject = mapper
				.convertValue(requestData, LinkedHashMap.class);
		for (Map.Entry<String, Object> entry : updateData.entrySet()) {
			System.out.println("Updating the Request Key::" + entry.getKey()
					+ " with value::" + entry.getValue());
			try {
				if (null == requestData || null == entry.getKey()) {
					return null;
				}
				if (requestMapObject.containsKey(entry.getKey())) {
					requestMapObject.put(entry.getKey(), entry.getValue());
				}
				// System.out.println(requestMapObject);
				updatedRequestedNodeObject = mapper
						.convertValue(requestMapObject, JsonNode.class);
				System.out.println(updatedRequestedNodeObject);
				log.info("updateRequestDataBeforeSend :"
						+ updatedRequestedNodeObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return updatedRequestedNodeObject;
	}

	// =======================================================================================

	// Added method to update multiple values at the desired level
	// Returns the parent JsonNode Object- Abhishek Garg
	@SuppressWarnings("unchecked")
	public static JsonNode updateRequestDataBeforeSend(JsonNode parentNode,
			String level, Map<String, Object> updateData) {
		JsonNode updatedRequestedNodeObject = null;
		// Map<String, String> map = updateData;
		ObjectMapper mapper = new ObjectMapper();

		JsonNode requestData = parentNode.get(level);
		LinkedHashMap<String, Object> requestMapObject = mapper
				.convertValue(requestData, LinkedHashMap.class);
		for (Map.Entry<String, Object> entry : updateData.entrySet()) {
			System.out.println("Updating the Request Key::" + entry.getKey()
					+ " with value::" + entry.getValue());
			try {
				if (null == requestData || null == entry.getKey()) {
					return null;
				}
				if (requestMapObject.containsKey(entry.getKey())) {
					requestMapObject.put(entry.getKey(), entry.getValue());
				}
				// System.out.println(requestMapObject);
				updatedRequestedNodeObject = mapper
						.convertValue(requestMapObject, JsonNode.class);
				System.out.println(updatedRequestedNodeObject);
				log.info("updateRequestDataBeforeSend :"
						+ updatedRequestedNodeObject);
				((ObjectNode) parentNode).put(level,
						updatedRequestedNodeObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return parentNode;
	}

	// =======================================================================================

	// Added method to update single value at the desired level
	// Returns the parent JsonNode Object- Abhishek Garg
	public static JsonNode updateRequestDataBeforeSend(JsonNode rootNode,
			String desiredNodeLevel, String key, Object value) {

		JsonNode updatedNode = updateRequestDataBeforeSend(
				rootNode.get(desiredNodeLevel), key, value);
		((ObjectNode) rootNode).put(desiredNodeLevel, updatedNode);
		return rootNode;

	}

	//Update request daya for timeslot object
	public static JsonNode updateRequestDataBeforeSendForGetCarouselDay(JsonNode rootNode,
													   String desiredNodeLevel, String key, Object value) {

		JsonNode updatedNode = updateRequestDataBeforeSendForGetCarouselDay(
				rootNode.get(desiredNodeLevel), key, value);
		((ObjectNode) rootNode).put(desiredNodeLevel, updatedNode);
		return rootNode;

	}

	// ===============================================================================================

	public static JsonNode updateRequestDataJsonObject(JsonNode jsonRequestData,
			String Path, String key, Object value) {
		JsonNode xyz = null;
		try {
			String abcd = jsonRequestData.toString();
			System.out.println(abcd);
			JSONObject abcde = new JSONObject(abcd);
			abcde.getJSONObject("area1").put("queryparam", "12345678");
			xyz = APIUtils.convertStringtoJSON(abcde.toString());
			System.out.println(xyz);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return xyz;
	}

	// =======================================================================================

	public static JsonNode getJsonNodeFromMap(
			Map<String, Object> requestMapObject) {
		JsonNode updatedRequestedNodeObject = null;
		try {

			if (null == requestMapObject) {
				return null;
			}
			ObjectMapper mapper = new ObjectMapper();

			updatedRequestedNodeObject = mapper.convertValue(requestMapObject,
					JsonNode.class);
			System.out.println(updatedRequestedNodeObject);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return updatedRequestedNodeObject;
	}

	
	
	private static JsonNode invokeClientWithPathParam(String baseURL,
			String apiKey, JsonNode jsonRequestData, String tid, String token,
			String authString) throws SwiggyClientException {

		String value;
		String requestType = null;
		String requestUrl = null;
		JsonNode reponse = null;
		String paramKey = null;

		try {

			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {

				System.out.println(
						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
				log.error(
						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");

				return null;
			}

			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);

			log.info("****************************************");

			StringBuilder builder = null;

			builder = new StringBuilder(requestUrl);

			builder.append("?");

			Iterator<String> itr = jsonRequestData.getFieldNames();

			while (itr.hasNext()) {

				paramKey = itr.next();

				System.out.println(paramKey);

				System.out.println(jsonRequestData.get(paramKey));

				if (paramKey.toString().equalsIgnoreCase("queryparam")) {

					log.info("queryparam type API under execution");

					// eg: /api/restaurants/321 passed as

					// {"queryparam":"page=0&collection=0"} from i/p data Sheet
					if (jsonRequestData.get(paramKey).asText().contains(" ")) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);
						String newjson = json.replaceAll("\\s", "+");
						newjson = newjson.replaceAll("^\"|\"$", "");

						// String newjson = URLEncoder.encode(json, "utf-8");
						builder.append(newjson);

					} else {
						builder.append(jsonRequestData.get(paramKey).asText());
					}

					jsonRequestData = null;
					break;
				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
						|| paramKey.toString()
								.equalsIgnoreCase("requestbody")) {
					log.info("pathvariable type API under execution");
					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
						replacePathVariable(jsonRequestData, paramKey, builder);
					}
					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
						jsonRequestData = jsonRequestData.get("requestbody");
					}

				} else if (paramKey.toString().contains("placeholder")) {

					replaceplacerHoldersInString(jsonRequestData, paramKey,

							builder);

				} else if (paramKey.toString().contains("lat")
						|| paramKey.toString().contains("uuid")
						|| paramKey.toString().contains("slug")) {
					if (!builder.toString().contains("?"))
						builder.append("?");
					builder.append(paramKey.toString()).append("=")

							.append(jsonRequestData.get(paramKey).asText());

					builder.append("&");
				} else {

					// eg: /api/restaurants? passed as

					// {"page":"0","collection":"0"} from i/p data Sheet

					System.out
							.println(jsonRequestData.get(paramKey).isObject());

					if (jsonRequestData.get(paramKey).isObject()) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);
						// json.replace("{", "%7B");
						// json.replace("}", "%7D");

						String newjson = URLEncoder.encode(json, "utf-8");

						builder.append(paramKey.toString()).append("=")
								.append(newjson);

					} else {
						builder.append(paramKey.toString()).append("=")

								.append(jsonRequestData.get(paramKey).asText());

						builder.append("&");
					}

					// need to check why jsonRequestData is initialized to null
					// jsonRequestData = null;

					// jsonRequestData = null;

				}

			}

			// log.info("API to be executed:" + builder.toString());

			log.info("****************************************");

			return execute(baseURL, builder.toString(), requestType,

					jsonRequestData, tid, token, authString);

		} catch (Exception e) {

			log.error(e.getMessage()

					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);

			e.printStackTrace();

		}

		return reponse;

	}

	
	private static JsonNode invokeClientWithPathParam(String baseURL,
			String apiKey, JsonNode jsonRequestData, HashMap<String, Object>headers) throws SwiggyClientException {

		String value;
		String requestType = null;
		String requestUrl = null;
		JsonNode reponse = null;
		String paramKey = null;

		try {

			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {

				System.out.println(
						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
				log.error(
						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");

				return null;
			}

			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);

			log.info("****************************************");

			StringBuilder builder = null;

			builder = new StringBuilder(requestUrl);

			builder.append("?");

			Iterator<String> itr = jsonRequestData.getFieldNames();

			while (itr.hasNext()) {

				paramKey = itr.next();

				System.out.println(paramKey);

				System.out.println(jsonRequestData.get(paramKey));

				if (paramKey.toString().equalsIgnoreCase("queryparam")) {

					log.info("queryparam type API under execution");

					// eg: /api/restaurants/321 passed as

					// {"queryparam":"page=0&collection=0"} from i/p data Sheet
					if (jsonRequestData.get(paramKey).asText().contains(" ")) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);
						String newjson = json.replaceAll("\\s", "+");
						newjson = newjson.replaceAll("^\"|\"$", "");

						// String newjson = URLEncoder.encode(json, "utf-8");
						builder.append(newjson);

					} else {
						builder.append(jsonRequestData.get(paramKey).asText());
					}

					jsonRequestData = null;
					break;
				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
						|| paramKey.toString()
								.equalsIgnoreCase("requestbody")) {
					log.info("pathvariable type API under execution");
					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
						replacePathVariable(jsonRequestData, paramKey, builder);
					}
					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
						jsonRequestData = jsonRequestData.get("requestbody");
					}

				} else if (paramKey.toString().contains("placeholder")) {

					replaceplacerHoldersInString(jsonRequestData, paramKey,

							builder);

				} else if (paramKey.toString().contains("lat")
						|| paramKey.toString().contains("uuid")
						|| paramKey.toString().contains("slug")) {
					if (!builder.toString().contains("?"))
						builder.append("?");
					builder.append(paramKey.toString()).append("=")

							.append(jsonRequestData.get(paramKey).asText());

					builder.append("&");
				} else {

					// eg: /api/restaurants? passed as

					// {"page":"0","collection":"0"} from i/p data Sheet

					System.out
							.println(jsonRequestData.get(paramKey).isObject());

					if (jsonRequestData.get(paramKey).isObject()) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);
						// json.replace("{", "%7B");
						// json.replace("}", "%7D");

						String newjson = URLEncoder.encode(json, "utf-8");

						builder.append(paramKey.toString()).append("=")
								.append(newjson);

					} else {
						builder.append(paramKey.toString()).append("=")

								.append(jsonRequestData.get(paramKey).asText());

						builder.append("&");
					}

					// need to check why jsonRequestData is initialized to null
					// jsonRequestData = null;

					// jsonRequestData = null;

				}

			}

			// log.info("API to be executed:" + builder.toString());

			log.info("****************************************");

			return execute(baseURL, builder.toString(), requestType,

					jsonRequestData, headers);

		} catch (Exception e) {

			log.error(e.getMessage()

					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);

			e.printStackTrace();

		}

		return reponse;

	}

	private static JsonNode invokeClientWithPathParamSecure(String host,
			Integer port, String apiKey, JsonNode jsonRequestData, String tid,
			String token, String authString, boolean httpsEnabled)
			throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		JsonNode reponse = null;
		String paramKey = null;

		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {

				System.out.println(
						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
				log.error(
						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");

				return null;
			}

			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);

			log.info("****************************************");

			StringBuilder builder = null;

			builder = new StringBuilder(requestUrl);

			builder.append("?");

			Iterator<String> itr = jsonRequestData.getFieldNames();

			while (itr.hasNext()) {

				paramKey = itr.next();

				System.out.println(paramKey);

				System.out.println(jsonRequestData.get(paramKey));

				if (paramKey.toString().equalsIgnoreCase("queryparam")) {

					log.info("queryparam type API under execution");

					// eg: /api/restaurants/321 passed as

					// {"queryparam":"page=0&collection=0"} from i/p data Sheet
					if (jsonRequestData.get(paramKey).asText().contains(" ")) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);
						String newjson = json.replaceAll("\\s", "+");
						newjson = newjson.replaceAll("^\"|\"$", "");

						// String newjson = URLEncoder.encode(json, "utf-8");
						builder.append(newjson);

					} else {
						builder.append(jsonRequestData.get(paramKey).asText());
					}

					jsonRequestData = null;
					break;
				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
						|| paramKey.toString()
								.equalsIgnoreCase("requestbody")) {
					log.info("pathvariable type API under execution");
					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
						replacePathVariable(jsonRequestData, paramKey, builder);
					}
					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
						jsonRequestData = jsonRequestData.get("requestbody");
					}

				} else if (paramKey.toString().contains("placeholder")) {

					replaceplacerHoldersInString(jsonRequestData, paramKey,

							builder);

				} else if (paramKey.toString().contains("lat")
						|| paramKey.toString().contains("uuid")
						|| paramKey.toString().contains("slug")) {
					builder.append("?");
					builder.append(paramKey.toString()).append("=")

							.append(jsonRequestData.get(paramKey).asText());

					builder.append("&");
				} else {

					// eg: /api/restaurants? passed as

					// {"page":"0","collection":"0"} from i/p data Sheet

					if (jsonRequestData.get(paramKey).isObject()) {
						ObjectMapper mapper = new ObjectMapper();
						JsonNode node = jsonRequestData.get(paramKey);

						Object obj = mapper.treeToValue(node, Object.class);
						String json = mapper.writeValueAsString(obj);

						String newjson = URLEncoder.encode(json, "utf-8");

						builder.append(paramKey.toString()).append("=")
								.append(newjson);

					} else {
						builder.append(paramKey.toString()).append("=")

								.append(jsonRequestData.get(paramKey).asText());

						builder.append("&");
					}
				}
			}

			// log.info("API to be executed:" + builder.toString());

			log.info("****************************************");

			return executeHttps(host, port, builder.toString(), requestType,

					jsonRequestData, tid, token, authString, httpsEnabled);

		} catch (Exception e) {

			log.error(e.getMessage()

					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);

			e.printStackTrace();

		}

		return reponse;

	}

	

	private static void replacePathVariable(JsonNode jsonRequestData,
			String paramKey, StringBuilder builder) {
		int index;
		index = builder.indexOf("?");
		System.out.println(index);
		builder.replace(index, index + 1, "/")
				.append(jsonRequestData.get(paramKey).asText());
	}

	private static void replaceplacerHoldersInString(JsonNode jsonRequestData,
			String paramKey, StringBuilder builder) {
		builder.replace(builder.indexOf(paramKey),
				builder.indexOf(paramKey) + paramKey.length(),
				jsonRequestData.get(paramKey).asText());
	}

	private static JsonNode invokeClientWithPathParam(String baseURL,
			String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		JsonNode reponse = null;
		String paramKey = null;
		boolean isRequestBody = false;
		try {

			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {
				System.out.println(
						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
				log.error(
						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");
				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			StringBuilder builder = null;
			builder = new StringBuilder(requestUrl);
			builder.append("?");

			Iterator<String> itr = jsonRequestData.getFieldNames();
			while (itr.hasNext()) {

				paramKey = itr.next();

				System.out.println(paramKey);

				System.out.println(jsonRequestData.get(paramKey));

				if (paramKey.toString().equalsIgnoreCase("queryparam")) {

					log.info("queryparam type API under execution");

					// eg: /api/restaurants/321 passed as

					// {"queryparam":"page=0&collection=0"} from i/p data Sheet

					builder.append(jsonRequestData.get(paramKey).asText());

					// jsonRequestData = null;
					// break;

				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
						|| paramKey.toString()
								.equalsIgnoreCase("requestbody")) {

					log.info("pathvariable type API under execution");
					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
						replacePathVariable(jsonRequestData, paramKey, builder);
					}
					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
						isRequestBody = true;
					}

				} else if (paramKey.toString().contains("placeholder")) {

					replaceplacerHoldersInString(jsonRequestData, paramKey,

							builder);

				} else {

					// eg: /api/restaurants? passed as

					// {"page":"0","collection":"0"} from i/p data Sheet

					builder.append(paramKey.toString()).append("=")

							.append(jsonRequestData.get(paramKey).asText());

					builder.append("&");

				}

			}

			log.info("API to be executed:" + builder.toString());
			System.out.println("BUILDER :" + builder.toString());
			log.info("****************************************");

			if (isRequestBody) {
				jsonRequestData = jsonRequestData.get("requestbody");
			}
			return execute(baseURL, builder.toString(), requestType,
					jsonRequestData, headers);
		} catch (Exception e) {
			log.error(e.getMessage()
					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return reponse;
	}

	private static ClientResponse invokeClientWithPathParamNGetHttpStatus(
			String baseURL, String apiKey, JsonNode jsonRequestData,
			String tid, String token, String authString)
			throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		String paramKey = null;
		boolean isRequestBody = false;
		try {

			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {
				System.out.println(
						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
				log.error(
						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");
				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			StringBuilder builder = null;
			builder = new StringBuilder(requestUrl);
			builder.append("?");

			Iterator<String> itr = jsonRequestData.getFieldNames();
			while (itr.hasNext()) {

				paramKey = itr.next();

				System.out.println(paramKey);

				System.out.println(jsonRequestData.get(paramKey));

				if (paramKey.toString().equalsIgnoreCase("queryparam")) {

					log.info("queryparam type API under execution");

					// eg: /api/restaurants/321 passed as

					// {"queryparam":"page=0&collection=0"} from i/p data Sheet

					builder.append(jsonRequestData.get(paramKey).asText());

					jsonRequestData = null;
					break;

				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
						|| paramKey.toString()
								.equalsIgnoreCase("requestbody")) {

					log.info("pathvariable type API under execution");
					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
						replacePathVariable(jsonRequestData, paramKey, builder);
					}
					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
						isRequestBody = true;
					}

				} else if (paramKey.toString().contains("placeholder")) {

					replaceplacerHoldersInString(jsonRequestData, paramKey,

							builder);

				} else {

					// eg: /api/restaurants? passed as

					// {"page":"0","collection":"0"} from i/p data Sheet

					builder.append(paramKey.toString()).append("=")
							.append(jsonRequestData.get(paramKey).asText());
					builder.append("&");

				}

			}

			log.info("API to be executed:" + builder.toString());
			System.out.println("BUILDER :" + builder.toString());
			log.info("****************************************");

			if (isRequestBody) {
				jsonRequestData = jsonRequestData.get("requestbody");
			}

			log.info("API to be executed:" + builder.toString());
			System.out.println("BUILDER :" + builder.toString());
			log.info("****************************************");

			// return execute(host, port, builder.toString(), requestType,
			// jsonRequestData, tid, token, authString);

			return executeNGetHttpresponse(baseURL, builder.toString(),
					requestType, jsonRequestData, tid, token, authString);
		} catch (Exception e) {
			log.error(e.getMessage()
					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;
	}

//	private static ClientResponse invokeClientWithPathParamNGetHttpStatus(
//			String baseURL, String apiKey, JsonNode jsonRequestData,
//			Map<String, Object> headers) throws SwiggyClientException {
//		String value;
//		String requestType = null;
//		String requestUrl = null;
//		String paramKey = null;
//		boolean isRequestBody = false;
//		try {
//
//			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
//			String requestData[] = value.split("=");
//
//			if (null == requestData || requestData.length < EEXPECTED_LENGTH) {
//				System.out.println(
//						"API not added to api.properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE}");
//				log.error(
//						"API not added to properties | API Value not given properly{Format:KEY=TYPE=APISIGNATURE} in api.properties.");
//				return null;
//			}
//			requestType = requestData[0].trim();
//			requestUrl = requestData[1].trim();
//			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
//			log.info("****************************************");
//
//			StringBuilder builder = null;
//			builder = new StringBuilder(requestUrl);
//			builder.append("?");
//
//			Iterator<String> itr = jsonRequestData.getFieldNames();
//			while (itr.hasNext()) {
//
//				paramKey = itr.next();
//
//				System.out.println(paramKey);
//
//				System.out.println(jsonRequestData.get(paramKey));
//
//				if (paramKey.toString().equalsIgnoreCase("queryparam")) {
//
//					log.info("queryparam type API under execution");
//
//					// eg: /api/restaurants/321 passed as
//
//					// {"queryparam":"page=0&collection=0"} from i/p data Sheet
//
//					builder.append(jsonRequestData.get(paramKey).asText());
//
//					jsonRequestData = null;
//					break;
//				} else if (paramKey.toString().equalsIgnoreCase("pathvariable")
//						|| paramKey.toString()
//								.equalsIgnoreCase("requestbody")) {
//					log.info("pathvariable type API under execution");
//					if (paramKey.toString().equalsIgnoreCase("pathvariable")) {
//						replacePathVariable(jsonRequestData, paramKey, builder);
//					}
//					if (paramKey.toString().equalsIgnoreCase("requestbody")) {
//						isRequestBody = true;
//					}
//
//				} else if (paramKey.toString().contains("placeholder")) {
//
//					replaceplacerHoldersInString(jsonRequestData, paramKey,
//
//							builder);
//
//				} else {
//
//					// eg: /api/restaurants? passed as
//
//					// {"page":"0","collection":"0"} from i/p data Sheet
//
//					builder.append(paramKey.toString()).append("=")
//							.append(jsonRequestData.get(paramKey).asText());
//
//					builder.append("&");
//					// jsonRequestData = null;
//
//					// jsonRequestData = null;
//
//				}
//
//			}
//
//			log.info("API to be executed:" + builder.toString());
//			System.out.println("BUILDER :" + builder.toString());
//			log.info("****************************************");
//
//			if (isRequestBody) {
//				jsonRequestData = jsonRequestData.get("requestbody");
//			}
//
//			return executeNGetHttpresponse(baseURL, builder.toString(),
//					requestType, jsonRequestData, headers);
//		} catch (Exception e) {
//			log.error(e.getMessage()
//					+ ">> invokeClientWithPathParam()   ECZZEPTION IZ :" + e);
//			e.printStackTrace();
//		}
//		return null;
//	}

	private static JsonNode invokeClient(String baseURL,
			String apiKey, JsonNode jsonRequestData, String tid, String token,
			String authString) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return execute(baseURL, requestUrl, requestType, jsonRequestData,
					tid, token, authString);
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static JsonNode invokeClient(String host, Integer port,
			String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return execute(host, port, requestUrl, requestType, jsonRequestData,
					headers);
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}
	
	private static JsonNode invokeClient(String baseUrl,
			String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return execute(baseUrl, requestUrl, requestType, jsonRequestData,
					headers);
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static ClientResponse invokeClientForHttpStatus(String baseURL, String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return executeNGetHttpresponse(baseURL, requestUrl, requestType,
					jsonRequestData, headers);
			
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static ClientResponse invokeClientForHttpStatusInfo(String baseURL, String apiKey, JsonNode jsonRequestData, String tid,
			String token, String authString) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return executeNGetHttpresponse(baseURL, requestUrl, requestType,
					jsonRequestData, tid, token, authString);
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static JsonNode invokeClientOMS(String host, Integer port,
			String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return executeWithOSRFToken(host, port, requestUrl, requestType,
					jsonRequestData, headers);
		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static ClientResponse invokeClientOMSNGetHttpStatus(String host,
			Integer port, String apiKey, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException {
		String value;
		String requestType = null;
		String requestUrl = null;
		try {
			value = STAFAgent.getSTAFAPIInfo(apiKey.trim());
			String requestData[] = value.split("=");

			if (null == requestData) {
				System.out.println("API not added to properties");
				log.error("API Not Defined in api.properties.");

				return null;
			}
			requestType = requestData[0].trim();
			requestUrl = requestData[1].trim();
			log.info("API TYPE and  URL :" + requestType + " , " + requestUrl);
			log.info("****************************************");

			return executeWithOSRFTokenNGetHttpStatus(host, port, requestUrl,
					requestType, jsonRequestData, headers);

		} catch (Exception e) {
			log.error(
					e.getMessage() + ">> invokeClient()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;

	}

	private static JsonNode execute(String baseURL,
			String requestUrl, String requestType, JsonNode jsonRequestData,
			String tid, String token, String authString)
			throws SwiggyClientException, IOException,
			SwiggyAPIAutomationException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(baseURL,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		Map<String, Object> map = new HashMap<String, Object>();

		if (null != tid) {
			log.debug("TID set :" + tid);
			request.setHeaders(getRequestHeaders(map, "tid", tid));
		}
		if (null != token) {
			log.debug("TOKEN set :" + token);
			request.setHeaders(getRequestHeaders(map, "token", token));
			request.setHeaders(getRequestHeaders(map, "accessToken", token));
		}

		if (null != authString) {
			log.debug("authorization set :" + authString);
			if (!authString.startsWith(STR_BASIC)) {
				request.setHeaders(getRequestHeaders(map, "authorization",
						STR_BASIC + getEncodedString(authString)));
			} else if (authString.startsWith(STR_BASIC)) {
				request.setHeaders(
						getRequestHeaders(map, "Authorization", authString));
			}
		}

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

		request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		log.info("client requestData:" + jsonRequestData);
		String resp = client.sendRequest(request, String.class);
		log.info("client reponse:" + resp);
		JsonNode response = APIUtils.convertStringtoJSON(resp);

		log.info("client jsonFormat reponse:" + response);
		return response;
	}

	private static JsonNode executeHttps(String host, Integer port,
			String requestUrl, String requestType, JsonNode jsonRequestData,
			String tid, String token, String authString, boolean httpsEnabled)
			throws SwiggyClientException, IOException,
			SwiggyAPIAutomationException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(host, port,
				requestUrl, false, false, httpsEnabled);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		Map<String, Object> map = new HashMap<String, Object>();

		if (null != tid) {
			log.debug("TID set :" + tid);
			request.setHeaders(getRequestHeaders(map, "tid", tid));
		}
		if (null != token) {
			log.debug("TOKEN set :" + token);
			request.setHeaders(getRequestHeaders(map, "token", token));
			request.setHeaders(getRequestHeaders(map, "accessToken", token));
		}

		if (null != authString) {
			log.debug("authorization set :" + authString);
			if (!authString.startsWith(STR_BASIC)) {
				request.setHeaders(getRequestHeaders(map, "authorization",
						STR_BASIC + getEncodedString(authString)));
			} else if (authString.startsWith(STR_BASIC)) {
				request.setHeaders(
						getRequestHeaders(map, "Authorization", authString));
			}
		}

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

		// request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		log.info("client requestData:" + jsonRequestData);
		String resp = client.sendRequest(request, String.class);
		log.info("client reponse:" + resp);
		JsonNode response = APIUtils.convertStringtoJSON(resp);

		log.info("client jsonFormat reponse:" + response);
		return response;
	}

	private static JsonNode execute(String host, Integer port,
			String requestUrl, String requestType, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException,
            IOException, SwiggyAPIAutomationException {
		
		
		

		SwiggyClientWrapper client = new SwiggyClientWrapper(host, port,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		request.setHeaders(headers);

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

		request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		// return client.sendRequest(request, JsonNode.class);
		log.info("client requestData:" + jsonRequestData);
		String resp = client.sendRequest(request, String.class);
		log.info("client  reponse:" + resp);
		JsonNode response = APIUtils.convertStringtoJSON(resp);
		log.info("client jsonFormat reponse:" + response);
		return response;
	}
	
	
	private static JsonNode execute(String baseURL,
			String requestUrl, String requestType, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException,
            IOException, SwiggyAPIAutomationException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(baseURL,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		request.setHeaders(headers);

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

		//request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		// return client.sendRequest(request, JsonNode.class);
		log.info("client requestData:" + jsonRequestData);
		String resp = client.sendRequest(request, String.class);
		log.info("client  reponse:" + resp);
		JsonNode response = APIUtils.convertStringtoJSON(resp);
		log.info("client jsonFormat reponse:" + response);
		return response;
	}

	private static JsonNode executeWithOSRFToken(String host, Integer port,
			String requestUrl, String requestType, JsonNode jsonRequestData,
			Map<String, Object> headers) throws SwiggyClientException,
            IOException, SwiggyAPIAutomationException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(host, port,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}
		request.setHeaders(headers);
		// request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		log.info("client requestData       :" + jsonRequestData);
		String resp = client.sendRequest(request, String.class);
		log.info("client  String reponse   :" + resp);
		JsonNode response = APIUtils.convertStringtoJSON(resp);
		log.info("client jsonFormat reponse:" + response);
		return response;
		// return client.sendRequest(request, JsonNode.class);
	}

	private static ClientResponse executeWithOSRFTokenNGetHttpStatus(
			String host, Integer port, String requestUrl, String requestType,
			JsonNode jsonRequestData, Map<String, Object> headers)
			throws SwiggyClientException, IOException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(host, port,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}
		request.setHeaders(headers);
		request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		log.info("client requestData:" + jsonRequestData);
		// String resp = client.sendRequest(request, String.class);
		// System.out.println(" ramzi Response :" + resp);
		// JsonNode response = APIUtils.convertStringtoJSON(resp);
		// return response;
		ClientResponse c = client.sendRequest1(request, ClientResponse.class);
		System.out.println(c.getStatus());
		log.info("client HTTP reponse:" + c);
		return c;
		// return client.sendRequest(request, JsonNode.class);
	}

	private static ClientResponse executeNGetHttpresponse(String baseURL, String requestUrl, String requestType,
			JsonNode jsonRequestData, String tid, String token,
			String authString)
			throws SwiggyClientException, IOException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(baseURL,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);
		Map<String, Object> map = new HashMap<String, Object>();

		if (null != tid) {
			log.debug("TID set :" + tid);
			request.setHeaders(getRequestHeaders(map, "tid", tid));
		}
		if (null != token) {
			log.debug("TOKEN set :" + token);
			request.setHeaders(getRequestHeaders(map, "token", token));
			request.setHeaders(getRequestHeaders(map, "accessToken", token));
		}

		if (null != authString) {
			log.debug("authorization set :" + authString);
			if (!authString.startsWith(STR_BASIC)) {
				request.setHeaders(getRequestHeaders(map, "authorization",
						STR_BASIC + getEncodedString(authString)));
			} else if (authString.startsWith(STR_BASIC)) {
				request.setHeaders(
						getRequestHeaders(map, "Authorization", authString));
			}
		}

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

	//	request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
	
		log.info("client requestData:" + jsonRequestData);
		ClientResponse c = client.sendRequest1(request, ClientResponse.class);
		System.out.println("status :" + c.getStatus());
		log.info("client HTTP reponse:" + c);
		return c;
	}

	private static ClientResponse executeNGetHttpresponse(String baseURL, String requestUrl, String requestType,
			JsonNode jsonRequestData, Map<String, Object> headers)
			throws SwiggyClientException, IOException {

		SwiggyClientWrapper client = new SwiggyClientWrapper(baseURL,
				requestUrl, false, false);
		SwiggyClientRequest<Object> request = new SwiggyClientRequest<Object>();
		request.setAcceptType(MediaType.APPLICATION_JSON_TYPE);
		request.setContentType(MediaType.APPLICATION_JSON_TYPE);

		request.setHeaders(headers);

		switch (requestType.trim()) {
		case "POST":
			request.setRequestType(SwiggyClient.RequestType.POST);
			break;
		case "GET":
			request.setRequestType(SwiggyClient.RequestType.GET);
			break;

		case "PUT":
			request.setRequestType(SwiggyClient.RequestType.PUT);
			break;
		case "DELETE":
			request.setRequestType(SwiggyClient.RequestType.DELETE);
			break;
		default:
			break;
		}

		request.setExpectedStatusCode(200);
		request.setRequest(jsonRequestData);
		log.info("client requestData:" + jsonRequestData);
		// return client.sendRequest(request, JsonNode.class);
		ClientResponse c = client.sendRequest1(request, ClientResponse.class);
		log.info("client HTTP reponse:" + c);
		System.out.println("status :" + c.getStatus());
		return c;
	}

	//

	public static String getCustomAuthString(Object field1, Object field2) {
		if (null == field1 || null == field2) {
			log.error("getCustomAuthString pass values are null");
			return null;
		}
		return field1 + ":" + field2;
	}

	public static String getEncodedString(String param)
			throws UnsupportedEncodingException {
		// Convert the string to UTF8 bytes for md5 input
		byte[] binaryData = param.getBytes("UTF8");
		return new String(Base64.encodeBase64(binaryData));
	}

	public static Map<String, Object> getRequestHeadersOms(String token,
			String cookie) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("X-CSRFToken", token);
		map.put("Cookie", "csrftoken=" + token + "; sessionid=" + cookie);

		return map;
	}

	public static Map<String, Object> getRequestHeaders(
			Map<String, Object> map, String key, String value) {
		map.put(key, value);
		return map;
	}

	public void getObject(JsonNode jsonNode, String tillParentPath,
			String[] check) {
		String[] anscestors = tillParentPath.split(".");
		String field = "";
		for (String anscestor : anscestors) {
			field += ".get(" + anscestor + ")";
		}
		String a = jsonNode + field.toString();
		System.out.println(a);

	}
}