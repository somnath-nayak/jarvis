package com.swiggy.automation.api.rest.json.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.swiggy.automation.reporting.ExtentReporting;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * 
 * @author mohammedramzi
 *
 */
public class JsonHandler {

	final static Logger log = Logger.getLogger(JsonHandler.class);

	ArrayList<String> pathFinderList = new ArrayList<String>();

	private static JsonHandler jsonObject = null;

	public static JsonHandler getJsonHandlerObject() {
		if (null == jsonObject) {
			jsonObject = new JsonHandler();
		}
		return jsonObject;
	}

	public int getNumberOfElements(JsonNode n) {
		try {
			if (n.isNull()) {
				return 0;
			}
			return n.size();
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> getNumberOfElements()   ECZZEPTION IZ :" + e);
			ExtentReporting.getTest().log(LogStatus.FAIL, e.getMessage()
					+ "  >> getNumberOfElements()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return -1;
	}

	public void parse(JsonNode expectedJson)
			throws IOException {

		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = expectedJson
					.getFields();

			while (fieldsIterator.hasNext()) {

				Entry<String, JsonNode> field = fieldsIterator.next();
				log.debug("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());
				System.out.println("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());

				if (field.getValue().isObject() && !field.getValue().isNull()) {
					pathFinderList.add(field.getKey());
					System.out.println(pathFinderList);
					parse(field.getValue());
				}
				if (field.getValue().isArray()) {
					// HandleNCompareArrayOfObjects(field.getValue());
				}

				// System.out.println(field.getKey());
				// System.out.println(field.getValue());

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> parse()   ECZZEPTION IZ :" + e);
			ExtentReporting.getTest().log(LogStatus.FAIL,
					e.getMessage() + "  >> parse()   ECZZEPTION IZ :" + e);
			e.printStackTrace();// TODO: handle exception
		}

		System.out.println("lets :" + pathFinderList);

	}

	public boolean handleNCompareArrayOfObjects(JsonNode value,
			JsonNode actualData) {
		boolean isSuccess = false;
		log.debug("handleNCompareArrayOfObjects  >>");
		log.debug(value);
		log.debug(actualData);

		if (value.size() != actualData.size()) {
			System.out.println("size mismatch");
			ExtentReporting.getTest().log(LogStatus.FAIL,
					"Expected & Actual Array Sizes MisMatch");
			return false;
		}

		JsonNode jj;
		try {
			int i = 0;
			Iterator<JsonNode> j = value.iterator();
			while (j.hasNext()) {
				jj = j.next();
				if (jj.isObject()) {
					isSuccess = handleObject(jj, actualData.get(i++)); // need
					if (!isSuccess) {
						return false;
					}
					// improve
					// here .get correct
					// object from
					// actual data list
					// of
					// objects.currently
					// i just selected
					// with index.
				} else if (jj.isTextual()) {
					System.out.println(jj.asText());

				} else {
					System.out.println(jj.toString());
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
					+ e);
			ExtentReporting.getTest().log(LogStatus.FAIL,
					e.getMessage()
							+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
							+ e);
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean handleNCompareArrayOfObjects(JsonNode expectedData,
			JsonNode actualData, JsonNode value) {
		boolean isSuccess = false;
		log.debug("handleNCompareArrayOfObjects  >>");
		log.debug(value);
		log.debug(expectedData);
		log.debug(actualData);

		JsonNode jj;
		try {
			int i = 0;
			Iterator<JsonNode> j = expectedData.iterator();
			while (j.hasNext()) {
				log.debug(j);
				jj = j.next();
				if (jj.isObject()) {
					// actualObject = getMeObjectMatchToexpectedObjectId(jj,
					// actualData);
					isSuccess = handleObject(jj, actualData.get(i),
							value.get(i)); // need
					if (!isSuccess) {
						return false;
					}
					// improve
					// here .get correct
					// object from
					// actual data list
					// of
					// objects.currently
					// i just selected
					// with index.
				} else if (jj.isTextual()) {
					compareValueNode((Entry<String, JsonNode>) jj, actualData);

				} else {
					System.out.println(jj.toString());
				}

				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
					+ e);
			ExtentReporting.getTest().log(LogStatus.FAIL,
					e.getMessage()
							+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
							+ e);
			e.printStackTrace();
		}
		return true;
	}

	public boolean handleObject(JsonNode expected, JsonNode actual) {
		boolean isObjectCheckPass = false, isArrayCheckPass = false,
				isFieldCheckPass = false;
		log.debug("handleObject>>");
		log.debug(expected);
		log.debug(actual);
		Entry<String, JsonNode> field = null;

		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = expected
					.getFields();

			while (fieldsIterator.hasNext()) {

				field = fieldsIterator.next();
				log.debug("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());

				if (field.getKey().startsWith("IGNORE_")) {
					log.debug("IGNORES....Key: " + field.getKey() + "\tValue:"
							+ field.getValue());
					continue;
				}
				if (field.getKey().startsWith("IGNORETYPE_")) {
					log.debug("IGNORES....TYPE: " + field.getKey() + "\tValue:"
							+ field.getValue());
					continue;
				}
				if(field.getKey().startsWith("CHECKFIRSTOBJ")){
					log.debug("---------------------------------");
					String key=field.getKey().toString();
					Integer id= expected.get(key).get(0).get("id").asInt();
					JsonNode expectedNode = getFirstObjectOfJSONArray(expected.get(key),id);

					key=field.getKey().split("CHECKFIRSTOBJ_")[1];
					JsonNode actualNode = getFirstObjectOfJSONArray(actual.get(key),id);

					boolean isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedNode,actualNode);

					if(isSuccess==false) {
                        return false;
                    }
					continue;
				}
				if (field.getKey().startsWith("MINCHECK")) {

					if (field.getKey().startsWith("MINCHECKSIZE_")) {

						String key = field.getKey().split("MINCHECKSIZE_")[1];
						if (!compareNodeValueSize(field, actual, key.trim())) {
							log.error(
									"MINCHECKTYPE FAILED FOR NODE VALUE SIZE>>>Key:"
											+ field.getKey() + "***Expected: "
											+ field + "**** Actual " + actual);
							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKSIZE FAILED FOR NODE VALUE SIZE>>>Key:"
											+ field.getKey() + "<br />"
											+ "***Expected: " + field + "<br />"
											+ "**** Actual " + actual
											+ "<br />");
							return false;
						}

					} else if (field.getKey().startsWith("MINCHECKTYPE_")) {
						String key = field.getKey().split("MINCHECKTYPE_")[1];
						System.out.println(field);
						if (!compareNodeValueType(field, actual, key.trim())) {
							log.error(
									"MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key: "
											+ field.getKey() + "***Expected: "
											+ field + "**** Actual " + actual);
							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key:"
											+ field.getKey() + "<br />"
											+ "***Expected: " + field + "<br />"
											+ "**** Actual " + actual
											+ "<br />");
							return false;
						}

					} else if (field.getKey().startsWith("MINCHECKFIELD_")) {
						log.debug("------------------------------------------");
						log.debug("Expected Key  :" + field.getKey());
						log.debug("Expected Data :" + field.getValue());
						log.debug("Actual Data   :" + actual.get(
								field.getKey().split("MINCHECKFIELD_")[1]));
						log.debug(
								"===========================================");
						if (!actual.has(
								field.getKey().split("MINCHECKFIELD_")[1])) {
							log.error(
									"MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key:"
											+ field.getKey() + "***Expected: "
											+ field + "**** Actual:" + actual);
							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key:"
											+ "<br />" + field.getKey()
											+ "***Expected: " + "<br />" + field
											+ "**** Actual:" + actual
											+ "<br />");
							return false;
						}
					}

					continue;
				}

				if (field.getValue().isObject() && !field.getValue().isNull()) {

					// log.debug("AAAAAAAAAAAAAAAAAAAAAAAAAAA");

					isObjectCheckPass = handleObject(field.getValue(),
							actual.get(field.getKey()));
					if (!isObjectCheckPass) {
						System.out.println("Object Check Failed!!!!!");
						log.error("Object Check Failed!!!!!");
						log.error(
								"()()()()()()()()()()()()()()()()()()()()()()");
						log.error("Key      :" + field.getKey());
						log.error("Expected :" + field.getValue());
						log.error("Actual   :" + actual.get(field.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Object Check Failed!!!!!" + "<br />"
										+ "()()()()()()()()()()()()()()()()()()()()()()"
										+ "<br />" + "Key      :"
										+ field.getKey() + "<br />"
										+ "Expected :" + field.getValue()
										+ "</br />" + "Actual   :"
										+ actual.get(field.getKey())
										+ "<br />");
						return false;
					}
				}

				else if (field.getValue().isArray()) {
					log.debug(field.getValue()
							+ " is  Array List of Json Objects");
					if (field.getValue().size() != actual.get(field.getKey())
							.size()) {
						log.warn(
								"--------------------------------------------------");
						log.warn(
								"MIATCH in Value size for arrayElement having key:"
										+ field.getKey());
						log.warn("MIATCH :expected Array elements :"
								+ field.getValue().size()
								+ " But,Actutal ArrayElements "
								+ actual.get(field.getKey()));

						log.warn("Expected:" + field.getValue());
						log.warn("Actual  :" + actual.get(field.getKey()));
						log.warn(
								"--------------------------------------------------");
					}

					if (field.getValue().size() < actual.get(field.getKey())
							.size()) {
						log.warn("More Objects present in Actual Data");

					}

					isArrayCheckPass = handleNCompareArrayOfObjects(
							field.getValue(), actual.get(field.getKey()));

					if (!isArrayCheckPass) {
						log.error("Array Check Failed!!!!!");
						log.error(
								"[][][][][][][][][][][][][][][][][][][][][][][][]");
						log.error("Key      :" + field.getKey());
						log.error("Expected :" + field.getValue());
						log.error("Actual   :" + actual.get(field.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Array Check Failed!!!!!" + "<br />"
										+ "[][][][][][][][][][][][][][][][][][][][][][][][]"
										+ "<br />" + "Key      :"
										+ field.getKey() + "<br />"
										+ "Expected :" + field.getValue()
										+ "<br />" + "Actual   :"
										+ actual.get(field.getKey())
										+ "<br />");
						return false;
					}

				}

				else if (field.getValue().isValueNode()) {
					isFieldCheckPass = handleNCompareValueNode(field, actual);

					if (!isFieldCheckPass) {
						log.error("Field Check Check Failed!!!!!");
						log.error("Key      :" + field.getKey());
						log.error("________________________________________");
						log.error("Expected :" + field.getValue());
						log.error("Actual   :" + actual.get(field.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Field Check Failed!!!!!" + "<br />"
										+ "Key      :" + field.getKey()
										+ "<br />"
										+ "________________________________________"
										+ "<br />" + "Expected :"
										+ field.getValue() + "<br />"
										+ "Actual   :"
										+ actual.get(field.getKey())
										+ "<br />");

						return false;
					}

				} else {
					log.info(
							"JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
									+ field.getValue());

					ExtentReporting.getTest().log(LogStatus.FAIL,
							"JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
									+ field.getValue());
					return false;
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> handleObject()   ECZZEPTION IZ :"
					+ e);
			ExtentReporting.getTest().log(LogStatus.FAIL, e.getMessage()
					+ "  >> handleObject()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public boolean handleObject(JsonNode expected, JsonNode actual,
			JsonNode apiExpectedFieldsData) {
		boolean isObjectCheckPass = false, isArrayCheckPass = false,
				isFieldCheckPass = false;
		log.debug("handleObject>>");
		log.debug("API FIELD:" + apiExpectedFieldsData);
		log.debug("EXPECTED :" + expected);
		log.debug("ACTUAL   :" + actual);
		Entry<String, JsonNode> apiExpectedField = null;

		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = apiExpectedFieldsData
					.getFields();
			while (fieldsIterator.hasNext()) {

				apiExpectedField = fieldsIterator.next();
				log.debug("Key: " + apiExpectedField.getKey() + "\tValue:"
						+ apiExpectedField.getValue());

				if (apiExpectedField.getKey().startsWith("IGNORE_")) {
					continue;
				}

				if (apiExpectedField.getKey().startsWith("CONVERTNCHECK_")) {
					String key = null;
					key = apiExpectedField.getKey().split("CONVERTNCHECK_")[1];

					if (!convertNompareNodeValue(expected, actual, key)) {
						log.error(
								"CONVERTNCHECK_ FAILED FOR NODE VALUE SIZE>>>KEY:"
										+ apiExpectedField.getKey()
										+ "***EXPECTED VALUE: "
										+ expected.get(key)
										+ "**** ACTUAL VALUE: "
										+ actual.get(key));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"CONVERTNCHECK_ FAILED FOR NODE VALUE SIZE>>>KEY:"
										+ apiExpectedField.getKey() + "</br />"
										+ "***EXPECTED VALUE: "
										+ expected.get(key) + "<br />"
										+ "**** ACTUAL VALUE: "
										+ actual.get(key) + "<br />");
						return false;
					}

					continue;

				}

				if (apiExpectedField.getKey()
						.startsWith("CONVERTNCHECKFLOAT_")) {
					String key = null;
					key = apiExpectedField.getKey()
							.split("CONVERTNCHECKFLOAT_")[1];

					if (!convertNompareFloatNodeValue(expected, actual, key)) {
						log.error(
								"CONVERTNCHECK_ FAILED FOR NODE VALUE SIZE>>>KEY:"
										+ apiExpectedField.getKey()
										+ "***EXPECTED VALUE: "
										+ expected.get(key)
										+ "**** ACTUAL VALUE: "
										+ actual.get(key));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"CONVERTNCHECK_ FAILED FOR NODE VALUE SIZE>>>KEY:"
										+ apiExpectedField.getKey() + "<br />"
										+ "***EXPECTED VALUE: "
										+ expected.get(key) + "<br />"
										+ "**** ACTUAL VALUE: "
										+ actual.get(key) + "<br />");
						return false;
					}

					continue;

				}

				if (apiExpectedField.getKey().startsWith("MINCHECK")) {
					String key = null;
					if (apiExpectedField.getKey().startsWith("MINCHECKSIZE_")) {
						key = apiExpectedField.getKey()
								.split("MINCHECKSIZE_")[1];
						if (!compareNodeValueSize(expected, actual, key)) {
							log.error(
									"MINCHECKTYPE FAILED FOR NODE VALUE SIZE>>>Key:"
											+ apiExpectedField.getKey()
											+ "***EXPECTED VALUE: "
											+ expected.get(key)
											+ "**** ACTUAL VALUE: "
											+ actual.get(key));

							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKTYPE FAILED FOR NODE VALUE SIZE>>>Key:"
											+ apiExpectedField.getKey()
											+ "<br />" + "***EXPECTED VALUE: "
											+ expected.get(key) + "<br />"
											+ "**** ACTUAL VALUE: "
											+ actual.get(key) + "<br />");
							return false;
						}

					} else if (apiExpectedField.getKey()
							.startsWith("MINCHECKTYPE_")) {
						key = apiExpectedField.getKey()
								.split("MINCHECKTYPE_")[1];
						if (!compareNodeValueType(expected, actual, key)) {
							log.error(
									"MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key:"
											+ apiExpectedField.getKey()
											+ "***EXPECTED VALUE: "
											+ expected.get(key)
											+ "**** ACTUAL VALUE: "
											+ actual.get(key));

							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key:"
											+ apiExpectedField.getKey()
											+ "<br />" + "***EXPECTED VALUE: "
											+ expected.get(key) + "<br />"
											+ "**** ACTUAL VALUE: "
											+ actual.get(key) + "<br />");
							return false;
						}

					} else if (apiExpectedField.getKey()
							.startsWith("MINCHECKFIELD_")) {
						key = apiExpectedField.getKey()
								.split("MINCHECKFIELD_")[1];
						if (!actual.has(key)) {
							log.error(
									"MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key not found in actual: key missing is:"
											+ apiExpectedField.getKey()
											+ "***Expected: "
											+ expected.get(key));

							ExtentReporting.getTest().log(LogStatus.FAIL,
									"MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key not found in actual: key missing is:"
											+ apiExpectedField.getKey()
											+ "<br />" + "***Expected: "
											+ expected.get(key) + "<br />");
							return false;
						}
					}

					continue;
				}

				if (apiExpectedField.getValue().isObject()
						&& !apiExpectedField.getValue().isNull()) {

					isObjectCheckPass = handleObject(
							expected.get(apiExpectedField.getKey()),
							actual.get(apiExpectedField.getKey()),
							apiExpectedField.getValue());
					if (!isObjectCheckPass) {
						log.error("Object Check Failed!!!!!");
						log.error("apiField :" + apiExpectedField.getValue());
						log.error("Expected   :"
								+ expected.get(apiExpectedField.getKey()));
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Object Check Failed!!!!!" + "<br />"
										+ "()()()()()()()()()()()()()()()()()()()()()()"
										+ "<br />" + "Key      :"
										+ apiExpectedField.getKey() + "<br />"
										+ "Expected :"
										+ apiExpectedField.getValue()
										+ "</br />" + "Actual   :"
										+ actual.get(apiExpectedField.getKey())
										+ "<br />");
						return false;
					}
				}

				else if (apiExpectedField.getValue().isArray()) {
					log.debug(apiExpectedField.getValue()
							+ " is  Array List of Json Objects");
					if (apiExpectedField.getKey()
							.startsWith("IGNOREARRAYSIZE_")) {

						if (expected.get(apiExpectedField.getKey())
								.size() != actual.get(apiExpectedField.getKey())
										.size()) {
							log.debug(
									"We are Ignoring the Array Size Check..  using prefix IGNOREARRAYSIZE_");
							log.debug("Expected Size:" + expected
									.get(apiExpectedField.getKey()).size());
							log.debug("Actual   Size:" + actual
									.get(apiExpectedField.getKey()).size());

						} else {
							log.debug("ArraySize Match");
							log.debug("Expected Size:" + expected
									.get(apiExpectedField.getKey()).size());
							log.debug("Actual   Size:" + actual
									.get(apiExpectedField.getKey()).size());

							ExtentReporting.getTest().log(LogStatus.FAIL,
									"ArraySize Match" + "<br />"
											+ "Expected Size:"
											+ expected.get(
													apiExpectedField.getKey())
													.size()
											+ "<br />" + "Actual   Size:"
											+ actual.get(
													apiExpectedField.getKey())
													.size()
											+ "<br />");
						}
					} else if (!apiExpectedField.getKey()
							.startsWith("IGNOREARRAYSIZE_")) {
						if (expected.get(apiExpectedField.getKey())
								.size() != actual.get(apiExpectedField.getKey())
										.size()) {
							log.error("Expected Size:" + expected
									.get(apiExpectedField.getKey()).size());
							log.error("Actual   Size:" + actual
									.get(apiExpectedField.getKey()).size());
							log.error(
									"Expected and Actual ArraySize Mismatch....Use IGNOREARRAYSIZE_ Prefix in Array List Key to Avoid the ArrayCheck if needed");

							ExtentReporting.getTest().log(LogStatus.FAIL,
									"Expected and Actual ArraySize Mismatch....Use IGNOREARRAYSIZE_ Prefix in Array List Key to Avoid the ArrayCheck if needed"
											+ "Expected Size:"
											+ expected.get(
													apiExpectedField.getKey())
													.size()
											+ "<br />" + "Actual   Size:"
											+ actual.get(
													apiExpectedField.getKey())
													.size()
											+ "<br />");

							return false;
						} else {
							log.info("ArraySize Match");
							log.info("Expected Size:" + expected
									.get(apiExpectedField.getKey()).size());
							log.info("Actual   Size:" + actual
									.get(apiExpectedField.getKey()).size());
						}
					}

					if (expected.get(apiExpectedField.getKey()).size() < actual
							.get(apiExpectedField.getKey()).size()) {
						log.warn(
								"More Objects present in Actual Data.. Please Include those fields in Expected Fields");

					}

					isArrayCheckPass = handleNCompareArrayOfObjects(

							expected.get(apiExpectedField.getKey()),
							actual.get(apiExpectedField.getKey()),
							apiExpectedField.getValue());

					if (!isArrayCheckPass) {
						log.error("Array Check Failed!!!!!");
						log.error("Key      :" + apiExpectedField.getKey());
						log.error("Expected :" + apiExpectedField.getValue());
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Array Check Failed!!!!!" + "<br />"
										+ "Key      :"
										+ apiExpectedField.getKey() + "<br />"
										+ "Expected :"
										+ apiExpectedField.getValue() + "<br />"
										+ "Actual   :"
										+ actual.get(apiExpectedField.getKey())
										+ "<br />");
						return false;
					}

				}

				else if (apiExpectedField.getValue().isValueNode()) {
					isFieldCheckPass = handleNCompareValueNode(expected, actual,
							apiExpectedField.getKey());

					if (!isFieldCheckPass) {
						log.error("Field Check Check Failed!!!!!");
						log.error("Key      :" + apiExpectedField.getKey());
						log.error("Expected :"
								+ expected.get(apiExpectedField.getKey()));
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));

						ExtentReporting.getTest().log(LogStatus.FAIL,
								"Field Check Check Failed!!!!!" + "<br />"
										+ "Key      :"
										+ apiExpectedField.getKey() + "<br />"
										+ "Expected :"
										+ expected
												.get(apiExpectedField.getKey())
										+ "<br />" + "Actual   :"
										+ actual.get(apiExpectedField.getKey())
										+ "<br />");

						return false;
					}

				} else {
					log.info(
							"JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
									+ apiExpectedField.getValue());

					ExtentReporting.getTest().log(LogStatus.FAIL,
							"JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
									+ apiExpectedField.getValue());
					return false;
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> handleObject()   ECZZEPTION IZ :"
					+ e);
			ExtentReporting.getTest().log(LogStatus.FAIL, e.getMessage()
					+ "  >> handleObject()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public boolean handleNCompareValueNode(Entry<String, JsonNode> field,
			JsonNode actualData) {

		if (false == compareValueNode(field, actualData)) {
			log.error("Comparison Failed for Value Node");
			// log.error("Exected :" + field);
			// log.error("Actual :" + actualData);

			return false;
		}

		// validate with different dataType for accuracy

		if (!field.getKey().startsWith("IGNORE_")
				|| !field.getKey().startsWith("MINCHECK_")) {
			if (field.getValue().asText().toString().equals(
					actualData.get(field.getKey()).asText().toString())) {
				return true;
			}
		}

		if (field.getKey().startsWith("IGNORE_")) {
			log.info("Ignores field :" + field.getKey());
			return true;
		}
		log.error("Comparison Failed for Value Node");
		log.error("Exected :" + field);
		log.error("Actual  :" + actualData.get(field.getKey()));
		return false;

	}

	public boolean handleNCompareValueNode(JsonNode expected,
			JsonNode actualData, String key) {

		if (false == compareValueNode(expected, actualData, key)) {
			log.error("Comparison Failed for Value Node");
			// log.error("Key :" + key);
			// log.error("Exected :" + expected.get(key));
			// log.error("Actual :" + actualData.get(key));

			return false;
		}

		return true;

	}

	private boolean compareValueNode(Entry<String, JsonNode> field,
			JsonNode actualData) {
		log.debug("--------------------------------------------------");
		log.debug("Expected Key       :" + field.getKey());
		log.debug("Expected Value     :" + field.getValue());
		log.debug("Actual   Value     :" + actualData.get(field.getKey()));
		log.debug("=================================================");

		if (actualData.get(field.getKey()) == null) {
			System.out.println(
					"Expected Key/Attribute Not Present in Actual Data > Expected Key:"
							+ field.getKey());

			log.error(
					"Expected Key/Attribute Not Present in Actual Data > Expected Key:"
							+ field.getKey());

			ExtentReporting.getTest().log(LogStatus.FAIL,
					"Expected Key/Attribute Not Present in Actual Data > Expected Key:"
							+ field.getKey());

			return false;
		}

		if (field.getValue().isNull() || field.getValue().isTextual()) {
			if (field.getValue().asText().toString().equals(
					actualData.get(field.getKey()).asText().toString())) {
				return true;
			}

		}

		if (field.getValue().isBoolean()) {
			if (field.getValue().asBoolean() == (actualData.get(field.getKey())
					.asBoolean())) {
				return true;
			}

		}

		if (field.getValue().isFloatingPointNumber()

				|| field.getValue().isDouble()) {
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				return true;
			}
		}

		if (field.getValue().isInt()) {
			if (field.getValue()
					.asInt() == (actualData.get(field.getKey()).asInt())) {
				return true;
			}
		}

		if (field.getValue().isLong()) {

            return field.getValue()
                    .asLong() == (actualData.get(field.getKey()).asLong());

        }

		if (field.getValue().isBigInteger()) {
			if (field.getValue()
					.asLong() == (actualData.get(field.getKey()).asLong())) {
				return true;
			}

		}
		if (field.getValue().isBinary()) {
			return true;
		}

		if (field.getValue().isDouble()) {
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				return true;
			}
		}
		if (field.getValue().isFloatingPointNumber()) {
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				return true;
			}
		}
		return false;

	}

	private boolean compareValueNode(JsonNode expectedData, JsonNode actualData,
			String key) {

		log.debug("--------------------------------------------------");
		log.debug("Expected Key Passed:" + key);
		log.debug("Expected Value     :" + expectedData.get(key));
		log.debug("Actual   Value     :" + actualData.get(key));
		log.debug("=================================================");
		if (actualData.get(key) == null) {
			System.out.println(
					"Element Not Present in Actual Data :>>>>>>>>>>>   " + key);

			log.error(key + "   Element Not Present in Actual Data Response");
			ExtentReporting.getTest().log(LogStatus.FAIL,
					key + "   Element Not Present in Actual Data Response");

			return false;
		}

		if ((expectedData.get(key).isNull() && actualData.get(key).isNull())
				|| expectedData.get(key).isTextual()
						&& actualData.get(key).isTextual()) {
			if (expectedData.get(key).asText().toString()
					.equals(actualData.get(key).asText().toString())) {
				log.debug("text field check pass");
				return true;
			}

		}

		if (expectedData.get(key).isBoolean()
				&& actualData.get(key).isBoolean()) {
			if (expectedData.get(key)
					.asBoolean() == (actualData.get(key).asBoolean())) {
				return true;
			}

		}

		if ((expectedData.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber())

				|| (expectedData.get(key).isDouble()
						&& actualData.get(key).isDouble())) {

			if (expectedData.get(key)
					.asDouble() == (actualData.get(key).asDouble())) {

				return true;
			}
		}

		if (expectedData.get(key).isInt() && actualData.get(key).isInt()) {
			if (expectedData.get(key)
					.asInt() == (actualData.get(key).asInt())) {
				log.debug("int field check pass");
				return true;
			}
		}

		if (expectedData.get(key).isLong() && actualData.get(key).isLong()) {

			if (expectedData.get(key)
					.asLong() == (actualData.get(key).asLong())) {
				log.debug("Long field check pass");
				return true;

			}

			return false;
		}

		if (expectedData.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			if (expectedData.get(key)
					.asLong() == (actualData.get(key).asLong())) {
				log.debug("BigInteger/Long field check pass");
				return true;
			}

		}
		if (expectedData.get(key).isBinary()
				&& actualData.get(key).isBinary()) {
			log.debug("binay field check pass");
			return true;
		}

		if (expectedData.get(key).isDouble()
				&& actualData.get(key).isDouble()) {
			if (expectedData.get(key)
					.asDouble() == (actualData.get(key).asDouble())) {
				log.debug("double field check pass");
				return true;
			}
		}
		if (expectedData.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()) {

			if (expectedData.get(key)
					.asDouble() == (actualData.get(key).asDouble())) {
				log.debug("float/double field check pass");
				return true;
			}
		}
		return false;

	}

	private boolean compareNodeValueType(Entry<String, JsonNode> field,
			JsonNode actualData, String key) {

		log.debug("--------------------------------------------------");
		log.debug("Expected Key Passed:" + key);
		log.debug("Expected Value     :" + field.getValue());
		log.debug("Actual   Value     :" + actualData.get(key));
		log.debug("=================================================");
		if (field.getValue().isTextual() && actualData.get(key).isTextual()) {
			return true;
		}

		if (field.getValue().isBoolean() && actualData.get(key).isBoolean()) {
			return true;
		}

		if (field.getValue().isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| field.getValue().isDouble()
						&& actualData.get(key).isDouble()) {
			return true;
		}

		if (field.getValue().isInt() && actualData.get(key).isInt()) {
			return true;
		}

		if (field.getValue().isLong() && actualData.get(key).isLong()) {
			return true;
		}

		if (field.getValue().isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			return true;
		}

		if (field.getValue().isBinary() && actualData.get(key).isBinary()) {
			return true;
		}

		if (field.getValue().isDouble() && actualData.get(key).isDouble()) {
			return true;
		}

		if (field.getValue().isNumber() && actualData.get(key).isNumber()) {
			return true;
		}

        return field.getValue().isNull() && actualData.get(key).isNull()
                || field.getValue().isNull()
                && actualData.get(key).equals("");

    }

	private boolean compareNodeValueType(JsonNode expectedData,
			JsonNode actualData, String key) {

		log.debug("--------------------------------------------------");
		log.debug("Expected Key Passed:" + key);
		log.debug("Expected Value     :" + expectedData.get(key));
		log.debug("Actual   Value     :" + actualData.get(key));
		log.debug("=================================================");
		if (expectedData.get(key).isTextual()
				&& actualData.get(key).isTextual()) {
			return true;
		}

		if (expectedData.get(key).isBoolean()
				&& actualData.get(key).isBoolean()) {
			return true;
		}

		if (expectedData.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| expectedData.get(key).isDouble()
						&& actualData.get(key).isDouble()) {
			return true;
		}

		if (expectedData.get(key).isInt() && actualData.get(key).isInt()) {
			return true;
		}

		if (expectedData.get(key).isLong() && actualData.get(key).isLong()) {
			return true;
		}

		if (expectedData.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			return true;
		}

		if (expectedData.get(key).isBinary()
				&& actualData.get(key).isBinary()) {
			return true;
		}

		if (expectedData.get(key).isDouble()
				&& actualData.get(key).isDouble()) {
			return true;
		}

        return expectedData.get(key).isNull() && actualData.get(key).isNull()
                || expectedData.get(key).isNull()
                && actualData.get(key).equals("");

    }

	private boolean compareNodeValueSize(Entry<String, JsonNode> field,
			JsonNode actualData, String key) {
		log.debug("--------------------------------------------------");
		log.debug("Expected Key Passed:" + key);
		log.debug("Expected Value     :" + field.getValue());
		log.debug("Actual   Value     :" + actualData.get(key));
		log.debug("=================================================");

		if (field.getValue().isTextual() && actualData.get(key).isTextual()) {

			if (field.getValue().asText().length() == actualData.get(key)
					.asText().length()) {
				return true;
			}
		}

		if (field.getValue().isBoolean() && actualData.get(key).isBoolean()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| field.getValue().isDouble()
						&& actualData.get(key).isDouble()) {

			if ((float) Math.log10(field.getValue().asDouble())
					+ 1 == (float) Math.log10(actualData.get(key).asDouble())
							+ 1) {
				return true;
			}

		}

		if (field.getValue().isInt() && actualData.get(key).isInt()) {
			if ((int) Math.log10(field.getValue().asInt())
					+ 1 == (int) Math.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (field.getValue().isLong() && actualData.get(key).isLong()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			if ((long) Math.log10(field.getValue().asInt())
					+ 1 == (long) Math.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (field.getValue().isBinary() && actualData.get(key).isBinary()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isDouble() && actualData.get(key).isDouble()) {
			if (Math.log10(field.getValue().asDouble())
					+ 1 == Math.log10(actualData.get(key).asDouble())
							+ 1) {
				return true;
			}
		}

		if (field.getValue().isNumber() && actualData.get(key).isNumber()) {
			if ((long) Math.log10(field.getValue().asLong())
					+ 1 == (long) Math.log10(actualData.get(key).asLong())
							+ 1) {
				return true;
			}
		}
		if (field.getValue().isObject() && actualData.get(key).isObject()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isArray() && actualData.get(key).isArray()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		return false;

	}

	private boolean compareNodeValueSize(JsonNode expected, JsonNode actualData,
			String key) {
		log.debug("--------------------------------------------------");
		log.debug("Expected Key Passed:" + key);
		log.debug("Expected Value     :" + expected.get(key));
		log.debug("Actual   Value     :" + actualData.get(key));
		log.debug("=================================================");

		if (expected.get(key).isTextual() && actualData.get(key).isTextual()) {

			if (expected.get(key).asText().length() == actualData.get(key)
					.asText().length()) {
				return true;
			}
		}

		if (expected.get(key).isBoolean() && actualData.get(key).isBoolean()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| expected.get(key).isDouble()
						&& actualData.get(key).isDouble()) {

			if ((float) Math.log10(expected.get(key).asDouble())
					+ 1 == (float) Math.log10(actualData.get(key).asDouble())
							+ 1) {
				return true;
			}

		}

		if (expected.get(key).isInt() && actualData.get(key).isInt()) {
			if ((int) Math.log10(expected.get(key).asInt())
					+ 1 == (int) Math.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (expected.get(key).isLong() && actualData.get(key).isLong()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			if ((long) Math.log10(expected.get(key).asInt())
					+ 1 == (long) Math.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (expected.get(key).isBinary() && actualData.get(key).isBinary()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isDouble() && actualData.get(key).isDouble()) {
			if (Math.log10(expected.get(key).asDouble())
					+ 1 == Math.log10(actualData.get(key).asDouble())
							+ 1) {
				return true;
			}
		}

		if (expected.get(key).isNumber() && actualData.get(key).isNumber()) {
			if ((long) Math.log10(expected.get(key).asLong())
					+ 1 == (long) Math.log10(actualData.get(key).asLong())
							+ 1) {
				return true;
			}
		}

		return false;

	}

	private boolean convertNompareNodeValue(JsonNode expected,
			JsonNode actualData, String key) {

        return expected.get(key).asText().toString()
                .equals(actualData.get(key).asText().toString());

    }

	private boolean convertNompareFloatNodeValue(JsonNode expected,
			JsonNode actualData, String key) {
		DecimalFormat d = new DecimalFormat();

        return d.format((float) expected.get(key).asDouble())
                .equals(d.format((float) actualData.get(key).asDouble()));

    }

	public boolean validateHighLevel(JsonNode expectedData,
			JsonNode actualData) {
		String statusCodeKey = null;
		String statusMessageKey = null;
		boolean isStatusCodeSuccess = false;
		boolean isStatusMessageSuccess = false;
		log.info("validateHighLevel");
		log.debug("EXPECTED RESP :" + expectedData);
		log.debug("ACTUAL   RESP :" + actualData);

		if (ExtentReporting.getTest() != null) {
			ExtentReporting.getTest().log(LogStatus.INFO,
					"EXPECTED RESP :" + expectedData);
			ExtentReporting.getTest().log(LogStatus.INFO,
					"ACTUAL   RESP :" + actualData);
		}

		try {
			if (expectedData.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_BACKEND)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_BACKEND)) {

				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_BACKEND;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_BACKEND;

			} else if (expectedData
					.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_OMS)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_OMS)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_OMS;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_OMS;

			} else if (expectedData
					.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_DE)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_DE)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_DE;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_DE;

			} else if (expectedData.has(IResponseStatusAPIKeys.STATUS_CODE_KEY)
					&& expectedData
							.has(IResponseStatusAPIKeys.STATUS_MESSAGE_KEY)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY;

			} else {
				log.error(
						"unable to find the statusCode/MessageKeys in available list of constants, IResponseStatusAPIKeys");
				log.error("SO RETURN   false");
				ExtentReporting.getTest().log(LogStatus.FAIL,
						"unable to find the statusCode/MessageKeys in available list of constants, IResponseStatusAPIKeys");
				return false;
			}
			log.debug("statusCodeKey    :" + statusCodeKey);
			log.debug("statusMessageKey :" + statusMessageKey);

			if (expectedData.get(statusCodeKey).toString()
					.equals(actualData.get(statusCodeKey).toString())) {
				isStatusCodeSuccess = true;
				if (expectedData.get(statusMessageKey).toString()
						.equals(actualData.get(statusMessageKey).toString())) {
					isStatusMessageSuccess = true;
				} else {
					log.error("StatusMessageMismatch Mismatch    :");
					log.error("expectedMessage                   :"
							+ expectedData.get(statusMessageKey).toString());
					log.error("actual  Message                   :"
							+ actualData.get(statusMessageKey).toString());

					ExtentReporting.getTest().log(LogStatus.FAIL,
							"Status Message Mismatch    :" + "<br />"
									+ "expectedMessage                   :"
									+ expectedData.get(statusMessageKey)
											.toString()
									+ "<br />"
									+ "actual  Message                   :"
									+ actualData.get(statusMessageKey)
											.toString()
									+ "<br />");

					isStatusMessageSuccess = false;
				}
			} else {
				System.out.println("StatusCodeMismatch");
				log.error("expectedCode                   :"
						+ expectedData.get(statusCodeKey).toString());
				log.error("actual  Code                   :"
						+ actualData.get(statusCodeKey).toString());

				ExtentReporting.getTest().log(LogStatus.FAIL,
						"StatusCodeMismatch" + "<br />"
								+ "expectedCode                   :"
								+ expectedData.get(statusCodeKey).toString()
								+ "<br />" + "actual  Code                   :"
								+ actualData.get(statusCodeKey).toString()
								+ "<br />");
				isStatusCodeSuccess = false;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

        return isStatusCodeSuccess && isStatusMessageSuccess;
    }

	public static JsonNode getFirstObjectOfJSONArray(JsonNode node, Integer id)
			throws SwiggyAPIAutomationException {
		System.out.println("Node :" + node);
		if (0 == node.size()) {
			log.error(new SwiggyAPIAutomationException("Object Is Empty"));
			throw new SwiggyAPIAutomationException("Object Is Empty");
		}
		int size = node.size();

		for (int i = 0; i < size; i++) {
			if (node.get(i).get("id").asInt() == id) {
				System.out.println(node.get(i));
				return node.get(i);

			}else
				return null;
		}

		log.error("Object Is Empty");
		throw new SwiggyAPIAutomationException("Object Is Empty");
	}

}
