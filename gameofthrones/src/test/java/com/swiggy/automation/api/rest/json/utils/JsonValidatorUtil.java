/*
 * 
 */
package com.swiggy.automation.api.rest.json.utils;

import com.relevantcodes.extentreports.LogStatus;
import com.swiggy.automation.reporting.ExtentReporting;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

/**
 * 
 * @author mohammedramzi
 *
 */
public class JsonValidatorUtil {

	public static final Logger log = Logger.getLogger(JsonValidatorUtil.class);

	/**
	 * 
	 * @param expectedData
	 * @param actualData
	 * @return
	 */
	public static boolean validateJsonDetailedLevel(JsonNode expectedData,
			JsonNode actualData) {
		boolean isSuccess = false;
		try {
			if (null == expectedData || null == actualData) {
				return false;
			}
			isSuccess = JsonHandler.getJsonHandlerObject()
					.handleObject(expectedData, actualData);
			logItOnFailure(isSuccess, expectedData, actualData);
		} catch (Exception e) {
			log.error(
					"Hey validateJsonDetailedLevel() Failed : Exception Details"
							+ e.getMessage() + "::::: " + e);
			e.printStackTrace();
		}
		return isSuccess;
	}

	/**
	 * 
	 * @param expectedData
	 * @param actualData
	 * @param apiFieldData
	 * @return
	 */
	public static boolean validateJsonWithExpectedAPIFieldsDetailedLevel(
			JsonNode expectedData, JsonNode actualData, JsonNode apiFieldData) {
		boolean isSuccess = false;
		try {
			if (null == expectedData || null == actualData
					|| null == apiFieldData) {
				log.error(
						"validateJsonWithExpectedAPIFieldsDetailedLevel()>>expected||actual||apifieldData null...so return false");
				ExtentReporting.getTest().log(LogStatus.FAIL,
						"validateJsonWithExpectedAPIFieldsDetailedLevel()>>expected||actual||apifieldData null...so return false");
				return false;
			}

			isSuccess = JsonHandler.getJsonHandlerObject()
					.handleObject(expectedData, actualData, apiFieldData);
			logItOnFailure(isSuccess, expectedData, actualData, apiFieldData);

		} catch (Exception e) {
			log.error(
					"Hey validateJsonWithExpectedAPIFieldsDetailedLevel() Failed : Exception Details"
							+ e.getMessage() + "::::: " + e);
			ExtentReporting.getTest().log(LogStatus.FAIL,
					"Hey validateJsonWithExpectedAPIFieldsDetailedLevel() Failed : Exception Details"
							+ e.getMessage() + "::::: " + e);
			e.printStackTrace();
		}
		return isSuccess;

	}

	/**
	 * 
	 * @param expectedData
	 * @param actualData
	 * @return
	 */
	public static boolean validateJsonHighLevel(JsonNode expectedData,
			JsonNode actualData) {
		String statusCodeKey = null;
		String statusMessageKey = null;
		boolean isStatusCodeSuccess = false;
		boolean isStatusMessageSuccess = false;

		try {
			if (expectedData.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_BACKEND)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_BACKEND)) {

				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_BACKEND;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_BACKEND;
				log.debug("statusKeys found in expected field :" + statusCodeKey
						+ "," + statusMessageKey);

			} else if (expectedData
					.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_OMS)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_OMS)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_OMS;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_OMS;
				log.debug("statusKeys found in expected field :" + statusCodeKey
						+ "," + statusMessageKey);

			} else if (expectedData
					.has(IResponseStatusAPIKeys.STATUS_CODE_KEY_DE)
					&& expectedData.has(
							IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_DE)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY_DE;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY_DE;
				log.debug("statusKeys found in expected field :" + statusCodeKey
						+ "," + statusMessageKey);

			} else if (expectedData.has(IResponseStatusAPIKeys.STATUS_CODE_KEY)
					&& expectedData
							.has(IResponseStatusAPIKeys.STATUS_MESSAGE_KEY)) {
				statusCodeKey = IResponseStatusAPIKeys.STATUS_CODE_KEY;
				statusMessageKey = IResponseStatusAPIKeys.STATUS_MESSAGE_KEY;
				log.debug("statusKeys found in expected field :" + statusCodeKey
						+ "," + statusMessageKey);

			} else {
				log.error(
						"unable to find the statusKeys provided in IResponseStatusAPIKeys.....returning false");
				ExtentReporting.getTest().log(LogStatus.FAIL,
						"unable to find the statusKeys provided in IResponseStatusAPIKeys.....returning false");
				return false;
			}

			if (expectedData.get(statusCodeKey).toString()
					.equals(actualData.get(statusCodeKey).toString())) {
				isStatusCodeSuccess = true;
				if (expectedData.get(statusMessageKey).toString()
						.equals(actualData.get(statusMessageKey).toString())) {
					isStatusMessageSuccess = true;
					// return true;
				} else {

					log.error(
							"Status Message Mismatch..so return false >> expected:"
									+ expectedData.get(statusMessageKey)
											.toString()
									+ "actual:" + actualData
											.get(statusMessageKey).toString());

					ExtentReporting.getTest().log(LogStatus.FAIL,
							"Status Message Mismatch..so return false >> expected:"
									+ "<br />"
									+ expectedData.get(statusMessageKey)
											.toString()
									+ "<br />"
									+ "actual:" + actualData
											.get(statusMessageKey).toString()
									+ "<br />");
					System.out.println("StatusMessageMismatch");
					isStatusMessageSuccess = false;
					// return false;
				}
			} else {
				log.error("Status Code Mismatch..so return false >> expected:"
						+ expectedData.get(statusCodeKey).toString() + "actual:"
						+ actualData.get(statusCodeKey).toString());
				System.out.println("StatusCodeMismatch");
				isStatusCodeSuccess = false;
				// return false;
			}

		} catch (Exception e) {
			log.error("Hey validateJsonHighLevel() Failed : Exception Details"
					+ e.getMessage() + "::::: " + e);
			e.printStackTrace();
		}

		if (!isStatusCodeSuccess || !isStatusMessageSuccess) {
			logItOnFailure(false, expectedData, actualData);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @param expectedData
	 * @param actualData
	 * @param fields
	 * @return
	 */
	public static boolean validateJsonHighLevel(JsonNode expectedData,
			JsonNode actualData, String[] fields) {

		try {

			for (String fieldName : fields) {
				if (expectedData.get(fieldName).toString()
						.equals(actualData.get(fieldName).toString())) {
					continue;
				} else {
					log.error("Status  Mismatch..so return false >> expected:"
							+ expectedData.get(fieldName).toString() + "actual:"
							+ actualData.get(fieldName).toString());
					ExtentReporting.getTest().log(LogStatus.FAIL,
							"Status  Mismatch..so return false >> expected:"
									+ expectedData.get(fieldName).toString()
									+ "<br />" + "actual:"
									+ actualData.get(fieldName).toString());
					logItOnFailure(false, expectedData, actualData);
					return false;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			log.error("Hey validateJsonHighLevel() Failed : Exception Details"
					+ e.getMessage() + "::::: " + e);
			ExtentReporting.getTest().log(LogStatus.FAIL,
					"Hey validateJsonHighLevel() Failed : Exception Details"
							+ e.getMessage() + "::::: " + e);
		}

		return true;
	}

	public static void logItOnFailure(boolean isSuccess, JsonNode expectedData,
			JsonNode actualData) {
		if (!isSuccess) {
			log.error("ExpectedData     :" + expectedData);
			log.error("ActualResponse   :" + actualData);

			// ExtentReporting.getTest().log(LogStatus.FAIL,
			// "ExpectedData :" + expectedData);
			// ExtentReporting.getTest().log(LogStatus.FAIL,
			// "ActualResponse :" + actualData);
		}
	}

	public static void logItOnFailure(boolean isSuccess, JsonNode expectedData,
			JsonNode actualData, JsonNode expectedAPIFields) {
		if (!isSuccess) {
			log.error("expectedAPIFields:" + expectedAPIFields);
			log.error("ExpectedData     :" + expectedData);
			log.error("ActualResponse   :" + actualData);
			//
			// ExtentReporting.getTest().log(LogStatus.FAIL,
			// "expectedAPIFields :" + expectedAPIFields.asText());
			// ExtentReporting.getTest().log(LogStatus.FAIL,
			// "ExpectedData :" + expectedData.asText());
			// ExtentReporting.getTest().log(LogStatus.FAIL,
			// "ActualResponse :" + actualData.asText());
		}
	}

}
