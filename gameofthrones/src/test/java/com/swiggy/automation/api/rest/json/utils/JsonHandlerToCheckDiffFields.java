package com.swiggy.automation.api.rest.json.utils;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * 
 * @author mohammedramzi
 *
 */
public class JsonHandlerToCheckDiffFields {

	final static Logger log = Logger
			.getLogger(JsonHandlerToCheckDiffFields.class);

	ArrayList<String> pathFinderList = new ArrayList<String>();

	public static boolean handleObject(JsonNode expected, JsonNode actual) {
		boolean isArrayCheckPass = false, isFieldCheckPass = false;
		log.info("handleObject>>");
		log.info(expected);
		log.info(actual);
		Entry<String, JsonNode> field = null;

		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = expected
					.getFields();

			while (fieldsIterator.hasNext()) {

				field = fieldsIterator.next();
				System.out
						.println("===============================================");
				if (field.getKey().equals("sla_time")) {

					log.info("Key: " + field.getKey() + "\tValue:"
							+ field.getValue());
				}

				log.info("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());

				if (field.getKey().startsWith("IGNORE_")) {
					log.info("IGNORE_  >>>>!!!!!!!!");
					log.info("ignoreKey" + field.getKey());
					continue;
				}
				if (field.getKey().startsWith("MINCHECK")) {

					if (field.getKey().startsWith("MINCHECKSIZE_")) {
						log.info("MINCHECKSIZE_  >>>>!!!!!!!!");

						log.info("Key: " + field.getKey() + "\tValue:"
								+ field.getValue());

						String key = field.getKey().split("MINCHECKSIZE_")[1];

						log.info(key);
						log.info(field.getValue().asText().length());
						log.info(actual);
						log.info(actual.get(key).asText().length());
						if (!compareNodeValueSize(field, actual, key)) {

							log.error("MINCHECKTYPE FAILED FOR NODE VALUE SIZE>>>Key:"
									+ field.getKey()
									+ "***Expected: "
									+ field.getValue()
									+ "**** Actual "
									+ actual.get(key));
						}

					} else if (field.getKey().startsWith("MINCHECKTYPE_")) {
						log.info("MINCHECKTYPE_  >>>>!!!!!!!!");
						if (!compareNodeValueType(field, actual, field.getKey()
								.split("MINCHECKTYPE_")[1])) {

							log.error("MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key:"
									+ field.getKey()
									+ "***Expected: "
									+ field
									+ "**** Actual "
									+ actual.get(field.getKey().split(
											"MINCHECKTYPE_")[1]));
						}

					} else if (field.getKey().startsWith("MINCHECKFIELD_")) {

						log.info("MINCHECKFIELD_  >>>>!!!!!!!!");
						log.info("MINCHECKFIELD_Key" + field.getKey());
						if (!actual
								.has(field.getKey().split("MINCHECKFIELD_")[1])) {
							log.error("MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key:"
									+ field.getKey()
									+ "***Expected: "
									+ field
									+ "**** Actual "
									+ actual.get(field.getKey().split(
											"MINCHECKFIELD_")[1]));
						}
					}

					continue;
				}

				if (field.getValue().isObject() && !field.getValue().isNull()) {

					// log.debug("AAAAAAAAAAAAAAAAAAAAAAAAAAA");

					handleObject(field.getValue(), actual.get(field.getKey()));
					// if (!isObjectCheckPass) {
					// log.info("Object Check Failed!!!!!");
					// log.error("Object Check Failed!!!!!");
					// log.error("Expected :" + field.getValue());
					// log.error("Actual   :" + actual.get(field.getKey()));
					//
					// return false;
					// }
				}

				else if (field.getValue().isArray()) {
					log.info("isArray  >>>>!!!!!!!!");
					log.info("isArray_Key" + field.getKey());
					log.debug(field.getValue()
							+ " is  Array List of Json Objects");
					if (field.getValue().size() != actual.get(field.getKey())
							.size()) {
						log.warn("--------------------------------------------------");
						log.warn("MISMATCH in Value size for arrayElement having key:"
								+ field.getKey());
						log.warn("MISMATCH :expected Array elements :"
								+ field.getValue().size()
								+ " But,Actutal ArrayElements "
								+ actual.get(field.getKey()));

						log.warn("Expected:" + field.getValue());
						log.warn("Actual  :" + actual.get(field.getKey()));
						log.warn("--------------------------------------------------");
					}

					if (field.getValue().size() < actual.get(field.getKey())
							.size()) {
						log.warn("More Objects present in Actual Data");

					}

					isArrayCheckPass = handleNCompareArrayOfObjects(
							field.getValue(), actual.get(field.getKey()));

					if (!isArrayCheckPass) {
						log.error("Array Check Failed!!!!!");
						log.error("Expected :" + field.getValue());
						log.error("Actual   :" + actual.get(field.getKey()));
						// return false;
					}

				}

				else if (field.getValue().isValueNode()) {
					isFieldCheckPass = handleNCompareValueNode(field, actual);

					// if (!isFieldCheckPass) {
					// log.info("field Check Failed!!!!!");
					// log.info("Key :" + field.getKey());
					// log.info("Expected :" + field.getValue());
					// log.info("Actual   :" + actual.get(field.getKey()));
					//
					// log.error("Field Check Check Failed!!!!!");
					// log.error("Expected :" + field.getValue());
					// log.error("Actual   :" + actual.get(field.getKey()));
					//
					// // return false;
					// }

				} else {
					System.out
							.println("JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK");
					log.info(field.getValue());
					log.info("JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
							+ field.getValue());
					return false;
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> handleObject()   ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
			// return false;
		}
		return true;

	}

	public boolean handleObject(JsonNode expected, JsonNode actual,
			JsonNode apiExpectedFieldsData) {
		boolean isObjectCheckPass = false, isArrayCheckPass = false, isFieldCheckPass = false;
		log.info("handleObject>>");
		log.info("API FIELD:" + apiExpectedFieldsData);
		log.info("EXPECTED :" + expected);
		log.info("ACTUAL   :" + actual);
		Entry<String, JsonNode> apiExpectedField = null;

		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = apiExpectedFieldsData
					.getFields();
			while (fieldsIterator.hasNext()) {

				apiExpectedField = fieldsIterator.next();
				log.debug("Key: " + apiExpectedField.getKey() + "\tValue:"
						+ apiExpectedField.getValue());
				System.out
						.println("===============================================");
				log.info("Key: " + apiExpectedField.getKey() + "\tValue:"
						+ apiExpectedField.getValue());

				if (apiExpectedField.getKey().startsWith("IGNORE_")) {
					continue;
				}

				if (apiExpectedField.getKey().startsWith("CONVERTNCHECK_")) {
					String key = null;
					key = apiExpectedField.getKey().split("CONVERTNCHECK_")[1];
					log.info("Key: " + apiExpectedField.getKey() + "\tValue:"
							+ apiExpectedField.getValue());

					if (!convertNompareNodeValue(expected, actual, key)) {
						log.error("CONVERTNCHECK_ FAILED FOR NODE VALUE SIZE>>>KEY:"
								+ apiExpectedField.getKey()
								+ "***EXPECTED VALUE: "
								+ expected.get(key)
								+ "**** ACTUAL VALUE: " + actual.get(key));
						// return false;
					}

					continue;

				}
				if (apiExpectedField.getKey().startsWith("MINCHECK")) {
					String key = null;
					if (apiExpectedField.getKey().startsWith("MINCHECKSIZE_")) {
						key = apiExpectedField.getKey().split("MINCHECKSIZE_")[1];
						log.info("Key: " + apiExpectedField.getKey()
								+ "\tValue:" + apiExpectedField.getValue());

						// String key =
						// field.getKey().split("MINCHECKSIZE_")[1];
						if (!compareNodeValueSize(expected, actual, key)) {
							log.error("MINCHECKTYPE FAILED FOR NODE VALUE SIZE>>>Key:"
									+ apiExpectedField.getKey()
									+ "***EXPECTED VALUE: "
									+ expected.get(key)
									+ "**** ACTUAL VALUE: " + actual.get(key));
							// return false;
						}

					} else if (apiExpectedField.getKey().startsWith(
							"MINCHECKTYPE_")) {
						key = apiExpectedField.getKey().split("MINCHECKTYPE_")[1];
						if (!compareNodeValueType(expected, actual,
								apiExpectedField.getKey()
										.split("MINCHECKTYPE_")[1])) {
							log.error("MINCHECKTYPE FAILED FOR NODE VALUE TYPE>>>Key:"
									+ apiExpectedField.getKey()
									+ "***EXPECTED VALUE: "
									+ expected.get(key)
									+ "**** ACTUAL VALUE: " + actual.get(key));
							// return false;
						}

					} else if (apiExpectedField.getKey().startsWith(
							"MINCHECKFIELD_")) {
						key = apiExpectedField.getKey().split("MINCHECKFIELD_")[1];
						if (!actual.has(key)) {
							log.error("MINCHECKFIELD FAILED FOR NODE VALUE TYPE>>>Key not found in actual: key missing is:"
									+ apiExpectedField.getKey()
									+ "***Expected: " + expected.get(key));
							// return false;
						}
					}

					continue;
				}

				if (apiExpectedField.getValue().isObject()
						&& !apiExpectedField.getValue().isNull()) {

					// log.debug("AAAAAAAAAAAAAAAAAAAAAAAAAAA");

					isObjectCheckPass = handleObject(
							expected.get(apiExpectedField.getKey()),
							actual.get(apiExpectedField.getKey()),
							apiExpectedField.getValue());
					if (!isObjectCheckPass) {
						log.info("Object Check Failed!!!!!");
						log.error("Object Check Failed!!!!!");
						log.error("apiField :" + apiExpectedField.getValue());
						log.error("Expected   :"
								+ actual.get(apiExpectedField.getKey()));
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));
						log.info("API   :" + apiExpectedField.getValue());
						log.info("Expected   :"
								+ actual.get(apiExpectedField.getKey()));
						log.info("Actual   :"
								+ actual.get(apiExpectedField.getKey()));

						// return false;
					}
				}

				else if (apiExpectedField.getValue().isArray()) {
					log.debug(apiExpectedField.getValue()
							+ " is  Array List of Json Objects");
					if (expected.get(apiExpectedField.getKey()).size() != actual
							.get(apiExpectedField.getKey()).size()) {
						log.warn("--------------------------------------------------");
						log.warn("MISMATCH in Value size for arrayElement having key:"
								+ apiExpectedField.getKey());
						log.warn("MISMATCH :expected Array elements :"
								+ expected.get(apiExpectedField.getKey())
										.size() + " But,Actutal ArrayElements "
								+ actual.get(apiExpectedField.getKey()).size());

						log.warn("Field:" + apiExpectedField.getValue());
						log.warn("Expected:"
								+ expected.get(apiExpectedField.getKey()));
						log.warn("Actual  :"
								+ actual.get(apiExpectedField.getKey()));
						log.warn("--------------------------------------------------");
					}

					if (expected.get(apiExpectedField.getKey()).size() < actual
							.get(apiExpectedField.getKey()).size()) {
						log.warn("More Objects present in Actual Data");

					}

					isArrayCheckPass = handleNCompareArrayOfObjects(

					expected.get(apiExpectedField.getKey()),
							actual.get(apiExpectedField.getKey()),
							apiExpectedField.getValue());

					if (!isArrayCheckPass) {
						log.error("Array Check Failed!!!!!");
						log.error("Expected :" + apiExpectedField.getValue());
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));
						// return false;
					}

				}

				else if (apiExpectedField.getValue().isValueNode()) {
					isFieldCheckPass = handleNCompareValueNode(expected,
							actual, apiExpectedField.getKey());

					if (!isFieldCheckPass) {
						log.error("Field Check Check Failed!!!!!");
						log.error("Expected :"
								+ expected.get(apiExpectedField.getKey()));
						log.error("Actual   :"
								+ actual.get(apiExpectedField.getKey()));

						// return false;
					}

				} else {
					System.out
							.println("JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK");
					log.info(apiExpectedField.getValue());
					log.info("JSON DATA NO HANDLED FOR THIS TYPE IN THE CODE...PLEASE CHECK :"
							+ apiExpectedField.getValue());
					return false;
				}

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> handleObject()   ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
			return false;
		}
		return true;

	}

	public static boolean handleNCompareValueNode(
			Entry<String, JsonNode> field, JsonNode actualData) {

		if (false == compareValueNode(field, actualData)) {

			log.error("Exected :" + field);
			log.error("Actual  :" + actualData);
			log.error("Data Mismatch");

			// return false;
		}

		// // validate with different dataType for accuracy
		log.info("Expected   Key:" + field.getKey());
		log.info("Expected Value:" + field.getValue().toString());
		log.info("Actual   Value:" + actualData.get(field.getKey()));
		// log.info(actualData.get(field.getKey()).asText().toString());

		if (!field.getKey().startsWith("IGNORE_")
				|| !field.getKey().startsWith("MINCHECK_")) {
			if (!actualData.has((field.getKey()))) {
				log.fatal("FIELD MISSING IN ACTUAL DATA :" + field.getKey());
				log.fatal("Expected Value Was :" + field.getValue());
			} else {
				// log.ignores the key
				if (field
						.getValue()
						.asText()
						.toString()
						.equals(actualData.get(field.getKey()).asText()
								.toString())) {
					log.info("DATA MATCH");
					return true;
				}
			}

		}

		if (field.getKey().startsWith("MINCHECK_")) {
			// log.ignores the key
			log.info("IGNORES THE COMPARISON FOR " + field.getKey());
			return true;

		}

		System.out
				.println("handleNCompareValueNode()>>>>expected and actual field Data mismatch:"
						+ field
						+ " <===//===>  "
						+ actualData.get(field.getKey()));
		log.error("Comparison Failed for Value Node");
		log.error("Exected :" + field);
		log.error("Actual  :" + actualData.get(field.getKey()));
		// return false;
		return true;

	}

	public boolean handleNCompareValueNode(JsonNode expected,
			JsonNode actualData, String key) {

		if (false == compareValueNode(expected, actualData, key)) {
			log.error("Comparison Failed for Value Node");
			log.error("Key :" + key);
			log.error("Exected :" + expected.get(key));
			log.error("Actual  :" + actualData.get(key));

			// return false;
		}

		return true;

	}

	private static boolean compareValueNode(Entry<String, JsonNode> field,
			JsonNode actualData) {

		// log.info("------------------------compareValueNode--------------");
		// log.info("Expected   Key:" + field.getKey());
		// log.info("Expected Value:" + field.getValue().toString());
		// log.info("Actual   Value:" + actualData.get(field.getKey()));
		// log.info(actualData.get(field.getKey()).asText().toString());

		if (actualData.get(field.getKey()) == null) {
			log.error(field.getKey()
					+ "   Element Not Present in Actual Data Response");
			return true;
		}

		if (field.getValue().isNull() || field.getValue().isTextual()) {

			log.info("null  or texual field comparison");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());
			if (field.getValue().asText().toString()
					.equals(actualData.get(field.getKey()).asText().toString())) {

				log.info("Data Match");
				return true;
			}

		}

		if (field.getValue().isBoolean()) {
			log.info("boolean");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText()
			// .toString());
			if (field.getValue().asBoolean() == (actualData.get(field.getKey())
					.asBoolean())) {
				log.info("DATA MATCH");
				return true;
			}

		}

		if (field.getValue().isFloatingPointNumber()

		|| field.getValue().isDouble()) {

			log.info("double or float");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText()
			// .toString());
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				log.info("DATA MATCH");
				return true;
			}
		}

		if (field.getValue().isInt()) {
			if (!actualData.get(field.getKey()).isInt()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("int type");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText()
			// .toString());

			if (field.getValue().asInt() == (actualData.get(field.getKey())
					.asInt())) {
				log.info("DATA MATCH");
				return true;
			}
		}

		if (field.getValue().isLong()) {
			if (!actualData.get(field.getKey()).isLong()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("long type");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());

			if (field.getValue().asLong() == (actualData.get(field.getKey())
					.asLong())) {
				log.info("DATA MATCH");
				return true;
			}

			return true;
		}

		if (field.getValue().isBigInteger()) {
			if (!actualData.get(field.getKey()).isBigInteger()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("isBigInteger type");
			//
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());
			if (field.getValue().asLong() == (actualData.get(field.getKey())
					.asLong())) {
				log.info("DATA MATCH");
				return true;
			}

		}
		if (field.getValue().isBinary()) {
			if (!actualData.get(field.getKey()).isBigInteger()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("isBinary type");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());
			return true;
		}

		if (field.getValue().isDouble()) {
			if (!actualData.get(field.getKey()).isDouble()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("isDouble type");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				log.info("DATA MATCH");
				return true;
			}
		}
		if (field.getValue().isFloatingPointNumber()) {
			if (!actualData.get(field.getKey()).isFloatingPointNumber()) {
				log.error("DATATYPE MISMATCH FOUND..But proceed with conversion wrt expectted field type");
			}
			log.info("isFloatingPointNumber type");
			// log.info(field.getKey());
			// log.info(field.getValue().toString());
			// log.info(actualData);
			// log.info(actualData.get(field.getKey()).asText().toString());
			if (field.getValue().asDouble() == (actualData.get(field.getKey())
					.asDouble())) {
				log.info("DATA MATCH");
				return true;
			}
		}

		// if (field.getValue().isNumber()) {
		// log.info("isNumber");
		// log.info(field.getKey());
		// log.info(field.getValue().toString());
		// log.info(actualData);
		// log.info(actualData.get(field.getKey()).asText()
		// .toString());
		// if (field.getValue().asLong() == (actualData.get(field.getKey())
		// .asLong())) {
		// log.info("TRUEEEEEE");
		// return true;
		// }
		// }
		return true;

	}

	private boolean compareValueNode(JsonNode expectedData,
			JsonNode actualData, String key) {

		log.info("compareValueNode--------------");
		log.info("key :" + key);
		log.info("Expected Field :" + expectedData.get(key));
		log.info("Actual   Field :" + actualData.get(key));

		if (actualData.get(key) == null) {
			System.out
					.println("Element Not Present in Actual Data :>>>>>>>>>>>   "
							+ key);

			log.error(key + "   Element Not Present in Actual Data Response");

			return true;
		}

		if ((expectedData.get(key).isNull() && actualData.get(key).isNull())
				|| expectedData.get(key).isTextual()
				&& actualData.get(key).isTextual()) {

			log.info("null  textutal");

			if (expectedData.get(key).asText().toString()
					.equals(actualData.get(key).asText().toString())) {
				log.info("TRUEEEEEE");
				return true;
			}

		}

		if (expectedData.get(key).isBoolean()
				&& actualData.get(key).isBoolean()) {

			log.info("boolean");
			if (expectedData.get(key).asBoolean() == (actualData.get(key)
					.asBoolean())) {
				log.info("TRUEEEEEE");
				return true;
			}

		}

		if ((expectedData.get(key).isFloatingPointNumber() && actualData.get(
				key).isFloatingPointNumber())

				|| (expectedData.get(key).isDouble() && actualData.get(key)
						.isDouble())) {

			log.info("double or float");

			if (expectedData.get(key).asDouble() == (actualData.get(key)
					.asDouble())) {
				log.info("TRUEEEEEE");
				return true;
			}
		}

		if (expectedData.get(key).isInt() && actualData.get(key).isInt()) {

			log.info("int");

			if (expectedData.get(key).asInt() == (actualData.get(key).asInt())) {
				log.info("TRUEEEEEE");
				return true;
			}
		}

		if (expectedData.get(key).isLong() && actualData.get(key).isLong()) {

			log.info("long");

			if (expectedData.get(key).asLong() == (actualData.get(key).asLong())) {
				log.info("TRUEEEEEE");
				return true;
			}

			return false;
		}

		if (expectedData.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			log.info("isBigInteger");

			if (expectedData.get(key).asLong() == (actualData.get(key).asLong())) {
				log.info("TRUEEEEEE");
				return true;
			}

		}
		if (expectedData.get(key).isBinary() && actualData.get(key).isBinary()) {

			return true;
		}

		if (expectedData.get(key).isDouble() && actualData.get(key).isDouble()) {
			log.info("isDouble");

			if (expectedData.get(key).asDouble() == (actualData.get(key)
					.asDouble())) {
				log.info("TRUEEEEEE");
				return true;
			}
		}
		if (expectedData.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()) {
			log.info("isFloatingPointNumber");

			if (expectedData.get(key).asDouble() == (actualData.get(key)
					.asDouble())) {
				log.info("TRUEEEEEE");
				return true;
			}
		}

		// if (expectedData.get(key).isNumber() &&
		// actualData.get(key).isNumber()) {
		// log.info("isNumber");
		//
		// if (expectedData.get(key).asLong() == (actualData.get(key).asLong()))
		// {
		// log.info("TRUEEEEEE");
		// return true;
		// }
		// }
		return true;

	}

	private static boolean compareNodeValueType(Entry<String, JsonNode> field,
			JsonNode actualData, String key) {

		log.info("compareNodeValueType--------------");
		log.info("Key      :" + key);
		log.info("expected :" + field.getValue().toString());
		log.info("actual   :"
				+ actualData.get(field.getKey()).asText().toString());

		if (field.getValue().isTextual() && actualData.get(key).isTextual()) {
			return true;
		}

		if (field.getValue().isBoolean() && actualData.get(key).isBoolean()) {
			return true;
		}

		if (field.getValue().isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| field.getValue().isDouble()
				&& actualData.get(key).isDouble()) {
			return true;
		}

		if (field.getValue().isInt() && actualData.get(key).isInt()) {
			return true;
		}

		if (field.getValue().isLong() && actualData.get(key).isLong()) {
			return true;
		}

		if (field.getValue().isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			return true;
		}

		if (field.getValue().isBinary() && actualData.get(key).isBinary()) {
			return true;
		}

		if (field.getValue().isDouble() && actualData.get(key).isDouble()) {
			return true;
		}

		if (field.getValue().isNumber() && actualData.get(key).isNumber()) {
			return true;
		}

        return field.getValue().isNull() && actualData.get(key).isNull()
                || field.getValue().isNull() && actualData.get(key).equals("");

    }

	private boolean compareNodeValueType(JsonNode expectedData,
			JsonNode actualData, String key) {

		log.info("compareNodeValueType--------------");
		log.info("KEY :" + key);
		log.info("expected:" + expectedData.get(key));
		log.info("actual  :" + actualData.get(key));
		// log.info(actualData.get(field.getKey()).asText().toString());

		if (expectedData.get(key).isTextual()
				&& actualData.get(key).isTextual()) {
			return true;
		}

		if (expectedData.get(key).isBoolean()
				&& actualData.get(key).isBoolean()) {
			return true;
		}

		log.info(expectedData.get(key).isFloatingPointNumber());
		log.info(actualData.get(key).isFloatingPointNumber());

		log.info(expectedData.get(key).isDouble());

		log.info(actualData.get(key).isDouble());

		if (expectedData.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| expectedData.get(key).isDouble()
				&& actualData.get(key).isDouble()) {
			return true;
		}

		if (expectedData.get(key).isInt() && actualData.get(key).isInt()) {
			return true;
		}

		if (expectedData.get(key).isLong() && actualData.get(key).isLong()) {
			return true;
		}

		if (expectedData.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			return true;
		}

		if (expectedData.get(key).isBinary() && actualData.get(key).isBinary()) {
			return true;
		}

		if (expectedData.get(key).isDouble() && actualData.get(key).isDouble()) {
			return true;
		}

		if (expectedData.get(key).isNull() && actualData.get(key).isNull()
				|| expectedData.get(key).isNull()
				&& actualData.get(key).equals("")) {
			return true;
		}

		return true;

	}

	private static boolean compareNodeValueSize(Entry<String, JsonNode> field,
			JsonNode actualData, String key) {

		log.info("compareNodeValueSize--------------");
		log.info("expected value:" + field.getValue().toString());
		log.info("expected value:" + actualData.get(key));
		// log.info(actualData.get(field.getKey()).asText().toString());

		if (field.getValue().isTextual() && actualData.get(key).isTextual()) {

			log.info(field.getValue().asText().length());
			log.info(actualData);
			log.info(actualData.get(key).asText().length());
			if (field.getValue().asText().length() == actualData.get(key)
					.asText().length()) {
				return true;
			}
		}

		if (field.getValue().isBoolean() && actualData.get(key).isBoolean()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| field.getValue().isDouble()
				&& actualData.get(key).isDouble()) {

			if ((float) Math.log10(field.getValue().asDouble()) + 1 == (float) Math
					.log10(actualData.get(key).asDouble()) + 1) {
				return true;
			}

		}

		if (field.getValue().isInt() && actualData.get(key).isInt()) {
			if ((int) Math.log10(field.getValue().asInt()) + 1 == (int) Math
					.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (field.getValue().isLong() && actualData.get(key).isLong()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			if ((long) Math.log10(field.getValue().asInt()) + 1 == (long) Math
					.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (field.getValue().isBinary() && actualData.get(key).isBinary()) {
			if (field.getValue().size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (field.getValue().isDouble() && actualData.get(key).isDouble()) {
			if (Math.log10(field.getValue().asDouble()) + 1 == Math
					.log10(actualData.get(key).asDouble()) + 1) {
				return true;
			}
		}

		if (field.getValue().isNumber() && actualData.get(key).isNumber()) {
			if ((long) Math.log10(field.getValue().asLong()) + 1 == (long) Math
					.log10(actualData.get(key).asLong()) + 1) {
				return true;
			}
		}

		return true;

	}

	private boolean compareNodeValueSize(JsonNode expected,
			JsonNode actualData, String key) {

		log.info("compareValueNode--------------");
		log.info(expected);

		log.info(actualData);
		// log.info(actualData.get(field.getKey()).asText().toString());

		if (expected.get(key).isTextual() && actualData.get(key).isTextual()) {

			log.info("Expected  :" + expected.get(key));
			log.info("Expected Length :" + expected.get(key).asText().length());
			log.info("Actual        :" + actualData.get(key));
			log.info("Actual length :" + actualData.get(key).asText().length());
			if (expected.get(key).asText().length() == actualData.get(key)
					.asText().length()) {
				return true;
			}
		}

		if (expected.get(key).isBoolean() && actualData.get(key).isBoolean()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isFloatingPointNumber()
				&& actualData.get(key).isFloatingPointNumber()
				|| expected.get(key).isDouble()
				&& actualData.get(key).isDouble()) {

			if ((float) Math.log10(expected.get(key).asDouble()) + 1 == (float) Math
					.log10(actualData.get(key).asDouble()) + 1) {
				return true;
			}

		}

		if (expected.get(key).isInt() && actualData.get(key).isInt()) {
			if ((int) Math.log10(expected.get(key).asInt()) + 1 == (int) Math
					.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (expected.get(key).isLong() && actualData.get(key).isLong()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isBigInteger()
				&& actualData.get(key).isBigInteger()) {
			if ((long) Math.log10(expected.get(key).asInt()) + 1 == (long) Math
					.log10(actualData.get(key).asInt()) + 1) {
				return true;
			}
		}

		if (expected.get(key).isBinary() && actualData.get(key).isBinary()) {
			if (expected.get(key).size() == actualData.get(key).size()) {
				return true;
			}
		}

		if (expected.get(key).isDouble() && actualData.get(key).isDouble()) {
			if (Math.log10(expected.get(key).asDouble()) + 1 == Math
					.log10(actualData.get(key).asDouble()) + 1) {
				return true;
			}
		}

		if (expected.get(key).isNumber() && actualData.get(key).isNumber()) {
			if ((long) Math.log10(expected.get(key).asLong()) + 1 == (long) Math
					.log10(actualData.get(key).asLong()) + 1) {
				return true;
			}
		}

		return true;

	}

	private boolean convertNompareNodeValue(JsonNode expected,
			JsonNode actualData, String key) {

		log.info("convertNompareNodeValue--------------");
		log.info("KEY       :" + key);
		log.info("Expected  :" + expected.get(key).asText());
		log.info("Actual    :" + actualData.get(key).asText());
		if (expected.get(key).asText().toString()
				.equals(actualData.get(key).asText().toString())) {
			return true;
		}
		return true;

	}

	public int getNumberOfElements(JsonNode n) {
		try {
			if (n.isNull()) {
				return 0;
			}
			return n.size();
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> getNumberOfElements()   ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return -1;
	}

	public void parse(JsonNode expectedJson) throws
            IOException {
		try {

			Iterator<Entry<String, JsonNode>> fieldsIterator = expectedJson
					.getFields();

			while (fieldsIterator.hasNext()) {

				Entry<String, JsonNode> field = fieldsIterator.next();
				log.debug("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());
				log.info("Key: " + field.getKey() + "\tValue:"
						+ field.getValue());

				if (field.getValue().isObject() && !field.getValue().isNull()) {
					pathFinderList.add(field.getKey());
					log.info(pathFinderList);
					parse(field.getValue());
				}
				if (field.getValue().isArray()) {
					// HandleNCompareArrayOfObjects(field.getValue());
				}

				log.info(field.getKey());
				log.info(field.getValue());

			}

		} catch (Exception e) {
			log.error(e.getMessage() + "  >> parse()   ECZZEPTION IZ :" + e);
			e.printStackTrace();// TODO: handle exception
		}

		log.info("lets :" + pathFinderList);

	}

	public static boolean handleNCompareArrayOfObjects(JsonNode value,
			JsonNode actualData) {

		log.info("ARRAY OF OBJECTS  :handleNCompareArrayOfObjects ");
		log.info("handleNCompareArrayOfObjects  >>");
		log.info(value);
		log.info(actualData);

		if (value.size() != actualData.size()) {
			log.info("size mismatch");
			return true;
		}

		JsonNode jj;
		try {
			int i = 0;
			Iterator<JsonNode> j = value.iterator();
			while (j.hasNext()) {
				log.debug(j);
				log.info("j :" + j);
				jj = j.next();
				if (jj.isObject()) {
					handleObject(jj, actualData.get(i++)); // need

					// improve
					// here .get correct
					// object from
					// actual data list
					// of
					// objects.currently
					// i just selected
					// with index.
				} else if (jj.isTextual()) {
					log.info(jj.asText());

				} else {
					log.info(jj.toString());
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	public boolean handleNCompareArrayOfObjects(JsonNode expectedData,
			JsonNode actualData, JsonNode value) {
		log.info("ARRAY OF OBJECTS  :handleNCompareArrayOfObjects ");
		log.info(value);
		log.info("expectedData:" + expectedData);
		log.info("Expected Array Contents" + actualData);

		if (expectedData.size() != actualData.size()) {
			log.info("size mismatch");
		}

		JsonNode jj;
		try {
			int i = 0;
			Iterator<JsonNode> j = expectedData.iterator();
			while (j.hasNext()) {
				log.debug(j);
				log.info("j :" + j);
				jj = j.next();
				if (jj.isObject()) {
					// actualObject = getMeObjectMatchToexpectedObjectId(jj,
					// actualData);
					handleObject(jj, actualData.get(i), value.get(i)); // need

					// improve
					// here .get correct
					// object from
					// actual data list
					// of
					// objects.currently
					// i just selected
					// with index.
				} else if (jj.isTextual()) {
					log.info(jj.asText());
					compareValueNode((Entry<String, JsonNode>) jj, actualData);

				} else {
					log.info(jj.toString());
				}

				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ "  >> handleNCompareArrayOfObjects()   ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return true;
	}

}
