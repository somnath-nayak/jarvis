package com.swiggy.automation.api.rest;

import com.swiggy.automation.utils.APITestBase;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * 
 * @author mohammedramzi
 *
 */
// @Listeners(ExtentReporting.class)
public abstract class RestTestHelper extends APITestBase {
	public static final Logger log = Logger.getLogger(RestTestHelper.class);
	public static LinkedHashSet<Long> orderIds;
	static {
		orderIds = new LinkedHashSet<Long>();
	}
	// below tid and token will be populated at run time.(W.R.T Backend)
	// If we need , tid and token, invoke those login or related APIs first.
	// This tid and token will be considered as default tid, default token.
	// No need to pass explicitly while automating apis

	protected static String defaulTID;
	protected static String defaultTOKEN;
	protected static String dtid;
	protected static Long orderId;
	protected static String OMSHost;
	protected static Integer OMSPort;

	protected static String oms_osrftoken;
	protected static String oms_token;
	protected static String oms_sessionid;
	protected static String deliveryAPIHost;
	protected static Integer deliveryAPIHostPort;
	public static String delHost;
	public static String dcHost;
	public static Integer dcPort;
	public static String backHost;
	public static Integer backHostPort;
	public static String deliveryAuthString;
	public static String couponsDataServiceAuthString;

	public static final String smokeTest = "_SmokeTest";
	public static final String sanityTest = "_SanityTest";
	public static final String regressionTest = "_RegressionTest";
	protected static HashMap<String, Object> cookie;
	protected static HashMap<String, Object> apikey;
	protected static HashMap<String, Object> Origin;

	// public static Long backendserviceableAddressId;

	// getEnvPropertyValue("keyToRead") :::use to read key from uat/prod
	// properties
	// getSuitePropertyValue("keyToRead") :::used to read key from
	// suite.properties

	// Initialize required variables inside this function or directly use
	public static void initializeRequiredvariables() {

		try {
//			OMSHost = getEnvPropertyValue("oms_host");
//			OMSPort = Integer.parseInt(getEnvPropertyValue("oms_port"));
//
//			oms_osrftoken = getEnvPropertyValue("oms_osrftoken");
//			oms_sessionid = getEnvPropertyValue("oms_sessionid");
//
//			deliveryAPIHost = getEnvPropertyValue("delivery_host");
//			deliveryAPIHostPort = Integer
//					.parseInt(getEnvPropertyValue("delivery_port"));
//
//			delHost = getEnvPropertyValue("deliveryAPIHost");
//
//			dcHost = getEnvPropertyValue("deliverycontrollerAPIHost");
//			dcPort = Integer
//					.parseInt(getEnvPropertyValue("deliveryAPIHostPort"));
//
//			backHost = getEnvPropertyValue("backend_host");
//			backHostPort = Integer
//					.parseInt(getEnvPropertyValue("backend_port"));
//			deliveryAuthString = getEnvPropertyValue(
//					"delivery_authstring_username") + DELIMITTER
//					+ getEnvPropertyValue("delivery_authstring_password");
//
//			couponsDataServiceAuthString=getEnvPropertyValue(
//					"backend_data_service_authstring_username")+ DELIMITTER
//					+ getEnvPropertyValue("backend_data_service_authstring_password");
			// backendserviceableAddressId = Long
			// .parseLong(getEnvPropertyValue("serviceable_address"));

		} catch (Exception e) {
			log.error(
					"Some variables couldNot get/Found to be Null from properties(suite/uat/prod) file");
			e.printStackTrace();
		}

	}

}
