package com.swiggy.automation.api.rest;

/**
 * 
 * @author mohammedramzi
 *
 */

public enum JsonValidationLevels {
	HIGH_LEVEL, DETAILED_LEVEL
}
