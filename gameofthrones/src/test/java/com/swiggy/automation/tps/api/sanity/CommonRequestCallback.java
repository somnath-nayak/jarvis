package com.swiggy.automation.tps.api.sanity;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CommonRequestCallback extends RestTestHelper {
//
//	HashMap<String, Object> update = new HashMap<>();
//	JsonNode ExpectedValue = null;
//	static int orderNo;
//	JsonNode ActualValue = null;
//
//	// @DataProvider(name = "TPS_HALDIRAMS_CONFIRM")
//	// public static Object[][] confirm() throws SwiggyAPIAutomationException {
//	// return APIDataReader.getDataSetData("TPS_HALDIRAMS_CONFIRM");
//	//
//	// }
//	//
//	// @Test(priority = 2, dataProvider = "TPS_HALDIRAMS_CONFIRM")
//	//
//	// public void ConfirmOrder(String dataGroup, String apiToExecute,
//	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//	// throws Exception {
//	// boolean isSuccess = false;
//	// JsonNode response = null;
//	// try {
//	//
//	// Integer order_id;
//	// order_id = (Integer) PublishOrdertoQueue.orderIds.get(13);
//	// System.out.println("new order id is" + order_id);
//	// apikey = new HashMap<>();
//	// apikey.put("api-key", "abcd");
//	// update.put("order_id", order_id);
//	//
//	// JsonNode updatedRequest = RestTestUtil
//	// .updateRequestDataBeforeSend(jsonRequestData, update);
//	// response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//	// apikey);
//	//
//	// isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//	// response);
//	//
//	// Assert.assertTrue(isSuccess,
//	// "Confirmed Order Successfully" + smokeTest);
//	//
//	// } catch (Exception e) {
//	// log.error(e);
//	// Assert.fail();
//	// }
//	//
//	// }
//	//
//	// @DataProvider(name = "TPS_HALDIRAMS_CONFIRM_NOUPDATE")
//	// public static Object[][] confirmO() throws SwiggyAPIAutomationException {
//	// return APIDataReader.getDataSetData("TPS_HALDIRAMS_CONFIRM_NOUPDATE");
//	//
//	// }
//	//
//	// @Test(priority = 3, dataProvider = "TPS_HALDIRAMS_CONFIRM_NOUPDATE")
//	//
//	// public void ConfirmOrde(String dataGroup, String apiToExecute,
//	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//	// throws Exception {
//	// boolean isSuccess = false;
//	// JsonNode response = null;
//	// try {
//	//
//	// apikey = new HashMap<>();
//	// apikey.put("api-key", "abcd");
//	//
//	// response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//	// apikey);
//	//
//	// isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//	// response);
//	//
//	// Assert.assertTrue(isSuccess,
//	// "Confirmed Order Successfully" + smokeTest);
//	//
//	// } catch (Exception e) {
//	// log.error(e);
//	// Assert.fail();
//	// }
//	//
//	// }
//
//	@DataProvider(name = "TPS_HALDIRAMS_CALLBACK")
//	public static Object[][] confirmcallback()
//			throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_HALDIRAMS_CALLBACK");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_HALDIRAMS_CALLBACK")
//
//	public void ConfirmOrdercb(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id; // = 1;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(13);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_HALDIRAMS_CALLBACK_NOUPDATE")
//	public static Object[][] confirmOcb() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_HALDIRAMS_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_HALDIRAMS_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrdecb(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_FRESHMENU_CALLBACKS")
//	public static Object[][] confirm() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_FRESHMENU_CALLBACKS");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_FRESHMENU_CALLBACKS")
//
//	public void ConfirmOrder(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(5);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_FRESHMENU_CALLBACK_NOUPDATE")
//	public static Object[][] confirmO() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_FRESHMENU_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_FRESHMENU_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrde(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_LIMETRAY_CALLBACKS")
//	public static Object[][] confirmLTC() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_LIMETRAY_CALLBACKS");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_LIMETRAY_CALLBACKS")
//
//	public void ConfirmOrderLTC(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(4);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_LIMETRAY_CALLBACK_NOUPDATE")
//	public static Object[][] confirmLT() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_LIMETRAY_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_LIMETRAY_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrdeLT(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_FORTHCODE_CALLBACKS")
//	public static Object[][] confirmFCC() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_FORTHCODE_CALLBACKS");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_FORTHCODE_CALLBACKS")
//
//	public void ConfirmOrderFCC(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(8);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_FORTHCODE_CALLBACK_NOUPDATE")
//	public static Object[][] confirmFC() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_FORTHCODE_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_FORTHCODE_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrdeFC(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_CHAIPOINT_CALLBACKS")
//	public static Object[][] confirmCPC() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_CHAIPOINT_CALLBACKS");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_CHAIPOINT_CALLBACKS")
//
//	public void ConfirmOrderCPC(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(6);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_CHAIPOINT_CALLBACK_NOUPDATE")
//	public static Object[][] confirmCP() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_CHAIPOINT_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_CHAIPOINT_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrdeCP(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_CCD_CALLBACKS")
//	public static Object[][] confirmCCDC() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_CCD_CALLBACKS");
//
//	}
//
//	@Test(priority = 2, dataProvider = "TPS_CCD_CALLBACKS")
//
//	public void ConfirmOrderCCDC(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			Integer order_id;
//			order_id = (Integer) PublishOrdertoQueue.orderIds.get(9);
//			System.out.println("new order id is" + order_id);
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//			update.put("order_id", order_id);
//
//			JsonNode updatedRequest = RestTestUtil
//					.updateRequestDataBeforeSend(jsonRequestData, update);
//			response = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "TPS_CCD_CALLBACK_NOUPDATE")
//	public static Object[][] confirmCCD() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_CCD_CALLBACK_NOUPDATE");
//
//	}
//
//	@Test(priority = 3, dataProvider = "TPS_CCD_CALLBACK_NOUPDATE")
//
//	public void ConfirmOrdeCCD(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		boolean isSuccess = false;
//		JsonNode response = null;
//		try {
//
//			apikey = new HashMap<>();
//			apikey.put("api-key", "abcd");
//
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData,
//					apikey);
//
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//
//			Assert.assertTrue(isSuccess,
//					"Confirmed Order Successfully" + smokeTest);
//
//		} catch (Exception e) {
//			log.error(e);
//			Assert.fail();
//		}
//
//	}

}