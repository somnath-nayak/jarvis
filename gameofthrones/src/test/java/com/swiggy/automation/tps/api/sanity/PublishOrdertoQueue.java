package com.swiggy.automation.tps.api.sanity;

import java.util.HashMap;
import java.util.Random;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class PublishOrdertoQueue extends RestTestHelper {
	private static String pubQueue = "swiggy.orders_prod_new";
	int order_id = 0;
	Random ran = new Random();
	public static HashMap<Integer, Object> orderIds = new HashMap<>();
	HashMap<String, Object> update = new HashMap<>();
	JsonNode expectedValue = null;
	boolean isSuccess = false;

	public void CreateRabbitMQCon(String json) throws java.io.IOException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("54.169.221.123");// rabbitmq host
		factory.setPort(5672);// rabbitmq port
		factory.setUsername("ds_swiggy");
		factory.setPassword("swiggy");
		try {
			Connection connection = factory.newConnection();
			System.out.println("Connection has been Established");
			Channel channel = connection.createChannel();

			channel.basicPublish("", pubQueue, null, json.getBytes());
			System.out.println("Message Pushed" + json);
			// channel.close();
			// connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@DataProvider(name = "TPS_REQCALLBACKS")
	public static Object[][] login() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TPS_REQCALLBACKS");
	}

	@Test(priority = 1, dataProvider = "TPS_REQCALLBACKS")
	public void PublishOrderQueue(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			for (int partnerId = 4; partnerId <= 13; partnerId++) {
				expectedValue = expectedData;
				order_id = GenerateOrderId();
				update.put("order_id", order_id);
				update.put("partner_id", partnerId);
				orderIds.put(partnerId, order_id);

				log.info("request data is" + jsonRequestData);
				JsonNode updaterequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);
				String strrequest = ConvJsontoString(updaterequest);
				CreateRabbitMQCon(strrequest);
				Thread.sleep(10000);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String ConvJsontoString(JsonNode jsonNode) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(jsonNode.toString(), Object.class);
			return mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(json);
		} catch (Exception e) {
			e.printStackTrace();
			return "Sorry, conversion from json to string failed";
		}
	}

	int GenerateOrderId() {
		return ran.nextInt(900000000) + 10000000;
	}

}