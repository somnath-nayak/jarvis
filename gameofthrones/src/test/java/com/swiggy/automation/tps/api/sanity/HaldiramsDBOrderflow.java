package com.swiggy.automation.tps.api.sanity;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.swiggy.automation.common.utils.DAOHelper;
import com.swiggy.automation.common.utils.DBConnectionPool;
import com.swiggy.automation.common.utils.DBTYpe;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.utils.APITestBase;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class HaldiramsDBOrderflow extends APITestBase {

//	static Connection con = null;
//	static Connection con1 = null;
//	static Statement statement = null;
//	static DBConnectionPool connectionPoolP = null;
//	static int orderNo;
//
//	@BeforeTest
//	public static void SqlConnection()
//			throws SQLException, InterruptedException {
//
//		con = DBUtil.getConnection(IDBDriverType.MSSQL,
//				serviceNameMap.get("tps_db_host"),
//				serviceNameMap.get("tps_db_name"),
//				serviceNameMap.get("tps_db_user"),
//				serviceNameMap.get("tps_db_password"));
//		System.out.println("Connection to the MSSQL DB is successfull");
//
//	}
//
//	@Test(priority = 1)
//	public static void UpdateOrd() {
//
//		try {
//
//			String updateOrder = "update S_OrderManager set event_status = 2 where order_id ='"
//					+ ReadOrderNumber() + "'";
//			log.info("Orderid is" + orderNo);
//			// System.out.println("Orderid is" + orderNo);
//			statement = con.createStatement();
//			statement.executeUpdate(updateOrder);
//			log.info("Executed Update Query successfully");
//			// System.out.println("Executed Update Query successfully");
//			Thread.sleep(1000);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	@Test(priority = 2)
//	public static void CheckOrdEventStatus() {
//
//		int eventstatus = 0;
//		try {
//			Thread.sleep(50000);
//			String verifyOrderEvent = "Select event_status from S_OrderManager where order_id = '"
//					+ ReadOrderNumber() + "'";
//			statement = con.createStatement();
//			ResultSet rs = statement.executeQuery(verifyOrderEvent);
//			while (rs.next()) {
//				eventstatus = rs.getInt("event_status");
//			}
//			System.out.println("status of resultset" + eventstatus);
//
//			if (eventstatus == 5) {
//				log.info("Order value is updated as expected");
//				// System.out.println("Order value is updated as expected");
//				Assert.assertTrue(true);
//			} else {
//				Assert.assertFalse(true);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	@AfterTest
//	public static void CloseConnection() throws Exception {
//		con.close();
//		statement.close();
//	}
//
//	@Test(priority = 3)
//	public void get1() {
//
//		try {
//			String status = CheckOMSOrderStatus(orderNo);
//			Assert.assertEquals(status, "placed");
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//
//	}
//
//	public static int ReadOrderNumber() {
//
//		{
//
//			try {
//
//				FileInputStream fstream = new FileInputStream(
//						"D:\\orderId.txt");
//				DataInputStream in = new DataInputStream(fstream);
//				BufferedReader br = new BufferedReader(
//						new InputStreamReader(in));
//
//				String sCurrentLine = "";
//				System.out.println("current line" + sCurrentLine);
//
//				while ((sCurrentLine = br.readLine()) != null) {
//					orderNo = Integer.parseInt(sCurrentLine);
//					System.out.println("Order line" + orderNo);
//				}
//
//				br.close();
//				in.close();
//				fstream.close();
//
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			System.out.println("Order No" + orderNo);
//			return orderNo;
//
//		}
//
//	}
//
//	static {
//		try {
//			IntializePostgreConnectionPool();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static long CheckIDOMS(long value) throws Exception {
//		String query = "Select id from oms_order where order_id=" + value;
//		con1 = getPostgreConnection();
//		try {
//			ResultSet rs = executeQuery(con1, query);
//			if (null == rs) {
//				throw new SwiggyAPIAutomationException("Result Set NULL");
//			}
//			rs.next();
//			Long id = rs.getLong("id");
//			System.out.println("ID of the Order is :" + id);
//			return id;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return -999;
//	}
//
//	public String CheckOMSOrderStatus(long value) throws Exception {
//		String query = "Select placed_status from oms_orderstatus where id="
//				+ CheckIDOMS(value);
//		try {
//			ResultSet rs = executeQuery(con1, query);
//			if (null == rs) {
//				throw new SwiggyAPIAutomationException("Result Set NULL");
//			}
//			rs.next();
//			String statusOfOrder = rs.getString("placed_status");
//			System.out.println("Status of the Order is :" + statusOfOrder);
//			return statusOfOrder;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
//
//	// Modify the below method for comments query
//	public long CheckCommentsOMSOrder(long value) throws Exception {
//		String query = "Select * from oms_ comment where order_id="
//				+ CheckIDOMS(value);
//		Connection con = getPostgreConnection();
//		try {
//			ResultSet rs = executeQuery(con, query);
//			if (null == rs) {
//				throw new SwiggyAPIAutomationException("Result Set NULL");
//			}
//			rs.next();
//			Long id = rs.getLong("id");
//			log.info("ID of the Order is :" + id);
//			return id;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return -999;
//	}
//
//	public static void IntializePostgreConnectionPool() throws Exception {
//
//		connectionPoolP = DAOHelper.getConnectionPool(DBTYpe.POSTGRES,
//				getEndPoint("omt_postgres_db_host"),
//				getEndPoint("omt_postgres_db_port"),
//				getEndPoint("omt_postgres_dbname"),
//				getEndPoint("omt_postgres_dbusername"),
//				getEndPoint("omt_postgres_dbpassword"),
//				Integer.parseInt(
//						getEndPoint("omt_postgres_dbintialconnection")),
//				Integer.parseInt(
//						getEndPoint("omt_postgres_dbmaxconnection")),
//				true);
//	}
//
//	public static Connection getPostgreConnection() throws Exception {
//
//		if (null == connectionPoolP) {
//			throw new SwiggyAPIAutomationException(
//					"Postgress Connection Pool Null");
//		}
//		Connection con = connectionPoolP.getConnection();
//		System.out
//				.println("We have got connection from PostgresConnectionPool");
//		return con;
//	}
//
//	private static ResultSet executeQuery(Connection con, String query)
//			throws SQLException {
//		PreparedStatement prepStmt;
//		prepStmt = con.prepareStatement(query);
//		ResultSet rs = prepStmt.executeQuery();
//		return rs;
//	}
}
