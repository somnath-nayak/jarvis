package com.swiggy.automation.tps.api.sanity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CcdOrderRelay extends RestTestHelper {
//	private static String pubQueue = "swiggy.orders_to_interocitor";
//	private static String recvQueue = "swiggy.partner_update_dummy";
//	// private static String tpsQueue = "post_tps_order_json";
//	private static List<String> queuelist = new ArrayList<>();
//	// private static List<String> queuelist1 = new ArrayList<>();
//
//	int order_id = 0;
//	JsonNode json = null;
//	Random ran = new Random();
//	public static HashMap<Integer, Object> orderIds = new HashMap<>();
//	Integer partnerId = 9;
//	HashMap<String, Object> update = new HashMap<>();
//	boolean isSuccess = false;
//	JsonNode expectedValue = null;
//	JsonNode expectedValue1 = null;
//
//	public void CreateRabbitMQCon(String json) throws java.io.IOException {
//
//		ConnectionFactory factory = new ConnectionFactory();
//		factory.setHost("52.77.65.240");// rabbitmq host
//		factory.setPort(5672);// rabbitmq port
//		factory.setUsername("ds_swiggy");
//		factory.setPassword("swiggy");
//		try {
//			Connection connection = factory.newConnection();
//			log.info("Connection has been Established");
//			Channel channel = connection.createChannel();
//
//			channel.basicPublish("", pubQueue, null, json.getBytes());
//			log.info("Message Pushed" + json);
//			channel.close();
//			connection.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	@DataProvider(name = "TPS_CCD")
//	public static Object[][] login() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("TPS_CCD");
//	}
//
//	@Test(priority = 1, dataProvider = "TPS_CCD")
//	public void PublishOrderQueue(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		try {
//
//			order_id = GenerateOrderId();
//			// String s = jsonRequestData.get("order_data").asText();
//			// JSONObject jo = new JSONObject(s);
//			// jo.put("order_id", order_id);
//
//			JsonNode order_data = APIUtils.convertStringtoJSON(
//					jsonRequestData.get("order_data").asText());
//			JsonNode updated = RestTestUtil.updateRequestDataBeforeSend(
//					order_data, "order_id", order_id);
//
//			JsonNode finall = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_data", updated);
//
//			// JSONObject j1 = new JSONObject(jsonRequestData);
//			// j1.put("order_data", jo.toString());
//
//			// JsonNode updatedJson =
//			// APIUtils.convertStringtoJSON(j1.toString());
//
//			expectedValue = expectedData.get("json1");
//			log.info(expectedValue);
//
//			expectedValue1 = expectedData.get("json2");
//			log.info(expectedValue1);
//
//			String strrequest = ConvJsontoString(finall);
//			CreateRabbitMQCon(strrequest);
//			Thread.sleep(10000);
//			ReceiveOrderQueue();
//			// ReceiveOrderTPSQueue();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	public void ReceiveOrderQueue()
//			throws java.io.IOException, java.lang.InterruptedException {
//
//		ConnectionFactory factory = new ConnectionFactory();
//		factory.setHost("52.77.65.240");
//		factory.setPort(5672);// rabbitmq port
//		factory.setUsername("ds_swiggy");
//		factory.setPassword("swiggy");
//		try {
//			Connection conn = factory.newConnection();
//			Channel channel = conn.createChannel();
//			QueueingConsumer consumer = new QueueingConsumer(channel);
//			AMQP.Queue.DeclareOk store = channel.queueDeclare(recvQueue, true,
//					false, false, null);
//			int queuecount = store.getMessageCount();
//			log.info("count" + queuecount);
//			System.out
//					.println(" [*] Waiting for messages. To exit press CTRL+C");
//			channel.basicConsume(recvQueue, false, consumer);
//			for (int j = 0; j < queuecount; j++) {
//				QueueingConsumer.Delivery delivery = consumer.nextDelivery();
//				log.info("Test");
//				String message = new String(delivery.getBody());
//				queuelist.add(message);
//				log.info(" [x] Received '" + message + "'");
//
//			}
//
//			channel.close();
//			conn.close();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/*
//	 * public void ReceiveOrderTPSQueue() throws java.io.IOException,
//	 * java.lang.InterruptedException {
//	 * 
//	 * ConnectionFactory factory = new ConnectionFactory();
//	 * factory.setHost("52.77.65.240"); factory.setPort(5672);// rabbitmq port
//	 * factory.setUsername("ds_swiggy"); factory.setPassword("swiggy"); try {
//	 * Connection conn = factory.newConnection(); Channel channel =
//	 * conn.createChannel(); QueueingConsumer consumer = new
//	 * QueueingConsumer(channel); //AMQP.Queue.DeclareOk store =
//	 * channel.queueDeclare(tpsQueue, true, //false, false, null); //int
//	 * queuecount = store.getMessageCount(); //System.out.println("count" +
//	 * queuecount); System.out.println(" TPS Queue is");
//	 * //channel.basicConsume(tpsQueue, false, consumer); //for (int j = 0; j <
//	 * queuecount; j++) { QueueingConsumer.Delivery delivery =
//	 * consumer.nextDelivery(); System.out.println("Test"); String message = new
//	 * String(delivery.getBody()); queuelist1.add(message);
//	 * System.out.println(" [x] TPS Queue is '" + message + "'");
//	 * 
//	 * }
//	 * 
//	 * channel.close(); conn.close();
//	 * 
//	 * } catch (Exception e) { e.printStackTrace(); } }
//	 */
//
//	@Test(priority = 2, dependsOnMethods = "PublishOrderQueue")
//	public void verifyOrderinOutputQueue() throws JsonParseException,
//			IOException, SwiggyAPIAutomationException {
//		JsonNode response = CheckOrderIdPresentoutput(1918195);
//		// JsonNode response1 = CheckOrderIdtpsoutput(order_id);
//		// System.out.println("Expected Data is " + expectedValue);
//		// System.out.println("Expected Data is " + expectedValue1);
//		log.info("Actual Response is " + response);
//		// System.out.println("Actual Response is " + response1);
//		log.info("Expected Data is " + expectedValue);
//		log.info("Actual Response is " + response);
//		/*
//		 * log.info("Expected Data is " + expectedValue1);
//		 * log.info("Actual Response is " + response1);
//		 */
//
//		boolean success1 = JsonValidator.DETAILED_LEVEL
//				.validateJson(expectedValue, response);
//		Assert.assertTrue(success1, "verifyOrderinOutputQueue CASE1");
//		/*
//		 * boolean success2 = JsonValidator.DETAILED_LEVEL
//		 * .validateJson(expectedValue1, response1); Assert.assertTrue(success2,
//		 * "verifyOrderinOutputQueue CASE2"); Assert.assertTrue(success1 &
//		 * success2, "verifyOrderinOutputQueue CASE1,2");
//		 */
//	}
//
//	private JsonNode stringtojson(String str) throws JsonParseException,
//			IOException, SwiggyAPIAutomationException {
//		return APIUtils.convertStringtoJSON(str);
//
//	}
//
//	public JsonNode CheckOrderIdPresentoutput(Integer OrderId)
//			throws JsonParseException, IOException,
//			SwiggyAPIAutomationException {
//
//		for (int k = 0; k < queuelist.size(); k++) {
//			json = stringtojson(queuelist.get(k));
//			// json = APIUtils.convertStringtoJSON(queuelist.get(k));
//			if (OrderId.equals(json.get("order_id").asInt())) {
//				return json;
//			}
//
//		}
//		return null;
//	}
//
//	// public JsonNode CheckOrderIdtpsoutput(Integer OrderId)
//	// throws JsonParseException, IOException,
//	// SwiggyAPIAutomationException {
//	//
//	// for (int k = 0; k < queuelist1.size(); k++) {
//	// json = stringtojson(queuelist1.get(k));
//	// // json = APIUtils.convertStringtoJSON(queuelist1.get(k));
//	//
//	// try {
//	//
//	// System.out.println(json);
//	//
//	// if (null != json.get("order_id")
//	// && OrderId == (json.get("order_id").asInt())) {
//	// return json;
//	// } else if (null != json.get("orderId")
//	// && OrderId == (json.get("orderId").asInt())) {
//	// return json;
//	// } else if (json.has("customer_info")) {
//	//
//	// if (null != json.get("customer_info").get("id")
//	// && OrderId == (json.get("customer_info").get("id")
//	// .asInt())) {
//	// return json;
//	// }
//	// } else if (null != json.get("OrderId")
//	// && OrderId == (json.get("OrderId").asInt())) {
//	// return json;
//	// } else if (null != json.get("OnlineOrderNo")
//	// && OrderId == (json.get("OnlineOrderNo").asInt())) {
//	// return json;
//	// } else if (null != json.get("external_order_id")
//	// && OrderId == (json.get("external_order_id").asInt())) {
//	// return json;
//	// }
//	//
//	// } catch (Exception e) {
//	// // TODO: handle exception
//	// e.printStackTrace();
//	// }
//	//
//	// }
//	// return null;
//	// }
//
//	public static HashMap<Integer, Object> GetPartnerMap() {
//		return orderIds;
//	}
//
//	public String ConvJsontoString(JsonNode jsonNode) {
//		try {
//			ObjectMapper mapper = new ObjectMapper();
//			Object json = mapper.readValue(jsonNode.toString(), Object.class);
//			return mapper.writerWithDefaultPrettyPrinter()
//					.writeValueAsString(json);
//		} catch (Exception e) {
//			return "Sorry, conversion from json to string failed";
//		}
//	}
//
//	int GenerateOrderId() {
//		return ran.nextInt(900000000) + 10000000;
//	}

}
