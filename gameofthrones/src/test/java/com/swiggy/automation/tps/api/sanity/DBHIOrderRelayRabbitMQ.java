package com.swiggy.automation.tps.api.sanity;

import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Random;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DBHIOrderRelayRabbitMQ {
	private static String publishQueue = "swiggy.orders_to_interocitor";
	static int order_id = 0;
	Random ran = new Random();
	public static HashMap<Integer, Object> orderIds = new HashMap<>();
	public static Integer partnerId = 13; // Partner Id of Haldirams
	HashMap<String, Object> update = new HashMap<>();
	JsonNode expectedValue = null;
	// JsonNode actualValue = null;

	public void RabbitMQConnection(String json) throws IOException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("52.77.65.240");// rabbitmq host
		factory.setPort(5672);// rabbitmq port
		factory.setUsername("ds_swiggy"); // rabbitmq credentials
		factory.setPassword("swiggy");
		try {
			Connection connection = factory.newConnection();
			System.out.println("Connection has been Established");
			Channel channel = connection.createChannel();
			channel.basicPublish("", publishQueue, null, json.getBytes());
			System.out.println("Message Pushed" + json);
			// channel.close();
			// connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@DataProvider(name = "TPS_HALDIRAMS")
	public static Object[][] Login() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TPS_HALDIRAMS");
	}

	@Test(dataProvider = "TPS_HALDIRAMS")
	public void PublishOrder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			expectedValue = expectedData;
			order_id = GenerateOrderId();
			update.put("order_id", order_id);
			update.put("partner_type", partnerId);
			orderIds.put(partnerId, order_id);
			System.out.println("request data is" + jsonRequestData);
			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_data", update);
			String strrequest = JsonToString(updaterequest);
			RabbitMQConnection(strrequest);
			System.out.println("order" + order_id);
			Thread.sleep(10000);
			OrderTempFileWrite();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static HashMap<Integer, Object> GetPartnerMap() {
		return orderIds;
	}

	public String JsonToString(JsonNode jsonNode) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Object json = mapper.readValue(jsonNode.toString(), Object.class);
			return mapper.writerWithDefaultPrettyPrinter()
					.writeValueAsString(json);
		} catch (Exception e) {
			return "Sorry,  print didn't work";
		}
	}


	public static void OrderTempFileWrite() {

		try {

			FileOutputStream foutstream = new FileOutputStream(
					"D:\\orderId.txt");
			DataOutputStream out = new DataOutputStream(foutstream);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(out));
			bw.write(String.valueOf(order_id));

			System.out.println("Order Id generated is:" + order_id);
			bw.close();
			out.close();
			foutstream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" File createed successfully");
	}

	int GenerateOrderId() {
		return ran.nextInt(900000000) + 10000000;
	}
}
