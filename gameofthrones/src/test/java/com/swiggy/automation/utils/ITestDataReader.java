package com.swiggy.automation.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * 
 * @author mohammedramzi
 *
 */
public interface ITestDataReader {

	Object[][] getDataSetTestDataMinimal(String dataGroupName);

	Object[][] getDataSetTestData(String dataGroupName);

	Object[][] getDataSetTestDataWithExpectedFields(String dataGroupName);

	Integer[] getTestDataGroupStartAndEndRow(String dataGroup);

	ArrayList<String> getDataSetNames(String dataGroup);

	LinkedHashMap<String, String> getTestData(String dataGroupName,
                                              String columnHeader);

	HashMap<String, Integer[]> getTestDataGroupInfo();

}
