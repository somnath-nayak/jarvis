package com.swiggy.automation.utils;

public final class STAFAgent extends APITestBase{

	public static String getSTAFAPIInfo(String key){
		try {
			return getValueFromPropertyMap(key,apiProperties);
		} catch (SwiggyAPIAutomationException e) {
			e.printStackTrace();
		}
		return key+ "API Info Not available .verify key :stfapiname in env xml";
	}
	
	public static String getSTAFValue(String key)
	{
		try {
			return getValueFromPropertyMap(key,commonKeyValuePairsMap);
		} catch (SwiggyAPIAutomationException e) {
			e.printStackTrace();
		}
		return key+" notFound please check in env stf kay-val session";
	}	
	public static String getSTAFEndPoint(String GOTserviceName)
			throws SwiggyAPIAutomationException {
		return getValueFromPropertyMap(GOTserviceName, serviceNameMap);
	}	
}
