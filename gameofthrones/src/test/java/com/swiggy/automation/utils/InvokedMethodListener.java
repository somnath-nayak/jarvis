package com.swiggy.automation.utils;

import org.apache.log4j.Logger;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

public class InvokedMethodListener implements IInvokedMethodListener {
	public static final Logger log = Logger
			.getLogger(InvokedMethodListener.class);
	protected static String testMethodUnderExecution = null;

	@Override
	public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
		if (method.getTestMethod().isTest()) {
			log.info("#############START#################");
			log.info(method.getTestMethod().getRealClass());
			log.info(method.getTestMethod().getMethodName());
			System.out.println(method.getTestMethod().getMethodName());
			testMethodUnderExecution = method.getTestMethod().getMethodName();
		}
	}

	@Override
	public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
		if (method.getTestMethod().isTest()) {
			log.info("##############END################");
		}
	}

}
