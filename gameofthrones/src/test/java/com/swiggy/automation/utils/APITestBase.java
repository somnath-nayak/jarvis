package com.swiggy.automation.utils;

import com.swiggy.automation.api.rest.RestTestHelper;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * 
 * @author mohammedramzi
 *
 */
public abstract class APITestBase extends CommonTestBase {

	public static final Logger log = Logger.getLogger(APITestBase.class);

	protected static HashMap<String, String> apiProperties = null;
	// public static HashMap<String, String> envProperties = null;
	protected static final String DELIMITTER = ":";
	// public static HashMap<String, String> suiteProperties = null;


	static {

		try {

			loadNSetEnvInfo();
			initializeCustomDataIfAny();
			//initializeDefaultData();
			logExecutionEnvInfo();
			RestTestHelper.initializeRequiredvariables();
		} catch (Exception e) {
			log.error(
					"Env Settings :APITestBase --->  Exception occured while intialization/loading :"
							+ e);
			e.printStackTrace();
		}
	}
	


	private static void loadNSetEnvInfo() {
//		apiProperties = CommonUtil
//				.getData(IDataInilizer.API_INPUT_PROPERTIESFILEPATH);
		apiProperties=mydata.get("STAF_API_INFO");
		// suiteProperties = CommonUtil
		// .getData(IDataInilizer.API_SUITE_PROPERTIESFILEPATH);
		if (null == apiProperties) {
			log.fatal("api properties file not found....So exiting");
			System.exit(0);
		}
	}
	
	public static String getSTAFAPIInfo(String key){
		try {
			return getValueFromPropertyMap(key,apiProperties);
		} catch (SwiggyAPIAutomationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return key+ "API Info Not available .verify key :stfapiname in env xml";
	}

	private static void initializeCustomDataIfAny() {

		System.out.println("initializeCustomDataIfAny");
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>");
		if (null != System.getProperty("CUSTOM_EXCEL_PATH")
				&& !System.getProperty("CUSTOM_EXCEL_PATH").isEmpty()) {
			suitePropertyMap.put("inputexceldatafilepath",
					System.getProperty("CUSTOM_EXCEL_PATH").trim());
			System.out.println("CUSTOM_EXCEL_PATH FOUND :"
					+ suitePropertyMap.get("inputexceldatafilepath").trim());
			log.info("CUSTOM_EXCEL_PATH FOUND :"
					+ suitePropertyMap.get("inputexceldatafilepath").trim());
		}
		if (null != System.getProperty("CUSTOM_SHEET_NAME")
				&& !System.getProperty("CUSTOM_SHEET_NAME").isEmpty()) {
			suitePropertyMap.put("excelDatasheetname",
					System.getProperty("CUSTOM_SHEET_NAME").trim());
			System.out.println("CUSTOM_SHEET_NAME FOUND :"
					+ suitePropertyMap.get("excelDatasheetname"));
			log.info("CUSTOM_SHEET_NAME FOUND :"
					+ suitePropertyMap.get("excelDatasheetname"));
		}

		if (null != System.getProperty("CUSTOM_OMS_OSRF_TOKEN")
				&& !System.getProperty("CUSTOM_OMS_OSRF_TOKEN").isEmpty()) {
			commonKeyValuePairsMap.put("oms_osrftoken",
					System.getProperty("CUSTOM_OMS_OSRF_TOKEN").trim());
			System.out.println("CUSTOM_OMS_OSRF_TOKEN FOUND :"
					+ commonKeyValuePairsMap.get("oms_osrftoken"));
			log.info("CUSTOM_OMS_OSRF_TOKEN FOUND :"
					+ commonKeyValuePairsMap.get("oms_osrftoken"));
		}

		if (null != System.getProperty("CUSTOM_OMS_SESSION_ID")
				&& !System.getProperty("CUSTOM_OMS_SESSION_ID").isEmpty()) {
			commonKeyValuePairsMap.put("oms_sessionid",
					System.getProperty("CUSTOM_OMS_SESSION_ID").trim());
			System.out.println("CUSTOM_OMS_SESSION_ID FOUND :"
					+ commonKeyValuePairsMap.get("oms_sessionid"));
			log.info("CUSTOM_OMS_SESSION_ID FOUND :"
					+ commonKeyValuePairsMap.get("oms_sessionid"));
		}

		System.out.println(">>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<");

	}


	private static void logExecutionEnvInfo() {
		log.info(
				"***********************************API AUTOMATION**************************");
		log.info("Environment Set #####:"
				+ System.getProperty(IDataInilizer.KEY_ENV));
		log.info(
				"***************************************************************************");
	}

}
