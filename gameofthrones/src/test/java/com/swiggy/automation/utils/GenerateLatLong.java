package com.swiggy.automation.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class GenerateLatLong {

    private static final String FILENAME = "Lisitng.csv";

    public static void main(String[] args) {

        BufferedWriter bw = null;
        FileWriter fw = null;
        double lat=12.9325353;
        double lng=77.6035236;

        try {
            double R = 6371;
            double radius = 2.5;
            double latMin = lng - Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
            double latMax = lng + Math.toDegrees(radius/R/Math.cos(Math.toRadians(lat)));
            double longMax = lat + Math.toDegrees(radius/R);
            double longMin = lat - Math.toDegrees(radius/R);

                    
            //String content = "This is the content to write into file\n";

            fw = new FileWriter(FILENAME);
            bw = new BufferedWriter(fw);
            /*for(double i=longMin;i<longMax;i++)
            {
                double longmin1=longMin+;
                bw.write("\""+longMin+","+latMin+"\"\n");
            }*/
            for(double i=longMin; i<longMax; i+=0.0001){
                for(double j=latMin; j<latMax; j+=0.0001){
                    
                    bw.write(i+","+j+"\n");
                }
            }
            System.out.println("Done");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)
                    bw.close();
                if (fw != null)
                    fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}