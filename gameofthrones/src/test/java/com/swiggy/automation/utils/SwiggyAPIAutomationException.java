package com.swiggy.automation.utils;

/**
 * 
 * @author mohammedramzi
 *
 */
public class SwiggyAPIAutomationException extends Exception {

	private static final long serialVersionUID = 1L;

	public SwiggyAPIAutomationException(String message) {
		super(message);
	}

}
