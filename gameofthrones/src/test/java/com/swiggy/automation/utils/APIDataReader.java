package com.swiggy.automation.utils;

import com.swiggy.automation.excel.utils.ExcelDataReader;
import org.apache.log4j.Logger;

/**
 * 
 * @author mohammedramzi
 *
 */
public class APIDataReader extends APITestBase {

	final static Logger log = Logger.getLogger(APIDataReader.class);

	private static ExcelDataReader excelObject = null;

	/**
	 * 
	 * @param dataGroupName
	 * @return DATAGROUPNAME,API KEY,DATASETNAME,REQUEST DATA,EXPECTED DATA
	 * @throws SwiggyAPIAutomationException
	 */

	public static Object[][] getDataSetData(String dataGroupName)
			throws SwiggyAPIAutomationException {

		if (null == dataGroupName || dataGroupName.trim().isEmpty()) {
			log.error("DataGroupName passed is empty!!!!!Passed DataGroupPassed"
					+ dataGroupName);
			throw new SwiggyAPIAutomationException(
					"DataGroupName is null/empty,passed DG:" + dataGroupName);
		}
		return getTestDataObject(dataGroupName,
				getSuitePropertyValue("inputexceldatafilepath"),
				getSuitePropertyValue("excelDatasheetname"));

	}

	/**
	 * 
	 * @param dataGroupName
	 * @param sheetName
	 * @return DATAGROUPNAME,API KEY,DATASETNAME,REQUEST DATA,EXPECTED DATA
	 * @throws SwiggyAPIAutomationException
	 */

	public static Object[][] getDataSetData(String dataGroupName,
			String sheetName) throws SwiggyAPIAutomationException {
		if (null == dataGroupName || dataGroupName.trim().isEmpty()) {
			log.error(
					"DataGroupName passed is empty!!!!!Passed ,DataGroupPassed :"
							+ dataGroupName);
			throw new SwiggyAPIAutomationException(
					"DataGroupName is null/empty,passed DG:" + dataGroupName);
		}

		if (null == sheetName || sheetName.trim().isEmpty()) {
			log.error("sheetName passed is empty!!!!!Passed, sheetName Passed :"
					+ sheetName);
			throw new SwiggyAPIAutomationException(
					"sheetName is null/empty,passed sheetName:" + sheetName);
		}
		log.info(
				"Passed Specific API SheetName to execute...Sheet to be executed Now:"
						+ sheetName);
		return getTestDataObject(dataGroupName,
				getSuitePropertyValue("inputexceldatafilepath"), sheetName);
	}

	/**
	 * 
	 * @param dataGroupName
	 * @param sheetName
	 * @param excelSheetPath
	 * @return DATAGROUPNAME,API KEY,DATASETNAME,REQUEST DATA,EXPECTED DATA
	 * @throws SwiggyAPIAutomationException
	 */

	public static Object[][] getDataSetData(String dataGroupName,
			String sheetName, String excelSheetPath)
			throws SwiggyAPIAutomationException {

		if (null == dataGroupName || dataGroupName.trim().isEmpty()) {
			log.error(
					"DataGroupName passed is empty!!!!!Passed ,DataGroupPassed :"
							+ dataGroupName);
			throw new SwiggyAPIAutomationException(
					"dataGroupName is null/empty,passed dataGroupName:"
							+ dataGroupName);
		}

		if (null == sheetName || sheetName.trim().isEmpty()) {
			log.error("sheetName passed is empty!!!!!Passed, sheetName Passed :"
					+ sheetName);
			throw new SwiggyAPIAutomationException(
					"sheetName is null/empty,passed sheetName:" + sheetName);
		}
		if (null == excelSheetPath || excelSheetPath.trim().isEmpty()) {
			log.error(
					"excelSheetPath passed is empty!!!!!Passed, excelSheetPath Passed :"
							+ excelSheetPath);
			throw new SwiggyAPIAutomationException(
					"excelSheetPath is null/empty,passed excelSheetPath:"
							+ excelSheetPath);
		}

		log.info(
				"Passed Separate ExcelSheet and Specific API SheetName  to execute...ExcelFIle and Sheet to be executed Now:File>Sheet :"
						+ excelSheetPath + ":-->" + sheetName);
		return getTestDataObject(dataGroupName, excelSheetPath, sheetName);
	}

	/**
	 * 
	 * @param dataGroupName
	 * @return DATAGROUPNAME,API KEY,DATASETNAME,REQUEST DATA,EXPECTED
	 *         DATA,APIFIELD DATA
	 * @throws SwiggyAPIAutomationException
	 */

	public static Object[][] getDataSetDataWithAPIFields(String dataGroupName)
			throws SwiggyAPIAutomationException {
		if (null == dataGroupName || dataGroupName.trim().isEmpty()) {
			log.error("DataGroupName passed is empty!!!!!Passed DataGroupPassed"
					+ dataGroupName);
			throw new SwiggyAPIAutomationException(
					"dataGroupName is null/empty,passed dataGroupName:"
							+ dataGroupName);
		}

		return getTestDataObjectWithExpectedAPIFields(dataGroupName,
				getSuitePropertyValue("inputexceldatafilepath"),
				getSuitePropertyValue("excelDatasheetname"));
	}

	/**
	 * 
	 * @param dataGroupName
	 * @param sheetName
	 * @return DATAGROUPNAME,API KEY,DATASETNAME,REQUEST DATA,EXPECTED
	 *         DATA,APIFIELD DATA
	 * @throws SwiggyAPIAutomationException
	 */

	public static Object[][] getDataSetDataWithAPIFields(String dataGroupName,
			String sheetName) throws SwiggyAPIAutomationException {

		if (null == dataGroupName || dataGroupName.trim().isEmpty()) {
			log.error(
					"DataGroupName passed is empty!!!!!Passed ,DataGroupPassed :"
							+ dataGroupName);
			throw new SwiggyAPIAutomationException(
					"dataGroupName is null/empty,passed dataGroupName:"
							+ dataGroupName);
		}

		if (null == sheetName || sheetName.trim().isEmpty()) {
			log.error("sheetName passed is empty!!!!!Passed, sheetName Passed :"
					+ sheetName);
			throw new SwiggyAPIAutomationException(
					"sheetName is null/empty,passed sheetName:" + sheetName);
		}

		log.info(
				"Passed Specific API SheetName to execute...Sheet to be executed Now:"
						+ sheetName);
		return getTestDataObjectWithExpectedAPIFields(dataGroupName,
				getSuitePropertyValue("inputexceldatafilepath"), sheetName);
	}

	private static Object[][] getTestDataObjectWithExpectedAPIFields(
			String dataGroupName, String excelPath, String sheetName)
			throws SwiggyAPIAutomationException {
		excelObject = ExcelDataReader.getExcelObject(excelPath, sheetName);
		return excelObject
				.getAPIDataSetTestDataWithExpectedFields(dataGroupName);
	}

	private static Object[][] getTestDataObject(String dataGroupName,
			String excelPath, String sheetName)
			throws SwiggyAPIAutomationException {
		excelObject = ExcelDataReader.getExcelObject(excelPath, sheetName);
		return excelObject.getAPIDataSetTestData(dataGroupName);
	}
}
