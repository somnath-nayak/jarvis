package com.swiggy.automation.utils;

import framework.gameofthrones.Aegon.StaticData;
import org.apache.log4j.Logger;

import java.util.HashMap;

public abstract class CommonTestBase {
	
	
	
	

	public static final Logger log = Logger.getLogger(CommonTestBase.class);
	protected static HashMap<String, String> serviceNameMap = null;
	protected static HashMap<String, String> suitePropertyMap = null;
	protected static HashMap<String, String> commonKeyValuePairsMap = null;
	protected static HashMap<String, HashMap<String, String>> mydata=null;
	static {

		try {

			loadNSetConfigInfo();

		} catch (Exception e) {
			log.error(
					"CommonTestBase--loadNSetConfigInfo() : --->  Exception occured while intialization/loading :"
							+ e);
			e.printStackTrace();
		}
	}

	private static void loadNSetConfigInfo() {
		mydata=new StaticData().getRequiredStafDataFromGOT();
		suitePropertyMap =mydata.get("STAF_SUITE_INFO");
		commonKeyValuePairsMap=mydata.get("KAYVALUE_PAIRS");
		if (null == suitePropertyMap) {
			log.error("env:suite.properties file not found....So exiting");
			System.exit(0);
		}
		if (null == System.getProperty(IDataInilizer.KEY_ENV)) {
			log.warn(
					"You Not Set the Environment.... So ,Taking Default Environment as IDataInilizer.KEY_ENV_STAGE3");
			System.setProperty(IDataInilizer.KEY_ENV,
					IDataInilizer.KEY_ENV_STAGE3);
		}
		 if (System.getProperty(IDataInilizer.KEY_ENV)
				.equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)||System.getProperty(IDataInilizer.KEY_ENV)
				.contains(IDataInilizer.KEY_ENV_STAGE1)||System.getProperty(IDataInilizer.KEY_ENV)
				.contains(IDataInilizer.KEY_ENV_STAGE2)||System.getProperty(IDataInilizer.KEY_ENV)
				.contains(IDataInilizer.KEY_ENV_STAGE3)) {
			serviceNameMap =mydata.get("STAF_ENV_INFO");
		} else {
			log.error(
					"Unable to find the property provided So,System will exist from execution: Provided>"
							+ System.getProperty(IDataInilizer.KEY_ENV));
			System.exit(0);
		}

	}



	protected static String getSuitePropertyValue(String propertyKey)
			throws SwiggyAPIAutomationException {
		return getValueFromPropertyMap(propertyKey, suitePropertyMap);
	}
	


	protected static String getValueFromPropertyMap(String propertyKey,
			HashMap<String, String> propertyMap)
			throws SwiggyAPIAutomationException {
		if (null == propertyKey || null == propertyMap) {
			log.error("Exception:" + new SwiggyAPIAutomationException(
					"Key/propertyFileMap is null"));
			throw new SwiggyAPIAutomationException(
					"Key/propertyFileMap is null");
		}
		return propertyMap.get(propertyKey).trim();
	}

}
