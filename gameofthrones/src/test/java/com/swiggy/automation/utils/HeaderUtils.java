package com.swiggy.automation.utils;

import com.swiggy.api.dash.kms.constants.KMS;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;

public class HeaderUtils {


    public static HashMap<String,String> getDefaultMapSearchHeader(){
        HashMap<String, String> hMap = new HashMap<>();
        hMap.put(HeaderConstant.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        return hMap;
    }

    public static HashMap<String,String> getHeaderForDeApp(String auth){
        HashMap<String, String> hMap = new HashMap<>();
        hMap.put(HeaderConstant.AUTH, auth);
        return hMap;
    }

    public static HashMap<String,String> getHeaderForKms(String token){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(KMS.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(KMS.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(KMS.HEADER_AUTH, "Token "+token);
        return headers;
    }





}
