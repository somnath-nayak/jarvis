package com.swiggy.automation.reporting;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;

public class ExtentReporting implements ITestListener {

	private static ExtentReports extent;
	private static ExtentTest test;
	private static boolean flag = false, finish = false;

	public static ExtentTest getTest() {
		return test;
	}

	public void setTest(ExtentTest test) {
        ExtentReporting.test = test;
	}

	static {
		extent = new ExtentReports(
				System.getProperty("user.dir") + File.separator + "test-output"
						+ File.separator + "Default Suite" + ".html",
				true);

		test = extent.startTest("Default Test");
	}

	@Override
	public synchronized void onTestStart(ITestResult result) {
		test = extent.startTest(result.getMethod().getMethodName());
		if (result.getMethod().getGroups().length > 0)
			test.assignCategory(result.getMethod().getGroups());

		if (result.getMethod().getDescription() != null)
			test.getTest().setDescription(result.getMethod().getDescription());
	}

	@Override
	public synchronized void onTestSuccess(ITestResult result) {
		test.log(LogStatus.PASS,
				LogStatus.PASS.toString().toUpperCase() + "ED");
		extent.endTest(test);
	}

	@Override
	public synchronized void onTestFailure(ITestResult result) {
		System.out.println(result.getThrowable().toString());
		String traces = "";
		StackTraceElement trace[] = result.getThrowable().getStackTrace();
		for (StackTraceElement e : trace) {
			System.out.println(e.toString());
			if (e != null)
				traces += e.toString() + "<br />";
		}
		test.log(LogStatus.FAIL,
				LogStatus.FAIL.toString().toUpperCase() + "ED : " + traces);
		extent.endTest(test);
	}

	@Override
	public synchronized void onTestSkipped(ITestResult result) {
		test.log(LogStatus.SKIP,
				LogStatus.SKIP.toString().toUpperCase() + "ED");
		extent.endTest(test);
	}

	@Override
	public synchronized void onTestFailedButWithinSuccessPercentage(
			ITestResult result) {
		// TODO Auto-generated method stub
	}

	@Override
	public synchronized void onStart(ITestContext context) {
		extent = new ExtentReports(System.getProperty("user.dir")
				+ File.separator + "test-output" + File.separator
				+ context.getSuite().getName() + ".html", true);
	}

	@Override
	public synchronized void onFinish(ITestContext context) {
		extent.flush();
		extent.close();
	}

}
