package com.swiggy.automation.rms.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ContactUsPage extends RestTestHelper {

	public static final Logger log = Logger.getLogger(ContactUsPage.class);

	@DataProvider(name = "RMS_CONTACUS")
	public static Object[][] getCategory() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CONTACUS");

	}

	@Test(priority = 17, dataProvider = "RMS_CONTACUS")

	public void getCategoryReasons(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,null, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Fetched Category reasons" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Fetched Category reasons" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_CONTACUS_RAISETICKET")
	public static Object[][] raiseTicket() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CONTACUS_RAISETICKET");
	}

	@Test(priority = 18, dataProvider = "RMS_CONTACUS_RAISETICKET")

	public void contactRaiseTicket(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Freshdesk Ticket Created Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Freshdesk Ticket Created Successfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
