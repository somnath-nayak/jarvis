package com.swiggy.automation.rms.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class GstDetails extends RestTestHelper {

	@DataProvider(name = "GST_DETAILS")
	public static Object[][] getGSTDetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GST_DETAILS");
	}

	@Test(priority = 4, dataProvider = "GST_DETAILS")

	public void getDetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved GST Details succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved GST Details succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GST_POPUP_NOTIFICATIONS")
	public static Object[][] getPOPNotifications()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GST_POPUP_NOTIFICATIONS");
	}

	@Test(priority = 5, dataProvider = "GST_POPUP_NOTIFICATIONS")

	public void getPOP(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved GST Notification succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved GST Notification succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GST_STATIC_INFO")
	public static Object[][] getStaticInfo()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GST_STATIC_INFO");
	}

	@Test(priority = 6, dataProvider = "GST_STATIC_INFO")

	public void getStatic(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved GST Info succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved GST Info succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "SUBMIT_GST_DETAILS")
	public static Object[][] submitGSTDetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SUBMIT_GST_DETAILS");
	}

	@Test(priority = 7, dataProvider = "SUBMIT_GST_DETAILS")

	public void SubmitGST(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Submitted GST Details succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Submitted GST Details succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "SUBMIT_NON_GST_DETAILS")
	public static Object[][] submitNonGSTDetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SUBMIT_NON_GST_DETAILS");
	}

	@Test(priority = 8, dataProvider = "SUBMIT_NON_GST_DETAILS")

	public void submitNonGST(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Submitted Non-GST Details succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Submitted Non-GST Details succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}
}
