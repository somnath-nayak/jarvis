package com.swiggy.automation.rms.api.sanity;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AuthLogin extends RestTestHelper {

	public static final Logger log = Logger.getLogger(AuthLogin.class);

	@DataProvider(name = "RMS_LOGIN")
	public static Object[][] loginRMS() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_LOGIN");
	}

	@Test(priority = 1, dataProvider = "RMS_LOGIN")

	public void logintoRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged In Successful" + smokeTest);
			defaultTOKEN = response.get("data").get("access_token").asText()
					.toString();
			log.info("Token For the Session  " + defaultTOKEN);

			cookie = new HashMap<String, Object>();
			cookie.put("Cookie", "Swiggy_Session-alpha=" + defaultTOKEN);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged In Successful" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();

		}

	}

	@DataProvider(name = "RMS_CHNGPWD")
	public static Object[][] changepwd() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CHNGPWD");
	}

	@Test(priority = 2, dataProvider = "RMS_CHNGPWD")

	public void changePassword(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Password Changed Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Password Changed Successfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_GETUSERINFO")
	public static Object[][] getUserInfo() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_GETUSERINFO");

	}

	@Test(priority = 3, dataProvider = "RMS_GETUSERINFO")

	public void getUserInformation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,null, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"User details fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User details fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_LOGOUT")
	public static Object[][] logout() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_LOGOUT");
	}

	@Test(priority = 60, dataProvider = "RMS_LOGOUT")

	public void logoutRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged Out Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged Out Successfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
