package com.swiggy.automation.rms.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class FindPartnerTypeDecisionEng extends RestTestHelper {

	public static final Logger log = Logger
			.getLogger(FindPartnerTypeDecisionEng.class);

	@DataProvider(name = "RMS_DECISIONENGINE")
	public static Object[][] partnerType() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_DECISIONENGINE");

	}

	@Test(priority = 8, dataProvider = "RMS_DECISIONENGINE")

	public void findPartnerType(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Fetched Partner Type succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Fetched Partner Type succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
