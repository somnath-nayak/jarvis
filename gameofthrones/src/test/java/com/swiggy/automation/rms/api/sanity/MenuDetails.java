package com.swiggy.automation.rms.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class MenuDetails extends RestTestHelper {

	public static final Logger log = Logger.getLogger(MenuDetails.class);

	@DataProvider(name = "RMS_FETCHITEMS")
	public static Object[][] fetchItems() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_FETCHITEMS");

	}

	@Test(priority = 20, dataProvider = "RMS_FETCHITEMS")

	public void getItems(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Items fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Items fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM")
	public static Object[][] markItemOOS() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM");

	}

	@Test(priority = 21, dataProvider = "RMS_TOGGLEITEM")

	public void menuMarkItemOOS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Item OOS succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Item OOS succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM3")
	public static Object[][] markItemInStock()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM3");

	}

	@Test(priority = 32, dataProvider = "RMS_TOGGLEITEM3")

	public void menuMarkItemInStock(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		JsonNode response = null;
		int counter = 0;
		try {

			while (counter <= 20) {
				Thread.sleep(30000);

				response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
						jsonRequestData, cookie);
				counter++;

				if (response.get("statusCode").asText().equalsIgnoreCase("0"))
					break;
			}

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEVARIANTS")
	public static Object[][] markVariantOOS()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEVARIANTS");

	}

	@Test(priority = 22, dataProvider = "RMS_TOGGLEVARIANTS")

	public void menuMarkVariantOOS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
