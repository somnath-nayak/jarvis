package com.swiggy.automation.rms.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class HealthChecker extends RestTestHelper {

	public static final Logger log = Logger.getLogger(HealthChecker.class);

	@DataProvider(name = "RMS_HEALTHCHECK")
	public static Object[][] healthCheck() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_HEALTHCHECK");

	}

	@Test(priority = 29, dataProvider = "RMS_HEALTHCHECK")

	public void healthChecker(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Health check succesfull" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
