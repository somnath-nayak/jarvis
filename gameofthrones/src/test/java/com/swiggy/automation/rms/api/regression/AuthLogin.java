package com.swiggy.automation.rms.api.regression;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class AuthLogin extends RestTestHelper {

	public static final Logger log = Logger.getLogger(AuthLogin.class);
	HashMap<String, Object> cookie1 = new HashMap<String, Object>();

	@DataProvider(name = "RMS_LOGIN")
	public static Object[][] loginRMS() throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("RMS_LOGIN", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "RMS_LOGIN")

	public void logintoRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged In Successful" + smokeTest);

			defaultTOKEN = response.get("data").get("access_token").asText()
					.toString();
			log.info("Token For the Session " + defaultTOKEN);

			cookie = new HashMap<String, Object>();
			cookie.put("Cookie", "Swiggy_Session-alpha=" + defaultTOKEN);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged In Successful" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();

		}

	}

	@DataProvider(name = "RMS_LOGIN1")
	public static Object[][] invalidLoginRMS()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("RMS_LOGIN1");
	}

	@Test(priority = 2, dataProvider = "RMS_LOGIN1")

	public void InvalidLogintoRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged In UnSuccessful" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged In UnSuccessful" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();

		}

	}

	@DataProvider(name = "RMS_CHNGPWD")
	public static Object[][] changepwd() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CHNGPWD");
	}

	@Test(priority = 3, dataProvider = "RMS_CHNGPWD")

	public void changePassword(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Password Changed Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Password Changed Successfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_GETUSERINFO")
	public static Object[][] getUserInfo() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_GETUSERINFO");

	}

	@Test(priority = 4, dataProvider = "RMS_GETUSERINFO")

	public void getUserInformation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,null, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"User details fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User details fetched succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_LOGOUT")
	public static Object[][] logout() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_LOGOUT");
	}

	@Test(priority = 25, dataProvider = "RMS_LOGOUT")

	public void logoutRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged Out Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged Out Successfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_LOGIN2")
	public static Object[][] loginLogoutRMS()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("RMS_LOGIN2");
	}

	@Test(priority = 26, dataProvider = "RMS_LOGIN2")

	public void loginlogouttoRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged In Successful" + smokeTest);

			defaultTOKEN = response.get("data").get("access_token").asText()
					.toString();
			log.info("Token For the Session " + defaultTOKEN);

			cookie1 = new HashMap<String, Object>();
			cookie1.put("Cookie", "Swiggy_Session-alpha=" + defaultTOKEN);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged In Successful" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();

		}

	}

	@DataProvider(name = "RMS_LOGOUT1")
	public static Object[][] invalidLogout()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_LOGOUT1");
	}

	@Test(priority = 27, dataProvider = "RMS_LOGOUT")

	public void invalidLogoutRMS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie1);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Logged Out Successfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Logged Out Successfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}
}
