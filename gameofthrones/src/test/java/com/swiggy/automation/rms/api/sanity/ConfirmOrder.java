package com.swiggy.automation.rms.api.sanity;

import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ConfirmOrder extends RestTestHelper {

	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = "fulfillment.swiggy.in";
	String backend_host = "api.swiggy.in";
	Integer backend_port = 80;
	Integer oms_port = 80;

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 11, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"), apiToExecute, jsonRequestData, "");
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "CREATE_CART_SANITY")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CART_SANITY");
	}

	@Test(priority = 12, dataProvider = "CREATE_CART_SANITY")
	public void testCreateCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("rms"), apiToExecute, jsonRequestData, defaulTID,
					defaultTOKEN);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testCreateCartAPI" + smokeTest);

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ALLADDRESS_SANITY")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALLADDRESS_SANITY");
	}

	@Test(priority = 14, dataProvider = "GET_ALLADDRESS_SANITY")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("rms"), apiToExecute, jsonRequestData, defaulTID,
					defaultTOKEN);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI");
			Thread.sleep(5000);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PLACE_ORDER_SANITY")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PLACE_ORDER_SANITY");
	}

	@Test(priority = 15, dataProvider = "PLACE_ORDER_SANITY")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			System.out.println(jsonRequestData);
			System.out.println(updatedRequest);
			JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("rms"), apiToExecute, updatedRequest, defaulTID,
					defaultTOKEN);
			log.info("****************************************************");

			orderId = placeOrderResponse.get("data").get("order_id").asLong();
			orderIds.add(orderId);
			orderKey = placeOrderResponse.get("data").get("key").asText()
					.toString();
			System.out.println("ORDER ID :::" + orderId);
			System.out.println("ORDER KEY :::" + orderKey);

			orderKey = placeOrderResponse.get("data").get("key").asText();
			System.out.println("ORDER KEY :::" + orderKey);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_CONFIRMORDER")
	public static Object[][] confirmOrder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CONFIRMORDER");
	}

	@Test(priority = 16, dataProvider = "RMS_CONFIRMORDER")

	public void confirm(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "orderId", orderId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Successfully confirmed the Order" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Successfully confirmed the Order" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_ORDER_REQCALLBACK")
	public static Object[][] reqCallBack() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_ORDER_REQCALLBACK");
	}

	@Test(priority = 17, dataProvider = "RMS_ORDER_REQCALLBACK")

	public void reqCall(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "orderId", orderId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Successfully requested for Call back" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Successfully requested for Call back" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_MARKORDEROOS")
	public static Object[][] orderMarkOOS()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_MARKORDEROOS");
	}

	@Test(priority = 18, dataProvider = "RMS_MARKORDEROOS")

	public void markOOS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "orderId", orderId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Successfully marked item out of stock" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Successfully marked item out of stock" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM2")
	public static Object[][] markItemInStock()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM2");

	}

	@Test(priority = 32, dataProvider = "RMS_TOGGLEITEM2")

	public void menuMarkItemInStock(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		JsonNode response = null;
		int counter = 0;
		try {

			while (counter <= 20) {
				Thread.sleep(30000);

				response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
						jsonRequestData, cookie);
				counter++;

				if (response.get("statusCode").asText().equalsIgnoreCase("0"))
					break;
			}

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@SuppressWarnings("rawtypes")
	@AfterTest()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();

		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		if (orderIds.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : orderIds) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
					oms_osrftoken);
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedResponseData, actualData,
					new String[] { "status", "message" });
			Thread.sleep(2000);
		}

	}

}
