package com.swiggy.automation.rms.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class GST extends RestTestHelper {

	@DataProvider(name = "GST_DETAILS")
	public static Object[][] getGSTDetailsWithInvalidRestId()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GST_DETAILS");
	}

	@Test(priority = 2, dataProvider = "GST_DETAILS")

	public void getDetailsWithInvalidRestId(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"GST details are not shown with invalid Restaurant ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"GST details are not shown with invalid Restaurant ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "SUBMIT_GST_DETAILS")
	public static Object[][] submitGSTDetailsWithInvalidRestId()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SUBMIT_GST_DETAILS");
	}

	@Test(priority = 5, dataProvider = "SUBMIT_GST_DETAILS")

	public void SubmitGSTWithInvalidRestId(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"GST Details are not sbmitted for invalid Restaurant ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"GST Details are not sbmitted for invalid Restaurant ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "SUBMIT_NON_GST_DETAILS")
	public static Object[][] submitNonGSTDetailsWithInvalidRestId()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SUBMIT_NON_GST_DETAILS");
	}

	@Test(priority = 6, dataProvider = "SUBMIT_NON_GST_DETAILS")

	public void submitNonGSTWithInvalidRestId(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Non-GST Details are not sbmitted for invalid Restaurant ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Non-GST Details are not sbmitted for invalid Restaurant ID"
							+ sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}