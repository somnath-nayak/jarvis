package com.swiggy.automation.rms.api.sanity;

import java.util.HashMap;
import java.util.Random;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RestaurantInfo extends RestTestHelper {

	Random ran = new Random();
	// HashMap<String, Object> cookies = new HashMap<>();
	int mobile_no = 0;
	HashMap<String, Object> update = new HashMap<>();
	String mobile_nos = "000" + GenerateMobileNo();

	int GenerateMobileNo() {
		return ran.nextInt(9000000) + 1000000;

	}

	@DataProvider(name = "RMS_RESTINFO")
	public static Object[][] restoInfo() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_RESTINFO");

	}

	@Test(priority = 12, dataProvider = "RMS_RESTINFO")

	public void getRestaurantInfo(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Resto Info retrieved succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Resto Info retrieved succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	public static final Logger log = Logger.getLogger(RestaurantInfo.class);

	@DataProvider(name = "RMS_CREATEUSERS")
	public static Object[][] createUser() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CREATEUSERS");

	}

	@Test(priority = 13, dataProvider = "RMS_CREATEUSERS")

	public void createUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			// update.put("phone_no", mobile_no);
			System.out.println("mb number" + mobile_nos);
			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "phone_no", mobile_nos);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updaterequest, cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User Created succesfully" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User Created succesfully" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "RMS_UPDATEUSERS")
	public static Object[][] updateUser() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_UPDATEUSERS");

	}

	@Test(priority = 14, dataProvider = "RMS_UPDATEUSERS")

	public void updateUserInfo(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "phone_no", mobile_nos);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updaterequest, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"User Updated succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User Updated succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_DISABLEUSERS")
	public static Object[][] disableUser() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_DISABLEUSERS");

	}

	@Test(priority = 15, dataProvider = "RMS_DISABLEUSERS")

	public void disableUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "phone_no", mobile_nos);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updaterequest, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"User Disabled succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"User Disabled succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_GETUSERPROFILE")
	public static Object[][] userUrofile() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_GETUSERPROFILE");

	}

	@Test(priority = 16, dataProvider = "RMS_GETUSERPROFILE")

	public void getUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		JsonNode expectedValue = null;
		// JsonNode expectedValue1 = null;
		try {

			System.out.println("expected" + expectedValue);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			expectedValue = expectedData.get("json1");

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedValue,
					response);
			Assert.assertTrue(isSuccess,
					"User Info retrieved succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}
}
