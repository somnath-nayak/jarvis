package com.swiggy.automation.rms.api.regression;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BusinessMetrics extends RestTestHelper {

	public static final Logger log = Logger.getLogger(BusinessMetrics.class);

	@DataProvider(name = "RMS_OVERVIEW")
	public static Object[][] overview() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_OVERVIEW");

	}

	@Test(priority = 21, dataProvider = "RMS_OVERVIEW")

	public void getOverview(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Overview results succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Overview results succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_REVENUE")
	public static Object[][] revenue() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_REVENUE");

	}

	@Test(priority = 22, dataProvider = "RMS_REVENUE")

	public void getRevenue(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date randomDate = new Date();
			c.setTime(randomDate);
			c.add(Calendar.YEAR, 0);
			String startDate = dateFormat.format(c.getTime());
			System.out.println(startDate);

			c.add(Calendar.DAY_OF_WEEK, 5);
			String endDate = dateFormat.format(c.getTime());
			System.out.println(endDate);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "startDate", startDate);
			JsonNode updaterequest1 = RestTestUtil.updateRequestDataBeforeSend(
					updaterequest, "endDate", endDate);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updaterequest1, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved revenue results succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved revenue results succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_OPERATION")
	public static Object[][] operation() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_OPERATION");

	}

	@Test(priority = 23, dataProvider = "RMS_OPERATION")

	public void getOperation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date randomDate = new Date();
			c.setTime(randomDate);
			c.add(Calendar.YEAR, 0);
			String startDate = dateFormat.format(c.getTime());
			System.out.println(startDate);

			c.add(Calendar.DAY_OF_WEEK, 5);
			String endDate = dateFormat.format(c.getTime());
			System.out.println(endDate);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "startDate", startDate);
			JsonNode updaterequest1 = RestTestUtil.updateRequestDataBeforeSend(
					updaterequest, "endDate", endDate);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updaterequest1, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Operations results succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Operations results succesfully"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_CUSTRATINGS")
	public static Object[][] custratings() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CUSTRATINGS");

	}

	@Test(priority = 24, dataProvider = "RMS_CUSTRATINGS")

	public void getCustomerRatings(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date randomDate = new Date();
			c.setTime(randomDate);
			c.add(Calendar.YEAR, 0);
			String startDate = dateFormat.format(c.getTime());
			System.out.println(startDate);

			c.add(Calendar.DAY_OF_WEEK, 5);
			String endDate = dateFormat.format(c.getTime());
			System.out.println(endDate);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "startDate", startDate);
			JsonNode updaterequest1 = RestTestUtil.updateRequestDataBeforeSend(
					updaterequest, "endDate", endDate);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updaterequest1, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Customer Ratings results succesfully"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Customer Ratings results succesfully"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
