package com.swiggy.automation.rms.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RestaurantTier extends RestTestHelper {

	@DataProvider(name = "ALL_TIERS_INFO")
	public static Object[][] getBenchmarksBenifitswithInvalidData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ALL_TIERS_INFO");
	}

	@Test(priority = 2, dataProvider = "ALL_TIERS_INFO")

	public void getBenifitswithInvalidData(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Tier Info is not displayed with invalid input"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Tier Info is not displayed with invalid input"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "REST_METRICS")
	public static Object[][] getRestMetricsWithInvalidData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("REST_METRICS");
	}

	@Test(priority = 3, dataProvider = "REST_METRICS")

	public void getMetricsWithInvalidData(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Rest Metrics is not shown with invalid input" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Rest Metrics is not shown with invalid input"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
