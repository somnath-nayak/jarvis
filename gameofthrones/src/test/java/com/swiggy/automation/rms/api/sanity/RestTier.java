package com.swiggy.automation.rms.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RestTier extends RestTestHelper {

	@DataProvider(name = "ALL_TIERS_INFO")
	public static Object[][] getBenchmarksBenifits()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ALL_TIERS_INFO");
	}

	@Test(priority = 2, dataProvider = "ALL_TIERS_INFO")

	public void getBenifits(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved All Tier Info succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved All Tier Info succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "REST_METRICS")
	public static Object[][] getRestMetrics()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("REST_METRICS");
	}

	@Test(priority = 3, dataProvider = "REST_METRICS")

	public void getMetrics(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Rest Metrics succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Rest Metrics succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
