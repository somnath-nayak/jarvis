package com.swiggy.automation.rms.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OrderFlows extends RestTestHelper {

	public static final Logger log = Logger.getLogger(OrderFlows.class);

	@DataProvider(name = "RMS_FETCHORDERS")
	public static Object[][] fetchOrders() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_FETCHORDERS");
	}

	@Test(priority = 4, dataProvider = "RMS_FETCHORDERS")

	public void fetchOrdersList(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Successfully fetched the Orders" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Successfully fetched the Orders" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_OLDORDERHISTORY")
	public static Object[][] oldOrderHistory()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_OLDORDERHISTORY");

	}

	@Test(priority = 5, dataProvider = "RMS_OLDORDERHISTORY")

	public void getOldOrderHistory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Old Order History fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Old Order History fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_ORDERHISTORY")
	public static Object[][] newOrderHistory()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_ORDERHISTORY");

	}

	@Test(priority = 6, dataProvider = "RMS_ORDERHISTORY")

	public void getNewOrderHistory(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Order History fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Order History fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_ORDERHISTCSV")
	public static Object[][] orderHistoryCSV()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_ORDERHISTCSV");

	}

	@Test(priority = 7, dataProvider = "RMS_ORDERHISTCSV")

	public void getOrderHistoryCSV(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			// Thread.sleep(10000);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "CSV fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"CSV fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_MULTICHAINORDERS")
	public static Object[][] multiRestOrders()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_MULTICHAINORDERS");

	}

	@Test(priority = 8, dataProvider = "RMS_MULTICHAINORDERS")

	public void getMultiChainRestOrders(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Multi Chain restaurant orders fetched succesfully"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Multi Chain restaurant orders fetched succesfully"
							+ sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
