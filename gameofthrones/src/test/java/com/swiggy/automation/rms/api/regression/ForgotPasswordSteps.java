package com.swiggy.automation.rms.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ForgotPasswordSteps extends RestTestHelper {

	public static final Logger log = Logger
			.getLogger(ForgotPasswordSteps.class);

	@DataProvider(name = "RMS_FGTPWD0")
	public static Object[][] forgotpwd() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetDataWithAPIFields("RMS_FGTPWD0");
	}

	@Test(priority = 1, dataProvider = "RMS_FGTPWD0")

	public void forgotPasswordZero(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "OTP Sent Successfully" + smokeTest);
			defaultTOKEN = response.get("data").get("sessionKey").asText()
					.toString();
			System.out.println("Token For the Session  " + defaultTOKEN);
			//
			// isSuccess =
			// JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
			// response, apiFieldsData);
			// Assert.assertTrue(isSuccess,
			// "OTP Sent Successfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();

		}
	}

}
