package com.swiggy.automation.rms.api.regression;

import org.codehaus.jackson.JsonNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class TradeDiscount extends RestTestHelper {
	long tdId = 0;

	@DataProvider(name = "CREATE_TD_REGRESSION")
	public static Object[][] createWithInvalidInputData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_TD_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 8, dataProvider = "CREATE_TD_REGRESSION")
	public void createWithInvalidData(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode res = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute, jsonRequestData,
					cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess,
					"TD not created with invalid input" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess,
					"TD not created with invalid input" + regressionTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "CREATE_TD_REGRESSION1")
	public static Object[][] createTradeDIscount()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_TD_REGRESSION1",
				"REGRESSION");
	}

	@Test(priority = 9, dataProvider = "CREATE_TD_REGRESSION1")
	public void createTD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode res = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute, jsonRequestData,
					cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess, "TD created successfully" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess,
					"TD created successfully" + regressionTest);
			JSONObject dicountId;
			dicountId = new JSONObject(res.toString());

			tdId = Integer.parseInt(dicountId.getJSONObject("data")
					.getJSONObject("dataRest").get("data").toString());

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "CREATE_TD_REGRESSION2")
	public static Object[][] createMutipleTDWithSameTimeSlot()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_TD_REGRESSION2",
				"REGRESSION");
	}

	@Test(priority = 10, dataProvider = "CREATE_TD_REGRESSION2")
	public void createMutipleTDWithSameTime(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode res = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute, jsonRequestData,
					cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess,
					"Multiple TD's are not created for time slot" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess,
					"Multiple TD's are not created for time slot"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "EDIT_TD_REGRESSION")
	public static Object[][] editTradeDiscountWithInvalidRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("EDIT_TD_REGRESSION");
	}

	@Test(priority = 11, dataProvider = "EDIT_TD_REGRESSION")

	public void editTradeDiscountWithInvalidRest(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode updaterequest = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "id", tdId);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updaterequest, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"TD cannot be Edited with invalid data" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"TD cannot be Edited with invalid data" + regressionTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_BY_REST_ID")
	public static Object[][] getDiscountByInvalidRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_BY_REST_ID");
	}

	@Test(priority = 12, dataProvider = "DISCOUNT_BY_REST_ID")

	public void getByInvalidRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid rest ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid rest ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_DETAILS")
	public static Object[][] getDiscByValidRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_DETAILS");
	}

	@Test(priority = 13, dataProvider = "DISCOUNT_DETAILS")

	public void getByValRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "discount_id", tdId);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_DETAILS2")
	public static Object[][] getDiscByInvalidRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_DETAILS2");
	}

	@Test(priority = 14, dataProvider = "DISCOUNT_DETAILS2")

	public void getByInvalRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "discount_id", tdId);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_DETAILS1")
	public static Object[][] getDiscByInvalidDiscID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_DETAILS1");
	}

	@Test(priority = 15, dataProvider = "DISCOUNT_DETAILS1")

	public void getByInvalDiscID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Discount details are not displayed with invalid Rest ID"
							+ regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
