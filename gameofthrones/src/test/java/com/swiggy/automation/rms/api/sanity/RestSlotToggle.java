package com.swiggy.automation.rms.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RestSlotToggle extends RestTestHelper {

	public static final Logger log = Logger.getLogger(RestSlotToggle.class);

	@DataProvider(name = "RMS_RESTSLOT_TOGGLE")
	public static Object[][] toggleRestoSlot()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_RESTSLOT_TOGGLE");

	}

	@Test(priority = 20, dataProvider = "RMS_RESTSLOT_TOGGLE")

	public void toggleRestaurantSlot(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			// isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
			// response);
			//
			// Assert.assertTrue(isSuccess,
			// "Restaurant Slot Toggle is succesfull" + smokeTest);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response,
					new String[] { "isOpen", "restaurantId" });
			Assert.assertTrue(isSuccess,
					"Restaurant Slot Toggle is succesfull" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
