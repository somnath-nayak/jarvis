package com.swiggy.automation.rms.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jettison.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class TradeDiscount extends RestTestHelper {
	long tdId = 0;
	// protected static HashMap<String, Object> td;

	@DataProvider(name = "CREATE_TD")
	public static Object[][] createTD() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_TD");
	}

	@Test(priority = 10, dataProvider = "CREATE_TD")

	public void create(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Created TD succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "Created TD succesfully" + sanityTest);
			JSONObject dicountId;
			dicountId = new JSONObject(response.toString());

			tdId = Integer.parseInt(dicountId.getJSONObject("data")
					.getJSONObject("dataRest").get("data").toString());
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_DETAILS")
	public static Object[][] getDiscountDetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_DETAILS");
	}

	@Test(priority = 11, dataProvider = "DISCOUNT_DETAILS")

	public void getDicount(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode updatedReqData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "discount_id", tdId);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					updatedReqData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Discount details succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Discount details succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNTS_BY_REST_ID")
	public static Object[][] getDiscountByRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNTS_BY_REST_ID");
	}

	@Test(priority = 12, dataProvider = "DISCOUNTS_BY_REST_ID")

	public void getByRestID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Discount details of restaurant succesfully"
							+ smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Discount details of restaurant succesfully"
							+ sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "EDIT_TD")
	public static Object[][] editTradeDiscount()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("EDIT_TD");
	}

	@Test(priority = 13, dataProvider = "EDIT_TD")

	public void editDiscount(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode updaterequest = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "id", tdId);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updaterequest, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Edited TD succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "Edited TD succesfully" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DISCOUNT_ATTRIBUTES")
	public static Object[][] getDiscountAttributes()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DISCOUNT_ATTRIBUTES");
	}

	@Test(priority = 14, dataProvider = "DISCOUNT_ATTRIBUTES")

	public void getAttributes(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Retrieved Meta Data succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Retrieved Meta Data succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}
}
