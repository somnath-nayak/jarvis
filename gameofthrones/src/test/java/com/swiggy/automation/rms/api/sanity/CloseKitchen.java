package com.swiggy.automation.rms.api.sanity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CloseKitchen extends RestTestHelper {

	long SlotId = 0;
	public static final Logger log = Logger.getLogger(CloseKitchen.class);

	@DataProvider(name = "RMS_CREATE_RESTHOLDSLOTS")
	public static Object[][] createRestoHolidaySlot()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_CREATE_RESTHOLDSLOTS");

	}

	@Test(priority = 9, dataProvider = "RMS_CREATE_RESTHOLDSLOTS")

	public void createRestaurantHolidaySlot(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Calendar c = Calendar.getInstance();
			Date randomDate = new Date();
			c.setTime(randomDate);
			c.add(Calendar.YEAR, 1);
			String fromTime = dateFormat.format(c.getTime());
			System.out.println(fromTime);

			c.add(Calendar.MINUTE, 5);
			String toTime = dateFormat.format(c.getTime());
			System.out.println(toTime);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "fromTime", fromTime);
			JsonNode updaterequest1 = RestTestUtil.updateRequestDataBeforeSend(
					updaterequest, "toTime", toTime);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updaterequest1, cookie);

			SlotId = response.get("data").get("id").asInt();

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Created restaurant holiday slot succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Created restaurant holiday slot succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_FETCH_RESTHOLDSLOTS")
	public static Object[][] restoHolidaySlot()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_FETCH_RESTHOLDSLOTS");

	}

	@Test(priority = 10, dataProvider = "RMS_FETCH_RESTHOLDSLOTS")

	public void getRestaurantHolidaySlot(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			System.out.println(jsonRequestData);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			System.out.println(response);
			Assert.assertTrue(isSuccess,
					"Fetched Restaurant holiday slots succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Fetched Restaurant holiday slots succesfully"
							+ sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_DELETE_RESTHOLDSLOTS")
	public static Object[][] deleteRestoHolidaySlot()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_DELETE_RESTHOLDSLOTS");

	}

	@Test(priority = 11, dataProvider = "RMS_DELETE_RESTHOLDSLOTS")

	public void deleteRestaurantHolidaySlot(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "slotId", SlotId);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("rms"),apiToExecute,
					updatedrequest, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Deleted Restaurant Holiday slot succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Deleted Restaurant Holiday slot succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
