package com.swiggy.automation.rms.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class MenuDetails extends RestTestHelper {

	public static final Logger log = Logger.getLogger(MenuDetails.class);

	@DataProvider(name = "RMS_FETCHITEMS")
	public static Object[][] fetchItems() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_FETCHITEMS");

	}

	@Test(priority = 4, dataProvider = "RMS_FETCHITEMS")

	public void getItems(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Items fetched succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Items fetched succesfully" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM")
	public static Object[][] markItemOOSWithInvalidInput()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM");

	}

	@Test(priority = 5, dataProvider = "RMS_TOGGLEITEM")

	public void menuMarkItemOOSWithInvalidInput(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "Item is not marked OOS" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Item is not marked OOS" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM1")
	public static Object[][] markItemOOS() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM1");

	}

	@Test(priority = 7, dataProvider = "RMS_TOGGLEITEM1")

	public void menuMarkItemOOS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Item OOS succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Item OOS succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEITEM2")
	public static Object[][] markItemInStock()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEITEM2");

	}

	@Test(priority = 60, dataProvider = "RMS_TOGGLEITEM2")

	public void menuMarkItemInStock(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		JsonNode response = null;
		int counter = 0;
		try {

			while (counter <= 20) {
				Thread.sleep(30000);

				response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
						jsonRequestData, cookie);
				counter++;

				if (response.get("statusCode").asText().equalsIgnoreCase("0"))
					break;
			}

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Item InStock succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEVARIANTS1")
	public static Object[][] markVariantOOSWithInvalidRestID()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEVARIANTS1");

	}

	@Test(priority = 6, dataProvider = "RMS_TOGGLEVARIANTS1")

	public void menuMarkVariantOOSWithInvalidRestID(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "RMS_TOGGLEVARIANTS")
	public static Object[][] markVariantOOS()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RMS_TOGGLEVARIANTS");

	}

	@Test(priority = 7, dataProvider = "RMS_TOGGLEVARIANTS")

	public void menuMarkVariantOOS(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("rms"),apiToExecute,
					jsonRequestData, cookie);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess,
					"Marked Variant OOS succesfully" + regressionTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
