package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEPolygonRegression extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEPolygonRegression.class);

	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@DataProvider(name = "GetPolygonById")
	public static Object[][] getPolygonById()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETPOLYGON", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "GetPolygonById", groups = "REGRESSION", enabled = true)
	public void testGetPolygonById(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonById" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonById" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetPolygon")
	public static Object[][] getPolygon() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETpolygonByLat", "REGRESSION");
	}

	/**
	 * GET: Method to get Polygon Details from Lat & Lan
	 * 
	 * API @Param Latitiude & Longitude
	 */
	@Test(priority = 1, dataProvider = "GetPolygon", groups = "REGRESSION", enabled = true)
	public void testGetPolygon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygon" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygon" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetPolygonTag")
	public static Object[][] getPolygonTag()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PolygonTag", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "GetPolygonTag", groups = "REGRESSION", enabled = true)
	public void testGetPolygonTag(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonTag" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonTag" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

}
