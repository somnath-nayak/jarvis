package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.*;  
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DECartReservation extends RestTestHelper {
	public final static Logger logger = Logger
			.getLogger(DEControllerRegression.class);

	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;
	public static String delivery_Controller_host = null;
	public static Integer delivery_Controller_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_Controller_host = getEndPoint(
//					"delivery_controller_host");
//			delivery_Controller_port = Integer
//					.parseInt(getEndPoint("delivery_controller_port"));
//
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "TOKENCREATE")
	public static Object[][] getControllerTrack()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TOKENCREATE");
	}

	@Test(priority = 0, dataProvider = "TOKENCREATE", enabled = true)
	public void createCart(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(
					serviceNameMap.get("delivery"),
					apiToExecute, jsonRequestData, deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "checkControllerTrack");

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "checkControllerTrack");

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
