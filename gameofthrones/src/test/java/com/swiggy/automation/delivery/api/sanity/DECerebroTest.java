package com.swiggy.automation.delivery.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DECerebroTest extends RestTestHelper {

	private final static Logger log = Logger.getLogger(DECerebroTest.class);

	private static String delivery_cerebro_host = null;
	private static Integer delivery_cerebro_port = null;
	private static String Backend_host = null;
	private static Integer Backend_port = null;
	private static String stopDtid = null;

	@BeforeClass
	public void beforeClass() {
		try {
			delivery_cerebro_host = STAFAgent.getSTAFValue(
					"delivery_cerebro_host");
			delivery_cerebro_port = Integer
					.parseInt( STAFAgent.getSTAFValue("delivery_cerebro_port"));

			Backend_host =  STAFAgent.getSTAFValue("backend_host");
			Backend_port = Integer
					.parseInt( STAFAgent.getSTAFValue("backend_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@DataProvider(name = "DEListings")
	public static Object[][] getDEListings()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELISTINGS");
	}

	/**
	 * Method gets the Listings for given lat/long
	 * 
	 * API @Param SessionId
	 */
	@Test(priority = 1, groups = "Sanity", dataProvider = "DEListings", enabled = true)
	public void testGetDEListings(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(
				serviceNameMap.get("delivery_cerebro"), apiToExecute,
				jsonRequestData, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedData, response,
				new String[] { "blackZoneResponse", "serviceableRestaurants" });

		Assert.assertTrue(success, "testGetDEListings" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEListings" + sanityTest);

	}

	@DataProvider(name = "DEServicability")
	public static Object[][] getDEServicability()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DESERVICABILITY");
	}

	/**
	 * Method to get the Listings without Servicability Checks for given
	 * lat/long
	 * 
	 * API @Param SessionId
	 */
	@Test(priority = 2, groups = "Sanity", dataProvider = "DEServicability", enabled = true)
	public void testGetDEServicability(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(
				serviceNameMap.get("delivery_cerebro"), apiToExecute,
				jsonRequestData, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedData, response,
				new String[] { "blackZoneResponse", "serviceableRestaurants" });

		Assert.assertTrue(success, "testGetDEServicability" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEServicability" + sanityTest);

	}

	@DataProvider(name = "CART_SLA")
	public static Object[][] getDECartSla()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CARSLA");
	}

	/**
	 * Method to get the Listings without Servicability Checks for given
	 * lat/long
	 * 
	 * API @Param SessionId
	 */
	@Test(priority = 3, groups = "Sanity", dataProvider = "CART_SLA", enabled = true)
	public void testGetDECartSla(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery_cerebro"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDECartSla" + sanityTest);

	}
}
