package com.swiggy.automation.delivery.api.regression;

import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEExotelRegression extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEExotelRegression.class);

	private static HashMap<String, String> exotelMap = new HashMap<>();
	private static String delivery_host = null;
	private static Integer delivery_port = null;

	@BeforeClass
	public void beforeClass() {
		exotelMap.put("CONFIRMED", "08033545109");
		exotelMap.put("ARRIVED", "08033013122");
		exotelMap.put("PICKEDUP", "08033545112");
		exotelMap.put("DELIVERED", "08030752906");
		exotelMap.put("REJECTED", "08039510686");
		exotelMap.put("CALL_CUSTOMER_KEY", "08039513466");
		exotelMap.put("CALL_ME_KEY", "08039513064");
		exotelMap.put("REACHED", "08039658618");
		exotelMap.put("confirmHubTransaction", "");
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage")) {
			log.info("Skipping Tests Since this is not a Staging Environment");
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
		}
	}

	@DataProvider(name = "EXOTEL-CONFIRMED")
	public static Object[][] getExotelConfirmed()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Confirmed", "REGRESSION");
	}

	/**
	 * Method to Mark Order Confirmed from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 0, dataProvider = "EXOTEL-CONFIRMED", groups = "REGRESSION", enabled = true)
	public void testExotelConfirmed(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelConfirmed" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-ARRIVED")
	public static Object[][] getExotelArrived()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Arrived", "REGRESSION");
	}

	/**
	 * Method to Mark Order Arrived from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 3, dataProvider = "EXOTEL-ARRIVED", groups = "REGRESSION", enabled = true)
	public void testExotelArrived(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelArrived" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-PICKEDUP")
	public static Object[][] getExotelPickedUp()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Pickedup", "REGRESSION");
	}

	/**
	 * Method to Mark Order PickedUp from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 4, dataProvider = "EXOTEL-PICKEDUP", groups = "REGRESSION", enabled = true)
	public void testExotelPickedUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelPickedUp" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-REACHED")
	public static Object[][] getExotelReached()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Reached", "REGRESSION");
	}

	/**
	 * Method to Mark Order Reached from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 5, dataProvider = "EXOTEL-REACHED", enabled = true, groups = "REGRESSION")
	public void testExotelReached(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelReached" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-DELIVERED")
	public static Object[][] getExotelDelivered()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Delivered", "REGRESSION");
	}

	/**
	 * Method to Mark Order Delivered from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 6, dataProvider = "EXOTEL-DELIVERED", enabled = true, groups = "REGRESSION")
	public void testlExotelDelivered(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelDelivered" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-REJECTED")
	public static Object[][] getExotelRejected()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Rejected", "REGRESSION");
	}

	/**
	 * Method to Mark Order Rejected from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-REJECTED", enabled = true, groups = "REGRESSION")
	public void testlExotelRejected(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallCustomer" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-CALLME")
	public static Object[][] getExotelCallMe()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Callme", "REGRESSION");
	}

	/**
	 * Method to DE Request for Call from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-CALLME", enabled = true, groups = "REGRESSION")
	public void testlExotelCallMe(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallMe" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-CALLCUSTOMER")
	public static Object[][] getExotelCallCustomer()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-CallCustomer",
				"REGRESSION");
	}

	/**
	 * Method to Call Customer from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-CALLCUSTOMER", enabled = true, groups = "REGRESSION")
	public void testlExotelCallCustomer(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallCustomer" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-CONFIRM-HUB-TRAN")
	public static Object[][] getExotelConfirmHub()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Confirm-Hub-Transaction",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "EXOTEL-CONFIRM-HUB-TRAN", enabled = true, groups = "REGRESSION")
	public void testlExotelConfirmHub(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelConfirmHub" + smokeTest);
	}

}
