package com.swiggy.automation.delivery.api.regression;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.annotations.*;  
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEServicesRegression extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEServicesRegression.class);

	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String Backend_host = null;
	private static Integer Backend_port = null;
	private static String oms_host = null;
	private static Integer oms_port = null;
	private static Long DEOTP = null;
	private static String DESessionId = null;
	private static Long DETransactionId = null;
	private static Long ZoneCashId = null;
	

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));
//
//			deliveryAuthString = getEndPoint("delivery_authstring_username")
//					+ DELIMITTER
//					+ getEndPoint("delivery_authstring_password");

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@Test(priority = 0, enabled = true, groups = "Sanity")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			// It takes time for DE to become Free
			Thread.sleep(20000);
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "Order id is null");
			// Since Oer Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 1, enabled = false, groups = "Sanity")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("delivery"),
					deliveryAuthString);
			success = true;
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFValue("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDEList")
	public static Object[][] getdelist() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("orderdelist", "REGRESSION");
	}

	@Test(priority = 1, groups = "REGRESSION", dataProvider = "GetDEList", enabled = true)
	public void testorderdelist(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode response = null;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("pathvariable", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}
		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testorderdelist" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testorderdelist" + sanityTest);
	}

	@DataProvider(name = "GiveOrderRating")
	public static Object[][] getOrderRating()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ORDERRATING", "REGRESSION");
	}

	/**
	 * POST: Method to Rate DE based on Order
	 * 
	 * API @Param DE Id, DE Rating, Order Id
	 * 
	 */
	@Test(priority = 2, dataProvider = "GiveOrderRating", groups = "REGRESSION", enabled = true, dependsOnMethods = "placeHolder")
	public void testGetDEOrderRating(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				HashMap<String, Object> update = new HashMap<String, Object>();
				update.put("order_id", DeliveryCommon.getOrderId());
				update.put("de_id", STAFAgent.getSTAFValue("delivery_Boy_id"));

				JsonNode updaterequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updaterequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetDEOrderRating" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetDEOrderRating" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDEAreaCount")
	public static Object[][] getDEAreaCount()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("AREACOUNT", "REGRESSION");
	}

	/**
	 * GET: Method for check DE Area Count
	 * 
	 * API @Param AreaId
	 */
	@Test(priority = 2, groups = "Regression", dataProvider = "GetDEAreaCount", enabled = true)
	public void testGetDEAreaCount(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);

			Assert.assertTrue(success, "testGetDEAreaCount" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetDEAreaCount" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDELocation")
	public static Object[][] getDELocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETDELOCATION", "REGRESSION");
	}

	/**
	 * GET: Method for check DE Location
	 * 
	 * API @Param DE Id
	 */
	@Test(priority = 3, groups = "REGRESSION", dataProvider = "GetDELocation", enabled = true)
	public void testGetDELocation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDELocation" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDELocation" + sanityTest);

	}

	@DataProvider(name = "GetDEStatus")
	public static Object[][] getDEStatus() throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("Delivery_Boy_Status", "REGRESSION");
	}

	/**
	 * GET: Method for Getting DE Status
	 * 
	 * API @Param token & DE Id
	 */
	@Test(priority = 4, groups = "REGRESSION", dataProvider = "GetDEStatus", enabled = true)
	public void testGetDEStatus(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEStatus" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEStatus" + sanityTest);

	}

	@DataProvider(name = "DEBUSY")
	public static Object[][] BusyDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BUSYDE", "REGRESSION");
	}

	@Test(priority = 5, dataProvider = "DEBUSY", groups = "REGRESSION", enabled = true)
	public void testBusyDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("valid input")) {
				ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);

				Assert.assertTrue(success, "testBusyDE" + sanityTest);
			} else {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testBusyDE" + smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testBusyDE" + sanityTest);

			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "DEFREE")
	public static Object[][] FREEDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREEDE", "REGRESSION");
	}

	@Test(priority = 6, dataProvider = "DEFREE", groups = "REGRESSION", enabled = true)
	public void testFREEDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("valid input")) {
				ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);

				Assert.assertTrue(success, "testFREEDE" + sanityTest);

			} else {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testFREEDE" + smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testFREEDE" + sanityTest);

			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "APIVersion")
	public static Object[][] getAPIVersion()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Version", "REGRESSION");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 7, dataProvider = "APIVersion", groups = "REGRESSION", enabled = true)
	public void testGetAPIVersion(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.convertValue(response, String.class);
			response = APIUtils.convertStringtoJSON(json);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetAPIVersion" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "Ordstatus")
	public static Object[][] getdeorderstatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OrderStatus", "REGRESSION");
	}

	@Test(priority = 8, groups = "REGRESSION", dataProvider = "Ordstatus", enabled = true, dependsOnMethods = "placeHolder")
	public void testdeorderstatus(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode response = null;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("pathvariable", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testdeorderstatus" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdeorderstatus" + sanityTest);

	}

	@DataProvider(name = "AppendMessage")
	public static Object[][] AppendMsg() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("APPENDMESSAGE", "REGRESSION");
	}

	@Test(priority = 9, groups = "Sanity", dataProvider = "AppendMessage", enabled = true, dependsOnMethods = "placeHolder")
	public void testAppendMessage(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode response = null;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("order_id", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExceute, updatedrequest, deliveryAuthString);

		} else {
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExceute, jsonRequestData, deliveryAuthString);
		}
		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testAppendMessage" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testAppendMessage" + sanityTest);
	}

	@DataProvider(name = "HEALTH_CHECK")
	public static Object[][] gethealthcheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HEALTH_CHECK", "REGRESSION");
	}

	@Test(priority = 10, groups = "REGRESSION", dataProvider = "HEALTH_CHECK", enabled = true)
	public void checkhealth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
				serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);
		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, response);
		Assert.assertTrue(success, "checkhealth" + smokeTest);

	}

	@DataProvider(name = "CreateSession")
	public static Object[][] getCreateSession(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMCREATESESSION", "REGRESSION");
	}

	/**
	 * Method To Create DE Session
	 * 
	 * API @Param DE ID
	 * 
	 */
	@Test(priority = 7, groups = "REGRESSION", dataProvider = "CreateSession", enabled = true)
	public void testdeCreateSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse actualData = RestTestUtil.sendDataNGetHttpStatus(
				serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, actualData);
		Assert.assertTrue(success, "testdeCreateSession" + smokeTest);
	}

	@DataProvider(name = "CloseSession")
	public static Object[][] getCloseSession(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMCLOSESESSION", "REGRESSION");
	}

	/**
	 * Method To Close DE Session
	 * 
	 * API @Param DE ID
	 * 
	 */
	@Test(priority = 8, groups = "REGRESSION", dataProvider = "CloseSession", enabled = true)
	public void testdeCloseSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse actualData = RestTestUtil.sendDataNGetHttpStatus(
				serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, actualData);
		Assert.assertTrue(success, "testdeCloseSession" + smokeTest);
	}

	@DataProvider(name = "RestaurantRainMode")
	public static Object[][] getRestaurantRainMode()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTRAINMODE", "REGRESSION");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 9, dataProvider = "RestaurantRainMode", groups = "REGRESSSION", enabled = true)
	public void testRestaurantRainMode(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			if (System.getProperty("env").equalsIgnoreCase("stage")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testRestaurantRainMode" + smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testRestaurantRainMode"
						+ sanityTest);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "RestaurantBulkUpdate")
	public static Object[][] getRestaurantBulUpdate()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTBULKUPDATE",
				"REGRESSION");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 0, dataProvider = "RestaurantBulkUpdate", groups = "REGRESSSION", enabled = true)
	public void testRestaurantBulUpdate(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			if (System.getProperty("env").equalsIgnoreCase("stage")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testRestaurantBulUpdate"
						+ smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testRestaurantBulUpdate"
						+ sanityTest);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "BannerTime")
	public static Object[][] getBannerTime()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETBANNERTIME", "REGRESSION");
	}

	/**
	 * GET: Method for verifying Banner Duration time for a particular Date &
	 * Area_id
	 * 
	 * API @Param date & area_id
	 */
	@Test(priority = 1, groups = "REGRESSION", dataProvider = "BannerTime", enabled = true)
	public void testGetBannerTime(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerTime" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerTime" + sanityTest);

	}

	@DataProvider(name = "BannerRecords")
	public static Object[][] getBannerRecords()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETBANNERRECORDS", "REGRESSION");
	}

	/**
	 * GET: Method for verifying Banner Records time for a particular Date &
	 * Area_id
	 * 
	 * API @Param date & area_id
	 */
	@Test(priority = 2, groups = "REGRESSION", dataProvider = "BannerRecords", enabled = true)
	public void testGetBannerRecords(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerRecords" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerRecords" + sanityTest);

	}

	@DataProvider(name = "GetNotices")
	public static Object[][] getNotices() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETNOTICES", "REGRESSION");
	}

	/**
	 * GET: Method to Get Notices for DE APP
	 * 
	 */
	@Test(priority = 31, dataProvider = "GetNotices", groups = "REGRESSION", enabled = true)
	public void testGetNotices(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ClientResponse response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				response = RestTestUtil.sendDataNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, dtid);
			} else {
				response = RestTestUtil.sendDataNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, "123");
			}

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testGetNotices" + smokeTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "RestaurantUpdate")
	public static Object[][] getRestaurantUpdate()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTUPDATE", "REGRESSION");
	}

	/**
	 * POST: Method to Update the Restaurant Data
	 *
	 */
	@Test(priority = 0, dataProvider = "RestaurantUpdate", groups = "REGRESSION", enabled = true)
	public void testRestaurantUpdate(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testRestaurantUpdate" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testRestaurantUpdate" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetSignUpSlots")
	public static Object[][] getSignUpSlots()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETSIGNUPSLOTS", "REGRESSION");
	}

	/**
	 * GET: Method to Get Notices for DE APP
	 * 
	 */
	@Test(priority = 31, dataProvider = "GetSignUpSlots", groups = "REGRESSION", enabled = true)
	public void testGetSignUpSlots(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ClientResponse response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				response = RestTestUtil.sendDataNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, dtid);
			} else {
				response = RestTestUtil.sendDataNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, "123");
			}

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testGetSignUpSlots" + smokeTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "delogin")
	public static Object[][] getdelogin() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("delogin", "REGRESSION");
	}

	/**
	 * Method for DE Login
	 * 
	 * requires DE id, App Version
	 */
	@Test(priority = 9, groups = "REGRESSION", dataProvider = "delogin")
	public void testdelogin(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> updaterequest = new HashMap<String, Object>();

			updaterequest.put("version",
					STAFAgent.getSTAFValue("delivery_App_version"));
			updaterequest.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, updaterequest);

			// @Param DeliveryBoy Id & Current DE App Version
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequestData, defaulTID,
					defaultTOKEN);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, defaulTID,
					defaultTOKEN);
		}
		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(b, "testdelogin" + smokeTest);

		b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(b, "testdelogin" + sanityTest);

	}

	@DataProvider(name = "GETOTP")
	public static Object[][] getOTP() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CHECKOTP", "REGRESSION");
	}

	@Test(priority = 10, dataProvider = "GETOTP", groups = "REGRESSION")
	public void testGetOTP(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		try {
			HashMap<String, Object> update = new HashMap<>();
			update.put("deId", STAFAgent.getSTAFValue("delivery_Boy_id"));

			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

				DEOTP = response.get("data").asLong();
				log.info("DEOTP is " + DEOTP);
			} else {
				response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);
			}
			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);

			Assert.assertTrue(success, "testGetOTP" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetOTP" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "DE_OTP")
	public static Object[][] getdeOTP() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_OTP", "REGRESSION");
	}

	/**
	 * Method for verifying the OTP received by DE Executive
	 * 
	 * requires received OTP & Delivery Boy Id
	 */
	@Test(priority = 11, groups = "REGRESSION", dataProvider = "DE_OTP", enabled = true)
	public void testDeotp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> updaterequest = new HashMap<>();

			updaterequest.put("otp", DEOTP);

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, updaterequest);

			// API @Param DeliveryBoyId & OTP Sent to registered DE Mobile
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequest,
					STAFAgent.getSTAFValue("defaultAuthString"));

			dtid = response.get("data").get("Authorization").asText()
					.toString();
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					STAFAgent.getSTAFValue("defaultAuthString"));
		}
		log.info("Tid is " + dtid);

		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(b, "testDeotp" + smokeTest);
		b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
		Assert.assertTrue(b, "testDeotp" + sanityTest);
	}

	@DataProvider(name = "cashmsessions")
	public static Object[][] getcmsessions()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("cashmsessions", "REGRESSION");
	}

	/**
	 * Method To get Delivery Boy Cash Management sessions
	 * 
	 * API @Param DE Id & current session Id
	 * 
	 */
	@Test(priority = 12, groups = "REGRESSION", dataProvider = "cashmsessions", enabled = true)
	public void testdecmsessions(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			DESessionId = actualData.get("data").get("sessions").get(0)
					.get("sessionId").asText();
		} else {
			actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}
		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdecmsessions" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdecmsessions" + sanityTest);
	}

	@DataProvider(name = "cashmsessindetails")
	public static Object[][] getcmsessiondetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("cashmsessindetails", "REGRESSION");
	}

	/**
	 * Method To get Cash Management session details
	 * 
	 * API @Param DE Id & Transaction Id
	 * 
	 */
	@Test(priority = 13, groups = "REGRESSION", dataProvider = "cashmsessindetails", enabled = true)
	public void testdecmsessiondetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("placeholder1", STAFAgent.getSTAFValue("delivery_Boy_id"));
			update.put("placeholder2", DESessionId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			log.info("defaultAuthString +" + STAFAgent.getSTAFValue("defaultAuthString"));
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			Assert.assertTrue(response.get("data").get("transactions").get(0)
					.get("id").asLong() > 0L,
					"Unable to Get Transaction Id for Current Session Id");

			DETransactionId = response.get("data").get("transactions").get(0)
					.get("id").asLong();
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}
		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdecmsessiondetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdecmsessiondetails" + sanityTest);
	}

	@DataProvider(name = "getSessionLogs")
	public static Object[][] getSessionLogs()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GetSessionLogs", "REGRESSION");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 14, groups = "REGRESSION", dataProvider = "getSessionLogs", enabled = true)
	public void testGetSessionLogs(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("placeholder1", STAFAgent.getSTAFValue("delivery_Boy_id"));
			update.put("placeholder2", DESessionId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetSessionLogs" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetSessionLogs" + sanityTest);

	}

	@DataProvider(name = "getComments")
	public static Object[][] getComments() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GetComments", "REGRESSION");
	}

	/**
	 * Method gets the Comments for DE Session GET API
	 * 
	 * API @Param SessionId
	 */
	@Test(priority = 15, groups = "REGRESSION", dataProvider = "getComments", enabled = true)
	public void testGetDEComments(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", DESessionId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEComments" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEComments" + sanityTest);

	}

	@DataProvider(name = "getConsistencyIssues")
	public static Object[][] getConsistencyIssues()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ConsistencyIssues", "REGRESSION");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 16, groups = "REGRESSION", dataProvider = "getConsistencyIssues")
	public void testGetConsistencyIssues(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, jsonRequestData, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetConsistencyIssues" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetConsistencyIssues" + sanityTest);

	}

	@DataProvider(name = "getZoneCashOverview")
	public static Object[][] getZoneCashOverview()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Zonecashoverview", "REGRESSION");
	}

	/**
	 * Method verifies the zone cash deatils GET API
	 * 
	 * API @Param zoneid
	 */
	@Test(priority = 17, groups = "REGRESSION", dataProvider = "getZoneCashOverview")
	public void testZoneCashOverview(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", STAFAgent.getSTAFValue("DeliveryZoneID"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			ZoneCashId = response.get("data").get("zoneCashDetails").get(0)
					.get("zoneCashId").asLong();

		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testZoneCashOverview" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testZoneCashOverview" + sanityTest);

	}

	@DataProvider(name = "geZoneCashLogs")
	public static Object[][] geZoneCashLogs()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZoneCashLogs", "REGRESSION");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 18, groups = "REGRESSION", dataProvider = "geZoneCashLogs", enabled = true)
	public void testGetZoneCashLogs(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", ZoneCashId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetZoneCashLogs" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetZoneCashLogs" + sanityTest);
	}

	@DataProvider(name = "zonefloatingcash")
	public static Object[][] getzonefloatingcash()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("zonefloatingcash", "REGRESSION");
	}

	@Test(priority = 18, groups = "REGRESSION", dataProvider = "zonefloatingcash")
	public void testdezonefloatingcash(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", STAFAgent.getSTAFEndPoint("DeliveryZoneID"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdezonefloatingcash" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdezonefloatingcash" + sanityTest);

	}

	@DataProvider(name = "DEOrderHistory")
	public static Object[][] DEOrderHistory()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEOrderHistory", "REGRESSION");
	}

	@Test(priority = 13, groups = "REGRESSION", dataProvider = "DEOrderHistory", enabled = true)
	public void testDEOrderHistory(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExceute, dtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEOrderHistory" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEOrderHistory" + sanityTest);
		} else {
			JsonNode res = RestTestUtil.sendData(
					serviceNameMap.get("delivery"), apiToExceute, "123");
//			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
//					expectedData, res);

			Assert.assertTrue(success, "testDEOrderHistory" + sanityTest);
		}

	}

	@DataProvider(name = "debankdetails")
	public static Object[][] debankdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("debankdetails", "REGRESSION");
	}

	@Test(priority = 14, dataProvider = "debankdetails", groups = "REGRESSION")
	public void testdebankdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		boolean success = false;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, dtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testdebankdetails" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testdebankdetails" + sanityTest);
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, "123");
			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, res);

			Assert.assertTrue(success, "testdebankdetails" + sanityTest);
		}

	}

	@DataProvider(name = "defloatingcash")
	public static Object[][] floatingcash() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("defloatingcash", "REGRESSION");
	}

	@Test(priority = 15, groups = "REGRESSION", dataProvider = "defloatingcash")
	public void testdefloatingcash(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, dtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testdefloatingcash" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testdefloatingcash" + sanityTest);
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, "123");
			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, res);

			Assert.assertTrue(success, "testdefloatingcash" + sanityTest);
		}
	}

	@DataProvider(name = "GetConfig")
	public static Object[][] getdeConfig() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETDECONFIGS", "REGRESSION");
	}

	@Test(priority = 15, groups = "REGRESSION", dataProvider = "GetConfig")
	public void testdeGetConfig(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, dtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testdeGetConfig" + sanityTest);
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, "123");
			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, res);

			Assert.assertTrue(success, "testdeGetConfig" + sanityTest);
		}
	}

	@DataProvider(name = "deorder")
	public static Object[][] getdeorderdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("deorder", "REGRESSION");
	}

	@Test(priority = 15, groups = "REGRESSION", dataProvider = "deorder", enabled = true, dependsOnMethods = "placeHolder")
	public void testdeorderdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = null;
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, dtid);
		} else {
			actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, "123");
		}

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdeorderdetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdeorderdetails" + sanityTest);
	}

	@DataProvider(name = "GetOrderLocation")
	public static Object[][] getDEOrderLocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEORDERLOCATION", "REGRESSION");
	}

	/**
	 * Method for get the Order location GET API
	 * 
	 * API @Param order_id
	 */
	@Test(priority = 15, groups = "REGRESSION", dataProvider = "GetOrderLocation", enabled = true, dependsOnMethods = "placeHolder")
	public void testGetDEOrderLocation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);
		}

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testGetDEOrderLocation" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEOrderLocation" + sanityTest);
	}

	@DataProvider(name = "CallDE")
	public static Object[][] DECall() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DECALLME", "REGRESSION");
	}

	@Test(priority = 16, groups = "REGRESSION", dataProvider = "CallDE", enabled = true, dependsOnMethods = "placeHolder")
	public void testCallDE(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", DeliveryCommon.getOrderId());

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updaterequest, dtid);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, jsonRequestData, dtid);
		}

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testCallDE" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testCallDE" + sanityTest);
	}

	@DataProvider(name = "contracturls")
	public static Object[][] contracurls() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("contracturls", "REGRESSION");
	}

	@Test(priority = 16, groups = "REGRESSION", dataProvider = "contracturls", enabled = true, dependsOnMethods = "placeHolder")
	public void testdecontracturls(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, dtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testdecontracturls" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testdecontracturls" + sanityTest);

		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, "123");
			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, res);

			Assert.assertTrue(success, "testdecontracturls" + sanityTest);
		}

	}

	@DataProvider(name = "debatch")
	public static Object[][] getdebatchdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("debatch", "REGRESSION");
	}

	@Test(priority = 17, groups = "REGRESSION", dataProvider = "debatch", enabled = true, dependsOnMethods = "placeHolder")
	public void testdebatchdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode actualData = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, dtid);
		} else {
			actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, "123");
		}

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdebatchdetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdebatchdetails" + sanityTest);

	}

	@DataProvider(name = "OrderAck")
	public static Object[][] OrderAck() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OrderAck", "REGRESSION");
	}

	@Test(priority = 18, groups = "REGRESSION", dataProvider = "OrderAck", enabled = true, dependsOnMethods = "placeHolder")
	public void testOrderAck(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("order_ids", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updatedrequest, dtid);
		} else {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, jsonRequestData, dtid);
		}

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testOrderAck" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testOrderAck" + sanityTest);

	}

	@DataProvider(name = "AddComment")
	public static Object[][] getAddComment(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMADDCOMMENTS", "REGRESSION");
	}

	/**
	 * Method To Add Comments for a DE Session
	 * 
	 * API @Param current session Id & Comment to be added
	 * 
	 */
	@Test(priority = 19, groups = "REGRESSION", dataProvider = "AddComment", enabled = true, dependsOnMethods = "placeHolder")
	public void testdeAddComment(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse actualData = null;
		if (dataSetName.equalsIgnoreCase("valid input")) {

			HashMap<String, Object> update = new HashMap<>();
			update.put("sessionId", DESessionId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			actualData = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);
		} else {
			actualData = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, deliveryAuthString);
		}
		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, actualData);
		Assert.assertTrue(success, "testdeAddComment" + smokeTest);

	}

	@DataProvider(name = "GetDEBulkUpdateStatus")
	public static Object[][] getGetDEBulkUpdateStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKUPDATE", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "GetDEBulkUpdateStatus", groups = "REGRESSION", enabled = true)
	public void testGetDEBulkUpdateStatus(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean success = false;
		try {
			ClientResponse response = null;
			if (dataSetName.equalsIgnoreCase("valid input")) {
				HashMap<String, Object> update = new HashMap<>();
				update.put("id", STAFAgent.getSTAFEndPoint("delivery_Boy_id"));

				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						updatedrequest, deliveryAuthString);
				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);
				Assert.assertTrue(success, "testGetDEBulkUpdateStatus"
						+ smokeTest);
			} else {
				response = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);
				Assert.assertTrue(success, "testGetDEBulkUpdateStatus"
						+ smokeTest);
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDEBulkUpdateEnable")
	public static Object[][] getDEBulkUpdateEnable()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKENABLE", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "GetDEBulkUpdateEnable", groups = "REGRESSION", enabled = true)
	public void testGetDEBulkUpdateEnable(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean success = false;
		try {
			ClientResponse response = null;
			if (dataSetName.equalsIgnoreCase("valid input")) {
				HashMap<String, Object> update = new HashMap<>();
				update.put("id", STAFAgent.getSTAFEndPoint("delivery_Boy_id"));

				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						updatedrequest, deliveryAuthString);
				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);
				Assert.assertTrue(success, "testGetDEBulkUpdateEnable"
						+ smokeTest);
			} else {
				response = RestTestUtil.sendDataAsPathParamNGetHttpStatus(
						serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
				success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
						expectedData, response);
				Assert.assertTrue(success, "testGetDEBulkUpdateEnable"
						+ smokeTest);
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "delogout")
	public static Object[][] getdelogout() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("delogout", "REGRESSION");
	}

	@Test(priority = 50, groups = "Sanity", dataProvider = "delogout")
	public void testdelogout(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = null;
		boolean b = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExecute, jsonRequestData, dtid);

			b = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

			Assert.assertTrue(b, "testdelogout" + smokeTest);

			b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(b, "testdelogout" + sanityTest);
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, "123");

			b = JsonValidator.HIGH_LEVEL
					.validateHttpResponse(expectedData, res);

			Assert.assertTrue(b, "testdelogout" + smokeTest);
		}

	}

	@DataProvider(name = "DELIVERY_BOY_SERVICESLOGOUT")
	public static Object[][] getDeliveryServiceLogout()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEServicesLogout", "REGRESSION");

	}

	@Test(priority = 51, dataProvider = "DELIVERY_BOY_SERVICESLOGOUT", groups = "REGRESSION")
	public void testMakeDeliveryBoyServiceLogout(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		try {
			JsonNode actualData = null;
			boolean success = false;
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, "id",
								STAFAgent.getSTAFEndPoint("delivery_Boy_id"));

				actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			log.info(apiToExecute);

			Assert.assertTrue(success, "testMakeDeliveryBoyServiceLogout"
					+ smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(success, "testMakeDeliveryBoyServiceLogout"
					+ sanityTest);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@DataProvider(name = "test")
	public static Object[][] AppendMsgtest()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("test", "REGRESSION");
	}

	@Test(priority = 9, groups = "Sanity", dataProvider = "test", enabled = false, dependsOnMethods = "placeHolder")
	public void test(String dataGroup, String apiToExceute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		boolean success = false;
		JsonNode response = null;

		if (dataSetName.equalsIgnoreCase("valid input")) {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("order_id", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExceute, updatedrequest, deliveryAuthString);

		} else if (dataSetName.equalsIgnoreCase("Check Object")
				|| dataSetName.equalsIgnoreCase("Valid Order Id")
				|| dataSetName.equalsIgnoreCase("AREABANNERRECORD")) {
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, jsonRequestData,
					deliveryAuthString);
		} else {
			response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
					apiToExceute, jsonRequestData, deliveryAuthString);
		}
		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testAppendMessage" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testAppendMessage" + sanityTest);
	}
}