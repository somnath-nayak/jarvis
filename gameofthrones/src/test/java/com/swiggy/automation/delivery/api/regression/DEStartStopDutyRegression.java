package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DEStartStopDuty;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEStartStopDutyRegression extends RestTestHelper {

	private final static Logger log = Logger.getLogger(DEStartStopDuty.class);

	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String stopDtid = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	/**
	 * Method to DE Login & get the DE TID
	 */
	@Test(groups = "Sanity", priority = 0, enabled = true)
	public void DELogin() {
		DeliveryCommon.DELogin(serviceNameMap.get("delivery"),
				deliveryAuthString);
		DeliveryCommon.GetOtp(serviceNameMap.get("delivery"), deliveryAuthString);
		DeliveryCommon.ValidateOTP(serviceNameMap.get("delivery"),
				deliveryAuthString);
		Assert.assertTrue(DeliveryCommon.getOTP() > 0L, "Unable to Login");
		log.info("DE Login Successful");

	}

	@DataProvider(name = "Start-Duty")
	public static Object[][] StartDuty() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Start-Duty", "REGRESSION");
	}

	/**
	 * POST: Method To Start DE Duty
	 * 
	 * API @Param DE TID from Login API
	 *
	 */
	@Test(priority = 1, groups = "REGRESSION", dataProvider = "Start-Duty", enabled = true)
	public void testdeStartDuty(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(success, "testdeStartDuty" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(success, "testdeStartDuty" + sanityTest);

			stopDtid = actualData.get("data").get("Authorization").asText();
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					DeliveryCommon.getDEAuth() + "123");

			success = JsonValidator.HIGH_LEVEL
					.validateHttpResponse(expectedData, res);

			Assert.assertTrue(success, "testdeStartDuty" + sanityTest);
		}

	}

	@DataProvider(name = "Stop-Duty")
	public static Object[][] StopDuty() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Stop-Duty", "REGRESSION");
	}

	/**
	 * POST: Method To Stop DE Duty
	 * 
	 * API @Param DE TID from Start Duty API
	 *
	 */
	@Test(priority = 2, groups = "REGRESSION", dataProvider = "Stop-Duty", enabled = true)
	public void testdeStopDuty(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		if (dataSetName.equalsIgnoreCase("valid input")) {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, stopDtid);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(success, "testdeStopDuty" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(success, "testdeStopDuty" + sanityTest);
		} else {
			ClientResponse res = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, "123");

			success = JsonValidator.HIGH_LEVEL
					.validateHttpResponse(expectedData, res);

			Assert.assertTrue(success, "testdeStartDuty" + sanityTest);
		}
	}

}
