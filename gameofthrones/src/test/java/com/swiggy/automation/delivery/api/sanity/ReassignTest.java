package com.swiggy.automation.delivery.api.sanity;

import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class ReassignTest extends RestTestHelper {

	public final static Logger logger = Logger.getLogger(ReassignTest.class);
	public static String delivery_host = null;
	private static String controller_delivery_host = null;
	private static Integer controller_delivery_port = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {
		try {

//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));
//
//			controller_delivery_host = getEndPoint("delivery_controller_host");
//			controller_delivery_port = Integer
//					.parseInt(getEndPoint("delivery_controller_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 0, enabled = true, groups = "Sanity")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			DeliveryCommon.LoginToBackend(serviceNameMap.get("sand"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "order id is null");
			// Since Order Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "CONTROLLER-REASSIGN")
	public static Object[][] ControllerReassign()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CONTROLLERREASSIGN");
	}

	@Test(priority = 2, dataProvider = "CONTROLLER-REASSIGN", enabled = true, dependsOnMethods = "placeHolder")
	public void testControllerReassign(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", DeliveryCommon.getOrderId());

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerReassign" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerReassign" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "AUTO-REASSIGN")
	public static Object[][] AutoReassign() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("AUTOREASSIGN");
	}

	@Test(priority = 3, dataProvider = "AUTO-REASSIGN", enabled = true, dependsOnMethods = "placeHolder")
	public void testAutoReassign(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", DeliveryCommon.getOrderId());

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testAutoReassign" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testAutoReassign" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", DeliveryCommon.getOrderId());
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "time", System.currentTimeMillis() / 1000);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
				oms_osrftoken);
		JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedResponseData, actualData, new String[] { "status",
						"message" });
		Thread.sleep(2000);
	}

}
