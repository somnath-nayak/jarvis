package com.swiggy.automation.delivery.api.sanity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEOrderReject extends RestTestHelper {

	private static String oms_host = null;
	private static Integer oms_port = null;
	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String Backend_host = null;
	private static Integer Backend_port = null;

	@BeforeClass
	public void beforeClass() {

		try {

			oms_osrftoken = STAFAgent.getSTAFValue("oms_osrftoken");
			oms_sessionid = STAFAgent.getSTAFValue("oms_sessionid");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	@Test(priority = 0, enabled = true, groups = "Sanity")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			// It takes time for DE to become Free
			Thread.sleep(20000);
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "order id is null");
			// Since Order Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 1, enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("checkout"),
					deliveryAuthString);
			success = true;
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFValue("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "SystemReject")
	public static Object[][] SystemRejectOrder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("System_Reject");
	}

	@Test(priority = 2, dataProvider = "SystemReject", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testSystemRejectOrder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		try {
			HashMap<String, Object> deorder = new HashMap<String, Object>();
			HashMap<String, Object> update = new HashMap<String, Object>();
			ArrayList<HashMap<String, Object>> list = new ArrayList<>();

			deorder.put("deId", DeliveryCommon.getAssignedMap().get("id"));
			deorder.put("orderId", DeliveryCommon.getOrderId());
			list.add(deorder);

			update.put("deOrderArray", list);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, updaterequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testSystemRejectOrder" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testSystemRejectOrder" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", DeliveryCommon.getOrderId());
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "time", System.currentTimeMillis() / 1000);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
				oms_osrftoken);
		JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedResponseData, actualData, new String[] { "status",
						"message" });
		Thread.sleep(20000);
	}
}
