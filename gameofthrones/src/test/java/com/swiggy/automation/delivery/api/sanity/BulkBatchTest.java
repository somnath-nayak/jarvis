package com.swiggy.automation.delivery.api.sanity;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BulkBatchTest extends RestTestHelper {

	private final static Logger log = Logger.getLogger(BulkBatchTest.class);

	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String Backend_host = null;
	private static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
		

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 0, enabled = true, groups = "Sanity")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString")
);
			for (int i = 0; i < 2; i++) {
				DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
						STAFAgent.getSTAFValue("defaultAuthString")
);
				DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
						STAFAgent.getSTAFValue("defaultAuthString")
);
				boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"), STAFAgent.getSTAFValue("defaultAuthString")
);
				Assert.assertTrue(x, "Order id is null");
				// Since Order Placing takes time
				Thread.sleep(5000);
				DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("checkout"),
						STAFAgent.getSTAFValue("defaultAuthString")
);
			}
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "BulkMerge")
	public static Object[][] DEBulkMerge() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKMERGE");
	}

	/**
	 * POST: Method To Bulk Merge Orders
	 * 
	 * API @Param BatchId's of Orders
	 *
	 */
	@Test(priority = 1, groups = "Sanity", dataProvider = "BulkMerge", enabled = true, dependsOnMethods = "placeHolder")
	public void testDEBulkMerge(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ArrayList<Long> BatchIds = new ArrayList<>();
		ArrayList<Long> childs = new ArrayList<>();
		try {

			childs = DeliveryCommon.getOrder_Ids();

			for (int i = 0; i < childs.size(); i++)
				BatchIds.add(DeliveryCommon.BatchIdFromOrderStatus(
						serviceNameMap.get("checkout"), deliveryAuthString,
						childs.get(i)));// childs.get(0)

			ArrayNode jsonarray = (ArrayNode) jsonRequestData.get("requests");
			JSONObject jsonobject = new JSONObject(jsonarray.get(0).toString());
			jsonobject.put("parent_batch_id", BatchIds.get(0));
			jsonobject.put("childs", BatchIds);

			JsonNode newjson = APIUtils.convertStringtoJSON(jsonobject
					.toString());

			((ObjectNode) jsonRequestData).putArray("requests").add(newjson);

			log.info("Updated Json Array Request " + jsonRequestData);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEBulkMerge" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEBulkMerge" + sanityTest);
		} catch (Exception e) {
			log.info(e.getStackTrace());
		}
	}

	@DataProvider(name = "BulkAssign")
	public static Object[][] DEBulkAssign() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKASSIGN");
	}

	/**
	 * POST: Method To Bulk Assign Orders
	 * 
	 * API @Param BatchId's of Orders
	 *
	 */
	@Test(priority = 2, groups = "Sanity", dataProvider = "BulkAssign", enabled = true, dependsOnMethods = "placeHolder")
	public void testDEBulkAssign(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		ArrayList<Long> childs = new ArrayList<>();
		try {

			// Since it takes time for Order Update
			Thread.sleep(3000);

			childs = DeliveryCommon.getOrder_Ids();

			ArrayNode jsonarray = (ArrayNode) jsonRequestData.get("requests");
			JSONObject jsonobject = new JSONObject(jsonarray.get(0).toString());
			jsonobject.put("batch_id", DeliveryCommon.BatchIdFromOrderStatus(
					serviceNameMap.get("checkout"), deliveryAuthString,
					childs.get(0)));
			jsonobject.put("de_id", STAFAgent.getSTAFEndPoint("delivery_Boy_id"));

			JsonNode newjson = APIUtils.convertStringtoJSON(jsonobject
					.toString());

			((ObjectNode) jsonRequestData).putArray("requests").add(newjson);

			log.info("Updated Json Array Request " + jsonRequestData);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEBulkAssign" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testDEBulkAssign" + sanityTest);
		} catch (Exception e) {
			log.info(e.getStackTrace());
		}
	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		ArrayList<Long> OrderList = new ArrayList<>();
		OrderList = DeliveryCommon.getOrder_Ids();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		if (OrderList.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : OrderList) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
					oms_osrftoken);
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedResponseData, actualData, new String[] { "status",
							"message" });
			Thread.sleep(2000);
		}

	}

}
