package com.swiggy.automation.delivery.api.sanity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;

public class DeliveryCommon extends RestTestHelper {

	private final static Logger log = Logger.getLogger(DeliveryCommon.class);
	private static HashMap<String, Object> LoginMap = new HashMap<>();
	private static HashMap<String, Object> BillMap = new HashMap<>();
	private static HashMap<String, String> AssignedMap = new HashMap<>();
	private static Long ServicableId = null;
	private static Long OrderId = null;
	private static Long OTP = null;
	private static Long BatchId = 0L;
	private static String DEAuth = null;
	private static ArrayList<Long> Order_Ids = new ArrayList<>();
	private static ArrayList<Long> FreeDEMap = new ArrayList<>();
	public static boolean skiptc;

	static {
		SetDeMap();
	}

	public static ArrayList<Long> getFreeDEMap() {
		return FreeDEMap;
	}

	public static void setFreeDEMap(ArrayList<Long> freeDEMap) {
		FreeDEMap = freeDEMap;
	}

	public static HashMap<String, String> getAssignedMap() {
		return AssignedMap;
	}

	public static ArrayList<Long> getOrder_Ids() {
		return Order_Ids;
	}

	public static String getDEAuth() {
		return DEAuth;
	}

	public static void setDEAuth(String dEAuth) {
		DEAuth = dEAuth;
	}

	public static Long getBatchId() {
		return BatchId;
	}

	public static void setBatchId(Long batchId) {
		BatchId = batchId;
	}

	public static Long getOTP() {
		return OTP;
	}

	public static void setOTP(Long oTP) {
		OTP = oTP;
	}

	public static HashMap<String, Object> getLoginMap() {
		return LoginMap;
	}

	public static void setLoginMap(HashMap<String, Object> loginMap) {
		LoginMap = loginMap;
	}

	public static HashMap<String, Object> getBillMap() {
		return BillMap;
	}

	public static void setBillMap(HashMap<String, Object> billMap) {
		BillMap = billMap;
	}

	public static Long getServicableId() {
		return ServicableId;
	}

	public static void setServicableId(Long servicableId) {
		ServicableId = servicableId;
	}

	public static Long getOrderId() {
		return OrderId;
	}

	public static void setOrderId(Long orderId) {
		OrderId = orderId;
	}

	public static String getAPI(Object[][] request) {
		return request[0][1].toString();
	}

	public static JsonNode getRequest(Object[][] request) {
		JsonNode node = null;
		try {
			node = APIUtils.convertStringtoJSON(request[0][3].toString());
		} catch (Exception e) {
			e.getStackTrace();
		}
		return node;
	}

	public static Long getBatchIdfromDB(Long OrderId) {
		Connection conn = null;
		ResultSet rs = null;
		Long result = 0L;
		String Query = "SELECT * FROM swiggy.trips where order_id = " + OrderId;

		try {

			conn = DBUtil.getConnection(IDBDriverType.MYSQL,
					 STAFAgent.getSTAFValue("prod_delivery_db_host"),
					 STAFAgent.getSTAFValue("prod_delivery_db_name"),
					 STAFAgent.getSTAFValue("prod_delivery_db_username"),
					 STAFAgent.getSTAFValue("prod_delivery_db_password"));

			rs = DBUtil.executeQuery(conn, Query);

			while (rs.next()) {
				// BatchId = rs.getLong("batch_id");
				result = rs.getLong("batch_id");
				setBatchId(result);
				log.info("Batch Id is " + BatchId);
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
		return result;
	}

	public static HashMap<String, Object> LoginToBackend(String baseURL, String Auth) {
		HashMap<String, Object> map = new HashMap<>();
		JsonNode response = null;

		try {
			Object[][] request = APIDataReader
					.getDataSetData("BACKEND_SANITY_DE_LOGIN");
			response = RestTestUtil.sendData(baseURL, getAPI(request),
					PasswordUtil.getLoginData(getRequest(request)), Auth);
			map.put("token", response.get("data").get("token").asText()
					.toString());
			map.put("tid", response.get("tid").asText().toString());
			setLoginMap(map);
			log.info("Successfully Logged into Backend");

		} catch (Exception e) {
			e.getStackTrace();
		}
		return map;
	}

	public static void CreateCart(String baseURL, String Auth) {
		JsonNode response = null;

		try {
			Object[][] request = APIDataReader
					.getDataSetData("CREATE_CART_DE_SANITY");
			response = RestTestUtil.sendData(baseURL, getAPI(request),
					getRequest(request), LoginMap.get("tid").toString(),
					LoginMap.get("token").toString());
			log.info("DeliveryCommon: Create Cart Response" + response);
			log.info("Cart Created Successfully");

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static void GetservicableAddress(String baseURL,
			String Auth) {
		JsonNode response = null;

		try {
			Object[][] request = APIDataReader
					.getDataSetData("GET_ALLADDRESS_DE_SANITY");
			response = RestTestUtil.sendData(baseURL, getAPI(request),
					getRequest(request), LoginMap.get("tid").toString(),
					LoginMap.get("token").toString());
			setServicableId(JsonTestUtils.getServiceableAddressId(response));
			log.info("DeliveryCommon: Get Servicable Address response"
					+ response);
			log.info("Got Servicable Address Successfully");

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static boolean PlaceOrder(String baseURL, String Auth) {
		JsonNode response = null;
		JsonNode updatedrequest = null;
		boolean success = true;

		try {
			Object[][] request = APIDataReader
					.getDataSetData("PLACE_ORDER_DE_SANITY");

			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(request), "address_id", getServicableId());

			response = RestTestUtil.sendData(baseURL, getAPI(request),
					updatedrequest, LoginMap.get("tid").toString(), LoginMap
							.get("token").toString());
			setOrderId(response.get("data").get("order_id").asLong());
			Order_Ids.add(response.get("data").get("order_id").asLong());
			BillMap.put("TotalBill", response.get("data").get("order_total")
					.asInt());
			BillMap.put("TotalIncoming",
					response.get("data").get("order_incoming").asInt());
			BillMap.put("TotalSpending",
					response.get("data").get("order_spending").asInt());
			if (OrderId == 0L && OrderId == null) {
				success = false;
			}
			log.info("DeliveryCommon: PlaceOrder:  Place Order Response"
					+ response);
			log.info("Order Placed Successfully with OrderID" + OrderId);

		} catch (Exception e) {
			e.getStackTrace();
			return false;
		}
		return success;
	}

	public static boolean UpdateOMSOrder(String baseURL, String Auth) {
		HashMap<String, Object> update = new HashMap<String, Object>();
		JsonNode response = null;
		JsonNode updatedrequest = null;
		boolean success = false;
		try {
			Object[][] request = APIDataReader.getDataSetData("OMSUpdateOrder");
			update.put("order_id", getOrderId());// getOrderId()
			update.put("action",
					 STAFAgent.getSTAFValue("deliveryStatusUpdateCode"));

			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(request), update);
			response = RestTestUtil.sendData(baseURL,
					getAPI(request), updatedrequest,
					 STAFAgent.getSTAFValue("oms_sessionid"),
					 STAFAgent.getSTAFValue("oms_osrftoken"));
			success = true;
			log.info("Update OMS Order Status Successfully" + response);

		} catch (Exception e) {
			e.getStackTrace();
		}

		return success;
	}

	public static void GetFreeDe(String baseURL, String Auth) {
		HashMap<String, Object> update = new HashMap<>();
		JsonNode response = null, delist = null;
		try {
			Object[][] request = APIDataReader.getDataSetData("orderdelist");
			update.put("pathvariable", 1095718363);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(request), update);

			response = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(request), updatedrequest, deliveryAuthString);

			delist = response.get("data");

			for (int i = 0; i < delist.size(); i++)
				FreeDEMap.add(delist.get(i).get("id").asLong());

			log.info("DE List API Response is " + response);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static void DELogin(String baseURL, String Auth) {
		HashMap<String, Object> update = new HashMap<>();
		JsonNode response = null;

		try {
			Object[][] request = APIDataReader.getDataSetData("delogin");
			update.put("version",  STAFAgent.getSTAFValue("delivery_App_version"));
			update.put("id",  STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(getRequest(request), update);

			response = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(request), updatedRequestData, defaulTID,
					defaultTOKEN);

			log.info("DE Login Successful OTP Sent" + response);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static void GetOtp(String baseURL, String Auth) {
		HashMap<String, Object> update = new HashMap<>();
		JsonNode response = null;

		try {

			Object[][] request = APIDataReader.getDataSetData("CHECKOTP");
			update.put("deId",  STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(request), update);

			response = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(request), updatedrequest, Auth);

			setOTP(response.get("data").asLong());

			log.info("OTP response is " + response);
			log.info("OTP For Session is " + getOTP());
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static void ValidateOTP(String baseURL, String Auth) {

		HashMap<String, Object> update = new HashMap<>();
		JsonNode response = null;
		try {
			Object[][] request = APIDataReader.getDataSetData("DE_OTP");
			update.put("id",  STAFAgent.getSTAFValue("delivery_Boy_id"));
			update.put("otp", OTP);

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(request), update);

			response = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(request), updatedRequest, Auth);

			setDEAuth(response.get("data").get("Authorization").asText()
					.toString());
			log.info("Tid is " + dtid);
			log.info("OTP is Validated Successfully " + response);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static JsonNode GetOrderStatus(String baseURL,
			String Auth, String OrderId) {
		JsonNode response = null;
		try {
			Object[][] OrderStatus = APIDataReader
					.getDataSetData("OrderStatus");
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("pathvariable", OrderId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(OrderStatus), update);

			response = RestTestUtil.sendData(baseURL, getAPI(OrderStatus),
					updatedrequest, deliveryAuthString);

		} catch (Exception e) {
			e.getStackTrace();
		}
		return response;
	}

	public static void AssignBatchOrder(String baseURL, String Auth) {
		HashMap<String, Object> update = new HashMap<>();
		HashMap<String, Object> freeMap = new HashMap<>();
		HashMap<String, Object> orderStatusUpdate = new HashMap<>();
		int count = 0, counter = 0, i = 0;
		JsonNode updatedFreeMap = null, response = null, StatusUpdate = null, AssignUpdatedRequest = null, statusResponse = null;
		try {
			Object[][] assign = APIDataReader.getDataSetData("assignbatch");
			Object[][] free = APIDataReader.getDataSetData("FREEDE");
			Object[][] OrderStatus = APIDataReader
					.getDataSetData("OrderStatus");
			counter = FreeDEMap.size();

			freeMap.put("id",  STAFAgent.getSTAFValue("delivery_Boy_id"));
			update.put("batch_id", getBatchIdfromDB(getOrderId()));
			update.put("de_id",  STAFAgent.getSTAFValue("delivery_Boy_id"));
			orderStatusUpdate.put("pathvariable", getOrderId());

			updatedFreeMap = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(free), freeMap);

			RestTestUtil.sendData(baseURL, getAPI(free), updatedFreeMap,
					Auth);

			StatusUpdate = RestTestUtil.updateRequestDataBeforeSend(
					getRequest(OrderStatus), orderStatusUpdate);

			statusResponse = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(OrderStatus), StatusUpdate, Auth);

			count = 0;

			statusResponse = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(OrderStatus), StatusUpdate, Auth);

			update.put("batch_id", statusResponse.get("data").get("batch_id")
					.asText());

			while (counter-- > 0 || !isAssigned(statusResponse) && i < 5) {

				if (!isAssigned(statusResponse)) {

					freeMap.put("id", FreeDEMap.get(i));
					updatedFreeMap = RestTestUtil.updateRequestDataBeforeSend(
							getRequest(free), freeMap);
					update.put("de_id", FreeDEMap.get(i));
					AssignUpdatedRequest = RestTestUtil
							.updateRequestDataBeforeSend(getRequest(assign),
									update);

					count = 0;
					while (!isAssigned(statusResponse)
							&& (!isAssignedToDe(statusResponse) && count < 3)) {

						RestTestUtil.sendData(baseURL, getAPI(free),
								updatedFreeMap, Auth);

						// Since Unassign takes time
						Thread.sleep(10000);
						RestTestUtil.sendData(baseURL, getAPI(assign),
								AssignUpdatedRequest, Auth);
						statusResponse = RestTestUtil.sendDataAsPathParam(baseURL, getAPI(OrderStatus), StatusUpdate, Auth);
						count++;
					}
				}
				i++;
			}
			if (isAssigned(statusResponse)) {
				AssignedMap.put("id",
						statusResponse.get("data").get("deDetails").get("id")
								.asText());
				AssignedMap.put(
						"mobile",
						statusResponse.get("data").get("deDetails")
								.get("mobileNumber").asText());
			}

			log.info("Assign Batch Order response is " + response);

			log.info("Order ID: " + OrderId
					+ " Successfully assigned to Delivery Boy ID:"
					+ AssignedMap.get("id"));
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	public static boolean isAssigned(JsonNode response) {
		return response.get("data").get("status").asText().equals("assigned");
	}

	public static boolean isAssignedToDe(JsonNode response) {
		boolean success = false;
		try {
			success = response.get("data").get("deDetails").get("id").asText()
					.equals( STAFAgent.getSTAFValue("delivery_Boy_id"))
					|| response.get("data").get("deDetails").get("id").asText()
							.equals( STAFAgent.getSTAFValue("delivery_Boy_id2"));
		} catch (Exception e) {
			e.getStackTrace();
		}
		return success;
	}

	public static boolean isAssignedToDe(JsonNode response, String DE) {
		boolean success = false;
		try {
			success = response.get("data").get("deDetails").get("id").asText()
					.equals(DE);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return success;
	}

	public static Long BatchIdFromOrderStatus(String baseURL,
			String Auth, Long OrderId) {
		Long batch_id = 0L;
		HashMap<String, Object> update = new HashMap<String, Object>();
		try {
			Object[][] request = APIDataReader.getDataSetData("OrderStatus");
			update.put("pathvariable", OrderId);

			JsonNode updatedrequeset = RestTestUtil
					.updateRequestDataBeforeSend(getRequest(request), update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(baseURL,
					getAPI(request), updatedrequeset, Auth);

			if (response.get("statusCode").asText().equals("0")) {
				batch_id = response.get("data").get("batch_id").asLong();
				log.info("batch id " + batch_id + "for Order Id is " + OrderId);
			} else
				Assert.assertTrue(false,
						"Unable to get the Batch Id for Order " + getOrderId()
								+ "Adding 0 as Default Batch Id");
		} catch (Exception e) {
			e.getStackTrace();
		}
		return batch_id;
	}

	public static void SetDeMap() {
		try {
			String Des =  STAFAgent.getSTAFValue("delivery_boys");
			String[] DE = Des.split(",");
			ArrayList<Long> D = new ArrayList<>();
			for (String a : DE)
				D.add(Long.parseLong(a));
			setFreeDEMap(D);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

//	public static void main(String[] args) {
//		LoginToBackend(backHost, backHostPort, defaultAuthString);
//		CreateCart(backHost, backHostPort, defaultAuthString);
//		GetservicableAddress(backHost, backHostPort, defaultAuthString);
//		PlaceOrder(backHost, backHostPort, defaultAuthString);
//		UpdateOMSOrder(OMSHost, OMSPort, defaultAuthString);
//		DELogin(deliveryAPIHost, deliveryAPIHostPort, defaultAuthString);
//		GetOtp(deliveryAPIHost, deliveryAPIHostPort, deliveryAuthString);
//		ValidateOTP(deliveryAPIHost, deliveryAPIHostPort, deliveryAuthString);
//		AssignBatchOrder(baseURL,
//				deliveryAuthString);
		// GetFreeDe(deliveryAPIHost, deliveryAPIHostPort, deliveryAuthString);
		// SetDeMap();
//	}

}
