package com.swiggy.automation.delivery.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEAlchemistSanity extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEAlchemistSanity.class);

	private static String Backend_host = null;
	private static Integer Backend_port = null;
	private static String oms_host = null;
	private static Integer oms_port = null;
	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String delivery_alchemist_host = null;
	private static Integer delivery_alchemist_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
		

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(groups = "Sanity", priority = 0, enabled = true)
	public void testDELogin() throws SwiggyAPIAutomationException {
		DeliveryCommon.DELogin(STAFAgent.getSTAFEndPoint("checkout"),
				deliveryAuthString);
		DeliveryCommon.GetOtp(STAFAgent.getSTAFEndPoint("checkout"), deliveryAuthString);
		DeliveryCommon.ValidateOTP(STAFAgent.getSTAFEndPoint("checkout"),
				deliveryAuthString);
		Assert.assertTrue(DeliveryCommon.getOTP() > 0L, "Unable to Login");
		log.info("DE Login Successful");

	}

	@DataProvider(name = "DEINCENTIVE-DAY")
	public static Object[][] getDeIncentive_Day()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEDAY");
	}

	@Test(priority = 1, groups = "Sanity", dataProvider = "DEINCENTIVE-DAY", enabled = true)
	public void checkDeInecentiveDay(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("checkout"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveDay" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveDay" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "DEINCENTIVE-Week")
	public static Object[][] getDeIncentive_Week()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEWEEK");
	}

	@Test(priority = 2, groups = "Sanity", dataProvider = "DEINCENTIVE-Week", enabled = true)
	public void checkDeInecentiveWeek(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("checkout"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveWeek" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveWeek" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "DEINCENTIVE-Month")
	public static Object[][] getDeIncentive_Month()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEMONTH");
	}

	@Test(priority = 3, groups = "Sanity", dataProvider = "DEINCENTIVE-Month", enabled = true)
	public void checkDeInecentiveMonth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("checkout"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveMonth" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveMonth" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
