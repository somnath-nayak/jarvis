package com.swiggy.automation.delivery.api.sanity;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEAPISanityTest extends RestTestHelper {

	public static final Logger log = Logger.getLogger(DEAPISanityTest.class);

	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	String oms_osrftoken = null;
	String oms_sessionid = null;
	String Backend_host = null;
	Integer Backend_port = null;
	Integer TotalBill;
	Integer TotalIncoming;
	Integer TotalSpending;
	String DESessionId = null;
	static Long BatchId;
	Long ZoneCashId;
	Long DETransactionId;
	Long DEOTP;
	String stopDtid;
	public JsonNode OrderJSON = null;
	public static boolean skiptc;

	@BeforeClass
	public void beforeClass() {

		try {


		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	public static boolean CanRun() {
		return System.getProperty("env").equalsIgnoreCase("stage");
	}

	@DataProvider(name = "Delivery_Boy_Status")
	public static Object[][] getDEStatus() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Delivery_Boy_Status");
	}

	/**
	 * GET: Method to Get the Status of Delivery Boy GET API
	 * 
	 * Param @API mobile & token
	 * 
	 */
	@Test(priority = 0, dataProvider = "Delivery_Boy_Status", groups = "SANITY", enabled = true)
	public void testDEStatus(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("token", STAFAgent.getSTAFValue("delivery_Token"));
			update.put("mobile", STAFAgent.getSTAFValue("delivery_Boy_Mobile"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testDEStatus" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testDEStatus " + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "APIVersion")
	public static Object[][] getAPIVersion()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Version");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 0, dataProvider = "APIVersion", groups = "SANITY", enabled = true)
	public void testGetAPIVersion(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.convertValue(response, String.class);
			response = APIUtils.convertStringtoJSON(json);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetAPIVersion" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "RestaurantUpdate")
	public static Object[][] getRestaurantUpdate()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTUPDATE");
	}

	/**
	 * POST: Method to Update the Restaurant Data
	 *
	 */
	@Test(priority = 0, dataProvider = "RestaurantUpdate", groups = "SANITY", enabled = true)
	public void testRestaurantUpdate(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			if (System.getProperty("env").equalsIgnoreCase("stage")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testRestaurantUpdate" + smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testRestaurantUpdate" + sanityTest);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "RestaurantRainMode")
	public static Object[][] getRestaurantRainMode()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTRAINMODE");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 0, dataProvider = "RestaurantRainMode", groups = "SANITY", enabled = true)
	public void testRestaurantRainMode(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			if (System.getProperty("env").equalsIgnoreCase("stage")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testRestaurantRainMode" + smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testRestaurantRainMode"
						+ sanityTest);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "RestaurantBulkUpdate")
	public static Object[][] getRestaurantBulUpdate()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RESTAURANTBULKUPDATE");
	}

	/**
	 * GET: Method to get the Latest API Version, Tag Name & Git Hash released
	 * to Production
	 *
	 */
	@Test(priority = 0, dataProvider = "RestaurantBulkUpdate", groups = "SANITY", enabled = true)
	public void testRestaurantBulUpdate(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			if (System.getProperty("env").equalsIgnoreCase("stage")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);

				Assert.assertTrue(success, "testRestaurantBulUpdate"
						+ smokeTest);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testRestaurantBulUpdate"
						+ sanityTest);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetPolygon")
	public static Object[][] getPolygon() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETpolygon");
	}

	/**
	 * GET: Method to get Polygon Details from Lat & Lan
	 * 
	 * API @Param Latitiude & Longitude
	 */
	@Test(priority = 0, dataProvider = "GetPolygon", groups = "SANITY", enabled = true)
	public void testGetPolygon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygon" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygon" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetPolygonTag")
	public static Object[][] getPolygonTag()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PolygonTag");
	}

	@Test(priority = 0, dataProvider = "GetPolygonTag", groups = "SANITY", enabled = true)
	public void testGetPolygonTag(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonTag" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonTag" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetPolygonById")
	public static Object[][] getPolygonById()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PolygonById");
	}

	@Test(priority = 0, dataProvider = "GetPolygonById", groups = "SANITY", enabled = true)
	public void testGetPolygonById(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonById" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testGetPolygonById" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDEBulkUpdateStatus")
	public static Object[][] getGetDEBulkUpdateStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKUPDATE");
	}

	@Test(priority = 0, dataProvider = "GetDEBulkUpdateStatus", groups = "SANITY", enabled = true)
	public void testGetDEBulkUpdateStatus(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean success = false;
		try {
			HashMap<String, Object> update = new HashMap<>();
			update.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));

			ClientResponse response = RestTestUtil
					.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
							deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);
			Assert.assertTrue(success, "testGetDEBulkUpdateStatus" + smokeTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetDEBulkUpdateEnable")
	public static Object[][] getDEBulkUpdateEnable()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEBULKENABLE");
	}

	@Test(priority = 0, dataProvider = "GetDEBulkUpdateEnable", groups = "SANITY", enabled = true)
	public void testGetDEBulkUpdateEnable(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean success = false;
		try {
			HashMap<String, Object> update = new HashMap<>();
			update.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));

			ClientResponse response = RestTestUtil
					.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
							deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);
			Assert.assertTrue(success, "testGetDEBulkUpdateEnable" + smokeTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GiveOrderRating")
	public static Object[][] getOrderRating()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ORDERRATING");
	}

	/**
	 * POST: Method to Rate DE based on Order
	 * 
	 * API @Param DE Id, DE Rating, Order Id
	 * 
	 */
	@Test(priority = 0, dataProvider = "GiveOrderRating", groups = "SANITY", enabled = true)
	public void testGetDEOrderRating(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetDEOrderRating" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetDEOrderRating" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "BannerTime")
	public static Object[][] getBannerTime()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETBANNERTIME");
	}

	/**
	 * GET: Method for verifying Banner Duration time for a particular Date &
	 * Area_id
	 * 
	 * API @Param date & area_id
	 */
	@Test(priority = 1, groups = "Sanity", dataProvider = "BannerTime", enabled = true)
	public void testGetBannerTime(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("area_id", STAFAgent.getSTAFValue("delivery_area_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerTime" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerTime" + sanityTest);

	}

	@DataProvider(name = "BannerRecords")
	public static Object[][] getBannerRecords()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETBANNERRECORDS");
	}

	/**
	 * GET: Method for verifying Banner Records time for a particular Date &
	 * Area_id
	 * 
	 * API @Param date & area_id
	 */
	@Test(priority = 2, groups = "Sanity", dataProvider = "BannerRecords", enabled = true)
	public void testGetBannerRecords(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("area_id", STAFAgent.getSTAFValue("delivery_area_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerRecords" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetBannerRecords" + sanityTest);

	}

	@DataProvider(name = "GetDELocation")
	public static Object[][] getDELocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETDELOCATION");
	}

	/**
	 * GET: Method for check DE Location
	 * 
	 * API @Param DE Id
	 */
	@Test(priority = 3, groups = "Sanity", dataProvider = "GetDELocation", enabled = true)
	public void testGetDELocation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", STAFAgent.getSTAFValue("delivery_Boy_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDELocation" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDELocation" + sanityTest);

	}

	@DataProvider(name = "GetDEAreaCount")
	public static Object[][] getDEAreaCount()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEAREACOUNT");
	}

	/**
	 * GET: Method for check DE Area Count
	 * 
	 * API @Param AreaId
	 */
	@Test(priority = 4, groups = "Sanity", dataProvider = "GetDEAreaCount", enabled = true)
	public void testGetDEAreaCount(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", STAFAgent.getSTAFValue("DeliveryZoneID"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEAreaCount" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEAreaCount" + sanityTest);

	}

	@DataProvider(name = "cashmsessions")
	public static Object[][] getcmsessions(Method method)
			throws SwiggyAPIAutomationException {
		log.info("Method name in Dataprovider is " + method.getName());
		return APIDataReader.getDataSetData("cashmsessions");
	}

	/**
	 * Method To get Delivery Boy Cash Management sessions
	 * 
	 * API @Param DE Id & current session Id
	 * 
	 */
	@Test(priority = 5, groups = "Sanity", dataProvider = "cashmsessions")
	public void testdecmsessions(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", STAFAgent.getSTAFValue("delivery_Boy_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode actualData = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		DESessionId = actualData.get("data").get("sessions").get(0)
				.get("sessionId").asText();

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdecmsessions" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdecmsessions" + sanityTest);
	}

	@DataProvider(name = "cashmsessindetails")
	public static Object[][] getcmsessiondetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("cashmsessindetails");
	}

	/**
	 * Method To get Cash Management session details
	 * 
	 * API @Param DE Id & Transaction Id
	 * 
	 */
	@Test(priority = 6, groups = "Sanity", dataProvider = "cashmsessindetails", enabled = true)
	public void testdecmsessiondetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("placeholder1", STAFAgent.getSTAFValue("delivery_Boy_id"));
		update.put("placeholder2", DESessionId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		log.info("defaultAuthString +" + STAFAgent.getSTAFValue("defaultAuthString"));
		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		Assert.assertTrue(
				response.get("data").get("transactions").get(0).get("id")
						.asLong() > 0L,
				"Unable to Get Transaction Id for Current Session Id");

		DETransactionId = response.get("data").get("transactions").get(0)
				.get("id").asLong();

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdecmsessiondetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdecmsessiondetails" + sanityTest);
	}

	@DataProvider(name = "AddComment")
	public static Object[][] getAddComment(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMADDCOMMENTS");
	}

	/**
	 * Method To Add Comments for a DE Session
	 * 
	 * API @Param current session Id & Comment to be added
	 * 
	 */
	@Test(priority = 7, groups = "Sanity", dataProvider = "AddComment", enabled = true)
	public void testdeAddComment(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		if (System.getProperty("env").equalsIgnoreCase("stage")) {

			HashMap<String, Object> update = new HashMap<>();
			update.put("sessionId", DESessionId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse actualData = RestTestUtil
					.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
							deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, actualData);
			Assert.assertTrue(success, "testdeAddComment" + smokeTest);
		}
	}

	@DataProvider(name = "CreateSession")
	public static Object[][] getCreateSession(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMCREATESESSION");
	}

	/**
	 * Method To Create DE Session
	 * 
	 * API @Param DE ID
	 * 
	 */
	@Test(priority = 7, groups = "Sanity", dataProvider = "CreateSession", enabled = true)
	public void testdeCreateSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		if (System.getProperty("env").equalsIgnoreCase("stage")) {

			HashMap<String, Object> update = new HashMap<>();
			update.put("deId", STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse actualData = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, actualData);
			Assert.assertTrue(success, "testdeCreateSession" + smokeTest);
		}
	}

	@DataProvider(name = "CloseSession")
	public static Object[][] getCloseSession(Method method)
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CMCLOSESESSION");
	}

	/**
	 * Method To Close DE Session
	 * 
	 * API @Param DE ID
	 * 
	 */
	@Test(priority = 8, groups = "Sanity", dataProvider = "CloseSession", enabled = true)
	public void testdeCloseSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		if (System.getProperty("env").equalsIgnoreCase("stage")) {

			HashMap<String, Object> update = new HashMap<>();
			update.put("deId", STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse actualData = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, actualData);
			Assert.assertTrue(success, "testdeCloseSession" + smokeTest);
		}
	}

	@DataProvider(name = "HEALTH_CHECK")
	public static Object[][] gethealthcheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HEALTH_CHECK");
	}

	@Test(priority = 7, groups = "Sanity", dataProvider = "HEALTH_CHECK", enabled = true)
	public void checkhealth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
				serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
				deliveryAuthString);
		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, response);
		Assert.assertTrue(success, "checkhealth" + smokeTest);

	}

	/* API - SLA Calculation for Restaurant listing */

	@DataProvider(name = "SlaRestaurantListing")
	public static Object[][] getdeslarestlisting()
			throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("SlaRestaurantListing");
	}

	@Test(priority = 8, groups = "Sanity", dataProvider = "SlaRestaurantListing")
	public void testdeslarestlisting(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		if (System.getProperty("env").equalsIgnoreCase("stage")) {
			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, deliveryAuthString);
			log.info(response.getStatus());
			boolean b = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);
			Assert.assertTrue(b, "testdeslarestlisting" + smokeTest);
		}
	}

	/* API - SLA Calculation for Cart */

	@DataProvider(name = "SlaCart")
	public static Object[][] getdeslacart() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SlaCart");
	}

	@Test(priority = 9, groups = "Sanity", dataProvider = "SlaCart")
	public void testdeslacart(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		if (System.getProperty("env").equalsIgnoreCase("stage")) {
			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, deliveryAuthString);
			log.info(response.getStatus());
			boolean b = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);
			Assert.assertTrue(b, "testdeslacart" + sanityTest);
		}
	}

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_DE_LOGIN");
	}

	@DataProvider(name = "getZoneCashOverview")
	public static Object[][] getZoneCashOverview()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Zonecashoverview");
	}

	/**
	 * Method verifies the zone cash deatils GET API
	 * 
	 * API @Param zoneid
	 */
	@Test(priority = 10, groups = "Sanity", dataProvider = "getZoneCashOverview")
	public void testZoneCashOverview(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", STAFAgent.getSTAFValue("DeliveryZoneID"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		ZoneCashId = response.get("data").get("zoneCashDetails").get(0)
				.get("zoneCashId").asLong();

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testZoneCashOverview" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testZoneCashOverview" + sanityTest);

	}

	@DataProvider(name = "getConsistencyIssues")
	public static Object[][] getConsistencyIssues()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ConsistencyIssues");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 11, groups = "Sanity", dataProvider = "getConsistencyIssues")
	public void testGetConsistencyIssues(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, jsonRequestData, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetConsistencyIssues" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetConsistencyIssues" + sanityTest);

	}

	@DataProvider(name = "getSessionLogs")
	public static Object[][] getSessionLogs()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GetSessionLogs");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 12, groups = "Sanity", dataProvider = "getSessionLogs", enabled = true)
	public void testGetSessionLogs(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("placeholder1", STAFAgent.getSTAFValue("delivery_Boy_id"));
		update.put("placeholder2", DESessionId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetSessionLogs" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetSessionLogs" + sanityTest);

	}

	@DataProvider(name = "geZoneCashLogs")
	public static Object[][] geZoneCashLogs()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZoneCashLogs");
	}

	/**
	 * Method verifies Consistency Issues GET API
	 * 
	 */
	@Test(priority = 13, groups = "Sanity", dataProvider = "geZoneCashLogs", enabled = true)
	public void testGetZoneCashLogs(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", ZoneCashId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetZoneCashLogs" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetZoneCashLogs" + sanityTest);

	}

	@DataProvider(name = "getTransactionDetails")
	public static Object[][] getTransactionDetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GetTransactionDetails");
	}

	/**
	 * Method verifies Transaction details GET API
	 * 
	 * API @Param deliveryboyId, SessionId, TransactionId
	 */
	@Test(priority = 14, groups = "Sanity", dataProvider = "getTransactionDetails", enabled = true)
	public void testGetDETransactionDetails(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("placeholder1", STAFAgent.getSTAFValue("delivery_Boy_id"));
		update.put("placeholder2", DESessionId);
		update.put("placeholder3", DETransactionId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDETransactionDetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDETransactionDetails" + sanityTest);
	}

	@DataProvider(name = "getComments")
	public static Object[][] getComments() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GetComments");
	}

	/**
	 * Method gets the Comments for DE Session GET API
	 * 
	 * API @Param SessionId
	 */
	@Test(priority = 15, groups = "Sanity", dataProvider = "getComments", enabled = true)
	public void testGetDEComments(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", DESessionId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEComments" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEComments" + sanityTest);

	}

	/**
	 * Method for User Login used for creating & updating Cart Tid & Token of
	 * the User will be captured for Cart Processing
	 * 
	 */
	@Test(priority = 16, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData),
					STAFAgent.getSTAFValue("defaultAuthString"));

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession" + smokeTest);
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			log.info("TID For the Session" + defaulTID);
			log.info("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				log.info("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "CREATE_CART_SANITY")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CART_DE_SANITY");
	}

	/**
	 * Method for creating a Cart
	 * 
	 */
	@Test(priority = 17, dataProvider = "CREATE_CART_SANITY")
	public void testCreateCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
			// expectedData, JsonValidationLevels.HIGH_LEVEL);

			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData, defaulTID,
					defaultTOKEN);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			Assert.assertTrue(status, "testCreateCartAPI" + smokeTest);

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ALLADDRESS_SANITY")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALLADDRESS_DE_SANITY");
	}

	@Test(priority = 18, dataProvider = "GET_ALLADDRESS_SANITY")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, defaulTID, defaultTOKEN);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI" + smokeTest);
			// Thread.sleep(5000);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "DEBUSY")
	public static Object[][] BusyDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BUSYDE");
	}

	@Test(priority = 18, dataProvider = "DEBUSY", groups = "Sanity")
	public void testBusyDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			HashMap<String, Object> update = new HashMap<>();
			update.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testBusyDE" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "DEFREE")
	public static Object[][] FREEDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREEDE");
	}

	@Test(priority = 19, dataProvider = "DEFREE")
	public void testFREEDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			HashMap<String, Object> update = new HashMap<>();
			update.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testFREEDE" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "PLACE_ORDER_SANITY")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PLACE_ORDER_DE_SANITY");
	}

	/**
	 * Method for Placing an Order
	 * 
	 * requires Valid Tid & Token of User
	 */
	@Test(priority = 19, dataProvider = "PLACE_ORDER_SANITY")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {
			// Thread.sleep(5000);
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			log.info(jsonRequestData);
			log.info(updatedRequest);
			JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, updatedRequest, defaulTID,
					defaultTOKEN);

			OrderJSON = placeOrderResponse;

			log.info("****************************************************");

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + smokeTest);
			orderId = placeOrderResponse.get("data").get("order_id").asLong();
			orderIds.add(orderId);

			orderKey = placeOrderResponse.get("data").get("key").asText()
					.toString();
			TotalBill = placeOrderResponse.get("data").get("order_total")
					.asInt();
			TotalIncoming = placeOrderResponse.get("data")
					.get("order_incoming").asInt();
			TotalSpending = placeOrderResponse.get("data")
					.get("order_spending").asInt();
			if (orderId == 0L && orderId == null) {
				Assert.assertTrue(false);
			}

			log.info("ORDER ID :::" + orderId);
			log.info("ORDER KEY :::" + orderKey);
			log.info("Total Bill is  :::" + TotalBill);
			log.info("Total Income :::" + TotalIncoming);
			log.info("Total Spending :::" + TotalSpending);

			orderKey = placeOrderResponse.get("data").get("key").asText();
			log.info("ORDER KEY :::" + orderKey);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "DELIVERY_BOY_ACTIVATE")
	public static Object[][] getDeliveryBoyIds()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELIVERY_BOY_ACTIVATE");

	}

	@Test(priority = 20, dataProvider = "DELIVERY_BOY_ACTIVATE")
	public void testMakeDeliveryBoyActive(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		try {
			log.info("Make delivery Boy Active");
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			log.info(apiToExecute);

			Assert.assertTrue(success, "testMakeDeliveryBoyActive" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(success, "testMakeDeliveryBoyActive" + sanityTest);

		} catch (Exception e) {
			e.printStackTrace();
			log.error("BackendCriticalSanityTest-->testMakeDeliveryBoyActive "
					+ e);
			Assert.fail(e.getMessage());

		}

	}

	/* API - Delivery Boy Login */

	@DataProvider(name = "delogin")
	public static Object[][] getdelogin() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("delogin");
	}

	/**
	 * Method for DE Login
	 * 
	 * requires DE id, App Version
	 */
	@Test(priority = 21, groups = "Sanity", dataProvider = "delogin")
	public void testdelogin(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> updaterequest = new HashMap<String, Object>();

		updaterequest.put("version",
				STAFAgent.getSTAFValue("delivery_App_version"));
		updaterequest.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));

		JsonNode updatedRequestData = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, updaterequest);

		// @Param DeliveryBoy Id & Current DE App Version
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequestData, defaulTID,
				defaultTOKEN);

		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(b, "testdelogin" + smokeTest);

		b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(b, "testdelogin" + sanityTest);

	}

	@DataProvider(name = "GETOTP")
	public static Object[][] getOTP() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CHECKOTP");
	}

	@Test(priority = 22, dataProvider = "GETOTP")
	public void testGetOTP(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			// Wait for OTP since it will take some time to generate
			Thread.sleep(5000);
			HashMap<String, Object> update = new HashMap<>();
			update.put("deId", STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			DEOTP = response.get("data").asLong();
			log.info("DEOTP is " + DEOTP);

			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);

			Assert.assertTrue(success, "testGetOTP" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetOTP" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "DE_OTP")
	public static Object[][] getdeOTP() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_OTP");
	}

	/**
	 * Method for verifying the OTP received by DE Executive
	 * 
	 * requires received OTP & Delivery Boy Id
	 */
	@Test(priority = 23, groups = "Sanity", dataProvider = "DE_OTP", enabled = true)
	public void testDeotp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> updaterequest = new HashMap<>();

		updaterequest.put("id", STAFAgent.getSTAFValue("delivery_Boy_id"));
		updaterequest.put("otp", DEOTP);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, updaterequest);

		// API @Param DeliveryBoyId & OTP Sent to registered DE Mobile
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequest, STAFAgent.getSTAFValue("defaultAuthString"));

		dtid = response.get("data").get("Authorization").asText().toString();
		log.info("Tid is " + dtid);

		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(b, "testDeotp" + smokeTest);
		b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
		Assert.assertTrue(b, "testDeotp" + sanityTest);
	}

	@DataProvider(name = "OMSUpdateOrder")
	public static Object[][] UpdateOrder() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMSUpdateOrder");
	}

	@Test(priority = 24, groups = "Sanity", dataProvider = "OMSUpdateOrder", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void UpdateOrderStatus(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<String, Object>();

		update.put("order_id", orderId);
		update.put("action", STAFAgent.getSTAFValue("deliveryStatusUpdateCode"));

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, updatedRequest, oms_sessionid,
				oms_osrftoken);

		boolean success = JsonValidator.HIGH_LEVEL
				.validateJsonWithCustomFields(expectedData, response,
						new String[] { "status", "message" });

		Assert.assertTrue(success, "UpdateOrderStatus" + smokeTest);

	}

	@DataProvider(name = "ORDEREDIT")
	public static Object[][] EDITORDER() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("EDITORDER");

	}

	@Test(priority = 25, dataProvider = "ORDEREDIT", groups = "SANITY", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testOrderEdit(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			OrderJSON = OrderJSON.get("data");

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, OrderJSON, deliveryAuthString);
			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testOrderEdit" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testOrderEdit" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	/* API - To get DE Order Details */

	@DataProvider(name = "deorder")
	public static Object[][] getdeorderdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("deorder");
	}

	@Test(priority = 25, groups = "Sanity", dataProvider = "deorder", enabled = true)
	public void testdeorderdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdeorderdetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdeorderdetails" + sanityTest);
	}

	@DataProvider(name = "debatch")
	public static Object[][] getdebatchdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("debatch");
	}

	@Test(priority = 26, groups = "Sanity", dataProvider = "debatch", enabled = true)
	public void testdebatchdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdebatchdetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdebatchdetails" + sanityTest);
	}

	/* API - To get Contract URLs */

	@DataProvider(name = "contracturls")
	public static Object[][] contracurls() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("contracturls");
	}

	@Test(priority = 27, groups = "Sanity", dataProvider = "contracturls", enabled = true)
	public void testdecontracturls(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdecontracturls" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdecontracturls" + sanityTest);
	}

	/* API - To get Delivery Boy Bank details */

	@DataProvider(name = "debankdetails")
	public static Object[][] debankdetails()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("debankdetails");
	}

	@Test(priority = 28, groups = "Sanity", dataProvider = "debankdetails")
	public void testdebankdetails(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdebankdetails" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdebankdetails" + sanityTest);
	}

	/* API - To get Delivery Boy Floating Cash */

	@DataProvider(name = "defloatingcash")
	public static Object[][] floatingcash() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("defloatingcash");
	}

	@Test(priority = 29, groups = "Sanity", dataProvider = "defloatingcash")
	public void testdefloatingcash(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdefloatingcash" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdefloatingcash" + sanityTest);
	}

	/* API - To get Floating Cash for a respective zone */

	@DataProvider(name = "zonefloatingcash")
	public static Object[][] getzonefloatingcash()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("zonefloatingcash");
	}

	@Test(priority = 30, groups = "Sanity", dataProvider = "zonefloatingcash")
	public void testdezonefloatingcash(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", STAFAgent.getSTAFValue("DeliveryZoneID"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdezonefloatingcash" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testdezonefloatingcash" + sanityTest);

	}

	/* API - To get order status */

	@DataProvider(name = "orderdelist")
	public static Object[][] getorderdelist()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("orderdelist");
	}

	@Test(priority = 31, groups = "Sanity", dataProvider = "orderdelist", dependsOnMethods = "testPlaceOrderAPI")
	public void testorderdelist(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", orderId);

		JsonNode updatedRequestData = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, updatedRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testorderdelist" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testorderdelist" + sanityTest);
	}

	@DataProvider(name = "OrderCollect")
	public static Object[][] getOrderCollect()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ORDERCOLLECT");
	}

	/**
	 * POST: Method to Update Order Collect from Customer
	 * 
	 * API @Param order_id and order_collect
	 * 
	 */
	@Test(priority = 31, dataProvider = "OrderCollect", groups = "SANITY", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testGetOrderCollect(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			HashMap<String, Object> update = new HashMap<>();
			update.put("order_id", orderId);
			update.put("order_collect", TotalIncoming);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetOrderCollect" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testGetOrderCollect" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetNotices")
	public static Object[][] getNotices() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETNOTICES");
	}

	/**
	 * GET: Method to Get Notices for DE APP
	 * 
	 */
	@Test(priority = 31, dataProvider = "GetNotices", groups = "SANITY", enabled = true)
	public void testGetNotices(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, dtid);

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testGetNotices" + smokeTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "GetSignUpSlots")
	public static Object[][] getSignUpSlots()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETSIGNUPSLOTS");
	}

	/**
	 * GET: Method to Get Notices for DE APP
	 * 
	 */
	@Test(priority = 31, dataProvider = "GetSignUpSlots", groups = "SANITY", enabled = true)
	public void testGetSignUpSlots(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
					serviceNameMap.get("delivery"), apiToExecute,
					jsonRequestData, dtid);

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);

			Assert.assertTrue(success, "testGetSignUpSlots" + smokeTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "UnAssignBatch")
	public static Object[][] UnAssignBatch()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Unassignbatch");
	}

	@Test(priority = 32, groups = "Sanity", dataProvider = "UnAssignBatch", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testUnAssignBatch(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		Long Batch_id = DeliveryCommon.getBatchIdfromDB(orderId);
		Assert.assertTrue(Batch_id > 0, "Unable to Fetch BatchId from Database");

		String apiToExecute1 = "ASSIGNBATCH";

		update.put("batch_id", Batch_id);
		update.put("de_id", STAFAgent.getSTAFValue("delivery_Boy_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		// Assigning Batch for a DE
		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute1, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		// Assert.assertTrue(success,
		// "testUnAssignBatch Assign Batch Failed" + smokeTest);

		// State change in Delivery system takes time
		Thread.sleep(5000);

		// UnAssigning Batch for a DE
		response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedrequest, deliveryAuthString);

		Assert.assertTrue(success, "testUnAssignBatch" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testUnAssignBatch" + sanityTest);

	}

	@DataProvider(name = "AssignBatch")
	public static Object[][] AssignBatch() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("assignbatch");
	}

	@Test(priority = 33, groups = "Sanity", dataProvider = "AssignBatch", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testDEAssignBatch(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		Long batch_id = DeliveryCommon.getBatchIdfromDB(orderId);
		Assert.assertTrue(batch_id > 0, "Unable to get BatchId from Database");

		update.put("batch_id", batch_id);
		update.put("de_id", STAFAgent.getSTAFValue("delivery_Boy_id"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		// Delivery takes time for state change
		Thread.sleep(5000);

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEAssignBatch" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEAssignBatch" + sanityTest);

	}

	@DataProvider(name = "UnAssignDe")
	public static Object[][] UnAssignDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UNASSIGNDE");
	}

	@Test(priority = 34, groups = "Sanity", dataProvider = "UnAssignDe", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testUnAssignDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		String apiToExecute1 = "ASSIGNDE";
		boolean success;

		update.put("delivery_boy_id", STAFAgent.getSTAFValue("delivery_Boy_id"));
		update.put("order_id", orderId);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		// Since Order Updation status takes time in delivery system
		Thread.sleep(5000);

		// API @Param Assign an DE using Assign DE API
		JsonNode response1 = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute1, updatedRequest,
				deliveryAuthString);

		log.info(response1);

		// Since Order Updation status takes time in delivery system
		Thread.sleep(5000);

		// UnAssign a DE for a Order using Unassign API
		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedRequest, deliveryAuthString);

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
		Assert.assertTrue(success, "testUnAssignDE" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testUnAssignDE" + sanityTest);

	}

	@DataProvider(name = "AssignDe")
	public static Object[][] AssignDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("AssignDe");
	}

	@Test(priority = 35, groups = "Sanity", dataProvider = "AssignDe", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testAssignDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();

		update.put("delivery_boy_id", STAFAgent.getSTAFValue("delivery_Boy_id"));
		update.put("order_id", orderId);

		// Since Order Updation status takes time in delivery system
		Thread.sleep(5000);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedRequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testAssignDE" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(success, "testAssignDE" + sanityTest);

	}

	@DataProvider(name = "OrderTrack")
	public static Object[][] OrderTrack() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TRACKING");
	}

	@Test(priority = 36, groups = "Sanity", dataProvider = "OrderTrack", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testOrderTrack(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", orderId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExceute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testOrderTrack" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testOrderTrack" + sanityTest);

	}

	@DataProvider(name = "AppendMessage")
	public static Object[][] AppendMsg() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("APPENDMESSAGE");
	}

	@Test(priority = 36, groups = "Sanity", dataProvider = "AppendMessage", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testAppendMessage(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		if (System.getProperty("env").equalsIgnoreCase("stage")) {

			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("order_id", orderId);// orderId

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExceute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testAppendMessage" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testAppendMessage" + sanityTest);
		}
	}

	@DataProvider(name = "OrderAck")
	public static Object[][] OrderAck() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OrderAck");
	}

	@Test(priority = 37, groups = "Sanity", dataProvider = "OrderAck", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testOrderAck(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("order_ids", orderId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updatedrequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testOrderAck" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testOrderAck" + sanityTest);

	}

	@DataProvider(name = "DEConfirm")
	public static Object[][] DEConfirm() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEConfirmStatus");
	}

	@Test(priority = 38, groups = "Sanity", dataProvider = "DEConfirm", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testConfirmDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since Order Updation status takes time in delivery system
		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();

		update.put("status", "confirmed");
		update.put("order_id", orderId);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testConfirmDE" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testConfirmDE" + sanityTest);

	}

	@DataProvider(name = "DELazyUpdate")
	public static Object[][] DELazyUpdate() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("LazyStatusUpdate");
	}

	@Test(priority = 39, groups = "Sanity", dataProvider = "DELazyUpdate", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testLazyUpdate(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();
		HashMap<String, Object> data = new HashMap<>();

		data.put("bill", TotalBill);
		data.put("pay", TotalSpending);
		data.put("collect", TotalIncoming);

		update.put("status", "confirmed");
		update.put("order_id", orderId);
		update.put("data", data);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testLazyUpdate" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testLazyUpdate" + sanityTest);

	}

	@DataProvider(name = "DEArrived")
	public static Object[][] DEArrived() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEArrived");
	}

	@Test(priority = 39, groups = "Sanity", dataProvider = "DEArrived", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testDEArrived(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since Order Updation status takes time in delivery system
		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();

		update.put("status", "arrived");
		update.put("order_id", orderId);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "DEArrived" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEArrived" + sanityTest);

	}

	@DataProvider(name = "DEpickedUp")
	public static Object[][] DEpickedUp() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEPickedUp");
	}

	@Test(priority = 40, groups = "Sanity", dataProvider = "DEpickedUp", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testDEPickedUp(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since Order Updation status takes time in delivery system
		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();
		HashMap<String, Object> data = new HashMap<>();

		data.put("bill", TotalBill);
		data.put("pay", TotalSpending);
		data.put("collect", TotalIncoming);

		update.put("status", "pickedup");
		update.put("order_id", orderId);
		update.put("data", data);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEPickedUp" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEPickedUp" + sanityTest);

	}

	@DataProvider(name = "DEReached")
	public static Object[][] DEReached() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEReached");
	}

	@Test(priority = 41, groups = "Sanity", dataProvider = "DEReached", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testDEReached(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since Order Updation status takes time in delivery system
		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();

		update.put("status", "reached");
		update.put("order_id", orderId);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"),apiToExceute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEReached" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEReached" + sanityTest);

	}

	@DataProvider(name = "CallDE")
	public static Object[][] DECall() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DECALLME");
	}

	@Test(priority = 41, groups = "Sanity", dataProvider = "CallDE", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testCallDE(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		if (System.getProperty("env").equalsIgnoreCase("stage")) {
			HashMap<String, Object> update = new HashMap<>();
			update.put("pathvariable", orderId);

			JsonNode updaterequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updaterequest, dtid);

			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);

			Assert.assertTrue(success, "testCallDE" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testCallDE" + sanityTest);
		}

	}

	@DataProvider(name = "DEDelivered")
	public static Object[][] DEDelivered() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEDelivered");
	}

	@Test(priority = 42, groups = "Sanity", dataProvider = "DEDelivered", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testDEDelivered(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since Order Updation status takes time in delivery system
		Thread.sleep(10000);

		HashMap<String, Object> update = new HashMap<>();
		HashMap<String, Object> data = new HashMap<>();

		data.put("bill", TotalBill);
		data.put("pay", TotalSpending);
		data.put("collect", TotalIncoming);

		update.put("status", "delivered");
		update.put("order_id", orderId);
		update.put("data", data);

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExceute, updatedRequest, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEDelivered" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEDelivered" + sanityTest);

	}

	@DataProvider(name = "DEOrderHistory")
	public static Object[][] DEOrderHistory()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEOrderHistory");
	}

	@Test(priority = 43, groups = "Sanity", dataProvider = "DEOrderHistory", enabled = true)
	public void testDEOrderHistory(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExceute, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEOrderHistory" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDEOrderHistory" + sanityTest);
	}

	@DataProvider(name = "GetOrderLocation")
	public static Object[][] getDEOrderLocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEORDERLOCATION");
	}

	/**
	 * Method for get the Order location GET API
	 * 
	 * API @Param order_id
	 */
	@Test(priority = 44, groups = "Sanity", dataProvider = "GetOrderLocation", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testGetDEOrderLocation(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", orderId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEOrderLocation" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testGetDEOrderLocation" + sanityTest);
	}

	/* API - Update DE Spending from OMS */

	@DataProvider(name = "updatedespending")
	public static Object[][] updatedespending()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("updatedespending");
	}

	@Test(priority = 45, groups = "Sanity", dataProvider = "updatedespending", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testupdatedespending(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<String, Object>();
		boolean success = false;

		update.put("bill", TotalBill);
		update.put("pay", TotalSpending);
		update.put("collect", TotalIncoming);
		update.put("order_id", orderId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedrequest, deliveryAuthString);

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testupdatedespending" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testupdatedespending" + sanityTest);

	}

	/* API - Update DE Incoming from OMS */

	@DataProvider(name = "updatedeincoming")
	public static Object[][] updatedeincoming()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("updatedeincoming");
	}

	@Test(priority = 46, groups = "Sanity", dataProvider = "updatedeincoming", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testupdatedeincoming(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<String, Object>();
		boolean success = false;

		update.put("bill", TotalBill);
		update.put("pay", TotalSpending);
		update.put("collect", TotalIncoming);
		update.put("order_id", orderId);

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, updatedrequest, deliveryAuthString);

		success = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(success, "testupdatedeincoming" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testupdatedeincoming" + sanityTest);

	}

	@DataProvider(name = "DELocation")
	public static Object[][] ChangeDELocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Change_DELocation");
	}

	@Test(priority = 47, groups = "Sanity", dataProvider = "DELocation", enabled = true)
	public void testDELocation(String dataGroup, String apiToExceute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExceute, jsonRequestData, dtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDELocation" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testDELocation" + sanityTest);

	}

	/* API - To get order status */

	@DataProvider(name = "Ordstatus")
	public static Object[][] getdeorderstatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OrderStatus");
	}

	@Test(priority = 48, groups = "Sanity", dataProvider = "Ordstatus", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testdeorderstatus(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		HashMap<String, Object> update = new HashMap<>();
		update.put("pathvariable", orderId);

		JsonNode updatedRequestData = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedRequestData,
				deliveryAuthString);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdeorderstatus" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(success, "testdeorderstatus" + sanityTest);

	}

	@DataProvider(name = "GetConfig")
	public static Object[][] getdeConfig() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETDECONFIGS");
	}

	@Test(priority = 49, groups = "Sanity", dataProvider = "GetConfig")
	public void testdeGetConfig(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, jsonRequestData, dtid);

		boolean succcess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(succcess);

		succcess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(succcess, "testdeGetConfig" + sanityTest);
	}

	@DataProvider(name = "DEOrderCancel")
	public static Object[][] CancelOrder() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEORDERCANCEL");
	}

	@Test(priority = 49, dataProvider = "DEOrderCancel", groups = "SANITY", enabled = true, dependsOnMethods = "testPlaceOrderAPI")
	public void testOrderCancel(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("order_id", orderId);

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testOrderCancel" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testOrderCancel" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	/* API - Delivery Boy Logout */

	@DataProvider(name = "delogout")
	public static Object[][] getdelogout() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("delogout");
	}

	@Test(priority = 50, groups = "Sanity", dataProvider = "delogout")
	public void testdelogout(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
				apiToExecute, jsonRequestData, dtid);

		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);

		Assert.assertTrue(b, "testdelogout" + smokeTest);

		b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(b, "testdelogout" + sanityTest);
	}

	@DataProvider(name = "DELIVERY_BOY_SERVICESLOGOUT")
	public static Object[][] getDeliveryServiceLogout()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEServicesLogout");

	}

	@Test(priority = 51, dataProvider = "DELIVERY_BOY_SERVICESLOGOUT")
	public void testMakeDeliveryBoyServiceLogout(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		try {

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "id",
					STAFAgent.getSTAFValue("delivery_Boy_id"));

			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			boolean success = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			log.info(apiToExecute);

			Assert.assertTrue(success, "testMakeDeliveryBoyServiceLogout"
					+ smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(success, "testMakeDeliveryBoyServiceLogout"
					+ sanityTest);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	/* API - Assign Batch to Delivery Boy */

	@DataProvider(name = "assignbatch")
	public static Object[][] assignbatch() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("assignbatch");
	}

	// @Test(priority = 4, groups = "Sanity", dataProvider = "assignbatch")
	public void assingbatch(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {

			HashMap<String, String> map = dbconfig
					.executeQuery("select * from batch order by 1 desc limit 1");

			String batchid = map.get("id");
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "batch_id",
							batchid);

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),apiToExecute,
					updatedRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	/* API - Merge two batches */

	@DataProvider(name = "mergebatches")
	public static Object[][] mergebatch() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("mergebatches");
	}

	// @Test(priority = 18, groups = "Sanity", dataProvider = "mergebatches")
	public void testmergebatch(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),apiToExecute,
						jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));

		boolean b = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(b);
	}

	/* API - UnAssign Batch */

	@DataProvider(name = "unassignbatch")
	public static Object[][] unassignbatch()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("unassignbatch");
	}

	// @Test(priority = 14, groups = "Sanity", dataProvider = "unassignbatch")
	public void unassingbatch(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("aditya");
		try {

			log.info("amazing");

			HashMap<String, String> map = dbconfig
					.executeQuery("select * from batch order by 1 desc limit 1");
			log.info("aditya singh" + map);
			String batchid = map.get("id");
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "batch_id",
							batchid);
			log.info("Aditya" + updatedRequestData);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),apiToExecute,
					updatedRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	/* API - Make DE Active */

	@DataProvider(name = "makedeactive")
	public static Object[][] makedeactive() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("makedeactive");
	}

	// @Test(priority = 10, groups = "Sanity", dataProvider = "makedeactive")
	public void testmakedeactive(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"),apiToExecute,
						jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));

		boolean b = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(b);
	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		if (orderIds.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : orderIds) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
					oms_osrftoken);
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedResponseData, actualData, new String[] { "status",
							"message" });
			Thread.sleep(20000);
		}

	}

}
