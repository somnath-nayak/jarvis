package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DeliveryRegression extends RestTestHelper {
	public final static Logger logger = Logger
			.getLogger(DEServicesRegression.class);

	public static String delivery_host = null;
	public static Integer delivery_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "cart_sla")
	public static Object[][] cartsla() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("cart_sla", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "cart_sla", groups = "REGRESSION", enabled = false)
	public void cartsla(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("Valid data")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("Invalid data")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("City id null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("invalid restid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("rest id null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("invalid areaid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("areaid null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("totalitemcountstringinvalid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("listingitemcountStringinvalid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("listingitemcountStringinvalid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("lastmiledistancenull")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}
			if (dataSetName.equalsIgnoreCase("customeraddressnull")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);
			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "listing_sla")
	public static Object[][] listsla() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("listing_sla", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "listing_sla", groups = "REGRESSION", enabled = false)
	public void listsla(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("Valid data")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("Invalid data")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("City id null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("customerlatlongnull")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("invalidrestid")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("restidnull")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("restid as string")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("Invalid area id")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("area id null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("last mile null")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "testcartsla" + sanityTest);

			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "Auto_Assign_updateparams")
	public static Object[][] autoassignupdateparam()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Auto_Assign_updateparams",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "Auto_Assign_updateparams", groups = "REGRESSION", enabled = true)
	public void autoassignupdateparam(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("valid inputs")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Autoassignparamupdate" + sanityTest);

			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "Order_Spending")
	public static Object[][] orderspeding() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Order_Spending", "REGRESSION");
	}

	@Test(priority = 4, dataProvider = "Order_Spending", groups = "REGRESSION", enabled = true)
	public void orderspeding(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("VALID INPUT")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}

			if (dataSetName.equalsIgnoreCase("INVALIDORDERID")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("PAYASVARIABLE")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("ORDERWITHVARIABLE")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("ORDERWITHVARIABLE")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("PAYASNULL")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order spending" + sanityTest);

			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "Order_Collect")
	public static Object[][] ordercollect() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Order_Collect", "REGRESSION");
	}

	@Test(priority = 5, dataProvider = "Order_Collect", groups = "REGRESSION", enabled = true)
	public void ordercollect(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("VALID INPUT")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order Collect" + sanityTest);

			}

			if (dataSetName.equalsIgnoreCase("INVALIDORDERID")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order Collect" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("COLLECTASVARIABLE")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order Collect" + sanityTest);

			}
			if (dataSetName.equalsIgnoreCase("ORDERWITHVARIABLE")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order Collect" + sanityTest);

			}

			if (dataSetName.equalsIgnoreCase("COLLECTASNULL")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);

				Assert.assertTrue(success, "Order Collect" + sanityTest);

			}
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "ORDER_ASSIGN")
	public static Object[][] assignorder() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ORDER_ASSIGN", "REGRESSION");
	}

	@Test(priority = 6, dataProvider = "ORDER_ASSIGN", groups = "REGRESSION", enabled = true)
	public void assignorder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"), STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"), STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(
					serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"), STAFAgent.getSTAFValue("defaultAuthString"));
			Long orderid = DeliveryCommon.orderId;

		} catch (Exception e) {
			e.getStackTrace();
		}
	}
}
