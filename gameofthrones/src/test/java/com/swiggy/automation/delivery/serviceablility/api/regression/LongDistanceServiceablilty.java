package com.swiggy.automation.delivery.serviceablility.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mysql.jdbc.Connection;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.delivery.api.sanity.DEAPISanityTest;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class LongDistanceServiceablilty extends RestTestHelper {

	public static final Logger log = Logger.getLogger(DEAPISanityTest.class);
	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	String Backend_host = null;
	Integer Backend_port = null;
	static Connection con;

	@BeforeClass
	public void beforeClass() {

		try {
	

		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}
	}

	@DataProvider(name = "DisableRainMode")
	public static Object[][] rainmodeenabled()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DisableRainMode", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "DisableRainMode", enabled = true)
	public void rainmodeenabled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "Enable heavy rain mode"
					+ regressionTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "LongDistance")
	public static Object[][] longdistance() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("LongDistance", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "LongDistance", enabled = true)
	public void longdistance(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("Increasing max SLA")) {
				JsonNode response = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Increasing Max sla"
						+ regressionTest);
			}

			if (dataSetName.equalsIgnoreCase("ListingMAXLASTMILEExceeds")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max Last mile Exceeds for Lisitng"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("CartMAXLASTMILEExceeds")) {

				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max Last mile Exceeds for cart"
						+ regressionTest);
			}

			if (dataSetName.equalsIgnoreCase("Reducing max SLA")) {
				JsonNode response = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success,
						"Max SLA decreasing for long distance polygon"
								+ regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("ListingMAXSLAExceeds")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for listing"
						+ regressionTest);
			}

			if (dataSetName.equalsIgnoreCase("CartMAXSLAExceeds")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("Increasing max SLA")) {
				JsonNode response = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Increasing Max sla"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("Serviceabe check Lisitng")) {
				Thread.sleep(5000);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("Serviceabe check Cart")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName
					.equalsIgnoreCase("Listinglastmilelessthanarealastmile")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName
					.equalsIgnoreCase("Cartlastmilelessthanarealastmile")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}

			if (dataSetName
					.equalsIgnoreCase("CustomerLatLongNotfallingListing")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("CustomerLatLongNotfallingCart")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("CustomerLatLongNotfallingCart")) {
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("BFEXCEEDSLONGDISTANCELIST")) {
				String query = "update zone set is_open=0 where id=8";
				con = (Connection) DBUtil.getConnection(IDBDriverType.MYSQL,
						"54.255.176.227", "delivery", "root",
						"notarealpassword");

				DBUtil.executeupdatequery(con, query);
				Thread.sleep(10000);

				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));
				JsonNode response1 = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
			}

			if (dataSetName.equalsIgnoreCase("BFEXCEEDSLONGDISTANCECART")) {
				String query1 = "update zone set is_open=1 where id=8";

				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString"));

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Max SLA Exceeds for CART"
						+ regressionTest);
				DBUtil.executeupdatequery(con, query1);
				DBUtil.close(con);
				Thread.sleep(10000);

			}

		} catch (Exception e) {
			e.getStackTrace();
		}
	}
}
