package com.swiggy.automation.delivery.api.regression;

import java.util.ArrayList;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DERejectRegression extends RestTestHelper {

	private final static Logger log = Logger
			.getLogger(DERejectRegression.class);

	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String Backend_host = null;
	private static Integer Backend_port = null;
	private static String stopDtid = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@Test(priority = 0, enabled = true, groups = "REGRESSION")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			// It takes time for DE to become Free
			Thread.sleep(20000);
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "Order id is null");
			// Since Order Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 1, enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("checkout"),
					deliveryAuthString);
			success = true;
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFEndPoint("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "SystemReject")
	public static Object[][] SystemRejectOrder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("System_Reject", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "SystemReject", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testSystemRejectOrder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				HashMap<String, Object> deorder = new HashMap<String, Object>();
				HashMap<String, Object> update = new HashMap<String, Object>();
				ArrayList<HashMap<String, Object>> list = new ArrayList<>();

				deorder.put("deId", DeliveryCommon.getAssignedMap().get("id"));
				deorder.put("orderId", DeliveryCommon.getOrderId());
				list.add(deorder);

				update.put("deOrderArray", list);

				JsonNode updaterequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updaterequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testSystemRejectOrder" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testSystemRejectOrder" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "DEOrderCancel")
	public static Object[][] CancelOrder() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DEORDERCANCEL", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "DEOrderCancel", groups = "REGRESSION", enabled = true, dependsOnMethods = "placeHolder")
	public void testOrderCancel(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("order_id", DeliveryCommon.getOrderId());

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
					deliveryAuthString);

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testOrderCancel" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testOrderCancel" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

}
