package com.swiggy.automation.delivery.serviceablility.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DEAPISanityTest;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEListingRegression extends RestTestHelper {

	public static final Logger log = Logger.getLogger(DEAPISanityTest.class);
	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	String Backend_host = null;
	Integer Backend_port = null;

	@BeforeClass
	public void beforeClass() {

		try {
			// oms_host = getEnvPropertyValue("oms_host");
			// oms_port = Integer.parseInt(getEnvPropertyValue("oms_port"));

			oms_osrftoken = STAFAgent.getSTAFValue("oms_osrftoken");
			oms_sessionid = STAFAgent.getSTAFValue("oms_sessionid");

		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	/*
	 * @DataProvider(name = "Delivery_Boy_Status") public static Object[][]
	 * getDEStatus() throws SwiggyAPIAutomationException { return
	 * APIDataReader.getDataSetData("Delivery_Boy_Status"); }
	 * 
	 * /** GET: Method to Get the Status of Delivery Boy GET API
	 * 
	 * Param @API mobile & token
	 */

	@DataProvider(name = "SlaRestaurantListing")
	public static Object[][] getControllerTrack()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SlaRestaurantListing",
				"REGRESSION");
	}

	@Test(priority = 0, dataProvider = "SlaRestaurantListing", enabled = true)
	public void slanormallisitng(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			/*
			 * JsonNode response =
			 * RestTestUtil.sendDataAsPathParam(delivery_host, delivery_port,
			 * apiToExecute, deliveryAuthString);
			 */

			JsonNode response = RestTestUtil.sendDataAsPathParam(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
					STAFAgent.getSTAFValue("defaultAuthString"));

			// success =
			// JsonValidator.HIGH_LEVEL.validateJson(expectedData,response);
			// Assert.assertTrue(success, "testDEStatus" + regressionTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "testDEStatus" + regressionTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}
}
