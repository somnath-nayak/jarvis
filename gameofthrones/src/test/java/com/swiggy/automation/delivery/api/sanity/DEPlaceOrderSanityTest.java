package com.swiggy.automation.delivery.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEPlaceOrderSanityTest extends RestTestHelper {

	public static final Logger log = Logger
			.getLogger(DEPlaceOrderSanityTest.class);

	/**
	 * 
	 * @author Aditya
	 * @throws SwiggyAPIAutomationException
	 */

	@DataProvider(name = "cartcreation")
	public static Object[][] CreateCart() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("cartcreation");
	}

	@Test(priority = 1, dataProvider = "cartcreation", groups = "Sanity")
	public void testCreateCart(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {

			JsonNode actualData = RestTestUtil.sendData(
					serviceNameMap.get("checkout"),
					
					apiToExecute, jsonRequestData, defaulTID, defaultTOKEN);

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			Assert.assertTrue(status, "testCreateCart");

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "getalladdress")
	public static Object[][] getAllAddresses()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("getalladdress");
	}

	@Test(priority = 2, dataProvider = "getalladdress", groups = "Sanity")
	public void testGetAllAddress(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			JsonNode actualData = RestTestUtil.sendData(
					serviceNameMap.get("checkout"),
					
					apiToExecute, defaulTID, defaultTOKEN);
			System.out.println("ABCD   :" + actualData);
			// JsonNode actualData = RestTestUtil.sendData(apiToExecute,
			// defaulTID,
			// defaultTOKEN);
			System.out.println(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			Assert.assertTrue(status, "testGetAllAddress");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "placeorder")
	public static Object[][] PlaceOrder() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("placeorder");
	}

	@Test(priority = 3, dataProvider = "placeorder", groups = "Sanity")
	public void testPlaceOrder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", 781835);
			System.out.println(jsonRequestData);
			System.out.println(updatedRequest);
			JsonNode actualData = RestTestUtil.sendData(
					serviceNameMap.get("checkout"),
					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
			// JsonNode placeOrderResponse = RestTestUtil.sendData(apiToExecute,
			// updatedRequest, defaulTID, defaultTOKEN);

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, actualData);
			Assert.assertTrue(status, "testPlaceOrderAPI");
			orderId = actualData.get("data").get("order_id").asLong();
			orderIds.add(orderId);

			System.out.println("ORDER ID :::" + orderId);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	// @DataProvider(name = "GET_ORDER_DETAILS")
	// public static Object[][] getOrderDetailsData() {
	// return APIDataReader.getDataSetData("GET_ORDER_DETAILS");
	// }
	//
	// @Test(priority = 4, dataProvider = "GET_ORDER_DETAILS")
	// public void testGetOrderDetailsAPI(String dataGroup, String apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// try {
	// JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
	// jsonRequestData, "order_id", orderId);
	// JsonNode getOrderDetailsResponse = RestTestUtil
	// .sendDataAsPathParam(apiToExecute, updatedRequest,
	// defaultAuthString);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(
	// expectedData, getOrderDetailsResponse);
	// Assert.assertTrue(status, "testGetOrderDetailsAPI");
	// } catch (Exception e) {
	// log.error(e);
	// Assert.fail();
	// }
	// }
	//
	// @DataProvider(name = "GET_SINGLE_ORDER")
	// public static Object[][] getSingleOrderData() {
	// return APIDataReader.getDataSetData("GET_SINGLE_ORDER");
	// }
	//
	// @Test(priority = 5, dataProvider = "GET_SINGLE_ORDER")
	// public void testGetSingleOrderAPI(String dataGroup, String apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// try {
	// JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
	// jsonRequestData, "order_id", orderId);
	// JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
	// apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(
	// expectedData, getSingleOrderResponse);
	// Assert.assertTrue(status, "testGetSingleOrderAPI");
	// } catch (Exception e) {
	// log.error(e);
	// Assert.fail();
	// }
	// }
	//
	// @DataProvider(name = "GET_ORDER_SUMMARY")
	// public static Object[][] getOrderSummary() {
	// return APIDataReader.getDataSetData("GET_ORDER_SUMMARY");
	// }
	//
	// @Test(priority = 6, dataProvider = "GET_ORDER_SUMMARY")
	// public void testGetOrderSummaryAPI(String dataGroup, String apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// try {
	// JsonNode getOrderSummary = RestTestUtil.sendData(apiToExecute);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(
	// expectedData, getOrderSummary);
	// Assert.assertTrue(status, "testGetOrderSummaryAPI");
	// } catch (Exception e) {
	// log.error(e);
	// Assert.fail();
	// }
	// }
	//
	// @DataProvider(name = "GET_MINIMAL_ORDER_DETAILS")
	// public static Object[][] getMinialOrderDetailsData() {
	// return APIDataReader.getDataSetData("GET_MINIMAL_ORDER_DETAILS");
	// }
	//
	// @Test(priority = 7, dataProvider = "GET_MINIMAL_ORDER_DETAILS")
	// public void testGetMinmalOrderAPI(String dataGroup, String apiToExecute,
	// String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
	// throws Exception {
	// try {
	// JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
	// jsonRequestData, "order_key", orderKey);
	// JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
	// apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
	// boolean status = JsonValidator.HIGH_LEVEL.validateJson(
	// expectedData, getSingleOrderResponse);
	// Assert.assertTrue(status, "testGetSingleOrderAPI");
	// } catch (Exception e) {
	// log.error(e);
	// Assert.fail();
	// }
	// }
	//
	// @AfterClass
	// public void cancelOrders() {
	//
	// }
}
