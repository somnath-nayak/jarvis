package com.swiggy.automation.delivery.api.sanity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class dbconfig {
	static Statement stmt;
	static Connection con;
	static String assetname, maintitle, status1;
	static ResultSetMetaData rsMetaData;
	static ResultSet resultset;
	static String dbUrl = "jdbc:mysql://ff-u-0014-server-01.swiggyops.de:3306/delivery";
	static String username = "mayur";
	static String password = "mayur";
	static String dbClass = "com.mysql.jdbc.Driver";
	static HashMap<Object, Object> map;

	public static Connection connect() throws ClassNotFoundException,
			SQLException {

		Class.forName(dbClass);
		Connection con = DriverManager.getConnection(dbUrl, username, password);

		return con;

	}

	public static HashMap<String, String> executeQuery(String query)
			throws SQLException, ClassNotFoundException {
		HashMap<String, String> queryResult = new HashMap<String, String>();
		String key = null, value = null;
		con = dbconfig.connect();
		stmt = con.createStatement();
		resultset = stmt.executeQuery(query);
		rsMetaData = resultset.getMetaData();
		int numberOfColumns = rsMetaData.getColumnCount();
		while (resultset.next()) {
			for (int i = 1; i <= numberOfColumns; i++) {
				key = rsMetaData.getColumnName(i);
				value = resultset.getString(i);
				queryResult.put(key, value);
			}
		}
		stmt.close();
		return queryResult;

		// assetname=rs.getString(column);
		// map.put("Assettitle",assetname);
		// status1=rs.getString("status");
		// map.put("status",status1);

	}

	// catch(Exception e)
	// {

	// }
	// try {

	// rs.last();
	// size = rs.getRow();
	// rs.beforeFirst();
	// map.put("rowcount",size);
	// }
	// catch(Exception ex) {
	// System.out.println("Not able to get row count");
	// System.out.println(map.get("rowcount"));

	public static void dbclose() throws SQLException {

		con.close();
	} // end try

}

// end main

// end class
