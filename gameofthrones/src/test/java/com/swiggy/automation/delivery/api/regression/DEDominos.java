package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEDominos extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEPolygonRegression.class);

	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "CART_SLA1")
	public static Object[][] getCART_SLA1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CART_SLA1", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "CART_SLA1")
	public void CART_SLA1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
					throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			// success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
			// response);
			// Assert.assertTrue(success, "testGetPolygonById" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, dataSetName + regressionTest);

			// if (dataSetName.contentEquals("MAX_SLA_NEGATIVE")) {
			// Assert.assertTrue(success,
			// "CART_SLA1_NEGATIVE" + regressionTest);
			// } else if (dataSetName.contentEquals("MAX_SLA")) {
			// Assert.assertTrue(success, "CART_SLA1" + regressionTest);
			// }
			//
			// else {
			// Assert.assertTrue(success, "Serviceablity_2" + regressionTest);
			// }

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "LIST_SLA1")
	public static Object[][] getLIST_SLA1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("LIST_SLA1", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "LIST_SLA1")
	public void LIST_SLA1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
					throws Exception {
		boolean success = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					deliveryAuthString);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, dataSetName + regressionTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
