package com.swiggy.automation.delivery.api.sanity;

import java.sql.Connection;
import java.sql.ResultSet;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.backend.coreapi.sanity.BackendCriticalSanityTest;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * @Warning: This Class should be Executed only in UAT since creating & updating
 *           of zone is not allowed in Production Environment.
 * 
 *           Run this class only in UAT Environment
 *
 */
public class DEAPIPartnerSanityTest extends RestTestHelper {
	public static final Logger log = Logger
			.getLogger(BackendCriticalSanityTest.class);

	@DataProvider(name = "DE_PARTNER_ADD_CITY")
	public static Object[][] departneraddcity()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_ADD_CITY");
		// return
		// APIDataReader.getDataSetDataWithAPIFields("DE_PARTNER_ADD_CITY");
	}

	@Test(priority = 1, groups = "Sanity", dataProvider = "DE_PARTNER_ADD_CITY", enabled = false)
	public void departneraddcity(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)// JsonNode
																				// apiFieldsData
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_ADD_ZONE")
	public static Object[][] departneraddzone()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_ADD_ZONE");
	}

	@Test(priority = 2, groups = "Sanity", dataProvider = "DE_PARTNER_ADD_ZONE", enabled = false)
	public void departneraddzone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_ADD_PARTNER")
	public static Object[][] departneraddpartner()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_ADD_PARTNER");
	}

	@Test(priority = 3, groups = "Sanity", dataProvider = "DE_PARTNER_ADD_PARTNER", enabled = false)
	public void departneraddpartner(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_UPDATE_CITY")
	public static Object[][] departnerupdatecity()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_UPDATE_CITY");
	}

	@Test(priority = 4, groups = "Sanity", dataProvider = "DE_PARTNER_UPDATE_CITY", enabled = false)
	public void departnerupdatecity(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_UPDATE_ZONE")
	public static Object[][] departnerupdatezone()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_UPDATE_ZONE");
	}

	@Test(priority = 5, groups = "Sanity", dataProvider = "DE_PARTNER_UPDATE_ZONE", enabled = true)
	public void departnerupdatezone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_DELETE_ZONE_MAP")
	public static Object[][] departnerdeletezone()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_DELETE_ZONE_MAP");
	}

	@Test(priority = 6, groups = "Sanity", dataProvider = "DE_PARTNER_DELETE_ZONE_MAP", enabled = false)
	public void departnerdeletezone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			Connection conn = null;
			ResultSet rs = null;
			String s = null;
			String Query = "Select * from partner_zone_map order by 1 desc limit 1 id";
			conn = DBUtil.getConnection(IDBDriverType.MYSQL,
					STAFAgent.getSTAFValue("prod_delivery_db_host"),
					STAFAgent.getSTAFValue("prod_delivery_db_name"),
					STAFAgent.getSTAFValue("prod_delivery_db_username"),
					STAFAgent.getSTAFValue("prod_delivery_db_password"));

			rs = DBUtil.executeQuery(conn, Query);

			while (rs.next()) {
				s = rs.getString(0);
			}

			// String s = DbConnection.execute(
			// "Select * from partner_zone_map order by 1 desc limit 1",
			// "id");
			int i = Integer.parseInt(s);
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData,
							"partner_zone_map_id", i);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, updatedRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "DE_PARTNER_DELETE_CITY_MAP")
	public static Object[][] departnerdeletecitymap()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_PARTNER_DELETE_CITY_MAP");
	}

	@Test(priority = 7, groups = "Sanity", dataProvider = "DE_PARTNER_DELETE_CITY_MAP", enabled = false)
	public void departnerdeletecitymap(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			Connection conn = null;
			ResultSet rs = null;
			String s = null;
			String Query = "Select * from partner_zone_map order by 1 desc limit 1 id";
			conn = DBUtil.getConnection(IDBDriverType.MYSQL,
					STAFAgent.getSTAFValue("prod_delivery_db_host"),
					STAFAgent.getSTAFValue("prod_delivery_db_name"),
					STAFAgent.getSTAFValue("prod_delivery_db_username"),
					STAFAgent.getSTAFValue("prod_delivery_db_password"));

			rs = DBUtil.executeQuery(conn, Query);

			while (rs.next()) {
				s = rs.getString(0);
			}
			// String s = DbConnection.execute(
			// "Select * from partner_city_map order by 1 desc limit 1",
			// "id");
			int i = Integer.parseInt(s);
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData,
							"partner_city_map_id", i);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delverycontroller"),
					apiToExecute, updatedRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

}