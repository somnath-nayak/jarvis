package com.swiggy.automation.delivery.api.regression;

import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.delivery.api.sanity.ZipDialTest;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEZipdialRegression extends RestTestHelper {

	public final static Logger logger = Logger.getLogger(ZipDialTest.class);
	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 0, enabled = true, groups = "REGRESSION")
	public static void placeOrder() {
		log.info("Placing Order from Backend");
		try {
			// It takes time for DE To become Free
			Thread.sleep(40000);
			DeliveryCommon.LoginToBackend(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "Order id is null");
			Thread.sleep(5000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 1, enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			// Assigning will take time
			Thread.sleep(10000);
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("delivery"),
					deliveryAuthString);
			success = true;
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFEndPoint("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "ZIPDIAL-CONFIRMED")
	public static Object[][] getZipdialConfirmed()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialConfirmed", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "ZIPDIAL-CONFIRMED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialConfirmed(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		JsonNode response = null;
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialConfirmed" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialConfirmed" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "ZIPDIAL-ARRIVED")
	public static Object[][] getZipdialArrived()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialArrived", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "ZIPDIAL-ARRIVED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialArrived(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		JsonNode response = null;
		try {

			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialArrived" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialArrived" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "ZIPDIAL-PICKEDUP")
	public static Object[][] getZipdialPickedUp()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialPickedup", "REGRESSION");
	}

	@Test(priority = 4, dataProvider = "ZIPDIAL-PICKEDUP", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialPickedUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialPickedUp" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialPickedUp" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "ZIPDIAL-REACHED")
	public static Object[][] getZipdialReached()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialReached", "REGRESSION");
	}

	@Test(priority = 5, dataProvider = "ZIPDIAL-REACHED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialReached(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialReached" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialReached" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "ZIPDIAL-DELIVERED")
	public static Object[][] getZipdialDelivered()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialDelivered", "REGRESSION");
	}

	@Test(priority = 6, dataProvider = "ZIPDIAL-DELIVERED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialDelivered(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialDelivered" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialDelivered" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "ZIPDIAL-REJECTED")
	public static Object[][] getZipdialRejected()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ZipDialRejected", "REGRESSION");
	}

	@Test(priority = 7, dataProvider = "ZIPDIAL-REJECTED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeOrder")
	public void testZipdialRejected(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialRejected" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testZipdialRejected" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", DeliveryCommon.getOrderId());
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "time", System.currentTimeMillis() / 1000);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
				oms_osrftoken);
		JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedResponseData, actualData, new String[] { "status",
						"message" });
		Thread.sleep(20000);
	}

}
