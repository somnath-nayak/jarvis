package com.swiggy.automation.delivery.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEStartStopDuty extends RestTestHelper {

	private final static Logger log = Logger.getLogger(DEStartStopDuty.class);

	private static String delivery_host = null;
	private static Integer delivery_port = null;
	private static String stopDtid = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	/**
	 * Method to DE Login & get the DE TID
	 */
	@Test(groups = "Sanity", priority = 0, enabled = true)
	public void DELogin() {
		DeliveryCommon.DELogin(serviceNameMap.get("delivery"),
				deliveryAuthString);
		DeliveryCommon.GetOtp(serviceNameMap.get("delivery"),deliveryAuthString);
		DeliveryCommon.ValidateOTP(serviceNameMap.get("delivery"),
				deliveryAuthString);
		Assert.assertTrue(DeliveryCommon.getOTP() > 0L, "Unable to Login");
		log.info("DE Login Successful");

	}

	@DataProvider(name = "Start-Duty")
	public static Object[][] StartDuty() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Start-Duty");
	}

	/**
	 * POST: Method To Start DE Duty
	 * 
	 * API @Param DE TID from Login API
	 *
	 */
	@Test(priority = 1, groups = "Sanity", dataProvider = "Start-Duty", enabled = true)
	public void testdeStartDuty(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, DeliveryCommon.getDEAuth());

		stopDtid = actualData.get("data").get("Authorization").asText();

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdeStartDuty" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdeStartDuty" + sanityTest);
	}

	@DataProvider(name = "Stop-Duty")
	public static Object[][] StopDuty() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Stop-Duty");
	}

	/**
	 * POST: Method To Stop DE Duty
	 * 
	 * API @Param DE TID from Start Duty API
	 *
	 */
	@Test(priority = 2, groups = "Sanity", dataProvider = "Stop-Duty", enabled = true)
	public void testdeStopDuty(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, stopDtid);

		boolean success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(success, "testdeStopDuty" + smokeTest);

		success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(success, "testdeStopDuty" + sanityTest);
	}

}
