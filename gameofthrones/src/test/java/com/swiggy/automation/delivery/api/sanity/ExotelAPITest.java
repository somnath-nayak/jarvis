package com.swiggy.automation.delivery.api.sanity;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Exotel : Third party
 * 
 * @author kiran.j
 *
 */
public class ExotelAPITest extends RestTestHelper {

	public final static Logger logger = Logger.getLogger(ExotelAPITest.class);
	public final static HashMap<String, String> exotelMap = new HashMap<>();
	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String delivery_mobile_no = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;

	@BeforeClass
	public void beforeClass() {

		exotelMap.put("CONFIRMED", "08033545109");
		exotelMap.put("ARRIVED", "08033013122");
		exotelMap.put("PICKEDUP", "08033545112");
		exotelMap.put("DELIVERED", "08030752906");
		exotelMap.put("REJECTED", "08039510686");
		exotelMap.put("CALL_CUSTOMER_KEY", "08039513466");
		exotelMap.put("CALL_ME_KEY", "08039513064");
		exotelMap.put("REACHED", "08039658618");
		exotelMap.put("confirmHubTransaction", "");
		try {
			// It takes time for DE to become free
			Thread.sleep(40000);
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 0, enabled = true, groups = "Sanity")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {

			DeliveryCommon.LoginToBackend(serviceNameMap.get("sand"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "order id is null");
			// Since Order Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("oms"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "EXOTEL-CONFIRM-HUB-TRAN")
	public static Object[][] getExotelConfirmHub()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Confirm-Hub-Transaction");
	}

	@Test(priority = 2, dataProvider = "EXOTEL-CONFIRM-HUB-TRAN", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testlExotelConfirmHub(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		if (System.getProperty("env").equalsIgnoreCase("stage")) {
			HashMap<String, Object> update = new HashMap<String, Object>();
			update.put("From", delivery_mobile_no);
			update.put("To", exotelMap.get("confirmHubTransaction"));

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, update);

			ClientResponse response = RestTestUtil
					.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
							deliveryAuthString);

			System.out.println("Response is " + response);

			success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
					expectedData, response);
			Assert.assertTrue(success, "testlExotelConfirmHub" + smokeTest);
		}
	}

	@Test(priority = 1, enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			// Assigning will take time
			Thread.sleep(20000);
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("delivery"),
					deliveryAuthString);
			success = true;
			HashMap<String, String> map = DeliveryCommon.getAssignedMap();
			delivery_mobile_no = "0" + map.get("mobile");
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFValue("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "EXOTEL-CONFIRMED")
	public static Object[][] getExotelConfirmed()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Confirmed");
	}

	/**
	 * Method to Mark Order Confirmed from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 2, dataProvider = "EXOTEL-CONFIRMED", enabled = true, dependsOnMethods = "placeHolder")
	public void testExotelConfirmed(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("CONFIRMED"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelConfirmed" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-ARRIVED")
	public static Object[][] getExotelArrived()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Arrived");
	}

	/**
	 * Method to Mark Order Arrived from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 3, dataProvider = "EXOTEL-ARRIVED", enabled = true, dependsOnMethods = "placeHolder")
	public void testExotelArrived(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("ARRIVED"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelArrived" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-PICKEDUP")
	public static Object[][] getExotelPickedUp()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Pickedup");
	}

	/**
	 * Method to Mark Order PickedUp from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 4, dataProvider = "EXOTEL-PICKEDUP", enabled = true, dependsOnMethods = "placeHolder")
	public void testExotelPickedUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("PICKEDUP"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelPickedUp" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-REACHED")
	public static Object[][] getExotelReached()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Reached");
	}

	/**
	 * Method to Mark Order Reached from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 5, dataProvider = "EXOTEL-REACHED", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testExotelReached(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);
		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("REACHED"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);

		Assert.assertTrue(success, "testExotelReached" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-DELIVERED")
	public static Object[][] getExotelDelivered()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Delivered");
	}

	/**
	 * Method to Mark Order Delivered from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = 6, dataProvider = "EXOTEL-DELIVERED", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testlExotelDelivered(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// Since changing Order Status takes time
		Thread.sleep(10000);

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("DELIVERED"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelDelivered" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-CALLME")
	public static Object[][] getExotelCallMe()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Callme");
	}

	/**
	 * Method to DE Request for Call from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-CALLME", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testlExotelCallMe(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("CALL_ME_KEY"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallMe" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-CALLCUSTOMER")
	public static Object[][] getExotelCallCustomer()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-CallCustomer");
	}

	/**
	 * Method to Call Customer from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-CALLCUSTOMER", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testlExotelCallCustomer(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("CALL_CUSTOMER_KEY"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallCustomer" + smokeTest);

	}

	@DataProvider(name = "EXOTEL-REJECTED")
	public static Object[][] getExotelRejected()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Exotel-Rejected");
	}

	/**
	 * Method to Mark Order Rejected from Exotel
	 * 
	 * API @Param DE Mobile No & Status Code
	 * 
	 */
	@Test(priority = -1, dataProvider = "EXOTEL-REJECTED", enabled = true, groups = "Sanity", dependsOnMethods = "placeHolder")
	public void testlExotelRejected(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		HashMap<String, Object> update = new HashMap<String, Object>();
		update.put("From", delivery_mobile_no);
		update.put("To", exotelMap.get("REJECTED"));

		JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, update);

		ClientResponse response = RestTestUtil
				.sendDataAsPathParamNGetHttpStatus(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);

		System.out.println("Response is " + response);

		success = JsonValidator.HIGH_LEVEL.validateHttpResponse(expectedData,
				response);
		Assert.assertTrue(success, "testlExotelCallCustomer" + smokeTest);

	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", DeliveryCommon.getOrderId());
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "time", System.currentTimeMillis() / 1000);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
				oms_osrftoken);
		JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedResponseData, actualData, new String[] { "status",
						"message" });
		Thread.sleep(20000);
	}

}
