package com.swiggy.automation.delivery.api.sanity;

import java.sql.Connection;
import java.sql.ResultSet;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * @Warning: This Class should be Executed only in UAT since creating & updating
 *           of zone is not allowed in Production Environment.
 * 
 *           Run this class only in UAT Environment
 *
 */
public class DEAPISanitySetDetailedLevel extends RestTestHelper {

	@DataProvider(name = "DE_PARTNER_ADD_CITY")
	public static Object[][] departneraddcity()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetDataWithAPIFields("DE_PARTNER_ADD_CITY");
	}

	// @Test(priority = 1, groups = "Sanity", dataProvider =
	// "DE_PARTNER_ADD_CITY")
	public void departneraddcity(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData)

			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);

			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_ADD_ZONE")
	public static Object[][] departneraddzone()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetDataWithAPIFields("DE_PARTNER_ADD_ZONE");

	}

	// @Test(priority = 2, groups = "Sanity", dataProvider =
	// "DE_PARTNER_ADD_ZONE")
	public void departneraddzone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_ADD_PARTNER")
	public static Object[][] departneraddpartner()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetDataWithAPIFields("DE_PARTNER_ADD_PARTNER");

	}

	// @Test(priority = 3, groups = "Sanity", dataProvider =
	// "DE_PARTNER_ADD_PARTNER")
	public void departneraddpartner(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_UPDATE_CITY")
	public static Object[][] departnerupdatecity()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetDataWithAPIFields("DE_PARTNER_UPDATE_CITY");

	}

	// @Test(priority = 4, groups = "Sanity", dataProvider =
	// "DE_PARTNER_UPDATE_CITY")
	public void departnerupdatecity(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_UPDATE_ZONE")
	public static Object[][] departnerupdatezone()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetDataWithAPIFields("DE_PARTNER_UPDATE_ZONE");
	}

	// @Test(priority = 5, groups = "Sanity", dataProvider =
	// "DE_PARTNER_UPDATE_ZONE")
	public void departnerupdatezone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "DE_PARTNER_DELETE_ZONE_MAP")
	public static Object[][] departnerdeletezone()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetDataWithAPIFields("DE_PARTNER_DELETE_ZONE_MAP");

	}

	// @Test(priority = 6, groups = "Sanity", dataProvider =
	// "DE_PARTNER_DELETE_ZONE_MAP")
	public void departnerdeletezone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {
			Connection conn = null;
			ResultSet rs = null;
			String s = null;
			String Query = "Select * from partner_zone_map order by 1 desc limit 1 id";
			conn = DBUtil.getConnection(IDBDriverType.MYSQL,
					STAFAgent.getSTAFValue("prod_delivery_db_host"),
					STAFAgent.getSTAFValue("prod_delivery_db_name"),
					STAFAgent.getSTAFValue("prod_delivery_db_username"),
					STAFAgent.getSTAFValue("prod_delivery_db_password"));

			rs = DBUtil.executeQuery(conn, Query);

			while (rs.next()) {
				s = rs.getString(0);
			}
			// String s = DbConnection.execute(
			// "Select * from partner_zone_map order by 1 desc limit 1",
			// "id");
			int i = Integer.parseInt(s);
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData,
							"partner_zone_map_id", i);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, updatedRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "DE_PARTNER_DELETE_CITY_MAP")
	public static Object[][] departnerdeletecitymap()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetDataWithAPIFields("DE_PARTNER_DELETE_CITY_MAP");

	}

	// @Test(priority = 7, groups = "Sanity", dataProvider =
	// "DE_PARTNER_DELETE_CITY_MAP")
	public void departnerdeletecitymap(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData,
			JsonNode apiFieldsData) throws Exception {
		try {
			Connection conn = null;
			ResultSet rs = null;
			String s = null;
			String Query = "Select * from partner_zone_map order by 1 desc limit 1 id";
			conn = DBUtil.getConnection(IDBDriverType.MYSQL,
					STAFAgent.getSTAFValue("prod_delivery_db_host"),
					STAFAgent.getSTAFValue("prod_delivery_db_name"),
					STAFAgent.getSTAFValue("prod_delivery_db_username"),
					STAFAgent.getSTAFValue("prod_delivery_db_password"));

			rs = DBUtil.executeQuery(conn, Query);

			while (rs.next()) {
				s = rs.getString(0);
			}
			// String s = DbConnection.execute(
			// "Select * from partner_city_map order by 1 desc limit 1",
			// "id");
			int i = Integer.parseInt(s);
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData,
							"partner_city_map_id", i);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
					apiToExecute, updatedRequestData, "");

			boolean b = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response, apiFieldsData);
			Assert.assertTrue(b);

		} catch (Exception e) {
			Assert.fail();

		}

	}

	@DataProvider(name = "HEALTH_CHECK")
	public static Object[][] gethealthcheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetDataWithAPIFields("HEALTH_CHECK");
	}

	@Test(priority = 8, groups = "Sanity", dataProvider = "HEALTH_CHECK", enabled = true)
	public void checkhealth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)

			throws Exception {
		try {
			JsonNode response = RestTestUtil
					.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, " ");
//			boolean b = JsonValidator.DETAILED_LEVEL
//					.validateHttpResponse(expectedData, response);
			//Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();
        }
	}

}