package com.swiggy.automation.delivery.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEAlchemistRegression extends RestTestHelper {
	public final static Logger logger = Logger
			.getLogger(DEAlchemistRegression.class);

	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;
	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String delivery_alchemist_host = null;
	public static Integer delivery_alchemist_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			delivery_alchemist_host = getEndPoint(
//					"delivery_alchemist_host");
//			delivery_alchemist_port = Integer
//					.parseInt(getEndPoint("delivery_alchemist_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@Test(groups = "Sanity", priority = 0, enabled = true)
	public void testDELogin() {
		DeliveryCommon.DELogin(serviceNameMap.get("delivery"),
				deliveryAuthString);
		DeliveryCommon.GetOtp(serviceNameMap.get("delivery"), deliveryAuthString);
		DeliveryCommon.ValidateOTP(serviceNameMap.get("delivery"),
				deliveryAuthString);
		Assert.assertTrue(DeliveryCommon.getOTP() > 0L, "Unable to Login");
		log.info("DE Login Successful");

	}

	@DataProvider(name = "DEINCENTIVE-DAY")
	public static Object[][] getDeIncentive_Day()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEDAY", "REGRESSION");
	}

	@Test(priority = 1, groups = "REGRESSION", dataProvider = "DEINCENTIVE-DAY", enabled = true)
	public void checkDeInecentiveDay(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveDay" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveDay" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "DEINCENTIVE-Week")
	public static Object[][] getDeIncentive_Week()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEWEEK", "REGRESSION");
	}

	@Test(priority = 2, groups = "REGRESSION", dataProvider = "DEINCENTIVE-Week", enabled = true)
	public void checkDeInecentiveWeek(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveWeek" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveWeek" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "DEINCENTIVE-Month")
	public static Object[][] getDeIncentive_Month()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("INCENTIVEMONTH", "REGRESSION");
	}

	@Test(priority = 3, groups = "REGRESSION", dataProvider = "DEINCENTIVE-Month", enabled = true)
	public void checkDeInecentiveMonth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData,
					DeliveryCommon.getDEAuth());

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveMonth" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "checkDeInecentiveMonth" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

}
