package com.swiggy.automation.delivery.serviceablility.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mysql.jdbc.Connection;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DBUtil;
import com.swiggy.automation.common.utils.IDBDriverType;
import com.swiggy.automation.delivery.api.sanity.DEAPISanityTest;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RainModeServiceablility extends RestTestHelper {

	public static final Logger log = Logger.getLogger(DEAPISanityTest.class);
	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	String Backend_host = null;
	Integer Backend_port = null;
	static Connection con;

	@BeforeClass
	public void beforeClass() {

		try {


		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}
	}

	@DataProvider(name = "EnableHeavyRainMode")
	public static Object[][] rainmodeenabled()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("EnableHeavyRainMode", "REGRESSION");
	}

	@Test(priority = 0, dataProvider = "EnableHeavyRainMode", enabled = true)
	public void rainmodeenabled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("sand"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "Enable heavy rain mode"
					+ regressionTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "RainModelistingheavy")
	public static Object[][] rainmodechecks()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RainModelistingheavy",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "RainModelistingheavy", enabled = true)
	public void rainmodechecks(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {
			Thread.sleep(5000);
			if (dataSetName.equalsIgnoreCase("PARAMUPDATEMORE")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("sand"),
						apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "rain param update" + regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("LASTMILEEXCEEDS")) {
				Thread.sleep(10000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success, "rain mode last mile check"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("LASTMILEEXCEEDSCART")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success,
						"rain mode last mile check for cart sla"
								+ regressionTest);
			}

			if (dataSetName.equalsIgnoreCase("PARAMUPDATELESS")) {
				Thread.sleep(2000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
						apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Param update less" + regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("MAXSLAEXCEEDS")) {

				Thread.sleep(5000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success, "rain mode SLA Ecxeeds"
						+ regressionTest);
			}
			if (dataSetName.equalsIgnoreCase("MAXSLAEXCEEDSCART")) {
				Thread.sleep(2000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Param update less" + regressionTest);

			}

			if (dataSetName.equalsIgnoreCase("PARAMUPDATEMORE1")) {
				Thread.sleep(2000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
						apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success, "Param update less" + regressionTest);

			}

			if (dataSetName.equalsIgnoreCase("BFEXCEEDS")) {

				String query = "update zone set is_open=0 where id=8";
				con = (Connection) DBUtil.getConnection(IDBDriverType.MYSQL,
						"54.255.176.227", "delivery", "root",
						"notarealpassword");

				DBUtil.executeupdatequery(con, query);
				Thread.sleep(10000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success, "rain mode heavy bf exceeds"
						+ regressionTest);
				Thread.sleep(10000);
			}
			if (dataSetName.equalsIgnoreCase("BFEXCEEDCART")) {

				String query1 = "update zone set is_open=1 where id=8";
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success, "rain mode heavy bf exceeds"
						+ regressionTest);
				Thread.sleep(10000);
				DBUtil.executeupdatequery(con, query1);
				DBUtil.close(con);
				Thread.sleep(10000);
			}

			if (dataSetName.equalsIgnoreCase("LISTSERVICEABLE")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success,
						"restaurant serviceable in rain mode" + regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("CARTSERVICEABLE")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success,
						"restaurant serviceable in rain mode" + regressionTest);

			}
		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "EnableLightRainMode")
	public static Object[][] lightrainmodeenabled()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("EnableLightRainMode", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "EnableLightRainMode", enabled = true)
	public void lightrainmodeenabled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "Enable Light rain mode"
					+ regressionTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "RainModelistinglight")
	public static Object[][] lightrainmodechecks()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("RainModelistinglight",
				"REGRESSION");
	}

	@Test(priority = 3, dataProvider = "RainModelistinglight", enabled = true)
	public void lightrainmodechecks(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		try {

			if (dataSetName.equalsIgnoreCase("LIGHTPARAMUP")) {

				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
						apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);

				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success,
						"Increasing SLA for Last Mile Exceeds check for light rain mode"
								+ regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("LASTMILEEXCEEDSSLIST")) {
				Thread.sleep(10000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success,
						"Restaurant Exceeds Last mile in Light rain mode in Listing API"
								+ regressionTest);

			}
			if (dataSetName.equalsIgnoreCase("LASTMILEEXCEEDSSLIGHTCART")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success,
						"Restaurant Exceeds Last mile in Light rain mode in CART API"
								+ regressionTest);

			}

			if (dataSetName.equalsIgnoreCase("LIGHTPARAMDOWN")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),
						apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response);
				Assert.assertTrue(success,
						"Decreasing SLA for MAX SLA Exceeds check for light rain mode"
								+ regressionTest);

			}

			if (dataSetName.equalsIgnoreCase("MAXSLALIGHTEXCEEDSLIST")) {
				Thread.sleep(10000);
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"),apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);
				Assert.assertTrue(success,
						"Restaurant Exceeds max SLA in Light rain mode in Listing API"
								+ regressionTest);

			}

			if (dataSetName.equalsIgnoreCase("MAXSLALIGHTEXCEEDSCART")) {
				JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonNode response1 = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("delivery"), apiToExecute, jsonRequestData,
						STAFAgent.getSTAFValue("defaultAuthString")
);
				success = JsonValidator.DETAILED_LEVEL.validateJson(
						expectedData, response1);

				Assert.assertTrue(success,
						"Restaurant Exceeds max SLA in Light rain mode in CART API"
								+ regressionTest);

			}

		} catch (Exception e) {
			e.getStackTrace();
		}

	}
}
