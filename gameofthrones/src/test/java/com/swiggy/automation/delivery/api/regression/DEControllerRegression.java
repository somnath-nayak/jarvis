package com.swiggy.automation.delivery.api.regression;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.delivery.api.sanity.DeliveryCommon;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEControllerRegression extends RestTestHelper {

	public final static Logger logger = Logger
			.getLogger(DEControllerRegression.class);

	public static String delivery_host = null;
	public static Integer delivery_port = null;
	public static String Backend_host = null;
	public static Integer Backend_port = null;
	public static String oms_host = null;
	public static Integer oms_port = null;
	public static String delivery_Controller_host = null;
	public static Integer delivery_Controller_port = null;

	@BeforeClass
	public void beforeClass() {
		try {
//			delivery_Controller_host = getEndPoint("delivery_controller_host");
//			delivery_Controller_port = Integer
//					.parseInt(getEndPoint("delivery_controller_port"));
//
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			Backend_host = getEndPoint("backend_host");
//			Backend_port = Integer
//					.parseInt(getEndPoint("backend_port"));
//
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));

		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@BeforeTest
	public static void checkEnv() {
		if (!System.getProperty("env").equalsIgnoreCase("stage"))
			throw new SkipException(
					"Skipping Tests Since this is not a Staging Environment");
	}

	@DataProvider(name = "CONTROLLER_HEALTH_CHECK")
	public static Object[][] getControllerHealthCheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CONTROLLERHEALTHCHECK",
				"REGRESSION");
	}

	@Test(priority = -1, groups = "REGRESSION", dataProvider = "CONTROLLER_HEALTH_CHECK", enabled = true)
	public void checkControllerHealth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
				serviceNameMap.get("delivery"),
				apiToExecute, jsonRequestData, deliveryAuthString);
		boolean success = JsonValidator.HIGH_LEVEL.validateHttpResponse(
				expectedData, response);
		Assert.assertTrue(success, "checkControllerHealth" + smokeTest);

	}

	@Test(priority = 0, enabled = true, groups = "REGRESSION")
	public static void placeHolder() {
		log.info("Placing Order from Backend");
		try {
			// It takes time for DE to become Free
			Thread.sleep(20000);
			DeliveryCommon.LoginToBackend(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.CreateCart(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			DeliveryCommon.GetservicableAddress(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			boolean x = DeliveryCommon.PlaceOrder(serviceNameMap.get("checkout"),
					STAFAgent.getSTAFValue("defaultAuthString"));
			Assert.assertTrue(x, "Not executing since order id is null");
			// Since Order Placing takes time
			Thread.sleep(10000);
			DeliveryCommon.UpdateOMSOrder(serviceNameMap.get("delivery"),
					STAFAgent.getSTAFValue("defaultAuthString"));

			log.info("Order Placed Successfully");
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@Test(priority = 1, enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public static void assignDeliveryBoy() {
		boolean success = false;
		try {
			DeliveryCommon.AssignBatchOrder(serviceNameMap.get("delivery"),
					deliveryAuthString);
			success = true;
			log.info("Assigned to DE Id :"
					+ STAFAgent.getSTAFEndPoint("delivery_Boy_id"));
			Assert.assertTrue(success, "assignDeliveryBoy" + sanityTest);
		} catch (Exception e) {
			e.getStackTrace();
		}
	}

	@DataProvider(name = "CONTROLLER_TRACK")
	public static Object[][] getControllerTrack()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TRACK-CONTROLLER", "REGRESSION");
	}

	@Test(priority = 1, groups = "Regression", dataProvider = "CONTROLLER_TRACK", enabled = true, dependsOnMethods = "placeHolder")
	public void checkControllerTrack(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean success = false;
		JsonNode response = null;
		try {
			if (dataSetName.equalsIgnoreCase("valid input")) {
				HashMap<String, Object> update = new HashMap<String, Object>();
				update.put("pathvariable", DeliveryCommon.getOrderId());

				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				response = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("delivery"),
						apiToExecute, updatedrequest, deliveryAuthString);

			} else {
				response = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("delivery"),
						apiToExecute, jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "checkControllerTrack");

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(success, "checkControllerTrack");

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "CONTROLLER-CONFIRMED")
	public static Object[][] getControllerConfirmed()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("ControllerConfirmed", "REGRESSION");
	}

	@Test(priority = 2, dataProvider = "CONTROLLER-CONFIRMED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testControllerConfirmed(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {

			// Since Order Status Change takes time
			Thread.sleep(10000);
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerConfirmed" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerConfirmed" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "CONTROLLER-ARRIVED")
	public static Object[][] getControllerArrived()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ControllerArrived", "REGRESSION");
	}

	@Test(priority = 3, dataProvider = "CONTROLLER-ARRIVED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testControllerArrived(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {

			// Since Order Status Change takes time
			Thread.sleep(5000);
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerArrived" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerArrived" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "CONTROLLER-PICKEDUP")
	public static Object[][] getControllerPickedup()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ControllerPickedup", "REGRESSION");
	}

	@Test(priority = 4, dataProvider = "CONTROLLER-PICKEDUP", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testControllerPickedup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {
			// Since Order Status Change takes time
			Thread.sleep(5000);
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerPickedup" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerPickedup" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "CONTROLLER-REACHED")
	public static Object[][] getControllerReached()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ControllerReached", "REGRESSION");
	}

	@Test(priority = 5, dataProvider = "CONTROLLER-REACHED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testControllerReached(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {
			// Since Order Status Change takes time
			Thread.sleep(5000);
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerReached" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerReached" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@DataProvider(name = "CONTROLLER-DELIVERED")
	public static Object[][] getControllerDelivered()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("ControllerDelivered", "REGRESSION");
	}

	@Test(priority = 6, dataProvider = "CONTROLLER-DELIVERED", enabled = true, groups = "REGRESSION", dependsOnMethods = "placeHolder")
	public void testControllerDelivered(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean success = false;
		JsonNode response = null;
		try {
			// Since Order Status Change takes time
			Thread.sleep(5000);
			if (dataSetName.equalsIgnoreCase("valid input")) {
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"orderid", DeliveryCommon.getOrderId());

				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedrequest,
						deliveryAuthString);
			} else {
				response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute,
						jsonRequestData, deliveryAuthString);
			}

			success = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerDelivered" + smokeTest);

			success = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(success, "testControllerDelivered" + sanityTest);

		} catch (Exception e) {
			e.getStackTrace();
		}

	}

	@SuppressWarnings("unchecked")
	@AfterClass()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", DeliveryCommon.getOrderId());
		updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				updatedRequest, "time", System.currentTimeMillis() / 1000);
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
				oms_osrftoken);
		JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
				expectedResponseData, actualData, new String[] { "status",
						"message" });
		Thread.sleep(20000);
	}

}
