package com.swiggy.automation.delivery.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Aditya
 */

public class DELoginToBackendSession extends RestTestHelper {

	/** The Constant log. */
	public static final Logger log = Logger
			.getLogger(DELoginToBackendSession.class);

	@DataProvider(name = "baclogin")
	public static Object[][] bacLogin() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("baclogin");
	}

	@Test(priority = 1, dataProvider = "baclogin", groups = "Sanity")
	public void loginTobacSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode actualData = RestTestUtil.sendData(
					serviceNameMap.get("checkout"),
					apiToExecute, jsonRequestData,  STAFAgent.getSTAFValue("defaultAuthString"));

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(isSuccess, "LoginToDESession");
			defaultTOKEN = actualData.get("data").get("token").asText()
					.toString();
			defaulTID = actualData.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

			// ClientResponse c = RestTestUtil.sendDataNGetHttpStatus(
			// apiToExecute, jsonRequestData);
			// JsonNode response = c.getEntity(JsonNode.class);
			//
			// isSuccess = JsonValidator.HIGH_LEVEL.validateHttpResponse(
			// expectedData, c);
			// Assert.assertTrue(isSuccess, "LoginToSession");
			//
			// token = response.get("data").get("token").asText().toString();
			// tid = response.get("tid").asText().toString();
			// System.out.println("TID For the Session" + tid);
			// System.out.println("Token For the Session" + token);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

}
