package com.swiggy.automation.delivery.api.sanity;

import java.util.HashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class DEAPISanitySet2 extends RestTestHelper {

	public static final Logger log = Logger.getLogger(DEAPISanitySet2.class);
	static String dtid, batchid, orderid, batchid1, orderid1;

	@DataProvider(name = "A")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SlaCartAPI");
	}

	@Test(priority = 15, groups = "Sanity", dataProvider = "A", enabled = false)
	public void testCreateCart2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = RestTestUtil
				.sendData(serviceNameMap.get("checkout"),apiToExecute, jsonRequestData);

	}

	@DataProvider(name = "PORTAL_LOGIN")
	public static Object[][] getportallogin()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PORTAL_LOGIN");
	}

	@Test(priority = 14, groups = "Sanity", dataProvider = "PORTAL_LOGIN", enabled = false)
	public void testportallogin(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// JsonNode
			// updatedRequestData=RestTestUtil.updateRequestDataBeforeSend(jsonRequestData,
			// "batch_id", value)
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),
					apiToExecute, jsonRequestData, "");

			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
			String portaltoken = response.get("data").get("token").asText()
					.toString();
			String portaltid = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + portaltid);
			System.out.println("Token For the Session" + portaltoken);
		} catch (Exception e) {
			Assert.fail();
		}

	}

	// Type2

	@DataProvider(name = "HEALTH_CHECK")
	public static Object[][] gethealthcheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HEALTH_CHECK");
	}

	@Test(priority = 1, groups = "Sanity", dataProvider = "HEALTH_CHECK", enabled = true)
	public void checkhealth(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)

			throws Exception {
		try {
			JsonNode response = RestTestUtil
					.sendData(serviceNameMap.get("deliverycontroller"), apiToExecute, " ");
//			boolean b = JsonValidator.HIGH_LEVEL
//					.validateHttpResponse(expectedData, response);
			//Assert.assertTrue(b);
		} catch (Exception e) {
			Assert.fail();
        }
	}

	@DataProvider(name = "DE_OTP")
	public static Object[][] getdeOTP() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_OTP");
	}

	@Test(priority = 2, groups = "Sanity", dataProvider = "DE_OTP", enabled = false)
	public void testDeotp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"),apiToExecute,
				jsonRequestData, deliveryAuthString);

		dtid = response.get("data").get("Authorization").asText().toString();
		System.out.println("Tid is " + dtid);
		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(b);
	}

	@DataProvider(name = "DE_FREE")
	public static Object[][] unassignDe() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DE_FREE");
	}

	@Test(priority = 3, groups = "Sanity", dataProvider = "DE_FREE", enabled = true)
	public void unassignDe(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			HashMap<String, String> map1 = dbconfig.executeQuery(
					"select * from trips where assigned_time IS NOT NULL and de_id=216 order by 1 desc limit 1");
			batchid1 = map1.get("batch_id");
			orderid1 = map1.get("order_id");
			// orderid = map1.get("order_id");
			// db
			// "batch_id", value)
			if (null == batchid1) {
				System.out.println(
						"No order assigned to Delivery boys 216 so no need of unassign");
			} else {
				JsonNode updatedRequestData = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData,
								"batch_id", batchid1);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),apiToExecute,
						updatedRequestData, deliveryAuthString);
				boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);
				Assert.assertTrue(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "Change_DELocation")
	public static Object[][] getNewDELocation()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Change_DELocation");
	}

	// String Deid = "2418";

	@Test(priority = 4, groups = "Sanity", dataProvider = "Change_DELocation", enabled = false)
	public void testAddNewAddressWay1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {
			// String a1 = RestTestUtil.getCustomAuthString(Deid, dtid);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),apiToExecute,
					jsonRequestData, dtid);
			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "ASSIGN_DE")
	public static Object[][] assignDe() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ASSIGN_DE");
	}

	@Test(priority = 5, groups = "Sanity", dataProvider = "ASSIGN_DE", enabled = true)
	public void assingde(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// "batch_id", value)
			// HashMap<String, String> map = DbConnection
			// .executeQuery("select b.id from batch b inner join trips t on
			// b.id=t.batch_id where b.de_id is null order by 1 desc limit 1");
			// batchid = map.get("id");

			if (null == batchid1) {
				HashMap<String, String> map = dbconfig.executeQuery(
						"select * from trips where assigned_time IS NULL order by 1 desc limit 1");
				batchid = map.get("batch_id");
				orderid = map.get("order_id");
				System.out.println("order id is " + orderid);
			} else {
				batchid = batchid1;
				orderid = orderid1;
			}
			JsonNode updatedRequestData = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "batch_id",
							batchid);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),apiToExecute,
					updatedRequestData, deliveryAuthString);
			boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(b);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "TRACKING")
	public static Object[][] getordertrack()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TRACKING");
	}

	@Test(priority = 6, groups = "Sanity", dataProvider = "TRACKING", enabled = true)
	public void testordertrack(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updateRequestData = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", orderid);
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("deliverycontroller"),
				apiToExecute, updateRequestData, deliveryAuthString);
		boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(b);
	}

	@DataProvider(name = "CONFIRM_ORDER")
	public static Object[][] confirmorder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CONFIRM_ORDER");
	}

	@Test(priority = 7, groups = "Sanity", dataProvider = "CONFIRM_ORDER", enabled = true)
	public void confirmorder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// "batch_id", value)
			if (null == orderid) {
				System.out.println("Order id is null");
			} else {
				JsonNode updatedRequestData = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, "orderid",
								orderid);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
						apiToExecute, updatedRequestData, deliveryAuthString);
				Thread.sleep(2000);
				boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);
				Assert.assertTrue(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "ARRIVED_ORDER")
	public static Object[][] arrivedorder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ARRIVED_ORDER");
	}

	@Test(priority = 8, groups = "Sanity", dataProvider = "ARRIVED_ORDER", enabled = true)
	public void arrivedmorder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// "batch_id", value)
			if (null == orderid) {
				System.out.println("Order id is null");
			} else {
				JsonNode updatedRequestData = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, "orderid",
								orderid);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
						apiToExecute, updatedRequestData, deliveryAuthString);
				Thread.sleep(2000);
				boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);
				Assert.assertTrue(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "PICKED_ORDER")
	public static Object[][] pickeduporder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PICKED_ORDER");
	}

	@Test(priority = 8, groups = "Sanity", dataProvider = "PICKED_ORDER", enabled = true)
	public void pickeduporder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// "batch_id", value)
			if (null == orderid) {
				System.out.println("Order id is null");
			} else {
				JsonNode updatedRequestData = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, "orderid",
								orderid);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
						apiToExecute, updatedRequestData, deliveryAuthString);
				Thread.sleep(2000);
				boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);
				Assert.assertTrue(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "REACHED_ORDER")
	public static Object[][] reachedorder()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("REACHED_ORDER");
	}

	@Test(priority = 8, groups = "Sanity", dataProvider = "REACHED_ORDER", enabled = true)
	public void reachedorder(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// db
			// "batch_id", value)
			if (null == orderid) {
				System.out.println("Order id is null");
			} else {
				JsonNode updatedRequestData = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, "orderid",
								orderid);
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("deliverycontroller"),
						apiToExecute, updatedRequestData, deliveryAuthString);
				Thread.sleep(2000);
				boolean b = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
						response);
				Assert.assertTrue(b);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}
