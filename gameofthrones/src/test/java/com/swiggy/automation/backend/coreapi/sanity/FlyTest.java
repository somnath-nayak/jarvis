package com.swiggy.automation.backend.coreapi.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class FlyTest extends RestTestHelper {

	@DataProvider(name = "ABCDE")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ABCDE");
	}

	@Test(priority = 1, dataProvider = "ABCDE")
	public void testMenu(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute, jsonRequestData);
		// boolean isSuccess = JsonValidator.HIGH_LEVEL
		// .validateJsonWithCustomFields(expectedData, response,
		// new String[] { "statusCode" });
		// Assert.assertTrue(isSuccess, "testMenu");
	}

	// @Test(priority = 1, dataProvider = "BACKEND_LOGIN", groups = "Sanity",
	// dataProviderClass = DataProviderDataService.class)
	public void template1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

	}

	// @Test(priority = 2, groups = "Sanity", dataProvider = "ADD_NEW_ADDRESS")
	public void template2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData, JsonNode apiFieldsData) throws Exception {

	}

}
