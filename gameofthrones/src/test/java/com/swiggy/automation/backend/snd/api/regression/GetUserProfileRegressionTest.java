package com.swiggy.automation.backend.snd.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class GetUserProfileRegressionTest extends RestTestHelper {
	/** The Constant log. */
	public static final Logger log = Logger
			.getLogger(GetUserProfileRegressionTest.class);

	@DataProvider(name = "LOGIN_SANITY")
	public static Object[][] getLoginData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("LOGIN_SANITY", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "LOGIN_SANITY")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("td"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "USERPROFILE_REGRESSION")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("USERPROFILE_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 2, dataProvider = "USERPROFILE_REGRESSION")
	public void testUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
				expectedData, response);
		Assert.assertTrue(isSuccess);
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
