package com.swiggy.automation.backend.revenuegrowth.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author SKG
 *
 */
public class coupons extends RestTestHelper {
	int address_id1 = -1;
	long order_id1 = -1;

	@DataProvider(name = "Coupons")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Get_Coupons");
	}

	@Test(priority = 1, dataProvider = "Coupons")
	public void allCoupons(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "allCoupons");

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "UserCoupons")
	public static Object[][] getCreateCartData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Get_User_Coupons");
	}

	@Test(priority = 2, dataProvider = "UserCoupons")
	public void userCoupons(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("td"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "userCoupons");

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "CREATE_CART")
	public static Object[][] getCreateCartData2()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CART");
	}

	@Test(priority = 3, dataProvider = "CREATE_CART")
	public void testCreateCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
	boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
			expectedData, response);
	Assert.assertTrue(isSuccess);
	}

	@DataProvider(name = "ADD_NEW_ADDRESS")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("NEWADDRESS");
	}

	@Test(priority = 4, dataProvider = "ADD_NEW_ADDRESS")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		// (apiToExecute, defaulTID, defaultTOKEN)
		address_id1 = actualData.get("data").get("address_id").asInt();

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetAllAddressAPI");

	}

	@DataProvider(name = "PLACE_ORDER")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PLACE_ORDER");
	}

	@Test(priority = 5, dataProvider = "PLACE_ORDER")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		System.out.println("HI");
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "address_id", address_id1);
		JsonNode cplaceOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				updatedRequest, defaulTID, defaultTOKEN);
		order_id1 = cplaceOrderResponse.get("data").get("order_id").asLong();
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				cplaceOrderResponse);
		Assert.assertTrue(status, "testPlaceOrderAPI");
		System.out.println(order_id1);
		System.out.println("*************");
	}

	@DataProvider(name = "Cancel_Order")
	public static Object[][] getOrderCancel()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Cancel_Order");
	}

	@Test(priority = 7, dataProvider = "Cancel_Order")
	public void couponOrderCancelled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest1 = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", order_id1);
		System.out.println("*************");
		System.out.println(order_id1);
		System.out.println("*************");

		JsonNode cOrderCancelled = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),
				apiToExecute, updatedRequest1,   STAFAgent.getSTAFValue("defaultAuthString"));
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				cOrderCancelled);
		Assert.assertTrue(status, "couponOrderCancelled");
	}

	/*
	 * (priority = 6, dataProvider = "Cancel_Order") public void
	 * couponOrderCancelled(String dataGroup, String apiToExecute, String
	 * dataSetName, JsonNode jsonRequestData, JsonNode expectedData) throws
	 * Exception { boolean isSuccess = false; try { JsonNode updatedRequest1 =
	 * RestTestUtil.updateRequestDataBeforeSend( jsonRequestData, "order_id",
	 * order_id1); JsonNode response = RestTestUtil.sendData(apiToExecute,
	 * updatedRequest1, ); isSuccess =
	 * JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
	 * Assert.assertTrue(isSuccess, "couponOrderCancelled");
	 * 
	 * } catch (Exception e) { e.printStackTrace(); Assert.fail(); } }
	 */
}
