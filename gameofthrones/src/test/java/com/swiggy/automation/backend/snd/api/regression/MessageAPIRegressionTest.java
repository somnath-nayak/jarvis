package com.swiggy.automation.backend.snd.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shilpy
 */
public class MessageAPIRegressionTest extends RestTestHelper {

	@DataProvider(name = "MESSAGE_API_REGRESSION")
	public static Object[][] viewMsgV1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MESSAGE_API_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "MESSAGE_API_REGRESSION")
	public void testMessageV1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
				apiToExecute, jsonRequestData);
		boolean status = JsonValidator.DETAILED_LEVEL.validateJson(
				expectedData, currentResponse);
		Assert.assertTrue(status, "testMessageV1");

	}

}
