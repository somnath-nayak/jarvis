package com.swiggy.automation.backend.checkout.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shwetha
 *
 */

public class HelpAPIsSanityTest extends RestTestHelper {

	// public static final Logger log = Logger.getLogger(LoginToSession.class);
	// HELPONE

	@DataProvider(name = "HELPONEDP")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HELP_ONE");
	}

	@Test(priority = 1, dataProvider = "HELPONEDP", groups = "Sanity")
	public void testCreateHelpAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testHelpAPI");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "HELPRECENTDP")
	public static Object[][] getCreateCartData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HELP_RECENT");
	}

	@Test(priority = 2, dataProvider = "HELPRECENTDP", groups = "Sanity")
	public void testHelpAPIRecent(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testHelpAPIRecent");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "HELPTHREEDP")
	public static Object[][] getCreateCartData3()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("HELP_RECENT");
	}

	@Test(priority = 3, dataProvider = "HELPTHREEDP", groups = "Sanity")
	public void testGetHelpAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testGetHelpAPI");
		} catch (Exception e) {
			Assert.fail();
		}
	}

}