package com.swiggy.automation.backend.snd.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class SnDSanityTest extends RestTestHelper {

	public static final Logger log = Logger.getLogger(SnDSanityTest.class);

	Long orderId = null;
	String orderKey = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	Long serviceableAddressId = null;
	String oms_osrftoken = null;
	String oms_sessionid = null;

	// V2 Login
	// V3Listing
	// Menu
	// GetProfile

	@BeforeClass
	public void beforeClass() {

		try {
//			oms_host = getEndPoint("oms_host");
//			oms_port = Integer.parseInt(getEndPoint("oms_port"));
//
//			delivery_host = getEndPoint("delivery_host");
//			delivery_port = Integer
//					.parseInt(getEndPoint("delivery_port"));
//
//			oms_osrftoken = getEndPoint("oms_osrftoken");
//			oms_sessionid = getEndPoint("oms_sessionid");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		 JsonNode response=null;
		
		try {
//			JsonNode response = RestExecutor.executeNValidateAPI(apiToExecute,
//					jsonRequestData, expectedData,
//					JsonValidationLevels.DETAILED_LEVEL);
			  response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
		} catch (Exception e) {
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}
	}

	@DataProvider(name = "GETPROFILE_SANITY")
	public static Object[][] gettestUserProfileData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETPROFILE_SANITY");
	}

	@Test(priority = 2, dataProvider = "GETPROFILE_SANITY")
	public void testUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
	
			 JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
		
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "V3_LISTING_CRITICALFLOW")
	public static Object[][] getV3Data() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V3_LISTING_CRITICALFLOW");
	}

	@Test(priority = 3, dataProvider = "V3_LISTING_CRITICALFLOW")
	public void testV3Listing(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
//			RestExecutor.executeNValidateAPI(apiToExecute, expectedData,
//					JsonValidationLevels.DETAILED_LEVEL);
			 JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "MENU_CRITICALFLOW")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MENU_CRITICALFLOW");
	}

	@Test(priority = 4, dataProvider = "MENU_CRITICALFLOW")
	public void testMenu(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			System.out.println("====MENU_CRITICALFLOW CREATECART=====");
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);
			Assert.assertTrue(isSuccess, "testMenu");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}
}
