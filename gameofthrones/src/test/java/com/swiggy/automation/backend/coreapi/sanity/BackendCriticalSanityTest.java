package com.swiggy.automation.backend.coreapi.sanity;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.ClientResponse;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.api.rest.json.utils.JsonValidatorUtil;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class BackendCriticalSanityTest extends RestTestHelper {

	public static final Logger log = Logger
			.getLogger(BackendCriticalSanityTest.class);

	Long orderId = null;
	String orderKey = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	Long serviceableAddressId = null;

	@BeforeClass
	public void beforeClass() {

		try {
		
		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	@DataProvider(name = "ABCDEXYZ")
	public static Object[][] H() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ABCDEXYZ");

	}

	// @Test(priority = -200, dataProvider = "ABCDEXYZ")
	public void g(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {

		JsonNode omssession = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData);
		System.out.println(omssession);
	}

	@DataProvider(name = "DELIVERY_BOY_ACTIVATE_CRITICALFLOW")
	public static Object[][] getDeliveryBoyIds()
			throws SwiggyAPIAutomationException {
		return APIDataReader
				.getDataSetData("DELIVERY_BOY_ACTIVATE_CRITICALFLOW");

	}

	@Test(priority = -100, dataProvider = "DELIVERY_BOY_ACTIVATE_CRITICALFLOW")
	public void testMakeDeliveryBoyActive(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			System.out.println(serviceableAddressId);
			System.out.println("Make delivery Boy Active");
			JsonNode actualData = RestTestUtil.sendData(
					serviceNameMap.get("delivery_host"),
					apiToExecute, jsonRequestData,
					serviceNameMap.get("delivery_authstring_username")
							+ DELIMITTER + serviceNameMap
									.get("delivery_authstring_password"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(status, "testMakeDeliveryBoyActive");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("BackendCriticalSanityTest-->testMakeDeliveryBoyActive "
					+ e);
			Assert.fail("testMakeDeliveryBoyActive", e);

		}

	}

	@DataProvider(name = "BACKEND_LOGIN_CIRTICALFLOW")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_LOGIN_CIRTICALFLOW");
	}

	@Test(priority = 1, dataProvider = "BACKEND_LOGIN_CIRTICALFLOW")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "BACKEND_LOGIN_CIRTICALFLOW");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "GETPROFILE_CRITICALFLOW")
	public static Object[][] gettestUserProfileData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETPROFILE_CRITICALFLOW");
	}

	@Test(priority = 2, dataProvider = "GETPROFILE_CRITICALFLOW")
	public void testUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			System.out.println("====GETPROFILE_CRITICALFLOW =====");
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testUserProfile");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "V3_LISTING_CRITICALFLOW")
	public static Object[][] getV3Data() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V3_LISTING_CRITICALFLOW");
	}

	@Test(priority = 3, dataProvider = "V3_LISTING_CRITICALFLOW")
	public void testV3Listing(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			System.out.println("====V3_LISTING_CRITICALFLOW CREATECART=====");
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, response);
			Assert.assertTrue(isSuccess, "testV3Listing");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "MENU_CRITICALFLOW")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MENU_CRITICALFLOW");
	}

	@Test(priority = 4, dataProvider = "MENU_CRITICALFLOW")
	public void testMenu(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			System.out.println("====MENU_CRITICALFLOW CREATECART=====");
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, response);
			Assert.assertTrue(isSuccess, "testMenu");
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "CART_CIRTICALFLOW")
	public static Object[][] getCreateCartDataCriticalFlow()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CART_CIRTICALFLOW");
	}

	@Test(priority = 5, dataProvider = "CART_CIRTICALFLOW")
	public void testCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			switch (apiToExecute) {
			case "CREATECART":
				System.out.println("====CART_CIRTICALFLOW CREATECART=====");
				JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
				boolean isSuccess = JsonValidator.HIGH_LEVEL
						.validateJson(expectedData, response);
				Assert.assertTrue(isSuccess, "CREATECART");

				break;
			case "ISADDRESSSERVICEABLE":
				System.out.println(
						"====CART_CIRTICALFLOW ISADDRESSSERVICEABLE=====");
				JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
				boolean status = JsonValidatorUtil.validateJsonHighLevel(expectedData,
						actualData);
				Assert.assertTrue(status, "ISADDRESSSERVICEABLE");
			
				break;
			case "CARTGET":
				System.out.println("====CART_CIRTICALFLOW CARTGET=====");
				
				
				 actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
				status = JsonValidatorUtil.validateJsonHighLevel(expectedData,
						actualData);
				Assert.assertTrue(status, "CARTGET");
				break;
			case "UPDATECART":
				System.out.println("====CART_CIRTICALFLOW UPDATECART=====");
				
				
				 actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
				break;
			case "CARTCHECKTOTAL":
				System.out.println("====CART_CIRTICALFLOW CARTCHECKTOTAL=====");
				 actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
							jsonRequestData);
				break;
			case "CARTFLUSH":
				System.out.println("====CART_CIRTICALFLOW CARTFLUSH=====");
				 actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
							jsonRequestData);
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "ADDRESS_CRITICALFLOW")
	public static Object[][] getAddressAPI()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("ADDRESS_CRITICALFLOW");
	}

	@Test(priority = 6, dataProvider = "ADDRESS_CRITICALFLOW")
	public void testAddressAPIs(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = null;
		JsonNode updatedrequest = null;
		boolean isSuccess = false;
		switch (apiToExecute) {
		case "ADDNEWADDRESS":
			System.out.println("====ADDRESS_CRITICALFLOW ADDNEWADDRESS=====");
			response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute, jsonRequestData);
			addressId = response.get("data").get("address_id").toString();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "AddNewAddress");
			break;

		case "UPDATEADDRESS":
			System.out.println("====ADDRESS_CRITICALFLOW UPDATEADDRESS=====");
			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", addressId);
			response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute, updatedrequest);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "UpdateAddress");
			break;
		case "DELETEADDRESS":
			System.out.println("====ADDRESS_CRITICALFLOW DELETEADDRESS=====");
			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", addressId);
			response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute, updatedrequest);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "DeleteAddress");
			break;
		case "GETALLADDRESS":
			System.out.println("====ADDRESS_CRITICALFLOW GETALLADDRESS=====");
			 JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
		}
	}

	@DataProvider(name = "CAPD")
	public static Object[][] f() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CAPD");
	}

	@Test(priority = 7, dataProvider = "CAPD")
	public void testEndToEndOrderProcessWithCAPD(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		Thread.sleep(5000);
		switch (apiToExecute) {
		case "CREATECART":
			System.out.println("====CAPRD CREATECART=====");
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, addressresponse);
			if (addressresponse.get("statusCode").asInt() == 6) {
				System.out.println("RESTUARNT CLOSED");
				log.error("RESTUARNT CLOSED...SO EXITING");
				System.exit(0);
			}
			Assert.assertTrue(isSuccess, "CREATE_CART");
			break;
		case "GETALLADDRESS":
			System.out.println("====CAPRD GETALLADDRESS=====");
			actualData = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(actualData);

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI");
			break;

		case "PLACEORDER":
			System.out.println("====CAPRD PLACEORDER=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),apiToExecute, updatedRequest,
					defaulTID, defaultTOKEN);

			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testPlaceOrderAPI");
			orderId = actualData.get("data").get("order_id").asLong();
			orderIds.add(orderId);
			orderKey = actualData.get("data").get("key").asText();
			System.out.println("ORDER ID :::" + orderId);
			System.out.println("ORDER KEY :::" + orderKey);
			break;

		case "MAKEDEFREE":

		case "OMSORDERQUERY":
			System.out.println("====CAPRD OMSORDERQUERY=====");
			int counter = 0;
			status = false;

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			do {
				actualData = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("oms"), apiToExecute, updatedRequest,
						RestTestUtil.getRequestHeadersOms(oms_osrftoken,oms_sessionid));
				System.out.println("OMS QUERY RESPONSE :" + actualData);
				System.out.println("OMS QUERY RESPONSE OBJECT SIZE:"
						+ actualData.get("objects").size());
				if (actualData.get("objects").size() < 1) {
					System.out.println("Order Not appear on the dashboard");
					counter++;
					status = false;
				} else {
					status = true;
					System.out.println(
							"Order appear on the OMS dashboard...So proceeding");
					log.error(
							"Order appear on the OMS dashboard...So proceeding");

					break;
				}
				Thread.sleep(10000);
			} while (counter < 12);

			Assert.assertTrue(status, "Order appear On OMS dashBoard");
			break;

		case "OMSUPDATEACTION":
			System.out.println("====CAPRD OMSUPDATEACTION=====");
			// int count = 0;
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);

			// do {
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, RestTestUtil.getRequestHeadersOms(oms_osrftoken,oms_sessionid));
			// if ((actualData.get("message")).asText().contains("Queued")) {
			Thread.sleep(10000);
			// count++;
			// } else {
			// break;
			// }
			// } while (count < 10);

			break;

		case "MARKDEFREE":
			System.out.println("====CAPRD DE FREE=====");

			try {
				HashMap<String, Object> update = new HashMap<>();
				update.put("id", serviceNameMap.get("de1_for_order"));
				JsonNode updatedrequest = RestTestUtil
						.updateRequestDataBeforeSend(jsonRequestData, update);

				ClientResponse response = RestTestUtil.sendDataNGetHttpStatus(
						serviceNameMap.get("delivery_host"), apiToExecute,
						updatedrequest, deliveryAuthString);

				boolean success = JsonValidator.HIGH_LEVEL
						.validateHttpResponse(expectedData, response);

				Assert.assertTrue(success, "testFREEDE" + sanityTest);
			} catch (Exception e) {
				e.getStackTrace();
			}

			break;
		case "OMSDEASSIGN":
			System.out.println("====CAPRD OMSDEASSIGN=====");
			Thread.sleep(10000);
			boolean code = false;
			int i = 0, retryCount = 3;
			String de = serviceNameMap.get("de1_for_order");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			do {
				try {
					updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
							updatedRequest, "delivery_boy_id", de);
					actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, updatedRequest,
							oms_sessionid, oms_osrftoken);
					code = actualData.get("status").asBoolean();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (code == true) {
					System.out.println("DE assigned >>>>>>>>>>>:" + de);
					break;
				} else {
					de = serviceNameMap.get("de2_for_order");
				}
				Thread.sleep(10000);
			} while (code == false && i++ < retryCount);

			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(expectedData,
					actualData, new String[] { "status", "message" });
			break;

		case "OMSUPDATESTATUS":
			System.out.println("====CAPRD OMSUPDATESTATUS=====");
			System.out.println("********************************");
			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken,oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status, "OMSUPDATESTATUS :" + dataSetName);
			Thread.sleep(10000);
			break;

		case "GETORDERDETAILS":
			System.out.println("====CAPRD GETORDERDETAILS=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("oms"),apiToExecute,
					updatedRequest,   STAFAgent.getSTAFValue("defaultAuthString"));
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(status, "GETORDERDETAILS_" + dataSetName);
			break;
		case "GETSINGLEORDER":
			System.out.println("====CAPRD GETSINGLEORDER=====");
			if (jsonRequestData.has("order_id")) {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_id", orderId);
			} else {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_key", orderKey);
			}

			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "GETSINGLEORDER_" + dataSetName);
			break;

		// case "GETORDERSUMMARY":
		// System.out.println("====CAPRD GETORDERSUMMARY=====");
		// int count = 0;
		// int statusCode = -888;
		// JsonNode getOrderSummary = null;
		// do {
		// try {
		// getOrderSummary = RestTestUtil.sendData(apiToExecute);
		// statusCode = getOrderSummary.get("statusCode").asInt();
		// if (statusCode == 0) {
		// break;
		// }
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// }
		// Thread.sleep(15000);
		// } while (count++ < 3);
		//
		// status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
		// getOrderSummary);
		// Assert.assertTrue(status, "GETORDERSUMMARY_" + dataSetName);
		// break;
		case "GETMINIMALORDERDETAILS":
			System.out.println("====CAPRD GETMINIMALORDERDETAILS=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_key", orderKey);
			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "GETMINIMALORDERDETAILS_" + dataSetName);
			break;

		case "TRACKORDER":
			System.out.println("====CAPRD TRACKORDER=====");
			if (jsonRequestData.has("order_id")) {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_id", orderId);
			} else {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_key", orderKey);
			}
			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "Track_" + dataSetName);

			break;

		case "TRACKMINIMAL":
			System.out.println("====CAPRD TRACKMINIMAL=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_key", orderKey);

			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "TRACKMINIMAL_" + dataSetName);
			break;

		}
	}

	@DataProvider(name = "EDIT_N_CANCEL_ORDER_BACKEND")
	public static Object[][] fe() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("EDIT_N_CANCEL_ORDER_BACKEND");
	}

	@Test(priority = 8, dataProvider = "EDIT_N_CANCEL_ORDER_BACKEND")
	public void testBackendEditOrderAndCancelAPIs(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		Thread.sleep(5000);
		switch (apiToExecute) {
		case "CREATECART":
			System.out
					.println("====EDIT_N_CANCEL_ORDER_BACKEND CREATECART=====");
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, addressresponse);
			if (addressresponse.get("statusCode").asInt() == 6) {
				System.out.println("RESTUARNT CLOSED");
				log.error("RESTUARNT CLOSED...SO EXITING");
				System.exit(0);
			}
			Assert.assertTrue(isSuccess, "CREATE_CART");
			break;
		case "GETALLADDRESS":
			System.out.println(
					"====EDIT_N_CANCEL_ORDER_BACKEND GETALLADDRESS=====");

			actualData = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(actualData);

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI");

			break;
		case "PLACEORDER":
			System.out
					.println("====EDIT_N_CANCEL_ORDER_BACKEND PLACEORDER=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute, updatedRequest,
					defaulTID, defaultTOKEN);

			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testPlaceOrderAPI");
			orderId = actualData.get("data").get("order_id").asLong();
			orderIds.add(orderId);
			orderKey = actualData.get("data").get("key").asText();
			System.out.println("ORDER ID :::" + orderId);
			System.out.println("ORDER KEY :::" + orderKey);
			break;

		case "OMSORDERQUERY":
			System.out.println(
					"====EDIT_N_CANCEL_ORDER_BACKEND OMSORDERQUERY=====");
			int counter = 0;
			status = false;
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			do {
				actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),
						 apiToExecute, updatedRequest,
						 RestTestUtil.getRequestHeadersOms(oms_osrftoken,oms_sessionid));
				System.out.println("OMS QUERY RESPONSE :" + actualData);
				System.out.println("OMS QUERY RESPONSE OBJECT SIZE:"
						+ actualData.get("objects").size());
				if (actualData.get("objects").size() < 1) {
					System.out.println("Order Not appear on the dashboard");
					counter++;
					status = false;
				} else {
					status = true;
					System.out.println(
							"Order appear on the OMS dashboard...So proceeding");
					log.info(
							"Order appear on the OMS dashboard...So proceeding");

					break;
				}
				Thread.sleep(30000);
			} while (counter < 4);

			Assert.assertTrue(status, "Order appear On OMS dashBoard");
			break;

		case "UPDATECHECK":
			System.out.println(
					"====EDIT_N_CANCEL_ORDER_BACKEND UPDATECHECK=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			JsonNode getOrderDetailsResponse = RestTestUtil
					.sendData(serviceNameMap.get("checkout"),apiToExecute, updatedRequest,   STAFAgent.getSTAFValue("defaultAuthString"));
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getOrderDetailsResponse);
			Assert.assertTrue(status, "UPDATECHECK_" + dataSetName);
			break;
		case "UPDATECONFIRM":
			System.out.println(
					"====EDIT_N_CANCEL_ORDER_BACKEND UPDATECONFIRM=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedRequest,   STAFAgent.getSTAFValue("defaultAuthString"));
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(status, "UPDATECONFIRM_" + dataSetName);
			break;
		case "GETORDERDETAILS":
			System.out.println(
					"====EDIT_N_CANCEL_ORDER_BACKEND GETORDERDETAILS=====");
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			getOrderDetailsResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),
					apiToExecute, updatedRequest,   STAFAgent.getSTAFValue("defaultAuthString"));
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getOrderDetailsResponse);
			Assert.assertTrue(status, "testGetOrderDetailsAPI");
			break;
		case "CANCEL":
			System.out.println("====EDIT_N_CANCEL_ORDER_BACKEND CANCEL=====");
			 actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
			break;
		}
	}

	@SuppressWarnings("unchecked")
	@AfterTest()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		if (orderIds.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : orderIds) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken,oms_sessionid));
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedResponseData, actualData,
					new String[] { "status", "message" });
			Thread.sleep(2000);
		}

	}
}
