package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class MenuSanityTest extends RestTestHelper {

	@DataProvider(name = "MENU_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MENU_SANITY");
	}

	@Test(priority = 1, dataProvider = "MENU_SANITY")
	public void testMenu(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		System.out.println(expectedData);
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData);
		System.out.println(response);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testMenu" + smokeTest);

		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testMenu" + sanityTest);
	}

}
