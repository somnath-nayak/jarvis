package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shilpy . ramzi: added detailed level validation
 */
public class SettingsAPIsSanityTest extends RestTestHelper {

	@DataProvider(name = "SETTINGSV2_SANITY")
	public static Object[][] viewMsgV1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SETTINGSV2_SANITY");
	}

	@Test(priority = 3, dataProvider = "SETTINGSV2_SANITY")
	public void testSettingsV2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute, jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "testSettingsV2" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "testSettingsV2" + sanityTest);

	}

}
