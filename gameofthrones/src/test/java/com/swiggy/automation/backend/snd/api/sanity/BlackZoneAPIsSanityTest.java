package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class BlackZoneAPIsSanityTest extends RestTestHelper {

	int polygonId = -1;
	int blackzoneId = -1;

	@DataProvider(name = "CREATE_POLYGON_SANITY")
	public static Object[][] ca() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_POLYGON_SANITY");
	}

	@Test(priority = 0, dataProvider = "CREATE_POLYGON_SANITY")
	public void testCreatePolyogon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode res = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, jsonRequestData,
					STAFAgent.getSTAFValue("defaultAuthString"));
			polygonId = res.get("data").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, res);
			Assert.assertTrue(isSuccess, "testCreatePolyogon");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "CREATE_BLACKZONE_SANITY")
	public static Object[][] c() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_BLACKZONE_SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_BLACKZONE_SANITY")
	public void testCreateBlackZone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "polygonId", polygonId);
			JsonNode resp = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, updatedRequest,
					STAFAgent.getSTAFValue("defaultAuthString"));
			blackzoneId = resp.get("data").get("id").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					resp);
			Assert.assertTrue(isSuccess, "testCreateBlackZone");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "UPDATE_BLACKZONE_SANITY")
	public static Object[][] u() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_BLACKZONE_SANITY");
	}

	@Test(priority = 1, dataProvider = "UPDATE_BLACKZONE_SANITY")
	public void testUpdateBlackZone(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "id", blackzoneId);
			JsonNode resp = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, updatedRequest,
					STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					resp);
			Assert.assertTrue(isSuccess, "testUpdateBlackZone");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_BLACKZONEBYID_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_BLACKZONEBYID_SANITY");
	}

	@Test(priority = 1, dataProvider = "GET_BLACKZONEBYID_SANITY")
	public void testGetBlackZoneById(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", blackzoneId);
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				updatedRequest, STAFAgent.getSTAFValue("defaultAuthString"));
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testGetBlackZoneById");
	}

	@DataProvider(name = "GET_BLACKZONEBYLATLONG_SANITY")
	public static Object[][] ff() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_BLACKZONEBYLATLONG_SANITY");
	}

	@Test(priority = 5, dataProvider = "GET_BLACKZONEBYLATLONG_SANITY")
	public void testGetBlackZoneByLatLong(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		try {

			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, getSingleOrderResponse);
			Assert.assertTrue(status, "testGetBlackZoneByLatLong");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ALL_BLACKZONES")
	public static Object[][] ff1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALL_BLACKZONES");
	}

	@Test(priority = 5, dataProvider = "GET_ALL_BLACKZONES")
	public void testGetAllBlackZones(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(apiToExecute,
					STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetAllBlackZones");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
