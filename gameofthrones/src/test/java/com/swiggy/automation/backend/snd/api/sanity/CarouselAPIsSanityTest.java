package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CarouselAPIsSanityTest extends RestTestHelper {

	int carouselId = -1;

	@DataProvider(name = "CREATE_CAROUSEL_SANITY")
	public static Object[][] c() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CAROUSEL_SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_CAROUSEL_SANITY")
	public void testCreateCarousel(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode resp = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			carouselId = resp.get("data").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					resp);
			Assert.assertTrue(isSuccess, "testCreateCarousel");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "UPDATE_CAROUSEL_SANITY")
	public static Object[][] u() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_CAROUSEL_SANITY");
	}

	@Test(priority = 1, dataProvider = "UPDATE_CAROUSEL_SANITY")
	public void testUpdateCarousel(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "id", carouselId);
			JsonNode resp = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, updatedRequest,
					STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					resp);
			Assert.assertTrue(isSuccess, "testUpdateCarousel");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_CAROUSELBYID_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_CAROUSELBYID_SANITY");
	}

	@Test(priority = 1, dataProvider = "GET_CAROUSELBYID_SANITY")
	public void testGetCarouselById(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", carouselId);
		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				updatedRequest, STAFAgent.getSTAFValue("defaultAuthString"));
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testGetCarouselById");
	}

	@DataProvider(name = "GET_CAROUSELBYLATLONG_SANITY")
	public static Object[][] ff() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_CAROUSELBYLATLONG_SANITY");
	}

	@Test(priority = 5, dataProvider = "GET_CAROUSELBYLATLONG_SANITY")
	public void testGetCarouselByLatLong(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, getSingleOrderResponse);
			Assert.assertTrue(status, "testGetCarouselByLatLong");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
