package com.swiggy.automation.backend.snd.api.regression;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

/**
 * Created by vishal.mahajan on 22/06/17.
 */
public class AggregatorV1RegressionTest extends RestTestHelper {

    static JsonNode actualResponse = null;
    static JsonNode expectedResponse = null;
    static int restaurant_id = -99;
    static Integer apiPort = 0;
    static String apiHost = "";

    @DataProvider(name = "V1_AGGREGATOR_REGRESSION")
    public static Object[][] getData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("V1_AGGREGATOR_REGRESSION","REGRESSION");
    }

    @Test(priority = 1, dataProvider = "V1_AGGREGATOR_REGRESSION")
    public void testAggregatorV1Listing(String dataGroup, String apiToExecute,
                                        String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

//        apiHost = getEndPoint("listing_backend_host_qa");
//        apiPort = Integer.parseInt(getEndPoint("listing_backend_port_qa"));

        restaurant_id = Integer.parseInt(STAFAgent.getSTAFEndPoint("defaultTestRestaurant"));
        expectedResponse = expectedData;
        actualResponse = RestTestUtil.sendData(serviceNameMap.get("td"),apiToExecute, jsonRequestData,  STAFAgent.getSTAFValue("defaultAuthString"));

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedResponse, actualResponse);
        Assert.assertTrue(isSuccess, "testAggregatorV1Listing" + smokeTest);

        isSuccess = testVerifyRestaurantObject(dataSetName,actualResponse,expectedResponse,restaurant_id);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing"+regressionTest);

    }

    public boolean testVerifyRestaurantObject(String dataSetName,JsonNode actualResponse,JsonNode expectedResponse,Integer restaurant_id)
            throws SwiggyAPIAutomationException {

        if(dataSetName.contains("INVALID") || dataSetName.equals("CAROUSEL ONLY")){
            return JsonValidator.DETAILED_LEVEL.validateJson(expectedResponse,actualResponse);
        }else {
            JsonNode actual = JsonTestUtils.getRestaurantObjectUsingElementForAggregator(actualResponse.get("data").get("restaurants"), restaurant_id);
            JsonNode expected = JsonTestUtils.getRestaurantObjectUsingElementForAggregator(expectedResponse.get("data").get("restaurants"), restaurant_id);

            return JsonValidator.DETAILED_LEVEL.validateJson(expected, actual);
        }
    }

}
