package com.swiggy.automation.backend.checkout.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shilpy
 *
 */

public class MobikwikDeepIntegrationSanityTest extends RestTestHelper {
	int address_id1 = -1;
	long order_id1 = -1;

	@DataProvider(name = "M_Link_Wallet")
	public static Object[][] getWalletLinkedMobi()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Link_Wallet");
	}

	@Test(priority = 1, dataProvider = "M_Link_Wallet")
	public void mobiLinkWallet(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "mobiLinkWallet");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "M_Verify_Otp")
	public static Object[][] getOtpVerified()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Verify_Otp");
	}

	@Test(priority = 2, dataProvider = "M_Verify_Otp", groups = "Sanity")
	public void mobiVerifyOtp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "M_Verify_Otp");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "M_Create_Cart")
	public static Object[][] getCartCreated()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Create_Cart");
	}

	@Test(priority = 3, dataProvider = "M_Create_Cart", groups = "Sanity")
	public void mobiCreateCart(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "mobiCreateCart");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "M_Check_Balance")
	public static Object[][] getBalanceCheck()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Check_Balance");
	}

	@Test(priority = 4, dataProvider = "M_Check_Balance", groups = "Sanity")
	public void mobiBalanceCheck(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "mobiBalanceCheck");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "M_Add_New_Add")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Add_New_Add");
	}

	@Test(priority = 5, dataProvider = "M_Add_New_Add")
	public void mobiGetAllAddress(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(apiToExecute, defaulTID,
				defaultTOKEN);
		address_id1 = actualData.get("data").get("address_id").asInt();
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "mobiGetAllAddress");

	}

	@DataProvider(name = "M_Place_Order")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Place_Order");
	}

	@Test(priority = 2, dataProvider = "M_Place_Order")
	public void mobiPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "address_id", address_id1);
		JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				updatedRequest, defaulTID, defaultTOKEN);
		order_id1 = placeOrderResponse.get("data").get("order_id").asLong();
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				placeOrderResponse);
		Assert.assertTrue(status, "mobiPlaceOrderAPI");

	}

	@DataProvider(name = "M_Cancel_Order")
	public static Object[][] getOrderCancelled()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Cancel_Order");
	}

	@Test(priority = 6, dataProvider = "M_Cancel_Order", groups = "Sanity")
	public void mobiOrderCancelled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode updatedRequest1 = RestTestUtil
					.updateRequestDataBeforeSend(jsonRequestData, "order_id",
							order_id1);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedRequest1, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "mobiOrderCancelled");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "M_Delink_Wallet")
	public static Object[][] delinkWallet() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("M_Delink_Wallet");
	}

	@Test(priority = 6, dataProvider = "M_Delink_Wallet", groups = "Sanity")
	public void mobiDelinkWallet(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "mobiDelinkWallet");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

}
