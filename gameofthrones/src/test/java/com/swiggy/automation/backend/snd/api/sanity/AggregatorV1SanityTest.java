package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 22/06/17.
 */
public class AggregatorV1SanityTest extends RestTestHelper {

	static JsonNode actualResponse = null;
	static JsonNode expectedResponse = null;
	static int restaurant_id = 223;// 211;// -99;
	static Integer apiPort = 0;
	static String apiHost = "";

	@DataProvider(name = "V1_AGGREGATOR")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V1_AGGREGATOR");
	}

	@Test(priority = 1, dataProvider = "V1_AGGREGATOR")
	public void testAggregatorV1Listing(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		// restaurant_id = Integer
		// .parseInt(getEnvPropertyValue("defaultTestRestaurant"));
		expectedResponse = expectedData;
		actualResponse = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, jsonRequestData,
				STAFAgent.getSTAFValue("defaultAuthString"));

		boolean isSuccess = JsonValidator.HIGH_LEVEL
				.validateJson(expectedResponse, actualResponse);
		Assert.assertTrue(isSuccess, "testAggregatorV1Listing" + smokeTest);

		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedResponse,
				actualResponse);

	}

	@Test(priority = 2, dependsOnMethods = "testAggregatorV1Listing")
	public void testverifyRestaurantObject()
			throws SwiggyAPIAutomationException {

		JsonNode actual = JsonTestUtils
				.getRestaurantObjectUsingElementForAggregator(
						actualResponse.get("data").get("restaurants"),
						restaurant_id);
		JsonNode expected = JsonTestUtils
				.getRestaurantObjectUsingElementForAggregator(
						expectedResponse.get("data").get("restaurants"),
						restaurant_id);
		// boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expected,
		// actual);
		boolean isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expected,
				actual);
		Assert.assertTrue(isSuccess, "testverifyRestaurantObject" + sanityTest);

	}

}
