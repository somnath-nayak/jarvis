package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DateTimeUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author abhishek.garg
 *
 */
public class TradeDiscountSanityTest extends RestTestHelper {

	static JsonNode createTDresponse = null;

	@DataProvider(name = "Create_TD")
	public static Object[][] getCreateTDData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Create_TD");
	}

	@Test(priority = 1, dataProvider = "Create_TD")
	public void testCreateTD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		Map<String, Object> createRequestUpdateData = new HashMap<String, Object>();

		switch (dataSetName) {
		case "VALIDFROM>VALIDTILL_DATE":
			break;
		default: {
			createRequestUpdateData
					.put("valid_from",
							DateTimeUtils
									.getCurrentDateTime(DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
			createRequestUpdateData.put("valid_till", DateTimeUtils
					.getFutureDateByMinutesFromToday(10,
							DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
			createRequestUpdateData.put("enabled", "true");
			jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, createRequestUpdateData);
			break;
		}
		}

		Map<String, Object> updateRequestUpdateData = new HashMap<String, Object>();
		switch (apiToExecute) {
		case "UPDATE_TD":
			updateRequestUpdateData.put("id", createTDresponse.get("data")
					.asInt());
			System.out.println("TD id for update == "
					+ createTDresponse.get("data").asInt());
			updateRequestUpdateData
					.put("valid_from",
							DateTimeUtils
									.getCurrentDateTime(DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
			updateRequestUpdateData.put("valid_till", DateTimeUtils
					.getFutureDateByMinutesFromToday(1,
							DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
			updateRequestUpdateData.put("enabled", "false");
			jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, updateRequestUpdateData);
			break;
		}

		createTDresponse = RestTestUtil.sendData(
				serviceNameMap.get("td"),
				apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				createTDresponse);
		Assert.assertTrue(status, "Create_TradeDiscount");
	}

	@DataProvider(name = "Update_TD")
	public static Object[][] get_Update_TD_Data()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Update_TD");
	}

	// @Test(priority = 2, dataProvider = "Update_TD")
	public void test_Update_TD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendData(
				serviceNameMap.get("td"),
				apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(status, "Update_TradeDiscount");

	}

	@DataProvider(name = "View_All_TD")
	public static Object[][] get_View_All_TD_Data()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetDataWithAPIFields("View_All_TD");
	}

	// @Test(priority = 3, dataProvider = "View_All_TD")
	public void test_View_All_TD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData, JsonNode apiFieldsData) throws Exception {

		JsonNode response = RestTestUtil.sendData(
				serviceNameMap.get("td"),
				apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(status, "View_ALL_TradeDiscount high level failed ");

		// boolean statusLowLevel = RestExecutor
		// .executeNValidateAPIAsPathParamWithExpecedAPIFields(
		// getEnvPropertyValue("marketing_host"),
		// Integer.parseInt(getEnvPropertyValue("marketing_port")),
		// apiToExecute, jsonRequestData, defaultAuthString,
		// expectedData, ValidationLevels.DETAILED_LEVEL,
		// apiFieldsData);

		// Assert.assertTrue(statusLowLevel,
		// "View_ALL_TradeDiscount Detailed level failed");

	}

	// View_TD_By_ID

	@DataProvider(name = "View_TD_By_ID")
	public static Object[][] get_View_By_ID_Data()
			throws SwiggyAPIAutomationException {
		// return APIDataReader.getDataSetData("View_TD_By_ID");
		return APIDataReader.getDataSetDataWithAPIFields("View_TD_By_ID");
	}

	// @Test(priority = 4, dataProvider = "View_TD_By_ID")
	public void testViewTDByID(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData, JsonNode apiFieldsData) throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(
				serviceNameMap.get("td"),
				apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

		boolean statusHighLevel = JsonValidator.HIGH_LEVEL.validateJson(
				expectedData, response);

		// RestExecutor.executeNValidateAPIAsPathParamWithExpecedAPIFields(
		// getEnvPropertyValue("marketing_host"),
		// Integer.parseInt(getEnvPropertyValue("marketing_port")),
		// apiToExecute, jsonRequestData, expectedData,
		// ValidationLevels.DETAILED_LEVEL, apiFieldsData);

		// boolean statusLowLevel = RestExecutor
		// .executeNValidateAPIAsPathParamWithExpecedAPIFields(
		// getEnvPropertyValue("marketing_host"),
		// Integer.parseInt(getEnvPropertyValue("marketing_port")),
		// apiToExecute, jsonRequestData, defaultAuthString,
		// expectedData, ValidationLevels.DETAILED_LEVEL,
		// apiFieldsData);

		Assert.assertTrue(statusHighLevel, "testViewTDByID");
		// Assert.assertTrue(statusLowLevel,
		// "View Trade Discount by ID Detailed Level failed -- Assert Messåge");

		// try catch in all the methods
		// Assert.fail in catch
		// Remove all RestExecutor
		// Add -ve for all the High Level TC
		// 4993 - automation restaurant

	}

	@DataProvider(name = "getTD_V2_WILLSTARTONMENULISTING")
	public static Object[][] getTD_V2_WILLSTARTONMENULISTING()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TD_V2_WILLSTARTONMENULISTING");
	}

	@Test(priority = 5, dataProvider = "getTD_V2_WILLSTARTONMENULISTING")
	public void testTD_V2_WILLSTARTONMENULISTING(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {

	}

}