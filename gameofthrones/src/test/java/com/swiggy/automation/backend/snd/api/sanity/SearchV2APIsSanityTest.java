package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shwetha
 *
 */
public class SearchV2APIsSanityTest extends RestTestHelper {

	// public static final Logger log = Logger.getLogger(LoginToSession.class);

	@DataProvider(name = "V2RESTSEARCHDP")
	public static Object[][] getCreateCartData12()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V2_REST_SEARCH");
	}

	@Test(priority = 2, dataProvider = "V2RESTSEARCHDP", groups = "Sanity")
	public void V2RESTSEARCHMETHOD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "V2RESTSEARCHMETHOD");
		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "SEARCHITEMDP")
	public static Object[][] getCreateCartData13()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SEARCH_ITEM");
	}

	@Test(priority = 3, dataProvider = "SEARCHITEMDP", groups = "Sanity")
	public void SEARCHITEMMETHOD(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "SEARCHITEMMETHOD");
		} catch (Exception e) {
			Assert.fail();
		}

	}

}