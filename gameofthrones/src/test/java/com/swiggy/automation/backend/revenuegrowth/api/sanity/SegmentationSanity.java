package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class SegmentationSanity extends RestTestHelper {

	@DataProvider(name = "segmentation")
	public static Object[][] getDataItem() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("segmentation");
	}

	@Test(priority = 0, dataProvider = "segmentation")
	public void testGetAddonGroup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "Segmentation" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "Segmentation" + sanityTest);
	}

}
