package com.swiggy.automation.backend.revenuegrowth.api.regression;

import com.swiggy.automation.common.utils.PasswordUtil;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 19/06/17.
 */
public class ListingV4RegressionTest extends RestTestHelper {
    static JsonNode actualResponse = null;
    static JsonNode expectedResponse = null;
    static int restaurant_id = -99;
    static Integer apiPort=0;
    static String apiHost="";
    JsonNode actualFeeDetailsRestaurant=null;


    @DataProvider(name = "BACKEND_SANITY_LOGIN")
    public static Object[][] getLoginData1()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
    }

    @Test(priority = 0, dataProvider = "BACKEND_SANITY_LOGIN")
    public void testV2Login(String dataGroup, String apiToExecute,
                            String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {
        boolean isSuccess = false;
        try {
//            apiHost= getEndPoint("listing_backend_host_qa");
//            apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));

            JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,PasswordUtil.getLoginData(jsonRequestData),  STAFAgent.getSTAFValue("defaultAuthString"));
            isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

            defaultTOKEN = response.get("data").get("token").asText().toString();
            defaulTID = response.get("tid").asText().toString();

            Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
                    response);
            Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);

        } catch (Exception e) {
            log.error(new SwiggyAPIAutomationException(e.toString()));
            Assert.fail();
        }

    }

    @DataProvider(name = "LISTING_V4_REGRESSION")
    public static Object[][] getData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("LISTING_V4_REGRESSION","REGRESSION");
    }

    @Test(priority = 1, dataProvider = "LISTING_V4_REGRESSION")
    public void testV4RestaurantListing(String dataGroup, String apiToExecute,
                                        String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        restaurant_id = Integer.parseInt(  STAFAgent.getSTAFValue("defaultTestRestaurant"));
        expectedResponse = expectedData;

        actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,jsonRequestData,defaulTID,defaultTOKEN);

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
                expectedResponse, actualResponse);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing" + smokeTest);

        isSuccess = testverifyRestaurantObject(dataSetName,actualResponse,expectedResponse,restaurant_id);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing"+regressionTest);

    }

    public boolean testverifyRestaurantObject(String dataSetName, JsonNode actualResponse, JsonNode expectedResponse, Integer restaurant_id)
            throws SwiggyAPIAutomationException {
        JsonNode actual=null;
        JsonNode actualVerifiedNode=JsonTestUtils.verifyRestaurantObjectMessageCount(actualResponse.get("data").get("cards"),"count");

        switch(dataSetName.trim())
        {
            case "FILTER_ON_PURE_VEG":
                JsonTestUtils.verifyOptionSelected(actualResponse.get("data").get("filters"),"Pure Veg");
                actual = JsonTestUtils.getRestaurantObjectUsingElementForVeg(actualVerifiedNode);
                restaurant_id = actual.get("data").get("id").asInt();
                break;
            case "FILTER_ON_OFFERS":
                JsonTestUtils.verifyOptionSelected(actualResponse.get("data").get("filters"),"Offers");
                actual = JsonTestUtils.getRestaurantObjectUsingElementForOffers(actualVerifiedNode);
                restaurant_id = actual.get("data").get("id").asInt();
                break;
            case "FILTER_ON_SWIGGY_ASSURED":
                JsonTestUtils.verifyOptionSelected(actualResponse.get("data").get("filters"),"Swiggy Assured");
                actual = JsonTestUtils.getRestaurantObjectUsingIdForListing(actualVerifiedNode,restaurant_id);
                restaurant_id = actual.get("data").get("id").asInt();
                break;
            case "VERIFY_WELCOME_MESSAGE":
                actualVerifiedNode=JsonTestUtils.verifyRestaurantObjectMessageCount(actualResponse.get("data").get("cards"),"message");
                actual = JsonTestUtils.getRestaurantObjectUsingIdForListing(actualVerifiedNode,restaurant_id);
                restaurant_id = actual.get("data").get("id").asInt();
                break;
            default:
                actual = JsonTestUtils.getRestaurantObjectUsingIdForListing(actualVerifiedNode,restaurant_id);
                break;
        }

        JsonNode expected = JsonTestUtils.getRestaurantObjectUsingIdForListing(expectedResponse.get("data").get("cards"),restaurant_id);

       return JsonValidator.DETAILED_LEVEL.validateJson(expected, actual);

    }

    @Test(priority = 3, dataProvider = "BACKEND_SANITY_LOGIN")
    public void testV2LoginNegative(String dataGroup, String apiToExecute,
                            String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {
        boolean isSuccess = false;
        try {
//            apiHost= getEndPoint("listing_backend_host_qa");
//            apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));

            JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, PasswordUtil.getLoginData(jsonRequestData),  STAFAgent.getSTAFValue("defaultAuthString"));
            isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
                    response);
            defaultTOKEN = response.get("data").get("token").asText().toString();
            defaulTID = response.get("tid").asText().toString();

            Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);

        } catch (Exception e) {
            log.error(new SwiggyAPIAutomationException(e.toString()));
            Assert.fail();
        }

    }

    @DataProvider(name = "LISTING_V4_REGRESSION_NEGATIVE")
    public static Object[][] getNegativeListingData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("LISTING_V4_REGRESSION_NEGATIVE","REGRESSION");
    }

    @Test(priority = 4, dataProvider = "LISTING_V4_REGRESSION_NEGATIVE")
    public void testV4RestaurantListingNegative(String dataGroup, String apiToExecute,
                                                String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,jsonRequestData,defaulTID,defaultTOKEN);

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualResponse);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing" + smokeTest);

        if(dataSetName.contains("INVALID PAGE NUMBER"))
        {
            isSuccess = testverifyRestaurantObject(dataSetName,actualResponse,expectedResponse,restaurant_id);
        }else
        {
            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, actualResponse);
        }

        Assert.assertTrue(isSuccess, "testV4RestaurantListing" + regressionTest);

    }

}
