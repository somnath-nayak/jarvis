package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 16/06/17.
 */
public class MenuV3SanityTest extends RestTestHelper {

	@DataProvider(name = "V3_MENU_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V3_MENU_SANITY");
	}

	@Test(priority = 1, dataProvider = "V3_MENU_SANITY")
	public void testMenuV3(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData);

		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testMenu V3 API" + smokeTest);

		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testMenu V3 API" + sanityTest);

	}

}
