package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DateTimeUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vishal.mahajan on 29/06/17.
 */
public class TradeDiscountV2SanityTest extends RestTestHelper {

        static JsonNode createTDresponse = null;

        @DataProvider(name = "Create_TD_V2")
        public static Object[][] getCreateTDData()
                throws SwiggyAPIAutomationException {
            return APIDataReader.getDataSetData("Create_TD_V2");
        }

        @Test(priority = 1, dataProvider = "Create_TD_V2")
        public void testCreateTD(String dataGroup, String apiToExecute,
                                 String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
                throws Exception {

            Map<String, Object> createRequestUpdateData = new HashMap<String, Object>();

            createRequestUpdateData.put("valid_from", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
            createRequestUpdateData.put("valid_till", DateTimeUtils.getFutureDateBySecondsFromToday(1,
                            DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
            createRequestUpdateData.put("enabled", true);
            jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);


            createTDresponse = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                    apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, createTDresponse);
            Assert.assertTrue(isSuccess, "Create_TradeDiscount"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, createTDresponse);
            Assert.assertTrue(isSuccess, "Create_TradeDiscount"+sanityTest);

            Thread.sleep(1000);
            disableTD(dataGroup,apiToExecute,dataSetName,jsonRequestData,expectedData);
        }


        @DataProvider(name = "Update_TD_V2")
        public static Object[][] get_Update_TD_Data()
                throws SwiggyAPIAutomationException {
            return APIDataReader.getDataSetData("Update_TD_V2");
        }

        @Test(priority = 2, dataProvider = "Update_TD_V2")
        public void testUpdateTD(String dataGroup, String apiToExecute,
                                   String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
                throws Exception {

            Map<String, Object> updateRequestUpdateData = new HashMap<String, Object>();

            updateRequestUpdateData.put("id", createTDresponse.get("data").asInt());
            updateRequestUpdateData.put("valid_from", DateTimeUtils
                                    .getCurrentDateTime(DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
            updateRequestUpdateData.put("valid_till", DateTimeUtils
                    .getFutureDateByMinutesFromToday(3, DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));

            updateRequestUpdateData.put("enabled", true);
            jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, updateRequestUpdateData);


            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                    apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "Update_TradeDiscount"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "Update_TradeDiscount"+sanityTest);

        }

        @DataProvider(name = "View_All_TD_V2")
        public static Object[][] get_View_All_TD_Data()
                throws SwiggyAPIAutomationException {
            return APIDataReader.getDataSetDataWithAPIFields("View_All_TD_V2");
        }

        @Test(priority = 3, dataProvider = "View_All_TD_V2")
        public void testViewAllTD(String dataGroup, String apiToExecute,
                                     String dataSetName, JsonNode jsonRequestData,
                                     JsonNode expectedData, JsonNode apiFieldsData) throws Exception {

            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                    apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "View_ALL_TradeDiscount high level failed "+smokeTest);

        }

        @DataProvider(name = "View_TD_By_ID_V2")
        public static Object[][] get_View_By_ID_Data()
                throws SwiggyAPIAutomationException {
            return APIDataReader.getDataSetDataWithAPIFields("View_TD_By_ID_V2");
        }

         @Test(priority = 4, dataProvider = "View_TD_By_ID_V2")
         public void testViewTDByID(String dataGroup, String apiToExecute,
                                   String dataSetName, JsonNode jsonRequestData,
                                   JsonNode expectedData, JsonNode apiFieldsData) throws Exception {
            Map<String, Object> updateRequestUpdateData = new HashMap<String, Object>();

            updateRequestUpdateData.put("id", createTDresponse.get("data").asInt());
            jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, updateRequestUpdateData);

            JsonNode response = RestTestUtil.sendDataAsPathParam(
            		serviceNameMap.get("td"),
                    apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

            Assert.assertTrue(isSuccess, "testViewTDByID"+smokeTest);

        }


        @DataProvider(name = "View_Campaign_By_ID")
        public static Object[][] get_View_Campaign_By_ID_data()
            throws SwiggyAPIAutomationException {
                return APIDataReader.getDataSetData("View_Campaign_By_ID");
        }

        @Test(priority = 5, dataProvider = "View_Campaign_By_ID")
        public void testViewCampaignByID(String dataGroup, String apiToExecute,
                                     String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "View_Campaign_By_ID"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "View_Campaign_By_ID"+sanityTest);

         }


        @DataProvider(name = "EVALUATE_TD_V2")
         public static Object[][] get_evaluate_listing_data()
                throws SwiggyAPIAutomationException {
            return APIDataReader.getDataSetData("EVALUATE_TD_V2");
        }
        @Test(priority = 6, dataProvider = "EVALUATE_TD_V2")
         public void testEvaluateTD(String dataGroup, String apiToExecute,
                                   String dataSetName, JsonNode jsonRequestData, JsonNode expectedData) throws Exception {

            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                    apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

            Assert.assertTrue(isSuccess, "testEvaluateTD"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "testEvaluateTD" + sanityTest);

        }


        @Test(priority = 7,dataProvider = "Update_TD_V2")
        public void disableTD(String dataGroup, String apiToExecute,
                          String dataSetName,JsonNode jsonRequestData,JsonNode expectedData)
            throws Exception
        {
             Map<String,Object> updateRequestUpdateData=new HashMap<String,Object>();

            updateRequestUpdateData.put("id", createTDresponse.get("data").asInt());

            updateRequestUpdateData.put("valid_from", DateTimeUtils
                .getCurrentDateTime(DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));
            updateRequestUpdateData.put("valid_till", DateTimeUtils
                .getFutureDateBySecondsFromToday(1, DateTimeUtils.FORMAT.EPOCHDATETINE_FORMAT));

            updateRequestUpdateData.put("enabled", false);
            jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, updateRequestUpdateData);


            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("td"),
                apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "Update_TradeDiscount"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "Update_TradeDiscount"+sanityTest);

        }

}
