package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class UserProfileSanityTest extends RestTestHelper {

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 0, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testV2Login(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);
		} catch (Exception e) {
			log.error(new SwiggyAPIAutomationException(e.toString()));
			Assert.fail();
		}

	}

	@DataProvider(name = "GETPROFILE_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETPROFILE_SANITY");
	}

	@Test(priority = 1, dataProvider = "GETPROFILE_SANITY")
	public void testUserProfile(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testUserProfile" + smokeTest);

		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testUserProfile" + sanityTest);
	}

}
