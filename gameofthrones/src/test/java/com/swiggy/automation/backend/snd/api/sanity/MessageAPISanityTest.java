package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class MessageAPISanityTest extends RestTestHelper {

	@DataProvider(name = "MESSAGINGV1_SANITY")
	public static Object[][] viewMsgV1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MESSAGINGV1_SANITY");
	}

	@Test(priority = 1, dataProvider = "MESSAGINGV1_SANITY")
	public void testMessageV1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil
				.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute, jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "testMessageV1" + smokeTest);
		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "testMessageV1" + sanityTest);

	}
}
