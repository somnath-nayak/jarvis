package com.swiggy.automation.backend.snd.api.regression;

import com.swiggy.automation.common.utils.PasswordUtil;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class RestaurantListingRegressionTest extends RestTestHelper {

	static JsonNode actualResponse = null;
	static JsonNode expectedResponse = null;
	static int restaurant_id = -99;
	static Integer apiPort = 0;
	static String apiHost = "";

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 0, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testV2Login(String dataGroup, String apiToExecute,
							String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
//			apiHost = getEndPoint("listing_backend_host_qa");
//			apiPort = Integer
//					.parseInt(getEndPoint("listing_backend_port_qa"));

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),
					apiToExecute, PasswordUtil.getLoginData(jsonRequestData),
					  STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();

			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);
			Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);

		} catch (Exception e) {
			log.error(new SwiggyAPIAutomationException(e.toString()));
			Assert.fail();
		}

	}

	@DataProvider(name = "V3_LISTING_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V3_LISTING_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "V3_LISTING_REGRESSION")
	public void testV3RestaurantListing(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		restaurant_id = Integer
				.parseInt(  STAFAgent.getSTAFValue("defaultTestRestaurant"));
		expectedResponse = expectedData;

		actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),
				apiToExecute, jsonRequestData, defaulTID, defaultTOKEN);

		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
				expectedResponse, actualResponse);
		Assert.assertTrue(isSuccess, "testV3RestaurantListing" + smokeTest);

	}

	@Test(dependsOnMethods = "testV3RestaurantListing")
	public void testverifyRestaurantObject()
			throws SwiggyAPIAutomationException {
		JsonNode actual = JsonTestUtils.getRestaurantObjectUsingId(
				actualResponse.get("data").get("rest_list")
						.findValue("restaurants"), restaurant_id);

		JsonNode expected = JsonTestUtils.getRestaurantObjectUsingId(
				expectedResponse.get("data").get("rest_list")
						.findValue("restaurants"), restaurant_id);

		boolean isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expected,
				actual);
		Assert.assertTrue(isSuccess, "testverifyRestaurantObject");

	}

}
