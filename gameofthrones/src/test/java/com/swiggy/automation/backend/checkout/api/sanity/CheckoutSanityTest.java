package com.swiggy.automation.backend.checkout.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class CheckoutSanityTest extends RestTestHelper {

	public static final Logger log = Logger.getLogger(CheckoutSanityTest.class);

	Long orderId = null;
	String orderKey = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	Long serviceableAddressId = null;
	String oms_osrftoken = null;
	String oms_sessionid = null;

	@BeforeClass
	public void beforeClass() {

		try {
			System.out.println(STAFAgent.getSTAFValue("backend_authstring_username"));
			oms_osrftoken = STAFAgent.getSTAFValue("oms_osrftoken");
			oms_sessionid = STAFAgent.getSTAFValue("oms_sessionid");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			
			JsonNode response = RestTestUtil.sendData(STAFAgent.getSTAFEndPoint("checkout"), apiToExecute, jsonRequestData);
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			isSuccess=true;
			boolean r=JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(r);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}
	}

//	@DataProvider(name = "CART_CIRTICALFLOW")
//	public static Object[][] getCreateCartDataCriticalFlow()
//			throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("CART_CIRTICALFLOW");
//	}

//	 @Test(priority = 5, dataProvider = "CART_CIRTICALFLOW")
//	public void testCartAPI(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//		try {
//			switch (apiToExecute) {
//			case "CREATECART":
//				System.out.println("====CART_CIRTICALFLOW CREATECART=====");
//				RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//						expectedData, JsonValidationLevels.HIGH_LEVEL);
//				break;
//			case "ISADDRESSSERVICEABLE":
//				System.out
//						.println("====CART_CIRTICALFLOW ISADDRESSSERVICEABLE=====");
//				RestExecutor.executeNValidatePathParamAPI(apiToExecute,
//						jsonRequestData, expectedData,
//						JsonValidationLevels.HIGH_LEVEL);
//				break;
//			case "CARTGET":
//				System.out.println("====CART_CIRTICALFLOW CARTGET=====");
//				RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//						expectedData, JsonValidationLevels.HIGH_LEVEL);
//				break;
//			case "UPDATECART":
//				System.out.println("====CART_CIRTICALFLOW UPDATECART=====");
//				RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//						expectedData, JsonValidationLevels.HIGH_LEVEL);
//				break;
//			case "CARTCHECKTOTAL":
//				System.out.println("====CART_CIRTICALFLOW CARTCHECKTOTAL=====");
//				RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//						expectedData, JsonValidationLevels.HIGH_LEVEL);
//				break;
//			case "CARTFLUSH":
//				System.out.println("====CART_CIRTICALFLOW CARTFLUSH=====");
//				RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//						expectedData, JsonValidationLevels.HIGH_LEVEL);
//				break;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			Assert.fail();
//		}
//
//	}
//
//	@DataProvider(name = "ADDRESS_CRITICALFLOW")
//	public static Object[][] getAddressAPI()
//			throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("ADDRESS_CRITICALFLOW");
//	}
//
//	// @Test(priority = 6, dataProvider = "ADDRESS_CRITICALFLOW")
//	public void testAddressAPIs(String dataGroup, String apiToExecute,
//			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
//			throws Exception {
//
//		JsonNode response = null;
//		JsonNode updatedrequest = null;
//		boolean isSuccess = false;
//		switch (apiToExecute) {
//		case "ADDNEWADDRESS":
//			System.out.println("====ADDRESS_CRITICALFLOW ADDNEWADDRESS=====");
//			response = RestTestUtil.sendData(apiToExecute, jsonRequestData);
//			addressId = response.get("data").get("address_id").toString();
//			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(isSuccess, "AddNewAddress");
//			break;
//
//		case "UPDATEADDRESS":
//			System.out.println("====ADDRESS_CRITICALFLOW UPDATEADDRESS=====");
//			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "address_id", addressId);
//			response = RestTestUtil.sendData(apiToExecute, updatedrequest);
//			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(isSuccess, "UpdateAddress");
//			break;
//		case "DELETEADDRESS":
//			System.out.println("====ADDRESS_CRITICALFLOW DELETEADDRESS=====");
//			updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "address_id", addressId);
//			response = RestTestUtil.sendData(apiToExecute, updatedrequest);
//			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(isSuccess, "DeleteAddress");
//			break;
//		case "GETALLADDRESS":
//			System.out.println("====ADDRESS_CRITICALFLOW GETALLADDRESS=====");
//			RestExecutor.executeNValidatePathParamAPI(apiToExecute,
//					expectedData, JsonValidationLevels.HIGH_LEVEL);
//		}
//	}
//
//	@DataProvider(name = "CAPD")
//	public static Object[][] f() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("CAPD");
//	}
//
//	// @Test(priority = 7, dataProvider = "CAPD")
//	public void testEndToEndOrderProcessWithCAPD(String dataGroup,
//			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
//			JsonNode expectedData) throws Exception {
//		JsonNode updatedRequest = null;
//		JsonNode actualData = null;
//		Thread.sleep(5000);
//		switch (apiToExecute) {
//		case "CREATECART":
//			System.out.println("====CAPRD CREATECART=====");
//			JsonNode addressresponse = RestTestUtil.sendData(apiToExecute,
//					jsonRequestData);
//			boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
//					expectedData, addressresponse);
//			if (addressresponse.get("statusCode").asInt() == 6) {
//				System.out.println("RESTUARNT CLOSED");
//				log.error("RESTUARNT CLOSED...SO EXITING");
//				System.exit(0);
//			}
//			Assert.assertTrue(isSuccess, "CREATE_CART");
//			break;
//		case "GETALLADDRESS":
//			System.out.println("====CAPRD GETALLADDRESS=====");
//			actualData = RestTestUtil.sendData(null,null,apiToExecute, defaulTID,
//					defaultTOKEN);
//			serviceableAddressId = JsonTestUtils
//					.getServiceableAddressId(actualData);
//
//			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
//					expectedData, actualData);
//			Assert.assertTrue(status, "testGetAllAddressAPI");
//			break;
//		case "PLACEORDER":
//			System.out.println("====CAPRD PLACEORDER=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "address_id", serviceableAddressId);
//			actualData = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					defaulTID, defaultTOKEN);
//
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					actualData);
//			Assert.assertTrue(status, "testPlaceOrderAPI");
//			orderId = actualData.get("data").get("order_id").asLong();
//			orderIds.add(orderId);
//			orderKey = actualData.get("data").get("key").asText();
//			System.out.println("ORDER ID :::" + orderId);
//			System.out.println("ORDER KEY :::" + orderKey);
//			break;
//
//		case "OMSORDERQUERY":
//			System.out.println("====CAPRD OMSORDERQUERY=====");
//			int counter = 0;
//			status = false;
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			do {
//				actualData = RestTestUtil.sendDataAsPathParamWithOSRFToken(
//						oms_host, oms_port, apiToExecute, updatedRequest,
//						oms_sessionid, oms_osrftoken);
//				System.out.println("OMS QUERY RESPONSE :" + actualData);
//				System.out.println("OMS QUERY RESPONSE OBJECT SIZE:"
//						+ actualData.get("objects").size());
//				if (actualData.get("objects").size() < 1) {
//					System.out.println("Order Not appear on the dashboard");
//					counter++;
//					status = false;
//				} else {
//					status = true;
//					System.out
//							.println("Order appear on the OMS dashboard...So proceeding");
//					log.error("Order appear on the OMS dashboard...So proceeding");
//
//					break;
//				}
//				Thread.sleep(30000);
//			} while (counter < 4);
//
//			Assert.assertTrue(status, "Order appear On OMS dashBoard");
//			break;
//
//		case "OMSUPDATEACTION":
//			System.out.println("====CAPRD OMSUPDATEACTION=====");
//			Thread.sleep(5000);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			actualData = RestTestUtil.sendDataWithOSRFToken(oms_host, oms_port,
//					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
//			Thread.sleep(5000);
//			break;
//
//		case "DEUNASSIGN":
//			System.out.println("====CAPRD DEUNASSIGN=====");
//
//			long assignedOrderId = DeliveryDAO
//					.getDeliveryBoyAssignedOrderId(Integer
//							.parseInt(envProperties.get("de1_for_order")));
//			Thread.sleep(5000);
//			if (assignedOrderId != -999) {
//				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//						jsonRequestData, "order_id", assignedOrderId);
//				actualData = RestTestUtil.sendData(delivery_host,
//						delivery_port, apiToExecute, updatedRequest,
//						defaultAuthString);
//				JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualData);
//			}
//			break;
//		case "OMSDEASSIGN":
//			System.out.println("====CAPRD OMSDEASSIGN=====");
//			Thread.sleep(5000);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					updatedRequest, "delivery_boy_id",
//					envProperties.get("de1_for_order"));
//			actualData = RestTestUtil.sendDataWithOSRFToken(oms_host, oms_port,
//					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
//			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(expectedData,
//					actualData, new String[] { "status", "message" });
//			break;
//
//		case "OMSUPDATESTATUS":
//			System.out.println("====CAPRD OMSUPDATESTATUS=====");
//			System.out.println("********************************");
//			Thread.sleep(10000);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					updatedRequest, "time", System.currentTimeMillis() / 1000);
//			actualData = RestTestUtil.sendDataWithOSRFToken(oms_host, oms_port,
//					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
//			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
//					expectedData, actualData, new String[] { "status",
//							"message" });
//			Assert.assertTrue(status, "OMSUPDATESTATUS :" + dataSetName);
//			Thread.sleep(10000);
//			break;
//
//		case "GETORDERDETAILS":
//			System.out.println("====CAPRD GETORDERDETAILS=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			JsonNode response = RestTestUtil.sendDataAsPathParam(apiToExecute,
//					updatedRequest, defaultAuthString);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(status, "GETORDERDETAILS_" + dataSetName);
//			break;
//		case "GETSINGLEORDER":
//			System.out.println("====CAPRD GETSINGLEORDER=====");
//			if (jsonRequestData.has("order_id")) {
//				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//						jsonRequestData, "order_id", orderId);
//			} else {
//				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//						jsonRequestData, "order_key", orderKey);
//			}
//
//			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
//					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getSingleOrderResponse);
//			Assert.assertTrue(status, "GETSINGLEORDER_" + dataSetName);
//			break;
//
//		case "GETORDERSUMMARY":
//			System.out.println("====CAPRD GETORDERSUMMARY=====");
//			JsonNode getOrderSummary = RestTestUtil.sendData(null,null,apiToExecute);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getOrderSummary);
//			Assert.assertTrue(status, "GETORDERSUMMARY_" + dataSetName);
//			break;
//		case "GETMINIMALORDERDETAILS":
//			System.out.println("====CAPRD GETMINIMALORDERDETAILS=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_key", orderKey);
//			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
//					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getSingleOrderResponse);
//			Assert.assertTrue(status, "GETMINIMALORDERDETAILS_" + dataSetName);
//			break;
//
//		case "TRACKORDER":
//			System.out.println("====CAPRD TRACKORDER=====");
//			if (jsonRequestData.has("order_id")) {
//				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//						jsonRequestData, "order_id", orderId);
//			} else {
//				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//						jsonRequestData, "order_key", orderKey);
//			}
//			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
//					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getSingleOrderResponse);
//			Assert.assertTrue(status, "Track_" + dataSetName);
//
//			break;
//
//		case "TRACKMINIMAL":
//			System.out.println("====CAPRD TRACKMINIMAL=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_key", orderKey);
//
//			getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
//					apiToExecute, updatedRequest, defaulTID, defaultTOKEN);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getSingleOrderResponse);
//			Assert.assertTrue(status, "TRACKMINIMAL_" + dataSetName);
//			break;
//
//		}
//	}
//
//	@DataProvider(name = "EDIT_N_CANCEL_ORDER_BACKEND")
//	public static Object[][] fe() throws SwiggyAPIAutomationException {
//		return APIDataReader.getDataSetData("EDIT_N_CANCEL_ORDER_BACKEND");
//	}
//
//	// @Test(priority = 8, dataProvider = "EDIT_N_CANCEL_ORDER_BACKEND")
//	public void testBackendEditOrderAndCancelAPIs(String dataGroup,
//			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
//			JsonNode expectedData) throws Exception {
//		JsonNode updatedRequest = null;
//		JsonNode actualData = null;
//		Thread.sleep(5000);
//		switch (apiToExecute) {
//		case "CREATECART":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND CREATECART=====");
//			JsonNode addressresponse = RestTestUtil.sendData(apiToExecute,
//					jsonRequestData);
//			boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
//					expectedData, addressresponse);
//			if (addressresponse.get("statusCode").asInt() == 6) {
//				System.out.println("RESTUARNT CLOSED");
//				log.error("RESTUARNT CLOSED...SO EXITING");
//				System.exit(0);
//			}
//			Assert.assertTrue(isSuccess, "CREATE_CART");
//			break;
//		case "GETALLADDRESS":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND GETALLADDRESS=====");
//
//			actualData = RestTestUtil.sendData(null,null,apiToExecute, defaulTID,
//					defaultTOKEN);
//			serviceableAddressId = JsonTestUtils
//					.getServiceableAddressId(actualData);
//
//			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
//					expectedData, actualData);
//			Assert.assertTrue(status, "testGetAllAddressAPI");
//
//			break;
//		case "PLACEORDER":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND PLACEORDER=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "address_id", serviceableAddressId);
//			actualData = RestTestUtil.sendData(apiToExecute, updatedRequest,
//					defaulTID, defaultTOKEN);
//
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					actualData);
//			Assert.assertTrue(status, "testPlaceOrderAPI");
//			orderId = actualData.get("data").get("order_id").asLong();
//			orderIds.add(orderId);
//			orderKey = actualData.get("data").get("key").asText();
//			System.out.println("ORDER ID :::" + orderId);
//			System.out.println("ORDER KEY :::" + orderKey);
//			break;
//
//		case "OMSORDERQUERY":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND OMSORDERQUERY=====");
//			int counter = 0;
//			status = false;
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			do {
//				actualData = RestTestUtil.sendDataAsPathParamWithOSRFToken(
//						oms_host, oms_port, apiToExecute, updatedRequest,
//						oms_sessionid, oms_osrftoken);
//				System.out.println("OMS QUERY RESPONSE :" + actualData);
//				System.out.println("OMS QUERY RESPONSE OBJECT SIZE:"
//						+ actualData.get("objects").size());
//				if (actualData.get("objects").size() < 1) {
//					System.out.println("Order Not appear on the dashboard");
//					counter++;
//					status = false;
//				} else {
//					status = true;
//					System.out
//							.println("Order appear on the OMS dashboard...So proceeding");
//					log.info("Order appear on the OMS dashboard...So proceeding");
//
//					break;
//				}
//				Thread.sleep(30000);
//			} while (counter < 4);
//
//			Assert.assertTrue(status, "Order appear On OMS dashBoard");
//			break;
//
//		case "UPDATECHECK":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND UPDATECHECK=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			JsonNode getOrderDetailsResponse = RestTestUtil.sendData(
//					apiToExecute, updatedRequest, defaultAuthString);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getOrderDetailsResponse);
//			Assert.assertTrue(status, "UPDATECHECK_" + dataSetName);
//			break;
//		case "UPDATECONFIRM":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND UPDATECONFIRM=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			JsonNode response = RestTestUtil.sendData(apiToExecute,
//					updatedRequest, defaultAuthString);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(status, "UPDATECONFIRM_" + dataSetName);
//			break;
//		case "GETORDERDETAILS":
//			System.out
//					.println("====EDIT_N_CANCEL_ORDER_BACKEND GETORDERDETAILS=====");
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			getOrderDetailsResponse = RestTestUtil.sendDataAsPathParam(
//					apiToExecute, updatedRequest, defaultAuthString);
//			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
//					getOrderDetailsResponse);
//			Assert.assertTrue(status, "testGetOrderDetailsAPI");
//			break;
//		case "CANCEL":
//			System.out.println("====EDIT_N_CANCEL_ORDER_BACKEND CANCEL=====");
//			RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//					expectedData, JsonValidationLevels.HIGH_LEVEL);
//			break;
//		}
//	}
//
//	@SuppressWarnings("unchecked")
//	@AfterTest()
//	public void cancelOrders() throws Exception {
//		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
//		@SuppressWarnings("rawtypes")
//		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
//		requestMap.put("order_id", 1062970706); // dummy structure
//		requestMap.put("status", "cancelled");
//		requestMap.put("time", 1467674409675L); // dummy structure
//		expectedResponse.put("status", true);
//		expectedResponse.put("message", "Order status updated to cancelled");
//		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
//		JsonNode expectedResponseData = RestTestUtil
//				.getJsonNodeFromMap(expectedResponse);
//		if (orderIds.size() > 0)
//			log.info("No of Orders to be cancelled :" + orderIds.size());
//		for (Long orderId : orderIds) {
//			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					jsonRequestData, "order_id", orderId);
//			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
//					updatedRequest, "time", System.currentTimeMillis() / 1000);
//			JsonNode actualData = RestTestUtil.sendDataWithOSRFToken(oms_host,
//					oms_port, "OMSUPDATESTATUS", updatedRequest, oms_sessionid,
//					oms_osrftoken);
//			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
//					expectedResponseData, actualData, new String[] { "status",
//							"message" });
//			Thread.sleep(2000);
//		}
//
//	}
}
