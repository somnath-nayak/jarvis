package com.swiggy.automation.backend.checkout.api.sanity;

import java.util.LinkedHashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.JsonValidationLevels;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OrrderSanityTest extends RestTestHelper {

	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	String oms_osrftoken = null;
	String oms_sessionid = null;

	@BeforeClass
	public void beforeClass() {

		try {
			oms_osrftoken = STAFAgent.getSTAFValue("oms_osrftoken");
			oms_sessionid = STAFAgent.getSTAFValue("oms_sessionid");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}

	}

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText().toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "CREATE_CART_SANITY")
	public static Object[][] getCreateCartData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CART_SANITY");
	}

	@Test(priority = 2, dataProvider = "CREATE_CART_SANITY")
	public void testCreateCartAPI(String dataGroup, String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		try {
//			RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData, expectedData,
//					JsonValidationLevels.HIGH_LEVEL);
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ALLADDRESS_SANITY")
	public static Object[][] getAllAddress() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALLADDRESS_SANITY");
	}

	@Test(priority = 3, dataProvider = "GET_ALLADDRESS_SANITY")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		try {
			JsonNode actualData = RestTestUtil.sendData(apiToExecute, defaulTID, defaultTOKEN);
			serviceableAddressId = JsonTestUtils.getServiceableAddressId(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI");
			Thread.sleep(5000);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "GET_ALL_PAYMENTSAPI_SANITY")
	public static Object[][] getAllPaymentOptions() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALL_PAYMENTSAPI_SANITY");
	}

	@Test(priority = 4, dataProvider = "GET_ALL_PAYMENTSAPI_SANITY")
	public void testGetAllPaymentAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		try {
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, jsonRequestData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualData);
			Assert.assertTrue(status, "testGetAllPaymentAPI" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, actualData);
			Assert.assertTrue(status, "testGetAllPaymentAPI" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PLACE_ORDER_SANITY")
	public static Object[][] getPlaceOrderData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PLACE_ORDER_SANITY");
	}

	@Test(priority = 5, dataProvider = "PLACE_ORDER_SANITY")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {

		try {
			// Thread.sleep(5000);
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "address_id",
					serviceableAddressId);
			System.out.println(jsonRequestData);
			System.out.println(updatedRequest);
			JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute, updatedRequest,
					defaulTID, defaultTOKEN);
			log.info("****************************************************");

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + sanityTest);
			orderId = placeOrderResponse.get("data").get("order_id").asLong();
			orderIds.add(orderId);
			orderKey = placeOrderResponse.get("data").get("key").asText().toString();
			System.out.println("ORDER ID :::" + orderId);
			System.out.println("ORDER KEY :::" + orderKey);

			orderKey = placeOrderResponse.get("data").get("key").asText();
			System.out.println("ORDER KEY :::" + orderKey);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "GET_ORDER_DETAILS_SANITY")
	public static Object[][] getOrderDetailsData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ORDER_DETAILS_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 6, dataProvider = "GET_ORDER_DETAILS_SANITY")
	public void testGetOrderDetailsAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		try {
			// Thread.sleep(30000);
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_id", orderId);
			JsonNode getOrderDetailsResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute,
					updatedRequest,   STAFAgent.getSTAFValue("defaultAuthString"));
			// JsonHandlerToCheckDiffFields.handleObject(expectedData,
			// getOrderDetailsResponse);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, getOrderDetailsResponse);
			Assert.assertTrue(status, "testGetOrderDetailsAPI" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, getOrderDetailsResponse);
			Assert.assertTrue(status, "testGetOrderDetailsAPI" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_SINGLE_ORDER_SANITY")
	public static Object[][] getSingleOrderData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_SINGLE_ORDER_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 7, dataProvider = "GET_SINGLE_ORDER_SANITY")
	public void testGetSingleOrderAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_id", orderId);
			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute,
					updatedRequest, defaulTID, defaultTOKEN);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, getSingleOrderResponse);
			Assert.assertTrue(status, "testGetSingleOrderAPI" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, getSingleOrderResponse);
			Assert.assertTrue(status, "testGetSingleOrderAPI" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ORDER_SUMMARY_SANITY")
	public static Object[][] getOrderSummary() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ORDER_SUMMARY_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 8, dataProvider = "GET_ORDER_SUMMARY_SANITY")
	public void testGetOrderSummaryAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		System.out.println("====CAPRD GETORDERSUMMARY=====");
		int count = 0;
		int statusCode = -888;
		JsonNode getOrderSummary = null;
		do {
			try {
				getOrderSummary = RestTestUtil.sendData(serviceNameMap.get("checkout"), apiToExecute);
				statusCode = getOrderSummary.get("statusCode").asInt();
				if (statusCode == 0) {
					break;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
			Thread.sleep(15000);
		} while (count++ < 3);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, getOrderSummary);
		Assert.assertTrue(status, "GETORDERSUMMARY_" + dataSetName);
	}

	@DataProvider(name = "GET_MINIMAL_ORDER_DETAILS_SANITY")
	public static Object[][] getMinialOrderDetailsData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_MINIMAL_ORDER_DETAILS_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 9, dataProvider = "GET_MINIMAL_ORDER_DETAILS_SANITY")
	public void testGetMinmalOrderAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_key", orderKey);
			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute,
					updatedRequest, defaulTID, defaultTOKEN);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, getSingleOrderResponse);
			Assert.assertTrue(status, "testGetMinmalOrderAPI" + smokeTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "TRACK_ORDER_SANITY")
	public static Object[][] getTrackOrderDetailsData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TRACK_ORDER_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 10, dataProvider = "TRACK_ORDER_SANITY")
	public void testTrackOrderOrderAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		JsonNode updatedRequest = null;
		JsonNode response = null;
		boolean status = false;
		try {
			if (jsonRequestData.has("order_id")) {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_id", orderId);
			} else {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_key", orderKey);
			}
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, updatedRequest, defaulTID,
					defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(status, "testTrackOrderOrderAPI" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(status, "testTrackOrderOrderAPI" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "TRACK_ORDER_MINIMAL_SANITY")
	public static Object[][] getTrackMinimalOrderDetailsData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TRACK_ORDER_MINIMAL_SANITY");
	}

	@Test(dependsOnMethods = "testPlaceOrderAPI", priority = 11, dataProvider = "TRACK_ORDER_MINIMAL_SANITY")
	public void testTrackMinimalOrderOrderAPI(String dataGroup, String apiToExecute, String dataSetName,
			JsonNode jsonRequestData, JsonNode expectedData) throws Exception {
		JsonNode updatedRequest = null;
		JsonNode response = null;
		boolean status = false;

		try {
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_key", orderKey);
			response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"), apiToExecute, updatedRequest, defaulTID,
					defaultTOKEN);
			status = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(status, "testTrackMinimalOrderOrderAPI" + smokeTest);
			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
			Assert.assertTrue(status, "testTrackMinimalOrderOrderAPI" + sanityTest);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@SuppressWarnings("unchecked")
	@AfterTest()
	public void cancelOrders() throws Exception {
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil.getJsonNodeFromMap(expectedResponse);
		if (orderIds.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : orderIds) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(updatedRequest, "time",
					System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest,
					RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(expectedResponseData, actualData,
					new String[] { "status", "message" });
			Thread.sleep(2000);
		}

	}

}
