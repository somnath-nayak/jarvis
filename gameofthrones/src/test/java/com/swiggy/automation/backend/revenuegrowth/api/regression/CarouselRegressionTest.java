package com.swiggy.automation.backend.revenuegrowth.api.regression;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DateTimeUtils;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.codehaus.jackson.JsonNode;
import org.joda.time.DateTime;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vishal.mahajan on 19/07/17.
 */
public class CarouselRegressionTest extends RestTestHelper{
    static JsonNode actualResponse = null;
    static JsonNode expectedResponse = null;
    static JsonNode carousleResponse =null;
    static int restaurant_id = -99;
    static Integer apiPort=0;
    static String apiHost="";
    static int polygonId = -1;
    static int carousleId=0;



    @DataProvider(name = "CREATE_POLYGON_SANITY")
    public static Object[][] cd() throws SwiggyAPIAutomationException {
        return APIDataReader
                .getDataSetDataWithAPIFields("CREATE_POLYGON_SANITY");
    }

    @Test(priority = 0, dataProvider = "CREATE_POLYGON_SANITY")
    public void testCreatePolyogonDetail(String dataGroup, String apiToExecute,
                                         String dataSetName, JsonNode jsonRequestData,
                                         JsonNode expectedData, JsonNode apiFieldsData) throws Exception {
        boolean isSuccess = false;

        try {
            JsonNode res = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
                    jsonRequestData,couponsDataServiceAuthString);
            polygonId = res.get("data").asInt();
           isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, res);
           Assert.assertTrue(isSuccess, "testCreatePolyogon");

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,res, apiFieldsData);
            Assert.assertTrue(isSuccess, "testCreatePolyogon");

        } catch (Exception e) {
            log.error(e);
            Assert.fail();
        }
    }

    @DataProvider(name = "CAROUSEL_REGRESSION")
    public static Object[][] getCaraouselData()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("CAROUSEL_REGRESSION","REGRESSION");
    }
    @Test(priority = 1, dataProvider = "CAROUSEL_REGRESSION")
    public void testCarouselRegression(String dataGroup, String apiToExecute,
                             String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        if(dataSetName.startsWith("CREATE_CAROUSEL"))
        {

            Map<String, Object> createRequestUpdateData = new HashMap<String, Object>();
            List<Integer> polygonIdList= new ArrayList<Integer>();
            polygonIdList.add(polygonId);
            createRequestUpdateData.put("addPolygonIds",polygonIdList);
            DateTime dt= new DateTime();
            DateTime.Property pDoW;

            switch (dataSetName) {

                case "CREATE_CAROUSEL_ON_SAME_DAY":
                {

                    pDoW = dt.dayOfWeek();
                    createRequestUpdateData.put("startDateTime", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));
                    createRequestUpdateData.put("endDateTime", DateTimeUtils.getFutureDateByMinutesFromToday(3, DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));

                    jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);
                    jsonRequestData = RestTestUtil.updateRequestDataBeforeSendForGetCarouselDay(jsonRequestData, "timeSlots","day",pDoW.getAsShortText().toString());
                    break;
                }
                case "CREATE_CAROUSEL_ON_NEXT_DAY":
                {
                    DateTime  pw= dt.plusDays(1);
                    DateTime.Property nDoW = pw.dayOfWeek();

                    createRequestUpdateData.put("startDateTime", DateTimeUtils.getFutureDateByMinutesFromToday(1440,DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));
                    createRequestUpdateData.put("endDateTime", DateTimeUtils.getFutureDateByMinutesFromToday(1443, DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));

                    jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);
                    jsonRequestData = RestTestUtil.updateRequestDataBeforeSendForGetCarouselDay(jsonRequestData, "timeSlots","day",nDoW.getAsShortText());
                    break;
                }
                default:
                {
                    createRequestUpdateData.put("startDateTime", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));
                    createRequestUpdateData.put("endDateTime", DateTimeUtils.getFutureDateByMinutesFromToday(5, DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));
                    jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);
                    break;
                }

            }
            carousleResponse = RestTestUtil.sendData(
            		serviceNameMap.get("checkout"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, carousleResponse);
            Assert.assertTrue(isSuccess, "CAROUSEL_SMOKE" + smokeTest);


            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, carousleResponse);
            Assert.assertTrue(isSuccess, "CAROUSEL_SANITY" + sanityTest);

        }else if (dataSetName.contains("GET_ALL_CAROUSEL_BANNER")){

            carousleId =carousleResponse.get("data").asInt();

            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("checkout"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            JsonNode actualCarousel = JsonTestUtils.getRestaurantObjectUsingId(response.get("data"),carousleId);
            JsonNode expectedCarousel = JsonTestUtils.getFirstObjectFromArray(expectedData.get("data"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "GET_CAROUSEL" + smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedCarousel, actualCarousel);
            Assert.assertTrue(isSuccess, "GET_CAROUSEL" + sanityTest);


        }else {
            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("checkout"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "CAROUSEL_SMOKE"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "CAROUSEL_SANITY"+sanityTest);


        }
    }


    @DataProvider(name = "BACKEND_SANITY_LOGIN")
    public static Object[][] getLoginData1()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
    }

    @Test(priority = 2, dataProvider = "BACKEND_SANITY_LOGIN")
    public void testV2Login(String dataGroup, String apiToExecute,
                            String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {
        boolean isSuccess = false;
        try {
//            apiHost= getEndPoint("listing_backend_host_qa");
//            apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));

            JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute, PasswordUtil.getLoginData(jsonRequestData),  STAFAgent.getSTAFValue("defaultAuthString"));
            isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

            defaultTOKEN = response.get("data").get("token").asText().toString();
            defaulTID = response.get("tid").asText().toString();

            Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
                    response);
            Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);

        } catch (Exception e) {
            log.error(new SwiggyAPIAutomationException(e.toString()));
            Assert.fail();
        }

    }


    @DataProvider(name = "V4_LISTING_SANITY")
    public static Object[][] getData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("V4_LISTING_SANITY");
    }

    @Test(priority = 3, dataProvider = "V4_LISTING_SANITY")
    public void testCaraouselV4ListingIntegration(String dataGroup, String apiToExecute,
                                        String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        restaurant_id = Integer.parseInt(  STAFAgent.getSTAFValue("defaultTestRestaurant"));
        expectedResponse = expectedData;

        Map<String,Object> headers=new HashMap<String,Object>();
        headers.put("tid",defaulTID);
        headers.put("token",defaultTOKEN);
        headers.put("User-Agent","Swiggy-Android");

        actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,jsonRequestData,headers);

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedResponse, actualResponse);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing" + smokeTest);

        isSuccess = testVerifyCarauselListing(dataSetName,actualResponse,expectedResponse,restaurant_id);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing"+regressionTest);


    }
    public boolean testVerifyCarauselListing(String dataSetName, JsonNode actualResponse, JsonNode expectedResponse, Integer restaurant_id)
            throws SwiggyAPIAutomationException {
        JsonNode actual=null;
        JsonNode actualVerifiedNode=JsonTestUtils.verifyRestaurantObjectMessageCount(actualResponse.get("data").get("cards"),"count");

        actual = JsonTestUtils.getCaraouselNode(actualVerifiedNode,carousleId);

        JsonNode expected = JsonTestUtils.getCaraouselNode(expectedResponse.get("data").get("cards"),carousleId);

        return JsonValidator.DETAILED_LEVEL.validateJson(expected, actual);

    }
 }
