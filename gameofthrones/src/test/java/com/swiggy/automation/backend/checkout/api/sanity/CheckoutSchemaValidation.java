package com.swiggy.automation.backend.checkout.api.sanity;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.CheckoutSchemaDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;

import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

public class CheckoutSchemaValidation extends CheckoutSchemaDP{

	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	CheckoutHelper helper= new CheckoutHelper();
	SoftAssert softAssertion= new SoftAssert();
		
	String tid;
    String token;
    
    String schemaPath= SchemaValidationConstants.SCHEMA_PATH;
    String mobile= System.getenv("mobile");
	String password= System.getenv("password");
	
    @BeforeClass
    public void login(){
    	if (mobile == null || mobile.isEmpty()){
    		mobile=CheckoutConstants.mobile;}
    	
    	if (password == null ||password.isEmpty()){
    		password=CheckoutConstants.password;}
    	
    	HashMap<String, String> hashMap=helper.TokenData(mobile,password);
    	tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }
    

    @Test(dataProvider="schemaValidation", groups = {"schemaValidation","chandan"},description = "schema Validation Test")
	public void createCartSchemaValidatorTest(String userAgent,String versionCode,Cart cartPayload)throws IOException, ProcessingException {
		
		String cartResponse=createCart(userAgent,versionCode,cartPayload);
		String fileName="createCart"+"_"+userAgent+".txt";
		//writeSchemaIntoFile(cartResponse, fileName);
        validateSchema(cartResponse,fileName,"Regular create cart");
		
		String getCartRes=getCart(userAgent,versionCode,cartPayload);
		fileName="getCart"+"_"+userAgent+".txt";
		validateSchema(getCartRes,fileName,"Regular get cart");
		
		String getAllPaymentRes=getAllPayments(userAgent,versionCode);
		fileName="getAllPaymentRes"+"_"+userAgent+".txt";
        validateSchema(getAllPaymentRes,fileName,"get All Payment");
		
		String placeOrderRes=placeOrder(userAgent,versionCode,cartPayload);
		fileName="placeOrder"+"_"+userAgent+".txt";
        validateSchema(placeOrderRes,fileName,"Regular Place Order");
        
		String OrderId=getOrderId(placeOrderRes);
		String OrderKey=getOrderKey(placeOrderRes);
		
		String getOrderRes=getOrderDetails(userAgent,versionCode,OrderId);
		fileName="getOrder"+"_"+userAgent+".txt";
		validateSchema(getOrderRes, fileName,"Get Order");
		
		String getSingleOrderRes=getSingleOrderKey(userAgent,versionCode,OrderKey);
		fileName="getSingleOrder"+"_"+userAgent+".txt";
		validateSchema(getSingleOrderRes, fileName,"Get Order");
		
		String getLastOrderRes=getLastOrder(userAgent, versionCode);    
		fileName="getLastOrder"+"_"+userAgent+".txt";
		validateSchema(getLastOrderRes, fileName,"get Last Order");
		
		String getMinimalOrderDetails=getMinimalOrderDetails(userAgent, versionCode,OrderKey);
		fileName="getMinimalOrderDetails"+"_"+userAgent+".txt";
		validateSchema(getMinimalOrderDetails, fileName,"get Minimal Order Details");
		
		String getOrderDetails=getOrderDetails(userAgent, versionCode,OrderId);    
		fileName="getOrderDetails"+"_"+userAgent+".txt";
		validateSchema(getOrderDetails, fileName,"get Order Details");
		
		String getAllOrdersRes=getAllOrders(userAgent, versionCode);  
		fileName="getAllOrder"+"_"+userAgent+".txt";
		validateSchema(getAllOrdersRes, fileName,"get All Orders");
		
		/*String confirmOrderRes=confirmOrder(userAgent, versionCode);
		fileName="confirmOrder"+"_"+userAgent+".txt";
		//validateSchema(confirmOrderRes, fileName,"confirmOrder");
*/		
		String trackRes=trackOrder(userAgent,versionCode,OrderId);
    	fileName="trackOrder"+"_"+userAgent+".txt";
    	validateSchema(trackRes, fileName,"track Order");
    	
    	String trackMinRes=trackMinimalOrder(userAgent,versionCode,OrderKey);
    	fileName="trackMinOrder"+"_"+userAgent+".txt";
    	validateSchema(trackMinRes, fileName,"track Minimal Order");
    	
    	String editOrderCheckRes=editOrderCheck(userAgent, versionCode, cartPayload, OrderId);
		fileName="editOrderCheck"+"_"+userAgent+".txt";
		validateSchema(editOrderCheckRes, fileName,"edit Order Check");
		
		String editOrderConfirmRes=editOrderConfirm(userAgent, versionCode, cartPayload, OrderId);
		fileName="editOrderConfirm"+"_"+userAgent+".txt";
		validateSchema(editOrderConfirmRes, fileName,"edit Order Confirm");
		
		String orderCancelCheckRes=orderCancelCheck(userAgent, versionCode, OrderId);
		fileName="orderCancelCheck"+"_"+userAgent+".txt";
		//validateSchema(orderCancelCheckRes, fileName,"cancel Order check");
		
		String cancelOrderRes=cancelOrder(userAgent, versionCode, OrderId);
		fileName="cancelOrder"+"_"+userAgent+".txt";
		validateSchema(cancelOrderRes, fileName,"cancel Order");
		
    	String cloneOrderCheckRes=cloneOrderCheck(userAgent, versionCode, cartPayload, OrderId);
		fileName="cloneOrderCheck"+"_"+userAgent+".txt";
		validateSchema(cloneOrderCheckRes, fileName,"clone Order Check");
    	
    	/*String cloneOrderConfirmRes=cloneOrderConfirm(userAgent, versionCode, cartPayload, OrderId);
    	fileName="cloneOrderConfirm"+"_"+userAgent+".txt";
    	writeSchemaIntoFile(cloneOrderConfirmRes, fileName);
    	validateSchema(cloneOrderCheckRes, fileName,"clone Order Confirm");
    	
    	String cloneOrderId=getOrderId(cloneOrderConfirmRes);
		cancelOrder(userAgent, versionCode, cloneOrderId);*/
		softAssertion.assertAll();
	}
	

	
	
	public void validateSchema(String response,String fileName,String message) throws IOException, ProcessingException{
		String expectedSchema = getSchemafromFile(schemaPath,fileName);
		System.out.println("Expected jsonschema   $$$$" + expectedSchema);
		List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(expectedSchema, response);
		softAssertion.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For "+message+" API");
		boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
		Reporter.log("FileName: " + fileName,true);
		Reporter.log("Contain empty nodes=>" + isEmpty,true);
		softAssertion.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
	}

	public String getSchemafromFile(String schemaPath,String fileName) throws IOException, ProcessingException{
		return new ToolBox().readFileAsString(System.getProperty("user.dir") + schemaPath+fileName);
	}
	
	public void writeSchemaIntoFile(String response,String fileName) throws IOException, ProcessingException{
		System.out.println("file name: "+fileName);
	    String s=System.getProperty("user.dir") + schemaPath+fileName;
	    System.out.println(s);
		FileWriter f1=new FileWriter(s);
		BufferedWriter writer = new BufferedWriter(f1);
	    writer.write(response);
	    writer.close();
	}
	
  public String createCart(String userAgent,String versionCode,Cart cartPayload){
	  HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String cartResponse=helper.createCart(header,Utility.jsonEncode(cartPayload))
				                  .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(cartResponse);
      
    return cartResponse;
  }
  
  public String getCart(String userAgent,String versionCode,Cart cartPayload){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getCartRes=helper.getCart(header).ResponseValidator.GetBodyAsText();
		helper.validateApiResStatusData(getCartRes);
    return getCartRes;
  }
  
  public String getAllPayments(String userAgent,String versionCode){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getAllPaymentsRes=helper.getAllPayments(header).ResponseValidator.GetBodyAsText();
		helper.validateApiResStatusData(getAllPaymentsRes);
   return getAllPaymentsRes;
 }
  
  public String placeOrder(String userAgent,String versionCode,Cart cartPayload){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
	    String cartResponse=createCart(userAgent,versionCode,cartPayload);
		String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
		String placeOrderRes=helper.orderPlaceV2(header, addressId,superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
				.ResponseValidator.GetBodyAsText();
		helper.validateApiResStatusData(placeOrderRes);
  return placeOrderRes;
   }
  
  public String getOrderId(String placeOrderRes){
  	String orderId=JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
  	System.out.println("Order Id:--->"+orderId);
  	return orderId;
  }
  
  public String getOrderKey(String placeOrderRes){
  	String[] rendkeys=JsonPath.read(placeOrderRes, "$.data..order_data..rendering_details..key")
  			                  .toString().replace("[","").replace("]","").replace("\"","").split(",");
	  	String[] keys=JsonPath.read(placeOrderRes, "$.data..order_data..key")
	  			                  .toString().replace("[","").replace("]","").replace("\"","").split(",");

	  	 ArrayList<String> key = new ArrayList<String>();
       for(int i = 0; i < keys.length; i++){
      	 if(!Arrays.asList(rendkeys).contains(keys[i]))
              	 key.add(keys[i]);
           }
	  	System.out.println("Order Key:--->"+key.get(0));
	  	return key.get(0);
	 }
  
  public String getOrderDetails(String userAgent,String versionCode,Cart cartPayload){
	  String placeOrderRes=placeOrder(userAgent,versionCode,cartPayload);
		String getOrderRes=helper.getOrderDetails(tid, token,getOrderId(placeOrderRes))
				                 .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(getOrderRes);
      return getOrderRes;
 }
  
    public String getOrderDetails(String userAgent,String versionCode,String OrderID){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getOrderRes=helper.getOrderDetails(header,OrderID)
				                 .ResponseValidator.GetBodyAsText();
      helper.validateApiResStatusData(getOrderRes);
      return getOrderRes;
    }
  
    public String getSingleOrderKey(String userAgent,String versionCode,String orderKey){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getSingleOrderRes=helper.getSingleOrderKey(header,orderKey)
				                 .ResponseValidator.GetBodyAsText();
    helper.validateApiResStatusData(getSingleOrderRes);
    return getSingleOrderRes;
    }
  
    public String getLastOrder(String userAgent,String versionCode){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getLastOrderRes=helper.getLastOrder(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getLastOrderRes);
        return getLastOrderRes;
     }
    
    public String getMinimalOrderDetails(String userAgent,String versionCode,String orderKey){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getLastOrderRes=helper.getMinimalOrderDetails(header,orderKey).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getLastOrderRes);
        return getLastOrderRes;
     }
    
    public String getAllOrders(String userAgent,String versionCode){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String getAllOrdersRes=helper.getAllOrders(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllOrdersRes);
        return getAllOrdersRes;
     }
    
    public String confirmOrder(String userAgent,String versionCode){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String confirmOrderRes=helper.confirmOrder(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(confirmOrderRes);
        return confirmOrderRes;
     }
    
    public String trackOrder(String userAgent,String versionCode,String OrderID){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String trackOrderRes=helper.track(header,OrderID).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(trackOrderRes);
        return trackOrderRes;
     }
    
    public String trackMinimalOrder(String userAgent,String versionCode,String OrderKey){
	    HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
		String trackMinOrderRes=helper.trackMinimalOrder(header,OrderKey).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(trackMinOrderRes);
        return trackMinOrderRes;
     }
    
    public String editOrderCheck(String userAgent,String versionCode,Cart cartPayload,String OrderID){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
        String[] editOrderPayload=new String[]{OrderID, CheckoutConstants.INITIATION_SOURCE, rawEditOrderPayload};
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
        String editOrderRes=helper.editOrderCheck(header,editOrderPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(editOrderRes);
        return editOrderRes;
     }
    
    public String editOrderConfirm(String userAgent,String versionCode,Cart cartPayload,String OrderID){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
    	String[] confirmEditOrderPayload=new String[]{OrderID, CheckoutConstants.orderComments, rawEditOrderPayload};
    	HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
    	String editConfirmOrderRes=helper.editOrderConfirm(header,confirmEditOrderPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(editConfirmOrderRes);
        return editConfirmOrderRes;
     }
    
    public String cancelOrder(String userAgent,String versionCode,String OrderID){
    	HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
    	String cancelOrderRes=helper.OrderCancel(header, superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, OrderID)
                .ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cancelOrderRes);
        return cancelOrderRes;
     }
    
    public String orderCancelCheck(String userAgent,String versionCode,String OrderID){
    	HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
    	String payload=helper.orderCancelCheckPayload(OrderID, false);
    	String orderCancelCheckRes=helper.orderCancelCheck(header, payload)
                .ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(orderCancelCheckRes);
        return orderCancelCheckRes;
     }
  
    public String cloneOrderCheck(String userAgent,String versionCode,Cart cartPayload,String OrderID){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
    	String[] cloneCheckResPayload=new String[]{OrderID,CheckoutConstants.INITIATION_SOURCE,rawEditOrderPayload};
    	HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
    	String cloneCheckRes=helper.cloneOrderCheck(header,cloneCheckResPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cloneCheckRes);
        return cloneCheckRes;
     }
    
    public String cloneOrderConfirm(String userAgent,String versionCode,Cart cartPayload,String OrderID){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
    	String[] cloneOrderConfirmPayload=new String[]{OrderID,CheckoutConstants.INITIATION_SOURCE,rawEditOrderPayload};
    	HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);
    	String cloneOrderConfirmRes=helper.cloneOrderConfirm(header,cloneOrderConfirmPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cloneOrderConfirmRes);
        return cloneOrderConfirmRes;
     }
    
}


	
