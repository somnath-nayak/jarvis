package com.swiggy.automation.backend.snd.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

// TODO: Auto-generated Javadoc

/**
 * 
 * @author mohammedramzi
 *
 */
public class BackendLoginSanityTest extends RestTestHelper {

	/** The Constant log. */
	public static final Logger log = Logger
			.getLogger(BackendLoginSanityTest.class);

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testV2Login(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(true, "testV2Login" + smokeTest);
//			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
//					response);
//			Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);
		} catch (Exception e) {
			log.error(new SwiggyAPIAutomationException(e.toString()));
			Assert.fail();
		}

	}

}
