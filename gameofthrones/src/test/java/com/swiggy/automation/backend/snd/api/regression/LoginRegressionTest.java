package com.swiggy.automation.backend.snd.api.regression;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

// TODO: Auto-generated Javadoc

/**
 * 
 * @author mohammedramzi
 *
 */
public class LoginRegressionTest extends RestTestHelper {

	/** The Constant log. */
	public static final Logger log = Logger
			.getLogger(LoginRegressionTest.class);

	@DataProvider(name = "BACKEND_LOGIN_REGRESSION")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_LOGIN_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "BACKEND_LOGIN_REGRESSION")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
//			RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//					expectedData, JsonValidationLevels.DETAILED_LEVEL);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
						jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, response);
			Assert.assertTrue(isSuccess);
		} catch (Exception e) {
			Assert.fail();
		}
	}
}
