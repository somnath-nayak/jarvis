package com.swiggy.automation.backend.checkout.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class HelpAPISanity extends RestTestHelper {

	static int item_id = 1222;
	static JsonNode actualResponse = null;
	static JsonNode expectedResponse = null;

	public static final Logger log = Logger.getLogger(CartV2SanityTest.class);

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = -12, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");

			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "GETHELP_API_SANITY")
	public static Object[][] getHelp() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETHELP_API_SANITY");
	}

	@Test(dataProvider = "GETHELP_API_SANITY")
	public void getHelp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCart" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCart" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "GETHELP_RECENT_API_SANITY")
	public static Object[][] testHelpRecent()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETHELP_RECENT_API_SANITY");
	}

	@Test(priority = 1, dataProvider = "GETHELP_RECENT_API_SANITY")
	public void testHelpRecent(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		expectedResponse = expectedData;

		try {
			item_id = jsonRequestData.get("item_id").asInt();
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			actualResponse = addressresponse;

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetHelp" + smokeTest);

			// isSuccess =
			// JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
			// addressresponse);
			// Assert.assertTrue(isSuccess, "testGetHelp" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@Test(dependsOnMethods = "testHelpRecent")
	public void testverifyItemObject() throws Exception {

		boolean isSuccess1 = false;
		{
			System.out.println(actualResponse.get("data"));
			System.out.println(actualResponse.get("data").get("last_order"));
			System.out.println(actualResponse.get("data").get("last_order")
					.get("order_items"));

			JsonNode a1 = actualResponse.get("data").get("last_order")
					.get("order_items");

			System.out.println(a1);

			JsonNode b1 = expectedResponse.get("data").get("last_order")
					.get("IGNORE_order_items");
			System.out.println(b1);

			JsonNode actual = JsonTestUtils.getItemObjectUsingId(a1, item_id);
			//
			// JsonNode expected = JsonTestUtils.getItemObjectUsingId(b1,
			// item_id);

			isSuccess1 = JsonValidator.DETAILED_LEVEL.validateJson(b1, a1);

			Assert.assertTrue(isSuccess1, "testveifyItemObject");

		}
	}

	@DataProvider(name = "POST_HELP_SANITY")
	public static Object[][] testHelp1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("POST_HELP_SANITY");
	}

	@Test(priority = 1, dataProvider = "POST_HELP_SANITY")
	public void testHelp1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCreateCart" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCreateCart" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
