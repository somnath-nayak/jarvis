package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class PolygonAPIsSanityTest extends RestTestHelper {

	int polygonId = -1;

	@DataProvider(name = "CREATE_POLYGON_SANITY")
	public static Object[][] cd() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_POLYGON_SANITY");
	}

	@Test(priority = 0, dataProvider = "CREATE_POLYGON_SANITY")
	public void testCreatePolyogonDetail(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode res = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, jsonRequestData,
					STAFAgent.getSTAFValue("defaultAuthString"));
			polygonId = res.get("data").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess, "testCreatePolyogon");

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					res);
			Assert.assertTrue(isSuccess, "testCreatePolyogon");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "UPDATE_POLYGON_SANITY")
	public static Object[][] u() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UPDATE_POLYGON_SANITY");
	}

	@Test(priority = 1, dataProvider = "UPDATE_POLYGON_SANITY")
	public void testUpdatePolyogon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "id", polygonId);
			JsonNode resp = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, updatedRequest,
					STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					resp);
			Assert.assertTrue(isSuccess, "testUpdatePolyogon");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_POLYBYID_SANITY")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_POLYBYID_SANITY");
	}

	@Test(priority = 1, dataProvider = "GET_POLYBYID_SANITY")
	public void testGetPolyogonById(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "pathvariable", polygonId);

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
		System.out.println("#####" + response.asText());
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testGetPolyogonById");
	}

	@DataProvider(name = "GET_POLYBYLATLONG_SANITY")
	public static Object[][] ff() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_POLYBYLATLONG_SANITY");
	}

	@Test(priority = 5, dataProvider = "GET_POLYBYLATLONG_SANITY")
	public void testGetPolyogonByLatLong(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {

			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(
					serviceNameMap.get("sand"),apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "testGetPolyogonByLatLong");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_POLYBYTAG")
	public static Object[][] ff1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_POLYBYTAG");
	}

	@Test(priority = 5, dataProvider = "GET_POLYBYTAG")
	public void testGetPolyogonByTag(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		try {
			// JsonNode updatedRequest =
			// RestTestUtil.updateRequestDataBeforeSend(
			// jsonRequestData, "order_id", orderId);
			JsonNode getSingleOrderResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
					apiToExecute, jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					getSingleOrderResponse);
			Assert.assertTrue(status, "testGetPolyogonByTag");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
