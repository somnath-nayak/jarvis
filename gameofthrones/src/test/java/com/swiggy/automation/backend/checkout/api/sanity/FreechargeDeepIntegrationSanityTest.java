package com.swiggy.automation.backend.checkout.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.JsonValidationLevels;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sumit
 *
 */

public class FreechargeDeepIntegrationSanityTest extends RestTestHelper {
	int address_id1;
	Long order_id1;

	@DataProvider(name = "FREECHARGE_Link_wallet")
	public static Object[][] getWalletLinkedFC()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_Link_wallet");
	}

	@Test(priority = 1, dataProvider = "FREECHARGE_Link_wallet", groups = "Sanity")
	public void fcLinkWallet(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "fcLinkWallet");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "FREECHARGE_Resend_Otp")
	public static Object[][] getOtpAgainFC()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_Resend_Otp");
	}

	@Test(priority = 2, dataProvider = "FREECHARGE_Resend_Otp", groups = "Sanity")
	public void fcResendOtp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "fcResendOtp");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "FREECHARGE_Validate_Otp")
	public static Object[][] getOtpValidatedFC()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_Validate_Otp");
	}

	@Test(priority = 3, dataProvider = "FREECHARGE_Validate_Otp", groups = "Sanity")
	public void fcValidateOtp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "fcResendOtp");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "FREECHARGE_check_balance")
	public static Object[][] FREECHARGE_check_balance()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_check_balance");
	}

	@Test(priority = 1, dataProvider = "FREECHARGE_check_balance", groups = "Sanity")
	public void fcCheckBbalance(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData, defaulTID, defaultTOKEN);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "fcCheckBbalance");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}

	}

	@DataProvider(name = "FREECHARGE_CREATE_CART")
	public static Object[][] getCreateCartData2()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_CREATE_CART");
	}

	@Test(priority = 2, dataProvider = "FREECHARGE_CREATE_CART")
	public void testCreateCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
//		RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//				expectedData, JsonValidationLevels.HIGH_LEVEL);
	}

	@DataProvider(name = "FREECHARGE_ADD_NEW_ADDRESS")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_NEWADDRESS");
	}

	@Test(priority = 3, dataProvider = "FREECHARGE_ADD_NEW_ADDRESS")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		// (apiToExecute, defaulTID, defaultTOKEN)
		address_id1 = actualData.get("data").get("address_id").asInt();

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "testGetAllAddressAPI");

	}

	@DataProvider(name = "FREECHARGE_PLACE_ORDER")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_PLACE_ORDER");
	}

	@Test(priority = 4, dataProvider = "FREECHARGE_PLACE_ORDER")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "address_id", address_id1);
		JsonNode cplaceOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				updatedRequest, defaulTID, defaultTOKEN);
		order_id1 = cplaceOrderResponse.get("data").get("order_id").asLong();
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				cplaceOrderResponse);
		Assert.assertTrue(status, "testPlaceOrderAPI");
	}

	@DataProvider(name = "FREECHARGE_Cancel_Order")
	public static Object[][] getOrderCancel()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FREECHARGE_Cancel_Order");
	}

	@Test(priority = 5, dataProvider = "FREECHARGE_Cancel_Order")
	public void couponOrderCancelled(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode updatedRequest1 = RestTestUtil.updateRequestDataBeforeSend(
				jsonRequestData, "order_id", order_id1);
		JsonNode cOrderCancelled = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),
				apiToExecute, updatedRequest1,   STAFAgent.getSTAFValue("defaultAuthString"));
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				cOrderCancelled);
		Assert.assertTrue(status, "couponOrderCancelled");
	}

}
