/**
 * 
 */
package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * @author srishty.m
 *
 */
public class SimilarRestaurantsSanityTest extends RestTestHelper {

	@DataProvider(name = "SIMILAR_RESTAURANTS")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("SIMILAR_RESTAURANTS", "SANITY");
	}

	@Test(priority = 1, dataProvider = "SIMILAR_RESTAURANTS")
	public void similarRestaurants(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode actualData = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData);

		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				actualData);
		Assert.assertTrue(status, "SimilarRestaurants" + smokeTest);

		status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				actualData);

		Assert.assertTrue(status, "SimilarRestaurants" + sanityTest);

	}

}
