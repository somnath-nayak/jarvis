package com.swiggy.automation.backend.checkout.api.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sangeetha: Added high level testcases Ramzi : Added testcases for
 *         DetailedLevel check for all APIs
 *
 */
public class CartV2SanityTest extends RestTestHelper {

	public static final Logger log = Logger.getLogger(CartV2SanityTest.class);

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 0, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");

			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "CREATE_CART_V2_SANITY")
	public static Object[][] createCart() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CREATE_CART_V2_SANITY");
	}

	@Test(priority = 1, dataProvider = "CREATE_CART_V2_SANITY")
	public void testCreateCartV2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCreateCart" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCreateCart" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GETCART_V2_SANITY")
	public static Object[][] getCart() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETCART_V2_SANITY");
	}

	@Test(priority = 2, dataProvider = "GETCART_V2_SANITY")
	public void testGetCart(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCart" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCart" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "GETCART_V2_MINIMAL_SANITY")
	public static Object[][] getCartMinimal()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETCART_V2_MINIMAL_SANITY");
	}

	@Test(priority = 2, dataProvider = "GETCART_V2_MINIMAL_SANITY")
	public void testGetCartMinimal(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCartMinimal" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testGetCartMinimal" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "CART_V2_APPLY_COUPON_SANITY")
	public static Object[][] applyCoupon() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CART_V2_APPLY_COUPON_SANITY");
	}

	@Test(priority = 3, dataProvider = "CART_V2_APPLY_COUPON_SANITY")
	public void testCartApplyCoupon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartApplyCoupon" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartApplyCoupon" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "CART_V2_REMOVE_COUPON_SANITY")
	public static Object[][] removeCoupon() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CART_V2_REMOVE_COUPON_SANITY");
	}

	@Test(priority = 3, dataProvider = "CART_V2_REMOVE_COUPON_SANITY")
	public void testCartRemoveCoupon(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartApplyCoupon" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartApplyCoupon" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "CART_V2_CHECK_TOTAL_SANITY")
	public static Object[][] cartTotal() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CART_V2_CHECK_TOTAL_SANITY");
	}

	@Test(priority = 4, dataProvider = "CART_V2_CHECK_TOTAL_SANITY")
	public void testCartTotal(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartTotal" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testCartTotal" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "DELETE_CART_V2_SANITY")
	public static Object[][] deleteCart() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_CART_V2_SANITY");
	}

	@Test(priority = 5, dataProvider = "DELETE_CART_V2_SANITY", dependsOnMethods = "testCreateCartV2")
	public void testFlushCartV2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testFlushCartCart" + smokeTest);

			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testFlushCartCart" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

}
