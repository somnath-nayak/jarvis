package com.swiggy.automation.backend.snd.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class SearchRegressionTest extends RestTestHelper {

	static JsonNode actualResponse = null;
	static JsonNode expectedResponse = null;
	static int restaurant_id = -99;

	@DataProvider(name = "V2_SEARCH_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("V2_SEARCH_REGRESSION",
				"REGRESSION");
	}

	@Test(priority = 1, dataProvider = "V2_SEARCH_REGRESSION")
	public void testV2SearchWithPageParamAsItem(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		restaurant_id = expectedData.get("restId").asInt();
		expectedResponse = expectedData.get("expected");
		actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,
				jsonRequestData);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(
				expectedResponse, actualResponse);
		Assert.assertTrue(isSuccess, "testV2SearchWithPageParamAsItem"
				+ smokeTest);
		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedResponse,
				actualResponse);
		Assert.assertTrue(isSuccess, "testV2SearchWithPageParamAsItem"
				+ sanityTest);

	}

	@Test(dependsOnMethods = "testV2SearchWithPageParamAsItem")
	public void testIfExpectedRestaurantListedWithItemsSearched()
			throws SwiggyAPIAutomationException {
		System.out.println("aaaaa");
		JsonNode actual = JsonTestUtils.getRestaurantObjectUsingId(
				actualResponse.get("data").get("restaurants")
						.findValue("restaurants"), restaurant_id);

		JsonNode expected = JsonTestUtils.getRestaurantObjectUsingId(
				expectedResponse.get("data").get("IGNORE_restaurants")
						.findValue("restaurants"), restaurant_id);

		Assert.assertTrue(
				JsonValidator.DETAILED_LEVEL.validateJson(expected, actual),
				"testIfSearchItemPresent");

	}

}
