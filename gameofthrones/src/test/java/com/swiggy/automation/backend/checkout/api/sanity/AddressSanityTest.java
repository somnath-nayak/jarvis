package com.swiggy.automation.backend.checkout.api.sanity;

import com.swiggy.automation.common.utils.PasswordUtil;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Sangeetha
 *
 */
public class AddressSanityTest extends RestTestHelper {

	Long orderId = null;
	String orderKey = null;
	String addressId = null;

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					PasswordUtil.getLoginData(jsonRequestData));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

	@DataProvider(name = "ADD_NEW_ADDRESS_SANITY")
	public static Object[][] addMyAddress() throws SwiggyAPIAutomationException {

		return APIDataReader.getDataSetData("ADD_NEW_ADDRESS_SANITY");
	}

	@Test(priority = 2, dataProvider = "ADD_NEW_ADDRESS_SANITY")
	public void testAddNewAddress(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {

			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			addressId = response.get("data").get("address_id").toString();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAddNewAddress" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAddNewAddress" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "TEST_UPDATE_ADDRESS_SANITY")
	public static Object[][] updateAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("TEST_UPDATE_ADDRESS_SANITY");
	}

	@Test(priority = 3, dataProvider = "TEST_UPDATE_ADDRESS_SANITY", dependsOnMethods = "testAddNewAddress")
	public void testAdddressUpdateTest(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", addressId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedrequest);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAdddressUpdateTest" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAdddressUpdateTest" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "DELETE_ADDRESS_SANITY")
	public static Object[][] deleteAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("DELETE_ADDRESS_SANITY");
	}

	@Test(priority = 4, dataProvider = "DELETE_ADDRESS_SANITY", dependsOnMethods = "testAddNewAddress")
	public void testAdddressDelteTest(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {

			JsonNode updatedrequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", addressId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedrequest);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAdddressDeleteTest" + smokeTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testAdddressDeleteTest" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
	}

	@DataProvider(name = "GET_ALLADDRESS_SANITY")
	public static Object[][] f() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GET_ALLADDRESS_SANITY");
	}

	@Test(priority = 5, dataProvider = "GET_ALLADDRESS_SANITY")
	public void testAllAdddressTest(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testAllAdddressTest" + smokeTest);
			Assert.assertTrue(addressresponse.get("data").get("addresses")
					.size() > 0, "testAllAdddressTestGETALLAddressesList"
					+ sanityTest);
			isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testAllAdddressTest" + sanityTest);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
