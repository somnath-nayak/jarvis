/**
 * 
 */
package com.swiggy.automation.backend.revenuegrowth.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * @author Shilpy
 *
 */
public class CityReferralCouponsSanityTest extends RestTestHelper {

	@DataProvider(name = "Create_Current_Referral_Scheme")
	public static Object[][] createCurrentScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Create_Current_Referral_Scheme");
	}

	@Test(priority = 1, dataProvider = "Create_Current_Referral_Scheme")
	public void createCurrentReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "createCurrentReferral");

	}

	@DataProvider(name = "Create_Next_Referral_Scheme")
	public static Object[][] createNextScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Create_Next_Referral_Scheme");
	}

	@Test(priority = 2, dataProvider = "Create_Next_Referral_Scheme")
	public void createNextReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "createNextReferral");

	}

	@DataProvider(name = "Update_Current_Referral_Scheme")
	public static Object[][] updateCurrentScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Update_Current_Referral_Scheme");
	}

	@Test(priority = 3, dataProvider = "Update_Current_Referral_Scheme")
	public void updateCurrentReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "updateCurrentReferral");

	}

	@DataProvider(name = "Update_Next_Referral_Scheme")
	public static Object[][] updateNextScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Update_Next_Referral_Scheme");
	}

	@Test(priority = 4, dataProvider = "Update_Next_Referral_Scheme")
	public void updateNextReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "updateNextReferral");

	}

	@DataProvider(name = "View_Current_Referral_Scheme")
	public static Object[][] viewCurrentScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("View_Current_Referral_Scheme");
	}

	@Test(priority = 5, dataProvider = "View_Current_Referral_Scheme")
	public void viewCurrentReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "viewCurrentReferral");

	}

	@DataProvider(name = "View_Next_Referral_Scheme")
	public static Object[][] viewNextScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("View_Next_Referral_Scheme");
	}

	@Test(priority = 6, dataProvider = "View_Next_Referral_Scheme")
	public void viewNextReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "viewNextReferral");

	}

	@DataProvider(name = "View_All_Referral_Schemes")
	public static Object[][] viewAllScheme()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("View_All_Referral_Schemes");
	}

	@Test(priority = 7, dataProvider = "View_All_Referral_Schemes")
	public void viewAllReferral(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "viewAllReferral");

	}

	@DataProvider(name = "View_Referral_For_City")
	public static Object[][] viewSchemes() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("View_Referral_For_City");
	}

	@Test(priority = 8, dataProvider = "View_Referral_For_City")
	public void viewReferrals(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		JsonNode currentResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				currentResponse);
		Assert.assertTrue(status, "viewReferrals");

	}

}
