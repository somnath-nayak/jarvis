package com.swiggy.automation.backend.snd.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */
public class MenuRegressionTest extends RestTestHelper {

	@DataProvider(name = "MENU_REGRESSION")
	public static Object[][] getData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("MENU_REGRESSION", "REGRESSION");
	}

	@Test(priority = 1, dataProvider = "MENU_REGRESSION")
	public void testMenu(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
//		String apiHost= getEndPoint("listing_backend_host_qa");
//		Integer apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute, jsonRequestData,  STAFAgent.getSTAFValue("defaultAuthString"));
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);

		Assert.assertTrue(isSuccess, "testMenu" + smokeTest);

		isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testMenu" + sanityTest);
	}

}
