package com.swiggy.automation.backend.checkout.api.sanity;

import java.util.HashMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.CODServiceDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

import framework.gameofthrones.Tyrion.RedisHelper;

import com.swiggy.api.erp.ff.helper.*;

public class CODServiceTest extends CODServiceDP {

    CheckoutHelper helper= new CheckoutHelper();
    FraudServiceHelper FraudServiceHelper=new FraudServiceHelper();
    RedisHelper redisHelper = new RedisHelper();

    String tid;
    String token;
    String userid;
    HashMap<String, String> hashMap;
    
    @BeforeClass
    public void login(){
    	hashMap=helper.TokenData(CheckoutConstants.mobile,CheckoutConstants.password);
    	tid=hashMap.get("Tid");
        token=hashMap.get("Token");
        userid=hashMap.get("CustomerId");
    }
    
    @BeforeMethod
    public void reset(){
    	FraudServiceHelper.clearRedisKeys("1");
    	String resetSMdbQuery="update user_credits set swiggy_money='0',cancellation_fee='0'  where user_id="+userid+";";
   	    helper.dbUpdateOperation(CheckoutConstants.checkout, resetSMdbQuery);
   	    redisHelper.deleteKey("checkoutredis",0,"user_credit_"+userid);
    }
    
    
    @Test(dataProvider="codTest", groups = {"Smoke","chandan"}, description = "API sanity Test")
    public void codDisableCancellationFeeTest(Cart cartPayload,String cancellationFee,
    		                             String isExempted,String codEnabled){
    	updateUserCredit("0", cancellationFee);
    	if(isExempted.equals("1")){
    	FraudServiceHelper.AddToExceptionList(userid);
    	}
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload))
    			                  .ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
    	
        String getAllPaymentRes=helper.getAllPayments(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllPaymentRes);
        String[] codDetails=getCODDetails(getAllPaymentRes);
        Assert.assertEquals(codDetails[0], codEnabled);
        
        if(codEnabled.equals("true")){
        	cancelOrderWithCancellationFee(cartPayload,"0");
        	}
        
    	}

    
    @Test(dataProvider="codSMTest", groups = {"Smoke","chandan"}, description = "API sanity Test")
    public void codCancellationFeeWithSMTest(Cart cartPayload,String cancellationFee,String amount,
    		                                  String isExempted,String codEnabled){
    	if(isExempted.equals("1")){
        	FraudServiceHelper.AddToExceptionList(userid);
        }
    	Double sm= Double.parseDouble(cancellationFee)+Double.parseDouble(amount);
      	updateUserCredit(sm.toString(), cancellationFee);
    	
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
    	
        String getAllPaymentRes=helper.getAllPayments(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllPaymentRes);
        String[] codDetails=getCODDetails(getAllPaymentRes);
        Assert.assertEquals(codDetails[0], codEnabled);
        
        if(codEnabled.equals("true")){
        	cancelOrderWithCancellationFee(cartPayload,"0");
        }
    }
    
    
    @Test(dataProvider="codNightFeeTest", groups = {"Smoke","chandan"}, description = "API sanity Test")
    public void codNightFeeTest(Cart payload,String cityId,String startTime,
    		String endTime,String fee,String isExcempted,String codEnabled){

        FraudServiceHelper.AddCityConfigurations(cityId, startTime, endTime, fee);
        
        if(isExcempted.equals("1")){
        	FraudServiceHelper.AddToExceptionList(userid);	
        }
        
    	Cart cartPayload=payload;
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload))
    			                  .ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
    	          
        String getAllPaymentRes=helper.getAllPayments(tid,token).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllPaymentRes);
        String[] codDetails=getCODDetails(getAllPaymentRes);
        Assert.assertEquals(codDetails[0], codEnabled);
        
        if(codEnabled.equals("true")){
        	cancelOrderWithCancellationFee(cartPayload,"0");
        }
    }
    
    public String getCartTotal(String cartResponse){
    	return JsonPath.read(cartResponse, "$.data.cart_total").toString().replace("[","").replace("]","");
    }
    
    public String getOrderId(String placeOrderRes){
    	return JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
    }
    
    public void validatePlaceOrderResponse(String placeOrderRes,String cartTotal){
    	String orderTotal=JsonPath.read(placeOrderRes, "$.data..order_total").toString().replace("[","").replace("]","");
    	Assert.assertEquals(Double.parseDouble(orderTotal),Double.parseDouble(cartTotal), "Mismatch in order_total & cart_total");
    }
    
    
    public void cancelOrder(String orderId){
    	String cancelOrderRes=helper.OrderCancel(tid, token, CheckoutConstants.authorization, superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, orderId)
                .ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cancelOrderRes);
    	
    }
    
    public String[] getCODDetails(String paymentRes){
    	String isCodEnabled="";
    	String diableMessage="";
    String[] groupName=JsonPath.read(paymentRes, "$.data..payment_group[*]..group_name")
    		                    .toString().replace("[","").replace("]","").replace("\"","")
    		                    .split(",");
    	for (int i=0;i<groupName.length;i++){
    		System.out.println(groupName[i]);
    		if(groupName[i].equals("cod")){
    			isCodEnabled=JsonPath.read(paymentRes, "$.data..payment_group.["+i+"]..payment_methods..enabled")
    					.toString().replace("[","").replace("]","");
    			diableMessage=JsonPath.read(paymentRes, "$.data..payment_group.["+i+"]..payment_methods..meta.disable_message")
    					.toString().replace("[","").replace("]","");
    		}
    	}
    	return new String[]{isCodEnabled,diableMessage};
    }
    
    public String cancelOrderWithCancellationFee(Cart cartPayload,String cancellationFee){
    	String orderID=placeOrder(cartPayload);
    	cancelOrder(orderID,cancellationFee);
    	return orderID;
   }
    
    public String placeOrder(Cart cartPayload){
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
   	    String addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
        String placeOrderRes=helper.orderPlaceV2(tid, token, addressId,superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
		                            .ResponseValidator.GetBodyAsText();
       helper.validateApiResStatusData(placeOrderRes);
       String orderID=getOrderId(placeOrderRes);
       return orderID;
   }
    
    public void cancelOrder(String orderID,String cancellationFee){
    	  String cancelOrderRes=helper.OrderCancel(tid, token, CheckoutConstants.authorization, superConstant.DEF_STRING,
               superConstant.EXPECTED_TRUE_FLAG, cancellationFee, superConstant.DEF_STRING, orderID)
               .ResponseValidator.GetBodyAsText();
   	   helper.validateApiResStatusData(cancelOrderRes);
   }
    
    
    
    public void revertCancellationFee(String orderID,String cancellationFee){
    	String[] payload1=helper.revertCancellationFeePayload(orderID, "Test", cancellationFee, "Test");
        String revertCancelRes=helper.revertCancellationFee(tid,token,payload1).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(revertCancelRes);
    	
    }
   
    public void updateUserCredit(String SM,String cancellationFee){
    	String resetSMdbQuery="update user_credits set swiggy_money='"+SM+"',cancellation_fee='"+cancellationFee+"'  where user_id="+userid+";";
   	    helper.dbUpdateOperation(CheckoutConstants.checkout, resetSMdbQuery);
   	    redisHelper.deleteKey("checkoutredis",0,"user_credit_"+userid);
    	
    }
   
 }