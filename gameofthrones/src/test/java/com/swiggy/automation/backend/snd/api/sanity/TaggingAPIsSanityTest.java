package com.swiggy.automation.backend.snd.api.sanity;

import java.util.ArrayList;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shweta
 */

public class TaggingAPIsSanityTest extends RestTestHelper {

	int tagId = -1;
	int catId = -1;

	@DataProvider(name = "Tag_Cat_Creation")
	public static Object[][] getCreateCartData11()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Create_tag_cat");
	}

	@Test(priority = 1, dataProvider = "Tag_Cat_Creation", groups = "Sanity")
	public void testCreateTagCat(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "name",
					"cat_" + System.currentTimeMillis());
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					updatedRequest, STAFAgent.getSTAFValue("defaultAuthString"));
			catId = response.get("data").get("id").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testCreateTagCat");
			System.out
					.println("**********************###############################*********************");
			System.out.println(catId);

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "Tag_Creation")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("Create_Item_tag");
	}

	@Test(priority = 2, dataProvider = "Tag_Creation", groups = "Sanity")
	public void testCreateTag(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "name",
					"tag_" + System.currentTimeMillis());
			// , "tagCategory", catId);

			JsonNode updatedRequest1 = RestTestUtil
					.updateRequestDataBeforeSend(updatedRequest, "tagCategory",
							catId);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					updatedRequest1, STAFAgent.getSTAFValue("defaultAuthString"));
			tagId = response.get("data").get("id").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testCreateTag");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail();
		}
	}

	@DataProvider(name = "Tag_View")
	public static Object[][] getCreateCartData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("View_tag");
	}

	@Test(priority = 3, dataProvider = "Tag_View", groups = "Sanity")
	public void testViewTag(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			ArrayList<Integer> tagIdArray = new ArrayList<>();

			tagIdArray.add(tagId);
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "tagIds", tagIdArray);
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					updatedRequest, STAFAgent.getSTAFValue("defaultAuthString")); // tagId =
			response.get("data").get("id").asInt();
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testViewTag");

		} catch (Exception e) {
			Assert.fail();
		}
	}

}
