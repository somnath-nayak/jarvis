package com.swiggy.automation.backend.snd.api.regression;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 16/06/17.
 */
public class MenuV3RegressionTest extends RestTestHelper {

    @DataProvider(name = "MENU_V3_REGRESSION")
    public static Object[][] getData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("MENU_V3_REGRESSION","REGRESSION");
    }

    @Test(priority = 1, dataProvider = "MENU_V3_REGRESSION")
    public void testMenu(String dataGroup, String apiToExecute,
                         String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

//        String apiHost= getEndPoint("listing_backend_host_qa");
//        Integer apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));
        boolean httpsEnabled=false;

        JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute, jsonRequestData);

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,response);
        Assert.assertTrue(isSuccess, "testMenu V3 API" + smokeTest);

        isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
        if(expectedData==null && response==null){
            Assert.assertFalse(isSuccess,"Test Menu V3 API Assertion False"+regressionTest);
        }else {
            Assert.assertTrue(isSuccess, "testMenu V3 API" + regressionTest);
        }
    }
}
