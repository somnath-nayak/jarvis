package com.swiggy.automation.backend.snd.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author Shwetha
 *
 */
public class UberPopUpAPIsSanityTest extends RestTestHelper {

	// public static final Logger log = Logger.getLogger(LoginToSession.class);

	@DataProvider(name = "UBERCREATEDP")
	public static Object[][] ff() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UBER_CREATE");
	}

	@Test(priority = 1, dataProvider = "UBERCREATEDP", groups = "Sanity")
	public void testCreateUberPopUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testCreateUberPopUp");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "UBERUPDATEDP")
	public static Object[][] getCreateCartData13()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UBER_UPDATE");
	}

	@Test(priority = 2, dataProvider = "UBERUPDATEDP", groups = "Sanity")
	public void testUpdateUberPopUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testUpdateUberPopUp");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "UBERGETDP")
	public static Object[][] f11() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UBER_GET");
	}

	@Test(priority = 3, dataProvider = "UBERGETDP", groups = "Sanity")
	public void testGetUberPopUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testGetUberPopUp");
		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "UBERGETALLDP")
	public static Object[][] f1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("UBER_GETALL");
	}

	@Test(priority = 4, dataProvider = "UBERGETALLDP", groups = "Sanity")
	public void testGetAllUberPopUp(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute,
					jsonRequestData, STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "testGetAllUberPopUp");
		} catch (Exception e) {
			Assert.fail();
		}
	}

}