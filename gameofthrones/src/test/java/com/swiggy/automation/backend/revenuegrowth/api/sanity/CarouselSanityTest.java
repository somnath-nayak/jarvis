package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DateTimeUtils;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static com.swiggy.automation.backend.revenuegrowth.api.sanity.ListingV4SanityTest.restaurant_id;

/**
 * Created by vishal.mahajan on 19/07/17.
 */
public class CarouselSanityTest extends RestTestHelper{
    static JsonNode carousleResponse = null;

    @DataProvider(name = "CAROUSEL_SANITY")
    public static Object[][] getCreateTDData()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("CAROUSEL_SANITY");
    }
    @Test(priority = 1, dataProvider = "CAROUSEL_SANITY")
    public void testCarouselSanity(String dataGroup, String apiToExecute,
                             String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        if(dataSetName.contains("CREATE_CAROUSEL_WITH_BANNER"))
        {

            Map<String, Object> createRequestUpdateData = new HashMap<String, Object>();

            createRequestUpdateData.put("startDateTime", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));
            createRequestUpdateData.put("endDateTime", DateTimeUtils.getFutureDateByMinutesFromToday(3,
                    DateTimeUtils.FORMAT.STANDARDDATE_FORMAT));

            jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);


            carousleResponse = RestTestUtil.sendData(
            		serviceNameMap.get("sand"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, carousleResponse);
            Assert.assertTrue(isSuccess, "CREATE_CAROUSEL" + smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, carousleResponse);
            Assert.assertTrue(isSuccess, "CREATE_CAROUSEL" + sanityTest);

        }else if (dataSetName.contains("GET_ALL_CAROUSEL_BANNER")){

            int carousleId =carousleResponse.get("data").asInt();

            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("sand"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            JsonNode actualCarousel = JsonTestUtils.getRestaurantObjectUsingId(response.get("data"),carousleId);
            JsonNode expectedCarousel = JsonTestUtils.getFirstObjectFromArray(expectedData.get("data"));

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "GET_CAROUSEL" + smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedCarousel, actualCarousel);
            Assert.assertTrue(isSuccess, "GET_CAROUSEL" + sanityTest);


        }else {
            JsonNode response = RestTestUtil.sendData(
            		serviceNameMap.get("sand"),
                    apiToExecute, jsonRequestData, couponsDataServiceAuthString);

            boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "CAROUSEL_SMOKE"+smokeTest);

            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, response);
            Assert.assertTrue(isSuccess, "CAROUSEL_SANITY"+sanityTest);


        }
    }
 }
