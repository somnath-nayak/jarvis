package com.swiggy.automation.backend.checkout.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.JsonValidationLevels;
import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * 
 * @author mohammedramzi
 *
 */

public class PayTMDeepIntegrationAPIsSanityTest extends RestTestHelper {

	Long serviceableAddressId = null;

	@DataProvider(name = "PAYTMSSO_LINK_WALLET_SANITY")
	public static Object[][] f() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_LINK_WALLET_SANITY");
	}

	@Test(priority = 1, dataProvider = "PAYTMSSO_LINK_WALLET_SANITY", groups = "Sanity")
	public void testPayTMLinkWallet(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMLinkWallet");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_VALIDATE_OTP_SANITY")
	public static Object[][] getV3Data() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_VALIDATE_OTP_SANITY");
	}

	@Test(priority = 2, dataProvider = "PAYTMSSO_VALIDATE_OTP_SANITY", groups = "Sanity")
	public void testVerifyPayTMOTP(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode response = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("checkout"),apiToExecute,
				jsonRequestData);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				response);
		Assert.assertTrue(isSuccess, "testVerifyPayTMOTP");
	}

	@DataProvider(name = "PAYTMSOO_GET_SSO_TOKEN_SANITY")
	public static Object[][] ssotoken() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSOO_GET_SSO_TOKEN_SANITY");
	}

	@Test(priority = 3, dataProvider = "PAYTMSOO_GET_SSO_TOKEN_SANITY", groups = "Sanity")
	public void testGetPayTMSSOToken(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
		boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
				addressresponse);
		Assert.assertTrue(isSuccess, "testGetPayTMSSOToken");
	}

	@DataProvider(name = "PAYTMSSO_CHECK_BALANCE_SANITY")
	public static Object[][] f2() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_CHECK_BALANCE_SANITY");
	}

	@Test(priority = 4, dataProvider = "PAYTMSSO_CHECK_BALANCE_SANITY", groups = "Sanity")
	public void testPayTMCheckBalance(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMCheckBalance");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_CREATECART_SANITY")
	public static Object[][] getCreateCartDataCriticalFlow()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_CREATECART_SANITY");
	}

	@Test(priority = 5, dataProvider = "PAYTMSSO_CREATECART_SANITY", groups = "Sanity")
	public void testCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

//		RestExecutor.executeNValidateAPI(apiToExecute, jsonRequestData,
//				expectedData, JsonValidationLevels.HIGH_LEVEL);

	}

	@DataProvider(name = "PAYTMSSO_GETALLADDRESS_SANITY")
	public static Object[][] f22() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_GETALLADDRESS_SANITY");
	}

	@Test(priority = 6, dataProvider = "PAYTMSSO_GETALLADDRESS_SANITY", groups = "Sanity")
	public void adddressTest(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(addressresponse);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "AAAddressTest");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_PLACEORDER_SANITY")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_PLACEORDER_SANITY");
	}

	@Test(priority = 7, dataProvider = "PAYTMSSO_PLACEORDER_SANITY", groups = "Sanity")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		try {

			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			System.out.println(jsonRequestData);
			System.out.println(updatedRequest);
			JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedRequest, defaulTID, defaultTOKEN);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(
					expectedData, placeOrderResponse);
			Assert.assertTrue(status, "testPlaceOrderAPI");
			orderId = placeOrderResponse.get("data").get("order_id").asLong();
			orderIds.add(orderId);
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_BLOCK_AMOUNT_SANITY")
	public static Object[][] f3() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_BLOCK_AMOUNT_SANITY");
	}

	@Test(priority = 8, dataProvider = "PAYTMSSO_BLOCK_AMOUNT_SANITY", groups = "Sanity")
	public void testPayTMBlockAmount(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMBlockAmount");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_WITHDRAW_AMOUNT_SANITY")
	public static Object[][] f4() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_WITHDRAW_AMOUNT_SANITY");
	}

	@Test(priority = 9, dataProvider = "PAYTMSSO_WITHDRAW_AMOUNT_SANITY", groups = "Sanity")
	public void testPayTMWithdrawBlockAmount(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMWithdrawBlockAmount");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_REFUND_AMOUNT_SANITY")
	public static Object[][] f5() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_REFUND_AMOUNT_SANITY");
	}

	@Test(priority = 10, dataProvider = "PAYTMSSO_REFUND_AMOUNT_SANITY", groups = "Sanity")
	public void testPayTMRefundoney(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMRefundoney");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

	@DataProvider(name = "PAYTMSSO_DELINK_WALLET_SANITY")
	public static Object[][] f1() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("PAYTMSSO_DELINK_WALLET_SANITY");
	}

	@Test(priority = 11, dataProvider = "PAYTMSSO_DELINK_WALLET_SANITY", groups = "Sanity")
	public void testPayTMDELinkWallet(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					addressresponse);
			Assert.assertTrue(isSuccess, "testPayTMDELinkWallet");

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}

	}

}
