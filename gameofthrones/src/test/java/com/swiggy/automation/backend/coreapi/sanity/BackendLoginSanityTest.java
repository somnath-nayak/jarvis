package com.swiggy.automation.backend.coreapi.sanity;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

// TODO: Auto-generated Javadoc

/**
 * 
 * @author mohammedramzi
 *
 */
public class BackendLoginSanityTest extends RestTestHelper {

	/** The Constant log. */
	public static final Logger log = Logger
			.getLogger(BackendLoginSanityTest.class);

	@DataProvider(name = "BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}

	}

}
