package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.PasswordUtil;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 19/06/17.
 */
public class ListingV4SanityTest extends RestTestHelper {

    static JsonNode actualResponse = null;
    static JsonNode expectedResponse = null;
    static int restaurant_id = -99;
    static Integer apiPort=0;
    static String apiHost="";

    @DataProvider(name = "BACKEND_SANITY_LOGIN")
    public static Object[][] getLoginData1()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("BACKEND_SANITY_LOGIN");
    }

    @Test(priority = 0, dataProvider = "BACKEND_SANITY_LOGIN")
    public void testV2Login(String dataGroup, String apiToExecute,
                            String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {
        boolean isSuccess = false;
        try {
//            apiHost= getEndPoint("listing_backend_host_qa");
//            apiPort= Integer.parseInt(getEndPoint("listing_backend_port_qa"));

            JsonNode response = RestTestUtil.sendData(serviceNameMap.get("sand"),apiToExecute, PasswordUtil.getLoginData(jsonRequestData),  STAFAgent.getSTAFValue("defaultAuthString"));
            isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
                    response);
            defaultTOKEN = response.get("data").get("token").asText().toString();
            defaulTID = response.get("tid").asText().toString();

            Assert.assertTrue(isSuccess, "testV2Login" + smokeTest);
            isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
                    response);
            Assert.assertTrue(isSuccess, "testV2Login" + sanityTest);

        } catch (Exception e) {
            log.error(new SwiggyAPIAutomationException(e.toString()));
            Assert.fail();
        }

    }

    @DataProvider(name = "V4_LISTING_SANITY")
    public static Object[][] getData() throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("V4_LISTING_SANITY");
    }

    @Test(priority = 1, dataProvider = "V4_LISTING_SANITY")
    public void testV4RestaurantListing(String dataGroup, String apiToExecute,
                                        String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        restaurant_id = Integer.parseInt(  STAFAgent.getSTAFValue("defaultTestRestaurant"));
        expectedResponse = expectedData;

        actualResponse = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),apiToExecute,jsonRequestData,defaulTID,defaultTOKEN);

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedResponse, actualResponse);
        Assert.assertTrue(isSuccess, "testV4RestaurantListing" + smokeTest);

    }

    @Test(priority = 2, dependsOnMethods = "testV4RestaurantListing")
    public void testverifyRestaurantObject()
            throws SwiggyAPIAutomationException {

        JsonNode actualVerifiedNode=JsonTestUtils.verifyRestaurantObjectMessageCount(actualResponse.get("data").get("cards"),"count");

        JsonNode actual= JsonTestUtils.getRestaurantObjectUsingIdForListing(actualVerifiedNode,restaurant_id);
        JsonNode expected = JsonTestUtils.getRestaurantObjectUsingIdForListing(expectedResponse.get("data").get("cards"),restaurant_id);

        boolean isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expected, actual);
        Assert.assertTrue(isSuccess, "testverifyRestaurantObject"+sanityTest);

    }

}
