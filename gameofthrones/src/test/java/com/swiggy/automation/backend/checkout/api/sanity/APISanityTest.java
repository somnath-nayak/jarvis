package com.swiggy.automation.backend.checkout.api.sanity;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.dp.APISanityDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.HelpHelper;
import com.swiggy.api.sf.checkout.helper.SuperHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;

import framework.gameofthrones.Aegon.Initialize;

public class APISanityTest extends APISanityDP {

    Initialize gameofthrones = new Initialize();
    CheckoutHelper helper= new CheckoutHelper();
    HelpHelper helpHelper=new HelpHelper();
    
    String mobile= System.getenv("mobile");
	String password= System.getenv("password");
	
    String tid;
    String token;
    String cartTotal;
    String addressId;
    String orderID;
    String cloneOrderID;
    String orderKey;
    
    @BeforeTest
    public void login(){
    	if (mobile == null || mobile.isEmpty()){
    		mobile=CheckoutConstants.mobile;}
    	
    	if (password == null ||password.isEmpty()){
    		password=CheckoutConstants.password;}
    	
    	HashMap<String, String> hashMap=helper.TokenData(mobile,password);
        tid=hashMap.get("Tid");
        token=hashMap.get("Token");
    }
            
    
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=1, description = "create regular Cart Test")
    public void createCartTest(Cart cartPayload){
    	String cartResponse=helper.createCart(tid, token,Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(cartResponse);
        cartTotal=getCartTotal(cartResponse);
        addressId=helper.getAddressIDfromCartResponse(tid, token, cartResponse);
    }
    
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=2, description = "get Cart Test")
    public void getCartTest(Cart cartPayload){
    	String getCartRes=helper.GetCart(tid, token).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(getCartRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=3, description = "get all Payment Test")
    public void getAllPaymentTest(Cart cartPayload){
        String getCartRes=helper.getAllPayments(tid, token).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getCartRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=4, description = "place Order Test")
    public void placeOrderTest(Cart cartPayload){
           String placeOrderRes=helper.orderPlaceV2(tid, token, addressId,superConstant.PAYMENT_METHOD_CASH,superConstant.DEF_STRING)
    		                           .ResponseValidator.GetBodyAsText();
            helper.validateApiResStatusData(placeOrderRes);
            orderID=getOrderId(placeOrderRes);
            orderKey=helper.getOrderKey(placeOrderRes);
            //validatePlaceOrderResponse(placeOrderRes,cartTotal);
     }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=5, description = "get Order details Test")
    public void getOrderDetailsTest(Cart cartPayload){
        String getOrderRes=helper.getOrderDetails(tid, token,orderID).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getOrderRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=6, description = "get Single Order Test")
    public void getSingleOrderTest(Cart cartPayload){
        String getSingleOrderRes=helper.getSingleOrder(orderID, tid, token).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getSingleOrderRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=7, description = "get Last Order Test")
    public void getLastOrder(Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token,"","");
        String getLastOrderRes=helper.getLastOrder(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getLastOrderRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=8, description = "get Minimal Order Details Test")
    public void getMinimalOrderDetails(Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, "", "");
        String getMinimalOrderRes=helper.getMinimalOrderDetails(header,orderKey).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getMinimalOrderRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=9, description = "get Order Test")
    public void getAllOrderTest(Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token,"","");
        String getAllOrderRes=helper.getAllOrders(header).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(getAllOrderRes);
    }


    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=10, description = "track Order Test")
    public void trackOrderTest(Cart cartPayload){
    	String trackRes=helpHelper.track1(tid, token, orderID).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(trackRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=11, description = "track minimal Order Test")
    public void trackMinimalOrder(Cart cartPayload){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, "", "");
        String trackMinOrderRes=helper.trackMinimalOrder(header,orderKey).ResponseValidator.GetBodyAsText();
        helper.validateApiResStatusData(trackMinOrderRes);
    }

    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=12, description = "edit Order Check Test")
    public void editOrderCheckTest(Cart cartPayload){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
        String[] editOrderPayload=new String[]{orderID, CheckoutConstants.INITIATION_SOURCE, rawEditOrderPayload};
        String editOrderRes=helper.editOrderCheck(tid,token,editOrderPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(editOrderRes);
    }
    
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=13, description = "edit Order Confirm Test")
    public void editOrderConfirmTest(Cart cartPayload){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
    	String[] confirmEditOrderPayload=new String[]{orderID, CheckoutConstants.orderComments, rawEditOrderPayload};
    	String editConfirmOrderRes=helper.editOrderConfirm(tid, token,confirmEditOrderPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(editConfirmOrderRes);
    }
    
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=14, description = "cancel Order Test")
    public void cancelOrderTest(Cart cartPayload){
    	cancelOrder(orderID);
    }
    
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=15, description = "clone Order Test")
    public void cloneOrderTest(Cart cartPayload){
    	String rawEditOrderPayload=helper.getEditOrderPayload(cartPayload);
    	String[] cloneCheckResPayload=new String[]{orderID,CheckoutConstants.INITIATION_SOURCE,rawEditOrderPayload};
    	String[] cloneOrderConfirmPayload=new String[]{orderID,CheckoutConstants.INITIATION_SOURCE,rawEditOrderPayload};
    	String cloneCheckRes=helper.cloneOrderCheck(tid,token,cloneCheckResPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cloneCheckRes);
    	cartTotal=getCartTotal(cloneCheckRes);
    	String cloneOrderConfirmRes=helper.cloneOrderConfirm(tid,token,cloneOrderConfirmPayload).ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cloneOrderConfirmRes);
        validatePlaceOrderResponse(cloneOrderConfirmRes,cartTotal);
        cloneOrderID=getOrderId(cloneOrderConfirmRes);
    }
       
    @Test(dataProvider="sanity1", groups = {"Sanity","chandan"},priority=16, description = "cancel Cloned Order Test")
    public void cancelClonedOrderTest(Cart cartPayload){
    	cancelOrder(cloneOrderID);
    }
    
    public String getCartTotal(String cartResponse){
    	return JsonPath.read(cartResponse, "$.data.cart_total").toString().replace("[","").replace("]","");
    }
    
    public String getOrderId(String placeOrderRes){
    	return JsonPath.read(placeOrderRes, "$.data..order_id").toString().replace("[","").replace("]","");
    }
    
    public void validatePlaceOrderResponse(String placeOrderRes,String cartTotal){
    	String orderTotal=JsonPath.read(placeOrderRes, "$.data..order_total").toString().replace("[","").replace("]","");
    	Assert.assertEquals(Double.parseDouble(orderTotal),Double.parseDouble(cartTotal), "Mismatch in order_total & cart_total");
    }
    
    
    public void cancelOrder(String orderId){
    	String cancelOrderRes=helper.OrderCancel(tid, token, CheckoutConstants.authorization, superConstant.DEF_STRING,
                superConstant.EXPECTED_TRUE_FLAG, superConstant.ZERO_AS_STRING, superConstant.DEF_STRING, orderId)
                .ResponseValidator.GetBodyAsText();
    	helper.validateApiResStatusData(cancelOrderRes);
    	
    }


 }