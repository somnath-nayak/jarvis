package com.swiggy.automation.backend.revenuegrowth.api.sanity;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.common.utils.DateTimeUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

/**
 * Created by vishal.mahajan on 18/07/17.
 */
public class CouponsSanityTest extends RestTestHelper{

    @DataProvider(name = "COUPONS_SANITY")
    public static Object[][] getCreateTDData()
            throws SwiggyAPIAutomationException {
        return APIDataReader.getDataSetData("COUPONS_SANITY");
    }
    @Test(priority = 1, dataProvider = "COUPONS_SANITY")
    public void testCouponsSanity(String dataGroup, String apiToExecute,
                                   String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
            throws Exception {

        Map<String, Object> createRequestUpdateData = new HashMap<String, Object>();

        createRequestUpdateData.put("created_on", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.LOCALDATETIME_FOPRMAT));
        createRequestUpdateData.put("valid_from", DateTimeUtils.getCurrentDateTime(DateTimeUtils.FORMAT.LOCALDATETIME_FOPRMAT));
        createRequestUpdateData.put("valid_till", DateTimeUtils.getFutureDateByMinutesFromToday(5,
                DateTimeUtils.FORMAT.LOCALDATETIME_FOPRMAT));

        jsonRequestData = RestTestUtil.updateRequestDataBeforeSend(jsonRequestData, createRequestUpdateData);

        JsonNode couponsResponse = RestTestUtil.sendData(
        		serviceNameMap.get("td"),
                apiToExecute, jsonRequestData,   STAFAgent.getSTAFValue("defaultAuthString"));

        boolean isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData, couponsResponse);
        Assert.assertTrue(isSuccess, "CREATE_COUPONS" + smokeTest);

        isSuccess = JsonValidator.DETAILED_LEVEL.validateJson(expectedData, couponsResponse);
        Assert.assertTrue(isSuccess, "CREATE_COUPONS" + sanityTest);

    }
}
