package com.swiggy.automation.common.utils;

import com.swiggy.automation.utils.SwiggyAPIAutomationException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOHelper {

	public static DBConnectionPool getConnectionPool(DBTYpe type,
			String hostName, String port, String dbName, String dbUser,
			String dbPassword, int intialConnections, int maxConnections,
			boolean waitIfBusy) throws SQLException {
		String driverName = null;
		String jdbcUrl = null;

		switch (type) {
		case MYSQL:
			driverName = "com.mysql.jdbc.Driver";
			jdbcUrl = "jdbc:mysql://" + hostName + ":" + port + "/" + dbName;
			System.out.println("MYSQL DB URL:" + jdbcUrl);
			break;
		case POSTGRES:
			driverName = "org.postgresql.Driver";
			jdbcUrl = "jdbc:postgresql://" + hostName + ":" + port + "/"
					+ dbName;
			System.out.println("POSTGRES DB URL:" + jdbcUrl);
			break;
		case MSSQL:
			driverName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
			jdbcUrl = "jdbc:sqlserver://" + hostName + ":" + "1433"
					+ ";databaseName=" + dbName;
			System.out.println("MSSQL DB URL:" + jdbcUrl);
			break;
		default:
			break;

		}
		DBConnectionPool connectionPool = new DBConnectionPool(driverName,
				jdbcUrl, dbUser, dbPassword, intialConnections, maxConnections,
				waitIfBusy);
		System.out.println("Connection Pooling Success");

		return connectionPool;

	}

	public static Long getRowCount(Connection con, String query) {
		try {
			ResultSet rs = executeQuery(con, query);
			if (null == rs) {
				throw new SwiggyAPIAutomationException("Result Set NULL");
			}
			rs.next();
			Long count = rs.getLong(1);
			System.out.println(count);
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static Long getMaxValue(Connection con, String query) {
		try {
			ResultSet rs = executeQuery(con, query);
			if (null == rs) {
				throw new SwiggyAPIAutomationException("Result Set NULL");
			}
			rs.next();
			Long order_d = rs.getLong(1);
			System.out.println(order_d);
			return order_d;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private static ResultSet executeQuery(Connection con, String query)
			throws SQLException {
		PreparedStatement prepStmt;
		prepStmt = con.prepareStatement(query);
		ResultSet rs = prepStmt.executeQuery();
		return rs;
	}

}
