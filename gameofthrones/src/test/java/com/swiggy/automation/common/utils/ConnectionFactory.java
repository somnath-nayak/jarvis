package com.swiggy.automation.common.utils;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {

	private static ConnectionFactory instance = null;
	private static Connection connection = null;
	public static final Logger log = Logger.getLogger(ConnectionFactory.class);

	private ConnectionFactory() {
	}

	private Connection createConnection(String dbDriverType, String host,
			String dbName, String userName, String password) {
		String url = null;
		String jdbcdDriver = null;
		try {
			switch (dbDriverType) {
			case IDBDriverType.MYSQL:
				jdbcdDriver = IDBDriverType.MYSQL_DRIVER_CLASS;
				url = "jdbc:mysql://" + host + ":3306/" + dbName;
				break;

			case IDBDriverType.POSTGRES:
				jdbcdDriver = IDBDriverType.POSTGRES_DRIVER_CLASS;
				url = "jdbc:postgresql://" + host + ":5432/" + dbName;
				break;

			case IDBDriverType.MSSQL:
				jdbcdDriver = IDBDriverType.MSSQLSERVER_DRIVER_CLASS;
				url = "jdbc:sqlserver://" + host + ":" + "1433"
						+ ";databaseName=" + dbName;
				break;
			}

			Class.forName(jdbcdDriver);

			connection = DriverManager.getConnection(url, userName, password);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}

	static Connection getConnection(String dbDriverType, String host,
			String dbName, String userName, String password) {
		if (null == connection) {
			instance = new ConnectionFactory();
			return instance.createConnection(dbDriverType, host, dbName,
					userName, password);
		} else {
			return connection;
		}
	}

	static void destroyConnection() throws SQLException {
		connection.close();
	}

}