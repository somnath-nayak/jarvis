package com.swiggy.automation.common.utils;

import com.swiggy.automation.utils.SwiggyAPIAutomationException;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

public class APIUtils {
	private static Logger log = Logger.getLogger(APIUtils.class);

	public static JsonNode convertStringtoJSON(String response)
			throws IOException,
			SwiggyAPIAutomationException {
		JsonNode obj1 = null;
		try {
			JsonFactory factory = new JsonFactory();
			JsonParser jp1 = factory.createJsonParser(response);
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
			mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS,
					true);
			obj1 = mapper.readTree(jp1);
		} catch (Exception e) {
			log.error(new SwiggyAPIAutomationException(
					"Error While Parsing Json ...Please check through JsonLint... Exception :"
							+ e));
			throw new SwiggyAPIAutomationException(
					"Error While Parsing Json...Please check through JsonLint... Exception :");
		}

		return obj1;
	}
}