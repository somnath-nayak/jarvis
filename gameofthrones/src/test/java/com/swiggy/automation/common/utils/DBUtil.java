package com.swiggy.automation.common.utils;

import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBUtil  {

	public static final Logger log = Logger.getLogger(DBUtil.class);

	

	public static Connection getConnection(String dbDriverType, String host,
			String dbName, String userName, String password) {
		if (null == host || null == dbName || null == userName
				|| null == password) {
			log.error("unable to get DB Connection... one of input param is null...So returns null as conn object");
			return null;
		}
		if (host.isEmpty() || dbName.isEmpty() || userName.isEmpty()
				|| password.isEmpty()) {
			log.error("unable to get DB Connection... one of input param is empty...So returns null as conn object");
			return null;
		}

		return ConnectionFactory.getConnection(dbDriverType.toString().trim(),
				host.trim(), dbName.trim(), userName.trim(), password.trim());
	}

	public static void close(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
				ConnectionFactory.destroyConnection();
				log.info("closed connection success");
			} catch (SQLException e) {
				log.error("error while closing connection, connection value :"
						+ connection);
				e.printStackTrace();
			}
		}
	}

	public static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
				log.info("close statement object success");
			} catch (SQLException e) {
				log.info("close statement object failed :" + e);
				e.printStackTrace();
			}
		}
	}

	public static void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
				log.info("close resultset object success");
			} catch (SQLException e) {
				log.info("close resultSet object failed :" + e);
				e.printStackTrace();
			}
		}
	}

	public static void closeAllDBObjects(Connection connection,
			Statement statement, ResultSet resultSet) {
		close(connection);
		close(statement);
		close(resultSet);
	}

	public static ResultSet executeQuery(Connection connection,
			final String query) throws SQLException {
		Statement statement = null;
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			rs = statement.executeQuery(query);
		} catch (Exception e) {
			e.printStackTrace();
			return rs;
		}
		return rs;

	}

	public static void executeupdatequery(Connection connection,
			final String query) throws SQLException {
		Statement statement = null;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

}
