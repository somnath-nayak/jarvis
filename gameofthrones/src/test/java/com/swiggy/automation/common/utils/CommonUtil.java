package com.swiggy.automation.common.utils;

import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Properties;

public class CommonUtil {

	public static HashMap<String, String> getData(String propertiesFile) {
		HashMap<String, String> data = null;
		Properties p = null;
		try {
			data = new HashMap<String, String>();
			p = new Properties();
			p.load(new FileReader(new File(propertiesFile)));
			for (final String name : p.stringPropertyNames())
				data.put(name, p.getProperty(name));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;

	}

	public static HashMap<String, String> getData(String propertiesFile,
			String envLevl) {
		HashMap<String, String> data = null;
		Properties p = null;
		String fileName = String.format(propertiesFile, envLevl);
		try {
			data = new HashMap<String, String>();
			p = new Properties();
			p.load(new FileReader(new File(fileName)));
			for (final String name : p.stringPropertyNames())
				data.put(name, p.getProperty(name));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;

	}

}
