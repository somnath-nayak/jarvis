package com.swiggy.automation.common.utils;

import org.apache.log4j.Logger;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * @author abhishek.garg
 * 
 *         INITIAL DRAFT - 5 OCT 2016 - This class contains the Date Time
 *         utilities 1st EDIT - 6 OCT 2016 - CODE REFRACTOR - CREATED
 *         getCurrentDate() AND format(LocalDateTime, FORMAT) - ALL METHODS TO
 *         CALL ABOVE TWO METHODS FOR CREATING DATE TIME OBJECTS
 *
 *         26 OCT 2016 - Added Date Time Format for EPOCH DATE TIME
 */

public class DateTimeUtils {

	public static final Logger log = Logger.getLogger(DateTimeUtils.class);

	/**
	 * FORMAT enum - defines the date and time formats supported.
	 */
	public enum FORMAT {
		LOCALDATETIME_FOPRMAT("yyyy-MM-dd'T'HH:mm:ss.S"), STANDARDDATE_FORMAT(
				"yyyy-MM-dd HH:mm:ss.S"), EPOCHDATETINE_FORMAT("EPOCH_TIME"), LOCALDAY("ddd");

		FORMAT(String data) { /* no code constructor */
		}
	}

	/**
	 * @param days
	 * @param dateFormat
	 * @return Date in Future based on number of days passed
	 */
	public static String getFutureDateByDaysFromToday(int days,
			FORMAT dateFormat) {
		return format(getCurrentDate().plusDays(days), dateFormat);
	}

	/**
	 * @param hours
	 * @param dateFormat
	 * @return Date in Future based on number of hours passed
	 */
	public static String getFutureDateByHoursFromToday(int hours,
			FORMAT dateFormat) {
		return format(getCurrentDate().plusHours(hours), dateFormat);
	}

	/**
	 * @param minutes
	 * @param dateFormat
	 * @return Date in Future based on number of hours passed
	 */
	public static String getFutureDateByMinutesFromToday(int minutes,
			FORMAT dateFormat) {
		return format(getCurrentDate().plusMinutes(minutes), dateFormat);
	}

	/**
	 * @param seconds
	 * @param dateFormat
	 * @return Date in Future based on number of hours passed
	 */
	public static String getFutureDateBySecondsFromToday(int seconds,
														 FORMAT dateFormat) {
		return format(getCurrentDate().plusSeconds(seconds), dateFormat);
	}

	/**
	 * @param nanoSeconds
	 * @param dateFormat
	 * @return Date in Future based on number of hours passed
	 */
	public static String getFutureDateByNanoSecondsFromToday(int nanoSeconds,
														 FORMAT dateFormat) {
		return format(getCurrentDate().plusNanos(nanoSeconds), dateFormat);
	}
	/**
	 * @param hours
	 * @param dateFormat
	 * @return Date in Past based on number of hours passed
	 */
	public static String getPastDateByHoursFromToday(int hours,
			FORMAT dateFormat) {
		return format(getCurrentDate().minusHours(hours), dateFormat);
	}

	/**
	 * @param days
	 * @param dateFormat
	 * @return Date in Past based on number of days passed
	 */
	public static String getPastDateByDaysFromToday(int days,
			FORMAT dateFormat) {
		return format(getCurrentDate().minusDays(days), dateFormat);
	}

	/**
	 * @param dateOfMonth
	 * @param month
	 * @param year
	 * @param hour
	 * @param minute
	 * @param dateFormat
	 * @return Date in Past based on number of days passed
	 */
	public static String createDate(int dateOfMonth, int month, int year,
			int hour, int minute, FORMAT dateFormat) {
		return format(getCurrentDate().withDayOfMonth(dateOfMonth)
				.withMonth(month).withYear(year).withHour(hour)
				.withMinute(minute).withSecond(1), dateFormat);
	}

	/**
	 * @param dateFormat
	 * @return String Object containing Date Time based on input dateObject
	 */
	public static String getCurrentDateTime(FORMAT dateFormat) {
		return format(getCurrentDate(), dateFormat);
	}

	/**
	 * @return LocalDateTime Object containing current Date Time
	 */
	private static LocalDateTime getCurrentDate() {
		return LocalDateTime.now();
	}

	/**
	 * @param dateObject
	 * @param dateFormat
	 * @return String Object containing Date Time based on input dateObject
	 */
	public static String format(LocalDateTime dateObject, FORMAT dateFormat) {
		if (dateFormat.equals(FORMAT.STANDARDDATE_FORMAT)) {
			String date[] = dateObject.toString().split("T");
			return date[0] + " " + date[1];
		} else if (dateFormat.equals(FORMAT.LOCALDATETIME_FOPRMAT)) {
			return dateObject.toString();
		} else if (dateFormat.equals(FORMAT.EPOCHDATETINE_FORMAT)) {
			return (dateObject.atZone(ZoneId.systemDefault()).toEpochSecond()
					* 1000) + "";
		}else if(dateFormat.equals(FORMAT.LOCALDAY)){}
		log.error(" FORMAT MODULE FAILED FOR DATE TIME FORMAT ");
		return null;
	}

	/**
	 * @param seconds
	 */
	public static void wait(int seconds) {
		try {
			long milliSeconds = seconds * 1000;
			// Thread.sleep(seconds*1000);
			Thread.sleep(milliSeconds);
		} catch (Exception e) {
			log.error(
					"ERROR IN THE WAIT MODULE :: DateTimeUtils.wait(int milliseconds)");
			return;
		}
	}

	public static void main(String[] args) {
		// "2017-03-10 16:13:06+05:30"
		System.out.println(
				getUSTDateTimeInISTyyyyMMHHmmss("2017-03-10 16:13:06+05:30"));
		System.out.println(getDateTimeInyyyyMMHHmmss("2017-01-12 15:51:42.0"));
		System.out.println("Current Date");
		System.out.println(getCurrentDateTime(FORMAT.EPOCHDATETINE_FORMAT));
		System.out.println(getCurrentDateTime(FORMAT.STANDARDDATE_FORMAT));
		System.out.println(getCurrentDateTime(FORMAT.LOCALDATETIME_FOPRMAT));

		System.out.println("Future Date by 1 Day Date");
		System.out.println(
				getFutureDateByDaysFromToday(1, FORMAT.EPOCHDATETINE_FORMAT));
		System.out.println(
				getFutureDateByDaysFromToday(1, FORMAT.STANDARDDATE_FORMAT));
		System.out.println(
				getFutureDateByDaysFromToday(1, FORMAT.LOCALDATETIME_FOPRMAT));

		System.out.println("Future Date by 10 min Date");
		System.out.println(getFutureDateByMinutesFromToday(10,
				FORMAT.EPOCHDATETINE_FORMAT));
		System.out.println(getFutureDateByMinutesFromToday(10,
				FORMAT.STANDARDDATE_FORMAT));
		System.out.println(getFutureDateByMinutesFromToday(10,
				FORMAT.LOCALDATETIME_FOPRMAT));

		System.out.println("Future Date by 10 hours Date");
		System.out.println(
				getFutureDateByHoursFromToday(10, FORMAT.EPOCHDATETINE_FORMAT));
		System.out.println(
				getFutureDateByHoursFromToday(10, FORMAT.STANDARDDATE_FORMAT));
		System.out.println(getFutureDateByHoursFromToday(10,
				FORMAT.LOCALDATETIME_FOPRMAT));

	}

	public static String getDateTimeInyyyyMMHHmmss(String dateTimeStamp) {
		java.text.SimpleDateFormat sdf = null;
		java.text.SimpleDateFormat sdf1 = null;
		Date dateTime = null;
		try {
			System.out.println("sdsdsdsd");
			sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
			dateTime = sdf.parse(dateTimeStamp);
			sdf1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
		return sdf1.format(dateTime);
	}

	public static String getUSTDateTimeInISTyyyyMMHHmmss(String dateTimeStamp) {
		java.text.SimpleDateFormat sdf = null;
		java.text.SimpleDateFormat sdf1 = null;
		Date dateTime = null;
		try {
			sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss+HH:mm");
			dateTime = sdf.parse(dateTimeStamp);
			sdf1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
		return sdf1.format(dateTime);
	}

	public static String getUSTDateTimeInISTyyyyMMHHmmss1(
			String dateTimeStamp) {
		java.text.SimpleDateFormat sdf = null;
		java.text.SimpleDateFormat sdf1 = null;
		Date dateTime = null;
		try {
			sdf = new java.text.SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss.SSSSSS+HH:mm");
			dateTime = sdf.parse(dateTimeStamp);
			sdf1 = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		} catch (Exception e) {
			e.printStackTrace();// TODO: handle exception
		}
		return sdf1.format(dateTime);
	}

	/*
    Generate EPOC time
     */
	public static long generateEPOCTime()
	{
		return (Instant.now().toEpochMilli());
	}



}
