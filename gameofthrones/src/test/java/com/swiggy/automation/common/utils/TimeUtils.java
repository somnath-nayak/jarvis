package com.swiggy.automation.common.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {

	public static int getCurrentTimeIn24HourFormat() {
		SimpleDateFormat simpDate = null;
		Date date = null;
		try {
			date = new Date();
			simpDate = new SimpleDateFormat("kkmm");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.parseInt(simpDate.format(date));
	}
}