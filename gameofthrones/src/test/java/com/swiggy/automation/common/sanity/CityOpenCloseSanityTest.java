package com.swiggy.automation.common.sanity;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.common.utils.TimeUtils;
import com.swiggy.automation.test.pod.helperutils.BackendDAO;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class CityOpenCloseSanityTest extends RestTestHelper {

	Map<Integer, HashMap<String, Integer>> data = null;

	@BeforeClass
	public void getOpenCloseTimeForCities() throws SQLException {
		data = BackendDAO.getOpenAndCloseData(null);
		System.out.println(data);
	}

	// @DataProvider(name = "DP")
	// public static Object[][] getData() {
	// return null;
	// }

	@Test(priority = 0)
	public void testCityIsOpenFlag() {
		Assert.assertEquals(1, data.get(1).get("is_open").intValue(),
				"checkCityIsOpenFlag");
	}

	@DataProvider(name = "CITY_OPEN_CLOSE_CHECK_SANITY")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("CITY_OPEN_CLOSE_CHECK_SANITY");
	}

	@Test(priority = 3, dataProvider = "CITY_OPEN_CLOSE_CHECK_SANITY")
	public void testCityOpen(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData) {

		try {

			System.out.println(jsonRequestData.get("city_id").asInt());
			int openTime = data.get(jsonRequestData.get("city_id").asInt())
					.get("open_time");
			int closeTime = data.get(jsonRequestData.get("city_id").asInt())
					.get("close_time");

			JsonNode actualResponse1 = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
					apiToExecute, jsonRequestData.get("area1"));
			JsonNode actualResponse2 = RestTestUtil.sendDataAsPathParam(serviceNameMap.get("sand"),
					apiToExecute, jsonRequestData.get("area2"));
			int size1 = actualResponse1.get("data").get("rest_list").get(0)
					.findValue("restaurants").size();
			int size2 = actualResponse2.get("data").get("rest_list").get(0)
					.findValue("restaurants").size();
			if (isCityOpen(openTime, closeTime)) {
				if (data.get(jsonRequestData.get("city_id").asInt())
						.get("is_open").intValue() == 1
						&& size1 > 0
						|| data.get(jsonRequestData.get("city_id").asInt())
								.get("is_open").intValue() == 1 && size2 > 0) {
					Assert.assertTrue(true, "testCityOpen");
				} else if (data.get(jsonRequestData.get("city_id").asInt())
						.get("is_open").intValue() == 1) {
					Assert.assertTrue(true, "testCityOpen isOpen Flag Set");

				} else {
					Assert.assertTrue(false,
							"testCityOpen Something Went Wrong");
				}

			} else {

				if (size1 > 0 || size2 > 0) {
					Assert.assertTrue(false,
							"City Closed but Still Area not closed");

				} else if (data.get(jsonRequestData.get("city_id").asInt())
						.get("is_open").intValue() == 0) {
					Assert.assertTrue(false, "testCityOpen isOpen Flag Set 0");

				} else {
					Assert.assertTrue(true,
							"testCityOpen  City Closed  wrt DB OpenCloseTime");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isCityOpen(int openTime, int closeTime) {
		int currentTime = TimeUtils.getCurrentTimeIn24HourFormat();
		System.out.println("currentTime " + currentTime);
		boolean isOpen = false;
		//
		// int openTime = 1537;
		// int closeTime = 1537;
		boolean a = TimeUtils.getCurrentTimeIn24HourFormat() < openTime;
		boolean b = TimeUtils.getCurrentTimeIn24HourFormat() > closeTime;

		System.out.println("TimeUtils.getCurrentTimeIn24HourFormat()<openTime"
				+ a);
		System.out.println("TimeUtils.getCurrentTimeIn24HourFormat()>closeTime"
				+ b);

		if (openTime <= TimeUtils.getCurrentTimeIn24HourFormat()
				&& closeTime >= TimeUtils.getCurrentTimeIn24HourFormat()) {
			System.out.println("Open City");
			isOpen = true;
		} else {
			System.out.println("Close City");
			isOpen = false;
		}

		return isOpen;
	}

	public static void main(String[] args) {
		updateJsonObjectArray(null);
	}

	public static void updateJsonObjectArray(String path) {
		String aa = "abcd/area1/queryparam";
		String a[] = aa.split("/");
		for (String aaa : a) {
			System.out.println(aaa);
		}

	}

}
