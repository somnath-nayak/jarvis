package com.swiggy.automation.common.utils;

public interface IDBDriverType {
	String MYSQL = "MYSQL";
	String POSTGRES = "POSTGRES";
	String MSSQL = "MSSQL";
	String MYSQL_DRIVER_CLASS = "com.mysql.jdbc.Driver";
	String POSTGRES_DRIVER_CLASS = "org.postgresql.Driver";
	String MSSQLSERVER_DRIVER_CLASS = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

}
