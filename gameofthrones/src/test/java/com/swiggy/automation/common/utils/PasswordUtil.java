package com.swiggy.automation.common.utils;

import com.swiggy.automation.api.rest.RestTestUtil;
import org.codehaus.jackson.JsonNode;
import sun.misc.BASE64Decoder;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;

public class PasswordUtil {

	public static JsonNode getLoginData(JsonNode jsonRequestData)
			throws Exception {
		if (null == jsonRequestData) {
			return null;
		}

		if (jsonRequestData.get("mobile").asText().equals("7022026831"))
			return jsonRequestData;

		if (jsonRequestData.get("mobile").asText().equals("7338471121")
				|| !jsonRequestData.get("mobile").asText().isEmpty()) {
			return RestTestUtil.updateRequestDataBeforeSend(jsonRequestData,
					"password",
					decrypt("QlluVNnJdfkz+PjvziJ3U6Xxfi2uHQq/WcZFnBs7sWAilktvBeb4HYoRW9E4cdbR"));
		} else if (System.getProperty("env").equalsIgnoreCase("stage"))
			return jsonRequestData;

		return null;
	}

	private static String decrypt(String data) throws Exception {
		Key key = new SecretKeySpec(new byte[] { '!', '&', '&', 'G', 'e', 's',
				't', 'S', 'r', 'a', 'm', 'z', 'i', 'K', 'e', 'e' }, "AES");
		Cipher c = Cipher.getInstance("AES");
		c.init(Cipher.DECRYPT_MODE, key);
		@SuppressWarnings("restriction")
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(data);
		byte[] decValue = c.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}
}