package com.swiggy.automation.common.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

public class DBConnectionPool {

	private String driver, url, username, password;
	private int maxConnections;
	private boolean waitIfBusy;
	public Vector<Connection> availableConnections, busyConnections;
	private boolean connectionPending = false;

	public DBConnectionPool(String driver, String url, String username,
			String password, int initialConnections, int maxConnections,
			boolean waitIfBusy) throws SQLException {
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.password = password;
		this.maxConnections = maxConnections;
		this.waitIfBusy = waitIfBusy;
		if (initialConnections > maxConnections) {
			initialConnections = maxConnections;
		}

		availableConnections = new Vector<Connection>();
		busyConnections = new Vector<Connection>();
		// create new connection and assignTo ConnectionPool
		for (int i = 0; i < maxConnections; i++) {
			availableConnections.addElement(makeNewConnection());
		}
	}

	private Connection makeNewConnection() {

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, username,
					password);

			return con;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public int getTotalConnections() {
		return availableConnections.size() + busyConnections.size();
	}

	public Connection getConnection() {

		try {

			if (!availableConnections.isEmpty()) {
				int index = availableConnections.size() - 1;
				Connection existingconnection = availableConnections.get(index);
				availableConnections.removeElementAt(index);
				if (existingconnection.isClosed()) {
					notifyAll();
					return getConnection();
				} else {
					busyConnections.addElement(existingconnection);
					return existingconnection;
				}

			} else {
				if (getTotalConnections() < maxConnections
						&& !connectionPending) {
					makeNewBackgroundConnection();
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	private void makeNewBackgroundConnection() {
		try {
			Thread t = new Thread();
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void run() {
		try {
			Connection con = makeNewConnection();
			synchronized (this) {
				availableConnections.addElement(con);
				connectionPending = false;
				notifyAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void freeUpConnection(Connection con) {
		try {
			System.out.println("giving back to con Pool after usage");
			busyConnections.remove(con);
			availableConnections.addElement(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeAllConnections() {
		try {
			System.out.println("releasing All connections");
			closeConnections(availableConnections);
			availableConnections = new Vector<Connection>();
			closeConnections(busyConnections);
			busyConnections = new Vector<Connection>();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void closeConnections(Vector<Connection> connections) {
		try {
			for (int i = 0; i < connections.size(); i++) {
				Connection con = connections.elementAt(i);
				if (!con.isClosed()) {
					con.close();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * String form of ConnectionPool class.
	 */
	public synchronized String toString() {
		String info = "ConnectionPool(" + url + "," + username + ")"
				+ ", available=" + availableConnections.size() + ", busy="
				+ busyConnections.size() + ", max=" + maxConnections;
		return (info);
	}
}
