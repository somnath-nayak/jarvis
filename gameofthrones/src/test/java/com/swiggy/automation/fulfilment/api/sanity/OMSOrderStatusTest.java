package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OMSOrderStatusTest extends RestTestHelper {

	@DataProvider(name = "OMS_PLACEDSTATUS")
	public static Object[][] dsfsdf() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_PLACEDSTATUS");
	}

	@Test(priority = 1, dataProvider = "OMS_PLACEDSTATUS", groups = "Sanity")
	public void loginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "PlacedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "OMSASSIGNDE")
	public static Object[][] getAssignDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMSASSIGNDE");
	}

	// @Test(priority = 2, dataProvider = "OMSASSIGNDE", groups = "Sanity")
	public void assignDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"),apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "AssigneDESuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "OMS_CONFIRMEDSTATUS")
	public static Object[][] getconfirmedStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_CONFIRMEDSTATUS");
	}

	@Test(priority = 3, dataProvider = "OMS_CONFIRMEDSTATUS", groups = "Sanity")
	public void test1(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "ConfirmedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "OMS_ARRIVEDSTATUS")
	public static Object[][] getplacedStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_ARRIVEDSTATUS");
	}

	@Test(priority = 4, dataProvider = "OMS_ARRIVEDSTATUS", groups = "Sanity")
	public void test2(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "PlacedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "OMS_PICKEDUPSTATUS")
	public static Object[][] getplacedStatus1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_PICKEDUPSTATUS");
	}

	@Test(priority = 5, dataProvider = "OMS_PICKEDUPSTATUS", groups = "Sanity")
	public void test3(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "PickedupUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "OMS_REACHEDSTATUS")
	public static Object[][] getplacedStatus2()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_REACHEDSTATUS");
	}

	@Test(priority = 6, dataProvider = "OMS_REACHEDSTATUS", groups = "Sanity")
	public void test4(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"),apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "ReachedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}
	}

	@DataProvider(name = "OMS_DELIVEREDSTATUS")
	public static Object[][] getplacedStatu12313()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_DELIVEREDSTATUS");
	}

	@Test(priority = 7, dataProvider = "OMS_DELIVEREDSTATUS", groups = "Sanity")
	public void test5(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "DeliveredUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}
	}
}
