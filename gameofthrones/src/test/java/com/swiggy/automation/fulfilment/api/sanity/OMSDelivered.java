package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OMSDelivered extends RestTestHelper {

	@DataProvider(name = "OMS_DELIVEREDSTATUS")
	public static Object[][] getplacedStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_DELIVEREDSTATUS");
	}

	@Test(priority = 1, dataProvider = "OMS_DELIVEREDSTATUS", groups = "Sanity")
	public void loginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "DeliveredUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

}
