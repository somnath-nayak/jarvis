package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class UpdateCart extends RestTestHelper {

	@DataProvider(name = "OMS_GETCARTTOTAL")
	public static Object[][] getplacedStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_GETCARTTOTAL");
	}

	@Test(priority = 1, dataProvider = "OMS_GETCARTTOTAL", groups = "Sanity")
	public void loginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "FetchedCartTotalSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

}
