package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

// TODO: Auto-generated Javadoc

/**
 * The Class LoginToSession.
 */
public class GetOrderJsonTest extends RestTestHelper {

	@DataProvider(name = "GETORDERJSON")
	public static Object[][] getLoginData() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("GETORDERJSON");
	}

	@Test(priority = 1, dataProvider = "GETORDERJSON")
	public void loginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "OrderJsonDisplayedSuccessfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

}
