package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OmsPlaced extends RestTestHelper {

	@DataProvider(name = "OMS_PLACEDSTATUS")
	public static Object[][] getplacedStatus()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_PLACEDSTATUS");
	}

	@Test(priority = 1, dataProvider = "OMS_PLACEDSTATUS", groups = "Sanity")
	public void loginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, response, new String[] { "status" });
			Assert.assertTrue(isSuccess, "PlacedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

	@DataProvider(name = "OMSASSIGNDE")
	public static Object[][] getAssignDE() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMSASSIGNDE");
	}

	// @Test(priority = 2, dataProvider = "OMSASSIGNDE", groups = "Sanity")
	public void assignDE(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {

		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData, oms_sessionid,
					oms_token);

			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);

			Assert.assertTrue(isSuccess, "AssigneDESuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}
	}
}
