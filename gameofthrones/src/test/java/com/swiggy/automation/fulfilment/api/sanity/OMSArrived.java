package com.swiggy.automation.fulfilment.api.sanity;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class OMSArrived extends RestTestHelper {

	@DataProvider(name = "OMS_ARRIVEDSTATUS")
	public static Object[][] getplacedStatus() throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("OMS_ARRIVEDSTATUS");
	}

	@Test(priority = 1, dataProvider = "OMS_ARRIVEDSTATUS", groups = "Sanity")
	public void loginToSession(String dataGroup, String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) throws Exception {
		boolean isSuccess = false;

		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("oms"), apiToExecute, jsonRequestData,
					RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			isSuccess = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(expectedData, response,
					new String[] { "status" });
			Assert.assertTrue(isSuccess, "PlacedUpdatedSuccesfully");

		} catch (Exception e) {
			Assert.fail();
		}

	}

}
