package com.swiggy.automation.fulfilment.api.sanity;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.swiggy.automation.api.rest.RestTestHelper;
import com.swiggy.automation.api.rest.RestTestUtil;
import com.swiggy.automation.api.rest.json.utils.JsonHandlerToCheckDiffFields;
import com.swiggy.automation.api.rest.json.utils.JsonValidator;
import com.swiggy.automation.test.pod.helperutils.DeliveryDAO;
import com.swiggy.automation.test.pod.helperutils.JsonTestUtils;
import com.swiggy.automation.utils.APIDataReader;
import com.swiggy.automation.utils.IDataInilizer;
import com.swiggy.automation.utils.STAFAgent;
import com.swiggy.automation.utils.SwiggyAPIAutomationException;

public class FulFilmentSanityTest extends RestTestHelper {

	public static final Logger log = Logger
			.getLogger(FulFilmentSanityTest.class);

	Long orderId = null;
	String orderKey = null;
	Long serviceableAddressId = null;
	String addressId = null;
	String oms_host = null;
	String delivery_host = null;
	Integer delivery_port = null;
	Integer oms_port = null;
	String oms_osrftoken = null;
	String oms_sessionid = null;

	@BeforeClass
	public void beforeClass() {
		log.info("");
		log.info(
				"****************************** Before Class started ******************************");
		log.info("");
		try {


			oms_osrftoken = STAFAgent.getSTAFValue("oms_osrftoken");
			oms_sessionid = STAFAgent.getSTAFValue("oms_sessionid");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("required variables not initized/could not retrived" + e);
		}
		log.info("");
		log.info(
				"############################## Before class completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_DELIVERY_BOY_ACTIVATE")
	public static Object[][] getDeliveryBoyIds()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_DELIVERY_BOY_ACTIVATE");

	}

	@Test(priority = -100, dataProvider = "FF_DELIVERY_BOY_ACTIVATE")
	public void testMakeDeliveryBoyActive(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("");
		log.info(
				"****************************** testMakeDeliveryBoyActive started ******************************");
		log.info("");
		try {
			System.out.println(serviceableAddressId);
			System.out.println("Make delivery Boy Active");
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("delivery")
				,
					apiToExecute, jsonRequestData,
					serviceNameMap.get("delivery_authstring_username")
							+ DELIMITTER + serviceNameMap
									.get("delivery_authstring_password"));
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);

			Assert.assertTrue(status, "testMakeDeliveryBoyActive");
		} catch (Exception e) {
			e.printStackTrace();
			log.error("BackendCriticalSanityTest-->testMakeDeliveryBoyActive "
					+ e);
			Assert.fail("testMakeDeliveryBoyActive", e);

		}
		log.info("");
		log.info(
				"############################## testMakeDeliveryBoyActive completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_BACKEND_SANITY_LOGIN")
	public static Object[][] getLoginData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_BACKEND_SANITY_LOGIN");
	}

	@Test(priority = 1, dataProvider = "FF_BACKEND_SANITY_LOGIN")
	public void testloginToSession(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("");
		log.info(
				"****************************** testloginToSession started ******************************");
		log.info("");
		boolean isSuccess = false;
		try {
			JsonNode response = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			isSuccess = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					response);
			Assert.assertTrue(isSuccess, "LoginToSession");
			defaultTOKEN = response.get("data").get("token").asText()
					.toString();
			defaulTID = response.get("tid").asText().toString();
			System.out.println("TID For the Session" + defaulTID);
			System.out.println("Token For the Session" + defaultTOKEN);

		} catch (Exception e) {
			Assert.fail();
		} finally {
			if (!isSuccess) {
				System.out.println("Login UnSuccessFul.....Exiting");
				log.error("Login UnSuccessFul.....Exiting");
				System.exit(0);
			}
		}
		log.info("");
		log.info(
				"############################## testloginToSession completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_CREATE_CART_SANITY")
	public static Object[][] getCreateCartData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_CREATE_CART_SANITY");
	}

	@Test(priority = 2, dataProvider = "FF_CREATE_CART_SANITY")
	public void testCreateCartAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("");
		log.info(
				"****************************** testCreateCartAPI started ******************************");
		log.info("");
		try {
			JsonNode addressresponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					jsonRequestData);
			boolean isSuccess = JsonValidator.HIGH_LEVEL
					.validateJson(expectedData, addressresponse);
			if (addressresponse.get("statusCode").asInt() == 6) {
				System.out.println("RESTUARNT CLOSED");
				log.error("RESTUARNT CLOSED...SO EXITING");
				System.exit(0);
			}
			Assert.assertTrue(isSuccess, "CREATE_CART");
		} catch (Exception e) {
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testCreateCartAPI completed ##############################");
		log.info("");
	}

	@DataProvider(name = "FF_GET_ALLADDRESS_SANITY")
	public static Object[][] getAllAddress()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_GET_ALLADDRESS_SANITY");
	}

	@Test(priority = 3, dataProvider = "FF_GET_ALLADDRESS_SANITY")
	public void testGetAllAddressAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("");
		log.info(
				"****************************** testGetAllAddressAPI started ******************************");
		log.info("");
		try {
			JsonNode actualData = RestTestUtil.sendData(apiToExecute, defaulTID,
					defaultTOKEN);
			serviceableAddressId = JsonTestUtils
					.getServiceableAddressId(actualData);
			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status, "testGetAllAddressAPI");
		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testGetAllAddressAPI completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_PLACE_ORDER_SANITY")
	public static Object[][] getPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_PLACE_ORDER_SANITY");
	}

	@Test(priority = 4, dataProvider = "FF_PLACE_ORDER_SANITY")
	public void testPlaceOrderAPI(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData, JsonNode expectedData)
			throws Exception {
		log.info("");
		log.info(
				"****************************** testPlaceOrderAPI started ******************************");
		log.info("");

		try {
			// Thread.sleep(5000);
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "address_id", serviceableAddressId);
			System.out.println(jsonRequestData);
			System.out.println(updatedRequest);
			JsonNode placeOrderResponse = RestTestUtil.sendData(serviceNameMap.get("checkout"),apiToExecute,
					updatedRequest, defaulTID, defaultTOKEN);
			log.info("****************************************************");

			boolean status = JsonValidator.HIGH_LEVEL.validateJson(expectedData,
					placeOrderResponse);

			Assert.assertTrue(status, "testPlaceOrderAPI" + smokeTest);
			orderId = placeOrderResponse.get("data").get("order_id").asLong();
			System.out.println("ORDER ID :::" + orderId);

		} catch (Exception e) {
			log.error(e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testPlaceOrderAPI completed ##############################");
		log.info("");
	}

	@DataProvider(name = "FF_OMSORDERQUERY_SANITY")
	public static Object[][] getPlaceOrderData1()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_OMSORDERQUERY_SANITY");
	}

	// @Test(priority = 5, dataProvider = "FF_OMSORDERQUERY_SANITY")
	public void testVerifyOMSDashBoardForOrder(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testVerifyOMSDashBoardForOrder started ******************************");
		log.info("");
		try {
			int counter = 0;
			boolean status = false;
			JsonNode actualData = null;
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			do {
				Thread.sleep(20000);
				actualData = RestTestUtil.sendDataAsPathParam(
						serviceNameMap.get("oms"), apiToExecute, updatedRequest,
						oms_sessionid, oms_osrftoken);

				System.out.println("OMS QUERY RESPONSE :" + actualData);
				System.out.println("OMS QUERY RESPONSE OBJECT SIZE:"
						+ actualData.get("objects").size());
				if (actualData.get("objects").size() < 1) {
					System.out.println("Order Not appear on the dashboard");
					counter++;
					status = false;
				} else {
					status = true;
					System.out.println(
							"Order appear on the OMS dashboard...So proceeding");
					log.error(
							"Order appear on the OMS dashboard...So proceeding");

					break;
				}
				Thread.sleep(10000);
			} while (counter < 12);
			Assert.assertTrue(status,
					"testVerifyOMSDashBoardForOrder" + smokeTest);
			JsonHandlerToCheckDiffFields.handleObject(expectedData, actualData);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderActionProceed" + sanityTest);

		} catch (Exception e) {
			log.error("testVerifyOMSDashBoardForOrder Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testVerifyOMSDashBoardForOrder completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATEACTION_SANITY")
	public static Object[][] getPlaceUpdateAction()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATEACTION_SANITY");
	}

	@Test(priority = 6, dataProvider = "FF_UPDATEACTION_SANITY")
	public void testUpdateOrderActionProceed(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderActionProceed started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);

			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);

			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData, new String[] { "status" });

			Assert.assertTrue(status,
					"testUpdateOrderActionProceed" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderActionProceed" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderActionProceed Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderActionProceed completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_DEUNASSIGN_SANITY")
	public static Object[][] getPlaceOrderData12()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_DEUNASSIGN_SANITY");
	}

	@Test(priority = 7, dataProvider = "FF_DEUNASSIGN_SANITY")
	public void testVerifyUnAssignDeliveryBoy(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testVerifyUnAssignDeliveryBoy started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			long assignedOrderId = DeliveryDAO.getDeliveryBoyAssignedOrderId(
					Integer.parseInt(serviceNameMap.get("de3_for_order")));
			Thread.sleep(5000);
			if (assignedOrderId != -999) {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_id", assignedOrderId);
				actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedRequest, STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualData);
			}

			assignedOrderId = DeliveryDAO.getDeliveryBoyAssignedOrderId(
					Integer.parseInt(serviceNameMap.get("de4_for_order")));
			Thread.sleep(5000);
			if (assignedOrderId != -999) {
				updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
						jsonRequestData, "order_id", assignedOrderId);
				actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"),
						apiToExecute, updatedRequest, STAFAgent.getSTAFValue("defaultAuthString")
);
				JsonValidator.HIGH_LEVEL.validateJson(expectedData, actualData);
			}

		} catch (Exception e) {
			log.error("testVerifyOMSDashBoardForOrder Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testVerifyUnAssignDeliveryBoy completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_DEASSIGN_SANITY")
	public static Object[][] getPlaceOrderData32()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_DEASSIGN_SANITY");
	}

	@Test(priority = 8, dataProvider = "FF_DEASSIGN_SANITY")
	public void testVerifyAssignDeliveryBoyFromOMS(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testVerifyAssignDeliveryBoyFromOMS started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			boolean code = false;
			String deId = null;
			int i = 0, retryCount = 3;
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			do {
				try {
					updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
							updatedRequest, "delivery_boy_id", deId);
					actualData = RestTestUtil.sendData(serviceNameMap.get("delivery"), apiToExecute, updatedRequest,
							oms_sessionid, oms_osrftoken);
					code = actualData.get("status").asBoolean();
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (code == true) {
					System.out.println("DE assigned >>>>>>>>>>>:" + deId);
					break;
				} else {
					deId = serviceNameMap.get("de4_for_order");
				}
				Thread.sleep(10000);
			} while (code == false && i++ < retryCount);

			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData, new String[] { "status" });

			Assert.assertTrue(status,
					"testVerifyAssignDeliveryBoyFromOMS" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testVerifyAssignDeliveryBoyFromOMS" + sanityTest);

		} catch (Exception e) {
			log.error("testVerifyAssignDeliveryBoyFromOMS Failed, Exception :"
					+ e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testVerifyAssignDeliveryBoyFromOMS completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_PLACED")
	public static Object[][] getPlaceOrderData13()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_PLACED");
	}

	@Test(priority = 9, dataProvider = "FF_UPDATESTATUS_PLACED")
	public void testUpdateOrderStatusAsPlaced(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsPlaced started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsPlaced Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsPlaced completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_UNPLACED")
	public static Object[][] getUnPlaceOrderData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_UNPLACED");
	}

	@Test(priority = 10, dataProvider = "FF_UPDATESTATUS_UNPLACED")
	public void testUpdateOrderStatusAsUnPlaced(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsUnPlaced started ******************************");
		log.info("");

		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;

		try {

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
			/*
			 * log.info("oms_host in unlace = " + oms_host +
			 * " oms_port in unplace = " + oms_port + " apiToExecute is = " +
			 * apiToExecute + " upated request is  = " + updatedRequest);
			 * log.info("Actual data is = " + actualData);
			 */
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsPlaced Failed, Exception :" + e);
			Assert.fail();
		}

		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsUnPlaced completed ##############################");
		log.info("");
	}

	@DataProvider(name = "FF_REASSIGN_PLACER")
	public static Object[][] getReassignPlacerData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_REASSIGN_PLACER");
	}

	@Test(priority = 11, dataProvider = "FF_REASSIGN_PLACER")
	public void testReassignPlacer(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testReassignPlacer started ******************************");
		log.info("");

		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;

		try {

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
			/*
			 * log.info("oms_host in unlace = " + oms_host +
			 * " oms_port in unplace = " + oms_port + " apiToExecute is = " +
			 * apiToExecute + " upated request is  = " + updatedRequest);
			 */
			log.info("Actual data in reassign placer is = " + actualData);

			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsPlaced Failed, Exception :" + e);
			Assert.fail();
		}

		log.info("");
		log.info(
				"############################## testReassignPlacer completed ##############################");
		log.info("");
	}

	@DataProvider(name = "FF_REASSIGN_FOLLOWUP")
	public static Object[][] getReassignFollowuprData()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_REASSIGN_FOLLOWUP");
	}

	@Test(priority = 12, dataProvider = "FF_REASSIGN_FOLLOWUP")
	public void testReassignFollowup(String dataGroup, String apiToExecute,
			String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testReassignFollowup started ******************************");
		log.info("");

		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;

		try {

			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest, oms_sessionid, oms_osrftoken);
			/*
			 * log.info("oms_host in unlace = " + oms_host +
			 * " oms_port in unplace = " + oms_port + " apiToExecute is = " +
			 * apiToExecute + " upated request is  = " + updatedRequest);
			 */
			log.info("Actual data in reassign followup is = " + actualData);

			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPlaced" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsPlaced Failed, Exception :" + e);
			Assert.fail();
		}

		log.info("");
		log.info(
				"############################## testReassignFollowup completed ##############################");
		log.info("");
	}

	public void testOrderEditNConfirmFromOMS(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testOrderEditNConfirmFromOMS started ******************************");
		log.info("");
		boolean status = false;
		try {

			Assert.assertTrue(status,
					"testVerifyOMSDashBoardForOrder" + smokeTest);

		} catch (Exception e) {
			log.error("testVerifyOMSDashBoardForOrder Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testOrderEditNConfirmFromOMS completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_CONFIRMED")
	public static Object[][] getPlaceOrderData21()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_CONFIRMED");
	}

	@Test(priority = 13, dataProvider = "FF_UPDATESTATUS_CONFIRMED")
	public void testUpdateOrderStatusAsConfirmed(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsConfirmed started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsConfirmed" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsConfirmed" + sanityTest);

		} catch (Exception e) {
			log.error(
					"testUpdateOrderStatusAsConfirmed Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsConfirmed completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_ARRIVED")
	public static Object[][] getPlaceOrderData09()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_ARRIVED");
	}

	@Test(priority = 14, dataProvider = "FF_UPDATESTATUS_ARRIVED")
	public void testUpdateOrderStatusAsArrived(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsArrived started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsArrived" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsArrived" + sanityTest);
		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsArrived Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsArrived completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_PICKED")
	public static Object[][] getPlaceOrderData02()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_PICKED");
	}

	@Test(priority = 15, dataProvider = "FF_UPDATESTATUS_PICKED")
	public void testUpdateOrderStatusAsPickedUp(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsPickedUp started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPickedUp" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsPickedUp" + sanityTest);

		} catch (Exception e) {
			log.error(
					"testUpdateOrderStatusAsPickedUp Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsPickedUp completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_REACHED")
	public static Object[][] getPlaceOrderData22()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_REACHED");
	}

	@Test(priority = 16, dataProvider = "FF_UPDATESTATUS_REACHED")
	public void testUpdateOrderStatusAsReached(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsReached started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsReached" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsReached" + sanityTest);

		} catch (Exception e) {
			log.error("testUpdateOrderStatusAsReached Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsReached completed ##############################");
		log.info("");

	}

	@DataProvider(name = "FF_UPDATESTATUS_DELIVERED")
	public static Object[][] getPlaceOrderData23()
			throws SwiggyAPIAutomationException {
		return APIDataReader.getDataSetData("FF_UPDATESTATUS_DELIVERED");
	}

	@Test(priority = 17, dataProvider = "FF_UPDATESTATUS_DELIVERED")
	public void testUpdateOrderStatusAsDelivered(String dataGroup,
			String apiToExecute, String dataSetName, JsonNode jsonRequestData,
			JsonNode expectedData) {
		log.info("");
		log.info(
				"****************************** testUpdateOrderStatusAsDelivered started ******************************");
		log.info("");
		boolean status = false;
		JsonNode updatedRequest = null;
		JsonNode actualData = null;
		try {

			Thread.sleep(10000);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			actualData = RestTestUtil.sendData(serviceNameMap.get("oms"),
					apiToExecute, updatedRequest,  RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid));
			status = JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedData, actualData,
					new String[] { "status", "message" });
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsDelivered" + smokeTest);

			status = JsonValidator.DETAILED_LEVEL.validateJson(expectedData,
					actualData);
			Assert.assertTrue(status,
					"testUpdateOrderStatusAsDelivered" + sanityTest);

		} catch (Exception e) {
			log.error(
					"testUpdateOrderStatusAsDelivered Failed, Exception :" + e);
			Assert.fail();
		}
		log.info("");
		log.info(
				"############################## testUpdateOrderStatusAsDelivered completed ##############################");
		log.info("");

	}

	@SuppressWarnings("unchecked")
	@AfterTest()
	public void cancelOrders() throws Exception {
		log.info("");
		log.info(
				"****************************** After Test started ******************************");
		log.info("");
		LinkedHashMap<String, Object> requestMap = new LinkedHashMap<String, Object>();
		@SuppressWarnings("rawtypes")
		LinkedHashMap expectedResponse = new LinkedHashMap<String, Object>();
		requestMap.put("order_id", 1086957949); // dummy structure
		requestMap.put("status", "cancelled");
		requestMap.put("time", 1492609269L); // dummy structure
		requestMap.put("reason_id", 65);
		requestMap.put("sub_reason_id", null);

		requestMap.put("reason_text", "Test Order by Tech Team");
		requestMap.put("is_food_prepared", false);
		requestMap.put("cancellation_fee_flag", false);
		requestMap.put("cancellation_fee", 0);
		requestMap.put("responsible", 1);
		requestMap.put("initiator", 3);
		requestMap.put("input_text", "");

		expectedResponse.put("status", true);
		expectedResponse.put("message", "Order status updated to cancelled");
		JsonNode jsonRequestData = RestTestUtil.getJsonNodeFromMap(requestMap);
		JsonNode expectedResponseData = RestTestUtil
				.getJsonNodeFromMap(expectedResponse);
		if (orderIds.size() > 0)
			log.info("No of Orders to be cancelled :" + orderIds.size());
		for (Long orderId : orderIds) {
			JsonNode updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					jsonRequestData, "order_id", orderId);
			updatedRequest = RestTestUtil.updateRequestDataBeforeSend(
					updatedRequest, "time", System.currentTimeMillis() / 1000);
			JsonNode actualData = RestTestUtil.sendData(serviceNameMap.get("oms"), "OMSUPDATESTATUS", updatedRequest, RestTestUtil.getRequestHeadersOms(oms_osrftoken, oms_sessionid)
					);
			JsonValidator.HIGH_LEVEL.validateJsonWithCustomFields(
					expectedResponseData, actualData,
					new String[] { "status", "message" });
			Thread.sleep(2000);
		}
		log.info("");
		log.info(
				"############################## After Test completed ##############################");
		log.info("");

	}

}
