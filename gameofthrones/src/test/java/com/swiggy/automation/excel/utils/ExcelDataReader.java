package com.swiggy.automation.excel.utils;

import com.google.gson.JsonParser;
import com.swiggy.automation.common.utils.APIUtils;
import com.swiggy.automation.utils.IDataInilizer;
import com.swiggy.automation.utils.ITestDataReader;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;

/***
 * 
 * @author mohammedramzi
 * 
 */

public class ExcelDataReader extends ExcelBase implements ITestDataReader {

	Logger log = Logger.getLogger(ExcelDataReader.class);

	private ExcelDataReader(String xlsSheetPath, String xlsSheetName) {
		super(xlsSheetPath, xlsSheetName);
	}

	public static ExcelDataReader getExcelObject(String test_inputExcelDataFilePath, String test_inputDataSheetName) {
		return new ExcelDataReader(test_inputExcelDataFilePath, test_inputDataSheetName);
	}

	public Object[][] getAPIDataSetTestData(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;
		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;
		System.out.println(System.getProperty(IDataInilizer.KEY_ENV));
		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equals(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equals(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equals(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][5];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;

			while (itr.hasNext()) {

				data[i][0] = dataGroupName;

				data[i][1] = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);
				data[i][2] = itr.next();

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (null == requestData || requestData.trim().isEmpty()) {
					data[i][3] = null;
					log.warn("request data in xls sheet is null..Just an Info");
				} else if (requestData.trim().startsWith("{\"filename\"")) {
					log.info("Read Json Expected Data From resources/Jsonfiles folder instead from ExcelSheet");

					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser.parse(new FileReader(new File(".//resources//jsonfiles//"
							+ APIUtils.convertStringtoJSON(requestData.trim()).get("filename").asText())));
					data[i][3] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][3] = APIUtils.convertStringtoJSON(requestData.trim());
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (null == expectedData || expectedData.trim().isEmpty()) {
					log.error("expectedData data in xls sheet is nullo empty.. for DATA GROUP:" + dataGroupName);
					data[i][4] = null;
				} else if (expectedData.trim().contains("\"filename\"")) {
					log.info("Read Json Expected Data From resources/Jsonfiles folder instead from ExcelSheet");
					JsonParser jsonparser = new JsonParser();
					JsonNode s = APIUtils.convertStringtoJSON(expectedData.toString().trim());

					System.out.println(s.get("filename").asText());

					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + s.get("filename").asText())));

					data[i][4] = APIUtils.convertStringtoJSON(object.toString());
					System.out.println("EDaata :" + data[i][4]);
				} else if (expectedData.trim().startsWith("{")) {
					data[i][4] = APIUtils.convertStringtoJSON(expectedData.trim());
				} else if (!expectedData.contains(null)) {
					String expectedOutput = "{\"expectedOutput\":\"" + expectedData.trim() + "\"}";
					data[i][4] = APIUtils.convertStringtoJSON(expectedOutput.trim());
				} else {
					data[i][4] = APIUtils.convertStringtoJSON(expectedData.trim());
				}

				startNEndIndex[0]++;
				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()>>> Issue while getting TestData(dataSet,req,resp,api) from Excel ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

	/**
	 * return only APIKEY,REQDATA,EXP DATA
	 * 
	 * @param dataGroupName
	 * @return
	 */

	public Object[][] getAPIDataSetTestDataMinimal(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;

		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;

		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][3];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;

			while (itr.hasNext()) {

				data[i][0] = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (null == requestData || requestData.trim().isEmpty()) {
					data[i][1] = null;
				} else if (!requestData.trim().startsWith("{")) {
					log.info("You have passed Json file as requestData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + requestData.trim())));
					data[i][1] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][1] = APIUtils.convertStringtoJSON(requestData);
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (expectedData.trim().isEmpty()) {
					data[i][2] = APIUtils.convertStringtoJSON(expectedData);
				}

				else if (!expectedData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + expectedData.trim())));
					data[i][2] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][2] = APIUtils.convertStringtoJSON(expectedData.trim());
				}

				startNEndIndex[0]++;
				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()>>> Issue while getting TestData(dataSet,req,resp,api) from Excel ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

	/***
	 * 
	 * This Function return the request or inputData,ExpectedData along with
	 * FieldDefined to be present. So that,In Test expected data will be compared
	 * against the field json elements. this field Json will be defined in seperate
	 * APIFIELDS sheet . For each API, there will be expected fields as JsonObjects
	 * 
	 * 
	 * @param dataGroupName
	 * @param requestDataColumnHeader
	 * @param expectedDataColumnHeader
	 * @return
	 */

	public Object[][] getAPIDataSetTestDataWithExpectedFields(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;
		ExcelDataReader reader = null;

		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;

		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][6];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;
			String apiFieldData = null;

			reader = getExcelObject(suitePropertyMap.get("inputexceldatafilepath"),
					suitePropertyMap.get("excelapifieldsheetname"));

			while (itr.hasNext()) {

				data[i][0] = dataGroupName;
				data[i][1] = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);
				data[i][2] = itr.next();

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (null == requestData || requestData.trim().isEmpty()) {
					data[i][3] = null;
				} else if (!requestData.trim().startsWith("{")) {
					log.info("You have passed Json file as requestData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + requestData.trim())));
					data[i][3] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][3] = APIUtils.convertStringtoJSON(requestData.trim());
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (null == expectedData || expectedData.trim().isEmpty()) {
					data[i][4] = null;
					log.error("Expected Data is null or empty for dataGroup :" + dataGroupName);
				}

				else if (!expectedData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + expectedData.trim())));
					data[i][4] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][4] = APIUtils.convertStringtoJSON(expectedData.trim());
				}

				// String x = getCellValue(reader,
				// IDataInilizer.API_COLUMN_HEADER_NAME);
				apiFieldData = getExcelData(reader, data[i][1].toString());

				if (null == apiFieldData || apiFieldData.trim().isEmpty()) {
					data[i][5] = null;
					log.error("apiField Data is null or empty for dataGroup :" + dataGroupName);
				} else if (!apiFieldData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + apiFieldData.trim())));
					data[i][5] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][5] = APIUtils.convertStringtoJSON(apiFieldData.trim());
				}

				startNEndIndex[0]++;
				i++;
			}

		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()..Issue while getting TestData(dataSet,req,resp,api,apiFieldsData) from Excel .Exception IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

	public String getExcelData(ExcelDataReader reader, String apiKey) {
		int row = 1;
		String a = null;
		int numberOfRows = reader.getNumberOfRows();
		log.debug(apiKey);
		log.debug(reader.getCellValue(row, IDataInilizer.API_COLUMN_HEADER_NAME));
		String apiexpectedDataColumnHeader = null;
		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			apiexpectedDataColumnHeader = IDataInilizer.API_PROD_EXPECTED_FIELD_COLUMN_HEADER_NAME;
		}
		while (row < numberOfRows) {
			if (reader.getCellValue(row, IDataInilizer.API_COLUMN_HEADER_NAME).equals(apiKey)) {
				a = reader.getCellValue(row, apiexpectedDataColumnHeader);
				break;
			}
			row++;
		}
		return a;
	}

	public String getCellValue(int rowId, String columnHeader) {
		int columnIndexToRead = -1;
		try {
			columnIndexToRead = getColumnIndex(columnHeader);
			return getCellValue(rowId, columnIndexToRead);
		} catch (Exception e) {

		}
		return null;
	}

	public ArrayList<String> getTestDataColumnHeaders() {
		ArrayList<String> headerFields = new ArrayList<String>();
		int columnsInEachRow = getNumberOfColumnsForRow();
		for (int i = 0; i < columnsInEachRow; i++) {
			try {
				headerFields.add(getCellValue(0, i));
			} catch (Exception e) {
				log.error(e.getMessage() + " ==> getTestDataColumnHeaders()>>>  ECZZEPTION IZ :" + e);
				e.printStackTrace();
			}

		}
		return headerFields;
	}

	public ArrayList<String> getColumnData(int rowId) {
		ArrayList<String> columnData = new ArrayList<String>();
		int columnsInEachRow = getNumberOfColumnsForRow();
		for (int i = 0; i < columnsInEachRow; i++) {
			try {
				columnData.add(getCellValue(rowId, i));
			} catch (Exception e) {
				log.error(e.getMessage() + " ==> getColumnData()>>>  ECZZEPTION IZ :" + e);
				e.printStackTrace();
			}

		}
		return columnData;
	}

	public HashMap<String, Integer[]> getTestDataGroupInfo() {
		HashMap<String, Integer[]> dataGroups = new HashMap<String, Integer[]>();
		String cellValue = null;
		int startRowForDataGroup = 1;
		String dataGroupName = null;
		int numberOfRows = getNumberOfRows();
		for (int row = 1; row < numberOfRows; row++) {
			cellValue = getCellValue(row, 0);
			if (!cellValue.equals(IDataInilizer.DATAGROUP_DELIMITTER)
					&& !cellValue.equals(IDataInilizer.SUITE_DELIMITTER)) {
				dataGroupName = cellValue.toString();
			}
			if (cellValue.equals(IDataInilizer.DATAGROUP_DELIMITTER)) {
				dataGroups.put(dataGroupName, new Integer[] { startRowForDataGroup, row - 1 });
				startRowForDataGroup = row + 1;
			}
			if (cellValue.equals(IDataInilizer.SUITE_DELIMITTER)) {
				dataGroups.put(dataGroupName, new Integer[] { startRowForDataGroup, row - 1 });
				break;
			}

		}

		return dataGroups;
	}

	public Integer[] getTestDataGroupStartAndEndRow(String dataGroup) {
		try {
			HashMap<String, Integer[]> dataGroupInfo = getTestDataGroupInfo();
			if (dataGroupInfo.containsKey(dataGroup)) {
				return dataGroupInfo.get(dataGroup);
			} else {
				log.error("given data group Not Found dataGroup :" + dataGroup);
				System.out.println("given data group Not Found dataGroup :" + dataGroup);
			}
		} catch (Exception e) {
			log.error(e.getMessage() + " ==> getTestDataGroupStartAndEndRow()>>>  ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<String> getDataSetNames(String dataGroup) {
		try {
			ArrayList<String> dataSetList = new ArrayList<String>();
			Integer[] dataGroupStartAndEndIndex = getTestDataGroupStartAndEndRow(dataGroup);
			int dataSetColumnIndex = getColumnIndex(IDataInilizer.DATASET_NAME);
			for (int row = dataGroupStartAndEndIndex[0]; row <= dataGroupStartAndEndIndex[1]; row++) {
				dataSetList.add(getCellValue(row, dataSetColumnIndex));
			}
			return dataSetList;
		} catch (Exception e) {
			log.error(e.getMessage() + " ==> getDataSetNames()>>>  ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return null;
	}

	public int getColumnIndex(String columnHeader) {
		String cellValue = null;
		int columnsInEachRow = getNumberOfColumnsForRow();
		for (int columnIndex = 0; columnIndex < columnsInEachRow; columnIndex++) {
			cellValue = getCellValue(0, columnIndex);
			if (cellValue.equals(columnHeader)) {
				return columnIndex;
			}
		}
		return -999;

	}

	public LinkedHashMap<String, String> getTestData(String dataGroupName, String columnHeader) {
		LinkedHashMap<String, String> testData = null;
		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;

		try {
			testData = new LinkedHashMap<String, String>();
			dataSetNames = getDataSetNames(dataGroupName);
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);
			Iterator<String> itr = dataSetNames.listIterator();
			while (itr.hasNext()) {
				testData.put(itr.next(), getCellValue(0, startNEndIndex[0]++));
			}
		} catch (Exception e) {
			log.error(e.getMessage() + " ==> getTestData()>>>  ECZZEPTION IZ :" + e);
			e.printStackTrace();
		}
		return testData;

	}

	// =======================================================

	public Object[][] getDataSetTestData(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;
		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;
		System.out.println(System.getProperty(IDataInilizer.KEY_ENV));
		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][4];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;

			while (itr.hasNext()) {

				data[i][0] = dataGroupName;

				data[i][1] = itr.next();

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (!requestData.trim().isEmpty()) {
					LinkedHashMap<Object, Object> a1 = null;
					// data[i][2] = APIUtils.convertStringtoJSON(requestData);
					System.out.println(requestData);
					String a[] = requestData.split("\n");

					System.out.println(a[0].trim());
					System.out.println(a[1].trim());
					a1 = new LinkedHashMap<Object, Object>();
					for (String x : a) {
						System.out.println(a);
						System.out.println(x);
						String[] abcd = x.split("=");

						System.out.println(abcd[0]);
						System.out.println(abcd[1]);
						a1.put(abcd[0], abcd[1]);
					}
					data[i][2] = a1;
					System.out.println(data[i][2]);
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (!expectedData.trim().isEmpty()) {
					LinkedHashMap<Object, Object> a1 = null;
					// data[i][3] = APIUtils.convertStringtoJSON(requestData);
					System.out.println(expectedData);
					String a[] = expectedData.split("\n");
					a1 = new LinkedHashMap<Object, Object>();
					for (String x : a) {
						System.out.println(a);
						System.out.println(x);
						String[] abcd = x.split("=");

						a1.put(abcd[0], abcd[1]);
					}
					data[i][3] = a1;
					System.out.println(data[i][3]);
				}

				startNEndIndex[0]++;
				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()>>> Issue while getting TestData(dataSet,req,resp,api) from Excel ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

	/**
	 * return only APIKEY,REQDATA,EXP DATA
	 * 
	 * @param dataGroupName
	 * @return
	 */

	public Object[][] getDataSetTestDataMinimal(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;

		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;
		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][3];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;

			while (itr.hasNext()) {

				data[i][0] = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (null == requestData || requestData.trim().isEmpty()) {
					data[i][3] = null;
				} else if (!requestData.trim().startsWith("{")) {
					log.info("You have passed Json file as requestData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + requestData.trim())));
					data[i][1] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][1] = APIUtils.convertStringtoJSON(requestData);
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (expectedData.trim().isEmpty()) {
					data[i][2] = APIUtils.convertStringtoJSON(expectedData);
				}

				else if (!expectedData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + expectedData.trim())));
					data[i][2] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][2] = APIUtils.convertStringtoJSON(expectedData.trim());
				}

				startNEndIndex[0]++;
				i++;
			}
		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()>>> Issue while getting TestData(dataSet,req,resp,api) from Excel ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

	/***
	 * 
	 * This Function return the request or inputData,ExpectedData along with
	 * FieldDefined to be present. So that,In Test expected data will be compared
	 * against the field json elements. this field Json will be defined in seperate
	 * APIFIELDS sheet . For each API, there will be expected fields as JsonObjects
	 * 
	 * 
	 * @param dataGroupName
	 * @param requestDataColumnHeader
	 * @param expectedDataColumnHeader
	 * @return
	 */

	public Object[][] getDataSetTestDataWithExpectedFields(String dataGroupName) {

		ArrayList<String> dataSetNames = null;
		Integer[] startNEndIndex = null;
		Object[][] data = null;
		int i = 0;
		ExcelDataReader reader = null;

		String inputDataColumnHeader = null;
		String expectedDataColumnHeader = null;
		if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_PROD)) {
			inputDataColumnHeader = IDataInilizer.PROD_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.PROD_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE1)) {
			inputDataColumnHeader = IDataInilizer.STAGE1_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE1_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE2)) {
			inputDataColumnHeader = IDataInilizer.STAGE2_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE2_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		} else if (System.getProperty(IDataInilizer.KEY_ENV).equalsIgnoreCase(IDataInilizer.KEY_ENV_STAGE3)) {
			inputDataColumnHeader = IDataInilizer.STAGE3_INPUTDATA_COLUMN_HEADER_NAME;
			expectedDataColumnHeader = IDataInilizer.STAGE3_EXEPECTEDDATA_COLUMN_HEADER_NAME;
		}

		try {
			dataSetNames = getDataSetNames(dataGroupName);
			data = new Object[dataSetNames.size()][6];
			startNEndIndex = getTestDataGroupStartAndEndRow(dataGroupName);

			log.debug("DataGroupName :" + dataGroupName + " StartNEndIntex :" + startNEndIndex[0] + ","
					+ startNEndIndex[1]);

			reader = new ExcelDataReader(suitePropertyMap.get("inputexceldatafilepath"),
					suitePropertyMap.get("excelapifieldsheetname"));

			// reader = getExcelObject(
			// suiteProperties.get("inputexceldatafilepath"),
			// suiteProperties.get("excelapifieldsheetname"));

			Iterator<String> itr = dataSetNames.listIterator();
			String requestData = null;
			String expectedData = null;
			String apiFieldData = null;

			while (itr.hasNext()) {

				data[i][0] = dataGroupName;
				data[i][1] = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);
				data[i][2] = itr.next();

				requestData = getCellValue(startNEndIndex[0], inputDataColumnHeader);
				if (null == requestData || requestData.trim().isEmpty()) {
					data[i][3] = null;
				} else if (!requestData.trim().startsWith("{")) {
					log.info("You have passed Json file as requestData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + requestData.trim())));
					data[i][3] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][3] = APIUtils.convertStringtoJSON(requestData);
				}

				expectedData = getCellValue(startNEndIndex[0], expectedDataColumnHeader);

				if (expectedData.trim().isEmpty()) {
					data[i][4] = APIUtils.convertStringtoJSON(expectedData);
				}

				else if (!expectedData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + expectedData.trim())));
					data[i][4] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][4] = APIUtils.convertStringtoJSON(expectedData.trim());
				}

				String x = getCellValue(startNEndIndex[0], IDataInilizer.API_COLUMN_HEADER_NAME);
				apiFieldData = getExcelData(reader, x);

				if (apiFieldData.trim().isEmpty()) {
					data[i][5] = APIUtils.convertStringtoJSON(apiFieldData);
				}

				else if (!apiFieldData.trim().startsWith("{")) {
					log.info("You have passed Json file as expectedData...I willread N prepare Json object for you");
					JsonParser jsonparser = new JsonParser();
					Object object = jsonparser
							.parse(new FileReader(new File(".//resources//jsonfiles//" + apiFieldData.trim())));
					data[i][5] = APIUtils.convertStringtoJSON(object.toString());
				} else {
					data[i][5] = APIUtils.convertStringtoJSON(apiFieldData.trim());
				}

				startNEndIndex[0]++;
				i++;
			}

		} catch (Exception e) {
			log.error(e.getMessage()
					+ " ==> getDataSetTestData()>>> Issue while getting TestData(dataSet,req,resp,api) from Excel ECZZEPTION IZ :"
					+ e);
			e.printStackTrace();
		}
		return data;

	}

}
