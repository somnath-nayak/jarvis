package com.swiggy.automation.excel.utils;

import com.swiggy.automation.utils.APITestBase;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * 
 * @author mohammedramzi
 *
 */
public class ExcelBase extends APITestBase {
	private InputStream inputstream;
	private Sheet sheet;
	private Workbook workbook;

	public ExcelBase(String xlsSheetPath, String xlsSheetName) {
		init(xlsSheetPath, xlsSheetName);
	}

	private void init(String testSheetPath, String testSheetName) {
		try {
			if (!(new File(testSheetPath).exists())) {
				System.out
						.println("File Not Exists...So Exiting from Execution");
			}
			System.out.println(new File(testSheetPath).getAbsolutePath());
			inputstream = new FileInputStream(
					new File(testSheetPath).getAbsolutePath());

			if (testSheetPath.endsWith(".xlsx")) {
				workbook = new XSSFWorkbook(inputstream);

			} else if (testSheetPath.endsWith(".xls")) {
				workbook = new HSSFWorkbook(inputstream);
			} else {
				System.out
						.println("FILE EXTENSION COULD NOT BE IDENTIFIED.... EXTITING");
				System.exit(0);
			}
			sheet = workbook.getSheet(testSheetName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getCellValue(int rowId, int columnId) {
		try {

			if (null != sheet.getRow(rowId).getCell(columnId).toString()) {
				return sheet.getRow(rowId).getCell(columnId)
						.getStringCellValue().toString();
			}

		} catch (Exception e) {
		}
		return null;
	}

	public int getNumberOfRows() {
		return sheet.getPhysicalNumberOfRows();
	}

	public int getNumberOfColumnsForRow() {
		return sheet.getRow(0).getPhysicalNumberOfCells();
	}

	public int getNumberOfColumnsForRow(int rowId) {
		return sheet.getRow(rowId).getPhysicalNumberOfCells();
	}

	public ArrayList<String> readColumnData(int rowId) {
		ArrayList<String> columnData = null;
		try {
			columnData = new ArrayList<String>();
			int numberOfColumns = getNumberOfColumnsForRow(0);
			for (int col = 0; col < numberOfColumns; col++) {
				columnData.add(sheet.getRow(rowId).getCell(col)
						.getStringCellValue().toString());
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return columnData;
	}

}
