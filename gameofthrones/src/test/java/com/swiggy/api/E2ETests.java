package com.swiggy.api;

import com.swiggy.api.datacreation.DataCreation;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.RestaurantHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import org.apache.commons.io.FileUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.math.BigInteger;

/**
 * Created by kiran.j on 3/30/18.
 */
public class E2ETests {

    @Test
    public void testDataCreation() {
        try {
            DataCreation dataCreation = new DataCreation();
            CheckoutHelper checkoutHelper = new CheckoutHelper();
            CMSHelper cmsHelper = new CMSHelper();
            RestaurantHelper restaurantHelper = new RestaurantHelper();
            File file = new File("../Data/Payloads/JSON/create_Data");
            String json = FileUtils.readFileToString(file);
            System.out.println(dataCreation.unwrapJson(json));
            int rest_id = dataCreation.createDataInOrder();
            restaurantHelper.pushRestaurantEvent(Integer.toString(rest_id));
            BigInteger item_id = (BigInteger)cmsHelper.getItemIds(Integer.toString(rest_id)).get(0).get("id");
            System.out.println("value is"+item_id.intValue());
            dataCreation.addAddress(Integer.toString(rest_id),"1000000003","swiggy");
            checkoutHelper.placeOrder("1000000003", "swiggy", Integer.toString(item_id.intValue()), "2", Integer.toString(rest_id));
        } catch (Exception e) {
            e.printStackTrace();
            Assert.assertTrue(false);
        }
    }

}
