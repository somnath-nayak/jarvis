/**
 * @author manu.chadha
 */

package com.swiggy.api;

import framework.gameofthrones.Cersei.GoogleSpreadSheetHelper;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.GeneralSecurityException;
import java.util.List;

public class GoogleSheetTest {

    public static void main(String[] args) throws GeneralSecurityException, IOException,URISyntaxException {
        GoogleSpreadSheetHelper helper=new GoogleSpreadSheetHelper();
        String range = "swiggyurls!A2:W191";
        List<List<Object>> values =helper.getSpreadSheetRecords("1rFN4dg1osYzdUgt1h-swiFBUZOkIGeVgxlAYIWCDJSk", range);
        System.out.printf("%s, %s\n", values.get(0), values.get(1));
    }

}

