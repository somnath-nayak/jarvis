package com.swiggy.api.order_flow;

import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kiran.j on 7/9/18.
 */
public class PlaceOrderDp {

    @DataProvider(name = "order")
    public Iterator<Object[]> order() {
        List<Object[]> obj = new ArrayList<>();
        obj.add(new Object[]{System.getenv("crm_mobile_number"), System.getenv("crm_password"), System.getenv("crm_rest_id"), System.getenv("crm_item_id"),
                        System.getenv("crm_item_quantity"), System.getenv("crm_to_mark_status"), System.getenv("ff_status")});
        return obj.iterator();
    }
}
