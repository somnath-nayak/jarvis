package com.swiggy.api.order_flow;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceHelper;
import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import com.swiggy.api.order_flow.constants.OrderFlowConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

import java.io.IOException;
import java.time.Instant;
import java.util.HashMap;

/**
 * Created by kiran.j on 7/9/18.
 */
public class PlaceOrder {

    CheckoutHelper checkoutHelper = new CheckoutHelper();
    CMSHelper cmsHelper = new CMSHelper();
    DeliveryDataHelper deliverydatahelper = new DeliveryDataHelper();
    DeliveryServiceHelper deliveryServiceHelper=new DeliveryServiceHelper();
    OMSHelper omsHelper = new OMSHelper();

    public HashMap<String, String> placeOrder(String mobileno, String password, String rest_id, String item_id, String quantity, String delivery_status, String ff_status) {
        HashMap<String, String> map = new HashMap<>();
        boolean success = false;
        try {
            map.put("order_id","0");
            map.put("success", "false");
            addServicableAddress(mobileno, password, rest_id);
            Processor processor = checkoutHelper.placeOrder(mobileno, password, item_id, quantity, rest_id);
            Long orderId = Long.parseLong(JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.order_id").toString());
            System.out.println("Order Created order_id: "+ orderId);

            success = processOrderInDeliveryStageCRM(orderId.toString(), delivery_status, rest_id, ff_status);
            map.put("order_id", (orderId == 0) ? "0": orderId.toString());
            map.put("success", (orderId > 0 && orderId.toString() != null) ? "true" : "false");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public void addServicableAddress(String mobileno, String password, String rest_id) {
        HashMap<String, String> address_map = new HashMap<String, String>();
        String epochTime = String.valueOf(Instant.now().getEpochSecond());
        address_map.put("name", OrderFlowConstants.address_name);
        address_map.put("mobile", OrderFlowConstants.address_mobile);
        address_map.put("address", OrderFlowConstants.address);
        address_map.put("landmark", OrderFlowConstants.address_landmark);
        address_map.put("area", OrderFlowConstants.address_area);
        address_map.put("flat_no", OrderFlowConstants.address_flatno);
        address_map.put("city", OrderFlowConstants.address_city);
        address_map.put("annotation", OrderFlowConstants.address_annotation);
        String lats[] = cmsHelper.getRestaurantDetails(rest_id).get("lat_long").toString().split(",");
        address_map.put("lat", lats[0]);
        address_map.put("lng", lats[1]);
        Processor loginResponse = SnDHelper.consumerLogin(mobileno, password);
        String tid = loginResponse.ResponseValidator.GetNodeValue("tid");
        String token = loginResponse.ResponseValidator.GetNodeValue("data.token");
        CheckoutHelper.addNewAddress(address_map, tid, token);
    }

    public boolean processOrderInDeliveryStageCRM(String orderId, String delivery_status, String rest_id, String ff_status) throws InterruptedException ,IOException {
        boolean success = false;
        String lats[] = cmsHelper.getRestaurantDetails(rest_id).get("lat_long").toString().split(",");
        String de_id = deliverydatahelper.CreateDE(1);
        deliverydatahelper.loginDEandUpdateLocation(de_id,OrderFlowConstants.app_version,lats[0],lats[1]);
        String orderQuery="select order_id from trips where  order_id="+orderId;
        boolean orderstatusindelivery= deliverydatahelper.polldb(DeliveryConstant.databaseName,orderQuery,"order_id",orderId,2,30);
        //Assign order
        Boolean defreestatus = deliveryServiceHelper.makeDEFree(de_id, "1.9");
        Processor  processor = deliveryServiceHelper.assignOrder(orderId, de_id);
        String regex = "\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}";
        Boolean assignmentStatus=deliverydatahelper.checkAssignmentStatus(processor,orderId,de_id);
        // take it toStatus
        try {
            String[] rules = deliveryServiceHelper.getRulesForExecution("unplaced" + ":" + delivery_status);
            for (String rule : rules) {
                String query=null;
                boolean orderstatus=false;
                System.out.println("--status--" + rule);
                Thread.sleep(5000);
                switch (rule) {
                    case "updateaction":
                        Assert.assertTrue(omsHelper.verifyOrders(orderId, "001",OrderFlowConstants.E2EUSER,OrderFlowConstants.E2EPASSWORD));
                        if(ff_status.equalsIgnoreCase("placed"))
                            omsHelper.changeOrderStatusToPlacedPartner(orderId);
                        break;
                    case OrderStatus.ASSIGNED:
                        Thread.sleep(5000);
                        if (null == de_id) {
                            //assignOrder(orderId);
                            Assert.assertTrue(deliveryServiceHelper.assignOrder(orderId), "order is not assigned to de");
                            query = "select assigned_time from trips where order_id="+ orderId;
                            orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "assigned_time", regex, 2, 20);
                        } else {
                            deliveryServiceHelper.assignOrder(orderId, de_id).ResponseValidator.GetResponseCode();
                        }
                        break;
                    case OrderStatus.CONFIRMED:
                        Thread.sleep(5000);
                        //zipDialConfirm(orderId);
                        Assert.assertTrue(deliveryServiceHelper.zipDialConfirm(orderId), "order is not confirmed");
                         query = DeliveryConstant.zipconfirmquery + orderId;
                        orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "confirmed_time", regex, 2, 20);
                        Assert.assertEquals(orderstatus,true,"Order not marked confirmed");
                        break;
                    case OrderStatus.ARRIVED:
                        Thread.sleep(5000);
                        //zipDialArrived(orderId);
                        Assert.assertTrue(deliveryServiceHelper.zipDialArrived(orderId), "order is not in arrived state");
                         query = DeliveryConstant.ziparrivequery + orderId;
                        orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "arrived_time", regex, 2, 20);
                        Assert.assertEquals(orderstatus,true,"Order not marked Arrived");
                        break;
                    case OrderStatus.PICKEDUP:
                        Thread.sleep(5000);
                        //zipDialPickedUp(orderId);
                        Assert.assertTrue(deliveryServiceHelper.zipDialPickedUp(orderId), "order is not in pickedup state");
                         query = DeliveryConstant.zippickedupquery + orderId;
                        orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "pickedup_time", regex, 2, 20);
                        Assert.assertEquals(orderstatus,true,"Order not marked picked up");
                        break;
                    case OrderStatus.REACHED:
                        Thread.sleep(5000);
                        //zipDialReached(orderId);
                        Assert.assertTrue(deliveryServiceHelper.zipDialReached(orderId), "order is not in reached state");
                         query = DeliveryConstant.zipreachedquery + orderId;
                        orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "reached_time", regex, 2, 20);
                        Assert.assertEquals(orderstatus,true,"Order not marked reached");
                        break;
                    case OrderStatus.DELIVERED:
                        Thread.sleep(5000);
                        //zipDialDelivered(orderId);
                        Assert.assertTrue(deliveryServiceHelper.zipDialDelivered(orderId), "order is not in delivered state");
                         query = DeliveryConstant.zipdeliveredquery + orderId;
                        orderstatus = deliverydatahelper.polldbWithRegex(DeliveryConstant.databaseName, query, "delivered_time", regex, 2, 20);
                        Assert.assertEquals(orderstatus,true,"Order not marked delivered");
                        break;
                    default:
                        break;
                }
            }
            success = true;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

}
