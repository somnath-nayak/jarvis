package com.swiggy.api.order_flow.constants;

import java.time.Instant;

/**
 * Created by kiran.j on 7/9/18.
 */
public interface OrderFlowConstants {
    String address_name = "automation" + String.valueOf(Instant.now().getEpochSecond());
    String address_mobile = "9000000000";
    String address = "199, Near Sai Baba Temple";
    String address_landmark = "Swiggy office Land Mark";
    String address_area = "Swiggy Test";
    String address_flatno = "4321";
    String address_city = "Bangalore";
    String address_annotation = "OTHER";
    String app_version = "1.9";
    String E2EUSER = "shashank";
    String E2EPASSWORD = "Shash@ank1";
}
