package com.swiggy.api.castleblack;

public class CastleBlackManager {
    CastleBlackHelper castleBlackHelper =new CastleBlackHelper();
    public static boolean isEnabled= System.getenv("castle") != null;

    int status;

    public void insideCastleBlack(String runId,String pageName, String endPoint, String rc, String desc)
    {
        if(isEnabled)
        {
            castleBlackHelper.insertRecord(runId,pageName,endPoint,rc,desc).ResponseValidator.GetBodyAsText();
        }
    }

}
