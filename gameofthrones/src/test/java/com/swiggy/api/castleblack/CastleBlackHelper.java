package com.swiggy.api.castleblack;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class CastleBlackHelper {

    //Initialize gameofthrones = new Initialize();
    Initialize gameofthrones =Initializer.getInitializer();

    public Processor insertRecord(String id, String pageName, String endPoint, String rc, String desc) {
        GameOfThronesService service = new GameOfThronesService("castleblack", "insertrecord", gameofthrones);
        String[] payloadparams = {id,pageName,endPoint,rc,desc,service.EnvironmentData.getName().toLowerCase()};
        Processor processor = new Processor(service, requestHeader(), payloadparams);
        return processor;
    }

    public Processor runId() {
        GameOfThronesService service = new GameOfThronesService("castleblack", "getrunid", gameofthrones);
        System.out.println("==============="+service.EnvironmentData.getName().toLowerCase());
        String []queryParams={service.EnvironmentData.getName().toLowerCase()};
        Processor processor = new Processor(service, requestHeader(), null,queryParams);
        System.out.println("status code-->"+processor.ResponseValidator.GetResponseCode());
        return processor;
    }

    public Processor getSummary() {
        GameOfThronesService service = new GameOfThronesService("castleblack", "getsummary", gameofthrones);
        Processor processor = new Processor(service, requestHeader(), null,null);

        return processor;
    }

    public void run(String runId,String pageName, String endPoint,String rc,String desc)
    {
        insertRecord(runId,pageName, endPoint,rc,desc);
    }

    public HashMap<String, String> requestHeader()
    {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        return requestheaders;
    }



}
