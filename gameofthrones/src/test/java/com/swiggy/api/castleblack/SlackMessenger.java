package com.swiggy.api.castleblack;

import com.github.seratch.jslack.Slack;
import com.github.seratch.jslack.api.webhook.Payload;
import com.github.seratch.jslack.api.webhook.WebhookResponse;
import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.SlackUser;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;

import java.io.IOException;

public class SlackMessenger {
    private static final SlackSession session = SlackSessionFactory.
            createWebSocketSlackSession("xoxb-284245994305-UVJB7UrW5YLHmT9NlNxyNWdt");
    private static String Webhook ="https://hooks.slack.com/services/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";
    private static boolean isSlackEnabled = System.getenv("slackenabled") == null ? true : true;

    private static String[] slackEmailList = {
            "jitender.kumar@swiggy.com",
            "manu.chadha@swiggy.com"
    };
    public static void connect(){
        if(isSlackEnabled && !session.isConnected()){
            try {
                session.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void disconnect(){
        if(isSlackEnabled ){
            try {
                session.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void send( String slackchannel, String message, String tagemail){
        if(isSlackEnabled && session.isConnected()){
            if(tagemail != null && !tagemail.isEmpty()){
                SlackUser user = session.findUserByEmail(tagemail);
                if(user != null){
                    send(slackchannel, "<@" + user.getId() + "> " + message);
                }
            }else{
                send(slackchannel, message);
            }
        }
    }

    public static void send( String slackchannel, String message, int componentId){
        if(isSlackEnabled && session.isConnected()){
            send(slackchannel, message);
        }
    }

    public static void send( String slackchannel, String message){
        if(isSlackEnabled && session.isConnected()){
            SlackChannel channel = session.findChannelByName(slackchannel);
            session.sendMessage(channel, message);
        }
    }

    public static void webHook(String message,int responseCode) throws IOException {
         if(responseCode!=200) {
             System.out.println("------->"+responseCode);
             Payload payload = Payload.builder()
                     .channel("#castleblack")
                     .username("Lord Commander's Order")
                     .text(message)
                     .build();
             Slack slack = Slack.getInstance();
             WebhookResponse response = slack.send(Webhook, payload);
         }
    }

    public static void webHook1(String message, String responseCode) throws IOException {
        if(responseCode.equals("200")) {
            System.out.println("------->");
            Payload payload = Payload.builder()
                    .channel("#castleblack")
                    .username("Lord Commander's Order")
                    .text(message)
                    .build();
            Slack slack = Slack.getInstance();
            WebhookResponse response = slack.send(Webhook, payload);
        }
    }

    public static void webHookSlack(String message, String channel) throws IOException {

        Payload payload = Payload.builder()
                .channel(channel)
                .username("Test restaurants in production")
                .text(message)
                .build();
        Slack slack = Slack.getInstance();
        WebhookResponse response = slack.send(Webhook, payload);
    }
}






