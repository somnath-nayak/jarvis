package com.swiggy.api.castleblack;

public class Run {
    private static Run single_instance = null;
    CastleBlackHelper castleBlackHelper= new CastleBlackHelper();
    CastleBlackManager castleBlackManager= new CastleBlackManager();
    public String s;

    private Run()
    {
        s = String.valueOf(castleBlackHelper.runId().ResponseValidator.GetNodeValueAsInt("data.key"));
    }
    public static Run getInstance()
    {
        if(CastleBlackManager.isEnabled) {
            if (single_instance == null)
                single_instance = new Run();
        }
            return single_instance;
    }

}
