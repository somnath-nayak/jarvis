package com.swiggy.api.daily.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Map;


public class CartPreOrderHelper{

    private static Initialize gameofthrones = new Initialize();
    DailyCheckoutConstants dailyCheckoutConstants;



    public void validatePreorderApiResStatusData1(String response) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode, dailyCheckoutConstants.VALID_STATUS_CODE);
        String getErrorCode = JsonPath.read(response, "$.data.errorCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getErrorCode, dailyCheckoutConstants.VALID_STATUS_CODE);
        String getNonCheckoutReason = JsonPath.read(response, "$.data.nonCheckoutReason").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getNonCheckoutReason, dailyCheckoutConstants.NONCHECKOUT_REASON);


        String isSuccessful = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(isSuccessful,dailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE);
        String mealCart = JsonPath.read(response, "$.data.cartItems[0].item.type").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(mealCart,dailyCheckoutConstants.ITEM_TYPE );
        String cartSubtype = JsonPath.read(response, "$.data.dailySubType").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(cartSubtype,dailyCheckoutConstants.CART_SUBTYPE );
    }



    public Processor cartUpdate(String payload, Map<String, String> requestHeaders)
    {
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "updatedailycart", gameofthrones);
        return new Processor(service, (HashMap<String, String>) requestHeaders, new String[]{payload});
    }

    public void validatePreorderApiResStatusData(Processor cartResponse)

    {
        String cartID = cartResponse.ResponseValidator.GetNodeValue("$.data.cartId");
        Assert.assertNotNull(cartID);


    }

    public Processor cartPLUpdate(String payload, Map<String, String> requestHeaders)
    {
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "cartplupdate", gameofthrones);
        return new Processor(service, (HashMap<String, String>) requestHeaders, new String[]{payload});
    }







    public Processor cartPlUpdate(String payload, HashMap<String, String> requestHeaders)
    {
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "cartplupdate", gameofthrones);
        return new Processor(service, requestHeaders, new String[]{payload});
    }


}
