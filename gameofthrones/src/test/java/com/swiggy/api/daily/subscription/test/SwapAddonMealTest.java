package com.swiggy.api.daily.subscription.test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.CreateSubscriptionPojo;
import com.swiggy.api.daily.subscription.pojo.ServerTimeChangePojo;
import com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo.SwapAddonMealPojo;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

public class SwapAddonMealTest extends SubscriptionDp {
    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
    CreateSubscriptionPojo createSubscriptionPojo=new CreateSubscriptionPojo();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    SwapAddonMealPojo swapAddonMealPojo=new SwapAddonMealPojo();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }

    //----------------------------------------------------------Swap & Addon Meal-------------------------------------------------------------------

    @Test(dataProvider = "swapMealSubscriptionDP", description = "Swap the first meal of subscription", groups = {"Sanity_Test", "Regression"})
    public void swapMealSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor, "Positive", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

    }

    @Test(dataProvider = "addonMealSubscriptionDP", description = "Add addon with first meal of subscription", groups = {"Sanity_Test", "Regression"})
    public void addonMealSubscriptionTest(String addonMealPayload) {
        responseSubscriptionId = JsonPath.read(addonMealPayload, "$.orders[0].order_id");
        Processor addonProcessor = subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
        String subscriptionMealId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(addonProcessor, "Positive", subscriptionMealId, subscriptionNewMealJobId, "ADDON", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

    }

    @Test(dataProvider="swapMealSubscriptionAfterFewMealDeliveredDP",description ="Swap the fourth meal after three meal delivered of subscription",groups={"Regression"})
    public void swapMealSubscriptionAfterFewMealDeliveredTest(String swapMealPayload) throws ParseException, IOException {
        try {
            responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
            String futureDate= DateUtility.getDateInReadableFormat(DateUtility.getCurrentDateInReadableFormat(),3);
            subscriptionTestHelper.changeServerTime(futureDate);
            Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
            String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
            String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
            subscriptionTestHelper.validateSwapResponse(swapProcessor,"Positive",subscriptionMealId,subscriptionNewMealJobId,"SWAP",SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="addonMealSubscriptionAfterFewMealDeliveredDP",description ="Add addon with fourth meal after three meal delivered of subscription",groups={"Regression"})
    public void addonMealSubscriptionAfterFewMealDeliveredTest(String addonMealPayload) throws IOException, ParseException {
        try {
            responseSubscriptionId = JsonPath.read(addonMealPayload, "$.orders[0].order_id");
            String futureDate = DateUtility.getDateInReadableFormat(DateUtility.getCurrentDateInReadableFormat(), 3);
            subscriptionTestHelper.changeServerTime(futureDate);
            Processor addonProcessor = subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
            String subscriptionMealId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
            String subscriptionNewMealJobId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].order_job_id");
            subscriptionTestHelper.validateSwapResponse(addonProcessor,"Positive",subscriptionMealId,subscriptionNewMealJobId,"ADDON",SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="swapMealSubscriptionDP",description ="Swap the meal in postcutoff time",groups={"Regression"})
    public void swapMealPostCutoffTimeTest(String swapMealPayload) throws IOException {
        try {
            responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
            String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
            String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
            subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative",subscriptionMealId,subscriptionNewMealJobId,"SWAP",SubscriptionConstant.SWAP_UNSUCCESSPOSTCUTOFF_MSG);

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="addonMealSubscriptionDP",description ="Add addon with meal in postcutoff time",groups={"Regression"})
    public void addAddonPostCutoffTimeTest(String addonMealPayload) throws IOException {
        try {
            responseSubscriptionId = JsonPath.read(addonMealPayload, "$.orders[0].order_id");
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
            String subscriptionMealId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
            String subscriptionNewMealJobId = JsonPath.read(addonMealPayload, "$.orders[0].order_jobs[0].order_job_id");
            subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative",subscriptionMealId,subscriptionNewMealJobId,"ADDON",SubscriptionConstant.SWAP_UNSUCCESSPOSTCUTOFF_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="swapMealSubscriptionDP",description ="Swap the meal which is already swapped",groups={"Regression"})
    public void swapMealAlreadySwappedTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        Processor swapProcessor=subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId= JsonPath.read(swapMealPayload,"$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId= JsonPath.read(swapMealPayload,"$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Positive",subscriptionMealId,subscriptionNewMealJobId,"SWAP",SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        Processor swapSecondProcessor=subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        subscriptionTestHelper.validateSwapResponse(swapSecondProcessor,"Negative",subscriptionMealId,subscriptionNewMealJobId,"SWAP",SubscriptionConstant.SWAP_UNSUCCESSALREADYSWAPPED_MSG);
    }

    @Test(dataProvider="addonMealSubscriptionDP",description ="add addons with meal which is already edited",groups={"Regression"})
    public void addAddonWithMealAlreadyEditedTest(String addonMealPayload) {
        responseSubscriptionId = JsonPath.read(addonMealPayload, "$.orders[0].order_id");
        Processor swapProcessor=subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
        String subscriptionMealId= JsonPath.read(addonMealPayload,"$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId= JsonPath.read(addonMealPayload,"$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Positive",subscriptionMealId,subscriptionNewMealJobId,"ADDON",SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        Processor swapSecondProcessor=subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
        subscriptionTestHelper.validateSwapResponse(swapSecondProcessor,"Negative",subscriptionMealId,subscriptionNewMealJobId,"ADDON",SubscriptionConstant.SWAP_UNSUCCESSALREADYSWAPPED_MSG);
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="swap the meal Which subscription already completed",groups={"Regression"})
    public void swapMealAlreadyCompletedSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "COMPLETED");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");

        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SWAP_UNSUCCESSNOTACTIVE_MSG);
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="swap the meal Which subscription already Cancelled",groups={"Regression"})
    public void swapMealAlreadyCancelledSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "CANCELLED");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");

        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SWAP_UNSUCCESSNOTACTIVE_MSG);
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="swap the meal Which subscription meal already Skipped",groups={"Regression"})
    public void swapMealAlreadySkippedSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        subscriptionHelper.updateSubscriptionMealStatus(subscriptionMealId, "SKIPPED");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");

        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SWAP_UNSUCCESSALREADYSWAPPED_MSG);
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="swap the meal Which subscription already Paused",groups={"Regression"})
    public void swapMealAlreadyPausedSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "PAUSED");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Negative", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SWAP_UNSUCCESSNOTACTIVE_MSG);
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="skip the meal Which subscription already swapped",groups={"Regression"})
    public void skipMealAlreadySwappedSubscriptionTest(String swapMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Positive", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        int statusCode=skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode,0,"Able to Skip the subscription meal");
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="pause the subscription Which subscription meal already swapped, precutoff time",groups={"Regression"})
    public void pauseSubscriptionAlreadySwappedSubscriptionPrecutoffTest(String swapMealPayload) throws ParseException {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor,"Positive", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        Processor pauseProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId,DateUtility.getCurrentDateInMilisecond(),DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getFutureDateInReadableFormat(6)));
        int statusCode=pauseProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode,0,"Able to pause the subscription");
    }

    @Test(dataProvider="swapMealSubscriptionDP",description="pause the subscription Which subscription meal already swapped postcutoff time",groups={"Regression"})
    public void pauseSubscriptionAlreadySwappedSubscriptionPostcutoffTest(String swapMealPayload) throws ParseException, IOException {
        try {
            responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
            Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
            String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
            String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
            subscriptionTestHelper.validateSwapResponse(swapProcessor, "Positive", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.getCurrentDateInMilisecond(), DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getFutureDateInReadableFormat(6)));
            int statusCode = pauseProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(statusCode, 1, "Not able to pause the subscription");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="addAddonAlreadySwappedMealSubscriptionDP",description="Add the addon where subscription meal already swapped",groups={"Regression"})
    public void addAddonAlreadySwappedMealSubscriptionTest(String swapMealPayload,String addonMealPayload) {
        responseSubscriptionId = JsonPath.read(swapMealPayload, "$.orders[0].order_id");
        Processor swapProcessor = subscriptionHelper.createSubscriptionWrapper(swapMealPayload);
        String subscriptionMealId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].metadata.subscriptionMealId");
        String subscriptionNewMealJobId = JsonPath.read(swapMealPayload, "$.orders[0].order_jobs[0].order_job_id");
        subscriptionTestHelper.validateSwapResponse(swapProcessor, "Positive", subscriptionMealId, subscriptionNewMealJobId, "SWAP", SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        Processor swapSecondProcessor = subscriptionHelper.createSubscriptionWrapper(addonMealPayload);
        subscriptionTestHelper.validateSwapResponse(swapSecondProcessor, "Negative", subscriptionMealId, subscriptionNewMealJobId, "ADDON", SubscriptionConstant.SWAP_UNSUCCESSALREADYSWAPPED_MSG);
    }

    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }

}
