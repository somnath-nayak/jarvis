package com.swiggy.api.daily.promotions.helper;

import io.gatling.commons.stats.assertion.In;
import scala.Int;

public interface PromotionConstants {
    
    String tenant = "DAILY";
    String actionType[] = new String[]{"FLAT_DISCOUNT", "PERCENTAGE_DISCOUNT"};
    String createdBy = "Ankita-Automation";
    String updatedBy = "Ankita-Automation";
    String duplicatePromotion = "Conflicting Promotions: [";
    String promotionType[] =new String[]{"DISCOUNT"};
    //DO not touch SKU id
    String NoPromotionSKU = "SKU9999_TEST_DONOT_TOUCH";
    String discountValueForFlat = "200";
    String discountValueForPercent = "10";
    Integer itemPrice = 1000;
    String givenDiscountForPercent = "900.0";
    String givenDiscountForFlat = "800.0";
    String zeroDiscount = "0.0";
    String [] shareType = new String[]{"PERCENTAGE"};
    String shareValue = "50.0";
    String invalidDiscont = "Discount value is invalid";
    
}