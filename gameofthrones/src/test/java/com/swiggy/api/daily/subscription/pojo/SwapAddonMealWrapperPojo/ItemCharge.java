
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "price",
    "tax",
    "inclusiveTax"
})
public class ItemCharge {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Double total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("tax")
    private Tax tax;
    @JsonProperty("inclusiveTax")
    private Boolean inclusiveTax;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public ItemCharge withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    public ItemCharge withTotal(Double total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public ItemCharge withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public ItemCharge withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @JsonProperty("tax")
    public Tax getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public ItemCharge withTax(Tax tax) {
        this.tax = tax;
        return this;
    }

    @JsonProperty("inclusiveTax")
    public Boolean getInclusiveTax() {
        return inclusiveTax;
    }

    @JsonProperty("inclusiveTax")
    public void setInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public ItemCharge withInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
        return this;
    }

    public ItemCharge setDefaultData() {
        return this.withTotalWithoutDiscount(SubscriptionConstant.ITEMCHARGE_TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.ITEMCHARGE_TOTAL)
                .withDiscount(SubscriptionConstant.ITEMCHARGE_DISCOUNT)
                .withPrice(SubscriptionConstant.ITEMCHARGE_PRICE)
                .withTax(new Tax().setDefaultData())
                .withInclusiveTax(SubscriptionConstant.INCLUSIVETAX);
    }

}
