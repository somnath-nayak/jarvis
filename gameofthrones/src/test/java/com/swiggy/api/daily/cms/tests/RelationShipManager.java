package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.pojo.Relationship.Destination;
import com.swiggy.api.daily.cms.pojo.Relationship.Relationship_data;
import com.swiggy.api.daily.cms.pojo.Relationship.Source;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.JonSnow.Validator;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

public class RelationShipManager {
    CMSDailyHelper helper = new CMSDailyHelper();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    String id;
    String sourcestoreid;
    String sourcepid;
    String deststoreid;
    String destspin;
    List li=new ArrayList();

    @Test(description = "This test will create Relation based on given source and desg,retype and relationship data", dataProviderClass = CMSDailyDP.class, dataProvider = "createrelation", priority = 0)
    public void createRelationShip(String sstoreid,String spid,String dstoreid,String dspinid,String retype) throws Exception {
        String res = helper.craeteRelationShip(spid, sstoreid, dstoreid, dspinid,retype).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(schemaValidatorUtils.validateSchema("/../Data/SchemaSet/Json/CMS/createrelation.txt",res));
        id = JsonPath.read(res, "$.create_update_relations..id").toString().replace("[", "").replace("]", "").replace("\"","");
        sourcestoreid = JsonPath.read(res, "$.create_update_relations..source.store_id").toString().replace("[", "").replace("]", "").replace("\"","");
        sourcepid = JsonPath.read(res, "$.create_update_relations..source.product_id").toString().replace("[", "").replace("]", "").replace("\"","");
        deststoreid = JsonPath.read(res, "$.create_update_relations..destination.store_id").toString().replace("[", "").replace("]", "").replace("\"","");
        destspin = JsonPath.read(res, "$.create_update_relations..destination.spin").toString().replace("[", "").replace("]", "").replace("\"","");
        Assert.assertNotNull(id);
        Assert.assertEquals(destspin,dspinid);
        li.add(id);
    }

    @Test(description = "This test will search relationship based page,row per page and search filter",priority = 1,dataProviderClass = CMSDailyDP.class,dataProvider = "searchrelation")
    public void searchRelationShip(Integer page, Integer row_per_page,String reltype,String operater) throws Exception{
        String res=helper.searchRelationShip(page,row_per_page,sourcestoreid,sourcepid,deststoreid,destspin,reltype,operater).ResponseValidator.GetBodyAsText();
        String idn= JsonPath.read(res,"$.data..id").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(idn,id);
    }

    @Test(description = "This test will update source ,desg,retype and relationship data based on given id", dataProviderClass = CMSDailyDP.class, dataProvider = "updaterelation", priority = 2)
    public void updateRelationShip(String sstoreid,String spid,String dstoreid,String dspinid,String retype) throws Exception {
        String res = helper.updateRelationShip(sstoreid, spid, dstoreid, dspinid, retype,li.get(0).toString()).ResponseValidator.GetBodyAsText();
        id = JsonPath.read(res, "$.create_update_relations..id").toString().replace("[", "").replace("]", "").replace("\"","");
        Assert.assertNotNull(id);

    }



    @Test(description = "This test will delete bulk relationship based on source ,desg,retype and relationship data", priority = 3)
    public void deleteRelationShipBulk() throws Exception {
        String res = helper.deleteRelationShipBulk(sourcestoreid,sourcepid,deststoreid,destspin, CMSDailyContants.rel_type.get(0).toString(),li.get(1).toString()).ResponseValidator.GetBodyAsText();
        //Assert.assertTrue(schemaValidatorUtils.validateSchema("/../Data/SchemaSet/Json/CMS/createrelation.txt",res));
        id = JsonPath.read(res, "$.delete_relations..id").toString().replace("[", "").replace("]", "").replace("\"","");
        Assert.assertNotNull(id);
        Assert.assertEquals(id,li.get(1).toString());
    }


    @Test(description = "This test will fetch Relation based on given id", priority = 4)
    public void getRelationShipByID() throws Exception {
        Validator va = helper.getRelationShip(li.get(0).toString()).ResponseValidator;
        int statuscode=va.GetResponseCode();
        String res=va.GetBodyAsText();
//      Assert.assertTrue(schemaValidatorUtils.validateSchema("/../Data/SchemaSet/Json/CMS/getrelationship.txt",res));
        String idn = JsonPath.read(res, "$.id").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(idn, li.get(0).toString());
        Assert.assertEquals(statuscode,200);
    }


    @Test(description = "This test will delete relationship based on id", priority = 5)
    public void deleteRelationShipByID() throws Exception {
        Validator va = helper.deleteRelationShipByID(li.get(0).toString()).ResponseValidator;
        int statucode=va.GetResponseCode();
        Assert.assertEquals(statucode,200);
    }



    @Test(description = "This test will verify available relationship type ", priority = 4)
    public void getAllRelationshiptype() throws Exception {
        String respose = helper.getallRelationShiptype().ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(respose);
        Assert.assertTrue(respose.contains(CMSDailyContants.rel_type.get(0).toString()));
    }



    @Test(description = "This test will verify negative cases for Relation creation", dataProviderClass = CMSDailyDP.class, dataProvider = "createrelationnegative", priority = 6)
    public void createRelationShipNegative(Source sr, Destination ds, Relationship_data data, String retype) throws Exception {
        int rescode = helper.craeteRelationShipNegative(sr,ds,data,retype).ResponseValidator.GetResponseCode();
        Assert.assertEquals(rescode,412);
    }

    @Test(description = "This test will verify negative cases for Relation updation", dataProviderClass = CMSDailyDP.class, dataProvider = "createrelationnegative", priority = 6)
    public void updateRelationShipNegative(Source sr, Destination ds, Relationship_data data, String retype) throws Exception {
        int rescode = helper.updateRelationShipNegative(sr,ds,data,retype,id).ResponseValidator.GetResponseCode();
        Assert.assertEquals(rescode,412);
    }

    @Test(description = "This test will verify negative cases for bulk Relation deletion", dataProviderClass = CMSDailyDP.class, dataProvider = "deleterelationnegative", priority = 6)
    public void deleteRelationShipNegativeBulk(Source sr, Destination ds, String retype) throws Exception {
        int rescode = helper.deleteRelationShipNegative(sr,ds,retype,id).ResponseValidator.GetResponseCode();
        Assert.assertEquals(rescode,412);
    }
    @Test(description = "This test will verify negative cases for Relation deletion by ID", priority = 6,dataProviderClass = CMSDailyDP.class,dataProvider ="getdeleterelationnegative")
    public void deleteRelationShipNegative(String id1) throws Exception {
        Validator va = helper.deleteRelationShipByID(id1).ResponseValidator;
        int statucode=va.GetResponseCode();
        Assert.assertEquals(statucode,204);
    }

    @Test(description = "This test will verify negative cases for Relation fetch by ID", priority = 6,dataProviderClass = CMSDailyDP.class,dataProvider ="getdeleterelationnegative" )
    public void getRelationShipNegative(String id2) throws Exception {
        Validator va = helper.getRelationShip(id2).ResponseValidator;
        int statucode=va.GetResponseCode();
        Assert.assertEquals(statucode,204);
    }

}