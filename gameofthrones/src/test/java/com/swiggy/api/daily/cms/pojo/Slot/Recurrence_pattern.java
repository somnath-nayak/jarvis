
package com.swiggy.api.daily.cms.pojo.Slot;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "frequency",
    "rRule"
})
public class Recurrence_pattern {

    @JsonProperty("frequency")
    private String frequency;
    @JsonProperty("rRule")
    private RRule rRule;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Recurrence_pattern() {
    }

    /**
     * 
     * @param rRule
     * @param frequency
     */
    public Recurrence_pattern(String frequency, RRule rRule) {
        super();
        this.frequency = frequency;
        this.rRule = rRule;
    }

    @JsonProperty("frequency")
    public String getFrequency() {
        return frequency;
    }

    @JsonProperty("frequency")
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public Recurrence_pattern withFrequency(String frequency) {
        this.frequency = frequency;
        return this;
    }

    @JsonProperty("rRule")
    public RRule getRRule() {
        return rRule;
    }

    @JsonProperty("rRule")
    public void setRRule(RRule rRule) {
        this.rRule = rRule;
    }

    public Recurrence_pattern withRRule(RRule rRule) {
        this.rRule = rRule;
        return this;
    }


}
