package com.swiggy.api.daily.vendor.helper;

import com.rabbitmq.client.AMQP;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import jdk.nashorn.internal.parser.JSONParser;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.FileReader;
import java.util.logging.Logger;

public class TestcaseHelper {

        RabbitMQHelper rabbitMQHelper = new RabbitMQHelper();

        public static final Logger logger = Logger.getGlobal();
        VendorCommonHelper vendorCommonHelper = new VendorCommonHelper();

        public void loginSuccessful(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String statusMessage = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertNotNull(statusMessage);
                String rest_id = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
                softAssert.assertNotNull(rest_id);
                String rest_name = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_name"));
                softAssert.assertNotNull(rest_name);
                String area_id = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].area_id"));
                softAssert.assertNotNull(area_id);
                String area_name = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].area_name"));
                softAssert.assertNotNull(area_name);
                String city_id = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].city_id"));
                softAssert.assertNotNull(city_id);
                String city_name = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].city_name"));
                softAssert.assertNotNull(city_name);
                String locality = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].locality"));
                softAssert.assertNotNull(locality);
                String assured = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rating"));
                softAssert.assertNotNull(assured);
                String restaurantId = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].restaurantId"));
                softAssert.assertNotNull(restaurantId);
                String attribute = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].attribute"));
                softAssert.assertNotNull(attribute);
                String value = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].value"));
                softAssert.assertNotNull(value);
                String mobile = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.mobile"));
                softAssert.assertNotNull(mobile);
                String rid = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rid"));
                softAssert.assertNotNull(rid);
                String name = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.name"));
                softAssert.assertNotNull(name);
                String city = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city"));
                softAssert.assertNotNull(city);
                String access_token = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
                softAssert.assertNotNull(access_token);
                String permissions = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.permissions"));
                softAssert.assertNotNull(permissions);
                String latLong = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong"));
                softAssert.assertNotNull(latLong);
                String lat = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong.lat"));
                softAssert.assertNotNull(lat);
                String long1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong.long"));
                softAssert.assertNotNull(long1);
        }

        public void getRestaurantAvailabilityDetails(Processor restaurantAvailability) {
                SoftAssert softAssert = new SoftAssert();
                String code = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
                softAssert.assertNotNull(code);
                String message = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.message"));
                softAssert.assertNotNull(message);
                String timeline1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeline"));
                softAssert.assertNotNull(timeline1);
                String size_in_days = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeline.size_in_days"));
                softAssert.assertNotNull(size_in_days);
                softAssert.assertAll();
        }

        public void loginInvalidCredentails(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String statusMessage = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, VendorConstant.statusMessage_Invalid_Input);
                softAssert.assertAll();
        }

        public void loginValidUsernameInvalidPassword(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String statusMessage = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, VendorConstant.statusMessage_Invalid_Password);
                softAssert.assertAll();
        }

        public void loginTest1(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String id = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].id"));
                softAssert.assertNotNull(id);
                String restaurantId = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].restaurantId"));
                softAssert.assertNotNull(restaurantId);
                String attribute = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].attribute"));
                softAssert.assertNotNull(attribute);
                String value = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[0].value"));
                softAssert.assertNotNull(value);
                String ID = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.ID"));
                softAssert.assertNotNull(ID);
                String id1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[1].id"));
                softAssert.assertNotNull(id1);
                String restaurantId1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[1].restaurantId"));
                softAssert.assertNotNull(restaurantId1);
                String attribute1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[1].attribute"));
                softAssert.assertNotNull(attribute1);
                String value1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].restMetadata[1].value"));
                softAssert.assertNotNull(value1);
                String ID1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.ID"));
                softAssert.assertNotNull(ID1);
                softAssert.assertAll();
        }

        public void loginTest2(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String latLong = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong"));
                softAssert.assertNotNull(latLong);
                String lat = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong.lat"));
                softAssert.assertNotNull(lat);
                String long1 = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].latLong.long"));
                softAssert.assertNotNull(long1);
                softAssert.assertAll();
        }

        public void loginTest3(Processor login) {
                SoftAssert softAssert = new SoftAssert();
                String userType = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.userType"));
                softAssert.assertEquals(userType, VendorConstant.daily);
                softAssert.assertAll();
        }

        public void invalidRestaurant(Processor restaurantAvailability) {
                SoftAssert softAssert = new SoftAssert();
                String status = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
                softAssert.assertEquals(status, VendorConstant.random_0);
                String code = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
                softAssert.assertEquals(code, VendorConstant.code_failure);
                String message = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.message"));
                softAssert.assertEquals(message, VendorConstant.message_invalidRestID);
                softAssert.assertAll();
        }

        public void startTimeAndEndTime(Processor restaurantAvailability) {
                SoftAssert softAssert = new SoftAssert();
                String size_in_days = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeline.size_in_days"));
                softAssert.assertNotNull(size_in_days);
                String open_close_epoch_millis = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeline.open_close_epoch_millis"));
                softAssert.assertNotNull(open_close_epoch_millis);
                String year = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].year"));
                softAssert.assertNotNull(year);
                String month = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].month"));
                softAssert.assertNotNull(month);
                String dayOfMonth = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].dayOfMonth"));
                softAssert.assertNotNull(dayOfMonth);
                String dayOfWeek = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].dayOfWeek"));
                softAssert.assertNotNull(dayOfWeek);
                String dayOfYear = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].dayOfYear"));
                softAssert.assertNotNull(dayOfYear);
                String nano = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].nano"));
                softAssert.assertNotNull(nano);
                String hour = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].hour"));
                softAssert.assertNotNull(hour);
                String minute = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].minute"));
                softAssert.assertNotNull(minute);
                String second = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].second"));
                softAssert.assertNotNull(second);
                String monthValue = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].monthValue"));
                softAssert.assertNotNull(monthValue);
                String chronology = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].chronology"));
                softAssert.assertNotNull(chronology);
                String calendarType = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].chronology.calendarType"));
                softAssert.assertNotNull(calendarType);
                String id1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[0].chronology.id"));
                softAssert.assertNotNull(id1);
                String year1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].year"));
                softAssert.assertNotNull(year1);
                String month1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].month"));
                softAssert.assertNotNull(month1);
                String dayOfMonth1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].dayOfMonth"));
                softAssert.assertNotNull(dayOfMonth1);
                String dayOfWeek1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].dayOfWeek"));
                softAssert.assertNotNull(dayOfWeek1);
                String dayOfYear1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].dayOfYear"));
                softAssert.assertNotNull(dayOfYear1);
                String nano1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].nano"));
                softAssert.assertNotNull(nano1);
                String hour1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].hour"));
                softAssert.assertNotNull(hour1);
                String minute1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].minute"));
                softAssert.assertNotNull(minute1);
                String second1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].second"));
                softAssert.assertNotNull(second1);
                String monthValue1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].monthValue"));
                softAssert.assertNotNull(monthValue1);
                String chronology1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].chronology"));
                softAssert.assertNotNull(chronology1);
                String calendarType1 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].chronology.calendarType"));
                softAssert.assertNotNull(calendarType1);
                String id2 = String.valueOf(restaurantAvailability.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meta.timeline[1].chronology.id"));
                softAssert.assertNotNull(id2);
                softAssert.assertAll();
        }

        public void orderDispatchSuccess(Processor orderDispatch) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertNotNull(statusCode);
                String statusMessage = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertNotNull(statusMessage);
                String restaurantData = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.restaurantData"));
                softAssert.assertNotNull(restaurantData);
                String restaurantId = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.restaurantData[0].restaurantId"));
                softAssert.assertNotNull(restaurantId);
                softAssert.assertAll();
        }

        public void orderDispatchInvalidRest(Processor orderDispatch) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, VendorConstant.statusCode_5);
                String statusMessage = String.valueOf(orderDispatch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, "don't have permission for requested restaurants");
                softAssert.assertAll();
        }

        public void slotServicePause(Processor holidaSlot) {
                SoftAssert softAssert = new SoftAssert();
                String entity_id = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
                softAssert.assertNotNull(entity_id);
                String entity_type = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_type"));
                softAssert.assertEquals(entity_type, "MEAL");
                String store_id = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
                softAssert.assertNotNull(store_id);
                String created_by = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].created_by"));
                softAssert.assertNotNull(created_by);
                String service = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].service"));
                softAssert.assertNotNull(service);
                String meta = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].meta"));
                softAssert.assertNotNull(meta);
                String store_id1 = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].meta.store_id"));
                softAssert.assertEquals(store_id1, store_id);
                String slot_info_list = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list"));
                softAssert.assertNotNull(slot_info_list);
                String id = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].id"));
                softAssert.assertNotNull(id);
                String zone = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].zone"));
                softAssert.assertNotNull(zone);
                String start_time = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
                softAssert.assertNotNull(start_time);
                String end_time = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
                softAssert.assertNotNull(end_time);
                String live_date = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
                softAssert.assertNotNull(live_date);
                String expiry_date = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
                softAssert.assertNotNull(expiry_date);
                String frequency = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].recurrence_pattern.frequency"));
                softAssert.assertNotNull(frequency);
                String rrule = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].recurrence_pattern.rrule"));
                softAssert.assertNotNull(rrule);
                String interval = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].recurrence_pattern.rrule.interval"));
                softAssert.assertNotNull(interval);
                String statusCode = String.valueOf(holidaSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, VendorConstant.random_1);
                softAssert.assertAll();
        }

        public void getPastOrders(Processor pastOrders) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, VendorConstant.random_0);
                String statusMessage = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, "Order History Fetched Successfully");
                String data = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                String orders = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders"));
                softAssert.assertNotNull(orders);
                String status = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].status"));
                softAssert.assertNotNull(status);
                String order_status = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].status.order_status"));
                softAssert.assertNotNull(order_status);
                String ordered_time = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].status.ordered_time"));
                softAssert.assertNotNull(ordered_time);
                String order_id = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].order_id"));
                softAssert.assertNotNull(order_id);
                String bill = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].bill"));
                softAssert.assertNotNull(bill);
                String service_charge = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].service_charge"));
                softAssert.assertNotNull(service_charge);
                String restaurant_trade_discount = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].restaurant_trade_discount"));
                softAssert.assertNotNull(restaurant_trade_discount);
                String spending = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].spending"));
                softAssert.assertNotNull(spending);
                String discount = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].discount"));
                softAssert.assertNotNull(discount);
                String order_type = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].order_type"));
                softAssert.assertNotNull(order_type);
                String cart = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart"));
                softAssert.assertNotNull(cart);
                String charges = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.charges"));
                softAssert.assertNotNull(charges);
                String packing_charge = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.charges.packing_charge"));
                softAssert.assertNotNull(packing_charge);
                String delivery_charge = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.charges.delivery_charge"));
                softAssert.assertNotNull(delivery_charge);
                String items = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items"));
                softAssert.assertNotNull(items);
                String name = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].name"));
                softAssert.assertNotNull(name);
                String item_id = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].item_id"));
                softAssert.assertNotNull(item_id);
                String packing_charges = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].packing_charges"));
                softAssert.assertNotNull(packing_charges);
                String sub_total = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].sub_total"));
                softAssert.assertNotNull(sub_total);
                String total = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].total"));
                softAssert.assertNotNull(total);
                String variants = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].variants"));
                softAssert.assertNotNull(variants);
                String addons = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].addons"));
                softAssert.assertNotNull(addons);
                String quantity = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].cart.items[0].quantity"));
                softAssert.assertNotNull(quantity);
                String GST_details = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details"));
                softAssert.assertNotNull(GST_details);
                String item_sgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.item_sgst"));
                softAssert.assertNotNull(item_sgst);
                String item_cgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.item_cgst"));
                softAssert.assertNotNull(item_cgst);
                String item_igst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.item_igst"));
                softAssert.assertNotNull(item_igst);
                String packing_charge_sgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.packing_charge_sgst"));
                softAssert.assertNotNull(packing_charge_sgst);
                String packing_charge_cgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.packing_charge_cgst"));
                softAssert.assertNotNull(packing_charge_cgst);
                String packing_charge_igst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.packing_charge_igst"));
                softAssert.assertNotNull(packing_charge_igst);
                String service_charge_sgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.service_charge_sgst"));
                softAssert.assertNotNull(service_charge_sgst);
                String service_charge_cgst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.service_charge_cgst"));
                softAssert.assertNotNull(service_charge_cgst);
                String service_charge_igst = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].GST_details.service_charge_igst"));
                softAssert.assertNotNull(service_charge_igst);
                String mou_type = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders[0].mou_type"));
                softAssert.assertNotNull(mou_type);
                softAssert.assertAll();
        }

        public void pastOrdersInvalid(Processor pastOrders) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, VendorConstant.random_0);
                String statusMessage = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, "Order History Fetched Successfully");
                String data = String.valueOf(pastOrders.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                softAssert.assertAll();
        }

        public void pauseForItemSuccessful(Processor pause) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, VendorConstant.random_0);
                String statusMessage = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertEquals(statusMessage, "Done Successfully");
                String data = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                String entity_id = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
                softAssert.assertNotNull(entity_id);
                String entity_type = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_type"));
                softAssert.assertNotNull(entity_type);
                String store_id = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
                softAssert.assertNotNull(store_id);
                String created_by = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].created_by"));
                softAssert.assertNotNull(created_by);
                String service = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].service"));
                softAssert.assertNotNull(service);
                String meta = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta"));
                softAssert.assertNotNull(meta);
                String store_id1 = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.store_id"));
                softAssert.assertNotNull(store_id1);
                String slot_info_list = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list"));
                softAssert.assertNotNull(slot_info_list);
                String id = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].id"));
                softAssert.assertNotNull(id);
                String zone = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].zone"));
                softAssert.assertNotNull(zone);
                String start_time = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].start_time"));
                softAssert.assertNotNull(start_time);
                String end_time = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].end_time"));
                softAssert.assertNotNull(end_time);
                String live_date = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].live_date"));
                softAssert.assertNotNull(live_date);
                String expiry_date = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].expiry_date"));
                softAssert.assertNotNull(expiry_date);
                String recurrence_pattern = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].recurrence_pattern"));
                softAssert.assertNotNull(recurrence_pattern);
                String frequency = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].recurrence_pattern.frequency"));
                softAssert.assertNotNull(frequency);
                String rrule = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].recurrence_pattern.rrule"));
                softAssert.assertNotNull(rrule);
                String interval = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot_info_list[0].recurrence_pattern.rrule.interval"));
                softAssert.assertNotNull(interval);
                softAssert.assertAll();
        }

        public void pauseForItemFailed(Processor pause) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertNotNull(statusCode);
                String statusMessage = String.valueOf(pause.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
                softAssert.assertNotNull(statusMessage);
                softAssert.assertAll();
        }

        public void paginatedSearchSuccess(Processor paginatedSearch) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
                softAssert.assertEquals(statusCode, VendorConstant.random_1);
                String code = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
                softAssert.assertEquals(code, "SUCCESS");
                String message = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.message"));
                softAssert.assertEquals(message, "");
                String data = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                String search_data = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data"));
                softAssert.assertNotNull(search_data);
                String id = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].id"));
                softAssert.assertNotNull(id);
                String uuid = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].uuid"));
                softAssert.assertNotNull(uuid);
                String name = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].name"));
                softAssert.assertNotNull(name);
                String city = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].city"));
                softAssert.assertNotNull(city);
                String createdOn = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].createdOn"));
                softAssert.assertNotNull(createdOn);
                String updatedOn = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].updatedOn"));
                softAssert.assertNotNull(updatedOn);
                String liveDate = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].liveDate"));
                softAssert.assertNotNull(liveDate);
                String cityCode = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].cityCode"));
                softAssert.assertNotNull(cityCode);
                String area = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].area"));
                softAssert.assertNotNull(area);
                String areaCode = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].areaCode"));
                softAssert.assertNotNull(areaCode);
                String avg_rating = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].avg_rating"));
                softAssert.assertNotNull(avg_rating);
                String total_ratings = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].total_ratings"));
                softAssert.assertNotNull(total_ratings);
                String cloudinaryImageId = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].cloudinaryImageId"));
                softAssert.assertNotNull(cloudinaryImageId);
                String cuisine = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].cuisine"));
                softAssert.assertNotNull(cuisine);
                String recommended = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].recommended"));
                softAssert.assertNotNull(recommended);
                String costForTwo = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].costForTwo"));
                softAssert.assertNotNull(costForTwo);
                String type = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].type"));
                softAssert.assertNotNull(type);
                String address = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].address"));
                softAssert.assertNotNull(address);
                String latLong = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].latLong"));
                softAssert.assertNotNull(latLong);
                String locality = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].locality"));
                softAssert.assertNotNull(locality);
                String parentId = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].parentId"));
                softAssert.assertNotNull(parentId);
                String menuId = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].menuId"));
                softAssert.assertNotNull(menuId);
                String phoneNumbers = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].phoneNumbers"));
                softAssert.assertNotNull(phoneNumbers);
                String commissionExp = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].commissionExp"));
                softAssert.assertNotNull(commissionExp);
                String packingCharges = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].packingCharges"));
                softAssert.assertNotNull(packingCharges);
                String serviceTax = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].serviceTax"));
                softAssert.assertNotNull(serviceTax);
                String serviceCharges = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].serviceCharges"));
                softAssert.assertNotNull(serviceCharges);
                String deliveryCharges = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].deliveryCharges"));
                softAssert.assertNotNull(deliveryCharges);
                String vat = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].vat"));
                softAssert.assertNotNull(vat);
                String modelBasedRating = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].modelBasedRating"));
                softAssert.assertNotNull(modelBasedRating);
                String codLimit = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].codLimit"));
                softAssert.assertNotNull(codLimit);
                String orderNotifyEmails = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].orderNotifyEmails"));
                softAssert.assertNotNull(orderNotifyEmails);
                String enableTnC = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].enableTnC"));
                softAssert.assertNotNull(enableTnC);
                String taxationType = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].taxationType"));
                softAssert.assertNotNull(taxationType);
                String tier = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].tier"));
                softAssert.assertNotNull(tier);
                String typeOfPartner = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].typeOfPartner"));
                softAssert.assertNotNull(typeOfPartner);
                softAssert.assertAll();
        }

        public String pushMessageInRabbitMQ() throws Exception{
                logger.info("++++++++++++++++++++======================RABBIT_MQ++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                String message = "{\"order_type\":\"DAILY\",\"order_id\":"+vendorCommonHelper.randomNumber()+",\"banner_factor_diff\":\"null\",\"payment_method\":\"Cash\",\"restaurant_id\":\"9990\",\"waive_off_amount\":0,\"swiggy_discount_hit\":0,\"trade_discount_meta\":[{\"shareValue\":100,\"description\":\"???10 off on all orders\",\"campaignId\":474874,\"shareType\":\"Percentage\",\"minCartAmount\":0,\"dormantUserDays\":0,\"discountType\":\"Flat\",\"header\":\"???10 off\",\"restaurantFirstOrder\":false,\"commissionLevy\":0,\"shortDescription\":\"???10 off on all orders\",\"public\":true}],\"customer_ip\":\"172.31.34.138, 127.0.0.1, 172.31.12.29\",\"billing_lng\":\"76.172577\",\"old_order_items\":[],\"payment_txn_id\":\"\",\"cancellation_fee_collected\":0,\"tax_expressions\":{\"packaging_IGST\":\"[TOTAL_PACKING_CHARGES]*0.0\",\"IGST\":\"[CART_SUBTOTAL_WITHOUT_PACKING]*0.0\",\"Service Tax\":\"0.0\",\"Service Charges\":\"0.0\",\"SGST\":\"[CART_SUBTOTAL_WITHOUT_PACKING]*0.025\",\"packaging_CGST\":\"[TOTAL_PACKING_CHARGES]*0.0\",\"packaging_SGST\":\"[TOTAL_PACKING_CHARGES]*0.0\",\"service_charge_IGST\":\"[SERVICE_CHARGE]*0.0\",\"Vat\":\"0.0\",\"service_charge_SGST\":\"[SERVICE_CHARGE]*0.0\",\"service_charge_CGST\":\"[SERVICE_CHARGE]*0.0\",\"service_charge_GST_inclusive\":false,\"CGST\":\"[CART_SUBTOTAL_WITHOUT_PACKING]*0.025\",\"packaging_GST_inclusive\":false,\"item_GST_inclusive\":false},\"pg_response_time\":\"2019-02-13 18:00:00\",\"cancellation_fee_gst_expression\":\"18\",\"restaurant_city_name\":\"Bangalore\",\"success_message_type\":\"REGULAR\",\"prep_time_pred\":\"24.74\",\"coupon_applied\":\"\",\"freebie_discount_hit\":0,\"GST_on_commission\":{\"SGST\":1.767150090819598,\"CGST\":1.767150090819598,\"GST\":3.53},\"with_de\":false,\"order_subscriptions\":[],\"order_notes\":\"test\",\"order_time\":\"2019-02-13 18:00:00\",\"order_tags\":[],\"cust_lat_lng\":{\"lat\":\"27.837445\",\"lng\":\"76.172577\"},\"success_message_info\":\"\",\"mCancellationTime\":0,\"rain_mode\":\"0\",\"trade_discount\":10,\"restaurant_commission_exp\":\"[bill]*0.17\",\"restaurant_coverage_area\":\"koramangala\",\"cancellation_order_summary_link\":\"/invoice/download?token=30263283230_null\",\"old_order_meals\":[],\"free_delivery_discount_hit\":0,\"customer_id\":\"387395\",\"convenience_fee_actual\":\"0\",\"banner_factor\":\"0.39\",\"restaurant_type\":\"f\",\"customer_care_number\":\"08067466792\",\"commission_on_full_bill\":false,\"initiation_source\":0,\"swuid\":\"00f22f8f-391a-47f7-9619-f784508a062d\",\"restaurant_area_name\":\"Koramangala\",\"payment_confirmation_channel\":\"api\",\"agreement_type\":\"1\",\"order_spending\":\"0\",\"GST_on_cancellation_fee\":{},\"partner_id\":\"165\",\"free_shipping\":\"0\",\"de_pickedup_refund\":0,\"nodal_spending\":\"91.18\",\"restaurant_new_slug\":\"prod_testing_for_vms-domluru-btm\",\"distance_calc_method\":\"COMMON_SENSE_LUCIFER\",\"GST_on_delivery_fee\":{\"SGST\":1.5254237651824951,\"CGST\":1.5254237651824951,\"GST\":3.05},\"cancellation_fee_actual\":\"0\",\"delivered_time_in_seconds\":\"0\",\"restaurant_taxation_type\":\"GST\",\"convenience_fee_gst_expression\":\"18\",\"last_failed_order_id\":0,\"is_assured\":0,\"ordered_time_in_seconds\":1547188083,\"has_rating\":\"0\",\"type_of_partner\":2,\"item_total\":110,\"TCS_on_bill\":{\"STCS\":0.5774999870918691,\"CTCS\":0.5774999870918691},\"key\":\"6YKMP0\",\"distance_fee\":0,\"is_coupon_applied\":false,\"trade_discount_breakup\":[{\"is_super\":false,\"discount_breakup\":null,\"campaign_id\":474874,\"reward_type\":\"Flat\",\"seeding_info\":{\"shareType\":\"Percentage\",\"shareValue\":100,\"merchantType\":\"RESTAURANT\",\"commissionLevy\":0},\"discount_amount\":10}],\"delivery_address\":{\"city\":\"budhwal\",\"address_line1\":\"Unnamed Road, Budhwal, Haryana 123023, India\",\"name\":\"test-Prod\",\"area\":\"test\",\"mobile\":\"7899772315\",\"id\":\"46591477\",\"reverse_geo_code_failed\":false,\"email\":\"swiggyqa1234@gmail.com\",\"landmark\":\"test\",\"flat_no\":\"test\",\"address\":\"Unnamed Road, Budhwal, Haryana 123023, India\",\"lat\":\"27.837445\",\"lng\":\"76.172577\",\"annotation\":\"home\"},\"original_order_total\":136,\"is_partner_enable\":true,\"is_refund_initiated\":0,\"post_name\":\"\",\"is_first_order_delivered\":true,\"is_ivr_enabled\":\"0\",\"prep_time\":\"25\",\"order_discount\":10,\"cod_verification_threshold\":1000,\"configurations\":{},\"is_replicated\":false,\"subscription_total\":0,\"restaurant_phone_numbers\":\"9999999999\",\"is_cancellable\":false,\"subscription_gst_expression\":\"18\",\"order_placement_status\":\"0\",\"order_incoming\":\"136\",\"restaurant_area_code\":\"1\",\"GST_on_convenience_fee\":{},\"special_fee\":0,\"cart_id\":1722206597,\"sla_min\":\"25\",\"customer_user_agent\":\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36\",\"cancellation_fee_collected_total\":0,\"start_banner_factor\":\"1.4\",\"payment_txn_status\":\"success\",\"subscription_tax\":0,\"order_meals\":[],\"app_version_code\":\"320\",\"rendering_details\":[{\"display_text\":\"Item Total\",\"intermediateText\":\"\",\"info_text\":\"\",\"hierarchy\":1,\"value\":\"110.00\",\"currency\":\"rupees\",\"key\":\"item_total\",\"type\":\"display\",\"icon\":\"\"},{\"display_text\":\"GST\",\"intermediateText\":\"\",\"info_text\":\"\",\"hierarchy\":1,\"value\":\"5.50\",\"currency\":\"rupees\",\"key\":\"gst\",\"type\":\"display\",\"icon\":\"\"},{\"display_text\":\"Order Packing Charges\",\"intermediateText\":\"\",\"info_text\":\"\",\"hierarchy\":1,\"value\":\"10.00\",\"currency\":\"rupees\",\"key\":\"order_packing_charges\",\"type\":\"display\",\"icon\":\"\"},{\"display_text\":\"Delivery Charges\",\"intermediateText\":\"\",\"info_text\":\"\",\"hierarchy\":1,\"value\":\"20.00\",\"currency\":\"rupees\",\"key\":\"delivery_charges\",\"type\":\"display\",\"icon\":\"\"},{\"display_text\":\"Discount Applied\",\"intermediateText\":\"\",\"is_negative\":1,\"info_text\":\"\",\"hierarchy\":1,\"value\":\"10.00\",\"currency\":\"rupees\",\"key\":\"discount_applied\",\"type\":\"display\",\"icon\":\"\"}],\"order_status\":\"processing\",\"edit_refund_amount\":0,\"success_message\":\"Your order will be delivered shortly. Keep Swiggying!\",\"placement_delay_pred\":\"0.0\",\"restaurant_customer_distance\":\"0.0\",\"pg_charge\":\"0\",\"delivery_time_in_seconds\":\"0\",\"sid\":\"dpnd1e9a-663a-4a74-96c6-4d7a49f6d4e9\",\"coupon_discount\":0,\"order_payment_method\":\"Cash\",\"restaurant_address\":\"This is reserved for partner brainpower\",\"convenience_fee\":\"0\",\"order_delivery_charge\":20,\"listing_version_shown\":\"\",\"trade_discount_reward_type\":\"Flat\",\"TCS_on_bill_expressions\":{\"STCS\":\"0.005\",\"CTCS\":\"0.005\",\"ITCS\":\"0\"},\"order_restaurant_bill\":\"126\",\"restaurant_email\":\"test@swiggy.in\",\"sla_time\":\"27\",\"order_delivery_status\":\"\",\"converted_to_cod\":false,\"success_title\":\"Yay! Order Received\",\"sla_difference\":\"0\",\"GST_on_commission_expressions\":{\"SGST\":\"0.09\",\"CGST\":\"0.09\",\"IGST\":\"0\"},\"delayed_placing\":0,\"restaurant_discount_hit\":10,\"restaurant_has_inventory\":\"0\",\"restaurant_mobile\":\"9999999999\",\"is_assured_restaurant\":false,\"sla\":\"27\",\"GST_details\":{\"packaging_IGST\":0,\"item_SGST\":2.75,\"item_IGST\":0,\"service_charge_SGST\":0,\"packaging_CGST\":0,\"cart_CGST\":2.75,\"service_charge_IGST\":0,\"external_GST\":0,\"cart_IGST\":0,\"item_CGST\":2.75,\"service_charge_CGST\":0,\"packaging_SGST\":0,\"cart_SGST\":2.75},\"promise_id\":\"302632832307670847985433285\",\"first_order\":false,\"restaurant_cuisine\":[\"North Indian\",\"South Indian\"],\"last_mile_time_pred\":\"7.03\",\"sharedOrder\":false,\"post_type\":\"\",\"restaurant_name\":\"Brainpower integration VMS\",\"first_mile_time_pred\":\"0.0\",\"billing_lat\":\"27.837445\",\"restaurant_payment_mode\":{\"agreement_type_string\":\"Postpaid\",\"agreement_type\":\"1\"},\"actual_sla_time\":\"0\",\"on_time\":false,\"restaurant_lat_lng\":\"27.8374445,76.17257740000002\",\"order_tax\":5.5,\"menu_shown\":{},\"post_status\":\"processing\",\"tid\":\"47a91c5c-d7d6-4bb4-bb04-51e248cf17bf\",\"pay_by_system_value\":true,\"sla_max\":\"35\",\"billing_address_id\":\"46591477\",\"restaurant_city_code\":\"1\",\"GST_on_subscription\":{},\"time_fee\":0,\"overbooking\":\"0\",\"assignement_delay_pred\":\"2.52\",\"restaurant_cover_image\":\"\",\"order_items\":[{\"meal_quantity\":\"1\",\"total\":\"110\",\"single_variant\":false,\"item_charges\":{\"Service Charges\":\"0\",\"GST\":\"5.5\",\"Vat\":\"0\",\"Service Tax\":\"0\"},\"added_by_user_id\":-1,\"item_restaurant_discount_hit\":0,\"packing_charges\":\"0\",\"global_sub_category\":\"null\",\"is_veg\":\"1\",\"item_swiggy_discount_hit\":0,\"category_details\":{\"category\":\"Mega Meals\",\"sub_category\":\"nota\"},\"image_id\":\"\",\"in_stock\":1,\"item_id\":\"26569875\",\"variants\":[{\"external_choice_id\":\"4867\",\"name\":\"variant 7.1\",\"variant_tax_charges\":{\"GST\":\"0.5\"},\"price\":10,\"variation_id\":8183792,\"group_id\":2414490,\"variant_tax_expressions\":{\"GST_inclusive\":false,\"SGST\":\"[VARIANT_PRICE]*0.025\",\"CGST\":\"[VARIANT_PRICE]*0.025\",\"IGST\":\"[VARIANT_PRICE]*0.0\"},\"GST_details\":{\"SGST\":0.25,\"CGST\":0.25,\"IGST\":0}}],\"subtotal\":\"110\",\"name\":\"Item 7 has addon\",\"global_main_category\":\"null\",\"external_item_id\":\"4866\",\"group_user_item_map\":{},\"item_tax_expressions\":{\"GST_inclusive\":false,\"IGST\":\"[SUBTOTAL]*0.0\",\"Service Charges\":\"0\",\"SGST\":\"[SUBTOTAL]*0.025\",\"Service Tax\":\"0\",\"CGST\":\"[SUBTOTAL]*0.025\",\"Vat\":\"0\"},\"added_by_username\":\"\",\"addons\":[{\"external_choice_id\":\"4880\",\"choice_id\":\"9770024\",\"name\":\"addon 1\",\"addon_tax_charges\":{\"GST\":\"1\"},\"addon_tax_expressions\":{\"GST_inclusive\":false,\"SGST\":\"[ADDON_PRICE]*0.025\",\"CGST\":\"[ADDON_PRICE]*0.025\",\"IGST\":\"[ADDON_PRICE]*0.0\"},\"price\":20,\"external_group_id\":\"\",\"group_id\":\"3784314\",\"GST_details\":{\"SGST\":0.5,\"CGST\":0.5,\"IGST\":0}}],\"GST_details\":{\"SGST\":2.75,\"CGST\":2.75,\"IGST\":0},\"quantity\":\"1\"}],\"delivery_charge_actual\":\"16.95\",\"stop_banner_factor\":\"1.6\",\"free_gifts\":[],\"is_select\":false,\"order_total\":136,\"order_discount_without_freebie\":10,\"swiggy_money\":0,\"payment\":\"successful\",\"device_id\":\"00f22f8f-391a-47f7-9619-f784508a062d\",\"threshold_fee\":20,\"trade_discount_reward_list\":[\"Flat\"],\"restaurant_locality\":\"domluru\",\"delivery_charge_gst_expression\":\"18\",\"charges\":{\"GST\":\"5.5\",\"Delivery Charges\":\"20\",\"Service Charges\":\"0\",\"Service Tax\":\"0\",\"Packing Charges\":\"10\",\"Convenience Fee\":\"0\",\"Cancellation Fee\":\"0\",\"Vat\":\"0\"},\"cancellation_fee_applied\":0,\"commission\":\"19.64\",\"coupon_code\":\"\",\"service_tax_on_commission\":\"0\",\"subscription_total_without_tax\":0}";

                vendorCommonHelper.pushMessage(VendorConstant.VVO_PLACER_SERVICE, VendorConstant.NEW_ORDERS_VVO_PLACER_SERVICE_EXCHANGE, new AMQP.BasicProperties().builder().contentType("application/json"), message);
                return message;
        }

        public void paginatedSearchInvalidCityID(Processor paginatedSearch) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
                softAssert.assertNotNull(statusCode);
                String code = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
                softAssert.assertNotNull(code);
                String message = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.message"));
                softAssert.assertEquals(message, "");
                String data = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                String page = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.page"));
                softAssert.assertNotNull(page);
                String rows_per_page = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rows_per_page"));
                softAssert.assertNotNull(rows_per_page);
                String search_data = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data"));
                softAssert.assertNotNull(search_data);
                softAssert.assertAll();
        }

        public void paginatedSearchFailure(Processor paginatedSearch) {
                SoftAssert softAssert = new SoftAssert();
                String statusCode = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
                softAssert.assertEquals(statusCode, VendorConstant.random_0);
                String code = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
                softAssert.assertEquals(code, "FAILURE");
                String message = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.message"));
                softAssert.assertEquals(message, "There was some error while processing this request.");
                String data = String.valueOf(paginatedSearch.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertNotNull(data);
                softAssert.assertAll();
        }

}
