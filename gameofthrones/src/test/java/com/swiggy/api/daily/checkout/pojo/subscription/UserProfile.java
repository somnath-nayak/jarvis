package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class UserProfile {


    @JsonProperty("referralUsed")
    private Long referralUsed;
    @JsonProperty("lastOrderCity")
    private Long lastOrderCity;
    @JsonProperty("superType")
    private String superType;
    @JsonProperty("customer_id")
    private Long customerId;
    @JsonProperty("email")
    private String email;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("name")
    private String name;
    @JsonProperty("referral_code")
    private String referralCode;
    @JsonProperty("verified")
    private Boolean verified;
    @JsonProperty("email_verification_flag")
    private Boolean emailVerificationFlag;
    @JsonProperty("referred_by")
    private Object referredBy;
    @JsonProperty("isEnabled")
    private Long isEnabled;
    @JsonProperty("optional_map")
    private Object optionalMap;

    @JsonProperty("referralUsed")
    public Long getReferralUsed() {
        return referralUsed;
    }

    @JsonProperty("referralUsed")
    public void setReferralUsed(Long referralUsed) {
        this.referralUsed = referralUsed;
    }

    public UserProfile withReferralUsed(Long referralUsed) {
        this.referralUsed = referralUsed;
        return this;
    }

    @JsonProperty("lastOrderCity")
    public Long getLastOrderCity() {
        return lastOrderCity;
    }

    @JsonProperty("lastOrderCity")
    public void setLastOrderCity(Long lastOrderCity) {
        this.lastOrderCity = lastOrderCity;
    }

    public UserProfile withLastOrderCity(Long lastOrderCity) {
        this.lastOrderCity = lastOrderCity;
        return this;
    }

    @JsonProperty("superType")
    public String getSuperType() {
        return superType;
    }

    @JsonProperty("superType")
    public void setSuperType(String superType) {
        this.superType = superType;
    }

    public UserProfile withSuperType(String superType) {
        this.superType = superType;
        return this;
    }

    @JsonProperty("customer_id")
    public Long getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public UserProfile withCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public UserProfile withEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public UserProfile withMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public UserProfile withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("referral_code")
    public String getReferralCode() {
        return referralCode;
    }

    @JsonProperty("referral_code")
    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public UserProfile withReferralCode(String referralCode) {
        this.referralCode = referralCode;
        return this;
    }

    @JsonProperty("verified")
    public Boolean getVerified() {
        return verified;
    }

    @JsonProperty("verified")
    public void setVerified(Boolean verified) {
        this.verified = verified;
    }

    public UserProfile withVerified(Boolean verified) {
        this.verified = verified;
        return this;
    }

    @JsonProperty("email_verification_flag")
    public Boolean getEmailVerificationFlag() {
        return emailVerificationFlag;
    }

    @JsonProperty("email_verification_flag")
    public void setEmailVerificationFlag(Boolean emailVerificationFlag) {
        this.emailVerificationFlag = emailVerificationFlag;
    }

    public UserProfile withEmailVerificationFlag(Boolean emailVerificationFlag) {
        this.emailVerificationFlag = emailVerificationFlag;
        return this;
    }

    @JsonProperty("referred_by")
    public Object getReferredBy() {
        return referredBy;
    }

    @JsonProperty("referred_by")
    public void setReferredBy(Object referredBy) {
        this.referredBy = referredBy;
    }

    public UserProfile withReferredBy(Object referredBy) {
        this.referredBy = referredBy;
        return this;
    }

    @JsonProperty("isEnabled")
    public Long getIsEnabled() {
        return isEnabled;
    }

    @JsonProperty("isEnabled")
    public void setIsEnabled(Long isEnabled) {
        this.isEnabled = isEnabled;
    }

    public UserProfile withIsEnabled(Long isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    @JsonProperty("optional_map")
    public Object getOptionalMap() {
        return optionalMap;
    }

    @JsonProperty("optional_map")
    public void setOptionalMap(Object optionalMap) {
        this.optionalMap = optionalMap;
    }

    public UserProfile withOptionalMap(Object optionalMap) {
        this.optionalMap = optionalMap;
        return this;
    }

}
