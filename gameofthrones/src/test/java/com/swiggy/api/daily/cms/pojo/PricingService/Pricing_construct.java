
package com.swiggy.api.daily.cms.pojo.PricingService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "price_list",
    "attributes"
})
public class Pricing_construct {

    @JsonProperty("price_list")
    private Price_list price_list;
    @JsonProperty("attributes")
    private Attributes attributes;

    @JsonProperty("price_list")
    public Price_list getPrice_list() {
        return price_list;
    }

    @JsonProperty("price_list")
    public void setPrice_list(Price_list price_list) {
        this.price_list = price_list;
    }

    public Pricing_construct withPrice_list(Price_list price_list) {
        this.price_list = price_list;
        return this;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Pricing_construct withAttributes(Attributes attributes) {
        this.attributes = attributes;
        return this;
    }
    public Pricing_construct setData(Integer list_price,Integer price,Integer total_price,Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes,Integer sgst_utgst,Integer sgst_utgst_percentage){
        Price_list pc=new Price_list().setData(list_price, price, total_price);
        Attributes at=new Attributes().setData(cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage);
        this.withAttributes(at).withPrice_list(pc);
        return this;
    }

}
