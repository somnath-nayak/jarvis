
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "price",
    "tax",
    "inclusiveTax"
})
public class CartPackingCharges {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("tax")
    private Tax__ tax;
    @JsonProperty("inclusiveTax")
    private Boolean inclusiveTax;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public CartPackingCharges withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public CartPackingCharges withTotal(Integer total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public CartPackingCharges withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public CartPackingCharges withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @JsonProperty("tax")
    public Tax__ getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Tax__ tax) {
        this.tax = tax;
    }

    public CartPackingCharges withTax(Tax__ tax) {
        this.tax = tax;
        return this;
    }

    @JsonProperty("inclusiveTax")
    public Boolean getInclusiveTax() {
        return inclusiveTax;
    }

    @JsonProperty("inclusiveTax")
    public void setInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public CartPackingCharges withInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
        return this;
    }


    public CartPackingCharges setDefaultData()  {
        return this.withTotalWithoutDiscount(SubscriptionConstant.CARTPACKINGCHARGES_TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.CARTPACKINGCHARGES_TOTAL)
                .withDiscount(SubscriptionConstant.CARTPACKINGCHARGES_DISCOUNT)
                .withPrice(SubscriptionConstant.CARTPACKINGCHARGES_PRICE)
                .withTax(new Tax__().setDefaultData())
                .withInclusiveTax(SubscriptionConstant.PRICINGDETAIL_INCLUSIVETAX);

    }

}
