
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "order_id",
    "payment_info",
    "order_jobs",
    "order_type",
    "order_metadata",
    "coupon_code",
    "created_at",
    "updated_at"
})
public class Order {
    RandomNumber rm = new RandomNumber(100001, 1000000);

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("payment_info")
    private Object paymentInfo;
    @JsonProperty("order_jobs")
    private List<OrderJob> orderJobs = null;
    @JsonProperty("order_type")
    private String orderType;
    @JsonProperty("order_metadata")
    private Object orderMetadata;
    @JsonProperty("coupon_code")
    private String couponCode;
    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Order withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("payment_info")
    public Object getPaymentInfo() {
        return paymentInfo;
    }

    @JsonProperty("payment_info")
    public void setPaymentInfo(Object paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public Order withPaymentInfo(Object paymentInfo) {
        this.paymentInfo = paymentInfo;
        return this;
    }

    @JsonProperty("order_jobs")
    public List<OrderJob> getOrderJobs() {
        return orderJobs;
    }

    @JsonProperty("order_jobs")
    public void setOrderJobs(List<OrderJob> orderJobs) {
        this.orderJobs = orderJobs;
    }

    public Order withOrderJobs(List<OrderJob> orderJobs) {
        this.orderJobs = orderJobs;
        return this;
    }

    @JsonProperty("order_type")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("order_type")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Order withOrderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    @JsonProperty("order_metadata")
    public Object getOrderMetadata() {
        return orderMetadata;
    }

    @JsonProperty("order_metadata")
    public void setOrderMetadata(Object orderMetadata) {
        this.orderMetadata = orderMetadata;
    }

    public Order withOrderMetadata(Object orderMetadata) {
        this.orderMetadata = orderMetadata;
        return this;
    }

    @JsonProperty("coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("coupon_code")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Order withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public Order withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public Long getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Order withUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Order setDefaultData(String action) throws ParseException {
        return this.withOrderId(String.valueOf(rm.nextInt()))
                .withPaymentInfo(null)
                .withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData(action));}})
                .withOrderType(SubscriptionConstant.ORDER_TYPE)
                .withOrderMetadata(null)
                .withCouponCode("")
                .withCreatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()))
                .withUpdatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()));
    }

}
