package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class DeliveryPricingDetails {
    
    @JsonProperty("deliveryFees")
    private ArrayList<DeliveryFees> deliveryFees;
    @JsonProperty("totalDeliveryFee")
    private Integer totalDeliveryFee;
    
    public ArrayList<DeliveryFees> getDeliveryFees() {
        return deliveryFees;
    }
    
    public void setDeliveryFees(ArrayList<DeliveryFees> deliveryFees) {
        this.deliveryFees = deliveryFees;
    }
    
    public Integer getTotalDeliveryFee() {
        return totalDeliveryFee;
    }
    
    public void setTotalDeliveryFee(Integer totalDeliveryFee) {
        this.totalDeliveryFee = totalDeliveryFee;
    }
    
    
    public DeliveryPricingDetails withDeliveryFees(ArrayList<DeliveryFees> deliveryFees) {
        setDeliveryFees(deliveryFees);
        return this;
    }
    
    public DeliveryPricingDetails withTotalDeliveryFee(Integer totalDeliveryFee) {
        setTotalDeliveryFee(totalDeliveryFee);
        return this;
    }
    
    public DeliveryPricingDetails deliveryPricingDetailsWithGivenValues(ArrayList<DeliveryFees> deliveryFees,Integer totalDeliveryFee){
        setDeliveryFees(deliveryFees); setTotalDeliveryFee(totalDeliveryFee);
        return this;
    }
    
//    public List<DeliveryPricingDetails> deliveryPricingDetailsListWithValues(){
//    List<DeliveryPricingDetails> deliveryPricingDetailsList = new ArrayList<>();
//    deliveryPricingDetailsList.add(0,withDefaultValues());
//
//    return deliveryPricingDetailsList;
//    }
    
    public DeliveryPricingDetails deliveryPricingDetailsListWithValues(){
        setTotalDeliveryFee(30);
        setDeliveryFees(new DeliveryFees().withGivenValues("distance_fee",30));
    return this;
    }
    
    
}
