package com.swiggy.api.daily.cms.availabilityService.helper;

public interface AvailServiceConstant {

    String city = "3";
    String area = "160";
    String store = "9990";
    String plan = "PLAN01";
    String product = "PPRODUCT01";
    String PLAN = "PLAN";
    String PRODUCT = "PRODUCT";
    String invalid = "7238921912213421425123523";
    String RANDOM = "RANDOM";
    String MEAL = "MEAL";
    String STORE = "STORE";
    String catalogDB = "catalog";
    String endQuotes = "' ";
    String deleteCitySlot = "delete from city_slots where city_id = '";
    String day = " and day = '";
    String updated_by = " and updated_by = '";
    String deleteAreaSlot = "delete from area_slots where area_id = '";
    String deleteRestaurantSlots = "delete from restaurant_slots where rest_id = '";
    String deleteSlotInfo = "delete from slot_info where entity_id = '";
    String query1= " and store_id = '";
    String limit = " limit 1";

}
