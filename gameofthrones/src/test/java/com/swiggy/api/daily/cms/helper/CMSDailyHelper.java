package com.swiggy.api.daily.cms.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.availabilityService.helper.AvailServiceHelper;
import com.swiggy.api.daily.cms.pojo.DailyManagerInventory.SubscriptionInventoryDaily;
import com.swiggy.api.daily.cms.pojo.InventoryService.InventoryDaily;
import com.swiggy.api.daily.cms.pojo.InventoryService.SearchInventory;
import com.swiggy.api.daily.cms.pojo.InventoryService.UpdateInventory;
import com.swiggy.api.daily.cms.pojo.MealListing.ProductAvailMeta;
import com.swiggy.api.daily.cms.pojo.PlanListing.PlanListing;
import com.swiggy.api.daily.cms.pojo.PricingService.PricingDaily;
import com.swiggy.api.daily.cms.pojo.Relationship.*;
import com.swiggy.api.daily.cms.pojo.Slot.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CMSDailyHelper {

    static Initialize gameofthrones = new Initialize();
    Processor processor = null;
    ObjectMapper mapper = new ObjectMapper();
    SlotCreate scr = new SlotCreate();
    SlotUpdate scu = new SlotUpdate();
    RelationShipManagerBulk rm = new RelationShipManagerBulk();
    RelationShipManagerDelete rd = new RelationShipManagerDelete();
    SearchFilter sf = new SearchFilter();
    SubscriptionInventoryDaily sc = new SubscriptionInventoryDaily();
    InventoryDaily inv = new InventoryDaily();
    SearchInventory si = new SearchInventory();
    ProductAvailMeta pv = new ProductAvailMeta();
    PricingDaily pri = new PricingDaily();
    AvailServiceHelper helper = new AvailServiceHelper();
    UpdateInventory up = new UpdateInventory();
    public static String filepath;

    public static HashMap headers() {

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", CMSDailyContants.cms_daily_authorization_key);
        requestHeaders.put("tenant-id", CMSDailyContants.cms_daily_tenant_id);
        requestHeaders.put("service", CMSDailyContants.cms_service);
        requestHeaders.put("user-info", CMSDailyContants.cms_user_info);
        requestHeaders.put("user-id", CMSDailyContants.cms_user_info);
        return requestHeaders;

    }

    public Processor prodctListingAvail(String starttime, String endtime, List pids, Integer storeid) throws Exception {
        pv.setData(storeid, pids);
        List li = new ArrayList();
        li.add(pv);
        String payload = mapper.writeValueAsString(li);
        GameOfThronesService service1 = new GameOfThronesService("swiggylistingservice", "mealmetaandavail", gameofthrones);
        String queryparam[] = {"AVAILABILITY,INVENTORY", starttime, endtime};
        processor = new Processor(service1, CMSDailyHelper.headers(), new String[]{payload}, queryparam);
        return processor;
    }

    public Processor availablePlan(String storeid, String starttime, String daystarttime, String endtime, String service) {
        GameOfThronesService service1 = new GameOfThronesService("cmsdaily", "availablemeals", gameofthrones);
        String queryparam[] = {storeid, starttime, endtime, daystarttime, service};
        processor = new Processor(service1, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor prodctListingMeta(String starttime, String endtime, List pids, Integer storeid) throws Exception {
        pv.setData(storeid, pids);
        List li = new ArrayList();
        li.add(pv);
        String payload = mapper.writeValueAsString(li);
        String queryparam[] = {"LIST-META", starttime, endtime};
        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "mealmetaandavail", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{payload}, queryparam);
        return processor;
    }

    public Processor availablePlanMeta(String payload, String servicename) {
        String queryparam[] = {servicename};
        GameOfThronesService service = new GameOfThronesService("cmsdaily", "availableplanmeta", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{payload}, queryparam);
        return processor;
    }

    public Processor getAddon(String storeid) {
        String queryparam[] = {storeid};
        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "getaddon", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getPlanByStoreSKU(String storeid, String skuid, String starttime, String endtime, String servicename) {
        String queryparam[] = {storeid, skuid, starttime, endtime, servicename};
        GameOfThronesService service = new GameOfThronesService("cmsdaily", "getplanavailabilitybystoresku", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getMealByStoreSKU(String storeid, String skuid, String starttime, String endtime, String servicename) {
        String queryparam[] = {storeid, skuid, starttime, endtime, servicename};
        GameOfThronesService service = new GameOfThronesService("cmsdaily", "getmealavailabilitybystoresku", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }


    public Processor createSlot(String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive, int count) throws Exception {
        scr.setData(entitytype, interval, days, freq, starttime, endtime, live_date, exp_date, is_exclusive);
        GameOfThronesService service = new GameOfThronesService("slotservice", "createslot", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{responseBulk(count, scr)});

        return processor;
    }

    public Processor createSlotNegative(String entityid, Integer storeid, String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive, int count) throws Exception {
        scr.setDataNegative(entityid, storeid, entitytype, interval, days, freq, starttime, endtime, live_date, exp_date, is_exclusive);
        GameOfThronesService service = new GameOfThronesService("slotservice", "createslot", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{"[" + mapper.writeValueAsString(scr) + "]"});

        return processor;
    }

    public Processor updateSlot(long sid, String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive, int count) throws Exception {
        scu.setData(sid, entitytype, interval, days, freq, starttime, endtime, live_date, exp_date, is_exclusive);
        GameOfThronesService service = new GameOfThronesService("slotservice", "updateslot", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{"[" + mapper.writeValueAsString(scu) + "]"});
        return processor;
    }

    public Processor updateSlotNegative(long sid, String entityid, Integer storeid, String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive, int count) throws Exception {
        scu.setDataNegative(sid, entityid, storeid, entitytype, interval, days, freq, starttime, endtime, live_date, exp_date, is_exclusive);
        GameOfThronesService service = new GameOfThronesService("slotservice", "updateslot", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{"[" + mapper.writeValueAsString(scu) + "]"});
        return processor;
    }

    public Processor getSlotbySlotID(int sid, String expand, int days) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getslotbysid", gameofthrones);
        String queryparam[] = {String.valueOf(sid), expand, String.valueOf(days)};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getSlotbyEntityID(String eid, String expand, int days) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getslotbyeid", gameofthrones);
        String queryparam[] = {eid, expand, String.valueOf(days)};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getSlotbyStoreID(String storeid) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getslotbystoreid", gameofthrones);
        String queryparam[] = {storeid};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getEntityByEntityType(String etype) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getentitybyentitytype", gameofthrones);
        String queryparam[] = {etype};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getEntityByStoreIAndEntityType(String storeid, String entitytype) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getallentitybystoreidandentitytype", gameofthrones);
        String queryparam[] = {storeid, entitytype};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getHolidaySlotbyEntityID(String eid, String expand, int days) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "getholidayslotbyeid", gameofthrones);
        String queryparam[] = {eid, expand, String.valueOf(days)};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor deleteSlotbySlotID(int sid) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "deleteslotbysid", gameofthrones);
        String queryparam[] = {String.valueOf(sid)};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor deleteSlotbyEntityID(String eid) {
        GameOfThronesService service = new GameOfThronesService("slotservice", "deleteslotbyeid", gameofthrones);
        String queryparam[] = {eid};
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }


    public static long createEPOCforDays(int day) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());
        cal.add(Calendar.DAY_OF_MONTH, day);
        timestamp = new Timestamp(cal.getTime().getTime());
        return timestamp.getTime();
    }

    public static long createEPOCforHour(int hr) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());
        cal.add(Calendar.HOUR, hr);
        timestamp = new Timestamp(cal.getTime().getTime());
        return timestamp.getTime();

    }

    public static long createEPOCforCurrent() {
        Instant instant = Instant.now();
        long seconds = instant.getEpochSecond() * 1000;
        return seconds;
    }

    public static String getFutureTimeForMin(int min) {
        return DateUtils.addMinutes(new Date(), min).toString().substring(10, 19).trim();
    }

    public static String getFutureTimeForHour(int hr) {
        return DateUtils.addHours(new Date(), hr).toString().substring(10, 19).trim();
    }

    public Processor craeteRelationShip(String sstoreid, String spid, String dstoreid, String dspinid, String retype) throws Exception {
        rm.setData(spid, sstoreid, dstoreid, dspinid, retype, null);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rm)});
        return processor;
    }

    public Processor craeteRelationShipNegative(Source sr, Destination ds, Relationship_data data, String retype) throws Exception {
        rm.setWrongData(sr, ds, data, retype, null);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rm)});
        return processor;
    }

    public Processor updateRelationShipNegative(Source sr, Destination ds, Relationship_data data, String retype, String id) throws Exception {
        rm.setWrongData(sr, ds, data, retype, id);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rm)});
        return processor;
    }

    public Processor deleteRelationShipNegative(Source sr, Destination ds, String retype, String id) throws Exception {
        rd.setWrongDataDelete(sr, ds, retype, id);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rm)});
        return processor;
    }

    public Processor updateRelationShip(String spid, String sstoreid, String dstoreid, String dspinid, String retype, String id) throws Exception {
        rm.setData(spid, sstoreid, dstoreid, dspinid, retype, id);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rm)}, null);
        return processor;
    }

    public Processor deleteRelationShipBulk(String sstoreid, String spid, String dstoreid, String dspinid, String retype, String id) throws Exception {
        rd.setData(sstoreid, spid, dstoreid, dspinid, retype, id);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "bulkcrud", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(rd)}, null);
        return processor;
    }

    public Processor getRelationShip(String id) throws Exception {
        String queryparam[] = {id};
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "getsinglerelation", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor deleteRelationShipByID(String id) throws Exception {
        String queryparam[] = {id};
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "deleterelation", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        return processor;
    }

    public Processor getallRelationShiptype() throws Exception {
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "getsupportedrelationship", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, null);
        return processor;
    }

    public Processor searchRelationShip(Integer page, Integer row_per_page, String ssid, String spid, String ddid, String spinid, String reltype, String operater) throws Exception {
        String queryparam[] = {page.toString(), row_per_page.toString()};
        sf.setData(ssid, spid, ddid, spinid, reltype, operater);
        GameOfThronesService service = new GameOfThronesService("relatiomanagerservice", "searchrelation", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(sf)}, queryparam);
        return processor;
    }


    public static String responseBulk(int count, Object ob) throws Exception {
        ObjectMapper mp = new ObjectMapper();
        String temp = "";
        if (count == 1)
            return "[" + mp.writeValueAsString(ob) + "]";

        else if (count > 1) {
            for (int i = 0; i < count; i++) {
                temp = temp + mp.writeValueAsString(ob) + ",";
            }
        }
        return "[" + temp.substring(0, temp.length() - 1) + "]";

    }

    public static String alphaNumericString(int count) {
        return RandomStringUtils.randomAlphanumeric(count);
    }

    public static int randomNumber() {
        int max = 999999;
        int min = 100000;
        double x = (Math.random() * (max - min));
        int y = (int) x;
        return y;
    }


    public Processor createInventory(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        inv.setData(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount);
        List li = new ArrayList();
        li.add(inv);
        HashMap header = new LinkedHashMap();
        header.put("service-line", serviceline);
        String payload = mapper.writeValueAsString(li);
        GameOfThronesService service = new GameOfThronesService("cmsdailyinventoryservice", "inventorycreate", gameofthrones);
        processor = new Processor(service, header, new String[
                ]{payload}, null);
        return processor;
    }

    public Processor getInventory(String key, String serviceline) {
        String queryparam[] = {key};
        HashMap header = new LinkedHashMap();
        header.put("service-line", serviceline);
        GameOfThronesService service = new GameOfThronesService("cmsdailyinventoryservice", "inventoryget", gameofthrones);
        processor = new Processor(service, header, null, queryparam);
        return processor;
    }

    public Processor searchInventory(String spinid, String storeid, Long slotid, String serviceline) throws Exception {
        si.setData(spinid, storeid, slotid);
        HashMap header = new LinkedHashMap();
        header.put("service-line", serviceline);
        String payload[] = {mapper.writeValueAsString(si)};
        GameOfThronesService service = new GameOfThronesService("cmsdailyinventoryservice", "inventorysearch", gameofthrones);
        processor = new Processor(service, header, payload, null);
        return processor;

    }

    public Processor deleteInventory(String key, String serviceline) throws Exception {
        HashMap header = new LinkedHashMap();
        String queyryparam[] = {key};
        header.put("service-line", serviceline);
        GameOfThronesService service = new GameOfThronesService("cmsdailyinventoryservice", "inventorydelete", gameofthrones);
        processor = new Processor(service, header, null, queyryparam);
        return processor;

    }

    public Processor adjustMealSubscrption(Integer cou, String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        sc.setData(cou, storeid, oprt, spin, startepoc, endepoc);
        GameOfThronesService service = new GameOfThronesService("cmsdailymanagerservice", "inventoryadjustmeal", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(sc)});
        return processor;

    }

    public Processor adjustPlanSubscrption(Integer cou, String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        sc.setData(cou, storeid, oprt, spin, startepoc, endepoc);
        GameOfThronesService service = new GameOfThronesService("cmsdailymanagerservice", "inventoryadjustplan", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(sc)});
        return processor;


    }

    public Processor fetchSpinFromProdctId(String pid) {
        String queyparm[] = {pid};
        GameOfThronesService service = new GameOfThronesService("cmsproductservice", "fetchspin", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queyparm);
        return processor;
    }

    public Processor fetchProdctIdfromSpin(String spin) {
        String queyparm[] = {spin};
        GameOfThronesService service = new GameOfThronesService("cmsproductservice", "spinget", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queyparm);
        return processor;
    }

    public boolean listingCheckMeal(Integer storeid) throws Exception {
        boolean check;
        pv.setData(storeid, null);
        List li = new ArrayList();
        li.add(pv);
        String payload = mapper.writeValueAsString(li);
        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "mealmetaandavail", gameofthrones);
        String queryparam[] = {"AVAILABILITY,INVENTORY", "", ""};
        Processor processor = new Processor(service, CMSDailyHelper.headers(), new String[]{payload}, queryparam);
        String queryparam1[] = {"LIST-META", "", ""};
        Processor processor1 = new Processor(service, CMSDailyHelper.headers(), new String[]{payload}, queryparam1);
        String responsemeta = processor1.ResponseValidator.GetBodyAsText();
        String responseavail = processor.ResponseValidator.GetBodyAsText();
        String spinavail = JsonPath.read(responseavail, "$.data..variations..spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        List listspinavail = Arrays.asList(spinavail.split("\\s*,\\s*"));
        String spinmeta = JsonPath.read(responsemeta, "$.data..variations..spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        List listspinmeta = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        Assert.assertEquals(listspinavail, listspinmeta);
        ToolBox tb = new ToolBox();
        check = tb.compareLists(listspinavail, listspinmeta);
        return check;

    }

    public Processor createPricingMeal(String key, String product_id, String sku_id, String spin, String store_id, Integer list_price, Integer price, Integer total_price, Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes, Integer sgst_utgst, Integer sgst_utgst_percentage) throws Exception {
        pri.setData(key, product_id, sku_id, spin, store_id, list_price, price, total_price, cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage);
        ObjectMapper mp = new ObjectMapper();
        ArrayList al = new ArrayList();
        al.add(pri);
        HashMap header = new LinkedHashMap();
        header.put("service-line", "daily_catalog_product");
        String payload[] = {mp.writeValueAsString(al)};
        GameOfThronesService service = new GameOfThronesService("cmsdailypricingservice", "createprice", gameofthrones);
        processor = new Processor(service, header, payload, null);
        return processor;
    }

    public Processor createPricingPlan(String key, String product_id, String sku_id, String spin, String store_id, Integer list_price, Integer price, Integer total_price, Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes, Integer sgst_utgst, Integer sgst_utgst_percentage) throws Exception {
        pri.setData(key, product_id, sku_id, spin, store_id, list_price, price, total_price, cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage);
        ObjectMapper mp = new ObjectMapper();
        ArrayList al = new ArrayList();
        al.add(pri);
        HashMap header = new LinkedHashMap();
        header.put("service-line", "daily_catalog_plan");
        String payload[] = {mp.writeValueAsString(al)};
        GameOfThronesService service = new GameOfThronesService("cmsdailypricingservice", "createprice", gameofthrones);
        processor = new Processor(service, header, payload, null);
        return processor;
    }

    public Processor getPricing(String key, String servicline) {
        String queryparam[] = {key};
        HashMap header = new LinkedHashMap();
        header.put("service-line", servicline);
        GameOfThronesService service = new GameOfThronesService("cmsdailypricingservice", "getprice", gameofthrones);
        processor = new Processor(service, header, null, queryparam);
        return processor;
    }


    public Processor updateMaxCap(String spinid, String pid, Long slotid, Integer maxcount, Integer soldcount, String servicline) {
        up.setData(spinid, pid, slotid, maxcount, soldcount);
        try {
            String payload[] = {mapper.writeValueAsString(up)};
            HashMap header = new LinkedHashMap();
            header.put("service-line", servicline);
            GameOfThronesService service = new GameOfThronesService("cmsdailyinventoryservice", "inventoryupdate", gameofthrones);
            processor = new Processor(service, header, payload, null);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return processor;

    }

    public static long createEPOCforDays() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timestamp.getTime());
        timestamp = new Timestamp(cal.getTime().getTime());
        return timestamp.getTime();
    }


    /**
     * This function will check availabilty of product which have one slot for date.
     *
     * @param entityid
     * @param startepoc
     * @param endepoc
     * @return
     */


    public boolean availCheckProduct(String entityid, Long startepoc, Long endepoc, int day) {
        boolean avail = false;
        int days = day - 1;
        String response = helper.product(entityid).ResponseValidator.GetBodyAsText();
        String start = JsonPath.read(response, "$.data.slot_timings[" + days + "].start_epoch").toString().replace("[", "").replace("]", "").replace("\"", "");
        String end = JsonPath.read(response, "$.data.slot_timings[" + days + "].end_epoch").toString().replace("[", "").replace("]", "").replace("\"", "");
        if (startepoc == null && endepoc == null) {
            startepoc = createEPOCforCurrent();
            endepoc = createEPOCforCurrent();
        }

        if (startepoc >= Long.parseLong(start) && endepoc <= Long.parseLong(end))
            avail = true;
        else
            avail = false;

        return avail;
    }

    /**
     * This function will check availabilty of product which have one or more than one slot for date.
     *
     * @param restid
     * @param spinid
     * @param startepoc
     * @param endepoc
     * @return
     */


    public Boolean availCheckProducts(String restid,String spinid, Long startepoc, Long endepoc) {
        String response = null;
        int count=0;
        String entityid=restid+"-"+spinid;
        try {
            response = helper.timeline(restid, "PRODUCT").ResponseValidator.GetBodyAsText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            AvailabiltityResponse availResponse = mapper.readValue(response, AvailabiltityResponse.class);
            for(int i=0;i<availResponse.getData().size();i++) {
                count++;
                if (availResponse.getData().get(i).getEntity_id().equals(entityid)) {
                    for (AvailSlot slot : availResponse.getData().get(count-1).getData().getSlot_timings()) {
                        if ((slot.getStart_epoch() <= startepoc && startepoc <= slot.getEnd_epoch()) || // when startTime in mid on timeline
                                (slot.getStart_epoch() <= endepoc && endepoc <= slot.getEnd_epoch()) ||
                                (startepoc <= slot.getStart_epoch() && slot.getStart_epoch() <= endepoc) || // when startTime in mid on timeline
                                (startepoc <= slot.getEnd_epoch() && slot.getEnd_epoch() <= endepoc)) {
                            return true;
                        }
                    }
                }
            }
            return false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    public Processor planAvailabilityAndInventory(String includeOnly, String startDate, String startTime, String endTime, String variant, Object[][] object) throws IOException {
        String[] queryparams = {includeOnly, startDate, startTime, endTime, variant};
        List<PlanListing> pojos = new ArrayList<>();
        for (Object[] obj : object) {
            String storeId = (obj[0]).toString();
            List<String> productIds = new ArrayList<>();
            try {
                for (String productId : (String[]) obj[1]) {
                    productIds.add(productId);
                }
            } catch (Exception ex) {
            }
            pojos.add(new PlanListing(Integer.parseInt(storeId), productIds));
        }
        ObjectMapper mapper = new ObjectMapper();
        String[] finalPojo = new String[pojos.size()];
        for (int i = 0; i < finalPojo.length; i++) {
            finalPojo[i] = mapper.writeValueAsString(pojos.get(i));
        }
        String fPojo = Arrays.toString(finalPojo);
        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "getplan", gameofthrones);
        processor = new Processor(service, headers(), new String[]{fPojo}, queryparams);
        return processor;
    }

    /**
     * Upload a Image for SPINs
     *
     * @param spins
     * @param entitytype(Product,Addon,Plan)
     * @return
     */

    public Processor imageUploadForSpins(List<String> spins, String entitytype) {

        URL url = null;
        String filename = null;
        if (entitytype.equalsIgnoreCase("product") || entitytype.equalsIgnoreCase("addon")) {
            for (int i = 0; i < spins.size(); i++) {
                filename = "meals" + "_" + spins.get(i) + "_" + "MN.jpeg";

                BufferedImage image = null;
                try {
                    url = new URL("https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_366,h_240,c_fill/m8m9z0b7divg915bo4gk");
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }

                try {
                    image = ImageIO.read(url);
                    ImageIO.write(image, "jpg", new File(filename));

                } catch (IOException e) {
                    e.printStackTrace();
                }
                zipFile("../gameofthrones/" + "" + filename);
                deleteFile("../gameofthrones/", ".jpeg");
                HashMap hm = new HashMap();
                hm.put("file", "../gameofthrones/a.zip");
                GameOfThronesService got = new GameOfThronesService("cmsingestionservice", "spinimage", gameofthrones);
                processor = new Processor(got, CMSDailyHelper.headers(), null, null, hm);
                deleteFile("../gameofthrones/", ".zip");

            }

        } else if (entitytype.equalsIgnoreCase("plan")) {
            for (int i = 0; i < spins.size(); i++) {
                filename = "plans" + "_" + spins.get(i) + "_" + "MN.jpeg";

                BufferedImage image = null;
                try {
                    url = new URL("https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_366,h_240,c_fill/zxzmn6mmh7gq9o7aat0q");
                } catch (MalformedURLException e1) {
                    e1.printStackTrace();
                }
                try {
                    image = ImageIO.read(url);
                    ImageIO.write(image, "jpg", new File(filename));

                } catch (IOException e) {
                    e.printStackTrace();
                }
                zipFile("../gameofthrones/" + "" + filename);
                HashMap hm = new HashMap();
                hm.put("file", "../gameofthrones/a.zip");
                GameOfThronesService got = new GameOfThronesService("cmsingestionservice", "spinimage", gameofthrones);
                processor = new Processor(got, CMSDailyHelper.headers(), null, null, hm);

            }
        }
        return processor;
    }

    /**
     * Zip a file which is given in Path
     *
     * @param filePath
     */


    public void zipFile(String filePath) {
        FileOutputStream fos = null;
        ZipOutputStream zipOut = null;
        FileInputStream fis = null;
        try {
            fos = new FileOutputStream("a.zip");
            zipOut = new ZipOutputStream(new BufferedOutputStream(fos));
            File input = new File(filePath);
            fis = new FileInputStream(input);
            ZipEntry ze = new ZipEntry(input.getName());
            System.out.println("Zipping the file: " + input.getName());
            zipOut.putNextEntry(ze);
            byte[] tmp = new byte[4 * 1024];
            int size = 0;
            while ((size = fis.read(tmp)) != -1) {
                zipOut.write(tmp, 0, size);
            }
            zipOut.flush();
            zipOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) fos.close();
                if (fis != null) fis.close();
            } catch (Exception ex) {

            }
        }
    }

    /**
     * Delete a file with Given a extension
     *
     * @param filepath
     * @param extension
     */

    public void deleteFile(String filepath, String extension) {
        File folder = new File(filepath);
        for (File file : folder.listFiles()) {
            if (file.getName().endsWith(extension)) {
                file.delete();
            }
        }
    }

    /***
     * Fetch All Spins For Store
     * @param store
     * @return
     */

    public List<String> fetchAllSpinForStore(String store) {
        String queryparam[] = {store};
        GameOfThronesService service = new GameOfThronesService("cmsskuservice", "spinsforstore", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        String spins = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$").toString().replace("[", "").replace("]", "").replace("\"", "");
        List<String> spinlist = Arrays.asList(spins.split("\\s*,\\s*"));
        return spinlist;

    }

    /***
     * Fetch All Spins For Store by Category-ID(Addon-1a244535-7c81-4521-9417-cbf4c8bc8a7a,Plan-3a525919-94aa-4417-8c6d-a4d9f891023b,Product-d0346f81-7470-446a-ace8-961ef06aeb72)
     * @param store
     * @param catid
     * @return
     */

    public List<String> fetchAllSpinForStoreByProductLine(String store, String catid) {
        String queryparam[] = {store};
        List<String> li = new ArrayList<>();
        GameOfThronesService service = new GameOfThronesService("cmsskuservice", "skusforstore", gameofthrones);
        processor = new Processor(service, CMSDailyHelper.headers(), null, queryparam);
        String sku = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$..id").toString().replace("[", "").replace("]", "").replace("\"", "");
        List<String> skus = Arrays.asList(sku.split("\\s*,\\s*"));
        for (int i = 0; i < skus.size(); i++) {
            String cat_id = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$[" + i + "]..category_id").toString().replace("[", "").replace("]", "").replace("\"", "");
            String spin = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$[" + i + "]..spin").toString().replace("[", "").replace("]", "").replace("\"", "");
            if (cat_id.equalsIgnoreCase(catid)) {
                li.add(spin);
            }

        }
        return li;

    }

    /**
     *
     * @param product_id
     * @param store_id
     * @param sku_id
     * @param spin
     * @param id
     * @param entitytype
     * @param interval
     * @param days
     * @param freq
     * @param starttime
     * @param endtime
     * @param live_date
     * @param exp_date
     * @param is_exclusive
     * @return
     */


    public Processor updateAvailSlot(String product_id, String store_id, String sku_id, String spin, Long id, String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive) {
        String eid = store_id + "-" + spin;
        scu.setDataAvail(eid, product_id, store_id, sku_id, spin, id, entitytype, interval, days, freq, starttime, endtime, live_date, exp_date, is_exclusive);
        ArrayList ar = new ArrayList();
        ar.add(scu);
        try {
            GameOfThronesService service = new GameOfThronesService("slotservice", "updateslot", gameofthrones);
            processor = new Processor(service, CMSDailyHelper.headers(), new String[]{mapper.writeValueAsString(ar)}, null);
            return processor;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This function will check availabilty of plan which have one or more than one slot for date.
     *
     * @param restid
     * @param pid
     * @param startepoc
     * @param endepoc
     * @return
     */


    public Boolean availCheckPlan(String restid,String pid, Long startepoc, Long endepoc) {
        String response = null;
        int count=0;
        String entityid=restid+"-"+pid;
        try {
            response = helper.timeline(restid, "PLAN").ResponseValidator.GetBodyAsText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            AvailabiltityResponse availResponse = mapper.readValue(response, AvailabiltityResponse.class);
            for(int i=0;i<availResponse.getData().size();i++) {
                count++;
                if (availResponse.getData().get(i).getEntity_id().equals(entityid)) {
                    for (AvailSlot slot : availResponse.getData().get(count-1).getData().getSlot_timings()) {
                        if ((slot.getStart_epoch() <= startepoc && startepoc <= slot.getEnd_epoch()) || // when startTime in mid on timeline
                                (slot.getStart_epoch() <= endepoc && endepoc <= slot.getEnd_epoch()) ||
                                (startepoc <= slot.getStart_epoch() && slot.getStart_epoch() <= endepoc) || // when startTime in mid on timeline
                                (startepoc <= slot.getEnd_epoch() && slot.getEnd_epoch() <= endepoc)) {
                            return true;
                        }
                    }
                }
            }
            return false;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}