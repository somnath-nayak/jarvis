
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "deliveryFeeType",
    "deliveryFeeDisplay",
    "deliveryFeeMessage"
})
public class DeliveryFeeList {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("deliveryFeeType")
    private String deliveryFeeType;
    @JsonProperty("deliveryFeeDisplay")
    private String deliveryFeeDisplay;
    @JsonProperty("deliveryFeeMessage")
    private String deliveryFeeMessage;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public DeliveryFeeList withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public DeliveryFeeList withTotal(Integer total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public DeliveryFeeList withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("deliveryFeeType")
    public String getDeliveryFeeType() {
        return deliveryFeeType;
    }

    @JsonProperty("deliveryFeeType")
    public void setDeliveryFeeType(String deliveryFeeType) {
        this.deliveryFeeType = deliveryFeeType;
    }

    public DeliveryFeeList withDeliveryFeeType(String deliveryFeeType) {
        this.deliveryFeeType = deliveryFeeType;
        return this;
    }

    @JsonProperty("deliveryFeeDisplay")
    public String getDeliveryFeeDisplay() {
        return deliveryFeeDisplay;
    }

    @JsonProperty("deliveryFeeDisplay")
    public void setDeliveryFeeDisplay(String deliveryFeeDisplay) {
        this.deliveryFeeDisplay = deliveryFeeDisplay;
    }

    public DeliveryFeeList withDeliveryFeeDisplay(String deliveryFeeDisplay) {
        this.deliveryFeeDisplay = deliveryFeeDisplay;
        return this;
    }

    @JsonProperty("deliveryFeeMessage")
    public String getDeliveryFeeMessage() {
        return deliveryFeeMessage;
    }

    @JsonProperty("deliveryFeeMessage")
    public void setDeliveryFeeMessage(String deliveryFeeMessage) {
        this.deliveryFeeMessage = deliveryFeeMessage;
    }

    public DeliveryFeeList withDeliveryFeeMessage(String deliveryFeeMessage) {
        this.deliveryFeeMessage = deliveryFeeMessage;
        return this;
    }

    public DeliveryFeeList setDefaultData()  {
        return this.withTotalWithoutDiscount(SubscriptionConstant.DELIVERYFEELIST_TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.DELIVERYFEELIST_TOTAL)
                .withDiscount(SubscriptionConstant.DELIVERYPRICEDETAILS_DISCOUNT)
                .withDeliveryFeeType(SubscriptionConstant.DELIVERYFEETYPE)
                .withDeliveryFeeDisplay(SubscriptionConstant.DELIVERYFEEDISPLAY)
                .withDeliveryFeeMessage(SubscriptionConstant.DELIVERYFEEMESSAGE);

    }


}
