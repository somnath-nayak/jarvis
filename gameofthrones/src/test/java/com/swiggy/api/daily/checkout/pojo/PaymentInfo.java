package com.swiggy.api.daily.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "transaction_amount",
        "order_context",
        "payment_type",
        "payment_method",
        "metadata"
})
public class PaymentInfo {

    @JsonProperty("transaction_amount")
    private Integer transactionAmount;
    @JsonProperty("order_context")
    private String orderContext;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("payment_method")
    private String paymentMethod;
    @JsonProperty("metadata")
    private String metadata;

    @JsonProperty("transaction_amount")
    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    @JsonProperty("transaction_amount")
    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    @JsonProperty("order_context")
    public String getOrderContext() {
        return orderContext;
    }

    @JsonProperty("order_context")
    public void setOrderContext(String orderContext) {
        this.orderContext = orderContext;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @JsonProperty("payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("payment_method")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty("metadata")
    public String getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("transactionAmount", transactionAmount).append("orderContext", orderContext).append("paymentType", paymentType).append("paymentMethod", paymentMethod).append("metadata", metadata).toString();
    }

}