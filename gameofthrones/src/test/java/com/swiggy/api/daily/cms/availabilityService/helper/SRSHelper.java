package com.swiggy.api.daily.cms.availabilityService.helper;

import com.swiggy.api.daily.cms.availabilityService.pojo.AreaSlotPOJO;
import com.swiggy.api.daily.cms.pojo.Slot.SlotCreate;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class SRSHelper {

    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();

    public Processor citySlotCreationService(String city) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta", "{\"source\":\"Neha-vendor-automation\"}");
        GameOfThronesService service = new GameOfThronesService("srs", "citySlotCreationService", gameofthrones);
        String[] payloadparams = new String[] {city};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor areaSlotCreationService(String area) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta", "{\"source\":\"Neha-vendor-automation\"}");
        GameOfThronesService service = new GameOfThronesService("srs", "areaSlotCreationService", gameofthrones);
        String[] payloadparams = new String[] {area};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor createNormalRestaurantSlot(String data) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta","{\"source\" : \"Neha-vendor-automation\" }");
        GameOfThronesService service = new GameOfThronesService("srs", "normalRestSlot", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor createItemSlot(String data) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user-info","neha.pandey@swiggy.in");
        requestHeaders.put("service","DAILY");
        GameOfThronesService service = new GameOfThronesService("slotservice", "createslot", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor areaSlotCreation(AreaSlotPOJO area) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta", "{\"source\":\"Neha-vendor-automation\"}");
        GameOfThronesService service = new GameOfThronesService("srs", "areaSlotCreationService", gameofthrones);
        String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(area)};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor createItemSlotService(List<SlotCreate> data) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user-info","neha.pandey@swiggy.in");
        requestHeaders.put("service","DAILY");
        GameOfThronesService service = new GameOfThronesService("slotservice", "createslot", gameofthrones);
        String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(data)};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

}
