package com.swiggy.api.daily.promotions.pojo.updatePromotion;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

public class Rules {
    
    @JsonProperty("fact")
    private String fact;
    @JsonProperty("comparisonOperator")
    private  String comparisonOperator;
    @JsonProperty("values")
    private String[] values;
    
    public Rules(){
    }
    
    // Getter & Setter
    public String getFact() {
        return fact;
    }
    
    public void setFact(String fact) {
        this.fact = fact;
    }
    
    public String getComparisonOperator() {
        return comparisonOperator;
    }
    
    public void setComparisonOperator(String comparisonOperator) {
        this.comparisonOperator = comparisonOperator;
    }
    
    public String[] getValues() {
        return values;
    }
    
    public void setValues(String[] values) {
        this.values = values;
    }
    
   // builder
    
    public Rules withFact(String fact){
        setFact(fact);
        return this;
    }
    
    public Rules withComparisonOperator(String comparisonOperator){
        setComparisonOperator(comparisonOperator);
        return this;
    }
    
    public Rules withValues(String[] values){
        setValues(values);
        return this;
    }
    
    public Rules withValues(String fact, String comparisonOperator, String[] values){
    
        withFact(fact).withComparisonOperator(comparisonOperator).withValues(values);
    
        return this;
    }
    
    public Rules withDefaultValues(){
        
        withFact("STORE_ID").withComparisonOperator("EQUAL").withValues(new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        
        return this;
    }
    
}
