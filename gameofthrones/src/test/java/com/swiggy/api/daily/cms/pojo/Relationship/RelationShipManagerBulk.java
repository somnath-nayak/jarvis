
package com.swiggy.api.daily.cms.pojo.Relationship;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "create_update_relations",
})
public class RelationShipManagerBulk {

    @JsonProperty("create_update_relations")
    private List<Create_update_relation> create_update_relations = null;

    @JsonProperty("create_update_relations")
    public List<Create_update_relation> getCreate_update_relations() {
        return create_update_relations;
    }

    @JsonProperty("create_update_relations")
    public void setCreate_update_relations(List<Create_update_relation> create_update_relations) {
        this.create_update_relations = create_update_relations;
    }


    public RelationShipManagerBulk setData(String spid,String sstoreid,String dstoreid,String dspinid,String retype,String id){
        List l=new ArrayList();
        List l1=new ArrayList();
        Create_update_relation cr=new Create_update_relation();
        Source sr=new Source();
        Destination ds=new Destination();
        Schedule s=new Schedule();
        Relationship_data rs=new Relationship_data();
        s.setStart_epoch(CMSDailyHelper.createEPOCforCurrent());
        s.setEnd_epoch(CMSDailyHelper.createEPOCforHour(3));
        l1.add(s);
        sr.setProduct_id(spid);
        sr.setStore_id(sstoreid);
        ds.setStore_id(dstoreid);
        ds.setSpin(dspinid);
        cr.setDestination(ds);
        cr.setSource(sr);
        cr.setId(id);
        rs.setSchedule(l1);
        cr.setRelation_type(retype);
        l.add(cr);
        cr.setRelationship_data(rs);
        this.setCreate_update_relations(l);
        return this;
    }

    public RelationShipManagerBulk setWrongData(Source sr,Destination ds,Relationship_data data,String retype,String id){
        List li=new ArrayList();
        Create_update_relation cr=new Create_update_relation();
        cr.setSource(sr);
        cr.setDestination(ds);
        cr.setRelation_type(retype);
        cr.setRelationship_data(data);
        cr.setId(id);
        li.add(cr);
        this.setCreate_update_relations(li);
        return this;
    }




}
