package com.swiggy.api.daily.cms.availabilityService.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.io.IOException;
import java.util.HashMap;

public class AvailServiceHelper {

    static Initialize gameofthrones = new Initialize();

    public Processor city(String city) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "city", gameofthrones);
        String[] urlparams = new String[] {city};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor area(String area) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "area", gameofthrones);
        String[] urlparams = new String[] {area};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor store(String store) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "store", gameofthrones);
        String[] urlparams = new String[] {store};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor plan(String plan) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "plan", gameofthrones);
        String[] urlparams = new String[] {plan};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor product(String product) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "product", gameofthrones);
        String[] urlparams = new String[] {product};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor timeline(String restaurantID, String entityType) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("slotservice", "timeline", gameofthrones);
        String[] urlparams = new String[] {restaurantID,entityType};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

}
