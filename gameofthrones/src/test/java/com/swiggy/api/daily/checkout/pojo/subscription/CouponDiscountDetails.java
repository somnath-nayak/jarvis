package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class CouponDiscountDetails {


    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("couponDiscount")
    private Long couponDiscount;
    @JsonProperty("couponMessage")
    private String couponMessage;

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public CouponDiscountDetails withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("couponDiscount")
    public Long getCouponDiscount() {
        return couponDiscount;
    }

    @JsonProperty("couponDiscount")
    public void setCouponDiscount(Long couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public CouponDiscountDetails withCouponDiscount(Long couponDiscount) {
        this.couponDiscount = couponDiscount;
        return this;
    }

    @JsonProperty("couponMessage")
    public String getCouponMessage() {
        return couponMessage;
    }

    @JsonProperty("couponMessage")
    public void setCouponMessage(String couponMessage) {
        this.couponMessage = couponMessage;
    }

    public CouponDiscountDetails withCouponMessage(String couponMessage) {
        this.couponMessage = couponMessage;
        return this;
    }
}
