
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "startDate",
    "endDate",
    "excludedDates",
    "tenure",
    "hasWeekend"
})
public class DeliverySchedule {

    @JsonProperty("startDate")
    private Long startDate;
    @JsonProperty("endDate")
    private Object endDate;
    @JsonProperty("excludedDates")
    private List<Object> excludedDates = null;
    @JsonProperty("tenure")
    private Integer tenure;
    @JsonProperty("hasWeekend")
    private Boolean hasWeekend;

    @JsonProperty("startDate")
    public Long getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public DeliverySchedule withStartDate(Long startDate) {
        this.startDate = startDate;
        return this;
    }

    @JsonProperty("endDate")
    public Object getEndDate() {
        return endDate;
    }

    @JsonProperty("endDate")
    public void setEndDate(Object endDate) {
        this.endDate = endDate;
    }

    public DeliverySchedule withEndDate(Object endDate) {
        this.endDate = endDate;
        return this;
    }

    @JsonProperty("excludedDates")
    public List<Object> getExcludedDates() {
        return excludedDates;
    }

    @JsonProperty("excludedDates")
    public void setExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
    }

    public DeliverySchedule withExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
        return this;
    }

    @JsonProperty("tenure")
    public Integer getTenure() {
        return tenure;
    }

    @JsonProperty("tenure")
    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public DeliverySchedule withTenure(Integer tenure) {
        this.tenure = tenure;
        return this;
    }

    @JsonProperty("hasWeekend")
    public Boolean getHasWeekend() {
        return hasWeekend;
    }

    @JsonProperty("hasWeekend")
    public void setHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
    }

    public DeliverySchedule withHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
        return this;
    }

    public DeliverySchedule setDefaultData()  {
        return this.withStartDate(DateUtility.getCurrentDateInMilisecond())
                .withEndDate(null)
                .withExcludedDates(new ArrayList<Object>() {{add(" ");}})
                .withTenure(SubscriptionConstant.DELIVERYSCHEDULE_TENURE)
                .withHasWeekend(SubscriptionConstant.DELIVERYSCHEDULE_HASWEEKEND);
    }

}
