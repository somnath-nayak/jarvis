
package com.swiggy.api.daily.mealSlot.pojo;

import com.swiggy.api.daily.mealSlot.helper.MealSlotConstant;
import com.swiggy.automation.common.utils.TimeUtils;
import org.codehaus.jackson.annotate.*;

import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "city_id",
        "slot",
        "start_time",
        "end_time",
        "created_by"
})
public class MealSlotsPOJO {

    @JsonProperty("city_id")
    private String cityId;
    @JsonProperty("slot")
    private String slot;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("created_by")
    private String createdBy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * No args constructor for use in serialization
     *
     */
    public MealSlotsPOJO() {
    }

    /**
     *
     * @param startTime
     * @param createdBy
     * @param cityId
     * @param endTime
     * @param slot
     */
    public MealSlotsPOJO(String cityId, String slot, String startTime, String endTime, String createdBy) {
        super();
        this.cityId = cityId;
        this.slot = slot;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createdBy = createdBy;
    }

    @JsonProperty("city_id")
    public String getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public MealSlotsPOJO withCityId(String cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("slot")
    public String getSlot() {
        return slot;
    }

    @JsonProperty("slot")
    public void setSlot(String slot) {
        this.slot = slot;
    }

    public MealSlotsPOJO withSlot(String slot) {
        this.slot = slot;
        return this;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public MealSlotsPOJO withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public MealSlotsPOJO withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public MealSlotsPOJO withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public MealSlotsPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public MealSlotsPOJO setDefaultData()
    {
//        setCityId((int)(Math. random() * 50)+"");
        setCityId(MealSlotConstant.cityID);
        setSlot(MealSlotConstant.LUNCH);
        setStartTime(TimeUtils.getCurrentTimeIn24HourFormat()+"");
        setEndTime(TimeUtils.getCurrentTimeIn24HourFormat()+200+"");
        setCreatedBy("user@swiggy.in");
        return this;
    }

}
