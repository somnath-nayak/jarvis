package com.swiggy.api.daily.checkout.pojo.slotdetails;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class Request {


    @JsonProperty("customer_locations")
    private List<CustomerLocation> customerLocations = null;
    @JsonProperty("store_ids")
    private List<String> storeIds = null;
    @JsonProperty("city_id")
    private int cityId;
    @JsonProperty("time_slot")
    private TimeSlot timeSlot;
    @JsonProperty("exclusion_dates")
    private List<Object> exclusionDates = null;

    @JsonProperty("customer_locations")
    public List<CustomerLocation> getCustomerLocations() {
        return customerLocations;
    }

    @JsonProperty("customer_locations")
    public void setCustomerLocations(List<CustomerLocation> customerLocations) {
        this.customerLocations = customerLocations;
    }

    @JsonProperty("store_ids")
    public List<String> getStoreIds() {
        return storeIds;
    }

    @JsonProperty("store_ids")
    public void setStoreIds(List<String> storeIds) {
        this.storeIds = storeIds;
    }

    @JsonProperty("city_id")
    public int getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    @JsonProperty("time_slot")
    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    @JsonProperty("time_slot")
    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }

    @JsonProperty("exclusion_dates")
    public List<Object> getExclusionDates() {
        return exclusionDates;
    }

    @JsonProperty("exclusion_dates")
    public void setExclusionDates(List<Object> exclusionDates) {
        this.exclusionDates = exclusionDates;
    }



}
