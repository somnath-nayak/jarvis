package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "from_time",
        "restaurant_id",
        "to_time"
})
public class HolidaySlotPOJO {

    @JsonProperty("from_time")
    private String fromTime;
    @JsonProperty("restaurant_id")
    private String restaurantId;
    @JsonProperty("to_time")
    private String toTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public HolidaySlotPOJO() {
    }

    /**
     *
     * @param fromTime
     * @param toTime
     * @param restaurantId
     */
    public HolidaySlotPOJO(String fromTime, String restaurantId, String toTime) {
        super();
        this.fromTime = fromTime;
        this.restaurantId = restaurantId;
        this.toTime = toTime;
    }

    @JsonProperty("from_time")
    public String getFromTime() {
        return fromTime;
    }

    @JsonProperty("from_time")
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public HolidaySlotPOJO withFromTime(String fromTime) {
        this.fromTime = fromTime;
        return this;
    }

    @JsonProperty("restaurant_id")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public HolidaySlotPOJO withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonProperty("to_time")
    public String getToTime() {
        return toTime;
    }

    @JsonProperty("to_time")
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public HolidaySlotPOJO withToTime(String toTime) {
        this.toTime = toTime;
        return this;
    }

    public HolidaySlotPOJO setDefault(String from_time, String rest_id, String to_time) {
        return this.withFromTime(from_time).withRestaurantId(rest_id).withToTime(to_time);
    }

}
