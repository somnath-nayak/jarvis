
package com.swiggy.api.daily.cms.pojo.PlanMeta;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "store_plan_ids",
    "start-time",
    "end-time",
    "day-o-start-time"
})
public class AvailablePlanMeta {

    @JsonProperty("store_plan_ids")
    private List<Store_plan_id> store_plan_ids = null;
    @JsonProperty("start-time")
    private Long start_time;
    @JsonProperty("end-time")
    private Long end_time;
    @JsonProperty("day-o-start-time")
    private Long day_o_start_time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public AvailablePlanMeta() {
    }

    /**
     * 
     * @param day_o_start_time
     * @param end_time
     * @param start_time
     * @param store_plan_ids
     */
    public AvailablePlanMeta(List<Store_plan_id> store_plan_ids, Long start_time, Long end_time, Long day_o_start_time) {
        super();
        this.store_plan_ids = store_plan_ids;
        this.start_time = start_time;
        this.end_time = end_time;
        this.day_o_start_time = day_o_start_time;
    }

    @JsonProperty("store_plan_ids")
    public List<Store_plan_id> getStore_plan_ids() {
        return store_plan_ids;
    }

    @JsonProperty("store_plan_ids")
    public void setStore_plan_ids(List<Store_plan_id> store_plan_ids) {
        this.store_plan_ids = store_plan_ids;
    }

    public AvailablePlanMeta withStore_plan_ids(List<Store_plan_id> store_plan_ids) {
        this.store_plan_ids = store_plan_ids;
        return this;
    }

    @JsonProperty("start-time")
    public Long getStart_time() {
        return start_time;
    }

    @JsonProperty("start-time")
    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public AvailablePlanMeta withStart_time(Long start_time) {
        this.start_time = start_time;
        return this;
    }

    @JsonProperty("end-time")
    public Long getEnd_time() {
        return end_time;
    }

    @JsonProperty("end-time")
    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public AvailablePlanMeta withEnd_time(Long end_time) {
        this.end_time = end_time;
        return this;
    }

    @JsonProperty("day-o-start-time")
    public Long getDay_o_start_time() {
        return day_o_start_time;
    }

    @JsonProperty("day-o-start-time")
    public void setDay_o_start_time(Long day_o_start_time) {
        this.day_o_start_time = day_o_start_time;
    }

    public AvailablePlanMeta withDay_o_start_time(Long day_o_start_time) {
        this.day_o_start_time = day_o_start_time;
        return this;
    }

    public AvailablePlanMeta setValue(Store_plan_id sc, Long start_time, Long end_time,Long o_start_time){
        this.withStore_plan_ids(new ArrayList<>(Arrays.asList(sc))).withStart_time(start_time).withEnd_time(end_time).
                withDay_o_start_time(o_start_time);
        return this;
    }

}
