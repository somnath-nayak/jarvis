package com.swiggy.api.daily.cms.pojo.Relationship;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id",
        "product_id"
})
public class Source {

    @JsonProperty("store_id")
    private String store_id;
    @JsonProperty("product_id")
    private String product_id;

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    @JsonProperty("product_id")
    public String getProduct_id() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

}
