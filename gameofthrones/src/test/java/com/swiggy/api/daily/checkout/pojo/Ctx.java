package com.swiggy.api.daily.checkout.pojo;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "slot_details",
        "start_date",
        "excluded_dates",
        "has_weekend",
        "meal_slot_id"
})
public class Ctx {

    @JsonProperty("slot_details")
    private SlotDetails slotDetails;
    @JsonProperty("start_date")
    private Long startDate;
    @JsonProperty("excluded_dates")
    private List<Object> excludedDates = null;
    @JsonProperty("has_weekend")
    private Boolean hasWeekend;
    @JsonProperty("meal_slot_id")
    private String mealSlotId;

    @JsonProperty("slot_details")
    public SlotDetails getSlotDetails() {
        return slotDetails;
    }

    @JsonProperty("slot_details")
    public void setSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
    }

    @JsonProperty("start_date")
    public Long getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("excluded_dates")
    public List<Object> getExcludedDates() {
        return excludedDates;
    }

    @JsonProperty("excluded_dates")
    public void setExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
    }

    @JsonProperty("has_weekend")
    public Boolean getHasWeekend() {
        return hasWeekend;
    }

    @JsonProperty("has_weekend")
    public void setHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
    }

    @JsonProperty("meal_slot_id")
    public String getMealSlotId() {
        return mealSlotId;
    }

    @JsonProperty("meal_slot_id")
    public void setMealSlotId(String mealSlotId) {
        this.mealSlotId = mealSlotId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("slotDetails", slotDetails).append("startDate", startDate).append("excludedDates", excludedDates).append("hasWeekend", hasWeekend).append("mealSlotId", mealSlotId).toString();
    }

}