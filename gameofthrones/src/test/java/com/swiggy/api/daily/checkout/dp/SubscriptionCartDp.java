package com.swiggy.api.daily.checkout.dp;

import com.swiggy.api.daily.checkout.helper.CheckoutCartHelper;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutCommonUtils;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutConstants;
import com.swiggy.api.daily.checkout.pojo.CartPayload;
import com.swiggy.api.daily.checkout.pojo.UserInfo;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;

import java.util.Map;

public class SubscriptionCartDp {

    private CheckoutCartHelper cartHelper;
    private DailyCheckoutCommonUtils commonUtils;
    private UserInfo userInfo;
    private String mobile;
    private String password;
    private Map<String, String> headerWithTidAndSid;
    private Map<String, String> allHeaders;
    private Map<String, String> userInfoHeader;


    public SubscriptionCartDp() {
        cartHelper = new CheckoutCartHelper();
        userInfo = new UserInfo();
        mobile = System.getenv("mobile");
        password = System.getenv("password");
        commonUtils = new DailyCheckoutCommonUtils();

        if (mobile == null || mobile.isEmpty()) {
            mobile = DailyCheckoutConstants.USER_MOBILE;
        }

        if (password == null || password.isEmpty()) {
            password = DailyCheckoutConstants.USER_PASSWORD;
        } else {
            System.out.println("Please provide mobile and password");
        }

        allHeaders = commonUtils.dailyLogin(mobile, password);
        userInfoHeader = commonUtils.getUserInfoHeader(allHeaders);
        headerWithTidAndSid = commonUtils.getHeader(userInfoHeader);

    }


    @DataProvider(name = "slotVerification")
    public Object[][] slotVerification() {
        CartPayload cartPayload = cartHelper.getDailyCartPayload("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID);


        Object[][] data = null;

        data = new Object[][]{
                {"plan", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID}
        };
        return data;
    }


    @DataProvider(name = "itemDetailsVerification")
    public Object[][] itemDetailsVerification() {
        return new Object[][]{

                {"plan", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID},
                {"product", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID},
                {"addons", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID}


        };
    }


    @DataProvider(name = "mealSlotVerification")
    public Object[][] mealSlotVerification() {
        return new Object[][]{

                {"plan", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, "lunch"},
                {"plan", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID,"dinner"},
                {"product", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, "lunch"},
                {"product", DailyCheckoutConstants.STOREID, userInfoHeader, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID,"dinner"}


        };
    }
}
