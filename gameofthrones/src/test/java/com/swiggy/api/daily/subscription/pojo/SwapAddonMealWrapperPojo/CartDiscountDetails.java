
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "tradeDiscount",
    "tradeDiscountMessage",
    "swiggyDiscount",
    "swiggyDiscountMessage"
})
public class CartDiscountDetails {

    @JsonProperty("tradeDiscount")
    private Integer tradeDiscount;
    @JsonProperty("tradeDiscountMessage")
    private String tradeDiscountMessage;
    @JsonProperty("swiggyDiscount")
    private Integer swiggyDiscount;
    @JsonProperty("swiggyDiscountMessage")
    private String swiggyDiscountMessage;

    @JsonProperty("tradeDiscount")
    public Integer getTradeDiscount() {
        return tradeDiscount;
    }

    @JsonProperty("tradeDiscount")
    public void setTradeDiscount(Integer tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    public CartDiscountDetails withTradeDiscount(Integer tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
        return this;
    }

    @JsonProperty("tradeDiscountMessage")
    public String getTradeDiscountMessage() {
        return tradeDiscountMessage;
    }

    @JsonProperty("tradeDiscountMessage")
    public void setTradeDiscountMessage(String tradeDiscountMessage) {
        this.tradeDiscountMessage = tradeDiscountMessage;
    }

    public CartDiscountDetails withTradeDiscountMessage(String tradeDiscountMessage) {
        this.tradeDiscountMessage = tradeDiscountMessage;
        return this;
    }

    @JsonProperty("swiggyDiscount")
    public Integer getSwiggyDiscount() {
        return swiggyDiscount;
    }

    @JsonProperty("swiggyDiscount")
    public void setSwiggyDiscount(Integer swiggyDiscount) {
        this.swiggyDiscount = swiggyDiscount;
    }

    public CartDiscountDetails withSwiggyDiscount(Integer swiggyDiscount) {
        this.swiggyDiscount = swiggyDiscount;
        return this;
    }

    @JsonProperty("swiggyDiscountMessage")
    public String getSwiggyDiscountMessage() {
        return swiggyDiscountMessage;
    }

    @JsonProperty("swiggyDiscountMessage")
    public void setSwiggyDiscountMessage(String swiggyDiscountMessage) {
        this.swiggyDiscountMessage = swiggyDiscountMessage;
    }

    public CartDiscountDetails withSwiggyDiscountMessage(String swiggyDiscountMessage) {
        this.swiggyDiscountMessage = swiggyDiscountMessage;
        return this;
    }

    public CartDiscountDetails setDefaultData()  {
        return this.withTradeDiscount(SubscriptionConstant.TRADEDISCOUNT)
                .withTradeDiscountMessage("")
                .withSwiggyDiscount(SubscriptionConstant.SWIGGYDISCOUNT)
                .withSwiggyDiscountMessage("");

    }

}
