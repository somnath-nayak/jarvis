
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "itemTotalCharges",
    "cartPackingCharges",
    "deliveryPriceDetails",
    "cartDiscountDetails",
    "couponDiscountDetails"
})
public class PricingDetail {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("itemTotalCharges")
    private ItemTotalCharges itemTotalCharges;
    @JsonProperty("cartPackingCharges")
    private CartPackingCharges cartPackingCharges;
    @JsonProperty("deliveryPriceDetails")
    private DeliveryPriceDetails deliveryPriceDetails;
    @JsonProperty("cartDiscountDetails")
    private CartDiscountDetails cartDiscountDetails;
    @JsonProperty("couponDiscountDetails")
    private CouponDiscountDetails couponDiscountDetails;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public PricingDetail withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public PricingDetail withTotal(Integer total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public PricingDetail withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("itemTotalCharges")
    public ItemTotalCharges getItemTotalCharges() {
        return itemTotalCharges;
    }

    @JsonProperty("itemTotalCharges")
    public void setItemTotalCharges(ItemTotalCharges itemTotalCharges) {
        this.itemTotalCharges = itemTotalCharges;
    }

    public PricingDetail withItemTotalCharges(ItemTotalCharges itemTotalCharges) {
        this.itemTotalCharges = itemTotalCharges;
        return this;
    }

    @JsonProperty("cartPackingCharges")
    public CartPackingCharges getCartPackingCharges() {
        return cartPackingCharges;
    }

    @JsonProperty("cartPackingCharges")
    public void setCartPackingCharges(CartPackingCharges cartPackingCharges) {
        this.cartPackingCharges = cartPackingCharges;
    }

    public PricingDetail withCartPackingCharges(CartPackingCharges cartPackingCharges) {
        this.cartPackingCharges = cartPackingCharges;
        return this;
    }

    @JsonProperty("deliveryPriceDetails")
    public DeliveryPriceDetails getDeliveryPriceDetails() {
        return deliveryPriceDetails;
    }

    @JsonProperty("deliveryPriceDetails")
    public void setDeliveryPriceDetails(DeliveryPriceDetails deliveryPriceDetails) {
        this.deliveryPriceDetails = deliveryPriceDetails;
    }

    public PricingDetail withDeliveryPriceDetails(DeliveryPriceDetails deliveryPriceDetails) {
        this.deliveryPriceDetails = deliveryPriceDetails;
        return this;
    }

    @JsonProperty("cartDiscountDetails")
    public CartDiscountDetails getCartDiscountDetails() {
        return cartDiscountDetails;
    }

    @JsonProperty("cartDiscountDetails")
    public void setCartDiscountDetails(CartDiscountDetails cartDiscountDetails) {
        this.cartDiscountDetails = cartDiscountDetails;
    }

    public PricingDetail withCartDiscountDetails(CartDiscountDetails cartDiscountDetails) {
        this.cartDiscountDetails = cartDiscountDetails;
        return this;
    }

    @JsonProperty("couponDiscountDetails")
    public CouponDiscountDetails getCouponDiscountDetails() {
        return couponDiscountDetails;
    }

    @JsonProperty("couponDiscountDetails")
    public void setCouponDiscountDetails(CouponDiscountDetails couponDiscountDetails) {
        this.couponDiscountDetails = couponDiscountDetails;
    }

    public PricingDetail withCouponDiscountDetails(CouponDiscountDetails couponDiscountDetails) {
        this.couponDiscountDetails = couponDiscountDetails;
        return this;
    }

    public PricingDetail setDefaultData()  {
        return this.withTotalWithoutDiscount(SubscriptionConstant.PRICINGDETAIL_TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.PRICINGDETAIL_TOTAL)
                .withDiscount(SubscriptionConstant.PRICINGDETAIL_DISCOUNT)
                .withItemTotalCharges(new ItemTotalCharges().setDefaultData())
                .withCartPackingCharges(new CartPackingCharges().setDefaultData())
                .withDeliveryPriceDetails(new DeliveryPriceDetails().setDefaultData())
                .withCartDiscountDetails(new CartDiscountDetails().setDefaultData())
                .withCouponDiscountDetails(new CouponDiscountDetails().setDefaultData());
    }

}
