package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;


public class PricingServiceDaily {

    CMSDailyHelper helper = new CMSDailyHelper();
    List keymeal=new ArrayList();
    List keyplan=new ArrayList();


    @Test(description = "This test will create pricing for a meal with inclusive of tax,without tax,with different variant of price", dataProviderClass = CMSDailyDP.class, dataProvider = "createpricemeal", priority = 0)
    public void createPricingMeal(String key, String product_id, String sku_id, String spin, String store_id, Integer list_price, Integer price, Integer total_price, Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes, Integer sgst_utgst, Integer sgst_utgst_percentage) throws Exception{
        String response = helper.createPricingMeal(key, product_id, sku_id, spin, store_id, list_price, price, total_price, cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        String pid=JsonPath.read(response, "$.data..meta.product_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String storeid=JsonPath.read(response, "$.data..meta.store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String spinid=JsonPath.read(response, "$.data..meta.spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        String skuid=JsonPath.read(response, "$.data..meta.sku_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        keymeal.add(keyact);
        Assert.assertEquals(keyact, key);
        Assert.assertEquals(pid,product_id);
        Assert.assertEquals(storeid,store_id);
        Assert.assertEquals(spinid,spin);
        Assert.assertEquals(skuid,sku_id);


    }

    @Test(description = "This test will create pricing for a meal with inclusive of tax,without tax,with different variant of price", dataProviderClass = CMSDailyDP.class, dataProvider = "createpriceplan", priority = 0)
    public void createPricingPlan(String key, String product_id, String sku_id, String spin, String store_id, Integer list_price, Integer price, Integer total_price, Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes, Integer sgst_utgst, Integer sgst_utgst_percentage) throws Exception{
        String response = helper.createPricingPlan(key, product_id, sku_id, spin, store_id, list_price, price, total_price, cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        String pid=JsonPath.read(response, "$.data..meta.product_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String storeid=JsonPath.read(response, "$.data..meta.store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        String spinid=JsonPath.read(response, "$.data..meta.spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        String skuid=JsonPath.read(response, "$.data..meta.sku_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        keyplan.add(keyact);
        Assert.assertEquals(keyact, key);
        Assert.assertEquals(pid,product_id);
        Assert.assertEquals(storeid,store_id);
        Assert.assertEquals(spinid,spin);
        Assert.assertEquals(skuid,sku_id);

    }

    @Test(description = "This test will fetch pricing for a meal with new created meal")
    public void getPricingMeal(){
        for(int i=0;i<keymeal.size();i++){
        String response = helper.getPricing(keymeal.get(i).toString(), CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyact, keymeal.get(i).toString());}

    }

    @Test(description = "This test will fetch pricing for a plan with with new created plan")
    public void getPricingPlan(){
        for(int i=0;i<keyplan.size();i++){
            String response = helper.getPricing(keyplan.get(i).toString(), CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
            String keyact = JsonPath.read(response, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
            Assert.assertEquals(keyact, keyplan.get(i).toString());}

    }

    @Test(description = "This test will fetch pricing for a plan for which key is not existing",dataProviderClass = CMSDailyDP.class,dataProvider = "getpricingdpnegative")
    public void getPricingPlanNegative(String pricekey){
        String response = helper.getPricing(pricekey, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        List<String> keyact= JsonPath.read(response,"$.data");
        Assert.assertEquals(keyact.size(),0,"Size should be zero");

    }

    @Test(description = "This test will fetch pricing for a meal for which key is not existing",dataProviderClass = CMSDailyDP.class,dataProvider = "getpricingdpnegative")
    public void getPricingMealNegative(String pricekey){
        String response = helper.getPricing(pricekey, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        List<String> keyact= JsonPath.read(response,"$.data");
        Assert.assertEquals(keyact.size(),0,"Size should be zero");

    }
}
