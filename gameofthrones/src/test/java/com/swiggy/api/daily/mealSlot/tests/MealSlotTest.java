package com.swiggy.api.daily.mealSlot.tests;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.mealSlot.dp.MealSlotDP;
import com.swiggy.api.daily.mealSlot.helper.CommonMethodsHelper;
import com.swiggy.api.daily.mealSlot.helper.MealSlotConstant;
import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import com.swiggy.api.daily.mealSlot.pojo.MealSlotsPOJO;
import com.swiggy.api.daily.mealSlot.pojo.UpdateMealSlotsPOJO;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;;


public class MealSlotTest extends MealSlotDP {


    MealSlotHelper mealSlotHelper = new MealSlotHelper();
    CommonMethodsHelper commonMethodsHelper = new CommonMethodsHelper();
    JsonHelper jsonHelper = new JsonHelper();
    SoftAssert softAssert = new SoftAssert();
    String cityID;


    @BeforeTest
    public void executeBeforeTest() throws Exception {
        Processor deleteMealSlot = mealSlotHelper.deleteMeal(MealSlotConstant.cityID);
        Thread.sleep(3000);
    }

//   ========================================================GET Mealslot test-cases=====================================================

    @Test(description = "GET Meal slot - Select the city(mandatory) and time(optional) to validate all the meal-slots available till next 24 hours", groups = {"Sanity", "Neha Pandey"})
    public void getMealSlotSuccessCase() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot - Get the details of the meal-slot available for a valid 'cityId'", groups = {"Regression", "Neha Pandey"})
    public void validCityIdTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot - Error in getting the details of the meal slopt for the non-existant 'cityId'", groups = {"Regression", "Neha Pandey"})
    public void invalidCityIdTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.invalidCityID, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.statusCode1Assert(getByCityId);
    }

    @Test(description = "GET Meal slot - Check whether the start time of the meal-slot is dependent on the cityID and the time from which user accesses the application", groups = {"Regression", "Neha Pandey"})
    public void start_timeParameterTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot - Check whether the end time of the meal-slot is dependent on the cityID and the time in which user accesses the application", groups = {"Regression", "Neha Pandey"})
    public void end_timeParameterTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot - The delivery start time for the service is dependent on the meal-slots available i.e. if a user is accessing the application during breakfast meal-slot, the delivery start time is equivalent to start_time", groups = {"Regression", "Neha Pandey"})
    public void delivery_start_timeParameterTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  If a user enters the application at 12:00 AM night, the active and available meal-slots available squentially must be visible", groups = {"Regression", "Neha Pandey"})
    public void sequenceCheckTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_12_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  If a user enters the application at 9:00 AM, validate the the meal-slot that exist at that time along with the delivery_start_time for the user; i.e. the active meal-slot will be BREAKFAST and the delivery_start_time will be 9:00 AM till the end_time", groups = {"Regression", "Neha Pandey"})
    public void userEntersAt9AMTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the response for BREAKFAST meal-slot", groups = {"Regression", "Neha Pandey"})
    public void validateBREAKFASTmealTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the response for LUNCH meal-slot", groups = {"Regression", "Neha Pandey"})
    public void validateLUNCHmealTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the response for SNACKS meal-slot", groups = {"Regression", "Neha Pandey"})
    public void validateSNACKSmealTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the response for DINNER meal-slot", groups = {"Regression", "Neha Pandey"})
    public void validateDINNERmealTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the response for LATE_NIGHT meal-slot", groups = {"Regression", "Neha Pandey"})
    public void validateLATE_NIGHTmealTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the value for \"day\" to be set as Today or Tonight", groups = {"Regression", "Neha Pandey"})
    public void validateDayParameterSuccessTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_12_AM);
        commonMethodsHelper.TodayOrTonightAssertsGetMealSlotServiceAPI(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the value for \"day\" to be set other than Today or Tonight", groups = {"Regression", "Neha Pandey"})
    public void validateDayParameterErrorTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_12_AM);
        commonMethodsHelper.TodayOrTonightAssertsGetMealSlotServiceAPI(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the meal slot to be set as only as BREAKFAST, LUNCH, SNACKS, DINNER or LATE NIGHT", groups = {"Regression", "Neha Pandey"})
    public void validateSlotParameterSuccessTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        if (getByCityId!=null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
    }

    @Test(description = "GET Meal slot -  Validate the meal slot to be set other than BREAKFAST, LUNCH, SNACKS, DINNER or LATE NIGHT", groups = {"Regression", "Neha Pandey"})
    public void validateSlotParametereRRORTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.TodayOrTonightAssertsGetMealSlotServiceAPI(getByCityId);
    }

    @Test(description = "GET Meal slot -  Validate the meal-slots for a user depending on the timing", groups = {"Regression", "Neha Pandey"})
    public void validateUserDependingOnSlotTimings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.TodayOrTonightAssertsGetMealSlotServiceAPI(getByCityId);

        Processor getByCityId2 = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, ""+DateUtility.getCurrentDateInMilisecond());
        commonMethodsHelper.randomAssertsGetMealSlotServiceAPI(getByCityId2);
    }

    @Test(description = "GET Meal slot -  Get the details of the meal-slot when the user enters the application in-between 12 PM-7AM", groups = {"Regression", "Neha Pandey"})
    public void getDetails12PMto7AMTimings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, MealSlotConstant.time_9_AM);
        commonMethodsHelper.statusCode1Assert(getByCityId);
    }


    @Test(description = "If a user enters the application in between 0000, the sequence of the meal-slot along with day must be displayed as LATE_NIGHT, BREAKFAST, LUNCH, SNACKS, DINNER (Today)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0000Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1543602600000");
        commonMethodsHelper.time0000to0059Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0001, the sequence of the meal-slot along with day must be displayed as LATE_NIGHT, BREAKFAST, LUNCH, SNACKS, DINNER (Today)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0001Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1543602660000");
        commonMethodsHelper.time0000to0059Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0030, the sequence of the meal-slot along with day must be displayed as LATE_NIGHT, BREAKFAST, LUNCH, SNACKS, DINNER (Today)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0030Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546282800000");
        commonMethodsHelper.time0000to0059Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0100, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0100Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546284600000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0130, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0130Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546286400000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0200, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0200Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546288200000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0230, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0230Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546290000000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0300, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0300Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546291800000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0330, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0330Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546293600000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0400, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0400Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546295400000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0430, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight) Regression, Neha Pandey", groups = {"Regression", "Neha Pandey"})
    public void getDetails0430Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546297200000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0500, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0500Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546299000000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0530, the sequence of the meal-slot along with day must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER (Today),LATE_NIGHT (Tonight)", groups = {"Regression", "Neha Pandey"})
    public void getDetails0530Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546300800000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0600, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0600Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546302600000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0630, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0630Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546304400000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0700, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0700Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546306200000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0730, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0730Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546308000000");
        commonMethodsHelper.time0100to0759Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0800, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0800Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546309800000");

    }

    @Test(description = "If a user enters the application in between 0830, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0830Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546311600000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0900, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0900Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546313400000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 0930, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails0930Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546315200000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1000, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1000Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546317000000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1030, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1030Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546318800000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1100, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1100Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546320600000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1130, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1130Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546322400000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1200, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1200Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546324200000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1230, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1230Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546326000000");
        commonMethodsHelper.time0800to1259Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1300, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1300Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546327800000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1330, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1330Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546329600000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1400, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1400Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546331400000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1430, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1430Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546333200000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1500, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1500Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546335000000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1530, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1530Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546336800000");
        commonMethodsHelper.time1300to1559Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1600, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1600Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546338600000");

    }

    @Test(description = "If a user enters the application in between 1630, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1630Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546340400000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1700, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1700Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546342200000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1730, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1730Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546344000000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1800, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1800Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546345800000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1830, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1830Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546347600000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1900, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1900Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546349400000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 1930, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails1930Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546351200000");
        commonMethodsHelper.time1600to1959Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2000, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2000Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546353000000");

    }

    @Test(description = "If a user enters the application in between 2030, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2030Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546354800000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2100, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2100Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546356600000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2130, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2130Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546358400000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2200, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2200Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546360200000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2230, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2230Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546362000000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2300, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2300Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546363800000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "If a user enters the application in between 2330, the sequence of the meal-slot must be displayed as BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void getDetails2330Timings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1546365600000");
        commonMethodsHelper.time2000to2359Asserts(getByCityId);
    }

    @Test(description = "For LATE_NIGHT meal-slot, the response validation for delivery_start_time will consist of slots of 45 minutes which may fall under today or tonight category and not tomorrow category, validate the varoius delivery_start_time", groups = {"Regression", "Neha Pandey"})
    public void getDetailsLATE_NIGHTTimings() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1543681800000");
        commonMethodsHelper.statusCode1Assert(getByCityId);

        Processor getByCityId1 = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1543687200000");
        commonMethodsHelper.statusCode1Assert(getByCityId1);
    }

    @Test(description = "Trying to get the details of a duplicate meal-slot for a particular day which is not possible as creation of a duplication meal-slot isn't possible", groups = {"Regression", "Neha Pandey"})
    public void getDetailsDuplicateTest() throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID_99, "1543681800000");
        commonMethodsHelper.statusCode1Assert(getByCityId);
    }

//   ========================================================Create Meal slot test-cases====================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a BREAKFAST meal-slot for a particular city starting from 7AM to 10AM", groups = {"Sanity", "Neha Pandey"})
    public void createBREAKFASTMealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.BREAKFAST);
            mealSlotsPOJO.withStartTime(MealSlotConstant._300);
            mealSlotsPOJO.withEndTime(MealSlotConstant._700);
            String cityid = null;
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot for city 0", groups = {"Regression", "Neha Pandey"})
    public void createcITYiD0errorCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
            mealSlotsPOJO.withCityId(MealSlotConstant.cityID_0+"");
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.invalidCreateCityID(createMealslot);
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a LUNCH meal-slot for a particular city starting from 12 PM till 15 PM", groups = {"Regression", "Neha Pandey"})
    public void createLUNCHMealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withStartTime(MealSlotConstant._1200);
            mealSlotsPOJO.setEndTime(MealSlotConstant._1500);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a SNACKS meal-slot for a particular city starting from 1600 till 1800", groups = {"Regression", "Neha Pandey"})
    public void createSNACKSealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.SNACKS);
            mealSlotsPOJO.withStartTime(MealSlotConstant._1600);
            mealSlotsPOJO.setEndTime(MealSlotConstant._1800);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a DINNER meal-slot for a particular city  starting from 1900 to 2200", groups = {"Regression", "Neha Pandey"})
    public void createDINNERmealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.DINNER);
            mealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            mealSlotsPOJO.setEndTime(MealSlotConstant._2200);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a SNACKS meal-slot for a particular city starting from 1600 till 1800", groups = {"Regression", "Neha Pandey"})
    public void createLATE_NIGHTmealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.LATE_NIGHT);
            mealSlotsPOJO.withStartTime(MealSlotConstant._2330);
            mealSlotsPOJO.setEndTime(MealSlotConstant._300);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot with particular start-time depending on the city and time-slot", groups = {"Regression", "Neha Pandey"})
    public void createmealslotStartTimeDependingOnCityCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot with particular end-time depending on the city", groups = {"Regression", "Neha Pandey"})
    public void createmealslotEndTimeDependingOnCityCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal slot with invalid/non-existant/empty/null cityId", groups = {"Regression", "Neha Pandey"})
    public void createInvalidCityDependingOnCityCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withCityId(MealSlotConstant.nonExistantCityID);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            if (createMealslot != null) {
                String status = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
               softAssert.assertEquals(status, MealSlotConstant._400);
                String error = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error"));
               softAssert.assertEquals(error, MealSlotConstant.BadRequest);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot containing values other than BREAKFAST, LUNCH, SNACKS, DINNER and LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void createInvalidSlotTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.Random);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            if (createMealslot != null) {
                String status = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status"));
               softAssert.assertEquals(status, MealSlotConstant._400);
                String error = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error"));
               softAssert.assertEquals(error, MealSlotConstant.BadRequest);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a BREAKFAST meal-slot with start-time to be equivalent to late evening and end_time being any random value", groups = {"Regression", "Neha Pandey"})
    public void createmealslotwithaLargerDurationCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create an overlapping meal-slot with another meal-slot timings i.e. create a meal-slot BREAKFAST with timings 7 AM till 10 AM and create another meal slot LUNCH timings 7 AM till 10 AM and observe the behaviour", groups = {"Regression", "Neha Pandey"})
    public void createOverlappingMealsTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);

            String statusCode1 = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create the same meal-slot twice a day wigth different timings i.e create one BREAKFAST meal-slots starting from 7AM till 10AM and another BREAKFAST meal-slot starting from 16PM till 18PM", groups = {"Regression", "Neha Pandey"})
    public void createBREAKFASTmealSlotTwicewithDifferentTimingsTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.BREAKFAST);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);

            mealSlotsPOJO.withSlot(MealSlotConstant.BREAKFAST);
            mealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            mealSlotsPOJO.withEndTime(MealSlotConstant._2200);
            Processor createMealslot1 = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode1 = String.valueOf(createMealslot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode1, MealSlotConstant.status_0);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "start < end is valid if meal slots is not LATE_NIGHT", groups = {"Regression", "Neha Pandey"})
    public void createMealSlotWithStartTimeLessThanEndTimeTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withStartTime(MealSlotConstant._1000);
            mealSlotsPOJO.withEndTime(MealSlotConstant._2330);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot with start_time = end_time", groups = {"Regression", "Neha Pandey"})
    public void createMealSlotWithStartTimeEqualToEndTimeTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withStartTime(MealSlotConstant._1000);
            mealSlotsPOJO.withEndTime(MealSlotConstant._1000);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant._401);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "start > end is valid only if meal slot is LATE NIGHT", groups = {"Regression", "Neha Pandey"})
    public void createMealSlotWithStartTimeGreaterThanEndTimeTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withStartTime(MealSlotConstant._2330);
            mealSlotsPOJO.withEndTime(MealSlotConstant._300);
            mealSlotsPOJO.withSlot(MealSlotConstant.LATE_NIGHT);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Creation of a duplicate meal-slot for a particular city i.e if BREAKFAST meal-slot for city 1 is existing, try creation of BREAKFAST mealslot again and observe the response", groups = {"Regression", "Neha Pandey"})
    public void createBREAKFASTmealSlotTwiceTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.BREAKFAST);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);

            mealSlotsPOJO.withSlot(MealSlotConstant.BREAKFAST);
            mealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            mealSlotsPOJO.withEndTime(MealSlotConstant._2200);
            Processor createMealslot1 = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode1 = String.valueOf(createMealslot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode1, MealSlotConstant.status_0);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a late night meal-slot and get the details of the late night meal slot on the user enters after 12am , i.e. the delivery start_time must change according to the user and the day must be Today and after 12 AM it must be Tonight", groups = {"Regression", "Neha Pandey"})
    public void createLATE_NIGHTMealTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            mealSlotsPOJO.withSlot(MealSlotConstant.LATE_NIGHT);
            mealSlotsPOJO.withStartTime(MealSlotConstant._2330);
            mealSlotsPOJO.withEndTime(MealSlotConstant._300);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, MealSlotConstant.time_1_AM);
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, MealSlotConstant.time_12_AM);
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

//    =====================================================Update Mealslot=================================================================

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update the start_time of the meal-slot for a particular city", groups = {"Sanity", "Neha Pandey"})
    public void updateMealSlotStartTimeSuccessCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withStartTime(MealSlotConstant._300);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            commonMethodsHelper.updateMealSlot(updateMealSlot);
            String id1 = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertEquals(id1, id);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update the end_time of the meal-slot for a particular city", groups = {"Regression", "Neha Pandey"})
    public void updateMealSlotEndTimeSuccessCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withStartTime(MealSlotConstant._2200);
            updateMealSlotsPOJO.withEndTime(MealSlotConstant._2330);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            commonMethodsHelper.updateMealSlot(updateMealSlot);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update the start_time and end_time of the meal-slots for a particular city", groups = {"Regression", "Neha Pandey"})
    public void updateMealSlotStartTimeEndTimeSuccessCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            updateMealSlotsPOJO.withEndTime(MealSlotConstant._2200);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            commonMethodsHelper.updateMealSlot(updateMealSlot);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Check whether the cityId can be updated", groups = {"Regression", "Neha Pandey"})
    public void updateMealSlotCityIdUpdateCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID + 1);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(status_Code, MealSlotConstant.status_1);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID + 1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Check whether the slot of the meal can be updated", groups = {"Regression", "Neha Pandey"})
    public void updateSlotCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withSlot(MealSlotConstant.LATE_NIGHT);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(status_Code, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update a meal-slot to an already existing meal-slot for a particular city i.e. the 24 hour slot contains BREAKFAST meal-slot and DINNER meal-slot, try updating BREAKFAST meal-slot to DINNER meal-slot for creation of a duplicate meal-slot entry", groups = {"Regression", "Neha Pandey"})
    public void updateandcreateDuplicateRecordCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);
            String start_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.start_time"));
            softAssert.assertNotNull(start_time);
            String end_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.end_time"));
            softAssert.assertNotNull(end_time);

            mealSlotsPOJO.withCityId(cityID);
            mealSlotsPOJO.withSlot(MealSlotConstant.DINNER);
            mealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            mealSlotsPOJO.withEndTime(MealSlotConstant._2200);
            Processor createMealslot1 = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode1 = String.valueOf(createMealslot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);
            String cityid = String.valueOf(createMealslot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertEquals(cityid, cityID);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withSlot(MealSlotConstant.DINNER);
            updateMealSlotsPOJO.withStartTime(start_time);
            updateMealSlotsPOJO.withEndTime(end_time);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(status_Code, MealSlotConstant._403);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update the meal-slot with respect to the mapped id", groups = {"Regression", "Neha Pandey"})
    public void updateIDCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(status_Code, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update for overlapping meal-slot must not be allowed", groups = {"Regression", "Neha Pandey"})
    public void updateOverlappingDataErrorCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            Assert.assertNotNull(id);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            Assert.assertNotNull(cityID);
            String start_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.start_time"));
            Assert.assertNotNull(start_time);
            String end_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.end_time"));
            Assert.assertNotNull(end_time);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            updateMealSlotsPOJO.withSlot(MealSlotConstant.DINNER);
            updateMealSlotsPOJO.withStartTime(start_time);
            updateMealSlotsPOJO.withEndTime(end_time);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(status_Code, MealSlotConstant._403);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Update the city with respect to the meal-slot", groups = {"Regression", "Neha Pandey"})
    public void updateCityIdUpdateCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            commonMethodsHelper.createMealSlotSuccessAsserts(createMealslot);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);

            updateMealSlotsPOJO.withCityId(cityID + 1);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(status_Code, MealSlotConstant.status_1);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID + 1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Trying to update the details of meal-slot that doesn't exist", groups = {"Regression", "Neha Pandey"})
    public void updateNonExistantMealSlotCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        updateMealSlotsPOJO.withCityId(MealSlotConstant.invalidCityID);
        updateMealSlotsPOJO.withId(MealSlotConstant.id);
        Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
        String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
       softAssert.assertEquals(status_Code, MealSlotConstant._402);
    }

//    =========================================================Delete Mealslot================================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Delete existing meal-slots for a particular city", groups = {"Sanity", "Neha Pandey"})
    public void deleteMealSlotSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);
        }
        finally {
            Processor deleteMealSlot1 = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Delete existing meal slot mapped to a particular time", groups = {"Regression", "Neha Pandey"})
    public void deleteMealSlotMappedToTimingsSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            softAssert.assertEquals(data, MealSlotConstant.data);
        }
        finally {
            Processor deleteMealSlot1 = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Delete meal-slots with invalid/non-existant/null/empty cityId", groups = {"Regression", "Neha Pandey"})
    public void deleteMealSlotMappedToInvalidCityCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        Processor deleteMealSlot = mealSlotHelper.deleteMeal(MealSlotConstant.nonExistantCityID);
        String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertEquals(statusCode, MealSlotConstant._404);
        String statusMessage = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
        softAssert.assertEquals(statusMessage, "Invalid delete request, no data found");
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Deletion of duplicate meal-slot is also not possible coz creation itself is not possible for duplicate", groups = {"Regression", "Neha Pandey"})
    public void deleteOfDuplicateMealslotCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
            softAssert.assertEquals(data, MealSlotConstant.data);

            Processor deleteMealSlot1 = mealSlotHelper.deleteMeal(cityID);
            String statusCode1 = String.valueOf(deleteMealSlot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode1, MealSlotConstant._404);
            String statusMessage = String.valueOf(deleteMealSlot1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
            softAssert.assertEquals(statusMessage, "Invalid delete request, no data found");
        }
        finally {
            Processor deleteMealSlot1 = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Get the meal-slot and delete the cache, Get the details of the meal-slot service again ,try getting the cache and delete cache again,", groups = {"E2E", "Neha Pandey"})
    public void complexScenario1Test(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(MealSlotConstant.cityID);

            mealSlotsPOJO.withStartTime(MealSlotConstant._1900);
            mealSlotsPOJO.withEndTime(MealSlotConstant._2200);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor evictCache = mealSlotHelper.evictCacheData();
            if (evictCache != null) {
                String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
                softAssert.assertEquals(data, MealSlotConstant.data);
            }

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());

            Processor evictCache1 = mealSlotHelper.evictCacheData();
            if (evictCache1 != null) {
                String statusCode = String.valueOf(evictCache1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
               softAssert.assertEquals(data, MealSlotConstant.data);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Get the meal-slot cache and delete the cache. Try updating the details for a meal-slot and then get cache then delete cache", groups = {"E2E", "Neha Pandey"})
    public void complexScenario2Test(MealSlotsPOJO mealSlotsPOJO,UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(MealSlotConstant.cityID);
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor evictCache = mealSlotHelper.evictCacheData();
            if (evictCache != null) {
                String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
               softAssert.assertEquals(data, MealSlotConstant.data);
            }

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            commonMethodsHelper.updateMealSlot(updateMealSlot);

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId1);

            Processor evictCache1 = mealSlotHelper.evictCacheData();
            if (evictCache1 != null) {
                String statusCode = String.valueOf(evictCache1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
               softAssert.assertEquals(data, MealSlotConstant.data);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


//    ==========================================================GET cache data===================================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Get the details of the existing slots with respect to a particular city", groups = {"Sanity", "Neha Pandey"})
    public void getApplicationCacheSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData = mealSlotHelper.getCacheData();
            if (getCacheData != null) {
                String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class, description = "Get the details of the existing slots with respect to a particular city and relative time-slot as well", groups = {"Regression", "Neha Pandey"})
    public void ComplexScenario1Case(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData = mealSlotHelper.getCacheData();
            if (getCacheData != null) {
                String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
                softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            }
        }
        finally {
//            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(description = "Get the meal-slot details of a non-existant city", groups = {"Regression", "Neha Pandey"})
    public void getDetailsforNonExistantCityTest() throws Exception {

        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.nonExistantCityID, ""+DateUtility.getCurrentDateInMilisecond());

        Processor getCacheData = mealSlotHelper.getCacheData();
        if(getCacheData!=null) {
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
    }

    @Test(description = "Get the meal-slot and then get the cache for a particular city", groups = {"Regression", "Neha Pandey"})
    public void getDetailsandGetCacheSuccessCase() throws Exception {

        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.cityID, ""+DateUtility.getCurrentDateInMilisecond());
        String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertEquals(statusCode, MealSlotConstant.status_1);

        Processor getCacheData = mealSlotHelper.getCacheData();
        if(getCacheData!=null) {
            String statusCode1 = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Get the meal-slot cache and try updating the details of the meal-slot and then try getting the meal-slot cache again", groups = {"E2E", "Neha Pandey"})
    public void getDetailsThenUpdateandAgainGetCase(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(4000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData = mealSlotHelper.getCacheData();
            if (getCacheData != null) {
                String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            }

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            commonMethodsHelper.updateMealSlot(updateMealSlot);

            Processor getByCityId1 = mealSlotHelper.getMealSlots(MealSlotConstant.cityID, ""+DateUtility.getCurrentDateInMilisecond());

            Processor getCacheData1 = mealSlotHelper.getCacheData();
            if (getCacheData1 != null) {
                String statusCode = String.valueOf(getCacheData1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


//    ==========================================================Evict cache data=============================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Delete the data from the application contating the meal-slots for a particular city", groups = {"Sanity", "Neha Pandey"})
    public void evictCacheDataSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor evictCache = mealSlotHelper.evictCacheData();
            if (evictCache != null) {
                String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
               softAssert.assertEquals(data, MealSlotConstant.data);
            }
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Delete the data from the application contating the meal-slots for a non-existant city", groups = {"Regression", "Neha Pandey"})
    public void evictCacheDataForNonExistantCityCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(MealSlotConstant.nonExistantCityID , ""+DateUtility.getCurrentDateInMilisecond());

        Processor evictCache = mealSlotHelper.evictCacheData();
        if(evictCache!=null) {
            String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Delete the meal-slots from Guava cache without getting the data from Meal-slot service or cache", groups = {"Regression", "Neha Pandey"})
    public void evictCacheDatawithoutGettingThedetailsFromServiceCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        Processor evictCache = mealSlotHelper.evictCacheData();
        if(evictCache!=null) {
            String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Trying to evict the cache before hitting the GET endpoint from meal-slot service", groups = {"Regression", "Neha Pandey"})
    public void evictCacheDatawithoutGettingThedetailsFromCacheCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        Processor evictCache = mealSlotHelper.evictCacheData();
        if(evictCache!=null) {
            String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "After deletion of the available meal-slots for a particular city, try getting the meal-slots, it must show as empty. If cache is evicted, service should take from DB. Also test the TTL of cache", groups = {"E2E", "Neha Pandey"})
    public void evictCacheComplexcse1DataSuccessCase(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
            softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor evictCache = mealSlotHelper.evictCacheData();
            if (evictCache != null) {
                String statusCode = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
               softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
                String data = String.valueOf(evictCache.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
               softAssert.assertEquals(data, MealSlotConstant.data);
            }

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            String statusCode1 = String.valueOf(getByCityId1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);

            Processor getCacheData = mealSlotHelper.getCacheData();
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


//    =============================================================================================CRUD====================================================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Create a meal-slot, get the details of the meal-slot", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase1Case(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class,description = "Create the meal-slot, get, update the details of the meal-slot and then get the details", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase2Case(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(MealSlotConstant.id);

            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class, description = "Create a meal-slot, get, update the details of the meal-slot then delete the meal-slot and get the details", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase3Case(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(5000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);

            Processor getByCityId1 = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            String statusCode1 = String.valueOf(getByCityId1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);
            String data1 = String.valueOf(getByCityId1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertNotNull(data1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class,description = "Create a meal-slot, delete the meal-slot and then try updating the meal-slot", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase4Case(MealSlotsPOJO mealSlotsPOJO,UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(4000);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String statusCode1 = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant._402);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Create a meal-slot, delete the meal-slot and then get the details of the meal-slot", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase5Case(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(4000);

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, MealSlotConstant.data);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            String statusCode1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Create an meal slot for a city, call get Cache data api , should return empty response", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase6Case(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(4000);

            Processor getCacheData = mealSlotHelper.getCacheData();
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, "{}");
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Create an meal slot for a city, call get meal slot api, then call get cache data api should return meal slots for city", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase7Case(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(4000);

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData = mealSlotHelper.getCacheData();
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class,description = "Create meal slot, call get cache data, response should be empty then call get meal slot api then again call get cache data it should return meal slots , now try to update the existing slot for city then again call get cache data api, should return again empty response", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase8Case(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(4000);

            Processor getCacheData = mealSlotHelper.getCacheData();
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, "{}");

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData2 = mealSlotHelper.getCacheData();
            String statusCode2 = String.valueOf(getCacheData2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode2, MealSlotConstant.status_1);
            String data1 = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data1, "{}");

            updateMealSlotsPOJO.withCityId(cityID);
            updateMealSlotsPOJO.withId(id);
            Processor updateMealSlot = mealSlotHelper.updateMeal(updateMealSlotsPOJO);
            String statusCode1 = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode1, MealSlotConstant.status_1);

            Processor getCacheData1 = mealSlotHelper.getCacheData();
            String statusCode11 = String.valueOf(getCacheData1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode11, MealSlotConstant.status_1);
            String data2 = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data2, "{}");
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(dataProvider = "updateMealSlot", dataProviderClass = MealSlotDP.class,description = "Create meal slot, call get cache data, response should be empty then call get meal slot api then again call get cache data it should return meal slots , now try to delete the existing slot for city then again call get cache data api, should return again empty response", groups = {"E2E", "Neha Pandey"})
    public void ComplexCase9Case(MealSlotsPOJO mealSlotsPOJO, UpdateMealSlotsPOJO updateMealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
           softAssert.assertNotNull(id);
            Thread.sleep(4000);

            Processor getCacheData = mealSlotHelper.getCacheData();
            String statusCode = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String data = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data, "{}");

            Processor getByCityId = mealSlotHelper.getMealSlots(cityID, ""+DateUtility.getCurrentDateInMilisecond());
            commonMethodsHelper.successGetMealSlotAPIasserts(getByCityId);

            Processor getCacheData2 = mealSlotHelper.getCacheData();
            String statusCode2 = String.valueOf(getCacheData2.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode2, MealSlotConstant.status_1);
            String data1 = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data1, "{}");

            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
            String statusCode22 = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode22, MealSlotConstant.status_1);
            String data22 = String.valueOf(deleteMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data22, MealSlotConstant.data);

            Processor getCacheData1 = mealSlotHelper.getCacheData();
            String statusCode11 = String.valueOf(getCacheData1.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode11, MealSlotConstant.status_1);
            String data2 = String.valueOf(getCacheData.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
           softAssert.assertEquals(data2, "{}");
        }
        finally {
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }


//    ===============================================================GET MealSlot BY ID======================================================================

    @Test(dataProvider = "createMealSlot", dataProviderClass = MealSlotDP.class,description = "Get mealslot by ID", groups = {"Sanity", "Neha Pandey"})
    public void getMealslotByIDTest(MealSlotsPOJO mealSlotsPOJO) throws Exception {
        try {
            Processor createMealslot = mealSlotHelper.createMeal(mealSlotsPOJO);
            cityID = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
           softAssert.assertNotNull(cityID);
            Thread.sleep(4000);

            Processor getMealSlotByCityId = mealSlotHelper.getMealSlots(cityID, "");
            String id = String.valueOf(getMealSlotByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].id"));
           softAssert.assertNotNull(id);

            Processor getMealSlotByID = mealSlotHelper.getMealSlotByID(id);
            String statusCode = String.valueOf(getMealSlotByID.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
           softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
        finally{
            Processor deleteMealSlot = mealSlotHelper.deleteMeal(cityID);
        }
    }

    @Test(description = "Create a meal-slot and get the meal-slot details by wrong id reference", groups = {"Regression", "Neha Pandey"})
    public void getMealslotByIDErrorTest() throws Exception {
        Processor getMealSlotByID = mealSlotHelper.getMealSlotByID(MealSlotConstant.invalidMealID);
        String statusCode = String.valueOf(getMealSlotByID.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
       softAssert.assertEquals(statusCode, MealSlotConstant._405);
        String statusMessage = String.valueOf(getMealSlotByID.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
       softAssert.assertEquals(statusMessage, "Invalid meal slot id");
    }

    @Test(description = "Get the details of the meal-slot by id refernce with non-existant ID", groups = {"Regression", "Neha Pandey"})
    public void getMealslotByIDNonExistantTest() throws Exception {
        Processor getMealSlotByID = mealSlotHelper.getMealSlotByID(MealSlotConstant.invalidMealID);
        String statusCode = String.valueOf(getMealSlotByID.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
       softAssert.assertEquals(statusCode, MealSlotConstant._405);
        String statusMessage = String.valueOf(getMealSlotByID.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
       softAssert.assertEquals(statusMessage, "Invalid meal slot id");
    }


}