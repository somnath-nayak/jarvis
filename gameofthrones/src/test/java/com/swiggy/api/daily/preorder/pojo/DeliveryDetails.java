
package com.swiggy.api.daily.preorder.pojo;

import com.swiggy.api.daily.common.DateUtility;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "start_time",
    "end_time"
})
public class DeliveryDetails {

    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end_time")
    private String endTime;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DeliveryDetails() {
    }

    /**
     * 
     * @param startTime
     * @param endTime
     */
    public DeliveryDetails(String startTime, String endTime) {
        super();
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public DeliveryDetails withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public DeliveryDetails withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }
    public DeliveryDetails setDefaultData()
    {
        //this.withEndTime(DateUtility.getFutureDateInMilisecond()).withStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond());
        this.withEndTime("5").withStartTime("3");
        return this;
    }

}
