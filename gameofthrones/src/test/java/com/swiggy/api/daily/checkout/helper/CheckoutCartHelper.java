package com.swiggy.api.daily.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.redis.S;
import com.swiggy.api.daily.checkout.pojo.*;
import com.swiggy.api.daily.checkout.pojo.listingPojo.ListingCartPayload;
import com.swiggy.api.daily.cms.helper.CMSCommonHelper;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import com.swiggy.api.daily.mealSlot.helper.*;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import com.swiggy.api.daily.promotions.helper.*;
import org.testng.annotations.Test;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckoutCartHelper {

    static Initialize gameofthrones = new Initialize();
    MealSlotHelper mealSlotHelper = new MealSlotHelper();
    JsonHelper jsonHelper = new JsonHelper();
    DailyAddressHelper addressHelper = new DailyAddressHelper();
    CMSCommonHelper cmsHelper=new CMSCommonHelper();
    CartPayload cartPayload;
    DailyCheckoutCommonUtils utils = new DailyCheckoutCommonUtils();
    static String tid, token, sid, userId, addressId;
    static String mealSlotID;
    static String spinID1, skuID1;

    CartPreOrderHelper preOrderHelper = new CartPreOrderHelper();
    PromotionsHelper promotionsHelper = new PromotionsHelper();



    public Processor cartUpdate1(String payload, HashMap<String, String> requestHeaders)
    {
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "updatedailycart", gameofthrones);
        return new Processor(service, requestHeaders, new String[]{payload});
    }


    public Processor consumerDailyLogin(String payload, HashMap<String, String> headers) {
        GameOfThronesService service = new GameOfThronesService("sanddaily", "applogin", gameofthrones);
        return new Processor(service, headers, new String[]{payload});
    }


    public Processor placeOrder(HashMap<String, String> headers,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "placeorder", gameofthrones);
        return new Processor(service, headers, new String[]{payload});
    }

    public Processor cartPlUpdate(HashMap<String, String> headers,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "cartplupdate", gameofthrones);
        return new Processor(service, headers, new String[]{payload});
    }


    public Processor placePlOrder(String tid,String token,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "placeplorder", gameofthrones);
        return new Processor(service, utils.getHeader(tid,token), new String[]{payload});
    }


    public Processor getItemsListFromInventory(String storeId) {
        String payload =null;
        ListingCartPayload listingCartPayload = new ListingCartPayload();
        listingCartPayload.setStoreId(Integer.parseInt(storeId));
        List<ListingCartPayload> lis = new ArrayList<>();
        lis.add(listingCartPayload);
        try{
         payload = (jsonHelper.getObjectToJSON(lis));
        } catch (Exception e){
            e.printStackTrace();
        }
        GameOfThronesService service = new GameOfThronesService("sand", "cartlisting", gameofthrones);
        return new Processor(service, setListingHeader(), new String[]{payload});
    }

    public Processor getPlanItemsListFromInventory(String storeId) {
        String payload =null;
        ListingCartPayload listingCartPayload = new ListingCartPayload();
        listingCartPayload.setStoreId(Integer.parseInt(storeId));
        List<ListingCartPayload> lis = new ArrayList<>();
        lis.add(listingCartPayload);
        try{
        payload = (jsonHelper.getObjectToJSON(lis));
        } catch (Exception e){
            e.printStackTrace();
        }
        GameOfThronesService service = new GameOfThronesService("sand", "cartlistingPlan", gameofthrones);
        return new Processor(service, setListingHeader(), new String[]{payload});
    }

    public HashMap<String, String> setListingHeader() {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", DailyCheckoutConstants.AUTHORIZATION);
        headers.put("content-type", "application/json");
        headers.put("tenant-id", DailyCheckoutConstants.TENANT_ID);

        return headers;
    }



    public Processor getAvailableAddonsInventory(String store) throws IOException {

        GameOfThronesService service = new GameOfThronesService("sand", "cartAddons", gameofthrones);
        String[] urlParams = {store};
        Processor processor = new Processor(service, setListingHeader(), null, urlParams);
        return processor;
    }

    /**
     * We can use loginAndGetUserInfo method instead of this method.
     * Making it deprecated as of now. Will reamove it once all the implementations are removed
     */

    public HashMap<String, String> setCompleteHeader(String userAgent, int versionCode) {
        HashMap<String, String> headers = new HashMap<String, String>();
        UserInfo userInfo  = new UserInfo();
        HashMap<String, String> userdetails = utils.dailyLogin(DailyCheckoutConstants.USER_MOBILE, DailyCheckoutConstants.USER_PASSWORD);

        String tid = userdetails.get("tid");
        String token = userdetails.get("token");
        String customer_id = userdetails.get("customer_id");
        String sid = userdetails.get("sid");

        userInfo.setVersionCode(versionCode);
        userInfo.setUserId(customer_id);
        userInfo.setUserAgent(userAgent);
        userInfo.setSid(sid);
        headers.put("userinfo", Utility.jsonEncode(userInfo));
        headers.put("content-type", "application/json");

        return headers;
    }



    /**
     * This is common method to verify the status and message returned in response
     */
    public void validateApiResStatusData(String response) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode, DailyCheckoutConstants.VALID_STATUS_CODE);
        String isSuccessful = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(isSuccessful, DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE, "Successful flag is false");
        Assert.assertNotNull(isSuccessful, DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE);
    }


    public String getMealSlotID(String cityId) throws Exception {
        Processor getByCityId = mealSlotHelper.getMealSlots(cityId, "" + DateUtility.getCurrentDateInMilisecond());
        String cartResponse = getByCityId.ResponseValidator.GetBodyAsText();
        mealSlotID = JsonPath.read(cartResponse, "$.data[1].id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(mealSlotID);
        return mealSlotID;
    }


    /** Get Valid Meal Slot for City - Returns Meal Slot ID**/
    public String VerifyValidMealSlot() throws Exception {

        Processor getByCityId = mealSlotHelper.getMealSlots(DailyCheckoutConstants.CITY_ID, "" + DateUtility.getCurrentDateInMilisecond());
        String cartResponse = getByCityId.ResponseValidator.GetBodyAsText();
        String mealSlotID = JsonPath.read(cartResponse, "$.data[0].id").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(mealSlotID);
        return mealSlotID;

    }



    public String getAddonsFromStore(String store) {
        Processor getByCityId = null;

        try {
            getByCityId = getAvailableAddonsInventory(store);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cartResponse = getByCityId.ResponseValidator.GetBodyAsText();
        System.out.println(cartResponse);
        String addonID = JsonPath.read(cartResponse, "$.data[0].spin").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(addonID);
        System.out.println("spin" + addonID);

        return addonID;
    }



    /**
     * This method is responsible for login
     *
     * @Returns UserInfo Payload
     */
    public UserInfo loginAndGetUserInfo( String userAgent, int versionCode) {
        HashMap<String, String> headers = new HashMap<String, String>();
        UserInfo userInfo = new UserInfo();
        headers.put("Content-Type", "application/json");
        LoginPayload loginPayload = new LoginPayload();

        loginPayload.setMobile(DailyCheckoutConstants.USER_MOBILE);
        loginPayload.setPassword(DailyCheckoutConstants.USER_PASSWORD);
        String payload = Utility.jsonEncode(loginPayload);

        String cartResponse = consumerDailyLogin(payload, headers).ResponseValidator.GetBodyAsText();
        tid = JsonPath.read(cartResponse, "$.tid").toString().replace("[", "").replace("]", "");
        sid = JsonPath.read(cartResponse, "$.sid").toString().replace("[", "").replace("]", "");
        token = JsonPath.read(cartResponse, "$.data.token").toString().replace("[", "").replace("]", "");
        userId = JsonPath.read(cartResponse, "$.data.addresses[0].user_id").toString().replace("[", "").replace("]", "");
        addressId = JsonPath.read(cartResponse, "$.data.addresses[0].id").toString().replace("[", "").replace("]", "");

        userInfo.setSid(sid);
        userInfo.setUserId(userId);
        userInfo.setVersionCode(versionCode);
        userInfo.setUserAgent(userAgent);
        return userInfo;
    }




    /**
     * This meethod is responsible for giving cart payload
     *
     * @param cartType it can PLAN or PRODUCT
     * @param storeId  this is store id for which we are building the cart
     */
    public CartPayload getDailyCartPayload(String cartType, String storeId,Map<String, String> header,String cityId) {
        DailyAddressHelper addressHelper = new DailyAddressHelper();
        CartPayload cartPayload = new CartPayload();
        Location location = new Location();
        Ctx ctx = new Ctx();
        List<Item> items = new ArrayList<>();
        SlotDetails slotDetails = new SlotDetails();
        List<Object> excludedDates = new ArrayList<>();
        Meta meta = new Meta();

        cartPayload.setCouponCode(getCouponForCart());

        items.add(getItems(cartType, "1", storeId ));

        cartPayload.setItems(items);
        cartPayload.setAddressId(addressHelper.getServiceableAddressId(header));
        ctx = getCtxDetails(cartType, storeId, "today", false, Integer.parseInt(cityId));
        cartPayload.setCtx(ctx);
        //ctx = getCtxDetails("today",false,false,cartType) ;
        location.setLng(DailyCheckoutConstants.LANGITUDE);
        location.setLat(DailyCheckoutConstants.LATITUDE);
        cartPayload.setLocation(location);


        return cartPayload;
    }

    public CartPayload getDailyCartPayloadInternal(String cartType, String storeId,Map<String, String> header,String cityId, boolean addons ) {
        DailyAddressHelper addressHelper = new DailyAddressHelper();
        CartPayload cartPayload = new CartPayload();
        Location location = new Location();
        Ctx ctx = new Ctx();
        List<Item> items = new ArrayList<>();
        SlotDetails slotDetails = new SlotDetails();
        List<Object> excludedDates = new ArrayList<>();
        Meta meta = new Meta();
        List<Item> Addons = new ArrayList<>();



        cartPayload.setCouponCode(getCouponForCart());

        items.add(0,getItems(cartType, "1", storeId ));

        if(addons) {
            items.add(1, getAddons("1", storeId));
        }

        cartPayload.setItems(items);
        //cartPayload.setItems(Addons);
        cartPayload.setAddressId(addressHelper.getServiceableAddressId(header));
        ctx = getCtxDetails(cartType, storeId, "today", false, Integer.parseInt(cityId));
        cartPayload.setCtx(ctx);
        //ctx = getCtxDetails("today",false,false,cartType) ;
        location.setLng(DailyCheckoutConstants.LANGITUDE);
        location.setLat(DailyCheckoutConstants.LATITUDE);
        cartPayload.setLocation(location);


        return cartPayload;
    }




    /**
     * this method will be used to get the coupon
     * Not defining it as coupon is not yet implemented in daily
     *
     * @return empty string as of now
     */
    private String getCouponForCart() {
        return " ";
    }




    /**
     * this method is used to get the Item object on a store
     *
     * @param itemQuantity quantity od item
     * @param itemType     type of item you want
     * @param storeId      Store id from which this item belongs
     * @return Item Object
     */

    private Item getItems(String itemType, String itemQuantity, String storeId) {
        Processor processor;
        GameOfThronesService service;
        Item item = new Item();


        if (itemType.equalsIgnoreCase("plan")) {
            item.setType("PLAN");
            item.setSpin(getPlanInventoryFromStore(storeId));
        } else if (itemType.equalsIgnoreCase("product")) {
            item.setType("PRODUCT");
            item.setSpin(getProductInventoryFromStore(storeId));
        } else if (itemType.equalsIgnoreCase("addons")) {
            item.setType("ADDON");
            item.setSpin(getAddonsFromStore(storeId));
        }

        item.setQuantity(Integer.parseInt(itemQuantity));
        item.setStoreId(Integer.parseInt(storeId));
        item.setServiceLine("daily");

        return item;

    }

    private Item getAddons(String itemQuantity, String storeId) {
        Processor processor;
        GameOfThronesService service;
        Item item = new Item();

        item.setType("ADDON");
        item.setSpin(getAddonsFromStore(storeId));
        item.setQuantity(Integer.parseInt(itemQuantity));
        item.setStoreId(Integer.parseInt(storeId));
        item.setServiceLine("daily");

        return item;

    }




    public String getProductInventoryFromStore(String storeId) {
        String itemSpin = null;
        Processor getItems=getItemsListFromInventory(storeId);


        JSONArray ItemID = getItems.ResponseValidator.GetNodeValueAsJsonArray("$.data[*].variations[*].spin");
        for (int i=0;i<ItemID.size();i++) {

            boolean isAvailable = getItems.ResponseValidator.GetNodeValueAsBool("$.data[" + i + "].variations[0].availability.is_avail");
            boolean inStock = getItems.ResponseValidator.GetNodeValueAsBool("$.data[" + i + "].variations[0].inventory.in_stock");
            if ( isAvailable && inStock) {
                itemSpin = ItemID.get(i).toString();
                System.out.println(itemSpin);
                return itemSpin;
            }
        }

        if (itemSpin==null){
            throw new RuntimeException("None of the SPIN is Available or In-Stock from this store id "+storeId+"." +
                    " Please contact CMS");
        }

        return null;
    }


    public String getPlanInventoryFromStore(String storeId) {
        String planSpin = null;
        Processor getPlanId = getPlanItemsListFromInventory(storeId);
        JSONArray ItemID = getPlanId.ResponseValidator.GetNodeValueAsJsonArray("$.data[*].variations[*].spin");
        for (int i = 0; i < ItemID.size(); i++) {
            boolean isAvailable = getPlanId.ResponseValidator.GetNodeValueAsBool("$.data[" + i + "].variations[" + i + "].availability.is_avail");
            boolean inStock = getPlanId.ResponseValidator.GetNodeValueAsBool("$.data[" + i + "].variations[0].inventory.in_stock");

            if (isAvailable && inStock) {
                planSpin = ItemID.get(i).toString();
                return planSpin;
            }

        }
        if (planSpin==null){
            throw new RuntimeException("None of the SPIN is Available or In-Stock from this store id "+storeId+"." +
                    " Please contact CMS");
        }
        return null;
    }



    /**
     * This method is responsible for cart slot creation
     *
     * @param startDate  It should be "today" 0r number of days after today
     * @param hasWeekend it can be true or false
     * @param cityID     This is required to get the slot in that specific city
     */
    private Ctx getCtxDetails(String cartType, String storeId, String startDate, boolean hasWeekend, int cityID) {
        Ctx ctx = new Ctx();
        SlotDetails slotDetails;
        String mealSlotId = null;
        long startDateInEpoch = 0;
        List<String> excludedDates = new ArrayList<>();

        if (startDate == null || startDate.isEmpty()) {
            System.out.println("please enter start date to get the slot details ");
        } else if (startDate.contains("today")) {
            startDateInEpoch = DateUtility.getCurrentDateInMilisecond();
        } else {
            startDateInEpoch = Long.parseLong(DateUtility.getFutureDateInDayMilisecond(Integer.parseInt(startDate)));

        }


        try {
            mealSlotId = getMealSlotID(Integer.toString(cityID));

        } catch (Exception e) {
            e.printStackTrace();
        }


        ctx.setStartDate(startDateInEpoch);
        ctx.setMealSlotId(mealSlotId);
        //ctx.setExcludedDates(excludedDates);
        ctx.setHasWeekend(hasWeekend);
        return ctx;
    }



    public Processor createCart(CartPayload cartPayload,HashMap<String,String> header){
        Processor cartResponse=preOrderHelper.cartUpdate(Utility.jsonEncode(cartPayload),header);
        return cartResponse;
    }


    /**
     * This method creates the cart and return the cartId
     * This is for external user use
     * */
    public Processor createCartForOthers(HashMap<String,String> header,String cartType,String cityId, String storeId){
        CartPayload cartPayload;
        cartPayload = getDailyCartPayload(cartType,storeId,new DailyCheckoutCommonUtils().getHeader(header),cityId );
        Processor cartResponse=preOrderHelper.cartUpdate(Utility.jsonEncode(cartPayload),header);
        return cartResponse;
    }


    public Processor createCartAndModifyPrice(HashMap<String,String> header,String cartType,String cityId, String storeId){
        CartPayload cartPayload;
        cartPayload = getDailyCartPayload(cartType,storeId,new DailyCheckoutCommonUtils().getHeader(header),cityId);
        String spinId=cartPayload.getItems().get(0).getSpin();
        cmsHelper.setPrice(DailyCheckoutConstants.STOREID,spinId,1,cartType);
        Processor cartResponse=preOrderHelper.cartUpdate(Utility.jsonEncode(cartPayload),header);
        return cartResponse;
    }

    /** create promotion with the given store, Spin and discount type
     *
     */
    public HashMap<String, String> createPromotionsTDForCart(String type, String discountValue , String storeId, String spinId) {

        HashMap<String, String> promotionID= null;
        try {
             promotionID = promotionsHelper.createPromotion(type, discountValue, storeId, spinId);
        } catch (Exception e) {
            e.printStackTrace();
        }


        return promotionID;
    }
    @Test
    public void disablePromotionsTDForCart(String promotionID)
    {

        try {
            promotionsHelper.deActivatePromotionByPromotionId(promotionID);
        }
        catch (Exception e) {
            e.printStackTrace();
        }


    }



    public Processor createCartPlForOthers(HashMap<String,String> header,String cartType,String cityId, String storeId){
        CartPayload cartPayload;
        cartPayload = getDailyCartPayload(cartType,storeId,header,cityId);
        Processor cartResponse=preOrderHelper.cartPlUpdate(Utility.jsonEncode(cartPayload),header);
        return cartResponse;
    }

}

