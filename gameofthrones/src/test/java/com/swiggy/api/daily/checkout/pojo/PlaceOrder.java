package com.swiggy.api.daily.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cart_id",
        "address_id",
        "payment_type",
        "payment_info"
})
public class PlaceOrder {

    @JsonProperty("cart_id")
    private String cartId;
    @JsonProperty("address_id")
    private Integer addressId;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("payment_info")
    private PaymentInfo paymentInfo;

    @JsonProperty("cart_id")
    public String getCartId() {
        return cartId;
    }

    @JsonProperty("cart_id")
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    @JsonProperty("address_id")
    public Integer getAddressId() {
        return addressId;
    }

    @JsonProperty("address_id")
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @JsonProperty("payment_info")
    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    @JsonProperty("payment_info")
    public void setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartId", cartId).append("addressId", addressId).append("paymentType", paymentType).append("paymentInfo", paymentInfo).toString();
    }

}