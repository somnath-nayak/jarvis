package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "store_id"
})
public class MetaPOJO {

    @JsonProperty("store_id")
    private Integer storeId;

    /**
     * No args constructor for use in serialization
     *
     */
    public MetaPOJO() {
    }

    /**
     *
     * @param storeId
     */
    public MetaPOJO(Integer storeId) {
        super();
        this.storeId = storeId;
    }

    @JsonProperty("store_id")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public MetaPOJO withStoreId(Integer storeId) {
        this.storeId = storeId;
        return this;
    }

    public MetaPOJO setDefault(Integer storeId) {
        this.withStoreId(storeId);
        return this;
    }

}
