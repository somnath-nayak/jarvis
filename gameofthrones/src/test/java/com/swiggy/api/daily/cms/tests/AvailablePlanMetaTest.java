package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class AvailablePlanMetaTest extends CMSDailyDP {

    CMSDailyHelper helper=new CMSDailyHelper();

    @Test(dataProvider = "availableplanmetadp",description = "This test will give all meta of Plan")
    public void availablePlanMeta(String payload,String service){
        SoftAssert sc=new SoftAssert();
        String res=helper.availablePlanMeta(payload,service).ResponseValidator.GetBodyAsText();
        String storeid= JsonPath.read(res,"$.data..store_id").toString();
        int status_code = JsonPath.read(res, "$.status_code");
        String status_message = JsonPath.read(res, "$.status_message");
        sc.assertEquals(status_code,1,"Status code not matching");
        sc.assertEquals(status_message,"success","Status message not matching");
        sc.assertAll();


    }
}
