
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "type",
    "total"
})
public class Tax__ {

    @JsonProperty("type")
    private String type;
    @JsonProperty("total")
    private Integer total;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Tax__ withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public Tax__ withTotal(Integer total) {
        this.total = total;
        return this;
    }

    public Tax__ setDefaultData() {
        return this.withType(SubscriptionConstant.TAX____TYPE)
                .withTotal(SubscriptionConstant.TAX____TOTAL);
    }

}
