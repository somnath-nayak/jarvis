package com.swiggy.api.daily.subscription.test;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.pojo.*;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

 /*
      Create Subscription and skip Subscription testcases
 */

public class CreateSubscriptionTest extends SubscriptionDp {

    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
    CreateSubscriptionPojo createSubscriptionPojo=new CreateSubscriptionPojo();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }
    //------------------------------------------------Create Subscription Positive Scenario-------------------------------------------------------------------

    @Test(dataProvider="createSubscriptionTenureDP",description ="Create subscription for 7, 10 days for weekend service true, false",groups={"Sanity_Test","Regression"})
    public void createSubscriptionTest(String payload) throws IOException, ParseException {
        String weekendService=Boolean.toString(JsonPath.read(payload,"$.schedule_details.has_weekend_service"));
        Processor processor = subscriptionHelper.createSubscription(payload);
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        subscriptionTestHelper.validateCreateSubscription(processor,"Positive",weekendService,payload,SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
    }

    @Test(dataProvider="createSubscriptionOnWeekendDP",description ="Create subscription on Weekend date, weekend service true, false",groups={"Sanity_Test","Regression"})
    public void createSubscriptionOnWeekendTest(String payload) throws IOException, ParseException {
        String weekendService=Boolean.toString(JsonPath.read(payload,"$.schedule_details.has_weekend_service"));
        Processor processor = subscriptionHelper.createSubscription(payload);
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        subscriptionTestHelper.validateCreateSubscription(processor,"Positive",weekendService,payload,SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
    }

    //----------------------------------------------- Create Subscription Negative Scenario--------------------------------------------------------------------

    @Test(dataProvider="createSubscriptionNegativeDP",description ="Create subscription for 7 days for weekend service true, false, Negative Scenario",groups={"Regression"})
    public void createSubscriptionNegativeTest(String payload) throws IOException, ParseException {
        try {
            String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
            Processor processor = subscriptionHelper.createSubscription(payload);
            String changeServerResumeTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+DateUtility.getCurrentTimeInReadableFormatWithColon()));
            subscriptionHelper.changeServerTime(changeServerResumeTimePayload);
            String expectedStatusMessage = "Subscription delivery slot cannot be less than " + DateUtility.getFutureTimeInReadableFormatWithColon(2, 0);
            subscriptionTestHelper.validateCreateSubscription(processor, "Negative", weekendService, payload, expectedStatusMessage);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionPastDaysDP",description ="Create subscription for 7 days, start date=past date, Negative Scenario",groups={"Regression"})
    public void createSubscriptionPastDaysTest(String payload,String statusMessage) throws IOException, ParseException {
            String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
            Processor processor = subscriptionHelper.createSubscription(payload);
            subscriptionTestHelper.validateCreateSubscription(processor, "Negative", weekendService, payload,statusMessage );
    }

    @Test(dataProvider="createSubscriptionDP",description ="Create subscription with existing orderId, Negative Scenario",groups={"Regression"})
    public void createSubscriptionWithExistingOrderIdTest(String payload) throws IOException, ParseException {
        String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
        Processor processor = subscriptionHelper.createSubscription(payload);
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String payloadExistingSubscription=jsonHelper.getObjectToJSON(createSubscriptionPojo.setDefaultData().withOrderId(subscriptionId));
        Processor existingSubscriptionProcessor = subscriptionHelper.createSubscription(payloadExistingSubscription);
        String statusMessage="Subscription already exists (SubscriptionId) : "+subscriptionId;
        subscriptionTestHelper.validateCreateSubscription(existingSubscriptionProcessor, "Negative", weekendService, payload,statusMessage);
    }

    @Test(dataProvider="createSubscriptionWithBlankFieldDP",description ="Create subscription with blank field, Negative Scenario",groups={"Regression"})
    public void createSubscriptionWithBlankFieldTest(String payload,String statusMessage) throws IOException, ParseException {
        String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
        Processor processor = subscriptionHelper.createSubscription(payload);
        subscriptionTestHelper.validateCreateSubscription(processor, "Negative", weekendService, payload,statusMessage );
    }

    @Test(dataProvider="createSubscriptionWithDeliveryStartTimeGreaterThanEndTimeDP",description ="Create subscription with delivery start time greater than delivery end time, Negative Scenario",groups={"Regression"})
    public void createSubscriptionWithDeliveryStartTimeGreaterThanEndTimeTest(String payload) throws IOException, ParseException {
        String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
        Processor processor = subscriptionHelper.createSubscription(payload);
        subscriptionTestHelper.validateCreateSubscription(processor, "Negative", weekendService, payload,SubscriptionConstant.SUBSCRIPTION_DELIVERYSTARTTIMEGREATERTHANENDTIME_MSG );
    }

    @Test(dataProvider="createSubscriptionBetween10PMTo12AMDP",description ="Create Subscription between 10 PM to 12 AM and delivery slot 3 AM to 4 AM, Negative Scenario",groups={"Regression"})
    public void createSubscriptionBetween10PMTo12AMTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String weekendService = Boolean.toString(JsonPath.read(payload, "$.schedule_details.has_weekend_service"));
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor processor = subscriptionHelper.createSubscription(payload);
            subscriptionTestHelper.validateCreateSubscription(processor, "Negative", weekendService, payload, "Subscription delivery slot cannot be less than 1:0");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }


    //----------------------------------------------------------Skip Subscription Positive Scenario-----------------------------------------------------------------------------

    @Test(dataProvider="createSubscriptionDP",description ="Skip the subscription in pre-cutoff time",groups={"Sanity_Test","Regression"})
    public void skipSubscriptionPreCutoffTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the subscription after 3 meal delivered in pre-cutoff time",groups={"Sanity_Test","Regression"})
    public void skipSubscriptionAfterFewMealDeliveredPreCutoffTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String dateAfterThreeDays=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);


            if(DateUtility.getDayOfDateInReadableFormat(dateAfterThreeDays).equals("Saturday")&&subscriptionType.equals("WEEKDAY")){
                subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(dateAfterThreeDays, 2));
            }else if(DateUtility.getDayOfDateInReadableFormat(dateAfterThreeDays).equals("Sunday")&&subscriptionType.equals("WEEKDAY")){
                subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(dateAfterThreeDays, 1));
            }else{
                subscriptionTestHelper.changeServerTime(dateAfterThreeDays);
            }

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        }finally{
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="first meal delivered, skip the second meal on same day",groups={"Regression"})
    public void skipSecondAfterFirstMealDeliveredTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(2,20)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.futureMeals.mealDetails[0].subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        }finally{
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="skip, pause, resume the subscription on same day",groups={"Regression"})
    public void skipPauseResumeSubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");

            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Positive", subscriptionType, subscriptionEndDate, 0, "success");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validation of skip subscription Not Null Field for weekend service true, false",groups={"Regression"})
    public void skipResponseFieldValidationTest(String payload) throws IOException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            String skipResponse = subscriptionHelper.skipSubscription(subscriptionMealId).ResponseValidator.GetBodyAsText();
            subscriptionTestHelper.validateNodeSkipResumeSubscriptionNotNull(skipResponse);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    //----------------------------------------------------------Skip Subscription Negative Scenario-----------------------------------------------------------------------------

    @Test(dataProvider = "skipSubscriptionPostCutoffDP",description ="Skip the subscription in Post Cutoff time",groups={"Sanity_Test", "Regression"})
    public void pauseSubscriptionPostCutoffTest(String payload,String serverTime) throws ParseException, IOException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_CUTOFFWINDOWSKIPUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }


    @Test(dataProvider="skipSubscriptionPostCutoffDP",description ="Skip the subscription after 3 meal delivered in post-cutoff time",groups={"Sanity_Test","Regression"})
    public void skipSubscriptionAfterFewMealDeliveredPostCutoffTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String dateAfterThreeDays=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
            if(DateUtility.getDayOfDateInReadableFormat(dateAfterThreeDays).equals("Friday")&&subscriptionType.equals("WEEKDAY")) {
                subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(dateAfterThreeDays, 3));
            }else if(DateUtility.getDayOfDateInReadableFormat(dateAfterThreeDays).equals("Saturday")&&subscriptionType.equals("WEEKDAY")){
                subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(dateAfterThreeDays, 2));
            }else if(DateUtility.getDayOfDateInReadableFormat(dateAfterThreeDays).equals("Sunday")&&subscriptionType.equals("WEEKDAY")){
                subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(dateAfterThreeDays, 1));
            }else{
                subscriptionTestHelper.changeServerTime(dateAfterThreeDays);
            }

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_CUTOFFWINDOWSKIPUNSUCCESS_MSG);
        }finally{
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="skip the subscription Which is already completed",groups={"Regression"})
    public void skipAlreadyCompletedSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "COMPLETED");
            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG);

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="skip the subscription Which is already Cancelled",groups={"Regression"})
    public void skipAlreadyCancelledSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");
            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "CANCELLED");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG);

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the meal which is already skipped",groups={"Regression"})
    public void skipMealAlreadySkippedMealTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

            Processor skipSameMealProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipSameMealProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_CONSECUTIVESKIPUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the meal, skip the second meal on same day",groups={"Regression"})
    public void skipMealSkipSecondMealOnSameDayTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");
            String subscriptionSecondMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.futureMeals.mealDetails[0].subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

            Processor skipSameMealProcessor = subscriptionHelper.skipSubscription(subscriptionSecondMealId);
            subscriptionTestHelper.validateSkipResponse(skipSameMealProcessor, "Negative", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_CONSECUTIVESKIPUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }


    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }

}
