package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class DeliveryPriceDetails {

    @JsonProperty("totalWithoutDiscount")
    private Long totalWithoutDiscount;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("discount")
    private Long discount;
    @JsonProperty("deliveryFeeList")
    private List<Object> deliveryFeeList = null;
    @JsonProperty("discountMessage")
    private String discountMessage;

    @JsonProperty("totalWithoutDiscount")
    public Long getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public DeliveryPriceDetails withTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Long total) {
        this.total = total;
    }

    public DeliveryPriceDetails withTotal(Long total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Long getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public DeliveryPriceDetails withDiscount(Long discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("deliveryFeeList")
    public List<Object> getDeliveryFeeList() {
        return deliveryFeeList;
    }

    @JsonProperty("deliveryFeeList")
    public void setDeliveryFeeList(List<Object> deliveryFeeList) {
        this.deliveryFeeList = deliveryFeeList;
    }

    public DeliveryPriceDetails withDeliveryFeeList(List<Object> deliveryFeeList) {
        this.deliveryFeeList = deliveryFeeList;
        return this;
    }

    @JsonProperty("discountMessage")
    public String getDiscountMessage() {
        return discountMessage;
    }

    @JsonProperty("discountMessage")
    public void setDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
    }

    public DeliveryPriceDetails withDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
        return this;
    }

}
