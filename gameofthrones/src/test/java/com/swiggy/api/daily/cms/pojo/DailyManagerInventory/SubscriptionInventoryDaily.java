
package com.swiggy.api.daily.cms.pojo.DailyManagerInventory;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "operation",
    "count",
    "store_id",
    "spin",
    "epoch_ranges"
})
public class SubscriptionInventoryDaily {

    @JsonProperty("operation")
    private String operation;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("store_id")
    private String store_id;
    @JsonProperty("spin")
    private String spin;
    @JsonProperty("epoch_ranges")
    private List<Epoch_range> epoch_ranges = null;

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    public SubscriptionInventoryDaily withOperation(String operation) {
        this.operation = operation;
        return this;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public SubscriptionInventoryDaily withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public SubscriptionInventoryDaily withStore_id(String store_id) {
        this.store_id = store_id;
        return this;
    }

    @JsonProperty("spin")
    public String getSpin() {
        return spin;
    }

    @JsonProperty("spin")
    public void setSpin(String spin) {
        this.spin = spin;
    }

    public SubscriptionInventoryDaily withSpin(String spin) {
        this.spin = spin;
        return this;
    }

    @JsonProperty("epoch_ranges")
    public List<Epoch_range> getEpoch_ranges() {
        return epoch_ranges;
    }

    @JsonProperty("epoch_ranges")
    public void setEpoch_ranges(List<Epoch_range> epoch_ranges) {
        this.epoch_ranges = epoch_ranges;
    }

    public SubscriptionInventoryDaily withEpoch_ranges(List<Epoch_range> epoch_ranges) {
        this.epoch_ranges = epoch_ranges;
        return this;
    }

    public  SubscriptionInventoryDaily setData(Integer cou,String storeid,String oprt,String spin,Long startepoc,Long endepoc) {
        List li=new ArrayList();
        Epoch_range ep=new Epoch_range().setData(startepoc,endepoc);
        li.add(ep);
        this.withCount(cou).withStore_id(storeid).withOperation(oprt).withSpin(spin).withEpoch_ranges(li);
        return this;
    }
}