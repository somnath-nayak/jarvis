package com.swiggy.api.daily.promotions.dp;

import com.sun.jna.platform.win32.WinDef;
import com.swiggy.api.daily.promotions.helper.PromotionConstants;
import com.swiggy.api.daily.promotions.helper.PromotionsHelper;
import com.swiggy.api.daily.promotions.pojo.createPromotion.CreatePromotion;
import com.swiggy.api.sf.rng.helper.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import javax.rmi.CORBA.Util;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class PromotionsDP {
    
    @DataProvider(name = "test")
    public static Object[][] test() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
       
            Processor response = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
        promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        promotionsHelper.hitCartRequest( storeId,  skuId, quantity,cartPrice,  itemTotal);
        
    return new Object[][]{{ }};
    }
    
    // One promotion for single SKU
    @DataProvider(name = "createPromotionOnSingleSKUFlatTypeDP")
    public static Object[][] createPromotionOnSingleSKUFlatType() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
//      Processor listingPlanResponse=  promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
//       Processor listingMealResponse= promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
//
//        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{createResponse, storeId[0] , skuId[0]}};
    }
    
    
    
    @DataProvider(name = "createPromotionCheckDefaultDAILYIfRequestingWithOutTenantTypeDP")
    public static Object[][] createPromotionCheckDefaultDAILYIfRequestingWithOutTenantType() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithOutTenant( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
//      Processor listingPlanResponse=  promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
//       Processor listingMealResponse= promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
//
//        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{createResponse, storeId[0] , skuId[0]}};
    }
    
    @DataProvider(name = "createPromotionCheckTenantDP")
    public static Object[][] createPromotionCheckTenant() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"","DEFAULT_AUTOMATION_TEXT");
        Processor createResponseWhiteSpace = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"       ","DEFAULT_AUTOMATION_TEXT");
        Processor createResponseRandomString = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"daily","DEFAULT_AUTOMATION_TEXT");
        
        return new Object[][]{{createResponse,createResponseWhiteSpace,createResponseRandomString}};
    }
    
    @DataProvider(name = "createPromotionCheckPDPCopyTextFieldDP")
    public static Object[][] createPromotionCheckPDPCopyTextFieldDP() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","");
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
       
        Processor createResponseWhiteSpace = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","      ");
        String promotionIdSecond = createResponseWhiteSpace.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionIdSecond, "null");
        Processor updateResponseSecond = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdSecond);
        
        return new Object[][]{{createResponse,createResponseWhiteSpace}};
    }
    
    @DataProvider(name = "createPromotionCheckPDPCopyTextFieldMaxLengthDP")
    public static Object[][] createPromotionCheckPDPCopyTextFieldMaxLength() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT_AUTOMATION_DESCRIPTION_COPY_PDP");
        
        return new Object[][]{{createResponse}};
    }
    @DataProvider(name = "createPromotionCheckStoreAndSKUValueAsEmptyStringDP")
    public static Object[][] createPromotionCheckStoreAndSKUValueAsEmptyString() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        //String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] storeId = new String[]{""};
        //String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{""};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "createPromotionForSameSPINAndSameStoreWithSlotAndWithOutSlotDP")
    public static Object[][] createPromotionForSameSPINAndSameStoreWithSlotAndWithOutSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
       // String[] storeId = new String[]{""};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        //String[] skuId = new String[]{""};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
    
        fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"}; values.add(2,new String[]{"0:2359:ALL"});
        Processor createResponseWithSlot = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        
        return new Object[][]{{createResponse,createResponseWithSlot}};
    }
    
    @DataProvider(name = "createPromotionCheckStoreAndSKUValueDP")
    public static Object[][] createPromotionCheckStoreAndSKUValue() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        //String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] storeId = new String[]{};
        //String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "createPromotionCheckPriceFieldAsEmptyStringDP")
    public static Object[][] createPromotionCheckPriceFieldAsEmptyString() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
   
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{""};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,null);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        discountValues = new String[]{"  "};
        
        Processor createResponseWithWhiteSpace = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        return new Object[][]{{createResponse, createResponseWithWhiteSpace}};
    }
    
    @DataProvider(name = "createPromotionCheckPriceFieldAsZeroValueDP")
    public static Object[][] createPromotionCheckPriceFieldAsZeroValue() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"0"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,null);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "createPromotionCheckPriceFieldAsNegativeValueDP")
    public static Object[][] createPromotionCheckPriceFieldAsNegativeValue() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"-1"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,null);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "createPromotionCheckPriceFieldForPercentMoreThanHundredDP")
    public static Object[][] createPromotionCheckPriceFieldForPercentMoreThanHundred() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"100.01"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,null);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        discountValues = new String[]{"101"};
        Processor createResponseDiscount = promotionsHelper.createPromotionWithGivenValues( fact ,comparisonOperator, values,actionType, actionParam, discountValues,"DAILY","DEFAULT___AUTOMATION");
        return new Object[][]{{createResponse,createResponseDiscount}};
    }
    
    @DataProvider(name = "createPromotionOnSingleSKUPercentageTypeDP")
    public static Object[][] createPromotionOnSingleSKUPercentageType() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor listingPlanResponse=  promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
        Processor listingMealResponse= promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{createResponse,listingPlanResponse ,listingMealResponse,updateResponse , storeId[0] , skuId[0]}};
    }
    
    @DataProvider(name = "createPromotionOnMultipleSKUDP")
    public static Object[][] createPromotionOnMultipleSKU() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000)),Integer.toString(Utility.getRandom(100000,100000000)),Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{createResponse , storeId[0] , skuId}};
    }
    
    @DataProvider(name = "createMultiplePromotionOnSingleSKUDP")
    public static Object[][] createMultiplePromotionOnSingleSKU() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> values2ndPromotion = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] storeIdTwo = new String[]{Integer.toString(Utility.getRandom(1000000,90000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        values2ndPromotion.add(0,storeIdTwo);
        values2ndPromotion.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // create 2nd promotion for same SKU id
        Processor create2ndResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values2ndPromotion,actionType, actionParam, discountValues);
        String promotion2ndId = create2ndResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor update2ndPromotionResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotion2ndId);
        
        
        
        return new Object[][]{{createResponse ,create2ndResponse, storeId[0] , skuId,storeIdTwo[0]}};
    }
    
    @DataProvider(name = "createSKULevelPromotionDP")
    public static Object[][] createSKULevelPromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , skuId}};
    }
    
    @DataProvider(name = "createStoreLevelPromotionDP")
    public static Object[][] createStoreLevelPromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID"};
        String[] comparisonOperator = new String[]{"EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,storeId);
        values.add(0,storeId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , storeId[0]}};
    }
    
    
    
    
    @DataProvider(name = "createMultipleStoreLevelPromotionDP")
    public static Object[][] createMultipleStoreLevelPromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","STORE_ID","STORE_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] storeTwoId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] storeThreeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,storeTwoId);
        values.add(2,storeThreeId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , storeId, storeTwoId, storeThreeId}};
    }
    
    @DataProvider(name = "checkCreatedByAndUpdatedByEmptyStringDP")
    public static Object[][] checkCreatedByAndUpdatedByEmptyString() throws IOException {
        CreatePromotion createPromotion = new CreatePromotion();
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
    
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedBy("").withUpdatedBy("");
        Processor createResponse= promotionsHelper.createPromotion( request);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse }};
    }
    
    @DataProvider(name = "checkCreatedAtAndUpdatedATDP")
    public static Object[][] checkCreatedAtAndUpdatedAT() throws IOException {
        CreatePromotion createPromotion = new CreatePromotion();
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createResponse= promotionsHelper.createPromotion( request);
       Processor getActiveResponse = promotionsHelper.getActivePromotions();
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse,createdDate,getActiveResponse, promotionId }};
    }
    
    @DataProvider(name = "checkCreatedAtAndUpdatedATWithRandomStringDP")
    public static Object[][] checkCreatedAtAndUpdatedATWithRandomString() throws IOException {
        CreatePromotion createPromotion = new CreatePromotion();
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        //String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt("test").withUpdatedAt("test");
        Processor createResponse= promotionsHelper.createPromotion( request);
        String status = createResponse.ResponseValidator.GetBodyAsText();
        return new Object[][]{{status }};
    }
    
    @DataProvider(name = "checkCreatedAtAndUpdatedATWithEmptyStringDP")
    public static Object[][] checkCreatedAtAndUpdatedATWithEmptyString() throws IOException {
        CreatePromotion createPromotion = new CreatePromotion();
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        //String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt("").withUpdatedAt("");
        Processor createResponse= promotionsHelper.createPromotion( request);
      
        return new Object[][]{{createResponse }};
    }
    
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        // 2nd promotion
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTwoType, actionParam, discountValues);
        String promotionTwodata = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(promotionTwodata, null);
        
       
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesTwo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] storeTwoId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        valuesTwo.add(0,storeTwoId); valuesTwo.add(1,skuId);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesTwo,actionTwoType, actionParam, discountValues);
        String promotionTwoId = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoId, null);
        
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreWithSlotDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStoreWithSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"0:2359:ALL"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTwoType, actionParam, discountValues);
        String promotionTwodata = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(promotionTwodata, null);
        
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresWithSlotDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresWithSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesTwo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] storeTwoId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"0:2359:ALL"});
        valuesTwo.add(0,storeTwoId); valuesTwo.add(1,skuId); valuesTwo.add(2,new String[]{"0:2359:ALL"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesTwo,actionTwoType, actionParam, discountValues);
        String promotionTwoId = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoId, null);
        
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtConflictingDateDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtConflictingDate() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"0:2359:ALL"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createSecondResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtDiffDateDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtDiffDate() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"0:2359:ALL"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createSecondResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
    
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtSameDateButDiffSlotDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtSameDateButDiffSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"10:1200:MON"});
        valuesSecondPromo.add(0,storeId);
        valuesSecondPromo.add(1,skuId);
        valuesSecondPromo.add(2,new String[]{"1201:1400:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionTwoType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesSecondPromo,actionTwoType, actionParam, discountValues);
        
       // String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
       // Assert.assertEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtDiffDateButSameSlotDP")
    public static Object[][] checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtDiffDateButSameSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"10:1200:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createSecondResponse= promotionsHelper.createPromotion( request);
    
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);

       Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateDP")
    public static Object[][] checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDate() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        valuesSecondPromo.add(0,storeId);
        valuesSecondPromo.add(1,skuIdOfSecond);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
    
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesSecondPromo,actionType, actionParam, discountValues);
    
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, null);
    
//        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
//        Processor createSecondResponse= promotionsHelper.createPromotion( request);
//
//        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
//        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateAndSameSlotDP")
    public static Object[][] checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateAndSameSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        valuesSecondPromo.add(0,storeId);
        valuesSecondPromo.add(1,skuIdOfSecond);
        valuesSecondPromo.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesSecondPromo,actionType, actionParam, discountValues);
        
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, null);

//        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
//        Processor createSecondResponse= promotionsHelper.createPromotion( request);
//
//        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
//        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateMultiplePromotionForSameSKUOfAStoreAtSameDateAndSameSlotDP")
    public static Object[][] checkCreateMultiplePromotionForSameSKUOfAStoreAtSameDateAndSameSlot() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        //List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
//        valuesSecondPromo.add(0,storeId);
//        valuesSecondPromo.add(1,skuIdOfSecond);
//        valuesSecondPromo.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(promotionTwoData, null);

//        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
//        Processor createSecondResponse= promotionsHelper.createPromotion( request);
//
//        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
//        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
//        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    @DataProvider(name = "checkCreateMultiplePromotionForSameSKUOfAStoreAtDiffDateDP")
    public static Object[][] checkCreateMultiplePromotionForSameSKUOfAStoreAtDiffDate() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        //List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
//        valuesSecondPromo.add(0,storeId);
//        valuesSecondPromo.add(1,skuIdOfSecond);
//        valuesSecondPromo.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        // 2nd promotion
        
//        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
//
//        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
//        Assert.assertEquals(promotionTwoData, null);

        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(31,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(60,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createSecondResponse= promotionsHelper.createPromotion( request);

        String promotionTwoData = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);

        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoData);
        
        return new Object[][]{{createResponse , createSecondResponse}};
    }
    
    
    @DataProvider(name = "checkCreatePromotionForOverLappingSlotForSameDayForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionForOverLappingSlotForSameDayForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON","0:2359:MON","2000:2101:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionData = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int status = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
      
       // Assert.assertEquals(promotionData, "{}","Creating promotion with the over lapping time slot of same day ");
        
//        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse,status}};
    }
    
   @DataProvider(name = "checkCreatePromotionForLateNightSlotForASKUOfAStoreDP")
   public static Object[][] checkCreatePromotionForLateNightSlotForASKUOfAStore() throws IOException {
       PromotionsHelper promotionsHelper = new PromotionsHelper();
       CreatePromotion createPromotion = new CreatePromotion();
       String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
       String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
       List<String[]> values = new ArrayList<>();
       String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
       String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
       String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
       values.add(0,storeId);
       values.add(1,skuId);
       values.add(2,new String[]{"2100:2359:ALL","0:4:ALL"});
       String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
       String[] actionParam = new String[]{"ITEMS_PRICE"};
       String[]  discountValues = new String[]{"10"};
       List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
       List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
       String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
       Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
       String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
       Assert.assertNotEquals(promotionId, "null");

        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
    
       return new Object[][]{{createResponse}};
   }
    
    
    
    @DataProvider(name = "checkCreatePromotionWithMultipleSlotForADayForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionWithMultipleSlotForADayForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON","0:4:MON","1000:1230:MON","1231:1530:MON","1800:2059:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");

        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkCreatePromotionWithMultipleSlotForDifferentDaysForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionWithMultipleSlotForDifferentDaysForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON","0:4:TUE","1000:1230:THUR","1231:1530:FRI","1800:2059:SAT","1531:1759:SUN"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkCreatePromotionWithSameSlotForDifferentDaysForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionWithSameSlotForDifferentDaysForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON","2100:2359:TUE","2100:2359:THUR","2100:2359:FRI","2100:2359:SAT","2100:2359:SUN"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkCreatePromotionDayALLWithOtherDaysSlotForDifferentTimeForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionDayALLWithOtherDaysSlotForDifferentTimeForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:ALL","1000:1200:MON","1600:2059:TUE"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkCreatePromotionDayALLWithOtherDaysSlotAtOverlappingTimeForASKUOfAStoreDP")
    public static Object[][] checkCreatePromotionDayALLWithOtherDaysSlotAtOverlappingTimeForASKUOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:ALL","1000:1700:MON","1600:2100:TUE"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0,"able to create promotion time of ALL day is overlapping with other days time");
        
//        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkPromotionTypeInCreatePromotionDP")
    public static Object[][] checkPromotionTypeInCreatePromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withOutPromotionTypeParam().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoData, "null");
        Processor getActiveResponse = promotionsHelper.getActivePromotions();
        return new Object[][]{{createResponse,getActiveResponse,promotionTwoData}};
    }
    
    @DataProvider(name = "checkPromotionTypeAsEmptyStringInCreatePromotionDP")
    public static Object[][] checkPromotionTypeAsEmptyStringInCreatePromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate).withPromotionType("");
        Processor createResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.status"));
       // Assert.assertEquals(promotionTwoData, "");
        
        return new Object[][]{{promotionTwoData}};
    }
    
    
    @DataProvider(name = "checkPromotionNameInCreatePromotionDP")
    public static Object[][] checkPromotionNameInCreatePromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withOutPromotionNameParam().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate);
        Processor createResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = Integer.toString(createResponse.ResponseValidator.GetNodeValueAsInt("$.status"));
         Assert.assertEquals(promotionTwoData, "400","able to create promotion without name field");
        
        return new Object[][]{{promotionTwoData}};
    }
    
    @DataProvider(name = "checkPromotionNameAsEmptyStringInCreatePromotionDP")
    public static Object[][] checkPromotionNameAsEmptyStringInCreatePromotion() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] skuIdOfSecond = new String[]{Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        CreatePromotion request =createPromotion.withDefaultValues().withRules(promotionsHelper.populateListOfRules(fact,comparisonOperator,values)).withActions(promotionsHelper.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withCreatedAt(createdDate).withUpdatedAt(createdDate).withName("");
        Processor createResponse= promotionsHelper.createPromotion( request);
        
        String promotionTwoData = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        Assert.assertEquals(promotionTwoData, null,"able to create promotion empty string for name field");
        
        return new Object[][]{{createResponse}};
    }
    
    @DataProvider(name = "checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoDP")
    public static Object[][] checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromo() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String[] skuIdOfSecond = new String[]{sku,Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
    
        valuesSecondPromo.add(0,storeId);
        valuesSecondPromo.add(1,skuIdOfSecond);
        valuesSecondPromo.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
    
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesSecondPromo,actionType, actionParam, discountValues);
        String promotionTwoId = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        Assert.assertEquals(promotionTwoId, null);

        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{createSecondResponse}};
    }
    
    
    @DataProvider(name = "checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoAndSKUStoresDiffDP")
    public static Object[][] checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoAndSKUStoresDiff() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        CreatePromotion createPromotion = new CreatePromotion();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        List<String[]> valuesSecondPromo = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String[] storeIdSecond = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String[] skuIdOfSecond = new String[]{sku,Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        values.add(0,storeId);
        values.add(1,skuId);
        values.add(2,new String[]{"2100:2359:MON"});
        
        valuesSecondPromo.add(0,storeIdSecond);
        valuesSecondPromo.add(1,skuIdOfSecond);
        valuesSecondPromo.add(2,new String[]{"2100:2359:MON"});
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor createSecondResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, valuesSecondPromo,actionType, actionParam, discountValues);
        String promotionTwoId = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionTwoId, null);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateSecondResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionTwoId);
        return new Object[][]{{createSecondResponse}};
    }
    
    
    @DataProvider(name = "CheckPlanMealListingAndCartWhenPromotionIsActiveForAGivenSKUDP")
    public static Object[][] CheckPlanMealListingAndCartWhenPromotionIsActiveForAGivenSKU() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,70000000))};
        String[] skuId = new String[]{PromotionConstants.NoPromotionSKU};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuId);
        values.add(0,storeId);
        values.add(1,skuId);
        Integer itemTotal = 735;
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,100);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        
       Processor planListingResponse = promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
        Processor mealListingResponse =promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeId,  skuId, quantity,cartPrice,  itemTotal);
        
        return new Object[][]{{planListingResponse, mealListingResponse, cartResponse}};
    }
    
    @DataProvider(name = "CheckPlanListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckPlanListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,10);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
       // Processor mealListingResponse =promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        //Processor cartResponse = promotionsHelper.hitCartRequest( storeId,  skuId, quantity,cartPrice,  itemTotal);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckMealListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,12);quantity.add(0,10);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor mealListingResponse =promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        //Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{ mealListingResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    @DataProvider(name = "CheckCartListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckCartListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,10);quantity.add(0,8);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        
        String[] actionType = new String[]{"PERCENTAGE_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    // Flat discount
    
    @DataProvider(name = "CheckPlanListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckPlanListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String[] storeId = new String[]{Integer.toString(Utility.getRandom(100000,100000000))};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,10);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,100);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeId, allSkuId ,  price);
        // Processor mealListingResponse =promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        //Processor cartResponse = promotionsHelper.hitCartRequest( storeId,  skuId, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckMealListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,12);quantity.add(0,10);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor mealListingResponse =promotionsHelper.hitMealListing(storeId, allSkuId ,  price);
        //Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{ mealListingResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    @DataProvider(name = "CheckCartListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP")
    public static Object[][] CheckCartListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStore() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,10);quantity.add(0,8);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion}};
    }
    
    
    // Same SKU in Multi Store Flat discount
    
    @DataProvider(name = "CheckPlanListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckPlanListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{sku, Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);listingPriceSecondStore.add(1,5000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckMealListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{sku, Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);listingPriceSecondStore.add(1,5000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
    
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckCartListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId,noPromotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion,sku,skuNoPromotion};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);quantity.add(3,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        cartPrice.add(2,PromotionConstants.itemPrice);cartPrice.add(3,2000);
        
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    // Same SKU in Multi Store %age discount
    
    @DataProvider(name = "CheckPlanListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckPlanListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{sku, Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);listingPriceSecondStore.add(1,5000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckMealListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{sku, Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);listingPriceSecondStore.add(1,5000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP")
    public static Object[][] CheckCartListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresent() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId,noPromotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion,sku,skuNoPromotion};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);quantity.add(3,2);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        cartPrice.add(2,PromotionConstants.itemPrice);cartPrice.add(3,2000);
        
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    // Multi Store FLAT discount higher than SKU's price
    
    @DataProvider(name = "CheckPlanListingWhenFlatPriceIsHigherThanSKUsPriceDP")
    public static Object[][] CheckPlanListingWhenFlatPriceIsHigherThanSKUsPrice() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{ Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"1010"};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatPriceIsHigherThanSKUsPriceDP")
    public static Object[][] CheckMealListingWhenFlatPriceIsHigherThanSKUsPrice() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{ Integer.toString(Utility.getRandom(10000,10000000))};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,5000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"1010"};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenFlatPriceIsHigherThanSKUsPriceDP")
    public static Object[][] CheckCartListingWhenFlatPriceIsHigherThanSKUsPrice() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storesForCart = new String[]{promotionActiveStoreId,noPromotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,skuNoPromotion,skuNoPromotion};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,2000);
        cartPrice.add(2,PromotionConstants.itemPrice);
        
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"1010"};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion,noPromotionStore}};
    }
    
    // Multi Store FLAT & %age both for a SKU in different stores
    
    @DataProvider(name = "CheckPlanListingWhenFlatAndPercentBothForASKUInDifferentStoresDP")
    public static Object[][] CheckPlanListingWhenFlatAndPercentBothForASKUInDifferentStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String percentPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{percentPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,percentPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{ sku};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
    
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        values.add(0,storeIdForSecond); values.add(1,skuId);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,percentPromotionStore,  promotionIdForPercent}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatAndPercentBothForASKUInDifferentStoresDP")
    public static Object[][] CheckMealListingWhenFlatAndPercentBothForASKUInDifferentStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String percentPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{percentPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,percentPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuNoPromotion};
        String[] skuIdSetSecondStore = new String[]{ sku};
        String[] skuId = new String[]{sku};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,200);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
    
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        values.add(0,storeIdForSecond); values.add(1,skuId);
    
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
    
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuNoPromotion,percentPromotionStore,  promotionIdForPercent}};
    }
    
    
    
    @DataProvider(name = "CheckCartWhenFlatAndPercentBothForASKUInDifferentStoresDP")
    public static Object[][] CheckCartWhenFlatAndPercentBothForASKUInDifferentStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStoreForPercent = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storeIdSecond = new String[]{promotionStoreForPercent};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionStoreForPercent,promotionStoreForPercent};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku};
        String[] skuIdForCart = new String[]{sku,sku,Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        String[] actionTypeForPercent = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForPercent = new String[]{PromotionConstants.discountValueForPercent};
    
        values.add(0,storeIdSecond);
        values.add(1,skuId);
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForPercent, actionParam, discountValuesForPercent);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuNoPromotion,promotionStoreForPercent,promotionIdForPercent}};
    }
    
    
    // FLAT & %age both for different SKU of same store
    
    @DataProvider(name = "CheckPlanListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP")
    public static Object[][] CheckPlanListingWhenFlatAndPercentBothForDiffSKUsOfSameStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{noPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuTwo};
        String[] skuIdSetSecondStore = new String[]{ skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeId); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,noPromotionStore,  promotionIdForPercent}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP")
    public static Object[][] CheckMealListingWhenFlatAndPercentBothForDiffSKUsOfSameStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{noPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku,skuTwo};
        String[] skuIdSetSecondStore = new String[]{ skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);listingPrice.add(1,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
    
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        values.add(0,storeId); values.add(1,skuIdTwo);
    
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
    
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,noPromotionStore,  promotionIdForPercent}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP")
    public static Object[][] CheckCartListingWhenFlatAndPercentBothForDiffSKUsOfSameStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String noPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storeIdSecond = new String[]{noPromotionStore};
        String[] storesForCart = new String[]{promotionActiveStoreId,promotionActiveStoreId,noPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] skuIdForCart = new String[]{sku,skuTwo,Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
    
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        String[] actionTypeForPercent = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForPercent = new String[]{PromotionConstants.discountValueForPercent};
        
        values.add(0,storeId);
        values.add(1,skuIdTwo);
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForPercent, actionParam, discountValuesForPercent);
        String promotionIdForPercent = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionIdForPercent);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuTwo,noPromotionStore,promotionIdForPercent}};
    }
    
    
    // FLAT discount for different SKU of different stores
    
    @DataProvider(name = "CheckPlanListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckPlanListingWhenFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[0]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForFlat};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckMealListingWhenFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
    
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[0]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForFlat};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
    
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
    
    
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckCartListingWhenFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storeIdSecond = new String[]{secondPromotionStore};
        String[] storesForCart = new String[]{promotionActiveStoreId,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] skuIdForCart = new String[]{sku,skuTwo,Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        String[] actionTypeForPercent = new String[]{PromotionConstants.actionType[0]};
        String[]  discountValuesForPercent = new String[]{PromotionConstants.discountValueForFlat};
        
        values.add(0,storeIdSecond);
        values.add(1,skuIdTwo);
        Processor secondPromotion = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForPercent, actionParam, discountValuesForPercent);
        String secondPromotionId = secondPromotion.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,secondPromotionId}};
    }
    
    
    // Percent discount for different SKU of different stores
    
    @DataProvider(name = "CheckPlanListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckPlanListingWhenPercentDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckMealListingWhenPercentDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckCartListingWhenPercentDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storeIdSecond = new String[]{secondPromotionStore};
        String[] storesForCart = new String[]{promotionActiveStoreId,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] skuIdForCart = new String[]{sku,skuTwo,Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        
        String[] actionType = new String[]{PromotionConstants.actionType[1]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForPercent};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        String[] actionTypeForPercent = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForPercent = new String[]{PromotionConstants.discountValueForPercent};
        
        values.add(0,storeIdSecond);
        values.add(1,skuIdTwo);
        Processor secondPromotion = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForPercent, actionParam, discountValuesForPercent);
        String secondPromotionId = secondPromotion.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,secondPromotionId}};
    }
    
    // Percent & Flat discount for different SKU of different stores
    @DataProvider(name = "CheckPlanListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckPlanListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
    
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
    
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
    
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
    
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
    
    
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    @DataProvider(name = "CheckMealListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckMealListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updateFlatResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{planListingResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,  secondPromotionId}};
    }
    
    
    
    @DataProvider(name = "CheckCartListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP")
    public static Object[][] CheckCartListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String promotionActiveStoreId = Integer.toString(Utility.getRandom(100000,100000000));
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionActiveStoreId};
        String[] storeIdSecond = new String[]{secondPromotionStore};
        String[] storesForCart = new String[]{promotionActiveStoreId,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuNoPromotion = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] skuIdForCart = new String[]{sku,skuTwo,Integer.toString(Utility.getRandom(10000,10000000))};
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        Integer itemTotal = 735;
        Processor createResponse = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        String[] actionTypeForPercent = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForPercent = new String[]{PromotionConstants.discountValueForPercent};
        
        values.add(0,storeIdSecond);
        values.add(1,skuIdTwo);
        Processor secondPromotion = promotionsHelper.createPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForPercent, actionParam, discountValuesForPercent);
        String secondPromotionId = secondPromotion.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        Processor cartResponse = promotionsHelper.hitCartRequest( storesForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        return new Object[][]{{  cartResponse,promotionId,storeId[0],sku,skuTwo,secondPromotionStore,secondPromotionId}};
    }
    
    
    @DataProvider(name = "CheckListingAndCartWhenExpiredPercentAndFlatDiscountForAStoresDP")
    public static Object[][] CheckListingAndCartWhenExpiredPercentAndFlatDiscountForAStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createExpiredPromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createExpiredPromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
       
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenFuturePercentAndFlatDiscountForAStoresDP")
    public static Object[][] CheckListingAndCartWhenFuturePercentAndFlatDiscountForAStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createFuturePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createFuturePromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
  
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenFuturePercentAndFlatDiscountWithSlotForAStoresDP")
    public static Object[][] CheckListingAndCartWhenFuturePercentAndFlatDiscountWithSlotForAStores() throws IOException, ParseException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId); values.add(2,new String[]{promotionsHelper.presentDayTimeSlot("HHmm",240,"ALL")});
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createFuturePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createFuturePromotionWithGivenInputs( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatInActiveDiscountForAStoresDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatInActiveDiscountForAStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithActiveFlagZero( fact ,comparisonOperator, values,actionType, actionParam, discountValues);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithActiveFlagZero( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenFuturePercentAndFlatDiscountWithActiveFlagZeroForAStoresDP")
    public static Object[][] CheckListingAndCartWhenFuturePercentAndFlatDiscountWithActiveFlagZeroForAStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createFuturePromotionWithActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,0);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createFuturePromotionWithActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,0);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWithActiveFlagWithGivenInputForAStoresDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWithActiveFlagWithGivenInputForAStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
    
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWithPresentlyActiveTimeSlotDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWithPresentlyActiveTimeSlot() throws IOException, ParseException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] slot = new String[]{promotionsHelper.presentDayTimeSlot( "HHmm",  60 , "ALL")};
      //  String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60 ,60, "ALL")};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId); values.add(2,slot);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);values.add(2,slot);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        
//        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
//        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWithInvalidFormatTimeSlotDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWithInvalidFormatTimeSlot() throws IOException, ParseException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] slot = new String[]{promotionsHelper.presentDayTimeSlot( "HHmm",  60 , "ALLll")};
        //  String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60 ,60, "ALL")};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId); values.add(2,slot);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);values.add(2,slot);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
     
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWithFutureActiveTimeSlotDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWithFutureActiveTimeSlot() throws IOException, ParseException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60,5 , "ALL")};
        //  String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60 ,60, "ALL")};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId); values.add(2,slot);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);values.add(2,slot);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        // Get all Active promotions API
        Processor allActivePromotions= promotionsHelper.getActivePromotions();
        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
       
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId,getActivePromotions}};
    }
    
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWithPastTimeSlotDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWithPastTimeSlot() throws IOException, ParseException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID","TIME_SLOT"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,secondPromotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60,-62 , "ALL")};
        //  String[] slot = new String[]{promotionsHelper.futureTimeSlot( "HHmm",  60 ,60, "ALL")};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId); values.add(2,slot);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);values.add(2,slot);
        
        String promotionId = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(promotionId, "null");
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenActiveFlag( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2);
        String secondPromotionId = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        Assert.assertNotEquals(secondPromotionId, "null");
        
        
        Processor planListingResponse = promotionsHelper.hitPlanListing( storeIdForListing, allSkuId ,  price);
        Processor mealListingResponse = promotionsHelper.hitMealListing( storeIdForListing, allSkuId ,  price);
        Processor cartResponse = promotionsHelper.hitCartRequest( storeIdForCart,  skuIdForCart, quantity,cartPrice,  itemTotal);
        // Get all Active promotions API
        Processor allActivePromotions= promotionsHelper.getActivePromotions();
        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        
        Processor updateResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, promotionId);
        Processor updatePercentResponse = promotionsHelper.updatePromotionWithGivenInputs( fact ,comparisonOperator, values,actionType, actionParam, discountValues, secondPromotionId);
        
        return new Object[][]{{planListingResponse,mealListingResponse,cartResponse,promotionId,secondPromotionId,getActivePromotions}};
    }
    
    
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromIsGreaterThanValidTillStoresDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromIsGreaterThanValidTillStores() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenValidFromAndValidTill( fact ,comparisonOperator, values,actionType, actionParam, discountValues,2,1);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionOne = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
       
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenValidFromAndValidTill( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,2,1);
        String secondPromotion = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        
        return new Object[][]{{promotionOne,secondPromotion}};
    }
    
    @DataProvider(name = "CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromAndValidTillIsPresentDateDP")
    public static Object[][] CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromAndValidTillIsPresentDate() throws IOException {
        PromotionsHelper promotionsHelper = new PromotionsHelper();
        String[] fact = new String[]{"STORE_ID","SKU_ID",};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        String secondPromotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String promotionStore = Integer.toString(Utility.getRandom(100000,100000000));
        String[] storeId = new String[]{promotionStore};  String[] storeIdForSecond = new String[]{secondPromotionStore};
        String[] storeIdForListing = new String[]{promotionStore,secondPromotionStore};
        String[] storeIdForCart = new String[]{promotionStore,promotionStore,secondPromotionStore};
        String sku = Integer.toString(Utility.getRandom(100000,100000000));
        String skuTwo = Integer.toString(Utility.getRandom(100000,100000000));
        String skuSecondStore = Integer.toString(Utility.getRandom(10000,10000000));
        String[] skuIdSet = new String[]{sku};
        String[] skuIdSetSecondStore = new String[]{ skuTwo,skuSecondStore};String [] skuIdForCart = new String[]{sku,skuTwo,skuSecondStore};
        String[] skuId = new String[]{sku}; String[] skuIdTwo = new String[]{skuTwo};
        List<String[]> allSkuId = new ArrayList<>(); allSkuId.add(0,skuIdSet);allSkuId.add(1,skuIdSetSecondStore);
        values.add(0,storeId);
        values.add(1,skuId);
        List<Integer> listingPrice = new ArrayList<>(); listingPrice.add(0,PromotionConstants.itemPrice);
        List<Integer> listingPriceSecondStore = new ArrayList<>();
        listingPriceSecondStore.add(0,PromotionConstants.itemPrice); listingPriceSecondStore.add(1,2000);
        List<List<Integer>> price = new ArrayList<>(); price.add(0,listingPrice);price.add(1,listingPriceSecondStore);
        String[] actionType = new String[]{PromotionConstants.actionType[0]};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{PromotionConstants.discountValueForFlat};
        List<Integer> quantity = new ArrayList<>(); quantity.add(0,4);quantity.add(1,10);
        quantity.add(2,1);
        Integer itemTotal = 735;
        List<Integer> cartPrice = new ArrayList<>(); cartPrice.add(0,PromotionConstants.itemPrice);cartPrice.add(1,PromotionConstants.itemPrice);
        cartPrice.add(2,2000);
        String[] actionTypeForSecond = new String[]{PromotionConstants.actionType[1]};
        String[]  discountValuesForSecond = new String[]{PromotionConstants.discountValueForPercent};
        Processor createResponse = promotionsHelper.createPromotionWithGivenValidFromAndValidTill( fact ,comparisonOperator, values,actionType, actionParam, discountValues,0,0);
        
        values.add(0,storeIdForSecond); values.add(1,skuIdTwo);
        
        String promotionOne = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        
        
        Processor createPercentResponse = promotionsHelper.createPromotionWithGivenValidFromAndValidTill( fact ,comparisonOperator, values,actionTypeForSecond, actionParam, discountValuesForSecond,0,0);
        String secondPromotion = createPercentResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        
        return new Object[][]{{promotionOne,secondPromotion}};
    }
    
    
}
