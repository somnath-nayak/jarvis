package com.swiggy.api.daily.delivery.pojo;

import java.time.LocalDateTime;

public class DailySubSlot {
    Long startTime;
    Long endTime;
    String day;
    LocalDateTime localDateTimeStart;
    LocalDateTime localDateTimeEnd;

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public LocalDateTime getLocalDateTimeStart() {
        return localDateTimeStart;
    }

    public void setLocalDateTimeStart(LocalDateTime localDateTimeStart) {
        this.localDateTimeStart = localDateTimeStart;
    }

    public LocalDateTime getLocalDateTimeEnd() {
        return localDateTimeEnd;
    }

    public void setLocalDateTimeEnd(LocalDateTime localDateTimeEnd) {
        this.localDateTimeEnd = localDateTimeEnd;
    }

    public DailySubSlot(Long startTime, Long endTime, String day, LocalDateTime localDateTimeStart, LocalDateTime localDateTimeEnd) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.localDateTimeStart = localDateTimeStart;
        this.localDateTimeEnd = localDateTimeEnd;
    }

    public DailySubSlot() {
    }
}