package com.swiggy.api.daily.mealSlot.helper;

import com.swiggy.api.daily.mealSlot.pojo.MealSlotsPOJO;
import com.swiggy.api.daily.mealSlot.pojo.UpdateMealSlotsPOJO;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.HashMap;

public class MealSlotHelper {

    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();

    public Processor getMealSlots(String cityId, String timings) throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "getMealSlot", gameofthrones);
        String[] urlparams = new String[] {cityId,timings};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor createMeal(MealSlotsPOJO payload) throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "createMealSlot", gameofthrones);
        String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
        Processor processor = new Processor(service, requestHeaders, payloadparams, null);
        return processor;
    }

    public Processor updateMeal(UpdateMealSlotsPOJO payload) throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "updateMealSlot", gameofthrones);
        String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
        Processor processor = new Processor(service, requestHeaders, payloadparams, null);
        return processor;
    }

    public Processor deleteMeal(String cityId) throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "deleteMealSlot", gameofthrones);
        String[] urlparams = new String[] {cityId};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor getCacheData() throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "getCacheData", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, null);
        return processor;
    }

    public Processor evictCacheData() throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "evictCache", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null, null);
        return processor;
    }

    public Processor getMealSlotByID(String mealID) throws Exception
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("mealslot", "getMealSlotByID", gameofthrones);
        String[] urlparams = new String[] {mealID};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }


}
