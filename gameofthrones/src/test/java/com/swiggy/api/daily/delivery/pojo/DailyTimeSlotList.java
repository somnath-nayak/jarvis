package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class DailyTimeSlotList {
    @JsonProperty
    private List<DailyTimeSlot> dailyTimeSlots;

    public List<DailyTimeSlot> getDailyTimeSlots() {
        return dailyTimeSlots;
    }

    public void setDailyTimeSlots(List<DailyTimeSlot> dailyTimeSlots) {
        this.dailyTimeSlots = dailyTimeSlots;
    }

    public DailyTimeSlotList(List<DailyTimeSlot> dailyTimeSlots) {
        this.dailyTimeSlots = dailyTimeSlots;
    }
}
