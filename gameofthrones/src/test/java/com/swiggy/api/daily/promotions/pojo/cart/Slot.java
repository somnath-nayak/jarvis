package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Slot {
    @JsonProperty("startTime")
    private String startTime;
    @JsonProperty("endTime")
    private String endTime;
    
    public String getStartTime() {
        return startTime;
    }
    
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
    
    public String getEndTime() {
        return endTime;
    }
    
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
    
    public Slot withStartTime(String startTime){
        setStartTime(startTime);
        return this;
    }
    public Slot withEndTime(String endTime){
        setEndTime(endTime);
        return this;
    }
    public Slot withGivenValues(String startTime,String endTime){
        setEndTime(endTime); setStartTime(startTime);
        return this;
    }
    
    public Slot  slotWithGivenValues(String startTime, String endTime){
       
         withGivenValues( startTime, endTime);
        return this;
    }
    
}
