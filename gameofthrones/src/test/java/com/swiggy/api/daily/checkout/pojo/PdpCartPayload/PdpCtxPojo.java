package com.swiggy.api.daily.checkout.pojo.PdpCartPayload;

import com.swiggy.api.daily.checkout.pojo.SlotDetails;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class PdpCtxPojo {

    @JsonProperty("excluded_dates")
    private List<Object> excludedDates = null;
    @JsonProperty("start_date")
    private Integer startDate;
    @JsonProperty("end_date")
    private Integer endDate;
    @JsonProperty("has_weekend")
    private Boolean hasWeekend;
    @JsonProperty("meal_slot_id")
    private String mealSlotId;



    @JsonProperty("excluded_dates")
    public List<Object> getExcludedDates() {
        return excludedDates;
    }

    @JsonProperty("excluded_dates")
    public void setExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
    }

    @JsonProperty("start_date")
    public Integer getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public void setStartDate(Integer startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("end_date")
    public Integer getEndDate() {
        return endDate;
    }

    @JsonProperty("end_date")
    public void setEndDate(Integer endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("has_weekend")
    public Boolean getHasWeekend() {
        return hasWeekend;
    }

    @JsonProperty("has_weekend")
    public void setHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
    }

    @JsonProperty("meal_slot_id")
    public String getMealSlotId() {
        return mealSlotId;
    }

    @JsonProperty("meal_slot_id")
    public void setMealSlotId(String mealSlotId) {
        this.mealSlotId = mealSlotId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("slotDetails").append("excludedDates", excludedDates).append("startDate", startDate).append("endDate", endDate).append("hasWeekend", hasWeekend).append("mealSlotId", mealSlotId).toString();
    }
}
