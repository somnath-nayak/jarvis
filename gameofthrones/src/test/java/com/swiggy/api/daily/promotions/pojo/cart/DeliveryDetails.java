package com.swiggy.api.daily.promotions.pojo.cart;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class DeliveryDetails {
    @JsonProperty("type")
    private String type;
    @JsonProperty("distanceDetails")
    private DistanceDetails distanceDetails;
    @JsonProperty("slot")
    private Slot slot;
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public DistanceDetails getDistanceDetails() {
        return distanceDetails;
    }
    
    public void setDistanceDetails(DistanceDetails distanceDetails) {
        this.distanceDetails = distanceDetails;
    }
    
    public Slot getSlot() {
        return slot;
    }
    
    public void setSlot(Slot slot) {
        this.slot = slot;
    }
    
    public DeliveryDetails withType(String type){
        setType(type);
        return this;
    }
    
    public DeliveryDetails withDistanceDetails(DistanceDetails distanceDetails){
        setDistanceDetails(distanceDetails);
        return this;
    }
    
    public DeliveryDetails withSlot(Slot slot){
        setSlot(slot);
        return this;
    }
    
    public DeliveryDetails withDefaultValues(){
    
     setType("SWIGGY");
     setDistanceDetails(new DistanceDetails().listOfDistanceDetailsWithGivenValues("REGULAR", 1.8));
     setSlot(new Slot().slotWithGivenValues(Utility.getPresentTimeInMilliSeconds(), Utility.getFuturetimeInMilliSeconds(600000)));
    return this;
    }
    
//    public List<DeliveryDetails> listOfDeliveryDetailsWithDefaultValues(){
//        List<DeliveryDetails> deliveryDetailsList = new ArrayList<>();
//        deliveryDetailsList.add(0, withDefaultValues());
//        return deliveryDetailsList;
//    }
    
}
