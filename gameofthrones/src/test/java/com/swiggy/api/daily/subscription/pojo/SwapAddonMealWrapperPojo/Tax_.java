
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "type",
    "cgst",
    "sgst",
    "igst",
    "total"
})
public class Tax_ {

    @JsonProperty("type")
    private String type;
    @JsonProperty("cgst")
    private Integer cgst;
    @JsonProperty("sgst")
    private Integer sgst;
    @JsonProperty("igst")
    private Integer igst;
    @JsonProperty("total")
    private Integer total;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Tax_ withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("cgst")
    public Integer getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(Integer cgst) {
        this.cgst = cgst;
    }

    public Tax_ withCgst(Integer cgst) {
        this.cgst = cgst;
        return this;
    }

    @JsonProperty("sgst")
    public Integer getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(Integer sgst) {
        this.sgst = sgst;
    }

    public Tax_ withSgst(Integer sgst) {
        this.sgst = sgst;
        return this;
    }

    @JsonProperty("igst")
    public Integer getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(Integer igst) {
        this.igst = igst;
    }

    public Tax_ withIgst(Integer igst) {
        this.igst = igst;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public Tax_ withTotal(Integer total) {
        this.total = total;
        return this;
    }

    public Tax_ setDefaultData() {
        return this.withType(SubscriptionConstant.TAX_TYPE)
                .withCgst(SubscriptionConstant.TAX_CGST)
                .withSgst(SubscriptionConstant.TAX_SGST)
                .withIgst(SubscriptionConstant.TAX_IGST)
                .withTotal(SubscriptionConstant.TAX_TOTAL);
    }

}
