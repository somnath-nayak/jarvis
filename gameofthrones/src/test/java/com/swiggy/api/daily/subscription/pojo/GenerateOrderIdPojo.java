
package com.swiggy.api.daily.subscription.pojo;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "name",
    "retryCount",
    "audit",
    "fetchRetryCount",
    "pageSize",
    "messageRetryCount",
    "startTime",
    "endTime"
})
public class GenerateOrderIdPojo {

    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("retryCount")
    private Integer retryCount;
    @JsonProperty("audit")
    private Boolean audit;
    @JsonProperty("fetchRetryCount")
    private Integer fetchRetryCount;
    @JsonProperty("pageSize")
    private Integer pageSize;
    @JsonProperty("messageRetryCount")
    private Integer messageRetryCount;
    @JsonProperty("startTime")
    private Integer startTime;
    @JsonProperty("endTime")
    private Integer endTime;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public GenerateOrderIdPojo withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public GenerateOrderIdPojo withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("retryCount")
    public Integer getRetryCount() {
        return retryCount;
    }

    @JsonProperty("retryCount")
    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public GenerateOrderIdPojo withRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
        return this;
    }

    @JsonProperty("audit")
    public Boolean getAudit() {
        return audit;
    }

    @JsonProperty("audit")
    public void setAudit(Boolean audit) {
        this.audit = audit;
    }

    public GenerateOrderIdPojo withAudit(Boolean audit) {
        this.audit = audit;
        return this;
    }

    @JsonProperty("fetchRetryCount")
    public Integer getFetchRetryCount() {
        return fetchRetryCount;
    }

    @JsonProperty("fetchRetryCount")
    public void setFetchRetryCount(Integer fetchRetryCount) {
        this.fetchRetryCount = fetchRetryCount;
    }

    public GenerateOrderIdPojo withFetchRetryCount(Integer fetchRetryCount) {
        this.fetchRetryCount = fetchRetryCount;
        return this;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public GenerateOrderIdPojo withPageSize(Integer pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    @JsonProperty("messageRetryCount")
    public Integer getMessageRetryCount() {
        return messageRetryCount;
    }

    @JsonProperty("messageRetryCount")
    public void setMessageRetryCount(Integer messageRetryCount) {
        this.messageRetryCount = messageRetryCount;
    }

    public GenerateOrderIdPojo withMessageRetryCount(Integer messageRetryCount) {
        this.messageRetryCount = messageRetryCount;
        return this;
    }

    @JsonProperty("startTime")
    public Integer getStartTime() {
        return startTime;
    }

    @JsonProperty("startTime")
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public GenerateOrderIdPojo withStartTime(Integer startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("endTime")
    public Integer getEndTime() {
        return endTime;
    }

    @JsonProperty("endTime")
    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public GenerateOrderIdPojo withEndTime(Integer endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public GenerateOrderIdPojo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public GenerateOrderIdPojo setDefaultData(){
        return this.withType(SubscriptionConstant.SUBSCRIPTION_TYPE)
                .withName(SubscriptionConstant.SUBSCRIPTION_NAME)
                .withRetryCount(SubscriptionConstant.SUBSCRIPTION_RETRYCOUNT)
                .withAudit(SubscriptionConstant.SUBSCRIPTION_AUDIT)
                .withFetchRetryCount(SubscriptionConstant.SUBSCRIPTION_FETCHRETRYCOUNT)
                .withPageSize(SubscriptionConstant.SUBSCRIPTION_pageSize)
                .withMessageRetryCount(SubscriptionConstant.SUBSCRIPTION_MESSAGERETRYCOUNT)
                .withStartTime(SubscriptionConstant.SUBSCRIPTION_STARTTIME)
                .withEndTime(SubscriptionConstant.SUBSCRIPTION_ENDTIME);
    }

}
