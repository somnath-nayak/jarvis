package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "username",
        "password",
        "versionNumber",
        "source"
})
public class LoginPOJO {

    @JsonProperty("username")
    private String username;
    @JsonProperty("password")
    private String password;
    @JsonProperty("versionNumber")
    private String versionNumber;
    @JsonProperty("source")
    private String source;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginPOJO() {
    }

    /**
     *
     * @param username
     * @param source
     * @param password
     * @param versionNumber
     */
    public LoginPOJO(String username, String password, String versionNumber, String source) {
        super();
        this.username = username;
        this.password = password;
        this.versionNumber = versionNumber;
        this.source = source;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    public LoginPOJO withUsername(String username) {
        this.username = username;
        return this;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    public LoginPOJO withPassword(String password) {
        this.password = password;
        return this;
    }

    @JsonProperty("versionNumber")
    public String getVersionNumber() {
        return versionNumber;
    }

    @JsonProperty("versionNumber")
    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public LoginPOJO withVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
        return this;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    public LoginPOJO withSource(String source) {
        this.source = source;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public LoginPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public LoginPOJO setDefault(String username, String password, String source, String versionNumber) {
        setUsername(username);
        setPassword(password);
        setSource(source);
        setVersionNumber(versionNumber);
        return this;
    }

//    public LoginPOJO setDefault() {
//        setUsername(VendorConstant.rest_id);
//        setPassword(VendorConstant.password);
//        setSource(VendorConstant.source);
//        setVersionNumber(VendorConstant.random_1);
//        return this;
//    }

}
