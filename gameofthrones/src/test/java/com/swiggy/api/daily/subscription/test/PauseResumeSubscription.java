package com.swiggy.api.daily.subscription.test;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.CreateSubscriptionPojo;
import com.swiggy.api.daily.subscription.pojo.ScheduleDetails;
import com.swiggy.api.daily.subscription.pojo.ServerTimeChangePojo;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;


/*
      Pause Subscription and Resume Subscription testcases
*/

public class PauseResumeSubscription extends SubscriptionDp {
    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
    CreateSubscriptionPojo createSubscriptionPojo=new CreateSubscriptionPojo();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }

    //--------------------------------------------------------------Pause Positive Scenario-------------------------------------------------------------------------

    @Test(dataProvider = "pauseSubscriptionPreCutoffPositiveDP",description ="Pause the subscription for seven days in pre-cutoff time",groups={"Sanity_Test", "Regression"})
    public void pauseSubscriptionPreCutoffPositiveTest(String payload,String pauseStartDate,String pauseEndDate,int tenure)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            if ((DateUtility.getDayOfDateInReadableFormat(pauseEndDate)).equals("Saturday") && subscriptionType.equals("WEEKDAY")) {
                tenure = tenure + 1;
            }
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, tenure, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "pauseSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP",description ="pause the subscription after 3 meal delivered in pre-Cutoff Positive",groups={"Regression"})
    public void pauseSubscriptionAfterFewMealDeliveredPreCutoffPositiveTest(String payload,int tenure,String serverTime,int mealDelivered) throws ParseException, IOException {
        try {
            String newPauseStartDate="";
            String newPauseEndDate="";

            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            if(subscriptionType.equals("WEEKDAY")) {
                newPauseStartDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 6);
            }else{
                newPauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 9);
            }

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+newPauseStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(newPauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, newPauseStartDate, newPauseEndDate, tenure-3, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "pauseSubscriptionPostCutoffPositiveDP",description ="pause the subscription for seven days in Post Cutoff time",groups={"Sanity_Test", "Regression"})
    public void pauseSubscriptionPostCutoffPositiveTest(String payload,String pauseStartDate,String pauseEndDate,int tenure,String serverTime) throws ParseException, IOException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            if((DateUtility.getDayOfDateInReadableFormat(pauseStartDate).equals("Saturday")||DateUtility.getDayOfDateInReadableFormat(pauseStartDate).equals("Sunday"))&&subscriptionType.equals("WEEKDAY")) {
                tenure =tenure+1; }
            if((DateUtility.getDayOfDateInReadableFormat(pauseEndDate)).equals("Saturday")&&subscriptionType.equals("WEEKDAY"))
            {tenure=tenure+1;}
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, tenure, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }

    @Test(dataProvider = "pauseSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP",description ="pause the subscription after 3 meal delivered in Post Cutoff time",groups={"Regression"})
    public void pauseSubscriptionAfterFewMealDeliveredPostCutoffPositiveTest(String payload,int tenure,int mealDelivered,String serverTime) throws ParseException, IOException {
        try {
            String newPauseStartDate="";
            String newPauseEndDate="";

            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            if(subscriptionType.equals("WEEKDAY")) {
                newPauseStartDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 6);
            }else{
                newPauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 9);
            }
            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+newPauseStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(newPauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, newPauseStartDate, newPauseEndDate, tenure-4, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }

    @Test(dataProvider="pauseResumePauseSubscriptionDP",description ="pause the subscription, resume the subscription, pause the subscription again, where weekend service true, false",groups={"Regression"})
    public void pauseResumePauseSubscriptionTest(String payload,String pauseStartDate,String pauseEndDate) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String resumeStatus = resumeSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(resumeStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(resumeStatus, "ACTIVE", "Subscription is not resumed");

            Processor pauseSubscriptionSecondTimeProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int secondTimePauseStatusCode = pauseSubscriptionSecondTimeProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String secondTimePauseStatus = pauseSubscriptionSecondTimeProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(secondTimePauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(secondTimePauseStatus, "PAUSED", "Subscription is not paused");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validation of pause subscription Not Null Field for weekend service true, false",groups={"Regression"})
    public void pauseResponseFieldValidationTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String pauseSubscriptionResponse = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate)).ResponseValidator.GetBodyAsText();
            subscriptionTestHelper.validateNodePauseSubscriptionNotNull(pauseSubscriptionResponse);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "pauseSubscriptionLateNightPostCutoffDP",description ="Pause the subscription between 10:01 and 11:59 for same day",groups={"Regression"})
    public void pauseSubscriptionInLateNightTest(String payload,String pauseStartDate,String pauseEndDate,int tenure,String time)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + time));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, tenure, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "createSubscriptionDP",description ="Pause the subscription after delivered first meal on same day",groups={"Regression"})
    public void pauseSubscriptionOnSameDayAfterFirstMealDeliveredTest(String payload)throws IOException,ParseException {
        try {
            int tenure=7;
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String pauseEndDate=DateUtility.getFutureDateInReadableFormat(6);
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(3,15)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, tenure-1, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the subscription, pause the same subscription on same day",groups={"Regression"})
    public void skipPauseSameDaySubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1, "Status code is not 1");
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, 7, "success");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the subscription, pause the same subscription from tomorrow On same day",groups={"Regression"})
    public void skipTodayPauseSubscriptionFromTomorrowTest(String payload) throws IOException, ParseException {
        try {
            String pauseStartDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            if(subscriptionType.equals("WEEKDAY")){
                pauseStartDate=DateUtility.getNextWorkingDateInReadableFormat(subscriptionStartDate);
            }else{
                pauseStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
            }

            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1, "Status code is not 1");

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(pauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, 7, "success");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(description ="Pause the subscription for Max days where weekend service=false",groups={"Regression"})
    public void pauseSubscriptionMaxDaysWeekendFalseTest() throws IOException, ParseException {
        try {
            int tenure=7;
            String pauseEndDate="";
            String payload=jsonHelper.getObjectToJSON(createSubscriptionPojo.setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)));
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            if(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")) {
                pauseEndDate = DateUtility.getFutureDateInReadableFormat(10);
            }else{
                pauseEndDate = DateUtility.getFutureDateInReadableFormat(8);
            }
            if ((DateUtility.getDayOfDateInReadableFormat(pauseEndDate)).equals("Saturday") && subscriptionType.equals("WEEKDAY")) {
                tenure = tenure + 1;
            }
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, subscriptionStartDate, pauseEndDate, tenure, "success");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "pauseSubscriptionMaxDaysPauseEndDateDayFridayWeekendFalseDP",description ="Pause the subscription for Max days where weekend service=false,Pause EndDateDay=Friday",groups={"Regression"})
    public void pauseSubscriptionMaxDaysPauseEndDateDayFridayWeekendFalseTest(String payload, String pauseEndDate,int tenure) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        if ((DateUtility.getDayOfDateInReadableFormat(pauseEndDate)).equals("Saturday") && subscriptionType.equals("WEEKDAY")) {
            tenure = tenure + 1;
        }
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
        String subscriptionEndDate = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.endDate");
        String actualResumeDate = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.resumeDate");

        Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
        Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

        Assert.assertEquals(subscriptionEndDate,subscriptionTestHelper.expectedEndDate(pauseEndDate,tenure) ,"Subscription end date is not correct");

        if(DateUtility.getDayOfDateInReadableFormat(pauseEndDate).equals("Friday")) {
            Assert.assertEquals(actualResumeDate,DateUtility.getDateInReadableFormat(pauseEndDate, 3), "Subscription resume date is not correct");
        }
        if(DateUtility.getDayOfDateInReadableFormat(pauseEndDate).equals("Saturday"))
        {
            Assert.assertEquals(actualResumeDate, DateUtility.getDateInReadableFormat(pauseEndDate, 2), "Subscription resume date is not correct");
        }
    }

    @Test(dataProvider = "pauseSubscriptionPostCutoffWithNextDayDatePositiveDP",description ="pause the subscription from next day Date for seven days in Post Cutoff time",groups={"Sanity_Test", "Regression"})
    public void pauseSubscriptionPostCutoffWithNextDayDatePositiveTest(String payload,int tenure,String serverTime) throws ParseException, IOException {
        try {

            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            String pauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);

            if ((DateUtility.getDayOfDateInReadableFormat(pauseStartDate)).equals("Saturday") && subscriptionType.equals("WEEKDAY")) {
                pauseStartDate = DateUtility.getDateInReadableFormat(pauseStartDate, 2);
            }

            if ((DateUtility.getDayOfDateInReadableFormat(pauseStartDate)).equals("Sunday") && subscriptionType.equals("WEEKDAY")) {
                pauseStartDate = DateUtility.getDateInReadableFormat(pauseStartDate, 1);
            }
            String pauseEndDate = DateUtility.getDateInReadableFormat(pauseStartDate, 6);

            if ((DateUtility.getDayOfDateInReadableFormat(pauseEndDate)).equals("Saturday") && subscriptionType.equals("WEEKDAY")) {
                tenure = tenure + 1;
            }
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(pauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Positive", subscriptionType, pauseStartDate, pauseEndDate, tenure-1, SubscriptionConstant.SUBSCRIPTION_PAUSESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="pause, resume, skip the subscription, where weekend service true, false",groups={"Regression"})
    public void pauseResumeSkipSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

        String pauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
        Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
        Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
        int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String resumeStatus = resumeSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
        Assert.assertEquals(resumeStatusCode, 1, "Status code is not 1");
        Assert.assertEquals(resumeStatus, "ACTIVE", "Subscription is not resumed");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(skipStatusCode, 1);
        subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
    }


    //--------------------------------------------------------------Pause Negative Scenario-------------------------------------------------------------------------

    @Test(dataProvider = "pauseSubscriptionPreCutoffNegativeDP",description ="Pause the subscription where pause date duration more than 7 days",groups={"Sanity_Test", "Regression"})
    public void pauseSubscriptionPreCutoffNegativeTest(String payload,String pauseStartDate,String pauseEndDate,int tenure,String statusMessage)throws IOException,ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(pauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, pauseStartDate, pauseEndDate, tenure, statusMessage);
    }

    @Test(dataProvider="createSubscriptionDP",description ="Pause the subscription which is already paused",groups={"Regression"})
    public void pauseAlreadyPausedSubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatusMessage = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(pauseStatusCode, 410, "Able to pause subscription");
            Assert.assertEquals(pauseStatusMessage, "Subscription is not currently Active", "Able to pause subscription");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }


    @Test(dataProvider="createSubscriptionDP",description ="Pause the subscription, Skip the subscription on same day",groups={"Regression"})
    public void pauseSkipSameDaySubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipSubscriptionProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");

            String skipStatusMessage = skipSubscriptionProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(skipStatusCode, 410, "Able to Skip subscription");
            Assert.assertEquals(skipStatusMessage, SubscriptionConstant.SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG, "Status message is not correct");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }


    @Test(dataProvider = "pauseSubscriptionWithDbOperationPreCutoffNegativeDP",description ="pause the subscription where past pause start date that day meal is already delivered in pre cutoff time",groups={"Regression"})
    public void pauseSubscriptionWithDbOperationPreCutoffNegativeTest(String payload,int count,String pauseEndDate,int tenure,String statusMessage,int days)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            String newPauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, count);
            subscriptionHelper.updateStartDateSubscription(DateUtility.getDateInReadableFormat(subscriptionStartDate,days), responseSubscriptionId);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, newPauseStartDate, pauseEndDate, tenure, statusMessage);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "pauseSubscriptionWithDbOperationPostCutoffNegativeDP",description ="pause the subscription where past pause start date that day meal is already delivered in post cutoff time",groups={"Regression"})
    public void pauseSubscriptionPostCutoffNegativeTest(String payload,int count,String pauseEndDate,int tenure,String statusMessage,String serverTime, int days) throws ParseException, IOException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String newPauseStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,count);

            subscriptionHelper.updateStartDateSubscription(DateUtility.getFutureDateInReadableFormat(days), responseSubscriptionId);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, newPauseStartDate, pauseEndDate, tenure, statusMessage);

        } finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="pause the subscription Where expiry date is less than subscription pause end date",groups={"Regression"})
    public void pauseSubscriptionExpiryDateLessThanPauseEndDateNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            subscriptionHelper.updateExpiryDateSubscription(DateUtility.getDateInReadableFormat(subscriptionStartDate, 5), responseSubscriptionId);
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, subscriptionStartDate, pauseEndDate, 7, SubscriptionConstant.SUBSCRIPTION_EXPIRYDATEEXCEEDUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="pause the subscription Where 3 meals are delivered, expiry date is less than subscription pause end date",groups={"Regression"})
    public void pauseSubscriptionAfterFewMealsDeliveredTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getDateInReadableFormat(subscriptionStartDate,4)));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormatWithColon()));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            subscriptionHelper.updateExpiryDateSubscription(DateUtility.getDateInReadableFormat(subscriptionStartDate,7), responseSubscriptionId);
            String newPauseStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,4);
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(9);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, newPauseStartDate, pauseEndDate, 7, SubscriptionConstant.SUBSCRIPTION_EXPIRYDATEEXCEEDUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="pause the subscription Which is already completed",groups={"Regression"})
    public void pauseAlreadyCompletedSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "COMPLETED");
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, subscriptionStartDate, pauseEndDate, 7, SubscriptionConstant.SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="pause the subscription Which is already Cancelled",groups={"Regression"})
    public void pauseAlreadyCancelledSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "CANCELLED");
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, subscriptionStartDate, pauseEndDate, 7, SubscriptionConstant.SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="pause the subscription Which pause start date and pause end date both are same",groups={"Regression"})
    public void pauseStartDatePauseEndDateBothSameNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate));
            subscriptionTestHelper.validatePauseResponse(pauseSubscriptionProcessor, "Negative", subscriptionType, subscriptionStartDate, subscriptionStartDate, 7, SubscriptionConstant.SUBSCRIPTION_INVALIDENDDATEUNSUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    //-----------------------------------------------------Resume Subscription Positive Scenario-----------------------------------------------------------------

    @Test(dataProvider = "resumeSubscriptionPreCutoffPositiveDP",description ="Resume the subscription on first day in pre-cutoff time",groups={"Sanity_Test", "Regression"})
    public void resumeSubscriptionPreCutoffPositiveTest(String payload,String pauseEndDate,int resumeAfterDays)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");
            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Positive", subscriptionType, subscriptionEndDate, resumeAfterDays, SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "resumeSubscriptionAfterFewDaysPreCutoffPositiveDP",description ="resume the subscription after 3 days of pause in pre-Cutoff Positive",groups={"Regression"})
    public void resumeSubscriptionAfterFewDaysPreCutoffPositiveTest(String payload,String pauseEndDate,String serverTime,int resumeAfterDays) throws ParseException, IOException {
        try {
            String resumeStartDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            if(subscriptionType.equals("WEEKDAY")) {
                resumeStartDate = subscriptionTestHelper.expectedEndDate(subscriptionStartDate, resumeAfterDays);
            }else{
                resumeStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,resumeAfterDays);
            }

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");
            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+resumeStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor resumeSubscriptionProcessor=subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor,"Positive",subscriptionType,subscriptionEndDate,resumeAfterDays,SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }

    @Test(dataProvider = "resumeSubscriptionPostCutoffPositiveDP",description ="Resume the subscription on first day in post-cutoff time")
    public void resumeSubscriptionPostCutoffPositiveTest(String payload,String pauseEndDate,int resumeAfterDays,String serverTime) throws IOException, ParseException, InterruptedException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor resumeSubscriptionProcessor=subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor,"Positive",subscriptionType,subscriptionEndDate,resumeAfterDays,SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "resumeSubscriptionAfterFewDaysPostCutoffPositiveDP",description ="resume the subscription after 3 days of pause in Post-Cutoff Positive",groups={"Regression"})
    public void resumeSubscriptionAfterFewDaysPostCutoffPositiveTest(String payload,int resumeAfterDays,String serverTime) throws ParseException, IOException {
        try {
            String resumeStartDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String newPauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);

            if(subscriptionType.equals("WEEKDAY")) {
                resumeStartDate = subscriptionTestHelper.expectedEndDate(subscriptionStartDate, resumeAfterDays);
            }else{
                resumeStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,resumeAfterDays);
            }

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(newPauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +resumeStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor resumeSubscriptionProcessor=subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor,"Positive",subscriptionType,subscriptionEndDate,resumeAfterDays+1,SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }

    }

    @Test(dataProvider = "resumeSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP",description ="Resume the subscription after 3 meal delivered, after 3 days from pause start date in pre-Cutoff Positive",groups={"Regression"})
    public void resumeSubscriptionAfterFewMealDeliveredPreCutoffPositiveTest(String payload,int tenure,String serverTime,int mealDelivered,int resumeAfterDays) throws ParseException, IOException {
        try {
            String newPauseStartDate="";
            String newPauseEndDate="";
            String resumeStartDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            if(subscriptionType.equals("WEEKDAY")) {
                newPauseStartDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 6);
            }else{
                newPauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 9);
            }

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+newPauseStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(newPauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

            if(subscriptionType.equals("WEEKDAY")) {
                resumeStartDate = DateUtility.getWorkingDateFromCurrentDate(newPauseStartDate, resumeAfterDays);
            }else{
                resumeStartDate=DateUtility.getDateInReadableFormat(newPauseStartDate,resumeAfterDays);
            }
            String changeServerResumeDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +resumeStartDate));
            subscriptionHelper.changeServerTime(changeServerResumeDatePayload);
            String changeServerResumeTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerResumeTimePayload);
            Processor resumeSubscriptionProcessor=subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor,"Positive",subscriptionType,subscriptionEndDate,resumeAfterDays,SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "resumeSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP",description ="Resume the subscription after 3 meal delivered, after 3 days from pause start date in Post Cutoff time",groups={"Regression"})
    public void resumeSubscriptionAfterFewMealDeliveredPostCutoffPositiveTest(String payload,int tenure,int mealDelivered,String serverTime,int resumeAfterDays) throws ParseException, IOException {
        try {
            String newPauseStartDate="";
            String newPauseEndDate="";
            String resumeStartDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            if(subscriptionType.equals("WEEKDAY")) {
                newPauseStartDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate, 6);
            }else{
                newPauseStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
                newPauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 9);
            }

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+newPauseStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+DateUtility.getCurrentTimeInReadableFormatWithColon()));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(newPauseStartDate), DateUtility.convertReadableDateTimeToInMilisecond(newPauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            String pauseStatus = pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.status");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");
            Assert.assertEquals(pauseStatus, "PAUSED", "Subscription not paused");

            if(subscriptionType.equals("WEEKDAY")) {
                resumeStartDate = subscriptionTestHelper.expectedEndDate(newPauseStartDate, resumeAfterDays);
            }else{
                resumeStartDate=DateUtility.getDateInReadableFormat(newPauseStartDate,resumeAfterDays);
            }
            String changeServerResumeDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +resumeStartDate));
            subscriptionHelper.changeServerTime(changeServerResumeDatePayload);
            String changeServerResumeTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+serverTime));
            subscriptionHelper.changeServerTime(changeServerResumeTimePayload);
            Processor resumeSubscriptionProcessor=subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor,"Positive",subscriptionType,subscriptionEndDate,resumeAfterDays+1,SubscriptionConstant.SUBSCRIPTION_RESUMESUCCESS_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="pause the subscription, resume the subscription, skip the subscription on same day",groups={"Regression"})
    public void pauseResumeSkipOnSameDaySubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);
            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");

            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(resumeStatusCode, 1, "Status code is not 0");

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipSubscriptionProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1, "Status code is not 1");

            String skipStatusMessage = skipSubscriptionProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(skipStatusMessage, "success", "Status message is not correct");
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validation of resume subscription Not Null Field")
    public void resumeResponseFieldValidationTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            String pauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
            subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

            String resumeSubscriptionBody = subscriptionHelper.resumeSubscription(responseSubscriptionId).ResponseValidator.GetBodyAsText();
            subscriptionTestHelper.validateNodeSkipResumeSubscriptionNotNull(resumeSubscriptionBody);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    //-----------------------------------------------------Resume Subscription Negative Scenario-----------------------------------------------------------------

    @Test(dataProvider="createSubscriptionDP",description="resume the subscription which is not paused",groups={"Regression"})
    public void resumeSubscriptionWhichNotPausedNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Negative",subscriptionType,subscriptionEndDate,1,SubscriptionConstant.SUBSCRIPTION_UNSUCCESSRESUME_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="Resume the subscription Which is already completed",groups={"Regression"})
    public void resumeAlreadyCompletedSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");

            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "COMPLETED");
            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Negative",subscriptionType,subscriptionEndDate,1,SubscriptionConstant.SUBSCRIPTION_UNSUCCESSRESUME_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description="Resume the subscription Which is already Cancelled",groups={"Regression"})
    public void resumeAlreadyCancelledSubscriptionNegativeTest(String payload)throws IOException,ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            String pauseEndDate = DateUtility.getFutureDateInReadableFormat(6);

            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1, "Status code is not 1");

            subscriptionHelper.updateSubscriptionStatus(responseSubscriptionId, "CANCELLED");
            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Negative",subscriptionType,subscriptionEndDate,1,SubscriptionConstant.SUBSCRIPTION_UNSUCCESSRESUME_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Skip the subscription, resume the same subscription on same day",groups={"Regression"})
    public void skipResumeSameDaySubscriptionTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            subscriptionTestHelper.changeServerTime(subscriptionStartDate);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipSubscriptionProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1, "Status code is not 1");
            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
            subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Negative",subscriptionType,subscriptionEndDate,1,SubscriptionConstant.SUBSCRIPTION_UNSUCCESSRESUME_MSG);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }
    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }
}
