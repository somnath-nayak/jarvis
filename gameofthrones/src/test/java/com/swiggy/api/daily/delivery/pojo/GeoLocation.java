package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class GeoLocation {

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("lat")
    private String lat;

    @JsonProperty("lng")
    private String lng;
    @JsonIgnore
    public double getLatAsDouble(){
        return Double.parseDouble(lat);
    }
    @JsonIgnore
    public double getLngAsDouble(){
        return Double.parseDouble(lng);
    }

    @Override
    public int hashCode() {
        int result = lat != null ? lat.hashCode() : 0;
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        return result;
    }

    @Override
    public String toString(){
        return lat+","+lng;
    }

}