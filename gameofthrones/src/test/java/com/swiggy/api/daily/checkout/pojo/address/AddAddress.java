package com.swiggy.api.daily.checkout.pojo.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "name",
        "mobile",
        "address",
        "landmark",
        "area",
        "lat",
        "lng",
        "flat_no",
        "city",
        "annotation"
})
public class AddAddress {

    @JsonProperty("name")
    private String name;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("flat_no")
    private String flat_no;
    @JsonProperty("city")
    private String city;
    @JsonProperty("annotation")
    private String annotation;

    /**
     * No args constructor for use in serialization
     *
     */
    public AddAddress() {
    }

    /**
     *
     * @param annotation
     * @param landmark
     * @param area
     * @param address
     * @param name
     * @param lng
     * @param flat_no
     * @param lat
     * @param city
     * @param mobile
     */
    public AddAddress(String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city, String annotation) {
        super();
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.landmark = landmark;
        this.area = area;
        this.lat = lat;
        this.lng = lng;
        this.flat_no = flat_no;
        this.city = city;
        this.annotation = annotation;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("flat_no")
    public String getFlat_no() {
        return flat_no;
    }

    @JsonProperty("flat_no")
    public void setFlat_no(String flat_no) {
        this.flat_no = flat_no;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("mobile", mobile).append("address", address).append("landmark", landmark).append("area", area).append("lat", lat).append("lng", lng).append("flat_no", flat_no).append("city", city).append("annotation", annotation).toString();
    }

}