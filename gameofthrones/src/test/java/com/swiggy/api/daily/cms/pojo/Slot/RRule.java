
package com.swiggy.api.daily.cms.pojo.Slot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "interval",
    "by_day_list",
    "bySetPos",
    "byMonthDay"
})
public class RRule {

    @JsonProperty("interval")
    private Integer interval;
    @JsonProperty("by_day_list")
    private List<String> by_day_list = null;
    @JsonProperty("bySetPos")
    private Integer bySetPos;
    @JsonProperty("byMonthDay")
    private Integer byMonthDay;


    /**
     * No args constructor for use in serialization
     * 
     */
    public RRule() {
    }

    /**
     *
     * @param bySetPos
     * @param interval
     * @param byMonthDay
     * @param by_day_list
     */
    public RRule(Integer interval, List<String> by_day_list, Integer bySetPos, Integer byMonthDay) {
        this.interval = interval;
        this.by_day_list = by_day_list;
        this.bySetPos = bySetPos;
        this.byMonthDay = byMonthDay;
    }

    @JsonProperty("interval")
    public Integer getInterval() {
        return interval;
    }

    @JsonProperty("interval")
    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public RRule withInterval(Integer interval) {
        this.interval = interval;
        return this;
    }

    @JsonProperty("by_day_list")
    public List<String> getBy_day_list() {
        return by_day_list;
    }

    @JsonProperty("by_day_list")
    public void setBy_day_list(List<String> by_day_list) {
        this.by_day_list = by_day_list;
    }

    public RRule withBy_day_list(List<String> by_day_list) {
        this.by_day_list = by_day_list;
        return this;
    }

    @JsonProperty("bySetPos")
    public Integer getBySetPos() {
        return bySetPos;
    }

    @JsonProperty("bySetPos")
    public void setBySetPos(Integer bySetPos) {
        this.bySetPos = bySetPos;
    }

    public RRule withBySetPos(Integer bySetPos) {
        this.bySetPos = bySetPos;
        return this;
    }

    @JsonProperty("byMonthDay")
    public Integer getByMonthDay() {
        return byMonthDay;
    }

    @JsonProperty("byMonthDay")
    public void setByMonthDay(Integer byMonthDay) {
        this.byMonthDay = byMonthDay;
    }

    public RRule withByMonthDay(Integer byMonthDay) {
        this.byMonthDay = byMonthDay;
        return this;
    }





}
