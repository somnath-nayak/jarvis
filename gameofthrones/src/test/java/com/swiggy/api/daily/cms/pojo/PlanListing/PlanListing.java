
package com.swiggy.api.daily.cms.pojo.PlanListing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "store_id",
    "product_ids"
})
public class PlanListing {

    @JsonProperty("store_id")
    private Integer storeId;
    @JsonProperty("product_ids")
    private List<String> productIds = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public PlanListing() {
    }

    /**
     * 
     * @param productIds
     * @param storeId
     */
    public PlanListing(Integer storeId, List<String> productIds) {
        super();
        this.storeId = storeId;
        this.productIds = productIds;
    }

    @JsonProperty("store_id")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    @JsonProperty("product_ids")
    public List<String> getProductIds() {
        return productIds;
    }

    @JsonProperty("product_ids")
    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }


}
