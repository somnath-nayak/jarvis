package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "entity_id",
        "meta",
        "store_id",
        "entity_type",
        "slot_info_list"
})
public class SlotServicePOJO {

    @JsonProperty("entity_id")
    private String entityId;
    @JsonProperty("meta")
    private MetaPOJO meta;
    @JsonProperty("store_id")
    private Integer storeId;
    @JsonProperty("entity_type")
    private String entityType;
    @JsonProperty("slot_info_list")
    private List<SlotInfoListPOJO> slotInfoList = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public SlotServicePOJO() {
    }

    /**
     *
     * @param slotInfoList
     * @param entityId
     * @param entityType
     * @param storeId
     * @param meta
     */
    public SlotServicePOJO(String entityId, MetaPOJO meta, Integer storeId, String entityType, List<SlotInfoListPOJO> slotInfoList) {
        super();
        this.entityId = entityId;
        this.meta = meta;
        this.storeId = storeId;
        this.entityType = entityType;
        this.slotInfoList = slotInfoList;
    }

    @JsonProperty("entity_id")
    public String getEntityId() {
        return entityId;
    }

    @JsonProperty("entity_id")
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public SlotServicePOJO withEntityId(String entityId) {
        this.entityId = entityId;
        return this;
    }

    @JsonProperty("meta")
    public MetaPOJO getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(MetaPOJO meta) {
        this.meta = meta;
    }

    public SlotServicePOJO withMeta(MetaPOJO meta) {
        this.meta = meta;
        return this;
    }

    @JsonProperty("store_id")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public SlotServicePOJO withStoreId(Integer storeId) {
        this.storeId = storeId;
        return this;
    }

    @JsonProperty("entity_type")
    public String getEntityType() {
        return entityType;
    }

    @JsonProperty("entity_type")
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public SlotServicePOJO withEntityType(String entityType) {
        this.entityType = entityType;
        return this;
    }

    @JsonProperty("slot_info_list")
    public List<SlotInfoListPOJO> getSlotInfoList() {
        return slotInfoList;
    }

    @JsonProperty("slot_info_list")
    public void setSlotInfoList(List<SlotInfoListPOJO> slotInfoList) {
        this.slotInfoList = slotInfoList;
    }

    public SlotServicePOJO withSlotInfoList(List<SlotInfoListPOJO> slotInfoList) {
        this.slotInfoList = slotInfoList;
        return this;
    }

    public SlotServicePOJO setDefault(Integer storeID, Boolean isInclusive, String liveDate, String expiryDate, String entityID, String startTime, String endTime, String entityType) {
    MetaPOJO metaPOJO = new MetaPOJO().setDefault(storeID);
    SlotInfoListPOJO slotInfoListPOJO = new SlotInfoListPOJO().setDefault(isInclusive,liveDate, expiryDate, startTime, endTime);
    List list = new ArrayList();
    list.add(slotInfoListPOJO);
        this.withEntityId(entityID)
                .withMeta(metaPOJO)
                .withStoreId(storeID)
                .withEntityType(entityType)
                .withSlotInfoList(list);
        return this;
    }

}
