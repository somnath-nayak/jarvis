package com.swiggy.api.daily.checkout.pojo.subscription;

import io.advantageous.boon.json.annotations.JsonProperty;

public class CartBill {

    @JsonProperty("totalWithoutDiscount")
    private Long totalWithoutDiscount;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("discount")
    private Long discount;
    @JsonProperty("itemTotalCharges")
    private ItemTotalCharges itemTotalCharges;
    @JsonProperty("cartPackingCharges")
    private CartPackingCharges cartPackingCharges;
    @JsonProperty("deliveryPriceDetails")
    private DeliveryPriceDetails deliveryPriceDetails;
    @JsonProperty("cartDiscountDetails")
    private CartDiscountDetails cartDiscountDetails;
    @JsonProperty("couponDiscountDetails")
    private CouponDiscountDetails couponDiscountDetails;

    @JsonProperty("totalWithoutDiscount")
    public Long getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public CartBill withTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Long total) {
        this.total = total;
    }

    public CartBill withTotal(Long total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Long getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public CartBill withDiscount(Long discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("itemTotalCharges")
    public ItemTotalCharges getItemTotalCharges() {
        return itemTotalCharges;
    }

    @JsonProperty("itemTotalCharges")
    public void setItemTotalCharges(ItemTotalCharges itemTotalCharges) {
        this.itemTotalCharges = itemTotalCharges;
    }

    public CartBill withItemTotalCharges(ItemTotalCharges itemTotalCharges) {
        this.itemTotalCharges = itemTotalCharges;
        return this;
    }

    @JsonProperty("cartPackingCharges")
    public CartPackingCharges getCartPackingCharges() {
        return cartPackingCharges;
    }

    @JsonProperty("cartPackingCharges")
    public void setCartPackingCharges(CartPackingCharges cartPackingCharges) {
        this.cartPackingCharges = cartPackingCharges;
    }

    public CartBill withCartPackingCharges(CartPackingCharges cartPackingCharges) {
        this.cartPackingCharges = cartPackingCharges;
        return this;
    }

    @JsonProperty("deliveryPriceDetails")
    public DeliveryPriceDetails getDeliveryPriceDetails() {
        return deliveryPriceDetails;
    }

    @JsonProperty("deliveryPriceDetails")
    public void setDeliveryPriceDetails(DeliveryPriceDetails deliveryPriceDetails) {
        this.deliveryPriceDetails = deliveryPriceDetails;
    }

    public CartBill withDeliveryPriceDetails(DeliveryPriceDetails deliveryPriceDetails) {
        this.deliveryPriceDetails = deliveryPriceDetails;
        return this;
    }

    @JsonProperty("cartDiscountDetails")
    public CartDiscountDetails getCartDiscountDetails() {
        return cartDiscountDetails;
    }

    @JsonProperty("cartDiscountDetails")
    public void setCartDiscountDetails(CartDiscountDetails cartDiscountDetails) {
        this.cartDiscountDetails = cartDiscountDetails;
    }

    public CartBill withCartDiscountDetails(CartDiscountDetails cartDiscountDetails) {
        this.cartDiscountDetails = cartDiscountDetails;
        return this;
    }

    @JsonProperty("couponDiscountDetails")
    public CouponDiscountDetails getCouponDiscountDetails() {
        return couponDiscountDetails;
    }

    @JsonProperty("couponDiscountDetails")
    public void setCouponDiscountDetails(CouponDiscountDetails couponDiscountDetails) {
        this.couponDiscountDetails = couponDiscountDetails;
    }

    public CartBill withCouponDiscountDetails(CouponDiscountDetails couponDiscountDetails) {
        this.couponDiscountDetails = couponDiscountDetails;
        return this;
    }

}
