package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class GetPlanAvailabilityByStoreTest extends CMSDailyDP {

    CMSDailyHelper helper=new CMSDailyHelper();

    @Test(dataProvider = "getplanbystoredp",description = "This test will get all available Plan by Specific store and SKU")
    public void getAvailableMealByStore(String store_id, String sku_id,String starttime, String endtime, String service){
        String res= helper.getPlanByStoreSKU(store_id,sku_id,starttime,endtime,service).ResponseValidator.GetBodyAsText();
        SoftAssert sc=new SoftAssert();
        String storeid= JsonPath.read(res,"$.data..store_id").toString();
        int status_code = JsonPath.read(res, "$.status_code");
        String status_message = JsonPath.read(res, "$.status_message");
        sc.assertEquals(status_code,1,"Status code not matching");
        sc.assertEquals(status_message,"success","Status message not matching");
        sc.assertAll();

    }
}
