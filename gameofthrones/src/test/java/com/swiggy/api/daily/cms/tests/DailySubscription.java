package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import framework.gameofthrones.JonSnow.Validator;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Arrays;
import java.util.List;

public class DailySubscription {
    CMSDailyHelper helper=new CMSDailyHelper();
    List<String> listspoc;
    List<String> listepoc;
    List<String> listspin;
    List<String> listspocplan;
    List<String> listepocplan;
    List<String> listspinplan;
    int sold1;
    int sold2;
    int count=9;


    @Test(description = "This test will fetch inventory slot for Product")
    public void searchMealInventoryByStore() throws Exception {
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String startepoc = JsonPath.read(responsesearch, "$.data..attributes.start_epoch").toString().replace("[", "");
        listspoc = Arrays.asList(startepoc.split("\\s*,\\s*"));
        String endepoc = JsonPath.read(responsesearch, "$.data..attributes.end_epoch").toString().replace("[", "");
        listepoc = Arrays.asList(endepoc.split("\\s*,\\s*"));
        String spinid = JsonPath.read(responsesearch, "$.data..meta.spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        listspin = Arrays.asList(spinid.split("\\s*,\\s*"));
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> sold = Arrays.asList(soldcount.split("\\s*,\\s*"));
        sold1=Integer.parseInt(sold.get(0));
        String storeactual=JsonPath.read(responsesearch,"$.data[0]..meta.store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(storeactual,CMSDailyContants.store_id_inventory);
    }

    @Test(description = "This test will fetch inventory slot for Plan")
    public void searchPlanInventoryByStore() throws Exception {
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String startepoc = JsonPath.read(responsesearch, "$.data..attributes.start_epoch").toString().replace("[", "");
        listspocplan = Arrays.asList(startepoc.split("\\s*,\\s*"));
        String endepoc = JsonPath.read(responsesearch, "$.data..attributes.end_epoch").toString().replace("[", "");
        listepocplan = Arrays.asList(endepoc.split("\\s*,\\s*"));
        String productid = JsonPath.read(responsesearch, "$.data..meta.product_id").toString().replace("[", "");
        List<String> listpidplan = Arrays.asList(productid.split("\\s*,\\s*"));
        String responepservice = helper.fetchSpinFromProdctId(listpidplan.get(0).replace("\"","")).ResponseValidator.GetBodyAsText();
        String spinid = JsonPath.read(responepservice , "$..spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        listspinplan = Arrays.asList(spinid.split("\\s*,\\s*"));
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> sold = Arrays.asList(soldcount.split("\\s*,\\s*"));
        sold2=Integer.parseInt(sold.get(0));
        String storeactual=JsonPath.read(responsesearch,"$.data[0]..meta.store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(storeactual,CMSDailyContants.store_id_inventory);
    }

    @Test(description = "This test will adjust meal increase sold count for valid data",priority = 1)
    public void adjustMealDecrement() throws Exception {
        Validator vi=helper.adjustMealSubscrption(count,CMSDailyContants.store_id_inventory, CMSDailyContants.dmoperation.get(1).toString(),listspin.get(0),Long.parseLong(listspoc.get(0)),Long.parseLong(listepoc.get(0))).ResponseValidator;
        int statuscode=vi.GetResponseCode();
        Assert.assertEquals(statuscode,200);
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> listsoldcount = Arrays.asList(soldcount.split("\\s*,\\s*"));
        int changedcount=Integer.parseInt(listsoldcount.get(0));
        boolean check=(changedcount==sold1+count)? true :false;
        Assert.assertTrue(check);
    }

    @Test(description = "This test will adjust meal decrease sold count for valid data",priority = 2)
    public void adjustMealIncrement() throws Exception {
        Validator vi=helper.adjustMealSubscrption(count,CMSDailyContants.store_id_inventory, CMSDailyContants.dmoperation.get(0).toString(),listspin.get(0),Long.parseLong(listspoc.get(0)),Long.parseLong(listepoc.get(0))).ResponseValidator;
        int statuscode=vi.GetResponseCode();
        Assert.assertEquals(statuscode,200);
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> listsoldcount = Arrays.asList(soldcount.split("\\s*,\\s*"));
        int changedcount=Integer.parseInt(listsoldcount.get(0));
        boolean check=(changedcount==sold1)? true :false;
        Assert.assertTrue(check);
    }


    @Test(description = "This test will adjust plan increase sold count for valid data",priority = 3)
    public void adjustPlanDecrement() throws Exception {
        Validator vi=helper.adjustPlanSubscrption(count,CMSDailyContants.store_id_inventory, CMSDailyContants.dmoperation.get(1).toString(),listspinplan.get(0),Long.parseLong(listspocplan.get(0)),Long.parseLong(listepocplan.get(0))).ResponseValidator;
        int statuscode=vi.GetResponseCode();
        Assert.assertEquals(statuscode,200);
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> listsoldcount = Arrays.asList(soldcount.split("\\s*,\\s*"));
        int changedcount=Integer.parseInt(listsoldcount.get(0));
        boolean check=(changedcount==sold2+count)? true :false;
        Assert.assertTrue(check);
    }

    @Test(description = "This test will adjust meal decrease sold count for valid data",priority = 4)
    public void adjustPlanIncrement() throws Exception {
        Validator vi=helper.adjustPlanSubscrption(count,CMSDailyContants.store_id_inventory, CMSDailyContants.dmoperation.get(0).toString(),listspinplan.get(0),Long.parseLong(listspocplan.get(0)),Long.parseLong(listepocplan.get(0))).ResponseValidator;
        int statuscode=vi.GetResponseCode();
        Assert.assertEquals(statuscode,200);
        String responsesearch = helper.searchInventory(null, CMSDailyContants.store_id_inventory, null, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String soldcount = JsonPath.read(responsesearch, "$.data..inventory_construct.inventories.sold_count").toString().replace("[", "").replace("]","");
        List<String> listsoldcount = Arrays.asList(soldcount.split("\\s*,\\s*"));
        int changedcount=Integer.parseInt(listsoldcount.get(0));
        boolean check=(changedcount==sold2)? true :false;
        Assert.assertTrue(check);
    }

    @Test(description = "This test will adjust plan decrease sold count for with invalid data",priority = 5,dataProviderClass = CMSDailyDP.class,dataProvider = "adjustmealplannegative")
    public void adjustPlanIncrementNegative(Integer cou, String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        int res=helper.adjustPlanSubscrption(cou,storeid, oprt,spin,startepoc,endepoc).ResponseValidator.GetResponseCode();
        Assert.assertEquals(res,500);

    }

    @Test(description = "This test will adjust plan increment sold count for with invalid data",priority = 5,dataProviderClass = CMSDailyDP.class,dataProvider = "adjustmealplannegative")
    public void adjustPlanDecrementtNegative(Integer cou, String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        int res=helper.adjustPlanSubscrption(cou,storeid, oprt,spin,startepoc,endepoc).ResponseValidator.GetResponseCode();
        Assert.assertEquals(res,500);

    }


    @Test(description = "This test will adjust meal decrement sold count for with invalid data",priority = 5,dataProviderClass = CMSDailyDP.class,dataProvider = "adjustmealplannegative")
    public void adjustMealDecrementtNegative(Integer cou,String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        int res=helper.adjustMealSubscrption(cou,storeid, oprt,spin,startepoc,endepoc).ResponseValidator.GetResponseCode();
        Assert.assertEquals(res,500);

    }

    @Test(description = "This test will adjust meal increment sold count for with invalid data",priority = 5,dataProviderClass = CMSDailyDP.class,dataProvider = "adjustmealplannegative")
    public void adjustMealIncrementNegative(Integer cou, String storeid, String oprt, String spin, Long startepoc, Long endepoc) throws Exception {
        int res = helper.adjustMealSubscrption(cou, storeid, oprt, spin, startepoc, endepoc).ResponseValidator.GetResponseCode();
        Assert.assertEquals(res, 500);
    }


}