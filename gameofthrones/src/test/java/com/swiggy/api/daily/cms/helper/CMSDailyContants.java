package com.swiggy.api.daily.cms.helper;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CMSDailyContants {
    public static final String cms_daily_authorization_key = "Basic dXNlcjpjaGVjaw==";
    public static final String cms_daily_tenant_id = "9012d90sdck3o48";
    public static final String cms_user_info = "Ashiwani";
    public static final String cms_service = "DAILY";
    public static final String service_name = "DAILY";
    public static final String entityid="123456";
    public static final Integer sku_id=12;
    public static final Integer product_id=110;
    public static final Integer store_id=10;
    public static final String spin_id="XYZNJNJ";
    public static final String start_time=CMSDailyHelper.getFutureTimeForMin(10);
    public static final String end_time=CMSDailyHelper.getFutureTimeForMin(20);
    public static final boolean inclusive_slot=false;
    public static final boolean inclusive_holidayslot=true;
    public static List day_list = Stream.of("MO", "TU", "WE","TH","FR","SA","SU").collect(Collectors.toList());
    public static List frequency = Stream.of("DAILY","WEEKLY", "MONTHLY", "YEARLY").collect(Collectors.toList());
    public static List entity_type = Stream.of("PLAN", "PRODUCT", "ADDON").collect(Collectors.toList());
    public static final String message_slot_not_exist="Invalid slot id";
    public static final String message_entity_not_exist="Invalid input id";
    public static final Object[] excelheader=new Object[]{"Store ID*","External SKU*","dietary preference","product name","product long description","product short description","parent product name","prepration style","spice level",
            "calorific value","is perishable","cuisine","ingredients"};
    public static List rel_type = Stream.of("plan-relation").collect(Collectors.toList());
    public static List dmoperation= Stream.of("INCREMENT","DECREMENT").collect(Collectors.toList());
    public static final String service_line_meal = "daily_catalog_product";
    public static final String service_line_plan = "daily_catalog_plan";
    public static final String store_id_inventory="39533";
    public static final Long slot_id=8920677l;




}
