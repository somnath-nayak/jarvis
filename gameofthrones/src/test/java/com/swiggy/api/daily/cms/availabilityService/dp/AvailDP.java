package com.swiggy.api.daily.cms.availabilityService.dp;

import com.swiggy.api.daily.cms.availabilityService.helper.AvailServiceConstant;
import com.swiggy.api.daily.cms.availabilityService.pojo.AreaSlotPOJO;
import com.swiggy.api.daily.cms.availabilityService.pojo.CitySlotPOJO;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.pojo.Slot.SlotCreate;
import com.swiggy.api.daily.vendor.helper.VendorConstant;
import com.swiggy.api.daily.vendor.pojo.RestaurantSlotPOJO;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AvailDP {

    JsonHelper jsonHelper = new JsonHelper();

    @DataProvider(name = "citySlot")
    public Object[][] citySlotCreationSuccess() throws Exception {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new CitySlotPOJO().setDefault(3,0,2359,"MON"))
                }
        };
    }

    @DataProvider(name = "areaSlot")
    public Object[][] areaSlotCreationSuccess() throws Exception {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new AreaSlotPOJO().setDefault(160,0,2359,"MON"))
                }
        };
    }

    @DataProvider(name = "restTimeSlot")
    public Object[][] restTimeSlot() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new RestaurantSlotPOJO().setDefault(VendorConstant.close_time, VendorConstant.MON, VendorConstant.open__time, "9990"))
                }
        };
    }

    @DataProvider(name = "itemSlotCreatePLAN")
    public Object[][] itemSlotPLAN() throws IOException {
        List list = new ArrayList();
        list.add("MO");
        list.add("TU");
        list.add("WE");
        list.add("TH");
        list.add("FR");
        list.add("SA");
        list.add("SU");

        List list1 = new ArrayList();
        list1.add(new SlotCreate().setData("PLAN",1,list,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.plan).withStore_id(9990));
        return new Object[][]{
                {
                    jsonHelper.getObjectToJSON(list1)
                }
        };
    }

    @DataProvider(name = "itemSlotCreatePRODUCT")
    public Object[][] itemSlotPRODUCT() throws IOException {
        List list = new ArrayList();
        list.add("MO");
        list.add("TU");
        list.add("WE");
        list.add("TH");
        list.add("FR");
        list.add("SA");
        list.add("SU");
        List list1 = new ArrayList();
        list1.add(new SlotCreate().setData("PRODUCT",1,list,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.product).withStore_id(9990));
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(list1)
                }
        };
    }


}


