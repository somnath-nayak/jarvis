package com.swiggy.api.daily.checkout.helper;

import com.swiggy.api.daily.checkout.pojo.*;
import com.swiggy.api.daily.checkout.pojo.OrderResponse.Orders;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;

import java.util.HashMap;
import java.util.Map;

public class DailyCheckoutOrderHelper {

    static Initialize gameOfThrones = new Initialize();
    DailyCheckoutCommonUtils utils=new DailyCheckoutCommonUtils();
    CheckoutCartHelper cartHelper=new CheckoutCartHelper();
    JsonHelper jsonHelper= new JsonHelper();
    DailyAddressHelper addresshelper=new DailyAddressHelper();




    public Processor getDailyOrderOMS(String orderJobId){
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "getOrderdDailyOMS", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        Processor processor = new Processor(service,requestHeader, null, new String[]{orderJobId});
        return processor;
    }

    public Processor getDailyPLOrder(String tid,String token,String orderJobId){
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "getDailyPlOrder", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("version-code",DailyCheckoutConstants.VERSION_CODE);
        requestHeader.put("deviceId",DailyCheckoutConstants.DEVICE_ID);
        requestHeader.put("swuid",DailyCheckoutConstants.DEVICE_ID);
        requestHeader.put("X-Forwarded-For","15.0.2.7");
        Processor processor = new Processor(service,requestHeader, null, new String[]{orderJobId});
        return processor;
    }


    public Processor dailyOrderRefund(String payment_txn_id,String amount){
        String payload=null;
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "refund", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("Authorization", DailyCheckoutConstants.REFUND_AUTHORIZATION);

        Refund refund=new Refund("ref-"+payment_txn_id,payment_txn_id,Double.parseDouble(amount),DailyCheckoutConstants.TEST_REFUND);
        try{
            payload = (jsonHelper.getObjectToJSON(refund));
        } catch (Exception e){
            e.printStackTrace();
        }
        Processor processor = new Processor(service, requestHeader, new String[]{payload});
        return processor;
    }

    public Processor dailyOmsOrderRefund(String payment_txn_id,String amount){
        String payload=null;
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "refund", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("Authorization", DailyCheckoutConstants.REFUND_AUTHORIZATION);

        Refund refund=new Refund("",payment_txn_id,Double.parseDouble(amount),DailyCheckoutConstants.TEST_REFUND);
        try{
            payload = (jsonHelper.getObjectToJSON(refund));
        } catch (Exception e){
            e.printStackTrace();
        }
        Processor processor = new Processor(service, requestHeader, new String[]{payload});
        return processor;
    }

    public Processor generatePlPaymentLink(String tid,String token,String cartId){
        GameOfThronesService service = new GameOfThronesService("checkoutpdpdaily", "generateplpaymentlink", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("version-code",DailyCheckoutConstants.VERSION_CODE);
        requestHeader.put("deviceId",DailyCheckoutConstants.DEVICE_ID);
        requestHeader.put("swuid",DailyCheckoutConstants.DEVICE_ID);
        String payload=null;
        PaymentLink paymentLink=new PaymentLink(cartId);
        try{
            payload = (jsonHelper.getObjectToJSON(paymentLink));
        } catch (Exception e){
            e.printStackTrace();
        }
        Processor processor = new Processor(service, requestHeader, new String[]{payload});
        return processor;
    }

    public Processor getAllPaymentOptions(String tid,String token,String paymentLinkId){
        GameOfThronesService service = new GameOfThronesService("checkoutpaaspl", "getpaymentoptions", gameOfThrones);
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("Authorization", DailyCheckoutConstants.REFUND_AUTHORIZATION);
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("version-code",DailyCheckoutConstants.VERSION_CODE);
        requestHeader.put("deviceId",DailyCheckoutConstants.DEVICE_ID);
        requestHeader.put("swuid",DailyCheckoutConstants.DEVICE_ID);
        Processor processor = new Processor(service,requestHeader, null, new String[]{paymentLinkId});
        return processor;
    }


    public String dailyCreateOrder(String mobile, String password,String cityId,String storeId,String cartType){

        HashMap<String, String> userInfo=utils.dailyLogin(mobile,password);
        HashMap<String, String> userInfoHeader=utils.getUserInfoHeader(userInfo);

        Processor cartProcessor=cartHelper.createCartForOthers(userInfoHeader,cartType,cityId,storeId);
        utils.validateErrorCode(cartProcessor);

        String cartId=cartProcessor.ResponseValidator.GetNodeValue("$.data.cartId");
        String addressId=Integer.toString(cartProcessor.
                ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.addressDetail.id"));

        String orderPayload=buildOrderPayload(cartId,addressId,DailyCheckoutConstants.PAYMENT_METHOD);

        Processor processor=cartHelper.placeOrder(userInfoHeader,orderPayload);
        utils.validateErrorCode(cartProcessor);

        String orderJobId=processor.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.orders..order_jobs..order_job_id")
                .replace("[\"","")
                .replace("\"]","");
        return orderJobId;
    }


    public Processor dailyCreateOrderInternal(HashMap<String, String> userInfoHeader,String cityId,
                                           String storeId,String cartType,String paymentMethod){

        Processor cartProcessor=cartHelper.createCartForOthers(userInfoHeader,cartType,cityId,storeId);
        utils.validateErrorCode(cartProcessor);

        String cartId=cartProcessor.ResponseValidator.GetNodeValue("$.data.cartId");
        String addressId=Integer.toString(cartProcessor.
                ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.addressDetail.id"));
        String orderPayload=buildOrderPayload(cartId,addressId,paymentMethod);

        Processor processor=cartHelper.placeOrder(userInfoHeader,orderPayload);
        utils.validateErrorCode(cartProcessor);

        return processor;
    }


    public Processor dailyCheckoutCreatePlOrder(String tid,String token,Processor cartProcessor,String paymentMethod){

        utils.validateErrorCode(cartProcessor);

        String cartId=cartProcessor.ResponseValidator.GetNodeValue("$.data.cartId");
        String addressId=Integer.toString(cartProcessor.
                ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.addressDetail.id"));
        String orderPayload=buildOrderPayload(cartId,addressId,paymentMethod);

        Processor processor=cartHelper.placePlOrder(tid, token,orderPayload);
        utils.validateErrorCode(cartProcessor);

        return processor;
    }



    public Processor dailyCheckoutCreateOrder(HashMap<String, String> userInfoHeader,Processor cartProcessor,String paymentMethod){

        utils.validateErrorCode(cartProcessor);

        String cartId=cartProcessor.ResponseValidator.GetNodeValue("$.data.cartId");
        String addressId=Integer.toString(cartProcessor.
                ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.addressDetail.id"));
        String orderPayload=buildOrderPayload(cartId,addressId,paymentMethod);

        Processor processor=cartHelper.placeOrder(userInfoHeader,orderPayload);
        utils.validateErrorCode(cartProcessor);

        return processor;
    }

    public String buildOrderPayload(String cartId,String addressId,String paymentMethod){
        String orderPayload=null;
        PlaceOrder placeOrder=new PlaceOrder();
        PaymentInfo paymentInfo=new PaymentInfo();

        paymentInfo.setPaymentType(DailyCheckoutConstants.PAYMENT_TYPE);
        paymentInfo.setPaymentMethod(paymentMethod);
        paymentInfo.setMetadata("metadata");

        placeOrder.setCartId(cartId);
        placeOrder.setAddressId(Integer.parseInt(addressId));
        placeOrder.setPaymentType(DailyCheckoutConstants.PAYMENT_TYPE);
        placeOrder.setPaymentInfo(paymentInfo);
        try{
            orderPayload=jsonHelper.getObjectToJSON(placeOrder);
        } catch (Exception e){
            e.printStackTrace();
        }
        return orderPayload;

    }

    public void cmsReducePrice(){

    }


    public String getOrderJobId(Processor orderProcessor){
            String orderData =orderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.orders")
                                                                .toJSONString();
            Orders data= Utility.jsonDecode(orderData.substring(1,orderData.length() - 1), Orders.class);
            Assert.assertNotNull(data.getOrder_jobs()[0].getOrder_job_id(),"Order_job_id is null");
            return data.getOrder_jobs()[0].getOrder_job_id();
    }

    public String  getTransactionId(Processor orderProcessor) {
        String orderData =orderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.orders")
                .toJSONString();
        Orders data=Utility.jsonDecode(orderData.substring(1,orderData.length() - 1), Orders.class);
        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_id(),"Transaction_id is null");
        return data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_id();
    }


    public Processor dailyCreateOrderExternal(String tid, String token,String cityId,String storeId,String cartType){

        HashMap<String, String> header=utils.getHeader(tid,token);
        Processor cartProcessor=cartHelper.createCartPlForOthers(header,cartType,cityId,storeId);
        utils.validateStatusCode(cartProcessor);

        String cartId=cartProcessor.ResponseValidator.GetNodeValue("$.data.cart_id");

        String addressId=null;
        try{
            addressId=cartProcessor.
                    ResponseValidator.GetNodeValue("$.data.addresses.serviceable_addresses[0].id");
        } catch (Exception e){
            e.printStackTrace();
            addressId=addresshelper.getServiceableAddressId(header);
        }

        String orderPayload=buildOrderPayload(cartId,addressId,DailyCheckoutConstants.PAYMENT_METHOD);

        Processor processor=cartHelper.placePlOrder(tid,token,orderPayload);
        utils.validateStatusCode(cartProcessor);
        return processor;
    }


    public Processor getDailyOrderDetails(String tid,String token,String orderID){
        GameOfThronesService service = new GameOfThronesService("checkout", "getOrderDetailsV1", gameOfThrones);
            HashMap<String, String> requestheaders = new HashMap<String, String>();
            requestheaders.put("content-type", "application/json");
            requestheaders.put("tid", tid);
            requestheaders.put("token", token);
            requestheaders.put("Authorization", CheckoutConstants.authorization);
        String[] urlParams = {orderID};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

}
