package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;
import scala.Int;

import java.util.ArrayList;

public class DeliveryFees {
    @JsonProperty("feeType")
    private String feeType;
    @JsonProperty("fee")
    private Integer fee;
    
    public String getFeeType() {
        return feeType;
    }
    
    public void setFeeType(String feeType) {
        this.feeType = feeType;
    }
    
    public Integer getFee() {
        return fee;
    }
    
    public void setFee(Integer fee) {
        this.fee = fee;
    }
    
   public DeliveryFees withFeeType(String feeType){
        setFeeType(feeType);
        return this;
   }
    public DeliveryFees withFee(Integer fee){
        setFee(fee);
        return this;
    }

    public DeliveryFees deliveryFeesWithGivenValues(String feeType , Integer fee){
        setFeeType(feeType); setFee(fee);
        return this;
    }
    
    public ArrayList<DeliveryFees> withGivenValues(String feeType, Integer fee){
        ArrayList<DeliveryFees> deliveryFeesArrayList = new ArrayList<>();
    
        deliveryFeesArrayList.add(0,deliveryFeesWithGivenValues(feeType,fee));
        
        return deliveryFeesArrayList;
    }
}
