package com.swiggy.api.daily.checkout.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "refund_request_id",
        "payment_txn_id",
        "amount",
        "reason"
})


public class Refund
{
    @JsonProperty("reason")
    private String reason;

    @JsonProperty("refund_request_id")
    private String refund_request_id;

    @JsonProperty("amount")
    private double amount;

    @JsonProperty("payment_txn_id")
    private String payment_txn_id;


    /**
     *
     * @param refund_request_id
     * @param payment_txn_id
     * @param amount
     * @param reason
     */
    public Refund(String refund_request_id, String payment_txn_id, double amount, String reason){
        super();
        this.refund_request_id = refund_request_id;
        this.payment_txn_id = payment_txn_id;
        this.amount = amount;
        this.reason = reason;
    }


    @JsonProperty("reason")
    public String getReason ()
    {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason (String reason)
    {
        this.reason = reason;
    }

    @JsonProperty("refund_request_id")
    public String getRefund_request_id ()
    {
        return refund_request_id;
    }

    @JsonProperty("refund_request_id")
    public void setRefund_request_id (String refund_request_id)
    {
        this.refund_request_id = refund_request_id;
    }

    @JsonProperty("amount")
    public double getAmount ()
    {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount (double amount)
    {
        this.amount = amount;
    }

    @JsonProperty("payment_txn_id")
    public String getPayment_txn_id ()
    {
        return payment_txn_id;
    }

    @JsonProperty("payment_txn_id")
    public void setPayment_txn_id (String payment_txn_id)
    {
        this.payment_txn_id = payment_txn_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [reason = "+reason+", refund_request_id = "+refund_request_id+", amount = "+amount+", payment_txn_id = "+payment_txn_id+"]";
    }
}
