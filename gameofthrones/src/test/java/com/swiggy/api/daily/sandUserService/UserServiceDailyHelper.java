package com.swiggy.api.daily.sandUserService;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import java.util.*;

public class UserServiceDailyHelper {
    Initialize gameofthrones = Initializer.getInitializer();

    public String tid = null;
    public String token = null;
    public String customerId = null;
    public String deviceId = null;
    public String itemId = null;
    public String restId = null;
    public String quantity = null;
    public String orderId = null;

    RedisHelper redisHelper = new RedisHelper();

    private HashMap<String, String> contentType() {
        HashMap<String, String> requestheaders = new HashMap<String, String>() {{
            put("content-type", "application/json");
        }};
        return requestheaders;
    }

    public String JsonString(String response, String jsonPath) {
        return JsonPath.read(response, jsonPath).toString().replace("[", "").replace("]", "").replace("\"", "");
    }

    public Processor signUpV2(String name, String mobile, String email, String password, String signupSource) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "signupv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return new Processor(service, headers, new String[]{name, mobile, email, password, signupSource});
    }

    public String getotpFromRedis(String tid) {
        return JsonPath.read(redisHelper.getValue(SANDConstants.sandRedis, 0, tid).toString(), "user.otp");
    }

    public Processor verifyOTP1(String tid) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "otpverifyv1", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return new Processor(service, headers, null, new String[]{getotpFromRedis(tid)});
    }

    public Processor login(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        contentType();
        GameOfThronesService gots = new GameOfThronesService("sanduser", "loginV22", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor login_via_tid(String tid, String mobile, String password) {
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        GameOfThronesService gots = new GameOfThronesService("sanduser", "loginviatid", gameofthrones);
        Processor processor = new Processor(gots, headers, new String[]{mobile, password});
        return processor;
    }


    /////////////////////////////////////////////////////////


    public Processor login1(String mobile, String password) {
        GameOfThronesService gots = new GameOfThronesService("sanduser", "loginV22", gameofthrones);
        return new Processor(gots, contentType(), new String[]{mobile, password});
    }


    public String redisconnectget(String keyinitial, String entity, int redisdb) {
        RedisHelper red = new RedisHelper();
        String s = keyinitial;
        String key = s.concat(entity);
        String value = (String) red.getValue("analyticsredis", redisdb, key);
        return value;

    }


    public HashMap<String, String> loginData(String mobile, String password) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "consumerloginv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        Processor p = new Processor(service, headers, new String[]{mobile, password});
        String resp = p.ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
        String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
        String userId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        HashMap<String, String> hashMap = new HashMap<String, String>() {{
            put("Tid", tid);
            put("Token", token);
            put("userId", userId);
        }};
        return hashMap;
    }

    public Processor loginDataProcessor(String mobile, String password) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "consumerloginv2", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Content-Type", "application/json");
        Processor p = new Processor(service, headers, new String[]{mobile, password});
        return p;
    }


    public void changeSLA(String delivery, String maxSla, String minSla, String zoneId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(delivery);
        int a = sqlTemplate.update(SANDConstants.updateSla + maxSla + SANDConstants.updateSla1 + minSla + SANDConstants.whereId + zoneId);
        System.out.println(a);
    }

    public String basic() {
        String s = null;
        try {
            s = Base64.getEncoder().encodeToString((SANDConstants.userName + ":" + SANDConstants.pass).getBytes("utf-8"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Basic " + s;
    }

    public HashMap<String, String> getHeader() {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", basic());
        return hm;
    }

    public HashMap<String, String> getTidHeader() {
        HashMap<String, String> hm = loginData(SANDConstants.mobile, SANDConstants.password);
        hm.put("Content-Type", "application/json");
        return hm;
    }

    private HashMap<String, String> getHeaderTid() {
        HashMap<String, String> hm = loginData(SANDConstants.mobile, SANDConstants.password);
        hm.put("Content-Type", "application/json");
        hm.put("Authorization", basic());
        return hm;
    }

    public String getMobile() {
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
    }



    public Processor userInternal(String referralCode, String value) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "userinternal", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{referralCode, value});
    }

    public Processor userInternalWithoutAuth(String referralCode, String value) {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanduser", "userinternal", gameofthrones);
        return new Processor(service, hm, null, new String[]{referralCode, value});
    }


    public Processor userProfileAdmin(String param, String key) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "userprofileadmin", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{param + "=" + key});
    }


    public Processor userInternalpatch(String key, String mobile, String userId) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "userinternalpatch", gameofthrones);
        return new Processor(service, getHeader(), new String[]{key, mobile}, new String[]{userId});
    }

    public Processor userProfileGet(String tid, String token, String addressReqd, String detailsReqd) {
        HashMap<String, String> hMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        GameOfThronesService service = new GameOfThronesService("sanduser", "userprofile", gameofthrones);
        return new Processor(service, hMap, null, new String[]{addressReqd, detailsReqd});
    }


    public Processor loginCheck(String mobile) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "logincheckv2", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{mobile});
    }

    public Processor updateMobile(String mobile, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "updatemobile", gameofthrones);
        return new Processor(service, header(tid, token), null, new String[]{mobile});
    }

    public Processor setPassword(String mobile, String password, String tid, String token) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "passwordsetv2", gameofthrones);
        return new Processor(service, header(tid, token), new String[]{mobile, password});
    }


    private HashMap<String, String> header(String tid, String token) {
        HashMap<String, String> hashMap = new HashMap<String, String>() {{
            put("tid", tid);
            put("token", token);
            contentType();
        }};
        return hashMap;
    }

    public Processor updateMobileOtp(String tid, String token, String mobile, String otp) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "updatemobileotp", gameofthrones);
        return new Processor(service, header(tid, token), new String[]{mobile, otp});
    }



    public Map<String, Object> referralCode(String userId) {
        String s = SANDConstants.referralCode + "'" + userId + "'";
        System.out.println(s);
        Map<String, Object> referral = SystemConfigProvider.getTemplate("user").queryForMap(SANDConstants.referralCode + "'" + userId + "'");
        return referral;
    }

    public Map<String, Object> userSignUp(String mobile) {
        Map<String, Object> referral = SystemConfigProvider.getTemplate("user").queryForMap(SANDConstants.signUp + "'" + mobile + "'");
        return referral;
    }

    public String getOTP(String mobile) {
        return redisHelper.getValue(SANDConstants.sandRedis, 0, mobile + SANDConstants.otp).toString();
    }


    public String updateMobileRedis(String userId, String newMobile) {
        return redisHelper.getValue("sandredisstage", 0, SANDConstants.mobileUpdateKey + userId + "_" + newMobile).toString();
    }

    public Processor signUp1(String mobile, String email, String name) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "signupv1", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{mobile, email, name});
    }

    public Processor sendOTP(String mobile) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "otpsendv2", gameofthrones);
        return new Processor(service, contentType(), null, new String[]{mobile});
    }


    public Processor verifyOTP(String tid, String otp) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "otpverifyv1", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>() {{
            contentType();
            put("tid", tid);
        }};
        return new Processor(service, headers, null, new String[]{getotpFromRedis(tid)});
    }


    public HashMap<String, String> createUser(String name, String mobile, String email, String password, String signupSource) {
        String response = signUpV2(name, mobile, email, password,signupSource).ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(response, "$.tid").toString().replace("[", "").replace("]", "").replace("\"", "");
        String otpResponse = verifyOTP(tid, "").ResponseValidator.GetBodyAsText();
        HashMap<String, String> hm = new HashMap<String, String>() {{
            put("tid", JsonString(otpResponse, "$.tid"));
            put("token", JsonString(otpResponse, "$.data.token"));
            put("userId", JsonString(otpResponse, "$.data.customer_id"));
            put("referral_code", JsonString(otpResponse, "$.data.referral_code"));
        }};
        return hm;
    }



    public String getemailString() {
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) {
            int index = (int) (rnd.nextFloat() * CHARS.length());
            salt.append(CHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr + "@gmail.com";
    }




    public Processor consumerSignUp(String name, String mobile, String email, String password) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "signupv2", gameofthrones);
        return new Processor(service, contentType(), new String[]{name, mobile, email, password});
    }

    public Processor referredTo(String referralCode) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "referredto", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{referralCode});
    }

    public Processor referredBy(String referralCode) {
        GameOfThronesService service = new GameOfThronesService("sanduser", "referredby", gameofthrones);
        return new Processor(service, getHeader(), null, new String[]{referralCode});
    }


    public String randomAlpha() {
        int count = 6;
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * alphabet.length());
            builder.append(alphabet.charAt(character));
        }
        return builder.toString();
    }

    public String convertArray (String...optionalKeys){
        return Arrays.toString(optionalKeys).replaceAll("\\]|\\[|\"| ", "");
    }
}







