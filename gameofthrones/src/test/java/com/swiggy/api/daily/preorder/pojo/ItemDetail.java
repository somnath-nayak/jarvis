
package com.swiggy.api.daily.preorder.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "item_id",
    "quantity"
})
public class ItemDetail {

    @JsonProperty("item_id")
    private String itemId;
    @JsonProperty("quantity")
    private long quantity;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ItemDetail() {
    }

    /**
     * 
     * @param quantity
     * @param itemId
     */
    public ItemDetail(String itemId, long quantity) {
        super();
        this.itemId = itemId;
        this.quantity = quantity;
    }

    @JsonProperty("item_id")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("item_id")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public ItemDetail withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("quantity")
    public long getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public ItemDetail withQuantity(long quantity) {
        this.quantity = quantity;
        return this;
    }
    public ItemDetail setDefaultData()
    {
        this.withItemId("123").withQuantity(12);
        return this;
    }

}
