package com.swiggy.api.daily.mealSlot.dp;

import com.swiggy.api.daily.mealSlot.helper.MealSlotConstant;
import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import com.swiggy.api.daily.mealSlot.pojo.MealSlotsPOJO;
import com.swiggy.api.daily.mealSlot.pojo.UpdateMealSlotsPOJO;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

public class MealSlotDP {

    MealSlotsPOJO mealSlotsPOJO;
    UpdateMealSlotsPOJO updateMealSlotsPOJO;
    JsonHelper jsonHelper=new JsonHelper();
    MealSlotHelper mealSlotHelper;

    @DataProvider(name = "createMealSlot")
    public Object[][] createMealSlot() throws Exception {
        {
            mealSlotsPOJO = new MealSlotsPOJO()
                    .setDefaultData();
            return new Object[][] {{mealSlotsPOJO}};
        }
    }

    @DataProvider(name = "updateMealSlot")
    public Object[][] updateMealSlot() throws Exception {
        mealSlotsPOJO = new MealSlotsPOJO()
                .setDefaultData();
        updateMealSlotsPOJO = new UpdateMealSlotsPOJO()
                .setDefaultData();
         mealSlotsPOJO.setSlot(MealSlotConstant.BREAKFAST);
        return new Object[][] {{mealSlotsPOJO, updateMealSlotsPOJO}};
    }


}
