package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class LatLong {
    private static final Logger logger = LoggerFactory.getLogger(LatLong.class);

    @JsonProperty("lat")
    String latitude;
    @JsonProperty("lng")
    String longitude;


    public static Logger getLogger() {
        return logger;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    @Override
    public String toString() {
        return latitude+","+longitude;
    }

    public LatLong(Double lat, Double lng){
        this.latitude = String.valueOf(lat);
        this.longitude = String.valueOf(lng);
    }

    public double getLatAsDouble(){
        return Double.parseDouble(latitude);
    }

    public double getLngAsDouble(){
        return Double.parseDouble(longitude);
    }

    public LatLong(String latLng){
        String[] latLngs = latLng.trim().split(",");
        this.latitude = latLngs[0];
        this.longitude = latLngs[1];
    }

    public GeoLocation toGeoLocation() {
        return new GeoLocation();
    }

    public static boolean inRange(LatLong latLong) {
        if (Double.compare(latLong.getLatAsDouble(), 90) <= 0 && Double.compare(latLong.getLatAsDouble(), -90) >= 0
                && Double.compare(latLong.getLngAsDouble(), 180) <= 0 && Double.compare(latLong.getLngAsDouble(), -180) >= 0) {
            return true;
        }
        return false;
    }
}
