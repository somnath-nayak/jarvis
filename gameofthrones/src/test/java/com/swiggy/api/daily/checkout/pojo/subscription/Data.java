package com.swiggy.api.daily.checkout.pojo.subscription;



import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;


public class Data {


    @JsonProperty("errorCode")
    private Long errorCode;
    @JsonProperty("nonCheckoutReason")
    private String nonCheckoutReason;
    @JsonProperty("cartType")
    private String cartType;
    @JsonProperty("cartId")
    private String cartId;
    @JsonProperty("cartItems")
    private List<CartItem> cartItems = null;
    @JsonProperty("cartVersion")
    private Long cartVersion;
    @JsonProperty("cartBill")
    private CartBill cartBill;
    @JsonProperty("couponDetails")
    private CouponDetails couponDetails;
    @JsonProperty("mealSlotId")
    private Object mealSlotId;
    @JsonProperty("addressId")
    private String addressId;
    @JsonProperty("dailySubType")
    private String dailySubType;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("storeDetails")
    private StoreDetails storeDetails;
    @JsonProperty("userProfile")
    private UserProfile userProfile;
    @JsonProperty("cartDeliveryDetails")
    private CartDeliveryDetails cartDeliveryDetails;
    @JsonProperty("deliverySlotAvailabilityDetails")
    private List<DeliverySlotAvailabilityDetail> deliverySlotAvailabilityDetails = null;



    @JsonProperty("errorCode")
    public Long getErrorCode() {
        return errorCode;
    }

    @JsonProperty("errorCode")
    public void setErrorCode(Long errorCode) {
        this.errorCode = errorCode;
    }

    public Data withErrorCode(Long errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    @JsonProperty("nonCheckoutReason")
    public String getNonCheckoutReason() {
        return nonCheckoutReason;
    }

    @JsonProperty("nonCheckoutReason")
    public void setNonCheckoutReason(String nonCheckoutReason) {
        this.nonCheckoutReason = nonCheckoutReason;
    }

    public Data withNonCheckoutReason(String nonCheckoutReason) {
        this.nonCheckoutReason = nonCheckoutReason;
        return this;
    }

    @JsonProperty("cartType")
    public String getCartType() {
        return cartType;
    }

    @JsonProperty("cartType")
    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public Data withCartType(String cartType) {
        this.cartType = cartType;
        return this;
    }

    @JsonProperty("cartId")
    public String getCartId() {
        return cartId;
    }

    @JsonProperty("cartId")
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public Data withCartId(String cartId) {
        this.cartId = cartId;
        return this;
    }

    @JsonProperty("cartItems")
    public List<CartItem> getCartItems() {
        return cartItems;
    }

    @JsonProperty("cartItems")
    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }


    public Data withCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
        return this;
    }

    @JsonProperty("cartVersion")
    public Long getCartVersion() {
        return cartVersion;
    }

    @JsonProperty("cartVersion")
    public void setCartVersion(Long cartVersion) {
        this.cartVersion = cartVersion;
    }

    public Data withCartVersion(Long cartVersion) {
        this.cartVersion = cartVersion;
        return this;
    }

    @JsonProperty("cartBill")
    public CartBill getCartBill() {
        return cartBill;
    }

    @JsonProperty("cartBill")
    public void setCartBill(CartBill cartBill) {
        this.cartBill = cartBill;
    }

    public Data withCartBill(CartBill cartBill) {
        this.cartBill = cartBill;
        return this;
    }

    @JsonProperty("couponDetails")
    public CouponDetails getCouponDetails() {
        return couponDetails;
    }

    @JsonProperty("couponDetails")
    public void setCouponDetails(CouponDetails couponDetails) {
        this.couponDetails = couponDetails;
    }

    public Data withCouponDetails(CouponDetails couponDetails) {
        this.couponDetails = couponDetails;
        return this;
    }

    @JsonProperty("mealSlotId")
    public Object getMealSlotId() {
        return mealSlotId;
    }

    @JsonProperty("mealSlotId")
    public void setMealSlotId(Object mealSlotId) {
        this.mealSlotId = mealSlotId;
    }

    public Data withMealSlotId(Object mealSlotId) {
        this.mealSlotId = mealSlotId;
        return this;
    }

    @JsonProperty("addressId")
    public String getAddressId() {
        return addressId;
    }

    @JsonProperty("addressId")
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public Data withAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }

    @JsonProperty("dailySubType")
    public String getDailySubType() {
        return dailySubType;
    }

    @JsonProperty("dailySubType")
    public void setDailySubType(String dailySubType) {
        this.dailySubType = dailySubType;
    }

    public Data withDailySubType(String dailySubType) {
        this.dailySubType = dailySubType;
        return this;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public Data withLocation(Location location) {
        this.location = location;
        return this;
    }

    @JsonProperty("storeDetails")
    public StoreDetails getStoreDetails() {
        return storeDetails;
    }

    @JsonProperty("storeDetails")
    public void setStoreDetails(StoreDetails storeDetails) {
        this.storeDetails = storeDetails;
    }

    public Data withStoreDetails(StoreDetails storeDetails) {
        this.storeDetails = storeDetails;
        return this;
    }

    @JsonProperty("userProfile")
    public UserProfile getUserProfile() {
        return userProfile;
    }

    @JsonProperty("userProfile")
    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public Data withUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
        return this;
    }

    @JsonProperty("cartDeliveryDetails")
    public CartDeliveryDetails getCartDeliveryDetails() {
        return cartDeliveryDetails;
    }

    @JsonProperty("cartDeliveryDetails")
    public void setCartDeliveryDetails(CartDeliveryDetails cartDeliveryDetails) {
        this.cartDeliveryDetails = cartDeliveryDetails;
    }

    public Data withCartDeliveryDetails(CartDeliveryDetails cartDeliveryDetails) {
        this.cartDeliveryDetails = cartDeliveryDetails;
        return this;
    }

    @JsonProperty("deliverySlotAvailabilityDetails")
    public List<DeliverySlotAvailabilityDetail> getDeliverySlotAvailabilityDetails() {
        return deliverySlotAvailabilityDetails;
    }

    @JsonProperty("deliverySlotAvailabilityDetails")
    public void setDeliverySlotAvailabilityDetails(List<DeliverySlotAvailabilityDetail> deliverySlotAvailabilityDetails) {
        this.deliverySlotAvailabilityDetails = deliverySlotAvailabilityDetails;
    }

    public Data withDeliverySlotAvailabilityDetails(List<DeliverySlotAvailabilityDetail> deliverySlotAvailabilityDetails) {
        this.deliverySlotAvailabilityDetails = deliverySlotAvailabilityDetails;
        return this;
    }


}
