package com.swiggy.api.daily.vendor.test;

import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.vendor.dp.VendorOrderListingDp;
import com.swiggy.api.daily.vendor.helper.VendorConstants;
import com.swiggy.api.daily.vendor.helper.VendorOrderListingHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public class VendorOrderListingTest {
    
    VendorOrderListingHelper vendorOrderListingHelper = new VendorOrderListingHelper();
    SubscriptionHelper subscriptionHelper = new SubscriptionHelper();
    
    @Test(dataProvider = "checkMealAllActiveSlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkMealAllActiveSlotsTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        System.out.println("access token " + token);
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        int i = 0;
        String dayBreaks = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..dayBreak");
        String[] mealSlots = dayBreaks.replace("[", "").replace("]", "").split(",");
        String[] startTime = (response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..slots[0].startTime")).replace("[", "").replace("]", "").split(",");
        String[] endTime = (response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..slots[0].endTime")).replace("[", "").replace("]", "").split(",");
        
        for (int j = 0; j < mealSlots.length; j++) {
            Assert.assertEquals(mealSlots[j], VendorConstants.slot[j]);
            Assert.assertEquals(startTime[j], VendorConstants.mealStartTime[j]);
            Assert.assertEquals(endTime[j], VendorConstants.mealSlotEndTime[j]);
            System.out.println("meal slot  " + mealSlots[j] + " compare with " + VendorConstants.slot[j] + "  startTime  " + startTime[j] + " compare with " + VendorConstants.mealStartTime[j] + " endTime  " + endTime[j] + " compare with " + VendorConstants.mealSlotEndTime[j]);
        }
    }
    
    @Test(dataProvider = "checkMealAllActiveSlotsNoDeliverySlotDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkMealAllActiveSlotsNoDeliverySlotTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        int i = 0;
        String dayBreaks = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..dayBreak");
        String[] mealSlots = dayBreaks.replace("[", "").replace("]", "").split(",");
        String[] startTime = (response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..slots[0].startTime")).replace("[", "").replace("]", "").split(",");
        String[] endTime = (response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..slots[0].endTime")).replace("[", "").replace("]", "").split(",");
        
        for (int j = 0; j < mealSlots.length; j++) {
            Assert.assertEquals(mealSlots[j], VendorConstants.slot[j]);
            Assert.assertEquals(startTime[j], VendorConstants.mealStartTime[j]);
            Assert.assertEquals(endTime[j], VendorConstants.mealSlotEndTime[j]);
            System.out.println("meal slot  " + mealSlots[j] + " compare with " + VendorConstants.slot[j] + "  startTime  " + startTime[j] + " compare with " + VendorConstants.mealStartTime[j] + " endTime  " + endTime[j] + " compare with " + VendorConstants.mealSlotEndTime[j]);
        }
    }
    
    @Test(dataProvider = "checkWhenMealSlotIsNotPresentDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkWhenMealSlotIsNotPresentTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String dayBreak = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks");
        Assert.assertEquals(dayBreak, "[[]]");
        
    }
    
    @Test(dataProvider = "checkWhenMealSlotIsPresentForOtherCityDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkWhenMealSlotIsPresentForOtherCityTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String dayBreak = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks");
        Assert.assertEquals(dayBreak, "[[]]");
    }
    
    @Test(dataProvider = "checkWhenOneMealSlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkWhenOneMealSlotsTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String dayBreaks = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..dayBreak").replace("[", "").replace("]", "");
        Assert.assertEquals(dayBreaks, VendorConstants.slot[0]);
    }
    
    
    @Test(dataProvider = "checkAllActiveDeliverySlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkAllActiveDeliverySlotsTest(String storeId, String password, String cityId, List<Integer> deliverySlotsId, Integer zoneID, String token) throws ParseException, IOException {
        
        Processor getDeliverySlot = vendorOrderListingHelper.getDeliverySlotsByZoneId(zoneID);
        String startTime[] = new String[deliverySlotsId.size()];
        String endTime[] = new String[deliverySlotsId.size()];
        for (int i = 0; i < deliverySlotsId.size(); i++) {
            startTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].startTime");
            endTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].endTime");
            
        }
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        SoftAssert softAssert = new SoftAssert();
        int k = 0;
        for (int i = 0; i < VendorConstants.mealSlot.length - 1; i++) {
            int x = 4, y = 1;
            String meal = VendorConstants.mealSlot[i + 1];
            
            for (int j = 0; j < x; j++) {
                String start = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + meal + "\")].slots[" + y + "].startTime");
                String end = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + meal + "\")].slots[" + y + "].endTime");
                softAssert.assertEquals(start, startTime[k + 5]);
                softAssert.assertEquals(end, endTime[k + 5]);
                System.out.println("Start " + start + "compare with  " + startTime[k + 5]);
                System.out.println("end " + end + " compare with " + endTime[k + 5]);
                k++;
                y++;
            }
        }
        softAssert.assertAll();
        
    }
    
    @Test(dataProvider = "checkWhenOneDeliverySlotForAllMealSlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkWhenOneDeliverySlotForAllMealSlotsTes(String storeId, String password, String cityId, List<Integer> deliverySlotsId, Integer zoneID, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        Processor getDeliverySlot = vendorOrderListingHelper.getDeliverySlotsByZoneId(zoneID);
        String startTime[] = new String[deliverySlotsId.size()];
        String endTime[] = new String[deliverySlotsId.size()];
        for (int i = 0; i < deliverySlotsId.size(); i++) {
            startTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].startTime");
            endTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].endTime");
        }
        
        String start = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + VendorConstants.mealSlot[1] + "\")].slots[" + 1 + "].startTime");
        String end = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + VendorConstants.mealSlot[1] + "\")].slots[" + 1 + "].endTime");
        softAssert.assertEquals(start, startTime[0]);
        softAssert.assertEquals(end, endTime[0]);
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "checkWhenOneDeliverySlotForMealSlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void checkWhenOneDeliverySlotForMealSlotsTest(String storeId, String password, String cityId, List<Integer> deliverySlotsId, Integer zoneID, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        Processor getDeliverySlot = vendorOrderListingHelper.getDeliverySlotsByZoneId(zoneID);
        String startTime[] = new String[deliverySlotsId.size()];
        String endTime[] = new String[deliverySlotsId.size()];
        
        for (int i = 0; i < deliverySlotsId.size(); i++) {
            startTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].startTime");
            endTime[i] = getDeliverySlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + deliverySlotsId.get(i).toString() + ")].endTime");
        }
        
        String start = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + VendorConstants.mealSlot[1] + "\")].slots[" + 1 + "].startTime");
        String end = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks[?(@.dayBreak==\"" + VendorConstants.mealSlot[1] + "\")].slots[" + 1 + "].endTime");
        softAssert.assertEquals(start, startTime[0]);
        softAssert.assertEquals(end, endTime[0]);
        
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "MealAndDeliverySlotEndTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void MealAndDeliverySlotEndTimeCheckTest(String storeId, String password, String cityId, List<Integer> deliverySlotsId, Integer zoneID, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String items = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"BREAKFAST\")].slots..items");
        Assert.assertEquals(items, "[[]]");
    }
    
    
    @Test(dataProvider = "CheckWhenOnlyOneOrderIsPresentInBarBarForGivenTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckWhenOnlyOneOrderIsPresentInBarBarForGivenTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String orderedItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items");
        String dinnerSlotItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"DINNER\")].slots[1].items");
        Assert.assertNotEquals(orderedItem, "[[]]");
        Assert.assertEquals(dinnerSlotItem, "[[]]");
        
        // TODO Cancel API to cancel subscription
    }
    
    @Test(dataProvider = "CheckWhenOnlyNoOrderIsPresentInBarBarForGivenTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckWhenOnlyNoOrderIsPresentInBarBarForGivenTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String orderedItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items");
        Assert.assertEquals(orderedItem, "[[]]");
        
        
    }
    
    @Test(dataProvider = "CheckWhenMultipleOrdersarePresentInBarBarForGivenTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckWhenMultipleOrdersarePresentInBarBarForGivenTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String dinnerSlotItems = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"DINNER\")].slots[1].items..quantity").replace("[", "").replace("]", "");
        Assert.assertEquals(dinnerSlotItems, "2");
        
        String lunchSlotItems = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items..quantity").replace("[", "").replace("]", "");
        Assert.assertEquals(lunchSlotItems, "1");
        
        // TODO Cancel API to cancel subscription
    }
    
    
    @Test(dataProvider = "CheckItemDetailsInFetchOrderDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckItemDetailsInFetchOrderTest(String storeId, String password, String cityId, HashMap<String, String> details, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        details.get("itemId");
        details.get("display_name");
        details.get("isVeg");
        details.get("maxServe");
        details.get("is_avail"); // inStock
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String orderedItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items");
        String dinnerSlotItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"DINNER\")].slots[1].items");
        String dispayName = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items..[?(@.item_id==\"" + details.get("itemId") + "\")].name").replace("[\"", "").replace("\"]", "");
        
        String isVeg = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items..[?(@.item_id==\"" + details.get("itemId") + "\")].isVeg").replace("[", "").replace("]", "");
        
        String maxServe = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items..[?(@.item_id==\"" + details.get("itemId") + "\")].maxServe").replace("[", "").replace("]", "");
        
        String isAvail = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items..[?(@.item_id==\"" + details.get("itemId") + "\")].inStock").replace("[", "").replace("]", "");
        
        
        softAssert.assertNotEquals(orderedItem, "[[]]");
        softAssert.assertEquals(dinnerSlotItem, "[[]]");
        softAssert.assertEquals(dispayName, details.get("display_name"));
        softAssert.assertEquals(isVeg, details.get("isVeg"));
        softAssert.assertEquals(maxServe, details.get("maxServe"));
        softAssert.assertEquals(isAvail, details.get("is_avail"));
        softAssert.assertAll();
        
        // TODO Cancel API to cancel subscription
    }
    
    
    @Test(dataProvider = "CheckWhenSuscriptionDeliverySlotsAreNotMatchingWithDeliveryServiceDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckWhenSuscriptionDeliverySlotsAreNotMatchingWithDeliveryServiceTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String dinnerMealsCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"LUNCH\")].mealsCount").replace("[", "").replace("]", "");
        
        String lunchMealsCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"DINNER\")].mealsCount").replace("[", "").replace("]", "");
        softAssert.assertEquals(lunchMealsCount, "0");
        softAssert.assertEquals(dinnerMealsCount, "0");
        
        softAssert.assertAll();
        // TODO Cancel API to cancel subscription
    }
    
    
    @Test(dataProvider = "CheckWhenOrdersArePresentForAllDeliverySlotsDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckWhenOrdersArePresentForAllDeliverySlotsTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        // also checked dayBreak meals count and slots meals count & same user with multiple subscription LUNCH -DINNER
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String dayBreakDinnerCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"LUNCH\")].mealsCount").replace("[", "").replace("]", "");
        softAssert.assertEquals(dayBreakDinnerCount, "2");
        
        String dayBreakLunchCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"DINNER\")].mealsCount").replace("[", "").replace("]", "");
        softAssert.assertEquals(dayBreakLunchCount, "2");
        
        String dinnerSlotsMealsCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"DINNER\")].slots[0].mealsCount").replace("[", "").replace("]", "");
        softAssert.assertEquals(dinnerSlotsMealsCount, "2");
        
        String lunchSlotsMealsCount = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks.[?(@.dayBreak==\"LUNCH\")].slots[0].mealsCount").replace("[", "").replace("]", "");
        softAssert.assertEquals(lunchSlotsMealsCount, "2");
        
        softAssert.assertAll();
        // TODO Cancel API to cancel subscription
    }
    
    @Test(dataProvider = "fetchOrderWithRestaurantWhichIsNotPresentDP", dataProviderClass = VendorOrderListingDp.class)
    public void fetchOrderWithRestaurantWhichIsNotPresentTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusCode = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        String statusMessage = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        
        softAssert.assertEquals(statusMessage, VendorConstants.statusMessagePermissionDenied);
        softAssert.assertEquals(statusCode, VendorConstants.statusCodeFive);
        softAssert.assertAll();
        
    }
    
    @Test(dataProvider = "fetchOrderCheckForFromTimeAndEndTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void fetchOrderCheckForFromTimeAndEndTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        
        // check for empty String
        Processor response = vendorOrderListingHelper.vendorOrderListing("", "", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForEmpty, VendorConstants.statusCodeInvalidInput, "Failure because of empty String allowed for Start and End time ");
        softAssert.assertEquals(statusMessageForEmpty, VendorConstants.statusMessageInvalidINput, "Failure because of empty String allowed for Start and End time ");

// check for whitespace character
        Processor responseTwo = vendorOrderListingHelper.vendorOrderListing("   ", "   ", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForWhitespace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForWhiteSpace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForWhiteSpace, VendorConstants.statusCodeInvalidInput, "Failure because of whiteSpace character allowed for Start and End time ");
        softAssert.assertEquals(statusMessageForWhitespace, VendorConstants.statusMessageInvalidINput, "Failure because of whiteSpace character allowed for Start and End time ");
        
        // check for same Start and End time
        Processor responseThree = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getStartTime(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForSameTime, VendorConstants.statusCodeInvalidInput, "Failure because of same Start and End time ");
        softAssert.assertEquals(statusMessageForSameTime, VendorConstants.statusMessageInvalidINput, "Failure because of same Start and End time ");
        
        softAssert.assertAll();
        
    }
    
    @Test(dataProvider = "fetchOrderCheckForLatAndLongDP", dataProviderClass = VendorOrderListingDp.class)
    public void fetchOrderCheckForLatAndLongTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        
        // check for empty String
        Processor response = vendorOrderListingHelper.vendorOrderListing("", "", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForEmpty, VendorConstants.statusCodeInvalidInput, "Failure because of empty String allowed for Lat-Long ");
        softAssert.assertEquals(statusMessageForEmpty, VendorConstants.statusMessageInvalidINput, "Failure because of empty String allowed for Lat-Long ");

// check for whitespace character
        Processor responseTwo = vendorOrderListingHelper.vendorOrderListing("   ", "   ", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForWhitespace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForWhiteSpace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForWhiteSpace, VendorConstants.statusCodeInvalidInput, "Failure because of whiteSpace character allowed for Lat-Long ");
        softAssert.assertEquals(statusMessageForWhitespace, VendorConstants.statusMessageInvalidINput, "Failure because of whiteSpace character allowed for Lat-Long ");
        
        
        // check for same lat-long
        Processor responseThree = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForSameTime, VendorConstants.statusCodeInvalidInput, "Failure because of same Lat-Long ");
        softAssert.assertEquals(statusMessageForSameTime, VendorConstants.statusMessageInvalidINput, "Failure because of same Lat-Long ");
        
        softAssert.assertAll();
        
    }
    
    @Test(dataProvider = "fetchOrderCheckForCityIdDP", dataProviderClass = VendorOrderListingDp.class)
    public void fetchOrderCheckForCityIdTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        SoftAssert softAssert = new SoftAssert();
        // check for empty String
        Processor response = vendorOrderListingHelper.vendorOrderListing("", "", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], "", token);
        String statusMessageForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForEmpty = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForEmpty, VendorConstants.statusCodeInvalidInput, "Failure because of empty String allowed for City ");
        softAssert.assertEquals(statusMessageForEmpty, VendorConstants.statusMessageInvalidINput, "Failure because of empty String allowed for City ");

// check for whitespace character
        Processor responseTwo = vendorOrderListingHelper.vendorOrderListing("   ", "   ", storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], "  ", token);
        String statusMessageForWhitespace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForWhiteSpace = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForWhiteSpace, VendorConstants.statusCodeInvalidInput, "Failure because of whiteSpace character allowed for City ");
        softAssert.assertEquals(statusMessageForWhitespace, VendorConstants.statusMessageInvalidINput, "Failure because of whiteSpace character allowed for City ");
        
        //Changing city id of store
        cityId = "" + (Integer.valueOf(cityId) + 1);
        // requesting with different City id not with the correct city id of the store
        Processor responseThree = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getStartTime(), vendorOrderListingHelper.getEndTIme(), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        String statusMessageForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String statusCodeForSameTime = responseThree.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
        softAssert.assertEquals(statusCodeForSameTime, VendorConstants.statusCodeInvalidInput, "Failure because of same City ");
        softAssert.assertEquals(statusMessageForSameTime, VendorConstants.statusMessageInvalidINput, "Failure because of same City ");
        
        softAssert.assertAll();
        
    }
    
    // Check tomorrows orders
    @Test(dataProvider = "CheckFutureOrderIsPresentInBarBarForGivenTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckFutureOrderIsPresentInBarBarForGivenTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getFutureStartTime("00", "00", 1), vendorOrderListingHelper.getFutureStartTime("23", "59", 1), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String orderedItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items");
        String dinnerSlotItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"DINNER\")].slots[1].items");
        Assert.assertNotEquals(orderedItem, "[[]]");
        Assert.assertEquals(dinnerSlotItem, "[[]]");
        
        // TODO Cancel API to cancel subscription
    }
    
    @Test(dataProvider = "CheckPastOrderIsPresentInBarBarForGivenTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckPastOrderIsPresentInBarBarForGivenTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getFutureStartTime("00", "00", -1), vendorOrderListingHelper.getFutureStartTime("23", "59", -1), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String orderedItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"LUNCH\")].slots[1].items");
        String dinnerSlotItem = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..dayBreaks..[?(@.dayBreak==\"DINNER\")].slots[1].items");
        Assert.assertNotEquals(orderedItem, "[[]]");
        Assert.assertEquals(dinnerSlotItem, "[[]]");
        vendorOrderListingHelper.changeSeverCurrentDate();
        
        // TODO Cancel API to cancel subscription
    }
    
    @Test()
    public void setServeTimeTOCurrent() throws IOException {
        
        vendorOrderListingHelper.changeSeverCurrentDate();
        
    }
    
    
    // Check for From time and To time of fetch orders
    @Test(dataProvider = "CheckOrderFetchFromTimeGreaterThanToTimeDP", dataProviderClass = VendorOrderListingDp.class)
    public void CheckOrderFetchFromTimeGreaterThanToTimeTest(String storeId, String password, String cityId, String token) throws ParseException, IOException {
        
        Processor response = vendorOrderListingHelper.vendorOrderListing(vendorOrderListingHelper.getFutureStartTime("00", "00", 2), vendorOrderListingHelper.getFutureStartTime("23", "59", 1), storeId, password, VendorConstants.deliveryLatLong[0], VendorConstants.deliveryLatLong[1], cityId, token);
        
        String statusMessage = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage").replace("[", "").replace("]", "");
        String statusCode = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode").replace("[", "").replace("]", "");
        
        Assert.assertEquals(statusMessage, VendorConstants.statusMessageInvalidINput, "Failure because in Fetch Orders API allowing fromTime greater than toTime and hitting subscription manager with this invalid time");
        Assert.assertEquals(statusCode, VendorConstants.statusCodeInvalidInput);
        
        // TODO Cancel API to cancel subscription
    }
    
    
}
