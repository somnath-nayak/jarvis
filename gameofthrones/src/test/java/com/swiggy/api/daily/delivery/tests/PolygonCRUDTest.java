package com.swiggy.api.daily.delivery.tests;

import com.swiggy.api.daily.delivery.helper.ServiceabilityHelper;
import com.swiggy.api.daily.helper.*;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class PolygonCRUDTest {

    ServiceabilityHelper serviceabilityHelper = new ServiceabilityHelper();
    static Integer polygId = 0;

    @BeforeClass
    public void beforeClass()
    {

        //Yet to implement

    }

    @Test(priority = 1)
    public void createPolygonTest()
    {
        Processor processor = serviceabilityHelper.createPolygon("DAILY");
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        int id = serviceabilityHelper.getPolygonCreationStatus(processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(id==0);
        int polygonId = serviceabilityHelper.getPolygonIdFromResponse(processor.ResponseValidator.GetBodyAsText());
        if(polygonId>0)
        {
            polygId = polygonId;
        }
        Assert.assertTrue(polygonId>0);
        Assert.assertTrue(serviceabilityHelper.verifySolrEntry(polygonId));

    }

    @Test(priority = 2)
    public void updatePolygonTest()
    {
        Processor processor = serviceabilityHelper.updatePolygon("DAILY");
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        int id = serviceabilityHelper.getPolygonCreationStatus(processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(id==0);
        String message = serviceabilityHelper.getPolygonMessageFromResponse(processor.ResponseValidator.GetBodyAsText());
        System.out.println("message = " + message);
        Assert.assertTrue(message.equalsIgnoreCase("Yay!Polygon updated successfully"));

        //todo verify in DB and Solr
        //        Assert.assertTrue(serviceabilityHelper.verifySolrEntry(polygonId));

    }

    @Test(priority = 3)
    public void getPolygonTest()
    {
        Processor processor = serviceabilityHelper.getPolygon("DAILY");
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        int id = serviceabilityHelper.getPolygonCreationStatus(processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(id==0);
        Assert.assertTrue(processor.ResponseValidator.GetBodyAsText().contains(String.valueOf(polygId)));
        //todo polygon data response verification
        //todo verify in DB and Solr
        //        Assert.assertTrue(serviceabilityHelper.verifySolrEntry(polygonId));

    }

    @Test(priority = 4)
    public void deletePolygonTest()
    {
        Processor processor = serviceabilityHelper.deletePolygon("DAILY",String.valueOf(polygId));
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        int id = serviceabilityHelper.getPolygonCreationStatus(processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(id==0);
        String message = serviceabilityHelper.getPolygonMessageFromResponse(processor.ResponseValidator.GetBodyAsText());
        System.out.println("message = " + message);

        //todo assert the right message
//        Assert.assertTrue(message.equalsIgnoreCase("Yay!Polygon updated successfully"));

        Processor processor1 = serviceabilityHelper.getPolygon("DAILY");
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor1.ResponseValidator.GetBodyAsText());
        Assert.assertEquals(processor1.ResponseValidator.GetResponseCode(), 200);
        int id2 = serviceabilityHelper.getPolygonCreationStatus(processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(id2==0);
        Assert.assertTrue(!processor1.ResponseValidator.GetBodyAsText().contains(String.valueOf(polygId)));


        //todo verify in DB and Solr
        //        Assert.assertTrue(serviceabilityHelper.verifySolrEntry(polygonId));

    }

}

