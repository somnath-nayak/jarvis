package com.swiggy.api.daily.ff.tests;

import com.swiggy.api.daily.ff.constants.DailyAssignmentServiceConstants;
import com.swiggy.api.erp.ff.constants.LosConstants;
import com.swiggy.api.erp.ff.constants.OMSConstants;
import com.swiggy.api.erp.ff.helper.AssignmentServiceHelper;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.OMSHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RabbitMQHelper;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class DailyOrderAssignmentServiceTest {
    LOSHelper losHelper = new LOSHelper();
    AssignmentServiceHelper assignmentServiceHelper = new AssignmentServiceHelper();
    DBHelper dbHelper = new DBHelper();
    Logger log = Logger.getLogger(DailyOrderAssignmentServiceTest.class);
    RabbitMQHelper rmqhelper = new RabbitMQHelper();
    OMSHelper omshelper = new OMSHelper();
    Long orderId[] = new Long[3];
    String s[] = new String[3];

    @BeforeMethod
    public void cleanUp() throws IOException, TimeoutException {

        rmqhelper.purgeQueue(LosConstants.hostName, LosConstants.manualOrder);
        omshelper.redisFlushAll(OMSConstants.REDIS);

        for (int i = 0; i < 3; i++) {

            SystemConfigProvider.getTemplate(OMSConstants.SERVICE)
                    .execute("update oms_orderexecutive set current_order_count=0,enabled=1,start_duty=1 where id=" + DailyAssignmentServiceConstants.daily_agent_id[i] + ";");

        }
    }

    @Test(groups = {"sanity", "regression"}, description = "Assignment for multiple Daily Agent OE with multiple orders ")
    public void testCreateMultipleOrdersLogInOEAndAssignmentDailyAgent() throws Exception {
        // Create 3 Daily Orders , 2 Daily Agent logged in - Assignment validations
        log.info("*************************Daily Orders Assignment to Daily Agents Test started *************************");
        for (int i = 0; i <= 1; i++) {
            //Create daily Order
            orderId[i] = Long.parseLong(losHelper.getAnOrder("daily"));
            Thread.sleep(5000);
            //Placing manual order
            assignmentServiceHelper.manualOrderRMQ(DailyAssignmentServiceConstants.daily_agent_assignment_reason_id, orderId[i], DailyAssignmentServiceConstants.daily_agent_role_id, assignmentServiceHelper.getOrderPk(orderId[i]));
            s[i] = String.valueOf(orderId[i]);
            //Checking order is present in ordersinqueue table
            Assert.assertTrue(assignmentServiceHelper.checkInOrdersInQueueTable(orderId[i], "DailyAgent"), orderId[i] + "==== is not present in OrdersInQueueTable");
        }

        for (int i = 0; i < 2; i++) {

            assignmentServiceHelper.oeLoginRMQ(DailyAssignmentServiceConstants.daily_agent_id[i], DailyAssignmentServiceConstants.daily_agent_role_id);
            Thread.sleep(6000);
            //Checking whether order is assigned to Daily Agent in mysql table
            Assert.assertTrue(dbHelper.pollDB(OMSConstants.ASSIGNMENT_DB, "SELECT order_id FROM db_assignment.assignment where order_id = "
                            + orderId[i] + " and oe_id = " + DailyAssignmentServiceConstants.daily_agent_id[i] + ";", "order_id", s[i], 5, 70), +orderId[i] + "==Orderid is not found in DB");

        }
        log.info("*************************Daily Orders Assignment to Daily Agents Test Completed *************************");
    }
}
