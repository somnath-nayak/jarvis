package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class DistanceDetails {
    
    @JsonProperty("type")
    private String type;
    @JsonProperty("lastMileDistanceInKm")
    private Double lastMileDistanceInKm;
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public Double getLastMileDistanceInKm() {
        return lastMileDistanceInKm;
    }
    
    public void setLastMileDistanceInKm(Double lastMileDistanceInKm) {
        this.lastMileDistanceInKm = lastMileDistanceInKm;
    }
    
    
    public DistanceDetails withType(String type){
        setType(type);
        return this;
    }
    public DistanceDetails withLastMileDistanceInKM(Double lastMileDistanceInKm){
        setLastMileDistanceInKm(lastMileDistanceInKm);
        return this;
    }
    
    public DistanceDetails withGivenValues(String type, Double lastMileDistanceInKm){
        setType(type); setLastMileDistanceInKm(lastMileDistanceInKm);
        return this;
    }
    
    public DistanceDetails listOfDistanceDetailsWithGivenValues(String type, Double lastMileDistanceInKm){
        
       
    
        withGivenValues(type,lastMileDistanceInKm);
        return this;
    }
}
