
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "customer_id",
    "orders",
    "payment_info",
    "created_at",
    "updated_at"
})
public class SwapAddonMealPojo {
    RandomNumber rm = new RandomNumber(100001, 1000000);

    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("orders")
    private List<Order> orders = null;
    @JsonProperty("payment_info")
    private Object paymentInfo;
    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public SwapAddonMealPojo withCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    @JsonProperty("orders")
    public List<Order> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public SwapAddonMealPojo withOrders(List<Order> orders) {
        this.orders = orders;
        return this;
    }

    @JsonProperty("payment_info")
    public Object getPaymentInfo() {
        return paymentInfo;
    }

    @JsonProperty("payment_info")
    public void setPaymentInfo(Object paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public SwapAddonMealPojo withPaymentInfo(Object paymentInfo) {
        this.paymentInfo = paymentInfo;
        return this;
    }

    @JsonProperty("created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public SwapAddonMealPojo withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public Long getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public SwapAddonMealPojo withUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public SwapAddonMealPojo setDefaultData(String action) throws ParseException {
        return this.withCustomerId(String.valueOf(rm.nextInt()))
                .withOrders(new ArrayList<Order>() {{add(new Order().setDefaultData(action));}})
                .withPaymentInfo(null)
                .withCreatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()))
                .withUpdatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()));
    }

}
