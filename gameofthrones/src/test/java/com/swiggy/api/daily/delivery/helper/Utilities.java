package com.swiggy.api.daily.delivery.helper;

import com.swiggy.api.daily.delivery.pojo.DailyListingRequest;
import com.swiggy.api.daily.delivery.pojo.DailySubSlot;
import com.swiggy.api.daily.delivery.pojo.EpochSlot;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Utilities extends DailyConstants{

    public static DailySubSlot convertEpochSlotToDailySubSlot(EpochSlot timeslots) {
        SimpleDateFormat hhmmDateFormater = new SimpleDateFormat(HHMM_DATE_FORMAT);
        SimpleDateFormat dayExtractor = new SimpleDateFormat(DAY_FORMAT_EXTRACTOR);

        Date startDate = new Date(timeslots.getStartTime());
        Date endDate = new Date(timeslots.getEndTime());

        String start = hhmmDateFormater.format(startDate);
        String end = hhmmDateFormater.format(endDate);

        DailySubSlot dailySubSlot = new DailySubSlot();

        dailySubSlot.setStartTime(Long.parseLong(start.substring(HH_START_OFFSET, HH_END_OFFSET) + start.substring(MM_START_OFFSET, MM_END_OFFSET)));
        dailySubSlot.setEndTime(Long.parseLong(end.substring(HH_START_OFFSET, HH_END_OFFSET) + end.substring(MM_START_OFFSET, MM_END_OFFSET)));
        dailySubSlot.setDay(dayExtractor.format(startDate));

        LocalDateTime dateTimeStart =
                Instant.ofEpochMilli(timeslots.getStartTime()).atZone(ZoneOffset.of(IST_ZONE_OFFSET)).toLocalDateTime();
        LocalDateTime dateTimeEnd =
                Instant.ofEpochMilli(timeslots.getEndTime()).atZone(ZoneOffset.of(IST_ZONE_OFFSET)).toLocalDateTime();

        dailySubSlot.setLocalDateTimeStart(dateTimeStart);
        dailySubSlot.setLocalDateTimeEnd(dateTimeEnd);

        return dailySubSlot;
    }
    public static String convertDateToEpoch(String date, String hour, String min)
    {
        String strDate = date+" "+hour+":"+min+":00";
        Long millis = 0l;
        System.out.println("strDate = " + strDate);
        try {
            millis = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS").parse(strDate).getTime();
            System.out.println("millis = " + millis);
//            ZonedDateTime ldate = LocalDateTime.parse(strDate, DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))
//                    .atZone(ZoneId.of("Asia/Kolkata"));
//            long time = ldate.toInstant().toEpochMilli();
//            System.out.println(time);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return String.valueOf(millis);
    }
    public static DailyListingRequest convertEpochTimeToMillis(DailyListingRequest dailyListingRequest) {
        dailyListingRequest.getTimeSlot().setStartTime(dailyListingRequest.getTimeSlot().getStartTime() * 1000);
        dailyListingRequest.getTimeSlot().setEndTime(dailyListingRequest.getTimeSlot().getEndTime() * 1000);
        return dailyListingRequest;
    }
    public static EpochSlot convertEpochTimeToMillis(EpochSlot epoch) {
        epoch.setStartTime(epoch.getStartTime() * 1000);
        epoch.setEndTime(epoch.getEndTime()* 1000);
        return epoch;
    }
    public static DailySubSlot incrementTimeSlotByXDays(DailySubSlot dailySubSlots, Integer days) {
        SimpleDateFormat dayExtractor = new SimpleDateFormat(DAY_FORMAT_EXTRACTOR);
        DailySubSlot newDailySubslot = new DailySubSlot();
        newDailySubslot.setLocalDateTimeStart((dailySubSlots.getLocalDateTimeStart().plusDays(days)));
        newDailySubslot.setLocalDateTimeEnd((dailySubSlots.getLocalDateTimeEnd().plusDays(days)));
        newDailySubslot.setDay(dayExtractor.format(Date.from(newDailySubslot.getLocalDateTimeStart().toInstant(ZoneOffset.of(IST_ZONE_OFFSET)))));
        newDailySubslot.setStartTime(dailySubSlots.getStartTime());
        newDailySubslot.setEndTime(dailySubSlots.getEndTime());
        return newDailySubslot;
    }

}
