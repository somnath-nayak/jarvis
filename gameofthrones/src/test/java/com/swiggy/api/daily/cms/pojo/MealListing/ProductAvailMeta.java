package com.swiggy.api.daily.cms.pojo.MealListing;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id",
        "product_ids"
})
public class ProductAvailMeta {

    @JsonProperty("store_id")
    private Integer store_id;
    @JsonProperty("product_ids")
    private List<String> product_ids = null;

    @JsonProperty("store_id")
    public Integer getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    public ProductAvailMeta withStore_id(Integer store_id) {
        this.store_id = store_id;
        return this;
    }

    @JsonProperty("product_ids")
    public List<String> getProduct_ids() {
        return product_ids;
    }

    @JsonProperty("product_ids")
    public void setProduct_ids(List<String> product_ids) {
        this.product_ids = product_ids;
    }

    public ProductAvailMeta withProduct_ids(List<String> product_ids) {
        this.product_ids = product_ids;
        return this;
    }

    public ProductAvailMeta setData(Integer storeid, List productids) {
        this.withStore_id(storeid).withProduct_ids(productids);
        return this;
    }

}