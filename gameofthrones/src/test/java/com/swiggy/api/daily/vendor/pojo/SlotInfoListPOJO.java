package com.swiggy.api.daily.vendor.pojo;

import com.swiggy.api.daily.vendor.helper.VendorCommonHelper;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "recurrence_pattern",
        "start_time",
        "end_time",
        "live_date",
        "expiry_date",
        "is_inclusive"
})
public class SlotInfoListPOJO {

    @JsonProperty("recurrence_pattern")
    private String recurrencePattern;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("live_date")
    private String liveDate;
    @JsonProperty("expiry_date")
    private String expiryDate;
    @JsonProperty("is_inclusive")
    private Boolean isInclusive;

    VendorCommonHelper vendorCommonHelper = new VendorCommonHelper();

    /**
     * No args constructor for use in serialization
     *
     */
    public SlotInfoListPOJO() {
    }

    /**
     *
     * @param startTime
     * @param expiryDate
     * @param liveDate
     * @param endTime
     * @param recurrencePattern
     * @param isInclusive
     */
    public SlotInfoListPOJO(String recurrencePattern, String startTime, String endTime, String liveDate, String expiryDate, Boolean isInclusive) {
        super();
        this.recurrencePattern = recurrencePattern;
        this.startTime = startTime;
        this.endTime = endTime;
        this.liveDate = liveDate;
        this.expiryDate = expiryDate;
        this.isInclusive = isInclusive;
    }

    @JsonProperty("recurrence_pattern")
    public String getRecurrencePattern() {
        return recurrencePattern;
    }

    @JsonProperty("recurrence_pattern")
    public void setRecurrencePattern(String recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
    }

    public SlotInfoListPOJO withRecurrencePattern(String recurrencePattern) {
        this.recurrencePattern = recurrencePattern;
        return this;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public SlotInfoListPOJO withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public SlotInfoListPOJO withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonProperty("live_date")
    public String getLiveDate() {
        return liveDate;
    }

    @JsonProperty("live_date")
    public void setLiveDate(String liveDate) {
        this.liveDate = liveDate;
    }

    public SlotInfoListPOJO withLiveDate(String liveDate) {
        this.liveDate = liveDate;
        return this;
    }

    @JsonProperty("expiry_date")
    public String getExpiryDate() {
        return expiryDate;
    }

    @JsonProperty("expiry_date")
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public SlotInfoListPOJO withExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }

    @JsonProperty("is_inclusive")
    public Boolean getIsInclusive() {
        return isInclusive;
    }

    @JsonProperty("is_inclusive")
    public void setIsInclusive(Boolean isInclusive) {
        this.isInclusive = isInclusive;
    }

    public SlotInfoListPOJO withIsInclusive(Boolean isInclusive) {
        this.isInclusive = isInclusive;
        return this;
    }

    public SlotInfoListPOJO setDefault(Boolean isInclusive, String liveDate, String expiryDate, String startTime, String endTime) {
        this
        .withStartTime(startTime)
        .withEndTime(endTime)
        .withLiveDate(liveDate)
        .withExpiryDate(expiryDate)
        .withIsInclusive(isInclusive);
        return this;
    }

}
