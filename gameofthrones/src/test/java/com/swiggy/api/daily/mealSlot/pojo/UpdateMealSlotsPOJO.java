package com.swiggy.api.daily.mealSlot.pojo;

import java.util.HashMap;
import java.util.Map;

import com.swiggy.api.daily.mealSlot.helper.MealSlotConstant;
import com.swiggy.automation.common.utils.TimeUtils;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "city_id",
        "slot",
        "start_time",
        "end_time",
        "created_by"
})
public class UpdateMealSlotsPOJO {

    @JsonProperty("id")
    private String id;
    @JsonProperty("city_id")
    private String cityId;
    @JsonProperty("slot")
    private String slot;
    @JsonProperty("start_time")
    private String startTime;
    @JsonProperty("end_time")
    private String endTime;
    @JsonProperty("created_by")
    private String createdBy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdateMealSlotsPOJO() {
    }

    /**
     *
     * @param startTime
     * @param id
     * @param createdBy
     * @param cityId
     * @param endTime
     * @param slot
     */
    public UpdateMealSlotsPOJO(String id, String cityId, String slot, String startTime, String endTime, String createdBy) {
        super();
        this.id = id;
        this.cityId = cityId;
        this.slot = slot;
        this.startTime = startTime;
        this.endTime = endTime;
        this.createdBy = createdBy;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public UpdateMealSlotsPOJO withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("city_id")
    public String getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public UpdateMealSlotsPOJO withCityId(String cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("slot")
    public String getSlot() {
        return slot;
    }

    @JsonProperty("slot")
    public void setSlot(String slot) {
        this.slot = slot;
    }

    public UpdateMealSlotsPOJO withSlot(String slot) {
        this.slot = slot;
        return this;
    }

    @JsonProperty("start_time")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public UpdateMealSlotsPOJO withStartTime(String startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public String getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public UpdateMealSlotsPOJO withEndTime(String endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonProperty("created_by")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("created_by")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UpdateMealSlotsPOJO withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public UpdateMealSlotsPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public UpdateMealSlotsPOJO setDefaultData()
    {
        setId(MealSlotConstant.id);
//        setCityId((int)(Math. random() * 50)+"");
        setCityId(MealSlotConstant.cityID);
        setSlot(MealSlotConstant.DINNER);
        setStartTime(TimeUtils.getCurrentTimeIn24HourFormat()-600+"");
        setEndTime(TimeUtils.getCurrentTimeIn24HourFormat()-300+"");
        setCreatedBy("user@swiggy.in");
        return this;
    }
}
