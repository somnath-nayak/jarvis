package com.swiggy.api.daily.delivery.helper;



import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.util.JSONParseException;
import com.redis.S;

import com.swiggy.api.daily.delivery.pojo.*;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.delivery.helper.SolrHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;

import org.json.JSONException;
import org.json.JSONObject;


import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ServiceabilityHelper extends DailyConstants{

    DeliveryHelperMethods dhm=new DeliveryHelperMethods();
    SolrHelper solrHelper = new SolrHelper();
    RedisHelper redisHelper = new RedisHelper();
    DailySubSlot dailySubSlot = new DailySubSlot();
    static Initialize gameofthrones = new Initialize();


    public Processor createPolygon(String polygonTag)
    {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "createPolygon", gameofthrones);
        HashMap<String,String> headers= dhm.doubleheader();
        String name = "polygon";
        String lat_lng = "12.919176658002105 77.64754737567137,12.915746656192992 77.6567741746826,12.905728271581637 77.65985335063169,12.899307091642587 77.65194619845579,12.899286175477677 77.63804162692259,12.909419852505232 77.63012374591062,12.913655168564498 77.63568128298948,12.919176658002105 77.64754737567137";
        //String lat_lng = "1.325123 103.924639,1.326496 103.936913,1.316113 103.932020,1.304357 103.912794,1.325123 103.924639";
        String cityId = "1";
        String maxSla = "100";
        String lastMile = "6.0";
        String[] payload = {name, lat_lng, polygonTag, cityId, maxSla, lastMile};
        Processor processor = new Processor(service, headers, payload);
        return processor;
    }

    public Processor updatePolygon(String polygonTag)
    {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "updatePolygon", gameofthrones);
//        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), null, new String[] {polygonTag});
        HashMap<String,String> headers= dhm.doubleheader();
        String name = "polygon-updated";
        String lat_lng = "12.919176658002105 77.64754737567137,12.915746656192992 77.6567741746826,12.905728271581637 77.65985335063169,12.899307091642587 77.65194619845579,12.899286175477677 77.63804162692259,12.909419852505232 77.63012374591062,12.913655168564498 77.63568128298948,12.919176658002105 77.64754737567137";
        String cityId = "1";
        String maxSla = "100";
        String lastMile = "6.0";
        String[] payload = {name, lat_lng, polygonTag, cityId, maxSla, lastMile};
        Processor processor = new Processor(service, headers, payload,new String[] {polygonTag});
        return processor;
    }

    public Processor getPolygon(String polygonTag)
    {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "updatePolygon", gameofthrones);
//        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), null, new String[] {polygonTag});
        HashMap<String,String> headers= dhm.doubleheader();
        String name = "polygon-updated";
        String lat_lng = "12.919176658002105 77.64754737567137,12.915746656192992 77.6567741746826,12.905728271581637 77.65985335063169,12.899307091642587 77.65194619845579,12.899286175477677 77.63804162692259,12.909419852505232 77.63012374591062,12.913655168564498 77.63568128298948,12.919176658002105 77.64754737567137";
        String cityId = "1";
        String maxSla = "100";
        String lastMile = "6.0";
        String[] payload = {name, lat_lng, polygonTag, cityId, maxSla, lastMile};
        Processor processor = new Processor(service, headers, payload,new String[] {polygonTag});
        return processor;
    }

    public Processor deletePolygon(String polygonTag, String polygonId)
    {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "updatePolygon", gameofthrones);
//        Processor processor = new Processor(gameOfThronesService, Serviceability_Constant.singleHeader(), null, new String[] {polygonTag});
        HashMap<String,String> headers= dhm.doubleheader();
        String name = "polygon-updated";
        String lat_lng = "12.919176658002105 77.64754737567137,12.915746656192992 77.6567741746826,12.905728271581637 77.65985335063169,12.899307091642587 77.65194619845579,12.899286175477677 77.63804162692259,12.909419852505232 77.63012374591062,12.913655168564498 77.63568128298948,12.919176658002105 77.64754737567137";
        String cityId = "1";
        String maxSla = "100";
        String lastMile = "6.0";
        String[] payload = {name, lat_lng, polygonTag, cityId, maxSla, lastMile};
        Processor processor = new Processor(service, headers, payload,new String[] {polygonId});
        return processor;
    }


    public Processor fetchCapacity(String lat, String lang, String uniqueId, String startTime, String endTime)
    {
        Processor processor = null;
        try {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "fetchCapacity", gameofthrones);
        HashMap<String,String> headers= dhm.doubleheader();
        CapacityFetchRequest capacityFetchRequest = new CapacityFetchRequest();
        List<EpochSlot> epochSlotList = new ArrayList<EpochSlot>();
        EpochSlot epochSlot = new EpochSlot();
        GeoLocation geoLocation = new GeoLocation();
        epochSlot.setStartTime(Long.valueOf(startTime));
        epochSlot.setEndTime(Long.valueOf(endTime));
        geoLocation.setLat(lat);
        geoLocation.setLng(lang);
        epochSlotList.add(epochSlot);
        capacityFetchRequest.setCustomerLocation(geoLocation);
        capacityFetchRequest.setDeliverySlot(epochSlotList);
        capacityFetchRequest.setUniqueId(uniqueId);
        String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(capacityFetchRequest);
        processor =new Processor(service, headers,payload,0);
        return processor;
        }catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor incrementCapacity(String lat, String lang, String uniqueId, String startTime, String endTime)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliverycerebro", "consumeCapacity", gameofthrones);
            HashMap<String,String> headers= dhm.doubleheader();
            CapacityFetchRequest capacityFetchRequest = new CapacityFetchRequest();
            List<EpochSlot> epochSlotList = new ArrayList<EpochSlot>();
            EpochSlot epochSlot = new EpochSlot();
            GeoLocation geoLocation = new GeoLocation();
            epochSlot.setStartTime(Long.valueOf(startTime));
            epochSlot.setEndTime(Long.valueOf(endTime));
            geoLocation.setLat(lat);
            geoLocation.setLng(lang);
            epochSlotList.add(epochSlot);
            capacityFetchRequest.setCustomerLocation(geoLocation);
            capacityFetchRequest.setDeliverySlot(epochSlotList);
            capacityFetchRequest.setUniqueId(uniqueId);
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(capacityFetchRequest);
            processor =new Processor(service, headers,payload,0);
        return processor;
    }catch (JsonProcessingException e)
    {
        e.printStackTrace();
    }
        return processor;
    }

    public Processor decrementCapacity(String lat, String lang, String uniqueId, String startTime, String endTime)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliverycerebro", "releaseCapacity", gameofthrones);
            HashMap<String,String> headers= dhm.doubleheader();
            CapacityFetchRequest capacityFetchRequest = new CapacityFetchRequest();
            List<EpochSlot> epochSlotList = new ArrayList<EpochSlot>();
            EpochSlot epochSlot = new EpochSlot();
            GeoLocation geoLocation = new GeoLocation();
            epochSlot.setStartTime(Long.valueOf(startTime));
            epochSlot.setEndTime(Long.valueOf(endTime));
            geoLocation.setLat(lat);
            geoLocation.setLng(lang);
            epochSlotList.add(epochSlot);
            capacityFetchRequest.setCustomerLocation(geoLocation);
            capacityFetchRequest.setDeliverySlot(epochSlotList);
            capacityFetchRequest.setUniqueId(uniqueId);
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(capacityFetchRequest);
            processor =new Processor(service, headers,payload,0);
            return processor;
        }catch (JsonProcessingException e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor dailyListing(String lat, String lon, String uniqueId, String startTime, String endTime, Long cityId, int packSize, boolean clusterDebugEnabled)
    {
        Processor processor = null;
        try {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "listingDaily", gameofthrones);
        HashMap<String,String> headers= dhm.doubleheader();
        LatLong latLong = new LatLong(Double.valueOf(lat), Double.valueOf(lon));
        EpochSlot slot = new EpochSlot();
        slot.setStartTime(Long.valueOf(startTime));
        slot.setEndTime(Long.valueOf(endTime));
        DailyListingRequest dailyListingRequest = new DailyListingRequest(latLong,cityId,slot,packSize,clusterDebugEnabled);

            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyListingRequest);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
        return processor;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor dailyListingPDP(String lat, String lon, String uniqueId, String startTime, String endTime, Long cityId, int packSize, boolean clusterDebugEnabled, String restaurantId)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliverycerebro", "listingDailyPDP", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            LatLong latLong = new LatLong(Double.valueOf(lat), Double.valueOf(lon));
            EpochSlot slot = new EpochSlot();
            slot.setStartTime(Long.valueOf(startTime));
            slot.setEndTime(Long.valueOf(endTime));
            DailyListingRequest dailyListingRequest = new DailyListingRequest(latLong, cityId, slot, packSize, clusterDebugEnabled);
            List<String> restaurantIds = new ArrayList<>();
            restaurantIds.add(restaurantId);
            dailyListingRequest.setRestaurantIds(restaurantIds);
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyListingRequest);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
            return processor;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public boolean verifyRedisEntry(String key, String value)
    {
        boolean result = false;
        try{
            result = redisHelper.sMembers("dailyredis",key,value);
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }

    public boolean verifySolrEntry(int polygonId)
    {
        boolean result = false;
        result = solrHelper.checkIfDailyPolygonCreated(polygonId);
        return result;
    }
    public int getPolygonIdFromResponse(String content) {
        int polygonId =0;

        try {

            JSONObject jObj = new JSONObject(content);
            Object aObj = jObj.get("statusMessage");
            System.out.println("Polygon id is : " + aObj.toString());
            polygonId = (Integer) aObj;
            return polygonId;
        }catch (JSONException jse)
        {
            jse.printStackTrace();

        }
        return polygonId;
    }

    public int getPolygonDataFromResponse(String content) {
        int polygonId =0;

        try {

            JSONObject jObj = new JSONObject(content);
            Object aObj = jObj.get("data");
            System.out.println("Polygon id is : " + aObj.toString());
            polygonId = (Integer) aObj;
            return polygonId;
        }catch (JSONException jse)
        {
            jse.printStackTrace();

        }
        return polygonId;
    }
    public String getPolygonMessageFromResponse(String content) {
        String message = null;

        try {

            JSONObject jObj = new JSONObject(content);
            Object aObj = jObj.get("statusMessage");
            System.out.println("Message is : " + aObj.toString());
            message = (String) aObj;
            return message;
        }catch (JSONException jse)
        {
            jse.printStackTrace();

        }
        return message;
    }

    public int getPolygonCreationStatus(String content) {
        int polygonId =0;

        try {

            JSONObject jObj = new JSONObject(content);
            Object aObj = jObj.get("statusCode");
            System.out.println("Polygon id is : " + aObj.toString());
            polygonId = (Integer) aObj;
            return polygonId;
        }catch (JSONException jse)
        {
            jse.printStackTrace();

        }
        return polygonId;
    }

    public DailySubSlot convertTime(EpochSlot slot)
    {
        return Utilities.convertEpochSlotToDailySubSlot(Utilities.convertEpochTimeToMillis(slot));

    }

    public String convertSubscriptionIdToUniqueId(String subscriptionId, DailySubSlot dailySubSlots) {
        return subscriptionId + DAILY_CAPACITY_UNIQUE_KEY_DELIMITER + dailySubSlots.getStartTime() +
                DAILY_CAPACITY_UNIQUE_KEY_DELIMITER + dailySubSlots.getEndTime() +
                DAILY_CAPACITY_UNIQUE_KEY_DELIMITER + dailySubSlots.getDay() +
                DAILY_CAPACITY_UNIQUE_KEY_DELIMITER + dailySubSlots.getLocalDateTimeStart();
    }
/*    public String createRedisKey(ZonePolygon zonePolygon, DailySubSlot dailySubSlot) {
        return zonePolygon.getZoneId() + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getStartTime() + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getEndTime() + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getLocalDateTimeStart();

    }*/

    public String createRedisKey(Long zoneId, DailySubSlot dailySubSlot) {
        return zoneId + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getStartTime() + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getEndTime() + DAILY_CAPACITY_REDIS_DELIMITER +
                dailySubSlot.getLocalDateTimeStart();

    }

    public Processor createDailySlots(Integer zone,String endTime, String startTime)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "createDailySlots", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setZone(zone);
            dailyTimeSlot.setEnabled(1);
            dailyTimeSlot.setEndTime(Long.parseLong(endTime));
            dailyTimeSlot.setStartTime(Long.parseLong(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
            return processor;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return processor;
    }
    

    public Processor updateDailySlots(Integer id,String endTime, String startTime)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "updateDailySlots", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setEndTime(Long.parseLong(endTime));
            dailyTimeSlot.setStartTime(Long.parseLong(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
            return processor;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor getDailySlots(Integer id,String endTime, String startTime)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "getDailySlots", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setEndTime(Long.parseLong(endTime));
            dailyTimeSlot.setStartTime(Long.parseLong(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,null,0);
            return processor;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public List<DailyTimeSlot> getDailySlotsAsList(Integer id,String endTime, String startTime)
    {
        Processor processor = null;
        List<DailyTimeSlot> dailyTimeSlotList = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "getDailySlots", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setEndTime(Long.parseLong(endTime));
            dailyTimeSlot.setStartTime(Long.parseLong(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
            String response = processor.ResponseValidator.GetBodyAsText();
            dailyTimeSlotList = mapper.readValue(response, new TypeReference<List<DailyTimeSlot>>(){});
            return dailyTimeSlotList;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return dailyTimeSlotList;
    }

    public void enableOnlyOneSlot(List<DailyTimeSlot> dailyTimeSlotList)
    {
        //todo enable only one slot
    }

    public void createTimeSlotsEveryhour(String startTime, String endTime,Integer zoneId)
    {
        Date now = new Date();
        String strDate = new SimpleDateFormat("dd/MM/yyyy").format(now);
        int start = Integer.parseInt(startTime);
        int end = Integer.parseInt(endTime);
        for(int i =start ; i <end; start++)
        {
            String startEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(start),"01");
            String endEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(start+1),"00");
            createDailySlots(zoneId,startEpochTime,endEpochTime).ResponseValidator.GetBodyAsText();
        }
    }


    public void createTimeSlots(String startTime, String endTime, int noOfTimeSlots, Integer zoneId)
    {
        Date now = new Date();
        String strDate = new SimpleDateFormat("dd/MM/yyyy").format(now);
        int start = Integer.parseInt(startTime);
        int end = Integer.parseInt(endTime);
        int diff = end - start;
        int totalSlots = diff/noOfTimeSlots;
        int temp = start;
        for(int i =0 ; i <totalSlots && totalSlots > 0 ; i++)
        {
            String startEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(start),"01");
            String endEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(start+1),"00");
            temp = start+1;
            createDailySlots(zoneId,startEpochTime,endEpochTime).ResponseValidator.GetBodyAsText();

        }
        String startEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(temp),"01");
        String endEpochTime = Utilities.convertDateToEpoch(strDate,String.valueOf(end),"00");
        createDailySlots(zoneId,startEpochTime,endEpochTime).ResponseValidator.GetBodyAsText();
    }

    public boolean verifyStatusFromResponse(String response, String status)
    {
        boolean result = false;
        try {
            JSONObject jObj = new JSONObject(response);
            Object code = jObj.get("statusCode");
            Integer statusCode = (Integer)code;
            System.out.println("statusCode is : "+statusCode);
            Object message = jObj.get("statusMessage");
            Integer statusMessage = (Integer)message;
            System.out.println("statusMessage is : "+statusMessage);
            if(status.equalsIgnoreCase("success"))
            {
                if(statusMessage.equals("Success") && statusCode == 0)
                {
                    result = true;
                }
            }else
            {
                if(statusMessage.equals("Failed") && statusCode == 1)
                {
                    result = true;
                }
            }
            return result;

        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return result;
    }
    
    public Processor updateDailySlotsWithEnabledFlag(Integer zone,String endTime, String startTime,Integer enabled)
    {
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "updateDailySlots", gameofthrones);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setZone(zone);
            dailyTimeSlot.setEnabled(enabled);
            dailyTimeSlot.setEndTime(Long.parseLong(endTime));
            dailyTimeSlot.setStartTime(Long.parseLong(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers,payload,0);
            return processor;
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return processor;
    }

    public String getZoneIdByLatLong(double lat, double lng)
    {
        return solrHelper.getCustomerZoneFromLatLon(lat,lng);
    }

    public String getCityIdByLatLong(double lat, double lng)
    {
        return solrHelper.getCityIdByLatLon(lat,lng);
    }
    

    public static void main(String[] args) {
        new ServiceabilityHelper().createPolygon("DAILY");
    }

}
