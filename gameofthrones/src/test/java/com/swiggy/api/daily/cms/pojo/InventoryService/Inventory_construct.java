package com.swiggy.api.daily.cms.pojo.InventoryService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "inventories",
        "attributes"
})
public class Inventory_construct {

    @JsonProperty("inventories")
    private Inventories inventories;
    @JsonProperty("attributes")
    private Attributes attributes;

    @JsonProperty("inventories")
    public Inventories getInventories() {
        return inventories;
    }

    @JsonProperty("inventories")
    public void setInventories(Inventories inventories) {
        this.inventories = inventories;
    }

    public Inventory_construct withInventories(Inventories inventories) {
        this.inventories = inventories;
        return this;
    }

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    public Inventory_construct withAttributes(Attributes attributes) {
        this.attributes = attributes;
        return this;
    }
    public Inventory_construct setData(Long startepc,Long endepoc,Long slotid,Integer maxcap,Integer soldcount){
        Attributes ab=new Attributes().setData(startepc,endepoc,slotid);
        Inventories inv=new Inventories().setData(maxcap,soldcount);
        this.withAttributes(ab).withInventories(inv);
        return this;
    }

}