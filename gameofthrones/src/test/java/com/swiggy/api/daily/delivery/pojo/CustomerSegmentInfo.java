package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

public class CustomerSegmentInfo {
    @JsonProperty("unique_id")
    Integer uniqueId;
    @JsonProperty("user_id")
    Long userId;
    @JsonProperty("value_segment")
    ValueSegmentEnum valueSegment;
    @JsonProperty("premium_segment")
    Integer premiumSegment;
    @JsonProperty("high_eng_segment")
    Integer highEngSegment;
    @JsonProperty("contact_affinity")
    Double contactAffinity;

    @JsonProperty("cancellation_probability")
    Double cancellationProbability;

    @JsonProperty("churn_probability")
    Double churnProbability;

    @JsonProperty("marketing_segment")
    MarketingSegmentEnum marketingSegment;

    @JsonProperty("new_user")
    Integer newUser;

    @JsonProperty("customer_zone_id")
    long customerZoneId;


    public Integer getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(Integer uniqueId) {
        this.uniqueId = uniqueId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public ValueSegmentEnum getValueSegment() {
        return valueSegment;
    }

    public void setValueSegment(ValueSegmentEnum valueSegment) {
        this.valueSegment = valueSegment;
    }

    public Integer getPremiumSegment() {
        return premiumSegment;
    }

    public void setPremiumSegment(Integer premiumSegment) {
        this.premiumSegment = premiumSegment;
    }

    public Integer getHighEngSegment() {
        return highEngSegment;
    }

    public void setHighEngSegment(Integer highEngSegment) {
        this.highEngSegment = highEngSegment;
    }

    public Double getContactAffinity() {
        return contactAffinity;
    }

    public void setContactAffinity(Double contactAffinity) {
        this.contactAffinity = contactAffinity;
    }

    public Double getCancellationProbability() {
        return cancellationProbability;
    }

    public void setCancellationProbability(Double cancellationProbability) {
        this.cancellationProbability = cancellationProbability;
    }

    public Double getChurnProbability() {
        return churnProbability;
    }

    public void setChurnProbability(Double churnProbability) {
        this.churnProbability = churnProbability;
    }

    public MarketingSegmentEnum getMarketingSegment() {
        return marketingSegment;
    }

    public void setMarketingSegment(MarketingSegmentEnum marketingSegment) {
        this.marketingSegment = marketingSegment;
    }

    public Integer getNewUser() {
        return newUser;
    }

    public void setNewUser(Integer newUser) {
        this.newUser = newUser;
    }

    public long getCustomerZoneId() {
        return customerZoneId;
    }

    public void setCustomerZoneId(long customerZoneId) {
        this.customerZoneId = customerZoneId;
    }


}

enum ValueSegmentEnum {
    HIGH_VALUE("H"),
    MEDIUM_VALUE("M"),
    LOW_VALUE("L");

    private String value ;

    @JsonValue
    public String getValue(){
        return value;
    }

    ValueSegmentEnum(String value){
        this.value=value;
    }

    public static ValueSegmentEnum find(String val) {
        for (ValueSegmentEnum segment : ValueSegmentEnum.values()) {
            if (segment.value.equals(val)) return segment;
        }
        return null;
    }
}

enum MarketingSegmentEnum {
    A("A"),
    B("B"),
    C("C"),
    D("D");

    private String value ;

    @JsonValue
    public String getValue(){
        return value;
    }

    MarketingSegmentEnum(String value){
        this.value=value;
    }

    public static MarketingSegmentEnum find(String val) {
        for (MarketingSegmentEnum segment : MarketingSegmentEnum.values()) {
            if (segment.value.equals(val)) return segment;
        }
        return null;
    }
}
