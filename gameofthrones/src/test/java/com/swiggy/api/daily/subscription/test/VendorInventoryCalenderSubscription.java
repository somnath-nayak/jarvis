package com.swiggy.api.daily.subscription.test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.helper.DailyPreorderHelper;
import com.swiggy.api.daily.preorder.pojo.CreatePreorederPojo;
import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.*;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
      Vendor Inventory Subscription, Get Subscription Nudge and Pause calender Subscription testcases
*/

public class VendorInventoryCalenderSubscription extends SubscriptionDp {
    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
    CreateSubscriptionPojo createSubscriptionPojo=new CreateSubscriptionPojo();
    DailyPreorderHelper preorderHelper = new DailyPreorderHelper();
    CreatePreorederPojo preorederPojo=new CreatePreorederPojo();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }

    //-------------------------------------------------------------------Vendor inventory API Positive Scenario-----------------------------------------------------------------------------------

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory with respect to store_id",groups={"Sanity_Test","Regression"})
    public void vendorInventoryTest(String payload) throws IOException, ParseException {

        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));
        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="createPreorderPositive",description ="get Vendor Inventory for pre-order",groups={"Sanity_Test","Regression"})
    public void vendorInventoryForPreorderTest(String payload) {

        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        Assert.assertTrue(status == 1, "The status is not 1");

        Long deliveryStartTime=JsonPath.parse(response).read("$.data.delivery_start_time",Long.class);
        Long deliveryEndTime=JsonPath.parse(response).read("$.data.delivery_end_time",Long.class);
        String storeId = JsonPath.read(response, "$.data.store_id");
        JSONArray slotDetailsArray = JsonPath.read(response, "$.data.entity_details.items");


        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(storeId,String.valueOf(deliveryStartTime),String.valueOf(deliveryEndTime));
        int vendorStatus=vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(vendorStatus, 1,"Vendor Inventory request is not success");

        Long actualDeliveryStartTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.start_time",Long.class);
        Long actualDeliveryEndTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.end_time",Long.class);
        Assert.assertEquals(actualDeliveryStartTime, deliveryStartTime,"Delivery Start time slot is not correct");
        Assert.assertEquals(actualDeliveryEndTime, deliveryEndTime,"Delivery End time slot is not correct");

        for(int i=0;i<slotDetailsArray.size();i++){
            String expectedItemId=JsonPath.read(response,"$.data.entity_details.items["+i+"].itemId");
            int expectedQuantity=JsonPath.read(response,"$.data.entity_details.items["+i+"].quantity");
            String actualItemId=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].item_details["+i+"].item_id");
            int actualQuantity=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].item_details["+i+"].quantity");
            Assert.assertEquals(actualItemId, expectedItemId,"ItemId is not matching with expected");
            Assert.assertEquals(actualQuantity, expectedQuantity,"Quantity is not matching with expected");
        }
    }

    @Test(dataProvider="createPreorderPositive",description ="get Vendor Inventory for 2 pre-order with same delivery slot, item id",groups={"Regression"})
    public void vendorInventoryForTwoPreorderWithSameDeliverySlotTest(String payload) throws IOException {

        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        Assert.assertTrue(status == 1, "The status is not 1");
        String storeId = JsonPath.read(response, "$.data.store_id");
        Long deliveryStartTime=JsonPath.parse(response).read("$.data.delivery_start_time",Long.class);
        Long deliveryEndTime=JsonPath.parse(response).read("$.data.delivery_end_time",Long.class);


        String secondPayload=jsonHelper.getObjectToJSON(preorederPojo.setDefaultData().withStoreId(storeId)
                .withDeliveryStartTime(deliveryStartTime).withDeliveryEndTime(deliveryEndTime));
        preorderHelper.createPreorder(secondPayload).ResponseValidator.GetBodyAsText();


        JSONArray slotDetailsArray = JsonPath.read(response, "$.data.entity_details.items");

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(storeId,String.valueOf(deliveryStartTime),String.valueOf(deliveryEndTime));
        int vendorStatus=vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(vendorStatus, 1,"Vendor Inventory request is not success");

        Long actualDeliveryStartTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.start_time",Long.class);
        Long actualDeliveryEndTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.end_time",Long.class);
        Assert.assertEquals(actualDeliveryStartTime, deliveryStartTime,"Delivery Start time slot is not correct");
        Assert.assertEquals(actualDeliveryEndTime, deliveryEndTime,"Delivery End time slot is not correct");

        for(int i=0;i<slotDetailsArray.size();i++){
            String expectedItemId=JsonPath.read(response,"$.data.entity_details.items["+i+"].itemId");
            int expectedQuantity=(JsonPath.read(response,"$.data.entity_details.items["+i+"].quantity"));
            String actualItemId=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].item_details["+i+"].item_id");
            int actualQuantity=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].item_details["+i+"].quantity");
            Assert.assertEquals(actualItemId, expectedItemId,"ItemId is not matching with expected");
            Assert.assertEquals(actualQuantity, expectedQuantity+expectedQuantity,"Quantity is not matching with expected");
        }
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory for 2 subscription with same delivery slot, item id",groups={"Regression"})
    public void vendorInventoryForTwoSubscriptionWithSameDeliverySlotTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");
        boolean WeekendService=JsonPath.read(payload,"$.schedule_details.has_weekend_service");

        String secondPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(WeekendService))
                .withDeliverySlot(new DeliverySlot().setDefaultData().withDeliveryStartTime(deliveryStartTime).withDeliveryEndTime(deliveryEndTime))
                .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData()
                        .withStoreIds(new ArrayList<Integer>() {{add(Integer.parseInt(subscriptionVendorId));}}));}}));
        subscriptionHelper.createSubscription(secondPayload);

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));
        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        JSONArray slotDetailsArray = vendorInventoryProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data[");
        Assert.assertEquals(slotDetailsArray.size(),tenure,"All slots are not present within a date range");
        for(int i=0; i<tenure;i++)
        {
            Long actualDeliveryStartTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].slot_details.start_time",Long.class);
            Long actualDeliveryEndTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].slot_details.end_time",Long.class);
            Assert.assertTrue(expectedDeliveryStartTime.contains(actualDeliveryStartTime),"Delivery StartDate is invalid");
            Assert.assertTrue(expectedDeliveryEndTime.contains(actualDeliveryEndTime),"Delivery EndDate is invalid");

            String actualItemId=vendorInventoryProcessor.ResponseValidator.GetNodeValue("$.data["+i+"].item_details[0].item_id");
            int actualQuantity=vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.data["+i+"].item_details[0].quantity");
            Assert.assertEquals(actualItemId,"4_1","Item_id is not correct");
            Assert.assertEquals(actualQuantity,2,"Quantity is not correct");
        }
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory for 1 subscription, 1 pre-order with same delivery slot",groups={"Regression"})
    public void vendorInventoryForOneSubscriptionOnePreorderWithSameDeliverySlotTest(String payload) throws IOException, ParseException {
        String vendorInventorySecondStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        Long deliveryStartTimeEpoch=DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime));
        Long deliveryEndTimeEpoch=DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryEndTime));

        String preorderPayload=jsonHelper.getObjectToJSON(preorederPojo.setDefaultData().withStoreId(subscriptionVendorId)
                .withDeliveryStartTime(deliveryStartTimeEpoch).withDeliveryEndTime(deliveryEndTimeEpoch));
        String preorderResponse=preorderHelper.createPreorder(preorderPayload).ResponseValidator.GetBodyAsText();

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,String.valueOf(deliveryStartTimeEpoch),String.valueOf(deliveryEndTimeEpoch));
        int vendorStatus=vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(vendorStatus, 1,"Vendor Inventory request is not success");

        Long actualDeliveryStartTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.start_time",Long.class);
        Long actualDeliveryEndTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].slot_details.end_time",Long.class);
        Assert.assertEquals(actualDeliveryStartTime, deliveryStartTimeEpoch,"Delivery Start time slot is not correct");
        Assert.assertEquals(actualDeliveryEndTime, deliveryEndTimeEpoch,"Delivery End time slot is not correct");

        JSONArray slotDetailsArray = JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].item_details[");
        ArrayList<String> expectedItemId=new ArrayList<>();

        expectedItemId.add(JsonPath.read(preorderResponse,"$.data.entity_details.items[0].itemId"));
        expectedItemId.add("4_1");
        for(int i=0;i<slotDetailsArray.size();i++){
            String actualItemId=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data[0].item_details["+i+"].item_id");
            Assert.assertTrue(expectedItemId.contains(actualItemId),"ItemId is not present for vendor inventory");
        }
        if(subscriptionType.equals("WEEKDAY")){
            vendorInventorySecondStartDate=DateUtility.getNextWorkingDateInReadableFormat(subscriptionStartDate);
        }else{
            vendorInventorySecondStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
        }

        Long vendorInventorySecondStartDateEpoch=DateUtility.convertReadableDateTimeToInMilisecond(vendorInventorySecondStartDate,String.valueOf(deliveryStartTime));
        Long vendorInventorySecondEndDateEpoch=DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime));

        Processor vendorInventorySecondProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,String.valueOf(vendorInventorySecondStartDateEpoch),String.valueOf(vendorInventorySecondEndDateEpoch));
        int vendorSecondStatus=vendorInventorySecondProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(vendorSecondStatus, 1,"Vendor Inventory request is not success");

        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(vendorInventorySecondStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(vendorInventorySecondStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventorySecondProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure-1);
        subscriptionTestHelper.validateItemSlot(vendorInventorySecondProcessor,tenure-1,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory for 1 subscription, 1 preorder with different delivery slot with respect to store_id",groups={"Sanity_Test","Regression"})
    public void vendorInventoryForOneSubscriptionOnePreorderWithDifferentDeliverySlotTest(String payload) throws IOException, ParseException {

        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

        Long deliveryStartTimeEpoch=DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,DateUtility.getFutureTimeInReadableFormat(2,17));
        Long deliveryEndTimeEpoch=DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime));

        String secondPayload=jsonHelper.getObjectToJSON(preorederPojo.setDefaultData().withStoreId(subscriptionVendorId)
                .withDeliveryStartTime(deliveryStartTimeEpoch).withDeliveryEndTime(deliveryEndTimeEpoch));
        preorderHelper.createPreorder(secondPayload).ResponseValidator.GetBodyAsText();

        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));
        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        expectedDeliveryStartTime.add(deliveryStartTimeEpoch);
        expectedDeliveryEndTime.add(deliveryEndTimeEpoch);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure+1);
    }


    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after skipping first meal with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterSkippingFirstMealTest(String payload) throws IOException, ParseException {
        String vendorInventoryStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        String endDateAfterSkip = skipProcessor.ResponseValidator.GetNodeValue("$.data.endDate");

        if(subscriptionType.equals("WEEKDAY")){
            vendorInventoryStartDate=DateUtility.getNextWorkingDateInReadableFormat(subscriptionStartDate);
        }else{
            vendorInventoryStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
        }

        String startDateInEpochAfterSkip=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpochAfterSkip=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessorAfterSkip=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpochAfterSkip,endDateInEpochAfterSkip);
        List<Long> expectedDeliveryStartTimeAfterSkip = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTimeAfterSkip = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessorAfterSkip,expectedDeliveryStartTimeAfterSkip,expectedDeliveryEndTimeAfterSkip,tenure-1);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessorAfterSkip,tenure-1,itemId,1);


        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(vendorInventoryStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(endDateAfterSkip,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, endDateAfterSkip,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, endDateAfterSkip,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after pausing for 6 days with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterPausingTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,5);

        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

        String resumeDate=pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.resumeDate");
        String endDateAfterPause=pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.endDate");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(resumeDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(endDateAfterPause,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(resumeDate, endDateAfterPause,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(resumeDate, endDateAfterPause,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after skip, pausing for 6 days with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterSkipPausingTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,5);

        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

        String resumeDate=pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.resumeDate");
        String endDateAfterPause=pauseSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.endDate");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(resumeDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(endDateAfterPause,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(resumeDate, endDateAfterPause,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(resumeDate, endDateAfterPause,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after skip, pause, resume subscription with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterSkipPauseResumeSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,5);

        subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
        subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Positive", subscriptionType, subscriptionEndDate, 0, "success");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate( subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }


    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after pause, resume subscription with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterPauseResumeSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,5);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
        subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Positive", subscriptionType, subscriptionEndDate, 0, "success");

        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate( subscriptionStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory after pause, resume, skip subscription with respect to store_id",groups={"Regression"})
    public void getVendorInventoryAfterPauseResumeSkipSubscriptionTest(String payload) throws IOException, ParseException {
        String vendorInventoryStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,5);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(responseSubscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(responseSubscriptionId);
        subscriptionTestHelper.validateResumeResponse(resumeSubscriptionProcessor, "Positive", subscriptionType, subscriptionEndDate, 0, "success");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(responseSubscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        subscriptionTestHelper.validateSkipResponse(skipProcessor, "Positive", subscriptionType, subscriptionEndDate, SubscriptionConstant.SUBSCRIPTION_SUCCESS_MSG);
        String endDateAfterSkip = skipProcessor.ResponseValidator.GetNodeValue("$.data.endDate");

        if(subscriptionType.equals("WEEKDAY")){
            vendorInventoryStartDate=DateUtility.getNextWorkingDateInReadableFormat(subscriptionStartDate);
        }else{
            vendorInventoryStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
        }

        String startDateInEpochAfterSkip=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpochAfterSkip=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessorAfterSkip=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpochAfterSkip,endDateInEpochAfterSkip);
        List<Long> expectedDeliveryStartTimeAfterSkip = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, subscriptionEndDate,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTimeAfterSkip = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, subscriptionEndDate,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessorAfterSkip,expectedDeliveryStartTimeAfterSkip,expectedDeliveryEndTimeAfterSkip,tenure-1);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessorAfterSkip,tenure-1,itemId,1);


        String startDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(vendorInventoryStartDate,String.valueOf(deliveryStartTime)));
        String endDateInEpoch=String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(endDateAfterSkip,String.valueOf(deliveryEndTime)));

        Processor vendorInventoryProcessor=subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId,startDateInEpoch,endDateInEpoch);
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, endDateAfterSkip,String.valueOf(deliveryStartTime),subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(vendorInventoryStartDate, endDateAfterSkip,String.valueOf(deliveryEndTime),subscriptionType);

        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor,expectedDeliveryStartTime,expectedDeliveryEndTime,tenure);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor,tenure,itemId,1);
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory with past start date, future end date with respect to store_id",groups={"Regression"})
    public void getVendorInventoryWithPastStartDateFutureEndDateTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
            int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
            int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
            int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

            subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(subscriptionStartDate, 3));

            String startDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate, String.valueOf(deliveryStartTime)));
            String endDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate, String.valueOf(deliveryEndTime)));

            Processor vendorInventoryProcessor = subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId, startDateInEpoch, endDateInEpoch);
            List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryStartTime), subscriptionType);
            List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryEndTime), subscriptionType);

            subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor, expectedDeliveryStartTime, expectedDeliveryEndTime, tenure);
            subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor, tenure, itemId, 1);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory with past start date, past end date with respect to store_id",groups={"Regression"})
    public void getVendorInventoryWithPastStartDatePastEndDateTest(String payload) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
            String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
            int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
            int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
            int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
            String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

            subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(subscriptionEndDate, 3));

            String startDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate, String.valueOf(deliveryStartTime)));
            String endDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate, String.valueOf(deliveryEndTime)));

            Processor vendorInventoryProcessor = subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId, startDateInEpoch, endDateInEpoch);
            List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryStartTime), subscriptionType);
            List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryEndTime), subscriptionType);

            subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor, expectedDeliveryStartTime, expectedDeliveryEndTime, tenure);
            subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor, tenure, itemId, 1);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory with less delivery end time for end date of subscription with respect to store_id",groups={"Regression"})
    public void getVendorInventoryWithLessDeliverySlotForEndDateSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
        int deliveryEndTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
        int tenure = processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String itemId = processor.ResponseValidator.GetNodeValue("$.data.entityDetails.id");

        String startDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate, String.valueOf(deliveryStartTime)));
        String endDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate, String.valueOf(deliveryStartTime)));
        List<Long> expectedDeliveryStartTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryStartTime), subscriptionType);
        List<Long> expectedDeliveryEndTime = DateUtility.getEpochBetweenDate(subscriptionStartDate, subscriptionEndDate, String.valueOf(deliveryEndTime), subscriptionType);

        Processor vendorInventoryProcessor = subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId, startDateInEpoch, endDateInEpoch);
        subscriptionTestHelper.validateDeliverySlot(vendorInventoryProcessor, expectedDeliveryStartTime, expectedDeliveryEndTime, tenure-1);
        subscriptionTestHelper.validateItemSlot(vendorInventoryProcessor, tenure-1, itemId, 1);
    }

    //-------------------------------------------------------------------Vendor inventory API Negative Scenario-----------------------------------------------------------------------------------

    @Test(dataProvider="vendorInventoryDP",description ="get Vendor Inventory where delivery start date is greater than delivery end date with respect to store_id, Negative Scenario",groups={"Regression"})
    public void getVendorInventoryWithStartDateGreaterThanEndDateTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");

        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionVendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        int deliveryStartTime = processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");

        String startDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate, String.valueOf(deliveryStartTime)));
        String endDateInEpoch = String.valueOf(DateUtility.convertReadableDateTimeToInMilisecond(subscriptionEndDate, String.valueOf(deliveryStartTime)));

        Processor vendorInventoryProcessor = subscriptionHelper.getVendorInventoryDetails(subscriptionVendorId, endDateInEpoch, startDateInEpoch);
        int pauseStatusCode = vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String pauseStatusMessage = vendorInventoryProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(pauseStatusCode, 0, "Status code is not 0");
        Assert.assertEquals(pauseStatusMessage, "start date is greater than end date", "Able to get Vendor Inventory with start date is greater than end date");
    }

    //------------------------------------------------------------Get Subscription Nudge----------------------------------------------------------------------

    @Test(description ="Get Subscription Nudge plan details with respect to userId which plan already taken by user",groups={"Sanity_Test","Regression"})
    public void getSubscriptionNudgePlanAlreadyTakenByUser() {
        Processor subscriptionNudgeProcessor=subscriptionHelper.getSubscriptionNudgeDetails("1","1234");
        int status=subscriptionNudgeProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status,1,"Status Code is not 1");
        boolean responseData=subscriptionNudgeProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        Assert.assertEquals(responseData,true,"Response data is not true");
    }

    @Test(description ="Get Subscription Nudge plan details with respect to userId which plan not taken by user",groups={"Sanity_Test","Regression"})
    public void getSubscriptionNudgePlanNotTakenByUser() {
        Processor subscriptionNudgeProcessor=subscriptionHelper.getSubscriptionNudgeDetails("ABC","1234");
        int status=subscriptionNudgeProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(status,1,"Status Code is not 1");
        boolean responseData=subscriptionNudgeProcessor.ResponseValidator.GetNodeValueAsBool("$.data");
        Assert.assertEquals(responseData,false,"Response data is not true");
    }

    //--------------------------------------------------------Pause Calender Positive Scenario--------------------------------------------------------------

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause calender from pause from to pause till date of subscription for Precutoff, where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getPauseCalenderDetailsPrecutoffSubscriptionTest(String payload) throws IOException, ParseException {
        String expectedPauseMaxDate="";
        String pauseTillStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
        Assert.assertEquals(pauseAvailableStatus,true);

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                ||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday"))) {
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 8);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else if(subscriptionType.equals("WEEKDAY")&&DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")){
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else{
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }
        Long actualPauseStartDate=JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date",Long.class);
        String actualPauseStartDateInReadableFormat=DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
        Assert.assertEquals(actualPauseStartDateInReadableFormat,subscriptionStartDate);
        subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor,subscriptionType,pauseTillStartDate,expectedPauseMaxDate);
    }


    @Test(description ="Validate pause Calender on day=friday for precutoff time, Weekend service= false",groups={"Regression"})
    public void pauseCalenderForFridayOnWeekendServiceFalsePreCutoffTest() throws IOException, ParseException {
        String expectedPauseMaxDate="";
        String pauseTillStartDate="";
        String payload=jsonHelper.getObjectToJSON(createSubscriptionPojo.setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData()
                .withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")).withHasWeekendService(false)));
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
        Assert.assertEquals(pauseAvailableStatus,true);

        if(subscriptionType.equals("WEEKDAY")&&DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")){
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }
        Long actualPauseStartDate=JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date",Long.class);
        String actualPauseStartDateInReadableFormat=DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
        Assert.assertEquals(actualPauseStartDateInReadableFormat,subscriptionStartDate);
        subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor,subscriptionType,pauseTillStartDate,expectedPauseMaxDate);
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause calender from pause from to pause till date after few days of subscription for Precutoff, where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getPauseCalenderDetailsAfterFewDaysPrecutoffSubscriptionTest(String payload) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            String workingDayAfterThreeDay="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            if(subscriptionType.equals("WEEKDAY")){
                workingDayAfterThreeDay=DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate,3);
            }else{
                workingDayAfterThreeDay=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
            }

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + workingDayAfterThreeDay));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormat()));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            Assert.assertEquals(pauseAvailableStatus, true);

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Monday") || DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Tuesday")
                    || DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Wednesday") || DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Thursday"))) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 1);
            } else if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Friday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, workingDayAfterThreeDay);
            subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor, subscriptionType, pauseTillStartDate, expectedPauseMaxDate);
            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus,true);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,workingDayAfterThreeDay);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }


    @Test(dataProvider="getPauseCalenderDetailsPostcutoffSubscriptionDP",description ="Validate pause calender from pause from to pause till date of subscription for PostCutoff, where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getPauseCalenderDetailsPostcutoffSubscriptionTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            String expectedPauseFromDate = "";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            Assert.assertEquals(pauseAvailableStatus,true);
            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
            } else {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);;
            }

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday") || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                    || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, expectedPauseFromDate);
            subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor, subscriptionType, pauseTillStartDate, expectedPauseMaxDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="pauseCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffDP",description ="Validate pause Calender on day=thursday, friday for postcutoff time, Weekend service= false",groups={"Regression"})
    public void pauseCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            String expectedPauseFromDate = "";

            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + subscriptionStartDate));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            Assert.assertEquals(pauseAvailableStatus,true);
            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
            } else {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            }

            if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            }else if(subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, expectedPauseFromDate);
            if(subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))){
                pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 4 );
            }
            subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor, subscriptionType, pauseTillStartDate, expectedPauseMaxDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="getPauseCalenderDetailsPostcutoffSubscriptionDP",description ="Validate pause calender from pause from to pause till date after few days of subscription for Precutoff, where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getPauseCalenderDetailsAfterFewDaysPostcutoffSubscriptionTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            String workingDayAfterThreeDay="";
            String expectedPauseFromDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            if(subscriptionType.equals("WEEKDAY")){
                workingDayAfterThreeDay=DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate,3);
            }else{
                workingDayAfterThreeDay=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
            }

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + workingDayAfterThreeDay));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            Assert.assertEquals(pauseAvailableStatus, true);

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Friday"))) {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 3);
            } else {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(workingDayAfterThreeDay, 1);;
            }

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Monday") || DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Tuesday")
                    || DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Friday"))) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(workingDayAfterThreeDay).equals("Thursday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, expectedPauseFromDate);


            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus,true);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,expectedPauseFromDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate skip date in pause calender for precutoff time  where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getSkipDateInPauseCalenderPrecutoffSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);

        boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
        Assert.assertEquals(skipAvailableStatus,true);
        subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,subscriptionStartDate);
    }

    @Test(dataProvider="getPauseCalenderDetailsPostcutoffSubscriptionDP",description ="Validate skip date in pause calender for postcutoff time where weekend service= true, false",groups={"Sanity_Test","Regression"})
    public void getSkipDateInPauseCalenderPostcutoffSubscriptionTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))){
                subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
            }else{
                subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
            }
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus,true);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor, subscriptionType, subscriptionStartDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="pauseCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffDP",description ="Validate skip Calender on day=thursday, friday for postcutoff time, Weekend service= false",groups={"Regression"})
    public void skipCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))){
                subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
            }else{
                subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
            }
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus,true);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor, subscriptionType, subscriptionStartDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate skip date in pause calender after skipping first day meal for precutoff time  where weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateAfterSkippingFirstDayMealPrecutoffTest(String payload) throws IOException, ParseException {
        String expectedPauseMaxDate="";
        String pauseTillStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(subscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(skipStatusCode, 1);

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))){
            subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,3);
        }else{
            subscriptionStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,1);
        }
        boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
        Assert.assertEquals(skipAvailableStatus,false);
        subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,subscriptionStartDate);

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                ||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday"))) {
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 8);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else if(subscriptionType.equals("WEEKDAY")&&DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")){
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else{
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }
        Long actualPauseStartDate=JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date",Long.class);
        String actualPauseStartDateInReadableFormat=DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
        Assert.assertEquals(actualPauseStartDateInReadableFormat,subscriptionStartDate);
        subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor,subscriptionType,pauseTillStartDate,expectedPauseMaxDate);
    }

    @Test(dataProvider="getPauseCalenderDetailsPostcutoffSubscriptionDP",description ="Validate skip date in pause calender after skipping second day meal in postcutoff time  where weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateAfterSkippingSecondDayMealPostcutoffTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(subscriptionId);
            int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(subscriptionDetailsStatusCode, 1);
            String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

            Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
            int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(skipStatusCode, 1);

            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                subscriptionStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
            } else {
                subscriptionStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            }
            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                subscriptionStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
            } else {
                subscriptionStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            }
            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus, false);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor, subscriptionType, subscriptionStartDate);

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday") || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                    || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday") || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday"))) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            } else if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, subscriptionStartDate);
            subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor, subscriptionType, pauseTillStartDate, expectedPauseMaxDate);
        } finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }


    @Test(dataProvider="createSubscriptionDP",description ="Validate pause, skip calender date in pause calender after pausing, resume the subscription on same day for precutoff time  where weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateAfterPauseResumeOnSameDayPrecutoffTest(String payload) throws IOException, ParseException {
        String expectedPauseMaxDate="";
        String pauseTillStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(subscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseStatusCode, 1);

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(subscriptionId);
        int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(resumeStatusCode, 1, "Status code is not 1");


        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
        Assert.assertEquals(pauseAvailableStatus,true);

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                ||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday"))) {
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 8);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else if(subscriptionType.equals("WEEKDAY")&&DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")){
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else{
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }
        Long actualPauseStartDate=JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date",Long.class);
        String actualPauseStartDateInReadableFormat=DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
        Assert.assertEquals(actualPauseStartDateInReadableFormat,subscriptionStartDate);
        subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor,subscriptionType,pauseTillStartDate,expectedPauseMaxDate);

        boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
        Assert.assertEquals(skipAvailableStatus,true);
        subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,subscriptionStartDate);
    }

    @Test(dataProvider="getPauseCalenderDetailsPostcutoffSubscriptionDP",description ="Validate pause, skip calender date in pause calender after pausing, resume the subscription on same day for postcutoff time  where weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateAfterPauseResumeOnSameDayPostcutoffTest(String payload,String serverTime) throws IOException, ParseException {
        try {
            String expectedPauseMaxDate = "";
            String pauseTillStartDate = "";
            String expectedPauseFromDate="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            String pauseEndDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
            Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(subscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
            int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseStatusCode, 1);

            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + serverTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(subscriptionId);
            int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(resumeStatusCode, 1, "Status code is not 1");


            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            Assert.assertEquals(pauseAvailableStatus, true);

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 3);
            } else {
                expectedPauseFromDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);;
            }

            if (subscriptionType.equals("WEEKDAY") && (DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday") || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                    || DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday"))) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 8);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else if (subscriptionType.equals("WEEKDAY") && DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday")) {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 10);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            } else {
                expectedPauseMaxDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 6);
                pauseTillStartDate = DateUtility.getDateInReadableFormat(expectedPauseFromDate, 1);
            }
            Long actualPauseStartDate = JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date", Long.class);
            String actualPauseStartDateInReadableFormat = DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
            Assert.assertEquals(actualPauseStartDateInReadableFormat, expectedPauseFromDate);
            subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor, subscriptionType, pauseTillStartDate, expectedPauseMaxDate);

            boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            Assert.assertEquals(skipAvailableStatus,true);
            subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor, subscriptionType, expectedPauseFromDate);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause, skip calender date in pause calender after skip, pause, resume the subscription on same day for precutoff time  where weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateAfterSkipPauseResumeOnSameDayPrecutoffTest(String payload) throws IOException, ParseException {
        String expectedPauseMaxDate="";
        String pauseTillStartDate="";
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(subscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(skipStatusCode, 1);

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(subscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseStatusCode, 1);

        Processor resumeSubscriptionProcessor = subscriptionHelper.resumeSubscription(subscriptionId);
        int resumeStatusCode = resumeSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(resumeStatusCode, 1, "Status code is not 1");

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        boolean pauseAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
        Assert.assertEquals(pauseAvailableStatus,true);

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Monday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Tuesday")
                ||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Wednesday")||DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Thursday"))) {
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 8);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else if(subscriptionType.equals("WEEKDAY")&&DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Friday")){
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 10);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }else{
            expectedPauseMaxDate = DateUtility.getDateInReadableFormat(subscriptionStartDate, 6);
            pauseTillStartDate=DateUtility.getDateInReadableFormat(subscriptionStartDate, 1);
        }
        Long actualPauseStartDate=JsonPath.parse(pauseCalenderProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseFrom[0].date",Long.class);
        String actualPauseStartDateInReadableFormat=DateUtility.getRedableDateFromEpoch(actualPauseStartDate).split(" ")[0];
        Assert.assertEquals(actualPauseStartDateInReadableFormat,subscriptionStartDate);
        subscriptionTestHelper.validatePauseTillDetails(pauseCalenderProcessor,subscriptionType,pauseTillStartDate,expectedPauseMaxDate);

        boolean skipAvailableStatus = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
        Assert.assertEquals(skipAvailableStatus,true);
        subscriptionTestHelper.validateSkipDetailsInPauseCalender(pauseCalenderProcessor,subscriptionType,subscriptionStartDate);
    }

    //--------------------------------------------------------Pause Calender Negative Scenario--------------------------------------------------------------

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause, skip calender date in pause calender after skip, pause the subscription on same day where weekend service= true, false, Negative Scenario",groups={"Regression"})
    public void getPauseSkipDateAfterSkipPauseOnSameDayPrecutoffTest(String payload) throws IOException, ParseException {

        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(subscriptionId);
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        Processor skipProcessor = subscriptionHelper.skipSubscription(subscriptionMealId);
        int skipStatusCode = skipProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(skipStatusCode, 1,"Not able to skip subscription");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(subscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseStatusCode, 1,"Not able to pause subscription");

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        int pauseCalenderStatusCode=pauseCalenderProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseCalenderStatusCode,410,"Able to get pause calender details after skip, pause the subscription");
        String pauseCalenderStatusMessage=pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(pauseCalenderStatusMessage,SubscriptionConstant.SUBSCRIPTION_ALREADYPAUSEDSUBSCRIPTION_MSG,"Able to get pause calender details after skip, pause the subscription");
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause, skip calender date in pause calender after paused the subscription where weekend service= true, false, Negative Scenario",groups={"Regression"})
    public void getPauseSkipDateAlreadyPausedSubscriptionTest(String payload) throws IOException, ParseException {
        Processor processor = subscriptionHelper.createSubscription(payload);
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
        Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
        String userId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");

        String pauseEndDate=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
        Processor pauseSubscriptionProcessor = subscriptionHelper.pauseSubscription(subscriptionId, DateUtility.convertReadableDateTimeToInMilisecond(subscriptionStartDate), DateUtility.convertReadableDateTimeToInMilisecond(pauseEndDate));
        int pauseStatusCode = pauseSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseStatusCode, 1,"Not able to pause subscription");

        Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
        int pauseCalenderStatusCode=pauseCalenderProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(pauseCalenderStatusCode,410,"Able to get pause calender details after pause the subscription");
        String pauseCalenderStatusMessage=pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(pauseCalenderStatusMessage,SubscriptionConstant.SUBSCRIPTION_ALREADYPAUSEDSUBSCRIPTION_MSG,"Able to get pause calender details after pause the subscription");
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate pause, skip calender date in pause calender user does not have any upcoming meals ,weekend service= true, false",groups={"Regression"})
    public void getPauseSkipDateUserNotHaveUpcomingMealTest(String payload) throws IOException, ParseException {
        try {
            String workingDayAfterThreeDay="";
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            Assert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
            String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");

            if(subscriptionType.equals("WEEKDAY")){
                workingDayAfterThreeDay=DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate,6);
            }else{
                workingDayAfterThreeDay=DateUtility.getDateInReadableFormat(subscriptionStartDate,6);
            }
            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + workingDayAfterThreeDay));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            int pauseCalenderStatusCode=pauseCalenderProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            Assert.assertEquals(pauseCalenderStatusCode,410,"Able to get pause calender details in post cutoff time of last meal");
            String pauseCalenderStatusMessage=pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(pauseCalenderStatusMessage,SubscriptionConstant.SUBSCRIPTION_NOUPCOMINGMEALSSUBSCRIPTION_MSG,"Able to get pause calender details in post cutoff time of last meal");

        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider="createSubscriptionDP",description ="Validate the pause calender where subscription expiry date remains only 7 days from current date and no meal delivered yet, Weekend Service= true, false",groups={"Regression"})
    public void getPauseCalenderExpiryDatesSevenDaysRemainsNoMealDeliveredYet(String payload) throws IOException {
            SoftAssert softAssert=new SoftAssert();
            Processor processor = subscriptionHelper.createSubscription(payload);
            int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
            responseSubscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            softAssert.assertEquals(responseStatusCode, 1,"Not able to create Subscription");
            String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
            String subscriptionId = processor.ResponseValidator.GetNodeValue("$.data.id");
            String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");

            subscriptionHelper.updateExpiryDateSubscription(subscriptionEndDate, subscriptionId);
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(subscriptionId, userId);
            String pauseTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseTitle");
            softAssert.assertEquals(pauseTitle,SubscriptionConstant.SUBSCRIPTION_NOPAUSEAVAILABLESUBSCRIPTION_MSG,"Pause slots are available");
            boolean pauseAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            softAssert.assertEquals(pauseAvailable,false,"Pause option are available for user");

            String skipTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.skipDetails.skipTitle");
            softAssert.assertEquals(skipTitle,SubscriptionConstant.SUBSCRIPTION_NOSKIPAVAILABLESUBSCRIPTION_MSG,"Skip slots are available");
            boolean skipAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            softAssert.assertEquals(skipAvailable,false,"Skip option is available for user");
            softAssert.assertAll();
    }

    @Test(description ="Validate the pause calender where subscription expiry date remains only 8 to 14 days from current date and no meal delivered yet, Weekend Service= true",groups={"Regression"})
    public void getPauseCalenderExpiryDatesEightToFourteenDaysRemainsNoMealDeliveredYetWeekendTrue() throws IOException, ParseException {
        HashMap<String, String> response = subscriptionHelper.createSubscription(true);
        responseSubscriptionId = response.get("id");
        subscriptionTestHelper.validatePauseCalenderUsingExpiryDateWeekendTrue(response);

    }
    @Test(description ="Validate the pause calender where subscription expiry date remains only 8 to 14 days from current date and no meal delivered yet, Weekend Service= false",groups={"Regression"})
    public void getPauseCalenderExpiryDatesEightToFourteenDaysRemainsNoMealDeliveredYetWeekendFalse() throws IOException, ParseException {
        HashMap<String, String> response = subscriptionHelper.createSubscription(false);
        responseSubscriptionId = response.get("id");
        subscriptionTestHelper.validatePauseCalenderUsingExpiryDateWeekendFalse(response);
    }

    @Test(description ="Validate the pause calender where subscription expiry date remains only 4 to 10 days from current date and three meal delivered yet, Weekend Service= true",groups={"Regression"})
    public void getPauseCalenderExpiryDatesFourToTenDaysRemainsThreeMealDeliveredYetWeekendTrue() throws IOException, ParseException {
        try {
            HashMap<String, String> response = subscriptionHelper.createSubscription(true);
            responseSubscriptionId = response.get("id");
            subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(response.get("subscriptionStartDate"), 3));
            subscriptionTestHelper.validatePauseCalenderUsingExpiryDateWeekendTrue(response);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }
    @Test(description ="Validate the pause calender where subscription expiry date remains only 4 to 10 days from current date and three meal delivered yet, Weekend Service= false",groups={"Regression"})
    public void getPauseCalenderExpiryDatesEightToFourteenDaysRemainsThreeMealDeliveredYetWeekendFalse() throws IOException, ParseException {
        try{
        HashMap<String, String> response = subscriptionHelper.createSubscription(false);
        responseSubscriptionId = response.get("id");
        subscriptionTestHelper.changeServerTime(DateUtility.getDateInReadableFormat(response.get("subscriptionStartDate"), 3));
            subscriptionTestHelper.validatePauseCalenderUsingExpiryDateWeekendFalse(response);
    }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }
    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }
}
