package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer_info
{
    private String email_verification_flag;

    private String mobile;

    private String verified;

    private String lastOrderCity;

    private String referral_code;

    private String isEnabled;

    private String name;

    private String superType;

    private String customer_id;

    private String referred_by;

    private String email;

    private String referralUsed;

    private String optional_map;

    public String getEmail_verification_flag ()
    {
        return email_verification_flag;
    }

    public void setEmail_verification_flag (String email_verification_flag)
    {
        this.email_verification_flag = email_verification_flag;
    }

    public String getMobile ()
    {
        return mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public String getVerified ()
    {
        return verified;
    }

    public void setVerified (String verified)
    {
        this.verified = verified;
    }

    public String getLastOrderCity ()
    {
        return lastOrderCity;
    }

    public void setLastOrderCity (String lastOrderCity)
    {
        this.lastOrderCity = lastOrderCity;
    }

    public String getReferral_code ()
    {
        return referral_code;
    }

    public void setReferral_code (String referral_code)
    {
        this.referral_code = referral_code;
    }

    public String getIsEnabled ()
    {
        return isEnabled;
    }

    public void setIsEnabled (String isEnabled)
    {
        this.isEnabled = isEnabled;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getSuperType ()
    {
        return superType;
    }

    public void setSuperType (String superType)
    {
        this.superType = superType;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getReferred_by ()
{
    return referred_by;
}

    public void setReferred_by (String referred_by)
    {
        this.referred_by = referred_by;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getReferralUsed ()
    {
        return referralUsed;
    }

    public void setReferralUsed (String referralUsed)
    {
        this.referralUsed = referralUsed;
    }

    public String getOptional_map ()
{
    return optional_map;
}

    public void setOptional_map (String optional_map)
    {
        this.optional_map = optional_map;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [email_verification_flag = "+email_verification_flag+", mobile = "+mobile+", verified = "+verified+", lastOrderCity = "+lastOrderCity+", referral_code = "+referral_code+", isEnabled = "+isEnabled+", name = "+name+", superType = "+superType+", customer_id = "+customer_id+", referred_by = "+referred_by+", email = "+email+", referralUsed = "+referralUsed+", optional_map = "+optional_map+"]";
    }
}
