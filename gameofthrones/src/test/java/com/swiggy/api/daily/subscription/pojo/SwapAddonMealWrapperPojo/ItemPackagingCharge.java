
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "price",
    "tax",
    "inclusiveTax"
})
public class ItemPackagingCharge {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("tax")
    private Tax_ tax;
    @JsonProperty("inclusiveTax")
    private Boolean inclusiveTax;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public ItemPackagingCharge withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public ItemPackagingCharge withTotal(Integer total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public ItemPackagingCharge withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public ItemPackagingCharge withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @JsonProperty("tax")
    public Tax_ getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Tax_ tax) {
        this.tax = tax;
    }

    public ItemPackagingCharge withTax(Tax_ tax) {
        this.tax = tax;
        return this;
    }

    @JsonProperty("inclusiveTax")
    public Boolean getInclusiveTax() {
        return inclusiveTax;
    }

    @JsonProperty("inclusiveTax")
    public void setInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public ItemPackagingCharge withInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
        return this;
    }

    public ItemPackagingCharge setDefaultData() {
        return this.withTotalWithoutDiscount(SubscriptionConstant.ITEMPACKAGINGCHARGETOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.ITEMPACKAGINGCHARGETOTAL)
                .withDiscount(SubscriptionConstant.ITEMPACKAGINGCHARGEDISCOUNT)
                .withPrice(SubscriptionConstant.ITEMPACKAGINGCHARGEPRICE)
                .withTax(new Tax_().setDefaultData())
                .withInclusiveTax(SubscriptionConstant.INCLUSIVETAX);
    }

}
