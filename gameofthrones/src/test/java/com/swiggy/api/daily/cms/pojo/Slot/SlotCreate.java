
package com.swiggy.api.daily.cms.pojo.Slot;

import java.util.*;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entity_id",
    "meta",
    "store_id",
    "entity_type",
    "slot_info_list"
})
public class SlotCreate {

    @JsonProperty("entity_id")
    private String entity_id;
    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("store_id")
    private Integer store_id;
    @JsonProperty("entity_type")
    private String entity_type;
    @JsonProperty("slot_info_list")
    private List<Slot_info_list> slot_info_list = null;


    /**
     * No args constructor for use in serialization
     * 
     */
    public SlotCreate() {
    }

    /**
     * 
     * @param slot_info_list
     * @param store_id
     * @param entity_type
     * @param entity_id
     * @param meta
     */
    public SlotCreate(String entity_id, Meta meta, Integer store_id, String entity_type, List<Slot_info_list> slot_info_list) {
        super();
        this.entity_id = entity_id;
        this.meta = meta;
        this.store_id = store_id;
        this.entity_type = entity_type;
        this.slot_info_list = slot_info_list;
    }

    @JsonProperty("entity_id")
    public String getEntity_id() {
        return entity_id;
    }

    @JsonProperty("entity_id")
    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    public SlotCreate withEntity_id(String entity_id) {
        this.entity_id = entity_id;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public SlotCreate withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    @JsonProperty("store_id")
    public Integer getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    public SlotCreate withStore_id(Integer store_id) {
        this.store_id = store_id;
        return this;
    }

    @JsonProperty("entity_type")
    public String getEntity_type() {
        return entity_type;
    }

    @JsonProperty("entity_type")
    public void setEntity_type(String entity_type) {
        this.entity_type = entity_type;
    }

    public SlotCreate withEntity_type(String entity_type) {
        this.entity_type = entity_type;
        return this;
    }

    @JsonProperty("slot_info_list")
    public List<Slot_info_list> getSlot_info_list() {
        return slot_info_list;
    }

    @JsonProperty("slot_info_list")
    public void setSlot_info_list(List<Slot_info_list> slot_info_list) {
        this.slot_info_list = slot_info_list;
    }

    public SlotCreate withSlot_info_list(List<Slot_info_list> slot_info_list) {
        this.slot_info_list = slot_info_list;
        return this;
    }
    public SlotCreate setData(String entitytype,Integer interval,List days,String freq,String starttime,String endtime,Long live_date,Long exp_date,boolean is_exclusive )
    {
        Meta me=new Meta();
        Slot_info_list ls=new Slot_info_list();
        List l=new ArrayList();
        l.add(ls);
        RRule ru=new RRule();
        ru.setInterval(interval);
        ru.setBy_day_list(days);
        Recurrence_pattern rp=new Recurrence_pattern();
        rp.setFrequency(freq);
        rp.setRRule(ru);
        ls.setStart_time(starttime);
        ls.setEnd_time(endtime);
        ls.setLive_date(live_date);
        ls.setExpiry_date(exp_date);
        ls.setIs_inclusive(is_exclusive);
        ls.setRecurrence_pattern(rp);

        this.withEntity_id(CMSDailyContants.entityid).withStore_id(CMSDailyContants.store_id)
                .withMeta(me).withSlot_info_list(l).withEntity_type(entitytype);
        return this;
    }
    public SlotCreate setDataNegative(String entityid,Integer storeid,String entitytype,Integer interval,List days,String freq,String starttime,String endtime,Long live_date,Long exp_date,boolean is_exclusive )
    {
        Meta me=new Meta();
        Slot_info_list ls=new Slot_info_list();
        List l=new ArrayList();
        l.add(ls);
        RRule ru=new RRule();
        ru.setInterval(interval);
        ru.setBy_day_list(days);
        Recurrence_pattern rp=new Recurrence_pattern();
        rp.setFrequency(freq);
        rp.setRRule(ru);
        ls.setStart_time(starttime);
        ls.setEnd_time(endtime);
        ls.setLive_date(live_date);
        ls.setExpiry_date(exp_date);
        ls.setIs_inclusive(is_exclusive);
        ls.setRecurrence_pattern(rp);

        this.withEntity_id(entityid).withStore_id(storeid)
                .withMeta(me).withSlot_info_list(l).withEntity_type(entitytype);
        return this;
    }


}
