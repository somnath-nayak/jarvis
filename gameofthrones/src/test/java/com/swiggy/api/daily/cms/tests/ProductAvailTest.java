package com.swiggy.api.daily.cms.tests;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.availabilityService.helper.AvailServiceHelper;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import framework.gameofthrones.Cersei.ToolBox;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductAvailTest extends CMSDailyDP {

    CMSDailyHelper helper=new CMSDailyHelper();
    AvailServiceHelper help=new AvailServiceHelper();
    List<String> listspin;
    List<String> listprodcut;
    SoftAssert softassert=new SoftAssert();

    @Test(dataProvider = "productavailmeta",description = "This test will verify ")
    public void availableMealAvail(String starttime, String endtime, List pids, String storeid) throws Exception{
        Assert.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        String res= helper.prodctListingAvail(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        softassert.assertEquals(storeid,storeidres);
        String spinmeta= JsonPath.read(res,"$.data..variations..spin").toString().replace("[","").replace("]","").replace("\"", "");
        listspin = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listspin.size()>0,"Data Coming as blank");
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        listprodcut = Arrays.asList(productmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        softassert.assertAll();

    }

    @Test(dataProvider = "productavailmeta",description = "This test will get all available Meal")
    public void availableMealAvailWithStoreandMultipleProductID(String starttime, String endtime, List pids, String storeid) throws Exception{
        softassert.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        String res= helper.prodctListingAvail(starttime, endtime, listprodcut, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        softassert.assertEquals(storeid,storeidres);
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> listprodcutactual = Arrays.asList(productmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        ToolBox tb=new ToolBox();
        softassert.assertTrue(tb.compareLists(listprodcutactual,listprodcut),"List Data Not matching");
        softassert.assertAll();

    }

    @Test(dataProvider = "productavailmeta",description = "This test will get all available Meal")
    public void availableMealAvailWithStoreandSingleProductID(String starttime, String endtime, List pids, String storeid) throws Exception{
        softassert.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        List<String> li=new ArrayList<>();
        li.add(listprodcut.get(0));
        String res= helper.prodctListingAvail(starttime, endtime, li, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        softassert.assertEquals(storeid,storeidres);
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> listprodcutactual = Arrays.asList(productmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        ToolBox tb=new ToolBox();
        softassert.assertTrue(tb.compareLists(listprodcutactual,li),"List Data Not matching");
        softassert.assertAll();

    }

    @Test(dataProvider = "productavailmeta",description = "This test will verify all spin inventory and remaining in listing with inventory search sold count and maxcap")
    public void mealStockVerify(String starttime, String endtime, List pids, String storeid) throws Exception{
        List<String> li=new ArrayList<>();
        List<String> li1=new ArrayList<>();
        softassert.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        String res= helper.prodctListingAvail(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        softassert.assertEquals(storeid,storeidres);
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> listprodcutactual = Arrays.asList(productmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listprodcutactual.size()>0,"Data Coming as blank");
        String spinmeta= JsonPath.read(res,"$.data..variations..spin").toString().replace("[","").replace("]","").replace("\"", "");
        listspin = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        softassert.assertTrue(listspin.size()>0,"Data Coming as blank");
        String inventory_total=JsonPath.read(res, "$.data..inventory.total").toString().replace("[", "").replace("]", "").replace("\"", "");
        List<String> inventorytotal=Arrays.asList(inventory_total.split("\\s*,\\s*"));
        softassert.assertTrue(inventorytotal.size()>0,"Inventory Total Coming as blank");
        String inventory_remaining=JsonPath.read(res, "$.data..inventory.remaining").toString().replace("[", "").replace("]", "").replace("\"", "");
        List<String> inventoryremain=Arrays.asList(inventory_remaining.split("\\s*,\\s*"));
        softassert.assertTrue(inventoryremain.size()>0,"Inventory remianing Coming as blank");
        for(int i=0;i<listspin.size();i++){
            String responseinventory=helper.searchInventory(listspin.get(i),storeid,null, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
            String maxcap=JsonPath.read(responseinventory,"$.data[0]..inventory_construct.inventories.max_cap").toString().replace("[","").replace("]","");
            li.add(maxcap);}
        ToolBox tb=new ToolBox();
        Reporter.log("Now verifying the List of stock from listing and list of maxcap from inventory");
        softassert.assertTrue(tb.compareLists(inventorytotal,li),"Max cap is not showing correct");
        for(int i=0;i<listspin.size();i++){
            String responseinventorynew=helper.searchInventory(listspin.get(i),storeid,null, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
            String maxcap=JsonPath.read(responseinventorynew,"$.data[0]..inventory_construct.inventories.max_cap").toString().replace("[","").replace("]","");
            String soldcount=JsonPath.read(responseinventorynew,"$.data[0]..inventory_construct.inventories.sold_count").toString().replace("[","").replace("]","");
            li1.add(Integer.toString(Integer.parseInt(maxcap)-Integer.parseInt(soldcount)));}
        ToolBox tb1=new ToolBox();
        Reporter.log("Now verifying the List of remaining from listing and list of sold count from inventory");
        softassert.assertTrue(tb1.compareLists(inventoryremain,li1),"Remaining count not showing correct");
        softassert.assertAll();

    }

    @Test(dataProvider = "productavailmeta",description = "This test will verify availabilty of spins inside the store and compare it to availability service data")
    public void mealAvailVerify(String starttime, String endtime, List pids, String storeid) throws Exception{
        List<String> li=new ArrayList<>();
        softassert.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        String res= helper.prodctListingAvail(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        softassert.assertEquals(storeid,storeidres);
        String spinmeta= JsonPath.read(res,"$.data..variations..spin").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> spin = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        softassert.assertTrue(spin.size()>0,"Data Coming as blank");
        String avialvalue= JsonPath.read(res,"$.data..variations..availability.is_avail").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> availlist = Arrays.asList(avialvalue.split("\\s*,\\s*"));
        softassert.assertTrue(availlist.size()>0,"Data Coming as blank");
        for(int i=0;i<availlist.size();i++){
            String entity=storeidres+"-"+spin.get(i);
            String val=Boolean.toString(helper.availCheckProduct(entity,null,null,1));
            li.add(val);}
            ToolBox tb=new ToolBox();
        softassert.assertTrue(tb.compareLists(availlist,li),"Availability not showing as expected");
        softassert.assertAll();
    }


    @Test(dataProvider = "meallistingnegative",description = "This test will verify negative cases for meal meta with different combination of data")
    public void availableMealAvailNegative(String starttime, String endtime, List pids, String storeid) throws Exception{
        String res= helper.prodctListingAvail(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String data=JsonPath.read(res,"$.data");
        if(data==null||data.equals("Start time cannot be less then End time")||data.equals("Not able to parse StartTime")||data.equals("Not able to parse EndTime")||data.equals("Endtime cannot be null if Starttime provided")){
            softassert.assertTrue(true,"Data should not be shown for invalid cases"); }
            else softassert.assertTrue(false,"Data should not be shown for invalid cases");
        softassert.assertAll();
    }


}
