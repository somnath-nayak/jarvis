package com.swiggy.api.daily.sandUserService;

import com.swiggy.api.sf.rng.tests.RandomNumber;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.annotations.DataProvider;

import java.util.Random;

public class UserServiceDailyDp implements SANDConstants {

    String Env;
    PropertiesHandler properties = new PropertiesHandler();
    Random random = new Random();
    DBHelper db = new DBHelper();
    SANDHelper helper = new SANDHelper();
    RandomNumber rm = new RandomNumber(2000, 3000);

    public UserServiceDailyDp() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }


    @DataProvider(name = "signUpDaily")
    public Object[][] signUpDaily() {
        return new Object[][]{
                {randomAlpha(), getMobile(), getemailString(), SANDConstants.password, "DAILY"}
        };
    }

    @DataProvider(name = "userId")
    public Object[][] userId() {
        return new Object[][]{{SANDConstants.id},
                {SANDConstants.mobileId},
                {SANDConstants.email},
                {SANDConstants.referal_code},
                {"DAILY"}};
    }


    private String getMobile() {
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
    }

    private String userIDD() {
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_00L) + 1_000_000_00L);
    }

    private String getemailString() {
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 10) {
            int index = (int) (rnd.nextFloat() * CHARS.length());
            salt.append(CHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr + "@gmail.com";
    }

    private String randomAlpha() {
        int count = 6;
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int) (Math.random() * alphabet.length());
            builder.append(alphabet.charAt(character));
        }
        return builder.toString();
    }

    @DataProvider(name = "userIdNegative")
    public Object[][] userIdNegative() {
        return new Object[][]{{SANDConstants.id, userIDD()},
                {SANDConstants.mobileId, getMobile()},
                {SANDConstants.email, getemailString()},
                {SANDConstants.referal_code, randomAlpha()}};
    }

    @DataProvider(name = "userProfileAdmin")
    public Object[][] userProfileAdmin() {
        return new Object[][]{{SANDConstants.user_id}, {SANDConstants.userEmail}, {SANDConstants.userMobile}};
    }

    @DataProvider(name = "userProfileAdminNegative")
    public Object[][] userProfileAdminNegative() {
        return new Object[][]{{SANDConstants.user_id, userIDD()}, {SANDConstants.userEmail, getemailString()}, {SANDConstants.userMobile, getMobile()}};
    }

    @DataProvider(name = "userProfile")
    public Object[][] userProfile() {
        return new Object[][]{{SANDConstants.tr, SANDConstants.tr},
                {SANDConstants.tr, SANDConstants.fal},
                {SANDConstants.fal, SANDConstants.tr},
                {SANDConstants.fal, SANDConstants.fal}};
    }

    @DataProvider(name = "signUpProfile")
    public Object[][] signUpProfile() {
        return new Object[][]{{getMobile(), getemailString(), SANDConstants.name, SANDConstants.tr, SANDConstants.tr},
                {getMobile(), getemailString(), SANDConstants.name, SANDConstants.tr, SANDConstants.fal},
                {getMobile(), getemailString(), SANDConstants.name, SANDConstants.fal, SANDConstants.tr},
                {getMobile(), getemailString(), SANDConstants.name, SANDConstants.fal, SANDConstants.fal}};
    }

    @DataProvider(name = "updateMobileNumber")
    public Object[][] updateMobileNumber() {
        return new Object[][]{{SANDConstants.name, getMobile(), getemailString(), SANDConstants.password,"DAILY"}, {SANDConstants.name, getMobile(), getemailString(), SANDConstants.password,"DAILY"}};
    }

    @DataProvider(name = "userInternalPatch")
    public Object[][] userInternalPatch() {
        return new Object[][]{
                {randomAlpha(), getMobile(), getemailString(), SANDConstants.password, mo, getMobile()},
                {randomAlpha(), getMobile(), getemailString(), SANDConstants.password, usedEmail, getemailString()},
                {randomAlpha(), getMobile(), getemailString(), SANDConstants.password, lastOrderCity, "1"},
                {randomAlpha(), getMobile(), getemailString(), SANDConstants.password, emailVerificationFlag, tr},
        };
    }

    @DataProvider(name = "setPassword")
    public Object[][] setPassword() {
        return new Object[][]{
                {getMobile(), getemailString()},
                {getMobile(), getemailString()},
        };
    }

    @DataProvider(name = "referCode")
    public Object[][] referCode() {
        return new Object[][]{
                {getMobile(), getemailString()},
                {getMobile(), getemailString()},
        };
    }

    @DataProvider(name = "signUpV1")
    public Object[][] signUpV1() {
        return new Object[][]{{getMobile(), getemailString(), SANDConstants.name}, {getMobile(), getemailString(), SANDConstants.name}};
    }


}
