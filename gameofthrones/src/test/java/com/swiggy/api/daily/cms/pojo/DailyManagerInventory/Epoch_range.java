
package com.swiggy.api.daily.cms.pojo.DailyManagerInventory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "start_epoch",
    "end_epoch"
})
public class Epoch_range {

    @JsonProperty("start_epoch")
    private Long start_epoch;
    @JsonProperty("end_epoch")
    private Long end_epoch;

    @JsonProperty("start_epoch")
    public Long getStart_epoch() {
        return start_epoch;
    }

    @JsonProperty("start_epoch")
    public void setStart_epoch(Long start_epoch) {
        this.start_epoch = start_epoch;
    }

    public Epoch_range withStart_epoch(Long start_epoch) {
        this.start_epoch = start_epoch;
        return this;
    }

    @JsonProperty("end_epoch")
    public Long getEnd_epoch() {
        return end_epoch;
    }

    @JsonProperty("end_epoch")
    public void setEnd_epoch(Long end_epoch) {
        this.end_epoch = end_epoch;
    }

    public Epoch_range withEnd_epoch(Long end_epoch) {
        this.end_epoch = end_epoch;
        return this;
    }

    public Epoch_range setData(Long startepoc,Long endepoc){
        this.withStart_epoch(startepoc).withEnd_epoch(endepoc);
        return this;
    }

}