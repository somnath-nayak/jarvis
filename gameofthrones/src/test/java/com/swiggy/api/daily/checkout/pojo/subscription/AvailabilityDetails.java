package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class AvailabilityDetails {


    @JsonProperty("message")
    private Long message;
    @JsonProperty("nonAvailableReason")
    private String nonAvailableReason;
    @JsonProperty("remainingCount")
    private Long remainingCount;
    @JsonProperty("available")
    private Boolean available;
    @JsonProperty("inStock")
    private Boolean inStock;

    @JsonProperty("message")
    public Long getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(Long message) {
        this.message = message;
    }

    public AvailabilityDetails withMessage(Long message) {
        this.message = message;
        return this;
    }

    @JsonProperty("nonAvailableReason")
    public String getNonAvailableReason() {
        return nonAvailableReason;
    }

    @JsonProperty("nonAvailableReason")
    public void setNonAvailableReason(String nonAvailableReason) {
        this.nonAvailableReason = nonAvailableReason;
    }

    public AvailabilityDetails withNonAvailableReason(String nonAvailableReason) {
        this.nonAvailableReason = nonAvailableReason;
        return this;
    }

    @JsonProperty("remainingCount")
    public Long getRemainingCount() {
        return remainingCount;
    }

    @JsonProperty("remainingCount")
    public void setRemainingCount(Long remainingCount) {
        this.remainingCount = remainingCount;
    }

    public AvailabilityDetails withRemainingCount(Long remainingCount) {
        this.remainingCount = remainingCount;
        return this;
    }

    @JsonProperty("available")
    public Boolean getAvailable() {
        return available;
    }

    @JsonProperty("available")
    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public AvailabilityDetails withAvailable(Boolean available) {
        this.available = available;
        return this;
    }

    @JsonProperty("inStock")
    public Boolean getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Boolean inStock) {
        this.inStock = inStock;
    }

    public AvailabilityDetails withInStock(Boolean inStock) {
        this.inStock = inStock;
        return this;
    }
}
