
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.redis.S;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "id",
    "userId",
    "defaultAddress",
    "name",
    "mobile",
    "address",
    "landmark",
    "area",
    "lat",
    "lng",
    "createdOn",
    "updatedOn",
    "updatedBy",
    "deleted",
    "edited",
    "annotation",
    "flatNo",
    "city",
    "reverseGeoCodeFailed"
})
public class AddressDetail {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("defaultAddress")
    private Boolean defaultAddress;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("lat")
    private Double lat;
    @JsonProperty("lng")
    private Double lng;
    @JsonProperty("createdOn")
    private String createdOn;
    @JsonProperty("updatedOn")
    private String updatedOn;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("deleted")
    private Boolean deleted;
    @JsonProperty("edited")
    private Boolean edited;
    @JsonProperty("annotation")
    private Object annotation;
    @JsonProperty("flatNo")
    private String flatNo;
    @JsonProperty("city")
    private Object city;
    @JsonProperty("reverseGeoCodeFailed")
    private Boolean reverseGeoCodeFailed;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public AddressDetail withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public AddressDetail withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    @JsonProperty("defaultAddress")
    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    @JsonProperty("defaultAddress")
    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public AddressDetail withDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AddressDetail withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public AddressDetail withMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public AddressDetail withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public AddressDetail withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public AddressDetail withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("lat")
    public Double getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Double lat) {
        this.lat = lat;
    }

    public AddressDetail withLat(Double lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public Double getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Double lng) {
        this.lng = lng;
    }

    public AddressDetail withLng(Double lng) {
        this.lng = lng;
        return this;
    }

    @JsonProperty("createdOn")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("createdOn")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public AddressDetail withCreatedOn(String createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    @JsonProperty("updatedOn")
    public String getUpdatedOn() {
        return updatedOn;
    }

    @JsonProperty("updatedOn")
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public AddressDetail withUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public AddressDetail withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @JsonProperty("deleted")
    public Boolean getDeleted() {
        return deleted;
    }

    @JsonProperty("deleted")
    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public AddressDetail withDeleted(Boolean deleted) {
        this.deleted = deleted;
        return this;
    }

    @JsonProperty("edited")
    public Boolean getEdited() {
        return edited;
    }

    @JsonProperty("edited")
    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    public AddressDetail withEdited(Boolean edited) {
        this.edited = edited;
        return this;
    }

    @JsonProperty("annotation")
    public Object getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(Object annotation) {
        this.annotation = annotation;
    }

    public AddressDetail withAnnotation(Object annotation) {
        this.annotation = annotation;
        return this;
    }

    @JsonProperty("flatNo")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flatNo")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public AddressDetail withFlatNo(String flatNo) {
        this.flatNo = flatNo;
        return this;
    }

    @JsonProperty("city")
    public Object getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(Object city) {
        this.city = city;
    }

    public AddressDetail withCity(Object city) {
        this.city = city;
        return this;
    }

    @JsonProperty("reverseGeoCodeFailed")
    public Boolean getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    @JsonProperty("reverseGeoCodeFailed")
    public void setReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    public AddressDetail withReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
        return this;
    }

    public AddressDetail setDefaultData()  {
        return this.withId(SubscriptionConstant.ADDRESSDETAIL_ID)
                .withUserId(SubscriptionConstant.USERID)
                .withDefaultAddress(SubscriptionConstant.DEFAULTADDRESS)
                .withName(SubscriptionConstant.ADDRESSDETAIL_NAME)
                .withMobile(SubscriptionConstant.MOBILE)
                .withAddress(SubscriptionConstant.ADDRESSDETAIL_ADDRESS)
                .withLandmark(SubscriptionConstant.ADDRESSDETAIL_LANDMARK)
                .withArea(SubscriptionConstant.ADDRESSDETAIL_AREA)
                .withLat(SubscriptionConstant.ADDRESSDETAIL_LAT)
                .withLng(SubscriptionConstant.ADDRESSDETAIL_LNG)
                .withCreatedOn(SubscriptionConstant.CREATEDON)
                .withUpdatedOn(SubscriptionConstant.UPDATEDON)
                .withUpdatedBy("")
                .withDeleted(SubscriptionConstant.DELETED)
                .withEdited(SubscriptionConstant.EDITED)
                .withAnnotation(null)
                .withFlatNo(SubscriptionConstant.FLATNO)
                .withCity(null)
                .withReverseGeoCodeFailed(SubscriptionConstant.REVERSE_GEO_CODE_FAILED);
    }

}
