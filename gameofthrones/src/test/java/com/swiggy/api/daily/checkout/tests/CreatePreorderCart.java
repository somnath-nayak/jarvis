package com.swiggy.api.daily.checkout.tests;




import com.swiggy.api.daily.checkout.helper.*;
import com.swiggy.api.daily.checkout.helper.CheckoutCartHelper;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutCommonUtils;
import com.swiggy.api.daily.checkout.pojo.address.IsAddressServiceable;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.daily.checkout.dp.*;
import com.swiggy.api.daily.checkout.pojo.*;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class CreatePreorderCart extends preorderCart{

   CartPreOrderHelper cartPreOrderHelper = new CartPreOrderHelper();
   DailyCheckoutCommonUtils dailyCheckoutCommonUtils = new DailyCheckoutCommonUtils();
   CheckoutCartHelper checkoutCartHelper = new CheckoutCartHelper();
   DailyAddressHelper addressHelper = new DailyAddressHelper();




    //=========================================================***Address Validation Test***======================================================================

    @Test(dataProvider="AddressValidatorsDP",  description = "Create Preorder cart and Validate address")
    public void createPreorderCartAndValidateAddress(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);


    }


    //==========================================================***Meal Slot Validation Test***======================================================================

    @Test(dataProvider="MealSlotValidatorsDP",  description = "Create Preorder cart and Validate Meal Slots")
    public void createPreorderCartAndVerifyMealSlotID(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);

    }

    //==========================================================***Delivery Slot Validation Test***======================================================================


    @Test(dataProvider="DefaultSlotValidatorsDP",  description = "Create Preorder cart and Update delivery slot with default slot details")
    public void createPreorderCartAndVerifyDefaultDeliverySlots(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        SlotDetails slotDetails = dailyCheckoutCommonUtils.getdefaultSlotDeatils(cartResponse);
        cartPayload.getCtx().setSlotDetails(slotDetails);
        Processor cartResponsenew = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponsenew,expectedStatusMessage);

    }

    @Test(dataProvider="DefaultSlotValidatorsDP",  description = "Create Preorder cart and switch delivery slot from default slot details to new")
    public void createPreorderCartAndSwitchDefaultDeliverySlots(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        SlotDetails slotDetails = dailyCheckoutCommonUtils.getdefaultSlotDeatils(cartResponse);


        cartPayload.getCtx().setSlotDetails(slotDetails);
        Processor cartResponsenew = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponsenew,expectedStatusMessage);

        slotDetails = getSlotDeatils(cartResponsenew);

        cartPayload.getCtx().setSlotDetails(slotDetails);
        Processor cartResponsewithslot = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponsewithslot,expectedStatusMessage);


    }



    //==========================================================*** Addons Validation Test***======================================================================


    @Test(dataProvider="AddonsValidatorsDP",  description = "Create Preorder cart and Validate Addons")
    public void createPreorderCartAndVerifyAddons(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);

    }



    //==========================================================*** Items Validation Test***======================================================================

    @Test(dataProvider="StoreItemsValidatorsDP",  description = "Create Preorder cart and Validate Items")
    public void createPreorderCartAndVerifyItems(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);

    }


    //==========================================================*** Item level Price calculations Validation Test***======================================================================

    @Test(dataProvider="ItemLevelPriceValidatorsDP",  description = "Create Preorder cart and Validate Item Price")
    public void createPreorderCartAndVerifyItemsPrice(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        dailyCheckoutCommonUtils.priceCalculationsAtItemLevel1(cartResponse);

    }

    @Test(dataProvider="ItemLevelquantityIncreasePriceValidatorsDP",  description = "Create Preorder cart and Validate Item Price and Increase item quantity")
    public void createPreorderCartAndVerifyItemsPriceafterIncreasingQuantity(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        dailyCheckoutCommonUtils.priceCalculationsAtItemLevel1(cartResponse);


    }

    //==========================================================*** Cart level Price calculations Validation Test***======================================================================

    @Test(dataProvider="ItemLevelPriceValidatorsDP",  description = "Create Preorder cart and Validate Item Price")
    public void createPreorderCartAndVerifyCartTotal(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        dailyCheckoutCommonUtils.finalCartcalcalculations(cartResponse);

    }


    //==========================================================*** TD creation and calculations Validation Test***======================================================================



    @Test(dataProvider="ItemLevelPriceValidatorsDP",  description = "Create Preorder cart and Create Percentage TD for Items")
    public void createPreorderCartAndCreateTDforItemPercentage(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String ItemSpin = cartPayload.getItems().get(0).getSpin();
        int storeIdForTD = cartPayload.getItems().get(0).getStoreId();
        String storeID= Integer.toString(storeIdForTD);
        HashMap<String, String> promotionID = checkoutCartHelper.createPromotionsTDForCart("PERCENTAGE_DISCOUNT","10",storeID, ItemSpin);
        String expectedPromotionID = promotionID.get("id");
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        String actualPromotionID = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartItems[0].itemBill.promotions[0].id");
        softAssert.assertEquals(expectedPromotionID, actualPromotionID, "Expected Promotion ID "+expectedPromotionID+" is not maching with Actual Promotion ID in Response "+actualPromotionID+"");
        softAssert.assertAll();



    }

    @Test(dataProvider="ItemLevelPriceValidatorsDP",  description = "Create Preorder cart and Create flat TD for Items")
    public void createPreorderCartAndCreateTDforItemFlat(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String ItemSpin = cartPayload.getItems().get(0).getSpin();
        int storeIdForTD = cartPayload.getItems().get(0).getStoreId();
        String storeID= Integer.toString(storeIdForTD);
        HashMap<String, String> promotionID = checkoutCartHelper.createPromotionsTDForCart("FLAT_DISCOUNT","10000",storeID, ItemSpin);
        String actualPromotionID = promotionID.get("id");
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);
        String expectedPromotionID = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartItems[0].itemBill.promotions[0].id");
        softAssert.assertEquals(expectedPromotionID, actualPromotionID, "Expected Promotion ID "+expectedPromotionID+" is not maching with Actual Promotion ID in Response "+actualPromotionID+"");
        softAssert.assertAll();

    }

    //==========================================================*** Is Address serviceable API***======================================================================


    @Test(dataProvider="IsAddressServiceableDP",  description = "Create Preorder cart and Validate address")
    public void isAddressServiceableTest(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage, Map<String, String> cartPLHeaders) throws IOException {


        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        String cartID = cartResponse.RequestValidator.GetNodeValue("$.data.cartId");
        IsAddressServiceable payload = addressHelper.checkServiceablePayloadBuilder(cartID);
        Processor addressServicecartResponse = checkAddressServiceability(payload,cartPLHeaders);






    }




    public Processor createPreorderCart(CartPayload cartPayload,Map<String, String> userInfoHeader){

        Processor cartResponse=cartPreOrderHelper.cartUpdate(Utility.jsonEncode(cartPayload),userInfoHeader);
        return cartResponse;
    }

    public Processor checkAddressServiceability(IsAddressServiceable Payload,Map<String, String> cartPLHeaders){

        Processor cartResponse=cartPreOrderHelper.cartUpdate(Utility.jsonEncode(Payload),cartPLHeaders);
        return cartResponse;
    }





    public SlotDetails getSlotDeatils(Processor cartResponse)
    {


        JSONArray slotID = cartResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data.deliverySlotAvailabilityDetails[*]");
        for (int i=0;i<slotID.size();i++) {

            boolean isEnabled = cartResponse.ResponseValidator.GetNodeValueAsBool("$.data.deliverySlotAvailabilityDetails["+i+"].is_enabled");
            boolean isSelected = cartResponse.ResponseValidator.GetNodeValueAsBool("$.data.deliverySlotAvailabilityDetails["+i+"].is_selected");
            if (isEnabled== true && isSelected ==false) {
                int startTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.deliverySlotAvailabilityDetails["+i+"].slot_details.start_time");
                int endTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.deliverySlotAvailabilityDetails["+i+"].slot_details.end_time");
                String selectedSlotID = cartResponse.ResponseValidator.GetNodeValue("$.data.deliverySlotAvailabilityDetails[" + i +"].slot_details.id");
                SlotDetails slotDetails = new SlotDetails();

                slotDetails.setId(selectedSlotID);
                slotDetails.setEndTime(Integer.toString(endTime));
                slotDetails.setStartTime(Integer.toString(startTime));
                return slotDetails;


            }
        }


        return null;

    }




}



