
package com.swiggy.api.daily.cms.pojo.PricingService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "list_price",
    "price",
    "total_price"
})
public class Price_list {

    @JsonProperty("list_price")
    private Integer list_price;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("total_price")
    private Integer total_price;

    @JsonProperty("list_price")
    public Integer getList_price() {
        return list_price;
    }

    @JsonProperty("list_price")
    public void setList_price(Integer list_price) {
        this.list_price = list_price;
    }

    public Price_list withList_price(Integer list_price) {
        this.list_price = list_price;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Price_list withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @JsonProperty("total_price")
    public Integer getTotal_price() {
        return total_price;
    }

    @JsonProperty("total_price")
    public void setTotal_price(Integer total_price) {
        this.total_price = total_price;
    }

    public Price_list withTotal_price(Integer total_price) {
        this.total_price = total_price;
        return this;
    }
    public Price_list setData(Integer list_price,Integer price,Integer total_price){
        this.withList_price(list_price).withPrice(price).withTotal_price(total_price);
        return this;
    }

}
