
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "order_job_id",
    "payment_info",
    "merchant_id",
    "status",
    "metadata",
    "status_meta",
    "customer_info",
    "created_at",
    "updated_at"
})
public class OrderJob {
    RandomNumber rm = new RandomNumber(100001, 1000000);

    @JsonProperty("order_job_id")
    private String orderJobId;
    @JsonProperty("payment_info")
    private List<PaymentInfo> paymentInfo = null;
    @JsonProperty("merchant_id")
    private Object merchantId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonProperty("status_meta")
    private Object statusMeta;
    @JsonProperty("customer_info")
    private Object customerInfo;
    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty("order_job_id")
    public String getOrderJobId() {
        return orderJobId;
    }

    @JsonProperty("order_job_id")
    public void setOrderJobId(String orderJobId) {
        this.orderJobId = orderJobId;
    }

    public OrderJob withOrderJobId(String orderJobId) {
        this.orderJobId = orderJobId;
        return this;
    }

    @JsonProperty("payment_info")
    public List<PaymentInfo> getPaymentInfo() {
        return paymentInfo;
    }

    @JsonProperty("payment_info")
    public void setPaymentInfo(List<PaymentInfo> paymentInfo) {
        this.paymentInfo = paymentInfo;
    }

    public OrderJob withPaymentInfo(List<PaymentInfo> paymentInfo) {
        this.paymentInfo = paymentInfo;
        return this;
    }

    @JsonProperty("merchant_id")
    public Object getMerchantId() {
        return merchantId;
    }

    @JsonProperty("merchant_id")
    public void setMerchantId(Object merchantId) {
        this.merchantId = merchantId;
    }

    public OrderJob withMerchantId(Object merchantId) {
        this.merchantId = merchantId;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public OrderJob withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    public OrderJob withMetadata(Metadata metadata) {
        this.metadata = metadata;
        return this;
    }

    @JsonProperty("status_meta")
    public Object getStatusMeta() {
        return statusMeta;
    }

    @JsonProperty("status_meta")
    public void setStatusMeta(Object statusMeta) {
        this.statusMeta = statusMeta;
    }

    public OrderJob withStatusMeta(Object statusMeta) {
        this.statusMeta = statusMeta;
        return this;
    }

    @JsonProperty("customer_info")
    public Object getCustomerInfo() {
        return customerInfo;
    }

    @JsonProperty("customer_info")
    public void setCustomerInfo(Object customerInfo) {
        this.customerInfo = customerInfo;
    }

    public OrderJob withCustomerInfo(Object customerInfo) {
        this.customerInfo = customerInfo;
        return this;
    }

    @JsonProperty("created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public OrderJob withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public Long getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public OrderJob withUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public OrderJob setDefaultData(String action) throws ParseException {
        return this.withOrderJobId(String.valueOf(rm.nextInt()))
                .withPaymentInfo(new ArrayList<PaymentInfo>() {{add(new PaymentInfo().setDefaultData());}})
                .withMerchantId(null)
                .withStatus(SubscriptionConstant.STATUS)
                .withMetadata(new Metadata().setDefaultData(action))
                .withStatusMeta(null)
                .withCustomerInfo(null)
                .withCreatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()))
                .withUpdatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()));


    }

}
