package com.swiggy.api.daily.subscription.helper;

import com.swiggy.api.daily.subscription.pojo.CancelSubscriptionPoJo;
import com.swiggy.api.daily.subscription.pojo.CreateSubscriptionPojo;

import com.swiggy.api.daily.subscription.pojo.ScheduleDetails;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubscriptionHelper {
    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper=new JsonHelper();
    CancelSubscriptionPoJo cancelSubscriptionPoJo=new CancelSubscriptionPoJo();

    public Processor createSubscription(String payload)throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] payloadArray=new String[]{payload};
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createsubscriptionPojo", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;

    }
    
    public Processor createSubscription(CreateSubscriptionPojo payload)throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] payloadArray=new String[]{jsonHelper.getObjectToJSON(payload)};
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createsubscriptionPojo", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;
        
    }

  /*  public Processor updateSubscription(String payload){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "updatesubscriptionPojo", gameofthrones);
        String[] payloadArray=new String[]{payload};
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;

    }*/

    public Processor pauseSubscription(String id,long startDate,long endDate){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "pausesubscription", gameofthrones);
        String[] urlparams = new String[] { id, String.valueOf(startDate), String.valueOf(endDate) };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }


    public Processor resumeSubscription(String id){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "resumesubscription", gameofthrones);
        String[] urlparams = new String[] { id };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }

    public Processor skipSubscription(String id){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "skipsubscription", gameofthrones);
        String[] urlparams = new String[] { id };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;
    }

    public Processor getSubscriptionDetails(String id){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "detailssubscription", gameofthrones);
        String[] urlparams = new String[] { id };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }

    public Processor getVendorInventoryDetails(String storeId,String startDate,String endDate){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "vendorinventorydetails", gameofthrones);
        String[] urlparams = new String[] { storeId, startDate, endDate };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }

    public Processor getSubscriptionNudgeDetails(String planId,String userId){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("userId",userId);
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "subscriptionnudgedetails", gameofthrones);
        String[] urlparams = new String[] { planId };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }

    public Processor getPauseCalenderDetails(String subscriptionId,String userId){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("userId",userId);
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "pausecalenderdetails", gameofthrones);
        String[] urlparams = new String[] { subscriptionId };
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;

    }

    public Processor createSubscriptionWrapper(String payload){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] payloadArray=new String[]{payload};
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createsubscriptionwrapperPojo", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;

    }

    public Processor getSubscriptionMealBillDetails(String subscriptionMealId,String mealType) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "subscriptionmealbilldetails", gameofthrones);
        String[] urlparams = new String[] { subscriptionMealId, mealType};
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;
    }

    public Processor getSubscriptionBillDetails(String subscriptionId) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "subscriptionbilldetails", gameofthrones);
        String[] urlparams = new String[] { subscriptionId};
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;
    }

    public Processor getOrderDailyOms(String subscriptionId) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "getorderdailyoms", gameofthrones);
        String[] urlparams = new String[] { subscriptionId};
        Processor processor = new Processor(service, requestHeaders,null,urlparams );
        return processor;
    }

    public Processor cancelSubscription(String subscriptionId,String reason) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "cancelsubscription", gameofthrones);
        String[] payloadArray=new String[]{jsonHelper.getObjectToJSON(cancelSubscriptionPoJo.withReason(reason))};
        String[] urlparams = new String[] { subscriptionId};
        Processor processor = new Processor(service, requestHeaders,payloadArray,urlparams);
        return processor;
    }

    public Processor getRefundSubscription(String subscriptionId) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "getrefundsubscription", gameofthrones);
        String[] urlparams = new String[] { subscriptionId};
        Processor processor = new Processor(service, requestHeaders,null,urlparams);
        return processor;
    }

    public Processor createOrderId(String subscriptionMealOrderId)  {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        String[] payloadArray=new String[]{subscriptionMealOrderId};
        GameOfThronesService service = new GameOfThronesService("schedulerservice", "createsubscriptionorderid", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;
    }


    public HashMap<String, String> createSubscription(boolean weekendService)throws IOException {
        String payload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(weekendService)));
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        HashMap<String, String> response=new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        String[] payloadArray=new String[]{payload};
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createsubscriptionPojo", gameofthrones);
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        response.put("id",subscriptionId);
        response.put("userId",userId);
        response.put("subscriptionEndDate",subscriptionEndDate);
        response.put("subscriptionStartDate",subscriptionStartDate);
        response.put("subscriptionType",subscriptionType);
        return response;
    }

    public Processor changeServerTime(String payload){
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("webtest", "timecreate", gameofthrones);
        String[] payloadArray=new String[]{payload};
        Processor processor = new Processor(service, requestHeaders,payloadArray);
        return processor;



    }
    public static HashMap<String, String> getDefaultHeaders() {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        return header;
    }



    public void updateStartDateSubscription(String startDate,String subscriptionId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update subscription_schedule set start_date='"+startDate+ "'where subscription_id='"+subscriptionId+"'";
        sqlTemplate.update(query);
    }
    public void updateExpiryDateSubscription(String expiryDate,String subscriptionId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update subscription_schedule set expiry_date='"+expiryDate+ "'where subscription_id='"+subscriptionId+"'";
        sqlTemplate.update(query);
    }
    public void updateSubscriptionStatus(String subscriptionId,String status)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update subscription set status='"+status+ "'where id='"+subscriptionId+"'";
        sqlTemplate.update(query);
    }
    public String getSubscriptionMealStatus(String subscriptionMealId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query1="select status from subscription_deliverable where id="+subscriptionMealId;
        String query = String.format(query1);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        return list.toString().substring(9,16);
    }

    public String getSubscriptionNewMealStatus(String subscriptionMealJobId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query1="select status from subscription_deliverable where order_id="+subscriptionMealJobId;
        String query = String.format(query1);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        return list.toString().substring(9,18);
    }

    public String getSubscriptionMealStatusForAddon(String subscriptionMealId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query1="select status from subscription_deliverable where id="+subscriptionMealId;
        String query = String.format(query1);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        return list.toString().substring(9,15);
    }

    public void updateSubscriptionMealStatus(String subscriptionMealId,String status)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update subscription_deliverable set status='"+status+ "'where id='"+subscriptionMealId+"'";
        sqlTemplate.update(query);
    }

    public void updateSubscriptionProgress(String subscriptionId,String progress)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update subscription_progress set progress='"+progress+ "'where subscription_id='"+subscriptionId+"'";
        sqlTemplate.update(query);
    }

    public void updatePreorderStatus(String preorderId,String status)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="update preorder set status='"+status+ "'where id='"+preorderId+"'";
        sqlTemplate.update(query);
    }

    public String getSubscriptionMealOrderId(String subscriptionMealId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query1="select order_id from subscription_deliverable where id="+subscriptionMealId;
        String query = String.format(query1);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        return list.toString().substring(9,20);
    }

    public void deleteSubscription(String subscriptionId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("daily_manager");
        String query="DELETE subscription , subscription_deliverable, subscription_fulfilment, subscription_progress, subscription_schedule, subscription_status_audit, subscription_aud, subscription_deliverable_aud, subscription_fulfilment_aud, subscription_progress_aud, subscription_schedule_aud FROM subscription  INNER JOIN subscription_deliverable \n" +
                "ON subscription.id= subscription_deliverable.subscription_id INNER JOIN subscription_fulfilment ON subscription.id=subscription_fulfilment.subscription_id INNER JOIN subscription_progress ON\n" +
                "subscription.id=subscription_progress.subscription_id INNER JOIN subscription_schedule ON subscription.id=subscription_schedule.subscription_id INNER JOIN subscription_status_audit\n" +
                "ON subscription.id=subscription_status_audit.subscription_id INNER JOIN subscription_aud ON subscription.id=subscription_aud.id INNER JOIN subscription_deliverable_aud ON subscription.id=subscription_deliverable_aud.subscription_id\n" +
                "INNER JOIN subscription_fulfilment_aud ON subscription.id=subscription_fulfilment_aud.subscription_id INNER JOIN subscription_progress_aud ON subscription.id=subscription_progress_aud.subscription_id\n" +
                "INNER JOIN subscription_schedule_aud ON subscription.id=subscription_schedule_aud.subscription_id WHERE subscription.id ="+subscriptionId;
        sqlTemplate.update(query);
    }


}
