package com.swiggy.api.daily.cms.dp;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.helper.SwiggyListingService;
import com.swiggy.api.daily.cms.pojo.PlanMeta.AvailablePlanMeta;
import com.swiggy.api.daily.cms.pojo.PlanMeta.Store_plan_id;
import com.swiggy.api.daily.cms.pojo.Relationship.Destination;
import com.swiggy.api.daily.cms.pojo.Relationship.Relationship_data;
import com.swiggy.api.daily.cms.pojo.Relationship.Source;
import com.swiggy.api.erp.delivery.helper.ServiceablilityHelper;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.*;


public class CMSDailyDP {

    AvailablePlanMeta ap = new AvailablePlanMeta();
    Store_plan_id as = new Store_plan_id();
    JsonHelper jsonHelper = new JsonHelper();
    Source sr = new Source();
    Destination ds = new Destination();
    Relationship_data rs = new Relationship_data();
    ServiceablilityHelper help=new ServiceablilityHelper();
    SwiggyListingService swiggyListingService = new SwiggyListingService();


    @DataProvider(name = "availableplanmetadp")
    public Object[][] availablePlanMetaDP() throws IOException {
        as.setStore_id(101l);
        as.setPlan_ids(new ArrayList<>(Arrays.asList(101l, 120l)));
        String payload = jsonHelper.getObjectToJSON(ap.setValue(as, 154330000232l, 154330000232l, 154330000232l));
        Reporter.log(payload, true);
        return new Object[][]{{payload, CMSDailyContants.service_name}};
    }



    @DataProvider(name = "productavailmeta")
    public Object[][] availableMealDPNEW()  {

        return new Object[][]{
                {"", "", null, CMSDailyContants.store_id_inventory}
        };
    }

    @DataProvider(name = "getavailableplandp")
    public Object[][] availablePlanDPNEW(){
        return new Object[][]{
                {new ArrayList<>(Arrays.asList(1, 2, 3)), "154330000232", "154330000232", "154330000232", CMSDailyContants.service_name}
        };
    }

    @DataProvider(name = "getplanbystoredp")
    public Object[][] availablePlanByStore() throws IOException {
        return new Object[][]{
                {"1", "12", "154330000232", "154330000232", CMSDailyContants.service_name}
        };
    }

    @DataProvider(name = "getmealbystoredp")
    public Object[][] availableMealByStore() throws IOException {
        return new Object[][]{
                {"1", "12", "154330000232", "154330000232", CMSDailyContants.service_name}
        };
    }

    @DataProvider(name = "getaddonsdp")
    public Object[][] getAddon() throws IOException {
        return new Object[][]{
                {CMSDailyContants.store_id_inventory}
        };
    }

    @DataProvider(name = "ingestionmeal")
    public Object[][] dataIngestion() {
        Map<String, Object[]> data = new TreeMap<>();
        data.put("1", CMSDailyContants.excelheader);
        data.put("2", new Object[]{
                "100", "2", "Veg	Pulao", "Simple rice dish.", "Will simple spices.	Simple rice dish.", "Pulao", "HOME STYLE", "MEDIUM", "Yes", "INDIAN"});

        return new Object[][]{{data}};
    }

    @DataProvider(name = "slotnegative")
    public Object[][] data() {
        return new Object[][]{
                {-1, "true", 60}, {-1, "false", 22}, {-999, "false", 120}, {-123, "false", -1}, {-22133, "false", -1}
        };
    }

    @DataProvider(name = "slotexpand")
    public Object[][] data12() {
        return new Object[][]{
                {"true", 60}, {"false", 30}, {"false", 120}, {"", 60}, {"true", 60}, {"false", 60}, {"false", 20}, {"true", 15}
        };
    }

    @DataProvider(name = "deleteslot")
    public Object[][] data1() {
        return new Object[][]{
                {0}, {-1}, {-999}, {-123}, {-22133}
        };
    }

    @DataProvider(name = "createslot")
    public Object[][] data2() {
        return new Object[][]{
                //Entity Plan
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2), true, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(0), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(0), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},///More than one Interval Daily
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Live date(It will take current date)
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, null, false, 1},//Daily No Live date and expiry date(Current date with no expiry)

                //Entity Plan

                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(1), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(1), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},///More than one Interval Daily
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Live date
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, null, false, 1},//Daily No Live date and expiry date(Current date with no expiry)
        };

    }

    @DataProvider(name = "updateslot")
    public Object[][] data5() {
        return new Object[][]{
                //Entity Plan
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(0), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(0), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},///More than one Interval Daily
                //Entity Plan

                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(1), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(1), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), true, 1},///More than one Interval Daily
        };

    }

    @DataProvider(name = "createslotnegative")
    public Object[][] data4() {
        return new Object[][]{
                //Entity Plan
                {null, 12, CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Entity ID
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(0), 1, null, "WRONG VALUE", CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Frequency Value.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(0), 1, null, null, CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Frequency Value.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, new ArrayList<>(Arrays.asList("TUESDAY", "WRONG")), CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Day List.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), null, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Start Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, null,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No End Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, "12:21:12:12",
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid End Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(2), 123456789l, false, 1},//Daily Invalid Expiry date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        123456789l, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Live date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily End date> Live Date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), "08:30:01", "08:10:21",
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily End Time> Start Time
//                {CMSDailyContants.entityid,12,CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time,CMSDailyContants.end_time,
//                        CMSDailyHelper.createEPOCforDays(2), CMSDailyHelper.createEPOCforDays(3), null,1},//Daily No Is_incusive value(Not able to do null bcz it boolean
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(3), false, 1},//Daily same Start-date End Date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), "08:30:12", "08:30:12",
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(3), false, 1},//Daily same Start-Time End_Time
                //Entity Plan
                {null, 12, CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Entity ID
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(0), 1, null, "WRONG VALUE", CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Frequency Value.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(0), 1, null, null, CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Frequency Value.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, new ArrayList<>(Arrays.asList("TUESDAY", "WRONG")), CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Day List.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), null, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Start Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, null,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No End Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, "12:21:12:12",
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid End Time.
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Live date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Expiry date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(2), 123456789l, false, 1},//Daily Invalid Expiry date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        123456789l, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily Invalid Live date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily End date> Live Date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), "08:30:01", "08:10:21",
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily End Time> Start Time
//                {CMSDailyContants.entityid,12,CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time,CMSDailyContants.end_time,
//                        CMSDailyHelper.createEPOCforDays(2), CMSDailyHelper.createEPOCforDays(3), null,1},//Daily No Is_incusive value
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(3), CMSDailyHelper.createEPOCforDays(3), false, 1},//Daily same Start-date End Date
                {CMSDailyContants.entityid, 12, CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), "08:30:12", "08:30:12",
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(3), false, 1},//Daily same Start-Time End-Time
        };

    }

    //String entityid,Integer storeid,String entitytype,Integer interval,List days,String freq,String starttime,String endtime,Long live_date,Long exp_date,boolean is_exclusive,int count
    @DataProvider(name = "createholidayslot")
    public Object[][] data3() {
        return new Object[][]{
                //Entity Meal(Product)
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(0), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(0), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(0), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(0), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},///More than one Interval Daily
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Live date(It will take current date)
                {CMSDailyContants.entity_type.get(0), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, null, false, 1},//Daily No Live date and expiry date(Current date with no expiry)
                //Entity Plan

                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily with Single slot
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 3},//Daily with Multiple slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//Weekly with Single slot
                {CMSDailyContants.entity_type.get(1), 1, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 3},//Weekly with Multitple slot
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//No Interval Daily
                {CMSDailyContants.entity_type.get(1), null, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//No Interval Weekly
                {CMSDailyContants.entity_type.get(1), 2, CMSDailyContants.day_list, CMSDailyContants.frequency.get(1), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},//More than one Interval Weekly
                {CMSDailyContants.entity_type.get(1), null, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        CMSDailyHelper.createEPOCforDays(1), CMSDailyHelper.createEPOCforDays(2), false, 1},///More than one Interval Daily
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, CMSDailyHelper.createEPOCforDays(2), false, 1},//Daily No Live date(It will take current date)
                {CMSDailyContants.entity_type.get(1), 1, null, CMSDailyContants.frequency.get(0), CMSDailyContants.start_time, CMSDailyContants.end_time,
                        null, null, false, 1},//Daily No Live date and expiry date(Current date with no expiry)
        };

    }

    @DataProvider(name = "createrelation")
    public Object[][] data15() {
        return new Object[][]{
                {CMSDailyContants.store_id.toString(), CMSDailyContants.product_id.toString(), CMSDailyContants.store_id.toString(), CMSDailyHelper.alphaNumericString(8).toUpperCase(), CMSDailyContants.rel_type.get(0)},
                {CMSDailyContants.store_id.toString(), CMSDailyContants.product_id.toString(), Integer.toString(CMSDailyHelper.randomNumber()), CMSDailyHelper.alphaNumericString(8).toUpperCase(), CMSDailyContants.rel_type.get(0)}


        };


    }

    @DataProvider(name = "updaterelation")
    public Object[][] data24() {
        return new Object[][]{
                {CMSDailyContants.store_id.toString(), CMSDailyContants.product_id.toString(), CMSDailyContants.store_id.toString(), CMSDailyHelper.alphaNumericString(8).toUpperCase(), CMSDailyContants.rel_type.get(0)}

        };


    }

    @DataProvider(name = "getdeleterelationnegative")
    public Object[][] data20() {
        return new Object[][]{
                {"Wrong DATA"},
                {"-1"},
                {"12344"},
        };


    }

    @DataProvider(name = "createrelationnegative")
    public Object[][] data19() {
        return new Object[][]{
                {sr, null, null, CMSDailyContants.rel_type.get(0).toString()},
                {sr, ds, null, CMSDailyContants.rel_type.get(0).toString()},
                {sr, null, rs, CMSDailyContants.rel_type.get(0).toString()},
                {null, ds, rs, CMSDailyContants.rel_type.get(0).toString()},
                {null, ds, null, CMSDailyContants.rel_type.get(0).toString()},
                {null, null, rs, CMSDailyContants.rel_type.get(0).toString()},
                {null, null, null, null},
        };
    }

    @DataProvider(name = "deleterelationnegative")
    public Object[][] data29() {
        return new Object[][]{
                {sr, null, CMSDailyContants.rel_type.get(0).toString()},
                {sr, ds, CMSDailyContants.rel_type.get(0).toString()},
                {sr, null, CMSDailyContants.rel_type.get(0).toString()},
                {null, ds, CMSDailyContants.rel_type.get(0).toString()},
                {null, null, CMSDailyContants.rel_type.get(0).toString()},
                {null, null, null},
                {sr, null, null}
        };
    }

    @DataProvider(name = "searchrelation")
    public Object[][] data16() {
        return new Object[][]{
                {0, 1, CMSDailyContants.rel_type.get(0).toString(), "EQ"},
        };
    }

    @DataProvider(name = "createinventorymeal")
    public Object[][] data18() {
        return new Object[][]{
                {"400-AK36LFT02I-" + String.valueOf(CMSDailyHelper.createEPOCforHour(1)) + "-" + String.valueOf(CMSDailyHelper.createEPOCforHour(5)), "H9K56DMV0Z", "M3", "AK36LFT02I", "400", CMSDailyHelper.createEPOCforHour(1), CMSDailyHelper.createEPOCforHour(5), new Long(CMSDailyHelper.randomNumber()), 100, 0, "daily_catalog_product"}
        };
    }


    @DataProvider(name = "addoncheckout")
    public Object[][] addonData() {
        return new Object[][]{
                {"270", "QQWMSJWJCM"}
        };

    }
    @DataProvider(name = "mealcheckout")
    public Object[][] mealData() {
        return new Object[][]{
                {"270", "KWFT0Y1T75", CMSDailyHelper.createEPOCforCurrent(), CMSDailyHelper.createEPOCforHour(6)}
        };

    }


    @DataProvider(name = "mealcheckoutinv")
    public Object[][] mealDataInv() {
        return new Object[][]{
                {"270", "YYO8SGJ2AR", CMSDailyHelper.createEPOCforCurrent(), CMSDailyHelper.createEPOCforHour(6)}
        };

    }

    @DataProvider(name = "mealcheckoutquantity")
    public Object[][] mealDataQuantity() {
        return new Object[][]{
                {"270", "YYO8SGJ2AR","922", CMSDailyHelper.createEPOCforCurrent(), CMSDailyHelper.createEPOCforHour(6)}
        };

    }
    @DataProvider(name = "mealcheckoutquantitylesser")
    public Object[][] mealDataQuantityLesser() {
        return new Object[][]{
                {"270", "YYO8SGJ2AR","33", CMSDailyHelper.createEPOCforCurrent(), CMSDailyHelper.createEPOCforHour(6)}
        };

    }

    @DataProvider(name = "plancheckout")
    public Object[][] planData() {
        return new Object[][]{
                {"270", "72ARMFF0PI", "1000", "2300", swiggyListingService.getDate(), "24-02-2019"}
        };

    }

    @DataProvider(name = "plancheckoutexcluded")
    public Object[][] planDataExcluded() {
        return new Object[][]{
                {"270", "72ARMFF0PI", "1000", "2300", swiggyListingService.getDate(), swiggyListingService.getDate()}
        };

    }

    @DataProvider(name = "plancheckoutquantity")
    public Object[][] planDataQuantity() {
        return new Object[][]{
                {"270", "72ARMFF0PI", "122","1000", "2300", swiggyListingService.getDate(), "24-02-2019"}
        };

    }
    @DataProvider(name = "plancheckoutquantitylesser")
    public Object[][] planDataQuantityLesser() {
        return new Object[][]{
                {"270", "72ARMFF0PI", "2","1000", "2300", swiggyListingService.getDate(), "24-02-2019"}
        };

    }
    @DataProvider(name = "plancheckoutinclusive")
    public Object[][] planDataInclusive() {
        return new Object[][]{
                {"270", "2BV96R535R", "1000", "2300", swiggyListingService.getDate(), "24-02-2019"}
        };

    }

    @DataProvider(name = "pdpproduct")
    public Object[][] pdpData() {
        return new Object[][]{
                {"270", "ZCSBFOCL1M", swiggyListingService.getDate(), "1000", "1630"}
        };

    }

    @DataProvider(name = "pdpwithoutproduct")
    public Object[][] pdpWithoutProductData() {
        return new Object[][]{
                {"270", swiggyListingService.getDate(), "1000", "1630"}
        };

    }

    @DataProvider(name = "pdpproductpast")
    public Object[][] pdpDataPast() {
        return new Object[][]{
                {"270", "ZCSBFOCL1M", swiggyListingService.getPastDate(), "1000", "1630"}
        };

    }


    @DataProvider(name = "createinventoryplan")
    public Object[][] data25() {
        return new Object[][]{
                {"400-BK36LFT02I-" + String.valueOf(CMSDailyHelper.createEPOCforHour(1)) + "-" + String.valueOf(CMSDailyHelper.createEPOCforHour(5)), "I9K56DMV0Z", null, null, "400", CMSDailyHelper.createEPOCforHour(1), CMSDailyHelper.createEPOCforHour(5), new Long(CMSDailyHelper.randomNumber()), 100, 0, "daily_catalog_plan"},
        };
    }

    @DataProvider(name = "storeidnegative")
    public Object[][] data26() {
        return new Object[][]{
                {99999999},
                {88888888},
                {978675646},
                {-1},
                {-99}
        };
    }

    @DataProvider(name = "entitytypenegative")
    public Object[][] data27() {
        return new Object[][]{
                {"WRONG"},
                {"SOMETHING"},
                {"SWIGGY"},
                {"DAIL"},
                {"JAVA"}
        };
    }

    @DataProvider(name = "entitytypeandstorenegative")
    public Object[][] data28() {
        return new Object[][]{
                {9999999, "WRONG"},
                {87777778, "SOMETHING"},
                {76756656, "SWIGGY"},
                {88667576, "DAIL"},
                {75757567, "JAVA"}
        };
    }

    @DataProvider(name = "adjustmealplannegative")
    public Object[][] data30() {
        return new Object[][]{
                {123, "6767565655", "Wrong", "Wrong", CMSDailyHelper.createEPOCforHour(5), CMSDailyHelper.createEPOCforHour(7)},
                {123, "676", "INCRE", "Wrong", CMSDailyHelper.createEPOCforHour(5), CMSDailyHelper.createEPOCforHour(7)},
                {123, "676", "DECRE", "Wrong", CMSDailyHelper.createEPOCforHour(5), CMSDailyHelper.createEPOCforHour(7)},
                {123, "676", "DECRE", "ABCDEF", CMSDailyHelper.createEPOCforHour(5), CMSDailyHelper.createEPOCforHour(7)}


        };
    }

    @DataProvider(name = "searchskusbyspinplan")
    public Object[][] searchSkus() {
        return new Object[][]{
                {270,"72ARMFF0PI"}

        };

    }
    @DataProvider(name = "searchskusbyspinplanmeta")
    public Object[][] searchSkusMeta() {
        return new Object[][]{
                {270,"08VPTDTYWK"}

        };

    }
    @DataProvider(name = "searchskusbyspinmealmeta")
    public Object[][] searchSkusMealMeta() {
        return new Object[][]{
                {270,"08VPTDTYWK"}

        };

    }

    @DataProvider(name = "searchskusbyspinmeal")
    public Object[][] searchSkusMeal() {
        return new Object[][]{
                {270,"KWFT0Y1T75"}

        };

    }

    @DataProvider(name = "searchskusbyspinplangst")
    public Object[][] searchSkusGst() {
        return new Object[][]{
                {270,"2BV96R535R"}

        };

    }
    @DataProvider(name = "searchskusbyspinplanconfig")
    public Object[][] searchSkusConfig() {
        return new Object[][]{
                {"270","72ARMFF0PI",3,CMSDailyHelper.createEPOCforHour(1)}

        };

    }



    @DataProvider(name = "createpricemeal")
    public Object[][] data31() {
        return new Object[][]{
                {"98352-ABCDEF1","STUVWXYZ1","MEAL-1","ABCDEF1","98352",100,120,100,0,5,"INR",0,5,true,0,5},
                {"98352-ABCDEF2","STUVWXYZ2","MEAL-2","ABCDEF2","98352",100,120,100,0,5,"INR",0,5,false,0,5},
                {"98352-ABCDEF3","STUVWXYZ3","MEAL-3","ABCDEF3","98352",100,120,100,0,5,"INR",0,5,true,0,5},
                {"98352-ABCDEF4","STUVWXYZ4","MEAL-4","ABCDEF4","98352",100,120,100,0,5,"INR",0,5,false,0,5}


        };
    }

    @DataProvider(name = "createpriceplan")
    public Object[][] data32() {
        return new Object[][]{
                {"98353-ABCDEF5","STUVWXYZ5","PLAN-1","ABCDEF5","98353",100,120,100,0,5,"INR",0,5,true,0,5},
                {"98353-ABCDEF6","STUVWXYZ6","PLAN-2","ABCDEF6","98353",100,120,100,0,5,"INR",0,5,false,0,5},
                {"98353-ABCDEF7","STUVWXYZ7","PLAN-3","ABCDEF7","98353",100,120,100,0,5,"INR",0,5,true,0,5},
                {"98353-ABCDEF8","STUVWXYZ8","PLAN-4","ABCDEF8","98353",100,120,100,0,5,"INR",0,5,false,0,5}


        };
    }

    @DataProvider(name = "getpricingdpnegative")
    public Object[][] prcingNegative(){
        return new Object[][]{{"-1"},{"1"},{"100-11111"},{" "},{"Something"}};
    }

    @DataProvider(name = "meallistingnegative")
    public Object[][] data33() {
        return new Object[][]{
                {"",Long.toString(CMSDailyHelper.createEPOCforHour(1)),null,"12345"},//No Start Time
                {Long.toString(CMSDailyHelper.createEPOCforHour(1)),"",null,"12345"},//No End time
                //{Long.toString(CMSDailyHelper.createEPOCforHour(1)),Long.toString(CMSDailyHelper.createEPOCforHour(1)),null,"984462412"},//Invalid StoreID
                {Long.toString(CMSDailyHelper.createEPOCforHour(5)),Long.toString(CMSDailyHelper.createEPOCforHour(1)),null,"984462"},//Start Time>End Time
                {"22:00",Long.toString(CMSDailyHelper.createEPOCforHour(1)),null,"9844624"},//Start Time in wrong Format
                {Long.toString(CMSDailyHelper.createEPOCforHour(5)),"23:00",null,"9844624"},//END Time in wrong Format
                {"19:20","22:20",null,"9844624"},//Start and End Time both in wrong Format
        };
    }

    @DataProvider(name = "updateinventorynegative")
    public Object[][] data40() {
        return new Object[][]{
                {"77474884",125667},
                {"-1",125667},
                {"0",125667},
                {"988868686",0},
                {"-123",125667}

        };
    }

    @DataProvider(name = "getinventorynegative")
    public Object[][] data41() {
        return new Object[][]{
                {"400-AK36LFT02I-15515250-155153"},
                {"400-XYZXSSS-15515250-155153"},
                {"400-XYZXSSS-000-0000"},
                {"000-000-000-0000"},
                {"90999999-999-99-999"}

        };
    }


}

//