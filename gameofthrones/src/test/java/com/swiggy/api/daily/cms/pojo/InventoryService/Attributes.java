
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "end_epoch",
        "slot_id",
        "start_epoch"
})
public class Attributes {

    @JsonProperty("end_epoch")
    private Long end_epoch;
    @JsonProperty("slot_id")
    private Long slot_id;
    @JsonProperty("start_epoch")
    private Long start_epoch;

    @JsonProperty("end_epoch")
    public Long getEnd_epoch() {
        return end_epoch;
    }

    @JsonProperty("end_epoch")
    public void setEnd_epoch(Long end_epoch) {
        this.end_epoch = end_epoch;
    }

    public Attributes withEnd_epoch(Long end_epoch) {
        this.end_epoch = end_epoch;
        return this;
    }

    @JsonProperty("slot_id")
    public Long getSlot_id() {
        return slot_id;
    }

    @JsonProperty("slot_id")
    public void setSlot_id(Long slot_id) {
        this.slot_id = slot_id;
    }

    public Attributes withSlot_id(Long slot_id) {
        this.slot_id = slot_id;
        return this;
    }

    @JsonProperty("start_epoch")
    public Long getStart_epoch() {
        return start_epoch;
    }

    @JsonProperty("start_epoch")
    public void setStart_epoch(Long start_epoch) {
        this.start_epoch = start_epoch;
    }

    public Attributes withStart_epoch(Long start_epoch) {
        this.start_epoch = start_epoch;
        return this;
    }

    public Attributes setData(Long startepc,Long endepoc,Long slotid){
        this.withStart_epoch(startepc).withEnd_epoch(endepoc).withSlot_id(slotid);
        return this;
    }
}