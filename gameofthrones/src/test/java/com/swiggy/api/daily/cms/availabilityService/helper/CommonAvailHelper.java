package com.swiggy.api.daily.cms.availabilityService.helper;

import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.SqlTemplate;

public class CommonAvailHelper {

    public String deleteCitySlot(String city_id, String day, String updated_by) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(AvailServiceConstant.catalogDB);
        String query = AvailServiceConstant.deleteCitySlot + city_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes;
        sqlTemplate.execute(AvailServiceConstant.deleteCitySlot + city_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes);
        return query;
    }

    public String deleteAreaSlot(String area_id, String day, String updated_by) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(AvailServiceConstant.catalogDB);
        String query = AvailServiceConstant.deleteAreaSlot + area_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes;
        sqlTemplate.execute(AvailServiceConstant.deleteAreaSlot + area_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes);
        return query;
    }

    public String deleteRestSlot(String rest_id, String day, String updated_by) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(AvailServiceConstant.catalogDB);
        String query = AvailServiceConstant.deleteRestaurantSlots + rest_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes;
        sqlTemplate.execute(AvailServiceConstant.deleteRestaurantSlots + rest_id + AvailServiceConstant.endQuotes + AvailServiceConstant.day + day + AvailServiceConstant.endQuotes +
                AvailServiceConstant.updated_by + updated_by + AvailServiceConstant.endQuotes);
        return query;
    }

    public String deleteSlotInfoEntry(String entity_id, String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(AvailServiceConstant.catalogDB);
        String query = AvailServiceConstant.deleteSlotInfo + entity_id + AvailServiceConstant.endQuotes + AvailServiceConstant.query1 + rest_id + AvailServiceConstant.endQuotes + AvailServiceConstant.limit;
        sqlTemplate.execute(AvailServiceConstant.deleteSlotInfo + entity_id + AvailServiceConstant.endQuotes + AvailServiceConstant.query1 + rest_id + AvailServiceConstant.endQuotes + AvailServiceConstant.limit);
        return query;
    }
}
