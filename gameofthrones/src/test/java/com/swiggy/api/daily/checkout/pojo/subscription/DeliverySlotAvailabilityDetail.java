package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class DeliverySlotAvailabilityDetail {


    @JsonProperty("slot_details")
    private SlotDetails slotDetails;
    @JsonProperty("is_enabled")
    private Boolean isEnabled;
    @JsonProperty("is_selected")
    private Boolean isSelected;
    @JsonProperty("display_message")
    private String displayMessage;

    @JsonProperty("slot_details")
    public SlotDetails getSlotDetails() {
        return slotDetails;
    }

    @JsonProperty("slot_details")
    public void setSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
    }

    public DeliverySlotAvailabilityDetail withSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
        return this;
    }

    @JsonProperty("is_enabled")
    public Boolean getIsEnabled() {
        return isEnabled;
    }

    @JsonProperty("is_enabled")
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public DeliverySlotAvailabilityDetail withIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    @JsonProperty("is_selected")
    public Boolean getIsSelected() {
        return isSelected;
    }

    @JsonProperty("is_selected")
    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public DeliverySlotAvailabilityDetail withIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
        return this;
    }

    @JsonProperty("display_message")
    public String getDisplayMessage() {
        return displayMessage;
    }

    @JsonProperty("display_message")
    public void setDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
    }

    public DeliverySlotAvailabilityDetail withDisplayMessage(String displayMessage) {
        this.displayMessage = displayMessage;
        return this;
    }
}
