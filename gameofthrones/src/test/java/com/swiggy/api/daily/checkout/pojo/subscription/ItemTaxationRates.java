package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class ItemTaxationRates {

    @JsonProperty("itemCgstRate")
    private Long itemCgstRate;
    @JsonProperty("itemSgstRate")
    private Long itemSgstRate;
    @JsonProperty("itemIgstRate")
    private Long itemIgstRate;
    @JsonProperty("packagingCgstRate")
    private Long packagingCgstRate;
    @JsonProperty("packagingSgstRate")
    private Long packagingSgstRate;
    @JsonProperty("packagingIgstRate")
    private Long packagingIgstRate;

    @JsonProperty("itemCgstRate")
    public Long getItemCgstRate() {
        return itemCgstRate;
    }

    @JsonProperty("itemCgstRate")
    public void setItemCgstRate(Long itemCgstRate) {
        this.itemCgstRate = itemCgstRate;
    }

    public ItemTaxationRates withItemCgstRate(Long itemCgstRate) {
        this.itemCgstRate = itemCgstRate;
        return this;
    }

    @JsonProperty("itemSgstRate")
    public Long getItemSgstRate() {
        return itemSgstRate;
    }

    @JsonProperty("itemSgstRate")
    public void setItemSgstRate(Long itemSgstRate) {
        this.itemSgstRate = itemSgstRate;
    }

    public ItemTaxationRates withItemSgstRate(Long itemSgstRate) {
        this.itemSgstRate = itemSgstRate;
        return this;
    }

    @JsonProperty("itemIgstRate")
    public Long getItemIgstRate() {
        return itemIgstRate;
    }

    @JsonProperty("itemIgstRate")
    public void setItemIgstRate(Long itemIgstRate) {
        this.itemIgstRate = itemIgstRate;
    }

    public ItemTaxationRates withItemIgstRate(Long itemIgstRate) {
        this.itemIgstRate = itemIgstRate;
        return this;
    }

    @JsonProperty("packagingCgstRate")
    public Long getPackagingCgstRate() {
        return packagingCgstRate;
    }

    @JsonProperty("packagingCgstRate")
    public void setPackagingCgstRate(Long packagingCgstRate) {
        this.packagingCgstRate = packagingCgstRate;
    }

    public ItemTaxationRates withPackagingCgstRate(Long packagingCgstRate) {
        this.packagingCgstRate = packagingCgstRate;
        return this;
    }

    @JsonProperty("packagingSgstRate")
    public Long getPackagingSgstRate() {
        return packagingSgstRate;
    }

    @JsonProperty("packagingSgstRate")
    public void setPackagingSgstRate(Long packagingSgstRate) {
        this.packagingSgstRate = packagingSgstRate;
    }

    public ItemTaxationRates withPackagingSgstRate(Long packagingSgstRate) {
        this.packagingSgstRate = packagingSgstRate;
        return this;
    }

    @JsonProperty("packagingIgstRate")
    public Long getPackagingIgstRate() {
        return packagingIgstRate;
    }

    @JsonProperty("packagingIgstRate")
    public void setPackagingIgstRate(Long packagingIgstRate) {
        this.packagingIgstRate = packagingIgstRate;
    }

    public ItemTaxationRates withPackagingIgstRate(Long packagingIgstRate) {
        this.packagingIgstRate = packagingIgstRate;
        return this;
    }
}
