
package com.swiggy.api.daily.subscription.pojo;

import java.util.HashMap;
import java.util.Map;

import com.swiggy.api.daily.common.DateUtility;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "arg",
    "pemname",
    "boxip",
    "query"
})
public class ServerTimeChangePojo {

    @JsonProperty("arg")
    private String arg;
    @JsonProperty("pemname")
    private String pemname;
    @JsonProperty("boxip")
    private String boxip;
    @JsonProperty("query")
    private String query;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("arg")
    public String getArg() {
        return arg;
    }

    @JsonProperty("arg")
    public void setArg(String arg) {
        this.arg = arg;
    }

    public ServerTimeChangePojo withArg(String arg) {
        this.arg = arg;
        return this;
    }

    @JsonProperty("pemname")
    public String getPemname() {
        return pemname;
    }

    @JsonProperty("pemname")
    public void setPemname(String pemname) {
        this.pemname = pemname;
    }

    public ServerTimeChangePojo withPemname(String pemname) {
        this.pemname = pemname;
        return this;
    }

    @JsonProperty("boxip")
    public String getBoxip() {
        return boxip;
    }

    @JsonProperty("boxip")
    public void setBoxip(String boxip) {
        this.boxip = boxip;
    }

    public ServerTimeChangePojo withBoxip(String boxip) {
        this.boxip = boxip;
        return this;
    }

    @JsonProperty("query")
    public String getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(String query) {
        this.query = query;
    }

    public ServerTimeChangePojo withQuery(String query) {
        this.query = query;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ServerTimeChangePojo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public ServerTimeChangePojo setDefaultData(){
        return this.withArg("-i")
                .withPemname("dragonstone.pem")
                .withBoxip("ubuntu@daily-manager-0.u2.swiggy.in")
                .withQuery("sudo timedatectl set-time "+DateUtility.getCurrentTimeInReadableFormat());

    }

}
