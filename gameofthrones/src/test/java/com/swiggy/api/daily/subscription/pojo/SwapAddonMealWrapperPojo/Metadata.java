
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


@JsonPropertyOrder({
    "dailySubType",
    "itemDetails",
    "pricingDetail",
    "couponDetail",
    "storeDetail",
    "deliveryDetail",
    "orderEditType",
    "subscriptionMealId"
})
public class Metadata {

    @JsonProperty("dailySubType")
    private String dailySubType;
    @JsonProperty("itemDetails")
    private List<ItemDetail> itemDetails = null;
    @JsonProperty("pricingDetail")
    private PricingDetail pricingDetail;
    @JsonProperty("couponDetail")
    private CouponDetail couponDetail;
    @JsonProperty("storeDetail")
    private StoreDetail storeDetail;
    @JsonProperty("deliveryDetail")
    private DeliveryDetail deliveryDetail;
    @JsonProperty("orderEditType")
    private String orderEditType;
    @JsonProperty("subscriptionMealId")
    private String subscriptionMealId;

    @JsonProperty("dailySubType")
    public String getDailySubType() {
        return dailySubType;
    }

    @JsonProperty("dailySubType")
    public void setDailySubType(String dailySubType) {
        this.dailySubType = dailySubType;
    }

    public Metadata withDailySubType(String dailySubType) {
        this.dailySubType = dailySubType;
        return this;
    }

    @JsonProperty("itemDetails")
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    @JsonProperty("itemDetails")
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public Metadata withItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
        return this;
    }

    @JsonProperty("pricingDetail")
    public PricingDetail getPricingDetail() {
        return pricingDetail;
    }

    @JsonProperty("pricingDetail")
    public void setPricingDetail(PricingDetail pricingDetail) {
        this.pricingDetail = pricingDetail;
    }

    public Metadata withPricingDetail(PricingDetail pricingDetail) {
        this.pricingDetail = pricingDetail;
        return this;
    }

    @JsonProperty("couponDetail")
    public CouponDetail getCouponDetail() {
        return couponDetail;
    }

    @JsonProperty("couponDetail")
    public void setCouponDetail(CouponDetail couponDetail) {
        this.couponDetail = couponDetail;
    }

    public Metadata withCouponDetail(CouponDetail couponDetail) {
        this.couponDetail = couponDetail;
        return this;
    }

    @JsonProperty("storeDetail")
    public StoreDetail getStoreDetail() {
        return storeDetail;
    }

    @JsonProperty("storeDetail")
    public void setStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
    }

    public Metadata withStoreDetail(StoreDetail storeDetail) {
        this.storeDetail = storeDetail;
        return this;
    }

    @JsonProperty("deliveryDetail")
    public DeliveryDetail getDeliveryDetail() {
        return deliveryDetail;
    }

    @JsonProperty("deliveryDetail")
    public void setDeliveryDetail(DeliveryDetail deliveryDetail) {
        this.deliveryDetail = deliveryDetail;
    }

    public Metadata withDeliveryDetail(DeliveryDetail deliveryDetail) {
        this.deliveryDetail = deliveryDetail;
        return this;
    }

    @JsonProperty("orderEditType")
    public String getOrderEditType() {
        return orderEditType;
    }

    @JsonProperty("orderEditType")
    public void setOrderEditType(String orderEditType) {
        this.orderEditType = orderEditType;
    }

    public Metadata withOrderEditType(String orderEditType) {
        this.orderEditType = orderEditType;
        return this;
    }

    @JsonProperty("subscriptionMealId")
    public String getSubscriptionMealId() {
        return subscriptionMealId;
    }

    @JsonProperty("subscriptionMealId")
    public void setSubscriptionMealId(String subscriptionMealId) {
        this.subscriptionMealId = subscriptionMealId;
    }

    public Metadata withSubscriptionMealId(String subscriptionMealId) {
        this.subscriptionMealId = subscriptionMealId;
        return this;
    }

    public Metadata setDefaultData(String action)  {

        if(action.equals("SWAP")) {
            return this.withDailySubType(SubscriptionConstant.DAILYSUBTYPE)
                    .withItemDetails(new ArrayList<ItemDetail>() {{
                        add(new ItemDetail().setDefaultData());
                    }})
                    .withPricingDetail(new PricingDetail().setDefaultData())
                    .withCouponDetail(new CouponDetail().setDefaultData())
                    .withStoreDetail(new StoreDetail().setDefaultData())
                    .withDeliveryDetail(new DeliveryDetail().setDefaultData())
                    .withOrderEditType(SubscriptionConstant.ORDEREDITTYPE)
                    .withSubscriptionMealId(SubscriptionConstant.SUBSCRIPTIONMEALID);
        }else{
            return this.withDailySubType(SubscriptionConstant.DAILYSUBTYPE)
                    .withItemDetails(new ArrayList<ItemDetail>() {{
                        add(new ItemDetail().setDefaultData());
                        add(new ItemDetail().setDefaultData().withItem(new Item().setDefaultData().withType("ADDON")));
                    }})
                    .withPricingDetail(new PricingDetail().setDefaultData())
                    .withCouponDetail(new CouponDetail().setDefaultData())
                    .withStoreDetail(new StoreDetail().setDefaultData())
                    .withDeliveryDetail(new DeliveryDetail().setDefaultData())
                    .withOrderEditType(SubscriptionConstant.ORDEREDITTYPEADDON)
                    .withSubscriptionMealId(SubscriptionConstant.SUBSCRIPTIONMEALID);
        }
    }

}
