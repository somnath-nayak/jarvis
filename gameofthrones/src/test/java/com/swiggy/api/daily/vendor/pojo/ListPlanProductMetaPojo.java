package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import scala.Int;

import java.util.List;

public class ListPlanProductMetaPojo {
    @JsonProperty("store-ids")
    private Integer storeIds;
    
    @JsonProperty("product-ids")
    private ProductId productIds;
    
    public Integer getStoreIds() {
        return storeIds;
    }
    
    public void setStoreIds(Integer storeIds) {
        this.storeIds = storeIds;
    }
    
    public ProductId getProductIds() {
        return productIds;
    }
    
    public void setProductIds(ProductId productIds) {
        this.productIds = productIds;
    }
    
    public ListPlanProductMetaPojo(Integer storeId){
        this.storeIds= storeId;
    }

public ListPlanProductMetaPojo withStoreId(Integer storeId){
        setStoreIds(storeIds);
        return this;
}

public ListPlanProductMetaPojo withProductId(ProductId productId){
        setProductIds(productId);
        return this;
}

public ListPlanProductMetaPojo setDefault(){
        this.setProductIds(new ProductId().setDefault());
        return this;
}


}
