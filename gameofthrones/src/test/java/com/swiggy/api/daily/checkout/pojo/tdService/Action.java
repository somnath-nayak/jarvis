package com.swiggy.api.daily.checkout.pojo.tdService;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "actionType",
        "actionParam",
        "value"
})
public class Action {

    @JsonProperty("actionType")
    private String actionType;
    @JsonProperty("actionParam")
    private String actionParam;
    @JsonProperty("value")
    private String value;

    @JsonProperty("actionType")
    public String getActionType() {
        return actionType;
    }

    @JsonProperty("actionType")
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    @JsonProperty("actionParam")
    public String getActionParam() {
        return actionParam;
    }

    @JsonProperty("actionParam")
    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }

    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("actionType", actionType).append("actionParam", actionParam).append("value", value).toString();
    }

}