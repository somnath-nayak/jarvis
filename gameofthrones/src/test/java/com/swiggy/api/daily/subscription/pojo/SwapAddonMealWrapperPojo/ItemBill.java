
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "itemCharge",
    "itemPackagingCharge"
})
public class ItemBill {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Double total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("itemCharge")
    private ItemCharge itemCharge;
    @JsonProperty("itemPackagingCharge")
    private ItemPackagingCharge itemPackagingCharge;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public ItemBill withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    public ItemBill withTotal(Double total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public ItemBill withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("itemCharge")
    public ItemCharge getItemCharge() {
        return itemCharge;
    }

    @JsonProperty("itemCharge")
    public void setItemCharge(ItemCharge itemCharge) {
        this.itemCharge = itemCharge;
    }

    public ItemBill withItemCharge(ItemCharge itemCharge) {
        this.itemCharge = itemCharge;
        return this;
    }

    @JsonProperty("itemPackagingCharge")
    public ItemPackagingCharge getItemPackagingCharge() {
        return itemPackagingCharge;
    }

    @JsonProperty("itemPackagingCharge")
    public void setItemPackagingCharge(ItemPackagingCharge itemPackagingCharge) {
        this.itemPackagingCharge = itemPackagingCharge;
    }

    public ItemBill withItemPackagingCharge(ItemPackagingCharge itemPackagingCharge) {
        this.itemPackagingCharge = itemPackagingCharge;
        return this;
    }

    public ItemBill setDefaultData() {
        return this.withTotalWithoutDiscount(SubscriptionConstant.TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.TOTAL)
                .withDiscount(SubscriptionConstant.DISCOUNT)
                .withItemCharge(new ItemCharge().setDefaultData())
                .withItemPackagingCharge(new ItemPackagingCharge().setDefaultData());


    }

}
