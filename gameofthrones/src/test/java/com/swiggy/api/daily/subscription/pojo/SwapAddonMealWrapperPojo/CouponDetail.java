
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "couponCode",
    "couponErrorMessage",
    "couponDescription",
    "couponApplied"
})
public class CouponDetail {

    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("couponErrorMessage")
    private String couponErrorMessage;
    @JsonProperty("couponDescription")
    private String couponDescription;
    @JsonProperty("couponApplied")
    private Boolean couponApplied;

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public CouponDetail withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("couponErrorMessage")
    public String getCouponErrorMessage() {
        return couponErrorMessage;
    }

    @JsonProperty("couponErrorMessage")
    public void setCouponErrorMessage(String couponErrorMessage) {
        this.couponErrorMessage = couponErrorMessage;
    }

    public CouponDetail withCouponErrorMessage(String couponErrorMessage) {
        this.couponErrorMessage = couponErrorMessage;
        return this;
    }

    @JsonProperty("couponDescription")
    public String getCouponDescription() {
        return couponDescription;
    }

    @JsonProperty("couponDescription")
    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public CouponDetail withCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
        return this;
    }

    @JsonProperty("couponApplied")
    public Boolean getCouponApplied() {
        return couponApplied;
    }

    @JsonProperty("couponApplied")
    public void setCouponApplied(Boolean couponApplied) {
        this.couponApplied = couponApplied;
    }

    public CouponDetail withCouponApplied(Boolean couponApplied) {
        this.couponApplied = couponApplied;
        return this;
    }

    public CouponDetail setDefaultData()  {
        return this.withCouponCode(SubscriptionConstant.COUPONCODE)
                .withCouponErrorMessage("")
                .withCouponDescription(SubscriptionConstant.COUPONDESCRIPTION)
                .withCouponApplied(SubscriptionConstant.COUPONAPPLIED);
    }

}
