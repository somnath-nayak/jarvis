package com.swiggy.api.daily.checkout.pojo.tdService;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "promotionType",
        "name",
        "active",
        "validFrom",
        "validTill",
        "shareType",
        "shareValue",
        "rules",
        "actions",
        "createdBy",
        "updatedBy",
        "createdAt",
        "updatedAt"
})
public class TdPayload {

    @JsonProperty("promotionType")
    private String promotionType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("active")
    private Integer active;
    @JsonProperty("validFrom")
    private String validFrom;
    @JsonProperty("validTill")
    private String validTill;
    @JsonProperty("shareType")
    private String shareType;
    @JsonProperty("shareValue")
    private Integer shareValue;
    @JsonProperty("rules")
    private List<Rule> rules = null;
    @JsonProperty("actions")
    private List<Action> actions = null;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;

    @JsonProperty("promotionType")
    public String getPromotionType() {
        return promotionType;
    }

    @JsonProperty("promotionType")
    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("active")
    public Integer getActive() {
        return active;
    }

    @JsonProperty("active")
    public void setActive(Integer active) {
        this.active = active;
    }

    @JsonProperty("validFrom")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("validFrom")
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    @JsonProperty("validTill")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("validTill")
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    @JsonProperty("shareType")
    public String getShareType() {
        return shareType;
    }

    @JsonProperty("shareType")
    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    @JsonProperty("shareValue")
    public Integer getShareValue() {
        return shareValue;
    }

    @JsonProperty("shareValue")
    public void setShareValue(Integer shareValue) {
        this.shareValue = shareValue;
    }

    @JsonProperty("rules")
    public List<Rule> getRules() {
        return rules;
    }

    @JsonProperty("rules")
    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @JsonProperty("actions")
    public List<Action> getActions() {
        return actions;
    }

    @JsonProperty("actions")
    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("updatedBy")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updatedBy")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updatedAt")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updatedAt")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("promotionType", promotionType).append("name", name).append("active", active).append("validFrom", validFrom).append("validTill", validTill).append("shareType", shareType).append("shareValue", shareValue).append("rules", rules).append("actions", actions).append("createdBy", createdBy).append("updatedBy", updatedBy).append("createdAt", createdAt).append("updatedAt", updatedAt).toString();
    }

}