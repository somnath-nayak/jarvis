
package com.swiggy.api.daily.subscription.pojo;

import com.swiggy.api.daily.common.DateUtility;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "delivery_start_time",
    "delivery_end_time"
})
public class DeliverySlot {

    @JsonProperty("delivery_start_time")
    private Integer deliveryStartTime;
    @JsonProperty("delivery_end_time")
    private Integer deliveryEndTime;

    @JsonProperty("delivery_start_time")
    public Integer getDeliveryStartTime() {
        return deliveryStartTime;
    }

    @JsonProperty("delivery_start_time")
    public void setDeliveryStartTime(Integer deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
    }

    public DeliverySlot withDeliveryStartTime(Integer deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
        return this;
    }

    @JsonProperty("delivery_end_time")
    public Integer getDeliveryEndTime() {
        return deliveryEndTime;
    }

    @JsonProperty("delivery_end_time")
    public void setDeliveryEndTime(Integer deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
    }

    public DeliverySlot withDeliveryEndTime(Integer deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
        return this;
    }

    public DeliverySlot setDefaultData(){
        return this.withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,15)))
                .withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(3,00)));
    }

}
