package com.swiggy.api.daily.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.checkout.dp.SubscriptionCartDp;
import com.swiggy.api.daily.checkout.helper.CartSubscriptionHelper;
import com.swiggy.api.daily.checkout.helper.CheckoutCartHelper;
import com.swiggy.api.daily.checkout.pojo.CartPayload;
import com.swiggy.api.daily.cms.helper.SwiggyListingService;
import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.json.JSONArray;
import org.json.JSONException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;


public class CreateCartSubscriptionTest extends SubscriptionCartDp {

    private CheckoutCartHelper cartHelper;
    private CartSubscriptionHelper cartSubscriptionHelper;
    CartPayload cartPayload;


    @BeforeClass
    public void beforeClass() {
        cartHelper = new CheckoutCartHelper();
        cartSubscriptionHelper = new CartSubscriptionHelper();


    }


    @Test(description = "This test case verifies the slots are populated properly as per the delivery service", dataProvider = "slotVerification")
    public void verifySlots(String cartType, String storeId, HashMap<String, String> header, HashMap<String, String> headerWithTidSid, String cityId) {

        cartPayload = cartHelper.getDailyCartPayload(cartType, storeId, headerWithTidSid, cityId);
        Processor response = cartHelper.createCart(cartPayload, header);
        SoftAssert softAssert = new SoftAssert();

        try {

            JSONArray checkoutSlotsArray = new JSONArray(response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.deliverySlotAvailabilityDetails"));

            //Get this data from delivery QA
            JSONArray deliverySlotsArray = new JSONArray("");

            for (int i = 0; i < checkoutSlotsArray.length() - 1; i++) {
                softAssert.assertTrue(checkoutSlotsArray.getJSONObject(i).get("is_enabled") == deliverySlotsArray.getJSONObject(i).get(""), "Is enabled is not matching with slot service");
                softAssert.assertTrue(checkoutSlotsArray.getJSONObject(i).get("display_message") == deliverySlotsArray.getJSONObject(i).get(""), "Display message is not matching with slot service");
                softAssert.assertTrue(checkoutSlotsArray.getJSONObject(i).getJSONObject("slot_details").get("start_time") == deliverySlotsArray.getJSONObject(i).get(""), "start time is not same as slot service");
                softAssert.assertTrue(checkoutSlotsArray.getJSONObject(i).getJSONObject("slot_details").get("end_time") == deliverySlotsArray.getJSONObject(i).get(""), "End time is not matching");
                softAssert.assertTrue(checkoutSlotsArray.getJSONObject(i).getJSONObject("slot_details").get("id") == deliverySlotsArray.getJSONObject(i).get(""), "Id of slot is not correct");

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        softAssert.assertAll();
    }


    @Test(description = "This test cases verifies the meal slot response in preorder and plan meals", dataProvider = "mealSlotVerification")
    public void verifyMealSlot(String cartType, String storeId, HashMap<String, String> header, HashMap<String, String> headerWithTidSid, String cityId, String mealSlotType) throws Exception {
        CartPayload payload;
        String checkoutResponse;
        String mealSlotResponse;
        MealSlotHelper mealSlotHelper = new MealSlotHelper();
        SoftAssert softAssert = new SoftAssert();

        String mealSlotId = cartSubscriptionHelper.getMealSlotId(mealSlotType);
        payload = cartHelper.getDailyCartPayload(cartType, storeId, headerWithTidSid, cityId);
        payload.getCtx().setMealSlotId(mealSlotId);

        checkoutResponse = cartHelper.createCart(payload, header).ResponseValidator.GetBodyAsText();
        mealSlotResponse = mealSlotHelper.getMealSlotByID(mealSlotId).ResponseValidator.GetBodyAsText();

        softAssert.assertTrue(JsonPath.read(mealSlotResponse, "$.data.slot").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.slot")), "Slot details not matching");
        softAssert.assertTrue(JsonPath.read(mealSlotResponse, "$.data.day").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.day")), "Day is not matching");
        softAssert.assertTrue(JsonPath.read(mealSlotResponse, "$.data.slot_date").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.slot_date")), "Slot date is not matching");
        softAssert.assertTrue(JsonPath.read(mealSlotResponse, "$.data.slot_end_date").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.slot_end_date")), "Slot end date is not matching");
        softAssert.assertTrue(JsonPath.read(mealSlotResponse, "$.data.slot_display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.slot_display_name")), "Slot display name is not proper");
        softAssert.assertTrue((int) JsonPath.read(mealSlotResponse, "$.data.id") == (int) JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.id"), "Id is not matching. " + "Resp from meal slot: " + JsonPath.read(mealSlotResponse, "$.data.id") + " Resp from checkout: " + JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.id"));
        softAssert.assertTrue((int)JsonPath.read(mealSlotResponse, "$.data.city_id") == (int)JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.city_id"),"City id is not matching");
        softAssert.assertTrue((int)JsonPath.read(mealSlotResponse, "$.data.delivery_start_time") == (int)JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.delivery_start_time"),"Delivery slot time is not matching");
        softAssert.assertTrue((int)JsonPath.read(mealSlotResponse, "$.data.start_time") == (int)JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.start_time"),"start time is not matching with meal slot response");
        softAssert.assertTrue((int)JsonPath.read(mealSlotResponse, "$.data.end_time") == (int)JsonPath.read(checkoutResponse, "$.data.mealSlotResponse.end_time"),"end time is not matching with meal slot response");
        softAssert.assertAll();
    }


    @Test(description = "This test case verifies the item details with CMS for all types of Cart", dataProvider = "itemDetailsVerification")
    public void verifyItemDetails(String cartType, String storeId, HashMap<String, String> header, HashMap<String, String> headerWithTidSid, String cityId) throws Exception {
        CartPayload payload;
        String checkoutResponse;
        String cmsResponse = "";
        String spinId, startTime, endTime, startDate, endDate, excludedDates, mealSlotId;
        MealSlotHelper mealSlotHelper = new MealSlotHelper();
        SwiggyListingService listingService = new SwiggyListingService();
        String mealSlotResponse;
        SoftAssert softAssert = new SoftAssert();

        payload = cartHelper.getDailyCartPayload(cartType, storeId, headerWithTidSid, cityId);

        spinId = payload.getItems().get(0).getSpin();
        storeId = String.valueOf(payload.getItems().get(0).getStoreId());
        mealSlotId = payload.getCtx().getMealSlotId();

        mealSlotResponse = mealSlotHelper.getMealSlotByID(mealSlotId).ResponseValidator.GetBodyAsText();
        checkoutResponse = cartHelper.createCart(payload, header).ResponseValidator.GetBodyAsText();

        startTime = JsonPath.read(mealSlotResponse, "$.data.start_time").toString().replace("[", "").replace("]", "");
        endTime = JsonPath.read(mealSlotResponse, "$.data.end_time").toString().replace("[", "").replace("]", "");


        if (cartType.equalsIgnoreCase("plan")) {
            DateHelper dateHelper = new DateHelper();
            startDate = dateHelper.convertEpochTodd_mm_yyy(payload.getCtx().getStartDate());
            cmsResponse = listingService.checkoutPlan(storeId, spinId, startTime, endTime, startDate, "24-02-2019").ResponseValidator.GetBodyAsText();

            softAssert.assertTrue(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.type").toString().contains("PLAN"), "Cart Type is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].spin").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.spin")), "Spin is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].store_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.store_id")), "Store id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.name")), "Store name is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.id")), "Item id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].product_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.product_id")), "Product id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.display_name")), "Meta display name is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.tenure_unit").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.tenure_unit")), "Tenure unit is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.plan_frequency").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.plan_frequency")), "plan frequency is not correct ");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.short_description").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.short_description")), "Short description is not matching with CMS");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.long_description").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.long_description")), "Long description is not matching with CMS");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.tenure") == JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.tenure"), "Tenure is not correct");

        }
        if (cartType.equalsIgnoreCase("product")) {
            cmsResponse = listingService.checkoutMeal(storeId, spinId, Long.parseLong(startTime), Long.parseLong(endTime)).ResponseValidator.GetBodyAsText();

            softAssert.assertTrue(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.type").toString().contains("PRODUCT"));
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].spin").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.spin")), "Spin is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].store_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.store_id")), "Store id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.name")), "Item name is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.id")), "Item id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].product_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.product_id")), "Product id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.display_name")), "Meta display name is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.short_description").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.short_description")), "Short description is not matching with CMS");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.long_description").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.long_description")), "Long description is not matching with CMS");

        }
        if (cartType.equalsIgnoreCase("addons")) {
            cmsResponse = listingService.checkoutAddon(storeId, spinId).ResponseValidator.GetBodyAsText();

            softAssert.assertTrue(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.type").toString().contains("ADDON"));
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].spin").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.spin")), "Spin is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].store_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.store_id")), "Store id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.name")), "Item name is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.id")), "Item id is not correct");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].product_id").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.product_id")), "Product id is not matching with CMS");
            softAssert.assertTrue(JsonPath.read(cmsResponse, "$.data[0].meta.display_name").toString().equalsIgnoreCase(JsonPath.read(checkoutResponse, "$.data.cartItems[0].item.meta.display_name")), "Meta display name is not matching with CMS");

        }
        softAssert.assertAll();
    }
}
