
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;


@JsonPropertyOrder({
    "id",
    "uuid",
    "name",
    "city",
    "cityCode",
    "area",
    "areaCode",
    "address",
    "latLong",
    "locality",
    "phoneNumbers"
})
public class StoreDetail {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("city")
    private String city;
    @JsonProperty("cityCode")
    private Integer cityCode;
    @JsonProperty("area")
    private String area;
    @JsonProperty("areaCode")
    private Integer areaCode;
    @JsonProperty("address")
    private String address;
    @JsonProperty("latLong")
    private String latLong;
    @JsonProperty("locality")
    private String locality;
    @JsonProperty("phoneNumbers")
    private List<String> phoneNumbers = null;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public StoreDetail withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StoreDetail withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public StoreDetail withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public StoreDetail withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("cityCode")
    public Integer getCityCode() {
        return cityCode;
    }

    @JsonProperty("cityCode")
    public void setCityCode(Integer cityCode) {
        this.cityCode = cityCode;
    }

    public StoreDetail withCityCode(Integer cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public StoreDetail withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("areaCode")
    public Integer getAreaCode() {
        return areaCode;
    }

    @JsonProperty("areaCode")
    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public StoreDetail withAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public StoreDetail withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("latLong")
    public String getLatLong() {
        return latLong;
    }

    @JsonProperty("latLong")
    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public StoreDetail withLatLong(String latLong) {
        this.latLong = latLong;
        return this;
    }

    @JsonProperty("locality")
    public String getLocality() {
        return locality;
    }

    @JsonProperty("locality")
    public void setLocality(String locality) {
        this.locality = locality;
    }

    public StoreDetail withLocality(String locality) {
        this.locality = locality;
        return this;
    }

    @JsonProperty("phoneNumbers")
    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    @JsonProperty("phoneNumbers")
    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public StoreDetail withPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        return this;
    }

    public StoreDetail setDefaultData()  {
        return this.withId(SubscriptionConstant.STOREDETAIL_ID)
                .withUuid(SubscriptionConstant.UUID)
                .withName(SubscriptionConstant.STOREDETAIL_NAME)
                .withCity(SubscriptionConstant.STOREDETAIL_CITY)
                .withCityCode(SubscriptionConstant.CITYCODE)
                .withArea(SubscriptionConstant.STOREDETAIL_AREA)
                .withAreaCode(SubscriptionConstant.AREACODE)
                .withAddress(SubscriptionConstant.STOREDETAIL_ADDRESS)
                .withLatLong(SubscriptionConstant.LATLONG)
                .withLocality(SubscriptionConstant.LOCALITY)
                .withPhoneNumbers(new ArrayList<String>() {{add(SubscriptionConstant.PHONENUMBERS);}});
    }

}
