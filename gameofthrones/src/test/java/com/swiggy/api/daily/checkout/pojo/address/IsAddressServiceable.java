package com.swiggy.api.daily.checkout.pojo.address;

import org.apache.commons.lang.builder.ToStringBuilder;
        import org.codehaus.jackson.annotate.JsonProperty;
        import org.codehaus.jackson.annotate.JsonPropertyOrder;
        import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "lat",
        "lng",
        "cart_id"
})
public class IsAddressServiceable {

    @JsonProperty("lat")
    private Float lat;
    @JsonProperty("lng")
    private Float lng;
    @JsonProperty("cart_id")
    private String cartId;

    @JsonProperty("lat")
    public Float getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Float lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public Float getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Float lng) {
        this.lng = lng;
    }

    @JsonProperty("cart_id")
    public String getCartId() {
        return cartId;
    }

    @JsonProperty("cart_id")
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("lat", lat).append("lng", lng).append("cartId", cartId).toString();
    }

}
