
package com.swiggy.api.daily.cms.pojo.PlanMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "store_id",
    "plan_ids"
})
public class Store_plan_id {

    @JsonProperty("store_id")
    private Long store_id;
    @JsonProperty("plan_ids")
    private List<Long> plan_ids = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Store_plan_id() {
    }

    /**
     * 
     * @param plan_ids
     * @param store_id
     */
    public Store_plan_id(Long store_id, List<Long> plan_ids) {
        super();
        this.store_id = store_id;
        this.plan_ids = plan_ids;
    }

    @JsonProperty("store_id")
    public Long getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Long store_id) {
        this.store_id = store_id;
    }

    public Store_plan_id withStore_id(Long store_id) {
        this.store_id = store_id;
        return this;
    }

    @JsonProperty("plan_ids")
    public List<Long> getPlan_ids() {
        return plan_ids;
    }

    @JsonProperty("plan_ids")
    public void setPlan_ids(List<Long> plan_ids) {
        this.plan_ids = plan_ids;
    }

    public Store_plan_id withPlan_ids(List<Long> plan_ids) {
        this.plan_ids = plan_ids;
        return this;
    }


}
