
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "query"
})
public class SearchInventory {

    @JsonProperty("query")
    private Query query;

    @JsonProperty("query")
    public Query getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(Query query) {
        this.query = query;
    }

    public SearchInventory withQuery(Query query) {
        this.query = query;
        return this;
    }
    public SearchInventory setData(String spinid, String storeid,Long slotid){
        Query q=new Query().setData(spinid,storeid,slotid,null);
        this.withQuery(q);
        return this;
    }


}
