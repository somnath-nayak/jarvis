package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;
import scala.Int;

import java.util.ArrayList;
import java.util.List;

public class PackagingCharges {
    @JsonProperty("baseCharge")
    private Integer baseCharge;

    
    public Integer getBaseCharge() {
        return baseCharge;
    }
    
    public void setBaseCharge(Integer baseCharge) {
        this.baseCharge = baseCharge;
    }
    
    public PackagingCharges withBaseCharge(Integer baseCharge){
        setBaseCharge(baseCharge);
        return this;
    }
    
    public List<PackagingCharges> withGivenValues(Integer baseCharge){
        List<PackagingCharges> packagingChargesList = new ArrayList<>();
        packagingChargesList.add(0,withBaseCharge(baseCharge));
        
        return packagingChargesList;
    }
    
    public PackagingCharges withDefaultValues(){
       
        withBaseCharge(10);
        
        return this;
    }
}
