package com.swiggy.api.daily.cms.tests;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.helper.SwiggyListingService;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.HashMap;

public class SearchSkus extends CMSDailyDP {
    SwiggyListingService swiggyListingService = new SwiggyListingService();
    CMSDailyHelper cmsDailyHelper = new CMSDailyHelper();

    @Test(dataProvider = "searchskusbyspinplan", description = "Verify spin,store and product type details")
    public void searchSkusbyStoreAndSpinPlan(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        int storeid = JsonPath.read(skuresponse, "$.data[0].store_id");
        String spinid = JsonPath.read(skuresponse, "$.data[0].spin");
        String producttype = JsonPath.read(skuresponse, "$.data[0].product_type");
        Assert.assertEquals(storeid, storeId);
        Assert.assertEquals(spinid, spinId);
        Assert.assertEquals(producttype, "PLAN");

    }

    @Test(dataProvider = "searchskusbyspinplan", description = "verify cgst and igst value to be zero if inclusive of taxes is true")
    public void VerifyGstIfInclusiveTrue(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        boolean inclusive = JsonPath.read(skuresponse, "$.data[0].price.inclusive_of_taxes");
        int cgst = JsonPath.read(skuresponse, "$.data[0].price.cgst");
        int igst = JsonPath.read(skuresponse, "$.data[0].price.igst");
        int price = JsonPath.read(skuresponse, "$.data[0].price.price");
        int lisprice = JsonPath.read(skuresponse, "$.data[0].price.list_price");
        Assert.assertNotNull(price);
        Assert.assertNotNull(lisprice);
        if (inclusive == true) {
            Assert.assertEquals(cgst, 0);
            Assert.assertEquals(igst, 0);
        }

    }

    @Test(dataProvider = "searchskusbyspinplangst", description = "verify cgst and igst value to be greater than zero if inclusive of taxes is true")
    public void VerifyGstIfInclusiveFalse(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        boolean inclusive = JsonPath.read(skuresponse, "$.data[0].price.inclusive_of_taxes");
        double cgst = JsonPath.read(skuresponse, "$.data[0].price.cgst");
        double igst = JsonPath.read(skuresponse, "$.data[0].price.igst");
        if (inclusive == true && cgst <= 0 && igst <= 0) {
            Assert.fail();
        }

    }

    @Test(dataProvider = "searchskusbyspinplan", description = "verify next change time in availability is greater than current date time")
    public void VerifyAvail(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        long nextchange = JsonPath.read(skuresponse, "$.data[0].availability.next_change");
        long currenttime = CMSDailyHelper.createEPOCforDays();
        if (currenttime > nextchange) {
            Assert.fail();
        }

    }

    @Test(dataProvider = "searchskusbyspinplanconfig", description = "verify number of configured meals is same as result meals and avail is greater than starttime given")
    public void searchSkusbyStoreAndSpinConfig(String storeId, String spinId, int mealcount, long startepoc) {
        String skuresponse = swiggyListingService.searchSkusbySpinIdConfig(storeId, spinId, mealcount, startepoc).ResponseValidator.GetBodyAsText();
        JSONArray js = JsonPath.read(skuresponse, "$.data[0].components");
        int meals = js.size();
        Assert.assertEquals(meals, 3);
        long inputstart = CMSDailyHelper.createEPOCforHour(1);
        long outputstart = JsonPath.read(skuresponse, "$.data[0].availability.next_change");
        if (inputstart > outputstart) {
            Assert.fail();
        }


    }

    @Test(dataProvider = "searchskusbyspinplangst", description = "verify next scheduled is greater than current time")
    public void VerifyComponentNextSchedule(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        long starttime = JsonPath.read(skuresponse, "$.data[0].components[0].next_scheduled_at.start_time");
        long currentime = CMSDailyHelper.createEPOCforCurrent();
        if (currentime > starttime) {
            Assert.fail();
        }


    }

    @Test(dataProvider = "searchskusbyspinplan", description = "Compare price from get price to sku search price")
    public void ComparePricePlan(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        int storeid = JsonPath.read(skuresponse, "$.data[0].store_id");
        String spinid = JsonPath.read(skuresponse, "$.data[0].spin");
        String key = storeid + "-" + spinid;
        int skuprice = JsonPath.read(skuresponse, "$.data[0].price.price");
        int skulistprice = JsonPath.read(skuresponse, "$.data[0].price.list_price");
        String pricingresponse = cmsDailyHelper.getPricing(key, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        int list_price = JsonPath.read(pricingresponse, "$.data[0].pricing_construct.price_list.list_price");
        int price = JsonPath.read(pricingresponse, "$.data[0].pricing_construct.price_list.total_price");
        Assert.assertEquals(list_price, skulistprice);
        Assert.assertEquals(price, skuprice);
    }

    @Test(dataProvider = "searchskusbyspinmeal", description = "Verify spin,store and product type details")
    public void searchSkusbyStoreAndSpinMeal(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        int storeid = JsonPath.read(skuresponse, "$.data[0].store_id");
        String spinid = JsonPath.read(skuresponse, "$.data[0].spin");
        String producttype = JsonPath.read(skuresponse, "$.data[0].product_type");
        Assert.assertEquals(storeid, storeId);
        Assert.assertEquals(spinid, spinId);
        Assert.assertEquals(producttype, "PRODUCT");

    }

    @Test(dataProvider = "searchskusbyspinmeal", description = "Compare meal price of sku and get meal price api")
    public void comparePriceMeal(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        int storeid = JsonPath.read(skuresponse, "$.data[0].store_id");
        String spinid = JsonPath.read(skuresponse, "$.data[0].spin");
        String key = storeid + "-" + spinid;
        int skuprice = JsonPath.read(skuresponse, "$.data[0].price.price");
        int skulistprice = JsonPath.read(skuresponse, "$.data[0].price.list_price");
        String pricingresponse = cmsDailyHelper.getPricing(key, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        double list_price = JsonPath.read(pricingresponse, "$.data[0].pricing_construct.price_list.list_price");
        int listprice=(int)list_price;
        double price = JsonPath.read(pricingresponse, "$.data[0].pricing_construct.price_list.total_price");
        int nprice=(int)price;
        Assert.assertEquals(listprice, skulistprice);
        Assert.assertEquals(nprice, skuprice);
    }

    @Test(dataProvider = "searchskusbyspinplanmeta", description = "verify plan meta details")
    public void VerifyPlanMeta(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        String cuisine=JsonPath.read(skuresponse,"$.data[0].meta.cuisine");
        String diettype=JsonPath.read(skuresponse,"$.data[0].meta.diet_type");
        String displayname=JsonPath.read(skuresponse,"$.data[0].meta.display_name");
        int isveg=JsonPath.read(skuresponse,"$.data[0].meta.is_veg");
        String frequency=JsonPath.read(skuresponse,"$.data[0].meta.plan_frequency");
        String speciality=JsonPath.read(skuresponse,"$.data[0].meta.speciality");
        int tenure=JsonPath.read(skuresponse,"$.data[0].meta.tenure");
        String tenureunit=JsonPath.read(skuresponse,"$.data[0].meta.tenure_unit");
        Assert.assertNotNull(cuisine);
        Assert.assertNotNull(diettype);
        Assert.assertNotNull(displayname);
        Assert.assertNotNull(isveg);
        Assert.assertNotNull(frequency);
        Assert.assertNotNull(speciality);
        Assert.assertNotNull(tenure);
        Assert.assertNotNull(tenureunit);

    }
    @Test(dataProvider = "searchskusbyspinplanmeta", description = "verify plan component meta details")
    public void VerifyComponentMeta(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        int calorificvalue=JsonPath.read(skuresponse,"$.data[0].components[0].meta.calorific_value");
        String cuisine=JsonPath.read(skuresponse,"$.data[0].components[0].meta.cuisine");
        String diettype=JsonPath.read(skuresponse,"$.data[0].components[0].meta.diet_type");
        String displayname=JsonPath.read(skuresponse,"$.data[0].components[0].meta.display_name");
        int isveg=JsonPath.read(skuresponse,"$.data[0].components[0].meta.is_veg");
        String ingredients=JsonPath.read(skuresponse,"$.data[0].components[0].meta.ingredients").toString().replace("[","").replace("]", "");
        String perishable=JsonPath.read(skuresponse,"$.data[0].components[0].meta.is_perishable");
        String spicelevel=JsonPath.read(skuresponse,"$.data[0].components[0].meta.spice_level");
        String speciality=JsonPath.read(skuresponse,"$.data[0].components[0].meta.speciality");
        Assert.assertNotNull(cuisine);
        Assert.assertNotNull(diettype);
        Assert.assertNotNull(displayname);
        Assert.assertNotNull(isveg);
        Assert.assertNotNull(calorificvalue);
        Assert.assertNotNull(speciality);
        Assert.assertNotNull(ingredients);
        Assert.assertNotNull(perishable);
        Assert.assertNotNull(spicelevel);

    }

    @Test(dataProvider = "searchskusbyspinmealmeta", description = "verify meal meta details")
    public void VerifyMealtMeta(int storeId, String spinId) {
        String skuresponse = swiggyListingService.searchSkusbySpinId(storeId, spinId).ResponseValidator.GetBodyAsText();
        HashMap<String,String> meta=JsonPath.read(skuresponse,"$.data[0].meta");
        Assert.assertNotNull(meta);
    }
}
