package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class SlotDetails {

    @JsonProperty("start_time")
    private Long startTime;
    @JsonProperty("end_time")
    private Long endTime;
    @JsonProperty("id")
    private String id;

    @JsonProperty("start_time")
    public Long getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public SlotDetails withStartTime(Long startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public Long getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public SlotDetails withEndTime(Long endTime) {
        this.endTime = endTime;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public SlotDetails withId(String id) {
        this.id = id;
        return this;
    }
}
