package com.swiggy.api.daily.checkout.dp;


import com.swiggy.api.daily.checkout.helper.DailyCheckoutConstants;
import com.swiggy.api.daily.checkout.tests.DailyOrderTest;
import com.swiggy.api.sf.checkout.tests.PlaceOrderRegularTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import java.io.IOException;

public class DailyOrderDP {

    @DataProvider
    public static Object[][] DailyOrder() {
        return new Object[][]{

              //  {"Swiggy-Android", "310", DailyCheckoutConstants.PRODUCT,DailyCheckoutConstants.MOBIKWIK},
               {"Swiggy-Android", "310", DailyCheckoutConstants.PRODUCT,DailyCheckoutConstants.CASH},
              //  {"Swiggy-Android", "310", DailyCheckoutConstants.PRODUCT,DailyCheckoutConstants.PAYTM},
                 /*
                {"Swiggy-iOS", "234",DailyCheckoutConstants.PLAN,"Cash"},
                {"Swiggy-iOS", "234",DailyCheckoutConstants.PRODUCT,"Cash"},*/
        };
  }


    @Factory(dataProvider="DailyOrder")
    public Object[] createInstances(String userAgent,String versionCode,String cartType,String paymentMethod) {
        return new Object[] {new DailyOrderTest(userAgent,versionCode,cartType,paymentMethod)};
    }

}