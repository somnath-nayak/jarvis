package com.swiggy.api.daily.promotions.pojo.createPromotion;

import com.swiggy.api.daily.promotions.helper.PromotionConstants;
import com.swiggy.api.daily.promotions.helper.PromotionsHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class CreatePromotion {
    
    @JsonProperty("promotionType")
    private String promotionType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("active")
    private int active;
    @JsonProperty("validFrom")
    private String validFrom;
    @JsonProperty("validTill")
    private String validTill;
    @JsonProperty("shareType")
    private String shareType;
    @JsonProperty("shareValue")
    private double shareValue;
    @JsonProperty("rules")
    private ArrayList<Rules> rules;
    @JsonProperty("actions")
    private ArrayList<Actions> actions;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;
    @JsonProperty("viewProperties")
    private Description viewProperties;
    @JsonProperty("tenant")
    private String tenant;
   
    public String getTenant() {
        return tenant;
    }
    
    public void setTenant(String tenant) {
        this.tenant = tenant;
    }
   
    public CreatePromotion withTenant(String tenant){
        setTenant(tenant);
        return this;
    }
    
    public Description getViewProperties() {
        return viewProperties;
    }

    public void setViewProperties(Description viewProperties) {
        this.viewProperties = viewProperties;
    }
    
   
    
 
    public CreatePromotion(){
        //Default constructor
    }
    
 // Getter and Setter methods
    public String getPromotionType() {
        return promotionType;
    }
    
    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getActive() {
        return active;
    }
    
    public void setActive(int active) {
        this.active = active;
    }
    
    public String getValidFrom() {
        return validFrom;
    }
    
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }
    
    public String getValidTill() {
        return validTill;
    }
    
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }
    
    public String getShareType() {
        return shareType;
    }
    
    public void setShareType(String shareType) {
        this.shareType = shareType;
    }
    
    public double getShareValue() {
        return shareValue;
    }
    
    public void setShareValue(double shareValue) {
        this.shareValue = shareValue;
    }
    
    public ArrayList<Rules> getRules() {
        return rules;
    }
    
    public void setRules(ArrayList<Rules> rules) {
        this.rules = rules;
    }
    
    public ArrayList<Actions> getActions() {
        return actions;
    }
    
    public void setActions(ArrayList<Actions> actions) {
        this.actions = actions;
    }
    
    public String getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    public String getCreatedAt() {
        return createdAt;
    }
    
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    
    public String getUpdatedAt() {
        return updatedAt;
    }
    
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
    
   // builders
    
    public CreatePromotion withPromotionType(String promotionType){
        setPromotionType(promotionType);
    return this;
    }
    
    public CreatePromotion withName(String name){
        setName(name);
        return this;
    }
    
    public CreatePromotion withActive(int active){
        setActive(active);
        return this;
    }
    
    public CreatePromotion withValidFrom(String validFrom){
        setValidFrom(validFrom);
        return this;
    }
    
    public CreatePromotion withValidTill(String validTill){
        setValidTill(validTill);
        return this;
    }
    
    public CreatePromotion withShareType(String shareType){
        setShareType(shareType);
        return this;
    }
    public CreatePromotion withShareValue(double shareValue){
        setShareValue(shareValue);
        return this;
    }
    public CreatePromotion withRules(ArrayList<Rules> rules){
        setRules(rules);
        return this;
    }
    public CreatePromotion withActions(ArrayList<Actions> actions){
        setActions(actions);
        return this;
    }
    public CreatePromotion withCreatedBy(String createdBy){
        setCreatedBy(createdBy);
        return this;
    }
    public CreatePromotion withCreatedAt(String createdAt){
        setCreatedAt(createdAt);
        return this;
    }
    public CreatePromotion withUpdatedBy(String updatedBy){
        setUpdatedBy(updatedBy);
        return this;
    }
    public CreatePromotion withUpdatedAt(String updatedAt){
        setUpdatedAt(updatedAt);
        return this;
    }
    
    public  CreatePromotion withViewProperties(Description text){
        setViewProperties(text);
        return this;
    }

    public CreatePromotion withViewPropertiesText(String text){
        withViewProperties(new Description().withText(text));
        return this;
    }
    
    // with out TIME_SLOT fact , only for one store & one SKU id and Flat type of discount value 10 at item level
    public CreatePromotion withDefaultValues(){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        values.add(1,new String[]{Integer.toString(Utility.getRandom(100000,100000000))});
    
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        withPromotionType("DISCOUNT").withName("Daily-Automatiom").withActive(1)
                .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(new PromotionsHelper().populateListOfRules(fact,comparisonOperator,values)).withActions(new PromotionsHelper().populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy(PromotionConstants.createdBy).withCreatedAt(createdDate).withUpdatedBy(PromotionConstants.updatedBy).withUpdatedAt(createdDate).withViewPropertiesText("TEST_COPY_TEXT").withTenant("DAILY");
        
       return this;
    }
    
    public CreatePromotion forGivenInput(String type, String discountValue ,String storeId, String spinId){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{storeId});
        values.add(1,new String[]{spinId});
        
        String[] actionType = new String[]{type};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{discountValue};
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        withPromotionType("DISCOUNT").withName("Daily-Automatiom").withActive(1)
                .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(new PromotionsHelper().populateListOfRules(fact,comparisonOperator,values)).withActions(new PromotionsHelper().populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy(PromotionConstants.createdBy).withCreatedAt(createdDate).withUpdatedBy(PromotionConstants.updatedBy).withUpdatedAt(createdDate).withViewPropertiesText("TEST_COPY_TEXT").withTenant("DAILY");
        
        return this;
    }
    
    
    
    public CreatePromotion withOutTenant(){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        values.add(1,new String[]{Integer.toString(Utility.getRandom(100000,100000000))});
        
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        withPromotionType("DISCOUNT").withName("Daily-Automatiom").withActive(1)
                .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(new PromotionsHelper().populateListOfRules(fact,comparisonOperator,values)).withActions(new PromotionsHelper().populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy(PromotionConstants.createdBy).withCreatedAt(createdDate).withUpdatedBy(PromotionConstants.updatedBy).withUpdatedAt(createdDate).withViewPropertiesText("TEST_COPY_TEXT");
        
        return this;
    }
    
    // with out TIME_SLOT fact , only for one store & one SKU id and Flat type of discount value 10 at item level
    public CreatePromotion withOutPromotionTypeParam(){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        values.add(1,new String[]{Integer.toString(Utility.getRandom(100000,100000000))});
        
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        withName("Daily-Automatiom").withActive(1)
                .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(new PromotionsHelper().populateListOfRules(fact,comparisonOperator,values)).withActions(new PromotionsHelper().populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy(PromotionConstants.createdBy).withCreatedAt(createdDate).withUpdatedBy(PromotionConstants.updatedBy).withUpdatedAt(createdDate).withViewPropertiesText("TEST_COPY_TEXT");
        
        return this;
    }
    
    public CreatePromotion withOutPromotionNameParam(){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        values.add(1,new String[]{Integer.toString(Utility.getRandom(100000,100000000))});
        
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        String createdDate = Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        withActive(1).
        withPromotionType("DISCOUNT") .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(new PromotionsHelper().populateListOfRules(fact,comparisonOperator,values)).withActions(new PromotionsHelper().populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy(PromotionConstants.createdBy).withCreatedAt(createdDate).withUpdatedBy(PromotionConstants.updatedBy).withUpdatedAt(createdDate).withViewPropertiesText("TEST_COPY_TEXT");
        
        return this;
    }
    
}
