package com.swiggy.api.daily.promotions.pojo.updatePromotion;

import org.codehaus.jackson.annotate.JsonProperty;

public class Actions {
    
    @JsonProperty("actionType")
    private String actionType;
    @JsonProperty("actionParam")
    private String actionParam;
    @JsonProperty("value")
    private String value;
    
    public  Actions(){
        //Default constructor
    }
    
    //Getter & Setter method
    public String getActionType() {
        return actionType;
    }
    
    public void setActionType(String actionType) {
        this.actionType = actionType;
    }
    
    public String getActionParam() {
        return actionParam;
    }
    
    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }
    
    public String getValue() {
        return value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
    
   // builders
    public Actions withActionType(String actionType){
        setActionType(actionType);
        return this;
    }
    
    public Actions withActionParam(String actionParam){
        setActionParam(actionParam);
        return this;
    }
    
    public Actions withValue(String value){
        setValue(value);
        return this;
    }
    
    // only flat type with value 10 at item level
    public Actions withDefaultValues(){
        withActionType("FLAT_DISCOUNT").withActionParam("ITEMS_PRICE").withValue("10");
        return this;
    }
    
    public Actions withGivenValues(String actionType, String actionParam, String value){
        withActionType(actionType).withActionParam(actionParam).withValue(value);
        return this;
    }
}
