package com.swiggy.api.daily.promotions.pojo.cart;

import com.swiggy.api.daily.promotions.pojo.planListing.Items;
import org.codehaus.jackson.annotate.JsonProperty;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class CartItems {
    
    @JsonProperty("skuId")
    private String skuId;
    @JsonProperty("storeId")
    private String storeId;
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("pricingDetails")
    private ItemsPricingDetails pricingDetails;
    
    public String getSkuId() {
        return skuId;
    }
    
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }
    
    public String getStoreId() {
        return storeId;
    }
    
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
    
    public int getQuantity() {
        return quantity;
    }
    
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public ItemsPricingDetails getPricingDetails() {
        return pricingDetails;
    }
    
    public void setPricingDetails(ItemsPricingDetails pricingDetails) {
        this.pricingDetails = pricingDetails;
    }
   
    
    
    public CartItems withGivenValues(String skuId, String storeId, Integer quantity, Integer price){
    
    setSkuId(skuId); setStoreId(storeId);setQuantity(quantity);
    setStoreId(storeId);
    setQuantity(quantity);
   setPricingDetails( new ItemsPricingDetails().withGivenValues(price));
    
    return this;
    }
    

    public ArrayList<CartItems> listOfCartItems(String[] skuId, String[] storeId, List<Integer> quantity, List<Integer> price){
    ArrayList<CartItems> listOfItems = new ArrayList<>();
    for(int i = 0 ; i< skuId.length ; i++){
        
        listOfItems.add(i,new CartItems().withGivenValues( skuId[i],  storeId[i],  quantity.get(i), price.get(i)));
    }
    
    return listOfItems;
    }
    
    
}
