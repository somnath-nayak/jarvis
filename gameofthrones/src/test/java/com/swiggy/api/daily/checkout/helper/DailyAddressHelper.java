package com.swiggy.api.daily.checkout.helper;

import com.swiggy.api.daily.checkout.pojo.address.AddAddress;
import com.swiggy.api.daily.checkout.pojo.address.IsAddressServiceable;
import com.swiggy.api.daily.checkout.pojo.address.UpdateAddress;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

public class DailyAddressHelper {

        private Initialize gameOfThrones = Initializer.getInitializer();
        JsonHelper jsonHelper= new JsonHelper();
        DailyCheckoutCommonUtils dailyCheckoutCommonUtils =new DailyCheckoutCommonUtils();


    public Processor addNewAddress(HashMap<String, String> requestHeader,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkout", "newaddress", gameOfThrones);
        Processor processor = new Processor(service, requestHeader, payload,0);
        return processor;
     }


    public Processor getAllAddress(Map<String, String> requestHeader){
            GameOfThronesService service = new GameOfThronesService("checkout", "getalladdress", gameOfThrones);
            Processor processor = new Processor(service, (HashMap<String, String>) requestHeader, null, null);
            return processor;
        }


    public Processor isAddressServiceable(HashMap<String, String> headers,String payload) {
        GameOfThronesService service = new GameOfThronesService("checkoutdaily", "isAddressServiceable", gameOfThrones);
        return new Processor(service, headers, new String[]{payload});
    }


    public Processor deleteAddress(HashMap<String, String> requestHeaders, String addressId) {
        GameOfThronesService service = new GameOfThronesService("checkout", "deleteaddress", gameOfThrones);
        String[] payloadParams = {addressId};
        Processor processor = new Processor(service, requestHeaders, payloadParams);
        return processor;
    }


    public Processor updatesAddress(HashMap<String, String> requestHeader,String payload) {
        Processor processor=null;
        try {
            GameOfThronesService service = new GameOfThronesService("checkout", "updateaddress", gameOfThrones);
            processor = new Processor(service, requestHeader, payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }



    public String newAddressBuilder(){
        String payload=null;
        AddAddress addAddress = new AddAddress();
        addAddress.setName(DailyCheckoutConstants.NAME);
        addAddress.setAddress(DailyCheckoutConstants.ADDRESS);
        addAddress.setLandmark(DailyCheckoutConstants.LANDMARK);
        addAddress.setArea(DailyCheckoutConstants.AREA);
        addAddress.setLat(DailyCheckoutConstants.LAT);
        addAddress.setLng(DailyCheckoutConstants.LNG);
        addAddress.setFlat_no(DailyCheckoutConstants.FLAT_NO);
        addAddress.setCity(DailyCheckoutConstants.CITY);
        addAddress.setAnnotation(DailyCheckoutConstants.ANNOTATION);
        try{
        payload=jsonHelper.getObjectToJSON(addAddress);}
        catch (Exception e){
            e.printStackTrace();
        }
        return payload ;
    }


    public String updateAddressBuilder(String addressId){
        String payload=null;
        UpdateAddress updateAddress = new UpdateAddress();
        updateAddress.setAddress_id(Integer.parseInt(addressId));
        updateAddress.setName(DailyCheckoutConstants.NAME);
        updateAddress.setAddress(DailyCheckoutConstants.ADDRESS);
        updateAddress.setLandmark(DailyCheckoutConstants.LANDMARK);
        updateAddress.setArea(DailyCheckoutConstants.AREA);
        updateAddress.setLat(DailyCheckoutConstants.LAT);
        updateAddress.setLng(DailyCheckoutConstants.LNG);
        updateAddress.setFlat_no(DailyCheckoutConstants.FLAT_NO);
        updateAddress.setCity(DailyCheckoutConstants.CITY);
        updateAddress.setAnnotation(DailyCheckoutConstants.ANNOTATION);
        try{
            payload=jsonHelper.getObjectToJSON(updateAddress);}
        catch (Exception e){
            e.printStackTrace();
        }
        return payload ;
    }

    public IsAddressServiceable checkServiceablePayloadBuilder(String cartId){
        String payload=null;
        IsAddressServiceable isAddressServiceable = new IsAddressServiceable();

        isAddressServiceable.setLat(Float.parseFloat(DailyCheckoutConstants.LAT));
        isAddressServiceable.setLng(Float.parseFloat(DailyCheckoutConstants.LNG));
        isAddressServiceable.setCartId(cartId);

        return isAddressServiceable;
    }


    public int addNewAddressAndReturnAddressId(Map<String, String> requestHeader){
        String payload=newAddressBuilder();
        Processor processor=addNewAddress((HashMap<String, String>) requestHeader,payload);
        dailyCheckoutCommonUtils.validateResponseCode(processor);
        dailyCheckoutCommonUtils.validateStatusCode(processor);
        return processor.ResponseValidator.GetNodeValueAsInt("$.data.address_id");

    }


    public Processor getAllSavedAddress(Map<String, String> requestHeader){
        Processor processor=getAllAddress(requestHeader);
        dailyCheckoutCommonUtils.validateResponseCode(processor);
        dailyCheckoutCommonUtils.validateStatusCode(processor);
        return processor;
    }


    public String searchServiceableAddress(Processor processor){
        JSONArray savedAddressId=processor.ResponseValidator.GetNodeValueAsJsonArray("$.data.addresses[*].id");
        for (int i=0;i<savedAddressId.size();i++) {
            String addressId = savedAddressId.get(i).toString();
            String lat=processor.ResponseValidator.GetNodeValue("$.data.addresses["+i+"].lat");
            String lng=processor.ResponseValidator.GetNodeValue("$.data.addresses["+i+"].lng");
            if(lat.equals(DailyCheckoutConstants.LAT) && lng.equals(DailyCheckoutConstants.LNG)){
                return addressId;
            }
        }
        return null;
    }


    public String getServiceableAddressId(Map<String, String> header){
        Processor processor=getAllSavedAddress(header);
        String address=searchServiceableAddress(processor);
        if (address==null){
            return Integer.toString(addNewAddressAndReturnAddressId(header));
        }
      return address;
    }

    }

