package com.swiggy.api.daily.checkout.pojo;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

    @JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
    @JsonPropertyOrder({
            "userId",
            "userAgent",
            "sid",
            "versionCode",
            "tid",
            "token"
    })
    public class UserInfo {

        @JsonProperty("userId")
        private String userId;
        @JsonProperty("userAgent")
        private String userAgent;
        @JsonProperty("sid")
        private String sid;
        @JsonProperty("versionCode")
        private int versionCode;
        @JsonProperty("tid")
        private String tid;
        @JsonProperty("token")
        private String token;

        @JsonProperty("userId")
        public String getUserId() {
            return userId;
        }

        @JsonProperty("userId")
        public void setUserId(String userId) {
            this.userId = userId;
        }

        @JsonProperty("userAgent")
        public String getUserAgent() {
            return userAgent;
        }

        @JsonProperty("userAgent")
        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        @JsonProperty("sid")
        public String getSid() {
            return tid;
        }

        @JsonProperty("sid")
        public void setSid(String sid) {
            this.sid = sid;
        }

        @JsonProperty("versionCode")
        public int getVersionCode() {
            return versionCode;
        }

        @JsonProperty("versionCode")
        public void setVersionCode(int versionCode) {
            this.versionCode = versionCode;
        }


        @JsonProperty("tid")
        public String getTid() {
            return tid;
        }

        @JsonProperty("tid")
        public void setTid(String tid) {
            this.tid = tid;
        }

        @JsonProperty("token")
        public String getToken() {
            return token;
        }

        @JsonProperty("token")
        public void setToken(String token) {
            this.token = token;
        }

    }