package com.swiggy.api.daily.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "type",
        "quantity",
        "spin",
        "serviceLine",
        "store_id"
})
public class Item {

    @JsonProperty("type")
    private String type;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("spin")
    private String spin;
    @JsonProperty("serviceLine")
    private String serviceLine;
    @JsonProperty("store_id")
    private Integer storeId;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("spin")
    public String getSpin() {
        return spin;
    }

    @JsonProperty("spin")
    public void setSpin(String spin) {
        this.spin = spin;
    }

    @JsonProperty("serviceLine")
    public String getServiceLine() {
        return serviceLine;
    }

    @JsonProperty("serviceLine")
    public void setServiceLine(String serviceLine) {
        this.serviceLine = serviceLine;
    }

    @JsonProperty("store_id")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("quantity", quantity).append("spin", spin).append("serviceLine", serviceLine).append("storeId", storeId).toString();
    }

}