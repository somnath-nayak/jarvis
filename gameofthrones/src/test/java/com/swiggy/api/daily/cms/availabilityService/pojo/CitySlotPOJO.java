package com.swiggy.api.daily.cms.availabilityService.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "city_id",
        "close_time",
        "day",
        "open_time"
})
public class CitySlotPOJO {

    @JsonProperty("city_id")
    private Integer cityId;
    @JsonProperty("close_time")
    private Integer closeTime;
    @JsonProperty("day")
    private String day;
    @JsonProperty("open_time")
    private Integer openTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public CitySlotPOJO() {
    }

    /**
     *
     * @param closeTime
     * @param cityId
     * @param openTime
     * @param day
     */
    public CitySlotPOJO(Integer cityId, Integer closeTime, String day, Integer openTime) {
        super();
        this.cityId = cityId;
        this.closeTime = closeTime;
        this.day = day;
        this.openTime = openTime;
    }

    @JsonProperty("city_id")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("city_id")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public CitySlotPOJO withCityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("close_time")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("close_time")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public CitySlotPOJO withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public CitySlotPOJO withDay(String day) {
        this.day = day;
        return this;
    }

    @JsonProperty("open_time")
    public Integer getOpenTime() {
        return openTime;
    }

    @JsonProperty("open_time")
    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public CitySlotPOJO withOpenTime(Integer openTime) {
        this.openTime = openTime;
        return this;
    }

    public CitySlotPOJO setDefault(Integer cityId, Integer open_time, Integer close_time, String day){
        return this.withCityId(cityId).withCloseTime(close_time).withDay(day).withOpenTime(open_time);
    }

}
