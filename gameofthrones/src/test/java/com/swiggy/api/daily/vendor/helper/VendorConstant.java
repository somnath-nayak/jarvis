package com.swiggy.api.daily.vendor.helper;

public interface VendorConstant {

    String restaurantID = "5462";
    String rest_9990 = "9990";
    String invalidrestaurantID = "38274362";
    String invalidCityID = "234567";
    String message_invalidRestID = "Invalid restaurant id";
    String random_0 = "0";
    int random0 = 0;
    String random_1 = "1";
    String random_2 = "-1";
    String random_3 = "-2";
    int random_10 = 10;
    String statusMessage_Login_Successful = "Login Successful";
    String statusMessage_Invalid_Input = "Invalid Input";
    String statusMessage_Invalid_Password = "Invalid Password";
    String rest_id = "13584";
    int store_id = 13584;
    String password = "123456";
    String attribute = "batch placing";
    String ID = "169";
    String daily = "daily";
    String code_failure = "failure";
    String code = "success";
    String message  = "done successfully";
    String RESTAURANT = "RESTAURANT";
    String random = "random";
    String Success = "Success";
    String statusCode_5 = "-5";
    String VENDOR_PASSWORD = "El3ment@ry";
    String VENDOR_USERNAME = "5271";
    String invalidOrderID = "56312836123129361";
    String Action_Failed = "Action Failed";
    String source = "vendor";
    String DAILY_STORE = "DAILY_STORE";
    String catalogDB = "catalog";
    String select_item_id = "select * from slot_info where entity_id = '";
    String delete_item_id = "delete from slot_info where entity_id = '";
    String delete_rest_holiday_slots = "delete from restaurant_holiday_slots where from_time = '";
    String select_rest_holiday_slots = "select * from restaurant_holiday_slots where from_time = '";
    String delete_rest_normal_slots = "delete from restaurant_slots where id = '";
    String deleteSlot = "delete from restaurant_slots where updated_by = '";
    String deleteRestSlot = "delete from restaurant_holiday_slots where updated_by = '";
    String limit = " limit 1";
    String endQuotes = "' ";
    String select_multiple_item_id = "select * from slot_info ;";
    String timePattern2 = "23:59:59";
    String timePattern1 = "00:01:00";
    String MON = "MON";
    String TUE = "TUE";
    String WED = "WED";
    String THU = "THU";
    String FRI = "FRI";
    String SAT = "SAT";
    String SUN = "SUN";
    String entityID = "1221";
    String close_time = "2359";
    String close_time2 = "2200";
    String open_time = "1200";
    String open_time2 = "1600";
    String open__time = "700";
    String to_time = " and to_time = '";
    String end = ";";
    String toTime = "23:59:59";
    String from_time = "00:01:00";
    String updated_by = "{\"source\":\"Neha-vendor-automation\",\"meta\":{}}";
    String VVO_PLACER_SERVICE = "daily";
    String PLACING_FSM_SERVICE = "placing-fsm-service";
    String NEW_ORDERS_VVO_PLACER_SERVICE_QUEUE = "vendor.vvo-placer.new-orders";
    String NEW_ORDERS_RMS_QUEUE = "vendor.rms.new_orders";
    String NEW_ORDERS_VVO_PLACER_SERVICE_EXCHANGE = "placing_service_new_order_exchange";
    String cityId = "1,2,3,7,9";
    String restaurants_and_restaurants_metadata_tables = "select * from restaurants r, restaurants_metadata rm where r.id = rm.restaurant_id and r.city_code = ";
    String rest_ID = " and r.id = ";
}
