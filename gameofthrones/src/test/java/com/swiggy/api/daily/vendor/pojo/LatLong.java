package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class LatLong {
    
    @JsonProperty("lat")
    private String lat;
    
    @JsonProperty("long")
    private  String lon ;
    
    public LatLong(){
    
    }
    public LatLong(String lat ,String lon){
        this.lat = lat;
        this.lon = lon;
    }
    
    public String getLat() {
        return lat;
    }
    
    public void setLat(String lat) {
        this.lat = lat;
    }
    
    public String getLon() {
        return lon;
    }
    
    public void setLon(String lon) {
        this.lon = lon;
    }
    
    public LatLong setDefault(){
        this.lat ="12.934581";
         this.lon ="77.61628199999996";
         return this;
    }
    
    public LatLong withLat(String lat){
        this.lat = lat;
        return this;
    }
    
    public LatLong withLong(String lon){
        this.lon = lon;
        return this;
    }
}
