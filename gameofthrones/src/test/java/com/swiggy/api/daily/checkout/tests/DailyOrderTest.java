package com.swiggy.api.daily.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.checkout.helper.*;
import com.swiggy.api.daily.cms.helper.CMSCommonHelper;
import com.swiggy.api.sf.checkout.helper.paas.PaasWalletUtils;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DailyOrderTest {

    DailyCheckoutOrderHelper orderHelper=new DailyCheckoutOrderHelper();
    static DailyCheckoutCommonUtils utils=new DailyCheckoutCommonUtils();
    static CheckoutCartHelper cartHelper=new CheckoutCartHelper();
    DailyCheckoutOrderValidator orderValidator=new DailyCheckoutOrderValidator();
    PaasWalletUtils walletUtils=new PaasWalletUtils();


    String orderJobId;
    String transactionId;
    static HashMap<String, String> userInfo;
    static HashMap<String, String> userInfoHeader;
    String intAmountAsString;
    String txnAmount;
    String cartId;
    Processor cartProcessor;
    private String paymentLinkId;

    final String userAgent;
    final String versionCode;
    final String cartType;
    final String paymentMethod;


    public DailyOrderTest(String userAgent,String versionCode,String cartType,String paymentMethod) {
        super();
        this.userAgent =userAgent;
        this.versionCode =versionCode;
        this.cartType=cartType;
        this.paymentMethod=paymentMethod;
    }


    static{
        userInfo=utils.dailyLogin(DailyCheckoutConstants.USER_MOBILE2,DailyCheckoutConstants.USER_PASSWORD2);
        userInfoHeader=utils.getUserInfoHeader(userInfo);
    }


    @Test()
    public void cartCreate(){
        cartProcessor=cartHelper.createCartAndModifyPrice(userInfoHeader,cartType,DailyCheckoutConstants.CITY_ID,DailyCheckoutConstants.STOREID);
        utils.validateErrorCode(cartProcessor);
        String cartRes=cartProcessor.ResponseValidator.GetBodyAsText();
        cartId= JsonPath.read(cartRes, "$.data.cartId").toString()
                .replaceAll("\\[|\\]|\"", "");

        String amount=cartProcessor.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.paymentToBeCollected.paymentToBeCollected")
                .replaceAll("\\[|\\]|\"", "");

        System.out.println("Cart amount"+amount);
    }

    @Test(dependsOnMethods = "cartCreate")
    public void cartGeneratePaymentLink(){
        Processor paymentLinkProcessor=orderHelper.generatePlPaymentLink(userInfo.get("tid"),userInfo.get("token"),cartId);
        utils.validateStatusCode(paymentLinkProcessor);
        paymentLinkId=paymentLinkProcessor.ResponseValidator.GetNodeValue("$.data");
        Assert.assertNotNull(paymentLinkId,"data is null in PaymentLink's response");
    }


    @Test(dependsOnMethods = "cartGeneratePaymentLink")
    public void cartGetAllPaymentOptions(){
        System.out.println("paymentLinkId------------------>"+paymentLinkId);
        Processor paymentLinkProcessor=orderHelper.getAllPaymentOptions(userInfo.get("tid"),userInfo.get("token"),paymentLinkId);
        utils.validateStatusCode(paymentLinkProcessor);
    }


    @Test(dependsOnMethods = "cartCreate")
    public void dailyPlaceOrderTest(){
        String amount=cartProcessor.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.paymentToBeCollected.paymentToBeCollected")
                .replaceAll("\\[|\\]|\"", "");
        intAmountAsString=Integer.toString((int) Double.parseDouble(amount));
        txnAmount=Double.toString(Double.parseDouble(amount));
        double balance=0;
        if(!paymentMethod.equalsIgnoreCase("cash")){
            balance=walletUtils.linkAndCheckWalletBalance(userInfo.get("tid"),userInfo.get("token"),paymentMethod);
            System.out.println("Balance--------------->"+balance);
            System.out.println("cart amount--------------->"+amount);
            if(balance<=Double.parseDouble(amount)){
                 throw new SkipException(paymentMethod+" Wallet has not sufficient balance");
            }
        }

        Processor orderProcessor=orderHelper.dailyCheckoutCreatePlOrder(userInfo.get("tid"),userInfo.get("token")
                                                            ,cartProcessor,paymentMethod);
        utils.validateStatusCode(orderProcessor);

        orderJobId=orderHelper.getOrderJobId(orderProcessor);
        transactionId=orderHelper.getTransactionId(orderProcessor);

        orderValidator.validateOrderDataPart(orderProcessor);
        orderValidator.validateOrderResponse(orderProcessor,DailyCheckoutConstants.PLACED);
        orderValidator.validateOrderPaymentInfo(orderProcessor,DailyCheckoutConstants.PENDING,intAmountAsString);
        orderValidator.validateOrderMetadata(orderProcessor,cartId,getCartSubType(cartType),intAmountAsString);
        orderValidator.validateTransactionPaymentDetails(orderProcessor,DailyCheckoutConstants.SUCCESS,DailyCheckoutConstants.FALSE);

        //DB validation- payment transaction v2 - transaction id
        List<Map<String, Object>> dbObject =utils.getDBTransactionDetails("payment_transaction_v2",transactionId);
        orderValidator.validateDbTransactionDetails(dbObject,paymentMethod,userInfo.get("userId"),"success",txnAmount);

    }

    @Test (dependsOnMethods ="dailyPlaceOrderTest")
    public void getDailyOrderOmsTest(){
        Processor getOrderOmsProcessor=orderHelper.getDailyOrderOMS(orderJobId);
        utils.validateStatusCode(getOrderOmsProcessor);
        System.out.println("response------->"+getOrderOmsProcessor.ResponseValidator.GetBodyAsText());


        orderValidator.validateOrderDataPart(getOrderOmsProcessor);
        orderValidator.validateOrderResponse(getOrderOmsProcessor,DailyCheckoutConstants.CONFIRMED);
        orderValidator.validateOrderPaymentInfo(getOrderOmsProcessor,DailyCheckoutConstants.SUCCESSFUL,intAmountAsString);
        orderValidator.validateOrderMetadata(getOrderOmsProcessor,cartId,getCartSubType(cartType),intAmountAsString);
   //     orderValidator.validateTransactionPaymentDetails(getOrderOmsProcessor,
   //             DailyCheckoutConstants.SUCCESS,DailyCheckoutConstants.FALSE);
    }

    @Test (dependsOnMethods ="dailyPlaceOrderTest" )
    public void getPlDailyOrderTest(){
        Processor getPlOrderProcessor=orderHelper.getDailyPLOrder(userInfo.get("tid"),userInfo.get("token"),orderJobId);
        utils.validateStatusCode(getPlOrderProcessor);
        System.out.println("Refund response------->"+getPlOrderProcessor.ResponseValidator.GetBodyAsText());

        orderValidator.validateOrderDataPart(getPlOrderProcessor);
        orderValidator.validateOrderResponse(getPlOrderProcessor,DailyCheckoutConstants.CONFIRMED);
        orderValidator.validateOrderPaymentInfo(getPlOrderProcessor,DailyCheckoutConstants.SUCCESSFUL,intAmountAsString);
        orderValidator.validateOrderMetadata(getPlOrderProcessor,cartId,getCartSubType(cartType),intAmountAsString);
   //     orderValidator.validateTransactionPaymentDetails(getPlOrderProcessor,
   //             DailyCheckoutConstants.SUCCESS,DailyCheckoutConstants.FALSE);
    }

    @Test (dependsOnMethods ="dailyPlaceOrderTest" )
    public void getDailyOrderDetails(){
        Processor getPlOrderProcessor=orderHelper.getDailyOrderDetails(userInfo.get("tid"),userInfo.get("token"),orderJobId);
        utils.validateStatusCode(getPlOrderProcessor);
    }

    @Test (dependsOnMethods ="dailyPlaceOrderTest" )
    public void refundOmsOrderTest() {
        Processor refundOrderProcessor = orderHelper.dailyOmsOrderRefund(transactionId, txnAmount);
        utils.validateStatusCode(refundOrderProcessor);
        System.out.println("refund response------->" + refundOrderProcessor.ResponseValidator.GetBodyAsText());

        //DB validation
        List<Map<String, Object>> dbObject =utils.getDBTransactionDetails("refund_transaction_v2",transactionId);
        if (paymentMethod==DailyCheckoutConstants.CASH){
        orderValidator.validateDbRefundTransactionDetails(dbObject,paymentMethod,"SUCCESSFUL",txnAmount);
        }else{
            orderValidator.validateDbRefundTransactionDetails(dbObject,paymentMethod,"INIT",txnAmount);
        }
    }


    @Test (dependsOnMethods ="refundOmsOrderTest" )
    public void getPlDailyOrderRefundTest(){
        Processor getPlOrderProcessor=orderHelper.getDailyPLOrder(userInfo.get("tid"),userInfo.get("token"),"167150522519");
        utils.validateStatusCode(getPlOrderProcessor);
        System.out.println(" response------->"+getPlOrderProcessor.ResponseValidator.GetBodyAsText());

        orderValidator.validateOrderDataPart(getPlOrderProcessor);
        orderValidator.validateOrderResponse(getPlOrderProcessor,DailyCheckoutConstants.CONFIRMED);

        orderValidator.validateRefundOrderPaymentInfo(1,getPlOrderProcessor,"REFUND_INITIATED",intAmountAsString);
        orderValidator.validateOrderMetadata(getPlOrderProcessor,cartId,getCartSubType(cartType),intAmountAsString);
    }



    private String getCartSubType(String cartType){
        if (cartType.equalsIgnoreCase(DailyCheckoutConstants.PRODUCT)){
            return DailyCheckoutConstants.ORDER_SUB_TYPE_PREORDER;
        } else {
           return DailyCheckoutConstants.ORDER_SUB_TYPE_SUBSCRIPTION;
        }
    }
    }
