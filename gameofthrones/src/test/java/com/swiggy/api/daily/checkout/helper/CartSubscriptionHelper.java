package com.swiggy.api.daily.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.checkout.pojo.slotdetails.CustomerLocation;
import com.swiggy.api.daily.checkout.pojo.slotdetails.Request;
import com.swiggy.api.daily.checkout.pojo.slotdetails.SlotDetailsPayload;
import com.swiggy.api.daily.checkout.pojo.slotdetails.TimeSlot;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class CartSubscriptionHelper extends CheckoutCartHelper {

    MealSlotHelper mealSlotHelper;
    Initialize gameofthrones = new Initialize();


    public Processor getSlotServiceProcessor(HashMap<String, String> header, String payload) {
        GameOfThronesService service = new GameOfThronesService("deliverycerebro", "getlots", gameofthrones);
        return new Processor(service, header, new String[]{payload});
    }


    /**
     * This method is used to get the meal slot id based on period.
     * Period might be lunch or dinner
     */
    public String getMealSlotId(String period) {
        mealSlotHelper = new MealSlotHelper();
        Processor processor = null;

        try {
            if (period.equalsIgnoreCase("lunch")) {
                processor = mealSlotHelper.getMealSlots(DailyCheckoutConstants.CITY_ID, String.valueOf(DateUtility.getCurrentDateInMilisecond()));
            } else if (period.equalsIgnoreCase("dinner")) {
                processor = mealSlotHelper.getMealSlots(DailyCheckoutConstants.CITY_ID, String.valueOf(DateUtility.getCurrentDateInMilisecond()));
            } else {
                System.out.println("Please enter the valid period");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        String cartResponse = processor.ResponseValidator.GetBodyAsText();
        mealSlotID = JsonPath.read(cartResponse, "$.data[0].id").toString().replace("[", "").replace("]", "");

        return mealSlotID;
    }


    /**
     * This method is used to get the slot details from cerebro
     * It needs the plan type and mealslot response from cart api
     */
    public SlotDetailsPayload getCerebroSlots(String planType, String mealSlotResponse, String tenure) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");

        SlotDetailsPayload slotDetailsPayload = new SlotDetailsPayload();
        Request request = new Request();
        CustomerLocation customerLocation = new CustomerLocation();
        TimeSlot timeSlot = new TimeSlot();
        List<Request> requestArray = new ArrayList<>();

        List<CustomerLocation> locationlist = new ArrayList<CustomerLocation>();
        List<String> storeIdList = new ArrayList<String>();


        storeIdList.add(String.valueOf(DailyCheckoutConstants.STORE_ID));

        customerLocation = getCustomerLocationForCerebro();
        locationlist.add(customerLocation);

        timeSlot = getTimeSlots(planType, mealSlotResponse, tenure);


        request.setTimeSlot(timeSlot);
        request.setCustomerLocations(locationlist);
        request.setStoreIds(storeIdList);
        request.setCityId(Integer.parseInt(DailyCheckoutConstants.CITY_ID));
        request.setExclusionDates(new ArrayList<>());

        requestArray.add(request);
        slotDetailsPayload.setRequests(requestArray);
        return slotDetailsPayload;
    }


    /**
     * This method is used to get the lat and lng for cerebro api
     * It gets CustomerLocation based on constant file
     */

    private CustomerLocation getCustomerLocationForCerebro() {
        CustomerLocation location = new CustomerLocation();
        location.setLat(Float.parseFloat(DailyCheckoutConstants.LAT));
        location.setLng(Float.parseFloat(DailyCheckoutConstants.LNG));

        return location;
    }


    /**
     * This method is used to get the time slots for cerebro request payload
     * Based on plan type it fetches start and end from meal slot response and convert that to epoch time
     */
    private TimeSlot getTimeSlots(String planType, String mealSlotResponse, String tenure) {
        TimeSlot timeSlot = new TimeSlot();

        Long startTimeEpoch = null;
        Long endTimeEpoch;

        try {
            String startDateFromResponse = JsonPath.read(mealSlotResponse, "$.slot_date").toString().replace("[", "").replace("]", "");
            String startTimeFromResponse = JsonPath.read(mealSlotResponse, "$.start_time").toString().replace("[", "").replace("]", "");

            String endTimeFromResponse = JsonPath.read(mealSlotResponse, "$.end_time").toString().replace("[", "").replace("]", "");

            SimpleDateFormat startTime = new SimpleDateFormat("DD-MM-YYYY HHmm");
            Date date = startTime.parse(startDateFromResponse + " " + startTimeFromResponse);
            startTimeEpoch = date.getTime();


            Calendar calender = Calendar.getInstance();
            calender.setTime(startTime.parse(startDateFromResponse));

            if (planType.equalsIgnoreCase("plan")) {
                calender.add(Calendar.DATE, Integer.parseInt(tenure));
            }

            endTimeEpoch = calender.getTime().getTime();

            timeSlot.setStartTime(String.valueOf(startTimeEpoch));
            timeSlot.setEndTime(String.valueOf(endTimeEpoch));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeSlot;
    }

}
