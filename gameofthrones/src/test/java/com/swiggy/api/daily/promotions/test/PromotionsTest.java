package com.swiggy.api.daily.promotions.test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.promotions.dp.PromotionsDP;
import com.swiggy.api.daily.promotions.helper.PromotionConstants;
import com.swiggy.api.daily.promotions.helper.PromotionsHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.apache.tomcat.jni.Proc;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;

public class PromotionsTest {
    PromotionsHelper promotionsHelper = new PromotionsHelper();
    
    @Test(dataProvider = "createPromotionOnSingleSKUFlatTypeDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionOnSingleSKUFlatTypeTest(Processor createResponse, String storeId, String skuId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");

        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String shareType = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.shareType");
        String shareValue = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.shareValue");
        String storeRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[1]").toString();
        String actionType = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.actions..actionType");
        String createdBy = JsonPath.read(storeBody, "$.data.createdBy").toString();
        String updatedBy = JsonPath.read(storeBody, "$.data.updatedBy").toString();
        String promotionType = JsonPath.read(storeBody, "$.data.promotionType").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(promotionType.contains(PromotionConstants.promotionType[0]), true);
        softAssert.assertEquals(storeRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeRuleEntry.contains(storeId), true);
        softAssert.assertEquals(skuRuleEntry.contains("SKU_ID"), true);
        softAssert.assertEquals(actionType.contains(PromotionConstants.actionType[0]), true);
        softAssert.assertEquals(skuRuleEntry.contains(skuId), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertEquals(createdBy, PromotionConstants.createdBy);
        softAssert.assertEquals(updatedBy, PromotionConstants.updatedBy);
        softAssert.assertEquals(shareType,PromotionConstants.shareType[0]);
        softAssert.assertEquals(shareValue,PromotionConstants.shareValue);
        softAssert.assertAll();
    }

    // Defaulty should take tenant type as DAILY if not passed or null
    @Test(dataProvider = "createPromotionCheckDefaultDAILYIfRequestingWithOutTenantTypeDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckDefaultDAILYIfRequestingWithOutTenantTypeTest(Processor createResponse, String storeId, String skuId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");

        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String shareType = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.shareType");
        String shareValue = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.shareValue");
        String storeRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[1]").toString();
        String actionType = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.actions..actionType");
        String createdBy = JsonPath.read(storeBody, "$.data.createdBy").toString();
        String updatedBy = JsonPath.read(storeBody, "$.data.updatedBy").toString();
        String promotionType = JsonPath.read(storeBody, "$.data.promotionType").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(promotionType.contains(PromotionConstants.promotionType[0]), true);
        softAssert.assertEquals(storeRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeRuleEntry.contains(storeId), true);
        softAssert.assertEquals(skuRuleEntry.contains("SKU_ID"), true);
        softAssert.assertEquals(actionType.contains(PromotionConstants.actionType[0]), true);
        softAssert.assertEquals(skuRuleEntry.contains(skuId), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertEquals(createdBy, PromotionConstants.createdBy);
        softAssert.assertEquals(updatedBy, PromotionConstants.updatedBy);
        softAssert.assertEquals(shareType,PromotionConstants.shareType[0]);
        softAssert.assertEquals(shareValue,PromotionConstants.shareValue);
        softAssert.assertAll();
    }


    @Test(dataProvider = "createPromotionCheckTenantDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckTenantTest(Processor createResponse, Processor createResponseWhiteSpace,Processor createResponseRandomString) {

        String status = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String error = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");

        String statusWithWhiteSpace = createResponseWhiteSpace.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String errorWithWhiteSpace = createResponseWhiteSpace.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");

        String statusWithRandomString = createResponseRandomString.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String errorWithRandomString = createResponseRandomString.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");


        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(error, "Bad Request");
        softAssert.assertEquals(status, "400");
        softAssert.assertEquals(errorWithWhiteSpace, "Bad Request");
        softAssert.assertEquals(statusWithWhiteSpace, "400");
        softAssert.assertEquals(errorWithRandomString, "Bad Request");
        softAssert.assertEquals(statusWithRandomString, "400");

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPDPCopyTextFieldDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPDPCopyTextFieldTest(Processor createResponse, Processor createResponseWhiteSpace) {
        int data = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        int dataEmpty = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        //TODO
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(data,0);
        softAssert.assertEquals(dataEmpty,0);

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPDPCopyTextFieldMaxLengthDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPDPCopyTextFieldMaxLengthTest(Processor createResponse) {
       String status = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String error = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(status, "400");
        softAssert.assertEquals(error, "Bad Request");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckStoreAndSKUValueAsEmptyStringDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckStoreAndSKUValueAsEmptyStringTest(Processor createResponse) {
        //TODO
        String promotionData = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(promotionData,"{}");

        softAssert.assertAll();
    }


    @Test(dataProvider = "createPromotionForSameSPINAndSameStoreWithSlotAndWithOutSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionForSameSPINAndSameStoreWithSlotAndWithOutSlotTest(Processor createResponse, Processor createResponseWithSlot) {
       String id = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
        String statusMessageWithSlot = createResponseWithSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(id,"{}");
        softAssert.assertEquals(statusMessageWithSlot.contains(PromotionConstants.duplicatePromotion),true );

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckStoreAndSKUValueDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckStoreAndSKUValueTest(Processor createResponse) {
        String status = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String error = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");


        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(error, "Bad Request");
        softAssert.assertEquals(status, "400");

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPriceFieldAsEmptyStringDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPriceFieldAsEmptyStringTest(Processor createResponse, Processor createResponseWithOutPrice) {
        String status = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String error = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");
        String statusTwo = createResponseWithOutPrice.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.status");
        String errorTwo = createResponseWithOutPrice.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.error");

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(error, "Bad Request");
        softAssert.assertEquals(status, "400");
        softAssert.assertEquals(errorTwo, "Bad Request");
        softAssert.assertEquals(statusTwo, "400");

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPriceFieldAsZeroValueDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPriceFieldAsZeroValueTest(Processor createResponse) {
        int statusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(statusCode, 0);
        softAssert.assertEquals(statusMessage, PromotionConstants.invalidDiscont);
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPriceFieldAsNegativeValueDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPriceFieldAsNegativeValueTest(Processor createResponse) {
        String message = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(message, PromotionConstants.invalidDiscont);

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionCheckPriceFieldForPercentMoreThanHundredDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionCheckPriceFieldForPercentMoreThanHundredTest(Processor createResponse , Processor createResponseDiscount) {
        String message = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        String messageTwo = createResponseDiscount.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(message, PromotionConstants.invalidDiscont);
        softAssert.assertEquals(messageTwo, PromotionConstants.invalidDiscont);

        softAssert.assertAll();
    }

    @Test(dataProvider = "createPromotionOnSingleSKUPercentageTypeDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createPromotionOnSingleSKUPercentageTypeTest(Processor createResponse, Processor planListingResponse, Processor mealListingResponse, Processor updateResponse,
                                                             String storeId, String skuId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String actionType = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.actions..actionType");
        String ruleEntities = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String storeRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[1]").toString();


        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(storeRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(actionType.contains(PromotionConstants.actionType[1]), true);
        softAssert.assertEquals(storeRuleEntry.contains(storeId), true);
        softAssert.assertEquals(skuRuleEntry.contains("SKU_ID"), true);
        softAssert.assertEquals(skuRuleEntry.contains(skuId), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntities, "null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createMultiplePromotionOnSingleSKUDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createMultiplePromotionOnSingleSKUTest(Processor createResponse, Processor createSecondResponse, String storeId, String[] skuId, String secondStoreId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String ruleEntities = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String storeRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[1]").toString();

        int createSecondStatusCode = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeSecondPromoFlag = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheckSecondPromo = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String ruleEntitiesSecondPromo = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBodySecondPromo = createSecondResponse.ResponseValidator.GetBodyAsText();
        String storeRuleEntrySecondPromo = JsonPath.read(storeBodySecondPromo, "$.data..rules[0]").toString();
        String skuRuleEntrySecondPromo = JsonPath.read(storeBodySecondPromo, "$.data..rules[1]").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(storeRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeRuleEntry.contains(storeId), true);
        softAssert.assertEquals(skuRuleEntry.contains("SKU_ID"), true);
        softAssert.assertEquals(skuRuleEntry.contains(skuId[0]), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntities, "null");

        softAssert.assertEquals(storeRuleEntrySecondPromo.contains("STORE_ID"), true);
        softAssert.assertEquals(storeRuleEntrySecondPromo.contains(secondStoreId), true);
        softAssert.assertEquals(skuRuleEntrySecondPromo.contains("SKU_ID"), true);
        softAssert.assertEquals(skuRuleEntrySecondPromo.contains(skuId[0]), true);
        softAssert.assertEquals(createSecondStatusCode, 1);
        softAssert.assertEquals(activeSecondPromoFlag, "true");
        softAssert.assertEquals(tenantCheckSecondPromo, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntitiesSecondPromo, "null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createSKULevelPromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createSKULevelPromotionTest(Processor createResponse, String[] skuId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String ruleEntities = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(skuRuleEntry.contains("SKU_ID"), true);
        softAssert.assertEquals(skuRuleEntry.contains(skuId[0]), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntities, "null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "createStoreLevelPromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createStoreLevelPromotionTest(Processor createResponse, String storeId) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String ruleEntities = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String skuRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(skuRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(skuRuleEntry.contains(storeId), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntities, "null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createMultipleStoreLevelPromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void createMultipleStoreLevelPromotionTest(Processor createResponse, String[] storeId, String[] storeIdTwo, String[] storeIdThree) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String activeFlag = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.active");
        String tenantCheck = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tenant");
        String ruleEntities = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.rules");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String storeOneRuleEntry = JsonPath.read(storeBody, "$.data..rules[0]").toString();
        String storeTwoRuleEntry = JsonPath.read(storeBody, "$.data..rules[1]").toString();
        String storeThreeRuleEntry = JsonPath.read(storeBody, "$.data..rules[2]").toString();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(storeOneRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeOneRuleEntry.contains(storeId[0]), true);
        softAssert.assertEquals(storeTwoRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeTwoRuleEntry.contains(storeIdTwo[0]), true);
        softAssert.assertEquals(storeThreeRuleEntry.contains("STORE_ID"), true);
        softAssert.assertEquals(storeThreeRuleEntry.contains(storeIdThree[0]), true);
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(activeFlag, "true");
        softAssert.assertEquals(tenantCheck, PromotionConstants.tenant);
        softAssert.assertNotEquals(ruleEntities, "null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "checkCreatedByAndUpdatedByEmptyStringDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatedByAndUpdatedByEmptyStringTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String storeBody = createResponse.ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(storeBody, "$.data").toString();
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(data, "null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatedAtAndUpdatedATDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatedAtAndUpdatedATTest(Processor createResponse, String createdDate, Processor getActiveResponse, String promotionId) {
        int createStatusCode = getActiveResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String storeBody = getActiveResponse.ResponseValidator.GetBodyAsText();
        String createdAt = JsonPath.read(storeBody, "$.data.[?(@.id=="+promotionId+")].createdAt").toString().replace("000+0000", "Z").replace("[\"","").replace("\"]","");
        String updatedAt = JsonPath.read(storeBody, "$.data.[?(@.id==\"+promotionId+)].updatedAt").toString().replace("000+0000", "Z").replace("[\"","").replace("\"]","");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createdAt, createdDate);
        softAssert.assertEquals(updatedAt, createdDate);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatedAtAndUpdatedATWithRandomStringDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatedAtAndUpdatedATWithRandomStringTest(String status) {
        Assert.assertEquals(status.contains("400"), true);

    }

    @Test(dataProvider = "checkCreatedAtAndUpdatedATWithEmptyStringDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatedAtAndUpdatedATWithEmptyStringTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String body = createResponse.ResponseValidator.GetNodeValue("$.data");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(statusMessage.contains("could not execute statement; SQL [n/a]; constraint [null]"), false);
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(body, null);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 0);
        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreWithSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreWithSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 0);
        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresWithSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfDiffStoresWithSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtConflictingDateDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtConflictingDateTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 0);
        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtDiffDateDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreButAtDiffDateTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        //softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion),true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtSameDateButDiffSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtSameDateButDiffSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 0);
        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtDiffDateButSameSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateBothFlatAndPercentageForSameSKUOfAStoreAtDiffDateButSameSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        //softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion),true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        //softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion),true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateAndSameSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateMultiplePromotionForDiffSKUOfAStoreAtSameDateAndSameSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
        //softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion),true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateMultiplePromotionForSameSKUOfAStoreAtSameDateAndSameSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateMultiplePromotionForSameSKUOfAStoreAtSameDateAndSameSlotTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 0);
        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreateMultiplePromotionForSameSKUOfAStoreAtDiffDateDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreateMultiplePromotionForSameSKUOfAStoreAtDiffDateTest(Processor createResponse, Processor createSecondResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String dataOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        int statusCodeOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessageOfSecondCamp = createSecondResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);
        softAssert.assertNotEquals(dataOfSecondCamp, null);
        softAssert.assertEquals(statusCodeOfSecondCamp, 1);
//        softAssert.assertEquals(statusMessageOfSecondCamp.contains(PromotionConstants.duplicatePromotion),true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionForOverLappingSlotForSameDayForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionForOverLappingSlotForSameDayForASKUOfAStoreTest(Processor createResponse,int status) {
        Assert.assertEquals(status,0,"Creating promotion with the over lapping time slot of same day ");
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValue("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionForLateNightSlotForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionForLateNightSlotForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionWithMultipleSlotForADayForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionWithMultipleSlotForADayForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionWithMultipleSlotForDifferentDaysForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionWithMultipleSlotForDifferentDaysForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionWithSameSlotForDifferentDaysForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionWithSameSlotForDifferentDaysForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionDayALLWithOtherDaysSlotForDifferentTimeForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionDayALLWithOtherDaysSlotForDifferentTimeForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionDayALLWithOtherDaysSlotAtOverlappingTimeForASKUOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionDayALLWithOtherDaysSlotAtOverlappingTimeForASKUOfAStoreTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValue("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkPromotionTypeInCreatePromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkPromotionTypeInCreatePromotionTest(Processor createResponse, Processor getActiveResponse, String promotionTwo) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String promotionType= getActiveResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id=="+promotionTwo+")].promotionType").replace("[\"","").replace("\"]","");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertEquals(promotionType,PromotionConstants.promotionType[0]);


        softAssert.assertAll();
    }

    @Test(dataProvider = "checkPromotionTypeAsEmptyStringInCreatePromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkPromotionTypeAsEmptyStringInCreatePromotionTest(String promotionTwoData) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(promotionTwoData, "400");
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkPromotionNameInCreatePromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkPromotionNameInCreatePromotionTest(String promotionTwoData) {
        Assert.assertEquals(promotionTwoData, "400", "able to create promotion without name field");

    }


    @Test(dataProvider = "checkPromotionNameAsEmptyStringInCreatePromotionDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkPromotionNameAsEmptyStringInCreatePromotionTest(Processor createResponse) {
        SoftAssert softAssert = new SoftAssert();
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValue("$.data");
        String statusMessage = createResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(data, null);
        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValue("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 0);
        softAssert.assertEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoAndSKUStoresDiffDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void checkCreatePromotionWithMultipleSKUSWhereOneOfASKUHaveActivePromoAndSKUStoresDiffTest(Processor createResponse) {
        int createStatusCode = createResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = createResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(createStatusCode, 1);
        softAssert.assertNotEquals(data, null);

        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckPlanMealListingAndCartWhenPromotionIsActiveForAGivenSKUDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanMealListingAndCartWhenPromotionIsActiveForAGivenSKUTest(Processor planListingResponse, Processor mealListingResponse, Processor cartResponse) {
        SoftAssert softAssert = new SoftAssert();

        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String planData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        softAssert.assertEquals(statusCodeOfCart, 1);
        softAssert.assertEquals(cartData, "{}");

        softAssert.assertEquals(statusCodeOfMeal, 1);
        softAssert.assertEquals(mealData, "{}");
        softAssert.assertEquals(statusCodeOfPlan, 1);
        softAssert.assertEquals(planData, "{}");

        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckPlanListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // plan
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionData, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenPercentPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");
        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);

        softAssert.assertAll();
    }
// Flat discount

    @Test(dataProvider = "CheckPlanListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // plan
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String mealBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(mealBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenFlatPromotionIsActiveForASKUOfStoreAndHittingWithMultipleSKUsOfAStoreTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();


        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);

        softAssert.assertAll();
    }


    // Same SKU in Multi Store Flat discount

    @Test(dataProvider = "CheckPlanListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // plan
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String typeInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].promotionType").replace("[\"", "").replace("\"]", "");

        String mealBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(mealBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(mealBody, "$.data.stores..storeId").toString();
        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(typeInPromotionDataOfMeal, PromotionConstants.promotionType[0]);
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenFlatPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");

        softAssert.assertAll();
    }


    // Same SKU in Multi Store %age discount

    @Test(dataProvider = "CheckPlanListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // plan
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionData, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String typeInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].promotionType").replace("[\"", "").replace("\"]", "");

        String mealBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(mealBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(mealBody, "$.data.stores..storeId").toString();
        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(typeInPromotionDataOfMeal, PromotionConstants.promotionType[0]);
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenPercentPromotionIsActiveForASKUAndHittingWithBothTheStoresInWhichSameSKUIsPresentTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");

        softAssert.assertAll();
    }


    // Multi Store FLAT discount higher than SKU's price

    @Test(dataProvider = "CheckPlanListingWhenFlatPriceIsHigherThanSKUsPriceDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatPriceIsHigherThanSKUsPriceTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // plan
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.zeroDiscount);
        softAssert.assertEquals(idInPromotionData, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenFlatPriceIsHigherThanSKUsPriceDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatPriceIsHigherThanSKUsPriceTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String typeInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].promotionType").replace("[\"", "").replace("\"]", "");

        String mealBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(mealBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(mealBody, "$.data.stores..storeId").toString();
        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(typeInPromotionDataOfMeal, PromotionConstants.promotionType[0]);
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.zeroDiscount);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);
        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenFlatPriceIsHigherThanSKUsPriceDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenFlatPriceIsHigherThanSKUsPriceTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String noPromotionStore) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.zeroDiscount);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false, "get Store on which no promotion is created in automation");

        softAssert.assertAll();
    }

    // Multi Store FLAT & %age both for a SKU in different stores
    @Test(dataProvider = "CheckPlanListingWhenFlatAndPercentBothForASKUInDifferentStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatAndPercentBothForASKUInDifferentStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String percentPromotionStore, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String discountOnItemForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForPercent, promotionIdForPercent);
        softAssert.assertEquals(discountOnItemForPercent, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, promotionIdForPercent);

        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(percentPromotionStore), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenFlatAndPercentBothForASKUInDifferentStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatAndPercentBothForASKUInDifferentStoresTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String percentPromotionStore, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();


        // meal listing verify
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itempriceOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItemOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        String typeInPromotionDataOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].promotionType").replace("[\"", "").replace("\"]", "");

        String mealBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(mealBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        // Percent discount

        String itempriceForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotionForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String discountOnItemForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + percentPromotionStore + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");

        String typeInPromotionDataForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionIdForPercent + ")].promotionType").replace("[\"", "").replace("\"]", "");

        String allStoresId = JsonPath.read(mealBody, "$.data.stores..storeId").toString();
        Assert.assertEquals(statusCodeOfMeal, 1);
        Assert.assertEquals(itempriceOfMeal, PromotionConstants.itemPrice.toString() + ".0");

        //meal
        softAssert.assertEquals(typeInPromotionDataOfMeal, PromotionConstants.promotionType[0]);
        softAssert.assertEquals(promotionOfMeal, promotionId);
        softAssert.assertEquals(discountOnItemOfMeal, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionDataOfMeal, promotionId);

        // percent
        softAssert.assertEquals(promotionForPercent, promotionIdForPercent);
        softAssert.assertEquals(discountOnItemForPercent, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, promotionIdForPercent);
        softAssert.assertEquals(typeInPromotionDataForPercent, PromotionConstants.promotionType[0]);

        softAssert.assertEquals(allSKUsId.contains(skuNoPromotion), false, "given discount for SKU on which no promotion is created in automation");
        softAssert.assertEquals(allStoresId.contains(percentPromotionStore), true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartWhenFlatAndPercentBothForASKUInDifferentStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartWhenFlatAndPercentBothForASKUInDifferentStoresTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuNoPromotion, String promotionStoreForPercent, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");

        // Percent

        String discountedSKUForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + promotionStoreForPercent + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + promotionStoreForPercent + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuNoPromotion), false);
        softAssert.assertEquals(allStoresId.contains(promotionStoreForPercent), true);
        softAssert.assertEquals(discountedSKUForPercentInCart, sku);
        softAssert.assertEquals(discountedPriceForPercentInCart, PromotionConstants.givenDiscountForPercent);

        softAssert.assertAll();
    }

    // FLAT & %age both for different SKU of same store
    @Test(dataProvider = "CheckPlanListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String noPromotionStore, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String discountOnItemForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForPercent, promotionIdForPercent);
        softAssert.assertEquals(discountOnItemForPercent, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, promotionIdForPercent);

        softAssert.assertEquals(allSKUsId.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false);
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckMealListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresTest(Processor mealListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String noPromotionStore, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String discountOnItemForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionIdForPercent + ")].id").replace("[", "").replace("]", "");


        String planBody = mealListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForPercent, promotionIdForPercent);
        softAssert.assertEquals(discountOnItemForPercent, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, promotionIdForPercent);

        softAssert.assertEquals(allSKUsId.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false);
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckCartListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenFlatAndPercentBothForDiffSKUsOfSameStoresTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuTwoPromotion, String noPromotionStore, String promotionIdForPercent) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");

        // Percent

        String discountedSKUForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + skuTwoPromotion + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + skuTwoPromotion + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuTwoPromotion), true);
        softAssert.assertEquals(allStoresId.contains(noPromotionStore), false);
        softAssert.assertEquals(discountedSKUForPercentInCart, skuTwoPromotion);
        softAssert.assertEquals(discountedPriceForPercentInCart, PromotionConstants.givenDiscountForPercent);

        softAssert.assertAll();
    }

    // FLAT discount for different SKU of different stores

    @Test(dataProvider = "CheckPlanListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenFlatDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckMealListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenFlatDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckCartListingWhenFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenFlatDiscountForDiffSKUsOfDiffStoresTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuTwoPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");

        // Percent

        String discountedSKUForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuTwoPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true);
        softAssert.assertEquals(discountedSKUForPercentInCart, skuTwoPromotion);
        softAssert.assertEquals(discountedPriceForPercentInCart, PromotionConstants.givenDiscountForFlat);

        softAssert.assertAll();
    }


    // Percent discount for different SKU of different stores

    @Test(dataProvider = "CheckPlanListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenPercentDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckMealListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenPercentDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckCartListingWhenPercentDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenPercentDiscountForDiffSKUsOfDiffStoresTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuTwoPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");

        // Percent

        String discountedSKUForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(allSkuIDs.contains(skuTwoPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true);
        softAssert.assertEquals(discountedSKUForPercentInCart, skuTwoPromotion);
        softAssert.assertEquals(discountedPriceForPercentInCart, PromotionConstants.givenDiscountForPercent);

        softAssert.assertAll();
    }

    // Percent & Flat discount for different SKU of different stores

    @Test(dataProvider = "CheckPlanListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckPlanListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckMealListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckMealListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresTest(Processor planListingResponse, String promotionId, String storeId, String sku, String skuSecondPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

        String itemprice = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].price").replace("[", "").replace("]", "");

        String promotion = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].promotions[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");


        String discountOnItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + storeId + ")].items..[?(@.skuId==" + sku + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionData = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + promotionId + ")].id").replace("[", "").replace("]", "");

        // Percent discount

        String itempriceForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].price").replace("[", "").replace("]", "");

        String promotionForSecondStore = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].promotions[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String discountSecondStoreItem = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.stores.[?(@.storeId==" + secondPromotionStore + ")].items..[?(@.skuId==" + skuSecondPromotion + ")].probableDiscountedPrice").replace("[", "").replace("]", "");

        String idInPromotionDataForPercent = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..[?(@.id==" + secondPromotionId + ")].id").replace("[", "").replace("]", "");


        String planBody = planListingResponse.ResponseValidator.GetBodyAsText();
        String allSKUsId = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + storeId + ")].items..skuId").toString();
        String allSKUsIdStoreTwo = JsonPath.read(planBody, "$.data.stores..[?(@.storeId==" + secondPromotionStore + ")].items..skuId").toString();

        String allStoresId = JsonPath.read(planBody, "$.data.stores..storeId").toString();

        Assert.assertEquals(statusCodeOfPlan, 1);
        Assert.assertEquals(itemprice, PromotionConstants.itemPrice.toString() + ".0");

        // Flat
        softAssert.assertEquals(promotion, promotionId);
        softAssert.assertEquals(discountOnItem, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(idInPromotionData, promotionId);

        // percent
        softAssert.assertEquals(promotionForSecondStore, secondPromotionId);
        softAssert.assertEquals(discountSecondStoreItem, PromotionConstants.givenDiscountForPercent);
        softAssert.assertEquals(idInPromotionDataForPercent, secondPromotionId);

        softAssert.assertEquals(allSKUsIdStoreTwo.contains(skuSecondPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true, "2nd promotion store ");
        softAssert.assertAll();
    }


    @Test(dataProvider = "CheckCartListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckCartListingWhenPercentAndFlatDiscountForDiffSKUsOfDiffStoresTest(Processor cartListingResponse, String promotionId, String storeId, String sku, String skuTwoPromotion, String secondPromotionStore, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        // TODO check promotion id and promotion details in cart response
        // cart listing verify
        int statusCodeOfCart = cartListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");

        String discountedSKUInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + storeId + " && @.skuId==" + sku + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");

        // Percent

        String discountedSKUForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].skuId").replace("[\"", "").replace("\"]", "");

        String discountedPriceForPercentInCart = cartListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartDetails..items.[?(@.storeId==" + secondPromotionStore + " && @.skuId==" + skuTwoPromotion + ")].pricingDetails.priceWithDiscount").replace("[", "").replace("]", "");


        String cartBody = cartListingResponse.ResponseValidator.GetBodyAsText();
        String allSkuIDs = JsonPath.read(cartBody, "$.data.cartDetails..items..skuId").toString();
        String allStoresId = JsonPath.read(cartBody, "$.data.cartDetails.items..storeId").toString();

        Assert.assertEquals(statusCodeOfCart, 1);
        Assert.assertNotEquals(data, "{}");
        softAssert.assertEquals(discountedSKUInCart, sku);
        softAssert.assertEquals(discountedPriceInCart, PromotionConstants.givenDiscountForFlat);
        softAssert.assertEquals(allSkuIDs.contains(skuTwoPromotion), true);
        softAssert.assertEquals(allStoresId.contains(secondPromotionStore), true);
        softAssert.assertEquals(discountedSKUForPercentInCart, skuTwoPromotion);
        softAssert.assertEquals(discountedPriceForPercentInCart, PromotionConstants.givenDiscountForPercent);

        Processor getActivePromotions = promotionsHelper.getActivePromotions();
        String allActivePromotions = getActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(allActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(allActivePromotions.contains(promotionId), false);
        softAssert.assertAll();
    }

    // Expired Percent & Flat discount for different SKU of different stores

    @Test(dataProvider = "CheckListingAndCartWhenExpiredPercentAndFlatDiscountForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenExpiredPercentAndFlatDiscountForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    // Future Percent & Flat discount for different SKU of different stores

    @Test(dataProvider = "CheckListingAndCartWhenFuturePercentAndFlatDiscountForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenFuturePercentAndFlatDiscountForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }


    // Future Percent & Flat discount for different SKU of different stores

    @Test(dataProvider = "CheckListingAndCartWhenFuturePercentAndFlatDiscountWithSlotForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenFuturePercentAndFlatDiscountWithSlotForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    // In-active Future Percent & Flat discount for different SKU of different stores
    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatInActiveDiscountForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatInActiveDiscountForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();
        
        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        
        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        
        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        
        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }
    
    
    @Test(dataProvider = "CheckListingAndCartWhenFuturePercentAndFlatDiscountWithActiveFlagZeroForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenFuturePercentAndFlatDiscountWithActiveFlagZeroForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWithActiveFlagWithGivenInputForAStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWithActiveFlagWithGivenInputForAStoresTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        int statusCodeOfPlan = planListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        int statusCodeOfMeal = mealListingResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        int statusCodeOfCart = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), false);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), false);
        softAssert.assertNotEquals(data,"{}");
        softAssert.assertNotEquals(mealData,"{}");
        softAssert.assertNotEquals(cartData,"{}");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWithPresentlyActiveTimeSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWithPresentlyActiveTimeSlotTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();
        
        //plan listing verify
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        String appliedPromotionsId = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..id").replace("[","").replace("]","");
        
        // Meal Listing
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        String appliedPromotionsIdInMeal = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..id").replace("[","").replace("]","");
        
        // Cart
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");
        String appliedPromotionsIdInCart = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.promotionsData..id").replace("[","").replace("]","");
        
        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");
        softAssert.assertEquals(appliedPromotionsIdInMeal.contains(promotionId),true);
        softAssert.assertEquals(appliedPromotionsIdInMeal.contains(secondPromotionId),true);
        softAssert.assertEquals(appliedPromotionsId.contains(promotionId),true);
        softAssert.assertEquals(appliedPromotionsId.contains(secondPromotionId),true);
        softAssert.assertEquals(appliedPromotionsIdInCart.contains(promotionId),true);
        softAssert.assertEquals(appliedPromotionsIdInCart.contains(secondPromotionId),true);
        
        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), true);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), true);
        softAssert.assertNotEquals(data,"{}");
        softAssert.assertNotEquals(mealData,"{}");
        softAssert.assertNotEquals(cartData,"{}");
        softAssert.assertAll();
    }
    
    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWithInvalidFormatTimeSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWithInvalidFormatTimeSlotTest(Processor planListingResponse, Processor mealListingResponse,Processor cartResponse,String promotionId, String secondPromotionId) {
        SoftAssert softAssert = new SoftAssert();
        Processor allActivePromotions= promotionsHelper.getActivePromotions();

        //plan listing verify
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        String  getActivePromotions = allActivePromotions.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..id");

        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), true);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), true);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWithFutureActiveTimeSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWithFutureActiveTimeSlotTest(Processor planListingResponse, Processor mealListingResponse, Processor cartResponse, String promotionId, String secondPromotionId, String getActivePromotions) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), true);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), true);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWithPastTimeSlotDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWithPastTimeSlotTest(Processor planListingResponse, Processor mealListingResponse, Processor cartResponse, String promotionId, String secondPromotionId, String getActivePromotions) {
        SoftAssert softAssert = new SoftAssert();

        //plan listing verify
        String data = planListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Meal Listing
        String mealData = mealListingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        // Cart
        String cartData = cartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data").replace("[","").replace("]","");

        softAssert.assertEquals(getActivePromotions.contains(secondPromotionId), true);
        softAssert.assertEquals(getActivePromotions.contains(promotionId), true);
        softAssert.assertEquals(data,"{}");
        softAssert.assertEquals(mealData,"{}");
        softAssert.assertEquals(cartData,"{}");
        softAssert.assertAll();
    }

    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromIsGreaterThanValidTillStoresDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromIsGreaterThanValidTillStoresTest(String promotionOne, String secondPromotion) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(promotionOne, "null", "Creating Flat type promotion when validFrom is greater than validTill ");
        softAssert.assertEquals(secondPromotion, "null","Creating Percent type promotion when validFrom is greater than validTill ");

       softAssert.assertAll();
    }


    @Test(dataProvider = "CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromAndValidTillIsPresentDateDP", dataProviderClass = PromotionsDP.class, description = "Create pormotion on single SKU")
    public void CheckListingAndCartWhenPercentAndFlatDiscountWhenValidFromAndValidTillIsPresentDateTest(String promotionOne, String secondPromotion) {
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(promotionOne, "null", "Creating Flat type promotion when validFrom is greater than validTill ");
        softAssert.assertNotEquals(secondPromotion, "null","Creating Percent type promotion when validFrom is greater than validTill ");

        softAssert.assertAll();
    }
    
//    @Test()
//    public void test() throws IOException {
//        HashMap<String, String> promotion = promotionsHelper.createPromotion( PromotionConstants.actionType[0],  "10" , "39533", "YJXfFHHFIdf73");
//        System.out.println("id  ----  " + promotion.get("id"));
//
//
//    }
}


