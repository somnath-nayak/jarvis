package com.swiggy.api.daily.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cartId"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentLink {

    public PaymentLink(String cartId){
        super();
        this.cartId = cartId;
    }

    @JsonProperty("cartId")
    private String cartId;

    @JsonProperty("cartId")
    public String getStartTime() {
        return cartId;
    }

    @JsonProperty("cartId")
    public void setStartTime(String cartId) {
        this.cartId = cartId;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cartId", cartId).toString();
    }

}