
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "couponCode",
    "couponDiscount",
    "couponMessage"
})
public class CouponDiscountDetails {

    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("couponDiscount")
    private Integer couponDiscount;
    @JsonProperty("couponMessage")
    private String couponMessage;

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public CouponDiscountDetails withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("couponDiscount")
    public Integer getCouponDiscount() {
        return couponDiscount;
    }

    @JsonProperty("couponDiscount")
    public void setCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
    }

    public CouponDiscountDetails withCouponDiscount(Integer couponDiscount) {
        this.couponDiscount = couponDiscount;
        return this;
    }

    @JsonProperty("couponMessage")
    public String getCouponMessage() {
        return couponMessage;
    }

    @JsonProperty("couponMessage")
    public void setCouponMessage(String couponMessage) {
        this.couponMessage = couponMessage;
    }

    public CouponDiscountDetails withCouponMessage(String couponMessage) {
        this.couponMessage = couponMessage;
        return this;
    }

    public CouponDiscountDetails setDefaultData()  {
        return this.withCouponCode(SubscriptionConstant.COUPONCODE)
                .withCouponDiscount(SubscriptionConstant.COUPONDISCOUNT)
                .withCouponMessage(SubscriptionConstant.COUPONMESSAGE);

    }

}
