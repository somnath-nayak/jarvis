package com.swiggy.api.daily.cms.pojo.SwiggyListingServiec;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
@JsonPropertyOrder({
        "store_id"
})
public class GetMealAndPlan {

    @JsonProperty("store_id")
    private Integer storeId;

    @JsonProperty("store_id")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public GetMealAndPlan withStoreId(Integer storeId) {
        this.storeId = storeId;
        return this;
    }

    public void setDefaultData(int storeId)
    {
        setStoreId(storeId);
    }

}