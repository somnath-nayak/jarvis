
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "start_time",
    "end_time"
})
public class SlotDetails {

    @JsonProperty("start_time")
    private Integer startTime;
    @JsonProperty("end_time")
    private Integer endTime;

    @JsonProperty("start_time")
    public Integer getStartTime() {
        return startTime;
    }

    @JsonProperty("start_time")
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public SlotDetails withStartTime(Integer startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("end_time")
    public Integer getEndTime() {
        return endTime;
    }

    @JsonProperty("end_time")
    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public SlotDetails withEndTime(Integer endTime) {
        this.endTime = endTime;
        return this;
    }
    public SlotDetails setDefaultData()  {
        return this.withStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,15)))
                .withEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(3,00)));
    }
}
