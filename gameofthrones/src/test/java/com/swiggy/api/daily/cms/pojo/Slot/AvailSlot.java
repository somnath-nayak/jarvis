package com.swiggy.api.daily.cms.pojo.Slot;

import org.codehaus.jackson.annotate.JsonProperty;


public class AvailSlot {

    @JsonProperty("start_epoch")
    Long start_epoch ;

    @JsonProperty("end_epoch")
    Long end_epoch ;

    public Long getStart_epoch() {
        return start_epoch;
    }

    public void setStart_epoch(Long start_epoch) {
        this.start_epoch = start_epoch;
    }

    public Long getEnd_epoch() {
        return end_epoch;
    }

    public void setEnd_epoch(Long end_epoch) {
        this.end_epoch = end_epoch;
    }
}
