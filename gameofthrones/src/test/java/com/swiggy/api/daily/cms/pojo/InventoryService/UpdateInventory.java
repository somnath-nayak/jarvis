
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "query",
    "updates"
})
public class UpdateInventory {

    @JsonProperty("query")
    private Query query;
    @JsonProperty("updates")
    private Updates updates;

    @JsonProperty("query")
    public Query getQuery() {
        return query;
    }

    @JsonProperty("query")
    public void setQuery(Query query) {
        this.query = query;
    }

    public UpdateInventory withQuery(Query query) {
        this.query = query;
        return this;
    }

    @JsonProperty("updates")
    public Updates getUpdates() {
        return updates;
    }

    @JsonProperty("updates")
    public void setUpdates(Updates updates) {
        this.updates = updates;
    }

    public UpdateInventory withUpdates(Updates updates) {
        this.updates = updates;
        return this;
    }

    public UpdateInventory setData(String spinid,String pid,Long slotid,Integer maxcount,Integer soldcount){
        Query q=new Query().setData(spinid,null,slotid,pid);
        Updates up=new Updates().setData(maxcount,soldcount);
        this.withQuery(q).withUpdates(up);
        return this;
    }

}
