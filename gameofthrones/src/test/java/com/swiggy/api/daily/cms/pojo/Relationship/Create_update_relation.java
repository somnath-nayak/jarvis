
package com.swiggy.api.daily.cms.pojo.Relationship;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "destination",
    "id",
    "relation_type",
    "relationship_data",
    "source",
})
public class Create_update_relation {

    @JsonProperty("destination")
    private Destination destination;
    @JsonProperty("id")
    private String id;
    @JsonProperty("relation_type")
    private String relation_type;
    @JsonProperty("relationship_data")
    private Relationship_data relationship_data;
    @JsonProperty("source")
    private Source source;
    @JsonProperty("destination")
    public Destination getDestination() {
        return destination;
    }

    @JsonProperty("destination")
    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("relation_type")
    public String getRelation_type() {
        return relation_type;
    }

    @JsonProperty("relation_type")
    public void setRelation_type(String relation_type) {
        this.relation_type = relation_type;
    }

    @JsonProperty("relationship_data")
    public Relationship_data getRelationship_data() {
        return relationship_data;
    }

    @JsonProperty("relationship_data")
    public void setRelationship_data(Relationship_data relationship_data) {
        this.relationship_data = relationship_data;
    }


    @JsonProperty("source")
    public Source getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Source source) {
        this.source = source;
    }

}
