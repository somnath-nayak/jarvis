package com.swiggy.api.daily.vendor.helper;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.QueueData;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateUtils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class VendorCommonHelper {
    Initialize init = new Initialize();

    public static final Logger logger = Logger.getGlobal();

    public String time(){
        Calendar cal = Calendar. getInstance();
        Date date= cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String formattedDate=dateFormat.format(date);
        logger.info("Time of the day using Calendar - 24 hour format: "+ formattedDate);
        return formattedDate;
    }

    public String getCurrentTime(){
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR_OF_DAY, 1);
        String formattedDate=dateFormat.format(cal.getTime());
        return formattedDate;
    }

    public String timePatternCheck(String time){
        Calendar cal = Calendar. getInstance();
        Date date= cal.getTime();
//        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat(time);
        String formattedDate=dateFormat.format(date);
        logger.info("Time of the day using Calendar - 24 hour format: "+ formattedDate);
        return formattedDate;
    }

    public static String getDateInToMilliseconds(String validDate) throws ParseException {
        String myDate = validDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(myDate);
        long millis = date.getTime();

        return Long.toString(millis);
    }

    public void deleteStoreIDInSlotInfoTable(String entity_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        sqlTemplate.execute(VendorConstant.delete_item_id + entity_id + VendorConstant.endQuotes + VendorConstant.limit + VendorConstant.end);
    }

    public void selectStoreIDInSlotInfoTable(String entity_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        sqlTemplate.execute(VendorConstant.select_item_id + entity_id + VendorConstant.endQuotes + VendorConstant.limit + VendorConstant.end);
    }

    public void deleteRestHolidaySlots(String from_time, String to_time) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        sqlTemplate.execute(VendorConstant.delete_rest_holiday_slots + from_time + VendorConstant.endQuotes + VendorConstant.to_time + to_time + VendorConstant.endQuotes + VendorConstant.end);
    }

    public void deleteRestNormalSlots(int id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        sqlTemplate.execute(VendorConstant.delete_rest_normal_slots + id + VendorConstant.endQuotes + VendorConstant.end);
    }

    public void deleteDBEntries() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        sqlTemplate.execute(VendorConstant.deleteSlot + VendorConstant.updated_by + VendorConstant.endQuotes + VendorConstant.end);
        sqlTemplate.execute(VendorConstant.deleteRestSlot + VendorConstant.updated_by + VendorConstant.endQuotes + VendorConstant.end);
    }

    public static String getFutureDateInMilisecond() {
        long a = DateUtils.addHours(new Date(), 1).toInstant().getEpochSecond() * 1100;
        String date = Long.toString(a);
        return date;
    }

    public static String getCurrentDateInMilisecond() {
        long a = DateUtils.addHours(new Date(), 1).toInstant().getEpochSecond() * 1000;
        String date = Long.toString(a);
        return date;
    }

    public static String selectDetailsFromCatalogDB(String cityID, String restID) {
        /*
        Select data from Catalog DB - restaurants and restaurants_metadata tables
        id attribute is url-params is specified as daily, mapping of above two tables must exist
        */
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        String query = String.format(VendorConstant.restaurants_and_restaurants_metadata_tables + cityID + VendorConstant.rest_ID + restID + VendorConstant.end);
        logger.info(VendorConstant.restaurants_and_restaurants_metadata_tables + cityID + VendorConstant.rest_ID + restID + VendorConstant.end);
        System.out.println(query);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        System.out.println("----------"+list+"----------");
        return list.toString();
    }

    public void alphaNumericFunction() {
        String generatedString = RandomStringUtils.randomAlphanumeric(10);
        logger.info(generatedString+ "-----------Generated String-----------");
    }

    public String selectMultipleStoreIDInSlotInfoTable() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(VendorConstant.catalogDB);
        String query = String.format(VendorConstant.select_multiple_item_id);
        logger.info(query);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        logger.info("----------"+list+"----------");
        return list.toString();
    }

    public String timePattern() {
        Calendar cal = Calendar. getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
        String formattedDate = dateFormat.format(date);
        logger.info("Time of the day using Calendar - 24 Hour format:" + formattedDate);
        return formattedDate;
    }

    public String currentDatePattern() {
        Calendar cal = Calendar. getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormat.format(date);
        logger.info("Time of the day using Calendar - " + formattedDate);
        return formattedDate;
    }

    public String futureDatePattern() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormat.format(date);
        logger.info("Time of the day using Calendar - " + formattedDate);
        return formattedDate;
    }

    public static String createFutureTimePattern(String hour) {
        Calendar cal = Calendar. getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'"+hour);
        String formattedDate = dateFormat.format(date);
        logger.info("Future time of the day using Calendar - 24 Hour format:" + formattedDate);
        return formattedDate;
    }

    public static String createFutureDateTimePattern(String hour) {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat;
        String formattedDate=null;
        int futureDate;
        for(futureDate=1;futureDate<=2;futureDate++){
            cal.setTime(new Date());
            cal.add(Calendar.DATE, futureDate);
            Date d = cal.getTime();
            dateFormat = new SimpleDateFormat( "yyyy-MM-dd'T'" + hour);
            formattedDate = dateFormat.format(d);
            logger.info("Future time of the day using Calendar - 24 Hour format:" + formattedDate);
        }
        return formattedDate;
    }

    public static String createCurrentDateTimePattern() {
        Calendar cal = Calendar. getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, 1);
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = dateFormat.format(date);
        logger.info("Future time of the day using Calendar - 24 Hour format:" + formattedDate);
        return formattedDate;
    }

    public static String createEpocCurrentPattern(String hour) {
        Calendar cal = Calendar. getInstance();
        cal.setTime(new Date());
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd "+ hour);
        String formattedDate = dateFormat.format(date);
        logger.info("Future time of the day using Calendar - 24 Hour format:" + formattedDate);
        return formattedDate;
    }

    public static String createFutureTimeDatePattern() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat;
        String formattedDate=null;
        int futureDate;
        for(futureDate=1;futureDate<=2;futureDate++){
            cal.setTime(new Date());
            cal.add(Calendar.DATE, futureDate);
            Date d = cal.getTime();
            dateFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
            formattedDate = dateFormat.format(d);
            logger.info("Future time of the day using Calendar - 24 Hour format:" + formattedDate);
        }
        return formattedDate;
    }

    public static String createEPOCforHour(int hour) {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        cal.add(Calendar.HOUR, hour);
        cal.setTimeInMillis(timestamp.getTime());
        long time = timestamp.getTime();
        String epoc = Long.toString(time);
        return epoc;
    }

    public String convertIntoMilitaryTime(String militarytime){
        Calendar cal = Calendar. getInstance();
        Date date = cal.getTime();
//        DateFormat dateFormat = new SimpleDateFormat("hhmm");
        DateFormat dateFormat = new SimpleDateFormat(militarytime);
        String formattedDate = dateFormat.format(date);
        logger.info("Military Time of the day using Calendar - 24 Hour format:" + formattedDate);
        return formattedDate;
    }

    public String randomNumber() {
        int max = 999;
        int min = 100;
        double x = (Math.random() * (max - min));
        logger.info(x + "=======> x");
        int y = (int) x;
        logger.info(y + "==========> y");
        return y+"";
    }

    //  RabbitMQ function
    public void pushMessage(String hostName, String queueName, AMQP.BasicProperties.Builder amp, Object message) {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            QueueData qdetails = init.EnvironmentDetails.setup.GetQueueDetails(hostName, queueName);
            if((System.getProperty("u2-env") != null)) {
                factory.setUsername("admin");
                factory.setPassword("admin");
            }
            factory.setHost(qdetails.HostUrl);
            factory.setPort(qdetails.Port);
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();
            System.out.println("23333====="+qdetails.Name);
            channel.queueDeclare(qdetails.Name, true, false, false, null);
            channel.basicPublish("",qdetails.Name, amp.build(), message.toString().getBytes());
            System.out.println(" [x] Sent '" + message + "'");
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
