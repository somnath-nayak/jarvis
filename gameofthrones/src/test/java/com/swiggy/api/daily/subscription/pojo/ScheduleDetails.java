
package com.swiggy.api.daily.subscription.pojo;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "start_date",
    "has_weekend_service"
})
public class ScheduleDetails {

    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("has_weekend_service")
    private Boolean hasWeekendService;

    @JsonProperty("start_date")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public ScheduleDetails withStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    @JsonProperty("has_weekend_service")
    public Boolean getHasWeekendService() {
        return hasWeekendService;
    }

    @JsonProperty("has_weekend_service")
    public void setHasWeekendService(Boolean hasWeekendService) {
        this.hasWeekendService = hasWeekendService;
    }

    public ScheduleDetails withHasWeekendService(Boolean hasWeekendService) {
        this.hasWeekendService = hasWeekendService;
        return this;
    }

    public ScheduleDetails setDefaultData(){
        return this.withStartDate(DateUtility.getCurrentDateInReadableFormat())
                .withHasWeekendService(SubscriptionConstant.HAS_WEEKEND_SERVICE);
    }

}
