package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties
public class DailyListingRequest {

    @JsonProperty(value = "customer_location")
    LatLong latlong;

    @JsonProperty("restaurant_ids")
    List<String> restaurantIds;

    @JsonProperty("city_id")
    long cityId;

    @JsonProperty("time_slot")
    EpochSlot timeSlot;

    @JsonProperty("pack_size")
    Integer packSize;

    @JsonProperty("customer_segment")
    CustomerSegmentInfo customerSegmentInfo;

    @JsonProperty("request_id")
    String requestId;

    @JsonProperty("session_id")
    String sessionId;

    @JsonProperty("cluster_debug_enabled")
    boolean clusterDebugEnabled = false;

    public DailyListingRequest(LatLong latlong, long cityId, EpochSlot timeSlot, Integer packSize, boolean clusterDebugEnabled) {
        this.latlong = latlong;
        this.cityId = cityId;
        this.timeSlot = timeSlot;
        this.packSize = packSize;
        this.clusterDebugEnabled = clusterDebugEnabled;
    }

    public LatLong getLatlong() {
        return latlong;
    }

    public void setLatlong(LatLong latlong) {
        this.latlong = latlong;
    }

    public List<String> getRestaurantIds() {
        return restaurantIds;
    }

    public void setRestaurantIds(List<String> restaurantIds) {
        this.restaurantIds = restaurantIds;
    }

    public long getCityId() {
        return cityId;
    }

    public void setCityId(long cityId) {
        this.cityId = cityId;
    }

    public EpochSlot getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(EpochSlot timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Integer getPackSize() {
        return packSize;
    }

    public void setPackSize(Integer packSize) {
        this.packSize = packSize;
    }

    public CustomerSegmentInfo getCustomerSegmentInfo() {
        return customerSegmentInfo;
    }

    public void setCustomerSegmentInfo(CustomerSegmentInfo customerSegmentInfo) {
        this.customerSegmentInfo = customerSegmentInfo;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isClusterDebugEnabled() {
        return clusterDebugEnabled;
    }

    public void setClusterDebugEnabled(boolean clusterDebugEnabled) {
        this.clusterDebugEnabled = clusterDebugEnabled;
    }
}
