package com.swiggy.api.daily.vendor.dp;

import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.vendor.helper.VendorConstants;
import com.swiggy.api.daily.vendor.helper.VendorOrderListingHelper;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import io.advantageous.boon.core.Str;
import org.testng.annotations.DataProvider;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public class VendorOrderListingDp {
    
    VendorOrderListingHelper vendorOrderListingHelper = new VendorOrderListingHelper();
    MealSlotHelper  mealSlotHelper = new MealSlotHelper();
    RMSHelper rmsHelper = new RMSHelper();
    SubscriptionHelper subscriptionHelper = new SubscriptionHelper();

@DataProvider(name = "checkMealAllActiveSlotsDP")
    public Object[][] checkMealAllActiveSlots() throws Exception {
    String[]  cityId = new String[]{"1"};
    String userId = Integer.toString(Utility.getRandom(10001, 90001));
    String orderId = Integer.toString(Utility.getRandom(900001,9000000));
    String[] storeId = new String[] {"13584"};
    String password = "123456";
    String [] itemId = new String[] {"RBZBK3IJ9X"};
    String subscriptionStartDate = Utility.getCurrentDateInHHMMDD();
    
    //Get Vendor details like city id , lat-long, store id & access token
    HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
    String token= vendorDetails.get("accessToken");
    
    // Todo call delivery helper for serviceable store on bases of lat-long
   
    
    // TOdo call CMS helper for getting available item of the store for plan subscription
   //String availItemId= vendorOrderListingHelper.getAvailableItemIdOfPlanByStoreId(storeId[0],"");
   
   // Todo call CMS helper for getting available meal of the store for Pre-order
   // vendorOrderListingHelper.getAvailableMealIdByStoreId(storeId[0],"");
    
    // TOdo call delivery helper to get zone id and city on bases of lat long
  // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
     vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
    //Delivery slot for BREAK fast
     vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);

//    //Delivery slot for LUNCH fast
    vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

//    //Delivery slot for SNACKS fast
vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//    //Delivery slot for DINNER fast
 vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
    
    //Delivery slot for LATE_NIGHT fast
    vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
    
    
    
    vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
    
   
    return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
}
    
    @DataProvider(name = "checkMealAllActiveSlotsNoDeliverySlotDP")
    public Object[][] checkMealAllActiveSlotsNoDeliverySlot() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
         
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);

        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }

    @DataProvider(name = "checkWhenMealSlotIsNotPresentDP")
    public Object[][] checkWhenMealSlotIsNotPresent() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // Todo call delivery helper for serviceable store on bases of lat-long
        
        
        // TOdo call CMS helper for getting available item of the store for plan subscription
        //String availItemId= vendorOrderListingHelper.getAvailableItemIdOfPlanByStoreId(storeId[0],"");
        
        // Todo call CMS helper for getting available meal of the store for Pre-order
        // vendorOrderListingHelper.getAvailableMealIdByStoreId(storeId[0],"");
    
        // call delivery helper to get zone id and city on bases of lat long
      //  //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for BREAK fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);
        
        // disable all meal slot for given city
    
        mealSlotHelper.deleteMeal(cityId[0]);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    
    @DataProvider(name = "checkWhenMealSlotIsPresentForOtherCityDP")
    public Object[][] checkWhenMealSlotIsPresentForOtherCity() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[]  mealSlotCityId = new String[]{"1000"};
        System.out.println("String "+vendorOrderListingHelper.addMinutesToTheGivenHHMM("0900", 45));
    
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // Todo call delivery helper for serviceable store on bases of lat-long
        
        
        // TOdo call CMS helper for getting available item of the store for plan subscription
        //String availItemId= vendorOrderListingHelper.getAvailableItemIdOfPlanByStoreId(storeId[0],"");
        
        // Todo call CMS helper for getting available meal of the store for Pre-order
        // vendorOrderListingHelper.getAvailableMealIdByStoreId(storeId[0],"");
    
        //call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        mealSlotHelper.deleteMeal(cityId[0]);
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, mealSlotCityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "checkWhenOneMealSlotsDP")
    public Object[][] checkWhenOneMealSlots() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        vendorOrderListingHelper.createMealSlots(1, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);

        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "checkAllActiveDeliverySlotsDP")
    public Object[][] checkAllActiveDeliverySlots() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for LATE_NIGHT
     vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);

        //Delivery slot for BREAK_Fast fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);
        
    //Delivery slot for LUNCH fast
     vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

    //Delivery slot for SNACKS fast
  vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);

    //Delivery slot for DINNER fast
    vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
       
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
    
       List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],deliverySlotsId , 12345,token}};
    }
    
    @DataProvider(name = "checkWhenOneDeliverySlotForAllMealSlotsDP")
    public Object[][] checkWhenOneDeliverySlotForAllMealSlots() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // call delivery helper to get zone id and city on bases of lat long
        ////HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for LATE_NIGHT
       // vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
        
        //Delivery slot for BREAK_Fast fast
        vendorOrderListingHelper.createDeliverySlots(1,45,12345,VendorConstants.mealStartTime[1],1500);
        
        //Delivery slot for LUNCH fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);
//
//        //Delivery slot for SNACKS fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//        //Delivery slot for DINNER fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],deliverySlotsId , 12345,token}};
    }
    
    
    @DataProvider(name = "checkWhenOneDeliverySlotForMealSlotsDP")
    public Object[][] checkWhenOneDeliverySlotForMealSlots() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"BREAKFAST"};
        String [] mealStartTime = new String[]{VendorConstants.mealStartTime[1]};
        String[] mealEndTIme = new String[] {VendorConstants.mealSlotEndTime[1]};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for LATE_NIGHT
        // vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
        
        //Delivery slot for BREAK_Fast fast
        vendorOrderListingHelper.createDeliverySlots(1,45,12345,mealStartTime[0],1500);
        
        //Delivery slot for LUNCH fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);
//
//        //Delivery slot for SNACKS fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//        //Delivery slot for DINNER fast
//        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        
        vendorOrderListingHelper.createMealSlots(1, mealSLot, cityId , mealStartTime,mealEndTIme);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],deliverySlotsId , 12345,token}};
    }
    
    @DataProvider(name = "MealAndDeliverySlotEndTimeDP")
    public Object[][] MealAndDeliverySlotEndTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"BREAKFAST"};
        String [] mealStartTime = new String[]{"700"};
        String[] mealEndTIme = new String[] {"701"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // call delivery helper to get zone id and city on bases of lat long
        ////HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for BREAK_Fast fast
        vendorOrderListingHelper.createDeliverySlots(1,45,12345,mealStartTime[0],1500);
        
        vendorOrderListingHelper.createMealSlots(1, mealSLot, cityId , mealStartTime,mealEndTIme);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],deliverySlotsId , 12345,token}};
    }
    
    @DataProvider(name = "CheckWhenOnlyOneOrderIsPresentInBarBarForGivenTimeDP")
    public Object[][] CheckWhenOnlyOneOrderIsPresentInBarBarForGivenTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
    
        //call delivery helper to get zone id and city on bases of lat long
        ////HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
     String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "CheckWhenOnlyNoOrderIsPresentInBarBarForGivenTimeDP")
    public Object[][] CheckWhenOnlyNoOrderIsPresentInBarBarForGivenTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
    
        //call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "CheckWhenMultipleOrdersarePresentInBarBarForGivenTimeDP")
    public Object[][] CheckWhenMultipleOrdersarePresentInBarBarForGivenTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String userIdTwo = Integer.toString(Utility.getRandom(90001, 990001));
        String userIdThree = Integer.toString(Utility.getRandom(7001, 10000));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        String orderIdTwo = Integer.toString(Utility.getRandom(100001,8000000));
        String orderIdThree = Integer.toString(Utility.getRandom(90001,8000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
    
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        // call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
    
        vendorOrderListingHelper.createSubscription(userIdTwo, orderIdTwo , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[1],mealEndTime[1], false);
    
        vendorOrderListingHelper.createSubscription(userIdThree, orderIdThree , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[1],mealEndTime[1], false);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "CheckItemDetailsInFetchOrderDP")
    public Object[][] CheckItemDetailsInFetchOrder() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
    
        //call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        HashMap<String,String> details=vendorOrderListingHelper.cmsItemDetails(VendorConstants.storeId[0],"4_1");
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
        
        
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],details,token}};
    }
    
    @DataProvider(name = "CheckWhenSuscriptionDeliverySlotsAreNotMatchingWithDeliveryServiceDP")
    public Object[][] CheckWhenSuscriptionDeliverySlotsAreNotMatchingWithDeliveryService() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String userIdTwo = Integer.toString(Utility.getRandom(90001, 990001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        String orderIdTwo = Integer.toString(Utility.getRandom(100001,8000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        
        // call delivery helper to get zone id and city on bases of lat long
     //   //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("long"));
    
        HashMap<String,String> details=vendorOrderListingHelper.cmsItemDetails(VendorConstants.storeId[0],"4_1");
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(2,5,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(2,5,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
    
        vendorOrderListingHelper.createSubscription(userIdTwo, orderIdTwo , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[1],mealEndTime[1], false);
    
    
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "CheckWhenOrdersArePresentForAllDeliverySlotsDP")
    public Object[][] CheckWhenOrdersArePresentForAllDeliverySlots() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        // meal Start time
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        // meal End time
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        
        //  Delivery endTime for LUNCH susbcription
        String [] deliveryEndTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+5),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10)};
    
        //  Delivery endTime for DINNER susbcription
        String [] dineDeliveryEndTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+16),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String userIdTwo = Integer.toString(Utility.getRandom(90001, 990001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        String orderIdTwo = Integer.toString(Utility.getRandom(100001,8000000));
    
        String userIdThree = Integer.toString(Utility.getRandom(7001, 10000));
        String orderIdThree = Integer.toString(Utility.getRandom(90001,8000000));
        String orderIdFour = Integer.toString(Utility.getRandom(10000,90000));
    
        // TOdo call delivery helper to get zone id and city on bases of lat long
      //  //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        
        HashMap<String,String> details=vendorOrderListingHelper.cmsItemDetails(VendorConstants.storeId[0],"4_1");
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(2,5,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(2,5,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        
        // LUNCH subscription
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[0],deliveryEndTime[0], false);
        vendorOrderListingHelper.createSubscription(userIdTwo, orderIdTwo , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,deliveryEndTime[0],deliveryEndTime[1], false);
    
        // DINNER subscription
        vendorOrderListingHelper.createSubscription(userId, orderIdThree , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,mealStartTime[1],dineDeliveryEndTime[0], false);
        vendorOrderListingHelper.createSubscription(userIdThree, orderIdFour , VendorConstants.storeId,Utility.getCurrentDateInHHMMDD() ,new String[]{spinId} ,dineDeliveryEndTime[0],dineDeliveryEndTime[1], false);
        
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "fetchOrderWithRestaurantWhichIsNotPresentDP")
    public Object[][] fetchOrderWithRestaurantWhichIsNotPresent() throws Exception {
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        // Todo call delivery helper for serviceable store on bases of lat-long
    
        // call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        HashMap<Integer,Integer> subsCreate = new HashMap<Integer, Integer>();
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for BREAK fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);

//    //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

//    //Delivery slot for SNACKS fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//    //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        //Delivery slot for LATE_NIGHT fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
        
        
        
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, VendorConstants.nonExistingCityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);

        return new Object[][] {{VendorConstants.nonExistingStoreId[0],VendorConstants.password,VendorConstants.nonExistingCityId[0],token}};
    }
    
    @DataProvider(name = "fetchOrderCheckForFromTimeAndEndTimeDP")
    public Object[][] fetchOrderCheckForFromTimeAndEndTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // Todo call delivery helper for serviceable store on bases of lat-long
    
        //call delivery helper to get zone id and city on bases of lat long
       // //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        HashMap<Integer,Integer> subsCreate = new HashMap<Integer, Integer>();
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for BREAK fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);

//    //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

//    //Delivery slot for SNACKS fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//    //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        //Delivery slot for LATE_NIGHT fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
    
    
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    @DataProvider(name = "fetchOrderCheckForLatAndLongDP")
    public Object[][] fetchOrderCheckForLatAndLong() throws Exception {
        String[]  cityId = new String[]{"1"};
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // Todo call delivery helper for serviceable store on bases of lat-long
    
        //call delivery helper to get zone id and city on bases of lat long
        ////HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
    
        HashMap<Integer,Integer> subsCreate = new HashMap<Integer, Integer>();
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for BREAK fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);

//    //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

//    //Delivery slot for SNACKS fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//    //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        //Delivery slot for LATE_NIGHT fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
        
        
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "fetchOrderCheckForCityIdDP")
    public Object[][] fetchOrderCheckForCityId() throws Exception {
        String[]  cityId = new String[]{"1"};
    
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        // Todo call delivery helper for serviceable store on bases of lat-long
    
        //call delivery helper to get zone id and city on bases of lat long
        //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
    
    
        HashMap<Integer,Integer> subsCreate = new HashMap<Integer, Integer>();
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        //Delivery slot for BREAK fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[1],1500);

//    //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[2],1500);

//    //Delivery slot for SNACKS fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[3],1500);
//
//    //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[4],1500);
        
        //Delivery slot for LATE_NIGHT fast
        vendorOrderListingHelper.createDeliverySlots(4,45,12345,VendorConstants.mealStartTime[0],1500);
        
        
        vendorOrderListingHelper.createMealSlots(5, VendorConstants.mealSlot, cityId , VendorConstants.mealStartTime,VendorConstants.mealSlotEndTime);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    
    @DataProvider(name = "CheckFutureOrderIsPresentInBarBarForGivenTimeDP")
    public Object[][] CheckFutureOrderIsPresentInBarBarForGivenTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        
        //call delivery helper to get zone id and city on bases of lat long
        //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
    
        System.out.println("future date for subscription " +vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingHours(24));
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getFutureDate(1,"yyyy-MM-dd") ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    @DataProvider(name = "CheckPastOrderIsPresentInBarBarForGivenTimeDP")
    public Object[][] CheckPastOrderIsPresentInBarBarForGivenTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        
        //call delivery helper to get zone id and city on bases of lat long
        //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
     vendorOrderListingHelper.changeServerTimeToPast(-1);
       
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getFutureDate(-1,"yyyy-MM-dd") ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    
    @DataProvider(name = "CheckOrderFetchFromTimeGreaterThanToTimeDP")
    public Object[][] CheckOrderFetchFromTimeGreaterThanToTime() throws Exception {
        String[]  cityId = new String[]{"1"};
        String[] mealSLot = new String[]{"LUNCH","DINNER"};
        String [] mealStartTime = new String[]{vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+11)};
        String[] mealEndTime = new String[] {vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+10),vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingMinute(VendorConstants.cutOffTime+21)};
        String userId = Integer.toString(Utility.getRandom(10001, 90001));
        String orderId = Integer.toString(Utility.getRandom(900001,9000000));
        HashMap<String, String> vendorDetails = vendorOrderListingHelper.vendorDetails(VendorConstants.username,VendorConstants.password);
        String token= vendorDetails.get("accessToken");
        
        // TODO delivery helper for making store serviceable
        // TODO CMS helper needed for meal availability and item id by passing totalAvailable
        
        //call delivery helper to get zone id and city on bases of lat long
        //HashMap<String, String>  zoneIdCityId=vendorOrderListingHelper.getZoneIdAndCityByLatLong(vendorDetails.get("lat"),vendorDetails.get("lon"));
        
        vendorOrderListingHelper.disableEnableAllDeliverySlots(12345,0);
        
        //Delivery slot for LUNCH fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[0],1500);
        
        //Delivery slot for DINNER fast
        vendorOrderListingHelper.createDeliverySlots(1,10,12345,mealStartTime[1],1500);
        
        vendorOrderListingHelper.createMealSlots(2, mealSLot, cityId , mealStartTime,mealEndTime);
        
        List<Integer> deliverySlotsId= vendorOrderListingHelper.getAllSlotsIdsOfZone( 12345);
        
        String spinId =  vendorOrderListingHelper.availableSpinIdFromServeDailyPlanListing(VendorConstants.storeId[0]);
        System.out.println(" spin id from CMS  "+spinId);
        
        System.out.println("future date for subscription " +vendorOrderListingHelper.getCurrentTIimeInHHMMByAddingHours(24));
        vendorOrderListingHelper.createSubscription(userId, orderId , VendorConstants.storeId,Utility.getFutureDate(1,"yyyy-MM-dd") ,new String[]{spinId} ,mealStartTime[0],mealEndTime[0], false);
        
        return new Object[][] {{VendorConstants.storeId[0],VendorConstants.password,cityId[0],token}};
    }
    
    
}
