package com.swiggy.api.daily.subscription.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.pojo.ServerTimeChangePojo;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SubscriptionTestHelper {
    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();

    public void validatePauseResponse(Processor processor, String value, String weekendService, String pauseStartDate, String pauseEndDate, int count, String statusMeassage)throws ParseException {


        switch (value){

            case "Positive":
                int pauseStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String pauseStatus=processor.ResponseValidator.GetNodeValue("$.data.status");
                String subscriptionEndDate=processor.ResponseValidator.GetNodeValue("$.data.endDate");
                String actualResumeDate=processor.ResponseValidator.GetNodeValue("$.data.resumeDate");

                Assert.assertEquals(pauseStatusCode,1, "Status code is not 1");
                Assert.assertEquals(pauseStatus,"PAUSED","Subscription not paused");

                if(weekendService.equals("EVERDAY")){
                    Assert.assertEquals(subscriptionEndDate, DateUtility.getDateInReadableFormat(pauseEndDate,count),"Subscription end date is not correct");
                    Assert.assertEquals(actualResumeDate,DateUtility.getDateInReadableFormat(pauseEndDate,1),"Subscription resume date is not correct");
                } else {
                    Assert.assertEquals(subscriptionEndDate,expectedEndDate(pauseEndDate,count) ,"Subscription end date is not correct");
                    if(DateUtility.getDayOfDateInReadableFormat(pauseEndDate).equals("Friday")) {
                        Assert.assertEquals(actualResumeDate, DateUtility.getDateInReadableFormat(pauseEndDate, 3), "Subscription resume date is not correct");
                    }else if(DateUtility.getDayOfDateInReadableFormat(pauseEndDate).equals("Saturday")) {
                        Assert.assertEquals(actualResumeDate, DateUtility.getDateInReadableFormat(pauseEndDate, 2), "Subscription resume date is not correct");
                    }else{
                        Assert.assertEquals(actualResumeDate, DateUtility.getDateInReadableFormat(pauseEndDate, 1), "Subscription resume date is not correct");
                    }
                }
                break;
            case "Negative":
                int pauseNegativeStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String pauseStatusMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");

                Assert.assertEquals(pauseNegativeStatusCode,410, "Able to pause subscription");
                Assert.assertEquals(pauseStatusMessage,statusMeassage,"Able to pause subscription");
                break;
        }
    }


    public void validateResumeResponse(Processor processor,String value,String weekendService,String endDate,int resumeAfterDays,String statusMeassage)throws ParseException{


        switch (value){
            case "Positive":
                int pauseStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String pauseStatus=processor.ResponseValidator.GetNodeValue("$.data.status");
                String subscriptionEndDate=processor.ResponseValidator.GetNodeValue("$.data.endDate");
                Assert.assertEquals(pauseStatusCode,1, "Status code is not 1");
                Assert.assertEquals(pauseStatus,"ACTIVE","Subscription not Resumed");

                if(weekendService.equals("EVERDAY")){
                    Assert.assertEquals(subscriptionEndDate,DateUtility.getDateInReadableFormat(endDate,resumeAfterDays),"Subscription end date is not correct");
                } else {
                    if(resumeAfterDays==0){
                        Assert.assertEquals(subscriptionEndDate,endDate ,"Subscription end date is not correct");
                    }else {
                        Assert.assertEquals(subscriptionEndDate, expectedEndDate(endDate, resumeAfterDays), "Subscription end date is not correct");
                    }
                }
                break;
            case "Negative":
                int pauseNegativeStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String pauseStatusMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");
                Assert.assertEquals(pauseNegativeStatusCode,410, "Able to Resume subscription");
                Assert.assertEquals(pauseStatusMessage,statusMeassage,"Able to Resume subscription");
                break;
        }
    }

    public void validateSkipResponse(Processor processor,String value,String weekendService,String endDate,String statusMeassage)throws ParseException{
        switch (value){
            case "Positive":
                int skipStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String skipStatus=processor.ResponseValidator.GetNodeValue("$.data.status");
                String subscriptionEndDate=processor.ResponseValidator.GetNodeValue("$.data.endDate");
                Assert.assertEquals(skipStatusCode,1, "Status code is not 1");
                Assert.assertEquals(skipStatus,"SKIPPED","Subscription not SKIPPED");

                if(weekendService.equals("EVERDAY")){
                    Assert.assertEquals(subscriptionEndDate,DateUtility.getDateInReadableFormat(endDate,1),"Subscription end date is not correct");
                } else {
                    Assert.assertEquals(subscriptionEndDate, expectedEndDate(endDate, 1), "Subscription end date is not correct");
                }
                break;
            case "Negative":
                int skipNegativeStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String skipStatusMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");

                Assert.assertEquals(skipNegativeStatusCode,410, "Able to skip subscription");
                Assert.assertEquals(skipStatusMessage,statusMeassage,"Able to skip subscription");
                break;
        }
    }

    public void validateCreateSubscription(Processor processor,String value,String weekendService,String payload,String statusMessage) throws ParseException {
        SoftAssert softassert=new SoftAssert();
        switch(value)
        {
            case "Positive":
                int statusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
                String subscriberId=processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
                String subscriptionStartDate=processor.ResponseValidator.GetNodeValue("$.data.startDate");
                String subscriptionEndDate=processor.ResponseValidator.GetNodeValue("$.data.endDate");
                String subscriptionExpiryDate=processor.ResponseValidator.GetNodeValue("$.data.expiryDate");
                String subscriptionWeekendType=processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
                int subscriptionTenure=processor.ResponseValidator.GetNodeValueAsInt("$.data.entityDetails.tenure");
                int subscriptionDeliveryStartTime=processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryStartTime");
                int subscriptionDeliveryEndTime=processor.ResponseValidator.GetNodeValueAsInt("$.data.deliveryEndTime");
                String subscriptionStatus=processor.ResponseValidator.GetNodeValue("$.data.status");
                String subscriptionStatusMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");

                String expectedSubscriptionId= JsonPath.read(payload,"$.order_id");
                String expectedSubscriberId=JsonPath.read(payload,"$.customer_id");
                int expectedTenure=JsonPath.read(payload,"$.item_details[0].tenure");
                String expectedStartDate=JsonPath.read(payload,"$.schedule_details.start_date");
                int expectedDeliveryStartTime=JsonPath.read(payload,"$.delivery_slot.delivery_start_time");
                int expectedDeliveryEndTime=JsonPath.read(payload,"$.delivery_slot.delivery_end_time");

                softassert.assertEquals(statusCode,1,"Status Code is not 1");
                softassert.assertEquals(subscriptionId,expectedSubscriptionId,"Subscription Id is not matching with expected");
                softassert.assertEquals(subscriberId,expectedSubscriberId,"Subscriber Id is not matching with expected");
                softassert.assertEquals(subscriptionExpiryDate,DateUtility.getDateInReadableFormat(subscriptionStartDate,59),"ExpiryDate is not matching with expected");
                softassert.assertEquals(subscriptionTenure,expectedTenure,"tenure is not matching with expected");
                softassert.assertEquals(subscriptionDeliveryStartTime,expectedDeliveryStartTime,"Delivery Start time is not matching with expected");
                softassert.assertEquals(subscriptionDeliveryEndTime,expectedDeliveryEndTime,"Delivery End time is not matching with expected");
                softassert.assertEquals(subscriptionStatus,"ACTIVE","Subscription status is not Active");
                softassert.assertEquals(subscriptionStatusMessage,statusMessage,"Subscription message is not success");
                if(weekendService.equals("true")) {
                    softassert.assertEquals(subscriptionStartDate,expectedStartDate,"StartDate is not matching with expected");
                    softassert.assertEquals(subscriptionEndDate,DateUtility.getDateInReadableFormat(expectedStartDate,(expectedTenure-1)),"EndDate is not matching with expected");
                    softassert.assertEquals(subscriptionWeekendType,"EVERDAY","weekend service is not everyday");
                } else{
                    softassert.assertEquals(subscriptionWeekendType,"WEEKDAY","weekend service is not everyday");
                    if(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Saturday"))
                    {
                        softassert.assertEquals(subscriptionStartDate,DateUtility.getDateInReadableFormat(expectedStartDate,2),"StartDate is not matching with expected");
                    }
                    if(DateUtility.getDayOfDateInReadableFormat(subscriptionStartDate).equals("Sunday"))
                    {
                        softassert.assertEquals(subscriptionStartDate,DateUtility.getDateInReadableFormat(expectedStartDate,1),"StartDate is not matching with expected");
                    }
                    String expectedEndDate=DateUtility.getWorkingDateFromCurrentDate(subscriptionStartDate,(expectedTenure-1));
                    softassert.assertEquals(subscriptionEndDate,expectedEndDate,"EndDate is not matching with expected");
                }
                break;
            case "Negative":
                int negativeStatusCode=processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String negativeSubscriptionStatusMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");
                softassert.assertEquals(negativeStatusCode,0,"Status Code is not 1");
                softassert.assertEquals(negativeSubscriptionStatusMessage,statusMessage,"Subscription message is not correct");
                break;
        }
        softassert.assertAll();
    }

    public void validatePauseTillDetails(Processor pauseCalenderDetailsProcessor,String subscriptionType,String pauseTillStartDate,String pauseTillEndDate) throws ParseException {
        SoftAssert softassert=new SoftAssert();
        softassert.assertNull(pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseFrom[0].nextMealDate"),"Next Meal Date Pause From is not null");
        softassert.assertNull(pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseFrom[0].nextMealDay"),"Next Meal Day Pause From is not null");

        JSONArray pauseTillDateArray = pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.pauseDetails.pauseTill[");
        for(int i=0;i<pauseTillDateArray.size();i++){
            Long actualPauseTillDateInEpoch=JsonPath.parse(pauseCalenderDetailsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseTill["+i+"].date",Long.class);
            String actualPauseTillDateInReadableFormat=(DateUtility.getRedableDateFromEpoch(actualPauseTillDateInEpoch)).split(" ")[0];
            softassert.assertEquals(actualPauseTillDateInReadableFormat,pauseTillStartDate,"Pause till date is not correct");

            String actualPauseTillDay=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseTill["+i+"].day");
            softassert.assertEquals(actualPauseTillDay,DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).toUpperCase(),"Pause till day is not correct");

            boolean actualPauseTillStatus=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseTill["+i+"].active");
            if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).equals("Saturday")||DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).equals("Sunday"))){
                softassert.assertEquals(actualPauseTillStatus,false,"Status is not false");
            }else{
                softassert.assertEquals(actualPauseTillStatus,true,"Status is not true");
            }
            Long actualMealDateInEpoch=JsonPath.parse(pauseCalenderDetailsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.pauseDetails.pauseTill["+i+"].nextMealDate",Long.class);
            String actualMealDateInReadableFormat=(DateUtility.getRedableDateFromEpoch(actualMealDateInEpoch)).split(" ")[0];
            String actualPauseNextMealDay=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseTill["+i+"].nextMealDay");

            if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).equals("Friday")))
            {
                String expectedMealDate=DateUtility.getDateInReadableFormat(pauseTillStartDate,3);
                softassert.assertEquals(actualMealDateInReadableFormat,expectedMealDate,"nextMealDate is not correct");
                softassert.assertEquals(actualPauseNextMealDay,DateUtility.getDayOfDateInReadableFormat(expectedMealDate).toUpperCase(),"nextMealDay is not correct");
            }else if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).equals("Saturday"))) {
                String expectedMealDate = DateUtility.getDateInReadableFormat(pauseTillStartDate, 2);
                softassert.assertEquals(actualMealDateInReadableFormat, expectedMealDate, "nextMealDate is not correct");
                softassert.assertEquals(actualPauseNextMealDay, DateUtility.getDayOfDateInReadableFormat(expectedMealDate).toUpperCase(), "nextMealDay is not correct");
            }else if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(pauseTillStartDate).equals("Sunday")))
            {
                String expectedMealDate=DateUtility.getDateInReadableFormat(pauseTillStartDate,1);
                softassert.assertEquals(actualMealDateInReadableFormat,expectedMealDate,"nextMealDate is not correct");
                softassert.assertEquals(actualPauseNextMealDay,DateUtility.getDayOfDateInReadableFormat(expectedMealDate).toUpperCase(),"nextMealDay is not correct");
            }else{
                String expectedMealDate=DateUtility.getDateInReadableFormat(pauseTillStartDate,1);
                softassert.assertEquals(actualMealDateInReadableFormat,expectedMealDate,"nextMealDate is not correct");
                softassert.assertEquals(actualPauseNextMealDay,DateUtility.getDayOfDateInReadableFormat(expectedMealDate).toUpperCase(),"nextMealDay is not correct");
            }
            pauseTillStartDate=DateUtility.getDateInReadableFormat(pauseTillStartDate,1);
        }
        softassert.assertEquals(DateUtility.getDateInReadableFormat(pauseTillStartDate,-1),pauseTillEndDate,"Pause till date is showing more than 7 days");
        softassert.assertAll();
    }

    public void validateSkipDetailsInPauseCalender(Processor pauseCalenderDetailsProcessor,String subscriptionType,String expectedSkipDate) throws ParseException {
        String expectedNextMealDate="";
        SoftAssert softassert=new SoftAssert();
        Long actualSkipDateInEpoch=JsonPath.parse(pauseCalenderDetailsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.skipDetails.skipDate.date",Long.class);
        String actualSkipDateInReadableFormat=(DateUtility.getRedableDateFromEpoch(actualSkipDateInEpoch)).split(" ")[0];
        String actualSkipDay=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.skipDetails.skipDate.day");
        boolean actualActiveStatus=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipDate.active");
        Long actualNextMealDateInEpoch=JsonPath.parse(pauseCalenderDetailsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.skipDetails.skipDate.nextMealDate",Long.class);
        String actualNextMealDateInReadableFormat=(DateUtility.getRedableDateFromEpoch(actualNextMealDateInEpoch)).split(" ")[0];
        String actualNextMealDay=pauseCalenderDetailsProcessor.ResponseValidator.GetNodeValue("$.data.skipDetails.skipDate.nextMealDay");

        softassert.assertEquals(actualSkipDateInReadableFormat,expectedSkipDate,"Skip Date is not correct");
        softassert.assertEquals(actualSkipDay,DateUtility.getDayOfDateInReadableFormat(expectedSkipDate),"Skip Day is not correct");
        softassert.assertEquals(actualActiveStatus,true,"Status is not correct");

        if(subscriptionType.equals("WEEKDAY")&&(DateUtility.getDayOfDateInReadableFormat(expectedSkipDate).equals("Friday"))){
            expectedNextMealDate=DateUtility.getDateInReadableFormat(expectedSkipDate,3);
        }else{
            expectedNextMealDate=DateUtility.getDateInReadableFormat(expectedSkipDate,1);
        }
        softassert.assertEquals(actualNextMealDateInReadableFormat,expectedNextMealDate,"Next Meal date is not correct");
        softassert.assertEquals(actualNextMealDay,DateUtility.getDayOfDateInReadableFormat(expectedNextMealDate),"Next Meal Day is not correct");
    }

    public void validatePauseCalenderUsingExpiryDateWeekendTrue(HashMap<String, String> response) throws ParseException {
        SoftAssert softAssert=new SoftAssert();
        for(int i=1;i<=7;i++)
        {
            subscriptionHelper.updateExpiryDateSubscription(DateUtility.getDateInReadableFormat(response.get("subscriptionEndDate"),i), response.get("id"));
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(response.get("id"), response.get("userId"));
            String pauseTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseTitle");
            boolean pauseAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            String skipTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.skipDetails.skipTitle");
            boolean skipAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");
            if (i == 1) {
                softAssert.assertEquals(pauseTitle,SubscriptionConstant.SUBSCRIPTION_NOPAUSEAVAILABLESUBSCRIPTION_MSG,"Pause slots are available");
                softAssert.assertEquals(pauseAvailable,false,"Pause option are available for user");
                softAssert.assertEquals(skipTitle,SubscriptionConstant.SUBSCRIPTION_SKIPAVAILABLESUBSCRIPTION_MSG,"Skip slots are not available");
                softAssert.assertEquals(skipAvailable,true,"Skip option is available for user");
            }else{
                softAssert.assertEquals(pauseTitle,SubscriptionConstant.SUBSCRIPTION_PAUSEAVAILABLESUBSCRIPTION_MSG,"Pause slots are not available");
                softAssert.assertEquals(pauseAvailable,true,"Pause option are available for user");
                softAssert.assertEquals(skipTitle,SubscriptionConstant.SUBSCRIPTION_SKIPAVAILABLESUBSCRIPTION_MSG,"Skip slots are not available");
                softAssert.assertEquals(skipAvailable,true,"Skip option is available for user");
                JSONArray pauseTill = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.pauseDetails.pauseTill[");
                softAssert.assertEquals(pauseTill.size(),i-1,"Pause till slots are either less or more");
            }
        }
        softAssert.assertAll();
    }

    public void validatePauseCalenderUsingExpiryDateWeekendFalse(HashMap<String, String> response) throws ParseException {
        SoftAssert softAssert=new SoftAssert();
        String subscriptionEndDate=response.get("subscriptionEndDate");
        for (int i = 1; i <= 7; i++) {
            subscriptionEndDate=DateUtility.getNextWorkingDateInReadableFormat(subscriptionEndDate);
            subscriptionHelper.updateExpiryDateSubscription(subscriptionEndDate, response.get("id"));
            Processor pauseCalenderProcessor = subscriptionHelper.getPauseCalenderDetails(response.get("id"), response.get("userId"));
            String pauseTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.pauseDetails.pauseTitle");
            boolean pauseAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.pauseDetails.pauseAvailable");
            String skipTitle = pauseCalenderProcessor.ResponseValidator.GetNodeValue("$.data.skipDetails.skipTitle");
            boolean skipAvailable = pauseCalenderProcessor.ResponseValidator.GetNodeValueAsBool("$.data.skipDetails.skipAvailable");

            if (i == 1) {
                softAssert.assertEquals(pauseTitle, SubscriptionConstant.SUBSCRIPTION_NOPAUSEAVAILABLESUBSCRIPTION_MSG, "Pause slots are available");
                softAssert.assertEquals(pauseAvailable, false, "Pause option are available for user");
                softAssert.assertEquals(skipTitle, SubscriptionConstant.SUBSCRIPTION_SKIPAVAILABLESUBSCRIPTION_MSG, "Skip slots are not available");
                softAssert.assertEquals(skipAvailable, true, "Skip option is available for user");
            } else {
                softAssert.assertEquals(pauseTitle, SubscriptionConstant.SUBSCRIPTION_PAUSEAVAILABLESUBSCRIPTION_MSG, "Pause slots are not available");
                softAssert.assertEquals(pauseAvailable, true, "Pause option are available for user");
                softAssert.assertEquals(skipTitle, SubscriptionConstant.SUBSCRIPTION_SKIPAVAILABLESUBSCRIPTION_MSG, "Skip slots are not available");
                softAssert.assertEquals(skipAvailable, true, "Skip option is available for user");
            }
        }
        softAssert.assertAll();
    }

    public String expectedEndDate(String pauseEndDate,int count)throws ParseException{
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
        Calendar time = new GregorianCalendar();

        Date date = sdf.parse(pauseEndDate);

        for (int i=1 ; i<=count; i++) {
            time.setTime(date);
            for (int promise_days=i ; promise_days!=0 ; promise_days--) {
                time.add(Calendar.DATE, 1);
                if (time.get(Calendar.DAY_OF_WEEK)==7) {
                    time.add(Calendar.DATE, 2);
                }

            }
        }
        return sdf.format(time.getTime());
    }


    public HashMap<String,String> createSubscriptionHelper(Processor processor){
        int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(responseStatusCode, 1);
        HashMap<String, String> response=new HashMap<>();
        String subscriptionId=processor.ResponseValidator.GetNodeValue("$.data.id");
        String userId = processor.ResponseValidator.GetNodeValue("$.data.subscriberId");
        String subscriptionEndDate = processor.ResponseValidator.GetNodeValue("$.data.endDate");
        String subscriptionStartDate = processor.ResponseValidator.GetNodeValue("$.data.startDate");
        String subscriptionType = processor.ResponseValidator.GetNodeValue("$.data.recurrencePattern");
        String vendorId = processor.ResponseValidator.GetNodeValue("$.data.vendorId");
        String expiryDate = processor.ResponseValidator.GetNodeValue("$.data.expiryDate");
        String status = processor.ResponseValidator.GetNodeValue("$.data.status");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");

        response.put("id",subscriptionId);
        response.put("userId",userId);
        response.put("subscriptionEndDate",subscriptionEndDate);
        response.put("subscriptionStartDate",subscriptionStartDate);
        response.put("subscriptionType",subscriptionType);
        response.put("vendorId",vendorId);
        response.put("expiryDate",expiryDate);
        response.put("status",status);
        response.put("statusMessage",statusMessage);

        return response;
    }



    public void validateDeliverySlot(Processor vendorInventoryProcessor, List<Long> expectedDeliveryStartTime, List<Long> expectedDeliveryEndTime, int tenure){
        for(int i=0; i<tenure;i++)
        {
            Long actualDeliveryStartTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].slot_details.start_time",Long.class);
            Long actualDeliveryEndTime=JsonPath.parse(vendorInventoryProcessor.ResponseValidator.GetBodyAsText()).read("$.data["+i+"].slot_details.end_time",Long.class);
            Assert.assertTrue(expectedDeliveryStartTime.contains(actualDeliveryStartTime),"Delivery StartDate is invalid");
            Assert.assertTrue(expectedDeliveryEndTime.contains(actualDeliveryEndTime),"Delivery EndDate is invalid");
        }
    }

    public void validateItemSlot(Processor vendorInventoryProcessor,int tenure,String itemId,int quantity){
        for(int i=0; i<tenure;i++)
        {
            String actualItemId=vendorInventoryProcessor.ResponseValidator.GetNodeValue("$.data["+i+"].item_details[0].item_id");
            int actualQuantity=vendorInventoryProcessor.ResponseValidator.GetNodeValueAsInt("$.data["+i+"].item_details[0].quantity");
            Assert.assertEquals(actualItemId,"4_1","Item_id is not correct");
            Assert.assertEquals(actualQuantity,quantity,"Quantity is not correct");
        }
    }

    public void changeServerTime(String subscriptionStartDate) throws IOException {
        String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +subscriptionStartDate ));
        subscriptionHelper.changeServerTime(changeServerDatePayload);
        String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormatWithColon()));
        subscriptionHelper.changeServerTime(changeServerTimePayload);
    }

    public void changeSeverCurrentDate() throws IOException {
        String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentDateInReadableFormat()));
        subscriptionHelper.changeServerTime(changeServerDatePayload);
        String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormatWithColon()));
        subscriptionHelper.changeServerTime(changeServerTimePayload);

    }

    public void validateNodePauseSubscriptionNotNull(String pauseSubscriptionResponse){
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.id"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.subscriberType"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.subscriberId"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.startDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.endDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.expiryDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.recurrencePattern"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.entityDetails.tenure"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.recurrenceCount"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.deliveryStartTime"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.deliveryEndTime"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.status"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.resumeDate"));
    }

    public void validateNodeSkipResumeSubscriptionNotNull(String pauseSubscriptionResponse){
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.id"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.subscriberType"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.subscriberId"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.startDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.endDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.expiryDate"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.recurrencePattern"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.entityDetails.tenure"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.recurrenceCount"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.deliveryStartTime"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.deliveryEndTime"));
        Assert.assertNotNull(JsonPath.read(pauseSubscriptionResponse,"$.data.status"));
    }

    public void validateSwapResponse(Processor processor,String value,String subscriptionMealId,String subscriptionNewMealJobId,String type,String statusMessage){
        switch (value){
            case "Positive":
                int responseStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String message = processor.ResponseValidator.GetNodeValue("$.statusMessage");
                if(type.equals("SWAP")) {

                    Assert.assertEquals(responseStatusCode, 1, "Meal not swapped");
                    Assert.assertEquals(message, "success", "Meal not swapped");
                    String mealStatus = subscriptionHelper.getSubscriptionMealStatus(subscriptionMealId);
                    Assert.assertEquals(mealStatus, "SWAPPED", "Status of meal is not swapped");
                    String newMealStatus = subscriptionHelper.getSubscriptionNewMealStatus(subscriptionNewMealJobId);
                    Assert.assertEquals(newMealStatus, "SCHEDULED", "Status of new meal is not scheduled");
                }else{
                    Assert.assertEquals(responseStatusCode,1,"Addons is not added with Meal");
                    Assert.assertEquals(message,"success","Addons is not added with Meal");
                    String mealStatus=subscriptionHelper.getSubscriptionMealStatusForAddon(subscriptionMealId);
                    Assert.assertEquals(mealStatus,"EDITED","Status of meal is not edited");
                    String newMealStatus=subscriptionHelper.getSubscriptionNewMealStatus(subscriptionNewMealJobId);
                    Assert.assertEquals(newMealStatus,"SCHEDULED","Status of new meal is not scheduled");
                }
                break;
            case "Negative":
                int responseSecondStatusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
                String swapSecondMessage=processor.ResponseValidator.GetNodeValue("$.statusMessage");
                Assert.assertEquals(responseSecondStatusCode,0,"Meal not swapped");
                Assert.assertEquals(swapSecondMessage,statusMessage,"Meal not swapped");
                break;
        }
    }


    public void validateSubscriptionBillDetailsResponse(Processor orderDailyOmsProcessor,Processor subscriptionBillProcessor) {
        SoftAssert softAssert=new SoftAssert();

        String expectedOrderId=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].order_job_id");
        Long expectedCreatedDate=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].created_at",Long.class);
        int expectedTenure=orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.meta.tenure");

        Double expectedOverallPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.total",Double.class);
        Double expectedIndividualItemPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].itemBill.total",Double.class);

        Double expectedItemTotal=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.itemTotalCharges.total",Double.class);

        Double expectedCartPackagingCharge=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartPackingCharges.total",Double.class);
        Double expectedDeliveryPriceDetails=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.deliveryPriceDetails.total",Double.class);
        Double expectedDiscount=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartDiscountDetails.swiggyDiscount",Double.class);
        String expectedPlanName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.name");
        String expectedPaymentMethod=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].payment_info[0].payment_method");


        String actualOrderId=subscriptionBillProcessor.ResponseValidator.GetNodeValue("$.data.orderId");
        Long actualCreatedDate=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderDate",Long.class);
        int actualTenure=subscriptionBillProcessor.ResponseValidator.GetNodeValueAsInt("$.data.totalQuantity");
        Double actualOverallPrice=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderTotal",Double.class);


        Double actualIndividualItemPrice=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderItems[0].itemPrice",Double.class);

        Double actualItemTotal=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[0].value",Double.class);

        Double actualCartPackagingCharge=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[1].value",Double.class);
        Double actualDeliveryPriceDetails=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[2].value",Double.class);
        Double actualDiscount=JsonPath.parse(subscriptionBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[3].value",Double.class);
        String actualPlanName=subscriptionBillProcessor.ResponseValidator.GetNodeValue("$.data.orderItems[0].name");
        String actualPaymentMethod=subscriptionBillProcessor.ResponseValidator.GetNodeValue("$.data.paymentInfo.paymentMethod");

        softAssert.assertEquals(actualOrderId,expectedOrderId,"Order Id is not correct");
        softAssert.assertEquals(actualCreatedDate,expectedCreatedDate,"Date is not correct");
        softAssert.assertEquals(actualTenure,expectedTenure,"Tenure is not correct");
        softAssert.assertEquals(actualOverallPrice,expectedOverallPrice,"Overall price is not correct");
        softAssert.assertEquals(actualIndividualItemPrice,expectedIndividualItemPrice,"Individual Item price is not correct");
        softAssert.assertEquals(actualItemTotal,expectedItemTotal,"Item total is not correct");
        softAssert.assertEquals(actualCartPackagingCharge,expectedCartPackagingCharge,"Cart Packaging charges is not correct");
        softAssert.assertEquals(actualDeliveryPriceDetails,expectedDeliveryPriceDetails,"Delivery Price details is not correct");
        softAssert.assertEquals(actualDiscount,expectedDiscount,"Discount is not correct");
        softAssert.assertEquals(actualPlanName,expectedPlanName,"Plan name is not correct");
        softAssert.assertEquals(actualPaymentMethod,expectedPaymentMethod,"Payment method is not correct");

        softAssert.assertAll();
    }

    public void validatePreorderBillDetailsResponse(Processor orderDailyOmsProcessor,Processor preorderBillProcessor,String mealStatus) {
        SoftAssert softAssert=new SoftAssert();

        String expectedOrderId=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].order_job_id");
        Long expectedCreatedDate=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].created_at",Long.class);

        Double expectedOverallPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.total",Double.class);
        Double expectedIndividualItemPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].itemBill.total",Double.class);

        Double expectedItemTotal=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.itemTotalCharges.total",Double.class);

        Double expectedCartPackagingCharge=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartPackingCharges.total",Double.class);
        Double expectedDeliveryPriceDetails=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.deliveryPriceDetails.total",Double.class);
        Double expectedDiscount=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartDiscountDetails.swiggyDiscount",Double.class);
        String expectedPlanName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.name");
        String expectedPaymentMethod=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].payment_info[0].payment_method");
        int expectedMealQuantity=orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].quantity");


        String expectedStoreName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.name");
        String expectedStoreCity=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.city");
        String expectedStoreArea=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.area");
        String expectedStoreAddress=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.address");
        String expectedStoreLocality=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.locality");


        String expectedUserName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.name");
        String expectedUserMobile=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.mobile");
        String expectedUserCity=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.city");
        String expectedUserArea=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.area");
        String expectedUserAddress=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.address");
        String expectedUserLandmark=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.landmark");
        String expectedUserAnnotation=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.annotation");
        String expectedUserFlatNo=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.flatNo");

        String actualOrderId=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.orderId");
        Long actualCreatedDate=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderDate",Long.class);

        Double actualOverallPrice=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderTotal",Double.class);
        Double actualIndividualItemPrice=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderItems[0].total",Double.class);

        Double actualItemTotal=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[0].value",Double.class);

        Double actualCartPackagingCharge=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[1].value",Double.class);
        Double actualDeliveryPriceDetails=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[2].value",Double.class);
        Double actualDiscount=JsonPath.parse(preorderBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[3].value",Double.class);
        String actualPlanName=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.orderItems[0].name");
        String actualPaymentMethod=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.paymentInfo.paymentMethod");
        String actualMealStatus=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.orderStatus");

        int actualMealQuantity=preorderBillProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orderItems[0].quantity");

        String actualStoreName=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.name");
        String actualStoreCity=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.city");
        String actualStoreArea=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.area");
        String actualStoreAddress=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.address");
        String actualStoreLocality=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.locality");

        String actualUserName=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.name");
        String actualUserMobile=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.mobile");
        String actualUserCity=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.city");
        String actualUserArea=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.area");
        String actualUserAddress=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.address");
        String actualUserLandmark=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.landmark");
        String actualUserAnnotation=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.annotation");
        String actualUserFlatNo=preorderBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.flat_no");

        softAssert.assertEquals(actualOrderId,expectedOrderId,"Order Id is not correct");
        softAssert.assertEquals(actualCreatedDate,expectedCreatedDate,"Date is not correct");

        softAssert.assertEquals(actualOverallPrice,expectedOverallPrice,"Overall price is not correct");
        softAssert.assertEquals(actualIndividualItemPrice,expectedIndividualItemPrice,"Individual Item price is not correct");
        softAssert.assertEquals(actualItemTotal,expectedItemTotal,"Item total is not correct");
        softAssert.assertEquals(actualCartPackagingCharge,expectedCartPackagingCharge,"Cart Packaging charges is not correct");
        softAssert.assertEquals(actualDeliveryPriceDetails,expectedDeliveryPriceDetails,"Delivery Price details is not correct");
        softAssert.assertEquals(actualDiscount,expectedDiscount,"Discount is not correct");
        softAssert.assertEquals(actualPlanName,expectedPlanName,"Plan name is not correct");
        softAssert.assertEquals(actualPaymentMethod,expectedPaymentMethod,"Payment method is not correct");

        softAssert.assertEquals(actualMealStatus,mealStatus,"Meal Status is not correct");
        softAssert.assertEquals(actualMealQuantity,expectedMealQuantity,"Meal Quantity is not correct");


        softAssert.assertEquals(actualStoreName,expectedStoreName,"Store Name is not correct");
        softAssert.assertEquals(actualStoreCity,expectedStoreCity,"Store City is not correct");
        softAssert.assertEquals(actualStoreArea,expectedStoreArea,"Store area is not correct");
        softAssert.assertEquals(actualStoreAddress,expectedStoreAddress,"Store address is not correct");
        softAssert.assertEquals(actualStoreLocality,expectedStoreLocality,"Store locality is not correct");
      //softAssert.assertEquals(actualUserName,expectedUserName,"user name is not correct");
        softAssert.assertEquals(actualUserMobile,expectedUserMobile,"use mobile is not correct");
        softAssert.assertEquals(actualUserCity,expectedUserCity,"user city is not correct");
        softAssert.assertEquals(actualUserArea,expectedUserArea,"user area is not correct");
        softAssert.assertEquals(actualUserAddress,expectedUserAddress,"user address is not correct");
        softAssert.assertEquals(actualUserLandmark,expectedUserLandmark,"user landmark is not correct");
        softAssert.assertEquals(actualUserAnnotation,expectedUserAnnotation,"user annotation is not correct");
        softAssert.assertEquals(actualUserFlatNo,expectedUserFlatNo,"user flat no is not correct");

        softAssert.assertAll();
    }


    public void validateSubscriptionMealBillDetailsResponse(Processor orderDailyOmsProcessor,Processor subscriptionMealBillProcessor,String mealStatus) {
        SoftAssert softAssert=new SoftAssert();

        String expectedOrderId=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].order_job_id");
        Long expectedCreatedDate=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].created_at",Long.class);

        Double expectedOverallPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.total",Double.class);
        Double expectedIndividualItemPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].itemBill.total",Double.class);

        Double expectedItemTotal=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.itemTotalCharges.total",Double.class);

        Double expectedCartPackagingCharge=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartPackingCharges.total",Double.class);
        Double expectedDeliveryPriceDetails=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.deliveryPriceDetails.total",Double.class);
        Double expectedDiscount=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.pricingDetail.cartDiscountDetails.swiggyDiscount",Double.class);
        String expectedPlanName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.name");
        int expectedMealQuantity=orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].quantity");


        String expectedStoreName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.name");
        String expectedStoreCity=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.city");
        String expectedStoreArea=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.area");
        String expectedStoreAddress=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.address");
        String expectedStoreLocality=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.storeDetail.locality");


        String expectedUserName=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.name");
        String expectedUserMobile=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.mobile");
        String expectedUserCity=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.city");
        String expectedUserArea=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.area");
        String expectedUserAddress=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.address");
        String expectedUserLandmark=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.landmark");
        String expectedUserAnnotation=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.annotation");
        String expectedUserFlatNo=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.addressDetail.flatNo");

        String actualOrderId=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.orderId");
        Long actualCreatedDate=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderDate",Long.class);

        Double actualOverallPrice=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderTotal",Double.class);
        Double actualIndividualItemPrice=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orderItems[0].total",Double.class);

        Double actualItemTotal=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[0].value",Double.class);

        Double actualCartPackagingCharge=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[1].value",Double.class);
        Double actualDeliveryPriceDetails=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[2].value",Double.class);
        Double actualDiscount=JsonPath.parse(subscriptionMealBillProcessor.ResponseValidator.GetBodyAsText()).read("$.data.renderingDetails[3].value",Double.class);
        String actualPlanName=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.orderItems[0].name");
        String actualMealStatus=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.orderStatus");

        int actualMealQuantity=subscriptionMealBillProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orderItems[0].quantity");

        String actualStoreName=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.name");
        String actualStoreCity=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.city");
        String actualStoreArea=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.area");
        String actualStoreAddress=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.address");
        String actualStoreLocality=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.storeDetails.locality");

        String actualUserName=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.name");
        String actualUserMobile=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.mobile");
        String actualUserCity=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.city");
        String actualUserArea=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.area");
        String actualUserAddress=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.address");
        String actualUserLandmark=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.landmark");
        String actualUserAnnotation=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.annotation");
        String actualUserFlatNo=subscriptionMealBillProcessor.ResponseValidator.GetNodeValue("$.data.deliveryAddressDetails.flat_no");

        softAssert.assertEquals(actualOrderId,expectedOrderId,"Order Id is not correct");
        softAssert.assertEquals(actualCreatedDate,expectedCreatedDate,"Date is not correct");

        softAssert.assertEquals(actualOverallPrice,expectedOverallPrice,"Overall price is not correct");
        softAssert.assertEquals(actualIndividualItemPrice,expectedIndividualItemPrice,"Individual Item price is not correct");
        softAssert.assertEquals(actualItemTotal,expectedItemTotal,"Item total is not correct");
        softAssert.assertEquals(actualCartPackagingCharge,expectedCartPackagingCharge,"Cart Packaging charges is not correct");
        softAssert.assertEquals(actualDeliveryPriceDetails,expectedDeliveryPriceDetails,"Delivery Price details is not correct");
        softAssert.assertEquals(actualDiscount,expectedDiscount,"Discount is not correct");
        softAssert.assertEquals(actualPlanName,expectedPlanName,"Plan name is not correct");

        softAssert.assertEquals(actualMealStatus,mealStatus,"Meal Status is not correct");
        softAssert.assertEquals(actualMealQuantity,expectedMealQuantity,"Meal Quantity is not correct");


        softAssert.assertEquals(actualStoreName,expectedStoreName,"Store Name is not correct");
        softAssert.assertEquals(actualStoreCity,expectedStoreCity,"Store City is not correct");
        softAssert.assertEquals(actualStoreArea,expectedStoreArea,"Store area is not correct");
        softAssert.assertEquals(actualStoreAddress,expectedStoreAddress,"Store address is not correct");
        softAssert.assertEquals(actualStoreLocality,expectedStoreLocality,"Store locality is not correct");
        //softAssert.assertEquals(actualUserName,expectedUserName,"user name is not correct");
        softAssert.assertEquals(actualUserMobile,expectedUserMobile,"use mobile is not correct");
        softAssert.assertEquals(actualUserCity,expectedUserCity,"user city is not correct");
        softAssert.assertEquals(actualUserArea,expectedUserArea,"user area is not correct");
        softAssert.assertEquals(actualUserAddress,expectedUserAddress,"user address is not correct");
        softAssert.assertEquals(actualUserLandmark,expectedUserLandmark,"user landmark is not correct");
        softAssert.assertEquals(actualUserAnnotation,expectedUserAnnotation,"user annotation is not correct");
        softAssert.assertEquals(actualUserFlatNo,expectedUserFlatNo,"user flat no is not correct");

        softAssert.assertAll();
    }

    public void validateCancelSubscriptionResponse(Processor orderDailyOmsProcessor,Processor cancelSubscriptionProcessor,int days ){

        SoftAssert softAssert=new SoftAssert();
        Double expectedOverallPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].payment_info[0].transaction_amount",Double.class);
        int expectedTenure=orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.meta.tenure");
        String refundTransactionId=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].payment_info[1].transaction_id");
        String expectedPaymentMethod=orderDailyOmsProcessor.ResponseValidator.GetNodeValue("$.data.orders[0].order_jobs[0].payment_info[1].payment_method");

        int statusCode=cancelSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Double actualOverallPrice=JsonPath.parse(cancelSubscriptionProcessor.ResponseValidator.GetBodyAsText()).read("$.data.total_refund_amount",Double.class);
        int actualTenure=cancelSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.data.meals_cancelled");
        String actualTransactionId=cancelSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.refund_details[0].refund_transaction_id");
        String actualPaymentMethod=cancelSubscriptionProcessor.ResponseValidator.GetNodeValue("$.data.refund_details[0].payment_mode");
        int onlineAmount=cancelSubscriptionProcessor.ResponseValidator.GetNodeValueAsInt("$.data.online_refund_amount");

        int refundAmount=(int)((expectedOverallPrice/expectedTenure)*(expectedTenure-days));
        int actualRefundAmount=actualOverallPrice.intValue();

        softAssert.assertEquals(statusCode,1,"Subscription is not cancelled");
        softAssert.assertEquals(onlineAmount,0,"Online amount is not correct");
        softAssert.assertEquals(actualRefundAmount,refundAmount,"Refund amount is not correct");
        softAssert.assertEquals(actualTenure,expectedTenure-days,"Tenure is not correct");
        softAssert.assertEquals(actualTransactionId,refundTransactionId,"Transaction id is not correct");
        softAssert.assertEquals(actualPaymentMethod,expectedPaymentMethod,"Payment method is not correct");

        softAssert.assertAll();
    }

    public void validateGenerateOrderIdResponse(Processor orderIdProcessor) {
        SoftAssert softAssert=new SoftAssert();
        int orderIdStatusCode=orderIdProcessor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String orderIdStatusMessage=orderIdProcessor.ResponseValidator.GetNodeValue("$.data");
        softAssert.assertEquals(orderIdStatusCode,1,"OrderID is not generated");
        softAssert.assertEquals(orderIdStatusMessage,"success","OrderID is not generated");
        softAssert.assertAll();
    }

    public void validateGetSubscriptionRefundResponse(Processor orderDailyOmsProcessor,Processor getSubscriptionRefund,int days ){

        SoftAssert softAssert=new SoftAssert();
        Double expectedOverallPrice=JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].payment_info[0].transaction_amount",Double.class);
        int expectedTenure=orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.itemDetails[0].item.meta.tenure");

        int statusCode=getSubscriptionRefund.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Double actualOverallPrice=JsonPath.parse(getSubscriptionRefund.ResponseValidator.GetBodyAsText()).read("$.data.refund_amount",Double.class);
        int actualTenure=getSubscriptionRefund.ResponseValidator.GetNodeValueAsInt("$.data.meals_remaining");
        int onlineAmount=getSubscriptionRefund.ResponseValidator.GetNodeValueAsInt("$.data.online_refund_amount");

        int refundAmount=(int)((expectedOverallPrice/expectedTenure)*(expectedTenure-days));
        int actualRefundAmount=actualOverallPrice.intValue();

        softAssert.assertEquals(statusCode,1,"Subscription is not cancelled");
        softAssert.assertEquals(onlineAmount,0,"Online amount is not correct");
        softAssert.assertEquals(actualRefundAmount,refundAmount,"Refund amount is not correct");
        softAssert.assertEquals(actualTenure,expectedTenure-days,"Meal remaining is not correct");


        softAssert.assertAll();
    }


}
