package com.swiggy.api.daily.subscription.dp;

import com.swiggy.api.daily.checkout.helper.DailyCheckoutConstants;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutOrderHelper;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.pojo.CreatePreorederPojo;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.*;
import com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo.OrderJob;
import com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo.SwapAddonMealPojo;
import com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo.Metadata;
import com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo.Order;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;


public class SubscriptionDp {
    JsonHelper jsonHelper=new JsonHelper();
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    DailyCheckoutOrderHelper dailyCheckoutOrderHelper=new DailyCheckoutOrderHelper();
    RandomNumber rm = new RandomNumber(5000, 100000);
    CreatePreorederPojo preorederPojo;

    //-------------------------------------------------------------Create Subscription-------------------------------------------------------------------------------

    @DataProvider(name = "createSubscriptionDP")
    public  Object[][] createSubscriptionDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))}
        };
    }

    //--------------------------------------------------------------Pause Subscription--------------------------------------------------------------------------------

   @DataProvider(name = "pauseSubscriptionPreCutoffPositiveDP")
    public  Object[][] pauseSubscriptionPreCutoffPositiveDP() throws IOException, ParseException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}}))
                        ,DateUtility.getCurrentDateInReadableFormat(),DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_TEN},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(7));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_TEN},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR}
        };
    }

    @DataProvider(name = "pauseSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP")
    public  Object[][] pauseSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP() throws IOException    {


        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3}
                };
        }

    @DataProvider(name = "pauseSubscriptionPostCutoffPositiveDP")
    public  Object[][] pauseSubscriptionPostCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(0,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(0,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(0,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(4));}})),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(5),SubscriptionConstant.TENURE_SUBSCRIPTION_FOUR-1,DateUtility.getFutureTimeInReadableFormatWithColon(2,14)}

        };
    }

    @DataProvider(name = "pauseSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP")
    public  Object[][] pauseSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)}
        };
    }

    @DataProvider(name = "pauseResumePauseSubscriptionDP")
    public  Object[][] pauseResumePauseSubscriptionDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6)}

        };
    }

    @DataProvider(name = "pauseSubscriptionPreCutoffNegativeDP")
    public  Object[][] pauseSubscriptionPreCutoffNegativeDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateInReadableFormat(2))))
                        ,DateUtility.getCurrentDateInReadableFormat(),DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN,SubscriptionConstant.SUBSCRIPTION_UNSUCCESS_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateInReadableFormat(2)).withHasWeekendService(false)))
                        ,DateUtility.getCurrentDateInReadableFormat(),DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN,SubscriptionConstant.SUBSCRIPTION_UNSUCCESS_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true)))
                        ,DateUtility.getCurrentDateInReadableFormat(),DateUtility.getFutureDateInReadableFormat(7),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN,SubscriptionConstant.SUBSCRIPTION_MAXPAUSEUNSUCCESS_MSG}

        };
    }


    @DataProvider(name = "pauseSubscriptionWithDbOperationPreCutoffNegativeDP")
    public  Object[][] pauseSubscriptionWithDbOperationPreCutoffNegativeDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),2
                        ,DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,2,DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true)))
                        ,-2,DateUtility.getFutureDateInReadableFormat(4),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,-2,DateUtility.getFutureDateInReadableFormat(4),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,-3}

        };
    }

    @DataProvider(name = "pauseSubscriptionWithDbOperationPostCutoffNegativeDP")
    public  Object[][] pauseSubscriptionWithDbOperationPostCutoffNegativeDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),2,DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,DateUtility.getFutureTimeInReadableFormatWithColon(1,16),-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),4,DateUtility.getFutureDateInReadableFormat(6)
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,DateUtility.getFutureTimeInReadableFormatWithColon(1,16),-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true)))
                        ,-2,DateUtility.getFutureDateInReadableFormat(4),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,DateUtility.getFutureTimeInReadableFormatWithColon(1,16),-3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,-2,DateUtility.getFutureDateInReadableFormat(4),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1
                        ,SubscriptionConstant.SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG,DateUtility.getFutureTimeInReadableFormatWithColon(1,16),-3},

        };
    }


    @DataProvider(name = "pauseSubscriptionLateNightPostCutoffDP")
    public  Object[][] pauseSubscriptionLateNightPostCutoffDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, "22:01"},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, "23:00"},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),DateUtility.getCurrentDateInReadableFormat(),
                        DateUtility.getFutureDateInReadableFormat(6),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN-1, "23:59"},

        };
    }


    @DataProvider(name = "pauseSubscriptionMaxDaysPauseEndDateDayFridayWeekendFalseDP")
    public  Object[][] pauseSubscriptionMaxDaysPauseEndDateDayFridayWeekendFalseDP() throws IOException, ParseException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData()
                        .withHasWeekendService(false).withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday"))))
                        ,DateUtility.getDateInReadableFormat(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday"),8),7},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData()
                        .withHasWeekendService(false).withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday"))))
                        ,DateUtility.getDateInReadableFormat(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday"),9),7},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData()
                        .withHasWeekendService(false).withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday"))))
                ,DateUtility.getDateInReadableFormat(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday"),7),7},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData()
                        .withHasWeekendService(false).withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday"))))
                        ,DateUtility.getDateInReadableFormat(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday"),8),7}
        };
    }

    @DataProvider(name = "pauseSubscriptionPostCutoffWithNextDayDatePositiveDP")
    public  Object[][] pauseSubscriptionPostCutoffWithNextDayDatePositiveDP() throws IOException, ParseException {

        return new Object[][]{

                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(0, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(1, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(2, 14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(0, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(1, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(2, 14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")).withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(0, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")).withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(1, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")).withHasWeekendService(false))), SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN, DateUtility.getFutureTimeInReadableFormatWithColon(2, 14)}
        };
    }

    //----------------------------------------------------------------------Resume subscription-------------------------------------------------------------------------

    @DataProvider(name = "resumeSubscriptionPreCutoffPositiveDP")
    public  Object[][] resumeSubscriptionPreCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())
                        ,DateUtility.getFutureDateInReadableFormat(6),0},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}}))
                        ,DateUtility.getFutureDateInReadableFormat(6),0},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(7));}}))
                        ,DateUtility.getFutureDateInReadableFormat(6),0},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}}))
                        ,DateUtility.getFutureDateInReadableFormat(6),0}
        };
    }

    @DataProvider(name = "resumeSubscriptionAfterFewDaysPreCutoffPositiveDP")
    public  Object[][] resumeSubscriptionAfterFewDaysPreCutoffPositiveDP() throws IOException    {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())
                        ,DateUtility.getFutureDateInReadableFormat(6)
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,DateUtility.getFutureDateInReadableFormat(6)
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3}
        };
    }

    @DataProvider(name = "resumeSubscriptionPostCutoffPositiveDP")
    public  Object[][] resumeSubscriptionPostCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(0,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))),
                        DateUtility.getFutureDateInReadableFormat(6),1, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},

        };
    }

    @DataProvider(name = "resumeSubscriptionAfterFewDaysPostCutoffPositiveDP")
    public  Object[][] resumeSubscriptionAfterFewDaysPostCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,3,DateUtility.getFutureTimeInReadableFormatWithColon(1,16)}
        };
    }

    @DataProvider(name = "resumeSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP")
    public  Object[][] resumeSubscriptionAfterFewMealDeliveredPreCutoffPositiveDP() throws IOException    {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3,3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))
                        ,SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,DateUtility.getCurrentTimeInReadableFormatWithColon(),3,2}
        };
    }

    @DataProvider(name = "resumeSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP")
    public  Object[][] resumeSubscriptionAfterFewMealDeliveredPostCutoffPositiveDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(0,17),3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(1,16),3},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()),SubscriptionConstant.TENURE_SUBSCRIPTION_SEVEN
                        ,3, DateUtility.getFutureTimeInReadableFormatWithColon(2,14),3}
        };
    }

    @DataProvider(name = "createSubscriptionTenureDP")
    public  Object[][] createSubscriptionTenureDP() throws IOException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}}))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(7));}}))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}}))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(7));}})
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateInReadableFormat(2)).withHasWeekendService(true)))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}})
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateInReadableFormat(2))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(7));}})
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateInReadableFormat(2)).withHasWeekendService(false)))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(10));}})
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateInReadableFormat(2)).withHasWeekendService(false)))}
        };
    }
    @DataProvider(name = "createSubscriptionOnWeekendDP")
    public  Object[][] createSubscriptionOnWeekendDP() throws IOException, ParseException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Saturday")).withHasWeekendService(false)))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Sunday")).withHasWeekendService(false)))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Saturday")).withHasWeekendService(true)))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Sunday")).withHasWeekendService(true)))}
        };
    }

    @DataProvider(name = "createSubscriptionNegativeDP")
    public  Object[][] createSubscriptionNegativeDP() throws IOException, ParseException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,1))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,46)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(1,59))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,44)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,1))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,46)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(1,59))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,44)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(true))
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,-1))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,44)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withDeliverySlot(new DeliverySlot().withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,-1))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(0,44)))))},
        };
    }

    @DataProvider(name = "createSubscriptionPastDaysDP")
    public  Object[][] createSubscriptionPastDaysDP() throws IOException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateInReadableFormat(-1)).withHasWeekendService(true))),SubscriptionConstant.SUBSCRIPTION_PREVIOUSDAYUNSUCCESS_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().withStartDate(DateUtility.getFutureDateInReadableFormat(-1)).withHasWeekendService(false))),SubscriptionConstant.SUBSCRIPTION_PREVIOUSDAYUNSUCCESS_MSG},
        };
    }

    @DataProvider(name = "createSubscriptionWithBlankFieldDP")
    public  Object[][] createSubscriptionWithBlankFieldDP() throws IOException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withCustomerId(" ")),SubscriptionConstant.SUBSCRIPTION_BLANKCUSTOMERID_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withOrderId(" ")),SubscriptionConstant.SUBSCRIPTION_BLANKORDERID_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(" "))),SubscriptionConstant.SUBSCRIPTION_BLANKSTARTDATE_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withItemId(null));}})),SubscriptionConstant.SUBSCRIPTION_NULLITEMID_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData().withTenure(null));}})),SubscriptionConstant.SUBSCRIPTION_NULLTENURE_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withDeliverySlot(new DeliverySlot().setDefaultData().withDeliveryStartTime(null))),SubscriptionConstant.SUBSCRIPTION_NULLDELIVERYSTARTTIME_MSG},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withDeliverySlot(new DeliverySlot().setDefaultData().withDeliveryEndTime(null))),SubscriptionConstant.SUBSCRIPTION_NULLDELIVERYENDTIME_MSG},
        };
    }

    @DataProvider(name = "createSubscriptionWithDeliveryStartTimeGreaterThanEndTimeDP")
    public  Object[][] createSubscriptionWithDeliveryStartTimeGreaterThanEndTimeDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withDeliverySlot(new DeliverySlot().setDefaultData()
                        .withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(3,0))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,15)))))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)).withDeliverySlot(new DeliverySlot().setDefaultData()
                        .withDeliveryStartTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(3,0))).withDeliveryEndTime(Integer.parseInt(DateUtility.getFutureTimeInReadableFormat(2,15)))))}
        };
    }


    @DataProvider(name = "createSubscriptionBetween10PMTo12AMDP")
    public  Object[][] createSubscriptionBetween10PMTo12AMDP() throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withDeliverySlot(new DeliverySlot().setDefaultData()
                        .withDeliveryStartTime(310).withDeliveryEndTime(355))),"23:00"},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)).withDeliverySlot(new DeliverySlot().setDefaultData()
                                .withDeliveryStartTime(310).withDeliveryEndTime(355))),"23:00"},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withDeliverySlot(new DeliverySlot().setDefaultData()
                        .withDeliveryStartTime(2340).withDeliveryEndTime(2355))),"23:00"},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)).withDeliverySlot(new DeliverySlot().setDefaultData()
                                .withDeliveryStartTime(2340).withDeliveryEndTime(2355))),"23:00"}
        };
    }

    //----------------------------------------------------------Skip Subscription-----------------------------------------------------------------------------

    @DataProvider(name = "skipSubscriptionPreCutoffDP")
    public  Object[][] skipSubscriptionPreCutoffDP() throws IOException {
        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData())},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false)))},
        };
    }

    @DataProvider(name = "skipSubscriptionPostCutoffDP")
    public  Object[][] skipSubscriptionPostCutoffDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(0,17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(2,14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(0,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(1,16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(2,14)}
        };
    }

    //-------------------------------------------------------------------------------Vendor inventory-----------------------------------------------------------------------------------

    @DataProvider(name = "vendorInventoryDP")
    public  Object[][] vendorInventoryDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData());}}))},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))
                        .withItemDetails(new ArrayList<ItemDetail>(){{add(new ItemDetail().setDefaultData());}}))}
        };
    }
    @DataProvider(name = "createPreorderPositive")
    public Object[][] createPreorderPositiveDp() throws IOException {
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().withStoreId(String.valueOf(rm.nextInt()))
                .withDeliveryStartTime(DateUtility.getFutureDateInMinMilisecond(125)).withDeliveryEndTime(DateUtility.getFutureDateInMinMilisecond(180));
        String payload1 = jsonHelper.getObjectToJSON(preorederPojo);
        return new Object[][]{{payload1}};
    }

    //--------------------------------------------------------------------------Pause Calender-----------------------------------------------------------------------------------

    @DataProvider(name = "getPauseCalenderDetailsPostcutoffSubscriptionDP")
    public  Object[][] getPauseCalenderDetailsPostcutoffSubscriptionDP()throws IOException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(1, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()), DateUtility.getFutureTimeInReadableFormatWithColon(2, 14)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(0, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(1, 16)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(2,14)}
        };
    }

    @DataProvider(name = "pauseCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffDP")
    public  Object[][] pauseCalenderForThursdayFridayOnWeekendServiceFalsePostCutoffDP() throws IOException, ParseException {

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday")))), DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData().withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")))), DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Thursday")).withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)},
                {jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData()
                        .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(DateUtility.getFutureDateBasedOnDayFromCurrentDate("Friday")).withHasWeekendService(false))), DateUtility.getFutureTimeInReadableFormatWithColon(0, 17)}
        };
    }

    //--------------------------------------------------------------------------Swap Meal Subscription-----------------------------------------------------------------------------------

    @DataProvider(name = "swapMealSubscriptionDP")
    public Object[][] swapMealSubscriptionDP() throws ParseException, IOException {
        String createSubscriptionPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData());
        Processor subscriptionProcessor =subscriptionHelper.createSubscription(createSubscriptionPayload);
        HashMap<String,String> response =subscriptionTestHelper.createSubscriptionHelper(subscriptionProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(response.get("id"));
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("SWAP").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("SWAP").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("SWAP").withMetadata(new Metadata().setDefaultData("SWAP").withSubscriptionMealId(subscriptionMealId)));}}));}}))}
        };
    }

    @DataProvider(name = "addonMealSubscriptionDP")
    public Object[][] addonMealSubscriptionDP() throws ParseException, IOException {
        String createSubscriptionPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData());
        Processor subscriptionProcessor =subscriptionHelper.createSubscription(createSubscriptionPayload);
        HashMap<String,String> response =subscriptionTestHelper.createSubscriptionHelper(subscriptionProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(response.get("id"));
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("ADDON").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("ADDON").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("ADDON").withMetadata(new Metadata().setDefaultData("ADDON").withSubscriptionMealId(subscriptionMealId)));}}));}}))}
        };
    }

    @DataProvider(name = "swapMealSubscriptionAfterFewMealDeliveredDP")
    public Object[][] swapMealSubscriptionAfterFewMealDeliveredDP() throws ParseException, IOException {
        String createSubscriptionPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData());
        Processor subscriptionProcessor =subscriptionHelper.createSubscription(createSubscriptionPayload);
        HashMap<String,String> response =subscriptionTestHelper.createSubscriptionHelper(subscriptionProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(response.get("id"));
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.futureMeals.mealDetails[2].subscriptionMealId");

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("SWAP").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("SWAP").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("SWAP").withMetadata(new Metadata().setDefaultData("SWAP").withSubscriptionMealId(subscriptionMealId)));}}));}}))}
        };
    }

    @DataProvider(name = "addonMealSubscriptionAfterFewMealDeliveredDP")
    public Object[][] addonMealSubscriptionAfterFewMealDeliveredDP() throws ParseException, IOException {
        String createSubscriptionPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData());
        Processor subscriptionProcessor =subscriptionHelper.createSubscription(createSubscriptionPayload);
        HashMap<String,String> response =subscriptionTestHelper.createSubscriptionHelper(subscriptionProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(response.get("id"));
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.futureMeals.mealDetails[2].subscriptionMealId");

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("ADDON").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("ADDON").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("ADDON").withMetadata(new Metadata().setDefaultData("ADDON").withSubscriptionMealId(subscriptionMealId)));}}));}}))}
        };
    }

    @DataProvider(name = "addAddonAlreadySwappedMealSubscriptionDP")
    public Object[][] addAddonAlreadySwappedMealSubscriptionDP() throws ParseException, IOException {
        String createSubscriptionPayload=jsonHelper.getObjectToJSON(new CreateSubscriptionPojo().setDefaultData());
        Processor subscriptionProcessor =subscriptionHelper.createSubscription(createSubscriptionPayload);
        HashMap<String,String> response =subscriptionTestHelper.createSubscriptionHelper(subscriptionProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(response.get("id"));
        int subscriptionDetailsStatusCode = subscriptionDetailsMeal.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(subscriptionDetailsStatusCode, 1);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");

        return new Object[][]{
                {jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("SWAP").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("SWAP").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("SWAP").withMetadata(new Metadata().setDefaultData("SWAP").withSubscriptionMealId(subscriptionMealId)));}}));}})),jsonHelper.getObjectToJSON(new SwapAddonMealPojo().setDefaultData("ADDON").withOrders(new ArrayList<Order>() {{add(new Order()
                        .setDefaultData("ADDON").withOrderId(response.get("id")).withOrderJobs(new ArrayList<OrderJob>() {{add(new OrderJob().setDefaultData("ADDON").withMetadata(new Metadata().setDefaultData("ADDON").withSubscriptionMealId(subscriptionMealId)));}}));}}))}
        };
    }

    @DataProvider(name = "subscriptionBillDetailsDP")
    public Object[][] subscriptionBillDetailsDP() {
        String orderId=dailyCheckoutOrderHelper.dailyCreateOrder(DailyCheckoutConstants.USER_MOBILE, DailyCheckoutConstants.USER_PASSWORD,
                DailyCheckoutConstants.CITY_ID, DailyCheckoutConstants.STOREID, SubscriptionConstant.SUBSCRIPTION_TYPE_PLAN);
        return new Object[][]{
                { orderId}
        };
    }

    @DataProvider(name = "preorderBillDetailsDP")
    public Object[][] preorderBillDetailsDP() {
        String orderId=dailyCheckoutOrderHelper.dailyCreateOrder(DailyCheckoutConstants.USER_MOBILE, DailyCheckoutConstants.USER_PASSWORD,
                DailyCheckoutConstants.CITY_ID, DailyCheckoutConstants.STOREID, SubscriptionConstant.SUBSCRIPTION_TYPE_PRODCUT);
        return new Object[][]{
                { orderId}
        };
    }
}

