package com.swiggy.api.daily.cms.pojo.Slot;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class AvailabiltityResponse {

    @JsonProperty("data")
    List<AvailabilityTimeline> data;

    @JsonProperty("statusCode")
    public String statusCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("statusMessage")
    public String statusMessage;

    public List<AvailabilityTimeline> getData() {
        return data;
    }

    public void setData(List<AvailabilityTimeline> data) {
        this.data = data;
    }
}
