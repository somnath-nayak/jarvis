package com.swiggy.api.daily.checkout.pojo;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "coupon_code",
        "items",
        "address_id",
        "meta",
        "ctx",
        "location"
})
public class CartPayload {

    @JsonProperty("coupon_code")
    private String couponCode;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonProperty("address_id")
    private String addressId;
    @JsonProperty("meta")
    private Meta meta;
    @JsonProperty("ctx")
    private Ctx ctx;
    @JsonProperty("location")
    private Location location;

    @JsonProperty("coupon_code")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("coupon_code")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonProperty("address_id")
    public String getAddressId() {
        return addressId;
    }

    @JsonProperty("address_id")
    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @JsonProperty("ctx")
    public Ctx getCtx() {
        return ctx;
    }

    @JsonProperty("ctx")
    public void setCtx(Ctx ctx) {
        this.ctx = ctx;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("couponCode", couponCode).append("items", items).append("addressId", addressId).append("meta", meta).append("ctx", ctx).append("location", location).toString();
    }

}
