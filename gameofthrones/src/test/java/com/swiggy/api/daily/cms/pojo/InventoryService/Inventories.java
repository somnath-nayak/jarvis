
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "max_cap",
        "sold_count"
})
public class Inventories {

    @JsonProperty("max_cap")
    private Integer max_cap;
    @JsonProperty("sold_count")
    private Integer sold_count;

    @JsonProperty("max_cap")
    public Integer getMax_cap() {
        return max_cap;
    }

    @JsonProperty("max_cap")
    public void setMax_cap(Integer max_cap) {
        this.max_cap = max_cap;
    }

    public Inventories withMax_cap(Integer max_cap) {
        this.max_cap = max_cap;
        return this;
    }

    @JsonProperty("sold_count")
    public Integer getSold_count() {
        return sold_count;
    }

    @JsonProperty("sold_count")
    public void setSold_count(Integer sold_count) {
        this.sold_count = sold_count;
    }

    public Inventories withSold_count(Integer sold_count) {
        this.sold_count = sold_count;
        return this;
    }

    public Inventories setData(Integer maxcap,Integer soldcount){
        this.withMax_cap(maxcap).withSold_count(soldcount);
        return this;
    }

}