
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "type",
    "cgst",
    "sgst",
    "igst",
    "total"
})
public class Tax {

    @JsonProperty("type")
    private String type;
    @JsonProperty("cgst")
    private Double cgst;
    @JsonProperty("sgst")
    private Integer sgst;
    @JsonProperty("igst")
    private Double igst;
    @JsonProperty("total")
    private Double total;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Tax withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("cgst")
    public Double getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }

    public Tax withCgst(Double cgst) {
        this.cgst = cgst;
        return this;
    }

    @JsonProperty("sgst")
    public Integer getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(Integer sgst) {
        this.sgst = sgst;
    }

    public Tax withSgst(Integer sgst) {
        this.sgst = sgst;
        return this;
    }

    @JsonProperty("igst")
    public Double getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(Double igst) {
        this.igst = igst;
    }

    public Tax withIgst(Double igst) {
        this.igst = igst;
        return this;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    public Tax withTotal(Double total) {
        this.total = total;
        return this;
    }

    public Tax setDefaultData() {
        return this.withType(SubscriptionConstant.TAX_TYPE)
                .withCgst(SubscriptionConstant.CGST)
                .withSgst(SubscriptionConstant.TAX_SGST)
                .withIgst(SubscriptionConstant.IGST)
                .withTotal(SubscriptionConstant.TAX__TOTAL);
    }

}
