
package com.swiggy.api.daily.subscription.pojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "address_line1",
    "address",
    "landmark",
    "area",
    "mobile",
    "annotation",
    "email",
    "flat_no",
    "city",
    "lat",
    "lng",
    "reverse_geo_code_failed"
})
public class AddressDetails {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("address_line1")
    private String addressLine1;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("email")
    private String email;
    @JsonProperty("flat_no")
    private String flatNo;
    @JsonProperty("city")
    private String city;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("reverse_geo_code_failed")
    private Boolean reverseGeoCodeFailed;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public AddressDetails withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AddressDetails withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("address_line1")
    public String getAddressLine1() {
        return addressLine1;
    }

    @JsonProperty("address_line1")
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public AddressDetails withAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public AddressDetails withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public AddressDetails withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public AddressDetails withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public AddressDetails withMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public AddressDetails withAnnotation(String annotation) {
        this.annotation = annotation;
        return this;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public AddressDetails withEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("flat_no")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flat_no")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public AddressDetails withFlatNo(String flatNo) {
        this.flatNo = flatNo;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public AddressDetails withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    public AddressDetails withLat(String lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    public AddressDetails withLng(String lng) {
        this.lng = lng;
        return this;
    }

    @JsonProperty("reverse_geo_code_failed")
    public Boolean getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    @JsonProperty("reverse_geo_code_failed")
    public void setReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    public AddressDetails withReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
        return this;
    }

    public AddressDetails setDefaultData(){
        return this.withId(SubscriptionConstant.id)
                .withName(SubscriptionConstant.NAME)
                .withAddressLine1(SubscriptionConstant.ADDRESS_LINE1)
                .withAddress(SubscriptionConstant.ADDRESS)
                .withLandmark(SubscriptionConstant.LANDMARK)
                .withArea(SubscriptionConstant.AREA)
                .withMobile(SubscriptionConstant.MOBILE)
                .withAnnotation(SubscriptionConstant.ANNOTATION)
                .withEmail(SubscriptionConstant.EMAIL)
                .withFlatNo(SubscriptionConstant.FLAT_NO)
                .withCity(SubscriptionConstant.CITY)
                .withLat(SubscriptionConstant.LAT)
                .withLng(SubscriptionConstant.LONG)
                .withReverseGeoCodeFailed(SubscriptionConstant.REVERSE_GEO_CODE_FAILED);
    }

}
