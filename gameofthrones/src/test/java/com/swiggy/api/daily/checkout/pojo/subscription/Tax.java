package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class Tax {

    @JsonProperty("cgst")
    private Long cgst;
    @JsonProperty("sgst")
    private Long sgst;
    @JsonProperty("igst")
    private Long igst;
    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private Long value;

    @JsonProperty("cgst")
    public Long getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(Long cgst) {
        this.cgst = cgst;
    }

    public Tax withCgst(Long cgst) {
        this.cgst = cgst;
        return this;
    }

    @JsonProperty("sgst")
    public Long getSgst() {
        return sgst;
    }

    @JsonProperty("sgst")
    public void setSgst(Long sgst) {
        this.sgst = sgst;
    }

    public Tax withSgst(Long sgst) {
        this.sgst = sgst;
        return this;
    }

    @JsonProperty("igst")
    public Long getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(Long igst) {
        this.igst = igst;
    }

    public Tax withIgst(Long igst) {
        this.igst = igst;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Tax withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("value")
    public Long getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Long value) {
        this.value = value;
    }

    public Tax withValue(Long value) {
        this.value = value;
        return this;
    }


}
