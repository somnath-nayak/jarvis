package com.swiggy.api.daily.subscription.helper;

public interface SubscriptionConstant {
    
    //Create Subscription
    String id = "27399250";
    String NAME = "Sayali";
    String ADDRESS_LINE1 = "Karve Nagar, Pune, M.. 411052, India";
    String ADDRESS = "Karve Nagar, Pune, Maharashtra 411052, India";
    String LANDMARK = "Near Cummins College Karvenagar";
    String AREA = "Karve Nagar";
    String MOBILE = "9923414982";
    String ANNOTATION = "Work";
    String EMAIL = "sayli1984@gmail.com";
    String FLAT_NO = "Flat No 15 Akshay Sahaniwa Karvenagar Pune";
    String CITY = "Pune";
    String LAT = "18.485927";
    String LONG = "73.81966";


    boolean REVERSE_GEO_CODE_FAILED = false;
    boolean HAS_WEEKEND_SERVICE = true;
    String ITEM_ID = "OBDCAI0OWJ";
    int TENURE = 7;
    int STORE_IDS = 39533;

    //Pause Subscription
    String SUBSCRIPTION_SUCCESS_MSG = "success";
    int TENURE_SUBSCRIPTION_SEVEN = 7;
    int TENURE_SUBSCRIPTION_TEN = 10;
    int TENURE_SUBSCRIPTION_FOUR = 4;
    String SUBSCRIPTION_UNSUCCESS_MSG = "Subscription has not yet started";
    String SUBSCRIPTION_PAUSESUCCESS_MSG = "success";
    String SUBSCRIPTION_MAXPAUSEUNSUCCESS_MSG = "No. of pause days can not be greater than 7 Days";
    String SUBSCRIPTION_INVALIDSTARTDATEUNSUCCESS_MSG = "Pause start date is Invalid";
    String SUBSCRIPTION_EXPIRYDATEEXCEEDUNSUCCESS_MSG = "Subscription End Date is exceeding expiry";
    String SUBSCRIPTION_INVALIDENDDATEUNSUCCESS_MSG = "Pause End Date is Incorrect";

    //Resume Subscription
    String SUBSCRIPTION_RESUMESUCCESS_MSG = "success";
    String SUBSCRIPTION_UNSUCCESSRESUME_MSG = "Subscription is not paused";

    //Skip Subscription
    String SUBSCRIPTION_NOTACTIVESKIPUNSUCCESS_MSG = "Subscription is not currently Active";
    String SUBSCRIPTION_CUTOFFWINDOWSKIPUNSUCCESS_MSG = "Subscription Meal is in cut off window";
    String SUBSCRIPTION_NOUPCOMINGMEALSUNSUCCESS_MSG = "No upcoming meals to pause";
    String SUBSCRIPTION_CONSECUTIVESKIPUNSUCCESS_MSG = "Consecutive skip cannot be done";

    //Create Subscription
    String SUBSCRIPTION_PREVIOUSDAYUNSUCCESS_MSG = "Start Date is before Today";
    String SUBSCRIPTION_STARTDATEMOREUNSUCCESS_MSG = "Subscription start date cannot be greater than 3 Days from today";
    String SUBSCRIPTION_BLANKORDERID_MSG = "orderId is Invalid";
    String SUBSCRIPTION_BLANKCUSTOMERID_MSG = "customerId is Invalid";
    String SUBSCRIPTION_BLANKSTARTDATE_MSG = "scheduleDetails.startDate is Invalid";
    String SUBSCRIPTION_NULLITEMID_MSG = "planDetails[0].itemId is Invalid";
    String SUBSCRIPTION_NULLTENURE_MSG = "planDetails[0].tenure is Invalid";
    String SUBSCRIPTION_NULLDELIVERYSTARTTIME_MSG = "deliverySlot.deliveryStartTime is Invalid";
    String SUBSCRIPTION_NULLDELIVERYENDTIME_MSG = "deliverySlot.deliveryEndTime is Invalid";
    String SUBSCRIPTION_DELIVERYSTARTTIMEGREATERTHANENDTIME_MSG = "Delivery end time cannot be before Delivery start time";

    //Pause Calender
    String SUBSCRIPTION_ALREADYPAUSEDSUBSCRIPTION_MSG = "Subscription is already Paused";
    String SUBSCRIPTION_NOUPCOMINGMEALSSUBSCRIPTION_MSG = "No Upcoming Meals Found";
    String SUBSCRIPTION_NOPAUSEAVAILABLESUBSCRIPTION_MSG = "Your subscription plan has reached expiry. You cannot Pause more.";
    String SUBSCRIPTION_PAUSEAVAILABLESUBSCRIPTION_MSG = "Pause plan for longer duration according to your convenience. Upto 7 days at a stretch.";
    String SUBSCRIPTION_NOSKIPAVAILABLESUBSCRIPTION_MSG = "Your subscription plan has reached expiry. You cannot skip more.";
    String SUBSCRIPTION_SKIPAVAILABLESUBSCRIPTION_MSG = "Your subscription plan will extend by one day if you choose to skip the upcoming meal.";

    //Create Subscription Wrapper
    String TRANSACTION_ID = "7998439031";
    String PAYMENT_STATUS = "FAILED";
    String PAYMENT_TYPE = "PRE_PAYMENT";
    String METADATA = "metadata";
    int TRANSACTION_AMOUNT = 100;
    String PAYMENT_METHOD = "Cash";
    String ORDER_CONTEXT = "ORDER_JOB";
    String REASON = "Connection Timeout";


/*
    String  ORDER_TYPE="DAILY";
    String  COUPON_CODE="SWIGGY";

    //Metadata Constant
    String CARTTYPE= "daily";
    Double ITEMTOTALWITHOUTDISCOUNT=732.5;
    int ITEMTOTALDISCOUNT= 50;
    Double ITEMTOTAL= 682.5;
    int CARTPACKINGCHARGE= 10;
    int CARTPACKINGCHARGEDISCOUNT= 0;
    Double CARTPACKINGCHARGESGST=0.25;
    Double CARTPACKINGCHARGECGST=0.25;
    int CARTPACKINGCHARGEIGST=0;
    Double CARTPACKINGCHARGETOTALWITHOUTDISCOUNT=10.5;
    Double CARTPACKINGCHARGETOTAL=10.5;

    String DELIVERYFEETYPE="distance_fee";
    String DELIVERYFEEDISPLAY="Distance Fee";
    String DELIVERYFEEMESSAGE="You are at distance";
    int DELIVERYFEEWITHOUTDISCOUNT=30;
    int DELIVERYFEEDISCOUNT=10;
    int DELIVERYFEE=0;

    int TOTALDELIVERYFEEWITHOUTDISCOUNT=30;
    int DELIVERYPRICEDISCOUNT=10;
    int TOTALDELIVERYFEES=20;

    int TRADEDISCOUNT=0;
    int SWIGGYDISCOUNT=0;

    String COUPONCODE="ABCDE";
    int COUPONDISCOUNT=5;
    String COUPONMESSAGE="Discount applied of 5 Rs";

    int CARTTOTALWITHOUTDISCOUNT=773;
    int CARTDISCOUNTTOTAL=65;
    int CARTTOTAL=708;

    String COUPONDESCRIPTION="Coupon Applied With Discount :5.0";
    boolean COUPONAPPLIED=true;

    Long STARTDATE=1550428200000L;
    int CARTTENURE=7;
    boolean HASWEEKEND=true;
    int STARTTIME=2300;
    int ENDTIME=1245;


    int ADDRESS_ID=1;
    int USERID=123;
    String ADDRESSDETAILSADDRESS="Test address";
    Double ADDRESSLAT=1.325123;
    Double ADDRESSLNG=103.924639;
    String FLATNO="#23";
    String ADDRESSCITY="Bangalore";
    String ADDRESSLANDMARK="opp to IBC";
    String ADDRESSANNOTATION="HOME";
    String ADDRESSAREA="BTM";
    String ADDRESSNAME="address";
    String ADDRESSMOBILE="7204691661";

    String TYPE="PRODUCT";
    String SERVICELINE="daily";
    String SKUID="123";
    String CARTITEMSNAME="Test name";
    int QUANTITY=1;

    int BASEPRICE=100;
    int BASEPRICEDISCOUNT=10;
    Double CGST=2.25;
    Double SGST=2.25;
    int IGST=0;
    int PACKINGCHARGE=40;
    int PACKAGINGCHARGEDISCOUNT=0;
    int CGSTPACKINGCHARGE=1;
    int IGSTPACKINGCHARGE=0;
    int SGSTPACKINGCHARGE=1;
    Double PRICINGDETAILITEMTOTALWITHOUTDISCOUNT=146.5;
    Double PRICINGDETAILITEMTOTAL=136.5;

    String STORENAME="Homely";
    String STOREAREA="Kormanagala";
    String STOREADDRESS="123";
    int STOREID=0;
    boolean PARTNER=false;
    String STORECITY="Bangalore";
    int CITYCODE=0;
    int AREACODE=0;
    String LOCALITY="area";
    String PHONENUMBERS="123";
    String CARTID="ae7044df-9fef-4c1d-bdf5-1ebd45578b46";

    String SUBSCRIPTIONMEALID="1234";
    String SUBTYPE="DAILY_SUBSCRIPTION";
    int VALUE=10;
    boolean REFUND=true;

    String REFUNDTRANSACTIONID="12323432";
    String ORDERJOBID="123223";
    int REFUNDAMOUNT=20;*/

    String STATUS = "FAILED";
    String DAILYSUBTYPE = "DAILY_SUBSCRIPTION_MEAL";
    String TYPE = "PRODUCT";
    String SPIN = "BELJHX8URT";
    String STORE_ID = "39533";
    String ITEM_NAME = "Daal Curry Chawal";
    String ITEMDETAILS_ID = "4_1";
    String PRODUCT_ID = "Z0853591V2";

    String PRODUCT_NAME = "Daal Curry Chawal";
    String SHORT_DESCRIPTION = "something";
    String LONG_DESCRIPTION = "something";
    int IS_VEG = 0;
    String IMAGES = "Swiggy-Daily/f7cgdflaxlt4iyyonffy";

    int QUANTITY = 1;

    int TOTALWITHOUTDISCOUNT = 115;
    double TOTAL = 99.5;
    int DISCOUNT = 15;
    int ITEMCHARGE_TOTALWITHOUTDISCOUNT = 105;
    double ITEMCHARGETOTAL = 94.5;
    int ITEMCHARGEDISCOUNT = 10;
    int PRICE = 100;

    String TAXTYPE = "GST";
    double CGST = 2.25;
    int SGST = 0;
    double IGST = 2.25;
    double TAXTOTAL = 4.5;
    boolean INCLUSIVETAX = false;

    int ITEMPACKAGINGCHARGETOTALWITHOUTDISCOUNT = 10;
    int ITEMPACKAGINGCHARGETOTAL = 5;
    int ITEMPACKAGINGCHARGEDISCOUNT = 5;
    int ITEMPACKAGINGCHARGEPRICE = 10;

    String TAX_TYPE = "GST";
    int TAX_CGST = 0;
    int TAX_SGST = 0;
    int TAX_IGST = 0;
    int TAX_TOTAL = 0;

    String ITEM_TYPE = "ADDON";
    String ITEM_SPIN = "RBZBK3IJ9X";
    String ITEM_STORE_ID = "13584";
    String ITEM__NAME = "Daal Curry Chawal";
    String ITEM__ID = "4_1";
    String ITEM_PRODUCT_ID = "SHYIO3Z30F";

    String METAPRODUCT_NAME = "Daal Curry Chawal";
    String METASHORT_DESCRIPTION = "something";
    String METALONG_DESCRIPTION = "something";
    String METAIS_VEG = "true";
    String METAIMAGE = "Swiggy-Daily/f7cgdflaxlt4iyyonffy";

    int ITEM_QUANTITY = 2;
    int ITEMBILL_TOTALWITHOUTDISCOUNT = 115;
    double ITEMBILLTOTAL = 99.5;
    int ITEMBILLDISCOUNT = 15;

    int ITEMCHARGE__TOTALWITHOUTDISCOUNT = 105;
    double ITEMCHARGE_TOTAL = 94.5;
    int ITEMCHARGE_DISCOUNT = 10;
    int ITEMCHARGE_PRICE = 100;

    String TAX__TYPE = "GST";
    double TAX__CGST = 2.25;
    int TAX__SGST = 0;
    double TAX__IGST = 2.25;
    double TAX__TOTAL = 4.5;

    int ITEMPACKAGINGCHARGE_TOTALWITHOUTDISCOUNT = 10;
    int ITEMPACKAGINGCHARGE_TOTAL = 5;
    int ITEMPACKAGINGCHARGE_DISCOUNT = 5;
    int ITEMPACKAGINGCHARGE_PRICE = 10;

    String TAX___TYPE = "GST";
    int TAX___CGST = 0;
    int TAX___SGST = 0;
    int TAX___IGST = 0;
    int TAX___TOTAL = 0;

    int PRICINGDETAIL_TOTALWITHOUTDISCOUNT = 270;
    int PRICINGDETAIL_TOTAL = 224;
    int PRICINGDETAIL_DISCOUNT = 46;

    int ITEMTOTALCHARGES_TOTALWITHOUTDISCOUNT = 230;
    int ITEMTOTALCHARGES_TOTAL = 199;
    int ITEMTOTALCHARGES_DISCOUNT = 31;

    int CARTPACKINGCHARGES_TOTALWITHOUTDISCOUNT = 10;
    int CARTPACKINGCHARGES_TOTAL = 10;
    int CARTPACKINGCHARGES_DISCOUNT = 0;
    int CARTPACKINGCHARGES_PRICE = 10;

    String TAX____TYPE = "INCLUSIVE";
    int TAX____TOTAL = 0;
    boolean PRICINGDETAIL_INCLUSIVETAX = true;

    int DELIVERYPRICEDETAILS_TOTALWITHOUTDISCOUNT = 30;
    int DELIVERYPRICEDETAILS_TOTAL = 20;
    int DELIVERYPRICEDETAILS_DISCOUNT = 10;

    int DELIVERYFEELIST_TOTALWITHOUTDISCOUNT = 30;
    int DELIVERYFEELIST_TOTAL = 20;
    int DELIVERYFEELIST_DISCOUNT = 10;
    String DELIVERYFEETYPE = "distance_fee";
    String DELIVERYFEEDISPLAY = "Distance Fee";
    String DELIVERYFEEMESSAGE = "You are at distance";

    int TRADEDISCOUNT = 0;
    int SWIGGYDISCOUNT = 0;

    String COUPONCODE = "ABCDE";
    int COUPONDISCOUNT = 5;
    String COUPONMESSAGE = "Discount applied of 5 Rs";

    String COUPONDETAIL_COUPONCODE = "ABCDE";
    String COUPONDESCRIPTION = "Coupon Applied With Discount :5.0";
    boolean COUPONAPPLIED = true;

    int STOREDETAIL_ID = 13584;
    String UUID = "c4162350-276d-41be-9dab-1b524239a34f";
    String STOREDETAIL_NAME = "Veggie Delights";
    String STOREDETAIL_CITY = "Kolkata";
    int CITYCODE = 7;
    String STOREDETAIL_AREA = "Lake Town";
    int AREACODE = 175;
    String STOREDETAIL_ADDRESS = "106, Bangur Avenue, Block D, Kol - 55.";
    String LATLONG = "22.604885,88.41236000000004";
    String LOCALITY = "Near D Block Park";
    String PHONENUMBERS = "9088338338";

    int DELIVERYSCHEDULE_TENURE = 7;
    boolean DELIVERYSCHEDULE_HASWEEKEND = false;

    int ADDRESSDETAIL_ID = 33938715;
    int USERID = 361367;
    boolean DEFAULTADDRESS = false;
    String ADDRESSDETAIL_NAME = "jinesh";
    String ADDRESSDETAIL_MOBILE = "7674009028";
    String ADDRESSDETAIL_ADDRESS = "Bhavani Nagar, Suddagunte Palya, Bengaluru, Karnataka 560029, India";
    String ADDRESSDETAIL_LANDMARK = "Djfj";
    String ADDRESSDETAIL_AREA = "Suddagunte Palya";
    double ADDRESSDETAIL_LAT = 12.932383;
    double ADDRESSDETAIL_LNG = 77.603586;
    String CREATEDON = "2018-08-24T12:34:43.000+0000";
    String UPDATEDON = "2018-10-31T14:42:45.000+0000";
    boolean DELETED = true;
    boolean EDITED = true;
    String FLATNO = "Qoe";
    boolean REVERSEGEOCODEFAILED = false;

    String ORDEREDITTYPE = "SWAP";
    String ORDEREDITTYPEADDON = "EXTRA";
    String SUBSCRIPTIONMEALID = "81187";

    String ORDER_TYPE = "DAILY";

    //Swap & Addon Meal
    String SWAP_UNSUCCESSPOSTCUTOFF_MSG = "This Meal is not your upcoming meal";
    String SWAP_UNSUCCESSALREADYSWAPPED_MSG = "Subscription Meal cannot be edited";
    String SWAP_UNSUCCESSNOTACTIVE_MSG="Subscription is not Active";

    //Subscription Bill
    String SUBSCRIPTION_TYPE_PLAN="PLAN";
    String SUBSCRIPTION_TYPE_PRODCUT="PRODUCT";
    String PREORDER_TYPE="DAILY_PREORDER";
    String DAILY_SUBSCRIPTION_MEAL_TYPE="DAILY_SUBSCRIPTION_MEAL";

    //Subscription Cancel
    String SUBSCRIPTION_CANCEL_REASON="Not Available";

    //Generate Order ID
    String SUBSCRIPTION_TYPE="DATA_COLLECT_AND_SEND";
    String SUBSCRIPTION_NAME="SUBSCRIPTION_ORDER_CREATE_TASK";
    int SUBSCRIPTION_RETRYCOUNT=5;
    boolean SUBSCRIPTION_AUDIT=true;
    int SUBSCRIPTION_FETCHRETRYCOUNT=5;
    int SUBSCRIPTION_pageSize=1;
    int SUBSCRIPTION_MESSAGERETRYCOUNT=5;
    int SUBSCRIPTION_STARTTIME=1900;
    int SUBSCRIPTION_ENDTIME=1945;


}
