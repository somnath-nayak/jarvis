package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order_jobs
{
    /*@JsonProperty("metadata")
    private String metadata;*/

    @JsonProperty("updated_at")
    private String updated_at;

    @JsonProperty("created_at")
    private String created_at;

    @JsonProperty("customer_info")
    private Customer_info customer_info;

    @JsonProperty("payment_info")
    private Payment_info[] payment_info;

    @JsonProperty("merchant_id")
    private String merchant_id;

    @JsonProperty("customer_id")
    private String customer_id;

    @JsonProperty("status_meta")
    private String status_meta;

    @JsonProperty("order_job_id")
    private String order_job_id;

    @JsonProperty("status")
    private String status;

    /*@JsonProperty("metadata")
    public String getMetadata ()
    {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata (String metadata)
    {
        this.metadata = null;
    }*/

    @JsonProperty("updated_at")
    public String getUpdated_at ()
    {
        return updated_at;
    }

    @JsonProperty("updated_at")
    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    @JsonProperty("created_at")
    public String getCreated_at ()
    {
        return created_at;
    }

    @JsonProperty("created_at")
    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    @JsonProperty("customer_info")
    public Customer_info getCustomer_info ()
    {
        return customer_info;
    }

    @JsonProperty("customer_info")
    public void setCustomer_info (Customer_info customer_info)
    {
        this.customer_info = customer_info;
    }

    @JsonProperty("payment_info")
    public Payment_info[] getPayment_info ()
    {
        return payment_info;
    }

    @JsonProperty("payment_info")
    public void setPayment_info (Payment_info[] payment_info)
    {
        this.payment_info = payment_info;
    }

    @JsonProperty("merchant_id")
    public String getMerchant_id ()
{
    return merchant_id;
}

    @JsonProperty("merchant_id")
    public void setMerchant_id (String merchant_id)
    {
        this.merchant_id = merchant_id;
    }

    @JsonProperty("customer_id")
    public String getCustomer_id ()
    {
        return customer_id;
    }

    @JsonProperty("customer_id")
    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    @JsonProperty("status_meta")
    public String getStatus_meta ()
{
    return status_meta;
}

    @JsonProperty("status_meta")
    public void setStatus_meta (String status_meta)
    {
        this.status_meta = status_meta;
    }

    @JsonProperty("order_job_id")
    public String getOrder_job_id ()
    {
        return order_job_id;
    }

    @JsonProperty("order_job_id")
    public void setOrder_job_id (String order_job_id)
    {
        this.order_job_id = order_job_id;
    }

    @JsonProperty("status")
    public String getStatus ()
    {
        return status;
    }

    @JsonProperty("status")
    public void setStatus (String status)
    {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [metadata = "+""+", updated_at = "+updated_at+", created_at = "+created_at+", customer_info = "+customer_info+", payment_info = "+payment_info+", merchant_id = "+merchant_id+", customer_id = "+customer_id+", status_meta = "+status_meta+", order_job_id = "+order_job_id+", status = "+status+"]";
    }
}
