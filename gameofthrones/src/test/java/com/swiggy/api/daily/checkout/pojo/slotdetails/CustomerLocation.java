package com.swiggy.api.daily.checkout.pojo.slotdetails;

import org.codehaus.jackson.annotate.JsonProperty;

public class CustomerLocation {

    @JsonProperty("lat")
    private Float lat;
    @JsonProperty("lng")
    private Float lng;

    @JsonProperty("lat")
    public Float getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Float lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public Float getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Float lng) {
        this.lng = lng;
    }

}
