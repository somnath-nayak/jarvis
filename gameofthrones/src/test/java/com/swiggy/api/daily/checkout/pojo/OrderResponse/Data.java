package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data
{
    private Transaction_payment_details[] transaction_payment_details;

    private String order_group_id;

    private Orders[] orders;

    private String customer_id;

    public Transaction_payment_details[] getTransaction_payment_details ()
    {
        return transaction_payment_details;
    }

    public void setTransaction_payment_details (Transaction_payment_details[] transaction_payment_details)
    {
        this.transaction_payment_details = transaction_payment_details;
    }

    public String getOrder_group_id ()
    {
        return order_group_id;
    }

    public void setOrder_group_id (String order_group_id)
    {
        this.order_group_id = order_group_id;
    }

    public Orders[] getOrders ()
    {
        return orders;
    }

    public void setOrders (Orders[] orders)
    {
        this.orders = orders;
    }

    public String getCustomer_id ()
{
    return customer_id;
}

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [transaction_payment_details = "+transaction_payment_details+", order_group_id = "+order_group_id+", orders = "+orders+", customer_id = "+customer_id+"]";
    }
}
