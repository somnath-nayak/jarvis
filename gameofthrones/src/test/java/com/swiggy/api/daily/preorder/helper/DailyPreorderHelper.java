package com.swiggy.api.daily.preorder.helper;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.pojo.CancelPreorderSubscriptionMealPojo;
import com.swiggy.api.daily.preorder.pojo.CreatePreorederPojo;
import com.swiggy.api.daily.subscription.pojo.ServerTimeChangePojo;
import com.swiggy.api.sf.rng.constants.RngConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class DailyPreorderHelper {

    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();

    public Processor createPreorder(String data)
    {
        Initialize gameofthrones = new Initialize();
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createPreorder", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }
    public Processor createPreorder() throws IOException {
        CreatePreorederPojo preorederPojo = new CreatePreorederPojo();
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData();
        String data = jsonHelper.getObjectToJSON(preorederPojo);
        Initialize gameofthrones = new Initialize();
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "createPreorder", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }
    public Processor cancelPreorder(String orderId)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "cancelPreorder", gameofthrones);
        String[] payloadparams = new String[] {orderId};
        Processor processor = new Processor(service, requestHeaders, null, payloadparams);
        return processor;
    }
    public Processor getPreorderDetails()
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", RngConstants.basic_auth);
        GameOfThronesService service = new GameOfThronesService("", "", gameofthrones);
        String[] queryparam = new String[] {};
        Processor processor = new Processor(service, requestHeaders, null,queryparam);
        return processor;

    }

    public Processor cancelPreorderAndSubscriptionMeal(String initiatedBy, int id,String orderType,String reason) throws IOException {
        CancelPreorderSubscriptionMealPojo cancelPreorderSubscriptionMealPojo = new CancelPreorderSubscriptionMealPojo();
        cancelPreorderSubscriptionMealPojo.withId(id).withInitiatedBy(initiatedBy).withOrderType(orderType).withReason(reason);
        JsonHelper helper = new JsonHelper();
        String data = helper.getObjectToJSON(cancelPreorderSubscriptionMealPojo);
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Authorization", RngConstants.basic_auth);
        GameOfThronesService service = new GameOfThronesService("dailysubscription", "cancelPreorderAndSubscriptionMeal", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }


    public long getDeliveryTime(int hour,int min)
    {
        return Long.parseLong(DateUtility.getFutureTimeInReadableFormatWithColon(hour,min).replace(":",""));
    }
    public Processor changeServerDateTimeOfDailyManager(String time) throws IOException {
        ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
        serverTimeChangePojo.withQuery("sudo timedatectl set-time  "+time).
                withArg("-i").withBoxip("ubuntu@daily-manager-0.u2.swiggy.in").withPemname("dragonstone.pem");
        String data = jsonHelper.getObjectToJSON(serverTimeChangePojo);
        Initialize gameofthrones = new Initialize();
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("webtest", "timecreate", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

}
