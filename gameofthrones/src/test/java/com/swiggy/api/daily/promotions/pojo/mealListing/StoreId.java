package com.swiggy.api.daily.promotions.pojo.mealListing;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class StoreId {
    @JsonProperty("storeId")
    private String storeId;
    @JsonProperty("items")
    private ArrayList<Items> items;
    
    public StoreId(){
    
    }
    
    public String getStoreId() {
        return storeId;
    }
    
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
    
    public ArrayList<Items> getItems() {
        return items;
    }
    
    public void setItems(ArrayList<Items> items) {
        this.items = items;
    }
    
   // builder
    public StoreId withStoreId(String storeId){
        setStoreId(storeId);
        return this;
    }
    
    public StoreId withItems(ArrayList<Items> itemsList){
        setItems(itemsList);
        return this;
    }
    
    public StoreId withGivenValue(String storeId, String[] allSkuId , List<Integer> price ){
       ArrayList<Items> itemsList= new Items().listOfItems( allSkuId , price );
       setItems(itemsList);
       setStoreId(storeId);
       
       return this;
    }
    
}
