package com.swiggy.api.daily.vendor.pojo;

import com.swiggy.automation.tps.api.sanity.PublishOrdertoQueue;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ProductId {
    
    @JsonProperty("product-ids")
    private List<String> productIds;
    
    public List<String> getProductIds() {
        return productIds;
    }
    
    public void setProductIds(List<String> productIds) {
        this.productIds = productIds;
    }
    
    public ProductId() {
        List<String> id = new ArrayList<>();
        id.add("");
    }
    
    public ProductId(String productId) {
        List<String> id = new ArrayList<>();
        id.add(productId);
        this.productIds = id;
    }
    
    public ProductId withProductId(List<String> productIds) {
        this.productIds = productIds;
        return this;
    }
    
    public ProductId setDefault(){
        
        return this;
        
    }
}
