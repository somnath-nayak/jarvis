
package com.swiggy.api.daily.subscription.pojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.commons.lang.StringEscapeUtils;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


public class Order {
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("metadata")
    private String metadata;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Order withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("metadata")
    public String getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public Order withMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    public Order setDefaultData(){
        return this.withOrderId("1600");
    }

    public Order withEscapedMetaData(Metadata metadata) throws IOException {
        return  this.withMetadata((new JsonHelper().getObjectToJSON(metadata)));

    }
}
