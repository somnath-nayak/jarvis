package com.swiggy.api.daily.vendor.tests;

import com.swiggy.api.daily.vendor.dp.VendorDp;
import com.swiggy.api.daily.vendor.helper.TestcaseHelper;
import com.swiggy.api.daily.vendor.helper.VendorCommonHelper;
import com.swiggy.api.daily.vendor.helper.VendorConstant;
import com.swiggy.api.daily.vendor.helper.VendorHelper;
import com.swiggy.api.daily.vendor.pojo.*;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class VendorTest extends VendorDp {

    LoginPOJO loginPOJO = new LoginPOJO();
    VendorHelper vendorHelper = new VendorHelper();
    VendorCommonHelper vendorCommonHelper = new VendorCommonHelper();
    SlotServicePOJO slotServicePOJO = new SlotServicePOJO();
    PausePOJO pausePOJO = new PausePOJO();
    HolidaySlotPOJO holidaySlotPOJO = new HolidaySlotPOJO();
    RestaurantSlotPOJO restaurantSlotPOJO = new RestaurantSlotPOJO();
    TestcaseHelper testcaseHelper = new TestcaseHelper();
    RMSHelper rmsHelper = new RMSHelper();
    JsonHelper jsonHelper=new JsonHelper();
    Processor processor;

    public static final Logger logger = Logger.getGlobal();

    @BeforeTest
    public void beforeTestExecution() {
        vendorCommonHelper.deleteDBEntries();
    }

    @AfterTest
    public void afterTestExecution() {
        vendorCommonHelper.deleteDBEntries();
    }

//    ------------------------------------------------------------------------------------------LOGIN------------------------------------------------------------------------------------------

    @Test(dataProvider = "loginSuccess", dataProviderClass = VendorDp.class, description = "Verify that User is able to Login with valid credentials", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginSuccessTest(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginSuccessful(processor);
    }

    @Test(dataProvider = "loginFailure", dataProviderClass = VendorDp.class, description = "Verify that User is not able to Login - Failure Scenarios", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginInvalidTestcases(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
    }

    @Test(dataProvider = "loginError", dataProviderClass = VendorDp.class, description = "Verify that User is not able to Login with valid Username and invalid Password", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginWithValidUsernameAndInvalidPassword(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginValidUsernameInvalidPassword(processor);
    }

    @Test(dataProvider = "loginAttempt", dataProviderClass = VendorDp.class, description = "Verify that there is a limit on the total number of unsuccessful login attempts - Max 5 attempts allowed", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginAttemptMaxLimitCheck(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
    }

    @Test(dataProvider = "loginSuccess", dataProviderClass = VendorDp.class, description = "Validate the response with parameter \"restMetaData\" must have valid restaurant data mapped to it", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginTest1(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginTest1(processor);
    }

    @Test(dataProvider = "loginSuccess", dataProviderClass = VendorDp.class, description = "Validate the response with parameter \"latlong\" must have valid restaurant data mapped to it", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginTest2(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginTest2(processor);
    }

    @Test(dataProvider = "loginPayload", dataProviderClass = VendorDp.class, description = "Validate the response with parameter \"userType\" must have valid restaurant data mapped to it", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginTest3(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginTest3(processor);
    }

    @Test(dataProvider = "loginFailure", dataProviderClass = VendorDp.class, description = "Verify the response after sending an invalid/incomplete payload", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void loginInvalidTestcases1(String payload) throws Exception {
        processor = vendorHelper.login(payload);
        testcaseHelper.loginInvalidCredentails(processor);
    }

//    ---------------------------------------------------------------------------------RESTAURANT AVAILABILITY---------------------------------------------------------------------------------

    @Test(description = "Get the details of the restaurant available", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void getRestaurantAvailabilityTest() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);
    }

    @Test(description = "Get the details of the restaurant availability on the basis of invalid restID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void getInvalidRestaurantAvailabilityTest() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.invalidrestaurantID);
        testcaseHelper.invalidRestaurant(processor);
    }

    @Test(description = "Validate the response of the API in respect to a valid payload", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationRestAvailabilityTest() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);
    }

    @Test(description = "Checking the start and end time of the restaurant depending on the time slot", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest1() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "Check the availability for restaurant for a particluar number of days in response", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest2() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "Check the timeline for the availability for particular restaurant", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest3() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "The start_time and end_time for each day in which the restaurant is available must be displayed depending on the \"size_in_days\" parameter", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest4() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "The start_time and end_time for each day in which the restaurant is available must be displayed depending on the \"size_in_days\" parameter", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest5() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "Validate the response with parameter \"id\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest6() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        SoftAssert softAssert = new SoftAssert();
        String id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertNotNull(id);
        softAssert.assertAll();
    }

    @Test(description = "Validate the response with parameter \"id\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest7() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        SoftAssert softAssert = new SoftAssert();
        String id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertNotNull(id);
        softAssert.assertAll();
    }

    @Test(description = "Validate the response with parameter \"entity\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest8() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        SoftAssert softAssert = new SoftAssert();
        String entity = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.entity"));
        softAssert.assertNotNull(entity);
        softAssert.assertAll();
    }

    @Test(description = "Validate the response for the parameter \"merges\" and check that merges gets no other value than RESTAURANT", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest9() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        SoftAssert softAssert = new SoftAssert();
        String merges = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.merges"));
        softAssert.assertNotNull(merges);
        softAssert.assertAll();
    }

    @Test(description = "Validate the response for the parameter \"merges\" and check that merges gets no other value than RESTAURANT", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void responseValidationTest10() throws Exception {
        processor = vendorHelper.getRestaurantAvailability(VendorConstant.restaurantID);
        testcaseHelper.startTimeAndEndTime(processor);
    }

    @Test(description = "Get the details of the timeline (availability of the restaurant) if the holidays slots are decreased in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase03() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);
        softAssert.assertAll();

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);
    }

    @Test(description = "Get the details of the server and validate the availability of the restaurant inclusion of holiday slots in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase01() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time),VendorConstant.MON,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);
    }

    @Test(description = "Get the details of the timeline availability of the restaurant) if the holidays slots are increased in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase02() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);

        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);
        softAssert.assertAll();

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
    }

    @Test(description = "Get the details of the API if a restaurant denies to serve after a certain period of time in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase04() throws Exception {

        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);
        softAssert.assertAll();

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id);

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
        vendorCommonHelper.deleteRestNormalSlots(id);
    }

    @Test(description = "Check whether the timeline is calculated for the restaurant in accordance to the size of days and holiday slots", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase05() throws Exception {

        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id);
        softAssert.assertAll();

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
        vendorCommonHelper.deleteRestNormalSlots(id);
    }


    @Test(description = "Get the details of the timeline for the restaurant if the restaurant slots are increased in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase06() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.WED,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id1 = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id1+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id1);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.THU,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id2 = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id2+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id2);
        softAssert.assertAll();

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
        vendorCommonHelper.deleteRestNormalSlots(id);
        vendorCommonHelper.deleteRestNormalSlots(id1);
        vendorCommonHelper.deleteRestNormalSlots(id2);
    }

    @Test(description = "Get the details of the timeline for the restaurant if the restaurant slots are increased in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase07() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id+"<<<<<<<<<<<<<<<<++++++++++++++++++++++++++>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id);
        softAssert.assertAll();

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
        vendorCommonHelper.deleteRestNormalSlots(id);
    }

    @Test(description = "Get the details of the timeline for the restaurant if the restaurant slots are decreased in SRS - Catalog DB", groups = {"Integration", "Neha Pandey"})
    public void integrationTestcase08() throws Exception {
        holidaySlotPOJO.setDefault(vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern1),VendorConstant.rest_id,vendorCommonHelper.createFutureDateTimePattern(VendorConstant.timePattern2));
        processor = vendorHelper.createHolidaySlots(holidaySlotPOJO);
        SoftAssert softAssert = new SoftAssert();
        String from_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.from_time"));
        softAssert.assertNotNull(from_time);
        String to_time = String.valueOf(processor.ResponseValidator.GetNodeValue("$.data.to_time"));
        softAssert.assertNotNull(to_time);

        restaurantSlotPOJO.setDefault(vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.close_time2),VendorConstant.TUE,vendorCommonHelper.convertIntoMilitaryTime(VendorConstant.open_time2),VendorConstant.rest_id);
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        int id = processor.ResponseValidator.GetNodeValueAsInt("$.data.id");
        logger.info(id+"<<<<<<<<<<<<<<<<------------------>>>>>>>>>>>>>>>>>>>");
        softAssert.assertNotNull(id);
        softAssert.assertAll();

        processor = vendorHelper.getRestaurantAvailability(VendorConstant.rest_id);
        testcaseHelper.getRestaurantAvailabilityDetails(processor);

        vendorCommonHelper.deleteRestHolidaySlots(from_time,to_time);
        vendorCommonHelper.deleteRestNormalSlots(id);
    }

//    --------------------------------------------------------------------------------------ORDER DISPATCH--------------------------------------------------------------------------------------

    @Test(dataProvider = "orderDispatch", dataProviderClass = VendorDp.class, description = "Fetch the details of the order for a particular restaurant", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void orderDispatchSuccess(String order) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor = vendorHelper.orderDispatch(order, access_token);
        testcaseHelper.orderDispatchSuccess(processor);
    }

    @Test(dataProvider = "orderDispatch", dataProviderClass = VendorDp.class, description = "Validate response from Order Dispatch API", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void orderDispatchResponse(String order) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor = vendorHelper.orderDispatch(order, access_token);
        testcaseHelper.orderDispatchSuccess(processor);
    }

    @Test(dataProvider = "orderDispatch", dataProviderClass = VendorDp.class, description = "Fetch the details of the order with a valid restaurant ID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void orderDispatchDetailsResponse(String order) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor  = vendorHelper.orderDispatch(order, access_token);
        testcaseHelper.orderDispatchSuccess(processor);
    }

    @Test(dataProvider = "orderDispatchFailure", dataProviderClass = VendorDp.class, description = "Fetch the details of the order with a invalid restaurant ID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void orderDispatchDetailsInvalidRestaurantResponse(String order) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor = vendorHelper.orderDispatch(order, access_token);
        testcaseHelper.orderDispatchInvalidRest(processor);
    }

    @Test( description = "The order is must be fetched from Redis and data must be displayed on Vendor Order Dispatch screen.", groups = {"Integration", "Neha Pandey"})
    public void pushMessageInRabbitMQ() throws Exception {
        loginPOJO = new LoginPOJO().setDefault("9990", VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        String msg = testcaseHelper.pushMessageInRabbitMQ();
        System.out.println(msg);

        OrderDispatchPOJO orderDispatchPOJO = new OrderDispatchPOJO().setDefault(rest_id);
        processor = vendorHelper.orderDispatchAPI(orderDispatchPOJO, access_token);
        testcaseHelper.orderDispatchSuccess(processor);
    }

//    --------------------------------------------------------------------------------------PAUSE API--------------------------------------------------------------------------------------

    @Test(dataProvider = "pauseItemSuccess", dataProviderClass = VendorDp.class, description = "Pause Success Cases", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pauseTest(String data) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        Processor login = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(login.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token+ "------ Access token");
        softAssert.assertAll();

        processor = vendorHelper.pause(data,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);
    }

    @Test(description = "An item is paused for a particular restaurant ID and the entity_id(item) which is paused must reflect in the Catalog DB - slot_info table ", groups = {"Sanity", "Regression", "Integration", "Neha Pandey"})
    public void pauseIntegrationTest() throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        testcaseHelper.loginSuccessful(processor);
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);

        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(6000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        slotServicePOJO.setDefault(Integer.parseInt(VendorConstant.rest_id), true,date1,date2, VendorConstant.entityID, from_time, to_time,"DAILY_STORE");
        List list = new ArrayList();
        list.add(slotServicePOJO);
        processor = vendorHelper.createSlotsVendorDaily(list, access_token);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        String start_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
        softAssert.assertNotNull(start_time);
        String end_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
        softAssert.assertNotNull(end_time);
        String live_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
        softAssert.assertNotNull(live_date);
        String expiry_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
        softAssert.assertNotNull(expiry_date);
        softAssert.assertAll();

        pausePOJO.setDefault(store_id,entity_id,live_date,expiry_date,start_time,end_time,VendorConstant.source);
        processor = vendorHelper.pauseVendor(pausePOJO,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);
    }

    @Test(description = "Vendor must be allowed to pause multiple items for a particular slot TODAY", groups = {"Sanity", "Regression", "Integration", "Neha Pandey"})
    public void pauseTODAYTest() throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        testcaseHelper.loginSuccessful(processor);
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        softAssert.assertNotNull(access_token);

        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        slotServicePOJO.setDefault(Integer.parseInt(VendorConstant.rest_id), true,date1,date2, VendorConstant.entityID, from_time, to_time,"DAILY_STORE");
        List list = new ArrayList();
        list.add(slotServicePOJO);
        processor = vendorHelper.createSlotsVendorDaily(list, access_token);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        String start_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
        softAssert.assertNotNull(start_time);
        String end_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
        softAssert.assertNotNull(end_time);
        String live_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
        softAssert.assertNotNull(live_date);
        String expiry_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
        softAssert.assertNotNull(expiry_date);
        softAssert.assertAll();

        pausePOJO.setDefault(store_id,entity_id,live_date,expiry_date,start_time,end_time,VendorConstant.source);
        processor = vendorHelper.pauseVendor(pausePOJO,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);
    }

    @Test(dataProvider = "pauseItemFailure", dataProviderClass = VendorDp.class, description = "Pause failure cases", groups = {"Sanity", "Regression", "Integration", "Neha Pandey"})
    public void pauseFailureTest(String data) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor = vendorHelper.pause(data,access_token);
        testcaseHelper.pauseForItemFailed(processor);
    }

    @Test(description = "Pausing multiple items on all the slots for TODAY", groups = {"Sanity", "Regression", "Integration", "Neha Pandey"})
    public void pauseTest01() throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);

        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        slotServicePOJO.setDefault(Integer.parseInt(VendorConstant.rest_id), true,date1,date2, vendorCommonHelper.randomNumber(), from_time, to_time,"DAILY_STORE");
        List list = new ArrayList();
        list.add(slotServicePOJO);
        processor = vendorHelper.createSlotsVendorDaily(list, access_token);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        String start_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
        softAssert.assertNotNull(start_time);
        String end_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
        softAssert.assertNotNull(end_time);
        String live_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
        softAssert.assertNotNull(live_date);
        String expiry_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
        softAssert.assertNotNull(expiry_date);
        pausePOJO.setDefault(store_id,entity_id,live_date,expiry_date,start_time,end_time,VendorConstant.source);
        processor = vendorHelper.pauseVendor(pausePOJO,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);

        String date3 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time1 = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date4 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time1 = vendorCommonHelper.getCurrentTime();
        slotServicePOJO.setDefault(Integer.parseInt(VendorConstant.rest_id), true,date3,date4, vendorCommonHelper.randomNumber(), from_time1, to_time1,"DAILY_STORE");
        List list1 = new ArrayList();
        list1.add(slotServicePOJO);
        processor = vendorHelper.createSlotsVendorDaily(list1, access_token);
        String entity_id1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id1);
        String store_id1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id1);
        String start_time1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
        softAssert.assertNotNull(start_time1);
        String end_time1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
        softAssert.assertNotNull(end_time1);
        String live_date1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
        softAssert.assertNotNull(live_date1);
        String expiry_date1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
        softAssert.assertNotNull(expiry_date1);
        softAssert.assertAll();
        pausePOJO.setDefault(store_id1,entity_id1,live_date1,expiry_date1,start_time1,end_time1,VendorConstant.source);
        processor = vendorHelper.pauseVendor(pausePOJO,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);

    }

    @Test(description = "Create and delete the item details from Slot Service and then try to pause the item", groups = {"Integration", "Neha Pandey"})
    public void pauseTest02() throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        testcaseHelper.loginSuccessful(processor);
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);

        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        slotServicePOJO.setDefault(Integer.parseInt(VendorConstant.rest_id), true,date1,date2, VendorConstant.entityID, from_time, to_time,"DAILY_STORE");
        List list = new ArrayList();
        list.add(slotServicePOJO);
        processor = vendorHelper.createSlotsVendorDaily(list, access_token);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        String start_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].start_time"));
        softAssert.assertNotNull(start_time);
        String end_time = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].end_time"));
        softAssert.assertNotNull(end_time);
        String live_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].live_date"));
        softAssert.assertNotNull(live_date);
        String expiry_date = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[0].slot_info_list[0].expiry_date"));
        softAssert.assertNotNull(expiry_date);
        softAssert.assertAll();

        vendorCommonHelper.selectStoreIDInSlotInfoTable(entity_id);
        Thread.sleep(2000);
        vendorCommonHelper.deleteStoreIDInSlotInfoTable(entity_id);

        pausePOJO.setDefault(store_id,entity_id,live_date,expiry_date,start_time,end_time,VendorConstant.source);
        processor = vendorHelper.pauseVendor(pausePOJO,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);
    }

    @Test(dataProvider = "pauseItemSuccess", dataProviderClass = VendorDp.class, description = "Integration Testcase - If the item is not present in the Catalog DB - Slot_info table , vendor must not be allowed to pause the item and accept orders depending on the max limit", groups = {"Integration", "Neha Pandey"})
    public void pauseTest03(String data) throws Exception {
        loginPOJO = new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1);
        processor = vendorHelper.login(jsonHelper.getObjectToJSON(loginPOJO));
        SoftAssert softAssert = new SoftAssert();
        String rest_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants[0].rest_id"));
        softAssert.assertNotNull(rest_id);
        String access_token = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.access_token"));
        logger.info(access_token);
        softAssert.assertNotNull(access_token);
        softAssert.assertAll();

        processor = vendorHelper.pause(data,access_token);
        testcaseHelper.pauseForItemSuccessful(processor);
    }

//    --------------------------------------------------------------------------------------PAST ORDERS--------------------------------------------------------------------------------------

    @Test(description = "Fetch the details of the order with a valid restaurant_id", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrdersSuccess() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18730234318","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order_status changes to \"reached\" once the the order is reached to the user", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrdersReachedTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18817106680","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order_status changes to \"Delivered\" once the DE delivers the order", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrdersDeliveredTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18730234318","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order_status changes to \"pickedup\" once the DE picks up the order", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrdersPickedUpTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18862192569","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order_status changes to \"Cancelled\" once the the preorder is cancelled by the user", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrdersCancelledTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18774217507","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check the order details in the response from the API", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrderResponseValiodationTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18730234318","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order dispatch details are getting fetched for an invalid restuarant ID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrderInvalidRestaurantIDTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.invalidrestaurantID, "18730234318","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

    @Test(description = "Check whether the order dispatch details are getting fetched for an invalid order ID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrderInvalidOrderIDTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, VendorConstant.invalidOrderID,"20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.pastOrdersInvalid(processor);
    }

    @Test(description = "Check whether the order dispatch details are getting fetched for an invalid restuarant ID and order ID", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrderInvalidRestIDandOrderIDTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.invalidrestaurantID, VendorConstant.invalidOrderID,"20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.pastOrdersInvalid(processor);
    }

    @Test(description = "The data is deleted from Finance DB, and then the details are fetched for Past orders", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void pastOrderIntegrationTest() throws Exception {
        processor = vendorHelper.pastOrders(VendorConstant.rest_id, "18730234318","20","0", "2019-01-20", "2019-01-28");
        testcaseHelper.getPastOrders(processor);
    }

//    ---------------------------------------------------------------------------------- PAGINATED SEARCH ------------------------------------------------------------------------------------------

    @Test(description = "Search with valid city, page number, rows per page - Paginated Search", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void paginatedSearchTest01() throws Exception {
        processor = vendorHelper.paginatedSearch(VendorConstant.cityId,VendorConstant.random_10,VendorConstant.random_10);
        testcaseHelper.paginatedSearchSuccess(processor);
    }

    @Test(description = "Search with invalid cityID - Paginated Search", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void paginatedSearchTest02() throws Exception {
        processor = vendorHelper.paginatedSearch(VendorConstant.invalidCityID,VendorConstant.random_10,VendorConstant.random_10);
        testcaseHelper.paginatedSearchInvalidCityID(processor);
    }

    @Test(description = "Page 0 validation - Paginated Search", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void paginatedSearchTest03() throws Exception {
        processor = vendorHelper.paginatedSearch(VendorConstant.cityId,VendorConstant.random0,VendorConstant.random_10);
        testcaseHelper.paginatedSearchFailure(processor);
    }

    @Test(description = "Check whether the attribute is specified as daily and then validate with db - Paginated Search", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void paginatedSearchTest04() throws Exception {
        processor = vendorHelper.paginatedSearch(VendorConstant.cityId,VendorConstant.random_10,VendorConstant.random_10);
        testcaseHelper.paginatedSearchSuccess(processor);
        SoftAssert softAssert = new SoftAssert();
        String id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].id"));
        softAssert.assertNotNull(id);
        String cityCode = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.search_data[0].cityCode"));
        softAssert.assertNotNull(cityCode);
        softAssert.assertAll();

        vendorCommonHelper.selectDetailsFromCatalogDB(cityCode,id);
    }

}
