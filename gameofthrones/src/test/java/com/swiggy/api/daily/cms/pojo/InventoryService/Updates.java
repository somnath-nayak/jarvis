package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "$set"
})
public class Updates {

    @JsonProperty("$set")
    private com.swiggy.api.daily.cms.pojo.InventoryService.$set $set;

    @JsonProperty("$set")
    public com.swiggy.api.daily.cms.pojo.InventoryService.$set get$set() {
        return $set;
    }

    @JsonProperty("$set")
    public void set$set(com.swiggy.api.daily.cms.pojo.InventoryService.$set $set) {
        this.$set = $set;
    }

    public Updates with$set(com.swiggy.api.daily.cms.pojo.InventoryService.$set $set) {
        this.$set = $set;
        return this;
    }
    public Updates setData(Integer inventory_construct_inventories_max_cap,Integer inventory_construct_inventories_sold_count){
        $set sc=new $set().setData(inventory_construct_inventories_max_cap,inventory_construct_inventories_sold_count);
        this.with$set(sc);
        return this;
    }

}