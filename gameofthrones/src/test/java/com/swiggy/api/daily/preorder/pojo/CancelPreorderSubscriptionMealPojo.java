
package com.swiggy.api.daily.preorder.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "initiatedBy",
    "id",
    "orderType",
    "reason"
})
public class CancelPreorderSubscriptionMealPojo {

    @JsonProperty("initiatedBy")
    private String initiatedBy;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("orderType")
    private String orderType;
    @JsonProperty("reason")
    private String reason;

    @JsonProperty("initiatedBy")
    public String getInitiatedBy() {
        return initiatedBy;
    }

    @JsonProperty("initiatedBy")
    public void setInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
    }

    public CancelPreorderSubscriptionMealPojo withInitiatedBy(String initiatedBy) {
        this.initiatedBy = initiatedBy;
        return this;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public CancelPreorderSubscriptionMealPojo withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("orderType")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("orderType")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public CancelPreorderSubscriptionMealPojo withOrderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    public CancelPreorderSubscriptionMealPojo withReason(String reason) {
        this.reason = reason;
        return this;
    }


}
