
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "item",
    "quantity",
    "itemBill"
})
public class ItemDetail {

    @JsonProperty("item")
    private Item item;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("itemBill")
    private ItemBill itemBill;

    @JsonProperty("item")
    public Item getItem() {
        return item;
    }

    @JsonProperty("item")
    public void setItem(Item item) {
        this.item = item;
    }

    public ItemDetail withItem(Item item) {
        this.item = item;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public ItemDetail withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("itemBill")
    public ItemBill getItemBill() {
        return itemBill;
    }

    @JsonProperty("itemBill")
    public void setItemBill(ItemBill itemBill) {
        this.itemBill = itemBill;
    }

    public ItemDetail withItemBill(ItemBill itemBill) {
        this.itemBill = itemBill;
        return this;
    }

    public ItemDetail setDefaultData()  {
        return this.withItem(new Item().setDefaultData())
                .withQuantity(SubscriptionConstant.QUANTITY)
                .withItemBill(new ItemBill().setDefaultData());
    }

}
