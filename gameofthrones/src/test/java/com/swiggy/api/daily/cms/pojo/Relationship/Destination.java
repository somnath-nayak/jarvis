package com.swiggy.api.daily.cms.pojo.Relationship;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id",
        "spin"
})
public class Destination {

    @JsonProperty("store_id")
    private String store_id;
    @JsonProperty("spin")
    private String spin;

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    @JsonProperty("spin")
    public String getSpin() {
        return spin;
    }

    @JsonProperty("spin")
    public void setSpin(String spin) {
        this.spin = spin;
    }

}
