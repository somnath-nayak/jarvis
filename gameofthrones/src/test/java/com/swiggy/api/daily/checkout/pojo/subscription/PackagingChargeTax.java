package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class PackagingChargeTax {

    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private Long value;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public PackagingChargeTax withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("value")
    public Long getValue() {
        return value;
    }

    @JsonProperty("value")
    public void setValue(Long value) {
        this.value = value;
    }

    public PackagingChargeTax withValue(Long value) {
        this.value = value;
        return this;
    }

}
