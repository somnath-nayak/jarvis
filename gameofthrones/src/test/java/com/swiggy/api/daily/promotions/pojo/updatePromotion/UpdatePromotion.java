package com.swiggy.api.daily.promotions.pojo.updatePromotion;

import com.swiggy.api.daily.promotions.helper.PromotionsHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class UpdatePromotion {
    
    @JsonProperty("promotionType")
    private String promotionType;
    @JsonProperty("name")
    private String name;
    @JsonProperty("active")
    private int active;
    @JsonProperty("validFrom")
    private String validFrom;
    @JsonProperty("validTill")
    private String validTill;
    @JsonProperty("shareType")
    private String shareType;
    @JsonProperty("shareValue")
    private double shareValue;
    @JsonProperty("rules")
    private ArrayList<Rules> rules;
    @JsonProperty("actions")
    private ArrayList<Actions> actions;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("updatedAt")
    private String updatedAt;
    
 
    public UpdatePromotion(){
        //Default constructor
    }
    
 // Getter and Setter methods
    public String getPromotionType() {
        return promotionType;
    }
    
    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getActive() {
        return active;
    }
    
    public void setActive(int active) {
        this.active = active;
    }
    
    public String getValidFrom() {
        return validFrom;
    }
    
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }
    
    public String getValidTill() {
        return validTill;
    }
    
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }
    
    public String getShareType() {
        return shareType;
    }
    
    public void setShareType(String shareType) {
        this.shareType = shareType;
    }
    
    public double getShareValue() {
        return shareValue;
    }
    
    public void setShareValue(double shareValue) {
        this.shareValue = shareValue;
    }
    
    public ArrayList<Rules> getRules() {
        return rules;
    }
    
    public void setRules(ArrayList<Rules> rules) {
        this.rules = rules;
    }
    
    public ArrayList<Actions> getActions() {
        return actions;
    }
    
    public void setActions(ArrayList<Actions> actions) {
        this.actions = actions;
    }
    
    public String getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
    public String getCreatedAt() {
        return createdAt;
    }
    
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
    
    public String getUpdatedAt() {
        return updatedAt;
    }
    
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
    
   // builders
    
    public UpdatePromotion withPromotionType(String promotionType){
        setPromotionType(promotionType);
    return this;
    }
    
    public UpdatePromotion withName(String name){
        setName(name);
        return this;
    }
    
    public UpdatePromotion withActive(int active){
        setActive(active);
        return this;
    }
    
    public UpdatePromotion withValidFrom(String validFrom){
        setValidFrom(validFrom);
        return this;
    }
    
    public UpdatePromotion withValidTill(String validTill){
        setValidTill(validTill);
        return this;
    }
    
    public UpdatePromotion withShareType(String shareType){
        setShareType(shareType);
        return this;
    }
    public UpdatePromotion withShareValue(double shareValue){
        setShareValue(shareValue);
        return this;
    }
    public UpdatePromotion withRules(ArrayList<Rules> rules){
        setRules(rules);
        return this;
    }
    public UpdatePromotion withActions(ArrayList<Actions> actions){
        setActions(actions);
        return this;
    }
    public UpdatePromotion withCreatedBy(String createdBy){
        setCreatedBy(createdBy);
        return this;
    }
    public UpdatePromotion withCreatedAt(String createdAt){
        setCreatedAt(createdAt);
        return this;
    }
    public UpdatePromotion withUpdatedBy(String updatedBy){
        setUpdatedBy(updatedBy);
        return this;
    }
    public UpdatePromotion withUpdatedAt(String updatedAt){
        setUpdatedAt(updatedAt);
        return this;
    }
    
    // with out TIME_SLOT fact , only for one store & one SKU id and Flat type of discount value 10 at item level
    public UpdatePromotion withDefaultValues(){
        String[] fact = new String[]{"STORE_ID","SKU_ID"};
        String[] comparisonOperator = new String[]{"EQUAL","EQUAL"};
        List<String[]> values = new ArrayList<>();
        values.add(0,new String[]{Integer.toString(Utility.getRandom(100000,70000000))});
        values.add(1,new String[]{Integer.toString(Utility.getRandom(100000,100000000))});
    
        String[] actionType = new String[]{"FLAT_DISCOUNT"};
        String[] actionParam = new String[]{"ITEMS_PRICE"};
        String[]  discountValues = new String[]{"10"};
        
        withPromotionType("DISCOUNT").withName("Daily-Automatiom").withActive(0)
                .withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withShareType("PERCENTAGE")
                .withShareValue(50).withRules(populateUpdateListOfRules(fact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues))
                .withCreatedBy("Ankita-Automation").withCreatedAt(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).withUpdatedAt("Ankita-Automation").withUpdatedAt(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
        
       return this;
    }
    
    
    public ArrayList<Rules> populateUpdateListOfRules(String[] typeOfFact , String[] comparisonOperator, List<String[]> values) {
        ArrayList<Rules> rules = new ArrayList<Rules>();
        
        for (int i =0; i<typeOfFact.length ; i++){
            rules.add(i, new Rules().withValues(typeOfFact[i],comparisonOperator[i],values.get(i)));
        }
        return rules;
    }
    
    public ArrayList<Actions> populateListOfActions(String[] actionType, String[] actionParam, String[] values ){
        
        ArrayList<Actions> actions = new ArrayList<>();
        for (int i = 0 ; i <actionType.length; i++){
            actions.add(i,new Actions().withGivenValues(actionType[i],actionParam[i],values[i]));
        }
        return actions;
    }
}
