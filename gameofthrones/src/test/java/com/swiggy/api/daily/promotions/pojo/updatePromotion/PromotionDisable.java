package com.swiggy.api.daily.promotions.pojo.updatePromotion;

import org.apache.xpath.operations.Bool;
import org.codehaus.jackson.annotate.JsonProperty;

public class PromotionDisable {
    
    @JsonProperty("active")
    private Boolean active;
    @JsonProperty("updatedBy")
    private String updatedBy;
    
    public Boolean getActive() {
        return active;
    }
    
    public void setActive(Boolean active) {
        this.active = active;
    }
    
    public String getUpdatedBy() {
        return updatedBy;
    }
    
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
    
   

public PromotionDisable withDefaultValues(){
        setActive(false);
        setUpdatedBy("Automation-ankita");
        return this;
}
    
    public PromotionDisable withActive(Boolean active){
        setActive(active);
       
        return this;
    }
    
    public PromotionDisable withUpdatedBy(String updatedBy){
        setUpdatedBy(updatedBy);
        return this;
    }

}
