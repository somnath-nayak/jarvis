package com.swiggy.api.daily.cms.availabilityService.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "area_id",
        "close_time",
        "day",
        "open_time"
})
public class AreaSlotPOJO {

    @JsonProperty("area_id")
    private Integer areaId;
    @JsonProperty("close_time")
    private Integer closeTime;
    @JsonProperty("day")
    private String day;
    @JsonProperty("open_time")
    private Integer openTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public AreaSlotPOJO() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     * @param areaId
     */
    public AreaSlotPOJO(Integer areaId, Integer closeTime, String day, Integer openTime) {
        super();
        this.areaId = areaId;
        this.closeTime = closeTime;
        this.day = day;
        this.openTime = openTime;
    }

    @JsonProperty("area_id")
    public Integer getAreaId() {
        return areaId;
    }

    @JsonProperty("area_id")
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public AreaSlotPOJO withAreaId(Integer areaId) {
        this.areaId = areaId;
        return this;
    }

    @JsonProperty("close_time")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("close_time")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public AreaSlotPOJO withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public AreaSlotPOJO withDay(String day) {
        this.day = day;
        return this;
    }

    @JsonProperty("open_time")
    public Integer getOpenTime() {
        return openTime;
    }

    @JsonProperty("open_time")
    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public AreaSlotPOJO withOpenTime(Integer openTime) {
        this.openTime = openTime;
        return this;
    }

    public AreaSlotPOJO setDefault(Integer areaId, Integer open_time, Integer close_time, String day){
        return this.withAreaId(areaId).withOpenTime(open_time).withCloseTime(close_time).withDay(day);
    }
}
