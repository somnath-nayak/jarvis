
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "meta.spin",
    "meta.store_id",
        "meta.product_id"
})
public class Query {

    @JsonProperty("meta.spin")
    private String meta_spin;
    @JsonProperty("meta.store_id")
    private String meta_store_id;
    @JsonProperty("inventory_construct.attributes.slot_id")
    private Long inventory_construct_attributes_slot_id;
    @JsonProperty("meta.product_id")
    private String meta_product_id;

    @JsonProperty("meta.spin")
    public String getMeta_spin() {
        return meta_spin;
    }

    @JsonProperty("meta.spin")
    public void setMeta_spin(String meta_spin) {
        this.meta_spin = meta_spin;
    }

    public Query withMeta_spin(String meta_spin) {
        this.meta_spin = meta_spin;
        return this;
    }

    @JsonProperty("meta.store_id")
    public String getMeta_store_id() {
        return meta_store_id;
    }

    @JsonProperty("meta.store_id")
    public void setMeta_store_id(String meta_store_id) {
        this.meta_store_id = meta_store_id;
    }

    public Query withMeta_store_id(String meta_store_id) {
        this.meta_store_id = meta_store_id;
        return this;
    }


    @JsonProperty("inventory_construct.attributes.slot_id")
    public Long getInventory_construct_attributes_slot_id() {
        return inventory_construct_attributes_slot_id;
    }

    @JsonProperty("inventory_construct.attributes.slot_id")
    public void setInventory_construct_attributes_slot_id(Long inventory_construct_attributes_slot_id) {
        this.inventory_construct_attributes_slot_id = inventory_construct_attributes_slot_id;
    }

    public Query withInventory_construct_attributes_slot_id(Long inventory_construct_attributes_slot_id) {
        this.inventory_construct_attributes_slot_id = inventory_construct_attributes_slot_id;
        return this;
    }

    @JsonProperty("meta.product_id")
    public String getMeta_product_id() {
        return meta_product_id;
    }

    @JsonProperty("meta.product_id")
    public void setMeta_product_id(String meta_product_id) {
        this.meta_product_id = meta_product_id;
    }

    public Query withMeta_product_id(String meta_product_id) {
        this.meta_product_id = meta_product_id;
        return this;
    }

    public Query setData(String spinid,String storeid,Long slotid,String pid ){
        this.withMeta_spin(spinid).withMeta_store_id(storeid).withInventory_construct_attributes_slot_id(slotid).withMeta_product_id(pid);
        return this;
    }


}
