package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "close_time",
        "day",
        "open_time",
        "restaurant_id"
})
public class RestaurantSlotPOJO {

    @JsonProperty("close_time")
    private String closeTime;
    @JsonProperty("day")
    private String day;
    @JsonProperty("open_time")
    private String openTime;
    @JsonProperty("restaurant_id")
    private String restaurantId;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantSlotPOJO() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     * @param restaurantId
     */
    public RestaurantSlotPOJO(String closeTime, String day, String openTime, String restaurantId) {
        super();
        this.closeTime = closeTime;
        this.day = day;
        this.openTime = openTime;
        this.restaurantId = restaurantId;
    }

    @JsonProperty("close_time")
    public String getCloseTime() {
        return closeTime;
    }

    @JsonProperty("close_time")
    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public RestaurantSlotPOJO withCloseTime(String closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public RestaurantSlotPOJO withDay(String day) {
        this.day = day;
        return this;
    }

    @JsonProperty("open_time")
    public String getOpenTime() {
        return openTime;
    }

    @JsonProperty("open_time")
    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public RestaurantSlotPOJO withOpenTime(String openTime) {
        this.openTime = openTime;
        return this;
    }

    @JsonProperty("restaurant_id")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurant_id")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public RestaurantSlotPOJO withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    public RestaurantSlotPOJO setDefault(String close_time, String day, String open_time, String rest_id) {
        return this.withCloseTime(close_time).withDay(day).withOpenTime(open_time).withRestaurantId(rest_id);
    }

}
