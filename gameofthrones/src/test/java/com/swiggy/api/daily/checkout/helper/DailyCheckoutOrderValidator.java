package com.swiggy.api.daily.checkout.helper;

import com.swiggy.api.daily.checkout.pojo.OrderResponse.Orders;
import com.swiggy.api.daily.checkout.pojo.OrderResponse.Transaction_payment_details;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

import java.util.List;
import java.util.Map;

public class DailyCheckoutOrderValidator {

    public void validateOrderDataPart(Processor orderProcessor){
        String orderGroupId =orderProcessor.ResponseValidator.GetNodeValue("$.data.order_group_id");
        Assert.assertNotNull(orderGroupId,"orderGroupId is null");

        String customerId =orderProcessor.ResponseValidator.GetNodeValue("$.data.customer_id");
        //  Assert.assertNotNull(customerId,"customer_id is null");
    }


    public void validateOrderResponse(Processor orderProcessor,String orderStatus) {
        String orderData =orderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.orders")
                .toJSONString();
        Orders data=Utility.jsonDecode(orderData.substring(1,orderData.length() - 1), Orders.class);

        Assert.assertNotNull(data.getOrder_id(),"order_id is null");
        Assert.assertNotNull(data.getCustomer_id(),"Customer_id is null");

        Assert.assertEquals(data.getOrder_type(),DailyCheckoutConstants.ORDER_TYPE,"Order_type is invalid");

        Assert.assertNotNull(data.getOrder_jobs()[0].getOrder_job_id(),"Order_job_id is null");
        Assert.assertNotNull(data.getOrder_jobs()[0].getCustomer_id(),"Customer_id is null");

        Assert.assertEquals(data.getOrder_jobs()[0].getStatus(),orderStatus,"order status is invalid");

        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_id(),"Transaction_id is null");
        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_id(),"Transaction_id is null");
    }

    public void validateOrderPaymentInfo(Processor orderProcessor,String paymentStatus,String txnAmount) {
        String orderData =orderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.orders")
                .toJSONString();
        Orders data=Utility.jsonDecode(orderData.substring(1,orderData.length() - 1), Orders.class);

        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_id(),"Transaction_id is null");
        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[0].getPayment_status(),paymentStatus,"Payment_status is invalid");

        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[0].getCustomer_id(),"Customer_id is null");
        System.out.println("Expected:------------->"+txnAmount);
        System.out.println("actual:-------------->"+data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_amount());

        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[0].getTransaction_amount(),txnAmount,"Transaction_amount is invalid");

//        String actualPaymentMethod=orderProcessor.RequestValidator.GetNodeValue("$.payment_info.payment_method");
//        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[0].getPayment_method(),actualPaymentMethod,"PaymentMethod is invalid");

        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[0].getOrder_context(),DailyCheckoutConstants.ORDER_CONTEXT,"ORDER_CONTEXT is invalid");
    }

    public void validateOrderMetadata(Processor orderProcessor,String cartId,String orderSubType,String orderTotal){

        String OrderCartId=orderProcessor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.orders..order_jobs..metadata.cartId")
                                                                    .replaceAll("\\[|\\]|\"", "");
        String dailySubType=orderProcessor.ResponseValidator
                                           .GetNodeValue("$.data.orders[0].order_jobs[0].metadata.dailySubType");
        String totalPrice=orderProcessor.ResponseValidator
                                        .GetNodeValueAsStringFromJsonArray("$.data.orders..order_jobs..metadata.pricingDetail.total")
                                        .replaceAll("\\[|\\]|\"", "");
        String intTotalPriceAsString=Integer.toString((int) Math.round(Double.parseDouble(totalPrice)));
        Assert.assertEquals(OrderCartId,cartId,"cart id is invalid");
        Assert.assertEquals(dailySubType,orderSubType,"orderSubType is invalid");
        Assert.assertEquals(intTotalPriceAsString,orderTotal,"orderTotal is invalid");
    }
    public void validateTransactionPaymentDetails(Processor orderProcessor,String txnStatus,String isRefundInitiated){
        String transactionPaymentDetails =orderProcessor.ResponseValidator
                .GetNodeValueAsJsonArray("$.data.transaction_payment_details")
                .toJSONString();
        Transaction_payment_details transactionPaymentData= Utility.jsonDecode(transactionPaymentDetails
                        .substring(1,transactionPaymentDetails.length() - 1),
                Transaction_payment_details.class);
        String reqPaymentMethod=orderProcessor.RequestValidator.GetNodeValue("$.payment_info.payment_method");

        Assert.assertEquals(transactionPaymentData.getPaymentMethod(),reqPaymentMethod,"Mismatch in payment method");
        Assert.assertEquals(transactionPaymentData.getPaymentTxnStatus(),txnStatus,"Mismatch in payment transaction status");
        Assert.assertEquals(transactionPaymentData.getRefundInitiated(),isRefundInitiated,"Mismatch in Refund Initiated status");
    }

    public void validateDbTransactionDetails(List<Map<String, Object>> dbData,String paymentMethod,String userId,
                                             String txnStatus,String amount){
        Assert.assertEquals(dbData.get(0).get("tenant"),DailyCheckoutConstants.TENANT,"Tenant is invalid in DB");
        Assert.assertEquals(dbData.get(0).get("payment_method"),paymentMethod,"paymentMethod is invalid in DB");
        Assert.assertEquals(dbData.get(0).get("payment_txn_status"),txnStatus,"payment_txn_status is invalid in DB");
        Assert.assertEquals(Float.toString((float) dbData.get(0).get("amount")),amount,"amount is invalid in DB");
        Assert.assertEquals(Long.toString((long) dbData.get(0).get("user_id")),userId,"amount is invalid in DB");
        System.out.println(dbData.get(0).get("payment_details"));

    }

    public void validateDbRefundTransactionDetails(List<Map<String, Object>> dbData,String paymentMethod,
                                             String refundStatus,String amount){
        Assert.assertEquals(dbData.get(0).get("payment_method"),paymentMethod,"paymentMethod is invalid in DB");
        Assert.assertEquals(dbData.get(0).get("refund_status"),refundStatus,"payment_txn_status is invalid in DB");
        Assert.assertEquals(Float.toString((float) dbData.get(0).get("amount")),amount,"amount is invalid in DB");
        System.out.println(dbData.get(0).get("refund_details"));

    }


    public void validateRefundOrderPaymentInfo(int index,Processor orderProcessor,String paymentStatus,String txnAmount) {
        String orderData =orderProcessor.ResponseValidator.GetNodeValueAsJsonArray("$.data.orders")
                .toJSONString();
        Orders data=Utility.jsonDecode(orderData.substring(1,orderData.length() - 1), Orders.class);

        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[index].getTransaction_id(),"Transaction_id is null");
        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[index].getPayment_status(),paymentStatus,"Payment_status is invalid");

        Assert.assertNotNull(data.getOrder_jobs()[0].getPayment_info()[index].getCustomer_id(),"Customer_id is null");
        System.out.println("Expected:------------->"+txnAmount);
        System.out.println("actual:-------------->"+data.getOrder_jobs()[0].getPayment_info()[index].getTransaction_amount());
        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[index].getTransaction_amount(),txnAmount,"Transaction_amount is invalid");
//        String actualPaymentMethod=orderProcessor.RequestValidator.GetNodeValue("$.payment_info.payment_method");
//        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[0].getPayment_method(),actualPaymentMethod,"PaymentMethod is invalid");
        Assert.assertEquals(data.getOrder_jobs()[0].getPayment_info()[index].getOrder_context(),DailyCheckoutConstants.ORDER_CONTEXT,"ORDER_CONTEXT is invalid");
    }
}
