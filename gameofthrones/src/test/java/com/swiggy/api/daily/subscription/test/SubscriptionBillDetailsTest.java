package com.swiggy.api.daily.subscription.test;

import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.GenerateOrderIdPojo;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class SubscriptionBillDetailsTest extends SubscriptionDp {
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    GenerateOrderIdPojo generateOrderIdPojo=new GenerateOrderIdPojo();
    JsonHelper jsonHelper=new JsonHelper();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }

    //------------------------------------------------------------------Subscription Bill Details-----------------------------------------------------

    @Test(dataProvider="subscriptionBillDetailsDP",description="Get Subscription bill details",groups={"Sanity_Test","Regression"})
    public void subscriptionBillDetailsTest(String orderId) {
        responseSubscriptionId=orderId;
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        Processor subscriptionBillProcessor=subscriptionHelper.getSubscriptionBillDetails(orderId);
        subscriptionTestHelper.validateSubscriptionBillDetailsResponse(orderDailyOmsProcessor,subscriptionBillProcessor);
    }

    //----------------------------------------------------------Subscription Meal Bill Details---------------------------------------------------------

    @Test(dataProvider="preorderBillDetailsDP",description="Get Preorder meal bill details of upcoming orders",groups={"Sanity_Test","Regression"})
    public void preorderBillDetailsForUpcomingTest(String orderId) {
        responseSubscriptionId=orderId;
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        Processor mealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(orderId, SubscriptionConstant.PREORDER_TYPE);
        subscriptionTestHelper.validatePreorderBillDetailsResponse(orderDailyOmsProcessor,mealBillProcessor,"UPCOMING");
    }

    @Test(dataProvider="preorderBillDetailsDP",description="Get Preorder meal bill details of Delivered orders",groups={"Regression"})
    public void preorderBillDetailsForDeliveredTest(String orderId) {
        responseSubscriptionId=orderId;
        subscriptionHelper.updatePreorderStatus(orderId,"DELIVERED");
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        Processor mealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(orderId, SubscriptionConstant.PREORDER_TYPE);
        subscriptionTestHelper.validatePreorderBillDetailsResponse(orderDailyOmsProcessor,mealBillProcessor,"DELIVERED");
    }


    @Test(dataProvider="preorderBillDetailsDP",description="Get Preorder meal bill details of Cancelled orders",groups={"Regression"})
    public void preorderBillDetailsForCancelledTest(String orderId) {
        responseSubscriptionId=orderId;
        subscriptionHelper.updatePreorderStatus(orderId,"CANCELLED");
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        Processor mealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(orderId, SubscriptionConstant.PREORDER_TYPE);
        subscriptionTestHelper.validatePreorderBillDetailsResponse(orderDailyOmsProcessor,mealBillProcessor,"CANCELLED");
    }

    @Test(dataProvider="subscriptionBillDetailsDP",description="Get subscription meal bill details of upcoming orders",groups={"Sanity_Test","Regression"})
    public void subscriptionUpcomingMealBillDetailsTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        Processor orderOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        int deliveryStartTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.start_time");
        int deliveryEndTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.end_time");
        String subscriptionMealOrderId=jsonHelper.getObjectToJSON(generateOrderIdPojo.setDefaultData().withStartTime(deliveryStartTime).withEndTime(deliveryEndTime));
        Processor orderIdProcessor=subscriptionHelper.createOrderId(subscriptionMealOrderId);
        subscriptionTestHelper.validateGenerateOrderIdResponse(orderIdProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(orderId);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");
        String mealOrderId=subscriptionHelper.getSubscriptionMealOrderId(subscriptionMealId);

        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(mealOrderId);
        Processor subscriptionMealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(subscriptionMealId, SubscriptionConstant.DAILY_SUBSCRIPTION_MEAL_TYPE);
        subscriptionTestHelper.validateSubscriptionMealBillDetailsResponse(orderDailyOmsProcessor,subscriptionMealBillProcessor,"UPCOMING");
    }

    @Test(dataProvider="subscriptionBillDetailsDP",description="Get subscription meal bill details of Delivered orders",groups={"Regression"})
    public void subscriptionDeliveredMealBillDetailsTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        Processor orderOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        int deliveryStartTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.start_time");
        int deliveryEndTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.end_time");
        String subscriptionMealOrderId=jsonHelper.getObjectToJSON(generateOrderIdPojo.setDefaultData().withStartTime(deliveryStartTime).withEndTime(deliveryEndTime));
        Processor orderIdProcessor=subscriptionHelper.createOrderId(subscriptionMealOrderId);
        subscriptionTestHelper.validateGenerateOrderIdResponse(orderIdProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(orderId);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");
        String mealOrderId=subscriptionHelper.getSubscriptionMealOrderId(subscriptionMealId);

        subscriptionHelper.updatePreorderStatus(orderId,"DELIVERED");

        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(mealOrderId);
        Processor subscriptionMealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(subscriptionMealId, SubscriptionConstant.DAILY_SUBSCRIPTION_MEAL_TYPE);
        subscriptionTestHelper.validateSubscriptionMealBillDetailsResponse(orderDailyOmsProcessor,subscriptionMealBillProcessor,"DELIVERED");
    }

    @Test(dataProvider="subscriptionBillDetailsDP",description="Get subscription meal bill details of Cancelled orders",groups={"Regression"})
    public void subscriptionCancelledMealBillDetailsTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        Processor orderOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        int deliveryStartTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.start_time");
        int deliveryEndTime=orderOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.end_time");
        String subscriptionMealOrderId=jsonHelper.getObjectToJSON(generateOrderIdPojo.setDefaultData().withStartTime(deliveryStartTime).withEndTime(deliveryEndTime));
        Processor orderIdProcessor=subscriptionHelper.createOrderId(subscriptionMealOrderId);
        subscriptionTestHelper.validateGenerateOrderIdResponse(orderIdProcessor);

        Processor subscriptionDetailsMeal = subscriptionHelper.getSubscriptionDetails(orderId);
        String subscriptionMealId = subscriptionDetailsMeal.ResponseValidator.GetNodeValue("$.data.nextMeal.mealDetails.subscriptionMealId");
        String mealOrderId=subscriptionHelper.getSubscriptionMealOrderId(subscriptionMealId);

        subscriptionHelper.updatePreorderStatus(orderId,"CANCELLED");

        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(mealOrderId);
        Processor subscriptionMealBillProcessor=subscriptionHelper.getSubscriptionMealBillDetails(subscriptionMealId, SubscriptionConstant.DAILY_SUBSCRIPTION_MEAL_TYPE);
        subscriptionTestHelper.validateSubscriptionMealBillDetailsResponse(orderDailyOmsProcessor,subscriptionMealBillProcessor,"CANCELLED");
    }
    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }
}
