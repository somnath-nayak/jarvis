package com.swiggy.api.daily.delivery.dp;

import org.testng.annotations.DataProvider;

public class listingDP {

    @DataProvider(name="dailyListingTest")
    public Object[][] dailyListingTest()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543472076","1543492076",false},
                {"17.965557","79.593702",75L,1,"99","1893441630","1893502830",false}
        };
    }

    @DataProvider(name="dailyListingTestPDP")
    public Object[][] dailyListingTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543472076","1543492076",false,"99"}};
    }

    @DataProvider(name="dailyListingNonDailyTestPDP")
    public Object[][] dailyListingNonDailyTestPDP()
    {
        return new Object[][]{
                {"12.325123","103.924639",10L,1,"99","1543472076","1543492076",false,"99"}};
    }

    @DataProvider(name="dailyListingOneCompleteNonBZAndOnePartialBZTestPDP")
    public Object[][] dailyListingOneCompleteNonBZAndOnePartialBZTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543386489","1543404609",false,"99"}};
    }

    @DataProvider(name="dailyListingCompleteBZTestPDP")
    public Object[][] dailyListingCompleteBZTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543386489","1543397409",false,"99"}};
    }

    @DataProvider(name="dailyListingCompleteBZAndExceptOnePartialBZTestPDP")
    public Object[][] dailyListingCompleteBZAndExceptOnePartialBZTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543386489","1543400889",false,"99"},
                {"1.325123","103.924639",10L,1,"99","1543559289","1543573689",false,"99"}
                };
    }

    @DataProvider(name="dailyListingOneCompleteNonBZTestPDP")
    public Object[][] dailyListingOneCompleteNonBZTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,1,"99","1543400889","1543408209",false,"99"}};
    }

    @DataProvider(name="dailyListingOneCompleteNonBZAndStraigtLineFailedByPackSizeTestPDP")
    public Object[][] dailyListingOneCompleteNonBZAndStraigtLineFailedByPackSizeTestPDP()
    {
        return new Object[][]{
                {"1.325123","103.924639",10L,3,"99","1543400889","1543354099",false,"99"}};
    }
    /*
    * 11am-5pm - all are blackzoned except one partial and one full
#########

"start_time": 1543386489,
"end_time": 1543404609

11am-3pm - all blackzoned
########

"start_time": 1543386489,
"end_time": 1543397409

11am-3.58pm - all blackzoned except one partial
########

"start_time": 1543386489,
"end_time": 1543400889

4pm-5pm all blackzoned except one full
#########

"start_time": 1543400889,
"end_time": 1543408209


11am-3.58pm - all blackzoned except one partial (packsize 2)
########

"start_time": 1543386489,
"end_time": 1543400889*/

    @DataProvider(name="capacityCheck")
    public Object[][] capacityCheck()
    {
        return new Object[][]{
                {"1.325123","103.924639","12345","1893441630","1893510030","Success"}};
    }

    @DataProvider(name="capacityIncrementCheck")
    public Object[][] capacityIncrementCheck()
    {
        return new Object[][]{
                {"1.325123","103.924639","12345","1893441630","1893510030","Success"}};
    }
    @DataProvider(name="capacityDecrementCheck")
    public Object[][] capacityDecrementCheck()
    {
        return new Object[][]{
                {"1.325123","103.924639","12345","1893441630","1893510030","Success"}};
    }
}
