package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartDeliveryDetails {


    @JsonProperty("deliverySchedule")
    private DeliverySchedule deliverySchedule;
    @JsonProperty("slotDetails")
    private SlotDetails slotDetails;
    @JsonProperty("addressDetail")
    private AddressDetail addressDetail;

    @JsonProperty("deliverySchedule")
    public DeliverySchedule getDeliverySchedule() {
        return deliverySchedule;
    }

    @JsonProperty("deliverySchedule")
    public void setDeliverySchedule(DeliverySchedule deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
    }

    public CartDeliveryDetails withDeliverySchedule(DeliverySchedule deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
        return this;
    }

    @JsonProperty("slotDetails")
    public SlotDetails getSlotDetails() {
        return slotDetails;
    }

    @JsonProperty("slotDetails")
    public void setSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
    }

    public CartDeliveryDetails withSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
        return this;
    }

    @JsonProperty("addressDetail")
    public AddressDetail getAddressDetail() {
        return addressDetail;
    }

    @JsonProperty("addressDetail")
    public void setAddressDetail(AddressDetail addressDetail) {
        this.addressDetail = addressDetail;
    }

    public CartDeliveryDetails withAddressDetail(AddressDetail addressDetail) {
        this.addressDetail = addressDetail;
        return this;
    }

}
