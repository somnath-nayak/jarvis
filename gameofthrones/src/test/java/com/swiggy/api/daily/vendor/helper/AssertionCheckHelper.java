package com.swiggy.api.daily.vendor.helper;

import com.swiggy.api.daily.mealSlot.helper.MealSlotConstant;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

public class AssertionCheckHelper {
    
    
    public String assertCheckForCreateSlot(Processor response) {
        String id = String.valueOf(response.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        String statusMessage =response.ResponseValidator.GetNodeValue("$.statusMessage");
        if (!(id.equalsIgnoreCase(MealSlotConstant._403)) ||statusMessage.equalsIgnoreCase("Ops something went wrong") ) {
            return VendorConstants.pass;
        } else {
            return VendorConstants.failure;
        }
    }
    
    
    public String assertCheckFroCreateSubscription(Processor response) {
        int StatusCode = response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String data = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String statusMessage = response.ResponseValidator.GetNodeValue("$.statusMessage");
        if (StatusCode != 0 && data != null) {
            return VendorConstants.pass;
        } else {
            if (statusMessage.contains(VendorConstants.subscriptionExist)) {
                return VendorConstants.subscriptionExist;
            }
            return VendorConstants.failure;
        }
    }
    
    
    public void assertCheckForCreateDeliverySlot(Processor response) {
        String statusMessage = response.ResponseValidator.GetNodeValue("$.statusMessage");
        int statusCode = response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, VendorConstants.pass);
        
    }
    
    public Integer assertCheckForGetIdOfDeliveryZone(Processor processor, Integer startTime, Integer endTime) {
        int statusCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = processor.ResponseValidator.GetNodeValue("$.statusMessage");
        String id = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[?(@.startTime==" + Integer.toString(startTime) + "&& @.endTime==" + Integer.toString(endTime) + ")].id");
        if (id.equalsIgnoreCase("[]")) {
            Assert.assertNotEquals(id, "[]");
        }
        id = "" + id.replace("[", "").replace("]", "");
        System.out.println("id-------->>>> " + id);
        return Integer.parseInt(id);
    }
    
    public void assertCheckForUpdateDeliverySlot(Processor response) {
        
        String statusMessage = response.ResponseValidator.GetNodeValue("$.statusMessage");
        if (!(statusMessage.equalsIgnoreCase(VendorConstants.pass))) {
            Assert.assertEquals(statusMessage, VendorConstants.pass);
        }
    }
    
    
    public void assertCheckForRestaurantSearch(Processor response) {
        String code = response.ResponseValidator.GetNodeValue("$.code");
        int status = response.ResponseValidator.GetNodeValueAsInt("$.status");
        Assert.assertEquals(code, "SUCCESS");
        Assert.assertEquals(status, 1);
    }
    
    public void assertionCheckForVendorFetchOrder(Processor response){
        
   //     int statusCode =response.ResponseValidator.GetNodeValueAsInt("");
       // Assert.assertEquals(statusCode,1);
    }
}
