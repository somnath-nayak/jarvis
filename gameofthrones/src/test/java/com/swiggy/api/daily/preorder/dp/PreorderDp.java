package com.swiggy.api.daily.preorder.dp;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.helper.DailyPreorderHelper;
import com.swiggy.api.daily.preorder.pojo.*;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PreorderDp {
    /*
     Yet to implement ...
     */
    DeliveryDetails deliveryDetails;
    ItemDetail itemDetail;
    AddressDetails addressDetails;
    CreatePreorederPojo preorederPojo;
    List<ItemDetail> itemDetails;
    JsonHelper jsonHelper=new JsonHelper();

    public PreorderDp() {
        deliveryDetails = new DeliveryDetails();
        itemDetail = new ItemDetail();
        addressDetails=new AddressDetails();
        itemDetails = new ArrayList<>();
        deliveryDetails.setDefaultData();
        itemDetail.setDefaultData();
        itemDetails.add(itemDetail);
    }

    @DataProvider(name = "createPreorderPositive")
    public Object[][] createPreorderPositiveDp() throws IOException {
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData();
        String payload1 = jsonHelper.getObjectToJSON(preorederPojo);

        return new Object[][]{{payload1}};
    }

    @DataProvider(name = "createPreorderNegativeTimeSlot")
    public Object[][] createPreorderNegativeTimeSlot() throws IOException {
        preorederPojo = new CreatePreorederPojo();
        List<ItemDetail> itemDetails;
        ItemDetail itemDetail = new ItemDetail();
        addressDetails=new AddressDetails();
        addressDetails.setDefaultData();
        itemDetails = new ArrayList<>();
        itemDetail.setDefaultData();
        itemDetails.add(itemDetail);
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").
                withOrderId("12345").
                withStoreId("1223").
                setDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond());

        String payload1 = jsonHelper.getObjectToJSON(preorederPojo);


        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").
                withOrderId("12345").withStoreId("1223").setDeliveryEndTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond());


        String payload2 = jsonHelper.getObjectToJSON(preorederPojo);

        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").
                withOrderId("12345").withStoreId("1223").withDeliveryEndTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond()).withDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond());


        String payload3 = jsonHelper.getObjectToJSON(preorederPojo);


        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").
                withOrderId("12345").withStoreId("1223").withDeliveryEndTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond()).withDeliveryStartTime(DateUtility.getFutureDateInMilisecond());


        String payload4 = jsonHelper.getObjectToJSON(preorederPojo);


        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).
                withOrderId("12345").withStoreId("1223").withDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond()).withDeliveryEndTime(DateUtility.getFutureDateInMilisecond());


        String payload5 = jsonHelper.getObjectToJSON(preorederPojo);

        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").
               withStoreId("1223").withDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond()).withDeliveryEndTime(DateUtility.getFutureDateInMilisecond());


        String payload6 = jsonHelper.getObjectToJSON(preorederPojo);

        preorederPojo = new CreatePreorederPojo();
        preorederPojo.withItemDetails(itemDetails).withAddressDetails(addressDetails).withUserId("1234").withOrderId("2323")
                .withDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond()).withDeliveryEndTime(DateUtility.getFutureDateInMilisecond());


        String payload7 = jsonHelper.getObjectToJSON(preorederPojo);

        return new Object[][]{{payload1,"Invalid create preOrder request, start time is greater than or equal to end time"},
                {payload2,"Invalid create preOrder request, start time is greater than or equal to end time"},
                {payload3,"Invalid create preOrder request, start time is greater than or equal to end time"},
                {payload4,"Invalid create preOrder request, start time is greater than or equal to end time"},
                {payload5,"userId is Invalid"},
                {payload6,"orderId is Invalid"},
                {payload6,"storeId is Invalid"}};
    }


    @DataProvider(name = "cancelPreorderPositive")
    public Object[][] cancelPreorderPositive() throws IOException {

        DailyPreorderHelper preorderHelper = new DailyPreorderHelper();

        CreatePreorederPojo preorederPojo = new CreatePreorederPojo();

        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(2,1));
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(2,45));
        String preorderData1 = jsonHelper.getObjectToJSON(preorederPojo);


        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(3,1));
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(3,45));
        String preorderData2 = jsonHelper.getObjectToJSON(preorederPojo);


        return new Object[][]{{preorderData1,},{preorderData2}};
    }

    @DataProvider(name = "cancelPreorderNegative")
    public Object[][] cancelPreorderNegative() throws IOException {

        DailyPreorderHelper preorderHelper = new DailyPreorderHelper();

        CreatePreorederPojo preorederPojo = new CreatePreorederPojo();

        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(1,1));
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(1,45));
        String preorderData1 = jsonHelper.getObjectToJSON(preorederPojo);


        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(2,0));
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(2,45));
        String preorderData2 = jsonHelper.getObjectToJSON(preorederPojo);

        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(0,0));
        preorederPojo.setDefaultData().setDeliveryStartTime(preorderHelper.getDeliveryTime(1,0));
        String preorderData3 = jsonHelper.getObjectToJSON(preorederPojo);


        return new Object[][]{{preorderData1,},{preorderData2},{preorderData3}};
    }

    @DataProvider(name = "createPreorderNegativeItemDetails")
    public Object[][] createPreorderNegetiveDpItemDetails() throws IOException {
        preorederPojo=new CreatePreorederPojo();
        itemDetails.add(itemDetail);
        itemDetails.add(itemDetail);
        preorederPojo.setDefaultData().setItemDetails(itemDetails);
        String payload1 = jsonHelper.getObjectToJSON(preorederPojo);


        itemDetails.clear();
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setItemDetails(itemDetails);
        String payload2 = jsonHelper.getObjectToJSON(preorederPojo);


        itemDetails.clear();
        itemDetails.add(new ItemDetail().withItemId("11"));
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setItemDetails(itemDetails);
        String payload3 = jsonHelper.getObjectToJSON(preorederPojo);


        itemDetails.clear();
        itemDetails.add(new ItemDetail().withQuantity(3));
        preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData().setItemDetails(itemDetails);
        String payload4 = jsonHelper.getObjectToJSON(preorederPojo);


        return new Object[][]{{payload1,"Only 1 item at a time"},
                              {payload2,"entityDetails is Invalid"},
                {payload3, "entityDetails[0].quantity is Invalid"},
                {payload4, "entityDetails[0].itemId is Invalid"}
                              };
    }
}
