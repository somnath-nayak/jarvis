
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "deliverySchedule",
    "slotDetails",
    "addressDetail"
})
public class DeliveryDetail {

    @JsonProperty("deliverySchedule")
    private DeliverySchedule deliverySchedule;
    @JsonProperty("slotDetails")
    private SlotDetails slotDetails;
    @JsonProperty("addressDetail")
    private AddressDetail addressDetail;

    @JsonProperty("deliverySchedule")
    public DeliverySchedule getDeliverySchedule() {
        return deliverySchedule;
    }

    @JsonProperty("deliverySchedule")
    public void setDeliverySchedule(DeliverySchedule deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
    }

    public DeliveryDetail withDeliverySchedule(DeliverySchedule deliverySchedule) {
        this.deliverySchedule = deliverySchedule;
        return this;
    }

    @JsonProperty("slotDetails")
    public SlotDetails getSlotDetails() {
        return slotDetails;
    }

    @JsonProperty("slotDetails")
    public void setSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
    }

    public DeliveryDetail withSlotDetails(SlotDetails slotDetails) {
        this.slotDetails = slotDetails;
        return this;
    }

    @JsonProperty("addressDetail")
    public AddressDetail getAddressDetail() {
        return addressDetail;
    }

    @JsonProperty("addressDetail")
    public void setAddressDetail(AddressDetail addressDetail) {
        this.addressDetail = addressDetail;
    }

    public DeliveryDetail withAddressDetail(AddressDetail addressDetail) {
        this.addressDetail = addressDetail;
        return this;
    }

    public DeliveryDetail setDefaultData()  {
        return this.withDeliverySchedule(new DeliverySchedule().setDefaultData())
                .withSlotDetails(new SlotDetails().setDefaultData())
                .withAddressDetail(new AddressDetail().setDefaultData());
    }

}
