
package com.swiggy.api.daily.cms.pojo.PricingService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import io.gatling.commons.stats.assertion.In;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cgst",
    "cgst_percentage",
    "currency",
    "igst",
    "igst_percentage",
    "inclusive_of_taxes",
    "packaging_charges",
    "sgst_utgst",
    "sgst_utgst_percentage"
})
public class Attributes {

    @JsonProperty("cgst")
    private Integer cgst;
    @JsonProperty("cgst_percentage")
    private Integer cgst_percentage;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("igst")
    private Integer igst;
    @JsonProperty("igst_percentage")
    private Integer igst_percentage;
    @JsonProperty("inclusive_of_taxes")
    private Boolean inclusive_of_taxes;
    @JsonProperty("packaging_charges")
    private Integer packaging_charges;
    @JsonProperty("sgst_utgst")
    private Integer sgst_utgst;
    @JsonProperty("sgst_utgst_percentage")
    private Integer sgst_utgst_percentage;

    @JsonProperty("cgst")
    public Integer getCgst() {
        return cgst;
    }

    @JsonProperty("cgst")
    public void setCgst(Integer cgst) {
        this.cgst = cgst;
    }

    public Attributes withCgst(Integer cgst) {
        this.cgst = cgst;
        return this;
    }

    @JsonProperty("cgst_percentage")
    public Integer getCgst_percentage() {
        return cgst_percentage;
    }

    @JsonProperty("cgst_percentage")
    public void setCgst_percentage(Integer cgst_percentage) {
        this.cgst_percentage = cgst_percentage;
    }

    public Attributes withCgst_percentage(Integer cgst_percentage) {
        this.cgst_percentage = cgst_percentage;
        return this;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Attributes withCurrency(String currency) {
        this.currency = currency;
        return this;
    }

    @JsonProperty("igst")
    public Integer getIgst() {
        return igst;
    }

    @JsonProperty("igst")
    public void setIgst(Integer igst) {
        this.igst = igst;
    }

    public Attributes withIgst(Integer igst) {
        this.igst = igst;
        return this;
    }

    @JsonProperty("igst_percentage")
    public Integer getIgst_percentage() {
        return igst_percentage;
    }

    @JsonProperty("igst_percentage")
    public void setIgst_percentage(Integer igst_percentage) {
        this.igst_percentage = igst_percentage;
    }

    public Attributes withIgst_percentage(Integer igst_percentage) {
        this.igst_percentage = igst_percentage;
        return this;
    }

    @JsonProperty("inclusive_of_taxes")
    public Boolean getInclusive_of_taxes() {
        return inclusive_of_taxes;
    }

    @JsonProperty("inclusive_of_taxes")
    public void setInclusive_of_taxes(Boolean inclusive_of_taxes) {
        this.inclusive_of_taxes = inclusive_of_taxes;
    }

    public Attributes withInclusive_of_taxes(Boolean inclusive_of_taxes) {
        this.inclusive_of_taxes = inclusive_of_taxes;
        return this;
    }

    @JsonProperty("packaging_charges")
    public Integer getPackaging_charges() {
        return packaging_charges;
    }

    @JsonProperty("packaging_charges")
    public void setPackaging_charges(Integer packaging_charges) {
        this.packaging_charges = packaging_charges;
    }

    public Attributes withPackaging_charges(Integer packaging_charges) {
        this.packaging_charges = packaging_charges;
        return this;
    }

    @JsonProperty("sgst_utgst")
    public Integer getSgst_utgst() {
        return sgst_utgst;
    }

    @JsonProperty("sgst_utgst")
    public void setSgst_utgst(Integer sgst_utgst) {
        this.sgst_utgst = sgst_utgst;
    }

    public Attributes withSgst_utgst(Integer sgst_utgst) {
        this.sgst_utgst = sgst_utgst;
        return this;
    }

    @JsonProperty("sgst_utgst_percentage")
    public Integer getSgst_utgst_percentage() {
        return sgst_utgst_percentage;
    }

    @JsonProperty("sgst_utgst_percentage")
    public void setSgst_utgst_percentage(Integer sgst_utgst_percentage) {
        this.sgst_utgst_percentage = sgst_utgst_percentage;
    }

    public Attributes withSgst_utgst_percentage(Integer sgst_utgst_percentage) {
        this.sgst_utgst_percentage = sgst_utgst_percentage;
        return this;
    }
    public Attributes setData(Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes,Integer sgst_utgst,Integer sgst_utgst_percentage) {
        this.withCgst(cgst).withCgst_percentage(cgst_percentage).withCurrency(currency).withIgst(igst).withIgst_percentage(igst_percentage).withInclusive_of_taxes(inclusive_of_taxes).withSgst_utgst(sgst_utgst).withSgst_utgst_percentage(sgst_utgst_percentage);
        return this;
    }
}
