package com.swiggy.api.daily.checkout.pojo.tdService;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "fact",
        "comparisonOperator",
        "values"
})
public class Rule {

    @JsonProperty("fact")
    private String fact;
    @JsonProperty("comparisonOperator")
    private String comparisonOperator;
    @JsonProperty("values")
    private List<String> values = null;

    @JsonProperty("fact")
    public String getFact() {
        return fact;
    }

    @JsonProperty("fact")
    public void setFact(String fact) {
        this.fact = fact;
    }

    @JsonProperty("comparisonOperator")
    public String getComparisonOperator() {
        return comparisonOperator;
    }

    @JsonProperty("comparisonOperator")
    public void setComparisonOperator(String comparisonOperator) {
        this.comparisonOperator = comparisonOperator;
    }

    @JsonProperty("values")
    public List<String> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("fact", fact).append("comparisonOperator", comparisonOperator).append("values", values).toString();
    }

}