package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class EpochSlot {
    @JsonProperty("start_time")
    Long startTime;
    @JsonProperty("end_time")
    Long endTime;

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }


}