package com.swiggy.api.daily.promotions.pojo.mealListing;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class MealListing {
    @JsonProperty("stores")
    private ArrayList<StoreId> stores;
    
    public ArrayList<StoreId> getStores() {
        return stores;
    }
    
    public void setStores(ArrayList<StoreId> stores) {
        this.stores = stores;
    }
    
   
    public MealListing withGivenValues(String[] storeId, List<String[]> allSkuId , List<List<Integer>> price  ){
        ArrayList<StoreId> storeIds = new ArrayList<>();
        for (int i =0 ; i<storeId.length ; i++) {
            storeIds.add(i, new StoreId().withGivenValue(storeId[i], allSkuId.get(i), price.get(i)));
    
        }
        
        setStores(storeIds);
        return this;
    }
    
    
    
}
