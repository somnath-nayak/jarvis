package com.swiggy.api.daily.cms.pojo.Relationship;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "schedule"
})
public class Relationship_data {

    @JsonProperty("schedule")
    private List<Schedule> schedule = null;

    @JsonProperty("schedule")
    public List<Schedule> getSchedule() {
        return schedule;
    }
    @JsonProperty("schedule")
    public void setSchedule(List<Schedule> schedule) {
        this.schedule = schedule;
    }

}
