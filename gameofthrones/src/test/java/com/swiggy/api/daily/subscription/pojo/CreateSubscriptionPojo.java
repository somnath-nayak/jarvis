
package com.swiggy.api.daily.subscription.pojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.sf.rng.tests.RandomNumber;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "customer_id",
    "order_id",
    "item_details",
    "schedule_details",
    "delivery_slot",
    "address_details"
})
public class CreateSubscriptionPojo {
    RandomNumber rm = new RandomNumber(100001, 1000000);

    @JsonProperty("customer_id")
    private String customerId;
    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("item_details")
    private List<ItemDetail> itemDetails = null;
    @JsonProperty("schedule_details")
    private ScheduleDetails scheduleDetails;
    @JsonProperty("delivery_slot")
    private DeliverySlot deliverySlot;
    @JsonProperty("address_details")
    private AddressDetails addressDetails;

    @JsonProperty("customer_id")
    public String getCustomerId() {
        return customerId;
    }

    @JsonProperty("customer_id")
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public CreateSubscriptionPojo withCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CreateSubscriptionPojo withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("item_details")
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    @JsonProperty("item_details")
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public CreateSubscriptionPojo withItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
        return this;
    }

    @JsonProperty("schedule_details")
    public ScheduleDetails getScheduleDetails() {
        return scheduleDetails;
    }

    @JsonProperty("schedule_details")
    public void setScheduleDetails(ScheduleDetails scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
    }

    public CreateSubscriptionPojo withScheduleDetails(ScheduleDetails scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
        return this;
    }

    @JsonProperty("delivery_slot")
    public DeliverySlot getDeliverySlot() {
        return deliverySlot;
    }

    @JsonProperty("delivery_slot")
    public void setDeliverySlot(DeliverySlot deliverySlot) {
        this.deliverySlot = deliverySlot;
    }

    public CreateSubscriptionPojo withDeliverySlot(DeliverySlot deliverySlot) {
        this.deliverySlot = deliverySlot;
        return this;
    }

    @JsonProperty("address_details")
    public AddressDetails getAddressDetails() {
        return addressDetails;
    }

    @JsonProperty("address_details")
    public void setAddressDetails(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
    }

    public CreateSubscriptionPojo withAddressDetails(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
        return this;
    }

    public CreateSubscriptionPojo setDefaultData(){
        return this.withCustomerId(String.valueOf(rm.nextInt()))
                .withOrderId(String.valueOf(rm.nextInt()))
                .withItemDetails(new ArrayList<ItemDetail>() {{add(new ItemDetail().setDefaultData());}})
                .withScheduleDetails(new ScheduleDetails().setDefaultData())
                .withDeliverySlot(new DeliverySlot().setDefaultData())
                .withAddressDetails(new AddressDetails().setDefaultData());

    }

}
