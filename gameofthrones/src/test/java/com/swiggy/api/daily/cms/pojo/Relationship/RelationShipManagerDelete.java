
package com.swiggy.api.daily.cms.pojo.Relationship;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "delete_relations"
})
public class RelationShipManagerDelete {

    @JsonProperty("delete_relations")
    private List<Delete_relation> delete_relations = null;

    @JsonProperty("delete_relations")
    public List<Delete_relation> getDelete_relations() {
        return delete_relations;
    }

    @JsonProperty("delete_relations")
    public void setDelete_relations(List<Delete_relation> delete_relations) {
        this.delete_relations = delete_relations;
    }

    public RelationShipManagerDelete setData(String sstoreid, String spid, String dstoreid, String dspinid, String retype, String id){
        List li2=new ArrayList();
        Delete_relation dr=new Delete_relation();
        Source sr=new Source();
        Destination ds=new Destination();
        sr.setStore_id(sstoreid);
        sr.setProduct_id(spid);
        ds.setStore_id(dstoreid);
        ds.setSpin(dspinid);
        dr.setSource(sr);
        dr.setDestination(ds);
        dr.setRelation_type(retype);
        dr.setId(id);
        li2.add(dr);
        this.setDelete_relations(li2);
        return this;
    }

    public RelationShipManagerDelete setWrongDataDelete(Source sr, Destination ds, String retype, String id){
        List li9=new ArrayList();
        Delete_relation cr=new Delete_relation();
        cr.setSource(sr);
        cr.setDestination(ds);
        cr.setRelation_type(retype);
        cr.setId(id);
        li9.add(cr);
        this.setDelete_relations(li9);
        return this;
    }



}
