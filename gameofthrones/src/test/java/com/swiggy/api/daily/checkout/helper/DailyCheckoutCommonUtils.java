package com.swiggy.api.daily.checkout.helper;

import com.google.gson.JsonArray;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.checkout.pojo.LoginPayload;
import com.swiggy.api.daily.checkout.pojo.SlotDetails;
import com.swiggy.api.daily.checkout.pojo.UserInfo;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import cucumber.api.java.ca.I;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.Validator;
import net.minidev.json.JSONArray;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DailyCheckoutCommonUtils {

    private Initialize gameOfThrones = Initializer.getInitializer();
    DailyCheckoutConstants dailyCheckoutConstants;

    public HashMap<String, String> setHeader(String tid, String token) {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("User-Agent", DailyCheckoutConstants.USER_AGENT);
        requestHeader.put("Version-Code", DailyCheckoutConstants.VERSION_CODE);
        requestHeader.put("deviceId", "TestDevice");
        requestHeader.put("swuid", "TestDevice");
        return requestHeader;
    }

    public HashMap<String, String> setHeader(String tid, String token,String user_agent, String version_code) {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("z", user_agent);
        requestHeader.put("Version-Code", version_code);
        requestHeader.put("deviceId", "TestDevice");
        requestHeader.put("swuid", "TestDevice");
        return requestHeader;
    }


    public HashMap<String, String> getHeader(String tid, String token) {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("User-Agent", DailyCheckoutConstants.USER_AGENT);
        requestHeader.put("Version-Code", DailyCheckoutConstants.VERSION_CODE);
        requestHeader.put("deviceId", DailyCheckoutConstants.DEVICE_ID);
        requestHeader.put("swuid", DailyCheckoutConstants.DEVICE_ID);
        return requestHeader;
    }

    public HashMap<String, String> getHeader(Map<String, String> userInfoHeader) {
        HashMap<String, String> requestHeader = new HashMap<>();
        UserInfo decode = Utility.jsonDecode(userInfoHeader.get("userInfo"), UserInfo.class);

        requestHeader.put("tid", decode.getTid());
        requestHeader.put("token", decode.getToken());
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("User-Agent", DailyCheckoutConstants.USER_AGENT);
        requestHeader.put("Version-Code", DailyCheckoutConstants.VERSION_CODE);
        return requestHeader;
    }

    public HashMap<String, String> getHeader(String tid, String token,
                                             String userAgent, String versionCode) {
        HashMap<String, String> requestHeader = new HashMap<>();
        requestHeader.put("content-type", DailyCheckoutConstants.CONTENT_TYPE);
        requestHeader.put("tid", tid);
        requestHeader.put("token", token);
        requestHeader.put("User-Agent", userAgent);
        requestHeader.put("Version-Code", versionCode);
        return requestHeader;
    }

    public HashMap<String, String> getUserInfoHeader(Map<String, String> userInfo) {
        HashMap<String, String> headers = new HashMap<>();
        UserInfo uInfo = new UserInfo();
        uInfo.setTid(userInfo.get("tid"));
        uInfo.setToken(userInfo.get("token"));
        uInfo.setSid(userInfo.get("sid"));
        uInfo.setUserId(userInfo.get("userId"));
        uInfo.setVersionCode(Integer.parseInt(DailyCheckoutConstants.VERSION_CODE));
        uInfo.setUserAgent(DailyCheckoutConstants.USER_AGENT);

        headers.put("userInfo", Utility.jsonEncode(uInfo));
        headers.put("content-type", "application/json");
        return headers;
    }

    public HashMap<String, String> getUserInfoHeader(Map<String, String> userInfo, String user_agent, String version_code) {
        HashMap<String, String> headers = new HashMap<>();
        UserInfo uInfo = new UserInfo();
        uInfo.setTid(userInfo.get("tid"));
        uInfo.setToken(userInfo.get("token"));
        uInfo.setSid(userInfo.get("sid"));
        uInfo.setUserId(userInfo.get("userId"));
        uInfo.setVersionCode(Integer.parseInt(version_code));
        uInfo.setUserAgent(user_agent);

        headers.put("userInfo", Utility.jsonEncode(uInfo));
        headers.put("content-type", "application/json");
        return headers;
    }


    public HashMap<String, String> dailyLogin(String mobile, String password) {

        Validator resValidator = consumerDailyLogin(mobile, password).ResponseValidator;

        String tid = resValidator.GetNodeValue("$.tid");
        String token = resValidator.GetNodeValue("$.data.token");
        String userId = resValidator.GetNodeValue("$.data.customer_id");
        String deviceId = resValidator.GetNodeValue("$.deviceId");
        String sid = resValidator.GetNodeValue("$.sid");

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("tid", tid);
        hashMap.put("token", token);
        hashMap.put("userId", userId);
        hashMap.put("deviceId", deviceId);
        hashMap.put("sid", sid);

        return hashMap;
    }

    public void validateResponseCode(Processor processor) {
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200);
    }


    public void validateStatusCode(Processor processor) {
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200);
        int errorCode = processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(errorCode, 0);
    }

    public void validateRefundStatusCode(Processor processor){
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200);
        int errorCode = processor.ResponseValidator.GetNodeValueAsInt("$.status_code");
        Assert.assertEquals(errorCode, 0);
    }

    public void validateErrorCode(Processor processor){
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200);
        int errorCode = processor.ResponseValidator.GetNodeValueAsInt("$.data.errorCode");
        Assert.assertEquals(errorCode, 0);
    }


    public Processor consumerDailyLogin(String mobile, String password) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanduserdaily", "applogin", gameOfThrones);

        LoginPayload loginPayload = new LoginPayload();
        loginPayload.setMobile(mobile);
        loginPayload.setPassword(password);
        String payload = Utility.jsonEncode(loginPayload);

        return new Processor(service, headers, new String[]{payload});
    }

    public void validateApiResStatusData(String response) {
        String getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        Assert.assertEquals(getStatusCode, dailyCheckoutConstants.VALID_STATUS_CODE);
        String isSuccessful = JsonPath.read(response, "$.statusMessage").toString().replace("[", "").replace("]", "");
        Assert.assertNotNull(isSuccessful, dailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE);
    }

    public SlotDetails getdefaultSlotDeatils(Processor cartResponse) {

        SlotDetails slotDetails = new SlotDetails();

        String slotID = cartResponse.ResponseValidator.GetNodeValue("$.data.cartDeliveryDetails.slotDetails.id");
        int endTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.slotDetails.end_time");
        int startTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.slotDetails.start_time");

        slotDetails.setId(slotID);
        slotDetails.setEndTime(Integer.toString(endTime));
        slotDetails.setStartTime(Integer.toString(startTime));

        return slotDetails;

    }

    public SlotDetails getPLDeliverySlotDeatils(Processor cartResponse)
    {


        JSONArray slotID = cartResponse.ResponseValidator.GetNodeValueAsJsonArray("$.data.delivery_slots.slot_details[*]");
        for (int i=0;i<slotID.size();i++) {

            boolean isEnabled = cartResponse.ResponseValidator.GetNodeValueAsBool("$.data.delivery_slots.slot_details["+i+"].is_enabled");
            boolean isSelected = cartResponse.ResponseValidator.GetNodeValueAsBool("$.data.delivery_slots.slot_details["+i+"].is_selected");
            if (isEnabled== true && isSelected ==false) {
                int startTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.delivery_slots.slot_details["+i+"].slot_details.start_time");
                int endTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.delivery_slots.slot_details["+i+"].slot_details.end_time");
                String selectedSlotID = cartResponse.ResponseValidator.GetNodeValue("$.data.delivery_slots.slot_details["+i+"].slot_details.id");
                SlotDetails slotDetails = new SlotDetails();

                slotDetails.setId(selectedSlotID);
                slotDetails.setEndTime(Integer.toString(endTime));
                slotDetails.setStartTime(Integer.toString(startTime));
                return slotDetails;


            }
        }


        return null;

    }

    public SlotDetails getPLdefaultSlotDeatils(Processor cartResponse) {

        SlotDetails slotDetails = new SlotDetails();

        String slotID = cartResponse.ResponseValidator.GetNodeValue("$.data.cartDeliveryDetails.slotDetails.id");
        int endTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.slotDetails.end_time");
        int startTime = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cartDeliveryDetails.slotDetails.start_time");

        slotDetails.setId(slotID);
        slotDetails.setEndTime(Integer.toString(endTime));
        slotDetails.setStartTime(Integer.toString(startTime));

        return slotDetails;

    }

    public void cartStatusValidators(Processor cartResponse, String expectedStatusMessage )
    {

        int expectedStatusCode = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if(expectedStatusCode==0) {
            Assert.assertEquals(DailyCheckoutConstants.VALID_STATUS_CODE, expectedStatusCode,"Expected Status code is "+expectedStatusCode+"");

            String expectedStatusMessage1 = cartResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE, expectedStatusMessage1,"DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE "+expectedStatusMessage1+" but found "+expectedStatusMessage1+"");
            String actualStatusMessage = cartResponse.ResponseValidator.GetNodeValue("$.data.nonCheckoutReason");
            Assert.assertEquals(expectedStatusMessage, actualStatusMessage);
        }else {
            String actualStatusMessage = cartResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(actualStatusMessage, expectedStatusMessage,"Expected status message is "+expectedStatusMessage+" but found "+actualStatusMessage+"");
        }
    }

    public void cartPLStatusValidators(Processor cartResponse, String expectedStatusMessage, int expectedStatusCode )
    {
        int actualStatusCode = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(expectedStatusCode,actualStatusCode,"Expected status code is "+expectedStatusCode+" but found "+actualStatusCode+"");

        String actualStatusMessage = cartResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        Assert.assertEquals(actualStatusMessage, expectedStatusMessage,"Expected status message is "+expectedStatusMessage+" but found "+actualStatusMessage+"");

    }

    public double packagingChargesCalculation(Processor cartResponse)
    {
        //Packaging charges
        String cartResponsenew = cartResponse.ResponseValidator.GetBodyAsText();

        String inclusivePackagingTax1 =JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.inclusiveTax").toString().replace("[", "").replace("]", "");
        String packageBasePrice = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.price").toString().replace("[", "").replace("]", "");
        String packagingChargesDiscount = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.discount").toString().replace("[", "").replace("]", "");
        String actualTotalPackagingCharge = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.total").toString().replace("[", "").replace("]", "");
        String totalPackagingPriceWithoutDiscount = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.totalWithoutDiscount").toString().replace("[", "").replace("]", "");
        double expectedPackagingTotalWithoutDiscount;
        double actualPackagingTotalWithoutDiscount=Double.parseDouble(totalPackagingPriceWithoutDiscount);
        double actualPackagingTotal=Double.parseDouble(actualTotalPackagingCharge);
        double PackagediscountTD=Double.parseDouble(packagingChargesDiscount);
        double PackageBasePrice = Double.parseDouble(packageBasePrice);
        boolean inclusivePackagingTax = Boolean.parseBoolean(inclusivePackagingTax1);

        double expectedPackagingtotal = PackageBasePrice - PackagediscountTD;
        System.out.println("Total" + expectedPackagingtotal);
        Assert.assertEquals(expectedPackagingtotal,actualPackagingTotal);

        if (!inclusivePackagingTax) {

            String tax = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.tax.value").toString().replace("[", "").replace("]", "");
            double itemTax = Double.parseDouble(tax);

            expectedPackagingTotalWithoutDiscount = itemTax + PackageBasePrice;
        }else {

            expectedPackagingTotalWithoutDiscount = PackageBasePrice;

        }
        Assert.assertEquals(expectedPackagingTotalWithoutDiscount,actualPackagingTotalWithoutDiscount, "Expected Packaging charges "+expectedPackagingTotalWithoutDiscount+" but found "+actualPackagingTotalWithoutDiscount+"");



        return expectedPackagingTotalWithoutDiscount;
    }


    public void priceCalculationsAtItemLevel1(Processor cartResponse) {
        String cartResponsenew = cartResponse.ResponseValidator.GetBodyAsText();
         String inclusiveTax1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.inclusiveTax" ).toString().replace("[", "").replace("]", "");
        String discount1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.discount").toString().replace("[", "").replace("]", "");
        String itemBasePrice1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.price").toString().replace("[", "").replace("]", "");
        String actualTotalPrice1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.total").toString().replace("[", "").replace("]", "");
        String totalPriceWithoutDiscount = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.total").toString().replace("[", "").replace("]", "");



        boolean inclusiveTax = Boolean.parseBoolean(inclusiveTax1);
        double discount = Double.parseDouble(discount1);
        double itemBasePrice = Double.parseDouble(itemBasePrice1);
        double actualTotalWithoutDiscount = Double.parseDouble(totalPriceWithoutDiscount);
        double actualTotalPrice = Double.parseDouble(actualTotalPrice1);


        double expectedTotalWithoutDiscount;
        double expectedtotal = itemBasePrice - discount;

        Assert.assertEquals(expectedtotal, actualTotalPrice);
        if (!inclusiveTax) {

            String tax = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.tax.value").toString().replace("[", "").replace("]", "");
            double itemTax = Double.parseDouble(tax);

            expectedTotalWithoutDiscount = itemTax + itemBasePrice ;
        } else {

            expectedTotalWithoutDiscount = itemBasePrice;

        }
       // Assert.assertEquals(expectedTotalWithoutDiscount, actualTotalWithoutDiscount);

        //Item price after Packaging charges

            double packagingCharges = packagingChargesCalculation(cartResponse);
           double ItemTotalWithPackagingCharges = expectedtotal + packagingCharges;
        double ItemTotalWithPackagingChargeswithoutDiscount = expectedTotalWithoutDiscount + packagingCharges;

        String actualTotalwithPachagingCharges1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.total").toString().replace("[", "").replace("]", "");
        double actualTotalwithPachagingCharges = Double.parseDouble(actualTotalwithPachagingCharges1);

        String actualItemTotalWithPackagingChargeswithoutDiscount1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.totalWithoutDiscount").toString().replace("[", "").replace("]", "");
        double actualItemTotalWithPackagingChargeswithoutDiscount = Double.parseDouble(actualItemTotalWithPackagingChargeswithoutDiscount1);

        Assert.assertEquals(actualItemTotalWithPackagingChargeswithoutDiscount,ItemTotalWithPackagingChargeswithoutDiscount);
        Assert.assertEquals(ItemTotalWithPackagingCharges,actualTotalwithPachagingCharges);

        //Final Item Price
        String actualFinalTotalwithPackagingCharges1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].total").toString().replace("[", "").replace("]", "");
        double actualFinalTotalwithPackagingCharges = Double.parseDouble(actualFinalTotalwithPackagingCharges1);

        String actualFinalItemTotalWithPackagingChargeswithoutDiscount1 = JsonPath.read(cartResponsenew,"$.data.cartItems[0].totalWithoutDiscount").toString().replace("[", "").replace("]", "");
        double actualFinalItemTotalWithPackagingChargeswithoutDiscount = Double.parseDouble(actualFinalItemTotalWithPackagingChargeswithoutDiscount1);

       int quantity = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cartItems[0].quantity");

        double expectedFinalTotalwithPachagingCharges = actualTotalwithPachagingCharges * quantity;
        double expectedFinalItemTotalWithPackagingChargeswithoutDiscount = ItemTotalWithPackagingChargeswithoutDiscount *quantity;

        Assert.assertEquals(expectedFinalTotalwithPachagingCharges,actualFinalTotalwithPackagingCharges);
        Assert.assertEquals(expectedFinalItemTotalWithPackagingChargeswithoutDiscount,actualFinalItemTotalWithPackagingChargeswithoutDiscount);
    }


    public void tdCartResponseValidator(Processor cartResponse, HashMap<String,String> expectedPromotionID){

        //sz actualPromotionID = cartResponse.ResponseValidator.GetNodeValueAsJsonArray("");

    }


    public double packagingChargesCalculationAtCartLevel(Processor cartResponse)
    {
        //Packaging charges
        String cartResponsenew = cartResponse.ResponseValidator.GetBodyAsText();

        String inclusivePackagingTax1 =JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.inclusiveTax").toString().replace("[", "").replace("]", "");
        String packageBasePrice = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.price").toString().replace("[", "").replace("]", "");
        String packagingChargesDiscount = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.discount").toString().replace("[", "").replace("]", "");
        String actualTotalPackagingCharge = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.total").toString().replace("[", "").replace("]", "");
        String totalPackagingPriceWithoutDiscount = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemPackagingCharge.totalWithoutDiscount").toString().replace("[", "").replace("]", "");
        double expectedPackagingTotalWithoutDiscount;
        double actualPackagingTotalWithoutDiscount=Double.parseDouble(totalPackagingPriceWithoutDiscount);
        double actualPackagingTotal=Double.parseDouble(actualTotalPackagingCharge);
        double PackagediscountTD=Double.parseDouble(packagingChargesDiscount);
        double PackageBasePrice = Double.parseDouble(packageBasePrice);
        boolean inclusivePackagingTax = Boolean.parseBoolean(inclusivePackagingTax1);

        double expectedPackagingtotal = PackageBasePrice - PackagediscountTD;
        System.out.println("Total" + expectedPackagingtotal);
        Assert.assertEquals(expectedPackagingtotal,actualPackagingTotal);

        if (!inclusivePackagingTax) {

            String tax = JsonPath.read(cartResponsenew,"$.data.cartItems[0].itemBill.itemCharge.tax.value").toString().replace("[", "").replace("]", "");
            double itemTax = Double.parseDouble(tax);

            expectedPackagingTotalWithoutDiscount = itemTax + PackageBasePrice;
        }else {

            expectedPackagingTotalWithoutDiscount = PackageBasePrice;

        }
        Assert.assertEquals(expectedPackagingTotalWithoutDiscount,actualPackagingTotalWithoutDiscount);



        return expectedPackagingTotalWithoutDiscount;
    }


    public void finalCartcalcalculations(Processor cartResponse)

    {
        String cartResponsenew = cartResponse.ResponseValidator.GetBodyAsText();

        Double  itemTotal = JsonPath.read(cartResponsenew,"$.data.cartBill.itemTotalCharges.total");
        int packagingTotal = JsonPath.read(cartResponsenew,"$.data.cartBill.cartPackingCharges.total");


        Double expectedFinalAmount1 = itemTotal + packagingTotal;
        Double expectedFinalAmount = Math.floor(expectedFinalAmount1 + 0.5);
        int expectedFinalAmountToPay = expectedFinalAmount.intValue();
        int actualFinalAmountToPay = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.paymentToBeCollected.paymentToBeCollected");
        Assert.assertEquals(expectedFinalAmountToPay,actualFinalAmountToPay);


    }

    public void finalCartPLcalcalculations(Processor cartResponse)

    {
        String cartResponsenew = cartResponse.ResponseValidator.GetBodyAsText();

        Double  itemTotal = JsonPath.read(cartResponsenew,"$.data.cartBill.itemTotalCharges.total");
        int packagingTotal = JsonPath.read(cartResponsenew,"$.data.cartBill.cartPackingCharges.total");


        Double expectedFinalAmount1 = itemTotal + packagingTotal;
        Double expectedFinalAmount = Math.floor(expectedFinalAmount1 + 0.5);
        int expectedFinalAmountToPay = expectedFinalAmount.intValue();
        int actualFinalAmountToPay = cartResponse.ResponseValidator.GetNodeValueAsInt("$.data.cart_pricing_rendering.cart_rendering[0].value");
        Assert.assertEquals(expectedFinalAmountToPay,actualFinalAmountToPay);


    }

    public List<Map<String, Object>> getDBTransactionDetails(String tableName,String transactionId)
    {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("checkoutPaasPl");
        String query1="select * from "+ tableName +" where order_id="+transactionId;
        String query = String.format(query1);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(query);
        //list.toString().substring(9,16);
        return list;
    }

}

