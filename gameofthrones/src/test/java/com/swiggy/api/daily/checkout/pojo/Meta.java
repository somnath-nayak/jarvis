package com.swiggy.api.daily.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({

})
public class Meta {


    @Override
    public String toString() {
        return new ToStringBuilder(this).toString();
    }

}