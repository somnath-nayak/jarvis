package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InventoryService {

    CMSDailyHelper helper = new CMSDailyHelper();
    List<Long> slotpoduct = new ArrayList();
    List<Long> slotplan = new ArrayList();
    List<String> keyproduct = new ArrayList();
    List<String> keyplan = new ArrayList();
    Integer maxcountupdated=109;

    @Test(description = "This test will create inventory for a meal with valid data", dataProviderClass = CMSDailyDP.class, dataProvider = "createinventorymeal",priority = 0)
    public void createInventoryMeal(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        slotpoduct.add(slotid);
        keyproduct.add(key);
        String response = helper.createInventory(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount, serviceline).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyact, key);

    }

    @Test(description = "This test will create inventory for a plan with valid data", dataProviderClass = CMSDailyDP.class, dataProvider = "createinventoryplan",priority = 0)
    public void createInventoryPlan(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        slotplan.add(slotid);
        keyplan.add(key);
        String response = helper.createInventory(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount, serviceline).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyact, key);
    }

    @Test(description = "This test fetch inventory for meal with valid data", dataProviderClass = CMSDailyDP.class, dataProvider = "createinventorymeal",priority = 1)
    public void getMealInventory(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        slotpoduct.add(slotid);
        String response = helper.createInventory(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount, serviceline).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        String responseget = helper.getInventory(keyact, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String keyget = JsonPath.read(responseget, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyget, keyact);
    }

    @Test(description = "This test fetch inventory type of plan with valid data", dataProviderClass = CMSDailyDP.class, dataProvider = "createinventoryplan",priority = 1)
    public void getPlanInventory(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        slotplan.add(slotid);
        String response = helper.createInventory(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount, serviceline).ResponseValidator.GetBodyAsText();
        String keyact = JsonPath.read(response, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        String responseget = helper.getInventory(keyact, CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String keyget = JsonPath.read(responseget, "$..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        Assert.assertEquals(keyget, keyact);
    }

    @Test(description = "This test will verify search inventory with SPIN", dataProviderClass = CMSDailyDP.class, dataProvider = "createinventorymeal",priority = 1)
    public void searchInventoryBySpin(String key, String pid, String skuid, String spinid, String storeid, Long startepc, Long endepoc, Long slotid, Integer maxcap, Integer soldcount, String serviceline) throws Exception {
        slotpoduct.add(slotid);
        String response = helper.createInventory(key, pid, skuid, spinid, storeid, startepc, endepoc, slotid, maxcap, soldcount, serviceline).ResponseValidator.GetBodyAsText();
        String responsesearch = helper.searchInventory(spinid, null, null, "daily_catalog_product").ResponseValidator.GetBodyAsText();
        String keys = JsonPath.read(responsesearch, "$.data..key").toString().replace("[", "").replace("]", "").replace("\"", "");
        List<String> list = Arrays.asList(keys.split("\\s*,\\s*"));
        Assert.assertNotNull(list);
        Assert.assertTrue(list.contains(key));

    }

    @Test(description = "This test will verify update of inventoty max-cap for Meal",priority = 2)
    public void updateMaxCapMeal() {
        String res=helper.updateMaxCap(null,null,Long.parseLong(slotpoduct.get(0).toString()),maxcountupdated,null,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String maxcap=JsonPath.read(res,"$..inventory_construct.inventories.max_cap").toString().replace("[","").replace("]","");;
        Assert.assertEquals(maxcap,maxcountupdated.toString(),"Max Cap Value is not matching");

    }


    @Test(description = "This test will verify update of inventoty max-cap for plan",priority = 2)
    public void updateMaxCapPlan()  {
        String res=helper.updateMaxCap(null,null,Long.parseLong(slotplan.get(0).toString()),maxcountupdated,null,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String maxcap=JsonPath.read(res,"$..inventory_construct.inventories.max_cap").toString().replace("[","").replace("]","");
        Assert.assertEquals(maxcap,maxcountupdated.toString(),"Max Cap Value is not matching");

    }

    @Test(description = "This test will verify negative cases for updating inventory for meal with different type of data",dataProviderClass = CMSDailyDP.class,dataProvider = "updateinventorynegative",priority = 3)
    public void updateMaxCapMealNegative(String slotid,Integer maxcount)  {
        String res=helper.updateMaxCap(null,null,Long.parseLong(slotid),maxcount,null,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String statusmsg=JsonPath.read(res,"$.status_message").toString();
        Assert.assertEquals(statusmsg,"no inventory updated.","Something wrong with datas");

    }


    @Test(description = "This test will verify negative cases for updating inventory for plan with different type of data",dataProviderClass = CMSDailyDP.class,dataProvider = "updateinventorynegative",priority = 3)
    public void updateMaxCapPlanNegative(String slotid,Integer maxcount)  {
        String res=helper.updateMaxCap(null,null,Long.parseLong(slotid),maxcount,null,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String statusmsg=JsonPath.read(res,"$.status_message").toString();
        Assert.assertEquals(statusmsg,"no inventory updated.","Something wrong with data");

    }

    @Test(description = "This test will verify negative cases for fetching inventory for meal with different type of data",dataProviderClass = CMSDailyDP.class,dataProvider = "getinventorynegative",priority = 3)
    public void getMealInventoryNegative(String key)  {
        String res=helper.getInventory(key,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        List<String> data=JsonPath.read(res,"$.data");
        Assert.assertEquals(data.size(),0,"List should be equal");

    }

    @Test(description = "This test will verify negative cases for fetching inventory for plan with different type of data",dataProviderClass = CMSDailyDP.class,dataProvider = "getinventorynegative",priority = 3)
    public void getPlanInventoryNegative(String key)  {
        String res=helper.getInventory(key,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        List<String> data=JsonPath.read(res,"$.data");
        Assert.assertEquals(data.size(),0,"List should be equal");

    }


    @Test(description = "This test will delete inventory for Meal with of valid data",priority = 4)
    public void deleteMealInventoryBySlot() throws Exception {
        for (int i=0;i<keyproduct.size();i++){
        String responsedelete = helper.deleteInventory(keyproduct.get(i), CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(responsedelete, "$..data").toString().replace("[", "").replace("]", "");
        List<String> list = Arrays.asList(data.split("\\s*,\\s*"));
        Assert.assertNotNull(list);}
    }

    @Test(description = "This test will delete inventory for Meal with of valid data",priority = 4)
    public void deletePlanInventoryBySlot() throws Exception {
        for (int i=0;i<keyplan.size();i++){
        String responsedelete = helper.deleteInventory(keyplan.get(i), CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(responsedelete, "$..data").toString().replace("[", "").replace("]", "");
        List<String> list = Arrays.asList(data.split("\\s*,\\s*"));
        Assert.assertNotNull(list);
        }
    }
}
