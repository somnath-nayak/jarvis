package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restaurantTimeMap"
})
public class OrderDispatchPOJO {

    @JsonProperty("restaurantTimeMap")
    private List<RestaurantTimeMapPOJO> restaurantTimeMap = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    RestaurantTimeMapPOJO restaurantTimeMapPOJO = new RestaurantTimeMapPOJO();

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderDispatchPOJO() {
    }

    /**
     *
     * @param restaurantTimeMap
     */
    public OrderDispatchPOJO(List<RestaurantTimeMapPOJO> restaurantTimeMap) {
        super();
        this.restaurantTimeMap = restaurantTimeMap;
    }

    @JsonProperty("restaurantTimeMap")
    public List<RestaurantTimeMapPOJO> getRestaurantTimeMap() {
        return restaurantTimeMap;
    }

    @JsonProperty("restaurantTimeMap")
    public void setRestaurantTimeMap(List<RestaurantTimeMapPOJO> restaurantTimeMap) {
        this.restaurantTimeMap = restaurantTimeMap;
    }

    public OrderDispatchPOJO withRestaurantTimeMap(List<RestaurantTimeMapPOJO> restaurantTimeMap) {
        this.restaurantTimeMap = restaurantTimeMap;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public OrderDispatchPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public OrderDispatchPOJO setDefault(String rest_id) {
        restaurantTimeMapPOJO.setDefault(rest_id);
        List list = new ArrayList();
        list.add(restaurantTimeMapPOJO);
        this.withRestaurantTimeMap(list);
        return this;
    }

}
