package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CartDetails {
    @JsonProperty("cartDetails")
    private Cart cartDetails;
    @JsonProperty("coupon")
    private String coupon;
    @JsonProperty("deliveryDetails")
    private DeliveryDetails deliveryDetails;
    
    public Cart getCartDetails() {
        return cartDetails;
    }
    
    public void setCartDetails(Cart cartDetails) {
        this.cartDetails = cartDetails;
    }
    
    public String getCoupon() {
        return coupon;
    }
    
    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
    
    public DeliveryDetails getDeliveryDetails() {
        return deliveryDetails;
    }
    
    public void setDeliveryDetails(DeliveryDetails deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }
    
   public CartDetails withCartDetails(Cart cartDetails){
        setCartDetails(cartDetails);
        return this;
   }
    
    public CartDetails withCoupon(String coupon){
        setCoupon(coupon);
        return this;
    }
    
    public CartDetails withDeliveryDetails(DeliveryDetails deliveryDetails){
        setDeliveryDetails(deliveryDetails);
        return this;
    }
    
    public CartDetails withGivenValues(Cart cartDetails,String coupon,  DeliveryDetails deliveryDetails){
    
        setCartDetails(cartDetails);setCoupon(coupon);setDeliveryDetails(deliveryDetails);
        return this;
        
    }
    
    public CartDetails cartRequest(String[] storeId, String[] skuID, List<Integer> quantity , List<Integer> price, Integer itemTotal ){
       setCartDetails(new Cart().withGivenValues( skuID, storeId,  quantity, price,itemTotal));
        setCoupon("");
        setDeliveryDetails(new DeliveryDetails().withDefaultValues());
    
       return this;
    }
    
    
}
