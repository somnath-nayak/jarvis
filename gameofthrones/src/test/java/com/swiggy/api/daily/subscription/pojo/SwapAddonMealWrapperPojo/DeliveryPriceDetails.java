
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "totalWithoutDiscount",
    "total",
    "discount",
    "deliveryFeeList",
    "discountMessage"
})
public class DeliveryPriceDetails {

    @JsonProperty("totalWithoutDiscount")
    private Integer totalWithoutDiscount;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("discount")
    private Integer discount;
    @JsonProperty("deliveryFeeList")
    private List<DeliveryFeeList> deliveryFeeList = null;
    @JsonProperty("discountMessage")
    private String discountMessage;

    @JsonProperty("totalWithoutDiscount")
    public Integer getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public DeliveryPriceDetails withTotalWithoutDiscount(Integer totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public DeliveryPriceDetails withTotal(Integer total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Integer getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public DeliveryPriceDetails withDiscount(Integer discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("deliveryFeeList")
    public List<DeliveryFeeList> getDeliveryFeeList() {
        return deliveryFeeList;
    }

    @JsonProperty("deliveryFeeList")
    public void setDeliveryFeeList(List<DeliveryFeeList> deliveryFeeList) {
        this.deliveryFeeList = deliveryFeeList;
    }

    public DeliveryPriceDetails withDeliveryFeeList(List<DeliveryFeeList> deliveryFeeList) {
        this.deliveryFeeList = deliveryFeeList;
        return this;
    }

    @JsonProperty("discountMessage")
    public String getDiscountMessage() {
        return discountMessage;
    }

    @JsonProperty("discountMessage")
    public void setDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
    }

    public DeliveryPriceDetails withDiscountMessage(String discountMessage) {
        this.discountMessage = discountMessage;
        return this;
    }

    public DeliveryPriceDetails setDefaultData()  {
        return this.withTotalWithoutDiscount(SubscriptionConstant.DELIVERYPRICEDETAILS_TOTALWITHOUTDISCOUNT)
                .withTotal(SubscriptionConstant.DELIVERYPRICEDETAILS_TOTAL)
                .withDiscount(SubscriptionConstant.DELIVERYPRICEDETAILS_DISCOUNT)
                .withDeliveryFeeList(new ArrayList<DeliveryFeeList>() {{add(new DeliveryFeeList().setDefaultData());}})
                .withDiscountMessage("");

    }


}
