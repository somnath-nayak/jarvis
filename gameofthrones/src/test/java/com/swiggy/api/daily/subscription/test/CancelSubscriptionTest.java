package com.swiggy.api.daily.subscription.test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.helper.DailyPreorderHelper;
import com.swiggy.api.daily.subscription.dp.SubscriptionDp;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.helper.SubscriptionTestHelper;
import com.swiggy.api.daily.subscription.pojo.ServerTimeChangePojo;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.ParseException;

public class CancelSubscriptionTest extends SubscriptionDp {
    SubscriptionHelper subscriptionHelper=new SubscriptionHelper();
    SubscriptionTestHelper subscriptionTestHelper=new SubscriptionTestHelper();
    ServerTimeChangePojo serverTimeChangePojo=new ServerTimeChangePojo();
    DailyPreorderHelper dailyPreorderHelper=new DailyPreorderHelper();
    JsonHelper jsonHelper=new JsonHelper();
    String responseSubscriptionId;

    @BeforeMethod
    public void setup() {
        responseSubscriptionId="";
    }

    //----------------------------------------------------------Cancel Subscription----------------------------------------------------------------

    @Test(dataProvider="subscriptionBillDetailsDP",description="Cancel the COD Subscription on first day",groups={"Sanity_Test","Regression"})
    public void cancelSubscriptionTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        Processor cancelSubscriptionProcessor =subscriptionHelper.cancelSubscription(orderId,SubscriptionConstant.SUBSCRIPTION_CANCEL_REASON );
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        subscriptionTestHelper.validateCancelSubscriptionResponse(orderDailyOmsProcessor,cancelSubscriptionProcessor,0);
    }

    @Test(dataProvider="subscriptionBillDetailsDP",description="Cancel the COD Subscription after first meal delivered",groups={"Regression"})
    public void cancelSubscriptionAfterFirstMealDeliveredTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        subscriptionHelper.updateSubscriptionProgress(orderId," {\"fulfilled\":1,\"cancelled\":0,\"skipped\":0,\"paused\":0}");
        Processor cancelSubscriptionProcessor =subscriptionHelper.cancelSubscription(orderId,SubscriptionConstant.SUBSCRIPTION_CANCEL_REASON );
        Processor orderDailyOmsProcessor=subscriptionHelper.getOrderDailyOms(orderId);
        subscriptionTestHelper.validateCancelSubscriptionResponse(orderDailyOmsProcessor,cancelSubscriptionProcessor,1);
    }

    @Test(dataProvider="subscriptionBillDetailsDP",description="Cancel the COD Subscription on second day post cutoff time",groups={"Regression"})
    public void cancelSubscriptionOnSecondDayPostcutoffTest(String orderId) throws IOException, ParseException {
        try {
            responseSubscriptionId = orderId;
            Processor orderDailyOmsProcessor = subscriptionHelper.getOrderDailyOms(orderId);
            Long startDate = JsonPath.parse(orderDailyOmsProcessor.ResponseValidator.GetBodyAsText()).read("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.deliverySchedule.startDate", Long.class);
            subscriptionHelper.updateSubscriptionProgress(orderId, " {\"fulfilled\":1,\"cancelled\":0,\"skipped\":0,\"paused\":0}");


            String date = DateUtility.getRedableDateWithoutTimeFromEpoch(startDate);
            date=DateUtility.getWorkingDateFromCurrentDate(date,1);
            String deliveryStartTime = String.valueOf(orderDailyOmsProcessor.ResponseValidator.GetNodeValueAsInt("$.data.orders[0].order_jobs[0].metadata.deliveryDetail.slotDetails.start_time"));
            deliveryStartTime=deliveryStartTime.substring(0,2)+":"+deliveryStartTime.substring(2,4);

            String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + date));
            subscriptionHelper.changeServerTime(changeServerDatePayload);
            String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + deliveryStartTime));
            subscriptionHelper.changeServerTime(changeServerTimePayload);

            Processor cancelSubscriptionProcessor = subscriptionHelper.cancelSubscription(orderId, SubscriptionConstant.SUBSCRIPTION_CANCEL_REASON);
            Processor orderDailyOmsAfterCancelProcessor = subscriptionHelper.getOrderDailyOms(orderId);
            subscriptionTestHelper.validateCancelSubscriptionResponse(orderDailyOmsAfterCancelProcessor, cancelSubscriptionProcessor, 2);
        }finally {
            subscriptionTestHelper.changeSeverCurrentDate();
        }
    }

    //------------------------------------------------------------Cancel Meal Subscription------------------------------------------------------------------

   /* @Test(dataProvider="subscriptionBillDetailsDP",description="Cancel the first COD meal order in precutoff time",groups={"Sanity_Test","Regression"})
    public void cancelFirstMealCODSubscriptionPrecutoffTest(String orderId) throws IOException {
        responseSubscriptionId=orderId;
        Processor cancelSubscriptionMealProcessor=dailyPreorderHelper.cancelPreorderAndSubscriptionMeal("CUSTOMER",Integer.parseInt(orderId),"DAILY_SUBSCRIPTION_MEAL","bad food");

    }
*/



    @AfterMethod
    public void teardown()
    {
        if(!responseSubscriptionId.equals("")) {
            subscriptionHelper.deleteSubscription(responseSubscriptionId);
        }
    }

}
