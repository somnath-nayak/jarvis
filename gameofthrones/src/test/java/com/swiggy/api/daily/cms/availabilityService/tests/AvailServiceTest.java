package com.swiggy.api.daily.cms.availabilityService.tests;

import com.swiggy.api.daily.cms.availabilityService.dp.AvailDP;
import com.swiggy.api.daily.cms.availabilityService.helper.*;
import com.swiggy.api.daily.cms.availabilityService.pojo.AreaSlotPOJO;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.pojo.Slot.SlotCreate;
import com.swiggy.api.daily.vendor.helper.VendorConstant;
import com.swiggy.api.daily.vendor.helper.VendorHelper;
import com.swiggy.api.daily.vendor.pojo.RestaurantSlotPOJO;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class AvailServiceTest {

    public static final Logger logger = Logger.getGlobal();
    Processor processor;
    AvailServiceHelper availServiceHelper = new AvailServiceHelper();
    SRSHelper srsHelper = new SRSHelper();
    TestcaseAvailHelper testcaseAvailHelper = new TestcaseAvailHelper();
    CMSDailyHelper cmsDailyHelper = new CMSDailyHelper();
    RedisHelper redisHelper = new RedisHelper();
    CommonAvailHelper commonAvailHelper = new CommonAvailHelper();
    VendorHelper vendorHelper = new VendorHelper();

//   --------------------------------------- CITY -----------------------------------------

    @Test(description = "Fetch the details of the city with a valid \"cityID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void citySuccessTest() throws Exception {
        processor = availServiceHelper.city(AvailServiceConstant.city);
        testcaseAvailHelper.cityCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{CITY_}3");
    }

    @Test(description = "Fetch the details of the city with an invalid \"cityID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void cityFailureTest() throws Exception {
        processor = availServiceHelper.city(AvailServiceConstant.invalid);
        testcaseAvailHelper.cityFailureCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{CITY_}3");
    }

    @Test(description = "Fetch the details of the city with valid \"cityID\" and check whether the key is getting set in Redis DB", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void cityRedisTest() throws Exception {
        RedisHelper rh=new RedisHelper();
            processor = availServiceHelper.city(AvailServiceConstant.city);
            testcaseAvailHelper.cityCheck(processor);
            Thread.sleep(2000);

            Object key = rh.getValue("cmsredis2", 0, "{CITY_}3");
            logger.info(key + "-----------key------------");
                redisHelper.deleteKey("cmsredis2", 0, "{CITY_}3");
    }

    @Test(description = "Validate the response of the update availability API for city", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void cityResponseTest() throws Exception {
        processor = availServiceHelper.city(AvailServiceConstant.city);
        testcaseAvailHelper.cityCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{CITY_}3");
    }

//   --------------------------------------- AREA -----------------------------------------

    @Test(description = "Fetch the details of the area with valid \"areaID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void areaSuccessTest() throws Exception {
        processor = availServiceHelper.area(AvailServiceConstant.area);
        testcaseAvailHelper.areaCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{AREA_}160");
    }

    @Test(description = "Update the details of the area with invalid \"areaID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void areaFailureTest() throws Exception {
        processor = availServiceHelper.area(AvailServiceConstant.invalid);
        testcaseAvailHelper.areaFailureCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{AREA_}160");
    }

    @Test(description = "Check whether the event gets mapped with Availability service for area and a proper response is recieved with key \"AREA_\" mapped in the Redis DB for the same area", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void areaRedisTest() throws Exception {
        RedisHelper rh = new RedisHelper();
            processor = availServiceHelper.area(AvailServiceConstant.area);
            testcaseAvailHelper.areaCheck(processor);
            Thread.sleep(2000);

            Object key = rh.getValue("cmsredis2", 0, "{AREA_}160");
            logger.info(key + "-----------key------------");
    }

    @Test(description = "Validate the response of the update availability API for city", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void areaResponseTest() throws Exception {
        processor = availServiceHelper.area(AvailServiceConstant.area);
        testcaseAvailHelper.areaCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{AREA_}160");
    }

//   --------------------------------------- STORE -----------------------------------------

    @Test(description = "Fetch the details of the area with valid \"storeID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void storeSuccessTest() throws Exception {
        processor = availServiceHelper.store(AvailServiceConstant.store);
        testcaseAvailHelper.storeSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }

    @Test(description = "Update the details of the area with invalid \"storeID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void storefailureTest() throws Exception {
        processor = availServiceHelper.store(AvailServiceConstant.store);
        testcaseAvailHelper.storeFailureCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }

    @Test(description = "Update the details of the store with valid \"storeID\" and check whether the key STORE_ is getting set in Redis DB", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void storeRedisTest() throws Exception {
        RedisHelper rh=new RedisHelper();
            processor = availServiceHelper.store(AvailServiceConstant.store);
            testcaseAvailHelper.storeFailureCheck(processor);
            Thread.sleep(2000);

            Object key = rh.getValue("cmsredis2", 0, "{STORE_}9990");
            logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }

    @Test(description = "Validate the response of the update availability API for store", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void storeResponseTest() throws Exception {
        processor = availServiceHelper.store(AvailServiceConstant.store);
        testcaseAvailHelper.storeSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }

//   --------------------------------------- PRODUCT -----------------------------------------

    @Test(description = "Update the details of the product with valid \"productID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void productSuccessTest() throws Exception {
        processor = availServiceHelper.product(AvailServiceConstant.product);
        testcaseAvailHelper.productSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

    @Test(description = "Update the details of the  with invalid \"productID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void productfailureTest() throws Exception {
        processor = availServiceHelper.product(AvailServiceConstant.product);
        testcaseAvailHelper.productFailureCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

    @Test(description = "Update the details of the store with valid \"storeID\" and check whether the key STORE_ is getting set in Redis DB", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void productRedisTest() throws Exception {
        RedisHelper rh=new RedisHelper();
        processor = availServiceHelper.product(AvailServiceConstant.product);
        testcaseAvailHelper.productSuccessCheck(processor);
        Thread.sleep(3000);

        Object key = rh.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

    @Test(description = "Validate the response of the update availability API for store", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void productResponseTest() throws Exception {
        processor = availServiceHelper.product(AvailServiceConstant.product);
        testcaseAvailHelper.productSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

//   --------------------------------------- PLAN -----------------------------------------

    @Test(description = "Fetch the details of the plan with valid \"planID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void planSuccessTest() throws Exception {
        processor = availServiceHelper.plan(AvailServiceConstant.plan);
        testcaseAvailHelper.planSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(description = "Update the details of the  with invalid \"productID\"", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void planfailureTest() throws Exception {
        processor = availServiceHelper.plan(AvailServiceConstant.plan);
        testcaseAvailHelper.planFailureCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(description = "Update the details of the store with valid \"storeID\" and check whether the key STORE_ is getting set in Redis DB", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void planRedisTest() throws Exception {
        RedisHelper rh=new RedisHelper();
        processor = availServiceHelper.plan(AvailServiceConstant.plan);
        testcaseAvailHelper.planSuccessCheck(processor);
        Thread.sleep(3000);

        Object key = rh.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(description = "Validate the response of the update availability API for store", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void planResponseTest() throws Exception {
        processor = availServiceHelper.plan(AvailServiceConstant.plan);
        testcaseAvailHelper.planSuccessCheck(processor);
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

//    ---------------------------------------------------TIMELINE-------------------------------------------------------------

//   TIMELINE - PLAN

    @Test(description = "Get the timeline of the restaurant with respect to the valid entity_type PLAN", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void timelinePLANSuccessTest() throws Exception {
        processor = availServiceHelper.store(AvailServiceConstant.store);

        processor = availServiceHelper.plan(AvailServiceConstant.plan);

        processor = availServiceHelper.timeline(AvailServiceConstant.store, AvailServiceConstant.PLAN);
        testcaseAvailHelper.availTimelinePLANCheck(processor);

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

//   TIMELINE - PRODUCT

    @Test(description = "Get the timeline of the restaurant with respect to the valid entity_type PRODUCT", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void timelinePRODUCTSuccessTest() throws Exception {
        processor = availServiceHelper.store(AvailServiceConstant.store);

        processor = availServiceHelper.product(AvailServiceConstant.product);

        processor = availServiceHelper.timeline(AvailServiceConstant.store, AvailServiceConstant.PRODUCT);
        testcaseAvailHelper.availTimelinePRODUCTCheck(processor);

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");

    }

    @Test(description = "Get the timeline of the restaurant with respect to the invalid entity_type", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void timelineInvalidEntityTypeTest() throws Exception {
        processor = availServiceHelper.timeline(AvailServiceConstant.store, AvailServiceConstant.RANDOM);
    }

//    --------------------------------------------------INTEGRATION------------------------------------------------------------

    @Test(dataProvider = "citySlot", dataProviderClass = AvailDP.class, description = "Precondition : Create a city slot from SRS and check whether the event is mapped to topic cms.avail.city.changes\n " +
            "Testcase : Check whether the event gets mapped with Availability service for city and a proper response with updated timeline is received with key CITY_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest1(String data) throws Exception {
        processor = srsHelper.citySlotCreationService(data);
        SoftAssert softAssert = new SoftAssert();
        String city_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        softAssert.assertAll();

        processor = availServiceHelper.city(city_id);
        testcaseAvailHelper.cityCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{CITY_}3");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2",0,"{CITY_}3");
    }

    @Test(dataProvider = "citySlot", dataProviderClass = AvailDP.class, description = "Precondition : Update a city slot from SRS and check whether the event is mapped to topic cms.avail.city.changes\n" +
            "Testcase : Check whether the event gets mapped with Availability service for city and a proper response with updated timeline is received with key CITY_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest2(String data) throws Exception {
        processor = srsHelper.citySlotCreationService(data);
        testcaseAvailHelper.citySlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String city_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        softAssert.assertAll();

        processor = availServiceHelper.city(city_id);
        testcaseAvailHelper.cityCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{CITY_}3");
        logger.info(key + "-----------key------------");
    }

    @Test(dataProvider = "citySlot", dataProviderClass = AvailDP.class, description = "Precondition : Delete a city slot from SRS and check whether the event is mapped to topic cms.avail.city.changes"+
            "Testcase : Check whether the event gets mapped with Availability service for city and a proper response with updated timeline is received with key CITY mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest11(String data) throws Exception {
        processor = srsHelper.citySlotCreationService(data);
        testcaseAvailHelper.citySlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String city_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        softAssert.assertAll();

        processor = availServiceHelper.city(city_id);
        testcaseAvailHelper.cityCheck(processor);
        Thread.sleep(3000);

        logger.info(commonAvailHelper.deleteCitySlot(city_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{CITY_}3");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2",0,"{CITY_}3");
    }

    @Test(dataProvider = "areaSlot", dataProviderClass = AvailDP.class, description = "Precondition : Create a restaurant slot from SRS and check whether the event is mapped to topic cms.avail.store.changes\n" +
            "Testcase : Check whether the event gets mapped with Availability service for store and a proper response with timeline calculated for the store is received with key STORE_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest3(String data) throws Exception {
        processor = srsHelper.areaSlotCreationService(data);
        testcaseAvailHelper.areaSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String area_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        softAssert.assertAll();

        processor = availServiceHelper.area(area_id);
        testcaseAvailHelper.areaCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{AREA_}160");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2",0,"{AREA_}160");
    }

    @Test(dataProvider = "areaSlot", dataProviderClass = AvailDP.class, description = "Precondition : Update a restaurant slot from SRS and check whether the event is mapped to topic cms.avail.store.changes\n" +
            "Testcase : Check whether the event gets mapped with Availability service for store and a proper response with timeline calculated for the store is received with key STORE_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest4(String data) throws Exception {
        processor = srsHelper.areaSlotCreationService(data);
        testcaseAvailHelper.areaSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String area_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        softAssert.assertAll();

        processor = availServiceHelper.area(area_id);
        testcaseAvailHelper.areaCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{AREA_}160");
        logger.info(key + "-----------key------------");
    }

    @Test(dataProvider = "areaSlot", dataProviderClass = AvailDP.class, description = "Precondition : Delete an area slot from SRS and check whether the event is mapped to topic cms.avail.area.changes\n"+
            "Testcase : Check whether the event gets mapped with Availability service for area and a proper response with timeline calculated for area is recieved with key AREA_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest12(String data) throws Exception {
        processor = srsHelper.areaSlotCreationService(data);
        testcaseAvailHelper.areaSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String area_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        softAssert.assertAll();

        processor = availServiceHelper.area(area_id);
        testcaseAvailHelper.areaCheck(processor);
        Thread.sleep(3000);

        logger.info(commonAvailHelper.deleteAreaSlot(area_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{AREA_}160");
        logger.info(key + "-----------key------------");
    }

    @Test(dataProvider = "restTimeSlot", dataProviderClass = AvailDP.class, description = "Precondition : Create a restaurant slot from SRS and check whether the event is mapped to topic cms.avail.store.changes\n"
            +"Testcase : Check whether the event gets mapped with Availability service for store and a proper response with timeline calculated for the store is received with key STORE_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest5(String data) throws Exception {
        processor = srsHelper.createNormalRestaurantSlot(data);
        testcaseAvailHelper.restaurantSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(restaurant_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{STORE_}9990");
        logger.info(key + "-----------key------------");
        redisHelper.deleteKey("cmsredis2",0,"{STORE_}9990");
    }

    @Test(dataProvider = "restTimeSlot", dataProviderClass = AvailDP.class, description = "Precondition : Update a restaurant slot from SRS and check whether the event is mapped to topic cms.avail.store.changes\n"
            +"Testcase : Check whether the event gets mapped with Availability service for store and a proper response with timeline calculated for the store is received with key STORE_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest6(String data) throws Exception {
        processor = srsHelper.createNormalRestaurantSlot(data);
        testcaseAvailHelper.restaurantSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(restaurant_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{STORE_}9990");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }

    @Test(dataProvider = "restTimeSlot", dataProviderClass = AvailDP.class, description = "Precondition : Delete a restaurant slot from SRS and check whether the event is mapped to topic cms.avail.store.changes\n"+
            "Testcase : Check whether the event gets mapped with Availability service for store and a proper response with timeline calculated for the store is recieved with key STORE_ mapped in the Redis DB", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest13(String data) throws Exception {
        processor = srsHelper.createNormalRestaurantSlot(data);
        testcaseAvailHelper.restaurantSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(restaurant_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        logger.info(commonAvailHelper.deleteRestSlot(restaurant_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{STORE_}9990");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
    }


    @Test(dataProvider = "itemSlotCreatePLAN", dataProviderClass = AvailDP.class, description = "Precondition : Create a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"
            +"Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is received with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest7(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.plan(entity_id);
        testcaseAvailHelper.planSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(dataProvider = "itemSlotCreatePLAN", dataProviderClass = AvailDP.class, description = "Precondition : Update a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"
            +"Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is received with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest8(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.plan(entity_id);
        testcaseAvailHelper.planSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(dataProvider = "itemSlotCreatePLAN", dataProviderClass = AvailDP.class, description = "Precondition : Update a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"
            +"Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is received with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest14(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.plan(entity_id);
        testcaseAvailHelper.planSuccessCheck(processor);
        Thread.sleep(3000);

        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id, store_id));

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(dataProvider = "itemSlotCreatePRODUCT", dataProviderClass = AvailDP.class, description = "Precondition : Create a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"+
            "Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is received with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest9(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.product(entity_id);
        testcaseAvailHelper.productSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

    @Test(dataProvider = "itemSlotCreatePRODUCT", dataProviderClass = AvailDP.class, description = "Precondition : Create a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"+
            "Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is received with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest10(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.product(entity_id);
        testcaseAvailHelper.productSuccessCheck(processor);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }


    @Test(dataProvider = "itemSlotCreatePRODUCT", dataProviderClass = AvailDP.class, description = "Precondition : Delete a timeline from slot service of CMS and check whether the event is mapped to topic cms.daily_restaurant_product_avail.change\n"
            +"Testcase : Check whether the event gets mapped with Availability service for product and a proper response with timeline calculation is recieved with key PRODUCT_ mapped in the Redis DB for the same product", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest15(String data) throws Exception {
        processor = srsHelper.createItemSlot(data);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotNull(entity_id);
        String store_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].store_id"));
        softAssert.assertNotNull(store_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(store_id);
        testcaseAvailHelper.storeSuccessCheck(processor);
        Thread.sleep(3000);

        processor = availServiceHelper.product(entity_id);
        testcaseAvailHelper.productSuccessCheck(processor);
        Thread.sleep(3000);

        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id, store_id));

        RedisHelper redisHelper = new RedisHelper();
        Object key = redisHelper.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key + "-----------key------------");

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }


    @Test(dataProvider = "restTimeSlot", dataProviderClass = AvailDP.class, description = "Precondition : Create restaurant slots, restaurant holiday slots and then get the availability of the restaurant from SRS"+
            "Testcase :  Get the timeline of the restaurant for the PLAN entity type", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void timelinePLANIntegrationTest(String data) throws Exception {
        processor = srsHelper.createNormalRestaurantSlot(data);
        testcaseAvailHelper.restaurantSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(restaurant_id);

        processor = availServiceHelper.plan(AvailServiceConstant.plan);

        processor = availServiceHelper.timeline(restaurant_id, AvailServiceConstant.PLAN);
        testcaseAvailHelper.availTimelinePLANCheck(processor);

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PLAN_}PLAN01");
    }

    @Test(dataProvider = "restTimeSlot", dataProviderClass = AvailDP.class, description = "Precondition : Create restaurant slots, restaurant holiday slots and then get the availability of the restaurant from SRS"+
            "Testcase :  Get the timeline of the restaurant for the PRODUCT entity type", groups = {"Sanity", "Regression", "Neha Pandey"})
    public void timelinePRODUCTIntegrationTest(String data) throws Exception {
        processor = srsHelper.createNormalRestaurantSlot(data);
        testcaseAvailHelper.restaurantSlotServiceCheck(processor);
        SoftAssert softAssert = new SoftAssert();
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        processor = availServiceHelper.store(AvailServiceConstant.store);

        processor = availServiceHelper.product(AvailServiceConstant.product);

        processor = availServiceHelper.timeline(AvailServiceConstant.store, AvailServiceConstant.PRODUCT);
        testcaseAvailHelper.availTimelinePRODUCTCheck(processor);

        redisHelper.deleteKey("cmsredis2", 0, "{STORE_}9990");
        redisHelper.deleteKey("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
    }

    @Test(dataProvider = "citySlot", dataProviderClass = AvailDP.class, description = "Check the integration of -->\n"+
            "-City with Area\n"+
            "-Area with Stores mapped\n"+
            "-Store with Products mapped\n"+
            "-Store with Plans mapped", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest0001(String data) throws Exception {
        processor = srsHelper.citySlotCreationService(data);
        SoftAssert softAssert = new SoftAssert();
        String city_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        softAssert.assertAll();

        AreaSlotPOJO areaSlotPOJO = new AreaSlotPOJO().setDefault(160,0,2359,"MON");
        processor = srsHelper.areaSlotCreation(areaSlotPOJO);
        String area_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        softAssert.assertAll();

        RestaurantSlotPOJO restaurantSlotPOJO = new RestaurantSlotPOJO().setDefault(VendorConstant.close_time, VendorConstant.MON, VendorConstant.open__time, "9990");
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        List list = new ArrayList();
        list.add("MO");
        List list1 = new ArrayList();
        SlotCreate slotCreate = new SlotCreate();
        slotCreate.setData("PLAN",1,list,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.plan).withStore_id(9990);
        list1.add(slotCreate);
        processor = srsHelper.createItemSlotService(list1);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        softAssert.assertAll();

        List list2 = new ArrayList();
        list.add("MO");
        List list3 = new ArrayList();
        slotCreate.setData("PRODUCT",1,list2,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.product).withStore_id(9990);
        list3.add(slotCreate);
        processor = srsHelper.createItemSlotService(list1);
        String entity_id1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id1);
        softAssert.assertAll();

        processor = availServiceHelper.city(city_id);
        Thread.sleep(3000);

        processor = availServiceHelper.area(area_id);
        Thread.sleep(3000);

        processor = availServiceHelper.store(restaurant_id);
        Thread.sleep(3000);

        processor = availServiceHelper.plan(entity_id);
        Thread.sleep(3000);

        processor = availServiceHelper.product(entity_id1);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();

        Object key = redisHelper.getValue("cmsredis2", 0, "{CITY_}3");
        logger.info(key + "-----------key------------");

        Object key1 = redisHelper.getValue("cmsredis2", 0, "{AREA_}160");
        logger.info(key1 + "-----------key------------");

        Object key2 = redisHelper.getValue("cmsredis2", 0, "{STORE_}9990");
        logger.info(key2 + "-----------key------------");

        Object key3 = redisHelper.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key3 + "-----------key------------");

        Object key4 = redisHelper.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key4 + "-----------key------------");

        redisHelper.deleteKey("cmsredis2",0,"{CITY_}3");
        redisHelper.deleteKey("cmsredis2",0,"{AREA_}160");
        redisHelper.deleteKey("cmsredis2",0,"{STORE_}9990");
        redisHelper.deleteKey("cmsredis2",0,"{PLAN_}PLAN01");
        redisHelper.deleteKey("cmsredis2",0,"{PRODUCT_}PPRODUCT01");

        logger.info(commonAvailHelper.deleteCitySlot(city_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteAreaSlot(area_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteRestSlot(restaurant_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id,restaurant_id));
        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id1,restaurant_id));
    }


    @Test(dataProvider = "citySlot", dataProviderClass = AvailDP.class, description = "Check the integration of: \n"+
            "-City slot creation API from SRS with city API of avail service along with events persisting to cms.avail.city.changes and redis key CITY_\n"+
            "-Area slot creation API integrated with Area API of avail service along with events mapped to cms.avail.area.changes and redis key AREA_\n"+
            "-Restaurant slot creation API integrated with Store API of avail service along with events mapped to cms.avail.store.changes and redis key STORE_\n"+
            "-Item slot service for Plan integrated with Plan API with events mapped to cms.daily_restaurant_product_avail.change and redis key PLAN_\n"+
            "-Item slot service for Product integrated with Plan API with events mapped to cms.daily_restaurant_plan_avail.change and redis key PRODUCT_\n"+
            "-DB mapping for city with city_slots table\n"+
            "-DB mapping for area with area_slots table\n"+
            "-DB mapping for store with restaurant_slots table\n"+
            "-DB mapping for plan with item_slots table\n"+
            "-DB mapping for product with item_slots table", groups = {"Sanity" , "Regression" , "Neha Pandey"})
    public void integrationTest0002(String data) throws Exception {
        processor = srsHelper.citySlotCreationService(data);
        SoftAssert softAssert = new SoftAssert();
        String city_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        softAssert.assertAll();

        AreaSlotPOJO areaSlotPOJO = new AreaSlotPOJO().setDefault(160,0,2359,"MON");
        processor = srsHelper.areaSlotCreation(areaSlotPOJO);
        String area_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        softAssert.assertAll();

        RestaurantSlotPOJO restaurantSlotPOJO = new RestaurantSlotPOJO().setDefault(VendorConstant.close_time, VendorConstant.MON, VendorConstant.open__time, "9990");
        processor = vendorHelper.createNormalRestaurantSlot(restaurantSlotPOJO);
        String restaurant_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        softAssert.assertAll();

        List list = new ArrayList();
        list.add("MO");
        List list1 = new ArrayList();
        SlotCreate slotCreate = new SlotCreate();
        slotCreate.setData("PLAN",1,list,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.plan).withStore_id(9990);
        list1.add(slotCreate);
        processor = srsHelper.createItemSlotService(list1);
        String entity_id = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        softAssert.assertAll();

        List list2 = new ArrayList();
        list.add("MO");
        List list3 = new ArrayList();
        slotCreate.setData("PRODUCT",1,list2,"DAILY", CMSDailyContants.start_time, CMSDailyContants.end_time,
                CMSDailyHelper.createEPOCforDays(0), CMSDailyHelper.createEPOCforDays(2),true).withEntity_id(AvailServiceConstant.product).withStore_id(9990);
        list3.add(slotCreate);
        processor = srsHelper.createItemSlotService(list1);
        String entity_id1 = String.valueOf(processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id1);
        softAssert.assertAll();

        processor = availServiceHelper.city(city_id);
        Thread.sleep(3000);

        processor = availServiceHelper.area(area_id);
        Thread.sleep(3000);

        processor = availServiceHelper.store(restaurant_id);
        Thread.sleep(3000);

        processor = availServiceHelper.plan(entity_id);
        Thread.sleep(3000);

        processor = availServiceHelper.product(entity_id1);
        Thread.sleep(3000);

        RedisHelper redisHelper = new RedisHelper();

        Object key = redisHelper.getValue("cmsredis2", 0, "{CITY_}3");
        logger.info(key + "-----------key------------");

        Object key1 = redisHelper.getValue("cmsredis2", 0, "{AREA_}160");
        logger.info(key1 + "-----------key------------");

        Object key2 = redisHelper.getValue("cmsredis2", 0, "{STORE_}9990");
        logger.info(key2 + "-----------key------------");

        Object key3 = redisHelper.getValue("cmsredis2", 0, "{PLAN_}PLAN01");
        logger.info(key3 + "-----------key------------");

        Object key4 = redisHelper.getValue("cmsredis2", 0, "{PRODUCT_}PPRODUCT01");
        logger.info(key4 + "-----------key------------");

        redisHelper.deleteKey("cmsredis2",0,"{CITY_}3");
        redisHelper.deleteKey("cmsredis2",0,"{AREA_}160");
        redisHelper.deleteKey("cmsredis2",0,"{STORE_}9990");
        redisHelper.deleteKey("cmsredis2",0,"{PLAN_}PLAN01");
        redisHelper.deleteKey("cmsredis2",0,"{PRODUCT_}PPRODUCT01");

        logger.info(commonAvailHelper.deleteCitySlot(city_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteAreaSlot(area_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteRestSlot(restaurant_id,"MON","{\"source\":\"Neha-vendor-automation\",\"meta\":{}}"));
        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id,restaurant_id));
        logger.info(commonAvailHelper.deleteSlotInfoEntry(entity_id1,restaurant_id));
    }





}
