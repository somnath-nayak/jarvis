package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import framework.gameofthrones.Cersei.ToolBox;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProductMetaTest extends CMSDailyDP {

    CMSDailyHelper helper=new CMSDailyHelper();
    List<String> listspin;
    List<String> listprodcut;
    List<String> price1=new ArrayList<>();
    SoftAssert asser=new SoftAssert();

    @Test(dataProvider = "productavailmeta",description = "This test will get all available Meal meta details",priority = 0)
    public void mealMeta(String starttime, String endtime, List pids, String storeid) throws Exception{
        String res= helper.prodctListingMeta(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        asser.assertEquals(storeid,storeidres);
        String spinmeta= JsonPath.read(res,"$.data..variations..spin").toString().replace("[","").replace("]","").replace("\"", "");
        listspin = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        asser.assertTrue(listspin.size()>0,"Data Coming as blank");
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        listprodcut = Arrays.asList(productmeta.split("\\s*,\\s*"));
        asser.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        asser.assertAll();
    }

    @Test(dataProvider = "productavailmeta",description = "This test will verify data for a single product and store",priority = 1)
    public void mealMetealWithStoreandSingleProductId(String starttime, String endtime, List pids, String storeid) throws Exception{
        asser.assertTrue(helper.listingCheckMeal(Integer.parseInt(storeid)));
        List<String> li=new ArrayList<>();
        li.add(listprodcut.get(0));
        String res= helper.prodctListingMeta(starttime, endtime, li, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        asser.assertEquals(storeid,storeidres);
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> listprodcutactual = Arrays.asList(productmeta.split("\\s*,\\s*"));
        asser.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        ToolBox tb=new ToolBox();
        asser.assertTrue(tb.compareLists(listprodcutactual,li),"List Data Not matching");
        asser.assertAll();

    }

    @Test(dataProvider = "productavailmeta",description = "This test will verify price of all spin in available Meal and compare to actual price")
    public void mealMetaPriceVerify(String starttime, String endtime, List pids, String storeid) throws Exception {
        String res = helper.prodctListingMeta(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        asser.assertEquals(storeid, storeidres);
        String spinmeta = JsonPath.read(res, "$.data..variations..spin").toString().replace("[", "").replace("]", "").replace("\"", "");
        listspin = Arrays.asList(spinmeta.split("\\s*,\\s*"));
        asser.assertTrue(listspin.size() > 0, "Data Coming as blank");
        String it = JsonPath.read(res, "$.data..variations..price.price").toString().replace("\"","").replace("[","").replace("]","");
        asser.assertNotNull(it,"Pricing showing null");
        List<String> pricemeta = Arrays.asList(it.split("\\s*,\\s*"));
        asser.assertTrue(pricemeta.size() > 0, "Data Coming as blank");
        for (int i = 0; i < listspin.size(); i++) {
            String key = storeid + "-" + listspin.get(i);
            String priceresponse = helper.getPricing(key, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
            String pri=JsonPath.read(priceresponse,"$.data..pricing_construct.price_list.list_price").toString().replace("[","").replace("]","");
            Double a=Double.parseDouble(pri)*100;
            price1.add(a.toString().replace(".0",""));
        }
        asser.assertTrue(new ToolBox().compareLists(price1,pricemeta),"Price is not Matching");
        asser.assertAll();
    }

    @Test(dataProvider = "productavailmeta",description = "This test will verify all fields data",priority = 1)
    public void mealMetaFeildsVerify(String starttime, String endtime, List pids, String storeid) throws Exception{
        List<String> li=new ArrayList<>();
        li.add(listprodcut.get(0));
        String res= helper.prodctListingMeta(starttime, endtime, li, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String storeidres = JsonPath.read(res, "$.data[0]..store_id").toString().replace("[", "").replace("]", "").replace("\"", "");
        asser.assertEquals(storeid,storeidres);
        String productmeta= JsonPath.read(res,"$.data..product_id").toString().replace("[","").replace("]","").replace("\"", "");
        List<String> listprodcutactual = Arrays.asList(productmeta.split("\\s*,\\s*"));
        asser.assertTrue(listprodcut.size()>0,"Data Coming as blank");
        ToolBox tb=new ToolBox();
        asser.assertTrue(tb.compareLists(listprodcutactual,li),"List Data Not matching");
        String display_name=JsonPath.read(res, "$.data[0]..meta.display_name").toString().replace("[", "").replace("]", "").replace("\"", "");
        String short_description=JsonPath.read(res, "$.data[0]..meta.short_description").toString().replace("[", "").replace("]", "").replace("\"", "");
        String is_veg=JsonPath.read(res, "$.data[0]..meta.is_veg").toString().replace("[", "").replace("]", "").replace("\"", "");
        String cuisne=JsonPath.read(res, "$.data[0]..meta.cuisine").toString().replace("[", "").replace("]", "").replace("\"", "");
        String prep_stylee=JsonPath.read(res, "$.data[0]..meta.preparation_style").toString().replace("[", "").replace("]", "").replace("\"", "");
        String spice_level=JsonPath.read(res, "$.data[0]..meta.spice_level").toString().replace("[", "").replace("]", "").replace("\"", "");
        String variations=JsonPath.read(res, "$.data..variations").toString().replace("[", "").replace("]", "").replace("\"", "");
        asser.assertNotNull(display_name,"Display name showing null");
        asser.assertNotNull(short_description,"Short description showing null");
        asser.assertNotNull(is_veg,"Veg prefernece showing null");
        asser.assertNotNull(cuisne,"Cusine showing null");
        asser.assertNotNull(prep_stylee,"prep stylee showing null");
        asser.assertNotNull(spice_level,"Spicy Level showing null");
        asser.assertNotNull(variations,"Varitions showing null");
        asser.assertAll();

    }


    @Test(dataProvider = "meallistingnegative",description = "This test will verify negative cases for meal meta with different combination of data")
    public void availableMealAvailNegative(String starttime, String endtime, List pids, String storeid) throws Exception{
        String res= helper.prodctListingMeta(starttime, endtime, pids, Integer.parseInt(storeid)).ResponseValidator.GetBodyAsText();
        String data=JsonPath.read(res,"$.data");
        if(data==null||data.equals("Start time cannot be less then End time")||data.equals("Not able to parse StartTime")||data.equals("Not able to parse EndTime")||data.equals("Endtime cannot be null if Starttime provided")){
            asser.assertTrue(true,"Data should not be shown for invalid cases"); }
        else asser.assertTrue(false,"Data should not be shown for invalid cases");
        asser.assertAll();
    }
}
