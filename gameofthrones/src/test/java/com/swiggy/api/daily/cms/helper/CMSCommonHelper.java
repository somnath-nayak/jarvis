package com.swiggy.api.daily.cms.helper;

import com.jayway.jsonpath.JsonPath;
import com.redis.S;
import com.swiggy.api.daily.cms.availabilityService.helper.AvailServiceHelper;
import com.swiggy.api.daily.cms.pojo.PricingService.PricingDaily;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Reporter;
import org.testng.annotations.Test;
import java.io.IOException;
import java.util.*;

public class CMSCommonHelper {

    PricingDaily pd=new PricingDaily();
    CMSDailyHelper dp=new CMSDailyHelper();
    ObjectMapper mp=new ObjectMapper();
    Initialize got=new Initialize();
    AvailServiceHelper helper=new AvailServiceHelper();

    /**
     * Please enter type as Product Or Plan
     * @param storeid
     * @param spinid
     * @param price
     * @param type
     * @return
     */

    public String setPrice(String storeid,String spinid,Integer price,String type){
        String key=storeid+"-"+spinid;
        if(type.equalsIgnoreCase("product")){
        String getprice=dp.getPricing(key,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        String productid= JsonPath.read(getprice,"$.data..meta.product_id").toString().replace("[","").replace("]","").replace("\"","");
        String skuid= JsonPath.read(getprice,"$.data..meta.sku_id").toString().replace("[","").replace("]","").replace("\"","");
        pd.setData(key,productid,skuid,spinid,storeid,price,price,price,0,2,"INR",0,1,true,0,2);
            List li=new ArrayList();
            li.add(pd);
            try {
                String payload[]={mp.writeValueAsString(li)};
                HashMap header = new LinkedHashMap();
                header.put("service-line", CMSDailyContants.service_line_meal);
                GameOfThronesService service = new GameOfThronesService("cmsdailypricingservice", "createprice", got);
                new Processor(service, header, payload, null);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        else if(type.equalsIgnoreCase("plan")){
            String getprice=dp.getPricing(key,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
            String productid= JsonPath.read(getprice,"$.data..meta.product_id").toString().replace("[","").replace("]","").replace("\"","");
            String skuid= JsonPath.read(getprice,"$.data..meta.sku_id").toString().replace("[","").replace("]","").replace("\"","");
            pd.setData(key,productid,skuid,spinid,storeid,price,price,price,0,1,"INR",0,2,true,0,2);
            List li=new ArrayList();
            li.add(pd);
            try {
                String payload[]={mp.writeValueAsString(li)};
                HashMap header = new LinkedHashMap();
                header.put("service-line", CMSDailyContants.service_line_plan);
                GameOfThronesService service = new GameOfThronesService("cmsdailypricingservice", "createprice", got);
                new Processor(service, header, payload, null);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
            return "Price changed successfully";

    }

    /***
     *
     * @param spinid
     * @param entitytype
     * @return
     */
    public String setInventoryOutOfStock(String spinid,String entitytype){
        String msg="";
        if(entitytype.equalsIgnoreCase("product")){
            dp.updateMaxCap(spinid,null,null,0,0,CMSDailyContants.service_line_meal);
            msg="Successfully Updated Inventory as Out of Stock for Product"; }
        else if (entitytype.equalsIgnoreCase("plan")){
            String response=dp.fetchProdctIdfromSpin(spinid).ResponseValidator.GetBodyAsText();
            String pid=JsonPath.read(response,"$.product_id").toString().replace("[", "").replace("]", "").replace("\"", "");
            dp.updateMaxCap(null,pid,null,0,0,CMSDailyContants.service_line_plan);
        msg="Successfully Updated Inventory as Out of Stock for Plan";}
        return msg;
    }

    /***
     *
     * @param spinid
     * @param inventorycount
     * @param entitytype
     * @return
     */
    public String setInventoryInStock(String spinid,Integer inventorycount,String entitytype){
        String msg="";
        if(entitytype.equalsIgnoreCase("product")){
            dp.updateMaxCap(spinid,null,null,inventorycount,0,CMSDailyContants.service_line_meal);
            msg="Successfully Updated Inventory as in Stock for Product"; }
        else if (entitytype.equalsIgnoreCase("plan")){
            String response=dp.fetchProdctIdfromSpin(spinid).ResponseValidator.GetBodyAsText();
            String pid=JsonPath.read(response,"$.product_id").toString().replace("[", "").replace("]", "").replace("\"", "");
            dp.updateMaxCap(null,pid,null,inventorycount,0,CMSDailyContants.service_line_plan);
            msg="Successfully Updated Inventory as in Stock for Plan";}
        return msg;
    }

    /***
     *
     * @param storeid
     * @param starttime
     * @param endtime
     * @return
     */

    public List<String> fetchAllInStockSpinsForProduct(Integer storeid,Long starttime,Long endtime){
        ArrayList<String> as=new ArrayList<>();
        if(starttime==null && endtime==null){
            starttime=CMSDailyHelper.createEPOCforCurrent();
            endtime=CMSDailyHelper.createEPOCforCurrent();
        }
        try {
            String response=dp.prodctListingAvail(Long.toString(starttime),Long.toString(endtime),null,storeid).ResponseValidator.GetBodyAsText();
            String inventorystatus=JsonPath.read(response, "$.data..inventory.in_stock").toString().replace("[", "").replace("]", "");
            List<String> inventorys= Arrays.asList(inventorystatus.split("\\s*,\\s*"));
            for(int i=0;i<inventorys.size();i++){
                String spin=JsonPath.read(response, "$.data["+i+"].variations..spin").toString().replace("[", "").replace("]", "").replace("\"","");
                if(inventorys.get(i).equals("true")){
                    as.add(spin);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }

    /***
     * start time and endtime should be in 24 hr format
     * @param storeid
     * @param startdate(dd-mm-yyyy)
     * @param starttime(hh:mm)
     * @param endtime(hh:mm)
     * @return
     */


    public List<String> fetchAllInStockSpinsForPlan(String storeid,String startdate,String starttime,String endtime){
        ArrayList<String> as=new ArrayList<>();
        ArrayList<String> as1=new ArrayList<>();
        Object[][] object = new Object[][]{
                {storeid}
        };

        try {
            String response=dp.planAvailabilityAndInventory("AVAILABILITY,INVENTORY",startdate,starttime,endtime,null,object).ResponseValidator.GetBodyAsText();
            String inventorystatus=JsonPath.read(response, "$.data..variations[0].inventory.in_stock").toString().replace("[", "").replace("]", "");
            List<String> inventorys= Arrays.asList(inventorystatus.split("\\s*,\\s*"));
            for(int i=0;i<inventorys.size();i++){
                String spin=JsonPath.read(response, "$.data["+i+"].product_id").toString().replace("[", "").replace("]", "").replace("\"","");
                if(inventorys.get(i).equals("true")){
                    as.add(spin);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=0;i<as.size();i++){
            String res=dp.fetchSpinFromProdctId(as.get(i)).ResponseValidator.GetBodyAsText();
            String spins=JsonPath.read(res, "$..spin").toString().replace("[", "").replace("]", "").replace("\"","");
            List<String> inventorys= Arrays.asList(spins.split("\\s*,\\s*"));
            as1.addAll(inventorys);


        }
        return as1;
    }

    /***
     *
     * @param storeid
     * @param starttime
     * @param endtime
     * @return
     */

    public List<String> fetchAllAvailableSpinsForProduct(Integer storeid,Long starttime,Long endtime){
        ArrayList<String> as=new ArrayList<>();
        if(starttime==null && endtime==null){
            starttime=CMSDailyHelper.createEPOCforCurrent();
            endtime=CMSDailyHelper.createEPOCforCurrent();
        }
        try {
            String response=dp.prodctListingAvail(Long.toString(starttime),Long.toString(endtime),null,storeid).ResponseValidator.GetBodyAsText();
            String availablestatus=JsonPath.read(response, "$.data..availability.is_avail").toString().replace("[", "").replace("]", "");
            List<String> availables= Arrays.asList(availablestatus.split("\\s*,\\s*"));
            for(int i=0;i<availables.size();i++){
                String spin=JsonPath.read(response, "$.data["+i+"].variations..spin").toString().replace("[", "").replace("]", "").replace("\"","");
                if(availables.get(i).equals("true")){
                    as.add(spin);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return as;
    }


    /***
     *
     * @param storeid
     * @param starttime
     * @param endtime
     * @return
     */

    public List<String> fetchAllAvailableSpinsForPlan(String storeid,String startdate,String starttime,String endtime){
        ArrayList<String> as=new ArrayList<>();
        ArrayList<String> as1=new ArrayList<>();
        Object[][] object = new Object[][]{
                {storeid}
        };

        try {
            String response=dp.planAvailabilityAndInventory("AVAILABILITY,INVENTORY",startdate,starttime,endtime,null,object).ResponseValidator.GetBodyAsText();
            String inventorystatus=JsonPath.read(response, "$.data..variations[0].availability.is_avail").toString().replace("[", "").replace("]", "");
            List<String> inventorys= Arrays.asList(inventorystatus.split("\\s*,\\s*"));
            for(int i=0;i<inventorys.size();i++){
                String spin=JsonPath.read(response, "$.data["+i+"].product_id").toString().replace("[", "").replace("]", "").replace("\"","");
                if(inventorys.get(i).equals("true")){
                    as.add(spin);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(int i=0;i<as.size();i++){
            String res=dp.fetchSpinFromProdctId(as.get(i)).ResponseValidator.GetBodyAsText();
            String spins=JsonPath.read(res, "$..spin").toString().replace("[", "").replace("]", "").replace("\"","");
            List<String> inventorys= Arrays.asList(spins.split("\\s*,\\s*"));
            as1.addAll(inventorys);


        }
        return as1;
    }

    /**
     *
     * @param spinid
     * @return Spin Information
     */

    public String spinMeta(String spinid){
        if(spinid==null||spinid.equals("")){
            return "Please Enter SPIN";
        }
        else {
        Processor pr=dp.fetchProdctIdfromSpin(spinid.toUpperCase());
        return pr.ResponseValidator.GetBodyAsText();}
    }

    /***
     *
     * @param storeid
     * @param starttime(EPOC)
     * @param endtime(EPOC)
     * @return List of SPINs which is in-stock and available,starttime and endtime is mandatory if you want to search for particular time.
     */

    public List<String> fetchAllAvailableInStockSpinsForProduct(Integer storeid,Long starttime,Long endtime){
        ArrayList<String> as=new ArrayList<>();
        if(starttime==null && endtime==null){
            starttime=CMSDailyHelper.createEPOCforCurrent();
            endtime=CMSDailyHelper.createEPOCforCurrent();
        }
        try {
            String response=dp.prodctListingAvail(Long.toString(starttime),Long.toString(endtime),null,storeid).ResponseValidator.GetBodyAsText();
            String availablestatus=JsonPath.read(response, "$.data..availability.is_avail").toString().replace("[", "").replace("]", "");
            String inventorystatus=JsonPath.read(response, "$.data..inventory.in_stock").toString().replace("[", "").replace("]", "");
            List<String> availables= Arrays.asList(availablestatus.split("\\s*,\\s*"));
            List<String> stocks= Arrays.asList(inventorystatus.split("\\s*,\\s*"));
            if(availables.size()==stocks.size()){
            for(int i=0;i<availables.size();i++){
                String spin=JsonPath.read(response, "$.data["+i+"].variations..spin").toString().replace("[", "").replace("]", "").replace("\"","");
                if(availables.get(i).equals("true") && stocks.get(i).equals("true")){
                    as.add(spin);
                }
            }}
            else {
                Reporter.log("No Data is Matching");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return as;
    }

    /***
     *
     * @param storeid
     * @return
     */

    public String getAddonData(String storeid) {
        String response = dp.getAddon(storeid).ResponseValidator.GetBodyAsText();
        return response;
    }

    /**
     * This function Will set avail true for Product/Plan avail false based on entity type(PRODUCT,PLAN)
     * @param store
     * @param spin
     * @param entitytype
     * @return
     */

    public boolean setproductavailFalse(String store,String spin,String entitytype){
        if(entitytype.equalsIgnoreCase("PRODUCT")) {
            String entityid = store + "-" + spin;
            String response = dp.getSlotbyEntityID(entityid, "false", 0).ResponseValidator.GetBodyAsText();
            String slotid = JsonPath.read(response, "$.data.slot_info_list..id").toString().replace("[", "").replace("]", "").replace("\"","");
            String productid = JsonPath.read(response, "$.data.meta.product_id").toString().replace("[", "").replace("]", "").replace("\"","");
            String skuid = JsonPath.read(response, "$.data.meta.sku_id").toString().replace("[", "").replace("]", "").replace("\"","");
            String freq = JsonPath.read(response, "$.data.slot_info_list..recurrence_pattern.frequency").toString().replace("[", "").replace("]", "").replace("\"","");
            dp.updateAvailSlot(productid, store, skuid, spin, Long.parseLong(slotid), "PRODUCT", 1, null, freq, "23:58:00", "23:58:58", CMSDailyHelper.createEPOCforCurrent(), null, true);
            return true;
        }
        else if(entitytype.equalsIgnoreCase("PLAN")) {
            String res=dp.fetchProdctIdfromSpin(spin).ResponseValidator.GetBodyAsText();
            String pid = JsonPath.read(res, "$.product_id").toString().replace("[", "").replace("]", "");
            String entityid = store + "-" + pid;
            String response = dp.getSlotbyEntityID(entityid, "false", 0).ResponseValidator.GetBodyAsText();
            String slotid = JsonPath.read(response, "$.data.slot_info_list..id").toString().replace("[", "").replace("]", "").replace("\"","");
            String freq = JsonPath.read(response, "$.data.slot_info_list..recurrence_pattern.frequency").toString().replace("[", "").replace("]", "").replace("\"","");
            dp.updateAvailSlot(pid, store, null, null, Long.parseLong(slotid), "PLAN", 1, null, freq, "23:58:00", "23:58:58", CMSDailyHelper.createEPOCforCurrent(), null, true);
            return true;
        }
        return false;
    }

    /**
     * This function Will set avail true for Product/Plan avail false based on entity type(PRODUCT,PLAN)
     * @param store
     * @param spin
     * @param entitytype
     * @return
     */

    public boolean setproductavailTrue(String store,String spin,String entitytype){
        if(entitytype.equalsIgnoreCase("PRODUCT")) {
            String entityid = store + "-" + spin;
            String response = dp.getSlotbyEntityID(entityid, "false", 0).ResponseValidator.GetBodyAsText();
            String slotid = JsonPath.read(response, "$.data.slot_info_list..id").toString().replace("[", "").replace("]", "").replace("\"","");
            String productid = JsonPath.read(response, "$.data.meta.product_id").toString().replace("[", "").replace("]", "").replace("\"","");
            String skuid = JsonPath.read(response, "$.data.meta.sku_id").toString().replace("[", "").replace("]", "").replace("\"","");
            String freq = JsonPath.read(response, "$.data.slot_info_list..recurrence_pattern.frequency").toString().replace("[", "").replace("]", "").replace("\"","");
            dp.updateAvailSlot(productid, store, skuid, spin, Long.parseLong(slotid), "PRODUCT", 1, null, freq, "00:00:00", "23:59:00", CMSDailyHelper.createEPOCforCurrent(), null, true);
            return true;
        }
        else if(entitytype.equalsIgnoreCase("PLAN")) {
            String res=dp.fetchProdctIdfromSpin(spin).ResponseValidator.GetBodyAsText();
            String pid = JsonPath.read(res, "$.product_id").toString().replace("[", "").replace("]", "");
            String entityid = store + "-" + pid;
            String response = dp.getSlotbyEntityID(entityid, "false", 0).ResponseValidator.GetBodyAsText();
            String slotid = JsonPath.read(response, "$.data.slot_info_list..id").toString().replace("[", "").replace("]", "").replace("\"","");
            String freq = JsonPath.read(response, "$.data.slot_info_list..recurrence_pattern.frequency").toString().replace("[", "").replace("]", "").replace("\"","");
            dp.updateAvailSlot(pid, store, null, null, Long.parseLong(slotid), "PLAN", 1, null, freq, "00:00:00", "23:59:00", CMSDailyHelper.createEPOCforCurrent(), null, true);
            return true;
        }
        return false;
    }

}
