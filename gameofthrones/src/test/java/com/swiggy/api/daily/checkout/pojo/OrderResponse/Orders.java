package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Orders
{
    private String coupon_code;

    private String updated_at;

    private Order_jobs[] order_jobs;

    private String order_metadata;

    private String created_at;

    private String payment_info;

    private String customer_id;

    private String order_id;

    private String order_type;

    public String getCoupon_code ()
{
    return coupon_code;
}

    public void setCoupon_code (String coupon_code)
    {
        this.coupon_code = coupon_code;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public Order_jobs[] getOrder_jobs ()
    {
        return order_jobs;
    }

    public void setOrder_jobs (Order_jobs[] order_jobs)
    {
        this.order_jobs = order_jobs;
    }

    public String getOrder_metadata ()
{
    return order_metadata;
}

    public void setOrder_metadata (String order_metadata)
    {
        this.order_metadata = order_metadata;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getPayment_info ()
{
    return payment_info;
}

    public void setPayment_info (String payment_info)
    {
        this.payment_info = payment_info;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getOrder_id ()
    {
        return order_id;
    }

    public void setOrder_id (String order_id)
    {
        this.order_id = order_id;
    }

    public String getOrder_type ()
    {
        return order_type;
    }

    public void setOrder_type (String order_type)
    {
        this.order_type = order_type;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [coupon_code = "+coupon_code+", updated_at = "+updated_at+", order_jobs = "+order_jobs+", order_metadata = "+order_metadata+", created_at = "+created_at+", payment_info = "+payment_info+", customer_id = "+customer_id+", order_id = "+order_id+", order_type = "+order_type+"]";
    }
}
