package com.swiggy.api.daily.cms.availabilityService.helper;

import framework.gameofthrones.JonSnow.Processor;
import org.testng.asserts.SoftAssert;

public class TestcaseAvailHelper {

    public void cityCheck(Processor city){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String id = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertEquals(id, AvailServiceConstant.city);
        String statusCode = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void cityFailureCheck(Processor city){
        SoftAssert softAssert =  new SoftAssert();
        String statusCode =  String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void areaCheck(Processor area){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String id = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertEquals(id, AvailServiceConstant.area);
        String statusCode = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void areaFailureCheck(Processor city){
        SoftAssert softAssert = new SoftAssert();
        String statusCode =  String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void storeSuccessCheck(Processor store){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(store.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String id = String.valueOf(store.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertEquals(id, AvailServiceConstant.store);
        String statusCode = String.valueOf(store.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void storeFailureCheck(Processor store){
        SoftAssert softAssert = new SoftAssert();
        String statusCode =  String.valueOf(store.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void productSuccessCheck(Processor product){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(product.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String id = String.valueOf(product.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertEquals(id, AvailServiceConstant.product);
        String statusCode = String.valueOf(product.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void productFailureCheck(Processor product){
        SoftAssert softAssert = new SoftAssert();
        String statusCode =  String.valueOf(product.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void planSuccessCheck(Processor plan){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String id = String.valueOf(plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
        softAssert.assertEquals(id, AvailServiceConstant.plan);
        String statusCode = String.valueOf(plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertNotNull(statusCode);
        softAssert.assertAll();
    }

    public void planFailureCheck(Processor plan) {
        SoftAssert softAssert = new SoftAssert();
        String statusCode = String.valueOf(plan.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertEquals(statusCode, "1");
        softAssert.assertAll();
    }

    public void availTimelinePLANCheck(Processor timeline){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String entity_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String type = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        softAssert.assertNotNull(type);
        String meta = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta"));
        softAssert.assertNotNull(meta);
        String store_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.store_id"));
        softAssert.assertNotNull(store_id);
        String product_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.product_id"));
        softAssert.assertNotNull(product_id);
        String sku_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.sku_id"));
        softAssert.assertNotNull(sku_id);
        String parentIds = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.parentIds"));
        softAssert.assertNotNull(parentIds);
        String store_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.meta.store_id"));
        softAssert.assertNotNull(store_id1);
        String product_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.product_id"));
        softAssert.assertNotNull(product_id1);
        String sku_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.sku_id"));
        softAssert.assertNotNull(sku_id1);
        String slot_timings = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.slot_timings"));
        softAssert.assertNotNull(slot_timings);
        softAssert.assertAll();
    }

    public void availTimelinePRODUCTCheck(Processor timeline){
        SoftAssert softAssert =  new SoftAssert();
        String data =  String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data"));
        softAssert.assertNotNull(data);
        String entity_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].entity_id"));
        softAssert.assertNotNull(entity_id);
        String type = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].type"));
        softAssert.assertNotNull(type);
        String meta = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta"));
        softAssert.assertNotNull(meta);
        String store_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.store_id"));
        softAssert.assertNotNull(store_id);
        String product_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.product_id"));
        softAssert.assertNotNull(product_id);
        String sku_id = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.sku_id"));
        softAssert.assertNotNull(sku_id);
        String parentIds = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.parentIds"));
        softAssert.assertNotNull(parentIds);
        String store_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.meta.store_id"));
        softAssert.assertNotNull(store_id1);
        String product_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.product_id"));
        softAssert.assertNotNull(product_id1);
        String sku_id1 = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].meta.sku_id"));
        softAssert.assertNotNull(sku_id1);
        String slot_timings = String.valueOf(timeline.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].data.slot_timings"));
        softAssert.assertNotNull(slot_timings);
        softAssert.assertAll();
    }

    public void citySlotServiceCheck(Processor city) {
        SoftAssert softAssert =  new SoftAssert();
        String status = String.valueOf(city.ResponseValidator.GetNodeValueAsInt("$.status"));
        softAssert.assertNotNull(status);
        String code = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
        softAssert.assertEquals(code, "SUCCESS");
        String city_id = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertNotNull(city_id);
        String open_time = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.open_time"));
        softAssert.assertNotNull(open_time);
        String close_time = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.close_time"));
        softAssert.assertNotNull(close_time);
        String day = String.valueOf(city.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.day"));
        softAssert.assertNotNull(day);
        softAssert.assertAll();
    }

    public void areaSlotServiceCheck(Processor area){
        SoftAssert softAssert =  new SoftAssert();
        String status = String.valueOf(area.ResponseValidator.GetNodeValueAsInt("$.status"));
        softAssert.assertNotNull(status);
        String code = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
        softAssert.assertEquals(code, "SUCCESS");
        String area_id = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.area_id"));
        softAssert.assertNotNull(area_id);
        String open_time = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.open_time"));
        softAssert.assertNotNull(open_time);
        String close_time = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.close_time"));
        softAssert.assertNotNull(close_time);
        String day = String.valueOf(area.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.day"));
        softAssert.assertNotNull(day);
        softAssert.assertAll();
    }

    public void restaurantSlotServiceCheck(Processor restaurant) {
        SoftAssert softAssert =  new SoftAssert();
        String status = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsInt("$.status"));
        softAssert.assertNotNull(status);
        String code = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.code"));
        softAssert.assertEquals(code, "SUCCESS");
        String restaurant_id = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurant_id"));
        softAssert.assertNotNull(restaurant_id);
        String open_time = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.open_time"));
        softAssert.assertNotNull(open_time);
        String close_time = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.close_time"));
        softAssert.assertNotNull(close_time);
        String day = String.valueOf(restaurant.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.day"));
        softAssert.assertNotNull(day);
        softAssert.assertAll();
    }

}
