
package com.swiggy.api.daily.subscription.pojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "item_id",
    "tenure",
    "store_ids"
})
public class ItemDetail {

    @JsonProperty("item_id")
    private String itemId;
    @JsonProperty("tenure")
    private Integer tenure;
    @JsonProperty("store_ids")
    private List<Integer> storeIds = null;

    @JsonProperty("item_id")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("item_id")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public ItemDetail withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("tenure")
    public Integer getTenure() {
        return tenure;
    }

    @JsonProperty("tenure")
    public void setTenure(Integer tenure) {
        this.tenure = tenure;
    }

    public ItemDetail withTenure(Integer tenure) {
        this.tenure = tenure;
        return this;
    }

    @JsonProperty("store_ids")
    public List<Integer> getStoreIds() {
        return storeIds;
    }

    @JsonProperty("store_ids")
    public void setStoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
    }

    public ItemDetail withStoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
        return this;
    }public ItemDetail setDefaultData(){
        return this.withItemId(SubscriptionConstant.ITEM_ID)
                .withTenure(SubscriptionConstant.TENURE)
                .withStoreIds(new ArrayList<Integer>() {{add(SubscriptionConstant.STORE_IDS);}});
    }



}
