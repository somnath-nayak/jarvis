package com.swiggy.api.daily.ff.constants;

public class DailyAssignmentServiceConstants {

    String SERVICE = "omsassignmentservice";

    public static final int[] verifier_id = {46,47,48};
    public static final int verifier_role_id = 6;
    public static final int logout_new_state_id = 4;
    public static final int verifier_assignment_reason_id = 2;
    public static final int verifier_city_id = 1;

    public static final int[] daily_agent_id = {52, 53, 54};
    public static final int daily_agent_role_id = 41;
    public static final int daily_agent_assignment_reason_id = 10;
    public static final int daily_agent_city_id = 1;

    String dailyAgent = "DAILY_AGENT";

}
