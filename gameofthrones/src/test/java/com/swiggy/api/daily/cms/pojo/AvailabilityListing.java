package com.swiggy.api.daily.cms.pojo;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.daily.cms.pojo
 **/

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "store_id"
})
public class AvailabilityListing {

    @JsonProperty("store_id")
    private Integer store_id;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("store_id")
    public Integer getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(Integer store_id) {
        this.store_id = store_id;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    private void setDefaultValues(Integer storeId) {
        if (this.getStore_id() == null)
            this.setStore_id(storeId);
    }

    public AvailabilityListing build(String storeId) {
        setDefaultValues(Integer.parseInt(storeId));
        return this;
    }

    //Overload
    public AvailabilityListing build(Integer storeId) {
        setDefaultValues(storeId);
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("store_id", store_id).append("additionalProperties", additionalProperties).toString();
    }

}