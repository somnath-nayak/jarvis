package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restaurantId"
})
public class RestaurantTimeMapPOJO {

    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantTimeMapPOJO() {
    }

    /**
     *
     * @param restaurantId
     */
    public RestaurantTimeMapPOJO(String restaurantId) {
        super();
        this.restaurantId = restaurantId;
    }

    @JsonProperty("restaurantId")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public RestaurantTimeMapPOJO withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public RestaurantTimeMapPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public RestaurantTimeMapPOJO setDefault(String rest_id) {
        this.withRestaurantId(rest_id);
        return this;
    }

}
