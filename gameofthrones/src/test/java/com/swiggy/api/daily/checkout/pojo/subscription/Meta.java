package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class Meta {


    @JsonProperty("display_name")
    private String displayName;
    @JsonProperty("short_description")
    private String shortDescription;
    @JsonProperty("long_description")
    private String longDescription;
    @JsonProperty("is_veg")
    private Long isVeg;

    @JsonProperty("display_name")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("display_name")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public Meta withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    @JsonProperty("short_description")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("short_description")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Meta withShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    @JsonProperty("long_description")
    public String getLongDescription() {
        return longDescription;
    }

    @JsonProperty("long_description")
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Meta withLongDescription(String longDescription) {
        this.longDescription = longDescription;
        return this;
    }

    @JsonProperty("is_veg")
    public Long getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(Long isVeg) {
        this.isVeg = isVeg;
    }

    public Meta withIsVeg(Long isVeg) {
        this.isVeg = isVeg;
        return this;
    }
}
