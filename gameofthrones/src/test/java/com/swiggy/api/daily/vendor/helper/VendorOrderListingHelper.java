package com.swiggy.api.daily.vendor.helper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.delivery.helper.ServiceabilityHelper;
import com.swiggy.api.daily.delivery.pojo.DailyTimeSlot;
import com.swiggy.api.daily.mealSlot.helper.MealSlotHelper;
import com.swiggy.api.daily.mealSlot.pojo.MealSlotsPOJO;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.pojo.*;
import com.swiggy.api.daily.vendor.pojo.FetchOrders;
import com.swiggy.api.daily.vendor.pojo.ListPlanProductMetaPojo;
import com.swiggy.api.daily.vendor.pojo.ProductId;
import com.swiggy.api.erp.delivery.helper.DeliveryHelperMethods;
import com.swiggy.api.erp.vms.helper.RMSHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class VendorOrderListingHelper {
    
    MealSlotHelper mealSlotHelper = new MealSlotHelper();
    AssertionCheckHelper assertCheck = new AssertionCheckHelper();
    SubscriptionHelper subscriptionHelper = new SubscriptionHelper();
    private static JsonHelper jsonHelper = new JsonHelper();
    static Initialize gameOfThrone = new Initialize();
    ServiceabilityHelper serviceabilityHelper = new ServiceabilityHelper();
    DeliveryHelperMethods dhm = new DeliveryHelperMethods();
    RMSHelper rmsHelper = new RMSHelper();
    ServerTimeChangePojo serverTimeChangePojo = new ServerTimeChangePojo();
    
    
    
    //------------------------------------------------- Meal slot helpers --------------------------------------------------------------------------
    public String createMealSlots(int numberOfSlots, String[] typeOfSlot, String[] cityId, String[] startTime, String[] endTime) throws Exception {
        MealSlotsPOJO mealSlotsPOJO = new MealSlotsPOJO();
        mealSlotHelper.deleteMeal(cityId[0]);
        String creationStatus = "success";
        String status = "";
        for (int i = 0; i < numberOfSlots; i++) {
            mealSlotsPOJO.setDefaultData().withSlot(typeOfSlot[i])
                    .withCityId(cityId[0])
                    .withCreatedBy("Ankita-Vendor-Automation")
                    .withStartTime(startTime[i])
            .withEndTime(endTime[i]);
            creationStatus = "" + assertCheck.assertCheckForCreateSlot(mealSlotHelper.createMeal(mealSlotsPOJO));
            status = creationStatus;
            if (creationStatus.equalsIgnoreCase("failure")) {
                status = creationStatus = "failure";
                break;
            }
        }
        return status;
    }
    
    //----------------------------------------------- Create Subscription helpers ------------------------------------------------------------------
    public CreateSubscriptionPojo populateCreateSubscriptionData(String customerId, String orderId, String[] allStoreId, String subscriptionStartDate, String itemId[], String deliveryStartTime, String deliveryEndTime, Boolean weekendOnOff) {
        List<ItemDetail> itemDetails = new ArrayList<>();
        List<Integer> storeIds = new ArrayList<>();
        for (int i = 0; i < allStoreId.length; i++) {
            storeIds.add(i, Integer.parseInt(allStoreId[i]));
        }
        
        for (int i = 0; i < itemId.length; i++) {
            itemDetails.add(i, new ItemDetail().setDefaultData().withItemId(itemId[i]).withStoreIds(storeIds).withTenure(3));
        }
        
        
        CreateSubscriptionPojo createSubscriptionPojo = new CreateSubscriptionPojo().setDefaultData()
                                                                .withCustomerId(customerId)
                                                                .withOrderId(orderId)
                                                                .withDeliverySlot(new DeliverySlot().setDefaultData().withDeliveryStartTime(Integer.valueOf(deliveryStartTime)).withDeliveryEndTime(Integer.valueOf(deliveryEndTime)))
                                                                .withItemDetails(itemDetails)
                                                                .withScheduleDetails(new ScheduleDetails().setDefaultData().withStartDate(subscriptionStartDate).withHasWeekendService(weekendOnOff))
                                                                .withAddressDetails(new AddressDetails().setDefaultData().withEmail("test@123").withName("Ankita-Vendor-Automation"));
        return createSubscriptionPojo;
    }
    
    
    public String createSubscription(String customerId, String orderId, String[] allStoreId, String subscriptionStartDate, String itemId[], String deliveryStartTime, String deliveryEndTime, Boolean weakendOnOff) throws IOException {
        // item_id can be plan id or meal id
        CreateSubscriptionPojo payload = populateCreateSubscriptionData(customerId, orderId, allStoreId, subscriptionStartDate, itemId, deliveryStartTime, deliveryEndTime, weakendOnOff);
        String status = assertCheck.assertCheckFroCreateSubscription(subscriptionHelper.createSubscription(payload));
        return status;
    }
    
    //------------------------------------------------ Delivery helpers -------------------------------------------------------------------------
    public  List<Integer> createDeliverySlots(Integer numberOfSlots, Integer slotInterval, Integer zone, String deliveryStarttime, Integer deliveryEndTime) throws ParseException {
        List<Integer> startSlots = new ArrayList<>();
        List<Integer> endSlots = new ArrayList<>();
        List<Integer> allIds = new ArrayList<>();
        String slot = "";
        HashMap<Integer, Integer> subscriptionCreateTime = new HashMap<Integer, Integer>();
        int curTime=Integer.valueOf(getCurrentTIimeInHHMMByAddingHours(2)).intValue();
       
       // long date = getDateFromStartTime( hh, mm);
        System.out.println("subscription time    ---  " + curTime  + " time  " +Integer.valueOf(curTime).intValue() + " & " );
       
        for (int j = 0; j < numberOfSlots; j++) {
            int start = 0;
            int endSlot = 0;
            int end = 0;
            if (j == 0) {
                 startSlots.add(j, Integer.valueOf(deliveryStarttime));
                slot =addMinutesToTheGivenHHMM(deliveryStarttime, slotInterval);
                endSlots.add(j, Integer.parseInt(slot));
                System.out.println("INSIDE LOOP");
                start = ((startSlots.get(j)).intValue());
                 endSlot = ((endSlots.get(j)).intValue());
                 end = (Integer.valueOf(addMinutesToTheGivenHHMM(startSlots.get(j).toString(), 45  )));
                if(start >= curTime && endSlot== end){
                    System.out.println("INSIDE LOOP");
                    subscriptionCreateTime.put(0,startSlots.get(j)); subscriptionCreateTime.put(1,endSlots.get(j));
                }
                System.out.println("start slot   "+startSlots.get(j) + " end slot " + endSlots.get(j) + " slot " +slot);
                System.out.println(" if start slot   "+startSlots.get(j) + " end slot " + endSlots.get(j));
            } else {
               
                startSlots.add(j, Integer.valueOf(slot));
    
                slot =  addMinutesToTheGivenHHMM(slot,slotInterval);
                endSlots.add(j,Integer.valueOf(slot));
                start = ((startSlots.get(j)).intValue());
                endSlot = ((endSlots.get(j)).intValue());
                 end = (Integer.valueOf(addMinutesToTheGivenHHMM(startSlots.get(j).toString(), 45  )));
               if (start >= curTime && endSlot== end){
                    System.out.println("INSIDE 2nd LOOP");
                    subscriptionCreateTime.put(0,startSlots.get(j)); subscriptionCreateTime.put(1,endSlots.get(j));
                }
                System.out.println("start slot subs &&  "+startSlots.get(j) + " end slot subs&& " + endSlots.get(j));
               
            }
            
            
        }
        for (int i = 0; i < numberOfSlots; i++) {
            if(endSlots.get(i) < startSlots.get(i)) {
                Processor response = serviceabilityHelper.createDailySlots(zone, "2359", Integer.toString(startSlots.get(i)));
                assertCheck.assertCheckForCreateDeliverySlot(response);
                allIds.add(assertCheck.assertCheckForGetIdOfDeliveryZone(getDeliverySlotsByZoneId(zone), startSlots.get(i), 2359));
                
                response = serviceabilityHelper.createDailySlots(zone, Integer.toString(endSlots.get(i)), "0000");
                assertCheck.assertCheckForCreateDeliverySlot(response);
                allIds.add(assertCheck.assertCheckForGetIdOfDeliveryZone(getDeliverySlotsByZoneId(zone), 0000, endSlots.get(i)));
    
            } else {
                Processor response = serviceabilityHelper.createDailySlots(zone, Integer.toString(endSlots.get(i)), Integer.toString(startSlots.get(i)));
                assertCheck.assertCheckForCreateDeliverySlot(response);
                allIds.add(assertCheck.assertCheckForGetIdOfDeliveryZone(getDeliverySlotsByZoneId(zone), startSlots.get(i), endSlots.get(i)));
            }
        }
        //returns ids of all created delivery slots
        return allIds;
    }
    
    
    
    
    public HashMap<String, Integer> getCreateSubscriptionTime(HashMap<String, Integer> createSubscriptionTime){
        
        return createSubscriptionTime;
    }
    
    // update start, end time or enabled flag of a delivery slot
    public void updateDeliverySlots(int id, Integer zoneId, String startTime, String endTime, Integer enabled) {
        //Processor response= serviceabilityHelper.updateDailySlots(id,startTime,endTime);
        Processor response = updateDailySlotsWithEnabledFlag(id, zoneId, endTime, startTime, enabled);
        assertCheck.assertCheckForUpdateDeliverySlot(response);
    }
    
    // disable or enable all delivery slot of a given zone
    public void disableEnableAllDeliverySlots(Integer zone, Integer enabled) {
        List<Integer> allIds = new ArrayList<>();
        List<Integer> allSlotIds = getAllSlotsIdsOfZone(zone);
        for (int i = 0; i < allSlotIds.size(); i++) {
            updateDeliverySlots(allSlotIds.get(i), zone, "1200", "1245", enabled);
        }
    }
    
    public List<Integer> getAllSlotsIdsOfZone(Integer zone) {
        List<Integer> allSlotids = new ArrayList<>();
        Processor response = getDeliverySlotsByZoneId(zone);
        String body = response.ResponseValidator.GetBodyAsText();
        String data = JsonPath.read(body, "$.data").toString();
        if (!(data.equalsIgnoreCase("[]"))) {
            String slotIds = JsonPath.read(body, "$.data..id").toString();
            String[] allIds = slotIds.split(",");
            int i = 0;
            
            for (String id : allIds) {
                id = "" + id.replace("[", "").replace("]", "");
                allSlotids.add(i, Integer.parseInt(id));
                System.out.println("slot id --->>> " + id);
                i++;
            }
            //returns ids of all created delivery slots
            return allSlotids;
        }
        
        return allSlotids;
    }
    
    
    public Processor getDeliverySlotsByZoneId(Integer zoneId) {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("deliveryservice", "getDailySlots", gameOfThrone);
        String[] urlParam = new String[]{Integer.toString(zoneId)};
        Processor processor = new Processor(service, header, null, urlParam);
        return processor;
    }
    
    public Processor updateDailySlotsWithEnabledFlag(Integer id, Integer zone, String endTime, String startTime, Integer enabled) {
        String[] urlParam = new String[]{Integer.toString(id)};
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("deliveryservice", "updateDailySlots", gameOfThrone);
            HashMap<String, String> headers = dhm.doubleheader();
            DailyTimeSlot dailyTimeSlot = new DailyTimeSlot();
            dailyTimeSlot.setZone(zone);
            dailyTimeSlot.setEnabled(enabled);
            dailyTimeSlot.setEndTime(Long.valueOf(endTime));
            dailyTimeSlot.setStartTime(Long.valueOf(startTime));
            String payload = new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(dailyTimeSlot);
            System.out.println("payload = " + payload);
            processor = new Processor(service, headers, payload, urlParam, 0);
            return processor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }
    
    public  HashMap<String, String> getZoneIdAndCityByLatLong(String byLat, String byLong){
        HashMap<String, String> zoneAndCityId = new HashMap<String, String>();
        
       zoneAndCityId.put("zoneId",serviceabilityHelper.getZoneIdByLatLong(Double.valueOf(byLat), Double.valueOf(byLong)));
    zoneAndCityId.put("cityId",serviceabilityHelper.getCityIdByLatLong(Double.valueOf(byLat),Double.valueOf(byLong)));
        System.out.println("zone id & city id from delivery helper ----> " + zoneAndCityId.get("zoneId") + " City id -->> " + zoneAndCityId.get("cityId"));
    return zoneAndCityId;
    }
    
    
    public  String getServiceableStoreAndItsCityId(String byLat, String byLong, String cityId){
        
        String startTime = "1548485016";
        String endTime = "1893502830";
       Processor response = serviceabilityHelper.dailyListing(byLat, byLong, "", startTime, endTime,Long.parseLong("10") , 10 ,true );
       
       
       return "";
    }
    
    
    
    
    
    
    
    
    
    
    //-------------------------------------------------- CMS Helper ----------------------------------------------------------------------------
    
   public String availableSpinIdFromServeDailyPlanListing(String storeId) throws IOException, JSONException {
     String id= getAvailableItemIdOfPlanByStoreId(storeId);
       Assert.assertNotEquals(id,"Not found");
    return id;
   }
   
    public ListPlanProductMetaPojo populateServeDailyPlansProduct(Integer storeId, String productId) {
        ListPlanProductMetaPojo listPlanProductMeta = new ListPlanProductMetaPojo(storeId);
        listPlanProductMeta.setDefault().withProductId(new ProductId(productId).setDefault());
        
        return listPlanProductMeta;
    }
    
    public  Processor serveDailyPlan(ListPlanProductMetaPojo request, String[] urlParam) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        //String[] param = new String[]{urlParam};
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("cms", "serveDailyPlans", gameOfThrone);
        Processor processor = new Processor(service, header, payload, urlParam);
        return processor;
    }
    
    public HashMap<String,String> cmsItemDetails(String store,String itemId) throws IOException {
       HashMap<String,String> details = new HashMap<String,String>();
       ListPlanProductMetaPojo payload= populateServeDailyPlansProduct(Integer.valueOf(store),"");
      Processor response= serveDailyProductsListMeta(payload); //TODO need to call INVENTORY param as well in this helper for getting maxServe of item
      //  response.RequestValidator.GetNodeValueAsStringFromJsonArray("");
        
      itemId ="4_1";
        details.put( "display_name" , "Daal Curry Chawal");
        details.put ("isVeg", "0");
        details.put ("maxServe" ,"1");
        details.put ("is_avail","1"); // inStock
        details.put ("itemId" ,itemId);
       

        
return details;
   }
    
    
    
    
    
    
    public Processor serveDailyProducts(ListPlanProductMetaPojo request, String[] urlParam) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        //String[] param = new String[]{urlParam};
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("cms", "serveDailyProducts", gameOfThrone);
        Processor processor = new Processor(service, header, payload, urlParam);
        return processor;
    }
    
    
    public  String getAvailableItemIdOfPlanByStoreId(String store) throws IOException, JSONException {
    
        ListPlanProductMetaPojo payload = populateServeDailyPlansProduct(Integer.valueOf(store), "");
        Processor response = serveDailyPlan(payload, new String[]{VendorConstants.serveDailyPlan[2], VendorConstants.serveDailyPlan[3]});
        String str = response.ResponseValidator.GetBodyAsText();
    
        JSONObject jsonpObject = new JSONObject(str);
    
        JSONArray jsonArray = (JSONArray) jsonpObject.get("data");
    
        for (int i = 0; i < jsonArray.length(); i++) {
            Integer storeId = (Integer) ((JSONObject) jsonArray.get(i)).get("store_id");
        
            JSONArray variationList = (JSONArray) ((JSONObject) jsonArray.get(i)).get("variations");
        
            for (int j = 0; j < variationList.length(); j++) {
            
                String id = (String) ((JSONObject) variationList.get(j)).get("spin");
            
                if (((JSONObject) variationList.get(j)).get("availability") != null &&
                            (Boolean) ((JSONObject) ((JSONObject) variationList.get(j)).get("availability")).get("is_avail")) {
                    return id;
                }
            
            }
        }
        return "Not found";

//        ListPlanProductMetaPojo payload = populateServeDailyPlansProduct(Integer.valueOf(storeId), productId);
//        String availTrue = "";
//        int index = 0;
//        Processor response = serveDailyPlan(payload, new String[]{VendorConstants.serveDailyPlan[2], VendorConstants.serveDailyPlan[3]});
//        String text = response.ResponseValidator.GetBodyAsText();
//
//        JSONObject jsonObject = new JSONObject(text);
//
//        JSONObject o = ((JSONObject)((JSONArray)jsonObject.get("data")).get(0));
//////        ((HashMap)((JSONObject)((JSONArray)jsonObject.get("data")).get(0))).map.entrySet().forEach( entry -> {
//////            String store = (String) ((HashMap) entry).get("store_id");
////
////        });
//
//        Iterator keysItr = o.keys();
//
//        while (keysItr.hasNext()) {
//            String key = (String) keysItr.next();
//            Object value = o.get(key);
//
//            JSONArray valueList = (JSONArray) o.get(key);
//
//            for(int i = 0; i < valueList.length(); i++) {
//                JSONObject object = (JSONObject) valueList.get(i);
//            }
//        }
//        for(int i = 0; i < jsonObject.getJSONArray("data").length(); i++) {
//            JSONObject object =  ((JSONObject)((JSONArray)jsonObject.get("data")).get(0));
//
//
////            JSONObject object = jsonObject.getJSONObject("data");
//
////            String store_id = (String) ((HashMap)object).get("store_id");
//        }
//
//        String data= JsonPath.read(text,"$.data..[?(@.store_id=="+storeId+")]");
//        String ProductId = JsonPath.read(data,"$.data..[?(@.store_id==12323)].product_id");
//        String idAndAvail = JsonPath.read(data, "$.data..variations..[?(@.id)]").toString();
//        String allIds = JsonPath.read(idAndAvail, "$.data..variations..[?(@.id)].id");
//        String isAvail = JsonPath.read(idAndAvail, "$.data..variations..[?(@.id)]..is_avail");
//        String[] ids = allIds.split(",");
//        String[] isAvailTrue = isAvail.split(",");
//        for (int i = 0; i < isAvailTrue.length; i++) {
//            String flag = isAvailTrue[i];
//            if (flag.equalsIgnoreCase("true")) {
//                availTrue = "" + flag;
//                index = i;
//                break;
//            }
//        }
//        String isAvailableTrueId = ids[index];
//        return null;
    }
    
    
    
    public String getAvailableMealIdByStoreId(String storeId , String productId) throws IOException {
      Processor response = serveDailyProducts(populateServeDailyPlansProduct(Integer.parseInt(storeId) , productId), new String[] {VendorConstants.serveDailyPlan[2], VendorConstants.serveDailyPlan[3]});
      
        return "4_1";
    }
    
    public  Processor serveDailyPlanListMeta(ListPlanProductMetaPojo request) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
         String[] urlParam = new String[] {"LIST-META"};
        //String[] param = new String[]{urlParam};
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("cms", "serveDailyListMetaPlans", gameOfThrone);
        Processor processor = new Processor(service, header, payload, urlParam);
        return processor;
    }
    
    public Processor serveDailyProductsListMeta(ListPlanProductMetaPojo request ) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        
        // TODO need to call INVENTORY as well to get maxServe of item
        String[] urlParam = new String[]{"LIST-META"};
        //String[] param = new String[]{urlParam};
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("cms", "serveDailyListMetaPlans", gameOfThrone);
        Processor processor = new Processor(service, header, payload, urlParam);
        return processor;
    }
    
    public String getMealMeta(String storeId, String mealId) throws IOException {
        ListPlanProductMetaPojo payload =populateServeDailyPlansProduct(Integer.valueOf(storeId),"");
       Processor response= serveDailyProductsListMeta(payload);
       // get meta & variations details
        return null;
    }
    
    public String getPlanItemMeta(String storeId , String itemId) throws IOException {
        ListPlanProductMetaPojo request= populateServeDailyPlansProduct(Integer.valueOf(storeId),"");
        Processor response= serveDailyPlanListMeta(request);
         // get meta & variation details
        return null;
    }
    
    public Integer getPlanItemTenure(String storeId , String itemId) throws IOException {
        ListPlanProductMetaPojo request= populateServeDailyPlansProduct(Integer.valueOf(storeId),"");
        Processor response= serveDailyPlanListMeta(request);
        int tenure = response.ResponseValidator.GetNodeValueAsInt("$.data.[?(@.store_id=="+storeId+")]..variations..[?(@.id==\" "+itemId+"\")].meta.tenure");
        return tenure;
    }
    
    public Processor getRestaurant(){
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cms", "restaurantSearch", gameOfThrone);
        Processor processor = new Processor(service, header);
        return processor;
    }
    
    public HashMap<String,String> getRestaurantAndItsLatLong(){
       HashMap<String, String> restaurantDetails = new HashMap<String, String>();
       
        Processor processor = getRestaurant();
        assertCheck.assertCheckForRestaurantSearch(processor);
        String completeResponse = processor.ResponseValidator.GetBodyAsText();
    String allRestaurnatDetails= processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..search_data..[?(@.id)]");
       String restaurantIds = JsonPath.read(completeResponse,"$.data..search_data..[?(@.id)].id").toString();
       String[] allRestaurant =restaurantIds.split(",");
    String id= allRestaurant[0].replace("[","");
     String RestLatLong= JsonPath.read(completeResponse,"$.data..search_data.[?(@.id=="+id+")].latLong").toString();
    String[] latLong= RestLatLong.split(",");
      restaurantDetails.put("id",id); restaurantDetails.put("lat",latLong[0].replace("[\"","")); restaurantDetails.put("long",latLong[1].replace("\"]",""));
    return restaurantDetails;
    }
    
    
    
    //----------------------------------Vendor order listing API --------------------------------------------
    
    public Processor fetchOrders(FetchOrders payload, String cookie) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type","application/json");
        System.out.println( "Swiggy_Session-alpha=   "  + cookie);
        header.put("Cookie","Swiggy_Session-alpha=" + cookie);
        GameOfThronesService service = new GameOfThronesService("vendorrms","fetchOrders" ,gameOfThrone);
        String[] request = new String[]{jsonHelper.getObjectToJSON(payload)};
        Processor processor = new Processor(service, header ,request,null);
        return processor;
     }
    
    public Processor vendorOrderListing(String fromTime ,String toTime, String restaurantId ,String password, String lat, String lon , String cityid, String token ) throws IOException {
    
        // call RMS login API to get token
        
        FetchOrders fetchOrders = new FetchOrders().fetchOrdersData( fromTime , toTime,  restaurantId ,  lat,  lon ,  cityid );
        Processor processor= fetchOrders(fetchOrders,token);
        assertCheck.assertionCheckForVendorFetchOrder(processor);
        return processor;
    }
    
    
    public String getStartTime() throws ParseException {
      return getTodayStartTime("00","00");
    }
    
    public String getEndTIme() throws ParseException {
        return getTodayStartTime("23","59");
    }
    
    // get Vendor details
    public  HashMap<String, String> vendorDetails(String userName, String password) {
        HashMap<String, String> restaurantDetails = new HashMap<String, String>();
        Processor response = rmsHelper.getLogin(userName, password);
        int statusCode = response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        String statusMessage = response.ResponseValidator.GetNodeValue("$.statusMessage").replace("[", "").replace("]", "");
    
        String userType = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.userType").replace("[","").replace("]","");
        if (statusCode == 0 && statusMessage.equalsIgnoreCase(VendorConstants.loginStatus) && userType.equalsIgnoreCase("daily")) {
        
            restaurantDetails.put("cityId", response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..city_id").replace("[", "").replace("]", ""));
            restaurantDetails.put("restaurantId", response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..rest_id").replace("[", "").replace("]", ""));
    
   restaurantDetails.put("lat",response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..latLong.lat").replace("[\"","").replace("\"]",""));
   restaurantDetails.put("long",response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..latLong.long").replace("[\"","").replace("\"]",""));
   restaurantDetails.put("accessToken", response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..access_token").replace("[\"","").replace("\"]",""));
   
    }
    return restaurantDetails;
    
    }
    
    // Change server time
    public void changeServerTimeToPast(int day) throws IOException {
       String date= Utility.getFutureDate(day, "yyyy-MM-dd");
        System.out.println("past date" + date );
        //serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time "+ date);
        String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " +date ));
        subscriptionHelper.changeServerTime(changeServerDatePayload);
        String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormatWithColon()));
        subscriptionHelper.changeServerTime(changeServerTimePayload);
    }
    
    // Change server time to current time
    public void changeSeverCurrentDate() throws IOException {
        String changeServerDatePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentDateInReadableFormat()));
        subscriptionHelper.changeServerTime(changeServerDatePayload);
        String changeServerTimePayload = jsonHelper.getObjectToJSON(serverTimeChangePojo.setDefaultData().withQuery("sudo timedatectl set-time " + DateUtility.getCurrentTimeInReadableFormatWithColon()));
        subscriptionHelper.changeServerTime(changeServerTimePayload);
        
    }
    
    // ----------------------------------------------Utilitymethod---------------------------------------------------------------------
    
    public String addMinutesToTheGivenMilitaryTime(String time, int interval) throws ParseException {
       String mm=time.substring(2); time =time.replace(mm,"," + mm);
      String[] hhmm = time.split(",");
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        Calendar calendar = Calendar.getInstance();
        System.out.println("1st--------- " + hhmm[0] + " 2nd" + hhmm[1] );
        calendar.set(Calendar.HOUR,Integer.valueOf(hhmm[0]));
        calendar.set(Calendar.MINUTE,Integer.valueOf(hhmm[1]));
        calendar.add(Calendar.MINUTE, interval);
        System.out.println("end date ---- " + sdf.format(calendar.getTime().getTime()));
        return sdf.format(calendar.getTime().getTime());
    }
    
    public String getCurrentTIimeInHHMMByAddingHours(int hours){
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.HOUR, hours);
            calendar.add(Calendar.MINUTE,1);
        return sdf.format(calendar.getTime().getTime());
    }
    
    public String getCurrentTIimeInHHMMByAddingMinute(int minute){
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE,minute);
        return sdf.format(calendar.getTime().getTime());
    }
    
    public long getDateFromStartTime(int hr, int mm) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, hr);
        calendar.set(Calendar.MINUTE, mm);
        
        return calendar.getTime().getTime();
    }
    public String getHHMMFromDate(long time) {
        DateFormat dateFormat = new SimpleDateFormat("HHmm");
        System.out.println("Date --->>  " + dateFormat.format(time));
        return dateFormat.format(time);
    }
    
    public String addMinutesToTheGivenHHMM(String hhmm, int interval) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        Calendar calendar = Calendar.getInstance();
        while (hhmm.length() < 4) {
            hhmm = "0" + hhmm;
        }
        String mm = hhmm.substring(2,4);
        String hrs = hhmm.substring(0, 2);
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hrs));
        calendar.set(Calendar.MINUTE, Integer.valueOf(mm));
        calendar.add(Calendar.MINUTE, interval);
        String data = sdf.format((calendar.getTime().getTime()));
        if(data.length()==2){
           return "00"+ data;
        }
        return sdf.format((calendar.getTime().getTime()));
    }
    
    
    public String getTodayStartTime(String hh ,String mm) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY,Integer.valueOf(hh));
        calendar.set(Calendar.MINUTE,Integer.valueOf(mm));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return Long.toString(calendar.getTime().getTime());
    }
    
    public String getFutureStartTime(String hh ,String mm, int day) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,day);
        calendar.set(Calendar.HOUR_OF_DAY,Integer.valueOf(hh));
        calendar.set(Calendar.MINUTE,Integer.valueOf(mm));
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return Long.toString(calendar.getTime().getTime());
    }
    
    
    public HashMap<String,String> getZoneIdAndCityIdByLatLon(String lat, String lon){
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("zoneId",serviceabilityHelper.getZoneIdByLatLong(Double.valueOf(lat),Double.valueOf(lon)));
        data.put("cityId",serviceabilityHelper.getZoneIdByLatLong(Double.valueOf(lat),Double.valueOf(lon)));
        
        return  data;
        
    }
    
    
    
    
    
    
    
    
//    public Processor cancelSubscription(String subscriptionId){
//        HashMap<String, String> header = new HashMap<String, String>();
//        header.put("Content-Type", "application/json");
//        GameOfThronesService service = new GameOfThronesService("","" ,gameOfThrone);
//        String [] urlParam = new String[] {subscriptionId};
//        Processor processor = new Processor(service, header, null , urlParam);
//        return processor;
//    }
}
