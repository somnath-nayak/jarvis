
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "inventory_construct.inventories.max_cap",
    "inventory_construct.inventories.sold_count"
})
public class $set {

    @JsonProperty("inventory_construct.inventories.max_cap")
    private Integer inventory_construct_inventories_max_cap;
    @JsonProperty("inventory_construct.inventories.sold_count")
    private Integer inventory_construct_inventories_sold_count;

    @JsonProperty("inventory_construct.inventories.max_cap")
    public Integer getInventory_construct_inventories_max_cap() {
        return inventory_construct_inventories_max_cap;
    }

    @JsonProperty("inventory_construct.inventories.max_cap")
    public void setInventory_construct_inventories_max_cap(Integer inventory_construct_inventories_max_cap) {
        this.inventory_construct_inventories_max_cap = inventory_construct_inventories_max_cap;
    }

    public $set withInventory_construct_inventories_max_cap(Integer inventory_construct_inventories_max_cap) {
        this.inventory_construct_inventories_max_cap = inventory_construct_inventories_max_cap;
        return this;
    }

    @JsonProperty("inventory_construct.inventories.sold_count")
    public Integer getInventory_construct_inventories_sold_count() {
        return inventory_construct_inventories_sold_count;
    }

    @JsonProperty("inventory_construct.inventories.sold_count")
    public void setInventory_construct_inventories_sold_count(Integer inventory_construct_inventories_sold_count) {
        this.inventory_construct_inventories_sold_count = inventory_construct_inventories_sold_count;
    }

    public $set withInventory_construct_inventories_sold_count(Integer inventory_construct_inventories_sold_count) {
        this.inventory_construct_inventories_sold_count = inventory_construct_inventories_sold_count;
        return this;
    }

    public $set setData(Integer inventory_construct_inventories_max_cap,Integer inventory_construct_inventories_sold_count){
        this.withInventory_construct_inventories_max_cap(inventory_construct_inventories_max_cap).withInventory_construct_inventories_sold_count(inventory_construct_inventories_sold_count);
        return this;
    }

}
