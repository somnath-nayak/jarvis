package com.swiggy.api.daily.mealSlot.helper;

public interface MealSlotConstant {

    String DELIVERY_ADDRESS = "";
    String LAT = "";
    String LONG = "";
    String BREAKFAST = "BREAKFAST";
    String LUNCH = "LUNCH";
    String SNACKS = "SNACKS";
    String DINNER = "DINNER";
    String LATE_NIGHT = "LATE_NIGHT";
    String cityID_0 = "0";
    String status_0 = "0";
    String status_1 = "1";
    String cityID = "1";
    String data = "success";
    String id = "1";
    String invalidCityID = "6324778";
    String invalidMealID = "3242342";
    String nonExistantCityID = "2#";
    String time_12_AM = "1546453800000";
    String time_1_AM = "1546457400000";
    String BadRequest = "Bad Request";
    String _404 = "404";
    String _400 = "400";
    String _401 = "401";
    String _402 = "402";
    String _403 = "403";
    String _405 = "405";
    String _300 = "300";
    String _700 = "1500";
    String _1000 = "1200";
    String _1200 = "1200";
    String _1500 = "1500";
    String _1600 = "1600";
    String _1800 = "1800";
    String _1900 = "1900";
    String _2200 = "2200";
    String _2330 = "2330";
    String cityID_99 = "99";
    String time_9_AM = "1546486200000";
    String Today = "Today";
    String Tonight = "Tonight";
    String Random = "Random";
    String Tomorrow = "Tomorrow";
}
