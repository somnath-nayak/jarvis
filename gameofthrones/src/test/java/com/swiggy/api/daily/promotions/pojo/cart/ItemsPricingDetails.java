package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ItemsPricingDetails {
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("cgst")
    private Double cgst;
    @JsonProperty("sgst")
    private Double sgst;
    @JsonProperty("igst")
    private Integer igst;
    @JsonProperty("packagingCharge")
    private Integer packingCharge;
    @JsonProperty("cgstPackingCharge")
    private Integer cgstPackingCharge;
    @JsonProperty("igstPackingCharge")
    private Integer igstPackingCharge;
    @JsonProperty("sgstPackingCharge")
    private Integer sgstPackingCharge;
    @JsonProperty("netPrice")
    private Integer netPrice;
    
    public ItemsPricingDetails() {
    
    }
    
    public Integer getPrice() {
        return price;
    }
    
    public void setPrice(Integer price) {
        this.price = price;
    }
    
    public Double getCgst() {
        return cgst;
    }
    
    public void setCgst(Double cgst) {
        this.cgst = cgst;
    }
    
    public Double getSgst() {
        return sgst;
    }
    
    public void setSgst(Double sgst) {
        this.sgst = sgst;
    }
    
    public Integer getIgst() {
        return igst;
    }
    
    public void setIgst(Integer igst) {
        this.igst = igst;
    }
    
    public Integer getPackingCharge() {
        return packingCharge;
    }
    
    public void setPackingCharge(Integer packingCharge) {
        this.packingCharge = packingCharge;
    }
    
    public Integer getCgstPackingCharge() {
        return cgstPackingCharge;
    }
    
    public void setCgstPackingCharge(Integer cgstPackingCharge) {
        this.cgstPackingCharge = cgstPackingCharge;
    }
    
    public Integer getIgstPackingCharge() {
        return igstPackingCharge;
    }
    
    public void setIgstPackingCharge(Integer igstPackingCharge) {
        this.igstPackingCharge = igstPackingCharge;
    }
    
    public Integer getSgstPackingCharge() {
        return sgstPackingCharge;
    }
    
    public void setSgstPackingCharge(Integer sgstPackingCharge) {
        this.sgstPackingCharge = sgstPackingCharge;
    }
    
    public Integer getNetPrice() {
        return netPrice;
    }
    
    public void setNetPrice(Integer netPrice) {
        this.netPrice = netPrice;
    }
    
    
    public ItemsPricingDetails withPrice(Integer price) {
        setPrice(price);
        return this;
    }
    
    public ItemsPricingDetails withCgst(Double cgst) {
        setCgst(cgst);
        return this;
    }
    
    public ItemsPricingDetails withSgst(Double sgst) {
        setSgst(sgst);
        return this;
    }
    
    public ItemsPricingDetails withIgst(Integer igst) {
        setIgst(igst);
        return this;
    }
    
    public ItemsPricingDetails withPackingCharge(Integer packingCharge) {
        setPackingCharge(packingCharge);
        return this;
    }
    
    public ItemsPricingDetails withCgstPackingCharge(Integer cgstPackingCharge) {
        setCgstPackingCharge(cgstPackingCharge);
        return this;
    }
    
    public ItemsPricingDetails withigstPackingCharge(Integer igstPackingCharge) {
        setIgstPackingCharge(igstPackingCharge);
        return this;
    }
    
    public ItemsPricingDetails withSgstPackingCharge(Integer sgstPackingCharge) {
        setSgstPackingCharge(sgstPackingCharge);
        return this;
    }
    
    public ItemsPricingDetails withNetPrice(Integer netPrice) {
        setNetPrice(netPrice);
        return this;
    }
    
    public ItemsPricingDetails withGivenValues(Integer price) {
        setPrice(price);
        setCgst(2.5);
        setSgst(2.5);
        setIgst(1);
        setPackingCharge(40);
        setCgstPackingCharge(1);
        setIgstPackingCharge(1);
       setSgstPackingCharge(1);
        setNetPrice(147);
        
        return this;
    }
    
    
//    public List<ItemsPricingDetails> withGivenValues(Integer price) {
//        List<ItemsPricingDetails> itemsPricingDetails = new ArrayList<>();
//
//        itemsPricingDetails.add(0,itemsPricingDetailsWithGivenValues(price));
//
//        return itemsPricingDetails;
//    }
    
}
