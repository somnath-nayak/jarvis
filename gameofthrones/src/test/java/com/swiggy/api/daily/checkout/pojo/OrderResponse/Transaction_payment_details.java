package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction_payment_details
{
    private String paymentTxnStatus;

    private String paymentMethod;

    private String refundInitiated;

    public String getPaymentTxnStatus ()
    {
        return paymentTxnStatus;
    }

    public void setPaymentTxnStatus (String paymentTxnStatus)
    {
        this.paymentTxnStatus = paymentTxnStatus;
    }

    public String getPaymentMethod ()
    {
        return paymentMethod;
    }

    public void setPaymentMethod (String paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public String getRefundInitiated ()
    {
        return refundInitiated;
    }

    public void setRefundInitiated (String refundInitiated)
    {
        this.refundInitiated = refundInitiated;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [paymentTxnStatus = "+paymentTxnStatus+", paymentMethod = "+paymentMethod+", refundInitiated = "+refundInitiated+"]";
    }
}
