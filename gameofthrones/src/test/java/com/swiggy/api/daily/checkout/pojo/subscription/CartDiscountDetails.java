package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartDiscountDetails {


    @JsonProperty("tradeDiscount")
    private Long tradeDiscount;
    @JsonProperty("tradeDiscountMessage")
    private String tradeDiscountMessage;
    @JsonProperty("swiggyDiscount")
    private Long swiggyDiscount;
    @JsonProperty("swiggyDiscountMessage")
    private String swiggyDiscountMessage;

    @JsonProperty("tradeDiscount")
    public Long getTradeDiscount() {
        return tradeDiscount;
    }

    @JsonProperty("tradeDiscount")
    public void setTradeDiscount(Long tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    public CartDiscountDetails withTradeDiscount(Long tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
        return this;
    }

    @JsonProperty("tradeDiscountMessage")
    public String getTradeDiscountMessage() {
        return tradeDiscountMessage;
    }

    @JsonProperty("tradeDiscountMessage")
    public void setTradeDiscountMessage(String tradeDiscountMessage) {
        this.tradeDiscountMessage = tradeDiscountMessage;
    }

    public CartDiscountDetails withTradeDiscountMessage(String tradeDiscountMessage) {
        this.tradeDiscountMessage = tradeDiscountMessage;
        return this;
    }

    @JsonProperty("swiggyDiscount")
    public Long getSwiggyDiscount() {
        return swiggyDiscount;
    }

    @JsonProperty("swiggyDiscount")
    public void setSwiggyDiscount(Long swiggyDiscount) {
        this.swiggyDiscount = swiggyDiscount;
    }

    public CartDiscountDetails withSwiggyDiscount(Long swiggyDiscount) {
        this.swiggyDiscount = swiggyDiscount;
        return this;
    }

    @JsonProperty("swiggyDiscountMessage")
    public String getSwiggyDiscountMessage() {
        return swiggyDiscountMessage;
    }

    @JsonProperty("swiggyDiscountMessage")
    public void setSwiggyDiscountMessage(String swiggyDiscountMessage) {
        this.swiggyDiscountMessage = swiggyDiscountMessage;
    }

    public CartDiscountDetails withSwiggyDiscountMessage(String swiggyDiscountMessage) {
        this.swiggyDiscountMessage = swiggyDiscountMessage;
        return this;
    }
}
