
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "product_name",
    "short_description",
    "long_description",
    "is_veg",
    "images"
})
public class Meta {

    @JsonProperty("product_name")
    private String productName;
    @JsonProperty("short_description")
    private String shortDescription;
    @JsonProperty("long_description")
    private String longDescription;
    @JsonProperty("is_veg")
    private Integer isVeg;
    @JsonProperty("images")
    private List<String> images = null;

    @JsonProperty("product_name")
    public String getProductName() {
        return productName;
    }

    @JsonProperty("product_name")
    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Meta withProductName(String productName) {
        this.productName = productName;
        return this;
    }

    @JsonProperty("short_description")
    public String getShortDescription() {
        return shortDescription;
    }

    @JsonProperty("short_description")
    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Meta withShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    @JsonProperty("long_description")
    public String getLongDescription() {
        return longDescription;
    }

    @JsonProperty("long_description")
    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public Meta withLongDescription(String longDescription) {
        this.longDescription = longDescription;
        return this;
    }

    @JsonProperty("is_veg")
    public Integer getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
    }

    public Meta withIsVeg(Integer isVeg) {
        this.isVeg = isVeg;
        return this;
    }

    @JsonProperty("images")
    public List<String> getImages() {
        return images;
    }

    @JsonProperty("images")
    public void setImages(List<String> images) {
        this.images = images;
    }

    public Meta withImages(List<String> images) {
        this.images = images;
        return this;
    }

    public Meta setDefaultData() {
        return this.withProductName(SubscriptionConstant.PRODUCT_NAME)
                .withShortDescription(SubscriptionConstant.SHORT_DESCRIPTION)
                .withLongDescription(SubscriptionConstant.LONG_DESCRIPTION)
                .withIsVeg(SubscriptionConstant.IS_VEG)
                .withImages(new ArrayList<String>() {{add(SubscriptionConstant.IMAGES);}});
    }

}
