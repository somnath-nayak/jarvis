package com.swiggy.api.daily.promotions.pojo.createPromotion;

import org.codehaus.jackson.annotate.JsonProperty;

public class Description {
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    @JsonProperty("description")
    private String description = "TEST_COPY_TEXT";
    
    public Description withText(String text){
        setDescription(text);
        return this;
    }
    
//    public List<Description> listOfText(String description){
//        List<Description> listOfText = new ArrayList<>();
//
//        listOfText.add(0,withText(description));
//        return listOfText;
//    }
}
