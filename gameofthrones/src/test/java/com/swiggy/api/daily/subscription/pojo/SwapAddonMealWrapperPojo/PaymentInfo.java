
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.text.ParseException;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "transaction_id",
    "payment_status",
    "refund_for_transaction_id",
    "payment_type",
    "metadata",
    "transaction_amount",
    "payment_method",
    "order_context",
    "reason",
    "created_at",
    "updated_at"
})
public class PaymentInfo {

    @JsonProperty("transaction_id")
    private String transactionId;
    @JsonProperty("payment_status")
    private String paymentStatus;
    @JsonProperty("refund_for_transaction_id")
    private Object refundForTransactionId;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("metadata")
    private String metadata;
    @JsonProperty("transaction_amount")
    private Integer transactionAmount;
    @JsonProperty("payment_method")
    private String paymentMethod;
    @JsonProperty("order_context")
    private String orderContext;
    @JsonProperty("reason")
    private String reason;
    @JsonProperty("created_at")
    private Long createdAt;
    @JsonProperty("updated_at")
    private Long updatedAt;

    @JsonProperty("transaction_id")
    public String getTransactionId() {
        return transactionId;
    }

    @JsonProperty("transaction_id")
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public PaymentInfo withTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return this;
    }

    @JsonProperty("payment_status")
    public String getPaymentStatus() {
        return paymentStatus;
    }

    @JsonProperty("payment_status")
    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentInfo withPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
        return this;
    }

    @JsonProperty("refund_for_transaction_id")
    public Object getRefundForTransactionId() {
        return refundForTransactionId;
    }

    @JsonProperty("refund_for_transaction_id")
    public void setRefundForTransactionId(Object refundForTransactionId) {
        this.refundForTransactionId = refundForTransactionId;
    }

    public PaymentInfo withRefundForTransactionId(Object refundForTransactionId) {
        this.refundForTransactionId = refundForTransactionId;
        return this;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentInfo withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @JsonProperty("metadata")
    public String getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public PaymentInfo withMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    @JsonProperty("transaction_amount")
    public Integer getTransactionAmount() {
        return transactionAmount;
    }

    @JsonProperty("transaction_amount")
    public void setTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public PaymentInfo withTransactionAmount(Integer transactionAmount) {
        this.transactionAmount = transactionAmount;
        return this;
    }

    @JsonProperty("payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("payment_method")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentInfo withPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    @JsonProperty("order_context")
    public String getOrderContext() {
        return orderContext;
    }

    @JsonProperty("order_context")
    public void setOrderContext(String orderContext) {
        this.orderContext = orderContext;
    }

    public PaymentInfo withOrderContext(String orderContext) {
        this.orderContext = orderContext;
        return this;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public void setReason(String reason) {
        this.reason = reason;
    }

    public PaymentInfo withReason(String reason) {
        this.reason = reason;
        return this;
    }

    @JsonProperty("created_at")
    public Long getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public PaymentInfo withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    @JsonProperty("updated_at")
    public Long getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
    }

    public PaymentInfo withUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public PaymentInfo setDefaultData() throws ParseException {
        return this.withTransactionId(SubscriptionConstant.TRANSACTION_ID)
                .withPaymentStatus(SubscriptionConstant.PAYMENT_STATUS)
                .withRefundForTransactionId(null)
                .withPaymentType(SubscriptionConstant.PAYMENT_TYPE)
                .withMetadata(SubscriptionConstant.METADATA)
                .withTransactionAmount(SubscriptionConstant.TRANSACTION_AMOUNT)
                .withPaymentMethod(SubscriptionConstant.PAYMENT_METHOD)
                .withOrderContext(SubscriptionConstant.ORDER_CONTEXT)
                .withReason(SubscriptionConstant.REASON)
                .withCreatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()))
                .withUpdatedAt(DateUtility.convertReadableDateTimeToInMilisecond(DateUtility.getCurrentDateInReadableFormat(),DateUtility.getCurrentDateInReadableFormat()));
    }

}
