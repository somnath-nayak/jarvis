package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "itemId",
        "restaurantId",
        "liveDate",
        "expiryDate",
        "fromTime",
        "toTime",
        "source"
})
public class PausePOJO {

    @JsonProperty("itemId")
    private String itemId;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("liveDate")
    private String liveDate;
    @JsonProperty("expiryDate")
    private String expiryDate;
    @JsonProperty("fromTime")
    private String fromTime;
    @JsonProperty("toTime")
    private String toTime;
    @JsonProperty("source")
    private String source;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public PausePOJO() {
    }

    /**
     *
     * @param source
     * @param expiryDate
     * @param liveDate
     * @param fromTime
     * @param itemId
     * @param toTime
     * @param restaurantId
     */
    public PausePOJO(String itemId, String restaurantId, String liveDate, String expiryDate, String fromTime, String toTime, String source) {
        super();
        this.itemId = itemId;
        this.restaurantId = restaurantId;
        this.liveDate = liveDate;
        this.expiryDate = expiryDate;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.source = source;
    }

    @JsonProperty("itemId")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public PausePOJO withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("restaurantId")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public PausePOJO withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonProperty("liveDate")
    public String getLiveDate() {
        return liveDate;
    }

    @JsonProperty("liveDate")
    public void setLiveDate(String liveDate) {
        this.liveDate = liveDate;
    }

    public PausePOJO withLiveDate(String liveDate) {
        this.liveDate = liveDate;
        return this;
    }

    @JsonProperty("expiryDate")
    public String getExpiryDate() {
        return expiryDate;
    }

    @JsonProperty("expiryDate")
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public PausePOJO withExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
        return this;
    }

    @JsonProperty("fromTime")
    public String getFromTime() {
        return fromTime;
    }

    @JsonProperty("fromTime")
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public PausePOJO withFromTime(String fromTime) {
        this.fromTime = fromTime;
        return this;
    }

    @JsonProperty("toTime")
    public String getToTime() {
        return toTime;
    }

    @JsonProperty("toTime")
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }

    public PausePOJO withToTime(String toTime) {
        this.toTime = toTime;
        return this;
    }

    @JsonProperty("source")
    public String getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(String source) {
        this.source = source;
    }

    public PausePOJO withSource(String source) {
        this.source = source;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PausePOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public PausePOJO setDefault(String restaurantID, String entityID, String liveDate, String expiryDate, String fromTime, String toTime, String source) {
        this.withItemId(entityID)
                .withRestaurantId(restaurantID)
                .withLiveDate(liveDate)
                .withExpiryDate(expiryDate)
                .withFromTime(fromTime)
                .withToTime(toTime)
                .withSource(source);
        return this;
    }
}
