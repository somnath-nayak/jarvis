package com.swiggy.api.daily.cms.pojo.Relationship;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "source_filters",
        "destination_filters",
        "relation_types"
})
public class SearchFilter {

    @JsonProperty("source_filters")
    private List<List<Source_filter>> source_filters = null;
    @JsonProperty("destination_filters")
    private List<List<Destination_filter>> destination_filters = null;
    @JsonProperty("relation_types")
    private List<String> relation_types = null;

    @JsonProperty("source_filters")
    public List<List<Source_filter>> getSource_filters() {
        return source_filters;
    }

    @JsonProperty("source_filters")
    public void setSource_filters(List<List<Source_filter>> source_filters) {
        this.source_filters = source_filters;
    }

    @JsonProperty("destination_filters")
    public List<List<Destination_filter>> getDestination_filters() {
        return destination_filters;
    }

    @JsonProperty("destination_filters")
    public void setDestination_filters(List<List<Destination_filter>> destination_filters) {
        this.destination_filters = destination_filters;
    }

    @JsonProperty("relation_types")
    public List<String> getRelation_types() {
        return relation_types;
    }

    @JsonProperty("relation_types")
    public void setRelation_types(List<String> relation_types) {
        this.relation_types = relation_types;
    }

    public SearchFilter setData(String ssid, String spid, String ddid, String spinid,String reltype,String operater){
        List lms=new ArrayList();
        List lmd=new ArrayList();
        List lis=new ArrayList();
        List lid=new ArrayList();
        List<String> lir=new ArrayList();
        lir.add(reltype);
        Destination_filter ds=new Destination_filter();
        Destination_filter ds1=new Destination_filter();
        Source_filter sc=new Source_filter();
        Source_filter sc1=new Source_filter();
        sc.setField("store_id");
        sc.setOperator(operater);
        sc.setValue(ssid);
        sc1.setField("product_id");
        sc1.setOperator(operater);
        sc1.setValue(spid);
        lis.add(sc);
        lis.add(sc1);
        ds.setField("store_id");
        ds.setOperator(operater);
        ds.setValue(ddid);
        ds1.setField("spin");
        ds1.setOperator(operater);
        ds1.setValue(spinid);
        lid.add(ds);
        lid.add(ds1);
        lms.add(lis);
        lmd.add(lid);
        this.setDestination_filters(lmd);
        this.setSource_filters(lms);
        this.setRelation_types(lir);
        return this;

    }

}
