package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class StoreDetails {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("uuid")
    private String uuid;
    @JsonProperty("name")
    private String name;
    @JsonProperty("city")
    private String city;
    @JsonProperty("cityCode")
    private Long cityCode;
    @JsonProperty("area")
    private String area;
    @JsonProperty("areaCode")
    private Long areaCode;
    @JsonProperty("address")
    private String address;
    @JsonProperty("latLong")
    private String latLong;
    @JsonProperty("locality")
    private String locality;
    @JsonProperty("cloudinaryImageId")
    private String cloudinaryImageId;
    @JsonProperty("phoneNumbers")
    private List<String> phoneNumbers = null;
    @JsonProperty("commissionExp")
    private String commissionExp;
    @JsonProperty("packingCharges")
    private String packingCharges;
    @JsonProperty("serviceTax")
    private String serviceTax;
    @JsonProperty("serviceCharges")
    private String serviceCharges;
    @JsonProperty("deliveryCharges")
    private String deliveryCharges;
    @JsonProperty("vat")
    private String vat;
    @JsonProperty("codLimit")
    private Long codLimit;
    @JsonProperty("orderNotifyEmails")
    private List<String> orderNotifyEmails = null;
    @JsonProperty("taxationType")
    private String taxationType;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("exclusive")
    private Boolean exclusive;
    @JsonProperty("assured")
    private Boolean assured;
    @JsonProperty("veg")
    private Boolean veg;
    @JsonProperty("short_story")
    private String shortStory;

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public StoreDetails withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("uuid")
    public String getUuid() {
        return uuid;
    }

    @JsonProperty("uuid")
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public StoreDetails withUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public StoreDetails withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public StoreDetails withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("cityCode")
    public Long getCityCode() {
        return cityCode;
    }

    @JsonProperty("cityCode")
    public void setCityCode(Long cityCode) {
        this.cityCode = cityCode;
    }

    public StoreDetails withCityCode(Long cityCode) {
        this.cityCode = cityCode;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public StoreDetails withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("areaCode")
    public Long getAreaCode() {
        return areaCode;
    }

    @JsonProperty("areaCode")
    public void setAreaCode(Long areaCode) {
        this.areaCode = areaCode;
    }

    public StoreDetails withAreaCode(Long areaCode) {
        this.areaCode = areaCode;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public StoreDetails withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("latLong")
    public String getLatLong() {
        return latLong;
    }

    @JsonProperty("latLong")
    public void setLatLong(String latLong) {
        this.latLong = latLong;
    }

    public StoreDetails withLatLong(String latLong) {
        this.latLong = latLong;
        return this;
    }

    @JsonProperty("locality")
    public String getLocality() {
        return locality;
    }

    @JsonProperty("locality")
    public void setLocality(String locality) {
        this.locality = locality;
    }

    public StoreDetails withLocality(String locality) {
        this.locality = locality;
        return this;
    }

    @JsonProperty("cloudinaryImageId")
    public String getCloudinaryImageId() {
        return cloudinaryImageId;
    }

    @JsonProperty("cloudinaryImageId")
    public void setCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
    }

    public StoreDetails withCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
        return this;
    }

    @JsonProperty("phoneNumbers")
    public List<String> getPhoneNumbers() {
        return phoneNumbers;
    }

    @JsonProperty("phoneNumbers")
    public void setPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }

    public StoreDetails withPhoneNumbers(List<String> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        return this;
    }

    @JsonProperty("commissionExp")
    public String getCommissionExp() {
        return commissionExp;
    }

    @JsonProperty("commissionExp")
    public void setCommissionExp(String commissionExp) {
        this.commissionExp = commissionExp;
    }

    public StoreDetails withCommissionExp(String commissionExp) {
        this.commissionExp = commissionExp;
        return this;
    }

    @JsonProperty("packingCharges")
    public String getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("packingCharges")
    public void setPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
    }

    public StoreDetails withPackingCharges(String packingCharges) {
        this.packingCharges = packingCharges;
        return this;
    }

    @JsonProperty("serviceTax")
    public String getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("serviceTax")
    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    public StoreDetails withServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
        return this;
    }

    @JsonProperty("serviceCharges")
    public String getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("serviceCharges")
    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public StoreDetails withServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
        return this;
    }

    @JsonProperty("deliveryCharges")
    public String getDeliveryCharges() {
        return deliveryCharges;
    }

    @JsonProperty("deliveryCharges")
    public void setDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    public StoreDetails withDeliveryCharges(String deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
        return this;
    }

    @JsonProperty("vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    public StoreDetails withVat(String vat) {
        this.vat = vat;
        return this;
    }

    @JsonProperty("codLimit")
    public Long getCodLimit() {
        return codLimit;
    }

    @JsonProperty("codLimit")
    public void setCodLimit(Long codLimit) {
        this.codLimit = codLimit;
    }

    public StoreDetails withCodLimit(Long codLimit) {
        this.codLimit = codLimit;
        return this;
    }

    @JsonProperty("orderNotifyEmails")
    public List<String> getOrderNotifyEmails() {
        return orderNotifyEmails;
    }

    @JsonProperty("orderNotifyEmails")
    public void setOrderNotifyEmails(List<String> orderNotifyEmails) {
        this.orderNotifyEmails = orderNotifyEmails;
    }

    public StoreDetails withOrderNotifyEmails(List<String> orderNotifyEmails) {
        this.orderNotifyEmails = orderNotifyEmails;
        return this;
    }

    @JsonProperty("taxationType")
    public String getTaxationType() {
        return taxationType;
    }

    @JsonProperty("taxationType")
    public void setTaxationType(String taxationType) {
        this.taxationType = taxationType;
    }

    public StoreDetails withTaxationType(String taxationType) {
        this.taxationType = taxationType;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public StoreDetails withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("exclusive")
    public Boolean getExclusive() {
        return exclusive;
    }

    @JsonProperty("exclusive")
    public void setExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
    }

    public StoreDetails withExclusive(Boolean exclusive) {
        this.exclusive = exclusive;
        return this;
    }

    @JsonProperty("assured")
    public Boolean getAssured() {
        return assured;
    }

    @JsonProperty("assured")
    public void setAssured(Boolean assured) {
        this.assured = assured;
    }

    public StoreDetails withAssured(Boolean assured) {
        this.assured = assured;
        return this;
    }

    @JsonProperty("veg")
    public Boolean getVeg() {
        return veg;
    }

    @JsonProperty("veg")
    public void setVeg(Boolean veg) {
        this.veg = veg;
    }

    public StoreDetails withVeg(Boolean veg) {
        this.veg = veg;
        return this;
    }

    @JsonProperty("short_story")
    public String getShortStory() {
        return shortStory;
    }

    @JsonProperty("short_story")
    public void setShortStory(String shortStory) {
        this.shortStory = shortStory;
    }

    public StoreDetails withShortStory(String shortStory) {
        this.shortStory = shortStory;
        return this;
    }

}
