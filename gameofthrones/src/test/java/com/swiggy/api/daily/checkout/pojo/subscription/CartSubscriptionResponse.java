package com.swiggy.api.daily.checkout.pojo.subscription;

import io.advantageous.boon.json.annotations.JsonProperty;

public class CartSubscriptionResponse {

    @JsonProperty("statusCode")
    private Long statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("data")
    private Data data;

    @JsonProperty("statusCode")
    public Long getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(Long statusCode) {
        this.statusCode = statusCode;
    }

    public CartSubscriptionResponse withStatusCode(Long statusCode) {
        this.statusCode = statusCode;
        return this;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public CartSubscriptionResponse withStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
        return this;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    public CartSubscriptionResponse withData(Data data) {
        this.data = data;
        return this;
    }
}
