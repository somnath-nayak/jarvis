package com.swiggy.api.daily.promotions.helper;

import com.swiggy.api.daily.promotions.pojo.cart.CartDetails;
import com.swiggy.api.daily.promotions.pojo.createPromotion.Actions;
import com.swiggy.api.daily.promotions.pojo.createPromotion.CreatePromotion;
import com.swiggy.api.daily.promotions.pojo.createPromotion.Rules;
import com.swiggy.api.daily.promotions.pojo.updatePromotion.*;
import com.swiggy.api.daily.promotions.pojo.planListing.PlanListing;
import com.swiggy.api.sf.rng.helper.Utility;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PromotionsHelper {
    private static JsonHelper jsonHelper = new JsonHelper();
    static Initialize gameOfThrone = new Initialize();
    CreatePromotion createPromotion = new CreatePromotion();
    PlanListing planListing = new PlanListing();
    UpdatePromotion updatePromotion = new UpdatePromotion();
    PromotionDisable promotionDisable = new PromotionDisable();
   
   public String presentDayTimeSlot(String format, int slotDurationInMinutes , String dayOfWeek) throws ParseException {
      String start = "", end = "", slot = "";
       
       start = "" + Utility.getFutureDateInGivenFormat(0,format);
     end = "" + Utility.addMinutesToTheGivenHHMM(start,slotDurationInMinutes);
       return  slot = start + ":" + end + ":" + dayOfWeek;
   }
    
    public String futureTimeSlot(String format, int slotDurationInMinutes ,int minutes, String dayOfWeek) throws ParseException {
        String start = "", end = "", slot = "", currentTime = "";
        currentTime = Utility.getCurrentDateInHHMM();
        System.out.println("currentTime  is  " +currentTime);
        start = "" + Utility.addMinutesToTheGivenHHMM(currentTime,minutes);
        end = "" + Utility.addMinutesToTheGivenHHMM(start,slotDurationInMinutes);
        return  slot = start + ":" + end + ":" + dayOfWeek;
    }
    
    
    public Processor getActivePromotions(){
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("Content-Type","application/json");
        GameOfThronesService service = new GameOfThronesService("promotion", "getActivePromotions", gameOfThrone);
        Processor processor = new Processor(service,header, null, null);
        return processor;
    }
    
    public Processor hitCartRequest(String[] storeId, String[] skuId,List<Integer> quantity,List<Integer> price, Integer itemTotal ) throws IOException {
       
      CartDetails request= new CartDetails().cartRequest(storeId,  skuId, quantity,price,  itemTotal);
       Processor response =cartRequest(request);
       return  response;
       
   }
   
   
   public Processor cartRequest(CartDetails request) throws IOException {
       HashMap<String , String> header = new HashMap<>();
       header.put("Content-Type","application/json");
       String[] payload = new String[]{jsonHelper.getObjectToJSON(request)};
       GameOfThronesService service = new GameOfThronesService("promotion", "cartRequest", gameOfThrone);
       Processor processor = new Processor(service,header, payload, null);
       return processor;
   }
   
  
    public Processor createPromotionWithDefaultValues() throws IOException {
       HashMap<String,String> data = new HashMap<>();
       CreatePromotion request= createPromotion.withDefaultValues();
       Processor response= createPromotion( request);
      
        return response;
    }
    
    
    public Processor disablePromotions(PromotionDisable request,String id) throws IOException {
        
        HashMap<String , String> header = new HashMap<>();
        header.put("Content-Type","application/json");
        String[] payload = new String[]{jsonHelper.getObjectToJSON(request)};
        String[] urlParams = new String[]{id};
        GameOfThronesService service = new GameOfThronesService("promotion", "updateActive", gameOfThrone);
        Processor processor = new Processor(service,header, payload, urlParams);
        return processor;
    }
    
    
    public PromotionDisable populateDisablePromotion(){
       
        PromotionDisable payload = promotionDisable.withDefaultValues();
      
        return payload;
    }
    
    public PromotionDisable populateDisablePromotionWIthGIvenValues(String updatedBy, Boolean active){
        
        PromotionDisable payload = promotionDisable.withDefaultValues().withActive(active).withUpdatedBy(updatedBy);
        
        return payload;
    }
    
    public void deActivatePromotionByPromotionId(String promotionId) throws IOException {
    
        PromotionDisable request = populateDisablePromotion();
         disablePromotions( request, promotionId);
       
    }
    
    
    
    // create promotion with the given store, Spin and discount type
    public HashMap<String, String> createPromotion(String type, String discountValue , String storeId, String spinId) throws IOException {
       HashMap<String,String> data = new HashMap<>();
       CreatePromotion request= createPromotion.forGivenInput( type, discountValue,  storeId,  spinId);
        
        Processor response= createPromotion( request);
        String promotion = "";
        int statusCode =  response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
       
        if(statusCode==0) {
            String statusMessage =  response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage").replace("[","").replace("]","");
             promotion = statusMessage.substring(24).replace("]\"", "").replace("]", "");
    
            deActivatePromotionByPromotionId(promotion);
            Processor responseTwo= createPromotion( request);
            promotion = responseTwo.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id");
    
            
        }
        
        else {
    
            String statusMessage =  response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage").replace("[","").replace("]","");
             promotion = statusMessage.substring(25).replace("]\"", "").replace("]", "");
    
    
        }
        
        data.put("id", promotion);
        return data;
    }
    
    
    
    // Listing promotion
    
    public Processor planListing(PlanListing request) throws IOException {
        
        HashMap<String , String> header = new HashMap<>();
        header.put("Content-Type","application/json");
        String[] payload = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("promotion", "planListing", gameOfThrone);
       Processor processor = new Processor(service,header, payload, null);
        return processor;
    }
    
    
    
    // Meal promotion
    
    public Processor mealListing(PlanListing request) throws IOException {
        
        HashMap<String , String> header = new HashMap<>();
        header.put("Content-Type","application/json");
        String[] payload = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("promotion", "mealListing", gameOfThrone);
        Processor processor = new Processor(service,header, payload, null);
        return processor;
    }
    
    public Processor hitPlanListing(String[] storeId, List<String[]> allSkuId , List<List<Integer>> price) throws IOException {
        PlanListing request = planListing.withGivenValues( storeId,  allSkuId ,  price);
         Processor response = planListing(request);
        
        return response;
    }
    
    
    public Processor hitMealListing(String[] storeId, List<String[]> allSkuId , List<List<Integer>> price) throws IOException {
        PlanListing request = planListing.withGivenValues( storeId,  allSkuId ,  price);
        Processor response = mealListing(request);
        
        return response;
    }
    
    
    public Processor createPromotionWithGivenInputs(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues) throws IOException {
       CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm"));
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createPromotionWithOutTenant(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues) throws IOException {
        CreatePromotion request = createPromotion.withOutTenant().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm"));
        Processor response= createPromotion( request);
        
        return response;
    }
    
    
    public Processor createPromotionWithGivenValues(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues,String tenant , String copy) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm"))
                .withTenant(tenant).withViewPropertiesText(copy);
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createPromotionWithGivenValidFromAndValidTill(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues,int validFromDayCount , int validTillDayCount) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getFutureDateInGivenFormat(validFromDayCount,"yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(validTillDayCount,"yyyy-MM-dd HH:mm"));
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createPromotionWithActiveFlagZero(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withActive(0);
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createPromotionWithGivenActiveFlag(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues, int activeFlag) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm")).withActive(activeFlag);
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createExpiredPromotionWithGivenInputs(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.pastTimeDateAndGetDateInGivenFormat(2,"yyyy-MM-dd HH:mm")).withValidTill(Utility.pastTimeDateAndGetDateInGivenFormat(1,"yyyy-MM-dd HH:mm"));
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createFuturePromotionWithGivenInputs(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.pastTimeDateAndGetDateInGivenFormat(-1500,"yyyy-MM-dd HH:mm")).withValidTill(Utility.pastTimeDateAndGetDateInGivenFormat(-2500,"yyyy-MM-dd HH:mm"));
        Processor response= createPromotion( request);
        
        return response;
    }
    
    public Processor createFuturePromotionWithActiveFlag(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues, int activeFlag) throws IOException {
        CreatePromotion request = createPromotion.withDefaultValues().withRules(populateListOfRules(typeOfFact,comparisonOperator,values)).withActions(populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.pastTimeDateAndGetDateInGivenFormat(-1500,"yyyy-MM-dd HH:mm")).withValidTill(Utility.pastTimeDateAndGetDateInGivenFormat(-2500,"yyyy-MM-dd HH:mm")).withActive(activeFlag);
        Processor response= createPromotion( request);
        
        return response;
    }
    
    // deactivate promotion
    public Processor updatePromotionWithGivenInputs(String[] typeOfFact , String[] comparisonOperator, List<String[]> values,String[] actionType, String[] actionParam, String[] discountValues, String id) throws IOException {
        UpdatePromotion request = updatePromotion.withDefaultValues().withRules(updatePromotion.populateUpdateListOfRules(typeOfFact,comparisonOperator,values)).withActions(updatePromotion.populateListOfActions(actionType,actionParam,discountValues)).withValidFrom(Utility.getCurrentDateInGivenFormat("yyyy-MM-dd HH:mm")).withValidTill(Utility.getFutureDateInGivenFormat(30,"yyyy-MM-dd HH:mm"));
        
        Processor response= updatePromotion(request,id);
        
        return response;
    }
    
    
    // create Promotion API
    public Processor createPromotion(CreatePromotion request) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("user-agent","Swiggy-Android");
        header.put("version-code","439");
        header.put("Content-Type","application/json");
    
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("promotion", "createPromotion", gameOfThrone);
        Processor processor = new Processor(service, header, payload, null);
        return processor;
    
    }
    
    
    // create Promotion API
    public Processor updatePromotion(UpdatePromotion request, String id) throws IOException {
        HashMap<String, String> header = new HashMap<String, String>();
        header.put("user-agent","Swiggy-Android");
        header.put("version-code","439");
        header.put("Content-Type","application/json");
        String[] urlparams = new String[] { id };
        String payload[] = new String[]{jsonHelper.getObjectToJSON(request)};
        GameOfThronesService service = new GameOfThronesService("promotion", "updatePromotion", gameOfThrone);
        Processor processor = new Processor(service, header, payload, urlparams);
        return processor;
        
    }
    public ArrayList<Rules> populateListOfRules(String[] typeOfFact , String[] comparisonOperator, List<String[]> values) {
        ArrayList<Rules> rules = new ArrayList<Rules>();
        
        for (int i =0; i<typeOfFact.length ; i++){
        rules.add(i, new Rules().withValues(typeOfFact[i],comparisonOperator[i],values.get(i)));
        }
        return rules;
    }
    
    
    
    
    public ArrayList<Actions> populateListOfActions(String[] actionType, String[] actionParam, String[] values ){
    
        ArrayList<Actions> actions = new ArrayList<>();
        for (int i = 0 ; i <actionType.length; i++){
            actions.add(i,new Actions().withGivenValues(actionType[i],actionParam[i],values[i]));
        }
        return actions;
    }
    
    
    
}
