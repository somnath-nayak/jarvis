
package com.swiggy.api.daily.cms.pojo.PricingService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "product_id",
    "sku_id",
    "spin",
    "store_id"
})
public class Meta {

    @JsonProperty("product_id")
    private String product_id;
    @JsonProperty("sku_id")
    private String sku_id;
    @JsonProperty("spin")
    private String spin;
    @JsonProperty("store_id")
    private String store_id;

    @JsonProperty("product_id")
    public String getProduct_id() {
        return product_id;
    }

    @JsonProperty("product_id")
    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public Meta withProduct_id(String product_id) {
        this.product_id = product_id;
        return this;
    }

    @JsonProperty("sku_id")
    public String getSku_id() {
        return sku_id;
    }

    @JsonProperty("sku_id")
    public void setSku_id(String sku_id) {
        this.sku_id = sku_id;
    }

    public Meta withSku_id(String sku_id) {
        this.sku_id = sku_id;
        return this;
    }

    @JsonProperty("spin")
    public String getSpin() {
        return spin;
    }

    @JsonProperty("spin")
    public void setSpin(String spin) {
        this.spin = spin;
    }

    public Meta withSpin(String spin) {
        this.spin = spin;
        return this;
    }

    @JsonProperty("store_id")
    public String getStore_id() {
        return store_id;
    }

    @JsonProperty("store_id")
    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public Meta withStore_id(String store_id) {
        this.store_id = store_id;
        return this;
    }

    public Meta setData(String product_id,String sku_id,String spin,String store_id){
        this.withProduct_id(product_id).withSku_id(sku_id).withSpin(spin).withStore_id(store_id);
        return this;
    }

}
