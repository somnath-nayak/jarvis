package com.swiggy.api.daily.checkout.pojo.subscription;

import io.advantageous.boon.json.annotations.JsonProperty;

public class Location {


    @JsonProperty("lat")
    private Float lat;
    @JsonProperty("lng")
    private Float lng;

    @JsonProperty("lat")
    public Float getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Location withLat(Float lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public Float getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Float lng) {
        this.lng = lng;
    }

    public Location withLng(Float lng) {
        this.lng = lng;
        return this;
    }
}
