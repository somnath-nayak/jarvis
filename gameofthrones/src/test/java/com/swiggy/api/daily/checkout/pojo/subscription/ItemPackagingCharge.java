package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

public class ItemPackagingCharge {

    @JsonProperty("totalWithoutDiscount")
    private Long totalWithoutDiscount;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("discount")
    private Long discount;
    @JsonProperty("price")
    private Long price;
    @JsonProperty("tax")
    private Tax tax;
    @JsonProperty("inclusiveTax")
    private Boolean inclusiveTax;

    @JsonProperty("totalWithoutDiscount")
    public Long getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public ItemPackagingCharge withTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Long total) {
        this.total = total;
    }

    public ItemPackagingCharge withTotal(Long total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Long getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public ItemPackagingCharge withDiscount(Long discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("price")
    public Long getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Long price) {
        this.price = price;
    }

    public ItemPackagingCharge withPrice(Long price) {
        this.price = price;
        return this;
    }

    @JsonProperty("tax")
    public Tax getTax() {
        return tax;
    }

    @JsonProperty("tax")
    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public ItemPackagingCharge withTax(Tax tax) {
        this.tax = tax;
        return this;
    }

    @JsonProperty("inclusiveTax")
    public Boolean getInclusiveTax() {
        return inclusiveTax;
    }

    @JsonProperty("inclusiveTax")
    public void setInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public ItemPackagingCharge withInclusiveTax(Boolean inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
        return this;
    }

}
