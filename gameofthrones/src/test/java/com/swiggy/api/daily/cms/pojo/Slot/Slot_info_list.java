
package com.swiggy.api.daily.cms.pojo.Slot;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
    "recurrence_pattern",
    "start_time",
    "end_time",
    "live_date",
    "expiry_date",
    "is_inclusive"
})
public class Slot_info_list {
    @JsonProperty("id")
    private Long id;
    @JsonProperty("recurrence_pattern")
    private Recurrence_pattern recurrence_pattern;
    @JsonProperty("start_time")
    private String start_time;
    @JsonProperty("end_time")
    private String end_time;
    @JsonProperty("live_date")
    private Long live_date;
    @JsonProperty("expiry_date")
    private Long expiry_date;
    @JsonProperty("is_inclusive")
    private Boolean is_inclusive;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Slot_info_list() {
    }


    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public Slot_info_list withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("recurrence_pattern")
    public Recurrence_pattern getRecurrence_pattern() {
        return recurrence_pattern;
    }

    @JsonProperty("recurrence_pattern")
    public void setRecurrence_pattern(Recurrence_pattern recurrence_pattern) {
        this.recurrence_pattern = recurrence_pattern;
    }

    public Slot_info_list withRecurrence_pattern(Recurrence_pattern recurrence_pattern) {
        this.recurrence_pattern = recurrence_pattern;
        return this;
    }

    @JsonProperty("start_time")
    public String getStart_time() {
        return start_time;
    }

    @JsonProperty("start_time")
    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public Slot_info_list withStart_time(String start_time) {
        this.start_time = start_time;
        return this;
    }

    @JsonProperty("end_time")
    public String getEnd_time() {
        return end_time;
    }

    @JsonProperty("end_time")
    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public Slot_info_list withEnd_time(String end_time) {
        this.end_time = end_time;
        return this;
    }

    @JsonProperty("live_date")
    public Long getLive_date() {
        return live_date;
    }

    @JsonProperty("live_date")
    public void setLive_date(Long live_date) {
        this.live_date = live_date;
    }

    public Slot_info_list withLive_date(Long live_date) {
        this.live_date = live_date;
        return this;
    }

    @JsonProperty("expiry_date")
    public Long getExpiry_date() {
        return expiry_date;
    }

    @JsonProperty("expiry_date")
    public void setExpiry_date(Long expiry_date) {
        this.expiry_date = expiry_date;
    }

    public Slot_info_list withExpiry_date(Long expiry_date) {
        this.expiry_date = expiry_date;
        return this;
    }

    @JsonProperty("is_inclusive")
    public Boolean getIs_inclusive() {
        return is_inclusive;
    }

    @JsonProperty("is_inclusive")
    public void setIs_inclusive(Boolean is_inclusive) {
        this.is_inclusive = is_inclusive;
    }

    public Slot_info_list withIs_inclusive(Boolean is_inclusive) {
        this.is_inclusive = is_inclusive;
        return this;
    }


}
