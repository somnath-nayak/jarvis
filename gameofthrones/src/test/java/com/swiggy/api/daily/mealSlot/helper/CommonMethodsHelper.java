package com.swiggy.api.daily.mealSlot.helper;

import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class CommonMethodsHelper {

    SoftAssert softAssert = new SoftAssert();
    String cityID;

    public void successGetMealSlotAPIasserts(Processor getByCityId) throws Exception {
        if (getByCityId!=null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String user_id = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].id"));
            softAssert.assertNotNull(user_id);
            String city_id = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].city_id"));
            softAssert.assertEquals(city_id, MealSlotConstant.cityID_99);
            String delivery_start_time = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].delivery_start_time"));
            softAssert.assertNotNull(delivery_start_time);
            String start_time = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].start_time"));
            softAssert.assertNotNull(start_time);
            String end_time = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].end_time"));
            softAssert.assertNotNull(end_time);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
        }
    }

    public void statusCode1Assert(Processor getByCityId) throws Exception {
        if (getByCityId!=null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
        }
    }

    public void TodayOrTonightAssertsGetMealSlotServiceAPI(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            softAssert.assertNotEquals(day, MealSlotConstant.Random);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[1].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            softAssert.assertNotEquals(day1, MealSlotConstant.Random);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[2].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            softAssert.assertNotEquals(day2, MealSlotConstant.Random);
        }
    }

    public void randomAssertsGetMealSlotServiceAPI(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[0].slot"));
            softAssert.assertNotEquals(slot, MealSlotConstant.Random);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[1].slot"));
            softAssert.assertNotEquals(slot1, MealSlotConstant.Random);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValue("$.data[2].slot"));
            softAssert.assertNotEquals(slot2, MealSlotConstant.Random);
        }
    }

    public void time0000to0059Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.LATE_NIGHT);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Today);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.BREAKFAST);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.LUNCH);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Today);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.SNACKS);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Today);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.DINNER);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Today);
        }
    }

    public void time0100to0759Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.BREAKFAST);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.LUNCH);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Today);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.SNACKS);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Today);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.DINNER);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Today);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.LATE_NIGHT);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Tonight);
        }
    }

    public void time0800to1259Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.LUNCH);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.SNACKS);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Today);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.DINNER);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Today);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.LATE_NIGHT);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Tonight);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.BREAKFAST);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Tomorrow);
        }
    }

    public void time1300to1559Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.SNACKS);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.DINNER);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Today);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.LATE_NIGHT);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Tonight);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.BREAKFAST);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Tomorrow);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.LUNCH);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Tomorrow);
        }
    }

    public void time1600to1959Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.DINNER);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Today);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.LATE_NIGHT);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Tonight);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.BREAKFAST);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Tomorrow);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.LUNCH);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Tomorrow);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.SNACKS);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Tomorrow);
        }
    }

    public void time2000to2359Asserts(Processor getByCityId) throws Exception {
        if (getByCityId != null) {
            String statusCode = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].slot"));
            softAssert.assertEquals(slot, MealSlotConstant.LATE_NIGHT);
            String day = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[0].day"));
            softAssert.assertEquals(day, MealSlotConstant.Tonight);
            String slot1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].slot"));
            softAssert.assertEquals(slot1, MealSlotConstant.BREAKFAST);
            String day1 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[1].day"));
            softAssert.assertEquals(day1, MealSlotConstant.Tomorrow);
            String slot2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].slot"));
            softAssert.assertEquals(slot2, MealSlotConstant.LUNCH);
            String day2 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[2].day"));
            softAssert.assertEquals(day2, MealSlotConstant.Tomorrow);
            String slot3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].slot"));
            softAssert.assertEquals(slot3, MealSlotConstant.SNACKS);
            String day3 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[3].day"));
            softAssert.assertEquals(day3, MealSlotConstant.Tomorrow);
            String slot4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].slot"));
            softAssert.assertEquals(slot4, MealSlotConstant.DINNER);
            String day4 = String.valueOf(getByCityId.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[4].day"));
            softAssert.assertEquals(day4, MealSlotConstant.Tomorrow);
        }
    }

    public void createMealSlotSuccessAsserts(Processor createMealslot) throws Exception {
        if (createMealslot != null) {
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            softAssert.assertEquals(statusCode, MealSlotConstant.status_1);
            String slot = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slot"));
            softAssert.assertEquals(slot, MealSlotConstant.BREAKFAST);
            String id = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.id"));
            softAssert.assertNotNull(id);
            String start_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.start_time"));
            softAssert.assertNotNull(start_time);
            String end_time = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.end_time"));
            softAssert.assertNotNull(end_time);
        }
    }

    public void invalidCreateCityID(Processor createMealslot) throws Exception {
        if (createMealslot != null) {
            String statusCode = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
            Assert.assertEquals(statusCode, MealSlotConstant.status_0);
            String statusMessage = String.valueOf(createMealslot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage"));
            Assert.assertEquals(statusMessage, "cityId is Invalid");
        }
    }

    public void updateMealSlot(Processor updateMealSlot) throws Exception {
        String status_Code = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode"));
        softAssert.assertEquals(status_Code, MealSlotConstant.status_1);
        String cityID1 = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.city_id"));
        softAssert.assertEquals(cityID1, cityID);
        String slot1 = String.valueOf(updateMealSlot.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.slot"));
        softAssert.assertEquals(slot1, MealSlotConstant.DINNER);
    }


}
