package com.swiggy.api.daily.checkout.helper;

import scala.Int;

public interface DailyCheckoutConstants {

    int VALID_STATUS_CODE = 0;
    String VALID_SUCCESSFUL_MESSAGE = "done successfully";
    String USER_MOBILE="9899072197";
    String USER_PASSWORD="rkonowhere";

    String USER_MOBILE1="8197982351";
    String USER_PASSWORD1="welcome123";

    String USER_MOBILE2="7990012757";
    String USER_PASSWORD2="Uat@1234";

    String CONTENT_TYPE= "application/json";
    String USER_AGENT="Swiggy-Android";
    String VERSION_CODE="443";
    String DEVICE_ID="AUTOMATION_DEVICE_ID";
    String REFUND_AUTHORIZATION="Basic UGFhJDoyMDE3U1chR0dZ";
    String CITY_ID = "75";
    int STORE_ID = 39533;

    String STOREID = "39533";
    String COUPON_CODE = "ABCD";
    String ITEM_TYPE = "PRODUCT";
    double  LATITUDE = 17.9689;
    double LANGITUDE= 79.5941;

    String NAME= "Test_Name";
    String ADDRESS="Test_Address";
    String LANDMARK="Test_Landmark";
    String AREA="Test_Area";
    String  LAT= "17.9689";
    String LNG= "79.5941";
    String LAT1="1.325123";
    String LNG1="103.924639";

    String FLAT_NO="999";
    String CITY="Daily_Test_City";
    String ANNOTATION="Daily_Annotation";

    String VALID_DELETE_MESSAGE = "Address deleted successfully";


    String CART_SUBTYPE = "DAILY_PREORDER";
    String NONCHECKOUT_REASON = "Cart Is Valid";

    String TENANT_ID = "9012d90sdck3o48";
    String AUTHORIZATION = "Basic dXNlcjpjaGVjaw==";

    String PAYMENT_TYPE= "PRE_PAYMENT";
    String PAYMENT_METHOD= "Cash";


    String INVALID_SPINID = "BELHSXSXJK";
    int INVALID_STOREID = 12335;
    String[] USER_AGENTS = new String[]{"Swiggy-Android","Swiggy-Ios"};
    String[] VERSION_CODES = new String[]{"123","123"};

    String SUCCESS="success";
    String PENDING="PENDING";
    String SUCCESSFUL="SUCCESSFUL";

    String ORDER_CONTEXT= "ORDER_JOB";
    String PLACED= "PLACED";
    String CONFIRMED= "CONFIRMED";
    String ORDER_TYPE="DAILY";

    String TRUE="true";
    String FALSE="false";

    String ORDER_SUB_TYPE_SUBSCRIPTION= "DAILY_SUBSCRIPTION";
    String ORDER_SUB_TYPE_PREORDER= "DAILY_PREORDER";

    String PLAN="PLAN";
    String PRODUCT="PRODUCT";

    String TENANT="DAILY";

    String TEST_REFUND="TEST_REFUND";

    String CASH="Cash";
    String MOBIKWIK="Mobikwik-SSO";
    String PAYTM="PayTM-SSO";
    String PHONEPE="PhonePe";
    String FREECHARGE="Freecharge-SSO";

}
