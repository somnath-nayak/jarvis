package com.swiggy.api.daily.checkout.dp;

import com.swiggy.api.daily.checkout.helper.*;
import com.swiggy.api.daily.checkout.pojo.*;
import com.swiggy.api.daily.checkout.pojo.UserInfo;
import com.swiggy.api.daily.checkout.pojo.address.IsAddressServiceable;
import org.testng.annotations.DataProvider;

import java.util.*;

public class preorderCart {

    private CheckoutCartHelper cartHelper;
    private DailyCheckoutCommonUtils commonUtils;
    private IsAddressServiceable isAddressServiceable;
    private DailyAddressHelper dailyAddressHelper;
    private UserInfo userInfo;
    private String mobile;
    private String password;
    private Map<String, String> headerWithTidAndSid;
    private Map<String, String> allHeaders;
    private Map<String, String> userInfoHeader;
    String[] user_agents = DailyCheckoutConstants.USER_AGENTS;
    String[] version_codes = DailyCheckoutConstants.VERSION_CODES;
    Map<String, String> cartPLHeaders;


    public preorderCart() {
        cartHelper = new CheckoutCartHelper();
        userInfo = new UserInfo();
        mobile = System.getenv("mobile");
        password = System.getenv("password");
        commonUtils = new DailyCheckoutCommonUtils();

        if (mobile == null || mobile.isEmpty()) {
            mobile = DailyCheckoutConstants.USER_MOBILE;
        }

        if (password == null || password.isEmpty()) {
            password = DailyCheckoutConstants.USER_PASSWORD;
        } else {
            System.out.println("Please provide mobile and password");
        }



        allHeaders = commonUtils.dailyLogin(mobile, password);
        cartPLHeaders = commonUtils.setHeader(allHeaders.get("tid"),allHeaders.get("token"));
        userInfoHeader = commonUtils.getUserInfoHeader(allHeaders);
        headerWithTidAndSid = commonUtils.getHeader(userInfoHeader);

    }

    public Map<String, String> getUserInfoHeader(String user_agent, String version_code) {
        userInfoHeader = commonUtils.getUserInfoHeader(allHeaders, user_agent, version_code);
        return userInfoHeader;
    }
    public Map<String, String> getUserInfoHeader(String tid,String token,String user_agent, String version_code) {
        userInfoHeader = commonUtils.setHeader(tid,token,user_agent, version_code);
        return userInfoHeader;
    }

    @DataProvider(name = "AddressValidatorsDP")
    public Iterator<Object[]> AddressVerification() {
        List<Object[]> obj = new ArrayList<>();
        CartPayload cartPayload;
        String expectedStatusMessage = "";
        //String[] user_agents = DailyCheckoutConstants.USER_AGENTS;
        //String[] version_codes = DailyCheckoutConstants.VERSION_CODES;

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});


            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            cartPayload.setAddressId("1231231");
            expectedStatusMessage = "Invalid Address Id for User";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            cartPayload.setAddressId("");
            expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});
        }

        return obj.iterator();
    }


    @DataProvider(name = "MealSlotValidatorsDP")
    public Iterator<Object[]> mealSlotVerification() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);

            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            cartPayload.getCtx().setMealSlotId("1234");
            expectedStatusMessage = "Invalid meal slot id";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            cartPayload.getCtx().setMealSlotId("");
            expectedStatusMessage = "meal slot id cannot be null";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});
        }

        return obj.iterator();
    }

    @DataProvider(name = "AddonsValidatorsDP")
    public Iterator<Object[]> AddonsValidations() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);

            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Invalid Addon Spin
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(1).setSpin(DailyCheckoutConstants.INVALID_SPINID);
            expectedStatusMessage = "CMS ERROR Item Empty or Not Found";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Increasing addons quantity
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(1).setQuantity(2);
            expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Invalid Store ID
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(1).setStoreId(DailyCheckoutConstants.INVALID_STOREID);
            expectedStatusMessage = "Stores Not Found In Request or More than One";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});
        }

        return obj.iterator();
    }


    @DataProvider(name = "DefaultSlotValidatorsDP")
    public Iterator<Object[]> defaultSlotVerification() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);

            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});
        }

        return obj.iterator();
    }

    @DataProvider(name = "StoreItemsValidatorsDP")
    public Iterator<Object[]> ItemsVerification() {
        List<Object[]> obj = new ArrayList<>();
        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);

            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Invalid Item Spin
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(0).setSpin(DailyCheckoutConstants.INVALID_SPINID);
            expectedStatusMessage = "CMS ERROR Item Empty or Not Found";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Increasing Item quantity
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(0).setQuantity(2);
            expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

            //Verify Invalid Store ID
            cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, true);
            cartPayload.getItems().get(0).setStoreId(DailyCheckoutConstants.INVALID_STOREID);
            expectedStatusMessage = "Stores Not Found In Request or More than One";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

        }
        return obj.iterator();
    }

    @DataProvider(name = "ItemLevelPriceValidatorsDP")
    public Iterator<Object[]> ItemLevelPriceVerification() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);
            //Cart Calculations at item level
            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage});

        }

        return obj.iterator();
    }

    @DataProvider(name = "ItemLevelquantityIncreasePriceValidatorsDP")
    public Iterator<Object[]> ItemLevelPriceVerificationwithqyQuantityIncraesed() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(user_agents[i], version_codes[i]);

            //Cart calculation with item level increasing quantity
            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            cartPayload.getItems().get(0).setQuantity(2);
            String expectedStatusMessage1 = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage1});

        }

        return obj.iterator();
    }

    @DataProvider(name = "IsAddressServiceableDP")
    public Iterator<Object[]> IsAddressServiceableTest() {
        List<Object[]> obj = new ArrayList<>();

        for(int i=0;i<user_agents.length;i++) {
            userInfoHeader = getUserInfoHeader(allHeaders.get("tid"),allHeaders.get("token"),user_agents[i], version_codes[i]);
            CartPayload cartPayload = cartHelper.getDailyCartPayloadInternal("product", DailyCheckoutConstants.STOREID, headerWithTidAndSid, DailyCheckoutConstants.CITY_ID, false);
            String expectedStatusMessage = "Cart Is Valid";
            obj.add(new Object[]{cartPayload, userInfoHeader, expectedStatusMessage,cartPLHeaders});


        }

        return obj.iterator();
    }










}
