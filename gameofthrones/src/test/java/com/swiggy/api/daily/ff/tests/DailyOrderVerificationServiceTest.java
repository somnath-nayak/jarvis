package com.swiggy.api.daily.ff.tests;

import com.swiggy.api.erp.ff.constants.VerificationServiceConstants;
import com.swiggy.api.erp.ff.helper.LOSHelper;
import com.swiggy.api.erp.ff.helper.VerificationServiceHelper;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class DailyOrderVerificationServiceTest {

    VerificationServiceHelper helper = new VerificationServiceHelper();
    LOSHelper losHelper = new LOSHelper();
    Logger log = Logger.getLogger(DailyOrderVerificationServiceTest.class);


    @Test(groups = {"sanity", "regression"}, description = "check for daily orders not going for verification")
    public void checkForDailyOrderVerification() throws IOException, InterruptedException {
        log.info("***************************************** Daily orders not going for verification Test started *****************************************");
        helper.clearFactTable(VerificationServiceConstants.customerIds);
        String orderId = losHelper.getAnOrder("daily");
        Thread.sleep(2000);
        String metadata = helper.getMetaData(Long.parseLong(orderId));
        Assert.assertTrue(metadata.contains("\"verificationRequired\":false")&& metadata.contains("\"duplicateValidation\":false"));
        log.info("######################################### Daily orders not going for verification Test completed #########################################");
    }
}
