package com.swiggy.api.daily.cms.pojo.Slot;

import lombok.Data;
import lombok.Getter;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

@Data
@Getter
public class AvailabilityTimeline {

    @JsonProperty("entity_id")
    String entity_id ;
    @JsonProperty("type")
    String type ;
    @JsonProperty("meta")
    AvailMeta meta ;
    @JsonProperty("data")
    AvailData data;

    @Data
    public static class AvailMeta {
        @JsonProperty("store_id")
        String store_id;
        @JsonProperty("spin")
        String spin ;
        @JsonProperty("product_id")
        String product_id;
        @JsonProperty("sku_id")
        String sku_id;

        public String getStore_id() {

            return store_id;
        }

        public void setStore_id(String store_id) {
            this.store_id = store_id;
        }

        public String getSpin() {
            return spin;
        }

        public void setSpin(String spin) {
            this.spin = spin;
        }

        public String getProduct_id() {
            return product_id;
        }

        public void setProduct_id(String product_id) {
            this.product_id = product_id;
        }

        public String getSku_id() {
            return sku_id;
        }

        public void setSku_id(String sku_id) {
            this.sku_id = sku_id;
        }
    }

    @Data
    public static class AvailData {

        @JsonProperty("id")
        String id;
        @JsonProperty("parentIds")
        List<String> parentIds;
        @JsonProperty("slot_timings")
        List<AvailSlot> slot_timings;
        @JsonProperty("meta")
        AvailMeta meta;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<String> getParentIds() {
            return parentIds;
        }

        public void setParentIds(List<String> parentIds) {
            this.parentIds = parentIds;
        }

        public List<AvailSlot> getSlot_timings() {
            return slot_timings;
        }

        public void setSlot_timings(List<AvailSlot> slot_timings) {
            this.slot_timings = slot_timings;
        }

        public AvailMeta getMeta() {
            return meta;
        }

        public void setMeta(AvailMeta meta) {
            this.meta = meta;
        }
    }

    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AvailMeta getMeta() {
        return meta;
    }

    public void setMeta(AvailMeta meta) {
        this.meta = meta;
    }

    public AvailData getData() {
        return data;
    }

    public void setData(AvailData data) {
        this.data = data;
    }
}
