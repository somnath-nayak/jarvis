package com.swiggy.api.daily.cms.tests;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.util.*;

public class SlotSevice {
    CMSDailyHelper helper=new CMSDailyHelper();
    SchemaValidatorUtils jso=new SchemaValidatorUtils();
    String slot="";
    String slotholiday="";
    List<String> list= new ArrayList<>();

    @Test(description = "This test will create Normal slot for Product and Plan with different type of valid data",dataProviderClass = CMSDailyDP.class, dataProvider = "createslot",priority = 0)
    public void createSlot(String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive,int count) throws Exception {
        String response=helper.createSlot(entitytype,interval,days,freq,starttime,endtime,live_date,exp_date,is_exclusive,count).ResponseValidator.GetBodyAsText();
        slot=JsonPath.read(response,"$.data..slot_info_list..id").toString().replace("[","").replace("]","");
        list.add(slot);
        List<String> myList = new ArrayList<String>(Arrays.asList(slot.split(",")));
        Assert.assertEquals(myList.size(),count);
        Assert.assertNotNull(JsonPath.read(response,"$.data"));
    }



    @Test(description = "This test will verify non-creation of  slot for Product and Plan with different type of invalid data",dataProviderClass = CMSDailyDP.class, dataProvider = "createslotnegative",priority = 0)
    public void createSlotNegativeCases(String entityid,Integer storeid,String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive,int count) throws Exception {
        String response=helper.createSlotNegative(entityid,storeid,entitytype,interval,days,freq,starttime,endtime,live_date,exp_date,is_exclusive,count).ResponseValidator.GetBodyAsText();
                if(response.contains("null")||response.contains("Bad Request")||response.contains("Invalid")){
                    Assert.assertTrue(true,"Some thing wrong");
                }
                else if(response.contains("id")||response.contains("entity_id")){
                    Assert.assertFalse(false,"Some thing wrong");

                }
    }

    @Test(description = "This test will create Holiday slot for Product and Plan with different type of valid data",dataProviderClass = CMSDailyDP.class, dataProvider = "createholidayslot",priority = 0)
    public void createHolidaySlot(String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive,int count) throws Exception {
        String response=helper.createSlot(entitytype,interval,days,freq,starttime,endtime,live_date,exp_date,is_exclusive,count).ResponseValidator.GetBodyAsText();
        slotholiday=JsonPath.read(response,"$.data..slot_info_list..id").toString().replace("[","").replace("]","");
        List<String> myList = new ArrayList<String>(Arrays.asList(slotholiday.split(",")));
        Assert.assertEquals(myList.size(),count);
        Assert.assertNotNull(JsonPath.read(response,"$.data"));

    }

    /***********************************UPDATE-SLOT*********************************************************************/
    @Test(description = "This test will update slot information from slot-id with valid input",dataProviderClass = CMSDailyDP.class, dataProvider = "updateslot",priority = 1)
    public void upadteSlot(String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive,int count) throws Exception{
        String response=helper.updateSlot(Long.parseLong(slot),entitytype,interval,days,freq,starttime,endtime,live_date,exp_date,is_exclusive,count).ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(JsonPath.read(response,"$.data"));

    }

    @Test(description = "This test will update slot information from slot-id with wrong data",dataProviderClass = CMSDailyDP.class, dataProvider = "createslotnegative",priority = 1)
    public void upadteSlotNegative(String entityid,Integer storeid,String entitytype, Integer interval, List days, String freq, String starttime, String endtime, Long live_date, Long exp_date, boolean is_exclusive,int count) throws Exception{
        String response=helper.updateSlotNegative(Long.parseLong(slot),entityid,storeid,entitytype,interval,days,freq,starttime,endtime,live_date,exp_date,is_exclusive,count).ResponseValidator.GetBodyAsText();
        if(response.contains("null")||response.contains("Bad Request")||response.contains("Invalid")){
            Assert.assertTrue(true,"Some thing wrong");
        }
    }

    /***********************************GET-SLOT*********************************************************************/
    @Test(dataProviderClass = CMSDailyDP.class,dataProvider = "slotexpand",description = "This test will fetch slot information from slot-id and expand days",priority = 2)
    public void getSlot(String evalue,int days){
        String response=helper.getSlotbySlotID(Integer.parseInt(slot),evalue,days).ResponseValidator.GetBodyAsText();
        String slotid= JsonPath.read(response,"$.data.slot_info_list[0].id").toString();
        String statucode= JsonPath.read(response,"$.statusCode").toString();
        Assert.assertEquals(slotid,slot);
        Assert.assertEquals(statucode,"1");
        if(evalue=="true"){
            Assert.assertTrue(response.contains("start_epoch"));
            Assert.assertTrue(response.contains("end_epoch"));
        }
        else if(evalue=="false"){
            Assert.assertFalse(response.contains("start_epoch"));
            Assert.assertFalse(response.contains("end_epoch"));
        }


   }

    @Test(description = "This test will fetch slot information from by store-id ",priority = 2)
    public void getSlotByStoreID(){
        String response=helper.getSlotbyStoreID(CMSDailyContants.store_id.toString()).ResponseValidator.GetBodyAsText();
        String storetid= JsonPath.read(response,"$.data.store_id").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertNotNull(storetid);
        Assert.assertEquals(storetid,CMSDailyContants.store_id.toString());
    }

    @Test(description = "This test will fetch entityid by Entity Type ",priority = 2)
    public void getEntityIDByEntityType(){
        String response=helper.getEntityByEntityType(CMSDailyContants.entity_type.get(0).toString()).ResponseValidator.GetBodyAsText();
        String entityid= JsonPath.read(response,"$.data").toString().replace("[","").replace("]","").replace("\"","");
        List<String> list = Arrays.asList(entityid.split("\\s*,\\s*"));
        Assert.assertNotNull(list);
        Assert.assertTrue(list.contains(CMSDailyContants.entityid));
    }

    @Test(description = "This test will fetch entityid by Entity Type plan and Store-ID ",priority = 2)
    public void getEntityIDByEntityTypePlanandStoreID(){
        String response=helper.getEntityByStoreIAndEntityType(CMSDailyContants.store_id.toString(),CMSDailyContants.entity_type.get(0).toString()).ResponseValidator.GetBodyAsText();
        String entityid= JsonPath.read(response,"$.data[0]").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entityid,CMSDailyContants.entityid);
        Assert.assertNotNull(entityid);
    }

    @Test(description = "This test will fetch entityid by Entity Type product and Store-ID ",priority = 2)
    public void getEntityIDByEntityTypeProductndStoreID(){
        String response=helper.getEntityByStoreIAndEntityType(CMSDailyContants.store_id.toString(),CMSDailyContants.entity_type.get(1).toString()).ResponseValidator.GetBodyAsText();
        String entityid= JsonPath.read(response,"$.data[0]").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entityid,CMSDailyContants.entityid);
        Assert.assertNotNull(entityid);
    }

    @Test(description = "This test will fetch entityid by wrong Entity Type ",priority = 2,dataProviderClass = CMSDailyDP.class,dataProvider = "entitytypenegative")
    public void getEntityIDByEntityTypeNegative(String entitytype){
        Processor response=helper.getEntityByEntityType(entitytype);
        int statuscode=response.ResponseValidator.GetResponseCode();
        Assert.assertEquals(statuscode,400);

    }

    @Test(description = "This test will fetch entityid by Entity Type product and Store-ID ",priority = 2,dataProviderClass = CMSDailyDP.class,dataProvider = "entitytypeandstorenegative")
    public void getEntityIDByEntityTypeStoreIDNegativeCases(Integer storeid,String etype){
        Processor response=helper.getEntityByStoreIAndEntityType(storeid.toString(),etype);
        int statuscode=response.ResponseValidator.GetResponseCode();
        Assert.assertEquals(statuscode,400);

    }

    @Test(description = "This test will not fetch slot with invalid store-id ",priority = 2,dataProviderClass = CMSDailyDP.class,dataProvider = "storeidnegative")
    public void getSlotByStoreIdNegative(Integer store){
        String response=helper.getSlotbyStoreID(store.toString()).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statusmsg,CMSDailyContants.message_entity_not_exist);
    }


    @Test(dataProviderClass = CMSDailyDP.class, dataProvider = "slotnegative",description = "This test will fetch test slot information from invalid slot-id",priority = 2)
    public void getSlotNegativeCase(int sid,String expand,int days){
        String response=helper.getSlotbySlotID(sid,expand,days).ResponseValidator.GetBodyAsText();
        String statuscode= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statuscode,CMSDailyContants.message_slot_not_exist);

    }

    @Test(dataProviderClass = CMSDailyDP.class,dataProvider = "slotexpand",description = "This test will fetch test slot information valid",priority = 2)
    public void getSlotByEntityID(String evalue,int days){
        String response=helper.getSlotbyEntityID(CMSDailyContants.entityid,evalue,days).ResponseValidator.GetBodyAsText();
        String entityid= JsonPath.read(response,"$.data..entity_id").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entityid,CMSDailyContants.entityid);
        if(evalue=="true"){
            Assert.assertTrue(response.contains("start_epoch"));
            Assert.assertTrue(response.contains("end_epoch"));
        }
        else if(evalue=="false"){
            Assert.assertFalse(response.contains("start_epoch"));
            Assert.assertFalse(response.contains("end_epoch"));
        }



    }

    @Test(dataProviderClass = CMSDailyDP.class, dataProvider = "slotnegative",description = "This test will fetch test slot information from invalid entity-id",priority = 2)
    public void getSlotByEntityNegativeCase(int sid,String expand,int days){
        String response=helper.getSlotbyEntityID(String.valueOf(sid),expand,days).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statusmsg,CMSDailyContants.message_entity_not_exist);

    }

    @Test(dataProviderClass = CMSDailyDP.class,dataProvider = "slotexpand",description = "This test will fetch holiday slot information from by entity-id",priority = 2)
    public void getHolidaySlotByEntityID(String evalue,int days){
        String response=helper.getHolidaySlotbyEntityID(CMSDailyContants.entityid,evalue,days).ResponseValidator.GetBodyAsText();
        String entityid= JsonPath.read(response,"$.data..entity_id").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entityid,CMSDailyContants.entityid);
        if(evalue=="true"){
            Assert.assertTrue(response.contains("start_epoch"));
            Assert.assertTrue(response.contains("end_epoch"));
        }
        else if(evalue=="false"){
            Assert.assertFalse(response.contains("start_epoch"));
            Assert.assertFalse(response.contains("end_epoch"));
        }



    }

    @Test(dataProviderClass = CMSDailyDP.class, dataProvider = "slotnegative",description = "This test will fetch test holiday slot information from invalid entity-id",priority = 2)
    public void getHolidaySlotByEntityNegativeCase(int sid,String expand,int days){
        String response=helper.getHolidaySlotbyEntityID(String.valueOf(sid),expand,days).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statusmsg,CMSDailyContants.message_entity_not_exist);//Message is Incorrect

    }

    /***********************************DELETE-SLOT*********************************************************************/
    @Test(description = "This test will delete a slot using slot-id",priority = 3)
    public void deleteSlotByID(){
        String response=helper.deleteSlotbySlotID(Integer.parseInt(slot)).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString();
        String statuscode= JsonPath.read(response,"$.statusCode").toString();
        Assert.assertEquals(data,"success");
        Assert.assertEquals(statuscode,"1");

    }

    @Test(dataProviderClass = CMSDailyDP.class, dataProvider = "deleteslot",description = "This test will delete a slot using slot-id that is not existing",priority = 3)
    public void deleteSlotByIDNegativeCases(int slot){
        String response=helper.deleteSlotbySlotID(slot).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statusmsg, CMSDailyContants.message_slot_not_exist);

    }


    @Test(dataProviderClass = CMSDailyDP.class, dataProvider = "deleteslot",description = "This test will delete a slot using entity-id that is not existing",priority = 3)
    public void deleteSlotByEntityIDNegativeCases(int sid){
        String response=helper.deleteSlotbySlotID(sid).ResponseValidator.GetBodyAsText();
        String statusmsg= JsonPath.read(response,"$.statusMessage").toString();
        Assert.assertEquals(statusmsg,CMSDailyContants.message_slot_not_exist);//Message not correct

    }

    @AfterTest(description = "This test will delete all slot to by entity-id")
    public void deleteSlotByEntityID(){
        String response=helper.deleteSlotbyEntityID(CMSDailyContants.entityid).ResponseValidator.GetBodyAsText();
        String data= JsonPath.read(response,"$.data").toString();
        String statuscode= JsonPath.read(response,"$.statusCode").toString();
        Assert.assertEquals(data,"success");
        Assert.assertEquals(statuscode,"1");
    }


}