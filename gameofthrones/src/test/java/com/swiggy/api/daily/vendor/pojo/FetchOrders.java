package com.swiggy.api.daily.vendor.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class FetchOrders {
    
    @JsonProperty("fromTime")
    private String fromTime;
    @JsonProperty("toTime")
    private String toTime;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("latLong")
    private LatLong latLong;
    @JsonProperty("cityId")
    private String cityId;
    
    public String getFromTime() {
        return fromTime;
    }
    
    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }
    
    public String getToTime() {
        return toTime;
    }
    
    public void setToTime(String toTime) {
        this.toTime = toTime;
    }
    
    public String getRestaurantId() {
        return restaurantId;
    }
    
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }
    
    public LatLong getLatLong() {
        return latLong;
    }
    
    public void setLatLong(LatLong latLong) {
        this.latLong = latLong;
    }
    
    public String getCityId() {
        return cityId;
    }
    
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }
    
    public FetchOrders withFromTime(String fromTime){
        this.fromTime = fromTime;
        return this;
    }
    
    public FetchOrders withToTime (String toTime){
        this.toTime = toTime;
        return this;
    }
    
    public FetchOrders withRestaurantId(String restaurantId){
        this.restaurantId = restaurantId;
        return this;
    }
    public FetchOrders withLatLong(LatLong latLong){
        this.latLong = latLong;
        return this;
    }
    public FetchOrders withCityId(String cityId){
        this.cityId = cityId;
        return this;
    }
    
    public FetchOrders fetchOrdersData(String fromTime , String toTime , String restaurantId , String lat , String lon , String cityId){
        withFromTime(fromTime).withToTime(toTime).withRestaurantId(restaurantId).withLatLong( new LatLong(lat,lon)).withCityId(cityId);
    
        
        return this;
    }
    
    
}
