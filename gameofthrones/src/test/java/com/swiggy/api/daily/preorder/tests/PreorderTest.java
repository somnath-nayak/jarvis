package com.swiggy.api.daily.preorder.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.Utility.GenerateRandomUtils;
import com.swiggy.api.daily.common.DateUtility;
import com.swiggy.api.daily.preorder.dp.PreorderDp;
import com.swiggy.api.daily.preorder.helper.DailyPreorderHelper;
import com.swiggy.api.daily.preorder.pojo.CreatePreorederPojo;
import com.swiggy.api.daily.subscription.helper.SubscriptionHelper;
import com.swiggy.api.daily.subscription.test.CreateSubscriptionTest;
import com.swiggy.api.sf.rng.helper.RandomNumber;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.io.IOException;

public class PreorderTest extends PreorderDp {


    DailyPreorderHelper preorderHelper = new DailyPreorderHelper();
    SubscriptionHelper subscriptionHelper = new SubscriptionHelper();
    JsonHelper jsonHelper=new JsonHelper();
    CreateSubscriptionTest createSubscriptionTest = new CreateSubscriptionTest();
    String orderId="";

    @BeforeClass
    public void beforeClass()
    {
        /*
        Yet to implement
         */
    }

    @Test(dataProvider = "createPreorderPositive", priority = 0)
    public void testCreatePreorderPostiveCases(String request) throws IOException {
        SoftAssert softAssert = new SoftAssert();

        String response = preorderHelper.createPreorder(request).ResponseValidator.GetBodyAsText();
        //.toString().replace("[", "").replace("]", "").replace("\"", "");
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        verifyCommonNodes(request,response);
        orderId = JsonPath.read(request, "$.order_id");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPreorderNegativeItemDetails", priority = 1)
    public void testCreatePreorderNegativeCasesItemDetails(String request,String errorMessage) throws IOException {

        SoftAssert softAssert = new SoftAssert();

        String response = preorderHelper.createPreorder(request).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");

        softAssert.assertTrue(message.equalsIgnoreCase(errorMessage),
                "Expected error Message is:[" +errorMessage+ "] But response says:["+message+ "]");
        softAssert.assertTrue(status == 0, "The status is not 1");
        softAssert.assertTrue( JsonPath.read(response, "$.data") == null, "Data should come as Null");
        softAssert.assertAll();
    }

    @Test(dataProvider = "createPreorderNegativeTimeSlot", priority = 1)
    public void testCreatePreorderNegativeTimeSlot(String request,String errorMessage) throws IOException {
        SoftAssert softAssert = new SoftAssert();

        String response = preorderHelper.createPreorder(request).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");

        softAssert.assertTrue(message.equalsIgnoreCase(errorMessage),
                "Expected error Message is:[" +errorMessage+ "] But response says:["+message+ "]");
        softAssert.assertTrue(status == 0, "The status is not 1");
        softAssert.assertTrue( JsonPath.read(response, "$.data") == null, "Data should come as Null");
        softAssert.assertAll();
    }


    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Customer",enabled = false)
    public void cancelPreorderMealCustomer(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();
        Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("CUSTOMER",Integer.parseInt(orderId),"DAILY_PREORDER" , "No Reason");
        String cancelRes = processor.ResponseValidator.GetBodyAsText();
        int canStatus = JsonPath.read(cancelRes, "$.statusCode");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"),"success",
                "The id has not shown");
        Assert.assertTrue(canStatus == 1, "The status is not 1");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"),"CANCELLED",
                "The status has not cancelled");

        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"),orderId,
                "The id has not shown");

    }
  // Made it enabled = false because the full dev is not completed so far.
    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Vendor",enabled = false)
    public void cancelPreorderMealVendor(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();
        Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("VENDOR",Integer.parseInt(orderId),"DAILY_PREORDER" , "No Reason");
        String cancelRes = processor.ResponseValidator.GetBodyAsText();
        int canStatus = JsonPath.read(cancelRes, "$.statusCode");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"),"success",
                "The id has not shown");
        Assert.assertTrue(canStatus == 1, "The status is not 1");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"),"CANCELLED",
                "The status has not cancelled");

        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"),orderId,
                "The id has not shown");

    }

    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Swiggy",enabled = false)
    public void cancelPreorderMealSwiggy(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();
        Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("SWIGGY",Integer.parseInt(orderId),"DAILY_PREORDER" , "No Reason");
        String cancelRes = processor.ResponseValidator.GetBodyAsText();
        int canStatus = JsonPath.read(cancelRes, "$.statusCode");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"),"success",
                "The id has not shown");
        Assert.assertTrue(canStatus == 1, "The status is not 1");
        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"),"CANCELLED",
                "The status has not cancelled");

        Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"),orderId,
                "The id has not shown");

    }

    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Customer for future meal",enabled = false)
    public void cancelPreorderMealCustomerFutureMeal(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();

        try {
            preorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            DailyPreorderHelper dailyPreorderHelper = new DailyPreorderHelper();
            dailyPreorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("CUSTOMER", Integer.parseInt(orderId), "DAILY_PREORDER", "No Reason");
            String cancelRes = processor.ResponseValidator.GetBodyAsText();
            int canStatus = JsonPath.read(cancelRes, "$.statusCode");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"), "success",
                    "The id has not shown");
            Assert.assertTrue(canStatus == 1, "The status is not 1");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"), "CANCELLED",
                    "The status has not cancelled");

            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"), orderId,
                    "The id has not shown");
        }
        finally {
            //createSubscriptionTest.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Vendor for future meal",enabled = false)
    public void cancelPreorderMealVendorFutureMeal(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();

        try {
            preorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            DailyPreorderHelper dailyPreorderHelper = new DailyPreorderHelper();
            dailyPreorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("VENDOR", Integer.parseInt(orderId), "DAILY_PREORDER", "No Reason");
            String cancelRes = processor.ResponseValidator.GetBodyAsText();
            int canStatus = JsonPath.read(cancelRes, "$.statusCode");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"), "success",
                    "The id has not shown");
            Assert.assertTrue(canStatus == 1, "The status is not 1");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"), "CANCELLED",
                    "The status has not cancelled");

            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"), orderId,
                    "The id has not shown");
        }
        finally {
            //createSubscriptionTest.changeSeverCurrentDate();
        }
    }

    @Test(dataProvider = "cancelPreorderPositive",description = "Cancel Preorder Meal Positive By Swiggy for future meal",enabled = false)
    public void cancelPreorderMealSwiggyFutureMeal(String payload) throws IOException {
        String id = "";
        SoftAssert softAssert = new SoftAssert();
        String response = preorderHelper.createPreorder(payload).ResponseValidator.GetBodyAsText();
        int status = JsonPath.read(response, "$.statusCode");
        String message = JsonPath.read(response, "$.statusMessage");
        softAssert.assertTrue(status == 1, "The status is not 1");
        softAssert.assertTrue(message.equalsIgnoreCase("success"),"The status message is not success");
        orderId = JsonPath.read(response, "$.data.order_id");
        //id = JsonPath.read(response, "$data.id");
        softAssert.assertAll();

        try {
            preorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            DailyPreorderHelper dailyPreorderHelper = new DailyPreorderHelper();
            dailyPreorderHelper.changeServerDateTimeOfDailyManager(DateUtility.getFutureDateInReadableFormat(-1));
            Processor processor = preorderHelper.cancelPreorderAndSubscriptionMeal("SWIGGY", Integer.parseInt(orderId), "DAILY_PREORDER", "No Reason");
            String cancelRes = processor.ResponseValidator.GetBodyAsText();
            int canStatus = JsonPath.read(cancelRes, "$.statusCode");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.statusMessage"), "success",
                    "The id has not shown");
            Assert.assertTrue(canStatus == 1, "The status is not 1");
            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.status"), "CANCELLED",
                    "The status has not cancelled");

            Assert.assertEquals(JsonPath.read(cancelRes, "$.data.id"), orderId,
                    "The id has not shown");
        }
        finally {
           // createSubscriptionTest.changeSeverCurrentDate();
        }
    }


    public void verifyCommonNodes(String request,String response)
    {

        SoftAssert softAssert = new SoftAssert();
        //.toString().replace("[", "").replace("]", "").replace("\"", "");

        softAssert.assertEquals(JsonPath.read(request, "$.user_id").toString()
                              , JsonPath.read(response, "$.data.user_id").toString(),
                    "The userID does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.store_id").toString()
                , JsonPath.read(response, "$.data.store_id").toString(),
                "The storeId does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.delivery_start_time").toString()
                , JsonPath.read(response, "$.data.delivery_start_time").toString(),
                "The delivery_start_time does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.delivery_end_time").toString()
                , JsonPath.read(response, "$.data.delivery_end_time").toString(),
                "The delivery_end_time does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.entity_details..item_id").toString()
                , JsonPath.read(response, "$.data.entity_details.items..itemId").toString(),
                "The item_details..item_id does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.entity_details..quantity").toString()
                , JsonPath.read(response, "$.data.entity_details.items..quantity").toString(),
                "The item_details..quantity does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.delivery_address_details.address").toString()
                , JsonPath.read(response, "$.data.delivery_address_details.address").toString(),
                "The address_details.address does not match");

        softAssert.assertEquals(JsonPath.read(request, "$.delivery_address_details.mobile").toString()
                , JsonPath.read(response, "$.data.delivery_address_details.mobile").toString(),
                "address_details.mobile done not match");

        softAssert.assertAll();
    }
    public String getFreshOrderId() throws IOException {
        CreatePreorederPojo preorederPojo=new CreatePreorederPojo();
        preorederPojo.setDefaultData();
        String creatPayload = jsonHelper.getObjectToJSON(preorederPojo);
        String createresponse = preorderHelper.createPreorder(creatPayload).ResponseValidator.GetBodyAsText();
        return JsonPath.read(createresponse, "$.data.order_id");

    }
}
