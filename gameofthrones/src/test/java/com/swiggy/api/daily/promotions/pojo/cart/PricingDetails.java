package com.swiggy.api.daily.promotions.pojo.cart;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class PricingDetails {
    
    @JsonProperty("packagingCharges")
    private PackagingCharges packagingCharges;
    @JsonProperty("itemTotal")
    private Integer itemTotal;
    @JsonProperty("deliveryPricingDetails")
    private DeliveryPricingDetails deliveryPricingDetails;
    @JsonProperty("cartTotal")
    private Double cartTotal;
    
    public PackagingCharges getPackagingCharges() {
        return packagingCharges;
    }
    
    public void setPackagingCharges(PackagingCharges packagingCharges) {
        this.packagingCharges = packagingCharges;
    }
    
    public Integer getItemTotal() {
        return itemTotal;
    }
    
    public void setItemTotal(Integer itemTotal) {
        this.itemTotal = itemTotal;
    }
    
    public DeliveryPricingDetails getDeliveryPricingDetails() {
        return deliveryPricingDetails;
    }
    
    public void setDeliveryPricingDetails(DeliveryPricingDetails deliveryPricingDetails) {
        this.deliveryPricingDetails = deliveryPricingDetails;
    }
    
    public Double getCartTotal() {
        return cartTotal;
    }
    
    public void setCartTotal(Double cartTotal) {
        this.cartTotal = cartTotal;
    }
    
    public PricingDetails withPackagaingCharges(PackagingCharges packagingCharges){
        
        setPackagingCharges( packagingCharges);
        return this;
    }
    
    public PricingDetails withItemTotal(Integer itemTotal){
        
        setPackagingCharges( packagingCharges);
        return this;
    }
    
  public  PricingDetails pricingDetailsWithGivenInput(PackagingCharges packagingCharges, Integer itemTotal ,DeliveryPricingDetails deliveryPricingDetails,Double cartTotal){
        setPackagingCharges(packagingCharges);setItemTotal(itemTotal);setDeliveryPricingDetails(deliveryPricingDetails);setCartTotal(cartTotal);
        return this;
  }
  
//public PricingDetails withGivenValues( Integer itemTotal ){
//        List<PricingDetails> pricingDetailsList = new ArrayList<>();
//        pricingDetailsList.add(0,withDefaultValues(itemTotal));
//        return pricingDetailsList;
//}
  



  public PricingDetails withGivenValues(Integer itemTotal){
        setCartTotal(745.5);
        setItemTotal(itemTotal);
        setDeliveryPricingDetails(new DeliveryPricingDetails().deliveryPricingDetailsListWithValues());
        setPackagingCharges(new PackagingCharges().withDefaultValues());
        return this;
  }
  

}
