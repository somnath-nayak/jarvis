package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class ItemBill {

    @JsonProperty("totalWithoutDiscount")
    private Long totalWithoutDiscount;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("discount")
    private Long discount;
    @JsonProperty("itemCharge")
    private ItemCharge itemCharge;
    @JsonProperty("itemPackagingCharge")
    private ItemPackagingCharge itemPackagingCharge;
    @JsonProperty("promotions")
    private List<Object> promotions = null;

    @JsonProperty("totalWithoutDiscount")
    public Long getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public ItemBill withTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Long total) {
        this.total = total;
    }

    public ItemBill withTotal(Long total) {
        this.total = total;
        return this;
    }

    @JsonProperty("discount")
    public Long getDiscount() {
        return discount;
    }

    @JsonProperty("discount")
    public void setDiscount(Long discount) {
        this.discount = discount;
    }

    public ItemBill withDiscount(Long discount) {
        this.discount = discount;
        return this;
    }

    @JsonProperty("itemCharge")
    public ItemCharge getItemCharge() {
        return itemCharge;
    }

    @JsonProperty("itemCharge")
    public void setItemCharge(ItemCharge itemCharge) {
        this.itemCharge = itemCharge;
    }

    public ItemBill withItemCharge(ItemCharge itemCharge) {
        this.itemCharge = itemCharge;
        return this;
    }

    @JsonProperty("itemPackagingCharge")
    public ItemPackagingCharge getItemPackagingCharge() {
        return itemPackagingCharge;
    }

    @JsonProperty("itemPackagingCharge")
    public void setItemPackagingCharge(ItemPackagingCharge itemPackagingCharge) {
        this.itemPackagingCharge = itemPackagingCharge;
    }

    public ItemBill withItemPackagingCharge(ItemPackagingCharge itemPackagingCharge) {
        this.itemPackagingCharge = itemPackagingCharge;
        return this;
    }

    @JsonProperty("promotions")
    public List<Object> getPromotions() {
        return promotions;
    }

    @JsonProperty("promotions")
    public void setPromotions(List<Object> promotions) {
        this.promotions = promotions;
    }

    public ItemBill withPromotions(List<Object> promotions) {
        this.promotions = promotions;
        return this;
    }

}
