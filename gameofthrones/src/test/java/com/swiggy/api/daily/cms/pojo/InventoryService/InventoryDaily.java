
package com.swiggy.api.daily.cms.pojo.InventoryService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "key",
        "inventory_construct",
        "meta"
})
public class InventoryDaily {

    @JsonProperty("key")
    private String key;
    @JsonProperty("inventory_construct")
    private Inventory_construct inventory_construct;
    @JsonProperty("meta")
    private Meta meta;

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public InventoryDaily withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("inventory_construct")
    public Inventory_construct getInventory_construct() {
        return inventory_construct;
    }

    @JsonProperty("inventory_construct")
    public void setInventory_construct(Inventory_construct inventory_construct) {
        this.inventory_construct = inventory_construct;
    }

    public InventoryDaily withInventory_construct(Inventory_construct inventory_construct) {
        this.inventory_construct = inventory_construct;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public InventoryDaily withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public InventoryDaily setData(String key,String pid,String skuid,String spinid,String storeid,Long startepc,Long endepoc,Long slotid,Integer maxcap,Integer soldcount){
        Meta m=new Meta().setData(pid, skuid, spinid, storeid);
        Inventory_construct inv=new Inventory_construct().setData(startepc, endepoc, slotid, maxcap, soldcount);
        this.withInventory_construct(inv).withKey(key).withMeta(m);
        return this;
    }

}