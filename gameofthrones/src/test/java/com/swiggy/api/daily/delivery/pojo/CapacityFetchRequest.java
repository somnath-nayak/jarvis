package com.swiggy.api.daily.delivery.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CapacityFetchRequest {
    @JsonProperty("customer_location")
    GeoLocation customerLocation;

    @JsonProperty("delivery_slots")
    List<EpochSlot> deliverySlot;

    @JsonProperty("unique_id")
    String uniqueId;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public GeoLocation getCustomerLocation() {
        return customerLocation;
    }

    public void setCustomerLocation(GeoLocation customerLocation) {
        this.customerLocation = customerLocation;
    }

    public List<EpochSlot> getDeliverySlot() {
        return deliverySlot;
    }

    public void setDeliverySlot(List<EpochSlot> deliverySlot) {
        this.deliverySlot = deliverySlot;
    }
}