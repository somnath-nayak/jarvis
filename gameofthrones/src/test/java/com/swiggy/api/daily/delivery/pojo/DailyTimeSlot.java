package com.swiggy.api.daily.delivery.pojo;


import com.fasterxml.jackson.annotation.JsonProperty;

public class DailyTimeSlot {
//    @JsonProperty
//    private Long id;
    @JsonProperty
    private Integer zone;
    @JsonProperty
    private Long startTime;
    @JsonProperty
    private Long endTime;
    @JsonProperty
    private Integer enabled;

    public DailyTimeSlot(Integer zone, Long startTime, Long endTime) {
        this.zone = zone;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public DailyTimeSlot() {
    }

//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public Integer getZone() {
        return zone;
    }

    public void setZone(Integer zone) {
        this.zone = zone;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }
}

