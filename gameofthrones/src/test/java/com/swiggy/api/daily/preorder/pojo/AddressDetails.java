
package com.swiggy.api.daily.preorder.pojo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "id",
    "name",
    "mobile",
    "address",
    "landmark",
    "flat_no",
    "lat",
    "long",
    "annotation"
})
public class AddressDetails {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("flat_no")
    private String flatNo;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("area")
    private String area;
    @JsonProperty("city")
    private String city;
    @JsonProperty("email")
    private String email;
    @JsonProperty("_long")
    private String _long;
    @JsonProperty("reversGeocodefailed")
    private Boolean reversGeocodefailed;
    @JsonProperty("address_line1")
    private String addressLine1;
    @JsonProperty("reverse_geo_code_failed")
    private Boolean reverseGeoCodeFailed;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public AddressDetails withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AddressDetails withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public AddressDetails withMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    public AddressDetails withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public AddressDetails withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("flat_no")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flat_no")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public AddressDetails withFlatNo(String flatNo) {
        this.flatNo = flatNo;
        return this;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    public AddressDetails withLat(String lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public AddressDetails withAnnotation(String annotation) {
        this.annotation = annotation;
        return this;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    public AddressDetails withLng(String lng) {
        this.lng = lng;
        return this;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    public AddressDetails withArea(String area) {
        this.area = area;
        return this;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    public AddressDetails withCity(String city) {
        this.city = city;
        return this;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    public AddressDetails withEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("_long")
    public String getLong() {
        return _long;
    }

    @JsonProperty("_long")
    public void setLong(String _long) {
        this._long = _long;
    }

    public AddressDetails withLong(String _long) {
        this._long = _long;
        return this;
    }

    @JsonProperty("reversGeocodefailed")
    public Boolean getReversGeocodefailed() {
        return reversGeocodefailed;
    }

    @JsonProperty("reversGeocodefailed")
    public void setReversGeocodefailed(Boolean reversGeocodefailed) {
        this.reversGeocodefailed = reversGeocodefailed;
    }

    public AddressDetails withReversGeocodefailed(Boolean reversGeocodefailed) {
        this.reversGeocodefailed = reversGeocodefailed;
        return this;
    }

    @JsonProperty("address_line1")
    public String getAddressLine1() {
        return addressLine1;
    }

    @JsonProperty("address_line1")
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public AddressDetails withAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
        return this;
    }

    @JsonProperty("reverse_geo_code_failed")
    public Boolean getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    @JsonProperty("reverse_geo_code_failed")
    public void setReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    public AddressDetails withReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
        return this;
    }


    public AddressDetails setDefaultData()
    {
        this.withAddress("new").withId("1").withAnnotation("122").
                withFlatNo("222").withLandmark("landmark").withLat("1.325123").
                withLng("103.924639").withMobile("9035592077").withName("Daily").withArea("area1").
                withAddressLine1("line1").withEmail("dummy@gmail.com").withCity("Gurgaon")
                .setReverseGeoCodeFailed(true);
        return this;
    }

}
