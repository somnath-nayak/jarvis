package com.swiggy.api.daily.promotions.pojo.cart;

import gherkin.lexer.Ca;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Cart {


    @JsonProperty("items")
    private ArrayList<CartItems> items;
    @JsonProperty("pricingDetails")
    private PricingDetails pricingDetails;
    
    public ArrayList<CartItems> getItems() {
        return items;
    }
    
    public void setItems(ArrayList<CartItems> items) {
        this.items = items;
    }
    
    public PricingDetails getPricingDetails() {
        return pricingDetails;
    }
    
    public void setPricingDetails(PricingDetails pricingDetails) {
        this.pricingDetails = pricingDetails;
    }
   
    public Cart withItems(ArrayList<CartItems> items){
        setItems(items);
        return this;
    }
    public Cart withPricingDetails(PricingDetails pricingDetails){
        setPricingDetails(pricingDetails);
        return this;
    }
   
    public Cart cartWithGivenValues(ArrayList<CartItems> items,PricingDetails pricingDetails){
        setItems(items); setPricingDetails(pricingDetails);
        return this;
        
    }
//    public Cart withGivenValues(String[] storeId, String[] skuID, List<Integer> quantity , List<Integer> price, Integer itemTotal){
//
//        withDefaultValues(storeId,  skuID,  quantity ,  price,  itemTotal));
//        return cartList;
//    }
    
    public Cart withGivenValues(String[] storeId, String[] skuID, List<Integer> quantity , List<Integer> price, Integer itemTotal){
    
       setPricingDetails( new PricingDetails().withGivenValues(itemTotal));
       setItems(new CartItems().listOfCartItems( storeId, skuID,  quantity ,  price));
       
       return this;
        
    }
    
}
