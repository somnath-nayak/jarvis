package com.swiggy.api.daily.checkout.tests;

import com.swiggy.api.daily.checkout.dp.ErrorCodeValidatorsDP;
import com.swiggy.api.daily.checkout.helper.CartPreOrderHelper;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutCommonUtils;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutConstants;
import com.swiggy.api.daily.checkout.pojo.CartPayload;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Map;

public class ErrorCodeValidators extends ErrorCodeValidatorsDP {

    CartPreOrderHelper cartPreOrderHelper = new CartPreOrderHelper();
    DailyCheckoutCommonUtils dailyCheckoutCommonUtils = new DailyCheckoutCommonUtils();


    //=========================================================***Address Validation Test***======================================================================

    @Test(dataProvider="AddressValidatorsDP",  description = "Address Error Code Valication Error Code")
    public void createPreorderCartAndValidateAddress(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage, int expectedStatusCode ) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        cartErrorStatusValidators(cartResponse,expectedStatusMessage,expectedStatusCode);


    }

    //==========================================================***Meal Slot Validation Test***======================================================================

    @Test(dataProvider="MealSlotValidatorsDP",  description = "Create Preorder cart and Validate Meal Slots Error Code")
    public void createPreorderCartAndVerifyMealSlotID(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage,int expectedStatusCode) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        cartErrorStatusValidators(cartResponse,expectedStatusMessage,expectedStatusCode);

    }
    //==========================================================*** Addons Validation Test***======================================================================


    @Test(dataProvider="AddonsValidatorsDP",  description = "Create Preorder cart and Validate Addons Error Code")
    public void createPreorderCartAndVerifyAddons(CartPayload cartPayload, Map<String, String> userInfoHeader, String expectedStatusMessage) throws IOException {
        Processor cartResponse = createPreorderCart(cartPayload, userInfoHeader);
        dailyCheckoutCommonUtils.cartStatusValidators(cartResponse,expectedStatusMessage);

    }


    public Processor createPreorderCart(CartPayload cartPayload,Map<String, String> userInfoHeader){

        Processor cartResponse=cartPreOrderHelper.cartUpdate(Utility.jsonEncode(cartPayload),userInfoHeader);
        return cartResponse;
    }

    public void cartErrorStatusValidators(Processor cartResponse, String expectedStatusMessage ,int errorCode)
    {

        int expectedStatusCode = cartResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if(expectedStatusCode==0) {
            Assert.assertEquals(DailyCheckoutConstants.VALID_STATUS_CODE, expectedStatusCode,"Expected Status code is "+expectedStatusCode+" but found errorCode");

            String expectedStatusMessage1 = cartResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE, expectedStatusMessage1,"DailyCheckoutConstants.VALID_SUCCESSFUL_MESSAGE "+expectedStatusMessage1+" but found "+expectedStatusMessage1+"");
            String actualStatusMessage = cartResponse.ResponseValidator.GetNodeValue("$.data.nonCheckoutReason");
            Assert.assertEquals(expectedStatusMessage, actualStatusMessage);
        }else {
            String actualStatusMessage = cartResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            Assert.assertEquals(actualStatusMessage, expectedStatusMessage,"Expected status message is "+expectedStatusMessage+" but found "+actualStatusMessage+"");
        }
    }


}
