package com.swiggy.api.daily.checkout.pojo.OrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment_info
{
    private String transaction_id;

    private String reason;

    private String payment_type;

    private String metadata;

    private String updated_at;

    private String order_context;

    private String payment_status;

    private String transaction_amount;

    private String refund_for_transaction_id;

    private String created_at;

    private String customer_id;

    private String payment_method;

    public String getTransaction_id ()
    {
        return transaction_id;
    }

    public void setTransaction_id (String transaction_id)
    {
        this.transaction_id = transaction_id;
    }

    public String getReason ()
{
    return reason;
}

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getPayment_type ()
    {
        return payment_type;
    }

    public void setPayment_type (String payment_type)
    {
        this.payment_type = payment_type;
    }

    public String getMetadata ()
    {
        return metadata;
    }

    public void setMetadata (String metadata)
    {
        this.metadata = metadata;
    }

    public String getUpdated_at ()
    {
        return updated_at;
    }

    public void setUpdated_at (String updated_at)
    {
        this.updated_at = updated_at;
    }

    public String getOrder_context ()
    {
        return order_context;
    }

    public void setOrder_context (String order_context)
    {
        this.order_context = order_context;
    }

    public String getPayment_status ()
    {
        return payment_status;
    }

    public void setPayment_status (String payment_status)
    {
        this.payment_status = payment_status;
    }

    public String getTransaction_amount ()
    {
        return transaction_amount;
    }

    public void setTransaction_amount (String transaction_amount)
    {
        this.transaction_amount = transaction_amount;
    }

    public String getRefund_for_transaction_id ()
{
    return refund_for_transaction_id;
}

    public void setRefund_for_transaction_id (String refund_for_transaction_id)
    {
        this.refund_for_transaction_id = refund_for_transaction_id;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    public String getCustomer_id ()
    {
        return customer_id;
    }

    public void setCustomer_id (String customer_id)
    {
        this.customer_id = customer_id;
    }

    public String getPayment_method ()
    {
        return payment_method;
    }

    public void setPayment_method (String payment_method)
    {
        this.payment_method = payment_method;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [transaction_id = "+transaction_id+", reason = "+reason+", payment_type = "+payment_type+", metadata = "+metadata+", updated_at = "+updated_at+", order_context = "+order_context+", payment_status = "+payment_status+", transaction_amount = "+transaction_amount+", refund_for_transaction_id = "+refund_for_transaction_id+", created_at = "+created_at+", customer_id = "+customer_id+", payment_method = "+payment_method+"]";
    }
}
