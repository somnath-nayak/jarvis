package com.swiggy.api.daily.promotions.pojo.planListing;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class Items {

@JsonProperty("skuId")
    private String skuId;
    
    @JsonProperty("price")
    private Integer price;

    public Items(){
    
    }
    
    public String getSkuId() {
        return skuId;
    }
    
    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }
    
    public Integer getPrice() {
        return price;
    }
    
    public void setPrice(Integer price) {
        this.price = price;
    }
    
    public Items withGiveValues(String skuId , Integer price){
//        setPrice(price);
//        setSkuId(skuId);
        this.price = price;
        this.skuId = skuId;
     return this;
    }
    
    
    public ArrayList<Items> listOfItems(String[] allSkuId , List<Integer> price ){
        
        List<Items> newitems=new ArrayList<Items>();
        
        
        
        for( int i = 0; i<allSkuId.length ; i++){
    
            Items item = new Items();
            item.withGiveValues(allSkuId[i], price.get(i));
            newitems.add(item);
        }
        ArrayList items = new ArrayList(newitems);
        
       return items;
    }
  
}
