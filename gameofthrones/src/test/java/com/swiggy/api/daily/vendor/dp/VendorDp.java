package com.swiggy.api.daily.vendor.dp;

import com.swiggy.api.daily.vendor.helper.VendorCommonHelper;
import com.swiggy.api.daily.vendor.helper.VendorConstant;
import com.swiggy.api.daily.vendor.pojo.*;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class VendorDp {

    JsonHelper jsonHelper = new JsonHelper();
    RestaurantTimeMapPOJO restaurantTimeMapPOJO;
    SlotServicePOJO slotServicePOJO = new SlotServicePOJO();
    VendorCommonHelper vendorCommonHelper = new VendorCommonHelper();

    @DataProvider(name = "loginSuccess")
    public Object[][] loginSuccessCase() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1))
                }
        };
    }

    @DataProvider(name = "loginPayload")
    public Object[][] loginPayloadCheckCase() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault(VendorConstant.rest_id, VendorConstant.password, VendorConstant.source, VendorConstant.random_1))
                }
        };
    }

    @DataProvider(name = "loginFailure")
    public Object[][] loginFailureCase() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault("#!@E#%@", "@!#$!^%", VendorConstant.source, VendorConstant.random_1))
                },
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault("", "", VendorConstant.source, VendorConstant.random_1))
                }
        };
    }

    @DataProvider(name = "loginError")
    public Object[][] loginerror() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault(VendorConstant.rest_id, "@!#$%!]", VendorConstant.source, VendorConstant.random_1))
                }
        };
    }

    @DataProvider(name = "loginAttempt")
    public Object[][] loginAttempt() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new LoginPOJO().setDefault("#!@E#%@", "@!#$!^%", VendorConstant.source, VendorConstant.random_1))
                }
        };
    }


    @DataProvider(name = "orderDispatch")
    public Object[][] orderDispatch() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new OrderDispatchPOJO().setDefault(VendorConstant.rest_id))
                }
        };
    }

    @DataProvider(name = "orderDispatchFailure")
    public Object[][] orderDispatchFailure() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new OrderDispatchPOJO().setDefault(VendorConstant.random))
                }
        };
    }
    @DataProvider(name = "pauseItemSuccess")
    public Object[][] pauseSuccess() throws Exception {
        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new PausePOJO().setDefault(VendorConstant.rest_id,VendorConstant.entityID, date1, date2, from_time, to_time, VendorConstant.source))
                }
        };
    }

    @DataProvider(name = "pauseMultipleItems")
    public Object[][] pauseMultipleItems() throws Exception {
        String date1 = vendorCommonHelper.createEPOCforHour(1);
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.createEPOCforHour(2);
        String to_time = vendorCommonHelper.getCurrentTime();
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new PausePOJO().setDefault(VendorConstant.rest_id, vendorCommonHelper.randomNumber(), date1, date2, from_time, to_time, VendorConstant.source))
                }
        };
    }

    @DataProvider(name = "pauseItemFailure")
    public Object[][] pauseFailure() throws Exception {
        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        String date_3 = vendorCommonHelper.getCurrentDateInMilisecond();
        Thread.sleep(3000);
        String date_4 = vendorCommonHelper.getCurrentDateInMilisecond();
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new PausePOJO().setDefault(VendorConstant.rest_id, VendorConstant.entityID, date_3, date_4, from_time, to_time, VendorConstant.source))
                },
                {
                        jsonHelper.getObjectToJSON(new PausePOJO().setDefault(VendorConstant.invalidrestaurantID, VendorConstant.entityID, date_3, date_4, from_time, to_time, VendorConstant.source))
                }
        };
    }

    @DataProvider(name = "pauseIntegration1")
    public Object[][] pauseIntegration1() throws Exception {
        String date1 = vendorCommonHelper.getCurrentDateInMilisecond();
        String from_time = vendorCommonHelper.getCurrentTime();
        Thread.sleep(3000);
        String date2 = vendorCommonHelper.getCurrentDateInMilisecond();
        String to_time = vendorCommonHelper.getCurrentTime();
        String date_3 = vendorCommonHelper.getCurrentDateInMilisecond();
        Thread.sleep(3000);
        String date_4 = vendorCommonHelper.getCurrentDateInMilisecond();
        return new Object[][]{
                {
                                jsonHelper.getObjectToJSON(new PausePOJO().setDefault(VendorConstant.invalidrestaurantID, VendorConstant.random_0, date_3, date_4, from_time, to_time, VendorConstant.source))
                }
        };
    }

    @DataProvider(name = "slotService")
    public Object[][] createHolidaySlot() throws Exception {
        SlotServicePOJO slotServicePOJO = new SlotServicePOJO().setDefault(VendorConstant.store_id, true,vendorCommonHelper.getDateInToMilliseconds(vendorCommonHelper.currentDatePattern()), vendorCommonHelper.getDateInToMilliseconds(vendorCommonHelper.futureDatePattern()), VendorConstant.entityID, vendorCommonHelper.currentDatePattern(), vendorCommonHelper.futureDatePattern(),VendorConstant.DAILY_STORE);
        List list = new ArrayList();
        list.add(slotServicePOJO);
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(list)
                }
        };
    }

    @DataProvider(name = "restTimeSlot")
    public Object[][] restTimeSlot() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new RestaurantSlotPOJO().setDefault(VendorConstant.close_time, VendorConstant.MON, VendorConstant.open__time, VendorConstant.rest_id))
                }
        };
    }

    @DataProvider(name = "holidaySlot")
    public Object[][] holidaySlot() throws IOException {
        return new Object[][]{
                {
                        jsonHelper.getObjectToJSON(new HolidaySlotPOJO().setDefault(vendorCommonHelper.timePattern(),VendorConstant.rest_id,vendorCommonHelper.timePattern()))
                }
        };
    }

}
