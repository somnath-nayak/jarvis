
package com.swiggy.api.daily.preorder.pojo;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.Utility.GenerateRandomUtils;
import com.swiggy.api.daily.common.DateUtility;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonPropertyOrder({
    "delivery_details",
    "item_details",
    "user_id",
    "store_id",
    "order_id",
    "delivery_address_details"
})
public class CreatePreorederPojo {

    @JsonProperty("delivery_details")
    private DeliveryDetails deliveryDetails;
    @JsonProperty("entity_details")
    private List<ItemDetail> entityDetails = null;
    @JsonProperty("user_id")
    private String userId;

    @JsonProperty("delivery_start_time")
    private long deliveryStartTime;

    @JsonProperty("delivery_end_time")
    private long deliveryEndTime;

    @JsonProperty("store_id")
    private String storeId;

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("delivery_address_details")
    private AddressDetails deliveryAddressDetails;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CreatePreorederPojo() {
    }

    /**
     * 
     * @param deliveryDetails
     * @param addressDetails
     * @param userId
     * @param storeId
     * @param orderId
     * @param itemDetails
     */
    public CreatePreorederPojo(DeliveryDetails deliveryDetails, List<ItemDetail> itemDetails, String userId, String storeId, String orderId, AddressDetails addressDetails) {
        super();
        this.deliveryDetails = deliveryDetails;
        this.entityDetails = itemDetails;
        this.userId = userId;
        this.storeId = storeId;
        this.orderId = orderId;
        this.deliveryAddressDetails = addressDetails;
    }

    @JsonProperty("delivery_details")
    public DeliveryDetails getDeliveryDetails() {
        return deliveryDetails;
    }

    @JsonProperty("delivery_details")
    public void setDeliveryDetails(DeliveryDetails deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
    }

    public CreatePreorederPojo withDeliveryDetails(DeliveryDetails deliveryDetails) {
        this.deliveryDetails = deliveryDetails;
        return this;
    }

    @JsonProperty("entity_details")
    public List<ItemDetail> getItemDetails() {
        return entityDetails;
    }

    @JsonProperty("entity_details")
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.entityDetails = itemDetails;
    }

    public CreatePreorederPojo withItemDetails(List<ItemDetail> itemDetails) {
        this.entityDetails = itemDetails;
        return this;
    }

    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    public CreatePreorederPojo withUserId(String userId) {
        this.userId = userId;
        return this;
    }


    @JsonProperty("store_id")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public CreatePreorederPojo withStoreId(String storeId) {
        this.storeId = storeId;
        return this;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CreatePreorederPojo withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("delivery_address_details")
    public AddressDetails getAddressDetails() {
        return deliveryAddressDetails;
    }

    @JsonProperty("delivery_address_details")
    public void setAddressDetails(AddressDetails addressDetails) {
        this.deliveryAddressDetails = addressDetails;
    }

    public CreatePreorederPojo withAddressDetails(AddressDetails addressDetails) {
        this.deliveryAddressDetails = addressDetails;
        return this;
    }
    public long getDeliveryStartTime() {
        return deliveryStartTime;
    }



    public void setDeliveryStartTime(long deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
    }


    public CreatePreorederPojo withDeliveryStartTime(long deliveryStartTime) {
        this.deliveryStartTime = deliveryStartTime;
        return this;
    }

    public long getDeliveryEndTime() {
        return deliveryEndTime;
    }

    public void setDeliveryEndTime(long deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
    }

    public CreatePreorederPojo withDeliveryEndTime(long deliveryEndTime) {
        this.deliveryEndTime = deliveryEndTime;
        return this;
    }

    public CreatePreorederPojo setDefaultData()
    {
        List<ItemDetail> itemDetails;
        ItemDetail itemDetail = new ItemDetail();
        deliveryAddressDetails=new AddressDetails();
        deliveryAddressDetails.setDefaultData();
        itemDetails = new ArrayList<>();
        itemDetail.setDefaultData();
        itemDetails.add(itemDetail);
        setItemDetails(itemDetails);
        setAddressDetails(deliveryAddressDetails);
        setUserId("3");
        setOrderId(""+GenerateRandomUtils.generateRandomNumber(5));
        setStoreId("2");
        setDeliveryStartTime(DateUtility.getMidNightTimeOfCurrentDayInMilisecond());
        setDeliveryEndTime(DateUtility.getFutureDateInMilisecond());
        return this;
    }

}
