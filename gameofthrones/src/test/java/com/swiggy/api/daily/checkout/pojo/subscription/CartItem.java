package com.swiggy.api.daily.checkout.pojo.subscription;

import io.advantageous.boon.json.annotations.JsonProperty;

public class CartItem {




    @JsonProperty("item")
    private Item item;
    @JsonProperty("quantity")
    private Long quantity;
    @JsonProperty("totalWithoutDiscount")
    private Long totalWithoutDiscount;
    @JsonProperty("total")
    private Long total;
    @JsonProperty("itemBill")
    private ItemBill itemBill;
    @JsonProperty("itemTaxationRates")
    private ItemTaxationRates itemTaxationRates;
    @JsonProperty("availabilityDetails")
    private AvailabilityDetails availabilityDetails;

    @JsonProperty("item")
    public Item getItem() {
        return item;
    }

    @JsonProperty("item")
    public void setItem(Item item) {
        this.item = item;
    }

    public CartItem withItem(Item item) {
        this.item = item;
        return this;
    }

    @JsonProperty("quantity")
    public Long getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public CartItem withQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("totalWithoutDiscount")
    public Long getTotalWithoutDiscount() {
        return totalWithoutDiscount;
    }

    @JsonProperty("totalWithoutDiscount")
    public void setTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
    }

    public CartItem withTotalWithoutDiscount(Long totalWithoutDiscount) {
        this.totalWithoutDiscount = totalWithoutDiscount;
        return this;
    }

    @JsonProperty("total")
    public Long getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Long total) {
        this.total = total;
    }

    public CartItem withTotal(Long total) {
        this.total = total;
        return this;
    }

    @JsonProperty("itemBill")
    public ItemBill getItemBill() {
        return itemBill;
    }

    @JsonProperty("itemBill")
    public void setItemBill(ItemBill itemBill) {
        this.itemBill = itemBill;
    }

    public CartItem withItemBill(ItemBill itemBill) {
        this.itemBill = itemBill;
        return this;
    }

    @JsonProperty("itemTaxationRates")
    public ItemTaxationRates getItemTaxationRates() {
        return itemTaxationRates;
    }

    @JsonProperty("itemTaxationRates")
    public void setItemTaxationRates(ItemTaxationRates itemTaxationRates) {
        this.itemTaxationRates = itemTaxationRates;
    }

    public CartItem withItemTaxationRates(ItemTaxationRates itemTaxationRates) {
        this.itemTaxationRates = itemTaxationRates;
        return this;
    }

    @JsonProperty("availabilityDetails")
    public AvailabilityDetails getAvailabilityDetails() {
        return availabilityDetails;
    }

    @JsonProperty("availabilityDetails")
    public void setAvailabilityDetails(AvailabilityDetails availabilityDetails) {
        this.availabilityDetails = availabilityDetails;
    }

    public CartItem withAvailabilityDetails(AvailabilityDetails availabilityDetails) {
        this.availabilityDetails = availabilityDetails;
        return this;
    }
}
