package com.swiggy.api.daily.delivery.helper;

public class DailyConstants {
    public static final String DAILY_CAPACITY_REDIS_DELIMITER =":";
    public static final String DAILY_CAPACITY_DELIVERY_CONFIG_PREFIX = "daily_delivery_capacity_zone_";
    public static final String DAILY_CAPACITY_UNIQUE_KEY_DELIMITER = ":";
    public static final String HHMM_DATE_FORMAT = "HH:mm";
    public static final String DAY_FORMAT_EXTRACTOR = "EEE";
    public static final String IST_ZONE_OFFSET = "+05:30";
    public static final Integer HH_START_OFFSET = 0;
    public static final Integer HH_END_OFFSET = 2;
    public static final Integer MM_START_OFFSET = 3;
    public static final Integer MM_END_OFFSET = 5;
    public static final String SERVICEABLE = "SERVICEABLE";
    public static final String NONSERVICEABLE = "NONSERVICEABLE";

}
