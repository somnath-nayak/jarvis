package com.swiggy.api.daily.checkout.pojo.slotdetails;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class SlotDetailsPayload {

    @JsonProperty("requests")
    private List<Request> requests = null;

    @JsonProperty("requests")
    public List<Request> getRequests() {
        return requests;
    }

    @JsonProperty("requests")
    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }
}
