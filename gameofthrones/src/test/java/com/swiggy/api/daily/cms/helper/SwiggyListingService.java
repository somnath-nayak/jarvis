package com.swiggy.api.daily.cms.helper;
import com.swiggy.api.daily.cms.pojo.SwiggyListingServiec.GetMealAndPlan;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;

public class SwiggyListingService {

    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();

    GetMealAndPlan getMealAndPlan = new GetMealAndPlan();

    String storeId = "13584";

    String tanentId= "9012d90sdck3o48";

    String authKey = "Basic dXNlcjpjaGVjaw==";

    HashMap<String, HashMap<String,String>> mealData= new HashMap<String, HashMap<String,String>>();

    public HashMap<String, String> setHeader()
    {
        /*
        @return it retrun the request header object which has all the CMS related header settings.
         */
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tenant-id", tanentId);
        requestHeaders.put("Authorization", authKey);
        return requestHeaders;
    }

    public HashMap<String,String> getMeal(int mealNumber) throws IOException {
        /*
        @param mealNumber is the number which means the serial meal provided by List. 0 mean first meal
        @return It returns given meal spin id & price, We can add many keys to hashmap obejct to return in future
         */
        getMealAndPlan = new GetMealAndPlan();
        HashMap<String,String> individualMeal = new HashMap<String,String>();
        JsonHelper helper = new JsonHelper();
        getMealAndPlan.setStoreId(Integer.parseInt(storeId));
        String data = helper.getObjectToJSON(getMealAndPlan);
        //CREATE MEAL FOR particular slot with inventory

        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "getMeal", gameofthrones);
        String[] payloadparams = new String[] {"["+data+"]"};
        Processor processor = new Processor(service, setHeader(), payloadparams);
        String spinId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data["+mealNumber+"].variations..spin")
                .replace("[","").replace("]","").replace("\"","").replace("\"","");
        individualMeal.put("spin",spinId);
        String price = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data["+mealNumber+"].variations..price..price")
                .replace("[","").replace("]","").replace("\"","").replace("\"","");
        individualMeal.put("price",price);
        return individualMeal;
    }
    public HashMap<String,String> getPlan(int mealNumber) throws IOException
    {
         /*
        @param mealNumber is the number which means the serial meal provided by List. 0 mean first meal
        @return It returns given meal spin id & price, We can add many keys to hashmap obejct to return in future
         */
        getMealAndPlan = new GetMealAndPlan();
        HashMap<String,String> individualPlan = new HashMap<String,String>();
        JsonHelper helper = new JsonHelper();
        getMealAndPlan.setStoreId(Integer.parseInt(storeId));
        //CREATE MEAL FOR particular slot with inventory
        String data = helper.getObjectToJSON(getMealAndPlan);
        GameOfThronesService service = new GameOfThronesService("swiggylistingservice", "getPlan", gameofthrones);
        String[] payloadparams = new String[] {"["+data+"]"};
        Processor processor = new Processor(service, setHeader(), payloadparams);
        String spinId = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data["+mealNumber+"].variations..spin")
                .replace("[","").replace("]","").replace("\"","").replace("\"","");
        individualPlan.put("spin",spinId);
        String price = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data["+mealNumber+"].variations..price..price")
                .replace("[","").replace("]","").replace("\"","").replace("\"","");
        individualPlan.put("price",price);
        return individualPlan;
    }

    public Processor checkoutAddon(String storeId, String spinId) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{storeId, spinId};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "addoncheckout", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor checkoutMeal(String storeId, String spinId,Long startepc,Long endepoc) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{storeId, spinId,startepc.toString(),endepoc.toString()};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "mealcheckout", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor checkoutMealQuantity(String storeId, String spinId,String quantity,Long startepc,Long endepoc) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{storeId, spinId,quantity,startepc.toString(),endepoc.toString()};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "mealcheckoutquantity", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor checkoutPlan(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate ) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{storeId, spinId,starttime,endtime,startdate,excludeddate};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "plancheckout", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor checkoutPlanQuantity(String storeId, String spinId,String quantity,String starttime,String endtime,String startdate,String excludeddate ) {
        Processor processor = null;
        String[] payloadparams = new String[]{storeId,spinId,quantity,starttime,endtime,startdate,excludeddate};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "plancheckoutquantity", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor pdpWithProductId(String storeId, String productId,String startdate,String starttime,String endtime ) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] urlparams = new String[3];
        urlparams[0]=startdate;
        urlparams[1]=starttime;
        urlparams[2]=endtime;
        String[] payloadparams = new String[]{storeId, productId};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "pdpproduct", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, urlparams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor pdpWithoutProductId(String storeId,String startdate,String starttime,String endtime ) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] urlparams = new String[3];
        urlparams[0]=startdate;
        urlparams[1]=starttime;
        urlparams[2]=endtime;
        String[] payloadparams = new String[]{storeId};

        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "pdpwithoutproduct", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, urlparams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public static String getDate(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, +1);
        String date = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
        return date;
    }

    public static String getPastDate(){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String date = new SimpleDateFormat("dd-MM-yyyy").format(cal.getTime());
        return date;
    }


    public Processor searchSkusbySpinId(int storeId,String spinId ) {
        Processor processor = null;
        JsonHelper jsonHelper = new JsonHelper();
        String[] payloadparams = new String[]{Integer.toString(storeId),spinId};
        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "searchskusbyspin", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor searchSkusbySpinIdConfig(String storeId,String spinId,int mealcount,long startepoc ) {
        Processor processor = null;
        String[] payloadparams = new String[]{storeId,spinId};
        String[] queryparams=new String[2];
        queryparams[0]=Integer.toString(mealcount);
        queryparams[1]= Long.toString(startepoc);
        GameOfThronesService got = new GameOfThronesService("swiggylistingservice", "searchskusbyspinconfig", gameofthrones);
        try {
            processor = new Processor(got, setHeader(), payloadparams, queryparams);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;
    }

    public Processor searchInventoryPlan(String storeId,String productId){
        Processor processor = null;
        String[] payloadparams = new String[]{storeId,productId};
        HashMap header = new LinkedHashMap();
        header.put("service-line", CMSDailyContants.service_line_plan);
        GameOfThronesService got = new GameOfThronesService("cmsdailyinventoryservice", "inventorysearchplan", gameofthrones);
        try {
            processor = new Processor(got, header, payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;

    }
    public Processor searchInventoryMeal(String storeId,String spinId){
        Processor processor = null;
        String[] payloadparams = new String[]{storeId,spinId};
        HashMap header = new LinkedHashMap();
        header.put("service-line", CMSDailyContants.service_line_meal);
        GameOfThronesService got = new GameOfThronesService("cmsdailyinventoryservice", "inventorysearchmeal", gameofthrones);
        try {
            processor = new Processor(got, header, payloadparams, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return processor;

    }
    public String epocToDate(long epoc) {
        Date date = new Date(epoc);
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        format.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        String formatted = format.format(date);
        return  formatted;
    }

    public long dateToEpoc(String date) throws Exception{
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        long date1 = format.parse(date).getTime();
        return  date1;
    }

    
}
