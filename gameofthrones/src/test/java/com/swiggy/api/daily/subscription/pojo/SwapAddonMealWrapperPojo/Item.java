
package com.swiggy.api.daily.subscription.pojo.SwapAddonMealWrapperPojo;

import com.swiggy.api.daily.subscription.helper.SubscriptionConstant;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "type",
    "spin",
    "store_id",
    "name",
    "id",
    "product_id",
    "meta"
})
public class Item {

    @JsonProperty("type")
    private String type;
    @JsonProperty("spin")
    private String spin;
    @JsonProperty("store_id")
    private String storeId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("product_id")
    private String productId;
    @JsonProperty("meta")
    private Meta meta;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Item withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("spin")
    public String getSpin() {
        return spin;
    }

    @JsonProperty("spin")
    public void setSpin(String spin) {
        this.spin = spin;
    }

    public Item withSpin(String spin) {
        this.spin = spin;
        return this;
    }

    @JsonProperty("store_id")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("store_id")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Item withStoreId(String storeId) {
        this.storeId = storeId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Item withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public Item withId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("product_id")
    public String getProductId() {
        return productId;
    }

    @JsonProperty("product_id")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Item withProductId(String productId) {
        this.productId = productId;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Item withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public Item setDefaultData()  {
        return this.withType(SubscriptionConstant.TYPE)
                .withSpin(SubscriptionConstant.SPIN)
                .withStoreId(SubscriptionConstant.STORE_ID)
                .withName(SubscriptionConstant.ITEM_NAME)
                .withId(SubscriptionConstant.ITEMDETAILS_ID)
                .withProductId(SubscriptionConstant.PRODUCT_ID)
                .withMeta(new Meta().setDefaultData());
    }

}
