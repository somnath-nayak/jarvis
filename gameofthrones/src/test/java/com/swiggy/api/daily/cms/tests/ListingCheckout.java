package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.helper.SwiggyListingService;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ListingCheckout extends CMSDailyDP {
    SwiggyListingService swiggyListingService=new SwiggyListingService();
    SchemaValidatorUtils schema=new SchemaValidatorUtils();
    CMSDailyHelper cmsDailyHelper=new CMSDailyHelper();

    @Test(dataProvider = "addoncheckout",description = "To verify basic details of addon checkout api response")
    public void addonCheckoutApi(String storeId, String spinId){
        String addonresponse=swiggyListingService.checkoutAddon(storeId,spinId).ResponseValidator.GetBodyAsText();
        String storeid=JsonPath.read(addonresponse,"$.data.[0].store_id");
        String spinid=JsonPath.read(addonresponse,"$.data.[0].spin");
        String id=JsonPath.read(addonresponse,"$.data.[0].id");
        String producttype=JsonPath.read(addonresponse,"$.data.[0].product_type");
        Assert.assertEquals(spinid,spinId);
        Assert.assertEquals(storeid,storeId);
        Assert.assertEquals(producttype,"ADDON");
        Assert.assertNotNull(id);
    }
    @Test(dataProvider = "addoncheckout",description = "To verify meta details of addon")
    public void addonCheckoutMeta(String storeId, String spinId) {
        String addonresponse=swiggyListingService.checkoutAddon(storeId,spinId).ResponseValidator.GetBodyAsText();
        int is_veg=JsonPath.read(addonresponse,"$.data.[0].meta.is_veg");
        String quantity=JsonPath.read(addonresponse,"$.data.[0].meta.quantity");
        String uom=JsonPath.read(addonresponse,"$.data.[0].meta.unit_of_measure");
        Assert.assertNotNull(is_veg);
        Assert.assertNotNull(quantity);
        Assert.assertNotNull(uom);


    }

    @Test(dataProvider = "addoncheckout",description = "To verify price details of addon")
    public void addonCheckoutPrice(String storeId, String spinId) {
        String addonresponse = swiggyListingService.checkoutAddon(storeId, spinId).ResponseValidator.GetBodyAsText();
        int packaging=JsonPath.read(addonresponse,"$.data.[0].price.packaging_charges");
        boolean inclusive=JsonPath.read(addonresponse,"$.data.[0].price.inclusive_of_taxes");
        Double cgst=JsonPath.read(addonresponse,"$.data.[0].price.cgst_percentage");
        Double igst=JsonPath.read(addonresponse,"$.data.[0].price.igst_percentage");
        int price=JsonPath.read(addonresponse,"$.data.[0].price.price");
        if(price>0){Assert.assertTrue(true);}
        Assert.assertNotNull(packaging);
        Assert.assertNotNull(cgst);
        Assert.assertNotNull(igst);
        Assert.assertNotNull(inclusive);
    }

    @Test(dataProvider = "mealcheckout",description = "To verify basic details of meal checkout api response")
    public void mealCheckoutApiBasic(String storeId, String spinId,Long startepc,Long endepoc){
        String mealresponse=swiggyListingService.checkoutMeal(storeId,spinId,startepc,endepoc).ResponseValidator.GetBodyAsText();
        String storeid=JsonPath.read(mealresponse,"$.data.[0].store_id");
        String spinid=JsonPath.read(mealresponse,"$.data.[0].spin");
        String id=JsonPath.read(mealresponse,"$.data.[0].id");
        String producttype=JsonPath.read(mealresponse,"$.data.[0].product_type");
        Assert.assertEquals(spinid,spinId);
        Assert.assertEquals(storeid,storeId);
        Assert.assertEquals(producttype,"PRODUCT");
        Assert.assertNotNull(id);
    }
    @Test(dataProvider = "mealcheckout",description = "To verify meta details of meal")
    public void mealCheckoutApiMeta(String storeId, String spinId,Long startepc,Long endepoc){
        String mealresponse=swiggyListingService.checkoutMeal(storeId,spinId,startepc,endepoc).ResponseValidator.GetBodyAsText();
        int is_veg=JsonPath.read(mealresponse,"$.data.[0].meta.is_veg");
        Assert.assertNotNull(is_veg);
    }
    @Test(dataProvider = "mealcheckout",description = "To verify price details of meal")
    public void mealCheckoutApiPrice(String storeId, String spinId,Long startepc,Long endepoc){
        String mealresponse=swiggyListingService.checkoutMeal(storeId,spinId,startepc,endepoc).ResponseValidator.GetBodyAsText();
        int packaging=JsonPath.read(mealresponse,"$.data.[0].price.packaging_charges");
        Double cgst=JsonPath.read(mealresponse,"$.data.[0].price.cgst_percentage");
        int price=JsonPath.read(mealresponse,"$.data.[0].price.price");
        if(price>0){Assert.assertTrue(true);}
        else {Assert.fail();}
        Assert.assertNotNull(packaging);
        Assert.assertNotNull(cgst);

    }

    @Test(dataProvider = "plancheckout",description = "To verify basic details of plan checkout api response")
    public void planCheckoutApiBasic(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse=swiggyListingService.checkoutPlan(storeId,spinId,starttime,endtime,startdate,excludeddate).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(schema.validateSchema("/../Data/SchemaSet/Json/CMS/checkoutplan.txt",planresponse));
        String storeid=JsonPath.read(planresponse,"$.data.[0].store_id");
        String spinid=JsonPath.read(planresponse,"$.data.[0].spin");
        String id=JsonPath.read(planresponse,"$.data.[0].id");
        String producttype=JsonPath.read(planresponse,"$.data.[0].product_type");
        Assert.assertEquals(spinid,spinId);
        Assert.assertEquals(storeid,storeId);
        Assert.assertEquals(producttype,"PLAN");
        Assert.assertNotNull(id);
    }
    @Test(dataProvider = "mealcheckout",description = "To verify meta details of meal")
    public void mealCheckoutAvail(String storeId, String spinId,Long startepc,Long endepoc){
        String mealresponse=swiggyListingService.checkoutMeal(storeId,spinId,startepc,endepoc).ResponseValidator.GetBodyAsText();
        boolean is_avail = JsonPath.read(mealresponse, "$.data.[0].availability.is_avail");
        boolean in_stock = JsonPath.read(mealresponse, "$.data.[0].inventory.in_stock");
        Assert.assertNotNull(is_avail);
        Assert.assertNotNull(in_stock);

    }

    @Test(dataProvider = "plancheckout",description = "To verify meta details of plan")
    public void planCheckoutApiMeta(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse=swiggyListingService.checkoutPlan(storeId,spinId,starttime,endtime,startdate,excludeddate).ResponseValidator.GetBodyAsText();
        String frequency=JsonPath.read(planresponse,"$.data.[0].meta.plan_frequency");
        int tenure=JsonPath.read(planresponse,"$.data.[0].meta.tenure");
        Assert.assertNotNull(frequency);
        Assert.assertNotNull(tenure);

    }
    @Test(dataProvider = "plancheckout",description = "To verify meta details of plan")
    public void planCheckoutNextChange(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse=swiggyListingService.checkoutPlan(storeId,spinId,starttime,endtime,startdate,excludeddate).ResponseValidator.GetBodyAsText();
        long currenttime=CMSDailyHelper.createEPOCforDays();
        long nextchange=JsonPath.read(planresponse,"$.data.[0].availability.next_change");
        if(currenttime>nextchange){
            Assert.fail();
        }


    }
    @Test(dataProvider = "plancheckout",description = "To verify price details of plan")
    public void planCheckoutApiPrice(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse=swiggyListingService.checkoutPlan(storeId,spinId,starttime,endtime,startdate,excludeddate).ResponseValidator.GetBodyAsText();
        int price=JsonPath.read(planresponse,"$.data.[0].price.price");
        int packaging=JsonPath.read(planresponse,"$.data.[0].price.packaging_charges");
        Double cgst=JsonPath.read(planresponse,"$.data.[0].price.cgst_percentage");
        Double igst=JsonPath.read(planresponse,"$.data.[0].price.igst_percentage");
        if(price>0){Assert.assertTrue(true);}
        else {Assert.fail();}
        Assert.assertNotNull(packaging);
        Assert.assertNotNull(cgst);
        Assert.assertNotNull(igst);

    }

    @Test(dataProvider = "plancheckout",description = "To verify avilability and inventory details of plan")
    public void planCheckoutApiAvail(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate) {
        String planresponse = swiggyListingService.checkoutPlan(storeId, spinId, starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        boolean is_avail = JsonPath.read(planresponse, "$.data.[0].availability.is_avail");
        boolean in_stock = JsonPath.read(planresponse, "$.data.[0].inventory.in_stock");
        Assert.assertNotNull(is_avail);
        Assert.assertNotNull(in_stock);

    }

    @Test(dataProvider = "plancheckout",description = "verify cgst and igst value to be zero if inclusive of taxes is true")
    public void VerifyGstIfInclusiveTrue(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse = swiggyListingService.checkoutPlan(storeId, spinId, starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        boolean inclusive=JsonPath.read(planresponse,"$.data[0].price.inclusive_of_taxes");
        int cgst=JsonPath.read(planresponse,"$.data[0].price.cgst");
        int igst=JsonPath.read(planresponse,"$.data[0].price.igst");
        if(inclusive==true){
            Assert.assertEquals(cgst,0);
            Assert.assertEquals(igst,0);
        }

    }

    @Test(dataProvider = "mealcheckout",description = "verify cgst and igst value to be zero if inclusive of taxes is true")
    public void VerifyMealGstIfInclusiveTrue(String storeId, String spinId,Long startepc,Long endepoc){
        String mealresponse=swiggyListingService.checkoutMeal(storeId,spinId,startepc,endepoc).ResponseValidator.GetBodyAsText();
        boolean inclusive=JsonPath.read(mealresponse,"$.data[0].price.inclusive_of_taxes");
        double cgst=JsonPath.read(mealresponse,"$.data[0].price.cgst");
        double igst=JsonPath.read(mealresponse,"$.data[0].price.igst");
        if(inclusive==true){
            Assert.assertEquals(cgst,0);
            Assert.assertEquals(igst,0);
        }

    }

    @Test(dataProvider = "plancheckoutinclusive",description = "verify cgst and igst value to be zero if inclusive of taxes is true")
    public void VerifyGstIfInclusiveFalse(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse = swiggyListingService.checkoutPlan(storeId, spinId, starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        boolean inclusive=JsonPath.read(planresponse,"$.data[0].price.inclusive_of_taxes");
        double cgst=JsonPath.read(planresponse,"$.data[0].price.cgst");
        double igst=JsonPath.read(planresponse,"$.data[0].price.igst");
        if(inclusive==true && cgst<=0 && igst<=0){
            Assert.fail();
        }
    }

    @Test(dataProvider = "plancheckout",description = "verify plan checkout price is matching with get price api")
    public void ComparePricePlan(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate){
        String planresponse=swiggyListingService.checkoutPlan(storeId,spinId,starttime,endtime,startdate,excludeddate).ResponseValidator.GetBodyAsText();
        String storeid=JsonPath.read(planresponse,"$.data[0].store_id");
        String spinid=JsonPath.read(planresponse,"$.data[0].spin");
        String key=storeid+"-"+spinid;
        int checkoutprice=JsonPath.read(planresponse,"$.data[0].price.price");
        String pricingresponse=cmsDailyHelper.getPricing(key,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        int price=JsonPath.read(pricingresponse,"$.data[0].pricing_construct.price_list.price");
        Assert.assertEquals(price,checkoutprice);
    }

    @Test(dataProvider = "mealcheckout",description = "verify meal checkout price is matching with get price api")
    public void ComparePriceMeal(String storeId, String spinId,Long startepc,Long endepoc) {
        String mealresponse = swiggyListingService.checkoutMeal(storeId, spinId, startepc, endepoc).ResponseValidator.GetBodyAsText();
        String storeid=JsonPath.read(mealresponse,"$.data[0].store_id");
        String spinid=JsonPath.read(mealresponse,"$.data[0].spin");
        String key=storeid+"-"+spinid;
        int mealprice=JsonPath.read(mealresponse,"$.data[0].price.price");
        String pricingresponse=cmsDailyHelper.getPricing(key,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        int price=JsonPath.read(pricingresponse,"$.data[0].pricing_construct.price_list.price");
        Assert.assertEquals(price,mealprice);
    }

    @Test(dataProvider = "addoncheckout",description = "verify addon checkout price is matching with get price api")
    public void ComparePriceAddon(String storeId, String spinId) {
        String addonresponse = swiggyListingService.checkoutAddon(storeId, spinId).ResponseValidator.GetBodyAsText();
        String storeid = JsonPath.read(addonresponse, "$.data.[0].store_id");
        String spinid = JsonPath.read(addonresponse, "$.data.[0].spin");
        String key=storeid+"-"+spinid;
        int addonprice=JsonPath.read(addonresponse,"$.data[0].price.price");
        String pricingresponse=cmsDailyHelper.getPricing(key,CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
        int price=JsonPath.read(pricingresponse,"$.data[0].pricing_construct.price_list.price");
        Assert.assertEquals(price,addonprice);
    }

    @Test(dataProvider = "plancheckout",description = "Get inventory from inventory service and verify with checkout api for plan")
    public void CompareInventoryPlan(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate) {
        String planresponse = swiggyListingService.checkoutPlan(storeId, spinId, starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        String storeid = JsonPath.read(planresponse, "$.data[0].store_id");
        String productid = JsonPath.read(planresponse, "$.data[0].product_id");
        int planinventory=JsonPath.read(planresponse,"$.data[0].inventory.remaining");
        String getinvresponse=swiggyListingService.searchInventoryPlan(storeid,productid).ResponseValidator.GetBodyAsText();
        int invmaxcap=JsonPath.read(getinvresponse,"$.data[0].inventory_construct.inventories.max_cap");
        Assert.assertEquals(planinventory,invmaxcap);
    }

    @Test(dataProvider = "mealcheckoutinv",description = "verify meal checkout price is matching with get price api")
    public void CompareInventoryMeal(String storeId, String spinId,Long startepc,Long endepoc) {
        String mealresponse = swiggyListingService.checkoutMeal(storeId, spinId, startepc, endepoc).ResponseValidator.GetBodyAsText();
        String storeid = JsonPath.read(mealresponse, "$.data[0].store_id");
        String spinid = JsonPath.read(mealresponse, "$.data[0].spin");
        int mealinventory=JsonPath.read(mealresponse,"$.data[0].inventory.remaining");
        String getinvresponse=swiggyListingService.searchInventoryMeal(storeid,spinid).ResponseValidator.GetBodyAsText();
        int invmaxcap=JsonPath.read(getinvresponse,"$.data[0].inventory_construct.inventories.max_cap");
        Assert.assertEquals(mealinventory,invmaxcap);

    }

    @Test(dataProvider = "plancheckoutquantity",description = "To verify instock should be false if request quantity is greater than response remaining count of inventory for plan")
    public void planCheckoutQuantityGreater(String storeId, String spinId,String quantity,String starttime,String endtime,String startdate,String excludeddate) {
        String planresponse = swiggyListingService.checkoutPlanQuantity(storeId, spinId,quantity ,starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        int planinventory=JsonPath.read(planresponse,"$.data[0].inventory.remaining");
        boolean instock=JsonPath.read(planresponse,"$.data[0].inventory.in_stock");
        int requestQuantity=Integer.parseInt(quantity);
        int responsequantity=planinventory;
        if(requestQuantity>responsequantity){
            Assert.assertEquals(instock,false);
        }

    }
    @Test(dataProvider = "plancheckoutquantitylesser",description = "To verify instock should be true if request quantity is lesser than response remaining count of inventory for plan")
    public void planCheckoutQuantityLesser(String storeId, String spinId,String quantity,String starttime,String endtime,String startdate,String excludeddate) {
        String planresponse = swiggyListingService.checkoutPlanQuantity(storeId, spinId,quantity ,starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        int planinventory=JsonPath.read(planresponse,"$.data[0].inventory.remaining");
        boolean instock=JsonPath.read(planresponse,"$.data[0].inventory.in_stock");
        int requestQuantity=Integer.parseInt(quantity);
        int responsequantity=planinventory;
        Reporter.log("Request quantity sent ----"+requestQuantity);
        Reporter.log("Response quantity received ----"+responsequantity);
        if(requestQuantity<responsequantity){
            Assert.assertEquals(instock,true);
        }

    }

    @Test(dataProvider = "mealcheckoutquantity",description = "To verify instock should be false if request quantity is greater than response remaining count of inventory for meal")
    public void mealCheckoutQuantityGreater(String storeId, String spinId,String quantity,Long startepc,Long endepoc) {
        String mealresponse = swiggyListingService.checkoutMealQuantity(storeId, spinId,quantity, startepc, endepoc).ResponseValidator.GetBodyAsText();
        int mealinventory=JsonPath.read(mealresponse,"$.data[0].inventory.remaining");
        boolean instock=JsonPath.read(mealresponse,"$.data[0].inventory.in_stock");
        int requestQuantity=Integer.parseInt(quantity);
        int responsequantity=mealinventory;
        Reporter.log("Request quantity sent ----"+requestQuantity);
        Reporter.log("Response quantity received ----"+responsequantity);
        if(requestQuantity>responsequantity){
            Assert.assertEquals(instock,false);
        }


    }

    @Test(dataProvider = "mealcheckoutquantitylesser",description = "To verify instock should be true if request quantity is lesser than response remaining count of inventory for meal")
    public void mealCheckoutQuantityLesser(String storeId, String spinId,String quantity,Long startepc,Long endepoc) {
        String mealresponse = swiggyListingService.checkoutMealQuantity(storeId, spinId,quantity, startepc, endepoc).ResponseValidator.GetBodyAsText();
        int mealinventory=JsonPath.read(mealresponse,"$.data[0].inventory.remaining");
        boolean instock=JsonPath.read(mealresponse,"$.data[0].inventory.in_stock");
        int requestQuantity=Integer.parseInt(quantity);
        int responsequantity=mealinventory;
        Reporter.log("Request quantity sent ----"+requestQuantity);
        Reporter.log("Response quantity received ----"+responsequantity);
        if(requestQuantity<responsequantity){
            Assert.assertEquals(instock,true);
        }


    }

    @Test(dataProvider = "plancheckoutexcluded",description = "verify response does not contain default date")
    public void verifyExcludedDate(String storeId, String spinId,String starttime,String endtime,String startdate,String excludeddate) throws Exception {
        String planresponse = swiggyListingService.checkoutPlan(storeId, spinId, starttime, endtime, startdate, excludeddate).ResponseValidator.GetBodyAsText();
        long nextchange = JsonPath.read(planresponse, "$.data[0].availability.next_change");
        long exludeddates = swiggyListingService.dateToEpoc(excludeddate);
        if (nextchange<exludeddates) {
            Assert.fail("Next change time is less than excluded date");
        }


    }
}
