package com.swiggy.api.daily.cms.tests;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import com.swiggy.api.daily.cms.helper.SwiggyListingService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.junit.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class PDPTest extends CMSDailyDP {
    SwiggyListingService swiggyListingService=new SwiggyListingService();
    CMSDailyHelper cmsDailyHelper=new CMSDailyHelper();
    SchemaValidatorUtils schema=new SchemaValidatorUtils();

    @Test(dataProvider = "pdpproduct",description = "To verify meta details")
    public void pdpWithProductIdMeta(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId,productId,startdate,starttime,endtime).ResponseValidator.GetBodyAsText();
        String displayname=JsonPath.read(pdpresponse,"$.data.[0].meta.display_name");
        int isveg=JsonPath.read(pdpresponse,"$.data.[0].meta.is_veg");
        String frequency=JsonPath.read(pdpresponse,"$.data.[0].meta.plan_frequency");
        Assert.assertNotNull(displayname);
        Assert.assertNotNull(isveg);
        Assert.assertNotNull(frequency);
    }
    @Test(dataProvider = "pdpproduct",description = "To verify meta details")
    public void pdpWithProductIdVariation(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId,productId,startdate,starttime,endtime).ResponseValidator.GetBodyAsText();
        int varprice=JsonPath.read(pdpresponse,"$.data.[0].variations[0].price.price");
        int varlistprice=JsonPath.read(pdpresponse,"$.data.[0].variations[0].price.list_price");
        String currency=JsonPath.read(pdpresponse,"$.data.[0].variations[0].price.currency");
        int isveg=JsonPath.read(pdpresponse,"$.data.[0].meta.is_veg");
        String frequency=JsonPath.read(pdpresponse,"$.data.[0].meta.plan_frequency");
        int tenure=JsonPath.read(pdpresponse,"$.data.[0].variations[0].meta.tenure");
        boolean avail=JsonPath.read(pdpresponse,"$.data.[0].variations[0].availability.is_avail");
        boolean invinstock=JsonPath.read(pdpresponse,"$.data.[0].variations[0].inventory.in_stock");
        Assert.assertNotNull(varprice);
        Assert.assertNotNull(varlistprice);
        Assert.assertEquals(currency,"PAISA");
        Assert.assertNotNull(isveg);
        Assert.assertNotNull(frequency);
        Assert.assertNotNull(tenure);
        Assert.assertNotNull(avail);
        Assert.assertNotNull(invinstock);
    }

    @Test(dataProvider = "pdpproduct",description = "To verify component/meal details")
    public void pdpWithProductIdComponents(String storeId, String productId,String startdate,String starttime,String endtime) {
        String pdpresponse = swiggyListingService.pdpWithProductId(storeId, productId, startdate, starttime, endtime).ResponseValidator.GetBodyAsText();
        int storeid=JsonPath.read(pdpresponse,"$.data.[0].components[0].store_id");
        String productid=JsonPath.read(pdpresponse,"$.data.[0].components[0].product_id");
        String id=JsonPath.read(pdpresponse,"$.data.[0].components[0].id");
        String spinid=JsonPath.read(pdpresponse,"$.data.[0].components[0].spin");
        Object meta=JsonPath.read(pdpresponse,"$.data.[0].components[0].meta");
        Object nextscheduled=JsonPath.read(pdpresponse,"$.data.[0].components[0].next_scheduled_at");
        Assert.assertNotNull(storeid);
        Assert.assertNotNull(productid);
        Assert.assertNotNull(id);
        Assert.assertNotNull(spinid);
        Assert.assertNotNull(meta);
        Assert.assertNotNull(nextscheduled);

    }
    @Test(dataProvider = "pdpwithoutproduct",description = "To verify the pdp api without product id")
    public void pdpWithoutProduct(String storeId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithoutProductId(storeId,startdate,starttime,endtime).ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(pdpresponse,"$.data.[0].components");
    }


    @Test(dataProvider = "pdpwithoutproduct",description = "To verify remaining count is less than or equal to total inv count")
    public void pdpWithoutProductIdInvRemCount(String storeId,String startdate,String starttime,String endtime) throws Exception{
        String pdpresponse=swiggyListingService.pdpWithoutProductId(storeId,startdate,starttime,endtime).ResponseValidator.GetBodyAsText();
        int remcount=JsonPath.read(pdpresponse,"$.data.[0].variations[0].inventory.remaining");
        int totalcount=JsonPath.read(pdpresponse,"$.data.[0].variations[0].inventory.total");
        if(remcount>totalcount){
            Assert.fail();
        }

    }


    @Test(dataProvider = "pdpproductpast",description = "To verify response with past date")
    public void pdpWithProductIdPastDate(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId,productId,startdate,starttime,endtime).ResponseValidator.GetBodyAsText();
        List<String> data=JsonPath.read(pdpresponse,"$.data");
        if(data.size()>0){
            Assert.fail();
        }
    }

    @Test(dataProvider = "pdpproduct",description = "verify next start time is is greater than current date time")
    public void VerifynextSheduled(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId, productId, startdate, starttime, endtime).ResponseValidator.GetBodyAsText();
        long nextstarttime=JsonPath.read(pdpresponse,"$.data[0].components[0].next_scheduled_at.start_time");
        long currenttime=CMSDailyHelper.createEPOCforDays();
        if(currenttime>nextstarttime){
            Assert.fail();
        }

    }

    @Test(dataProvider = "pdpproduct",description = "verify next change in avail is is greater than current date time")
    public void VerifynextChange(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId, productId, startdate, starttime, endtime).ResponseValidator.GetBodyAsText();
        long nextchange=JsonPath.read(pdpresponse,"$.data[0].variations[0].availability.next_change");
        long currenttime=CMSDailyHelper.createEPOCforDays();
        if(currenttime>nextchange){
            Assert.fail();
        }

    }

    @Test(dataProvider = "pdpproduct",description = "verify pdp price is matching with get price api")
    public void ComparePrice(String storeId, String productId,String startdate,String starttime,String endtime){
        String pdpresponse=swiggyListingService.pdpWithProductId(storeId, productId, startdate, starttime, endtime).ResponseValidator.GetBodyAsText();
        String storeid=JsonPath.read(pdpresponse,"$.data[0].store_id");
        String spinid=JsonPath.read(pdpresponse,"$.data[0].variations[0].spin");
        String key=storeid+"-"+spinid;
        int pdpprice=JsonPath.read(pdpresponse,"$.data[0].variations[0].price.price");
        int pdplistprice=JsonPath.read(pdpresponse,"$.data[0].variations[0].price.list_price");
        double finalpdpprice=pdpprice*0.01;
        double finalpdplistprice=pdplistprice*0.01;
        String pricingresponse=cmsDailyHelper.getPricing(key,CMSDailyContants.service_line_plan).ResponseValidator.GetBodyAsText();
        int list_price=JsonPath.read(pricingresponse,"$.data[0].pricing_construct.price_list.list_price");
        int price=JsonPath.read(pricingresponse,"$.data[0].pricing_construct.price_list.total_price");
        Assert.assertEquals(list_price,finalpdplistprice,0);
        Assert.assertEquals(price,finalpdpprice,0);


    }


    @Test(dataProvider = "pdpproduct",description = "Get inventory from inventory service and verify with pdp api for plan")
    public void CompareInventory(String storeId, String productId,String startdate,String starttime,String endtime) {
        String pdpresponse = swiggyListingService.pdpWithProductId(storeId, productId, startdate, starttime, endtime).ResponseValidator.GetBodyAsText();
        String storeid = JsonPath.read(pdpresponse, "$.data[0].store_id");
        String productid=JsonPath.read(pdpresponse,"$.data[0].product_id");
        int pdpinventory=JsonPath.read(pdpresponse,"$.data[0].variations[0].inventory.remaining");
        String getinvresponse=swiggyListingService.searchInventoryPlan(storeid,productid).ResponseValidator.GetBodyAsText();
        int invmaxcap=JsonPath.read(getinvresponse,"$.data[0].inventory_construct.inventories.max_cap");
        Assert.assertEquals(pdpinventory,invmaxcap);


    }



}




