package com.swiggy.api.daily.checkout.pojo.subscription;

import io.advantageous.boon.json.annotations.JsonProperty;

public class CouponDetails {

    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("couponErrorMessage")
    private String couponErrorMessage;
    @JsonProperty("couponDescription")
    private String couponDescription;
    @JsonProperty("couponApplied")
    private Boolean couponApplied;

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public CouponDetails withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("couponErrorMessage")
    public String getCouponErrorMessage() {
        return couponErrorMessage;
    }

    @JsonProperty("couponErrorMessage")
    public void setCouponErrorMessage(String couponErrorMessage) {
        this.couponErrorMessage = couponErrorMessage;
    }

    public CouponDetails withCouponErrorMessage(String couponErrorMessage) {
        this.couponErrorMessage = couponErrorMessage;
        return this;
    }

    @JsonProperty("couponDescription")
    public String getCouponDescription() {
        return couponDescription;
    }

    @JsonProperty("couponDescription")
    public void setCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
    }

    public CouponDetails withCouponDescription(String couponDescription) {
        this.couponDescription = couponDescription;
        return this;
    }

    @JsonProperty("couponApplied")
    public Boolean getCouponApplied() {
        return couponApplied;
    }

    @JsonProperty("couponApplied")
    public void setCouponApplied(Boolean couponApplied) {
        this.couponApplied = couponApplied;
    }

    public CouponDetails withCouponApplied(Boolean couponApplied) {
        this.couponApplied = couponApplied;
        return this;
    }

}
