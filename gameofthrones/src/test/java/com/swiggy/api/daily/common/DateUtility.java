package com.swiggy.api.daily.common;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DateUtility {

    public static long getMidNightTimeOfCurrentDayInMilisecond()
    {
        Calendar c = Calendar.getInstance();
        long now = c.getTimeInMillis();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }
    public static long getFutureDateInMilisecond() {
        return DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100;
    }
    public static long getCurrentDateInMilisecond() {
        return DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000;
    }
    public static String getFutureDateInDayMilisecond(int day) {
        return ""+DateUtils.addDays(new Date(), day).toInstant().getEpochSecond() * 1000;
    }
    public static long getFutureDateInMinMilisecond(int min) {
        return DateUtils.addMinutes(new Date(), min).toInstant().getEpochSecond() * 1000;
    }
    public static String getFutureDateInReadableFormat(int day)
    {
        return DateUtils.addDays(new Date(), day).toInstant().toString().split("T")[0];
    }
    public static String getCurrentDateInReadableFormat()
    {
        return DateUtils.addDays(new Date(), 0).toInstant().toString().split("T")[0];
    }

    public static String getDayOfDateInReadableFormat(String  date) throws ParseException{
        Date newDate=new SimpleDateFormat("yyyy-MM-dd").parse(date);
        return new SimpleDateFormat("EEEE").format(newDate);
    }
    public static String getCurrentTimeInReadableFormat()
    {
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(date).replace(":","");
    }

    public static String getFutureTimeInReadableFormat(int hours,int min)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        calendar.add(Calendar.MINUTE, min);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(calendar.getTime()).replace(":","");
    }

    public static String getDateInReadableFormat(String date,int count)throws ParseException
    {
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(date));
        c.add(Calendar.DAY_OF_MONTH,count);
        return sdf.format(c.getTime());

    }
    public static String getCurrentTimeInReadableFormatWithColon()
    {
        Calendar cal = Calendar.getInstance();
        Date date=cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(date);
    }
    public static String getFutureTimeInReadableFormatWithColon(int hours,int min)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, hours);
        calendar.add(Calendar.MINUTE, min);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        return dateFormat.format(calendar.getTime());
    }

    public static long convertReadableDateTimeToInMilisecond(String date,String time) throws ParseException {
        String str = date+" "+time;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HHmm");
        Date parseDate = df.parse(str);
        long epoch = parseDate.getTime();
        return epoch;
    }

    public static long convertReadableDateTimeToInMilisecond(String date) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date parseDate = df.parse(date);
        long epoch = parseDate.getTime();
        return epoch;
    }

    public static List<Long> getEpochBetweenDate(String startDate, String endDate,String deliveryStartTime,String weekendService) throws ParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate start = LocalDate.parse(startDate);
        LocalDate end = LocalDate.parse(endDate);
        List<Long> totalDates = new ArrayList<>();
        while (!start.isAfter(end)) {
            String day=DateUtility.getDayOfDateInReadableFormat(start.format(formatter));
            if(((day.equals("Saturday")) || (day.equals("Sunday")))&&weekendService.equals("WEEKDAY"))
            {
                start = start.plusDays(1);
                continue;
            }else {
                long dateTime = convertReadableDateTimeToInMilisecond(start.format(formatter), deliveryStartTime);
                totalDates.add(dateTime);
                start = start.plusDays(1);
            }
        }
        return totalDates;
    }

    public static String getNextWorkingDateInReadableFormat(String date)throws ParseException
    {
        boolean day=true;
        String newDate=date;
        while (day==true) {
            newDate=getDateInReadableFormat(newDate,1);
            if(DateUtility.getDayOfDateInReadableFormat(newDate).equals("Saturday")||DateUtility.getDayOfDateInReadableFormat(newDate).equals("Sunday"))
            { day=true;
            }else{day=false;}
        }
        return newDate;
    }

    public static String getRedableDateFromEpoch(Long epochDate)
    {
        Date date = new Date(epochDate);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        String readableDate = dateFormat.format(date);
        return readableDate;
    }

    public static String getRedableDateWithoutTimeFromEpoch(Long epochDate)
    {
        Date date = new Date(epochDate);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        String readableDate = dateFormat.format(date);
        return readableDate;
    }

    public static String getFutureDateBasedOnDayFromCurrentDate(String day) throws ParseException {
        SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(sdf.parse(DateUtility.getCurrentDateInReadableFormat()));
        if(day.equals("Saturday")) {
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY) {
                c.add(Calendar.DATE, 1);
            }
        }
        if(day.equals("Sunday"))
        {
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
                c.add(Calendar.DATE, 1);
            }
        }
        if(day.equals("Thursday"))
        {
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.THURSDAY) {
                c.add(Calendar.DATE, 1);
            }
        }
        if(day.equals("Friday"))
        {
            while (c.get(Calendar.DAY_OF_WEEK) != Calendar.FRIDAY) {
                c.add(Calendar.DATE, 1);
            }
        }
        return sdf.format(c.getTime());
    }



    public static String getWorkingDateFromCurrentDate(String date,int workdays) throws ParseException {
    /*
      Param1: date - Start date of working day
              dayCount- How many working days wants to add in date
     */

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate result = LocalDate.parse(date);
        int addedDays = 0;
        if(getDayOfDateInReadableFormat(date).equals("Saturday")||getDayOfDateInReadableFormat(date).equals("Sunday"))
        {
            workdays=workdays+1;
        }
        while (addedDays < workdays) {
            result = result.plusDays(1);
            if (!(result.getDayOfWeek() == DayOfWeek.SATURDAY ||
                    result.getDayOfWeek() == DayOfWeek.SUNDAY)) {
                ++addedDays;
            }
        }

        return result.format(formatter);
    }
}
