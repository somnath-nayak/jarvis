package com.swiggy.api.daily.delivery.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.delivery.helper.ServiceabilityHelper;
import framework.gameofthrones.JonSnow.Processor;
import io.gatling.commons.stats.assertion.In;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.swiggy.api.daily.delivery.dp.listingDP;

public class DailyServiceabilityTest extends listingDP{
    ServiceabilityHelper serviceabilityHelper = new ServiceabilityHelper();
    static Integer polygId = 0;

    //todo

/*
    1) Verify the positive case daily polygon + non blackzone on given timeslot + packsize 1 --> and serviceable
    2) Verify daily polygon + non blackzone on given timeslot + packsize 10 (blackzoned) --> and non serviceable
    3) Verify daily polygon + non blackzone on given timeslot + packsize 10 (not getting straightlined slots) --> and non serviceable
    4) Verify non-daily polygon + non blackzone on given timeslot + packsize 1 -->
    5) Verify non-daily polygon + non blackzone on given timeslot + packsize 10 (blackzoned) --> and non serviceable
    6) Verify non-daily polygon + non blackzone on given timeslot + packsize 10 (not getting straightlined slots) --> and non serviceable
    7) Verify the positive case daily polygon + non blackzone on given timeslot + packsize 1 + capacity not available--> and serviceable
*/

    //verify the same for listingPDP as well


    @Test(dataProvider = "dailyListingTest")
    public void dailyListingTest(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled)
    {
        Processor processor = serviceabilityHelper.dailyListing(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());



    }

    @Test(dataProvider = "dailyListingTestPDP")
    public void dailyListingPDPTest(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(serviceabilityHelper.verifyStatusFromResponse(processor.ResponseValidator.GetBodyAsText(),"Success"));

    }

    @Test(dataProvider = "dailyListingNonDailyTestPDP")
    public void dailyListingPDPNonDailyTest(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        Assert.assertTrue(serviceabilityHelper.verifyStatusFromResponse(processor.ResponseValidator.GetBodyAsText(),"Failed"));

    }

    @Test(dataProvider = "dailyListingNonDailyTestPDP")
    public void dailyListingPDPPartialBlackzoneSingleSlotTest(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());

    }

    @Test(dataProvider = "dailyListingOneCompleteNonBZAndOnePartialBZTestPDP")
    public void dailyListingOneCompleteNonBZAndOnePartialBZTestPDP(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());

    }

    @Test(dataProvider = "dailyListingCompleteBZTestPDP")
    public void dailyListingCompleteBZTestPDP(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());

    }

    @Test(dataProvider = "dailyListingCompleteBZAndExceptOnePartialBZTestPDP")
    public void dailyListingCompleteBZAndExceptOnePartialBZTestPDP(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());

    }

    @Test(dataProvider = "dailyListingOneCompleteNonBZTestPDP")
    public void dailyListingOneCompleteNonBZTestPDP(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
    }

    @Test(dataProvider = "dailyListingOneCompleteNonBZAndStraigtLineFailedByPackSizeTestPDP")
    public void dailyListingOneCompleteNonBZAndStraigtLineFailedByPackSizeTestPDP(String lat, String lon, long cityId, int packSize, String restaurantIds, String startTime, String endTime, boolean clusterDebugEnabled,String restaurantId)
    {
        Processor processor = serviceabilityHelper.dailyListingPDP(lat,lon,"",startTime,endTime,cityId,packSize,clusterDebugEnabled,restaurantIds);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());

    }

    @Test(dataProvider = "capacityCheck",priority = 1)
    public void capacityCheck(String lat, String lon, String uniqueId, String startTime, String endTime, String status_message)
    {
        Processor processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String response = processor.ResponseValidator.GetBodyAsText().toString();
        String statusMessage = JsonPath.read(response, "$.statusMessage").toString();
        Assert.assertTrue(status_message.equalsIgnoreCase(statusMessage));
        String count = JsonPath.read(response, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("count = " + count);
        Assert.assertTrue(Integer.parseInt(count) > 1);


    }

    @Test(dataProvider = "capacityIncrementCheck",priority = 2)
    public void capacityIncermentCheck(String lat, String lon, String uniqueId, String startTime, String endTime,String status_message)
    {
        Processor processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String response = processor.ResponseValidator.GetBodyAsText().toString();
        String initialCount = JsonPath.read(response, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("count = " + initialCount);
        processor = serviceabilityHelper.incrementCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String incrementResponse = processor.ResponseValidator.GetBodyAsText().toString();
        String statusMessage = JsonPath.read(incrementResponse, "$.statusMessage").toString();
        Assert.assertTrue(status_message.equalsIgnoreCase(statusMessage));
        String statusCode = JsonPath.read(incrementResponse, "$.data.updateResponse..status").toString().replace("[", "").replace("]", "");;
        System.out.println("statusCode = " + statusCode);
        Assert.assertEquals(Integer.parseInt(statusCode),1);
        processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String finalResponse = processor.ResponseValidator.GetBodyAsText().toString();
        String finalCount = JsonPath.read(finalResponse, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("finalCount = " + finalCount);
        Assert.assertEquals(Integer.parseInt(finalCount)+1,Integer.parseInt(initialCount));


    }

    @Test(dataProvider = "capacityDecrementCheck",priority = 3)
    public void capacityDecermentCheck(String lat, String lon, String uniqueId, String startTime, String endTime,String status_message)
    {
        Processor processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String response = processor.ResponseValidator.GetBodyAsText().toString();
        String initialCount = JsonPath.read(response, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("count = " + initialCount);
        processor = serviceabilityHelper.decrementCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String incrementResponse = processor.ResponseValidator.GetBodyAsText().toString();
        String statusMessage = JsonPath.read(incrementResponse, "$.statusMessage").toString();
        Assert.assertTrue(status_message.equalsIgnoreCase(statusMessage));
        String statusCode = JsonPath.read(incrementResponse, "$.data.updateResponse..status").toString().replace("[", "").replace("]", "");;
        System.out.println("statusCode = " + statusCode);
        Assert.assertEquals(Integer.parseInt(statusCode),1);
        processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String finalResponse = processor.ResponseValidator.GetBodyAsText().toString();
        String finalCount = JsonPath.read(finalResponse, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("finalCount = " + finalCount);
        Assert.assertEquals(Integer.parseInt(finalCount)-1,Integer.parseInt(initialCount));


    }

    @Test(dataProvider = "capacityIncrementCheck", priority = 4)
    public void capacityIncermentAndDecrementCheckBetWeenE2E(String lat, String lon, String uniqueId, String startTime, String endTime,String status_message)
    {
        Processor processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String response = processor.ResponseValidator.GetBodyAsText().toString();
        String initialCount = JsonPath.read(response, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("count = " + initialCount);

        for(int i =0 ; i <= Integer.parseInt(initialCount) ; i++)
        {
            processor = serviceabilityHelper.incrementCapacity(lat,lon,String.valueOf(Integer.parseInt(uniqueId)+i),startTime,endTime);
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
            System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
            String incrementResponse = processor.ResponseValidator.GetBodyAsText().toString();
            String statusMessage = JsonPath.read(incrementResponse, "$.statusMessage").toString();
            Assert.assertTrue(status_message.equalsIgnoreCase(statusMessage));
            String statusCode = JsonPath.read(incrementResponse, "$.data.updateResponse..status").toString().replace("[", "").replace("]", "");
            System.out.println("statusCode = " + statusCode);
            if(i<Integer.parseInt(initialCount))
            {
                Assert.assertEquals(Integer.parseInt(statusCode),1);
            }else
            {
                Assert.assertEquals(Integer.parseInt(statusCode),0);
            }


        }
        processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String finalResponse = processor.ResponseValidator.GetBodyAsText().toString();
        String finalCount = JsonPath.read(finalResponse, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("finalCount = " + finalCount);
        Assert.assertEquals(Integer.parseInt(finalCount),0);

        for(int i =0 ; i <= Integer.parseInt(initialCount) ; i++)
        {
            processor = serviceabilityHelper.decrementCapacity(lat,lon,String.valueOf(Integer.parseInt(uniqueId)+i),startTime,endTime);
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
            System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
            String decrementResponse = processor.ResponseValidator.GetBodyAsText().toString();
            String destatusMessage = JsonPath.read(decrementResponse, "$.statusMessage").toString();
            Assert.assertTrue(status_message.equalsIgnoreCase(destatusMessage));
            String destatusCode = JsonPath.read(decrementResponse, "$.data.updateResponse..status").toString().replace("[", "").replace("]", "");;
            System.out.println("statusCode = " + destatusCode);
            if(i<Integer.parseInt(initialCount)){
                Assert.assertEquals(Integer.parseInt(destatusCode),1);
            }else
            {
                Assert.assertEquals(Integer.parseInt(destatusCode),0);
            }

        }

        processor = serviceabilityHelper.fetchCapacity(lat,lon,uniqueId,startTime,endTime);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        System.out.println("processor.ResponseValidator.GetBodyAsText() = " + processor.ResponseValidator.GetBodyAsText());
        String finalResponse1 = processor.ResponseValidator.GetBodyAsText().toString();
        String finalCount1 = JsonPath.read(finalResponse1, "$.data.updateResponse..count").toString().replace("[", "").replace("]", "");;
        System.out.println("finalCount1 = " + finalCount1);
        Assert.assertEquals(Integer.parseInt(finalCount1),Integer.parseInt(initialCount));

    }


}
