package com.swiggy.api.daily.checkout.pojo.subscription;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class DeliverySchedule {

    @JsonProperty("startDate")
    private Long startDate;
    @JsonProperty("excludedDates")
    private List<Object> excludedDates = null;
    @JsonProperty("tenure")
    private Long tenure;
    @JsonProperty("hasWeekend")
    private Boolean hasWeekend;

    @JsonProperty("startDate")
    public Long getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public DeliverySchedule withStartDate(Long startDate) {
        this.startDate = startDate;
        return this;
    }

    @JsonProperty("excludedDates")
    public List<Object> getExcludedDates() {
        return excludedDates;
    }

    @JsonProperty("excludedDates")
    public void setExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
    }

    public DeliverySchedule withExcludedDates(List<Object> excludedDates) {
        this.excludedDates = excludedDates;
        return this;
    }

    @JsonProperty("tenure")
    public Long getTenure() {
        return tenure;
    }

    @JsonProperty("tenure")
    public void setTenure(Long tenure) {
        this.tenure = tenure;
    }

    public DeliverySchedule withTenure(Long tenure) {
        this.tenure = tenure;
        return this;
    }

    @JsonProperty("hasWeekend")
    public Boolean getHasWeekend() {
        return hasWeekend;
    }

    @JsonProperty("hasWeekend")
    public void setHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
    }

    public DeliverySchedule withHasWeekend(Boolean hasWeekend) {
        this.hasWeekend = hasWeekend;
        return this;
    }
}
