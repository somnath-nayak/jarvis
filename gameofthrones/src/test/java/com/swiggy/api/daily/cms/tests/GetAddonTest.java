package com.swiggy.api.daily.cms.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.daily.cms.dp.CMSDailyDP;
import com.swiggy.api.daily.cms.helper.CMSDailyContants;
import com.swiggy.api.daily.cms.helper.CMSDailyHelper;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GetAddonTest extends CMSDailyDP {

    CMSDailyHelper helper=new CMSDailyHelper();
    SoftAssert sc=new SoftAssert();
    List<String> lip=new ArrayList<>();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();


    @Test(dataProvider = "getaddonsdp",description = "This test will verify presence of Product Type and Store in addon listing")
    public void getAddon(String store_id){
        String res= helper.getAddon(store_id).ResponseValidator.GetBodyAsText();
        String storeid= JsonPath.read(res,"$.data[0]..store_id").toString().replace("[","").replace("]","");
        sc.assertEquals(storeid,store_id);
        String ptype = JsonPath.read(res, "$.data[0].product_type").toString().replace("[","").replace("]","");
        sc.assertEquals(ptype,CMSDailyContants.entity_type.get(2));
        sc.assertAll();
    }

    @Test(dataProvider = "getaddonsdp",description = "This test will verify price of addon with pricing service in addon listing")
    public void getAddonPriceTotalVerify(String store_id){
        String res= helper.getAddon(store_id).ResponseValidator.GetBodyAsText();
        String storeid= JsonPath.read(res,"$.data[0]..store_id").toString().replace("[","").replace("]","");
        sc.assertEquals(storeid,store_id);
        String spins = JsonPath.read(res, "$.data..spin").toString().replace("[","").replace("]","").replace("\"","");
        List<String> spin = Arrays.asList(spins.split("\\s*,\\s*"));
        sc.assertTrue(spin.size()>0,"Data Coming as blank");
        String prices = JsonPath.read(res, "$.data..price.price").toString().replace("[","").replace("]","").replace("\"","");
        List<String> price = Arrays.asList(prices.split("\\s*,\\s*"));
        sc.assertTrue(price.size()>0,"Data Coming as blank");
        for (int i = 0; i < spin.size(); i++) {
            String key = store_id + "-" + spin.get(i);
            String priceresponse = helper.getPricing(key, CMSDailyContants.service_line_meal).ResponseValidator.GetBodyAsText();
            String pri=JsonPath.read(priceresponse,"$.data..pricing_construct.price_list.price").toString().replace("[","").replace("]","");
            Double a=Double.parseDouble(pri);
            lip.add(a.toString());
        }
        sc.assertAll();
    }

    @Test(dataProvider = "getaddonsdp",description = "This test will verify all the fields of Addon Listing")
    public void getAddonMetaVerify(String store_id) {
        String res = helper.getAddon(store_id).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(schemaValidatorUtils.validateSchema("/../Data/SchemaSet/Json/CMS/addonlisting.txt",res));
        sc.assertAll();
    }

}
