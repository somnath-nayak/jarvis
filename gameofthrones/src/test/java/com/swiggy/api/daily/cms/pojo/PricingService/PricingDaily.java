
package com.swiggy.api.daily.cms.pojo.PricingService;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "key",
    "pricing_construct",
    "meta"
})
public class PricingDaily {

    @JsonProperty("key")
    private String key;
    @JsonProperty("pricing_construct")
    private Pricing_construct pricing_construct;
    @JsonProperty("meta")
    private Meta meta;

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public void setKey(String key) {
        this.key = key;
    }

    public PricingDaily withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("pricing_construct")
    public Pricing_construct getPricing_construct() {
        return pricing_construct;
    }

    @JsonProperty("pricing_construct")
    public void setPricing_construct(Pricing_construct pricing_construct) {
        this.pricing_construct = pricing_construct;
    }

    public PricingDaily withPricing_construct(Pricing_construct pricing_construct) {
        this.pricing_construct = pricing_construct;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public PricingDaily withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }
    public PricingDaily setData(String key,String product_id,String sku_id,String spin,String store_id,Integer list_price,Integer price,Integer total_price,Integer cgst, Integer cgst_percentage, String currency, Integer igst, Integer igst_percentage, boolean inclusive_of_taxes,Integer sgst_utgst,Integer sgst_utgst_percentage){
        Meta m=new Meta().setData(product_id, sku_id, spin, store_id);
        Pricing_construct pc=new Pricing_construct().setData(list_price, price, total_price, cgst, cgst_percentage, currency, igst, igst_percentage, inclusive_of_taxes, sgst_utgst, sgst_utgst_percentage);
        this.withKey(key).withMeta(m).withPricing_construct(pc);
        return this;
    }

}
