package com.swiggy.api.daily.vendor.helper;
import com.swiggy.api.daily.vendor.pojo.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class VendorHelper {

    static Initialize gameofthrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();

    public Processor login(String data) throws IOException
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("vendor", "login", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor pause(String data, String access_token) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Cookie", "Swiggy_Session-alpha="+access_token);
        GameOfThronesService service = new GameOfThronesService("vendor", "clientPause", gameofthrones);
        String[] payloadparams = new String[] {data};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor getRestaurantAvailability(String restaurantID) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("srs", "restavailability", gameofthrones);
        String[] urlparams = new String[] {restaurantID};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor orderDispatch(String orderDispatchPOJO, String access_token) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Cookie", "Swiggy_Session-alpha="+access_token);
        GameOfThronesService service = new GameOfThronesService("vendor", "orderdispatch", gameofthrones);
        String[] payloadaparams = new String[] {orderDispatchPOJO};
        Processor processor = new Processor(service, requestHeaders, payloadaparams);
        return processor;
    }

    public Processor pastOrders(String restaurantID, String orderID, String limit, String offset, String ordered_time__gte, String ordered_time__lte) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("finance", "pastOrder", gameofthrones);
        String[] urlparams = new String[] {restaurantID,orderID,limit,offset,ordered_time__gte,ordered_time__lte};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor createSlotsVendorDaily(List<SlotServicePOJO> createSlot, String access_token) throws IOException {
        GameOfThronesService services = new GameOfThronesService("slot", "createSlots", gameofthrones);
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("Content-Type", "application/json");
        requestheaders.put("user-info","Neha Pandey");
        requestheaders.put("service", "DAILY");
        requestheaders.put("Cookie", "Swiggy_Session-alpha="+access_token);
        String[] payloadaparams = new String[] {jsonHelper.getObjectToJSON(createSlot)};
        Processor processor = new Processor(services, requestheaders, payloadaparams);
        return processor;
    }

    public Processor createHolidaySlots(HolidaySlotPOJO data) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta","{\"source\" : \"Neha-vendor-automation\" }");
        GameOfThronesService service = new GameOfThronesService("srs", "holidaySlot", gameofthrones);
        String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(data)};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor createNormalRestaurantSlot(RestaurantSlotPOJO data) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("user_meta","{\"source\" : \"Neha-vendor-automation\" }");
        GameOfThronesService service = new GameOfThronesService("srs", "normalRestSlot", gameofthrones);
        String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(data)};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor pauseVendor(PausePOJO data, String access_token) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Cookie", "Swiggy_Session-alpha="+access_token);
        GameOfThronesService service = new GameOfThronesService("vendor", "clientPause", gameofthrones);
        String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(data)};
        Processor processor = new Processor(service, requestHeaders, payloadparams);
        return processor;
    }

    public Processor paginatedSearch(String cityId, int page, int rows_per_page) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("srs", "paginatedSearch", gameofthrones);
        String[] urlparams = new String[] {cityId,page+"",rows_per_page+""};
        Processor processor = new Processor(service, requestHeaders, null, urlparams);
        return processor;
    }

    public Processor orderDispatchAPI(OrderDispatchPOJO orderDispatchPOJO, String access_token) throws IOException {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("Cookie", "Swiggy_Session-alpha="+access_token);
        GameOfThronesService service = new GameOfThronesService("vendor", "orderdispatch", gameofthrones);
        String[] payloadaparams = new String[] {jsonHelper.getObjectToJSON(orderDispatchPOJO)};
        Processor processor = new Processor(service, requestHeaders, payloadaparams);
        return processor;
    }

}
