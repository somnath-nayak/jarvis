
package com.swiggy.api.daily.subscription.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
    "item_details",
    "schedule_details",
    "delivery_slot",
    "address_details"
})
public class Metadata {

    @JsonProperty("item_details")
    private List<ItemDetail> itemDetails = null;
    @JsonProperty("schedule_details")
    private ScheduleDetails scheduleDetails;
    @JsonProperty("delivery_slot")
    private DeliverySlot deliverySlot;
    @JsonProperty("address_details")
    private AddressDetails addressDetails;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("item_details")
    public List<ItemDetail> getItemDetails() {
        return itemDetails;
    }

    @JsonProperty("item_details")
    public void setItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
    }

    public Metadata withItemDetails(List<ItemDetail> itemDetails) {
        this.itemDetails = itemDetails;
        return this;
    }

    @JsonProperty("schedule_details")
    public ScheduleDetails getScheduleDetails() {
        return scheduleDetails;
    }

    @JsonProperty("schedule_details")
    public void setScheduleDetails(ScheduleDetails scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
    }

    public Metadata withScheduleDetails(ScheduleDetails scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
        return this;
    }

    @JsonProperty("delivery_slot")
    public DeliverySlot getDeliverySlot() {
        return deliverySlot;
    }

    @JsonProperty("delivery_slot")
    public void setDeliverySlot(DeliverySlot deliverySlot) {
        this.deliverySlot = deliverySlot;
    }

    public Metadata withDeliverySlot(DeliverySlot deliverySlot) {
        this.deliverySlot = deliverySlot;
        return this;
    }

    @JsonProperty("address_details")
    public AddressDetails getAddressDetails() {
        return addressDetails;
    }

    @JsonProperty("address_details")
    public void setAddressDetails(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
    }

    public Metadata withAddressDetails(AddressDetails addressDetails) {
        this.addressDetails = addressDetails;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Metadata withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public Metadata setDefaultData(){
        return this.withItemDetails(new ArrayList<ItemDetail>() {{add(new ItemDetail().setDefaultData());}})
                .withScheduleDetails(new ScheduleDetails().setDefaultData())
                .withDeliverySlot(new DeliverySlot().setDefaultData())
                .withAddressDetails(new AddressDetails().setDefaultData());

    }

}
