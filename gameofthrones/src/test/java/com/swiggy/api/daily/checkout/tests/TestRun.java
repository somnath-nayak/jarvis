package com.swiggy.api.daily.checkout.tests;

import com.swiggy.api.daily.checkout.helper.DailyCheckoutCommonUtils;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutConstants;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutOrderHelper;
import com.swiggy.api.daily.checkout.helper.DailyCheckoutOrderValidator;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestRun {

    DailyCheckoutCommonUtils utils= new DailyCheckoutCommonUtils();
    DailyCheckoutOrderValidator validator=new DailyCheckoutOrderValidator();
    DailyCheckoutOrderHelper orderHelper=new DailyCheckoutOrderHelper();

    @Test
    public void testDB(){

        List<Map<String, Object>> a =utils.getDBTransactionDetails("payment_transaction_v2","108366080775");
        validator.validateDbTransactionDetails(a,"PayTM-SSO","1581691","success","30.0");

        List<Map<String, Object>> a1 =utils.getDBTransactionDetails("refund_transaction_v2","108366080775");
        validator.validateDbRefundTransactionDetails(a1,"PayTM-SSO","FAILED","30.0");
    }


    @Test ()
    public void dailyPlaceOrderTest1(){
        String orderJobId= orderHelper.dailyCreateOrder(DailyCheckoutConstants.USER_MOBILE,DailyCheckoutConstants.USER_PASSWORD,
                DailyCheckoutConstants.CITY_ID,DailyCheckoutConstants.STOREID,"PLAN");
        System.out.println("Order job id:--->"+ orderJobId);
    }

    @Test ()
    public void dailyPlaceOrderTest2(){

        HashMap<String, String> userInfo=utils.dailyLogin(DailyCheckoutConstants.USER_MOBILE,DailyCheckoutConstants.USER_PASSWORD);

        Processor p=orderHelper.dailyCreateOrderExternal(userInfo.get("tid"),userInfo.get("token"),
                DailyCheckoutConstants.CITY_ID,DailyCheckoutConstants.STOREID,"PLAN");
        System.out.println(p.ResponseValidator.GetBodyAsText());
    }



}
