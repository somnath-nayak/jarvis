package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lat",
        "lng"
})
public class Location {

    @JsonProperty("lat")
    private Integer lat;
    @JsonProperty("lng")
    private Integer lng;

    @JsonProperty("lat")
    public Integer getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(Integer lat) {
        this.lat = lat;
    }

    public Location withLat(Integer lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public Integer getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(Integer lng) {
        this.lng = lng;
    }

    public Location withLng(Integer lng) {
        this.lng = lng;
        return this;
    }



}