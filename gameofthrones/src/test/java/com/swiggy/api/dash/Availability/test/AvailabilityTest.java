package com.swiggy.api.dash.Availability.test;

import com.swiggy.api.dash.Availability.helper.AvailabilityConstant;
import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;
import com.swiggy.api.dash.Availability.helper.DeleteData;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.RedisHelper;
//import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.springframework.data.redis.core.RedisTemplate;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.stream.IntStream;

public class AvailabilityTest extends AvailabilityDp {


    RedisHelper redisHelper=new RedisHelper();


    @Test(dataProvider = "availabilitydata", description = "Verify store availibility ",priority = 1,enabled = true)
    public void verifyAvalibilityOfStore(String fromTime, String toTime, int[] storeList,int[] categoryList,boolean[] response){
        Processor processor=dashAvailabilityHelper.getAvailability(fromTime,toTime,storeList,categoryList);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        for(int i=0;i<response.length;i++){
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool(Integer.toString(storeList[i])),Boolean.valueOf(response[i]));
        }

    }
    @Test(dataProvider = "availabilityCategorydata", description = "Verify category availibility",priority = 2,enabled = true)
    public void verifyAvalibilityOfCategory(int cityId,int areaId,int[] categoryList,boolean result){
        Processor processor=dashAvailabilityHelper.getAvailabilityOfCategory(cityId,areaId,categoryList);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool("availability"),Boolean.valueOf(result));
    }


    @Test(description = "Availibilty response when store not available in kms ",priority = 3,enabled = true)
    public void VerifyAvailibilityOfStoreWhenStoreNotAvailable() {
        int invalidStoreId=1000000;
        Processor processor = dashAvailabilityHelper.getAvailability("2018-08-20T12:00:31.072+05:30", "2018-08-20T12:00:31.072+05:30", new int[]{invalidStoreId}, new int[]{40});
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool(Integer.toString(1000000)),Boolean.valueOf(false));
        ArrayList<String> redisKeys= redisHelper.getAllKeys(AvailabilityConstant.DASH_AVAILIBILITY_REDIS,0);
        Assert.assertTrue(redisKeys.contains("store"+invalidStoreId));


    }
    @Test(dataProvider = "availibilityStoreDataRedisKeyNotAvailable",description = "Verify availibility response when store redis key not available in availibility ",priority = 4,enabled = true)
    public void VerifyAvailibilityOfStoreWhenStoreRedisKeyNotAvailable(String fromTime, String toTime, int[] storeList,int[] categoryList,boolean[] response) {
        IntStream.range(0, storeList.length).forEach(i -> redisHelper.deleteKey(AvailabilityConstant.DASH_AVAILIBILITY_REDIS,0,"store"+storeList[i]));
        Processor processor=dashAvailabilityHelper.getAvailability(fromTime,toTime,storeList,categoryList);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        for(int i=0;i<response.length;i++){
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool(Integer.toString(storeList[i])),Boolean.valueOf(response[i]));
        }
        ArrayList<String> redisKeys= redisHelper.getAllKeys(AvailabilityConstant.DASH_AVAILIBILITY_REDIS,0);
        IntStream.range(0, storeList.length).forEach(i ->Assert.assertTrue(redisKeys.contains("store"+storeList[i])));
    }

    @Test(dataProvider = "availibilityAreaBlackZoneRedisKeyNotAvailable",description = "Verify availibility response when store redis key not available in availibility ",priority = 5,enabled = true)
    public void VerifyAreaRedisKeyNotAvailable(String fromTime, String toTime, int[] storeList,int[] categoryList,boolean[] response,String keyOf) {
        redisHelper.deleteKey(AvailabilityConstant.DASH_AVAILIBILITY_REDIS,0,keyOf+"_blackzone_all");
        Processor processor=dashAvailabilityHelper.getAvailability(fromTime,toTime,storeList,categoryList);
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(),200);
        for(int i=0;i<response.length;i++){
            Assert.assertEquals(processor.ResponseValidator.GetNodeValueAsBool(Integer.toString(storeList[i])),Boolean.valueOf(response[i]));
        }
        ArrayList<String> redisKeys= redisHelper.getAllKeys(AvailabilityConstant.DASH_AVAILIBILITY_REDIS,0);
        Assert.assertTrue(redisKeys.contains(keyOf+"_blackzone_all"));
    }

    @AfterTest
    public void deleteData(){
        //DeleteData.deleteAllData();
    }











}
