package com.swiggy.api.dash.Availability.test;

import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.swiggy.api.dash.Availability.helper.CreateStoresForAvailablity;
import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;

import com.swiggy.api.dash.Availability.models.Createstorecsv;
import com.swiggy.api.dash.kms.pojos.CategoryBuilder;
import com.swiggy.api.dash.kms.pojos.Tags;
import org.testng.annotations.DataProvider;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AvailabilityDp {
    DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
    CreateStoresForAvailablity createStoresForAvailablity=new CreateStoresForAvailablity();
    @DataProvider(name = "availabilitydata", parallel = false)
    public Object[][] availabilitydata() throws Exception{
        ArrayList<String> storeList = new ArrayList<>();
        storeList.add("Store1");
        storeList.add("Store2");
        storeList.add("Store3");
        storeList.add("Store4");
        storeList.add("Store5");
        storeList.add("Store6");
        storeList.add("Store7");
        int cityId_1=dashAvailabilityHelper.createCity("Gurgaon");
        int areadID_1=dashAvailabilityHelper.createArea(cityId_1,"areaForStoreTest_1");

        int storeAreaCategory_1=dashAvailabilityHelper.createCategory("testStoreArea_1");
        int storeAreaCategory_2=dashAvailabilityHelper.createCategory("testStoreArea_2");
        int storeAreaCategory_3=dashAvailabilityHelper.createCategory("testStoreArea_3");
        int storeAreaCategory_4=dashAvailabilityHelper.createCategory("testStoreArea_4");
        int storeAreaCategory_5=dashAvailabilityHelper.createCategory("testStoreArea_5");
        int storeAreaCategory_6=dashAvailabilityHelper.createCategory("testStoreArea_6");
        int storeCityCategory_1=dashAvailabilityHelper.createCategory("cityCategory_1");


        int storeAreaCategory_NOBlackZone_1=dashAvailabilityHelper.createCategory("testStoreArea_noBlackZone_1");
        int storeAreaCategory_NOBlackZone_2=dashAvailabilityHelper.createCategory("testStoreArea_noBlackZone_2");

        List<CategoryBuilder> categories_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_NOBlackZone_1 , "testStoreArea_noBlackZone_1")).returnListOfCategories();
        List<CategoryBuilder> categoriesBlackZone_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_1 , "testStoreArea_1")).returnListOfCategories();
        List<CategoryBuilder> categoriesBlackZone_2 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_1 , "testStoreArea_1")).addNewCategory(new CategoryBuilder(storeAreaCategory_2,"testStoreArea_2")).returnListOfCategories();
        List<CategoryBuilder> categoriesBlackZoneForCity_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeCityCategory_1 , "cityCategory_1")).returnListOfCategories();

        List<Tags> tags = new Tags().returnListOfTags();

        HashMap<String,Integer> storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);



        createStoresForAvailablity.createStore_1_ForAvailability(storeList.get(0),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categories_1,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStore_2_ForAvailability(storeList.get(1),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categories_1,tags,storeAvailibility.get(storeList.get(1)));
        createStoresForAvailablity.createStore_3_ForAvailability(storeList.get(2),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categories_1,tags,storeAvailibility.get(storeList.get(2)));
        createStoresForAvailablity.createStore_4_ForAvailability(storeList.get(3),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",0,categories_1,tags,storeAvailibility.get(storeList.get(3)));
        createStoresForAvailablity.createStore_5_ForAvailability(storeList.get(4),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categoriesBlackZone_1,tags,storeAvailibility.get(storeList.get(4)));
        createStoresForAvailablity.createStore_6_ForAvailability(storeList.get(5),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categoriesBlackZoneForCity_1,tags,storeAvailibility.get(storeList.get(5)));
        createStoresForAvailablity.createStore_7_ForAvailability(storeList.get(6),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categoriesBlackZone_2,tags,storeAvailibility.get(storeList.get(6)));


        HashMap<String,Integer> storeAvailibilityNew=dashAvailabilityHelper.isStoreAvailable(storeList);
        System.out.println(storeAvailibilityNew.size());
        ArrayList<Integer> al=new ArrayList<>();
        return new Object[][]{
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store1")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store2")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store3")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{false}},
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store3")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-25T09:00:31.072+05:30","2018-08-25T09:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store3")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-25T00:00:31.072+05:30","2018-08-25T00:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store3")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store4")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store5")},new int[]{storeAreaCategory_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store5")},new int[]{storeAreaCategory_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store6")},new int[]{storeCityCategory_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_1,storeAreaCategory_2},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_2},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_2,storeAreaCategory_NOBlackZone_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeAreaCategory_2,storeCityCategory_1},new boolean[]{false}},
                {"2018-08-28T12:00:31.072+05:30","2018-08-28T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store7")},new int[]{storeCityCategory_1},new boolean[]{true}},
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store1")},new int[]{},new boolean[]{true}},
                {"2018-08-27T12:00:31.072+05:30","2018-08-26T12:00:31.072+05:30",new int[]{storeAvailibilityNew.get("Store1")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},



        };
    }

    @DataProvider(name = "availabilityCategorydata", parallel = false)
    public Object[][] availabilitydataForCategory(){
        ArrayList<Integer> al=new ArrayList<>();
        int cityId_1=dashAvailabilityHelper.createCity("Gurgaon");
        int cityId_2=dashAvailabilityHelper.createCity("Bangalore");
        System.out.println(cityId_1);
        int areadID_1=dashAvailabilityHelper.createArea(cityId_1,"AreaForTest_1");
        int areadID_2=dashAvailabilityHelper.createArea(cityId_1,"AreaForTest_2");
        int areadID_3=dashAvailabilityHelper.createArea(cityId_1,"AreaForTest_3");
        int areadID_4=dashAvailabilityHelper.createArea(cityId_1,"AreaForTest_4");
        int areadID_5=dashAvailabilityHelper.createArea(cityId_1,"AreaForTest_5");


        int areaId_CityId2_1=dashAvailabilityHelper.createArea(cityId_2,"AreaForTestInOtherCity_1");

        int categoryBlackZoneArea_1=dashAvailabilityHelper.createCategory("testAreaBlackzone_1");
        int categoryBlackZoneArea_2=dashAvailabilityHelper.createCategory("testAreaBlackzone_2");
        int categoryBlackZoneArea_3=dashAvailabilityHelper.createCategory("testAreaBlackzone_3");
        int categoryBlackZoneArea_4=dashAvailabilityHelper.createCategory("testAreaBlackzone_4");
        int categoryBlackZoneArea_5=dashAvailabilityHelper.createCategory("testAreaBlackzone_5");
        int categoryBlackZoneArea_6=dashAvailabilityHelper.createCategory("testAreaBlackzone_6");

        int categoryBlackZoneCity_1=dashAvailabilityHelper.createCategory("testCityBlackzone_1");
        int categoryBlackZoneCity_2=dashAvailabilityHelper.createCategory("testCityBlackzone_2");

        dashAvailabilityHelper.createAreaBlackZone(areadID_1,categoryBlackZoneArea_1);
        dashAvailabilityHelper.createAreaBlackZone(areadID_1,categoryBlackZoneArea_2);
        dashAvailabilityHelper.createAreaBlackZone(areadID_3,categoryBlackZoneArea_5);
        dashAvailabilityHelper.createAreaBlackZone(areadID_4,categoryBlackZoneArea_6);

        dashAvailabilityHelper.createCityBlackzone(cityId_1,categoryBlackZoneCity_1);
        dashAvailabilityHelper.createCityBlackzone(cityId_1,categoryBlackZoneCity_2);


        return new Object[][]{
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneArea_2},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneArea_3},true},
                {cityId_1,areadID_2,new int[]{categoryBlackZoneArea_1},true},
                {cityId_1,areadID_2,new int[]{categoryBlackZoneArea_1},true},
                {cityId_1,areadID_2,new int[]{categoryBlackZoneArea_1,categoryBlackZoneArea_2},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_3,categoryBlackZoneArea_4},true},
                {cityId_2,areaId_CityId2_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneArea_2},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneCity_1},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneCity_1,categoryBlackZoneCity_2},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneCity_1,categoryBlackZoneArea_3},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_3,categoryBlackZoneArea_4},true},
                {cityId_1,areadID_1,new int[]{},true},
                {cityId_2,areaId_CityId2_1,new int[]{categoryBlackZoneCity_1},true},

                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneArea_2},false},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_5},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_6},true},
                {cityId_1,areadID_3,new int[]{categoryBlackZoneArea_5},false},
                {cityId_1,areadID_3,new int[]{categoryBlackZoneArea_6},true},
                {cityId_1,areadID_4,new int[]{categoryBlackZoneArea_5},true},
                {cityId_1,areadID_4,new int[]{categoryBlackZoneArea_6},false},
                {cityId_1,areadID_3,new int[]{categoryBlackZoneArea_5,categoryBlackZoneArea_3},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_5,categoryBlackZoneArea_1},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_5,categoryBlackZoneArea_1},true},

                //Area,city blackzone combination

                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1},false},
                {cityId_1,areadID_2,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1},true},
                {cityId_1,areadID_3,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1,categoryBlackZoneArea_5},true},
                {cityId_1,areadID_5,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1,categoryBlackZoneArea_5},true},
                {cityId_2,areaId_CityId2_1,new int[]{categoryBlackZoneArea_1,categoryBlackZoneCity_1,categoryBlackZoneArea_5},true},
                {cityId_1,areadID_1,new int[]{categoryBlackZoneCity_1,categoryBlackZoneCity_2,categoryBlackZoneArea_1},false},
                {cityId_1,areadID_3,new int[]{categoryBlackZoneCity_1,categoryBlackZoneCity_2,categoryBlackZoneArea_1,},true},
        };
    }


    @DataProvider(name = "availibilityStoreDataRedisKeyNotAvailable", parallel = false)
    public Object[][] availabilitydataRedisNotAvailable() throws Exception {
        ArrayList<String> storeList = new ArrayList<>();
        storeList.add("Store1");
        int cityId_1=dashAvailabilityHelper.createCity("Gurgaon");
        int areadID_1=dashAvailabilityHelper.createArea(cityId_1,"areaForStoreTest_1");
        int storeAreaCategory_NOBlackZone_1=dashAvailabilityHelper.createCategory("testStoreArea_noBlackZone_1");
        List<CategoryBuilder> categories_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_NOBlackZone_1 , "testStoreArea_noBlackZone_1")).returnListOfCategories();
        List<Tags> tags = new Tags().returnListOfTags();
        HashMap<String,Integer> storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);
        createStoresForAvailablity.createStore_1_ForAvailability(storeList.get(0),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categories_1,tags,storeAvailibility.get(storeList.get(0)));
        storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);
        return new Object[][]{
        {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibility.get("Store1")},new int[]{storeAreaCategory_NOBlackZone_1},new boolean[]{true}},

        };
    }
    @DataProvider(name = "availibilityAreaBlackZoneRedisKeyNotAvailable", parallel = false)
    public Object[][] areaBlackzoneRedisKeyNotAvailable() throws Exception {
        ArrayList<String> storeList = new ArrayList<>();
        storeList.add("Store5");
        storeList.add("Store6");
        int cityId_1=dashAvailabilityHelper.createCity("Gurgaon");
        int areadID_1=dashAvailabilityHelper.createArea(cityId_1,"areaForStoreTest_1");
        int storeAreaCategory_1=dashAvailabilityHelper.createCategory("testStoreArea_1");
        int storeCityCategory_1=dashAvailabilityHelper.createCategory("cityCategory_1");

        List<CategoryBuilder> categoriesBlackZone_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_1 , "testStoreArea_1")).returnListOfCategories();
        List<Tags> tags = new Tags().returnListOfTags();
        HashMap<String,Integer> storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);
        List<CategoryBuilder> categoriesBlackZoneForCity_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeCityCategory_1 , "cityCategory_1")).returnListOfCategories();

        createStoresForAvailablity.createStore_5_ForAvailability(storeList.get(0),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categoriesBlackZone_1,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStore_6_ForAvailability(storeList.get(1),"28.435892,77.004015",areadID_1,"areaForStoreTest_1",cityId_1,"Gurgaon",1,categoriesBlackZoneForCity_1,tags,storeAvailibility.get(storeList.get(1)));

        storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);


        return new Object[][]{
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibility.get("Store5")},new int[]{storeAreaCategory_1},new boolean[]{false},"area"},
                {"2018-08-27T12:00:31.072+05:30","2018-08-27T12:00:31.072+05:30",new int[]{storeAvailibility.get("Store6")},new int[]{storeCityCategory_1},new boolean[]{false},"city"},

        };
    }





    public void csvReader(){
        try {
            Reader reader = Files.newBufferedReader(Paths.get(""));
            //  CSVReader csvReader = new CSVReader(reader);

            CsvToBean<Createstorecsv> csvToBean = new CsvToBeanBuilder(reader)
                    .withType(Createstorecsv.class)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            List<Createstorecsv> csvUsers = csvToBean.parse();
            for(Createstorecsv csvUser: csvUsers) {
                System.out.println("Name : " + csvUser.getStoreName());
                System.out.println("Email : " + csvUser.getAreaName());
                System.out.println("PhoneNo : " + csvUser.getCityName());
                System.out.println("Country : " + csvUser.getCategory());
                System.out.println("==========================");
            }

            // Reading Records One by One in a String array

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getCategoryList(){

    }


}


