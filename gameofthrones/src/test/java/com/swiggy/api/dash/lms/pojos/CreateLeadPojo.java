package com.swiggy.api.dash.lms.pojos;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@JsonPropertyOrder({
        "id",
        "duplicates",
        "name",
        "phone_number",
        "lat",
        "lng",
        "address",
        "is_duplicate",
        "parent_id",
        "landmark",
        "email",
        "website",
        "category",
        "subcategory",
        "popular_category",
        "popular_subcategory",
        "suggested_items",
        "list_category",
        "tags",
        "images",
        "chain",
        "id_from_source",
        "source_link",
        "source_rating",
        "source_votes",
        "type",
        "partner_intent",
        "holidays",
        "score",
        "onground_rating",
        "onground_verified",
        "tele_verified",
        "blacklisted",
        "available_jd",
        "available_google",
        "available_swiggy",
        "available_dunzo",
        "time_slots",
        "wait_time",
        "home_delivery",
        "payment_methods",
        "commission",
        "source",
        "city",
        ""
})
public class CreateLeadPojo {
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("duplicates")
    private List<Object> duplicates = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("phone_numbers")
    private String phoneNumber;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("address")
    private String address;
    @JsonProperty("is_duplicate")
    private Boolean isDuplicate;
    @JsonProperty("parent_id")
    private Integer parentId;
    @JsonProperty("landmark")
    private Object landmark;
    @JsonProperty("email")
    private Object email;
    @JsonProperty("website")
    private Object website;
    @JsonProperty("category")
    private Object category;
    @JsonProperty("subcategory")
    private Object subcategory;
    @JsonProperty("popular_category")
    private Object popularCategory;
    @JsonProperty("popular_subcategory")
    private Object popularSubcategory;
    @JsonProperty("suggested_items")
    private Object suggestedItems;
    @JsonProperty("list_category")
    private Object listCategory;
    @JsonProperty("tags")
    private Object tags;
    @JsonProperty("images")
    private Object images;
    @JsonProperty("chain")
    private Object chain;
    @JsonProperty("id_from_source")
    private Object idFromSource;
    @JsonProperty("source_link")
    private Object sourceLink;
    @JsonProperty("source_rating")
    private Float sourceRating;
    @JsonProperty("source_votes")
    private Integer sourceVotes;
    @JsonProperty("type")
    private Object type;
    @JsonProperty("partner_intent")
    private Object partnerIntent;
    @JsonProperty("holidays")
    private Object holidays;
    @JsonProperty("score")
    private Float score;
    @JsonProperty("onground_rating")
    private Float ongroundRating;
    @JsonProperty("onground_verified")
    private Boolean ongroundVerified;
    @JsonProperty("tele_verified")
    private Boolean teleVerified;
    @JsonProperty("blacklisted")
    private Boolean blacklisted;
    @JsonProperty("available_jd")
    private Boolean availableJd;
    @JsonProperty("available_google")
    private Boolean availableGoogle;
    @JsonProperty("available_swiggy")
    private Boolean availableSwiggy;
    @JsonProperty("available_dunzo")
    private Boolean availableDunzo;
    @JsonProperty("time_slots")
    private Object timeSlots;
    @JsonProperty("wait_time")
    private Object waitTime;
    @JsonProperty("home_delivery")
    private Boolean homeDelivery;
    @JsonProperty("payment_methods")
    private String paymentMethods;
    @JsonProperty("commission")
    private Float commission;
    @JsonProperty("source")
    private Object source;
    @JsonProperty("city")
    private Integer city;
    @JsonProperty("business_hours")
    private List<BusinessHours> businessHoursList;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateLeadPojo() {
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public CreateLeadPojo setId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("duplicates")
    public List<Object> getDuplicates() {
        return duplicates;
    }

    @JsonProperty("duplicates")
    public CreateLeadPojo setDuplicates(List<Object> duplicates) {
        this.duplicates = duplicates;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public CreateLeadPojo setName(String name) {
        this.name = name;
        return this;
    }


    @JsonProperty("phone_numbers")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @JsonProperty("phone_numbers")
    public CreateLeadPojo setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }


    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public CreateLeadPojo setLat(String lat) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public CreateLeadPojo setLng(String lng) {
        this.lng = lng;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public CreateLeadPojo setAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("is_duplicate")
    public Boolean getIsDuplicate() {
        return isDuplicate;
    }

    @JsonProperty("is_duplicate")
    public CreateLeadPojo setIsDuplicate(Boolean isDuplicate) {
        this.isDuplicate = isDuplicate;
        return this;
    }

    @JsonProperty("parent_id")
    public Integer getParentId() {
        return parentId;
    }

    @JsonProperty("parent_id")
    public CreateLeadPojo setParentId(Integer parentId) {
        this.parentId = parentId;
        return this;
    }

    @JsonProperty("landmark")
    public Object getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public CreateLeadPojo setLandmark(Object landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("email")
    public Object getEmail() {
        return email;
    }

    @JsonProperty("email")
    public CreateLeadPojo setEmail(Object email) {
        this.email = email;
        return this;
    }

    @JsonProperty("website")
    public Object getWebsite() {
        return website;
    }

    @JsonProperty("website")
    public CreateLeadPojo setWebsite(Object website) {
        this.website = website;
        return this;
    }

    @JsonProperty("category")
    public Object getCategory() {
        return category;
    }

    @JsonProperty("category")
    public CreateLeadPojo setCategory(Object category) {
        this.category = category;
        return this;
    }

    @JsonProperty("subcategory")
    public Object getSubcategory() {
        return subcategory;
    }

    @JsonProperty("subcategory")
    public CreateLeadPojo setSubcategory(Object subcategory) {
        this.subcategory = subcategory;
        return this;
    }

    @JsonProperty("popular_category")
    public Object getPopularCategory() {
        return popularCategory;
    }

    @JsonProperty("popular_category")
    public CreateLeadPojo setPopularCategory(Object popularCategory) {
        this.popularCategory = popularCategory;
        return this;
    }

    @JsonProperty("popular_subcategory")
    public Object getPopularSubcategory() {
        return popularSubcategory;
    }

    @JsonProperty("popular_subcategory")
    public CreateLeadPojo setPopularSubcategory(Object popularSubcategory) {
        this.popularSubcategory = popularSubcategory;
        return this;
    }

    @JsonProperty("suggested_items")
    public Object getSuggestedItems() {
        return suggestedItems;
    }

    @JsonProperty("suggested_items")
    public CreateLeadPojo setSuggestedItems(Object suggestedItems) {
        this.suggestedItems = suggestedItems;
        return this;
    }

    @JsonProperty("list_category")
    public Object getListCategory() {
        return listCategory;
    }

    @JsonProperty("list_category")
    public CreateLeadPojo setListCategory(Object listCategory) {
        this.listCategory = listCategory;
        return this;
    }

    @JsonProperty("tags")
    public Object getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public CreateLeadPojo setTags(Object tags) {
        this.tags = tags;
        return this;
    }

    @JsonProperty("images")
    public Object getImages() {
        return images;
    }

    @JsonProperty("images")
    public CreateLeadPojo setImages(Object images) {
        this.images = images;
        return this;
    }


    @JsonProperty("chain")
    public Object getChain() {
        return chain;
    }

    @JsonProperty("chain")
    public CreateLeadPojo setChain(Object chain) {
        this.chain = chain;
        return this;
    }

    @JsonProperty("id_from_source")
    public Object getIdFromSource() {
        return idFromSource;
    }

    @JsonProperty("id_from_source")
    public CreateLeadPojo setIdFromSource(Object idFromSource) {
        this.idFromSource = idFromSource;
        return this;
    }

    @JsonProperty("source_link")
    public Object getSourceLink() {
        return sourceLink;
    }

    @JsonProperty("source_link")
    public CreateLeadPojo setSourceLink(Object sourceLink) {
        this.sourceLink = sourceLink;
        return this;
    }

    @JsonProperty("source_rating")
    public Float getSourceRating() {
        return sourceRating;
    }

    @JsonProperty("source_rating")
    public CreateLeadPojo setSourceRating(Float sourceRating) {
        this.sourceRating = sourceRating;
        return this;
    }

    @JsonProperty("source_votes")
    public Integer getSourceVotes() {
        return sourceVotes;
    }

    @JsonProperty("source_votes")
    public CreateLeadPojo setSourceVotes(Integer sourceVotes) {
        this.sourceVotes = sourceVotes;
        return this;
    }

    @JsonProperty("type")
    public Object getType() {
        return type;
    }

    @JsonProperty("type")
    public CreateLeadPojo setType(Object type) {
        this.type = type;
        return this;
    }

    @JsonProperty("partner_intent")
    public Object getPartnerIntent() {
        return partnerIntent;
    }

    @JsonProperty("partner_intent")
    public CreateLeadPojo setPartnerIntent(Object partnerIntent) {
        this.partnerIntent = partnerIntent;
        return this;
    }

    @JsonProperty("holidays")
    public Object getHolidays() {
        return holidays;
    }

    @JsonProperty("holidays")
    public CreateLeadPojo setHolidays(Object holidays) {
        this.holidays = holidays;
        return this;
    }

    @JsonProperty("score")
    public Float getScore() {
        return score;
    }

    @JsonProperty("score")
    public CreateLeadPojo setScore(Float score) {
        this.score = score;
        return this;
    }

    @JsonProperty("onground_rating")
    public Float getOngroundRating() {
        return ongroundRating;
    }

    @JsonProperty("onground_rating")
    public CreateLeadPojo setOngroundRating(Float ongroundRating) {
        this.ongroundRating = ongroundRating;
        return this;
    }

    @JsonProperty("onground_verified")
    public Boolean getOngroundVerified() {
        return ongroundVerified;
    }

    @JsonProperty("onground_verified")
    public CreateLeadPojo setOngroundVerified(Boolean ongroundVerified) {
        this.ongroundVerified = ongroundVerified;
        return this;
    }

    @JsonProperty("tele_verified")
    public Boolean getTeleVerified() {
        return teleVerified;
    }

    @JsonProperty("tele_verified")
    public CreateLeadPojo setTeleVerified(Boolean teleVerified) {
        this.teleVerified = teleVerified;
        return this;
    }

    @JsonProperty("blacklisted")
    public Boolean getBlacklisted() {
        return blacklisted;
    }

    @JsonProperty("blacklisted")
    public CreateLeadPojo setBlacklisted(Boolean blacklisted) {
        this.blacklisted = blacklisted;
        return this;
    }

    @JsonProperty("available_jd")
    public Boolean getAvailableJd() {
        return availableJd;
    }

    @JsonProperty("available_jd")
    public CreateLeadPojo setAvailableJd(Boolean availableJd) {
        this.availableJd = availableJd;
        return this;
    }

    @JsonProperty("available_google")
    public Boolean getAvailableGoogle() {
        return availableGoogle;
    }

    @JsonProperty("available_google")
    public CreateLeadPojo setAvailableGoogle(Boolean availableGoogle) {
        this.availableGoogle = availableGoogle;
        return this;
    }

    @JsonProperty("available_swiggy")
    public Boolean getAvailableSwiggy() {
        return availableSwiggy;
    }

    @JsonProperty("available_swiggy")
    public CreateLeadPojo setAvailableSwiggy(Boolean availableSwiggy) {
        this.availableSwiggy = availableSwiggy;
        return this;
    }

    @JsonProperty("available_dunzo")
    public Boolean getAvailableDunzo() {
        return availableDunzo;
    }

    @JsonProperty("available_dunzo")
    public CreateLeadPojo setAvailableDunzo(Boolean availableDunzo) {
        this.availableDunzo = availableDunzo;
        return this;
    }

    @JsonProperty("time_slots")
    public Object getTimeSlots() {
        return timeSlots;
    }

    @JsonProperty("time_slots")
    public CreateLeadPojo setTimeSlots(Object timeSlots) {
        this.timeSlots = timeSlots;
        return this;
    }

    @JsonProperty("wait_time")
    public Object getWaitTime() {
        return waitTime;
    }

    @JsonProperty("wait_time")
    public CreateLeadPojo setWaitTime(Object waitTime) {
        this.waitTime = waitTime;
        return this;
    }

    @JsonProperty("home_delivery")
    public Boolean getHomeDelivery() {
        return homeDelivery;
    }

    @JsonProperty("home_delivery")
    public CreateLeadPojo setHomeDelivery(Boolean homeDelivery) {
        this.homeDelivery = homeDelivery;
        return this;
    }

    @JsonProperty("payment_methods")
    public String getPaymentMethods() {
        return paymentMethods;
    }

    @JsonProperty("payment_methods")
    public CreateLeadPojo setPaymentMethods(String paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    @JsonProperty("commission")
    public Float getCommission() {
        return commission;
    }

    @JsonProperty("commission")
    public CreateLeadPojo setCommission(Float commission) {
        this.commission = commission;
        return this;
    }

    @JsonProperty("source")
    public Object getSource() {
        return source;
    }

    @JsonProperty("source")
    public CreateLeadPojo setSource(Object source) {
        this.source = source;
        return this;
    }

    @JsonProperty("city")
    public Integer getCity() {
        return city;
    }

    @JsonProperty("city")
    public CreateLeadPojo setCity(Integer city) {
        this.city = city;
        return this;
    }

    @JsonProperty("business_hours")
    public List<BusinessHours> getBusinessHoursList() {
        return businessHoursList;
    }

    @JsonProperty("business_hours")
    public CreateLeadPojo setBusinessHoursList(List<BusinessHours> businessHours) {
        this.businessHoursList = businessHours;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("duplicates", duplicates).append("name", name).append("phoneNumber", phoneNumber).append("lat", lat).append("lng", lng).append("address", address).append("isDuplicate", isDuplicate).append("parentId", parentId).append("landmark", landmark).append("email", email).append("website", website).append("category", category).append("subcategory", subcategory).append("popularCategory", popularCategory).append("popularSubcategory", popularSubcategory).append("suggestedItems", suggestedItems).append("listCategory", listCategory).append("tags", tags).append("images", images).append("chain", chain).append("idFromSource", idFromSource).append("sourceLink", sourceLink).append("sourceRating", sourceRating).append("sourceVotes", sourceVotes).append("type", type).append("partnerIntent", partnerIntent).append("holidays", holidays).append("score", score).append("ongroundRating", ongroundRating).append("ongroundVerified", ongroundVerified).append("teleVerified", teleVerified).append("blacklisted", blacklisted).append("availableJd", availableJd).append("availableGoogle", availableGoogle).append("availableSwiggy", availableSwiggy).append("availableDunzo", availableDunzo).append("timeSlots", timeSlots).append("waitTime", waitTime).append("homeDelivery", homeDelivery).append("paymentMethods", paymentMethods).append("commission", commission).append("source", source).append("city", city).toString();
    }

}
