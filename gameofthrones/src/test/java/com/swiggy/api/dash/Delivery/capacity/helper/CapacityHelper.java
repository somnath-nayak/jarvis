package com.swiggy.api.dash.Delivery.capacity.helper;

import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;

public class CapacityHelper {

    Initialize gameofthrones=new Initialize();
    JsonHelper jsonHelper;
    RedisHelper redisHelper=new RedisHelper();
    public CapacityHelper(){
        this.jsonHelper= new JsonHelper();
    }


    public Processor setDeZoneMap( String deId,String zoneId){
        Processor processor;
        try {
            GameOfThronesService gots = new GameOfThronesService("capacitycontroller", "dezonemap", gameofthrones);
            String[] payloadparams = {deId,zoneId};
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }
    public Processor setDedeAttendance(String deId,String action,String serviceLineId){
        Processor processor;
        try {
            GameOfThronesService gots = new GameOfThronesService("capacitycontroller", "deattendance", gameofthrones);
            String[] payloadparams = {deId,action,serviceLineId};
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
        }
        catch(Exception e){
            return null;
        }
        return processor;

    }
    public Processor setDeServiceLineMap(String deId,String eventType,String serviceLineId){
        Processor processor;
        try {
            GameOfThronesService gots = new GameOfThronesService("capacitycontroller", "deservicelinemap", gameofthrones);
            String[] payloadparams = {deId,serviceLineId,eventType};
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }
    public Processor setDeStatus(String deId,String status){
        Processor processor;
        try {
            GameOfThronesService gots = new GameOfThronesService("capacitycontroller", "destatus", gameofthrones);
            String[] payloadparams = {deId,status};
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
        }
        catch(Exception e){
            return null;
        }
        return processor;

    }

    public void getAllkeys(){
        redisHelper.getAllKeys("capacitycontrollerredis", 0);

    }







}
