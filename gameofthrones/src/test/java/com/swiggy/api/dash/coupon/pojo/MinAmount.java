package com.swiggy.api.dash.coupon.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cart"
})
public class MinAmount {

    @JsonProperty("cart")
    private Integer cart;

    /**
     * No args constructor for use in serialization
     *
     */
    public MinAmount() {
    }

    /**
     *
     * @param cart
     */
    public MinAmount(Integer cart) {
        super();
        this.cart = cart;
    }

    @JsonProperty("cart")
    public Integer getCart() {
        return cart;
    }

    @JsonProperty("cart")
    public MinAmount setCart(Integer cart) {
        this.cart = cart;
        return this;
    }

    public MinAmount withCart(Integer cart) {
        this.cart = cart;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("cart", cart).toString();
    }

}