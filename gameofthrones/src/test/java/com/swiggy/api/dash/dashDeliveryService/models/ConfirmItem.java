package com.swiggy.api.dash.dashDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.List;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "taskId",
        "items"
})

public class ConfirmItem {

    @JsonProperty("taskId")
    private int taskId;
    @JsonProperty("items")
    private List<ItemsToConfirm> items;

    public int gettaskId() {
        return taskId;
    }

    public void settaskId(int taskId) {
        this.taskId = taskId;
    }

    public List<ItemsToConfirm> getItems() {
        return items;
    }

    public void setItems(List<ItemsToConfirm> items) {
        this.items = items;
    }
}
