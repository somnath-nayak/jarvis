
package com.swiggy.api.dash.viewservice.models;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "categories",
    "subCategories",
    "tags",
    "cityId",
    "cloudinaryImageId",
    "createdBy",
    "description",
    "endDateTime",
    "filters",
    "id",
    "minStoreCount",
    "onboardingImageId",
    "onboardingMessages",
    "onboardingPresent",
    "onboardingTitle",
    "startDateTime",
    "storeIdentifier",
    "topCarouselWidgetName",
    "tradeDiscountInfo",
    "viewName",
    "viewTitle",
    "viewType",
    "visible",
    "storeIds"
})
public class Createviewpojo {

    @JsonProperty("categories")
    private List<Integer> categories = null;
    @JsonProperty("subCategories")
    private List<Integer> subCategories = null;
    @JsonProperty("tags")
    private List<Integer> tags = null;
    @JsonProperty("cityId")
    private Integer cityId;
    @JsonProperty("cloudinaryImageId")
    private String cloudinaryImageId="1";
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("description")
    private String description="hello";
    @JsonProperty("endDateTime")
    private String endDateTime="2018-09-07 12:00:00";
    @JsonProperty("filters")
    private List<Filter> filters = null;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("minStoreCount")
    private String minStoreCount;
    @JsonProperty("onboardingImageId")
    private String onboardingImageId="";
    @JsonProperty("onboardingMessages")
    private List<String> onboardingMessages = null;
    @JsonProperty("onboardingPresent")
    private Boolean onboardingPresent=true;
    @JsonProperty("onboardingTitle")
    private String onboardingTitle="hello";
    @JsonProperty("startDateTime")
    private String startDateTime="2018-09-09 12:00:00";
    @JsonProperty("storeIdentifier")
    private String storeIdentifier;
    @JsonProperty("topCarouselWidgetName")
    private String topCarouselWidgetName="topCarouselWidgetName";
    @JsonProperty("tradeDiscountInfo")
    private TradeDiscountInfo tradeDiscountInfo;
    @JsonProperty("viewName")
    private String viewName;
    @JsonProperty("viewTitle")
    private String viewTitle;
    @JsonProperty("viewType")
    private String viewType;
    @JsonProperty("visible")
    private Boolean visible;
    @JsonProperty("storeIds")
    private List<Integer> storeIds = null;

    @JsonProperty("categories")
    public List<Integer> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public void setCategories(List<Integer> categories) {
        this.categories = categories;
    }

    public Createviewpojo withCategories(List<Integer> categories) {
        this.categories = categories;
        return this;
    }

    @JsonProperty("subCategories")
    public List<Integer> getSubCategories() {
        return subCategories;
    }

    @JsonProperty("subCategories")
    public void setSubCategories(List<Integer> subCategories) {
        this.subCategories = subCategories;
    }

    public Createviewpojo withSubCategories(List<Integer> subCategories) {
        this.subCategories = subCategories;
        return this;
    }

    @JsonProperty("tags")
    public List<Integer> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public void setTags(List<Integer> tags) {
        this.tags = tags;
    }

    public Createviewpojo withTags(List<Integer> tags) {
        this.tags = tags;
        return this;
    }

    @JsonProperty("cityId")
    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Createviewpojo withCityId(Integer cityId) {
        this.cityId = cityId;
        return this;
    }

    @JsonProperty("cloudinaryImageId")
    public String getCloudinaryImageId() {
        return cloudinaryImageId;
    }

    @JsonProperty("cloudinaryImageId")
    public void setCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
    }

    public Createviewpojo withCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Createviewpojo withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public Createviewpojo withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("endDateTime")
    public String getEndDateTime() {
        return endDateTime;
    }

    @JsonProperty("endDateTime")
    public void setEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
    }

    public Createviewpojo withEndDateTime(String endDateTime) {
        this.endDateTime = endDateTime;
        return this;
    }

    @JsonProperty("filters")
    public List<Filter> getFilters() {
        return filters;
    }

    @JsonProperty("filters")
    public void setFilters(List<Filter> filters) {
        this.filters = filters;
    }

    public Createviewpojo withFilters(List<Filter> filters) {
        this.filters = filters;
        return this;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Createviewpojo withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("minStoreCount")
    public String getMinStoreCount() {
        return minStoreCount;
    }

    @JsonProperty("minStoreCount")
    public void setMinStoreCount(String minStoreCount) {
        this.minStoreCount = minStoreCount;
    }

    public Createviewpojo withMinStoreCount(String minStoreCount) {
        this.minStoreCount = minStoreCount;
        return this;
    }

    @JsonProperty("onboardingImageId")
    public String getOnboardingImageId() {
        return onboardingImageId;
    }

    @JsonProperty("onboardingImageId")
    public void setOnboardingImageId(String onboardingImageId) {
        this.onboardingImageId = onboardingImageId;
    }

    public Createviewpojo withOnboardingImageId(String onboardingImageId) {
        this.onboardingImageId = onboardingImageId;
        return this;
    }

    @JsonProperty("onboardingMessages")
    public List<String> getOnboardingMessages() {
        return onboardingMessages;
    }

    @JsonProperty("onboardingMessages")
    public void setOnboardingMessages(List<String> onboardingMessages) {
        this.onboardingMessages = onboardingMessages;
    }

    public Createviewpojo withOnboardingMessages(List<String> onboardingMessages) {
        this.onboardingMessages = onboardingMessages;
        return this;
    }

    @JsonProperty("onboardingPresent")
    public Boolean getOnboardingPresent() {
        return onboardingPresent;
    }

    @JsonProperty("onboardingPresent")
    public void setOnboardingPresent(Boolean onboardingPresent) {
        this.onboardingPresent = onboardingPresent;
    }

    public Createviewpojo withOnboardingPresent(Boolean onboardingPresent) {
        this.onboardingPresent = onboardingPresent;
        return this;
    }

    @JsonProperty("onboardingTitle")
    public String getOnboardingTitle() {
        return onboardingTitle;
    }

    @JsonProperty("onboardingTitle")
    public void setOnboardingTitle(String onboardingTitle) {
        this.onboardingTitle = onboardingTitle;
    }

    public Createviewpojo withOnboardingTitle(String onboardingTitle) {
        this.onboardingTitle = onboardingTitle;
        return this;
    }

    @JsonProperty("startDateTime")
    public String getStartDateTime() {
        return startDateTime;
    }

    @JsonProperty("startDateTime")
    public void setStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
    }

    public Createviewpojo withStartDateTime(String startDateTime) {
        this.startDateTime = startDateTime;
        return this;
    }

    @JsonProperty("storeIdentifier")
    public String getStoreIdentifier() {
        return storeIdentifier;
    }

    @JsonProperty("storeIdentifier")
    public void setStoreIdentifier(String storeIdentifier) {
        this.storeIdentifier = storeIdentifier;
    }

    public Createviewpojo withStoreIdentifier(String storeIdentifier) {
        this.storeIdentifier = storeIdentifier;
        return this;
    }

    @JsonProperty("topCarouselWidgetName")
    public String getTopCarouselWidgetName() {
        return topCarouselWidgetName;
    }

    @JsonProperty("topCarouselWidgetName")
    public void setTopCarouselWidgetName(String topCarouselWidgetName) {
        this.topCarouselWidgetName = topCarouselWidgetName;
    }

    public Createviewpojo withTopCarouselWidgetName(String topCarouselWidgetName) {
        this.topCarouselWidgetName = topCarouselWidgetName;
        return this;
    }

    @JsonProperty("tradeDiscountInfo")
    public TradeDiscountInfo getTradeDiscountInfo() {
        return tradeDiscountInfo;
    }

    @JsonProperty("tradeDiscountInfo")
    public void setTradeDiscountInfo(TradeDiscountInfo tradeDiscountInfo) {
        this.tradeDiscountInfo = tradeDiscountInfo;
    }

    public Createviewpojo withTradeDiscountInfo(TradeDiscountInfo tradeDiscountInfo) {
        this.tradeDiscountInfo = tradeDiscountInfo;
        return this;
    }

    @JsonProperty("viewName")
    public String getViewName() {
        return viewName;
    }

    @JsonProperty("viewName")
    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public Createviewpojo withViewName(String viewName) {
        this.viewName = viewName;
        return this;
    }

    @JsonProperty("viewTitle")
    public String getViewTitle() {
        return viewTitle;
    }

    @JsonProperty("viewTitle")
    public void setViewTitle(String viewTitle) {
        this.viewTitle = viewTitle;
    }

    public Createviewpojo withViewTitle(String viewTitle) {
        this.viewTitle = viewTitle;
        return this;
    }

    @JsonProperty("viewType")
    public String getViewType() {
        return viewType;
    }

    @JsonProperty("viewType")
    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public Createviewpojo withViewType(String viewType) {
        this.viewType = viewType;
        return this;
    }

    @JsonProperty("visible")
    public Boolean getVisible() {
        return visible;
    }

    @JsonProperty("visible")
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    @JsonProperty("storeIds")
    public List<Integer> getStoreIds() {
        return storeIds;
    }

    @JsonProperty("storeIds")
    public void setStoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
    }

    public Createviewpojo withstoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
        return this;
    }


    public Createviewpojo withVisible(Boolean visible) {
        this.visible = visible;
        return this;
    }

}
