package com.swiggy.api.dash.orderAuditService.dp;

import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.orderAuditService.constants.OAS_CONSTANTS;
import com.swiggy.api.dash.orderAuditService.pojos.OrderUpdatePojo;
import org.testng.annotations.DataProvider;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
public class OASDp {

    @DataProvider
    public Object[][] orderUpdate(){
        return new Object[][] {
                { 20 },
                { 15 }
        };
    }

    public OrderUpdatePojo giveMeRandomOrderUpdate(){
        OrderUpdatePojo order = new OrderUpdatePojo()
                .setOrderId("")
                .setOrderGroupId("")
                .setUpdaterType("")
                .setUpdateJson("")
                .setOrderVersion(0)
                .setUpdatedBy("")
                .setUpdateTime(10000)
                .setUpdaterType("");
        return order;
    }

    public List<OrderUpdatePojo> giveMeListOfOrderUpdateJson(int number) {
        List<OrderUpdatePojo> list =  new LinkedList<>();
        String orderId = UUID.randomUUID().toString().substring(0, 10);
        String orderGroupId = UUID.randomUUID().toString().substring(0, 10);
        for(int i = 0; i < number; i++){
            OrderUpdatePojo blankJson =  giveMeRandomOrderUpdate()
                    .setUpdateJson("Updating something")
                    .setOrderId(orderId)
                    .setOrderGroupId(orderGroupId)
                    .setUpdateType(AutomationHelper.randomSubArrayWithSeperator(OAS_CONSTANTS.STATUS_LIST, "", 1))
                    .setUpdaterType(AutomationHelper.randomSubArrayWithSeperator(OAS_CONSTANTS.UPDATER_TYPES, "", 1))
                    .setUpdatedBy(AutomationHelper.randomSubArrayWithSeperator(OAS_CONSTANTS.UPDATER_TYPES, "", 1))
                    .setOrderVersion(i)
                    .setUpdateTime(new Random().nextInt(1000000));
            list.add(blankJson);
        }

        return list;
    }

}
