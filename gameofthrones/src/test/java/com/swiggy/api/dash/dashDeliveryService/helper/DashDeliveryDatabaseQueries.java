package com.swiggy.api.dash.dashDeliveryService.helper;

public class DashDeliveryDatabaseQueries {

    public static String tripManagerDb = "tripmanagerdb";
    public  String getMetaDataFromJobId;

    public String getGetMetaDataFromJobId() {
        return getMetaDataFromJobId;
    }

    public void setGetMetaDataFromJobId(int jobId) {

        String getMetaDataFromJobId = "select task.id as taskId,task.meta_data from task join job_leg on job_leg.id = task.job_leg_id where job_leg.job_id = "+jobId;
        this.getMetaDataFromJobId = getMetaDataFromJobId;
    }
    public String getTaskInProgressAndMetaDataFromJobId(int jobId)
    {

        String taskInProgressAndMetaDataFromJobId = "select task.id as taskId,task.meta_data from task join job_leg on job_leg.id = task.job_leg_id where job_leg.job_id = "+jobId+" and in_progress = "+1;
        return taskInProgressAndMetaDataFromJobId;
    }
    public String getTaskIdInProgress(int jobId)
    {

        String taskInProgressAndMetaDataFromJobId = "select task.id as taskId from task join job_leg on job_leg.id = task.job_leg_id where job_leg.job_id = "+jobId+" and in_progress = "+1;
        return taskInProgressAndMetaDataFromJobId;
    }
}
