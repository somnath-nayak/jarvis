package com.swiggy.api.dash.dashDeliveryService.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "tripId",
        "deId",
        "source"
    })

    public class AssignTrip {
        @JsonProperty("tripId")
        private Integer tripId;
        @JsonProperty("deId")
        private Integer deId;
        @JsonProperty("source")
        private String source;

        @JsonProperty("tripId")
        public Integer getTripId() {
            return tripId;
        }

        @JsonProperty("tripId")
        public void setTripId(Integer tripId) {
            this.tripId = tripId;
        }

        public AssignTrip withTripId(Integer tripId) {
            this.tripId = tripId;
            return this;
        }

        @JsonProperty("deId")
        public Integer getDeId() {
            return deId;
        }

        @JsonProperty("deId")
        public void setDeId(Integer deId) {
            this.deId = deId;
        }

        public AssignTrip withDeId(Integer deId) {
            this.deId = deId;
            return this;
        }

        @JsonProperty("source")
        public String getSource() {
            return source;
        }

        @JsonProperty("source")
        public void setSource(String source) {
            this.source = source;
        }

        public AssignTrip withSource(String source) {
            this.source = source;
            return this;
        }


}
