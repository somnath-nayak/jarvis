package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "description",
        "fileId"
})
public class Attachment {

    @JsonProperty("description")
    private String description;
    @JsonProperty("fileId")
    private String fileId;

    private List<Attachment> listOfAttachments = new ArrayList<>();
    /**
     * No args constructor for use in serialization
     *
     */
    public Attachment() {
    }

    /**
     *
     * @param fileId
     * @param description
     */
    public Attachment(String description, String fileId) {
        super();
        this.description = description;
        this.fileId = fileId;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public Attachment setDescription(String description) {
        this.description = description;
        return this;
    }

    public Attachment withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("fileId")
    public String getFileId() {
        return fileId;
    }

    @JsonProperty("fileId")
    public Attachment setFileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public Attachment withFileId(String fileId) {
        this.fileId = fileId;
        return this;
    }

    public Attachment addItem(Attachment attachment){
        listOfAttachments.add(attachment);
        return this;
    }

    public List<Attachment> getListOfAttachments(){
        return listOfAttachments;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("description", description).append("fileId", fileId).toString();
    }

}