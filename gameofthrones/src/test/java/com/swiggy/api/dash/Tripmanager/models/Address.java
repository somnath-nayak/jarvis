
package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "primaryContact",
    "landmark",
    "flatNo",
    "addressLine",
    "isVerfied",
    "location"
})
public class Address {

    @JsonProperty("name")
    private String name;
    @JsonProperty("primaryContact")
    private String primaryContact;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("flatNo")
    private String flatNo;
    @JsonProperty("addressLine")
    private String addressLine;
    @JsonProperty("isVerfied")
    private Boolean isVerfied;
    @JsonProperty("location")
    private String location;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Address withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("primaryContact")
    public String getPrimaryContact() {
        return primaryContact;
    }

    @JsonProperty("primaryContact")
    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public Address withPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
        return this;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public Address withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("flatNo")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flatNo")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public Address withFlatNo(String flatNo) {
        this.flatNo = flatNo;
        return this;
    }

    @JsonProperty("addressLine")
    public String getAddressLine() {
        return addressLine;
    }

    @JsonProperty("addressLine")
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Address withAddressLine(String addressLine) {
        this.addressLine = addressLine;
        return this;
    }

    @JsonProperty("isVerfied")
    public Boolean getIsVerfied() {
        return isVerfied;
    }

    @JsonProperty("isVerfied")
    public void setIsVerfied(Boolean isVerfied) {
        this.isVerfied = isVerfied;
    }

    public Address withIsVerfied(Boolean isVerfied) {
        this.isVerfied = isVerfied;
        return this;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    public Address withLocation(String location) {
        this.location = location;
        return this;
    }

}
