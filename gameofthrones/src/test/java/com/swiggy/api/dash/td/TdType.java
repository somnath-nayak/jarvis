package com.swiggy.api.dash.td;

import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.helper.FunctionalP;
import com.swiggy.api.dash.td.pojo.RuleDiscountView;
import com.swiggy.api.dash.td.pojo.SlotView;
import com.swiggy.api.dash.td.pojo.TdView;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;

public class TdType extends FunctionalP<String, TdView> {


    // couponBuilder = new CouponBuilder();

    public TdView buildFlatTd(){
        return flatBuilder();
    }

    public TdView buildPercentTd(){
        return percentBuilder();
    }

    @Override
    public Function<String, TdView> Entity(String type) {
        return code ->{
            if(type.equalsIgnoreCase(CART.PERCENTAGE)){
                return this.buildPercentTd();
            }
            if(type.equalsIgnoreCase(CART.FLAT)){
                return this.buildFlatTd();
            }
            if(type.equalsIgnoreCase(CART.FREEDELIVERY)){
                return this.freeTdBuilder();
            }
            return null;
        };
    }

    public TdView Cores(String type, String storeId, String end) {

            if (type.equalsIgnoreCase(CART.PERCENTAGE)) {
                return this.percentBuilder(storeId, end);
            }
            if (type.equalsIgnoreCase(CART.FLAT)) {
                return this.buildFlatTd();
            }
            if (type.equalsIgnoreCase(CART.FREEDELIVERY)) {
                return this.freeTdBuilder();
            }
            return null;
    }


    public TdView freeTdBuilder(){
        List<Integer> store= new ArrayList<>();
        store.add(41288);
        RuleDiscountView ruleDiscount= new RuleDiscountView()
                .setType("FREE_DELIVERY").setDiscountLevel("Store").setMinCartAmount("0");
        List<SlotView> timeSlot= new ArrayList<>();
        timeSlot.add(new SlotView(800,2359,"ALL"));
        TdView td= new TdView().setNamespace("dash_free_del").setHeader("freedel").setDescription("freedel").setValidFrom("1548314617000").setValidTill("1582841162000").setCampaignType("FREE_DELIVERY").setOperationType("STORE").setStoreHit("100").setEnabled(true)
                .setSwiggyHit("0").setDiscountLevel("Store").setDiscountCap(20).setCreatedBy("jitender").setFreeDeliveryDistanceInKm(2.0).setStoreIds(store).setRuleDiscount(ruleDiscount).setSlots(timeSlot)
                .setCommissionOnFullBill(false).setTaxesOnDiscountedBill(false).setFirstOrderRestriction(false).setTimeSlotRestriction(false).setUserRestriction(false);
        return td;

    }

    public TdView percentBuilder(){
        List<Integer> store= new ArrayList<>();
        store.add(41288);
        RuleDiscountView ruleDiscount= new RuleDiscountView()
                .setType("Percentage").setDiscountLevel("Store").setMinCartAmount("10").setPercentDiscount("20");
        List<SlotView> timeSlot= new ArrayList<>();
        timeSlot.add(new SlotView(800,2359,"ALL"));
        TdView td= new TdView().setNamespace("dash_percent").setHeader("percent").setDescription("percent").setValidFrom("1548314617000").setValidTill("1582841162000").setCampaignType("Percentage").setOperationType("STORE").setStoreHit("0").setEnabled(true)
                .setSwiggyHit("100").setDiscountLevel("Store").setDiscountCap(20).setCreatedBy("jitender").setFreeDeliveryDistanceInKm(2.0).setStoreIds(store).setRuleDiscount(ruleDiscount).setSlots(timeSlot)
                .setCommissionOnFullBill(false).setTaxesOnDiscountedBill(false).setFirstOrderRestriction(false).setTimeSlotRestriction(false).setUserRestriction(false);
        return td;

    }

    public TdView flatBuilder(){
        List<Integer> store= new ArrayList<>();
        store.add(41288);
        RuleDiscountView ruleDiscount= new RuleDiscountView()
                .setType("Flat").setDiscountLevel("Store").setMinCartAmount("0").setFlatDiscountAmount("30");
        List<SlotView> timeSlot= new ArrayList<>();
        timeSlot.add(new SlotView(800,2359,"ALL"));
        TdView td= new TdView().setNamespace("dash_flat").setHeader("flat").setDescription("flat").setValidFrom("1548314617000").setValidTill("1582841162000").setCampaignType("Flat").setOperationType("STORE").setStoreHit("0").setEnabled(true)
                .setSwiggyHit("100").setDiscountLevel("Store").setDiscountCap(20).setCreatedBy("jitender").setFreeDeliveryDistanceInKm(2.0).setStoreIds(store).setRuleDiscount(ruleDiscount).setSlots(timeSlot)
                .setCommissionOnFullBill(false).setTaxesOnDiscountedBill(false).setFirstOrderRestriction(false).setTimeSlotRestriction(false).setUserRestriction(false);
        return td;
    }

    public TdView disableTd(String id){
        List<Integer> store= new ArrayList<>();
        store.add(41288);
        RuleDiscountView ruleDiscount= new RuleDiscountView()
                .setType("Percentage").setDiscountLevel("Store").setMinCartAmount("10").setPercentDiscount("20");
        List<SlotView> timeSlot= new ArrayList<>();
        timeSlot.add(new SlotView(800,2359,"ALL"));
        TdView td= new TdView().setId(id).setNamespace("dash_percent").setHeader("percent").setDescription("percent").setValidFrom("1548314617000").setValidTill("1582841162000").setCampaignType("Percentage").setOperationType("STORE").setStoreHit("0").setEnabled(false)
                .setSwiggyHit("100").setDiscountLevel("Store").setDiscountCap(20).setCreatedBy("jitender").setFreeDeliveryDistanceInKm(2.0).setStoreIds(store).setRuleDiscount(ruleDiscount).setSlots(timeSlot)
                .setCommissionOnFullBill(false).setTaxesOnDiscountedBill(false).setFirstOrderRestriction(false).setTimeSlotRestriction(false).setUserRestriction(false);
        return td;

    }

    public TdView percentBuilder(String storeId, String end){
        List<Integer> store= new ArrayList<>();
        store.add(Integer.parseInt(storeId));
        RuleDiscountView ruleDiscount= new RuleDiscountView()
                .setType("Percentage").setDiscountLevel("Store").setMinCartAmount("10").setPercentDiscount("20");
        List<SlotView> timeSlot= new ArrayList<>();
        timeSlot.add(new SlotView(800,2359,"ALL"));
        TdView td= new TdView().setNamespace("dash_percent").setHeader("percent").setDescription("percent").setValidFrom("1582841162000").setValidTill(end).setCampaignType("Percentage").setOperationType("STORE").setStoreHit("0").setEnabled(true)
                .setSwiggyHit("100").setDiscountLevel("Store").setDiscountCap(20).setCreatedBy("jitender").setFreeDeliveryDistanceInKm(2.0).setStoreIds(store).setRuleDiscount(ruleDiscount).setSlots(timeSlot)
                .setCommissionOnFullBill(false).setTaxesOnDiscountedBill(false).setFirstOrderRestriction(false).setTimeSlotRestriction(false).setUserRestriction(false);
        return td;

    }



}
