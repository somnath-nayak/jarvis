package com.swiggy.api.dash.Tripmanager.test;

import com.swiggy.api.daily.common.HttpCore;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerConstant;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryDataHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiHelper;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiWrapperHelper;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.Test;

import java.util.HashMap;

public class Tripmanagefoodordertest {

    JsonHelper helper = new JsonHelper();
    HttpCore core = new HttpCore(TripManagerConstant.TRIPMANAGER);
    JsonHelper jsonHelper=new JsonHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    TripManagerHelper tripManagerHelper=new TripManagerHelper();
    DeliveryServiceApiHelper deliveryServiceApiHelper = new DeliveryServiceApiHelper();
    DeliveryServiceApiWrapperHelper deliveryServiceApiWrapperHelper = new DeliveryServiceApiWrapperHelper();
    DeliveryDataHelper dataHelper = new DeliveryDataHelper();

    @Test(description = "Verfiy CAPRD for order pushed in swiggy_orders queue", enabled = true)
    public void createFoodOrderDeliveryByTripmanager() {
        //String de_id=deliveryServiceApiHelper.createDEandmakeDEActive(4);
        String de_id = "98125";
        HashMap<String, String> orderjson = new HashMap<>();
        String order_id = deliveryServiceApiHelper.createNewOrder(orderjson);
        dataHelper.updateDELocationInRedis(de_id);
        deliveryServiceApiHelper.assignDE(order_id, de_id);
    }

}
