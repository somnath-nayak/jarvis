
package com.swiggy.api.dash.viewservice.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "discountType",
    "discountValue"
})
public class TradeDiscountInfo {

    @JsonProperty("discountType")
    private String discountType;
    @JsonProperty("discountValue")
    private String discountValue;

    @JsonProperty("discountType")
    public String getDiscountType() {
        return discountType;
    }

    @JsonProperty("discountType")
    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public TradeDiscountInfo withDiscountType(String discountType) {
        this.discountType = discountType;
        return this;
    }

    @JsonProperty("discountValue")
    public String getDiscountValue() {
        return discountValue;
    }

    @JsonProperty("discountValue")
    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public TradeDiscountInfo withDiscountValue(String discountValue) {
        this.discountValue = discountValue;
        return this;
    }

}
