package com.swiggy.api.dash.Availability.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.swiggy.api.dash.kms.pojos.AreaBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "area_id",
        "city_id",
        "action"
})
public class KafkaMessage {
    @JsonProperty("area_id")
    private Integer area_id;

    @JsonProperty("city_id")
    private Integer city_id;


    @JsonProperty("action")
    private String action;


    public KafkaMessage() {
    }
    public KafkaMessage(Integer id, String action,String type) {
        super();
        if(type.equalsIgnoreCase("area")) {
            this.area_id = id;
            this.action = action;
        }
        if(type.equalsIgnoreCase("city")){
            this.city_id = id;
            this.action = action;
        }
    }


    @JsonProperty("area_id")
    public Integer getAreaId() {
        return area_id;
    }
    @JsonProperty("city_id")
    public Integer getCityId() {
        return city_id;
    }



    @JsonProperty("area_id")
    public void setAreaId(Integer id) {
        this.area_id = id;
    }
    @JsonProperty("city_id")
    public void setCityId(Integer id) {
        this.city_id = id;
    }
    @JsonProperty("action")
    public String getAction() {
        return action;
    }
    @JsonProperty("action")
    public void setAction(String action) {
        this.action = action;
    }
}


