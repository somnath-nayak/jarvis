package com.swiggy.api.dash.Availability.models;

import com.opencsv.bean.CsvBindByPosition;

public class Createstorecsv {
    @CsvBindByPosition(position = 0)
    private String storeName;

    @CsvBindByPosition(position = 1)
    private String areaName;

    @CsvBindByPosition(position = 2)
    private String cityName;

    @CsvBindByPosition(position = 3)
    private int active;

    @CsvBindByPosition(position = 4)
    private String category;

    @CsvBindByPosition(position = 5)
    private String tags;

    @CsvBindByPosition(position = 6)
    private int bussinesshours1;

    @CsvBindByPosition(position = 7)
    private int bussinesshours2;

    @CsvBindByPosition(position = 8)
    private int bussinesshours3;

    @CsvBindByPosition(position = 9)
    private int bussinesshours4;

    @CsvBindByPosition(position = 10)
    private int bussinesshours5;

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public int getBussinesshours1() {
        return bussinesshours1;
    }

    public void setBussinesshours1(int bussinesshours1) {
        this.bussinesshours1 = bussinesshours1;
    }

    public int getBussinesshours2() {
        return bussinesshours2;
    }

    public void setBussinesshours2(int bussinesshours2) {
        this.bussinesshours2 = bussinesshours2;
    }

    public int getBussinesshours3() {
        return bussinesshours3;
    }

    public void setBussinesshours3(int bussinesshours3) {
        this.bussinesshours3 = bussinesshours3;
    }

    public int getBussinesshours4() {
        return bussinesshours4;
    }

    public void setBussinesshours4(int bussinesshours4) {
        this.bussinesshours4 = bussinesshours4;
    }

    public int getBussinesshours5() {
        return bussinesshours5;
    }

    public void setBussinesshours5(int bussinesshours5) {
        this.bussinesshours5 = bussinesshours5;
    }

    public int getBussinesshours6() {
        return bussinesshours6;
    }

    public void setBussinesshours6(int bussinesshours6) {
        this.bussinesshours6 = bussinesshours6;
    }

    @CsvBindByPosition(position = 11)
    private int bussinesshours6;



}
