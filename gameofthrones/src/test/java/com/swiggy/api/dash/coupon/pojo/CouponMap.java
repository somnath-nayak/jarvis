package com.swiggy.api.dash.coupon.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "code",
        "couponId",
        "restaurantId"
})
public class CouponMap {

    @JsonProperty("code")
    private String code;
    @JsonProperty("couponId")
    private String couponId;
    @JsonProperty("restaurantId")
    private String restaurantId;

    /**
     * No args constructor for use in serialization
     *
     */
    public CouponMap() {
    }

    /**
     *
     * @param code
     * @param couponId
     * @param restaurantId
     */
    public CouponMap(String code, String couponId, String restaurantId) {
        super();
        this.code = code;
        this.couponId = couponId;
        this.restaurantId = restaurantId;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public CouponMap setCode(String code) {
        this.code = code;
        return this;
    }

    public CouponMap withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("couponId")
    public String getCouponId() {
        return couponId;
    }

    @JsonProperty("couponId")
    public CouponMap setCouponId(String couponId) {
        this.couponId = couponId;
        return this;
    }

    public CouponMap withCouponId(String couponId) {
        this.couponId = couponId;
        return this;
    }

    @JsonProperty("restaurantId")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public CouponMap setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    public CouponMap withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

}
