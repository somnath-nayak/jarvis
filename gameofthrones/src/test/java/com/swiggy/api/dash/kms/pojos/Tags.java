
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name"
})
public class Tags {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    List<Tags> listOfTags = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Tags() {
    }

    /**
     *
     * @param id
     * @param name
     */
    public Tags(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public Tags setId(Integer id) {
        this.id = id;
        return this;
    }

    public Tags withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public Tags setName(String name) {
        this.name = name;
        return this;
    }

    public Tags withName(String name) {
        this.name = name;
        return this;
    }

    public Tags addNewTag(Tags newTag){
        listOfTags.add(newTag);
        return this;
    }

    public List<Tags> returnSubListOfTags() {
        return listOfTags;
    }

    public List<Tags> returnListOfTags() {
        return listOfTags;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }

}
