package com.swiggy.api.dash.SuperAppDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "lat",
        "lng",
        "accuracy"
})


public class Location {

    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("accuracy")
    private String accuracy;

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }


    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    public Location withLat(String id) {
        this.lat = lat;
        return this;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }


    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    public Location withLng(String lng) {
        this.lng = lng;
        return this;
    }


    @JsonProperty("accuracy")
    public String getAccuracy() {
        return accuracy;
    }


    @JsonProperty("accuracy")
    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public Location withAccuracy(String accuracy) {
        this.accuracy = accuracy;
        return this;
    }

}
