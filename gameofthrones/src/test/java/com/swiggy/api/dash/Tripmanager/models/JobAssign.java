package com.swiggy.api.dash.Tripmanager.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "jobId",
        "deId",
    })

    public class JobAssign {
        @JsonProperty("jobId")
        private Integer jobId;
        @JsonProperty("deId")
        private Integer deId;

        @JsonProperty("jobId")
        public Integer getJobId() {
            return jobId;
        }

        @JsonProperty("jobId")
        public void setJobId(Integer jobId) {
            this.jobId = jobId;
        }

        public JobAssign withJobId(Integer jobId) {
            this.jobId = jobId;
            return this;
        }

        @JsonProperty("deId")
        public Integer getDeId() {
            return deId;
        }

        @JsonProperty("deId")
        public void setDeId(Integer deId) {
            this.deId = deId;
        }

        public JobAssign withDeId(Integer deId) {
            this.deId = deId;
            return this;
        }




}
