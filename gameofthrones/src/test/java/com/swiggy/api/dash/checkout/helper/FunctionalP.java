package com.swiggy.api.dash.checkout.helper;

import java.util.function.Function;

public abstract class FunctionalP<BO, E> {
    public FunctionalP() {
    }

    public abstract Function<BO, E> Entity(String cartType);
    //public abstract Function<BO, E> Entity(String cartType, String couponEntity);

    public E convertToEntity(BO bo, String c) {
        return this.Entity(c).apply(bo);
    }
}


