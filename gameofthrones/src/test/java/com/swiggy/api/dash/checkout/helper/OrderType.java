package com.swiggy.api.dash.checkout.helper;

import com.swiggy.api.dash.checkout.constants.Help;
import com.swiggy.api.dash.checkout.pojos.OrderCreation;

import java.util.function.Function;


public class OrderType extends FunctionalP<String, OrderCreation>{

    ViewBuilder viewBuilder= new ViewBuilder();

    @Override
    public Function<String, OrderCreation> Entity(String amount) {
        return order ->{
            if(isDouble(amount)){
               return this.structuredOrder(amount);
            }
            if(amount.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
                return this.unStructuredOrder();
            }
            if(amount.equalsIgnoreCase(Help.TYPE.CUSTOM.name())){
                return this.customOrder();
            }
            return null;
        };
    }

   /* @Override
    public Function<String, OrderCreation> Entity(String amount, String couponType) {
        return order ->{
            if(isDouble(amount)){
                return this.structuredOrder(amount);
            }
            if(amount.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
                return this.unStructuredOrder();
            }
            if(amount.equalsIgnoreCase(Help.TYPE.CUSTOM.name())){
                return this.customOrder();
            }
            return null;
        };
    }*/
   /*@Override
   public Function<String, OrderCreation> Core(String cartType, String couponType,String tdType){
       return help -> {
           OrderCreation cart = new OrderCreation();
           if(isDouble(amount)){
               return this.structuredOrder(amount);
           }
           if(amount.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
               return this.unStructuredOrder();
           }
           if(amount.equalsIgnoreCase(Help.TYPE.CUSTOM.name())){
               return this.customOrder();
           }
           return null;
       };
   }*/


    private OrderCreation structuredOrder(String amount){
        return viewBuilder.buildStructureOrder(amount);
    }

    private OrderCreation unStructuredOrder(){
        return viewBuilder.buildUnStructureOrder();
    }

    private OrderCreation customOrder(){
        return viewBuilder.buildCustomOrder();
    }

    boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


}
