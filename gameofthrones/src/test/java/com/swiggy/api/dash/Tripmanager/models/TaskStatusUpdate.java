package com.swiggy.api.dash.Tripmanager.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.SuperAppDeliveryService.models.Location;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "geoFenceBreached",
        "id",
        "status",
        "pay",
        "bill",
        "collect",
        "isGeoFenceBreached",
        "battery",
        "location",
        "deId"
})
public class TaskStatusUpdate {

    @JsonProperty("geoFenceBreached")
    private Boolean geoFenceBreached;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("pay")
    private Integer pay;
    @JsonProperty("bill")
    private Integer bill;
    @JsonProperty("collect")
    private Integer collect;
    @JsonProperty("isGeoFenceBreached")
    private Boolean isGeoFenceBreached;
    @JsonProperty("battery")
    private Integer battery;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("deId")
    private Integer deId;

    @JsonProperty("geoFenceBreached")
    public Boolean getGeoFenceBreached() {
        return geoFenceBreached;
    }

    @JsonProperty("geoFenceBreached")
    public void setGeoFenceBreached(Boolean geoFenceBreached) {
        this.geoFenceBreached = geoFenceBreached;
    }

    public TaskStatusUpdate withGeoFenceBreached(Boolean geoFenceBreached) {
        this.geoFenceBreached = geoFenceBreached;
        return this;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public TaskStatusUpdate withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public TaskStatusUpdate withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("pay")
    public Integer getPay() {
        return pay;
    }

    @JsonProperty("pay")
    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public TaskStatusUpdate withPay(Integer pay) {
        this.pay = pay;
        return this;
    }

    @JsonProperty("bill")
    public Integer getBill() {
        return bill;
    }

    @JsonProperty("bill")
    public void setBill(Integer bill) {
        this.bill = bill;
    }

    public TaskStatusUpdate withBill(Integer bill) {
        this.bill = bill;
        return this;
    }

    @JsonProperty("collect")
    public Integer getCollect() {
        return collect;
    }

    @JsonProperty("collect")
    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public TaskStatusUpdate withCollect(Integer collect) {
        this.collect = collect;
        return this;
    }

    @JsonProperty("isGeoFenceBreached")
    public Boolean getIsGeoFenceBreached() {
        return isGeoFenceBreached;
    }

    @JsonProperty("isGeoFenceBreached")
    public void setIsGeoFenceBreached(Boolean isGeoFenceBreached) {
        this.isGeoFenceBreached = isGeoFenceBreached;
    }

    public TaskStatusUpdate withIsGeoFenceBreached(Boolean isGeoFenceBreached) {
        this.isGeoFenceBreached = isGeoFenceBreached;
        return this;
    }

    @JsonProperty("battery")
    public Integer getBattery() {
        return battery;
    }

    @JsonProperty("battery")
    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public TaskStatusUpdate withBattery(Integer battery) {
        this.battery = battery;
        return this;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public TaskStatusUpdate withLocation(Location location) {
        this.location=location;
        return this;
    }

    @JsonProperty("deId")
    public Integer getDeId() {
        return deId;
    }

    @JsonProperty("deId")
    public void setDeId(Integer deId) {
        this.deId = deId;
    }

    public TaskStatusUpdate withDeId(Integer deId) {
        this.deId = deId;
        return this;
    }

}