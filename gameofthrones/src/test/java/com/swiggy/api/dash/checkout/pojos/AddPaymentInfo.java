package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddPaymentInfo {


    public AddPaymentInfo() {
    }

    public AddPaymentInfo(String orderContext, String transactionAmount) {
        this.orderContext = orderContext;
        this.transactionAmount = transactionAmount;
    }

    @JsonProperty("order_context")
    private String orderContext;

    @JsonProperty("transaction_amount")
    private String transactionAmount;



    public String getOrderContext() {
        return orderContext;
    }

    public AddPaymentInfo setOrderContext(String orderContext) {
        this.orderContext = orderContext;
        return this;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public AddPaymentInfo setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
        return this;
    }

}
