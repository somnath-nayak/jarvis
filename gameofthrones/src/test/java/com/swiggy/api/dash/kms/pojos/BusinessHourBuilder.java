
package com.swiggy.api.dash.kms.pojos;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonPropertyOrder({
    "day_id",
    "close_time",
    "open_24_hours",
    "open_time"
})
public class BusinessHourBuilder {
    @JsonProperty("day_id")
    private Integer dayId;

    @JsonProperty("close_time")
    private String closeTime;

    @JsonProperty("open_24_hours")
    private Integer open24Hours;

    @JsonProperty("open_time")
    private String openTime;

    @JsonIgnore
    List<BusinessHourBuilder> listOfBusinessHours = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public BusinessHourBuilder() {
    }

    /**
     * 
     * @param closeTime
     * @param dayId
     * @param open24Hours
     * @param openTime
     */
    public BusinessHourBuilder(Integer dayId, String closeTime, Integer open24Hours, String openTime) {
        super();
        this.dayId = dayId;
        this.closeTime = closeTime;
        this.open24Hours = open24Hours;
        this.openTime = openTime;
    }

    @JsonProperty("day_id")
    public Integer getDayId() {
        return dayId;
    }

    @JsonProperty("day_id")
    public BusinessHourBuilder setDayId(Integer dayId) {
        this.dayId = dayId;
        return this;
    }

    public BusinessHourBuilder withDayId(Integer dayId) {
        this.dayId = dayId;
        return this;
    }

    @JsonProperty("close_time")
    public String getCloseTime() {
        return closeTime;
    }

    @JsonProperty("close_time")
    public BusinessHourBuilder setCloseTime(String closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    public BusinessHourBuilder withCloseTime(String closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("open_24_hours")
    public Integer getOpen24Hours() {
        return open24Hours;
    }

    @JsonProperty("open_24_hours")
    public BusinessHourBuilder setOpen24Hours(Integer open24Hours) {
        this.open24Hours = open24Hours;
        return this;
    }

    public BusinessHourBuilder withOpen24Hours(Integer open24Hours) {
        this.open24Hours = open24Hours;
        return this;
    }

    @JsonProperty("open_time")
    public String getOpenTime() {
        return openTime;
    }

    @JsonProperty("open_time")
    public BusinessHourBuilder setOpenTime(String openTime) {
        this.openTime = openTime;
        return this;
    }

    public BusinessHourBuilder withOpenTime(String openTime) {
        this.openTime = openTime;
        return this;
    }

    public BusinessHourBuilder addBusinessHour(BusinessHourBuilder newBusinessHour){
        listOfBusinessHours.add(newBusinessHour);
        return this;
    }

    public List<BusinessHourBuilder> returnBusinessHourList(){
        return listOfBusinessHours;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("dayId", dayId).append("closeTime", closeTime).append("open24Hours", open24Hours).append("openTime", openTime).toString();
    }

}
