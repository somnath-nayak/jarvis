package com.swiggy.api.dash.Delivery.serviceability.models;

public class Left {
    private String name;
    private int value;
    private Left left;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    private String operator;

    public Right getRight() {
        return right;
    }

    public void setRight(Right right) {
        this.right = right;
    }

    private Right right;

    public Left getLeft() {
        return left;
    }

    public void setLeft(Left left) {
        this.left = left;
    }




    public  Left(String name,int value){
        this.name=name;
        this.value=value;
    }
    public  Left(){

    }
    public  Left(Left left,Right right,String operator){
        this.left=left;
        this.right=right;
        this.operator=operator;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
