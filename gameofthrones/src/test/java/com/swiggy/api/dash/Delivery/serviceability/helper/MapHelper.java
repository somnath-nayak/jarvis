package com.swiggy.api.dash.Delivery.serviceability.helper;

import com.swiggy.api.dash.Delivery.serviceability.models.Condition;
import com.swiggy.api.dash.Delivery.serviceability.models.LatLong;
import com.swiggy.api.dash.Delivery.serviceability.models.MapSearch;
import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class MapHelper {

    Initialize gameofthrones=new Initialize();
    JsonHelper jsonHelper;
    public MapHelper(){
        this.jsonHelper= new JsonHelper();
    }

    public Processor getMapsData(String cityId,String lat,String lon,String name,String tagValue,String from,String size,String maxdistance){
        Processor processor=null;
        try {
            GameOfThronesService gots = new GameOfThronesService("deliverymaps", "search", gameofthrones);
            String[] payloadparams = {cityId,lat,lon,name,tagValue,from,size,maxdistance };
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }
    public Processor getMapsData(MapSearch mapSearch){
        Processor processor=null;
        try {

            String payload=jsonHelper.getObjectToJSONallowNullValues(mapSearch);
            GameOfThronesService gots = new GameOfThronesService("deliverymaps", "search", gameofthrones);
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }
    public Processor getMapsData(int cityId,Double lat,Double lon,Condition c,int from,int size,int maxdistance){
        Processor processor=null;
        try {
            MapSearch mapSearch=new MapSearch(cityId,new LatLong(lat,lon),c,from,size,maxdistance);
            GameOfThronesService gots = new GameOfThronesService("deliverymaps", "search", gameofthrones);
            String payload=jsonHelper.getObjectToJSON(mapSearch);
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }


    /*public MapSearch getMapRequestObject(String cityId,LatLong latLong,String category,String offset,String size,String maxdistance){
        MapSearch mapSearch=new MapSearch(cityId,latLong,category,offset,size,maxdistance);
        return mapSearch;
    }*/

    public Processor fetchDataFromElasticSearch(int cityId,Double lat,Double lng,int offset,int size,String querypayload){
        Processor processor=null;
        try {
            String[] payloadparams = {querypayload,Double.toString(lat),Double.toString(lng),Integer.toString(offset),Integer.toString(size)};
            GameOfThronesService gots = new GameOfThronesService("deliverymapes", "esQuery", gameofthrones);
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payloadparams);
            System.out.println(processor.ResponseValidator.GetBodyAsText());
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return processor;

    }
    public String getAbsolutePathOfSchema(String apiname){
        return Mapconstant.MAP_SCHEMA_PATH + apiname + ".json";
    }




}
