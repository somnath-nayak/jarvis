package com.swiggy.api.dash.td.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.swiggy.api.dash.checkout.helper.CheckoutHelper;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "storeIdDistanceList"
})
@JsonSubTypes({
        @JsonSubTypes.Type(value=TdCart.StoreIdDistanceList.class, name="StoreIdDistanceList")})

public class TdCart {
    @JsonProperty("storeIdDistanceList")
    private List<TdCart.StoreIdDistanceList> storeIdDistanceList = null;

        /**
         * No args constructor for use in serialization
         *
         */
        public TdCart() {
        }

        /**
         *
         * @param storeIdDistanceList
         */
        public TdCart(List<TdCart.StoreIdDistanceList> storeIdDistanceList) {
            super();
            this.storeIdDistanceList = storeIdDistanceList;
        }
        @JsonProperty("storeIdDistanceList")
        public List<TdCart.StoreIdDistanceList> getStoreIdDistanceList() {
            return storeIdDistanceList;
        }

        @JsonProperty("storeIdDistanceList")
        public TdCart setStoreIdDistanceList(List<TdCart.StoreIdDistanceList> storeIdDistanceList) {
            this.storeIdDistanceList = storeIdDistanceList;
            return this;
        }

        public TdCart withStoreIdDistanceList(List<TdCart.StoreIdDistanceList> storeIdDistanceList) {
            this.storeIdDistanceList = storeIdDistanceList;
            return this;
        }

    public TdCart addItem(TdCart.StoreIdDistanceList list) {
        storeIdDistanceList.add(list);
        return this;
    }
    public List<TdCart.StoreIdDistanceList> returnListOfItems(){
        return storeIdDistanceList;
    }

        @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "storeId",
            "distance"
    })
    public static class StoreIdDistanceList extends TdCart {

        @JsonProperty("storeId")
        private String storeId;
        @JsonProperty("distance")
        private Double distance;

        private List<TdCart.StoreIdDistanceList> storeIdDistanceList = null;
        /**
         * No args constructor for use in serialization
         *
         */
        public StoreIdDistanceList() {
        }

        /**
         *
         * @param distance
         * @param storeId
         */
        public StoreIdDistanceList(String storeId, Double distance) {
            super();
            this.storeId = storeId;
            this.distance = distance;
        }

        @JsonProperty("storeId")
        public String getStoreId() {
            return storeId;
        }

        @JsonProperty("storeId")
        public StoreIdDistanceList setStoreId(String storeId) {
            this.storeId = storeId;
            return this;
        }

        public StoreIdDistanceList withStoreId(String storeId) {
            this.storeId = storeId;
            return this;
        }

        @JsonProperty("distance")
        public Double getDistance() {
            return distance;
        }

        @JsonProperty("distance")
        public StoreIdDistanceList setDistance(Double distance) {
            this.distance = distance;
            return this;
        }

        public StoreIdDistanceList withDistance(Double distance) {
            this.distance = distance;
            return this;
        }
            public TdCart.StoreIdDistanceList addStore(TdCart.StoreIdDistanceList store) {
                System.out.println(store);
                CheckoutHelper checkoutHelper = new CheckoutHelper();
                System.out.println(checkoutHelper.serialize(store));
                storeIdDistanceList.add(store);
                System.out.println(store);
                System.out.println(storeIdDistanceList);
                return this;
            }
            public List<TdCart.StoreIdDistanceList> returnStore(){
                return storeIdDistanceList;
            }


    }

}
