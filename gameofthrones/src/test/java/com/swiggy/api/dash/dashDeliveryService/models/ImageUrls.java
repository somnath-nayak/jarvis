package com.swiggy.api.dash.dashDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;

import java.util.ArrayList;

import static java.util.Arrays.asList;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "url"
})
public class ImageUrls {

    @JsonProperty("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static ArrayList<String> setDefaultArrayImageUrls()
    {
        ArrayList<String> images = new ArrayList<>(asList(DashDeliveryConstant.IMAGE_URL1,DashDeliveryConstant.IMAGE_UR2));

        return images;
    }
    public static ArrayList<ImageUrls> setDefaultImageUrls()
    {
        ImageUrls imageUrl = new ImageUrls();
        imageUrl.setUrl(DashDeliveryConstant.IMAGE_URL1);
        ArrayList<ImageUrls> images = new ArrayList<>(asList(imageUrl));
        return images;
    }

}
