package com.swiggy.api.dash.Tripmanager.helper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.swiggy.api.dash.Tripmanager.models.JoblegBo;
import com.swiggy.api.dash.Tripmanager.models.TaskBo;
import org.springframework.jdbc.core.RowMapper;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TaskRowMapper implements RowMapper {
    public TaskBo mapRow(ResultSet rs, int rowNum) throws SQLException {
        ObjectMapper mapper = new ObjectMapper();

        TaskBo taskBo=new TaskBo();
        taskBo.setId(rs.getLong("id"));
        taskBo.setBill(rs.getInt("bill"));
        try {
            taskBo.setMetaData(mapper.readTree(rs.getString("meta_data")));
        }
        catch (IOException e){

        }
        return taskBo;
    }
}
