package com.swiggy.api.dash.SuperAppDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "otp"
})
public class Otp {

    @JsonProperty("id")
    private long id;
    @JsonProperty("otp")
    private long otp;

    @JsonProperty("id")
    public long getId() {
        return this.id;
    }


    @JsonProperty("id")
    public void setId(long id) {
        this.id = id;
    }

    public Otp withId(long id) {
        this.id = id;
        return this;
    }


    @JsonProperty("otp")
    public long getOtp() {
        return this.otp;
    }


    @JsonProperty("otp")
    public void setOtp(long otp) {
        this.otp = otp;
    }

    public Otp withOtp(long otp) {
        this.otp = otp;
        return this;
    }






}
