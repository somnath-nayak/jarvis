package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonProperty;


import java.util.List;

public class NoStoreOrderRequest {

    @JsonProperty("address_id")
    private Integer addressId;

    @JsonProperty("payment_type")
    private String paymentType;

    @JsonProperty("items")
    private List<Item> items;

    public NoStoreOrderRequest(Integer addressId, String paymentType,  List<Item> items) {
        this.addressId = addressId;
        this.paymentType = paymentType;
        //this.paymentInfos = paymentInfos;
        this.items = items;
    }

    public NoStoreOrderRequest() {
    }

    public Integer getAddressId() {
        return addressId;
    }

    public NoStoreOrderRequest setAddressId(Integer addressId) {
        this.addressId = addressId;
        return this;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public NoStoreOrderRequest setPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }


    public List<Item> getItems() {
        return items;
    }

    public NoStoreOrderRequest setItems(List<Item> items) {
        this.items = items;
        return this;
    }
}
