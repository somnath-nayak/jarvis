package com.swiggy.api.dash.Tripmanager.test;

import com.swiggy.api.dash.Tripmanager.helper.JobLegStatus;
import com.swiggy.api.dash.Tripmanager.helper.JobType;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerConstant;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.dash.Tripmanager.models.MetaDataforJob;
import com.swiggy.api.dash.Tripmanager.models.Task;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.viewservice.helper.ErrorMessage;
import com.swiggy.api.dash.viewservice.helper.Viewserviceconstant;
import com.swiggy.api.dash.viewservice.helper.Viewservicehelper;
import com.swiggy.api.dash.viewservice.models.Createviewpojo;
import com.swiggy.api.dash.viewservice.test.Viewservicedp;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.gatling.http.request.builder.Http;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.Instant;
import java.util.ArrayList;

public class Tripmanagetest {

    JsonHelper helper = new JsonHelper();
    HttpCore core = new HttpCore(TripManagerConstant.TRIPMANAGER);
    JsonHelper jsonHelper=new JsonHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    TripManagerHelper tripManagerHelper=new TripManagerHelper();

    int job;



    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "createjobdata",priority = 0)
    public void verifycreateJob(JobType jobType, String ordeId, int sla, ArrayList<Task> task, MetaDataforJob metadata,int statuscode) throws Exception{
        String apiname="createjob";
        String payload=jsonHelper.getObjectToJSONallowNullValues(tripManagerHelper.createJob(jobType,ordeId,sla,task,metadata));
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        Assert.assertEquals(response.getResponseCode(),statuscode);
        if(statuscode==200){
            job=AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.id",Integer.class);
        }

    }
    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "assigntrip",priority = 0)
    public void verifyAssignUnAssignDe(int tripid,int deid,String msg) throws Exception{
        HttpResponse response=tripManagerHelper.assigntripToDe(tripid,deid);
        Assert.assertTrue(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.statusMessage",String.class).contains(msg));
        HttpResponse responseuunassign=tripManagerHelper.unAssignTripToDe(tripid,deid);
        Assert.assertTrue(AutomationHelper.getDataFromJson(responseuunassign.getResponseMsg(),"$.statusMessage",String.class).contains(msg));
        //TO-DO - database and redis validation
    }
    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "assignedTrip",priority = 0)
    public void getAssignedJob(int tripid,int jobLegId,int deId,String messageforassign,String statusMessage,int statusCode) throws Exception{
        HttpResponse response=tripManagerHelper.getAssignedJob(deId,0l );
        Assert.assertTrue(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.statusMessage",String.class).contains(statusMessage));
        Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(statusCode));
        if(messageforassign.equalsIgnoreCase("assign")){
            HttpResponse assigntripresponse=tripManagerHelper.assigntripToDe(tripid,deId);
            response=tripManagerHelper.getAssignedJob(deId,0l );
            Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.trips[0].status",String.class),"CREATED");
            Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(0));
            Long lastupdatetime=AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
            response=tripManagerHelper.getAssignedJob(deId,lastupdatetime);
            Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(5));
            tripManagerHelper.unAssignTripToDe(tripid,deId);
        }

    }


    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "joblegBulk",priority = 0)
    public void verigyGetJobLegBulk(int jobLegId,int numberoftask){
        HttpResponse bulkResponse=tripManagerHelper.getJobLegBulk(jobLegId);
        Assert.assertEquals(AutomationHelper.getDataFromJson(bulkResponse.getResponseMsg(),"$.data.length()",Integer.class),Integer.valueOf(numberoftask));
    }
    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "joblegBulk",priority = 0)
    public void verifyUpdateTaskStatuUpdate(int jobLegId,int numberoftask){
        HttpResponse bulkResponse=tripManagerHelper.getJobLegBulk(jobLegId);
        Assert.assertEquals(AutomationHelper.getDataFromJson(bulkResponse.getResponseMsg(),"$.data.length()",Integer.class),Integer.valueOf(numberoftask));
    }
















}
