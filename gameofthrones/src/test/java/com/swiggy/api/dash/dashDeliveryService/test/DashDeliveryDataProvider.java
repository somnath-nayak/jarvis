package com.swiggy.api.dash.dashDeliveryService.test;

import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.dash.checkout.helper.CheckoutHelper;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.dashDeliveryService.helper.*;
import com.swiggy.api.dash.dashDeliveryService.models.*;
import com.swiggy.api.dash.kms.helper.HttpResponse;
import cucumber.api.java.it.Data;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import static java.util.Arrays.asList;

public class DashDeliveryDataProvider {

    HashMap<String,Integer> nameIdMapping=new HashMap<>();
    DashDeliveryHelper dashDeliveryHelper =new DashDeliveryHelper();
    JsonHelper jsonHelper = new JsonHelper();
    CheckoutHelper checkoutHelper = new CheckoutHelper();

    @DataProvider(name = "createJobData", parallel = false)
    public Object[][] createjobdata() throws Exception{

       // CheckoutHelper checkoutHelper = new CheckoutHelper();
        String mobile = "7406734416";
        String password = "swiggy@123";
        HttpResponse httpResponse =  checkoutHelper.orderDetails(DashDeliveryConstant.STRUCTURED_CART_TYPE,null,DashDeliveryConstant.TD_TYPE,mobile,password);
        String response = httpResponse.toString().substring(httpResponse.toString().indexOf("{"));
        String orderId = AutomationHelper.getDataFromJson(response,"$.data.orders[0].order_jobs[0].order_job_id",String.class);
        CreateJob createJob = CreateJob.setDefaultCreateJobData(orderId);




       // CreateJob createJob = CreateJob.setDefaultCreateJobData(dashDeliveryHelper.generateOrderId());
        String createJobPayload = jsonHelper.getObjectToJSON(createJob);

        return new Object[][]{
                {createJobPayload}
        };

    }

    @DataProvider(name = "createTMSJob", parallel = false)
    public Object[][] updatejobdata() throws Exception{

        CreateJob createJob = CreateJob.setDefaultCreateJobData(dashDeliveryHelper.generateOrderId());
        createJob.setServiceLineId(ServiceLine.DASH.getVal());
        String updateJobPayload = jsonHelper.getObjectToJSON(createJob);

        return new Object[][]{
                {updateJobPayload}
        };


    }



}
