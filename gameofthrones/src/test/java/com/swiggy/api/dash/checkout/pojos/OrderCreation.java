package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "address_id",
        "payment_info",
        "payment_type"
})
public class OrderCreation {

    @JsonProperty("address_id")
    private Integer addressId;
    @JsonProperty("payment_info")
    private PaymentInfo paymentInfo;
    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("items")
    private List<Item> items;


    /**
     * No args constructor for use in serialization
     *
     */
    public OrderCreation() {
    }

    /**
     *
     * @param paymentInfo
     * @param paymentType
     * @param addressId
     */
    public OrderCreation(Integer addressId, PaymentInfo paymentInfo, String paymentType) {
        super();
        this.addressId = addressId;
        this.paymentInfo = paymentInfo;
        this.paymentType = paymentType;
    }
    public OrderCreation(Integer addressId, String paymentType) {
        super();
        this.addressId = addressId;
        this.paymentType = paymentType;
    }

    public List<Item> getItems() {
        return items;
    }

    public OrderCreation setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    /*
        no store order or custom order
         */
    public OrderCreation(List<Item> items,Integer addressId, String paymentType) {
        super();
        this.addressId = addressId;
        this.paymentType = paymentType;
        this.items = items;
    }

    @JsonProperty("address_id")
    public Integer getAddressId() {
        return addressId;
    }

    @JsonProperty("address_id")
    public OrderCreation setAddressId(Integer addressId) {
        this.addressId = addressId;
        return this;
    }

    public OrderCreation withAddressId(Integer addressId) {
        this.addressId = addressId;
        return this;
    }

    @JsonProperty("payment_info")
    public PaymentInfo getPaymentInfo() {
        return paymentInfo;
    }

    @JsonProperty("payment_info")
    public OrderCreation setPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
        return this;
    }

    public OrderCreation withPaymentInfo(PaymentInfo paymentInfo) {
        this.paymentInfo = paymentInfo;
        return this;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public OrderCreation setPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public OrderCreation withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("address_id", addressId).append("payment_info", paymentInfo).append("payment_type", paymentType).toString();
    }

}