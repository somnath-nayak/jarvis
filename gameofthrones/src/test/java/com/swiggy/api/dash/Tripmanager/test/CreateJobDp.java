package com.swiggy.api.dash.Tripmanager.test;

import com.swiggy.api.dash.Tripmanager.helper.JobType;
import com.swiggy.api.dash.Tripmanager.helper.TaskType;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerConstant;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.dash.Tripmanager.models.Address;
import com.swiggy.api.dash.Tripmanager.models.Task;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpResponse;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.HashMap;

public class CreateJobDp {

    HashMap<String,Integer> nameIdMapping=new HashMap<>();
    TripManagerHelper tripManagerHelper=new TripManagerHelper();
    JsonHelper jsonHelper = new JsonHelper();

    @DataProvider(name = "createjobdata", parallel = false)
    public Object[][] createjobdata() throws Exception{
        Address defaltRestaurantAddress=tripManagerHelper.createAddressObject("Meghna Biryani","12312312","near Jyothi nivas","","addressline",true,"12.7,77.1");
        return new Object[][]{
                {JobType.FOOD,"21212",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null,200},
                {JobType.FOOD,null,40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null,400},
                {null,"21212",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null,400},
                {JobType.FOOD,"21212",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,tripManagerHelper.createAddressObject("Meghna Biryani",null,"Near ibc","A311","addresslinke",true,"12.7,77.1"),0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null,400},

        };

    }
    @DataProvider(name = "assigntrip", parallel = false)
    public Object[][] assigntrip() throws Exception{
        Address defaltRestaurantAddress=tripManagerHelper.createAddressObject("Meghna Biryani","12312312","near Jyothi nivas","","addressline",true,"12.7,77.1");
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        HttpResponse response2=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int tripId1=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        int tripId2=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response2.getResponseMsg(),"$.data.id",Integer.class));
        return new Object[][]{
                {tripId1, TripManagerConstant.TEST_DE_ID,"success"},
                {tripId2,112312,"DE id not present"},
                {123131,TripManagerConstant.TEST_DE_ID,"Invalid trip id"}


        };

    }
    @DataProvider(name = "assignedTrip", parallel = false)
    public Object[][] assignedTrip() throws Exception{
        Address defaltRestaurantAddress=tripManagerHelper.createAddressObject("Meghna Biryani","12312312","near Jyothi nivas","","addressline",true,"12.7,77.1");
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        HttpResponse response2=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int tripId1=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        int joblegid1=AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        return new Object[][]{
                {tripId1,joblegid1,TripManagerConstant.TEST_DE_ID,"notassign","success",3},
                {tripId1,joblegid1,TripManagerConstant.TEST_DE_ID_1,"assign","success",3},
                {tripId1,joblegid1,12331312,"notassign","Invalid DeId",0}



        };

    }
    @DataProvider(name = "joblegBulk", parallel = false)
    public Object[][] joblegBulk() throws Exception{
        Address defaltRestaurantAddress=tripManagerHelper.createAddressObject("Meghna Biryani","12312312","near Jyothi nivas","","addressline",true,"12.7,77.1");
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int joblegid1=AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        return new Object[][]{
                {joblegid1,1},
                {1221312,0},



        };

    }
    @DataProvider(name = "taskstatusupdate", parallel = false)
    public Object[][] taskstatusupdate() throws Exception{
        Address defaltRestaurantAddress=tripManagerHelper.createAddressObject("Meghna Biryani","12312312","near Jyothi nivas","","addressline",true,"12.7,77.1");
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",40,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int joblegid1=AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        return new Object[][]{
                {joblegid1,1},
                {1221312,0},



        };

    }
    @DataProvider(name = "whattoedit", parallel = false)
    public Object[][] editJobData() throws Exception{

        return new Object[][]{
                {"Quantity"},
                {"addNewItem"},
                {"changePickupAddress"},
                {"AFTERARRIVEDONPICKUP"},
                {"CHANGEDELIVERYADDRESS_AFTERPICKUP"}



        };

    }




}
