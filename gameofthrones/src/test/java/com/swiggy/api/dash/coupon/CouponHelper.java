package com.swiggy.api.dash.coupon;

import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.helper.FunctionalP;
import com.swiggy.api.dash.coupon.pojo.Coupon;
import java.util.function.Function;

public class CouponHelper extends FunctionalP<String, Coupon> {

    CouponBuilder couponBuilder = new CouponBuilder();

    public Coupon buildFlatCoupon(){
        return couponBuilder.flatCouponBuilder();
    }

    public Coupon buildPercentCoupon(){
        return couponBuilder.percentCouponBuilder();
    }

    @Override
    public Function<String, Coupon> Entity(String type) {
        return code ->{
            if(type.equalsIgnoreCase(CART.FLAT)){
                return this.buildFlatCoupon();
            }
            if(type.equalsIgnoreCase(CART.PERCENTAGE)){
                return this.buildPercentCoupon();
            }
            if(type.equalsIgnoreCase(CART.FREEDELIVERY)){
                return this.buildPercentCoupon();
            }
            return null;
        };
    }


    public Coupon Mapping(String cType, int enable, int amount){
            if(cType.equalsIgnoreCase(CART.PERCENTAGE)){
                return couponBuilder.percentCouponBuilderUn(enable, amount);
            }
            if(cType.equalsIgnoreCase(CART.FLAT)){
            return couponBuilder.flatCouponBuilderUn(enable, amount);
            }
            if(cType.equalsIgnoreCase(CART.FREEDELIVERY)){
                return this.buildPercentCoupon();
            }
            return null;
    }







//cart type, coupon type, mapping,
  /* public Coupon apply(String type, String cType, String third, Coupon coupon) {
       return code ->{
           if(cType.equalsIgnoreCase(CART.FLAT)){
               return this.buildFlatCoupon();
           }
           if(cType.equalsIgnoreCase(CART.PERCENTAGE)){
               return this.buildPercentCoupon();
           }
           if(cType.equalsIgnoreCase(CART.FREEDELIVERY)){
               return this.buildPercentCoupon();
           }
           return null;
       };

    }*/



}
