
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squareup.moshi.Json;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.util.Arrays.asList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "id",
    "name",
    "price",
    "quantity",
    "description",
    "imageUrls",
    "variants",
    "isRemoved",
})
public class Item {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("imageUrls")
    private List<String> imageUrls;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("description")
    private String description;
    @JsonProperty("price")
    private double price;
    @JsonProperty("isRemoved")
    private boolean isRemoved;
    private boolean isAvailable;
    private List<ItemAlternative> alternatives;



    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Item withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("imageUrls")
    public List<String> getimageUrls() {
        return imageUrls;
    }

    @JsonProperty("imageUrls")
    public void setimageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Item withimageUrls(List<String> imageUrls) {
        this.imageUrls = imageUrls;
        return this;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public Item withVariants(List<Variant> variants) {
        this.variants = variants;
        return this;
    }

    @JsonProperty("price")
    public double getPrice() {
        return price;
    }
    @JsonProperty("price")
    public void setPrice(double price) {
        this.price = price;
    }


    public boolean getIsAvailable() {
        return isAvailable;
    }

    public void setIsAvailable(boolean isAvailable) {
        this.isAvailable = isAvailable;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        this.isRemoved = removed;
    }

    public List<ItemAlternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<ItemAlternative> alternatives) {
        this.alternatives = alternatives;
    }

    public static List<Item> createDefaultItem()
   {

       Item item = new Item();
       item.setimageUrls(ImageUrls.setDefaultArrayImageUrls());
       item.setName(DashDeliveryConstant.ITEM);
       item.setQuantity(DashDeliveryConstant.QTY);
       item.setVariants(Variant.setDefaultVariant());
       item.setId(DashDeliveryConstant.ITEM_ID);
       ArrayList<Item> items = new ArrayList<>(asList(item));
       return items;

   }


}
