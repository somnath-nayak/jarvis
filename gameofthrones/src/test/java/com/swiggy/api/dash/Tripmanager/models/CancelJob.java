package com.swiggy.api.dash.Tripmanager.models;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "jobId",
        "cancellationReason",
        "source"
    })

    public class CancelJob {
        @JsonProperty("jobId")
        private Integer jobId;
        @JsonProperty("source")
        private String source;
        @JsonProperty("cancellationReason")
        private String cancellationReason;

        @JsonProperty("jobId")
        public Integer getJobId() {
            return jobId;
        }

        @JsonProperty("jobId")
        public void setJobId(Integer jobId) {
            this.jobId = jobId;
        }

        public CancelJob withJobId(Integer jobId) {
            this.jobId = jobId;
            return this;
        }

        @JsonProperty("source")
        public String getSource() {
        return source;
    }

        @JsonProperty("source")
        public void setSource(String source) {
        this.source = source;
    }

        public CancelJob withSource(String source) {
            this.source = source;
                return this;
        }
        @JsonProperty("cancellationReason")
        public String getCancellationReason() {
        return cancellationReason;
    }

        @JsonProperty("source")
        public void setCancellationReason(String source) {
        this.cancellationReason = cancellationReason;
    }

         public CancelJob withCancellationReason(String source) {
           this.cancellationReason = cancellationReason;
           return this;
    }




}
