package com.swiggy.api.dash.Availability.models;

public class AvailableCategory {

    private int cityId;
    private int areaId;
    private int[] categories;

    public AvailableCategory(int cityId,int areaId,int[] categories){
        this.cityId=cityId;
        this.areaId=areaId;
        this.categories=categories;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int[] getCategories() {
        return categories;
    }

    public void setCategories(int[] categories) {
        this.categories = categories;
    }


}
