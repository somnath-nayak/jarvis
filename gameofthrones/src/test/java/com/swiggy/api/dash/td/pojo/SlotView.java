package com.swiggy.api.dash.td.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "openTime",
        "closeTime",
        "day"
})
public class SlotView {

    @JsonProperty("openTime")
    private Integer openTime;
    @JsonProperty("closeTime")
    private Integer closeTime;
    @JsonProperty("day")
    private String day;

    /**
     * No args constructor for use in serialization
     *
     */
    public SlotView() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     */
    public SlotView(Integer openTime, Integer closeTime, String day) {
        super();
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.day = day;
    }

    @JsonProperty("openTime")
    public Integer getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public SlotView withOpenTime(Integer openTime) {
        this.openTime = openTime;
        return this;
    }

    @JsonProperty("closeTime")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public SlotView withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public SlotView withDay(String day) {
        this.day = day;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("openTime", openTime).append("closeTime", closeTime).append("day", day).toString();
    }

}