package com.swiggy.api.dash.checkout.dp;

import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.pojos.*;
import com.swiggy.api.dash.userService.Login;
import cucumber.api.java.it.Data;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckoutDp {
    //CheckoutHelper checkoutHelper = new CheckoutHelper();

    @DataProvider
    public Object[][] getCart(){
        return new Object[][] {
                {CART.UNSTRUCTURE},
                {CART.STRUCTURE},
        };
    }

    @DataProvider
    public Object[][] itemInfo(){
        return new Object[][] {
                {CART.STRUCTURE},
        };
    }

    @DataProvider
    public Object[][] getCartTd(){
        return new Object[][] {
                {CART.STRUCTURE, CART.PERCENTAGE },
                {CART.STRUCTURE, CART.FLAT},
                {CART.STRUCTURE, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getUnstrcuturedTd(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT},
                //{CART.UNSTRUCTURE, CART.freeDelivery},
        };
    }


    @DataProvider
    public Object[][] getCartCoupon(){
        return new Object[][] {
                {CART.STRUCTURE, CART.PERCENTAGE},
                {CART.STRUCTURE, CART.FLAT},
        };
    }

    @DataProvider
    public Object[][] getTdCouponStruct(){
        return new Object[][] {
                {CART.STRUCTURE, CART.PERCENTAGE, CART.PERCENTAGE},
                {CART.STRUCTURE, CART.FLAT, CART.PERCENTAGE},
                {CART.STRUCTURE, CART.FLAT, CART.FLAT},
                {CART.STRUCTURE, CART.PERCENTAGE, CART.FLAT},
                {CART.STRUCTURE, null, CART.PERCENTAGE},
                {CART.STRUCTURE, null, CART.FLAT},
                {CART.STRUCTURE, CART.FLAT, null},
                {CART.STRUCTURE, CART.PERCENTAGE, null},
                {CART.STRUCTURE, CART.PERCENTAGE, CART.FREEDELIVERY},
                {CART.STRUCTURE, CART.FLAT, CART.FREEDELIVERY},
                {CART.STRUCTURE, null, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getTdCouponStructure(){
        return new Object[][] {
                {CART.STRUCTURE, CART.PERCENTAGE,1,0, CART.PERCENTAGE},
                {CART.STRUCTURE, CART.FLAT,1, 0,CART.PERCENTAGE},
                {CART.STRUCTURE, CART.FLAT,1,0, CART.FLAT},
                {CART.STRUCTURE, CART.PERCENTAGE,1, 0,CART.FLAT},
                {CART.STRUCTURE, CART.FLAT,1,0, null},
                {CART.STRUCTURE, CART.PERCENTAGE,1,0, null},
                {CART.STRUCTURE, CART.PERCENTAGE, 1,0,CART.FREEDELIVERY},
                {CART.STRUCTURE, CART.FLAT,1,0, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getTdCouponUnStructure(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,0, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT,1,0, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT,1,0, CART.FLAT},
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,0, CART.FLAT},
                {CART.UNSTRUCTURE, CART.FLAT,1,0, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,0, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, 1,0,CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, CART.FLAT,1,0, CART.FREEDELIVERY},
        };
    }

    //minCartAmount in coupon call
    @DataProvider
    public Object[][] getTdCouponUnStructureCartAmount(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,100, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT,1,100, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT,1,100, CART.FLAT},
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,100, CART.FLAT},
                {CART.UNSTRUCTURE, CART.FLAT,1,100, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE,1,100, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, 1,100,CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, CART.FLAT,1,100, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getTdCouponStructureCartAmount(){
        return new Object[][] {
                { CART.PERCENTAGE,1,100, CART.PERCENTAGE},
                { CART.FLAT,1,100, CART.PERCENTAGE},
                { CART.FLAT,1,100, CART.FLAT},
                { CART.PERCENTAGE,1,100, CART.FLAT},
                { CART.FLAT,1,100, null},
                { CART.PERCENTAGE,1,100, null},
                { CART.PERCENTAGE, 1,100,CART.FREEDELIVERY},
                { CART.FLAT,1,100, CART.FREEDELIVERY},
        };
    }



    @DataProvider
    public Object[][] getTdCouponUnStruct(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT, CART.FLAT},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.FLAT},
                {CART.UNSTRUCTURE, CART.FLAT, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, CART.FLAT, CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, null, CART.FREEDELIVERY},
        };
    }


    @DataProvider
    public Object[][] getTdCouponUnStructItems(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, CART.FLAT, CART.FLAT},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.FLAT},
                {CART.UNSTRUCTURE, CART.FLAT, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, null},
                {CART.UNSTRUCTURE, CART.PERCENTAGE, CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, CART.FLAT, CART.FREEDELIVERY},
                {CART.UNSTRUCTURE, null, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getTdCouponNull(){
        return new Object[][]{
                {CART.UNSTRUCTURE, null, CART.PERCENTAGE},
                {CART.UNSTRUCTURE, null, CART.FLAT},
                {CART.UNSTRUCTURE, null, CART.FREEDELIVERY},
        };
    }

    @DataProvider
    public Object[][] getCustom(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
                {CART.UNSTRUCTURE, CART.CUSTOM},
        };
    }

    @DataProvider
    public Object[][] getCustomCoupon(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.CUSTOM,CART.PERCENTAGE} ,
                {CART.UNSTRUCTURE, CART.CUSTOM, CART.FLAT}
        };
    }

    @DataProvider
    public Object[][] getCustomCouponTD(){
        return new Object[][] {
                {CART.UNSTRUCTURE, CART.CUSTOM,CART.PERCENTAGE, CART.PERCENTAGE} ,
                {CART.UNSTRUCTURE, CART.CUSTOM, CART.FLAT, CART.PERCENTAGE}
        };
    }


    public Login getLogin(){
        return new Login().setMobile(CART.mobile).setPassword(CART.pwd);
    }


public CartCreation getStructuredCart(String coupon){
        List<Item> item = new Item()
                .addItem(new Item("Item","41288", 1,"structure","5",""))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

    List<Attachment> attachment = new Attachment()
            .addItem(new Attachment("Description for the file","Field Id"))
            .getListOfAttachments();

        CartCreation cart= new CartCreation()
                .setAddressId("37667544")
                .setCouponCode(coupon)
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
    return cart;
}

    public CartCreation getUnStructuredCart(){
        List<Object> images = new ArrayList<>();

        List<Item> item = new Item()
                .addItem(new Item("Milk","41288", 1,"unstructure","", images,"comment", "dash"))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file","Field Id"))
                .getListOfAttachments();

        CartCreation cart= new CartCreation()
                .setAddressId("37667531")
                .setCouponCode("usage_count31")
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }

    public EditItemPatch getItemPatch(){
         List<Object> images = new ArrayList<>();
      //  String type, String serviceLine, Integer storeId, Object itemTag, Integer quantity, String itemId, String name, Object variant, List<String> imageIds, Object comment
        List<Item> item = new Item()
                //.addItem(new Item("unstructure","dash", 41288,null,1,"hscjv", "milk", 50, images, "We"))
                .addItem(new Item("Beer","41288", 1,"unstructure","750ml",images,"comment","dash"))
                .returnListOfItems();
        Map<String, Object> ctx = new HashMap<>();

        return new EditItemPatch().setItems(item)
                .setCtx(ctx)
                .setVerifyItemAvailability(true)
                .setVerifyItemPricing(true)
                .setStoreAvailability(true)
                .setVerifyServiceability(true);
    }

    public ForceUpdate forceUpdate(Double amount){
        return new ForceUpdate().setType("TotalBill").setAmount(amount);

    }

    public AddPaymentInfo addPaymentInfo(String amount){
        return new AddPaymentInfo().setOrderContext("ORDER_JOB").setTransactionAmount(amount);
    }

    public NoStoreOrderRequest noStoreOrder() {
        List<Object> images = new ArrayList<>();
        List<Item> item = new Item()
                .addItem(new Item("Beer", "", 1, "unstructure", "750ml", images, "comment", "dash"))
                .returnListOfItems();
        return new NoStoreOrderRequest().setItems(item).setPaymentType("POST_PAYMENT").setAddressId(37667531);
    }







}
