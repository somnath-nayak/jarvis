package com.swiggy.api.dash.Delivery.serviceability.models;

public class MapSearch {
    private int cityId;
    private int from;
    private LatLong latLong;
    private int size;
    private Condition condition;
    private int maxRadiusInMeters;

    public int getMaxRadiusInMeters() {
        return maxRadiusInMeters;
    }

    public void setMaxRadiusInMeters(int maxRadiusInMeters) {
        this.maxRadiusInMeters = maxRadiusInMeters;
    }


    public MapSearch(int cityId,LatLong latLong,Condition condition,int from,int size,int maxRadiusInMeters){
        this.cityId=cityId;
        this.latLong=latLong;
        this.condition=condition;
        this.from=from;
        this.size=size;
        this.maxRadiusInMeters=maxRadiusInMeters;
    }

    public Condition getCondition ()
    { return condition; }
    public void setCategory (int category)
    {
        this.condition = condition;
    }
    public int getCityId ()
    {
        return cityId;
    }
    public void setCityId (int cityId)
    {
        this.cityId = cityId;
    }
    public int getFrom ()
    {
        return from;
    }
    public void setFrom (int from)
    {
        this.from = from;
    }
    public LatLong getLatLong ()
    {
        return latLong;
    }
    public void setLatLong (LatLong latLong)
    {
        this.latLong = latLong;
    }
    public int getSize ()
    {
        return size;
    }
    public void setSize (int size)
    {
        this.size = size;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [category = "+condition+", cityId = "+cityId+", from = "+from+", latLong = "+latLong+", size = "+size+"]";
    }
}
