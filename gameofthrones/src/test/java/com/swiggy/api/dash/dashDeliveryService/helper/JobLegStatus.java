package com.swiggy.api.dash.dashDeliveryService.helper;

public enum JobLegStatus {
    CREATED,
    ASSIGNED,
    CONFIRMED,
    REJECTED,
    COMPLETED,
    AUTO_REJECTED,

}
