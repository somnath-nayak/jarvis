package com.swiggy.api.dash.orderAuditService.tests;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.orderAuditService.constants.OAS_CONSTANTS;
import com.swiggy.api.dash.orderAuditService.dp.OASDp;
import com.swiggy.api.dash.orderAuditService.helpers.OASCommon;
import com.swiggy.api.dash.orderAuditService.helpers.OASDBManager;
import com.swiggy.api.dash.orderAuditService.pojos.OrderUpdatePojo;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.KafkaHelper;
import net.minidev.json.JSONArray;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderAuditTests {
    JsonHelper helper = new JsonHelper();
    HttpCore core = new HttpCore(OAS_CONSTANTS.SERVICE_NAME);
    OASDp dp = new OASDp();
    KafkaHelper kafka = new KafkaHelper();
    OASDBManager db = new OASDBManager();
    OASCommon oas = new OASCommon();

    @Test(description = "Update Order in Kafka and verify in DB", dataProviderClass = OASDp.class, dataProvider = "orderUpdate")
    public void updateOrderAndCheckStatusInDB(int numberOfUpdates){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(numberOfUpdates);
        String orderId = list.get(0).getOrderId();
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");

                /** Fetch details from DB **/
                Map<String, Object> dbDetails = db.getOrderUpdateDetailsUnique(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                oas.verifyRequestWithDB(order, dbDetails);
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }
    }

    @Test(description = "get Logs by order id")
    public void getLogsByOrderId(){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(10);
        String orderId = list.get(0).getOrderId();
        HashMap<String, Integer> updateTypesAndCount = new HashMap<>();
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                updateTypesAndCount.put(order.getUpdateType(), updateTypesAndCount.getOrDefault(order.getUpdateType(), 0) + 1);
                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }

        for(Map.Entry<String, Integer> entry : updateTypesAndCount.entrySet()) {
            HttpResponse resp = core.invoker(OAS_CONSTANTS.LOGS_BY_ORDER_ID, core.getBasicHeaders(), new String[] { orderId });
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response code is not 200 OK");
            JSONArray data = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.data", JSONArray.class);
            Assert.assertEquals(Integer.valueOf(data.size()), Integer.valueOf(db.getOrderUpdateCount(orderId)), "Count is not matching for Order ID="+orderId );
        }
    }

    @Test(description = "get Logs by order id")
    public void getLogsByOrderIdAndUpdateType(){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(10);
        String orderId = list.get(0).getOrderId();
        HashMap<String, Integer> updateTypesAndCount = new HashMap<>();
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                updateTypesAndCount.put(order.getUpdateType(), updateTypesAndCount.getOrDefault(order.getUpdateType(), 0) + 1);

                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }

        for(Map.Entry<String, Integer> entry : updateTypesAndCount.entrySet()) {
            HttpResponse resp = core.invoker(OAS_CONSTANTS.LOGS_BY_ORDER_ID_UPDATETYPE, core.getBasicHeaders(), new String[] { orderId, entry.getKey()});
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response code is not 200 OK");
            JSONArray data = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.data", JSONArray.class);
            Assert.assertEquals(Integer.valueOf(data.size()), Integer.valueOf(entry.getValue()), "Count is not matching for Order ID="+ orderId +" and updateType="+entry.getValue());
        }
    }

    @Test(description = "get Logs by order group id")
    public void getLogsByOrderGroupId(){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(10);
        String orderGroupId = list.get(0).getOrderGroupId();
        HashMap<String, Integer> updateTypesAndCount = new HashMap<>();
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                updateTypesAndCount.put(order.getUpdateType(), updateTypesAndCount.getOrDefault(order.getUpdateType(), 0) + 1);
                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }

        for(Map.Entry<String, Integer> entry : updateTypesAndCount.entrySet()) {
            HttpResponse resp = core.invoker(OAS_CONSTANTS.LOGS_BY_GROUPORDER_ID, core.getBasicHeaders(), new String[] { orderGroupId });
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response code is not 200 OK");
            JSONArray data = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.data", JSONArray.class);
            Assert.assertEquals(Integer.valueOf(data.size()), Integer.valueOf(db.getOrderUpdateCountG(orderGroupId)), "Count is not matching for Order ID="+orderGroupId );
        }
    }

    @Test(description = "get Logs by order id")
    public void getLogsByOrderGroupIdAndUpdateType(){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(10);
        String orderGroupId = list.get(0).getOrderGroupId();
        HashMap<String, Integer> updateTypesAndCount = new HashMap<>();
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                updateTypesAndCount.put(order.getUpdateType(), updateTypesAndCount.getOrDefault(order.getUpdateType(), 0) + 1);

                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }

        for(Map.Entry<String, Integer> entry : updateTypesAndCount.entrySet()) {
            HttpResponse resp = core.invoker(OAS_CONSTANTS.LOGS_BY_GROUPORDER_UPDATETYPE, core.getBasicHeaders(), new String[] { orderGroupId, entry.getKey()});
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response code is not 200 OK");
            JSONArray data = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.data", JSONArray.class);
            Assert.assertEquals(Integer.valueOf(data.size()), Integer.valueOf(entry.getValue()), "Count is not matching for Order Group ID="+ orderGroupId +" and updateType="+entry.getValue());
        }
    }

    @Test(description = "get activity logs")
    public void getActivityLogs(){
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(1);
        for (OrderUpdatePojo order : list) {
            String orderUpdate = null;
            try {
                orderUpdate = helper.getObjectToJSON(order);
                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);

                /** Checking entries in DB **/
                boolean dbCount = db.isCountAsExpected(order.getOrderId(), order.getOrderGroupId(), order.getOrderVersion(), order.getUpdateType());
                Assert.assertTrue(dbCount, "Mismatch in No. of order updates vs No. of records in DB");
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }

            HttpResponse resp = core.invoker(OAS_CONSTANTS.ACTIVITY, core.getBasicHeaders(), new String[] { order.getUpdatedBy(), order.getUpdaterType()});
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response code is not 200 OK");
            JSONArray data = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.data", JSONArray.class);
            Assert.assertEquals(Integer.valueOf(data.size()), Integer.valueOf(db.getOrderUpdateCountAgent(order.getUpdatedBy(), order.getUpdaterType())), "Count is not matching for updated By");
        }
    }
}