
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name"
})
public class Source {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    private List<Source> listOfSources = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public Source() {
    }

    /**
     * 
     * @param id
     * @param name
     */
    public Source(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public Source setId(Integer id) {
        this.id = id;
        return this;
    }

    public Source withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public Source setName(String name) {
        this.name = name;
        return this;
    }

    public Source withName(String name) {
        this.name = name;
        return this;
    }

    public Source addSource(Source newSource) {
        listOfSources.add(newSource);
        return this;
    }

    public List<Source> returnListOfSources(){
        return listOfSources;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }

}
