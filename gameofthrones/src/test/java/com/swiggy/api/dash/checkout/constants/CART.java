package com.swiggy.api.dash.checkout.constants;

public class CART {
    public static String HEADER_CONTENT_TYPE = "Content-Type";
    public static String HEADER_ACCEPT = "Accept";
    public static String HEADER_AUTH = "Authorization";
    public static String TID= "tid";
    public static String TOKEN ="token";
    public static String VERSIONCODE = "versionCode";
    public static String USERAGENT = "userAgent";
    public static String USERAG="User-Agent";
    public static String CODE= "235";
    public static String ANDROID = "ANDROID";
    public static String KEY = "39d0f3f5-622e-4c7f-a714-af76247f31da";
    public static String TOKENKEY = "e7f3c526-db2b-4728-9763-0af27ffa8adf14524eef-982e-41f2-86f6-a5161cc07055";
    public static String PAYMENTVERSIONCODE = "version-code";
    public static Integer status0 = 0;
    public static String STATUS="0";
    public static String PLACED = "PLACED";
    public static String CONFIRMED = "CONFIRMED";
    public static String SECRETKEY = "secret_key";
    public static String SECRETVALUE="0bbd1ff2-dce6-446b-84ef-c027a1d43373";
    public static String PREPAYMENT= "PRE_PAYMENT";
    public static String POSTPAYMENT = "POST_PAYMENT";
    public static String ORDERJOB ="ORDER_JOB";
    public static String CASH= "CASH";
    public static String UNSTRUCTURE = "unstructure";
    public static String STRUCTURE = "structure";
    public static String CUSTOM = "custom";
    public static String FREEDELIVERY = "FreeDelivery";
    public static String PERCENTAGE ="Percentage";
    public static String FLAT ="Flat";
    public static String PERCENT="PERCENT";
    public static String structure ="structure";
    public static String mobile = "7406734416";
    public static String pwd = "swiggy@123";
    public static String SID="sid";
    public static String SIDVAL="32";
    public static String SERVICELINE ="service-line";
    public static String DASH = "dash";
    public static String CATALOG = "dash_catalog";
    public static String ADDRESSNO131 ="131";
    public static String STATUS120="120";
    public static String COUPONERROR="Coupon will be applicable if order value is satisfied";


}
