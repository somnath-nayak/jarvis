
package com.swiggy.api.dash.viewservice.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "tagId"
})
public class Filter {

    @JsonProperty("name")
    private String name;
    @JsonProperty("tagId")
    private Integer tagId;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Filter withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("tagId")
    public Integer getTagId() {
        return tagId;
    }

    @JsonProperty("tagId")
    public void setTagId(Integer tagId) {
        this.tagId = tagId;
    }

    public Filter withTagId(Integer tagId) {
        this.tagId = tagId;
        return this;
    }

}
