package com.swiggy.api.dash.checkout.constants;

import com.swiggy.api.dash.checkout.pojos.CartCreation;

public class Help {
    public static String Structure = "structure";
    public static String UnStructure = "unstructure";
    public static String Custom = "custom";
    public static String FLAT= "FLAT";
    public static CartCreation cartCreation;

    public enum CALCULATION_STRATEGY {
        Coupon("Coupon"),
        TradeDiscount("TradeDiscount"),
        STROEDISCOUNT("StoreDiscount"),
        PERCENT("PERCENT"),
        FLAT("FLAT"),
        FREE_DELIVERY("FREE_DELIVERY");

        String value;

        CALCULATION_STRATEGY(String value) {
                this.value = value;
        }
    }

    public enum TYPE{
        STRUCTURE("structure"),
        UNSTRUCTURE("unstructure"),
        CUSTOM("CUSTOM");


        String value;
        TYPE(String value){ this.value = value;}

    }
}
