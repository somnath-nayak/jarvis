package com.swiggy.api.dash.Delivery.serviceability.models;

public class Right {
    private String name;
    private int value;
    private Condition condition;
    private Left left;
    private Right right;
    private String operator;

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }


    public Right getRight() {
        return right;
    }

    public void setRight(Right right) {
        this.right = right;
    }


    public Left getLeft() {
        return left;
    }

    public void setLeft(Left left) {
        this.left = left;
    }
    public  Right(String name,int value){
        this.name=name;
        this.value=value;
    }
    public  Right(){

    }
    public  Right(Left left,Right right,String operator){
        this.left=left;
        this.right=right;
        this.operator=operator;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }


}
