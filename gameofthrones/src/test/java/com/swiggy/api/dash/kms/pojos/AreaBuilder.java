package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "enabled"
})
public class AreaBuilder {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("enabled")
    private Integer enabled;

    /**
     * No args constructor for use in serialization
     *
     */
    public AreaBuilder() {
    }

    /**
     *
     * @param id
     * @param enabled
     * @param name
     */
    public AreaBuilder(Integer id, String name, Integer enabled) {
        super();
        this.id = id;
        this.name = name;
        this.enabled = enabled;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public AreaBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public AreaBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public AreaBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public AreaBuilder withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("enabled")
    public Integer getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public AreaBuilder setEnabled(Integer enabled) {
        this.enabled = enabled;
        return this;
    }

    public AreaBuilder withEnabled(Integer enabled) {
        this.enabled = enabled;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("enabled", enabled).toString();
    }

}
