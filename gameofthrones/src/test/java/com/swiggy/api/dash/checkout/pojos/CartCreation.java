package com.swiggy.api.dash.checkout.pojos;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "location",
        "couponCode",
        "addressId",
        "attachments",
        "items"
})
public class CartCreation {

    @JsonProperty("location")
    private Location location;
    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("addressId")
    private String addressId;
    @JsonProperty("attachments")
    private List<Attachment> attachments = null;
    @JsonProperty("items")
    private List<Item> items = null;


    /**
     * No args constructor for use in serialization
     *
     */
    public CartCreation() {
    }

    /**
     *
     * @param items
     * @param location
     * @param couponCode
     * @param attachments
     * @param addressId
     */
    public CartCreation(Location location, String couponCode, String addressId, List<Attachment> attachments, List<Item> items) {
        super();
        this.location = location;
        this.couponCode = couponCode;
        this.addressId = addressId;
        this.attachments = attachments;
        this.items = items;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public CartCreation setLocation(Location location) {
        this.location = location;
        return this;
    }

    public CartCreation withLocation(Location location) {
        this.location = location;
        return this;
    }

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public CartCreation setCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    public CartCreation withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("addressId")
    public String getAddressId() {
        return addressId;
    }

    @JsonProperty("addressId")
    public CartCreation setAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }

    public CartCreation withAddressId(String addressId) {
        this.addressId = addressId;
        return this;
    }

    @JsonProperty("attachments")
    public List<Attachment> getAttachments() {
        return attachments;
    }

    @JsonProperty("attachments")
    public CartCreation setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
        return this;
    }

    public CartCreation withAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
        return this;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public CartCreation setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public CartCreation withItems(List<Item> items) {
        this.items = items;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("location", location).append("couponCode", couponCode).append("addressId", addressId).append("attachments", attachments).append("items", items).toString();
    }

}