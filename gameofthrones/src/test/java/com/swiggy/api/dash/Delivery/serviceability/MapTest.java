package com.swiggy.api.dash.Delivery.serviceability;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.swiggy.api.dash.Delivery.serviceability.helper.MapHelper;
import com.swiggy.api.dash.Delivery.serviceability.models.Condition;
import com.swiggy.api.dash.Delivery.serviceability.models.LatLong;
import com.swiggy.api.dash.Delivery.serviceability.models.MapSearch;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import com.swiggy.api.dash.Delivery.serviceability.MapDp;

public class MapTest extends MapDp {

    MapHelper mapHelper=new MapHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();

    @Test(dataProvider = "searchData", description = "Verify map search data",priority = 1)
    public void verifyMapSearch(String cityId,String lat,String lon,String name,String value,String from,String size,int statuscode,int httpstatuscode,String maxdistance){
        SoftAssert softAssert =new SoftAssert();
        Processor processor =mapHelper.getMapsData(cityId,lat,lon,name,value,from,size,maxdistance);
        System.out.println(processor);
        String response=processor.ResponseValidator.GetBodyAsText();
        softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(),httpstatuscode);
        if(processor.ResponseValidator.GetResponseCode()== HttpStatus.SC_OK){
            softAssert.assertEquals(statuscode,Integer.parseInt(JsonPath.read(response,"$.statusCode").toString()));
            softAssert.assertNotNull(JsonPath.read(response,"$.requestId"));
            softAssert.assertEquals("Success",JsonPath.read(response,"$.statusMsg").toString());
        }
        softAssert.assertAll();
    }
    @Test(dataProvider = "searchDatawithnullvalue", description = "Verify map search for null category",priority = 2)
    public void verifyMapSearchForValueNull(int cityId,double lat,double lon,String name,int value,int from,int size,int statuscode,int httpstatuscode,int maxdistance){
        SoftAssert softAssert =new SoftAssert();
        MapSearch mapSearch=new MapSearch(cityId,new LatLong(lat,lon),new Condition(name,value),from,size,maxdistance);
        Processor processor =mapHelper.getMapsData(mapSearch);
        String response=processor.ResponseValidator.GetBodyAsText();
        System.out.println(statuscode);
        softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(),httpstatuscode);
        softAssert.assertAll();
    }

    @Test(dataProvider = "searchDataFroSchemaValidation", description = "Schema validation",priority = 3)
    public void verifyMapSearchSchemaValidation(String cityId,String lat,String lon,String name,String value,String from,String size,int statuscode,int httpstatuscode,String maxdistance){
        SoftAssert softAssert =new SoftAssert();
        Processor processor =mapHelper.getMapsData(cityId,lat,lon,name,value,from,size,maxdistance);
        String response=processor.ResponseValidator.GetBodyAsText();
        System.out.println(statuscode);
        softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(),httpstatuscode);
        softAssert.assertTrue( schema.validateSchema(mapHelper.getAbsolutePathOfSchema("mapsearch"),response ), "Schema validation failed");
        softAssert.assertAll();
    }
    @Test(dataProvider = "searchDatacompareWithESData", description = "Verify map search data with various combination of category & subcategory",priority = 4,enabled = true)
    public void verifyMapSearchWithExpression(int cityId,double lat,double lon,Condition c,int from,int size,int statuscode,int httpstatuscode,int maxdistance,Processor elasticSearch){

        SoftAssert softAssert =new SoftAssert();
        Processor processor =mapHelper.getMapsData(cityId,lat,lon,c,from,size,maxdistance);
        String response=processor.ResponseValidator.GetBodyAsText();
        System.out.println(statuscode);
        softAssert.assertEquals(processor.ResponseValidator.GetResponseCode(),httpstatuscode);
        Assert.assertEquals(JsonPath.read(response,"$.body.totalHits").toString(),JsonPath.read(elasticSearch.ResponseValidator.GetBodyAsText(),"$.hits.total").toString());
        ReadContext ctx = JsonPath.parse(response);
        int value = ctx.read("$.body.stores.length()", Integer.class);
        for(int i=0;i<value;i++){
            Assert.assertEquals(JsonPath.read(response,"$.body.stores["+i+"].name").toString(),JsonPath.read(elasticSearch.ResponseValidator.GetBodyAsText(),"$.hits.hits["+i+"]._source.name").toString());
            Assert.assertEquals(JsonPath.read(response,"$.body.stores["+i+"].id").toString(),JsonPath.read(elasticSearch.ResponseValidator.GetBodyAsText(),"$.hits.hits["+i+"]._source.id").toString());
            Assert.assertEquals(JsonPath.read(response,"$.body.stores["+i+"].latLong.lat").toString(),JsonPath.read(elasticSearch.ResponseValidator.GetBodyAsText(),"$.hits.hits["+i+"]._source.latLong.lat").toString());
            Assert.assertEquals(JsonPath.read(response,"$.body.stores["+i+"].latLong.lon").toString(),JsonPath.read(elasticSearch.ResponseValidator.GetBodyAsText(),"$.hits.hits["+i+"]._source.latLong.lon").toString());
        }
        softAssert.assertAll();
    }


}
