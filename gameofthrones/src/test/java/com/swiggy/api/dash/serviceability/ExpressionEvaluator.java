package com.swiggy.api.dash.serviceability;

import com.fathzer.soft.javaluator.AbstractEvaluator;
import com.fathzer.soft.javaluator.BracketPair;
import com.fathzer.soft.javaluator.Operator;
import com.fathzer.soft.javaluator.Parameters;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExpressionEvaluator extends AbstractEvaluator<String> {
    final static Operator AND = new Operator("&&", 2, Operator.Associativity.LEFT, 2);
    final static Operator OR = new Operator("||", 2, Operator.Associativity.LEFT, 1);
    private static final Parameters PARAMETERS;
    public static int counter = 0;

    static {
        PARAMETERS = new Parameters();
        PARAMETERS.add(AND);
        PARAMETERS.add(OR);
        PARAMETERS.addExpressionBracket(BracketPair.PARENTHESES);
    }

    public ExpressionEvaluator() {
        super(PARAMETERS);
    }

    @Override
    protected String toValue(String literal, Object evaluationContext) {
        return literal;
    }

    @Override
    protected String evaluate(Operator operator, Iterator<String> operands, Object evaluationContext) {
        List<String> tree = (List<String>) evaluationContext;
        String o1 = operands.next();
        String o2 = operands.next();
        String result;
        if (operator == OR || operator == AND) {
            result = String.valueOf(++counter);
            System.out.println(o1);
            System.out.println(o2);
        }else {
            throw new IllegalArgumentException();
        }
        String eval = "("+o1+" "+operator.getSymbol()+" "+o2+")="+result;
        tree.add(eval);
        return eval;
    }

    public static void main(String[] args) {
        ExpressionEvaluator evaluator = new ExpressionEvaluator();
        doIt(evaluator, "A:TAG1_TAG2 && ( B:_TAG3 || ( C && D ) )");
    }

    private static void doIt(ExpressionEvaluator evaluator, String expression) {
        List<String> sequence = new ArrayList<String>();
        evaluator.evaluate(expression, sequence);
        System.out.println ("Evaluation sequence for :"+expression);
        for (String string : sequence) {
            System.out.println (string);
        }
        System.out.println ();
    }
}