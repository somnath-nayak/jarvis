package com.swiggy.api.dash.dashDeliveryService.helper;

public enum ServiceLine {
    FOOD,
    DASH,
    DASHCOMPLEX;
    public int getVal()
    {
        return ordinal() + 1;
    }


}
