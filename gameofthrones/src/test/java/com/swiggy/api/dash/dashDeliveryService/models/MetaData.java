
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squareup.moshi.Json;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "billTolerance",
    "payTolerance",
    "collectTolerance",
    "deliveryCartConfirmationStatus",
    "itemConfirmationCallbackUrl",
    "cartConfirmationCallbackUrl",
    "attachment",
    "items"
})
public class MetaData {

    @JsonProperty("billTolerance")
    private Integer billTolerance;
    @JsonProperty("payTolerance")
    private Integer payTolerance;
    @JsonProperty("collectTolerance")
    private Integer collectTolerance;
    @JsonProperty("deliveryCartConfirmationStatus")
    private String deliveryCartConfirmationStatus;
    @JsonProperty("itemConfirmationCallbackUrl")
    private String itemConfirmationCallbackUrl;
    @JsonProperty("cartConfirmationCallbackUrl")
    private String cartConfirmationCallbackUrl;
    @JsonProperty("attachment")
    private Attachment attachment;
    @JsonProperty("items")
    private List<Item> items = null;

    @JsonProperty("billTolerance")
    public Integer getBillTolerance() {
        return billTolerance;
    }

    @JsonProperty("billTolerance")
    public void setBillTolerance(Integer billTolerance) {
        this.billTolerance = billTolerance;
    }

    public MetaData withBillTolerance(Integer billTolerance) {
        this.billTolerance = billTolerance;
        return this;
    }

    @JsonProperty("payTolerance")
    public Integer getPayTolerance() {
        return payTolerance;
    }

    @JsonProperty("payTolerance")
    public void setPayTolerance(Integer payTolerance) {
        this.payTolerance = payTolerance;
    }

    public MetaData withPayTolerance(Integer payTolerance) {
        this.payTolerance = payTolerance;
        return this;
    }

    @JsonProperty("collectTolerance")
    public Integer getCollectTolerance() {
        return collectTolerance;
    }

    @JsonProperty("collectTolerance")
    public void setCollectTolerance(Integer collectTolerance) {
        this.collectTolerance = collectTolerance;
    }

    public MetaData withCollectTolerance(Integer collectTolerance) {
        this.collectTolerance = collectTolerance;
        return this;
    }


    @JsonProperty("deliveryCartConfirmationStatus")
    public String getDeliveryCartConfirmationStatus() {
        return deliveryCartConfirmationStatus;
    }

    @JsonProperty("deliveryCartConfirmationStatus")
    public void setDeliveryCartConfirmationStatus(String deliveryCartConfirmationStatus) {
        this.deliveryCartConfirmationStatus = deliveryCartConfirmationStatus;
    }

    @JsonProperty("itemConfirmationCallbackUrl")
    public String getItemConfirmationCallbackUrl() {
        return itemConfirmationCallbackUrl;
    }


    @JsonProperty("itemConfirmationCallbackUrl")
    public void setItemConfirmationCallbackUrl(String itemConfirmationCallbackUrl) {
        this.itemConfirmationCallbackUrl = itemConfirmationCallbackUrl;
    }


    @JsonProperty("cartConfirmationCallbackUrl")
    public String getCartConfirmationCallbackUrl() {
        return cartConfirmationCallbackUrl;
    }

    @JsonProperty("cartConfirmationCallbackUrl")

    public void setCartConfirmationCallbackUrl(String cartConfirmationCallbackUrl) {
        this.cartConfirmationCallbackUrl = cartConfirmationCallbackUrl;
    }

    @JsonProperty("attachment")

    public Attachment getAttachment() {
        return attachment;
    }

    @JsonProperty("attachment")
    public void setAttachment(Attachment attachment) {
        this.attachment = attachment;
    }



    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public MetaData withItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public static MetaData defaultMetaDataForJob()
    {


        MetaData metaData = new MetaData();
        // MetaData metaData = dashDeliveryHelper.createMetaDataForTask(20,20,20,"NA",item,"localhost/api/v1/dash/ff/delivery_job/12400525167/item_confirmation_request","localhost/api/v1/dash/ff/delivery_job/12400525167/cart_confirmation_request",attachment);
        metaData.setPayTolerance(DashDeliveryConstant.PAY_TOLERANCE);
        metaData.setCollectTolerance(DashDeliveryConstant.COLLECT_TOLERANCE);
        metaData.setBillTolerance(DashDeliveryConstant.BILL_TOLERANCE);
        metaData.setCartConfirmationCallbackUrl(DashDeliveryConstant.FF_URL_DELIVERY_JOB+""+DashDeliveryConstant.ORDER_ID+""+DashDeliveryConstant.CART_CONFIRMATION_ENDPOINT);
        metaData.setItemConfirmationCallbackUrl(DashDeliveryConstant.FF_URL_DELIVERY_JOB+""+DashDeliveryConstant.ORDER_ID+""+DashDeliveryConstant.ITEM_CONFIRMATION_ENDPOINT);
        metaData.setItems(Item.createDefaultItem());
        metaData.setAttachment(Attachment.setDefaultAttachment());
        metaData.setDeliveryCartConfirmationStatus(DashDeliveryConstant.STATUS_NA);
        return metaData;

    }

    public static MetaData defaultMetaDataForJob(String orderId,String deliveryCartConfirmationStatus)
    {


        MetaData metaData = new MetaData();
        // MetaData metaData = dashDeliveryHelper.createMetaDataForTask(20,20,20,"NA",item,"localhost/api/v1/dash/ff/delivery_job/12400525167/item_confirmation_request","localhost/api/v1/dash/ff/delivery_job/12400525167/cart_confirmation_request",attachment);
        metaData.setPayTolerance(DashDeliveryConstant.PAY_TOLERANCE);
        metaData.setCollectTolerance(DashDeliveryConstant.COLLECT_TOLERANCE);
        metaData.setBillTolerance(DashDeliveryConstant.BILL_TOLERANCE);
        metaData.setCartConfirmationCallbackUrl(DashDeliveryConstant.FF_URL_DELIVERY_JOB+""+orderId+""+DashDeliveryConstant.CART_CONFIRMATION_ENDPOINT);
        metaData.setItemConfirmationCallbackUrl(DashDeliveryConstant.FF_URL_DELIVERY_JOB+""+orderId+""+DashDeliveryConstant.ITEM_CONFIRMATION_ENDPOINT);
        metaData.setItems(Item.createDefaultItem());
        metaData.setAttachment(Attachment.setDefaultAttachment());
        metaData.setDeliveryCartConfirmationStatus(deliveryCartConfirmationStatus);
        return metaData;

    }




}
