package com.swiggy.api.dash.dashDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({

        "items",
        "cartImages",
        "estimatedBill",
        "taskId"
})
public class ConfirmItemCart {


 @JsonProperty("items")
    private List<Item> items;
 @JsonProperty("cartImages")
    private List<ImageUrls> cartImages;
 @JsonProperty("estimatedBill")
    private double estimatedBill;
 @JsonProperty("taskId")
    private int taskId;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public List<ImageUrls> getCartImages() {
        return cartImages;
    }

    public void setCartImages(List<ImageUrls> cartImages) {
        this.cartImages = cartImages;
    }

    public double getEstimatedBill() {
        return estimatedBill;
    }

    public void setEstimatedBill(double estimatedBill) {
        this.estimatedBill = estimatedBill;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }
}

