package com.swiggy.api.dash.checkout.helper;

import com.swiggy.api.dash.checkout.constants.Help;
import com.swiggy.api.dash.checkout.pojos.CartCreation;

import java.util.function.Function;

public class CartType extends FunctionalP<String,CartCreation> implements Ty<String, CartCreation>{

    ViewBuilder viewBuilder = new ViewBuilder();

    private CartCreation structuredCart(CartCreation cart, String ctype,String tdType){
        return viewBuilder.buildStructure(cart,ctype,tdType);
    }


    private CartCreation unStructuredCart(CartCreation cart,String ctype, String tdType){
        return viewBuilder.buildUnStructure(cart,ctype,tdType);
    }

    @Override
    public Function<String, CartCreation> Entity(String cartType) {
        return help -> {
            CartCreation cart = new CartCreation();
            if(cartType.equalsIgnoreCase(Help.TYPE.STRUCTURE.name())) {
              return viewBuilder.buildStructure(cart);
            }
            if(cartType.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
                return viewBuilder.buildUnStructure(cart);
            }
            return null;
        };
    }


    @Override
    public Function<String, CartCreation> Core(String cartType, String couponType,String tdType){
        return help -> {
            CartCreation cart = new CartCreation();
            if(cartType.equalsIgnoreCase(Help.TYPE.STRUCTURE.name())) {
                return this.structuredCart(cart,couponType, tdType);
            }
            if(cartType.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
                return this.unStructuredCart(cart, couponType, tdType);
            }
            return null;
        };
    }


    public Function<String, CartCreation> Cores(String cartType, String couponType,int enable,int amount,String tdType){
        return help -> {
            CartCreation cart = new CartCreation();
            if(cartType.equalsIgnoreCase(Help.TYPE.STRUCTURE.name())) {
                return viewBuilder.buildStructureMap(cart,couponType, enable,amount,tdType);
            }
            if(cartType.equalsIgnoreCase(Help.TYPE.UNSTRUCTURE.name())) {
                return viewBuilder.buildUnStructureMap(cart, couponType, enable, amount,tdType);
            }
            return null;
        };
    }






}
