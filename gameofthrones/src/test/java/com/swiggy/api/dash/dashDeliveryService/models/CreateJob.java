
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squareup.moshi.Json;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.JobType;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "jobType",
    "slaInMins",
    "orderId",
    "cityId",
    "tasks",
    "metaData",
    "serviceLineId",
    "areaId"
})
public class CreateJob {

    @JsonProperty("jobType")
    private JobType jobType;
    @JsonProperty("slaInMins")
    private Integer slaInMins;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("cityId")
    private Integer cityId;
    @JsonProperty("tasks")
    private List<Task> tasks = null;
    @JsonProperty("metaData")
    private MetaDataforJob metaData;
    @JsonProperty("serviceLineId")
    private Integer serviceLineId;
    @JsonProperty("areaId")
    private Integer areaId;

    @JsonProperty("jobType")
    public JobType getJobType() {
        return jobType;
    }

    @JsonProperty("jobType")
    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    @JsonProperty("slaInMins")
    public Integer getSlaInMins() {
        return slaInMins;
    }

    @JsonProperty("slaInMins")
    public void setSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
    }

    public CreateJob withSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
        return this;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CreateJob withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("tasks")
    public List<Task> getTasks() {
        return tasks;
    }

    @JsonProperty("tasks")
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public CreateJob withTasks(List<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    @JsonProperty("metaData")
    public MetaDataforJob getMetaData() {
        return metaData;
    }

    @JsonProperty("metaData")
    public void setMetaData(MetaDataforJob metaData) {
        this.metaData = metaData;
    }

    public CreateJob withMetaData(MetaDataforJob metaData) {
        this.metaData = metaData;
        return this;
    }

    @JsonProperty("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }


    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("serviceLineId")

    public void setServiceLineId(Integer serviceLineId) {
        this.serviceLineId = serviceLineId;
    }

    @JsonProperty("serviceLineId")
    public Integer getServiceLineId()
    {
        return serviceLineId;
    }


    @JsonProperty("areaId")
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    @JsonProperty("areaId")

    public Integer getAreaId() {
        return areaId;
    }

    public static CreateJob setDefaultCreateJobData(String orderId)
    {

        CreateJob createJob = new CreateJob();
        createJob.setCityId(DashDeliveryConstant.CITY_ID_GURGOAN);
        createJob.setAreaId(DashDeliveryConstant.AREA_ID);
        createJob.setSlaInMins(DashDeliveryConstant.DEFAULT_SLA);
        createJob.setTasks(Task.setDefaultTask());
        createJob.setOrderId(orderId);
        createJob.setMetaData(MetaDataforJob.defaultMetaDataForJob());
        createJob.setServiceLineId(DashDeliveryConstant.SERVICE_LINE_ID);
        createJob.setOrderId(orderId);
        createJob.setJobType(JobType.BUY);

        return createJob;

    }




}
