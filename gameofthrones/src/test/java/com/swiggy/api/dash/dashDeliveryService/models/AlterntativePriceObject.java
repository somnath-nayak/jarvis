package com.swiggy.api.dash.dashDeliveryService.models;

public class AlterntativePriceObject {

    public int numberOfAlternatives;
    public String priceOfAlternatives;

    public int getNumberOfAlternatives() {
        return numberOfAlternatives;
    }

    public void setNumberOfAlternatives(int numberOfAlternatives) {
        this.numberOfAlternatives = numberOfAlternatives;
    }

    public String getPriceOfAlternatives() {
        return priceOfAlternatives;
    }

    public void setPriceOfAlternatives(String priceOfAlternatives) {
        this.priceOfAlternatives = priceOfAlternatives;
    }
}
