package com.swiggy.api.dash.coupon.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "openTime",
        "closeTime",
        "day"
})
public class TimeSlot {

    @JsonProperty("openTime")
    private Integer openTime;
    @JsonProperty("closeTime")
    private Integer closeTime;
    @JsonProperty("day")
    private String day;

    /**
     * No args constructor for use in serialization
     *
     */
    public TimeSlot() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     */

    private List<TimeSlot> listOfTimeSlots = new ArrayList<>();

    public TimeSlot(Integer openTime, Integer closeTime, String day) {
        super();
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.day = day;
    }

    @JsonProperty("openTime")
    public Integer getOpenTime() {
        return openTime;
    }

    @JsonProperty("openTime")
    public TimeSlot setOpenTime(Integer openTime) {
        this.openTime = openTime;
        return this;
    }

    public TimeSlot withOpenTime(Integer openTime) {
        this.openTime = openTime;
        return this;
    }

    @JsonProperty("closeTime")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public TimeSlot setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    public TimeSlot withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public TimeSlot setDay(String day) {
        this.day = day;
        return this;
    }

    public TimeSlot withDay(String day) {
        this.day = day;
        return this;
    }

    public TimeSlot addItem(TimeSlot itemlist) {
        listOfTimeSlots.add(itemlist);
        return this;
    }
    public List<TimeSlot> returnListOfItems(){
        return listOfTimeSlots;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("openTime", openTime).append("closeTime", closeTime).append("day", day).toString();
    }

}
