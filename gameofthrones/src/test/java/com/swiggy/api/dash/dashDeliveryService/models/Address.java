
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import org.apache.commons.math3.analysis.function.Add;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "location",
    "name",
    "primaryContact",
    "addressLine",

})

public class Address {

    @JsonProperty("location")
    private String location;

    @JsonProperty("name")
    private String name;

    @JsonProperty("primaryContact")
    private String primaryContact;
 
    @JsonProperty("addressLine")
    private String addressLine;
 


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Address withName(String name) {
        this.name = name;
        return this;
    }


    @JsonProperty("primaryContact")
    public String getPrimaryContact() {
        return primaryContact;
    }

    @JsonProperty("primaryContact")
    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public Address withPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
        return this;
    }

  

    @JsonProperty("addressLine")
    public String getAddressLine() {
        return addressLine;
    }

    @JsonProperty("addressLine")
    public void setAddressLine(String addressLine) {
        this.addressLine = addressLine;
    }

    public Address withAddressLine(String addressLine) {
        this.addressLine = addressLine;
        return this;
    }


    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    public Address withLocation(String location) {
        this.location = location;
        return this;
    }

    public static Address setDefaultAddress()
    {
        Address address = new Address();
        address.setLocation(DashDeliveryConstant.LATLONG);
        address.setPrimaryContact(DashDeliveryConstant.PHONE);
        address.setName(DashDeliveryConstant.STORE_NAME);
        address.setAddressLine(DashDeliveryConstant.STORE_ADDRESS);
        return address;
    }

}
