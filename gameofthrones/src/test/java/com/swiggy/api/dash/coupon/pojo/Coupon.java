package com.swiggy.api.dash.coupon.pojo;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
            "name",
            "code",
            "description",
            "timeSlot",
            "image",
            "title",
            "tnc",
            "capping_percentage",
            "applicable_on",
            "coupon_type",
            "is_private",
            "valid_from",
            "valid_till",
            "updated_on",
            "created_on",
            "total_available",
            "total_per_user",
            "min_amount",
            "global_offset_days",
            "customer_restriction",
            "city_restriction",
            "area_restriction",
            "restaurant_restriction",
            "category_restriction",
            "item_restriction",
            "discount_percentage",
            "discount_amount",
            "free_shipping",
            "free_gifts",
            "first_order_restriction",
            "preferred_payment_method",
            "user_client",
            "slot_restriction",
            "coupon_bucket",
            "cuisine_restriction",
            "discount_item",
            "usage_count",
            "expiry_offset",
            "applicable_with_swiggy_money",
            "is_bank_discount",
            "refund_source",
            "pg_message",
            "supported_ios_version",
            "created_by",
            "logo_id",
            "coupon_user_type_id",
            "coupon_cooling_type_id",
            "num_txn_payment_type",
            "min_quantity",
            "coupon_domain"
    })
public class Coupon {

        @JsonProperty("name")
        private String name;
        @JsonProperty("code")
        private String code;
        @JsonProperty("description")
        private String description;
        @JsonProperty("timeSlot")
        private List<TimeSlot> timeSlot = null;
        @JsonProperty("image")
        private String image;
        @JsonProperty("title")
        private String title;
        @JsonProperty("tnc")
        private List<String> tnc = null;
        @JsonProperty("capping_percentage")
        private Integer cappingPercentage;
        @JsonProperty("applicable_on")
        private Integer applicableOn;
        @JsonProperty("coupon_type")
        private String couponType;
        @JsonProperty("is_private")
        private Integer isPrivate;
        @JsonProperty("valid_from")
        private String validFrom;
        @JsonProperty("valid_till")
        private String validTill;
        @JsonProperty("updated_on")
        private String updatedOn;
        @JsonProperty("created_on")
        private String createdOn;
        @JsonProperty("total_available")
        private Integer totalAvailable;
        @JsonProperty("total_per_user")
        private Integer totalPerUser;
        @JsonProperty("min_amount")
        private MinAmount minAmount;
        @JsonProperty("global_offset_days")
        private Integer globalOffsetDays;
        @JsonProperty("customer_restriction")
        private Integer customerRestriction;
        @JsonProperty("city_restriction")
        private Integer cityRestriction;
        @JsonProperty("area_restriction")
        private Integer areaRestriction;
        @JsonProperty("restaurant_restriction")
        private Integer restaurantRestriction;
        @JsonProperty("category_restriction")
        private Integer categoryRestriction;
        @JsonProperty("item_restriction")
        private Integer itemRestriction;
        @JsonProperty("discount_percentage")
        private Integer discountPercentage;
        @JsonProperty("discount_amount")
        private Integer discountAmount;
        @JsonProperty("free_shipping")
        private Integer freeShipping;
        @JsonProperty("free_gifts")
        private Integer freeGifts;
        @JsonProperty("first_order_restriction")
        private Integer firstOrderRestriction;
        @JsonProperty("preferred_payment_method")
        private String preferredPaymentMethod;
        @JsonProperty("user_client")
        private Integer userClient;
        @JsonProperty("slot_restriction")
        private Integer slotRestriction;
        @JsonProperty("coupon_bucket")
        private String couponBucket;
        @JsonProperty("cuisine_restriction")
        private Integer cuisineRestriction;
        @JsonProperty("discount_item")
        private String discountItem;
        @JsonProperty("usage_count")
        private Integer usageCount;
        @JsonProperty("expiry_offset")
        private Integer expiryOffset;
        @JsonProperty("applicable_with_swiggy_money")
        private Boolean applicableWithSwiggyMoney;
        @JsonProperty("is_bank_discount")
        private Integer isBankDiscount;
        @JsonProperty("refund_source")
        private String refundSource;
        @JsonProperty("pg_message")
        private String pgMessage;
        @JsonProperty("supported_ios_version")
        private Integer supportedIosVersion;
        @JsonProperty("created_by")
        private String createdBy;
        @JsonProperty("logo_id")
        private String logoId;
        @JsonProperty("coupon_user_type_id")
        private Integer couponUserTypeId;
        @JsonProperty("coupon_cooling_type_id")
        private Integer couponCoolingTypeId;
        @JsonProperty("num_txn_payment_type")
        private Integer numTxnPaymentType;
        @JsonProperty("min_quantity")
        private MinQuantity minQuantity;
        @JsonProperty("coupon_domain")
        private String couponDomain;

        /**
         * No args constructor for use in serialization
         *
         */
        public Coupon() {
        }

        /**
         *
         * @param pgMessage
         * @param applicableOn
         * @param isBankDiscount
         * @param usageCount
         * @param supportedIosVersion
         * @param freeShipping
         * @param cuisineRestriction
         * @param cappingPercentage
         * @param userClient
         * @param itemRestriction
         * @param validTill
         * @param discountAmount
         * @param logoId
         * @param couponBucket
         * @param firstOrderRestriction
         * @param title
         * @param expiryOffset
         * @param isPrivate
         * @param description
         * @param cityRestriction
         * @param discountPercentage
         * @param name
         * @param totalPerUser
         * @param updatedOn
         * @param couponUserTypeId
         * @param couponDomain
         * @param tnc
         * @param couponType
         * @param restaurantRestriction
         * @param applicableWithSwiggyMoney
         * @param image
         * @param totalAvailable
         * @param customerRestriction
         * @param preferredPaymentMethod
         * @param code
         * @param numTxnPaymentType
         * @param timeSlot
         * @param freeGifts
         * @param createdOn
         * @param categoryRestriction
         * @param minAmount
         * @param couponCoolingTypeId
         * @param createdBy
         * @param slotRestriction
         * @param discountItem
         * @param areaRestriction
         * @param globalOffsetDays
         * @param validFrom
         * @param refundSource
         * @param minQuantity
         */
        public Coupon(String name, String code, String description, List<TimeSlot> timeSlot, String image, String title, List<String> tnc, Integer cappingPercentage, Integer applicableOn, String couponType, Integer isPrivate, String validFrom, String validTill, String updatedOn, String createdOn, Integer totalAvailable, Integer totalPerUser, MinAmount minAmount, Integer globalOffsetDays, Integer customerRestriction, Integer cityRestriction, Integer areaRestriction, Integer restaurantRestriction, Integer categoryRestriction, Integer itemRestriction, Integer discountPercentage, Integer discountAmount, Integer freeShipping, Integer freeGifts, Integer firstOrderRestriction, String preferredPaymentMethod, Integer userClient, Integer slotRestriction, String couponBucket, Integer cuisineRestriction, String discountItem, Integer usageCount, Integer expiryOffset, Boolean applicableWithSwiggyMoney, Integer isBankDiscount, String refundSource, String pgMessage, Integer supportedIosVersion, String createdBy, String logoId, Integer couponUserTypeId, Integer couponCoolingTypeId, Integer numTxnPaymentType, MinQuantity minQuantity, String couponDomain) {
            super();
            this.name = name;
            this.code = code;
            this.description = description;
            this.timeSlot = timeSlot;
            this.image = image;
            this.title = title;
            this.tnc = tnc;
            this.cappingPercentage = cappingPercentage;
            this.applicableOn = applicableOn;
            this.couponType = couponType;
            this.isPrivate = isPrivate;
            this.validFrom = validFrom;
            this.validTill = validTill;
            this.updatedOn = updatedOn;
            this.createdOn = createdOn;
            this.totalAvailable = totalAvailable;
            this.totalPerUser = totalPerUser;
            this.minAmount = minAmount;
            this.globalOffsetDays = globalOffsetDays;
            this.customerRestriction = customerRestriction;
            this.cityRestriction = cityRestriction;
            this.areaRestriction = areaRestriction;
            this.restaurantRestriction = restaurantRestriction;
            this.categoryRestriction = categoryRestriction;
            this.itemRestriction = itemRestriction;
            this.discountPercentage = discountPercentage;
            this.discountAmount = discountAmount;
            this.freeShipping = freeShipping;
            this.freeGifts = freeGifts;
            this.firstOrderRestriction = firstOrderRestriction;
            this.preferredPaymentMethod = preferredPaymentMethod;
            this.userClient = userClient;
            this.slotRestriction = slotRestriction;
            this.couponBucket = couponBucket;
            this.cuisineRestriction = cuisineRestriction;
            this.discountItem = discountItem;
            this.usageCount = usageCount;
            this.expiryOffset = expiryOffset;
            this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
            this.isBankDiscount = isBankDiscount;
            this.refundSource = refundSource;
            this.pgMessage = pgMessage;
            this.supportedIosVersion = supportedIosVersion;
            this.createdBy = createdBy;
            this.logoId = logoId;
            this.couponUserTypeId = couponUserTypeId;
            this.couponCoolingTypeId = couponCoolingTypeId;
            this.numTxnPaymentType = numTxnPaymentType;
            this.minQuantity = minQuantity;
            this.couponDomain = couponDomain;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public Coupon setName(String name) {
            this.name = name;
            return this;
        }

        public Coupon withName(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("code")
        public String getCode() {
            return code;
        }

        @JsonProperty("code")
        public Coupon setCode(String code) {
            this.code = code;
            return this;
        }

        public Coupon withCode(String code) {
            this.code = code;
            return this;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public Coupon setDescription(String description) {
            this.description = description;
            return this;
        }

        public Coupon withDescription(String description) {
            this.description = description;
            return this;
        }

        @JsonProperty("timeSlot")
        public List<TimeSlot> getTimeSlot() {
            return timeSlot;
        }

        @JsonProperty("timeSlot")
        public Coupon setTimeSlot(List<TimeSlot> timeSlot) {
            this.timeSlot = timeSlot;
            return this;
        }

        public Coupon withTimeSlot(List<TimeSlot> timeSlot) {
            this.timeSlot = timeSlot;
            return this;
        }

        @JsonProperty("image")
        public String getImage() {
            return image;
        }

        @JsonProperty("image")
        public Coupon setImage(String image) {
            this.image = image;
            return this;
        }

        public Coupon withImage(String image) {
            this.image = image;
            return this;
        }

        @JsonProperty("title")
        public String getTitle() {
            return title;
        }

        @JsonProperty("title")
        public Coupon setTitle(String title) {
            this.title = title;
            return this;
        }

        public Coupon withTitle(String title) {
            this.title = title;
            return this;
        }

        @JsonProperty("tnc")
        public List<String> getTnc() {
            return tnc;
        }

        @JsonProperty("tnc")
        public Coupon setTnc(List<String> tnc) {
            this.tnc = tnc;
            return this;
        }

        public Coupon withTnc(List<String> tnc) {
            this.tnc = tnc;
            return this;
        }

        @JsonProperty("capping_percentage")
        public Integer getCappingPercentage() {
            return cappingPercentage;
        }

        @JsonProperty("capping_percentage")
        public Coupon setCappingPercentage(Integer cappingPercentage) {
            this.cappingPercentage = cappingPercentage;
            return this;
        }
        @JsonProperty("capping_percentage")
        public Coupon withCappingPercentage(Integer cappingPercentage) {
            this.cappingPercentage = cappingPercentage;
            return this;
        }

        @JsonProperty("applicable_on")
        public Integer getApplicableOn() {
            return applicableOn;
        }

        @JsonProperty("applicable_on")
        public Coupon setApplicableOn(Integer applicableOn) {
            this.applicableOn = applicableOn;
            return this;
        }

        public Coupon withApplicableOn(Integer applicableOn) {
            this.applicableOn = applicableOn;
            return this;
        }

        @JsonProperty("coupon_type")
        public String getCouponType() {
            return couponType;
        }

        @JsonProperty("coupon_type")
        public Coupon setCouponType(String couponType) {
            this.couponType = couponType;
            return this;
        }

        public Coupon withCouponType(String couponType) {
            this.couponType = couponType;
            return this;
        }

        @JsonProperty("is_private")
        public Integer getIsPrivate() {
            return isPrivate;
        }

        @JsonProperty("is_private")
        public Coupon setIsPrivate(Integer isPrivate) {
            this.isPrivate = isPrivate;
            return this;
        }

        public Coupon withIsPrivate(Integer isPrivate) {
            this.isPrivate = isPrivate;
            return this;
        }

        @JsonProperty("valid_from")
        public String getValidFrom() {
            return validFrom;
        }

        @JsonProperty("valid_from")
        public Coupon setValidFrom(String validFrom) {
            this.validFrom = validFrom;
            return this;
        }

        public Coupon withValidFrom(String validFrom) {
            this.validFrom = validFrom;
            return this;
        }

        @JsonProperty("valid_till")
        public String getValidTill() {
            return validTill;
        }

        @JsonProperty("valid_till")
        public Coupon setValidTill(String validTill) {
            this.validTill = validTill;
            return this;
        }

        public Coupon withValidTill(String validTill) {
            this.validTill = validTill;
            return this;
        }

        @JsonProperty("updated_on")
        public String getUpdatedOn() {
            return updatedOn;
        }

        @JsonProperty("updated_on")
        public Coupon setUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
            return this;
        }

        public Coupon withUpdatedOn(String updatedOn) {
            this.updatedOn = updatedOn;
            return this;
        }

        @JsonProperty("created_on")
        public String getCreatedOn() {
            return createdOn;
        }

        @JsonProperty("created_on")
        public Coupon setCreatedOn(String createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        public Coupon withCreatedOn(String createdOn) {
            this.createdOn = createdOn;
            return this;
        }

        @JsonProperty("total_available")
        public Integer getTotalAvailable() {
            return totalAvailable;
        }

        @JsonProperty("total_available")
        public Coupon setTotalAvailable(Integer totalAvailable) {
            this.totalAvailable = totalAvailable;
            return this;
        }

        public Coupon withTotalAvailable(Integer totalAvailable) {
            this.totalAvailable = totalAvailable;
            return this;
        }

        @JsonProperty("total_per_user")
        public Integer getTotalPerUser() {
            return totalPerUser;
        }

        @JsonProperty("total_per_user")
        public Coupon setTotalPerUser(Integer totalPerUser) {
            this.totalPerUser = totalPerUser;
            return this;
        }

        public Coupon withTotalPerUser(Integer totalPerUser) {
            this.totalPerUser = totalPerUser;
            return this;
        }

        @JsonProperty("min_amount")
        public MinAmount getMinAmount() {
            return minAmount;
        }

        @JsonProperty("min_amount")
        public Coupon setMinAmount(MinAmount minAmount) {
            this.minAmount = minAmount;
            return this;
        }

        public Coupon withMinAmount(MinAmount minAmount) {
            this.minAmount = minAmount;
            return this;
        }

        @JsonProperty("global_offset_days")
        public Integer getGlobalOffsetDays() {
            return globalOffsetDays;
        }

        @JsonProperty("global_offset_days")
        public Coupon setGlobalOffsetDays(Integer globalOffsetDays) {
            this.globalOffsetDays = globalOffsetDays;
            return this;
        }

        public Coupon withGlobalOffsetDays(Integer globalOffsetDays) {
            this.globalOffsetDays = globalOffsetDays;
            return this;
        }

        @JsonProperty("customer_restriction")
        public Integer getCustomerRestriction() {
            return customerRestriction;
        }

        @JsonProperty("customer_restriction")
        public Coupon setCustomerRestriction(Integer customerRestriction) {
            this.customerRestriction = customerRestriction;
            return this;
        }

        public Coupon withCustomerRestriction(Integer customerRestriction) {
            this.customerRestriction = customerRestriction;
            return this;
        }

        @JsonProperty("city_restriction")
        public Integer getCityRestriction() {
            return cityRestriction;
        }

        @JsonProperty("city_restriction")
        public Coupon setCityRestriction(Integer cityRestriction) {
            this.cityRestriction = cityRestriction;
            return this;
        }

        public Coupon withCityRestriction(Integer cityRestriction) {
            this.cityRestriction = cityRestriction;
            return this;
        }

        @JsonProperty("area_restriction")
        public Integer getAreaRestriction() {
            return areaRestriction;
        }

        @JsonProperty("area_restriction")
        public Coupon setAreaRestriction(Integer areaRestriction) {
            this.areaRestriction = areaRestriction;
            return this;
        }

        public Coupon withAreaRestriction(Integer areaRestriction) {
            this.areaRestriction = areaRestriction;
            return this;
        }

        @JsonProperty("restaurant_restriction")
        public Integer getRestaurantRestriction() {
            return restaurantRestriction;
        }

        @JsonProperty("restaurant_restriction")
        public Coupon setRestaurantRestriction(Integer restaurantRestriction) {
            this.restaurantRestriction = restaurantRestriction;
            return this;
        }

        public Coupon withRestaurantRestriction(Integer restaurantRestriction) {
            this.restaurantRestriction = restaurantRestriction;
            return this;
        }

        @JsonProperty("category_restriction")
        public Integer getCategoryRestriction() {
            return categoryRestriction;
        }

        @JsonProperty("category_restriction")
        public Coupon setCategoryRestriction(Integer categoryRestriction) {
            this.categoryRestriction = categoryRestriction;
            return this;
        }

        public Coupon withCategoryRestriction(Integer categoryRestriction) {
            this.categoryRestriction = categoryRestriction;
            return this;
        }

        @JsonProperty("item_restriction")
        public Integer getItemRestriction() {
            return itemRestriction;
        }

        @JsonProperty("item_restriction")
        public Coupon setItemRestriction(Integer itemRestriction) {
            this.itemRestriction = itemRestriction;
            return this;
        }

        public Coupon withItemRestriction(Integer itemRestriction) {
            this.itemRestriction = itemRestriction;
            return this;
        }

        @JsonProperty("discount_percentage")
        public Integer getDiscountPercentage() {
            return discountPercentage;
        }

        @JsonProperty("discount_percentage")
        public Coupon setDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
            return this;
        }

        public Coupon withDiscountPercentage(Integer discountPercentage) {
            this.discountPercentage = discountPercentage;
            return this;
        }

        @JsonProperty("discount_amount")
        public Integer getDiscountAmount() {
            return discountAmount;
        }

        @JsonProperty("discount_amount")
        public Coupon setDiscountAmount(Integer discountAmount) {
            this.discountAmount = discountAmount;
            return this;
        }

        public Coupon withDiscountAmount(Integer discountAmount) {
            this.discountAmount = discountAmount;
            return this;
        }

        @JsonProperty("free_shipping")
        public Integer getFreeShipping() {
            return freeShipping;
        }

        @JsonProperty("free_shipping")
        public Coupon setFreeShipping(Integer freeShipping) {
            this.freeShipping = freeShipping;
            return this;
        }

        public Coupon withFreeShipping(Integer freeShipping) {
            this.freeShipping = freeShipping;
            return this;
        }

        @JsonProperty("free_gifts")
        public Integer getFreeGifts() {
            return freeGifts;
        }

        @JsonProperty("free_gifts")
        public Coupon setFreeGifts(Integer freeGifts) {
            this.freeGifts = freeGifts;
            return this;
        }

        public Coupon withFreeGifts(Integer freeGifts) {
            this.freeGifts = freeGifts;
            return this;
        }

        @JsonProperty("first_order_restriction")
        public Integer getFirstOrderRestriction() {
            return firstOrderRestriction;
        }

        @JsonProperty("first_order_restriction")
        public Coupon setFirstOrderRestriction(Integer firstOrderRestriction) {
            this.firstOrderRestriction = firstOrderRestriction;
            return this;
        }

        public Coupon withFirstOrderRestriction(Integer firstOrderRestriction) {
            this.firstOrderRestriction = firstOrderRestriction;
            return this;
        }

        @JsonProperty("preferred_payment_method")
        public String getPreferredPaymentMethod() {
            return preferredPaymentMethod;
        }

        @JsonProperty("preferred_payment_method")
        public Coupon setPreferredPaymentMethod(String preferredPaymentMethod) {
            this.preferredPaymentMethod = preferredPaymentMethod;
            return this;
        }

        public Coupon withPreferredPaymentMethod(String preferredPaymentMethod) {
            this.preferredPaymentMethod = preferredPaymentMethod;
            return this;
        }

        @JsonProperty("user_client")
        public Integer getUserClient() {
            return userClient;
        }

        @JsonProperty("user_client")
        public Coupon setUserClient(Integer userClient) {
            this.userClient = userClient;
            return this;
        }

        public Coupon withUserClient(Integer userClient) {
            this.userClient = userClient;
            return this;
        }

        @JsonProperty("slot_restriction")
        public Integer getSlotRestriction() {
            return slotRestriction;
        }

        @JsonProperty("slot_restriction")
        public Coupon setSlotRestriction(Integer slotRestriction) {
            this.slotRestriction = slotRestriction;
            return this;
        }

        public Coupon withSlotRestriction(Integer slotRestriction) {
            this.slotRestriction = slotRestriction;
            return this;
        }

        @JsonProperty("coupon_bucket")
        public String getCouponBucket() {
            return couponBucket;
        }

        @JsonProperty("coupon_bucket")
        public Coupon setCouponBucket(String couponBucket) {
            this.couponBucket = couponBucket;
            return this;
        }

        public Coupon withCouponBucket(String couponBucket) {
            this.couponBucket = couponBucket;
            return this;
        }

        @JsonProperty("cuisine_restriction")
        public Integer getCuisineRestriction() {
            return cuisineRestriction;
        }

        @JsonProperty("cuisine_restriction")
        public Coupon setCuisineRestriction(Integer cuisineRestriction) {
            this.cuisineRestriction = cuisineRestriction;
            return this;
        }

        public Coupon withCuisineRestriction(Integer cuisineRestriction) {
            this.cuisineRestriction = cuisineRestriction;
            return this;
        }

        @JsonProperty("discount_item")
        public String getDiscountItem() {
            return discountItem;
        }

        @JsonProperty("discount_item")
        public Coupon setDiscountItem(String discountItem) {
            this.discountItem = discountItem;
            return this;
        }

        public Coupon withDiscountItem(String discountItem) {
            this.discountItem = discountItem;
            return this;
        }

        @JsonProperty("usage_count")
        public Integer getUsageCount() {
            return usageCount;
        }

        @JsonProperty("usage_count")
        public Coupon setUsageCount(Integer usageCount) {
            this.usageCount = usageCount;
            return this;
        }

        public Coupon withUsageCount(Integer usageCount) {
            this.usageCount = usageCount;
            return this;
        }

        @JsonProperty("expiry_offset")
        public Integer getExpiryOffset() {
            return expiryOffset;
        }

        @JsonProperty("expiry_offset")
        public Coupon setExpiryOffset(Integer expiryOffset) {
            this.expiryOffset = expiryOffset;
            return this;
        }

        public Coupon withExpiryOffset(Integer expiryOffset) {
            this.expiryOffset = expiryOffset;
            return this;
        }

        @JsonProperty("applicable_with_swiggy_money")
        public Boolean getApplicableWithSwiggyMoney() {
            return applicableWithSwiggyMoney;
        }

        @JsonProperty("applicable_with_swiggy_money")
        public Coupon setApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
            this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
            return this;
        }

        public Coupon withApplicableWithSwiggyMoney(Boolean applicableWithSwiggyMoney) {
            this.applicableWithSwiggyMoney = applicableWithSwiggyMoney;
            return this;
        }

        @JsonProperty("is_bank_discount")
        public Integer getIsBankDiscount() {
            return isBankDiscount;
        }

        @JsonProperty("is_bank_discount")
        public Coupon setIsBankDiscount(Integer isBankDiscount) {
            this.isBankDiscount = isBankDiscount;
            return this;
        }

        public Coupon withIsBankDiscount(Integer isBankDiscount) {
            this.isBankDiscount = isBankDiscount;
            return this;
        }

        @JsonProperty("refund_source")
        public String getRefundSource() {
            return refundSource;
        }

        @JsonProperty("refund_source")
        public Coupon setRefundSource(String refundSource) {
            this.refundSource = refundSource;
            return this;
        }

        public Coupon withRefundSource(String refundSource) {
            this.refundSource = refundSource;
            return this;
        }

        @JsonProperty("pg_message")
        public String getPgMessage() {
            return pgMessage;
        }

        @JsonProperty("pg_message")
        public Coupon setPgMessage(String pgMessage) {
            this.pgMessage = pgMessage;
            return this;
        }

        public Coupon withPgMessage(String pgMessage) {
            this.pgMessage = pgMessage;
            return this;
        }

        @JsonProperty("supported_ios_version")
        public Integer getSupportedIosVersion() {
            return supportedIosVersion;
        }

        @JsonProperty("supported_ios_version")
        public Coupon setSupportedIosVersion(Integer supportedIosVersion) {
            this.supportedIosVersion = supportedIosVersion;
            return this;
        }

        public Coupon withSupportedIosVersion(Integer supportedIosVersion) {
            this.supportedIosVersion = supportedIosVersion;
            return this;
        }

        @JsonProperty("created_by")
        public String getCreatedBy() {
            return createdBy;
        }

        @JsonProperty("created_by")
        public Coupon setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        public Coupon withCreatedBy(String createdBy) {
            this.createdBy = createdBy;
            return this;
        }

        @JsonProperty("logo_id")
        public String getLogoId() {
            return logoId;
        }

        @JsonProperty("logo_id")
        public Coupon setLogoId(String logoId) {
            this.logoId = logoId;
            return this;
        }

        public Coupon withLogoId(String logoId) {
            this.logoId = logoId;
            return this;
        }

        @JsonProperty("coupon_user_type_id")
        public Integer getCouponUserTypeId() {
            return couponUserTypeId;
        }

        @JsonProperty("coupon_user_type_id")
        public Coupon setCouponUserTypeId(Integer couponUserTypeId) {
            this.couponUserTypeId = couponUserTypeId;
            return this;
        }

        public Coupon withCouponUserTypeId(Integer couponUserTypeId) {
            this.couponUserTypeId = couponUserTypeId;
            return this;
        }

        @JsonProperty("coupon_cooling_type_id")
        public Integer getCouponCoolingTypeId() {
            return couponCoolingTypeId;
        }

        @JsonProperty("coupon_cooling_type_id")
        public Coupon setCouponCoolingTypeId(Integer couponCoolingTypeId) {
            this.couponCoolingTypeId = couponCoolingTypeId;
            return this;
        }

        public Coupon withCouponCoolingTypeId(Integer couponCoolingTypeId) {
            this.couponCoolingTypeId = couponCoolingTypeId;
            return this;
        }

        @JsonProperty("num_txn_payment_type")
        public Integer getNumTxnPaymentType() {
            return numTxnPaymentType;
        }

        @JsonProperty("num_txn_payment_type")
        public Coupon setNumTxnPaymentType(Integer numTxnPaymentType) {
            this.numTxnPaymentType = numTxnPaymentType;
            return this;
        }

        public Coupon withNumTxnPaymentType(Integer numTxnPaymentType) {
            this.numTxnPaymentType = numTxnPaymentType;
            return this;
        }

        @JsonProperty("min_quantity")
        public MinQuantity getMinQuantity() {
            return minQuantity;
        }

        @JsonProperty("min_quantity")
        public Coupon setMinQuantity(MinQuantity minQuantity) {
            this.minQuantity = minQuantity;
            return this;
        }

        @JsonProperty("min_quantity")
        public Coupon withMinQuantity(MinQuantity minQuantity) {
            this.minQuantity = minQuantity;
            return this;
        }

        @JsonProperty("coupon_domain")
        public String getCouponDomain() {
            return couponDomain;
        }

        @JsonProperty("coupon_domain")
        public Coupon setCouponDomain(String couponDomain) {
            this.couponDomain = couponDomain;
            return this;
        }

        public Coupon withCouponDomain(String couponDomain) {
            this.couponDomain = couponDomain;
            return this;
        }


    }

