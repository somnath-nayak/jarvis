package com.swiggy.api.dash;

import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;
import cucumber.api.java.cs.A;

import java.util.HashMap;

public class DashContant {


    public static String CATEGORY_SUPERMARKET="Supermarkets";
    public static String CATEGORY_BAKERY="Bakery";
    public static String CATEGORY_BOOKSTORE="Book store";
    public static String CATEGORY_VEGETABLES_SHOP="Vegetables shop";
    public static String CATEGORY_GROCERY_STORE="Grocery store";
    public static String CATEGORY_LIQUOR_STORE="Liquor stores";
    public static String CATEGORY_PHARMACY="Pharmacy";





    public static String SUBCATEGORY_WHISKY="Whisky";
    public static String SUBCATEGORY_MEDICINE="medicine";
    public static String SUBCATEGORY_FRUIT="fruit";
    public static String SUBCATEGORY_CIGARETTE="cigarette";
    public static String SUBCATEGORY_BIRTHDAYGIFT="Birthdaygift";
    public static String SUBCATEGORY_BREAD="Bread";
    public static String SUBCATEGORY_SAMSUNGPHONE="Samsung phones";


    public static String TAG_DIWALI="Diwali";
    public static String TAG_FRUITSPECIAL="fruit special";
    public static String TAG_KIDSWEAR="kids wear";

    public static String CITYNAME_1="GURGAON";
    public static int CITYID_1;

    public static String AREANAME_1="HUDACITY";
    public static String LATLONG_1="28.435892,77.004015";
    public static int AREAID_1;


    static{
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        CITYID_1=dashAvailabilityHelper.createCity(CITYNAME_1);
        AREAID_1=dashAvailabilityHelper.createArea(CITYID_1, AREANAME_1);
    }

    public static HashMap<String,Integer> getCategoryIds(){
        HashMap<String,Integer> categorymapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        categorymapping.put(CATEGORY_BAKERY,dashAvailabilityHelper.createCategory(CATEGORY_BAKERY));
        categorymapping.put(CATEGORY_BOOKSTORE,dashAvailabilityHelper.createCategory(CATEGORY_BOOKSTORE));
        categorymapping.put(CATEGORY_VEGETABLES_SHOP,dashAvailabilityHelper.createCategory(CATEGORY_VEGETABLES_SHOP));
        categorymapping.put(CATEGORY_GROCERY_STORE,dashAvailabilityHelper.createCategory(CATEGORY_GROCERY_STORE));
        categorymapping.put(CATEGORY_LIQUOR_STORE,dashAvailabilityHelper.createCategory(CATEGORY_LIQUOR_STORE));
        categorymapping.put(CATEGORY_PHARMACY,dashAvailabilityHelper.createCategory(CATEGORY_PHARMACY));
        categorymapping.put(CATEGORY_SUPERMARKET,dashAvailabilityHelper.createCategory(CATEGORY_SUPERMARKET));
        categorymapping.put("Fruit shops",dashAvailabilityHelper.createCategory("Fruit shops"));


        return categorymapping;
    }

    public static HashMap<String,Integer> getSubCategoryIds(){
        HashMap<String,Integer> subcategorymapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        subcategorymapping.put(SUBCATEGORY_WHISKY,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_WHISKY));
        subcategorymapping.put(SUBCATEGORY_MEDICINE,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_MEDICINE));
        subcategorymapping.put(SUBCATEGORY_FRUIT,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_FRUIT));
        subcategorymapping.put(SUBCATEGORY_CIGARETTE,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_CIGARETTE));
        subcategorymapping.put(SUBCATEGORY_BIRTHDAYGIFT,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_BIRTHDAYGIFT));
        subcategorymapping.put(SUBCATEGORY_BREAD,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_BREAD));
        subcategorymapping.put(SUBCATEGORY_SAMSUNGPHONE,dashAvailabilityHelper.createSubCategory(SUBCATEGORY_SAMSUNGPHONE));


        return subcategorymapping;
    }

    public static HashMap<String,Integer> getTagIds(){
        HashMap<String,Integer> tagMapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        tagMapping.put(TAG_DIWALI,dashAvailabilityHelper.createTag(TAG_DIWALI));
        tagMapping.put(TAG_KIDSWEAR,dashAvailabilityHelper.createTag(TAG_KIDSWEAR));
        tagMapping.put(TAG_FRUITSPECIAL,dashAvailabilityHelper.createTag(TAG_FRUITSPECIAL));



        return tagMapping;
    }


}
