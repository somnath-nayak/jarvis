package com.swiggy.api.dash.common;
import com.swiggy.api.dash.kms.constants.KMS;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;

public class HttpCore {
    Initialize gameofthrones = new Initialize();
    String serviceName = "";

    public HttpCore(String serviceName) {
        this.serviceName = serviceName;
    }

    /**
     *
     * @param apiName
     * @param headers
     * @return
     */
    public HttpResponse invoker(String apiName, HashMap<String, String> headers){
        return invoker(apiName, headers, null);
    }

    /**
     *
     * @param apiName
     * @param headers
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, HashMap<String, String> headers, String[] urlParam){
        return invoker(apiName, headers, null, urlParam);
    }

    /**
     *
     * @param apiName
     * @param headers
     * @param payloadParam
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, HashMap<String, String> headers, String[] payloadParam, String[] urlParam){
        GameOfThronesService service = new GameOfThronesService(serviceName,apiName, gameofthrones);
        Processor proc = new Processor(service, headers, payloadParam, urlParam);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(),  proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     *
     * @param apiName Name of the API
     * @param headers
     * @param payloadParam Array of payload params
     * @return
     */
    public HttpResponse invokerPayloadParam(String apiName, HashMap<String, String> headers, String[] payloadParam){
        return invoker(apiName, headers, payloadParam, null);
    }

    /**
     *
     * @param apiName
     * @param headers
     * @param jsonAsStr
     * @return
     */
    public HttpResponse invokePostWithStringJson(String apiName, HashMap<String, String> headers, String jsonAsStr){
        return invokePostWithStringJson(apiName, headers, jsonAsStr, null);
    }

    /**
     *
     * @param apiName
     * @param headers
     * @param jsonAsStr
     * @param urlParams
     * @return
     */
    public HttpResponse invokePostWithStringJson(String apiName, HashMap<String, String> headers, String jsonAsStr, String[] urlParams){
        GameOfThronesService service = new GameOfThronesService(serviceName,apiName, gameofthrones);
        Processor proc = new Processor(service, headers, jsonAsStr, urlParams, 0);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(),  proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     *
     * @param apiName
     * @param headers
     * @param formData
     * @return
     */
    public HttpResponse invokeFormData(String apiName, HashMap<String, String> headers, HashMap<String, String> formData){
        GameOfThronesService service = new GameOfThronesService(serviceName, apiName, gameofthrones);
        Processor proc =  new Processor(service, headers, null, null, formData);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(),  proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     * get all headers
     * @return
     */
    public HashMap getBasicHeaders(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(KMS.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(KMS.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        return headers;
    }

    /**
     * get all headers
     * @return
     */
    public HashMap getFormTypeHeaders(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(KMS.HEADER_CONTENT_TYPE, MediaType.MULTIPART_FORM_DATA);
        headers.put(KMS.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        return headers;
    }
}
