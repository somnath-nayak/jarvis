package com.swiggy.api.dash.Delivery.serviceability.models;

import javax.validation.constraints.NotNull;

public class Condition {

    private String tag;
    private int value;
    private Left left;
    private Right right;
    private String operator;

    public Condition(String tag,int value){
        this.tag=tag;
        this.value=value;
    }
    public Condition(){

    }
    public Condition(Left left,Right right,String operator){
        this.left=left;
        this.right=right;
        this.operator=operator;
    }



    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }



    public Left getLeft() {
        return left;
    }
    @NotNull
    public void setLeft(Left left) {
        this.left = left;
    }
    @NotNull
    public Right getRight() {
        return right;
    }
    @NotNull
    public void setRight(Right right) {
        this.right = right;
    }
    @NotNull
    public String getOperator() {
        return operator;
    }
    @NotNull
    public void setOperator(String operator) {
        this.operator = operator;
    }



}
