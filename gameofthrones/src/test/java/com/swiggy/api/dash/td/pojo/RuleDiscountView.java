package com.swiggy.api.dash.td.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "discountLevel",
        "minCartAmount",
        "percentDiscount",
        "flatDiscountAmount"
})
public class RuleDiscountView {

    @JsonProperty("type")
    private String type;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("minCartAmount")
    private String minCartAmount;
    @JsonProperty("flatDiscountAmount")
    private String flatDiscountAmount;
    @JsonProperty("percentDiscount")
    private String percentDiscount;

    public String getFlatDiscountAmount() {
        return flatDiscountAmount;
    }

    public RuleDiscountView setFlatDiscountAmount(String flatDiscountAmount) {
        this.flatDiscountAmount = flatDiscountAmount;
        return this;
    }


    public String getPercentDiscount() {
        return percentDiscount;
    }

    public RuleDiscountView setPercentDiscount(String percentDiscount) {
        this.percentDiscount = percentDiscount;
        return this;
    }



    /**
     * No args constructor for use in serialization
     *
     */
    public RuleDiscountView() {
    }

    /**
     *
     * @param minCartAmount
     * @param discountLevel
     * @param type
     */
    public RuleDiscountView(String type, String discountLevel, String minCartAmount) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.minCartAmount = minCartAmount;
    }

    public RuleDiscountView(String type, String discountLevel, String minCartAmount, String flatDiscountAmount) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.minCartAmount = minCartAmount;
        this.flatDiscountAmount= flatDiscountAmount;
    }

    /*public RuleDiscount(String type, String discountLevel, String minCartAmount, String percentDiscount) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.minCartAmount = minCartAmount;
        this.percentDiscount= percentDiscount;
    }*/



    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public RuleDiscountView setType(String type) {
        this.type = type;
        return this;
    }

    public RuleDiscountView withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public RuleDiscountView setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    public RuleDiscountView withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("minCartAmount")
    public String getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public RuleDiscountView setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
        return this;
    }

    public RuleDiscountView withMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("discountLevel", discountLevel).append("minCartAmount", minCartAmount).toString();
    }

}