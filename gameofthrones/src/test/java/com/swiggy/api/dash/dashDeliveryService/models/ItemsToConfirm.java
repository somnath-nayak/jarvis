package com.swiggy.api.dash.dashDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "isAvailable",
        "isRemoved",
        "alternatives",
        "description"
    })
public class ItemsToConfirm {

    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("isAvailable")
    private boolean isAvailable;
    @JsonProperty("isRemoved")
    private boolean isRemoved;
    @JsonProperty("alternatives")
    private List<ItemAlternative> alternatives;
    @JsonProperty("description")
    private String description;





    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }

    public List<ItemAlternative> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<ItemAlternative> alternatives) {
        this.alternatives = alternatives;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public List<ItemsToConfirm> setDefaultItemsToConfirm()
    {
        ItemsToConfirm itemsToConfirm = new ItemsToConfirm();
        itemsToConfirm.setAlternatives(ItemAlternative.setDefaultItemAlternative());
        itemsToConfirm.setId(1);
        itemsToConfirm.setAvailable(false);
        itemsToConfirm.setRemoved(false);
        itemsToConfirm.setName(DashDeliveryConstant.ALTERNATIVE_ITEM_NAME);
        itemsToConfirm.setDescription(DashDeliveryConstant.ITEM_DESC);

        ArrayList<ItemsToConfirm> itemsToConfirmArrayList = new ArrayList<>(asList(itemsToConfirm));
        return itemsToConfirmArrayList;


    }

}
