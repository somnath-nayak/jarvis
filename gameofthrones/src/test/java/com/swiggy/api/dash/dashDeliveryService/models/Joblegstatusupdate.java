package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "status",
        "battery",
        "location",
        "deId"
})
public class Joblegstatusupdate {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("status")
    private String status;
    @JsonProperty("battery")
    private Integer battery;
    @JsonProperty("location")
    private Location location;
    @JsonProperty("deId")
    private Integer deId;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    public Joblegstatusupdate withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    public Joblegstatusupdate withStatus(String status) {
        this.status = status;
        return this;
    }

    @JsonProperty("battery")
    public Integer getBattery() {
        return battery;
    }

    @JsonProperty("battery")
    public void setBattery(Integer battery) {
        this.battery = battery;
    }

    public Joblegstatusupdate withBattery(Integer battery) {
        this.battery = battery;
        return this;
    }

    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    public Joblegstatusupdate withLocation(Location location) {
        this.location = location;
        return this;
    }

    @JsonProperty("deId")
    public Integer getDeId() {
        return deId;
    }

    @JsonProperty("deId")
    public void setDeId(Integer deId) {
        this.deId = deId;
    }

    public Joblegstatusupdate withDeId(Integer deId) {
        this.deId = deId;
        return this;
    }

}
