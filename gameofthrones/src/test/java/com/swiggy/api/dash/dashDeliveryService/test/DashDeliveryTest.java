package com.swiggy.api.dash.dashDeliveryService.test;

import com.swiggy.api.dash.SuperAppDeliveryService.helper.Superappdeliveryhelper;
import com.swiggy.api.dash.Tripmanager.helper.*;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryHelper;
import com.swiggy.api.dash.dashDeliveryService.helper.TaskMetaDataStatus;
import com.swiggy.api.dash.dashDeliveryService.models.*;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.validation.constraints.AssertTrue;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class DashDeliveryTest {

    JsonHelper helper = new JsonHelper();
    JsonHelper jsonHelper=new JsonHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    DashDeliveryHelper dashDeliveryHelper =new DashDeliveryHelper();
    Superappdeliveryhelper superappdeliveryhelper=new Superappdeliveryhelper();
    DeliveryServiceApiHelper deliveryServiceApiHelper = new DeliveryServiceApiHelper();
    TripManagerHelper tripManagerHelper=new TripManagerHelper();
    SetJob setJob = new SetJob();
    public static int deId;



   /*
   Create Job
    */

    @Test(description = "create job", dataProviderClass = DashDeliveryDataProvider.class, dataProvider = "createJobData",priority = 0)
    public void verifyCreateJob(String requestPayload) throws Exception {


        Processor p1 = dashDeliveryHelper.createJob(requestPayload);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        //Set Job Id generated
        int jobId = dashDeliveryHelper.getJobId(p1.ResponseValidator.GetBodyAsText());
        setJob.setJobId(jobId);

    }

    /*
    Create Delivery Executive and assign the order
     */

    @Test(description = "Create Delivery Executive and confirm the order created",priority = 1)
    public void createDeAndMarkOrderConfirmedAndArrived() throws Exception
    {
        int jobId = setJob.getJobId();
        deId=Integer.parseInt(deliveryServiceApiHelper.createDEandmakeDEActive(DashDeliveryConstant.ZONE_ID_87));
        superappdeliveryhelper.loginDeliveryExecutie((long)deId);
        tripManagerHelper.assigntripToDeByJobId(jobId,deId);
        tripManagerHelper.updatejobLegStatusByJobId(JobLegStatus.CONFIRMED,jobId,deId, JobType.BUY);
    }

    /*
    Update "ARRIVED" status
     */
    @Test(description = "update DE arrived status",priority = 1)
    public void updateDEArrivedStatus()throws Exception
    {
        int taskId =dashDeliveryHelper.getTaskInProgressFromJobId(setJob.getJobId());
        tripManagerHelper.updateTaskStatus(taskId, TaskStatus.ARRIVED.toString(),(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETPAY,DashDeliveryConstant.TEST_DE_ID,JobType.BUY);
        //Assert.assertTrue(dashDeliveryHelper.checkStatusInTaskMetaDataFromJobId(setJob.getJobId(),TaskMetaDataStatus.ITEM_CONFIRMED));
    }

    /*
    Confirm Items
     */

    @Test(description = "confirm Items ",priority = 1)
    public void itemConfirmation()throws Exception
    {
        int jobId= setJob.getJobId();
        Processor p1 = dashDeliveryHelper.itemConfirmationUsingJobId(jobId);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        Thread.sleep(50000);
        Assert.assertTrue(dashDeliveryHelper.checkStatusInTaskMetaDataFromJobId(jobId,TaskMetaDataStatus.ITEM_CONFIRMED));
    }


    /*
    Cart confirmation
     */

    @Test(description = "confirm cart and check if status is updated in meta data for both PICK_UP and DROP tasks should give 200",priority = 2)
    public void cartConfirmation() throws Exception {


        int jobId = setJob.getJobId();
        // int jobId = 185723;
        Processor p1=dashDeliveryHelper.cartConfirmationUsingJobId(jobId);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        Thread.sleep(50000);
        //Check if status updated in database
        /*List<Map<String, Object>> metaDataDetails = dashDeliveryHelper.getTaskInProgressMetaDataFromJobId(jobId);
        for(int i =0;i<metaDataDetails.size();i++)
        {

            String metaData = metaDataDetails.get(i).get("meta_data").toString();
            if(metaData != null)
            {

                Assert.assertTrue(dashDeliveryHelper.checkStatusInTaskMetaData(metaData, TaskMetaDataStatus.CART_CONFIRMED));
            }
        }*/
        Assert.assertTrue(dashDeliveryHelper.checkStatusInTaskMetaDataFromJobId(jobId,TaskMetaDataStatus.CART_CONFIRMED));


    }

    /*
    Update  Drop Status after pickup
     */

    @Test(description = "Update  Drop Status after pickup",priority = 2)
    public void updateDropTaskStatusAfterPickedUp()throws Exception
    {
        int jobId = setJob.getJobId();
        tripManagerHelper.updateJobStatus(jobId,deId,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.IN_TRANSIT);
        tripManagerHelper.updateJobStatus(jobId,deId,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.REACHED);
        tripManagerHelper.updateJobStatus(jobId,deId,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.COMPLETED);

    }

    @Test(description = "Unassign DE when task completed",priority = 2)
    public void unassignDE()throws Exception
    {
        int jobId = setJob.getJobId();
        tripManagerHelper.unAssignTripToDeByJobId(jobId,deId);

    }




    /*
    Item Confirmation Test Cases
     */



    //TODO Check with dev
    @Test(description = "Validate if isAvailable:false with no alternatives when delivery status is ITEM_CONFIRMATION_PENDING must give 400", dataProviderClass = DashDeliveryDataProvider.class,dataProvider = "createJobData",priority = 3)
    public void noAlternativeCheck(String payload) throws Exception
    {
        verifyCreateJob(payload);
        createDeAndMarkOrderConfirmedAndArrived();
        updateDEArrivedStatus();
        Processor p1 = dashDeliveryHelper.itemConfirmationUsingJobIdWithIsAvailableAndAlternatives(setJob.getJobId(),null,false);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_BAD_REQUEST);
    }

    @Test(description = "Validate if isAvailable:true with alternatives when delivery status is ITEM_CONFIRMATION_PENDING must give 400",priority = 3)
    public void alternativeCheckWithIsAvailableTrue() throws Exception
    {

        Processor p1 = dashDeliveryHelper.itemConfirmationUsingJobIdWithIsAvailableAndAlternatives(setJob.getJobId(),ItemAlternative.setDefaultItemAlternative(),true);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_BAD_REQUEST);
    }


    @Test(description = "Validate if isAvailable:false with alternatives when delivery status is ITEM_CONFIRMATION_PENDING must give 200",priority = 4)
    public void alternativeCheck() throws Exception
    {

        Processor p1 = dashDeliveryHelper.itemConfirmationUsingJobIdWithIsAvailableAndAlternatives(setJob.getJobId(),ItemAlternative.setDefaultItemAlternative(),false);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
    }


    /*
    Item Confirmation using HashMap Of Number of Alternatives and JobId
     */
    @Test(priority = 5)
    public void itemConfirmationTempToCheckHashMapHelper()throws Exception
    {
        int jobId= 142164;
        Processor p1= dashDeliveryHelper.itemConfirmationUsingJobId(jobId,dashDeliveryHelper.createMockHashMap());
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
    }







   /*
    Item Availability Using Order ID
     *//*

    @Test(description = "Item Availability Check")
    public void itemAvailabilityUsingOrderId()throws Exception
    {

        Processor p1 = dashDeliveryHelper.itemConfirmationUsingJobId(113338);
        String response = p1.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);

    }

    /*
    Validate if isAvailable : false then alternatives must be there
     */




   /*
    Cart Confirmation
     */

 /*  @Test(description = "confirm cart when trip not in progress should give 400 ",priority = 1)
    public void cartConfirmationForTripIdNotInProgress() throws Exception
   {
       int jobId = setJob.getJobId();
       Processor p1=dashDeliveryHelper.cartConfirmationUsingJobId(setJob.getJobId());
       Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);

   }*/





    //END TO END
    /*
    Entire CAPRD
     */


    @Test(description = "Verify CAPRD", dataProviderClass = DashDeliveryDataProvider.class, dataProvider = "createJobData",priority = 4)
    public void verifyCAPRD(String requestPayload) throws Exception {


        Processor p1 = dashDeliveryHelper.createJob(requestPayload);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        //Set Job Id generated
        int jobId = dashDeliveryHelper.getJobId(p1.ResponseValidator.GetBodyAsText());
        setJob.setJobId(jobId);
        Superappdeliveryhelper s=new Superappdeliveryhelper();
        TripManagerHelper t=new TripManagerHelper();
        DashDeliveryHelper d=new DashDeliveryHelper();
        s.loginDeliveryExecutie((long)(DashDeliveryConstant.TEST_DE_ID));
        t.assigntripToDeByJobId(jobId,DashDeliveryConstant.TEST_DE_ID);
        t.updatejobLegStatusByJobId(JobLegStatus.CONFIRMED,jobId,DashDeliveryConstant.TEST_DE_ID, JobType.BUY);
        int taskId =d.getTaskInProgressFromJobId(jobId);
        t.updateTaskStatus(taskId, TaskStatus.ARRIVED.toString(),(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETPAY,DashDeliveryConstant.TEST_DE_ID,JobType.BUY);
        p1 = d.itemConfirmationUsingJobId(jobId);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        p1=dashDeliveryHelper.cartConfirmationUsingJobId(jobId);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        t.updateJobStatus(jobId,DashDeliveryConstant.TEST_DE_ID,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.IN_TRANSIT);
        t.updateJobStatus(jobId,DashDeliveryConstant.TEST_DE_ID,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.REACHED);
        t.updateJobStatus(jobId,DashDeliveryConstant.TEST_DE_ID,(int)DashDeliveryConstant.SETPAY,(int)DashDeliveryConstant.SETBILL,(int)DashDeliveryConstant.SETBILL, Jobstatus.COMPLETED);
        t.unAssignTripToDeByJobId(jobId,DashDeliveryConstant.TEST_DE_ID);

    }

     /*
     Create and Cancel Job
     */

    @Test(description = "cancel job",dataProviderClass = DashDeliveryDataProvider.class, dataProvider = "createTMSJob",priority = 4)
    public void verifyCancelJob(String requestPayload) throws IOException {



        Processor p1 = dashDeliveryHelper.createJob(requestPayload);
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);
        //Set Job Id generated
        int jobId = dashDeliveryHelper.getJobId(p1.ResponseValidator.GetBodyAsText());
        setJob.setJobId(jobId);
        String cancelJobPayload = jsonHelper.getObjectToJSON(setJob);
       // System.out.println(cancelJobPayload);
        p1 = dashDeliveryHelper.cancelJob(cancelJobPayload);
        //String response = p1.ResponseValidator.GetBodyAsText();
        Assert.assertEquals(p1.ResponseValidator.GetResponseCode(),HttpStatus.SC_OK);

    }




}
