package com.swiggy.api.dash.Delivery.serviceability.models;

public class LatLong {
    private double lon;
    private double lat;

    public LatLong(double lat,double lon){
        this.lat=lat;
        this.lon=lon;
    }
    public double getLon ()
    {
        return lon;
    }
    public void setLon (double lon)
    {
        this.lon = lon;
    }
    public double getLat ()
    {
        return lat;
    }
    public void setLat (double lat)
    {
        this.lat = lat;
    }

}
