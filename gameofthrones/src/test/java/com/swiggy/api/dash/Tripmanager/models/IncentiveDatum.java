
package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ruleName",
    "bonus",
    "timestamp",
    "description"
})
public class IncentiveDatum {

    @JsonProperty("ruleName")
    private String ruleName;
    @JsonProperty("bonus")
    private Integer bonus;
    @JsonProperty("timestamp")
    private Integer timestamp;
    @JsonProperty("description")
    private String description;

    @JsonProperty("ruleName")
    public String getRuleName() {
        return ruleName;
    }

    @JsonProperty("ruleName")
    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public IncentiveDatum withRuleName(String ruleName) {
        this.ruleName = ruleName;
        return this;
    }

    @JsonProperty("bonus")
    public Integer getBonus() {
        return bonus;
    }

    @JsonProperty("bonus")
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public IncentiveDatum withBonus(Integer bonus) {
        this.bonus = bonus;
        return this;
    }

    @JsonProperty("timestamp")
    public Integer getTimestamp() {
        return timestamp;
    }

    @JsonProperty("timestamp")
    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public IncentiveDatum withTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public IncentiveDatum withDescription(String description) {
        this.description = description;
        return this;
    }

}
