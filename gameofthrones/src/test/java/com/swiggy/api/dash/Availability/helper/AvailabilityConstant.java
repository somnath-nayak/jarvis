package com.swiggy.api.dash.Availability.helper;

import java.util.HashMap;

public class AvailabilityConstant {


    public static HashMap<String,String> kafkatopic;
    static
    {
        kafkatopic = new HashMap<String, String>();
        kafkatopic.put("city", "kmsupdates.cityitemblackzone");
        kafkatopic.put("area", "kmsupdates.areaitemblackzone");
        kafkatopic.put("holidayslot","kmsupdates.holidayslot");
        kafkatopic.put("store","kmsupdates.store");
    }
    public static String DASH_AVAILIBILITY_REDIS="dashavailibilityredis";
    public static String AVAILIBILITY_SERVICE_NAME = "dashavailability";
    public static String KMS_SERVICE_NAME = "kms";




}
