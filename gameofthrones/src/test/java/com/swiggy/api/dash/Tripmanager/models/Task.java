
package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "address",
    "pay",
    "collect",
    "bill",
    "metaData",
        "id"
})
public class Task {

    @JsonProperty("type")
    private String type;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("pay")
    private Integer pay;
    @JsonProperty("collect")
    private Integer collect;
    @JsonProperty("bill")
    private Integer bill;
    @JsonProperty("metaData")
    private MetaData metaData;
    @JsonProperty("id")
    private String id=null;

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Task withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    public Task withAddress(Address address) {
        this.address = address;
        return this;
    }

    @JsonProperty("pay")
    public Integer getPay() {
        return pay;
    }

    @JsonProperty("pay")
    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public Task withPay(Integer pay) {
        this.pay = pay;
        return this;
    }

    @JsonProperty("collect")
    public Integer getCollect() {
        return collect;
    }

    @JsonProperty("collect")
    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public Task withCollect(Integer collect) {
        this.collect = collect;
        return this;
    }

    @JsonProperty("bill")
    public Integer getBill() {
        return bill;
    }

    @JsonProperty("bill")
    public void setBill(Integer bill) {
        this.bill = bill;
    }

    public Task withBill(Integer bill) {
        this.bill = bill;
        return this;
    }

    @JsonProperty("metaData")
    public MetaData getMetaData() {
        return metaData;
    }

    @JsonProperty("metaData")
    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public Task withMetaData(MetaData metaData) {
        this.metaData = metaData;
        return this;
    }
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public Task withId(String id) {
        this.id = id;
        return this;
    }

}
