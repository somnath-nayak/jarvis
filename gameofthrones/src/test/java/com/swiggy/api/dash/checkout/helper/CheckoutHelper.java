package com.swiggy.api.dash.checkout.helper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.constants.Help;

import com.swiggy.api.dash.checkout.pojos.EditItemPatch;
import com.swiggy.api.dash.checkout.pojos.Item;
import com.swiggy.api.dash.checkout.pojos.OrderCreation;
import com.swiggy.api.dash.cms.InventoryBuilder;
import com.swiggy.api.dash.coupon.pojo.Coupon;
import com.swiggy.api.dash.coupon.pojo.CouponMap;
import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.kms.helper.HttpResponse;
import com.swiggy.api.dash.td.TdType;
import com.swiggy.api.dash.td.pojo.TdCart;
import com.swiggy.api.dash.td.pojo.TdView;
import com.swiggy.api.dash.userService.Login;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.apache.http.HttpStatus;
import org.testng.Reporter;
import org.testng.annotations.Test;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class CheckoutHelper{

    Initialize gameofthrones = new Initialize();
    String serviceName = "";
    static List<String> list = new ArrayList<>();
    HashMap <String, String> hashMap = new HashMap<>();

    /*public CheckoutHelper(String serviceName) {
        this.serviceName = serviceName;
    }*/

    private ObjectMapper objectMapper = new ObjectMapper();

    /**
     * @param apiName
     * @return
     */
    public HttpResponse invoker(String apiName) {
        return invoker(apiName, null);
    }

    /**
     * @param apiName
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, String[] urlParam) {
        return invoker(apiName, null, urlParam);
    }

    /**
     * @param apiName
     * @param payloadParam
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, String[] payloadParam, String[] urlParam) {
        GameOfThronesService service = new GameOfThronesService("generalcart", apiName, gameofthrones);
        Processor proc = new Processor(service, getAllCartHeaders(), payloadParam, urlParam);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(), proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     * @param apiName      Name of the API
     * @param payloadParam Array of payload params
     * @return
     */
    public HttpResponse invokerPayloadParam(String apiName, String[] payloadParam) {
        return invoker(apiName, payloadParam, null);
    }

    public HttpResponse invokePostWithStringJson(String apiName, String jsonAsStr) {
        return invokePostWithStringJson(apiName, jsonAsStr, null);
    }

    public HttpResponse invokePostWithStringJson(String apiName, String jsonAsStr, String[] urlParams) {
        GameOfThronesService service = new GameOfThronesService("generalcart", apiName, gameofthrones);
        Processor proc = new Processor(service, getAllCartHeaders(), jsonAsStr, urlParams, 0);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(), proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }


    public HttpResponse invokePostWithStringJson(String apiName, String jsonAsStr, String[] urlParams, HashMap hMap) {
        GameOfThronesService service = new GameOfThronesService("generalcart", apiName, gameofthrones);
        Processor proc = new Processor(service, hMap, jsonAsStr, urlParams, 0);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(), proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     * get all headers
     *
     * @return
     */
    public HashMap getAllCartHeaders() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        headers.put(CART.VERSIONCODE, CART.CODE);
        headers.put(CART.USERAGENT, CART.ANDROID);
        return headers;
    }

    public HashMap contentType() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        return headers;
    }


    public HashMap commonHead() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(CART.TID, CART.KEY);
        headers.put(CART.TOKEN, CART.TOKENKEY);
        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        return headers;
    }

    //cart,
    public HashMap commonHeaders(HashMap<? extends String, ? extends String> hashMap) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(CART.USERAGENT, CART.ANDROID);
        headers.put(CART.VERSIONCODE, CART.CODE);
        headers.put(CART.SID, CART.SIDVAL);
        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        return headers;
    }

    public HashMap header(HashMap<? extends String, ? extends String> hashMap) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(CART.USERAG, CART.ANDROID);
        headers.put(CART.VERSIONCODE, CART.CODE);
        headers.put(CART.SID, CART.SIDVAL);
        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        return headers;
    }

    public HashMap commonHeaders(HashMap<String, String> hashMap, List<String> list) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(CART.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(CART.TID, CART.KEY);
        headers.put(CART.TOKEN, CART.TOKENKEY);
        headers.put(CART.SID, "22");

        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        return headers;
    }

    public HashMap payment() {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.PAYMENTVERSIONCODE, CART.CODE);
        headers.put(CART.USERAGENT, CART.ANDROID);
        headers.put(CART.SECRETKEY, CART.SECRETVALUE);
        return headers;
    }

    public HashMap payment(HashMap<? extends String, ? extends String> hashMap) {
        HashMap<String, String> headers = new HashMap<>();
        headers.put(CART.PAYMENTVERSIONCODE, CART.CODE);
        headers.put(CART.USERAGENT, CART.ANDROID);
        headers.put(CART.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        hashMap.forEach((key, value) -> headers.merge(key, value, (v1, v2) -> v1.equals(v2) ? v1 : v1 + "," + v2));
        return headers;
    }


    public String getAbsolutePathOfSchema(String apiname) {
        return KMS.KMS_SCHEMA_PATH + apiname + ".json";
    }

    /**
     * @param min       Start index
     * @param max       End Index
     * @param separator value of separator e.g , # etc
     * @param size      number of records
     * @return
     */
    public String randomNumbersWithSeperator(int min, int max, String separator, int size) {
        StringBuffer stringToReturn = new StringBuffer();
        HashSet<Integer> uniqueInts = new HashSet<>();
        while (uniqueInts.size() != size) {
            uniqueInts.add(ThreadLocalRandom.current().nextInt(min, max + 1));
        }
        uniqueInts.forEach(number -> stringToReturn.append(String.valueOf(number)).append(separator));
        return separator.length() > 0 ? stringToReturn.substring(0, (stringToReturn.length() - 1)) : stringToReturn.toString();
    }

    /**
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T getDataFromJson(String jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    /**
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T getDataFromJson(Object jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }


    public <T> String serialize(T object) {
        try {
            return objectMapper.writeValueAsString(object);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot serialize given object");
        }
    }

    public <T> T deserialize(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }

    public <T> T deserialize(String json, TypeReference<T> type) {
        try {
            return objectMapper.readValue(json, type);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }


    public String couponCreation(Coupon coupon) {
        HttpResponse resp = invokePostWithStringJson("dashcoupon", "buildcoupon", serialize(coupon), null, contentType());
        return getDataFromJson(resp.getResponseMsg(), "$.data.id", String.class);
    }

    public void couponMap(List<CouponMap> couponMap) {
        HttpResponse resp = invokePostWithStringJson("dashcoupon", "mapcoupon", serialize(couponMap), null, contentType());
    }

    public HttpResponse invokePostWithStringJson(String serviceName, String apiName, String jsonAsStr, String[] urlParams, HashMap hMap) {
        GameOfThronesService service = new GameOfThronesService(serviceName, apiName, gameofthrones);
        Processor proc = new Processor(service, hMap, jsonAsStr, urlParams, 0);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(), proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }


    public void menu(String s, String lat, String lng) {
        invokePostWithStringJson("dashds", "dashmenu", null, new String[]{s, lat, lng}, contentType());
    }

    public HashMap<String, String> signIn(Login login) {
        HttpResponse resp = invokePostWithStringJson("dashuser", "dashlogin", serialize(login), null, contentType());
        String key = getDataFromJson(resp.getResponseMsg(), "$.tid", String.class);
        String tokenKey = getDataFromJson(resp.getResponseMsg(), "$.data.token", String.class);
        hashMap.put("tid", key);
        hashMap.put("token", tokenKey);
        return hashMap;
    }
    TdType tdType = new TdType();

    public void tdCreation(TdView td){
        List<Object> l=getTdOnCart(String.valueOf(td.getStoreIds().get(0)));
        for(int i=0; i<l.size(); i++) {
            HttpResponse resp1 = invokePostWithStringJson("dashtd", "disabletd", serialize(tdType.disableTd(String.valueOf(l.get(i)))), null, contentType());
        }
        HttpResponse response = invokePostWithStringJson("dashtd", "buildtd", serialize(td), null, contentType());
        if (getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class) == 2) {
            String test = getDataFromJson(response.getResponseMsg(), "$.data", String.class);
            String last = test.substring(test.lastIndexOf(" ") + 1);
            tdType.disableTd(last);
            HttpResponse resp = invokePostWithStringJson("dashtd", "disabletd", serialize(tdType.disableTd(last)), null, contentType());
            tdCreation(td);
            return;
        } else {
           /* String l=getTdOnCart(String.valueOf(td.getStoreIds().get(0)));
            HttpResponse resp = invokePostWithStringJson("dashtd", "disabletd", serialize(tdType.disableTd(l)), null, contentType());*/
             getDataFromJson(response.getResponseMsg(), "$.data", String.class);
        }
    }

    public List<Object> getTdOnCart(String stroeId){
        HttpResponse getTdCart= invokePostWithStringJson("dashtd", "evalutecart", serialize(getStoreTd(stroeId)), null, contentType());
        JSONArray tt=getDataFromJson(getTdCart.getResponseMsg(), "$.data.stores..campaignId", JSONArray.class);
        List<Object> te=tt.stream().filter(t-> t!=null).collect(Collectors.toList());
        return te;
    }

   /* @Test
    public void te(){
        getTdOnCart("41288");
    }*/

    public TdCart getStoreTd(String id){
        TdCart tdCart= new TdCart();
        List<TdCart.StoreIdDistanceList> storeIdDistanceLists= new ArrayList<>();
        storeIdDistanceLists.add(new TdCart.StoreIdDistanceList(id,0.0));
        return tdCart.setStoreIdDistanceList(storeIdDistanceLists);
    }


    public void disableTd(String store){
        List<Object> l=getTdOnCart(String.valueOf(store));
        for(int i=0; i<l.size(); i++) {
            HttpResponse resp1 = invokePostWithStringJson("dashtd", "disabletd", serialize(tdType.disableTd(String.valueOf(l.get(i)))), null, contentType());
        }

    }

   public List<Item> itemList(String storeId){
       List<Object> images1 = new ArrayList<>();
       List<Item> itemList= new Item().addItem(new Item("Item", storeId, 1, CART.STRUCTURE,"7", ""))
               .addItem(new Item("Milk",storeId, 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
               .addItem(new Item("Beer",storeId, 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
               .returnListOfItems();
       return itemList;
   }

   public List<Item> itemListUpdate(String storeId){
        List<Object> images1 = new ArrayList<>();
        List<Item> itemList= new Item()
                .addItem(new Item("Item", storeId, 1, CART.STRUCTURE,"7", ""))
                .returnListOfItems();
        return itemList;
    }

    public EditItemPatch addItemPatch(List<Item> item){
        Map<String, Object> ctx = new HashMap<>();
        return new EditItemPatch().setItems(item)
                .setCtx(ctx)
                .setVerifyItemAvailability(true)
                .setVerifyItemPricing(true)
                .setStoreAvailability(true)
                .setVerifyServiceability(true);
    }


    public HttpResponse getOrderJob(String orderJob) {
        return invoker("getorder", new String[]{orderJob});
    }

    public HttpResponse getMenu(String storeId, String lat, String lng) {
        return invoker("dashmenu", new String[]{storeId, lat, lng});
    }

    public List<String> getItems(String storeId, String lat, String lng) {
        HttpResponse response = invokePostWithStringJson("dashds", "dashmenu", null, new String[]{storeId, lat, lng}, contentType());
        JSONArray j = getDataFromJson(response.getResponseMsg(), "$..data..items..variations..id", JSONArray.class);
        JSONArray stock = getDataFromJson(response.getResponseMsg(), "$..data..items..variations..in_stock", JSONArray.class);
        List<String> getItems= new ArrayList<>();
        for(int i=0; i<stock.size(); i++){
            if(stock.get(i).equals(true)){
                getItems.add(j.get(i).toString());
            }
        }
        return getItems;
    }


 public HttpResponse orderDetails(String cType, String couTy, String tdType,String mobile, String password) {
        CartType cartType = new CartType();
        OrderType orderType = new OrderType();
        ViewBuilder viewBuilder = new ViewBuilder();

        Login login = new Login().setMobile(mobile).setPassword(password);
        HashMap<String, String>hMap=signIn(login);
        if (cType.equalsIgnoreCase(CART.structure)) {
            String createCartRequest = serialize(cartType.Core(cType, couTy, tdType).apply(Help.Structure));
            HttpResponse response = invokePostWithStringJson("create", createCartRequest,null,commonHeaders(hMap));
            if (response.getResponseCode() != HttpStatus.SC_OK) {

            } else {
                Reporter.log("ID " + getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
                String address=getDataFromJson(response.getResponseMsg(),"$.data.addresses..id", JSONArray.class).get(0).toString();

                HttpResponse paymentLink = invokePostWithStringJson("paymentlink", null, null, payment(hMap));
                Double amount = getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
                String orderRequest = serialize(viewBuilder.buildStructureOrder(amount,address));
                return invokePostWithStringJson("order", orderRequest, null, commonHeaders(hMap));
            }
        }

        if (cType.equalsIgnoreCase(CART.UNSTRUCTURE)) {
            String createUNCartRequest = serialize(cartType.Core(cType, couTy, tdType).apply(Help.UnStructure));
            HttpResponse respons = invokePostWithStringJson("create", createUNCartRequest);
            if (respons.getResponseCode() != HttpStatus.SC_OK) {

            } else {
                Reporter.log("ID " + getDataFromJson(respons.getResponseMsg(), "$.statusCode", Integer.class), true);
                String address=getDataFromJson(respons.getResponseMsg(),"$.data.addresses..id", JSONArray.class).get(0).toString();
                String cartId = getDataFromJson(respons.getResponseMsg(), "$.data.cartId", String.class);
                String orderRequest = serialize(viewBuilder.buildUnStructureOrder(address));
                return invokePostWithStringJson("order", orderRequest, null, commonHeaders(hMap));
            }
        }
        return null;
    }

    public EditItemPatch getItemPatch(List<Item> items){
        List<Object> images = new ArrayList<>();
        List<Item> item = new Item()
                .addItem(new Item("Beer","41288", 1,"unstructure","750ml",images,"comment","dash"))
                .returnListOfItems();
        item.addAll(items);
        Map<String, Object> ctx = new HashMap<>();
        return new EditItemPatch().setItems(item)
                .setCtx(ctx)
                .setVerifyItemAvailability(true)
                .setVerifyItemPricing(true)
                .setStoreAvailability(true)
                .setVerifyServiceability(true);
    }

    public Login getLogin(){
        return new Login().setMobile(CART.mobile).setPassword(CART.pwd);
    }



    public void inventoryUpdate(){
        List<InventoryBuilder> list= new ArrayList<>();
        InventoryBuilder.InventoryConstruct inventoryConstruct= new InventoryBuilder.InventoryConstruct().setAttributes(new InventoryBuilder.InventoryConstruct.Attributes())
                .setInventories(new InventoryBuilder.InventoryConstruct.Inventories(0));
        InventoryBuilder inventoryBuilder= new InventoryBuilder().setInventoryConstruct(inventoryConstruct).setKey("41288-1").setMeta(new InventoryBuilder.Meta("41288"));
        list.add(inventoryBuilder);
        hashMap.put(CART.HEADER_CONTENT_TYPE,MediaType.APPLICATION_JSON);
        hashMap.put(CART.SERVICELINE, CART.CATALOG);
        HttpResponse response=invokePostWithStringJson("cmsinv", "createinv", serialize(list),null, hashMap);
    }

    //store-itemId combination
    public void clearInv(String sI){
        List<String> l= new ArrayList<>();
        l.add(sI);
        HttpResponse response=invokePostWithStringJson("cmsinv", "clearinv", serialize(l),null, hashMap);
    }


   /* @Test
    public void t(){
        List<InventoryBuilder> list= new ArrayList<>();
        InventoryBuilder.InventoryConstruct inventoryConstruct= new InventoryBuilder.InventoryConstruct().setAttributes(new InventoryBuilder.InventoryConstruct.Attributes())
                .setInventories(new InventoryBuilder.InventoryConstruct.Inventories(0));
        InventoryBuilder inventoryBuilder= new InventoryBuilder().setInventoryConstruct(inventoryConstruct).setKey("41288-1").setMeta(new InventoryBuilder.Meta("41288"));
        list.add(inventoryBuilder);
        System.out.println(serialize(list));
        HashMap<String, String> hashMap= new HashMap<>();
        hashMap.put(CART.HEADER_CONTENT_TYPE,MediaType.APPLICATION_JSON);
        hashMap.put("", "dash_catalog");
        HttpResponse response=invokePostWithStringJson("cmsinv", "createinv", serialize(list),null, hashMap);
        System.out.println(response.getResponseMsg());

    }
*/

    public OrderCreation buildCustomOrder(Integer addressId){
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        return new OrderCreation().setItems(item1)
                .setAddressId(addressId)
                .setPaymentType(CART.POSTPAYMENT);
    }


}
