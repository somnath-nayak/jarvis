package com.swiggy.api.dash.SuperAppDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.Tripmanager.models.Addon;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "version"
})


public class DELoginRequest {

    @JsonProperty("id")
    private long id;
    @JsonProperty("version")
    private String version;


    @JsonProperty("id")
    public long getId() {
        return id;
    }


    @JsonProperty("id")
    public void setId(long name) {
        this.id = id;
    }

    public DELoginRequest withId(long id) {
        this.id = id;
        return this;
    }


    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String name) {
        this.version = version;
    }

    public DELoginRequest withVersion(String version) {
        this.version = version;
        return this;
    }
}
