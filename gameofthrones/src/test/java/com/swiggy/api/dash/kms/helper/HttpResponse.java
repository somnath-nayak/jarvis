package com.swiggy.api.dash.kms.helper;

import java.util.Map;

public class HttpResponse {

    private int responseCode;
    private String responseMsg;
    private Map<String, String> headers;

    public HttpResponse(int responseCode, String responseMsg) {
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
    }

    public HttpResponse(int responseCode, String responseMsg, Map<String, String> headers){
        this.responseCode = responseCode;
        this.responseMsg = responseMsg;
        this.setHeaders(headers);
    }

    /**
     * @return the responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * @return the responseMsg
     */
    public String getResponseMsg() {
        return responseMsg;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return this.responseCode + "::" + this.getResponseMsg();
    }
}
