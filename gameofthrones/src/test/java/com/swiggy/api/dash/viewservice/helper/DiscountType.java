package com.swiggy.api.dash.viewservice.helper;

public enum DiscountType {
    Percentage,
    Freebie,
    Flat,
    BXGY,
    FREE_DELIVERY
}
