package com.swiggy.api.dash.dashDeliveryService.helper;

public enum TaskType {
    PICK_UP,
    DROP,
}
