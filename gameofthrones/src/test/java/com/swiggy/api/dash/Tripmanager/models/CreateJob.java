
package com.swiggy.api.dash.Tripmanager.models;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squareup.moshi.Json;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "jobType",
    "slaInMins",
    "orderId",
    "tasks",
    "metaData",
     "id"
})
public class CreateJob {

    @JsonProperty("jobType")
    private String jobType;
    @JsonProperty("slaInMins")
    private Integer slaInMins;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("tasks")
    private List<Task> tasks = null;
    @JsonProperty("metaData")
    private MetaDataforJob metaData;
    @JsonProperty("id")
    private String id;

    @JsonProperty("jobType")
    public String getJobType() {
        return jobType;
    }

    @JsonProperty("jobType")
    public void setJobType(String jobType) {
        this.jobType = jobType;
    }


    public CreateJob withJobType(String jobType) {
        this.jobType = jobType;
        return this;
    }

    @JsonProperty("slaInMins")
    public Integer getSlaInMins() {
        return slaInMins;
    }

    @JsonProperty("slaInMins")
    public void setSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
    }

    public CreateJob withSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
        return this;
    }

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public CreateJob withOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("tasks")
    public List<Task> getTasks() {
        return tasks;
    }

    @JsonProperty("tasks")
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public CreateJob withTasks(List<Task> tasks) {
        this.tasks = tasks;
        return this;
    }

    @JsonProperty("metaData")
    public MetaDataforJob getMetaData() {
        return metaData;
    }

    @JsonProperty("metaData")
    public void setMetaData(MetaDataforJob metaData) {
        this.metaData = metaData;
    }

    public CreateJob withMetaData(MetaDataforJob metaData) {
        this.metaData = metaData;
        return this;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String jobid) {
        this.id = jobid;
    }

    public CreateJob withId(String jobid) {
        this.id = jobid;
        return this;
    }

}
