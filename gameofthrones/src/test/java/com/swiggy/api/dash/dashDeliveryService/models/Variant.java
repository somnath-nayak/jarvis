
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;

import java.util.ArrayList;

import static java.util.Arrays.asList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name"
})
public class Variant {

    @JsonProperty("name")
    private String name;

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    public static ArrayList<Variant> setDefaultVariant()
    {

        Variant v1 = new Variant();
        v1.setName(DashDeliveryConstant.VARIANT_QTY);

        Variant v2 = new Variant();
        v1.setName(DashDeliveryConstant.VARIANT_QTY);
        ArrayList<Variant> variants = new ArrayList<>((asList(v1,v2)));

        return variants;

    }


   /* @JsonProperty("price")
    private Integer price;




    public Variant withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Variant withPrice(Integer price) {
        this.price = price;
        return this;
    }*/

}
