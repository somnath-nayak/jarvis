package com.swiggy.api.dash.viewservice.helper;

public enum StoreIdentifier {
    CATEGORY,
    STORE,
    TAG
}
