package com.swiggy.api.dash.lms.helpers;

import com.swiggy.api.dash.lms.constants.LMS;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.Map;

public class LmsDBManager {
    DBHelper db = new DBHelper();

    public interface APILead {
        String ID = "id";
        String NAME = "name";
        String PHONE_NUMBER = "phone_numbers";
        String LAT="lat";
        String LNG="lng";
        String ADDRESS="address";
        String IS_DUPLICATE="is_duplicate";
        String PARENT_ID="parent_id";
        String LANDMARK="landmark";
        String EMAIL="email";
        String WEBSITE="website";
        String CATEGORY="category";
        String SUB_CATEGORY="subcategory";
        String POPULAR_CATEGORY="popular_category";
        String POPULAR_SUB_CATEGORY="popular_subcategory";
        String SUGGESTED_ITEMS="suggested_items";
        String LIST_CATEGORY="list_category";
        String TAGS="tags";
        String IMAGES="images";
        String CHAIN="chain";
        String ID_FROM_SOURCE="id_from_source";
        String SOURCE_LINK="source_link";
        String SOURCE_RATING="source_rating";
        String SOURCE_VOTES="source_votes";
        String TYPE="type";
        String PARTNER_INTENT="partner_intent";
        String HOLIDAYS="holidays";
        String SCORE="score";
        String ONGROUND_RATING="onground_rating";
        String ONGROUND_VERIFIED="onground_verified";
        String TELE_VERIFIED="tele_verified";
        String BLACKLISTED="blacklisted";
        String AVAILABLE_JD="available_jd";
        String AVAILABLE_GOOLE="available_google";
        String AVAILABLE_SWIGGY="available_swiggy";
        String AVAILABLE_DUNZO="available_dunzo";
        String TIME_SLOTS="time_slots";
        String WAIT_TIME="wait_time";
        String HOME_DELIVERY="home_delivery";
        String PAYMENT_METHODS="payment_methods";
        String COMMISSION="commission";
        String AREA_ID="area_id";
        String CITY_ID="city_id";
        String SOURCE_ID="source_id";
        String DEDUP_STATUS="dedup_status";
    }

    public interface APIDuplicateLead {
        String ID="id";
        String CHILD_ID="child_id";
        String PARENT_ID="parent_id";
    }

    public interface LeadsName{
        String NAMES = "'A', " +
                "'B', " +
                "'C', " +
                "'D', " +
                "'E', " +
                "'F', " +
                "'G', " +
                "'H', " +
                "'I', " +
                "'J', " +
                "'K', " +
                "'L', " +
                "'M', " +
                "'N', " +
                "'O', " +
                "'P', " +
                "'Q', " +
                "'R', " +
                "'S', " +
                "'T', " +
                "'U', " +
                "'V', " +
                "'"+ LMS.P_100_NAME_MATCH_1 + "'," +
                "'"+ LMS.P_100_NAME_MATCH_2 + "'," +
                "'"+ LMS.P_90_NAME_MATCH_1 + "'," +
                "'"+ LMS.P_90_NAME_MATCH_2 + "'," +
                "'"+ LMS.P_90_TO_98_NAME_MATCH_1 + "'," +
                "'"+ LMS.P_90_TO_98_NAME_MATCH_2 + "'," +
                "'"+ LMS.P_95_ABOVE_NAME_MATCH_1 + "'," +
                "'"+ LMS.P_95_ABOVE_NAME_MATCH_2 + "'" ;
    }

    /**
     *
     * @param leadID
     * @return
     */
    public Map<String, Object> leadDetails(int leadID) {
        return getDbInstance().queryForMap("SELECT * from api_lead WHERE id=?", leadID);
    }

    /**
     *
     * @param childId
     * @param parendId
     * @return
     */
    public Map<String, Object> duplicateLeadDetails(String childId, String parendId) {
        return getDbInstance().queryForMap("SELECT * from api_duplicatelead WHERE child_id=? AND parent_id=?", childId, parendId);
    }

    /**
     *
     * @param listOfLeads
     */
    public void deleteLeadsFromTable(String listOfLeads){
        getDbInstance().execute("DELETE FROM api_businesshour WHERE lead_id IN ( " + listOfLeads + " )");
        getDbInstance().execute("DELETE FROM api_lead WHERE id IN ( " + listOfLeads + " )");
    }

    /**
     *
     * @param listOfLeads
     */
    public void deleteDuplicateLeadsFromTable(String listOfLeads){
        getDbInstance().execute("DELETE FROM api_businesshour WHERE lead_id IN ( " + listOfLeads + " )");
        getDbInstance().execute("DELETE FROM api_duplicatelead WHERE child_id IN ( " + listOfLeads + " )");
        getDbInstance().execute("DELETE FROM api_duplicatelead WHERE parent_id IN ( " + listOfLeads + " )");
    }

    /**
     *
     * @param childID
     * @param parentID
     * @return
     */
    public int getDuplicateLeadId(int childID, int parentID) {
        return Integer.parseInt(getDbInstance().queryForMap("SELECT * from api_duplicatelead WHERE child_id=? AND parent_id=?", childID, parentID).get(APIDuplicateLead.ID).toString());
    }

    /**
     *
     * @param Id
     * @return
     */
    public String getDeDupeStausFromLead(int Id) {
        return getDbInstance().queryForMap("SELECT * from api_lead WHERE id=?", Id).get(APILead.DEDUP_STATUS).toString();
    }

    /**
     *
     * @param Id
     * @return
     */
    public int getParentFromId(int Id) {
        return Integer.parseInt(getDbInstance().queryForMap("SELECT * from api_lead WHERE id=?", Id).get(APILead.PARENT_ID).toString());
    }

    /**
     *
     * @return
     */
    public int getLeadTableSize(){
        return getDbInstance().queryForList("SELECT * FROM api_lead").size();
    }

    /**
     *
     * @param leadId
     * @return
     */
    public boolean isLeadExists(int leadId) {
        return getDbInstance().queryForList("SELECT * FROM api_lead WHERE id=" + leadId).size() == 0;
    }

    /**
     *
     * @param leadId
     * @return
     */
    public boolean isDuplicateLeadExists(int leadId) {
        return getDbInstance().queryForList("SELECT * FROM api_duplicatelead WHERE id=" + leadId).size() == 0;
    }

    /**
     *
     * @param dedupStatus
     * @return
     */
    public int getLeadCount(String dedupStatus){
        return getDbInstance().queryForList("SELECT * FROM api_lead WHERE dedup_status='" + dedupStatus + "'").size();
    }

    /**
     *
     * @param leadId
     * @param dedupStatus
     * @return
     */
    public boolean verifyDeDupStatus(int leadId, String dedupStatus){
        return DBHelper.pollDB(LMS.LMS_TEMPLATE, "SELECT * from api_lead where id=" + leadId, APILead.DEDUP_STATUS, String.valueOf(dedupStatus), 5, 60);
    }

    /**
     *
     * @return
     */
    public SqlTemplate getDbInstance(){
        return db.getMySqlTemplate(LMS.LMS_TEMPLATE);
    }

    public void deleteBulkUploadLeads(){
        getDbInstance().execute("DELETE FROM api_businesshour where lead_id IN ( SELECT id FROM api_lead WHERE name IN (" + LeadsName.NAMES + ") )");
        //getDbInstance().execute("DELETE FROM api_duplicatelead where child_id IN (" + LeadsName.NAMES + ") OR parent_id IN (" + LeadsName.NAMES + ")");

        getDbInstance().execute("DELETE FROM api_duplicatelead where " +
                "child_id IN ( SELECT id FROM api_lead WHERE name IN (" + LeadsName.NAMES + ") ) " +
                "OR " +
                "parent_id IN (SELECT id FROM api_lead WHERE name IN (" + LeadsName.NAMES + ") )");

        getDbInstance().execute("DELETE FROM api_lead where name IN (" + LeadsName.NAMES + ")");
    }

    /**
     *
     * @param name
     * @return
     */
    public int countOfLeadsWithName(String name){
        return getDbInstance().queryForList("SELECT * FROM api_lead WHERE name IN (" + name + ")").size();
    }

    /**
     *
     * @param Id
     * @param col
     * @return
     */
    public Object getValuesFromLeadTable(int Id, String col){
        return getDbInstance().queryForMap("SELECT * from api_lead WHERE id=?", Id).get(col);
    }
}