package com.swiggy.api.dash.lms.pojos;
import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;

public class BusinessHoursList {

    private List<BusinessHours> businessHours = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public BusinessHoursList() {
    }

    /**
     *
     * @param businessHours
     */
    public BusinessHoursList(List<BusinessHours> businessHours) {
        super();
        this.businessHours = businessHours;
    }

    public List<BusinessHours> getBusinessHours() {
        return businessHours;
    }

    public BusinessHoursList setBusinessHours(List<BusinessHours> businessHours) {
        this.businessHours = businessHours;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("businessHours", businessHours).toString();
    }
}