package com.swiggy.api.dash.Delivery.serviceability;


import com.google.gson.JsonObject;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.Availability.helper.CreateStoresForAvailablity;
import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;
import com.swiggy.api.dash.Delivery.serviceability.helper.MapHelper;
import com.swiggy.api.dash.Delivery.serviceability.helper.Mapconstant;
import com.swiggy.api.dash.Delivery.serviceability.models.*;
import com.swiggy.api.dash.kms.pojos.CategoryBuilder;
import com.swiggy.api.dash.kms.pojos.Tags;
import com.swiggy.api.erp.ff.constants.createTaskAPI.MapConstants;
import com.tectonica.core.Hash;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
//import org.codehaus.groovy.runtime.powerassert.SourceText;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.apache.commons.lang3.RandomStringUtils.*;

public class MapDp  {
    MapHelper mapHelper=new MapHelper();





    @DataProvider(name = "searchData",parallel = false)
    public Object[][] mapdataProvider() {
        return new Object[][] {
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,randomAlphabetic(10),Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,400,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,"10",Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,"10000",Mapconstant.DEFAULT_SIZE,1,400,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,"1000",0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,"1001","1002",0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,"12.928842", "77.627901",Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,"-1",Mapconstant.DEFAULT_SIZE,1,400,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,1,400,"0"},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,1,400,"-1"},
                {"5","12.928842", "77.627901",Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_TAGID,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},
                {Mapconstant.CITY_ID,Mapconstant.LAT1,Mapconstant.LNG1,Mapconstant.NAME_SUBCATEGORY,Mapconstant.TAG_1,Mapconstant.DEFAULT_FROM,Mapconstant.DEFAULT_SIZE,0,200,Mapconstant.DEFAULT_MAXDISTANCE},

        };
    }
    @DataProvider(name = "searchDatawithnullvalue",parallel = true)
    public Object[][] mapdataProviderWith() {
        return new Object[][] {
                {Integer.parseInt(Mapconstant.CITY_ID),Double.parseDouble(Mapconstant.LAT1),Double.parseDouble(Mapconstant.LNG1),Mapconstant.NAME_CATEGORY,1,Integer.parseInt(Mapconstant.DEFAULT_FROM),Integer.parseInt(Mapconstant.DEFAULT_SIZE),1,400,Integer.parseInt(Mapconstant.DEFAULT_MAXDISTANCE)},
                {Integer.parseInt(Mapconstant.CITY_ID),Double.parseDouble(Mapconstant.LAT1),Double.parseDouble(Mapconstant.LNG1),null,1,Integer.parseInt(Mapconstant.DEFAULT_FROM),Integer.parseInt(Mapconstant.DEFAULT_SIZE),1,400,Integer.parseInt(Mapconstant.DEFAULT_MAXDISTANCE)},

        };
    }
    @DataProvider(name = "searchDataFroSchemaValidation",parallel = false)
    public Object[][] mapdataProviderForSchema() {
        return new Object[][]{
                {Mapconstant.CITY_ID, Mapconstant.LAT1, Mapconstant.LNG1, Mapconstant.NAME_CATEGORY,Mapconstant.TAG_1, Mapconstant.DEFAULT_FROM, Mapconstant.DEFAULT_SIZE, 0, 200, Mapconstant.DEFAULT_MAXDISTANCE}
        };
    }
    @DataProvider(name = "searchDatacompareWithESData",parallel = false)
    public Object[][] mapdata() throws Exception{
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();



        Left l1;
        Left l2;
        Right r1;
        Right r2;
        Condition c=new Condition();
        Left l_1=new Left(Mapconstant.NAME_CATEGORY,26);//Pharmacy
        Left l_2=new Left(Mapconstant.NAME_SUBCATEGORY,24);//medicine
        Right r_3=new Right(Mapconstant.NAME_SUBCATEGORY,24);//medicine

        Right r_1=new Right(Mapconstant.NAME_CATEGORY,29);//Supermarkets
        Right r_2=new Right(Mapconstant.NAME_SUBCATEGORY,23);//toys


        HashMap<String,Integer> categoriesId=Mapconstant.getCategoryIds();
        HashMap<String,Integer> subCategoriesId=Mapconstant.getSubCategoryIds();
        HashMap<String,Integer> tagId=Mapconstant.getTagsId();

        BoolQueryBuilder query = QueryBuilders.boolQuery();
        QueryBuilder qb1;
        QueryBuilder qb2;
        QueryBuilder qb3;
        CreateStoresForAvailablity createStoresForAvailablity=new CreateStoresForAvailablity();
        ArrayList<String> storeList = new ArrayList<>();
        storeList.add("Store1");
        storeList.add("Store2");
        storeList.add("Store3");
        storeList.add("Store4");
        storeList.add("Store5");
        storeList.add("Store6");
        storeList.add("Store7");
        HashMap<String,Integer> storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);

        List<CategoryBuilder> categories_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Bakery") , "Bakery")).returnListOfCategories();
        List<CategoryBuilder> categories_2 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Liquor stores") , "Liquor stores")).addNewCategory(new CategoryBuilder(categoriesId.get("Supermarkets"),"Supermarkets")).returnListOfCategories();
        List<CategoryBuilder> categories_3 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Liquor stores") , "Liquor stores")).addNewCategory(new CategoryBuilder(categoriesId.get("Supermarkets"),"Supermarkets")).addNewCategory(new CategoryBuilder(categoriesId.get("Grocery store"),"Grocery store")).addNewCategory(new CategoryBuilder(categoriesId.get("Fruit shops"),"Fruit shops")).returnListOfCategories();
        List<CategoryBuilder> categories_4 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Fruit shops") , "Fruit shops")).returnListOfCategories();
        List<CategoryBuilder> categories_5 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Supermarkets") , "Supermarkets")).addNewCategory(new CategoryBuilder(categoriesId.get("Bakery"),"Bakery")).addNewCategory(new CategoryBuilder(categoriesId.get("Vegetables shop"),"Vegetables shop")).returnListOfCategories();
        List<CategoryBuilder> categories_6 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Bakery") , "Bakery")).returnListOfCategories();
        List<CategoryBuilder> categories_7 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Vegetables shop") , "Vegetables shop")).returnListOfCategories();
        List<CategoryBuilder> categories_8 = new CategoryBuilder().addNewCategory( new CategoryBuilder(categoriesId.get("Liquor stores") , "Liquor stores")).returnListOfCategories();


        List<Tags> subCategory_1 = new Tags().addNewTag( new Tags(subCategoriesId.get("Whisky") , "Whisky")).returnSubListOfTags();
        List<Tags> subCategory_2 = new Tags().addNewTag( new Tags(subCategoriesId.get("Birthdaygift") , "Birthdaygift")).returnSubListOfTags();
        List<Tags> subCategory_3 = new Tags().addNewTag( new Tags(subCategoriesId.get("fruit") , "fruit")).returnSubListOfTags();
        List<Tags> subCategory_4 = new Tags().addNewTag( new Tags(subCategoriesId.get("Bread") , "Bread")).returnSubListOfTags();
        List<Tags> subCategory_5 = new Tags().addNewTag( new Tags(subCategoriesId.get("Whisky") , "Whisky")).returnSubListOfTags();
        List<Tags> subCategory_6 = new Tags().addNewTag( new Tags(subCategoriesId.get("Samsung phones") , "Samsung phones")).returnSubListOfTags();

        List<Tags> tag_1 = new Tags().addNewTag( new Tags(tagId.get("50% off on samsungphone") , "50% off on samsungphone")).returnSubListOfTags();



        createStoresForAvailablity.createStoreForMaps(storeList.get(0),"28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_1,subCategory_6,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("More supermarket","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_2,subCategory_4,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Kuldeep wines","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_3,subCategory_1,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Hypercity","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_3,subCategory_2,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Fruit shop","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_4,subCategory_3,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Dmart","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_5,subCategory_6,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Kuldeep bakery","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_6,subCategory_6,tag_1,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps("Kuldeep vegetables","28.435892,77.004015",dashAvailabilityHelper.createArea(3,"Hudacity"),"Hudacity",3,"Gurgaon",1,categories_7,subCategory_6,tag_1,storeAvailibility.get(storeList.get(0)));


        //Data -1 sub Whisky AND  category Liquor stores
        Condition cData1=new Condition(new Left(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("Whisky")),new Right(Mapconstant.NAME_CATEGORY,categoriesId.get("Liquor stores")),"AND");
        qb1 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("Whisky"));
        qb2 = QueryBuilders.termQuery(Mapconstant.NAME_CATEGORY, categoriesId.get("Liquor stores"));
        query = query.filter(qb1).filter(qb2);
        BoolQueryBuilder bQueryBuilder_1=query.filter(QueryBuilders.geoDistanceQuery("latLong").distance(20000, DistanceUnit.METERS).point(28.45243032, 77.00496384));
        System.out.println(bQueryBuilder_1);

        //Data -2 sub BirthdayGift AND  sub  Fruit
        query = QueryBuilders.boolQuery();
        Condition cData2=new Condition(new Left(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("Birthdaygift")),new Right(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("fruit")),"AND");
        qb1 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("Birthdaygift"));
        qb2 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("fruit"));
        query = query.filter(qb1).filter(qb2);
        BoolQueryBuilder bQueryBuilder_2=query.filter(QueryBuilders.geoDistanceQuery("latLong").distance(20000, DistanceUnit.METERS).point(28.45243032, 77.00496384));
        System.out.println(bQueryBuilder_2);


        //Data -3 sub BirthdayGift OR  sub  Fruit
        query = QueryBuilders.boolQuery();
        Condition cData3=new Condition(new Left(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("Birthdaygift")),new Right(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("fruit")),"OR");
        qb1 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("Birthdaygift"));
        qb2 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("fruit"));
        query = query.filter(QueryBuilders.boolQuery().should(qb1).should(qb2));
        BoolQueryBuilder bQueryBuilder_3=query.filter(QueryBuilders.geoDistanceQuery("latLong").distance(20000, DistanceUnit.METERS).point(28.45243032, 77.00496384));
        System.out.println(bQueryBuilder_3);


        //Data -5 (Organic store && vegetables store)) AND (SuperMarket)
        query = QueryBuilders.boolQuery();
         l1=new Left(Mapconstant.NAME_CATEGORY,categoriesId.get("Bakery"));
         r1=new Right(Mapconstant.NAME_CATEGORY,categoriesId.get("Vegetables shop"));
        Condition cData5=new Condition(new Left(l1,r1,"AND"),new Right(Mapconstant.NAME_CATEGORY,categoriesId.get("Supermarkets")),"AND");
        qb1 = QueryBuilders.termQuery(Mapconstant.NAME_CATEGORY, categoriesId.get("Bakery"));
        qb2 = QueryBuilders.termQuery(Mapconstant.NAME_CATEGORY, categoriesId.get("Vegetables shop"));
        qb3 = QueryBuilders.termQuery(Mapconstant.NAME_CATEGORY, categoriesId.get("Supermarkets"));
        query = query.filter(QueryBuilders.boolQuery().filter(QueryBuilders.boolQuery().filter(qb1).filter(qb2)).filter(qb3));
        //query=query.filter(qb3);
        BoolQueryBuilder bQueryBuilder_5=query.filter(QueryBuilders.geoDistanceQuery("latLong").distance(20000, DistanceUnit.METERS).point(28.45243032, 77.00496384));
        System.out.println(bQueryBuilder_5);


        //Data -4 (Category Supermarkets AND  sub  Fruit) AND (BirthdayGift)
        query = QueryBuilders.boolQuery();
        l1=new Left(Mapconstant.NAME_CATEGORY,categoriesId.get("Supermarkets"));
        r1=new Right(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("fruit"));
        Condition cData4=new Condition(new Left(l1,r1,"AND"),new Right(Mapconstant.NAME_SUBCATEGORY,subCategoriesId.get("Birthdaygift")),"AND");
        qb1 = QueryBuilders.termQuery(Mapconstant.NAME_CATEGORY, categoriesId.get("Supermarkets"));
        qb2 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("fruit"));
        qb3 = QueryBuilders.termQuery(Mapconstant.NAME_SUBCATEGORY, subCategoriesId.get("Birthdaygift"));
        query = query.filter(QueryBuilders.boolQuery().filter(QueryBuilders.boolQuery().filter(qb1).filter(qb2)).filter(qb3));
        BoolQueryBuilder bQueryBuilder_4=query.filter(QueryBuilders.geoDistanceQuery("latLong").distance(20000, DistanceUnit.METERS).point(28.45243032, 77.00496384));
        System.out.println(bQueryBuilder_4);
        return new Object[][]{


                {3, 28.45243032, 77.00496384, cData1, 0, 10, 0, 200, 20000,mapHelper.fetchDataFromElasticSearch(1, 28.45243032, 77.00496384,0,10,bQueryBuilder_1.toString())},
                {3, 28.45243032, 77.00496384, cData2, 0, 10, 0, 200, 20000,mapHelper.fetchDataFromElasticSearch(1, 28.45243032, 77.00496384,0,10,bQueryBuilder_2.toString())},
                {3, 28.45243032, 77.00496384, cData3, 0, 10, 0, 200, 20000,mapHelper.fetchDataFromElasticSearch(1, 28.45243032, 77.00496384,0,10,bQueryBuilder_3.toString())},
                {3, 28.45243032, 77.00496384, cData4, 0, 10, 0, 200, 20000,mapHelper.fetchDataFromElasticSearch(1, 28.45243032, 77.00496384,0,10,bQueryBuilder_4.toString())},


        };
    }



}
