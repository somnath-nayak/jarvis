package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.swiggy.api.dash.Tripmanager.helper.TaskStatus;
import com.swiggy.api.dash.Tripmanager.helper.TaskType;

import java.io.Serializable;
import java.util.Date;

public class TaskBo {
        private Long id;
        private Boolean inProgress = false;
        private Boolean canComplete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getInProgress() {
        return inProgress;
    }

    public void setInProgress(Boolean inProgress) {
        this.inProgress = inProgress;
    }

    public Boolean getCanComplete() {
        return canComplete;
    }

    public void setCanComplete(Boolean canComplete) {
        this.canComplete = canComplete;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public Long getJobLegId() {
        return jobLegId;
    }

    public void setJobLegId(Long jobLegId) {
        this.jobLegId = jobLegId;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public JsonNode getMetaData() {
        return metaData;
    }

    public void setMetaData(JsonNode metaData) {
        this.metaData = metaData;
    }

    public Long getTripId() {
        return tripId;
    }

    public void setTripId(Long tripId) {
        this.tripId = tripId;
    }

    public Integer getPay() {
        return pay;
    }

    public void setPay(Integer pay) {
        this.pay = pay;
    }

    public Integer getCollect() {
        return collect;
    }

    public void setCollect(Integer collect) {
        this.collect = collect;
    }

    public Integer getBill() {
        return bill;
    }

    public void setBill(Integer bill) {
        this.bill = bill;
    }

    public Integer getItemCount() {
        return itemCount;
    }

    public void setItemCount(Integer itemCount) {
        this.itemCount = itemCount;
    }

    public Integer getTtrInMins() {
        return ttrInMins;
    }

    public void setTtrInMins(Integer ttrInMins) {
        this.ttrInMins = ttrInMins;
    }

    public Integer getDtrInMtrs() {
        return dtrInMtrs;
    }

    public void setDtrInMtrs(Integer dtrInMtrs) {
        this.dtrInMtrs = dtrInMtrs;
    }

    public Integer getWaitTimeInMins() {
        return waitTimeInMins;
    }

    public void setWaitTimeInMins(Integer waitTimeInMins) {
        this.waitTimeInMins = waitTimeInMins;
    }

    public Date getArrivedTime() {
        return arrivedTime;
    }

    public void setArrivedTime(Date arrivedTime) {
        this.arrivedTime = arrivedTime;
    }

    public Date getCompletedTime() {
        return completedTime;
    }

    public void setCompletedTime(Date completedTime) {
        this.completedTime = completedTime;
    }

    public Long getNextTaskInTrip() {
        return nextTaskInTrip;
    }

    public void setNextTaskInTrip(Long nextTaskInTrip) {
        this.nextTaskInTrip = nextTaskInTrip;
    }

    public Long getNextTaskInJobLeg() {
        return nextTaskInJobLeg;
    }

    public void setNextTaskInJobLeg(Long nextTaskInJobLeg) {
        this.nextTaskInJobLeg = nextTaskInJobLeg;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    private TaskType type;
        private Long jobLegId;
        private TaskStatus status = TaskStatus.CREATED;
        private String createdBy;
        private Date updatedAt;
        private Date createdAt;
        private JsonNode metaData;
        private Long tripId;
        private Integer pay;
        private Integer collect;
        private Integer bill;
        private Integer itemCount;
        private Integer ttrInMins;
        private Integer dtrInMtrs;
        private Integer waitTimeInMins;
        private Date arrivedTime;
        private Date completedTime;
        private Long nextTaskInTrip;
        private Long nextTaskInJobLeg;
        private Long cityId;
}
