package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "name",
        "storeId",
        "quantity",
        "type",
        "itemId",
        "itemVariantId"
})
public class Item {

    @JsonProperty("name")
    private String name;
    @JsonProperty("storeId")
    private String storeId;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("type")
    private String type;
    @JsonProperty("itemId")
    private String itemId;
    @JsonProperty("itemVariantId")
    private String itemVariantId;
    @JsonProperty("variant")
    private String variant;
    @JsonProperty("imageIds")
    private List<Object> imageIds = null;
    @JsonProperty("comment")
    private String comment;
    @JsonProperty()
    private String serviceLine;


    private List<Item> listOfItems = new ArrayList<>();
    /**
     * No args constructor for use in serialization
     *
     */
    public Item() {
    }

    /**
     *
     * @param name
     * @param quantity
     * @param itemId
     * @param type
     * @param itemVariantId
     * @param storeId
     */
    public Item(String name, String storeId, Integer quantity, String type, String itemId, String itemVariantId) {
        super();
        this.name = name;
        this.storeId = storeId;
        this.quantity = quantity;
        this.type = type;
        this.itemId = itemId;
        this.itemVariantId = itemVariantId;

    }

    public Item(String name, String storeId, Integer quantity, String type, String variant, List<Object> imageIds,String comment, String serviceLine) {
        super();
        this.name = name;
        this.storeId = storeId;
        this.quantity = quantity;
        this.type = type;
        this.variant = variant;
        this.imageIds = imageIds;
        this.comment = comment;
        this.serviceLine= serviceLine;
    }


    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Item withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("storeId")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("storeId")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public Item withStoreId(String storeId) {
        this.storeId = storeId;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public Item withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("itemId")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public Item withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("itemVariantId")
    public String getItemVariantId() {
        return itemVariantId;
    }

    @JsonProperty("itemVariantId")
    public void setItemVariantId(String itemVariantId) {
        this.itemVariantId = itemVariantId;
    }

    public Item withItemVariantId(String itemVariantId) {
        this.itemVariantId = itemVariantId;
        return this;
    }

    @JsonProperty("variant")
    public String getVariant() {
        return variant;
    }

    @JsonProperty("variant")
    public void setVariant(String variant) {
        this.variant = variant;
    }

    public Item withVariant(String variant) {
        this.variant = variant;
        return this;
    }

    public String getServiceLine() {
        return serviceLine;
    }

    public Item setServiceLine(String serviceLine) {
        this.serviceLine = serviceLine;
        return this;
    }

    public Item withServiceLine(String serviceLine) {
        this.serviceLine = serviceLine;
        return this;
    }

    @JsonProperty("imageIds")
    public List<Object> getImageIds() {
        return imageIds;
    }

    @JsonProperty("imageIds")
    public void setImageIds(List<Object> imageIds) {
        this.imageIds = imageIds;
    }

    public Item withImageIds(List<Object> imageIds) {
        this.imageIds = imageIds;
        return this;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    public Item withComment(String comment) {
        this.comment = comment;
        return this;
    }


    public Item addItem(Item itemlist) {
        listOfItems.add(itemlist);
        return this;
    }
    public List<Item> returnListOfItems(){
        return listOfItems;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("storeId", storeId).append("quantity", quantity).append("type", type).append("itemId", itemId).append("itemVariantId", itemVariantId).toString();
    }

}