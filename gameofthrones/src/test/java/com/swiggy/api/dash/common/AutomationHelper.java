package com.swiggy.api.dash.common;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.kms.pojos.StoreBuilder;
import org.testng.Reporter;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class AutomationHelper {

    /**
     *
     * @param min Start index
     * @param max End Index
     * @param separator value of separator e.g , # etc
     * @param size number of records
     * @return
     */
    public static String randomNumbersWithSeperator(int min, int max, String separator, int size){
        StringBuffer stringToReturn = new StringBuffer();
        HashSet<Integer> uniqueInts = new HashSet<>();
        while(uniqueInts.size() != size) {
            uniqueInts.add(ThreadLocalRandom.current().nextInt(min, max + 1));
        }
        uniqueInts.forEach(number -> stringToReturn.append(String.valueOf(number)).append(separator));
        return separator.length() > 0 ? stringToReturn.substring(0, (stringToReturn.length() - 1 ) ) : stringToReturn.toString();
    }

    public static String randomSubArrayWithSeperator(String[] array, String separator, int size){
        StringBuffer stringToReturn = new StringBuffer();
        HashSet<String> uniqueInts = new HashSet<>();
        if(size > array.length) {
            return "";
        }
        while(uniqueInts.size() != size) {
            uniqueInts.add(array[ThreadLocalRandom.current().nextInt(array.length)]);
        }
        uniqueInts.forEach(number -> stringToReturn.append(String.valueOf(number)).append(separator));
        return separator.length() > 0 ? stringToReturn.substring(0, (stringToReturn.length() - 1 ) ) : stringToReturn.toString();
    }

    /**
     *
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getDataFromJson(String jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    /**
     *
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getDataFromJson(Object jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    /**
     *
     * @param allStores
     * @return
     */
    public static int sendMeRandomKeyFromHashMap(HashMap<Integer, StoreBuilder> allStores){
        List asList = new ArrayList(allStores.keySet());
        Collections.shuffle(asList);
        return (Integer)asList.get(0);
    }

    /**
     *
     * @return
     */
    public static String GetRandomPhone(String countryCode){
        return  countryCode+String.format("9%03d%03d%03d",
                (int) Math.floor(999*Math.random()),
                (int) Math.floor(999*Math.random()),
                (int) Math.floor(999*Math.random()));
    }
}
