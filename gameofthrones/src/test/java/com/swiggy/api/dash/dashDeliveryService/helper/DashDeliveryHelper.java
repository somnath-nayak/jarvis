package com.swiggy.api.dash.dashDeliveryService.helper;


import com.swiggy.api.dash.SuperAppDeliveryService.helper.Superappdeliveryhelper;
import com.swiggy.api.dash.Tripmanager.helper.JobLegStatus;
import com.swiggy.api.dash.Tripmanager.helper.TripManagerHelper;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.dashDeliveryService.models.*;
import com.swiggy.api.erp.delivery.helper.DeliveryServiceApiHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import io.gatling.commons.stats.assertion.In;
import org.testng.Assert;

import java.time.Instant;
import java.util.*;

public class DashDeliveryHelper {

    HttpCore core = new HttpCore(DashDeliveryConstant.DASHDELIVERY);
    String serviceName = DashDeliveryConstant.SERVICE_NAME;
    JsonHelper jsonHelper = new JsonHelper();
    ObjectMapper mapper = new ObjectMapper();
    Superappdeliveryhelper s=new Superappdeliveryhelper();
    TripManagerHelper t=new TripManagerHelper();
    DashDeliveryDatabaseQueries databaseQueries = new DashDeliveryDatabaseQueries();
    DeliveryServiceApiHelper deliveryServiceApiHelper = new DeliveryServiceApiHelper();

    private Initialize gameofthrones = new Initialize();

    Processor processor=null;

    public static HashMap headers(){

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", DashDeliveryConstant.CONTENT_TYPE_JSON);
        requestHeaders.put("Authorization",DashDeliveryConstant.BASIC_AUTHORIZATION);
        requestHeaders.put("cache-control",DashDeliveryConstant.NO_CACHE_CONTROL);
        return  requestHeaders;
    }

      /*
    Create Job
     */

    public Processor createJob(String payloadReceived){

        String apiname="createJob";
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        processor = new Processor(service, headers(), payloadReceived,0);
        return processor;


    }


    /*
    Update Job
     */


    public Processor updateJob(String payloadReceived){
        String apiname="updateJob";
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        String[] payload = new String[] {payloadReceived};
        processor = new Processor(service, headers(), payloadReceived,0);
        return processor;
    }


     /*
    Cancel Job
     */


    public Processor cancelJob(String payloadReceived){
        String apiname="cancelJob";
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        HashMap<String,String> map = headers();
        map.put("Job-Type","BUY");
        processor = new Processor(service, map, payloadReceived,0);

        return processor;
    }




    /*
    Cart Confirmation
    */

    public Processor cartConfirm(String payloadReceived,String[] queryParam){
        String apiname="cartConfirm";

        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payloadReceived,queryParam,0);

        return processor;
    }

     /*
    Cart confirmation with orderId
     */



    public Processor cartConfirmationUsingJobId(int jobId,int estimatedBill) throws Exception
    {
        String apiname="cartConfirm";

        List<Map<String, Object>> list = getMetaDataFromJobId(jobId);
        int taskId = Integer.parseInt(list.get(0).get("taskId").toString());
        String metaData = list.get(0).get("meta_data").toString();
        ArrayList items = AutomationHelper.getDataFromJson(metaData, "$.items", ArrayList.class);
        List<Item> itemList = mapper.readValue(items.toString(), List.class);
        ConfirmItemCart confirmItemCart = new ConfirmItemCart();
        confirmItemCart.setItems(itemList);
        confirmItemCart.setTaskId(taskId);
        confirmItemCart.setEstimatedBill(100.0);
        String payload = jsonHelper.getObjectToJSON(confirmItemCart);
        String[] requestParameters ={"300",Integer.toString(taskId)};
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payload,requestParameters,0);
        return processor;
    }


    public Processor cartConfirmationUsingJobId(int jobId) throws Exception
    {
        String apiname="cartConfirm";

       // List<Map<String, Object>> list = getMetaDataFromJobId(jobId);
        List<Map<String, Object>> list = getTaskInProgressMetaDataFromJobId(jobId);
        int taskId = Integer.parseInt(list.get(0).get("taskId").toString());
        String metaData = list.get(0).get("meta_data").toString();
        ArrayList items = AutomationHelper.getDataFromJson(metaData, "$.items", ArrayList.class);
        List<Item> itemList = mapper.readValue(items.toString(), List.class);
        ConfirmItemCart confirmItemCart = new ConfirmItemCart();
        confirmItemCart.setItems(itemList);
        confirmItemCart.setTaskId(taskId);
        confirmItemCart.setEstimatedBill(100.0);
        String payload = jsonHelper.getObjectToJSON(confirmItemCart);
        String[] requestParameters ={"300",Integer.toString(taskId)};
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payload,requestParameters,0);
        return processor;
    }

    /*
    Cart confirm for invalid task id
     */
//    public Processor confirmCartForInvalidTaskId(){};
//
//
//
//    /*
//    Cart confirm for task id not in progress
//     */
//    public Processor comfirmCartForTripNotInProgress(int jobId)
//    {
//        Processor p = new Processor();
//
//    }





     /*
    Item Availability
    */


    public Processor itemAvailability(String payloadReceived){
        String apiname="itemAvailability";
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payloadReceived,0);
        return processor;
    }

    /*
    Item Availability with jobId
     */


    public Processor itemConfirmationUsingJobId(int jobId) throws Exception
    {
        String apiname="itemAvailability";
        List<Map<String, Object>> list = getMetaDataFromJobId(jobId);
        int taskId = Integer.parseInt(list.get(0).get("taskId").toString());
        String metaData = list.get(0).get("meta_data").toString();
        ArrayList items = AutomationHelper.getDataFromJson(metaData, "$.items", ArrayList.class);

        /*
        Set isAvailable = true for each item
         */
        Item[] itemArray = mapper.readValue(items.toString(), Item[].class);
        for(int i = 0; i < itemArray.length; i++)
        {
            Item temp;
            temp = itemArray[i];
            temp.setIsAvailable(true);
            itemArray[i]=temp;

        }
        List<Item> itemList = Arrays.asList(itemArray);
        ConfirmItemCart confirmItemCart = new ConfirmItemCart();
        confirmItemCart.setItems(itemList);
        confirmItemCart.setTaskId(taskId);
        String payload = jsonHelper.getObjectToJSON(confirmItemCart);
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payload,0);
        return processor;
    }

    public Processor itemConfirmationUsingJobId(int jobId,HashMap<String,Object> itemAlternatives) throws Exception
    {
        String apiname="itemAvailability";
        List<Map<String, Object>> list = getMetaDataFromJobId(jobId);
        int taskId = Integer.parseInt(list.get(0).get("taskId").toString());
        String metaData = list.get(0).get("meta_data").toString();
        ArrayList items = AutomationHelper.getDataFromJson(metaData, "$.items", ArrayList.class);

        /*
        Set isAvailable = true for each item
         */
        Item[] itemArray = mapper.readValue(items.toString(), Item[].class);
        for(int i = 0; i < itemArray.length; i++)
        {
            Item temp;
            temp = itemArray[i];
            String itemId = temp.getId();
            AlterntativePriceObject alternativesCountAndPrice =(AlterntativePriceObject) itemAlternatives.get(itemId);
            int numberOfAlternativesRequired = alternativesCountAndPrice.getNumberOfAlternatives();
            String priceOfAlternative = alternativesCountAndPrice.getPriceOfAlternatives();
            if(numberOfAlternativesRequired == 0)
                temp.setIsAvailable(true);
            else if(numberOfAlternativesRequired > 0)
            {
                temp.setAlternatives(ItemAlternative.getNNumberOfAlternatives(numberOfAlternativesRequired,priceOfAlternative));
            }
            itemArray[i]=temp;

        }
        List<Item> itemList = Arrays.asList(itemArray);
        ConfirmItemCart confirmItemCart = new ConfirmItemCart();
        confirmItemCart.setItems(itemList);
        confirmItemCart.setTaskId(taskId);
        String payload = jsonHelper.getObjectToJSON(confirmItemCart);
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payload,0);
        return processor;
    }



     /*
    Item Availability with jobId with is available false and no alternatives
     */


    public Processor itemConfirmationUsingJobIdWithIsAvailableAndAlternatives(int jobId, List<ItemAlternative> itemAlternative,boolean isAvailable) throws Exception
    {
        String apiname="itemAvailability";
        List<Map<String, Object>> list = getMetaDataFromJobId(jobId);
        int taskId = Integer.parseInt(list.get(0).get("taskId").toString());
        String metaData = list.get(0).get("meta_data").toString();
        ArrayList items = AutomationHelper.getDataFromJson(metaData, "$.items", ArrayList.class);

        /*
        Set isAvailable = true for each item
         */
        Item[] itemArray = mapper.readValue(items.toString(), Item[].class);
        for(int i = 0; i < itemArray.length; i++)
        {
            Item temp;
            temp = itemArray[i];
            temp.setIsAvailable(isAvailable);
            temp.setAlternatives(itemAlternative);
            itemArray[i]=temp;

        }
        List<Item> itemList = Arrays.asList(itemArray);
        ConfirmItemCart confirmItemCart = new ConfirmItemCart();
        confirmItemCart.setItems(itemList);
        confirmItemCart.setTaskId(taskId);
        String payload = jsonHelper.getObjectToJSON(confirmItemCart);
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(), payload,0);
        return processor;
    }







    /*
    Create a job using order id and return response
     */
    private Processor createJobUsingOrderId(String orderId) throws  Exception
    {


        String createJobPayload = jsonHelper.getObjectToJSON(CreateJob.setDefaultCreateJobData(orderId));
        Processor p1 = createJob(createJobPayload);
        return p1;

    }

    /*
    Get details of job leg
     */
    private Processor getJobLegDetailsFromJobLegId(int jobLegId)
    {
        String apiname="jobLegDetails";
        System.out.println("Job Leg id received is "+jobLegId);

        String [] payloadParam = new String[]{Integer.toString(jobLegId)};
        GameOfThronesService service = new GameOfThronesService(serviceName, apiname, gameofthrones);
        Processor processor = new Processor(service, headers(),null ,payloadParam,0);
        return processor;
    }

    /*
    Get task details from Job leg id
     */
    public List<Task> getTaskDetailsFromJobLegId(int jobLegId)
    {
        Processor p1 = getJobLegDetailsFromJobLegId(jobLegId);
        String responseBody = p1.ResponseValidator.GetBodyAsText().toString();
        List<Task> tasks=AutomationHelper.getDataFromJson(responseBody,"$.data[0].tasks",ArrayList.class);

        System.out.println("Tasks for the job are"+tasks);
        return tasks;

    }

    /*
    Get Items List from jobLegId
    NOTE: will be helpful if we have only one job
     */

    public List<Item> getPickUpItemListFromJobLegId(int jobLegId)
    {
        Processor p1 = getJobLegDetailsFromJobLegId(jobLegId);
        String responseBody = p1.ResponseValidator.GetBodyAsText().toString();
        List<Item> items=AutomationHelper.getDataFromJson(responseBody,"$.data[0].tasks[0].metaData.items",ArrayList.class);

        System.out.println("List of Items"+items);
        return items;

    }


    public CreateJob createJobObject(JobType jobType, String orderId, int sla, List<Task> task, MetaDataforJob metaData,int areaId,int cityId,int serviceLineId){

        CreateJob createJob = new CreateJob();
        if(jobType!=null) {

            createJob.setJobType(jobType);
            createJob.setMetaData(metaData);
            createJob.setOrderId(orderId);
            createJob.setTasks(task);
            createJob.setSlaInMins(sla);
            createJob.setAreaId(areaId);
            createJob.setCityId(cityId);
            createJob.setServiceLineId(serviceLineId);


        }
        return createJob;



    }


    public Task createTaskObject(TaskType taskType, Address address,MetaData metaData, double pay, double bill, int waitTime ){

        Task task=new Task();
        task.setType(taskType);
        task.setAddress(address);
        task.setBill(bill);
        task.setMetaData(metaData);
        task.setPay(pay);
        task.setBill(bill);
        task.setWaitTime(waitTime);

        return task;
    }


    public Address createAddressObjectForTask(String location,String name,String primaryContact, String addressLine){


       Address address = new Address();
       address.setAddressLine(addressLine);
       address.setLocation(location);
       address.setName(name);
       address.setPrimaryContact(primaryContact);

        return address;
    }

    public MetaData createMetaDataForTask(int billTolerance,int payTolerance,int collectTolerance,String deliveryCartConfirmationStatus,List<Item> items,String itemConfirmationCallbackUrl,String cartConfirmationCallbackUrl,Attachment attachment){

        MetaData metaData = new MetaData();
        metaData.setAttachment(attachment);
        metaData.setPayTolerance(payTolerance);
        metaData.setBillTolerance(billTolerance);
        metaData.setCartConfirmationCallbackUrl(cartConfirmationCallbackUrl);
        metaData.setCollectTolerance(collectTolerance);
        metaData.setDeliveryCartConfirmationStatus(deliveryCartConfirmationStatus);
        metaData.setItems(items);
        metaData.setItemConfirmationCallbackUrl(itemConfirmationCallbackUrl);

        return metaData;

    }
    public Attachment createAttachmentForMetaData(List<String> imageUrls,String description)
    {
        Attachment attachment = new Attachment();
        attachment.setDescription(description);
        attachment.setImageUrls(imageUrls);

        return attachment;
    }

    public Item createItemsForTaskMetaData(String id,String name,int quantity,List<String> imageUrls,List<Variant> variants)
    {

        Item item = new Item();
        item.setimageUrls(imageUrls);
        item.setName(name);
        item.setQuantity(quantity);
        item.setVariants(variants);
        item.setId(id);

        return item;

    }

    public Variant createVariantsForItem(String name)
    {
        Variant variant = new Variant();
        variant.setName(name);
        return variant;
    }

    public MetaDataforJob createMetaDataForJob(String customerNumber,long orderedTimeInSec)
    {
        MetaDataforJob metaDataforJob = new MetaDataforJob();
        metaDataforJob.setCustomerNumber(customerNumber);
        metaDataforJob.setOrderedTimeInSec(orderedTimeInSec);
        return metaDataforJob;
    }


    private ConfirmItem createItemConfirmationObject(int taskId,List<ItemsToConfirm> items)
    {
        ConfirmItem item= new ConfirmItem();
        item.setItems(items);
        item.settaskId(taskId);
        return item;
    }

    /*
    Generate Random Order Id
     */
    public String generateOrderId()
    {
        Random rnd = new Random();
        String n = Integer.toString((100000 + rnd.nextInt(900000)));
        return n;
    }
    /*
    Get Meta data List from Job Id
     */
    public List<Map<String, Object>> getMetaDataFromJobId(int jobId)
    {
        databaseQueries.setGetMetaDataFromJobId(jobId);
        String query = databaseQueries.getMetaDataFromJobId;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DashDeliveryConstant.TRIP_MANAGER_DB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return list;
    }
    public List<Map<String, Object>> getTaskInProgressMetaDataFromJobId(int jobId)
    {

        String query = databaseQueries.getTaskInProgressAndMetaDataFromJobId(jobId);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DashDeliveryConstant.TRIP_MANAGER_DB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return list;
    }


    public int getTaskInProgressFromJobId(int jobId)
    {

        String query = databaseQueries.getTaskIdInProgress(jobId);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(DashDeliveryConstant.TRIP_MANAGER_DB);
        int taskId = sqlTemplate.queryForObjectInteger(query,null,null);
        return taskId;
    }

    /*
    Set Job Id for new orders
     */
    public int getJobId(String createOrderPayload)
    {
        int jobId=AutomationHelper.getDataFromJson(createOrderPayload,"$.data.id",Integer.class);
        return jobId;
    }


    public String checkIfAlterantivesPresent(int jobId) {

        List<Map<String,Object>> list = getMetaDataFromJobId(jobId);
        String metaData = list.get(0).get("meta_data").toString();
        return metaData;


    }

    public void loginDEAndConfirmOrder(int jobId) throws Exception
    {

        s.loginDeliveryExecutie((long)(DashDeliveryConstant.TEST_DE_ID));
        t.assigntripToDeByJobId(jobId,DashDeliveryConstant.TEST_DE_ID);
        t.updatejobLegStatusByJobId(JobLegStatus.CONFIRMED,jobId,DashDeliveryConstant.TEST_DE_ID, com.swiggy.api.dash.Tripmanager.helper.JobType.BUY);
    }

    public static int generateAlternativeItemId()
    {
        Random rnd = new Random();
        int n = 10 + rnd.nextInt(900);
        return n;
    }

    /*
    Create HashMap For item confirmation
     */
    public static HashMap<String,Object> createMockHashMap()
    {
        HashMap<String,Object> hashMap = new HashMap<>();
        AlterntativePriceObject a1 = new AlterntativePriceObject();
        a1.setNumberOfAlternatives(0);
        a1.setPriceOfAlternatives("0");
        hashMap.put("7",a1);

        AlterntativePriceObject a2 = new AlterntativePriceObject();
        a2.setNumberOfAlternatives(2);
        a2.setPriceOfAlternatives("100,200");
        hashMap.put("8",a2);

        return hashMap;

    }

    /*
    Create DE using deliveryServiceApiHelper.createDEandmakeDEActive()
     */

    public String createDEGetDEId()
    {
        String deId=deliveryServiceApiHelper.createDEandmakeDEActive(DashDeliveryConstant.ZONE_ID_87);
        return deId;
    }

    /*
    Check status is available in task meta data
     */
    public boolean checkStatusInTaskMetaData(String metaData,TaskMetaDataStatus status)
    {

        if(metaData.contains(status.toString()))
        {
            return true;
        }
        return false;

    }

    public boolean checkStatusInTaskMetaDataFromJobId(int jobId,TaskMetaDataStatus status)
    {
        List<Map<String, Object>> metaDataDetails = getTaskInProgressMetaDataFromJobId(jobId);
        int flag =0;
        for(int i =0;i<metaDataDetails.size();i++)
        {

            String metaData = metaDataDetails.get(i).get("meta_data").toString();
            if(metaData != null)
            {

               // Assert.assertTrue(checkStatusInTaskMetaData(metaData, status));
                if(!(checkStatusInTaskMetaData(metaData, status)))
                {
                    flag=1;
                    break;
                }
            }
        }

        if(flag == 0)
        {
            return true;
        }
        return false;

    }












}
