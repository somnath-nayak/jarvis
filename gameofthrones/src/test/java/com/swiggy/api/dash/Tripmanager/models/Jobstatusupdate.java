package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.SuperAppDeliveryService.models.Location;
import com.swiggy.api.dash.Tripmanager.helper.Jobstatus;
import lombok.*;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bill",
        "collect",
        "id",
        "pay",
        "source",
        "status",


})
public class Jobstatusupdate {

    @JsonProperty("bill")
    private int bill;
    @JsonProperty("collect")
    private int collect;
    @JsonProperty("id")
    private int id;
    @JsonProperty("pay")
    private int pay;
    @JsonProperty("source")
    private String source="Automation";
    @JsonProperty("status")
    private Jobstatus status;

    public float getId() {
            return id;
        }
    public Jobstatusupdate withId(int id) {
        this.id = id;
        return this;
    }
     public Jobstatus getStatus() {
            return status;
        }
    public Jobstatusupdate withStatus(Jobstatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(Jobstatus status) {
            this.status = status;
    }

    public Jobstatusupdate withId(Integer bill) {
        this.bill = bill;
        return this;
    }
    public float getPay() {
        return pay;
    }
    public float getCollect() {
        return collect;
    }
    public float getBill() {
        return bill;
    }
    public void setBill(int bill) {
        this.bill = bill;
    }

    public void setCollect(int collect) {
        this.collect = collect;
    }
    public void setPay(int pay) {
        this.pay = pay;
    }

    public Jobstatusupdate withPay(int pay){
        this.pay = pay;
        return this;
    }
    public Jobstatusupdate withCollect(int collect){
        this.collect = collect;
        return this;
    }
    public Jobstatusupdate withBill(int bill){
        this.bill = bill;
        return this;
    }







}

