
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name"
})
public class CityBuilder {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    /**
     * No args constructor for use in serialization
     * 
     */
    public CityBuilder() {
    }

    /**
     * 
     * @param id
     * @param enabled
     * @param name
     */
    public CityBuilder(Integer id, String name, Integer enabled) {
        super();
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public CityBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public CityBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public CityBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CityBuilder withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }

}
