package com.swiggy.api.dash.kms.pojos;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.List;

@JsonPropertyOrder({
    "name",
    "lat_long",
    "address",
    "landmark",
    "phone_numbers",
    "image_id",
    "description",
    "active",
    "quality",
    "area",
    "cityBuilder",
    "source",
    "payment_methods",
    "business_hours",
    "categories",
    "sub_categories",
    "tags",
    "is_blacklisted",
    "reason_for_blacklisting",
    "wait_time",
    "close_day",
    "on_ground_verified",
    "commission"
})
public class StoreBuilder {
    @JsonProperty("name")
    private String name;
    @JsonProperty("lat_long")
    private String latLong;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("phone_numbers")
    private String phoneNumbers;
    @JsonProperty("image_id")
    private String imageId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("active")
    private Integer active;
    @JsonProperty("quality")
    private Integer quality;
    @JsonProperty("area")
    private AreaBuilder area;
    @JsonProperty("cityBuilder")
    private CityBuilder cityBuilder;
    @JsonProperty("source")
    private List<Source> source = null;
    @JsonProperty("payment_methods")
    private List<PaymentMethod> paymentMethods = null;
    @JsonProperty("business_hours")
    private List<BusinessHourBuilder> businessHourBuilders = null;
    @JsonProperty("categories")
    private List<CategoryBuilder> categories = null;
    @JsonProperty("sub_categories")
    private List<Tags> subCategories = null;
    @JsonProperty("tags")
    private List<Tags> tags = null;
    @JsonProperty("is_blacklisted")
    private Integer isBlacklisted;
    @JsonProperty("reason_for_blacklisting")
    private ReasonForBlacklisting reasonForBlacklisting;
    @JsonProperty("wait_time")
    private Integer waitTime;
    @JsonProperty("close_day")
    private Integer closeDay;
    @JsonProperty("on_ground_verified")
    private Integer onGroundVerified;
    @JsonProperty("commission")
    private String commission;
    @JsonProperty("is_partner")
    private Integer isPartner=0;

    /**
     * No args constructor for use in serialization
     * 
     */
    public StoreBuilder() {
    }

    /**
     * 
     * @param tags
     * @param waitTime
     * @param closeDay
     * @param phoneNumbers
     * @param onGroundVerified
     * @param isBlacklisted
     * @param cityBuilder
     * @param landmark
     * @param imageId
     * @param area
     * @param subCategories
     * @param source
     * @param address
     * @param businessHourBuilders
     * @param description
     * @param name
     * @param quality
     * @param paymentMethods
     * @param active
     * @param categories
     * @param commission
     * @param reasonForBlacklisting
     * @param latLong
     */
    public StoreBuilder(String name, String latLong, String address, String landmark, String phoneNumbers, String imageId, String description, Integer active, Integer quality, AreaBuilder area, CityBuilder cityBuilder, List<Source> source, List<PaymentMethod> paymentMethods, List<BusinessHourBuilder> businessHourBuilders, List<CategoryBuilder> categories, List<Tags> subCategories, List<Tags> tags, Integer isBlacklisted, ReasonForBlacklisting reasonForBlacklisting, Integer waitTime, Integer closeDay, Integer onGroundVerified, String commission) {
        super();
        this.name = name;
        this.latLong = latLong;
        this.address = address;
        this.landmark = landmark;
        this.phoneNumbers = phoneNumbers;
        this.imageId = imageId;
        this.description = description;
        this.active = active;
        this.quality = quality;
        this.area = area;
        this.cityBuilder = cityBuilder;
        this.source = source;
        this.paymentMethods = paymentMethods;
        this.businessHourBuilders = businessHourBuilders;
        this.categories = categories;
        this.subCategories = subCategories;
        this.tags = tags;
        this.isBlacklisted = isBlacklisted;
        this.reasonForBlacklisting = reasonForBlacklisting;
        this.waitTime = waitTime;
        this.closeDay = closeDay;
        this.onGroundVerified = onGroundVerified;
        this.commission = commission;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public StoreBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public StoreBuilder withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("lat_long")
    public String getLatLong() {
        return latLong;
    }

    @JsonProperty("lat_long")
    public StoreBuilder setLatLong(String latLong) {
        this.latLong = latLong;
        return this;
    }

    public StoreBuilder withLatLong(String latLong) {
        this.latLong = latLong;
        return this;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public StoreBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    public StoreBuilder withAddress(String address) {
        this.address = address;
        return this;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public StoreBuilder setLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    public StoreBuilder withLandmark(String landmark) {
        this.landmark = landmark;
        return this;
    }

    @JsonProperty("phone_numbers")
    public String getPhoneNumbers() {
        return phoneNumbers;
    }

    @JsonProperty("phone_numbers")
    public StoreBuilder setPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        return this;
    }

    public StoreBuilder withPhoneNumbers(String phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
        return this;
    }

    @JsonProperty("image_id")
    public String getImageId() {
        return imageId;
    }

    @JsonProperty("image_id")
    public StoreBuilder setImageId(String imageId) {
        this.imageId = imageId;
        return this;
    }

    public StoreBuilder withImageId(String imageId) {
        this.imageId = imageId;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public StoreBuilder setDescription(String description) {
        this.description = description;
        return this;
    }

    public StoreBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("active")
    public Integer getActive() {
        return active;
    }

    @JsonProperty("active")
    public StoreBuilder setActive(Integer active) {
        this.active = active;
        return this;
    }

    public StoreBuilder withActive(Integer active) {
        this.active = active;
        return this;
    }

    @JsonProperty("quality")
    public Integer getQuality() {
        return quality;
    }

    @JsonProperty("quality")
    public StoreBuilder setQuality(Integer quality) {
        this.quality = quality;
        return this;
    }

    public StoreBuilder withQuality(Integer quality) {
        this.quality = quality;
        return this;
    }

    @JsonProperty("area")
    public AreaBuilder getArea() {
        return area;
    }

    @JsonProperty("area")
    public StoreBuilder setArea(AreaBuilder area) {
        this.area = area;
        return this;
    }

    public StoreBuilder withArea(AreaBuilder area) {
        this.area = area;
        return this;
    }

    @JsonProperty("city")
    public CityBuilder getCityBuilder() {
        return cityBuilder;
    }

    @JsonProperty("city")
    public StoreBuilder setCityBuilder(CityBuilder cityBuilder) {
        this.cityBuilder = cityBuilder;
        return this;
    }

    public StoreBuilder withCity(CityBuilder cityBuilder) {
        this.cityBuilder = cityBuilder;
        return this;
    }

    @JsonProperty("source")
    public List<Source> getSource() {
        return source;
    }

    @JsonProperty("source")
    public StoreBuilder setSource(List<Source> source) {
        this.source = source;
        return this;
    }

    public StoreBuilder withSource(List<Source> source) {
        this.source = source;
        return this;
    }

    @JsonProperty("payment_methods")
    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }

    @JsonProperty("payment_methods")
    public StoreBuilder setPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    public StoreBuilder withPaymentMethods(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    @JsonProperty("business_hours")
    public List<BusinessHourBuilder> getBusinessHourBuilders() {
        return businessHourBuilders;
    }

    @JsonProperty("business_hours")
    public StoreBuilder setBusinessHourBuilders(List<BusinessHourBuilder> businessHourBuilders) {
        this.businessHourBuilders = businessHourBuilders;
        return this;
    }

    public StoreBuilder withBusinessHours(List<BusinessHourBuilder> businessHourBuilders) {
        this.businessHourBuilders = businessHourBuilders;
        return this;
    }

    @JsonProperty("categories")
    public List<CategoryBuilder> getCategories() {
        return categories;
    }

    @JsonProperty("categories")
    public StoreBuilder setCategories(List<CategoryBuilder> categories) {
        this.categories = categories;
        return this;
    }

    public StoreBuilder withCategories(List<CategoryBuilder> categories) {
        this.categories = categories;
        return this;
    }

    @JsonProperty("sub_categories")
    public List<Tags> getSubCategories() {
        return subCategories;
    }

    @JsonProperty("sub_categories")
    public StoreBuilder setSubCategories(List<Tags> subCategories) {
        this.subCategories = subCategories;
        return this;
    }

    public StoreBuilder withSubCategories(List<Tags> subCategories) {
        this.subCategories = subCategories;
        return this;
    }

    @JsonProperty("tags")
    public List<Tags> getTags() {
        return tags;
    }

    @JsonProperty("tags")
    public StoreBuilder setTags(List<Tags> tags) {
        this.tags = tags;
        return this;
    }

    public StoreBuilder withTags(List<Tags> tags) {
        this.tags = tags;
        return this;
    }

    @JsonProperty("is_blacklisted")
    public Integer getIsBlacklisted() {
        return isBlacklisted;
    }

    @JsonProperty("is_blacklisted")
    public StoreBuilder setIsBlacklisted(Integer isBlacklisted) {
        this.isBlacklisted = isBlacklisted;
        return this;
    }

    public StoreBuilder withIsBlacklisted(Integer isBlacklisted) {
        this.isBlacklisted = isBlacklisted;
        return this;
    }

    @JsonProperty("reason_for_blacklisting")
    public ReasonForBlacklisting getReasonForBlacklisting() {
        return reasonForBlacklisting;
    }

    @JsonProperty("reason_for_blacklisting")
    public StoreBuilder setReasonForBlacklisting(ReasonForBlacklisting reasonForBlacklisting) {
        this.reasonForBlacklisting = reasonForBlacklisting;
        return this;
    }

    public StoreBuilder withReasonForBlacklisting(ReasonForBlacklisting reasonForBlacklisting) {
        this.reasonForBlacklisting = reasonForBlacklisting;
        return this;
    }

    @JsonProperty("wait_time")
    public Integer getWaitTime() {
        return waitTime;
    }

    @JsonProperty("wait_time")
    public StoreBuilder setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
        return this;
    }

    public StoreBuilder withWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
        return this;
    }

    @JsonProperty("close_day")
    public Integer getCloseDay() {
        return closeDay;
    }

    @JsonProperty("close_day")
    public StoreBuilder setCloseDay(Integer closeDay) {
        this.closeDay = closeDay;
        return this;
    }

    public StoreBuilder withCloseDay(Integer closeDay) {
        this.closeDay = closeDay;
        return this;
    }

    @JsonProperty("on_ground_verified")
    public Integer getOnGroundVerified() {
        return onGroundVerified;
    }

    @JsonProperty("on_ground_verified")
    public StoreBuilder setOnGroundVerified(Integer onGroundVerified) {
        this.onGroundVerified = onGroundVerified;
        return this;
    }

    public StoreBuilder withOnGroundVerified(Integer onGroundVerified) {
        this.onGroundVerified = onGroundVerified;
        return this;
    }

    @JsonProperty("commission")
    public String getCommission() {
        return commission;
    }

    @JsonProperty("commission")
    public StoreBuilder setCommission(String commission) {
        this.commission = commission;
        return this;
    }
    @JsonProperty("is_partner")
    public Integer getIsPartner() {
        return isPartner;
    }

    @JsonProperty("is_partner")
    public StoreBuilder setIsPartner(Integer isPartner) {
        this.isPartner = isPartner;
        return this;
    }

    public StoreBuilder withCommission(String commission) {
        this.commission = commission;
        return this;
    }

    /*
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("latLong", latLong).append("address", address).append("landmark", landmark).append("phoneNumbers", phoneNumbers).append("imageId", imageId).append("description", description).append("active", active).append("quality", quality).append("area", area).append("cityBuilder", cityBuilder).append("source", source).append("paymentMethods", paymentMethods).append("businessHourBuilders", businessHourBuilders).append("categories", categories).append("subCategories", subCategories).append("tags", tags).append("isBlacklisted", isBlacklisted).append("reasonForBlacklisting", reasonForBlacklisting).append("waitTime", waitTime).append("closeDay", closeDay).append("onGroundVerified", onGroundVerified).append("commission", commission).toString();
    }
    */

}
