package com.swiggy.api.dash.td.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({

        "namespace",
        "header",
        "description",
        "valid_from",
        "valid_till",
        "campaign_type",
        "store_hit",
        "enabled",
        "swiggy_hit",
        "discountLevel",
        "discountCap",
        "createdBy",
        "operationType",
        "freeDeliveryDistanceInKm",
        "storeIds",
        "ruleDiscount",
        "slots",
        "commissionOnFullBill",
        "taxesOnDiscountedBill",
        "firstOrderRestriction",
        "timeSlotRestriction",
        "userRestriction"
})
public class TdView {

    @JsonProperty("id")
    private String id;
    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("store_hit")
    private String storeHit;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("discountCap")
    private Integer discountCap;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("operationType")
    private String operationType;
    @JsonProperty("freeDeliveryDistanceInKm")
    private Double freeDeliveryDistanceInKm;
    @JsonProperty("storeIds")
    private List<Integer> storeIds = null;
    @JsonProperty("ruleDiscount")
    private RuleDiscountView ruleDiscount;
    @JsonProperty("slots")
    private List<SlotView> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;

    /**
     * No args constructor for use in serialization
     *
     */
    public TdView() {
    }

    /**
     *
     * @param ruleDiscount
     * @param timeSlotRestriction
     * @param storeIds
     * @param enabled
     * @param storeHit
     * @param campaignType
     * @param commissionOnFullBill
     * @param freeDeliveryDistanceInKm
     * @param userRestriction
     * @param swiggyHit
     * @param discountCap
     * @param validTill
     * @param header
     * @param namespace
     * @param firstOrderRestriction
     * @param createdBy
     * @param operationType
     * @param slots
     * @param description
     * @param validFrom
     * @param discountLevel
     * @param taxesOnDiscountedBill
     */
    public TdView(String namespace, String header, String description, String validFrom, String validTill, String campaignType, String storeHit, Boolean enabled, String swiggyHit, String discountLevel, Integer discountCap, String createdBy, String operationType, Double freeDeliveryDistanceInKm, List<Integer> storeIds, RuleDiscountView ruleDiscount, List<SlotView> slots, Boolean commissionOnFullBill, Boolean taxesOnDiscountedBill, Boolean firstOrderRestriction, Boolean timeSlotRestriction, Boolean userRestriction) {
        super();
        this.namespace = namespace;
        this.header = header;
        this.description = description;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.campaignType = campaignType;
        this.storeHit = storeHit;
        this.enabled = enabled;
        this.swiggyHit = swiggyHit;
        this.discountLevel = discountLevel;
        this.discountCap = discountCap;
        this.createdBy = createdBy;
        this.operationType = operationType;
        this.freeDeliveryDistanceInKm = freeDeliveryDistanceInKm;
        this.storeIds = storeIds;
        this.ruleDiscount = ruleDiscount;
        this.slots = slots;
        this.commissionOnFullBill = commissionOnFullBill;
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        this.firstOrderRestriction = firstOrderRestriction;
        this.timeSlotRestriction = timeSlotRestriction;
        this.userRestriction = userRestriction;
    }

    public String getId() {
        return id;
    }

    public TdView setId(String id) {
        this.id = id;
        return this;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public TdView setNamespace(String namespace) {
        this.namespace = namespace;
        return this;
    }

    public TdView withNamespace(String namespace) {
        this.namespace = namespace;
        return this;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public TdView setHeader(String header) {
        this.header = header;
        return this;
    }

    public TdView withHeader(String header) {
        this.header = header;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public TdView setDescription(String description) {
        this.description = description;
        return this;
    }

    public TdView withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("valid_from")
    public TdView setValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public TdView withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    @JsonProperty("valid_till")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public TdView setValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    public TdView withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    @JsonProperty("campaign_type")
    public String getCampaignType() {
        return campaignType;
    }

    @JsonProperty("campaign_type")
    public TdView setCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    public TdView withCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    @JsonProperty("store_hit")
    public String getStoreHit() {
        return storeHit;
    }

    @JsonProperty("store_hit")
    public TdView setStoreHit(String storeHit) {
        this.storeHit = storeHit;
        return this;
    }

    public TdView withStoreHit(String storeHit) {
        this.storeHit = storeHit;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public TdView setEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public TdView withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("swiggy_hit")
    public String getSwiggyHit() {
        return swiggyHit;
    }

    @JsonProperty("swiggy_hit")
    public TdView setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
        return this;
    }

    public TdView withSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public TdView setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    public TdView withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("discountCap")
    public Integer getDiscountCap() {
        return discountCap;
    }

    @JsonProperty("discountCap")
    public TdView setDiscountCap(Integer discountCap) {
        this.discountCap = discountCap;
        return this;
    }

    public TdView withDiscountCap(Integer discountCap) {
        this.discountCap = discountCap;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public TdView setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public TdView withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("operationType")
    public String getOperationType() {
        return operationType;
    }

    @JsonProperty("operationType")
    public TdView setOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    public TdView withOperationType(String operationType) {
        this.operationType = operationType;
        return this;
    }

    @JsonProperty("freeDeliveryDistanceInKm")
    public Double getFreeDeliveryDistanceInKm() {
        return freeDeliveryDistanceInKm;
    }

    @JsonProperty("freeDeliveryDistanceInKm")
    public TdView setFreeDeliveryDistanceInKm(Double freeDeliveryDistanceInKm) {
        this.freeDeliveryDistanceInKm = freeDeliveryDistanceInKm;
        return this;
    }

    public TdView withFreeDeliveryDistanceInKm(Double freeDeliveryDistanceInKm) {
        this.freeDeliveryDistanceInKm = freeDeliveryDistanceInKm;
        return this;
    }

    @JsonProperty("storeIds")
    public List<Integer> getStoreIds() {
        return storeIds;
    }

    @JsonProperty("storeIds")
    public TdView setStoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
        return this;
    }

    public TdView withStoreIds(List<Integer> storeIds) {
        this.storeIds = storeIds;
        return this;
    }

    @JsonProperty("ruleDiscount")
    public RuleDiscountView getRuleDiscount() {
        return ruleDiscount;
    }

    @JsonProperty("ruleDiscount")
    public TdView setRuleDiscount(RuleDiscountView ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
        return this;
    }

    public TdView withRuleDiscount(RuleDiscountView ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
        return this;
    }

    @JsonProperty("slots")
    public List<SlotView> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public TdView setSlots(List<SlotView> slots) {
        this.slots = slots;
        return this;
    }

    public TdView withSlots(List<SlotView> slots) {
        this.slots = slots;
        return this;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public TdView setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
        return this;
    }

    public TdView withCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
        return this;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public TdView setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        return this;
    }

    public TdView withTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        return this;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public TdView setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    public TdView withFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public TdView setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
        return this;
    }

    public TdView withTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
        return this;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public TdView setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    public TdView withUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("namespace", namespace).append("header", header).append("description", description).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("storeHit", storeHit).append("enabled", enabled).append("swiggyHit", swiggyHit).append("discountLevel", discountLevel).append("discountCap", discountCap).append("createdBy", createdBy).append("operationType", operationType).append("freeDeliveryDistanceInKm", freeDeliveryDistanceInKm).append("storeIds", storeIds).append("ruleDiscount", ruleDiscount).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("userRestriction", userRestriction).toString();
    }

}
