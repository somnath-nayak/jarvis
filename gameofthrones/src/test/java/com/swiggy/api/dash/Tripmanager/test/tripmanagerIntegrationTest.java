/*
package com.swiggy.api.dash.Tripmanager.test;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.swiggy.api.dash.Tripmanager.helper.*;
import com.swiggy.api.dash.Tripmanager.models.*;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import io.gatling.http.request.builder.Http;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static java.util.Arrays.asList;

public class tripmanagerIntegrationTest {

    JsonHelper helper = new JsonHelper();
    HttpCore core = new HttpCore(TripManagerConstant.TRIPMANAGER);
    JsonHelper jsonHelper=new JsonHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    TripManagerHelper tripManagerHelper=new TripManagerHelper();


    Address defaltRestaurantAddress=tripManagerHelper.createAddressObject(TripManagerConstant.RESTAURANT_NAME,TripManagerConstant.RESTAURANT_PHONE,TripManagerConstant.RESTAURANT_LANDMARK,"",TripManagerConstant.RESTAURANT_ADDRESS,true,TripManagerConstant.RESTAURANT_LATLONG);


    @Test(description = "create job And deliver it")
    public void CreateAJobAndDeliverIt() throws Exception {
        HttpResponse response1 = tripManagerHelper.createJob(JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA, new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, null), tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, null))), null);
        int jobLegId = AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class);
        int tripId = tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class));
        HttpResponse assignResponse = tripManagerHelper.assigntripToDe(tripId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Long lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID, jobLegId);
        HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        //ReadContext ctx = JsonPath.parse(appAssignedResponse);
        //int value = (int) ctx.read("$.data.cards.length()", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        for (int j = 0; j < nooftask; j++) {
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].status", String.class), "CREATED");
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegId", Integer.class), Integer.valueOf(jobLegId));
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegStatus", String.class), JobLegStatus.ASSIGNED.toString());
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobType", String.class), JobType.FOOD.toString());
        }
        HttpResponse appAssignedResponse_updated_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse_updated_1.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_1.getResponseMsg(), "$.data.trips[0].tasks[0].jobLegStatus", String.class), JobLegStatus.CONFIRMED.toString());
        for (int j = 0; j < nooftask; j++) {
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
            if (headtaskId == 0) {
                HttpResponse appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

            }
        }
    }


    @Test(description = "create job Reject from DE and assign again")
    public void CreateAJobDeRejectAndAssignAgain() throws Exception{
            HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
            int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
            int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
            HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
            HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
            Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
            tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
            HttpResponse updateresponse=tripManagerHelper.updatejobLegStatus(JobLegStatus.REJECTED,jobLegId,TripManagerConstant.TEST_DE_ID);
            HttpResponse appAssignedResponseAfterReject=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponseAfterReject.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
            HttpResponse joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);
            Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].status",String.class),JobLegStatus.CREATED.toString());
            assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
            appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
            int headtaskId=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].headTaskId",Integer.class);
            //ReadContext ctx = JsonPath.parse(appAssignedResponse);
            //int value = (int) ctx.read("$.data.cards.length()", Integer.class);
            int nooftask=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.length()",Integer.class);
            for(int j=0;j<nooftask;j++){
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.["+j+"].status",String.class),"CREATED");
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.["+j+"].jobLegId",Integer.class),Integer.valueOf(jobLegId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.["+j+"].jobLegStatus",String.class),JobLegStatus.ASSIGNED.toString());
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.["+j+"].jobType",String.class),JobType.FOOD.toString());
            }
            HttpResponse appAssignedResponse_updated_1=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
            tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
            appAssignedResponse_updated_1=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_1.getResponseMsg(),"$.data.trips[0].tasks[0].jobLegStatus",String.class),JobLegStatus.CONFIRMED.toString());
            for(int j=0;j<nooftask;j++){
                tripManagerHelper.updateTaskStatus(headtaskId,TaskStatus.ARRIVED.toString(),0,0,0,TripManagerConstant.TEST_DE_ID);
                joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].status",String.class),JobLegStatus.CONFIRMED.toString());
                for(int i=0;i<nooftask;i++){
                    int taskId=AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].tasks["+i+"].id",Integer.class);
                    if(taskId==headtaskId){
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].tasks["+i+"].status",String.class), TaskStatus.ARRIVED.toString());
                    }

                }
                tripManagerHelper.updateTaskStatus(headtaskId,TaskStatus.COMPLETED.toString(),0,0,0,TripManagerConstant.TEST_DE_ID);
                joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);

                for(int i=0;i<nooftask;i++){
                    int taskId=AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].tasks["+i+"].id",Integer.class);
                    if(taskId==headtaskId){
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].tasks["+i+"].status",String.class), TaskStatus.COMPLETED.toString());
                    }

                }
                headtaskId=tripManagerHelper.getNextTaskId(headtaskId,appAssignedResponse);
                if(headtaskId==0){
                    HttpResponse appAssignedResponse_updated_2=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                    joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].status",String.class),JobLegStatus.COMPLETED.toString());

                }
            }
    }
    @Test(description = "create job ,Reject after de arrived on first task")
    public void CreateJobRejectAfterDEArrivedOnFirstTask() throws Exception{
        HttpResponse response1 = tripManagerHelper.createJob(JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA, new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, null), tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, null))), null);
        int jobLegId = AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class);
        int tripId = tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class));
        HttpResponse assignResponse = tripManagerHelper.assigntripToDe(tripId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Long lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID, jobLegId);
        HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        //ReadContext ctx = JsonPath.parse(appAssignedResponse);
        //int value = (int) ctx.read("$.data.cards.length()", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        HttpResponse appAssignedResponse_updated_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
        tripManagerHelper.updatejobLegStatus(JobLegStatus.REJECTED, jobLegId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponseAfterReject = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponseAfterReject.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
        joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);
        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].status",String.class),JobLegStatus.CREATED.toString());
        for(int i=0;i<nooftask;i++){
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].tasks["+i+"].status",String.class), TaskStatus.CREATED.toString());
                }


    }
    @Test(description = "create job with single task")
    public void CreateAJobWithSingleTask() throws Exception{
        HttpResponse response1 = tripManagerHelper.createJob(JobType.MISC, "2121", TripManagerConstant.DEFAULT_SLA, new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, null))), null);
        int jobLegId = AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class);
        int tripId = tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class));
        HttpResponse assignResponse = tripManagerHelper.assigntripToDe(tripId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Long lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.length()",Integer.class),Integer.valueOf(1));
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID, jobLegId);
        HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        for (int j = 0; j < nooftask; j++) {
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].status", String.class), "CREATED");
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegId", Integer.class), Integer.valueOf(jobLegId));
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegStatus", String.class), JobLegStatus.ASSIGNED.toString());
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobType", String.class), JobType.MISC.toString());
        }
        HttpResponse appAssignedResponse_updated_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_1.getResponseMsg(), "$.data.trips[0].tasks[0].jobLegStatus", String.class), JobLegStatus.CONFIRMED.toString());
        for (int j = 0; j < nooftask; j++) {
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
            if (headtaskId == 0) {
                HttpResponse appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

            }
        }
    }
    @Test(description = "create job with multiple task")
    public void CreateAJobWithMultipleTask() throws Exception {
        HttpResponse response1 = tripManagerHelper.createJob(JobType.PUDO, "2121", TripManagerConstant.DEFAULT_SLA, new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, null), tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 100, null), tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, null), tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, null))), null);
        int jobLegId = AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class);
        int tripId = tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class));
        HttpResponse assignResponse = tripManagerHelper.assigntripToDe(tripId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Long lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class), Integer.valueOf(4));
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID, jobLegId);
        HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        for (int j = 0; j < nooftask; j++) {
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].status", String.class), "CREATED");
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegId", Integer.class), Integer.valueOf(jobLegId));
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobLegStatus", String.class), JobLegStatus.ASSIGNED.toString());
            Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].jobType", String.class), JobType.PUDO.toString());
        }
        HttpResponse appAssignedResponse_updated_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_1.getResponseMsg(), "$.data.trips[0].tasks[0].jobLegStatus", String.class), JobLegStatus.CONFIRMED.toString());
        for (int j = 0; j < nooftask; j++) {
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
            if (headtaskId == 0) {
                HttpResponse appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

            }
        }
    }
    @Test(description = "Auto reject the job from app")
    public void CreateAJobAutoRejectFromApp() throws Exception {
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        HttpResponse updateresponse=tripManagerHelper.updatejobLegStatus(JobLegStatus.AUTO_REJECTED,jobLegId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponseAfterReject=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponseAfterReject.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
        HttpResponse joblegresponse=tripManagerHelper.getJobLegBulk(jobLegId);
        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(),"$.data[0].status",String.class),JobLegStatus.CREATED.toString());
    }
    @Test(description = "MarkNextTaskArrivedWithoutCompletingFirst")
    public void MarkNextTaskArrivedWithoutCompletingFirst() throws Exception {
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        //ReadContext ctx = JsonPath.parse(appAssignedResponse);
        //int value = (int) ctx.read("$.data.cards.length()", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        HttpResponse appAssignedResponse_updated_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        int nextTaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
        HttpResponse updateResponse=tripManagerHelper.updateTaskStatus(nextTaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
        Assert.assertTrue(AutomationHelper.getDataFromJson(updateResponse.getResponseMsg(),"$.statusMessage",String.class).contains("Task not in progress"));
        joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(0));
        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.CONFIRMED.toString());
        for (int j = 0; j < nooftask; j++) {
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
            if (headtaskId == 0) {
                appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

            }
        }


    }

    @Test(description = "createJobAssignToDeAndUnassignAndTryToConfirm")
    public void createJobAssignToDeAndUnassignAndTryToConfirm() throws Exception{
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        tripManagerHelper.unAssignTripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        Assert.assertTrue(AutomationHelper.getDataFromJson(updateresponse.getResponseMsg(),"$.statusMessage",String.class).contains("Status update not valid"));
        appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));




    }
    @Test(description = "createJobAssignToDeAndUnassignAndTryToReject")
    public void createJobAssignToDeAndUnassignAndTryToReject() throws Exception{
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        tripManagerHelper.unAssignTripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.REJECTED, jobLegId, TripManagerConstant.TEST_DE_ID);
        Assert.assertTrue(AutomationHelper.getDataFromJson(updateresponse.getResponseMsg(),"$.statusMessage",String.class).contains("Status update not valid"));

    }
    @Test(description = "createJobAssignToDeAndUnassignAfterDEConfirmation")
    public void createJobAssignToDeAndUnassignAfterDEConfirmation() throws Exception{
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        tripManagerHelper.unAssignTripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,lastupdatetime);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
    }
    @Test(description = "createJobAssignToDeAndUnassignAfterFirstTaskCompletion")
    public void createJobAssignToDeAndUnassignAfterFirstTaskCompletion() throws Exception{
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].headTaskId",Integer.class);
        tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
        tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
        tripManagerHelper.unAssignTripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.statusCode",Integer.class),Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
    }
    @Test(description = "UpdateTaskStatusCompletedBeforeArrived")
    public void UpdateTaskStatusCompletedBeforeArrived() throws Exception{
        HttpResponse response1=tripManagerHelper.createJob(JobType.FOOD,"2121",TripManagerConstant.DEFAULT_SLA,new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP,defaltRestaurantAddress,0,100,0,null),tripManagerHelper.createTaskObject(TaskType.DROP,defaltRestaurantAddress,0,100,100,null))),null);
        int jobLegId= AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class);
        int tripId=tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(),"$.data.id",Integer.class));
        HttpResponse assignResponse=tripManagerHelper.assigntripToDe(tripId,TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse=tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID,0l);
        Long lastupdatetime=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.lastUpdatedTime",Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID,jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        int headtaskId=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].headTaskId",Integer.class);
        int nooftask=AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks.length()",Integer.class);
        HttpResponse taskupdateResponse=tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
        Assert.assertTrue(AutomationHelper.getDataFromJson(taskupdateResponse.getResponseMsg(),"$.statusMessage",String.class).contains("Status update not valid"));
        for (int j = 0; j < nooftask; j++) {
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
            headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
            if (headtaskId == 0) {
                HttpResponse appAssignedResponse_updated_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse_updated_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                HttpResponse joblegresponse = tripManagerHelper.getJobLegBulk(jobLegId);
                Assert.assertEquals(AutomationHelper.getDataFromJson(joblegresponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

            }
        }
    }

    @Test(description = "create job", dataProviderClass = CreateJobDp.class, dataProvider = "whattoedit",priority = 0)
    public void editTheJobAfterConfirmation(String whatToUpdate) throws Exception {
        //String whatToUpdate="changePickupAddress";
        Item item = new Item().withName("biryani").withQuantity(1).withPrice(1).withUrl("1").withAddons(new ArrayList<>(asList(new Addon().withName("addon").withPrice(1).withQuantity(1)))).withVariants(new ArrayList<>(asList(new Variant().withName("test").withPrice(1))));
        MetaData metaData = new MetaData().withItems(new ArrayList<>(asList(item)));

        HttpResponse response1 = tripManagerHelper.createJob(JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA, new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, metaData), tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 0, metaData))), null);
        int jobLegId = AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class);
        int tripId = tripManagerHelper.getTripIdForJobLeg(AutomationHelper.getDataFromJson(response1.getResponseMsg(), "$.data.id", Integer.class));
        HttpResponse joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
        ArrayList<Integer> taskList = AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[*].id", ArrayList.class);

        HttpResponse assignResponse = tripManagerHelper.assigntripToDe(tripId, TripManagerConstant.TEST_DE_ID);
        HttpResponse appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, 0l);
        Long lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
        tripManagerHelper.ackForJobLeg(TripManagerConstant.TEST_DE_ID, jobLegId);
        joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HttpResponse updateresponse = tripManagerHelper.updatejobLegStatus(JobLegStatus.CONFIRMED, jobLegId, TripManagerConstant.TEST_DE_ID);
        joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
        HashMap<String, Integer> taskMap = tripManagerHelper.getTaskBasedOnType(appAssignedResponse);
        int headtaskId = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].headTaskId", Integer.class);
        int nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);


        switch (whatToUpdate) {
            case "Quantity":
                item = new Item().withName("biryani").withQuantity(2).withPrice(1).withUrl("1").withAddons(new ArrayList<>(asList(new Addon().withName("addon").withPrice(1).withQuantity(1)))).withVariants(new ArrayList<>(asList(new Variant().withName("test").withPrice(1))));
                metaData = new MetaData().withItems(new ArrayList<>(asList(item)));
                tripManagerHelper.editJob(String.valueOf(jobLegId), JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA
                        , new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, metaData, taskMap.get(TaskType.PICK_UP.toString()))
                                , tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, metaData, taskMap.get(TaskType.DROP.toString())))), null);
                appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                //Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks[0].jobLegStatus",String.class),JobLegStatus.CONFIRMED.toString());
                joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);

                for (int j = 0; j < nooftask; j++) {
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].metaData.items[0].quantity", Integer.class), Integer.valueOf(2));

                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
                    if (headtaskId == 0) {
                        appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                        joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

                    }
                }
                break;
            case "addNewItem":
                item = new Item().withName("biryani").withQuantity(1).withPrice(1).withUrl("1").withAddons(new ArrayList<>(asList(new Addon().withName("addon").withPrice(1).withQuantity(1)))).withVariants(new ArrayList<>(asList(new Variant().withName("test").withPrice(1))));
                metaData = new MetaData().withItems(new ArrayList<>(asList(item, item)));
                tripManagerHelper.editJob(String.valueOf(jobLegId), JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA
                        , new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, metaData, taskMap.get(TaskType.PICK_UP.toString())))), null);
                joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                nooftask = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);

                for (int j = 0; j < nooftask; j++) {
                    int taskId = AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].id", Integer.class);
                    if (taskId == taskMap.get(TaskType.PICK_UP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].metaData.items.length()", Integer.class), Integer.valueOf(2));
                    }
                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
                    if (headtaskId == 0) {
                        appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                        joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

                    }
                }


                break;
            case "changePickupAddress":
                Address changeAddress = tripManagerHelper.createAddressObject("Mani biryani", "12312", "near Jyothi nivas", "", "addressline", true, "12.7,77.1");
                tripManagerHelper.editJob(String.valueOf(jobLegId), JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA
                        , new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, changeAddress, 0, 100, 0, metaData, taskMap.get(TaskType.PICK_UP.toString())))), null);
                HttpResponse appAssignedResponseAfterEdit_1 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponseAfterEdit_1.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_BUSY));
                lastupdatetime = AutomationHelper.getDataFromJson(appAssignedResponseAfterEdit_1.getResponseMsg(), "$.data.lastUpdatedTime", Long.class);
                HttpResponse appAssignedResponseAfterEdit_2 = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponseAfterEdit_2.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_NOUPDATE));
                joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);

                for (int j = 0; j < nooftask; j++) {
                    int taskId = AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].id", Integer.class);
                    if (taskId == taskMap.get(TaskType.PICK_UP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].address.name", String.class), "Mani biryani");
                    }
                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                    headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
                    if (headtaskId == 0) {
                        appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                        joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());

                    }

                }
                break;
            case "REMOVEONETASK":
                //Functionality not supported still
                break;
            case "AFTERARRIVEDONPICKUP":
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                item = new Item().withName("biryani").withQuantity(2).withPrice(1).withUrl("1").withAddons(new ArrayList<>(asList(new Addon().withName("addon").withPrice(1).withQuantity(1)))).withVariants(new ArrayList<>(asList(new Variant().withName("test").withPrice(1))));
                metaData = new MetaData().withItems(new ArrayList<>(asList(item)));
                tripManagerHelper.editJob(String.valueOf(jobLegId), JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA
                        , new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.PICK_UP, defaltRestaurantAddress, 0, 100, 0, metaData, taskMap.get(TaskType.PICK_UP.toString()))
                                , tripManagerHelper.createTaskObject(TaskType.DROP, defaltRestaurantAddress, 0, 100, 100, metaData, taskMap.get(TaskType.DROP.toString())))), null);
                appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                //Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks[0].jobLegStatus",String.class),JobLegStatus.CONFIRMED.toString());
                joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                for (int j = 0; j < nooftask; j++) {
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].metaData.items[0].quantity", Integer.class), Integer.valueOf(2));
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.CONFIRMED.toString());
                    int taskId = AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].id", Integer.class);
                    if (taskId == taskMap.get(TaskType.PICK_UP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].status", String.class), TaskStatus.ARRIVED.toString());

                    } else {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].status", String.class), TaskStatus.CREATED.toString());

                    }
                    int taskId_appassignedResponse = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].id", Integer.class);
                    if (taskId_appassignedResponse == taskMap.get(TaskType.PICK_UP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].status", String.class), TaskStatus.ARRIVED.toString());

                    } else {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].status", String.class), TaskStatus.CREATED.toString());

                    }
                }


                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                if (headtaskId == 0) {
                    appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                    joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());
                }

                break;
            case "CHANGEDELIVERYADDRESS_AFTERPICKUP":
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);

                Address deliveryAddressNew = tripManagerHelper.createAddressObject("Mani biryani", "12312", "near Jyothi nivas", "", "addressline", true, "12.7,77.1");
                tripManagerHelper.editJob(String.valueOf(jobLegId), JobType.FOOD, "2121", TripManagerConstant.DEFAULT_SLA
                        , new ArrayList<Task>(asList(tripManagerHelper.createTaskObject(TaskType.DROP, deliveryAddressNew, 0, 100, 0, metaData, taskMap.get(TaskType.DROP.toString())))), null);
                appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                //Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(),"$.data.trips[0].tasks[0].jobLegStatus",String.class),JobLegStatus.CONFIRMED.toString());
                joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                for (int j = 0; j < nooftask; j++) {
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.CONFIRMED.toString());
                    int taskId = AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].id", Integer.class);
                    if (taskId == taskMap.get(TaskType.DROP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].inProgress", Boolean.class), Boolean.valueOf(true));
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].status", String.class), TaskStatus.CREATED.toString());

                    } else {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].inProgress", Boolean.class), Boolean.valueOf(true));
                        Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].tasks[" + j + "].status", String.class), TaskStatus.COMPLETED.toString());

                    }
                    int taskId_appassignedResponse = AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].id", Integer.class);
                    if (taskId_appassignedResponse == taskMap.get(TaskType.DROP.toString())) {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].status", String.class), TaskStatus.CREATED.toString());

                    } else {
                        Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.data.trips[0].tasks[" + j + "].status", String.class), TaskStatus.COMPLETED.toString());

                    }
                }


                headtaskId = tripManagerHelper.getNextTaskId(headtaskId, appAssignedResponse);
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.ARRIVED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                tripManagerHelper.updateTaskStatus(headtaskId, TaskStatus.COMPLETED.toString(), 0, 0, 0, TripManagerConstant.TEST_DE_ID);
                if (headtaskId == 0) {
                    appAssignedResponse = tripManagerHelper.getAssignedJob(TripManagerConstant.TEST_DE_ID, lastupdatetime);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(appAssignedResponse.getResponseMsg(), "$.statusCode", Integer.class), Integer.valueOf(TripManagerConstant.STATUSCODE_FREE));
                    joblegResponse = tripManagerHelper.getJobLegBulk(jobLegId);
                    Assert.assertEquals(AutomationHelper.getDataFromJson(joblegResponse.getResponseMsg(), "$.data[0].status", String.class), JobLegStatus.COMPLETED.toString());
                }

                break;
        }
    }
}
*/
