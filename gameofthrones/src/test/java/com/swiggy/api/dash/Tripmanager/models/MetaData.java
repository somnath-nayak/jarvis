
package com.swiggy.api.dash.Tripmanager.models;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "billTolerance",
    "payTolerance",
    "collectTolerance",
    "QRVerficationRequired",
    "billMismatchTimeoutInSec",
    "noBillBlockerTimeoutInSec",
    "vendorIssueConfig",
    "instruction",
    "items"
})
public class MetaData {

    @JsonProperty("billTolerance")
    private Integer billTolerance=20;
    @JsonProperty("payTolerance")
    private Integer payTolerance=20;
    @JsonProperty("collectTolerance")
    private Integer collectTolerance=30;
    @JsonProperty("QRVerficationRequired")
    private Boolean qRVerficationRequired=true;
    @JsonProperty("billMismatchTimeoutInSec")
    private Integer billMismatchTimeoutInSec=30;
    @JsonProperty("noBillBlockerTimeoutInSec")
    private Integer noBillBlockerTimeoutInSec=20;
    @JsonProperty("vendorIssueConfig")
    private String vendorIssueConfig="New blocker";
    @JsonProperty("instruction")
    private String instruction="Sambhal ke";
    @JsonProperty("items")
    private List<Item> items = null;

    @JsonProperty("billTolerance")
    public Integer getBillTolerance() {
        return billTolerance;
    }

    @JsonProperty("billTolerance")
    public void setBillTolerance(Integer billTolerance) {
        this.billTolerance = billTolerance;
    }

    public MetaData withBillTolerance(Integer billTolerance) {
        this.billTolerance = billTolerance;
        return this;
    }

    @JsonProperty("payTolerance")
    public Integer getPayTolerance() {
        return payTolerance;
    }

    @JsonProperty("payTolerance")
    public void setPayTolerance(Integer payTolerance) {
        this.payTolerance = payTolerance;
    }

    public MetaData withPayTolerance(Integer payTolerance) {
        this.payTolerance = payTolerance;
        return this;
    }

    @JsonProperty("collectTolerance")
    public Integer getCollectTolerance() {
        return collectTolerance;
    }

    @JsonProperty("collectTolerance")
    public void setCollectTolerance(Integer collectTolerance) {
        this.collectTolerance = collectTolerance;
    }

    public MetaData withCollectTolerance(Integer collectTolerance) {
        this.collectTolerance = collectTolerance;
        return this;
    }

    @JsonProperty("QRVerficationRequired")
    public Boolean getQRVerficationRequired() {
        return qRVerficationRequired;
    }

    @JsonProperty("QRVerficationRequired")
    public void setQRVerficationRequired(Boolean qRVerficationRequired) {
        this.qRVerficationRequired = qRVerficationRequired;
    }

    public MetaData withQRVerficationRequired(Boolean qRVerficationRequired) {
        this.qRVerficationRequired = qRVerficationRequired;
        return this;
    }

    @JsonProperty("billMismatchTimeoutInSec")
    public Integer getBillMismatchTimeoutInSec() {
        return billMismatchTimeoutInSec;
    }

    @JsonProperty("billMismatchTimeoutInSec")
    public void setBillMismatchTimeoutInSec(Integer billMismatchTimeoutInSec) {
        this.billMismatchTimeoutInSec = billMismatchTimeoutInSec;
    }

    public MetaData withBillMismatchTimeoutInSec(Integer billMismatchTimeoutInSec) {
        this.billMismatchTimeoutInSec = billMismatchTimeoutInSec;
        return this;
    }

    @JsonProperty("noBillBlockerTimeoutInSec")
    public Integer getNoBillBlockerTimeoutInSec() {
        return noBillBlockerTimeoutInSec;
    }

    @JsonProperty("noBillBlockerTimeoutInSec")
    public void setNoBillBlockerTimeoutInSec(Integer noBillBlockerTimeoutInSec) {
        this.noBillBlockerTimeoutInSec = noBillBlockerTimeoutInSec;
    }

    public MetaData withNoBillBlockerTimeoutInSec(Integer noBillBlockerTimeoutInSec) {
        this.noBillBlockerTimeoutInSec = noBillBlockerTimeoutInSec;
        return this;
    }

    @JsonProperty("vendorIssueConfig")
    public String getVendorIssueConfig() {
        return vendorIssueConfig;
    }

    @JsonProperty("vendorIssueConfig")
    public void setVendorIssueConfig(String vendorIssueConfig) {
        this.vendorIssueConfig = vendorIssueConfig;
    }

    public MetaData withVendorIssueConfig(String vendorIssueConfig) {
        this.vendorIssueConfig = vendorIssueConfig;
        return this;
    }

    @JsonProperty("instruction")
    public String getInstruction() {
        return instruction;
    }

    @JsonProperty("instruction")
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public MetaData withInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    public MetaData withItems(List<Item> items) {
        this.items = items;
        return this;
    }

}
