package com.swiggy.api.dash.Availability.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.kms.helper.kmsHelper;
import com.swiggy.api.dash.kms.pojos.*;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Reporter;

import java.util.List;

public class CreateStoresForAvailablity {
    JsonHelper jsonHelper = new JsonHelper();
    HttpCore core=new HttpCore(AvailabilityConstant.KMS_SERVICE_NAME);
    //Normal
    public void createStore_1_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);
        storeApi(store,storeId);
    }
    //Bussiness hours 24 hours
    public void createStore_2_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{

        List<BusinessHourBuilder> bHours = new BusinessHourBuilder()
                .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 1, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(2, "22:30:00", 1, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(3, "22:30:00", 1, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(4, "22:30:00", 1, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(5, "22:30:00", 1, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(6, "22:30:00", 1, "11:00:00"))
                .returnBusinessHourList();
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, bHours, categories, tags, isActive);
        storeApi(store,storeId);
    }
   //Bussiness hours -open only 2 days
    public void createStore_3_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{

        List<BusinessHourBuilder> bHours = new BusinessHourBuilder()
                .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(6, "22:30:00", 1, "11:00:00"))
                .returnBusinessHourList();
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, bHours, categories, tags, isActive);
        storeApi(store,storeId);
    }
    // check for active store
    public void createStore_4_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);
        storeApi(store,storeId);
       /* DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        dashAvailabilityHelper.createAreaBlackZone(areaId,categories.get(0).getId());
        createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);*/
    }
    // AREA BLACKZONE FOR FIRST CATEGORY
    public void createStore_5_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        dashAvailabilityHelper.createAreaBlackZone(areaId,categories.get(0).getId());
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);
        storeApi(store,storeId);
    }
    //CITY BLACKZONE FOR FIRST CATEGORY
    public void createStore_6_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        dashAvailabilityHelper.createCityBlackzone(cityId,categories.get(0).getId());
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);
        storeApi(store,storeId);
    }
    //ALL CATEGORY AREA BLACKZONE
    public void createStore_7_ForAvailability(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> tags, int storeId) throws Exception{
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        for(CategoryBuilder l:categories){
            dashAvailabilityHelper.createAreaBlackZone(areaId,l.getId());
        }
        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories, tags, isActive);
        storeApi(store,storeId);
    }
//AREA AND CITY BLACKZONE FOR CATEGORY

    public List<BusinessHourBuilder> getNormalBussinessHours(){
        List<BusinessHourBuilder> bHours = new BusinessHourBuilder()
                .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(2, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(3, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(4, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(5, "22:30:00", 0, "11:00:00"))
                .addBusinessHour( new BusinessHourBuilder(6, "22:30:00", 0, "11:00:00"))
                .returnBusinessHourList();
        return bHours;
    }






    public StoreBuilder createStore(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, List<BusinessHourBuilder> bHours, List<CategoryBuilder> categories, List<Tags> tags, int isActive) throws Exception{

        List<Source> storeSources = new Source()
                    .addSource( new Source(3, "GP"))
                    .addSource( new Source(2, "JD"))
                    .addSource( new Source(1, "SW"))
                    .returnListOfSources();
            AreaBuilder area = new AreaBuilder()
                    .setId(areaId)
                    .setName(areaName)
                    .setEnabled(1);
            CityBuilder city = new CityBuilder()
                    .setId(cityId)
                    .setName(cityName);
            List<PaymentMethod> payment = new PaymentMethod()
                    .addPaymentMethod( new PaymentMethod(1, "Cash"))
                    .addPaymentMethod( new PaymentMethod(2, "Credit Card/Debit Card"))
                    .returnListOfPaymentMethods();
            List<Tags> subTags = new Tags()
                    .returnListOfTags();
            ReasonForBlacklisting blockReason = new ReasonForBlacklisting(0 , "Not blacklisted");

            StoreBuilder store =  new StoreBuilder()
                    .setName(storeName)
                    .setLatLong(latlng)
                    .setPhoneNumbers("1212121212")
                    .setImageId("ABCDEFGH")
                    .setActive(isActive)
                    .setQuality(1)
                    .setDescription("Automation Desc with special chars ~!@#$%^&*()_+")
                    .setAddress("Automation Address with special chars ~!@#$%^&*()_+")
                    .setLandmark("Automation Landmark with special chars ~!@#$%^&*()_+")
                    .setWaitTime(3)
                    .setOnGroundVerified(0)
                    .setCommission("[bill]*0.01")
                    .setSource(storeSources).setCloseDay(-1)
                    .setArea(area)
                    .setCityBuilder(city)
                    .setPaymentMethods(payment)
                    .setBusinessHourBuilders(bHours)
                    .setCategories(categories)
                    .setTags(tags)
                    .setSubCategories(subTags)
                    .setIsBlacklisted(0)
                    .setReasonForBlacklisting(blockReason);
        return  store;
    }
    public StoreBuilder createStore(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, List<BusinessHourBuilder> bHours, List<CategoryBuilder> categories,List<Tags> subCategoryList ,List<Tags> tags, int isActive) throws Exception{

        List<Source> storeSources = new Source()
                .addSource( new Source(3, "GP"))
                .addSource( new Source(2, "JD"))
                .addSource( new Source(1, "SW"))
                .returnListOfSources();
        AreaBuilder area = new AreaBuilder()
                .setId(areaId)
                .setName(areaName)
                .setEnabled(1);
        CityBuilder city = new CityBuilder()
                .setId(cityId)
                .setName(cityName);
        List<PaymentMethod> payment = new PaymentMethod()
                .addPaymentMethod( new PaymentMethod(1, "Cash"))
                .addPaymentMethod( new PaymentMethod(2, "Credit Card/Debit Card"))
                .returnListOfPaymentMethods();
        List<Tags> subTags = new Tags()
                .returnListOfTags();
        ReasonForBlacklisting blockReason = new ReasonForBlacklisting(0 , "Not blacklisted");

        StoreBuilder store =  new StoreBuilder()
                .setName(storeName)
                .setLatLong(latlng)
                .setPhoneNumbers("1212121212")
                .setImageId("ABCDEFGH")
                .setActive(isActive)
                .setQuality(1)
                .setDescription("Automation Desc with special chars ~!@#$%^&*()_+")
                .setAddress("Automation Address with special chars ~!@#$%^&*()_+")
                .setLandmark("Automation Landmark with special chars ~!@#$%^&*()_+")
                .setWaitTime(3)
                .setOnGroundVerified(0)
                .setCommission("[bill]*0.01")
                .setSource(storeSources).setCloseDay(-1)
                .setArea(area)
                .setCityBuilder(city)
                .setPaymentMethods(payment)
                .setBusinessHourBuilders(bHours)
                .setCategories(categories)
                .setTags(tags)
                .setSubCategories(subCategoryList)
                .setIsBlacklisted(0)
                .setReasonForBlacklisting(blockReason);
        return  store;
    }


    public void storeApi(StoreBuilder store, int storeId) throws Exception{
       HttpResponse response;

        if(storeId!=-1) {
            response = core.invokePostWithStringJson("update_store", HeaderUtils.getHeaderForKms(KMS.TOKEN),jsonHelper.getObjectToJSONallowNullValues(store), new String[] { String.valueOf(storeId) });
        }
        else{
            response = core.invokePostWithStringJson("create_store", HeaderUtils.getHeaderForKms(KMS.TOKEN),jsonHelper.getObjectToJSONallowNullValues(store));
        }
        DeleteData.AddStoreForDelete(getDataFromJson(response.getResponseMsg(), "$.data.id", Integer.class),store);
    }

    public <T> T getDataFromJson(String jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    public void createStoreForMaps(String storeName, String latlng, int areaId, String areaName, int cityId, String cityName, int isActive, List<CategoryBuilder> categories, List<Tags> subcategory ,List<Tags> tags, int storeId) throws Exception{



        StoreBuilder store=createStore(storeName, latlng, areaId, areaName, cityId, cityName, getNormalBussinessHours(), categories,subcategory, tags, isActive);
        storeApi(store,storeId);
    }








}
