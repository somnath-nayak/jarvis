package com.swiggy.api.dash.orderAuditService.helpers;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.orderAuditService.constants.OAS_CONSTANTS;
import com.swiggy.api.dash.orderAuditService.dp.OASDp;
import com.swiggy.api.dash.orderAuditService.pojos.OrderUpdatePojo;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.KafkaHelper;
import org.apache.http.HttpStatus;
import org.apache.kafka.clients.consumer.Consumer;
import org.testng.Assert;
import java.util.List;
import java.util.Map;

public class OASCommon {
    HttpCore coreAuditChaos = new HttpCore(OAS_CONSTANTS.SERVICE_NAME_CHAOS);
    OASDp dp = new OASDp();
    JsonHelper helper = new JsonHelper();
    KafkaHelper kafka = new KafkaHelper();

    /**
     *
     * @param order
     * @param dbDetails
     */
    public void verifyRequestWithDB(OrderUpdatePojo order, Map<String, Object> dbDetails){
        Assert.assertEquals(order.getOrderId(), dbDetails.get(OASDBManager.ORDER_UPDATE.ORDER_ID));
        Assert.assertEquals(order.getOrderGroupId(), dbDetails.get(OASDBManager.ORDER_UPDATE.ORDER_GROUP_ID));
        Assert.assertEquals(order.getUpdateJson(), dbDetails.get(OASDBManager.ORDER_UPDATE.UPDATE_JSON));
        Assert.assertEquals(order.getUpdateType(), dbDetails.get(OASDBManager.ORDER_UPDATE.UPDATE_TYPE));
        Assert.assertEquals(order.getOrderVersion(), dbDetails.get(OASDBManager.ORDER_UPDATE.ORDER_VERSION));
        Assert.assertEquals(String.valueOf(order.getUpdateTime()), String.valueOf(dbDetails.get(OASDBManager.ORDER_UPDATE.UPDATE_TIME)));
        Assert.assertEquals(order.getUpdatedBy() + "^" + order.getUpdaterType(), dbDetails.get(OASDBManager.ORDER_UPDATE.UPDATED_BY));
    }

    public void mysqlActions(String action){
        HttpResponse stopMysql = coreAuditChaos.invokerPayloadParam(OAS_CONSTANTS.MYSQL, coreAuditChaos.getBasicHeaders(), new String[] { action } );
        Assert.assertEquals(stopMysql.getResponseCode(), HttpStatus.SC_OK,  "Stop Mysql Response is not 200 OK");
    }

    public void publishMessageInKafka(int count) {
        List<OrderUpdatePojo> list = dp.giveMeListOfOrderUpdateJson(count);
        for (OrderUpdatePojo order : list) {
            String orderUpdate;
            try {
                orderUpdate = helper.getObjectToJSON(order);

                /** push order updates in kafka **/
                kafka.runProducer(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_TOPIC_NAME, orderUpdate);
            } catch (Exception e) {
                Assert.fail("Exception : ", e.fillInStackTrace());
            }
        }
    }

    public int consumerCount(String kafkaDb, String topic){
        Consumer<Long, String> consumerStart = kafka.createConsumer(kafkaDb, topic);
        return kafka.runConsumer(consumerStart).count();
    }



}
