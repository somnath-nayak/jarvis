package com.swiggy.api.dash.lms.tests;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.lms.constants.LMS;
import com.swiggy.api.dash.lms.dp.LmsDP;
import com.swiggy.api.dash.lms.helpers.LmsCommon;
import com.swiggy.api.dash.lms.helpers.LmsDBManager;
import com.swiggy.api.dash.lms.pojos.CreateLeadPojo;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.IOException;
import java.util.*;

public class CreateLead {
    JsonHelper helper = new JsonHelper();
    LmsDP lmsDp = new LmsDP();
    HttpCore core = new HttpCore(LMS.SERVICE_NAME);
    LmsDBManager db = new LmsDBManager();
    List<Integer> allLeads = new ArrayList<>();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    LmsCommon lmsHelper = new LmsCommon();
    HashMap<String, String>  headers;
    HashMap<String, String>  formHeaders;
    String tokenFromGetTokenApi;

    @BeforeTest
    public void getHeaders(){
        String apiName = "get_token";
        HttpResponse resp = core.invokerPayloadParam(apiName, headers, new String[] { LMS.USER_NAME, LMS.PASSWORD });
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response of get_token api is not 200 OK");
        String token = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.token", String.class);
        /**
         * Add Auth header in normal call
         */
        headers =  core.getBasicHeaders();
        headers.put("Authorization", "Token "+ token);

        /**
         * Add Auth header in form data call
         */
        formHeaders = core.getFormTypeHeaders();
        formHeaders.put("Authorization", "Token "+ token);

        tokenFromGetTokenApi = token;
    }

    @Test(description = "Create a new lead with same source and id_from_source")
    public void createLeadWithSameSourceAndID(){
        /**
         * Create a lead with same source as 1 and same id as 'A'
         */
        CreateLeadPojo newLeadRequest = lmsDp.giveMeRandomPayloadForCreateLead()
                .setSource(1)
                .setIdFromSource("A")
                .setName("G");

        String newLeadPayload = null;
        try {
            newLeadPayload = helper.getObjectToJSON(newLeadRequest);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
        HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayload);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");

        /**
         * Trying to create same lead, this time lead should not get created
         */
        HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayload);
        Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_BAD_REQUEST, "Status is not 400 Bad Request");
    }

    @Test(description = "Stores with 100% name & 100% phone no match & less than 1000 m")
    public void createDupLeadAutoConditionOne(){
        String newLeadPayloadFirst;
        String newLeadPayloadSec;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_100_NAME_MATCH_1)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);

            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/r");


            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_1000M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_1000M_FROM_SOURCE)
                    .setName(LMS.P_100_NAME_MATCH_2)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_2);

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);

            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 1);
            lmsHelper.verifyParentId(mapSec, leadIdSec, leadIdFirst);
            lmsHelper.verifyDedupStatus(mapSec, "d");
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Stores with 100% name & 100% phone no match & less than 1000 m")
    public void createDupLeadAutoConditionOneMultiplePhones(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_100_NAME_MATCH_1)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/d");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_1000M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_1000M_FROM_SOURCE)
                    .setName(LMS.P_100_NAME_MATCH_2)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_2 + "," + AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);

            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 1);
            lmsHelper.verifyParentId(mapSec, leadIdSec, leadIdFirst);
            lmsHelper.verifyDedupStatus(mapSec, "d");
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Stores with > 90% name match & HV < 50m")
    public void createDupLeadAutoConditionSec(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_100_NAME_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/d");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_50M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_50M_FROM_SOURCE)
                    .setName(LMS.P_90_NAME_MATCH_2)
                    .setPhoneNumber(AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);
            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 1);
            lmsHelper.verifyParentId(mapSec, leadIdSec, leadIdFirst);
            lmsHelper.verifyDedupStatus(mapSec, "d");

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Stores with 100% phone no match & name match > 90% & HV distance < 250m")
    public void createDupLeadAutoConditionThird(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_90_NAME_MATCH_1)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/d");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_LESS_250M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_LESS_250M_FROM_SOURCE)
                    .setName(LMS.P_90_NAME_MATCH_2)
                    .setPhoneNumber(LMS.P_100_PHONE_NO_MATCH_2);

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);
            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 1);
            lmsHelper.verifyParentId(mapSec, leadIdSec, leadIdFirst);
            lmsHelper.verifyDedupStatus(mapSec, "d");

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Store with 50% string match & HV distance less than 1000m HV\n")
    public void createDupLeadManualConditionOne(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_50_NAME_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/d");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_1000M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_1000M_FROM_SOURCE)
                    .setName(LMS.P_50_NAME_MATCH_2)
                    .setPhoneNumber(AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);
            Map<String, Object> mapSec = db.leadDetails(leadIdSec);

            /**
             * 1. Verify DB values with request
             * 2. Verify is_duplicate status in DB
             * 3. check parent Id in DB
             * 4. check DeDup status in DB
             */
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 2);
            lmsHelper.verifyParentId(mapSec, leadIdSec, 0);
            lmsHelper.verifyDedupStatus(mapSec, "n/r");

            /**
             * Get Duplicate Lead Id from DB
             */
            int duplicateLeadId = db.getDuplicateLeadId(leadIdFirst, leadIdSec);

            /**
             * Get Duplicate Leads By ID
             */
            lmsHelper.getDuplicateLeadById(duplicateLeadId, leadIdFirst, leadIdSec);

            /**
             * Get all Duplicate Leads
             */
            lmsHelper.getAllDuplicateLeads(duplicateLeadId);

            /**
             * Mark lead as duplicate Lead
             */
            lmsHelper.markLeadAsDuplicate(leadIdSec, leadIdFirst);

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Stores with > 90% & < 98% name match & HV distance < 250m")
    public void createDupLeadManualConditionTwo(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_90_TO_98_NAME_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/r");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_LESS_250M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_LESS_250M_FROM_SOURCE)
                    .setName(LMS.P_90_TO_98_NAME_MATCH_2)
                    .setPhoneNumber(AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);
            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 2);
            lmsHelper.verifyParentId(mapSec, leadIdSec, 0);
            lmsHelper.verifyDedupStatus(mapSec, "n/r");

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Stores with > 95% name match & HV distance < 50m & different or unavailable phone nos", enabled = false)
    public void createDupLeadManualConditionThird(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_95_ABOVE_NAME_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);
            Map<String, Object> map = db.leadDetails(leadIdFirst);
            lmsHelper.verifyDBWithRequest(map, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(map, 0);
            lmsHelper.verifyParentId(map, leadIdFirst, 0);
            lmsHelper.verifyDedupStatus(map, "n/r");

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_50M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_50M_FROM_SOURCE)
                    .setName(LMS.P_95_ABOVE_NAME_MATCH_2)
                    .setPhoneNumber(AutomationHelper.GetRandomPhone("+91") + "," + AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);
            Map<String, Object> mapSec = db.leadDetails(leadIdSec);
            lmsHelper.verifyDBWithRequest(mapSec, newLeadFirst);
            lmsHelper.verifyIsDuplicateStatus(mapSec, 0);
            lmsHelper.verifyParentId(mapSec, leadIdSec, 0);
            lmsHelper.verifyDedupStatus(mapSec, "n/r");

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "update lead")
    public void updateLead(){
        CreateLeadPojo leadRequest = lmsDp.giveMeRandomPayloadForCreateLead().setName("H");
        CreateLeadPojo leadRequestUpdated = new CreateLeadPojo();
        String leadRequestStr = null;
        String leadRequestStrUpdated = null;
        try {
            leadRequestStr = helper.getObjectToJSON(leadRequest);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
        HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, leadRequestStr);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
        int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
        allLeads.add(leadIdFirst);

        /**
         * Update few keys
         */
        leadRequestUpdated = leadRequest.setName("I")
                .setAddress(UUID.randomUUID().toString());
        try {
            leadRequestStrUpdated = helper.getObjectToJSON(leadRequestUpdated);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }

        HttpResponse respUpdate = core.invokePostWithStringJson(LMS.UPDATE_LEAD, headers, leadRequestStrUpdated, new String[] { String.valueOf(leadIdFirst) } );
        Assert.assertEquals(respUpdate.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK");

        Map<String, Object> map = db.leadDetails(leadIdFirst);
        /**
         * Verify DB details with request payload
         */
        lmsHelper.verifyDBWithRequest(map, leadRequestUpdated);
    }

    @Test(description = "update lead with same source and id_from_source")
    public void updateLeadWithSameSourceAndSourceID(){
        CreateLeadPojo leadRequest = lmsDp.giveMeRandomPayloadForCreateLead()
                .setIdFromSource("Z")
                .setSource(1)
                .setName("J");
        CreateLeadPojo leadRequestUpdated = new CreateLeadPojo();
        String leadRequestStr = null;
        String leadRequestStrUpdated = null;
        try {
            leadRequestStr = helper.getObjectToJSON(leadRequest);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
        HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, leadRequestStr);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
        int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
        allLeads.add(leadIdFirst);

        /**
         * Update few keys
         */
        leadRequestUpdated = leadRequest
                .setAddress(UUID.randomUUID().toString());
                //.setSource(1)
                //.setIdFromSource("Z");
        try {
            leadRequestStrUpdated = helper.getObjectToJSON(leadRequestUpdated);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }

        HttpResponse respUpdate = core.invokePostWithStringJson(LMS.UPDATE_LEAD, headers, leadRequestStrUpdated, new String[] { String.valueOf(leadIdFirst) } );
        Assert.assertEquals(respUpdate.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK. "+respUpdate.getResponseMsg());

        Map<String, Object> map = db.leadDetails(leadIdFirst);
        /**
         * Verify DB details with request payload
         */
        lmsHelper.verifyDBWithRequest(map, leadRequestUpdated);
    }

    @Test(description = "Get all leads")
    public void getAllLeads(){
        HttpResponse resp = core.invoker(LMS.GET_ALL_LEADS, headers);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK. "+resp.getResponseMsg());
        Assert.assertTrue( schema.validateSchema(LmsCommon.getAbsolutePathOfSchema(LMS.GET_ALL_LEADS), resp.getResponseMsg()), "Schema validation failed");
        /**
         * Removed size check
        JSONArray respArr = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$", JSONArray.class);
        Assert.assertEquals(db.getLeadTableSize(), respArr.size(), "Response size is != DB size");
         */
    }

    @Test(description = "Get Lead By ID")
    public void getLeadById(){
        CreateLeadPojo leadRequest = lmsDp.giveMeRandomPayloadForCreateLead();
        String leadRequestStr = null;
        try {
            leadRequestStr = helper.getObjectToJSON(leadRequest);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
        /**
         * Create a lead first
         */
        HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, leadRequestStr);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
        int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
        allLeads.add(leadIdFirst);

        /**
         * Get the same lead
         */
        HttpResponse respGetLeadId = core.invoker(LMS.GET_LEAD_BY_ID, headers, new String[] { String.valueOf(leadIdFirst) });
        Assert.assertEquals(HttpStatus.SC_OK, respGetLeadId.getResponseCode(), "Status is not 200 Created");
        Map<String, Object> mapSec = db.leadDetails(leadIdFirst);
        lmsHelper.verifyDBWithRequest(mapSec, leadRequest);
    }

    @Test(description = "Delete lead by id")
    public void deleteLeadById(){
        CreateLeadPojo leadRequest = lmsDp.giveMeRandomPayloadForCreateLead();
        String leadRequestStr = null;
        try {
            leadRequestStr = helper.getObjectToJSON(leadRequest);
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
        /**
         * Create a lead first
         */
        HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, leadRequestStr);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
        int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
        allLeads.add(leadIdFirst);

        /**
         * Delete same lead
         */
        HttpResponse respGetLeadId = core.invoker(LMS.DELETE_LEAD_BY_ID, headers, new String[] { String.valueOf(leadIdFirst) });
        Assert.assertEquals(respGetLeadId.getResponseCode(), HttpStatus.SC_NO_CONTENT, "Status is not 204 No Content");
        Assert.assertEquals(Boolean.valueOf("true"), Boolean.valueOf(db.isLeadExists(leadIdFirst)), "Lead should get deleted from DB");
    }

    @Test(description = "Download all duplicate leads")
    public void aDownloadAllDuplicateLeads() throws InterruptedException {
        String newLeadPayloadFirst; String newLeadPayloadSec;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead().setName("L");
        try {

            /**
             * Create first Lead
             */
            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);

            /**
             * Create duplicate Lead
             */

            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);

            /**
             * Create one More Lead
             */

            HttpResponse respThird = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(respThird.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdThird = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdThird);

            if (db.verifyDeDupStatus(leadIdThird, "d")) {
                /**
                 * DownLoad all duplicate leads
                 */
                HttpResponse downloadLeadsResp = core.invoker(LMS.DOWNLOAD_ALL_DUPLICATE_LEADS, headers);
                Assert.assertEquals(downloadLeadsResp.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK");
                int numberOfLeads = downloadLeadsResp.getResponseMsg().split("\n").length - 1;

                Assert.assertEquals(numberOfLeads, db.getLeadCount("d"), "Count of 'duplicate leads is not matching with DB");
            }

            /**
             * Need to implement csv parser
            String[] allLeads = downloadLeadsResp.getResponseMsg().split("\n");
            for(int i = 1; i < allLeads.length; i++) {
                Map<String, Object> map = db.leadDetails(lmsHelper.returnLeadIdFromCSV(allLeads[i]));
                lmsHelper.verifyDBWithRequest(map, lmsHelper.csvParser(allLeads[i]));
            }
             **/

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Delete duplicate lead by id")
    public void deleteDuplicateLeadById(){
        String newLeadPayloadFirst = null;
        String newLeadPayloadSec = null;
        CreateLeadPojo newLeadFirst = lmsDp.giveMeRandomPayloadForCreateLead();

        try {
            /**
             * Create 1st Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE)
                    .setLng(LMS.LONG_SOURCE)
                    .setName(LMS.P_90_TO_98_NAME_MATCH_1);

            newLeadPayloadFirst = helper.getObjectToJSON(newLeadFirst);
            HttpResponse resp = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadFirst);
            Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdFirst = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdFirst);

            /**
             * Create 2nd Lead
             */
            newLeadFirst.setLat(LMS.LAT_SOURCE_LESS_250M_FROM_SOURCE)
                    .setLng(LMS.LONG_SOURCE_LESS_250M_FROM_SOURCE)
                    .setName(LMS.P_90_TO_98_NAME_MATCH_2)
                    .setPhoneNumber(AutomationHelper.GetRandomPhone("+91"));

            newLeadPayloadSec = helper.getObjectToJSON(newLeadFirst);
            HttpResponse respSec = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadPayloadSec);
            Assert.assertEquals(respSec.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
            int leadIdSec = AutomationHelper.getDataFromJson(respSec.getResponseMsg(), "$.id", Integer.class);
            allLeads.add(leadIdSec);

            int duplicateId = db.getDuplicateLeadId(leadIdSec, leadIdFirst);
            HttpResponse respDelete = core.invoker(LMS.DELETE_DUPLICATE_LEAD_BY_ID, headers, new String[] {String.valueOf(duplicateId)});
            Assert.assertEquals(respDelete.getResponseCode(), HttpStatus.SC_NO_CONTENT, "Status is not 200 OK \n" + respDelete.getResponseMsg());
            Assert.assertTrue(db.isDuplicateLeadExists(duplicateId), "Delete duplicate lead by id api is not removing entries from table");

        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "get sample upload file")
    public void getSampleUploadFile(){
        HttpResponse resp = core.invoker(LMS.SAMPLE_UPLOAD_FILE, headers);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_OK, "Response is not 200 OK");
        System.out.println(resp.getResponseMsg());
    }

    @Test(description = "Bulk Upload", dataProviderClass = LmsDP.class, dataProvider = "bulkUpload")
    public void bulkUpload(String type, String fileName) throws InterruptedException {
        /**
         * Bulk Drop
         */
        HttpResponse resp =  core.invokeFormData(LMS.BULK_UPLOAD, formHeaders, lmsHelper.formDataForBulkUpload(fileName));
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_CREATED, "Response is not 201 CREATED");
        String taskId = AutomationHelper.getDataFromJson(resp.getResponseMsg(), "$.task_id", String.class);

        Assert.assertTrue(lmsHelper.isProcessingDone(taskId), "Delay is lead process");

        /** Process further is worker mark the lead as done **/
        HttpResponse respGetTask =  core.invoker(LMS.TASKS_BY_ID, formHeaders, new String[] { taskId } );
        Assert.assertEquals(respGetTask.getResponseCode(), HttpStatus.SC_OK, "Response is not 200 OK\n"+respGetTask.getResponseMsg());
        String data = AutomationHelper.getDataFromJson(respGetTask.getResponseMsg(), "$.data", String.class);
        switch (type) {
            case "DUPLICATE" :
                Assert.assertEquals(LMS.BULK_DUPLICATE_LEADS_COUNT, db.countOfLeadsWithName(LMS.BULK_DUPLICATE_LEADS_NAME), "Lead count is not maching");
                Assert.assertEquals(data, "Successfully added "+LMS.BULK_DUPLICATE_LEADS_COUNT);
                break;
            case "UNIQUE" :
                Assert.assertEquals(LMS.BULK_UNIQUE_LEADS_COUNT, db.countOfLeadsWithName(LMS.BULK_UNIQUE_LEADS_NAME), "Lead count is not maching");
                Assert.assertEquals(data, "Successfully added "+LMS.BULK_UNIQUE_LEADS_COUNT);
                break;
            case "FEW_INVALID" :
                Assert.assertEquals(LMS.FEW_INVALID_LEADS_COUNT, db.countOfLeadsWithName(LMS.FEW_INVALID_LEADS_NAME), "Lead count is not maching");
                Assert.assertEquals(data, "Successfully added "+LMS.FEW_INVALID_LEADS_COUNT);
                JSONArray errors = AutomationHelper.getDataFromJson(respGetTask.getResponseMsg(), "$.errors", JSONArray.class);
                JSONArray nameError = AutomationHelper.getDataFromJson(errors.get(0), "$.2.name", JSONArray.class);
                JSONArray latError = AutomationHelper.getDataFromJson(errors.get(1), "$.3.lat", JSONArray.class);
                Assert.assertEquals(nameError.get(0), "This field may not be blank.", "Name Error is not coming in response");
                Assert.assertEquals(latError.get(0), "A valid number is required.", "Lat Error is not coming in response");
                break;
        }
        db.deleteBulkUploadLeads();
    }

    @Test(description = "request validation", dataProviderClass = LmsDP.class, dataProvider = "requestValidationDP")
    public void requestValidation(String fieldName){
        CreateLeadPojo newLead = lmsDp.giveMeRandomPayloadForCreateLead();
        String newLeadModified;
        try {
            switch (fieldName) {
                case "PHONE":
                    newLead.setPhoneNumber("12345").setName("P");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respName = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respName.getResponseCode(), HttpStatus.SC_BAD_REQUEST, "Status is not 400 BR");
                    break;
                case "EMAIL":
                    newLead.setEmail("notavalidemail").setName("Q");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respEmail = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respEmail.getResponseCode(), HttpStatus.SC_BAD_REQUEST, "Status is not 400 BR");
                    break;
                case "WEBSITE":
                    newLead.setWebsite("notavalidwebsite").setName("R");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respWeb = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respWeb.getResponseCode(), HttpStatus.SC_BAD_REQUEST, "Status is not 400 BR");
                    break;
                case "LEAD_SOURCE_DEFAULT":
                    newLead.setSource(null).setName("S");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respSource = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respSource.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
                    int leadIdSource = AutomationHelper.getDataFromJson(respSource.getResponseMsg(), "$.id", Integer.class);
                    allLeads.add(leadIdSource);
                    Assert.assertNotNull(db.getValuesFromLeadTable(leadIdSource, LmsDBManager.APILead.SOURCE_ID));
                    break;
                case "LEAD_TYPE_DEFAULT":
                    newLead.setType(null).setName("T");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respType = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respType.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
                    int leadIdType = AutomationHelper.getDataFromJson(respType.getResponseMsg(), "$.id", Integer.class);
                    allLeads.add(leadIdType);
                    Assert.assertNotNull(db.getValuesFromLeadTable(leadIdType, LmsDBManager.APILead.TYPE));
                    break;
                case "WAIT_TIME":
                    newLead.setWaitTime(null).setName("U");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respWaitTime = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respWaitTime.getResponseCode(), HttpStatus.SC_CREATED, "Status is not 201 Created");
                    int leadIdWaitTime = AutomationHelper.getDataFromJson(respWaitTime.getResponseMsg(), "$.id", Integer.class);
                    allLeads.add(leadIdWaitTime);
                    Assert.assertNotNull(db.getValuesFromLeadTable(leadIdWaitTime, LmsDBManager.APILead.WAIT_TIME));
                    Assert.assertEquals(db.getValuesFromLeadTable(leadIdWaitTime, LmsDBManager.APILead.WAIT_TIME), LMS.DEFAULT_WAIT_TIME, "Default wait is incorrect");
                    break;
                case "MIN_CATEGOTY":
                    newLead.setCategory(null).setName("V");
                    newLeadModified = helper.getObjectToJSON(newLead);
                    HttpResponse respMinCat = core.invokePostWithStringJson(LMS.CREATE_LEAD, headers, newLeadModified);
                    Assert.assertEquals(respMinCat.getResponseCode(), HttpStatus.SC_BAD_REQUEST, "Status is not 400 BR");
                    break;
            }
        } catch (IOException e) {
            Assert.fail("Not able to covert object to string",  e.fillInStackTrace());
        }
    }

    @Test(description = "Multiple duplicate lead test")
    public void multipleDuplicateLeadsAuto(){

    }


    @AfterMethod(enabled = true, alwaysRun = true)
    public void tearDown(){
        lmsHelper.deleteAllLeads((ArrayList<Integer>) allLeads);
        allLeads.clear();
    }

    @AfterClass(enabled = true, alwaysRun = true)
    public void deleteBulkUploadLeads(){
        db.deleteBulkUploadLeads();
    }

    @BeforeClass(enabled = true)
    public void deleteAllAutomationLeads(){
        db.deleteBulkUploadLeads();
    }
}