package com.swiggy.api.dash.checkout.pojos;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class EditItemPatch {
    @JsonProperty("items")
    private List<Item> items;

    private Map<String, Object> ctx;

    private boolean verifyServiceability;

    private boolean verifyItemAvailability;

    private boolean verifyItemPricing;

    private boolean storeAvailability;

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public EditItemPatch setItems(List<Item> items) {
        this.items = items;
        return this;
    }

    public EditItemPatch withItems(List<Item> items) {
        this.items = items;
        return this;
    }
    public boolean getVerifyServiceability() {
        return verifyServiceability;
    }

    public EditItemPatch setVerifyServiceability(Boolean verifyServiceability) {
        this.verifyServiceability = verifyServiceability;
        return this;
    }

    public boolean getVerifyItemAvailability() {
        return verifyItemAvailability;
    }

    public EditItemPatch setVerifyItemAvailability(Boolean verifyItemAvailability) {
        this.verifyItemAvailability = verifyItemAvailability;
        return this;
    }

    public boolean getVerifyItemPricing() {
        return verifyItemPricing;
    }

    public EditItemPatch setVerifyItemPricing(Boolean verifyItemPricing) {
        this.verifyItemPricing = verifyItemPricing;
        return this;
    }

    public boolean getStoreAvailability() {
        return storeAvailability;
    }

    public EditItemPatch setStoreAvailability(Boolean storeAvailability) {
        this.storeAvailability = storeAvailability;
        return this;
    }

    public Map<String, Object> getCtx() {
        return Optional.ofNullable(ctx).orElse(Collections.EMPTY_MAP);
    }

    public EditItemPatch setCtx(Map<String, Object> ctx) {
        this.ctx = ctx;
        return this;
    }


}
