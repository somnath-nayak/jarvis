package com.swiggy.api.dash.SuperAppDeliveryService.helper;

import com.swiggy.api.dash.SuperAppDeliveryService.models.DELoginRequest;
import com.swiggy.api.dash.SuperAppDeliveryService.models.Location;
import com.swiggy.api.dash.SuperAppDeliveryService.models.Otp;
import com.swiggy.api.dash.Tripmanager.helper.JobType;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.HashMap;

public class Superappdeliveryhelper {

    HttpCore core = new HttpCore(Superappdeliverycontant.SUPERAPPDELIVERYSERVICE);
    JsonHelper jsonHelper = new JsonHelper();

    public void deLogin(long deId) throws Exception{
        String apiname="loginv2";
        DELoginRequest deLoginRequest=new DELoginRequest().withId(deId).withVersion(Superappdeliverycontant.appversion);
       HttpResponse response=core.invokePostWithStringJson(apiname,getSuperAppHeader(),jsonHelper.getObjectToJSON(deLoginRequest));
    }

    public HttpResponse enterOtp(long deId,long otptext) throws Exception{
        String apiname="otpv2";
        Otp otp =new Otp().withId(deId).withOtp(otptext);
        String payload=jsonHelper.getObjectToJSON(otp);
        HttpResponse response=core.invokePostWithStringJson(apiname,getSuperAppHeader(),payload);
        getDeAuth(response);
        return response;

    }
    public HttpResponse startDuty(long deId,String auth){
        String apiname="startduty";
        HttpResponse response=core.invokePostWithStringJson(apiname,getSuperAppHeader(),null);
        return response;
    }

    public HttpResponse stopDuty(int deId,String auth){
        String apiname="stopduty";
        HttpResponse response=core.invokePostWithStringJson(apiname,getHeaderForSuperAppDeliveryService(auth),null);
        return response;
    }


    public HashMap<String,String> getHeaderForSuperAppDeliveryService(String auth){
        HashMap<String,String> headers=new HashMap<>();
        headers.put("Authorization",auth);
        return headers;
    }


    public void updateDeliveryExecutiveLocation(int deId,String auth,String lat,String lng) throws Exception{
        String apiname="updatelocationv2";
        Location location =new Location().withLat(lat).withLng(lng);
        String payload=jsonHelper.getObjectToJSON(location);

    }


    public void loginDeliveryExecutie(Long deId) throws Exception{
            deLogin(deId);
            enterOtp(deId, Superappdeliverycontant.MASTEROTP);
            startDuty(deId,Superappdeliverycontant.auth);

    }

    public String getDeAuth(HttpResponse response){
        String auth=null;
        if(response!=null){
            auth = AutomationHelper.getDataFromJson(response.getResponseMsg(), "$.data.Authorization", String.class);
        }
        Superappdeliverycontant.auth=auth;
        return auth;
    }


    public HashMap<String,String> getSuperAppHeader(){
        HashMap<String,String> header=new HashMap<>();
        header.put("app-version-name",Superappdeliverycontant.appversion);
        header.put("app-version-code",Superappdeliverycontant.appversion);
        header.put("uid","test");
        header.put("device","MOTO");
        header.put("os-version","9.01");
        header.put("Authorization",Superappdeliverycontant.auth);
        return header;


    }








}
