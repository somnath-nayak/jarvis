package com.swiggy.api.dash.dashDeliveryService.helper;

public enum TaskMetaDataStatus {

    ITEM_CONFIRMATION_REQUESTED,
    CART_CONFIRMATION_REQUESTED,
    ITEM_CONFIRMED,
    CART_CONFIRMED,
}
