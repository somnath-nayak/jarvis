package com.swiggy.api.dash.dashDeliveryService.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryHelper;
import com.swiggy.api.erp.cms.pojo.AttributeItem.ItemAttribute;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "id",
        "isSelected",
        "name",
        "price",
        "imageUrls"
})
public class ItemAlternative {

    @JsonProperty("id")
    private int id;
    @JsonProperty("isSelected")
    private boolean isSelected;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private double price;
    @JsonProperty("imageUrls")
    private ArrayList<String> imageUrls;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<String> getImageUrls() {
        return imageUrls;
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
    }


    public static List<ItemAlternative> setDefaultItemAlternative()
    {

        ItemAlternative itemAlternative = new ItemAlternative();
        itemAlternative.setId(DashDeliveryConstant.ALTERNATIVE_ITEM_ID);
        itemAlternative.setImageUrls(ImageUrls.setDefaultArrayImageUrls());
        itemAlternative.setName(DashDeliveryConstant.ALTERNATIVE_ITEM_NAME);
        itemAlternative.setPrice(DashDeliveryConstant.PRICE);
        itemAlternative.setSelected(true);
        ArrayList<ItemAlternative> alternativeArrayList = new ArrayList<>(asList(itemAlternative));
        return alternativeArrayList;


    }

    public static List<ItemAlternative> getNNumberOfAlternatives(int n,String price)
    {
        //price is comma seperated set of prices
        String[] priceOfAlternatives = price.split(",");

        ArrayList<ItemAlternative> alternativeArrayList = new ArrayList<>();
        for(int i=0;i<n;i++)
        {
            ItemAlternative itemAlternative = new ItemAlternative();
            itemAlternative.setId(DashDeliveryHelper.generateAlternativeItemId());
            itemAlternative.setImageUrls(ImageUrls.setDefaultArrayImageUrls());
            itemAlternative.setName(DashDeliveryConstant.ALTERNATIVE_ITEM_NAME);
            if (priceOfAlternatives.length > 0)
            itemAlternative.setPrice(Integer.parseInt(priceOfAlternatives[i]));
            itemAlternative.setSelected(false);
            alternativeArrayList.add(i,itemAlternative);
        }
        return alternativeArrayList;
    }

}
