package com.swiggy.api.dash.userService;

public class Login {

    private String mobile;
    private String password;

    public String getMobile() {
        return mobile;
    }

    public Login setMobile(String mobile) {
        this.mobile = mobile;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Login setPassword(String password) {
        this.password = password;
        return this;
    }


}
