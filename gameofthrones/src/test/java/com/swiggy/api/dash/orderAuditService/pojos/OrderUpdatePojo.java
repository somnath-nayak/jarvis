package com.swiggy.api.dash.orderAuditService.pojos;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "order_id",
        "order_group_id",
        "update_json",
        "update_type",
        "update_time",
        "updated_by",
        "updater_type",
        "order_version"
})
public class OrderUpdatePojo {

    @JsonProperty("order_id")
    private String orderId;
    @JsonProperty("order_group_id")
    private String orderGroupId;
    @JsonProperty("update_json")
    private String updateJson;
    @JsonProperty("update_type")
    private String updateType;
    @JsonProperty("update_time")
    private Integer updateTime;
    @JsonProperty("updated_by")
    private String updatedBy;
    @JsonProperty("updater_type")
    private String updaterType;
    @JsonProperty("order_version")
    private Integer orderVersion;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderUpdatePojo() {
    }

    /**
     *
     * @param updateTime
     * @param updateJson
     * @param updaterType
     * @param updateType
     * @param orderVersion
     * @param orderId
     * @param updatedBy
     * @param orderGroupId
     */
    public OrderUpdatePojo(String orderId, String orderGroupId, String updateJson, String updateType, Integer updateTime, String updatedBy, String updaterType, Integer orderVersion) {
        super();
        this.orderId = orderId;
        this.orderGroupId = orderGroupId;
        this.updateJson = updateJson;
        this.updateType = updateType;
        this.updateTime = updateTime;
        this.updatedBy = updatedBy;
        this.updaterType = updaterType;
        this.orderVersion = orderVersion;
    }

    @JsonProperty("order_id")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("order_id")
    public OrderUpdatePojo setOrderId(String orderId) {
        this.orderId = orderId;
        return this;
    }

    @JsonProperty("order_group_id")
    public String getOrderGroupId() {
        return orderGroupId;
    }

    @JsonProperty("order_group_id")
    public OrderUpdatePojo setOrderGroupId(String orderGroupId) {
        this.orderGroupId = orderGroupId;
        return this;
    }

    @JsonProperty("update_json")
    public String getUpdateJson() {
        return updateJson;
    }

    @JsonProperty("update_json")
    public OrderUpdatePojo setUpdateJson(String updateJson) {
        this.updateJson = updateJson;
        return this;
    }

    @JsonProperty("update_type")
    public String getUpdateType() {
        return updateType;
    }

    @JsonProperty("update_type")
    public OrderUpdatePojo setUpdateType(String updateType) {
        this.updateType = updateType;
        return this;
    }

    @JsonProperty("update_time")
    public Integer getUpdateTime() {
        return updateTime;
    }

    @JsonProperty("update_time")
    public OrderUpdatePojo setUpdateTime(Integer updateTime) {
        this.updateTime = updateTime;
        return this;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public OrderUpdatePojo setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @JsonProperty("updater_type")
    public String getUpdaterType() {
        return updaterType;
    }

    @JsonProperty("updater_type")
    public OrderUpdatePojo setUpdaterType(String updaterType) {
        this.updaterType = updaterType;
        return this;
    }

    @JsonProperty("order_version")
    public Integer getOrderVersion() {
        return orderVersion;
    }

    @JsonProperty("order_version")
    public OrderUpdatePojo setOrderVersion(Integer orderVersion) {
        this.orderVersion = orderVersion;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("orderId", orderId).append("orderGroupId", orderGroupId).append("updateJson", updateJson).append("updateType", updateType).append("updateTime", updateTime).append("updatedBy", updatedBy).append("updaterType", updaterType).append("orderVersion", orderVersion).toString();
    }

}