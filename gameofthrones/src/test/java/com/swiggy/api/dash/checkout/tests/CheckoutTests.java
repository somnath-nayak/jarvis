package com.swiggy.api.dash.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.constants.Help;
import com.swiggy.api.dash.checkout.dp.CheckoutDp;
import com.swiggy.api.dash.checkout.helper.CartType;
import com.swiggy.api.dash.checkout.helper.CheckoutHelper;
import com.swiggy.api.dash.checkout.helper.OrderType;
import com.swiggy.api.dash.checkout.helper.TriFunctional;
import com.swiggy.api.dash.checkout.pojos.CartCreation;
import com.swiggy.api.dash.checkout.pojos.Item;
import com.swiggy.api.dash.coupon.CouponHelper;
import com.swiggy.api.dash.coupon.pojo.Coupon;
import com.swiggy.api.dash.coupon.pojo.CouponMap;
import com.swiggy.api.dash.kms.helper.HttpResponse;
import com.swiggy.api.dash.td.TdType;
import com.swiggy.api.dash.td.pojo.TdView;
import net.minidev.json.JSONArray;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CheckoutTests extends CheckoutDp {

    SoftAssert softAssert;
    CheckoutHelper checkoutHelper = new CheckoutHelper();
    CheckoutDp checkoutDp = new CheckoutDp();
    CartType cartType = new CartType();
    CouponHelper couponHelper =new CouponHelper();
    OrderType orderType = new OrderType();
    TdType td = new TdType();

    private HashMap<? extends String,? extends String> hashMap= new HashMap<>();

    @BeforeClass
    public void before(){
        hashMap=checkoutHelper.signIn(checkoutDp.getLogin());
    }

    @Test(description = "create structured cart", dataProvider = "getCart")
    public void createCart(String carttype) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Entity(carttype).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        softAssert.assertAll();
    }

    /*coupons and Trade discount
    1.Only Coupon(Flat, Percent, Free Del)
    2.Only Td(Flat, Percent, Free Del)
    3.combination of td and coupon
    4.No Multi td combination
     */
    /*@Test(description = "create structured cart store configs")
    public void cartCalculation() {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core("structure",null, "percentage").apply(Help.Structure));
        String coupons;
        String discount;
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemPriceAfterTD", ArrayList.class), true);
            List<String> discounts = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemPriceAfterTD", ArrayList.class);
            List<String> itemStorePrice = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemStorePrice", ArrayList.class);

            Double d = convertList(discounts, s -> Double.valueOf(s)).stream().collect(Collectors.summingDouble(Double::doubleValue));
            Double itemStore = convertList( itemStorePrice , s ->Double.valueOf(s)).stream().collect(Collectors.summingDouble(Double::doubleValue));
            String coupon = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
            Optional<String> opt = Optional.ofNullable(coupon);
            JSONArray td=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts..type", JSONArray.class);
            JSONArray tdType= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts..tdType", JSONArray.class);
            JSONArray discountType=  checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts", JSONArray.class);
            for(Object je : discountType){

            }

            discount = td.get(0).toString();
            coupons=td.get(1).toString();
            System.out.println(opt.isPresent() + coupons);
            Double extraPrices= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storePackingCharges", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.gst", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
            Double amountToPay=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);

            if (opt.isPresent() && Help.CALCULATION_STRATEGY.Coupon.name().equals(coupons) && Help.CALCULATION_STRATEGY.TradeDiscount.name().equals(discount)) {
                Double v1 = d + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                System.out.println(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts", JSONArray.class));
                Double toPay = v1 - Double.valueOf(j.get(0).toString());

                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                System.out.println(d + " "+v1 +" " +j.get(0));
                System.out.println(toPay+ +roundedToPay);
                System.out.println(roundedToPay + " " + j + +v1);
                Double storePrice = v1 - checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
                softAssert.assertEquals(amountToPay, roundedToPay, "to pay amount is not same");
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storeBill", Double.class), storePrice, "store level bill is not same/correct");
            }

            if(opt.isPresent() && Help.CALCULATION_STRATEGY.Coupon.name().equals(coupons) && !Help.CALCULATION_STRATEGY.TradeDiscount.name().equals(discount)){
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double toPay= itemStore+extraPrices - Double.valueOf(j.get(0).toString());
                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                softAssert.assertEquals(amountToPay, roundedToPay, "to pay amount is not same");
            }

            //free del td is pending
            if(!opt.isPresent() && Help.CALCULATION_STRATEGY.TradeDiscount.name().equals(coupons) &&
                    (Help.CALCULATION_STRATEGY.PERCENT.name().equals(tdType.get(0).toString()) || Help.CALCULATION_STRATEGY.FLAT.name().equals(tdType.get(0).toString()))){
                Double v1 = d + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                System.out.println(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts", JSONArray.class));
                Double toPay = v1;
                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                Double storePrice = v1 - checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
                Double roundedStorePrice = ((double) Math.round(storePrice * 100)) / 100;
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class), roundedToPay, "to pay amount is not same");
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storeBill", Double.class), roundedStorePrice, "store level bill is not same/correct");
            }
            softAssert.assertAll();
        }
    }*/

    /*
    for percent and flat td itempriceaftertd key will be used,
    for free del itemStorePrice will be used
     */
    @Test(description = "create structured cart, apply td and verify the cart calculation", dataProvider = "getCartTd")
    public void cartTdCalculation(String cartTy, String tdTy) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core(cartTy,null, tdTy).apply(Help.Structure));
        System.out.println("Payload \n" + createCartRequest);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemPriceAfterTD", ArrayList.class), true);
            List<String> discounts = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemPriceAfterTD", ArrayList.class);
            List<String> itemStorePrice = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemStorePrice", ArrayList.class);

            Double d = convertList(discounts, s -> Double.valueOf(s)).stream().collect(Collectors.summingDouble(Double::doubleValue));
            Double itemStore = convertList( itemStorePrice , s ->Double.valueOf(s)).stream().collect(Collectors.summingDouble(Double::doubleValue));
            JSONArray tdType= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts..tdType", JSONArray.class);

            Double extraPrices= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storePackingCharges", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.gst", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
            Double amountToPay=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);

            //percentage
            if (Help.CALCULATION_STRATEGY.PERCENT.name().equals(tdType.get(0).toString())) {
                Double v1 = d + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double toPay = v1;
                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                Double storePrice = v1 - checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
                softAssert.assertEquals(amountToPay, roundedToPay, "to pay amount is not same");
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storeBill", Double.class), storePrice, "store level bill is not same/correct");
                softAssert.assertEquals(itemStore-d, Double.valueOf(j.get(0).toString()), "discount price is not matching");
            }

            //flat
            if(Help.CALCULATION_STRATEGY.FLAT.name().equals(tdType.get(0).toString())){
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double toPay= d+extraPrices;
                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                softAssert.assertEquals(amountToPay, roundedToPay, "to pay amount is not same");
                softAssert.assertEquals(itemStore-d, Double.valueOf(j.get(0).toString()), "discount price is ot matching");
            }

            //free del td is pending
            if(Help.CALCULATION_STRATEGY.FREE_DELIVERY.name().equals(tdType.get(0).toString())){
                Double v1 = itemStore + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double toPay = v1 - Double.valueOf(j.get(0).toString());
                Double roundedToPay = ((double) Math.round(toPay * 100)) / 100;
                Double storePrice = v1 - checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
                Double roundedStorePrice = ((double) Math.round(storePrice * 100)) / 100;
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class), roundedToPay, "to pay amount is not same");
                softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storeBill", Double.class), roundedStorePrice, "store level bill is not same/correct");
            }
            softAssert.assertAll();
        }
    }


    @Test(description = "create structured cart, apply td and verify the cart calculation", dataProvider = "getCartCoupon")
    public void cartCouponCalculation(String cartTy, String couponTy) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core(cartTy, couponTy, null).apply(Help.Structure));
        System.out.println("Payload \n" + createCartRequest);
        checkoutHelper.disableTd("41288");
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            List<String> itemStorePrice = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemStorePrice", ArrayList.class);
            Double itemStore = convertList(itemStorePrice, s -> Double.valueOf(s)).stream().collect(Collectors.summingDouble(Double::doubleValue));
            Double extraPrices = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.storePackingCharges", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.gst", Double.class)
                    + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.deliveryCharges", Double.class);
            Double amountToPay = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
            JSONArray couponType= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts..couponType", JSONArray.class);
            //Double amountToPay=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);

            if (Help.CALCULATION_STRATEGY.FLAT.name().equals(couponType.get(0).toString())) {
                Double v1 = itemStore + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double priceAfterCoupon=v1 - Double.valueOf(j.get(0).toString());
                Double roundedToPay = ((double) Math.round(priceAfterCoupon * 100)) / 100;
                softAssert.assertEquals(roundedToPay,amountToPay, "to pay amount is not equal" );
            }
            if (Help.CALCULATION_STRATEGY.PERCENT.name().equals(couponType.get(0).toString())) {
                Double v1 = itemStore + extraPrices;
                JSONArray j = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..value", JSONArray.class);
                Double priceAfterCoupon=v1 - Double.valueOf(j.get(0).toString());
                Double roundedToPay = ((double) Math.round(priceAfterCoupon * 100)) / 100;
                softAssert.assertEquals(roundedToPay,amountToPay, "to pay amount is not equal" );
            }
        }
        softAssert.assertAll();
    }


    //for edit flow, cases will be same only cart and order creation is different acc. to cart type
    @Test(description = "create unstructured cart", dataProvider = "getUnstrcuturedTd")
    public void unstructuredCart(String cartty, String tdTy) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core(cartty,null,tdTy).apply(Help.UnStructure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
            //order job id, cart id, should be return
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
            JSONArray flat= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..tradeDiscount.type", JSONArray.class);
            softAssert.assertEquals(flat.get(0).toString(),tdTy, "discounts are not of same type");
            String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
            String orderRequest = checkoutHelper.serialize(orderType.Entity(CART.UNSTRUCTURE).apply(Help.UnStructure));
            HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.header(hashMap));
            JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);
            String orderJobId="";
            for(Object je : orderJob){
                orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
                softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
                softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
            }
            softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
            HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
            JSONArray cloneFlat= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data..tradeDiscount.type", JSONArray.class);
            softAssert.assertEquals(cloneFlat.get(0).toString(),tdTy, "discounts are not of same type");
            String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
            String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
            HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHead());
            if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
                softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
            } else {
                softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"statussss is not 0");
                JSONArray patchFlat= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data..tradeDiscount.type", JSONArray.class);
                softAssert.assertEquals(patchFlat.get(0).toString(), tdTy,"discount type is not same in patch items");

            }
            //parameter for forceupdate
            String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
            HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
            if (response.getResponseCode() != HttpStatus.SC_OK) {
                softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
                JSONArray forceTd= checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data..metaItems[1].tdType", JSONArray.class);
                softAssert.assertEquals(forceTd.get(0).toString(), tdTy,"discount type is not same in force update");
            }
            HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null,new String[]{orderJobId,cloneCartId},hashMap);
            String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
            softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        }
        softAssert.assertAll();
    }

    @Test(description = "payment link", dataProvider = "getTdCouponStruct")
    public void structuredOrder(String cartty, String cType, String tdType){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core(cartty,cType, tdType).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart  is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of payment link not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);
        for(Object je : orderJob){
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        softAssert.assertAll();
    }

/*
    @Test(description = "item level billing")
    public void itemLevelBillStructured(){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Entity("structure").apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        List<String> totalItemPrice= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemStorePrice", ArrayList.class);
        Double d=convertList(totalItemPrice, s-> Double.valueOf(s))
                .stream()
                .collect(Collectors.summingDouble(Double::doubleValue));
        System.out.println("==="+d);
        List<String> discountItemPrice = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.billedItems..itemPriceAfterTD", ArrayList.class);

        Double disPrice= convertList(discountItemPrice, s ->Double.valueOf(s))
                .stream()
                .collect(Collectors.summingDouble(Double::doubleValue));
        JSONArray data=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..bill.discounts..value", JSONArray.class);
        softAssert.assertEquals(-disPrice+d, Double.parseDouble(data.get(0).toString()),"dicsounted amount is not matching");softAssert.assertAll();
        softAssert.assertAll();
    }*/

    @Test(description = "store Info")
    public void storeInfo(){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Entity("structure").apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null,checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        JSONArray p=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..storesInfo..partner", JSONArray.class);
        JSONArray p1=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..storesInfo", JSONArray.class);
        for(Object je : p1) {
            JSONArray areaId = checkoutHelper.getDataFromJson(je, "$..areaId", JSONArray.class);
            JSONArray cityId = checkoutHelper.getDataFromJson(je, "$..cityId", JSONArray.class);
            softAssert.assertTrue(func(cityId.get(0)).isPresent() && !cityId.get(0).toString().isEmpty(), "city id is empty");
            softAssert.assertTrue(func(areaId.get(0)).isPresent() && !areaId.get(0).toString().isEmpty(), "area id is empty");
        }
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        softAssert.assertTrue(func(cartId).isPresent() && !cartId.isEmpty(), "cart id is empty");
        softAssert.assertTrue(p.contains(true), "not a partner store");
        softAssert.assertAll();
    }

    @Test(description = "item Info from sku service", dataProvider = "itemInfo")
    public void itemInfo(String carttype) {
        softAssert = new SoftAssert();
        CartCreation cartCreation= cartType.Entity(carttype).apply(Help.Structure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null,checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        JSONArray p = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..items", JSONArray.class);
        for(Object je : p) {
            JSONArray itemId = checkoutHelper.getDataFromJson(je, "$..itemId", JSONArray.class);
            JSONArray outOfStock=checkoutHelper.getDataFromJson(je, "$..outOfStock", JSONArray.class);
            JSONArray prices = checkoutHelper.getDataFromJson(je, "$..metadata..price.store_price", JSONArray.class);
            List<String> cartItemIdList = cartCreation.getItems().stream()
                    .map(Item::getItemId)
                    .collect(Collectors.toList());
            itemId.stream().forEach(item -> softAssert.assertTrue(cartItemIdList.contains(item), " id is empty"));
            JSONArray storeId = checkoutHelper.getDataFromJson(je, "$..storeId", JSONArray.class);
            List<String> storeIdList = cartCreation.getItems().stream()
                    .map(Item::getStoreId)
                    .collect(Collectors.toList());
            System.out.println(outOfStock);
            storeId.stream().forEach( store -> softAssert.assertTrue(storeIdList.contains(store.toString()), "store id mismatch"));
            outOfStock.stream().forEach(oos -> softAssert.assertFalse(Boolean.valueOf(oos.toString()), "oos for item mismatch"));
            prices.stream().forEach(price -> softAssert.assertTrue(func(price).isPresent() && !price.toString().isEmpty(), "price is empty from sku service"));
        }
        softAssert.assertAll();
    }


    @Test(description = "coupon valid or not",dataProviderClass = CheckoutDp.class, dataProvider = "itemInfo")
    public void couponValid(String carttype){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Entity(carttype).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        String coupon=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
        //softAssert.assertTrue(func(coupon).isPresent()&& !coupon.isEmpty() && !coupon.equals(null), "store id mismatch");
        Optional<String> opt= Optional.ofNullable(coupon);
       /* opt.ifPresent(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..type", JSONArray.class).contains("Coupon"), "coupon is not present")
        )*/
        if(opt.isPresent() )
            softAssert.assertTrue(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..type", JSONArray.class).contains("Coupon"), "coupon is not present");
       /* else softAssert.assertFalse();
        if(func(coupon).isPresent()&& !coupon.isEmpty() ){
            softAssert.assertTrue(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..type", JSONArray.class).contains("Coupon"), "coupon is not present");
        }else{
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.discounts..type",JSONArray.class).contains("Coupon"), "coupon is presnt");
        }*/
        softAssert.assertAll();
    }


    @Test(description = "attchements should not be empty or null",dataProviderClass = CheckoutDp.class, dataProvider = "itemInfo")
    public void attachments(String carttype) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Entity(carttype).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        JSONArray j= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.attachments", JSONArray.class);
        for(Object je :j){
            JSONArray field= checkoutHelper.getDataFromJson(je, "$..fileId", JSONArray.class);
            JSONArray desc = checkoutHelper.getDataFromJson(je, "$..fileId", JSONArray.class);
           /* ji.stream().filter(ja -> !ja.toString().isEmpty())
                    .findAny()
                    .orElse("NOt present"/"null");*/
            field.stream().forEach( fiel -> softAssert.assertTrue(!fiel.toString().isEmpty(), " field is empty"));
            desc.stream().forEach( descr -> softAssert.assertTrue(!descr.toString().isEmpty(), " description is empty"));
        }
        softAssert.assertAll();
    }


    @Test(description ="unstructure order")
    public void unstructuredCartClone() {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Core("unstructure",null,null).apply(Help.UnStructure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        String coupon = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
        Optional<String> optCoupon = Optional.ofNullable(coupon);
        String td=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.tradeDiscount.type", String.class);
        Optional<String> opt= Optional.ofNullable(td);
        if(opt.isPresent() && !optCoupon.isPresent()) {
            //softAssert.assertTrue(opt.isPresent(), "Td is not present");
        }
        if(opt.isPresent() && optCoupon.isPresent()){
        }

        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(CART.UNSTRUCTURE).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.header(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");

        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        if(opt.isPresent() && optCoupon.isPresent()){
            checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data.", String.class);
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null,new String[]{orderJobId,cloneCartId},hashMap);
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        //JSONArray s=checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.payment_info..payment_type", JSONArray.class);
        //System.out.println(patchOrderId+s.get(0));
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
       // String addInfo= checkoutHelper.serialize(checkoutDp.addPaymentInfo(String.valueOf(checkoutDp.forceUpdate().getAmount())));
       /* HttpResponse addInfoResponse = checkoutHelper.invokePostWithStringJson("updatepayment", addInfo, new String[]{orderJobId},checkoutHelper.commonHead());
        if (addInfoResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + addInfoResponse.getResponseMsg());
        }*/
        softAssert.assertAll();

    }


    @Test(dataProvider = "getCustom",description = "order type, create cart and coupon type")
    public void noStore(String carttype, String oType){
        softAssert = new SoftAssert();
        String noStoreCartRequest = checkoutHelper.serialize(orderType.Entity(oType).apply(oType));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("nostorecart", noStoreCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String orderJobId=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..orders..order_jobs..order_job_id", JSONArray.class).get(0).toString();
        String createCartByUser= checkoutHelper.serialize(cartType.Entity(carttype).apply(carttype));
        HttpResponse createCartResponse = checkoutHelper.invokePostWithStringJson("create", createCartByUser);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(createCartResponse.getResponseMsg(), "$.statusCode", String.class), CART.status0,"status is not 0");
        }
        String cartId= checkoutHelper.getDataFromJson(createCartResponse.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);

        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invoker("patchorder",new String[]{orderJobId,cloneCartId});
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");

    }

    @Test(dataProvider = "getCustomCoupon",description = "order type, create cart and coupon type")
    public void noStoreCoupon(String carttype, String oType, String couponTy){
        softAssert = new SoftAssert();
        String noStoreCartRequest = checkoutHelper.serialize(orderType.Entity(oType).apply(oType));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("nostorecart", noStoreCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String orderJobId=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..orders..order_jobs..order_job_id", JSONArray.class).get(0).toString();
        String createCartByUser= checkoutHelper.serialize(cartType.Core(carttype,couponTy,null).apply(carttype));
        HttpResponse createCartResponse = checkoutHelper.invokePostWithStringJson("create", createCartByUser);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
            softAssert.assertEquals(checkoutHelper.getDataFromJson(createCartResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        String cartId= checkoutHelper.getDataFromJson(createCartResponse.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);

        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invoker("patchorder",new String[]{orderJobId,cloneCartId});
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
    }

    @Test(dataProvider = "getCustomCoupon",description = "order type, create cart and coupon type")
    public void noStoreAddress(String carttype, String oType, String couponTy){
        softAssert = new SoftAssert();
        String noStoreCartRequest = checkoutHelper.serialize(checkoutHelper.buildCustomOrder(37667520));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("nostorecart", noStoreCartRequest,null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
            softAssert.assertEquals(CART.ADDRESSNO131, checkoutHelper.getDataFromJson(response.getResponseMsg(),"$.statusCode",String.class), "status is not 131");
        }
    }

    @Test(dataProvider = "getCustomCouponTD",description = "Check Td works on customorder")
    public void noStoreTD(String carttype, String oType, String couponTy, String tdType) {
        String noStoreCartRequest = checkoutHelper.serialize(orderType.Entity(oType).apply(oType));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("nostorecart", noStoreCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String orderJobId = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data..orders..order_jobs..order_job_id", JSONArray.class).get(0).toString();
        String createCartByUser = checkoutHelper.serialize(cartType.Core(carttype, couponTy, tdType).apply(carttype));
        HttpResponse createCartResponse = checkoutHelper.invokePostWithStringJson("create", createCartByUser);
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String cartId = checkoutHelper.getDataFromJson(createCartResponse.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null, new String[]{cartId}, checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId = checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);

        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(120.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId}, checkoutHelper.commonHead());
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invoker("patchorder", new String[]{orderJobId, cloneCartId});
        String patchOrderId = checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        //System.out.println(patchOrderId+orderJobId);
        Assert.assertEquals(patchOrderId,orderJobId,"order id is not same");
    }



    public CartCreation crt(String ct, String t){
      return  cartType.Core("structure", ct,t).apply(Help.UnStructure);
    }

    @Test(description = "item level billing")
    public void itemLevelBillStr() {
        BiFunction<String, String,CartCreation> cc = (x,y) ->{
            return crt(x,y);
        };
        String cart= checkoutHelper.serialize(cc.apply("Percentage", "Percentage"));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", cart);
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.commonHeaders(checkoutHelper.payment()));

        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(new HashMap<String, String>()));
        System.out.println(orderResponse);
    }

   /* public Map<String, String> convertListToMap(List<String> choices) {
        return choices.stream()
                .collect(Collectors.toMap(String::String, choice -> choice,
                        (oldValue, newValue) -> newValue));
    }

*/

   @Test(description = "structure edit with new set of items in patch call",dataProvider = "getTdCouponStruct")
   public void structureEdit(String cartty, String ctype, String tdType){
       softAssert = new SoftAssert();
       String createCartRequest = checkoutHelper.serialize(partial(cartType::Core,cartty).apply(ctype, tdType).apply(Help.Structure));
       HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
       if (response.getResponseCode() != HttpStatus.SC_OK) {
           softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
       }else{
           softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
       }
       HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
       if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
           softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
       }else{
           Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
       }
       Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
       String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
       HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
       JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

       String orderJobId="";
       for(Object je : orderJob){
           orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
           softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
           softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
       }
       softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
       String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
       HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
       String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
       String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
       HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
       if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
           softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
       } else {
           softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 01");
       }
       //parameter for forceupdate
       String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
       HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
       if (response.getResponseCode() != HttpStatus.SC_OK) {
           softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
       }
       HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
       String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
       softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
       softAssert.assertAll();
   }


    @Test(description = "unstructure edit with new set of items in patch call",dataProvider = "getTdCouponUnStruct")
    public void unStructureEdit(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        CartCreation cartCreation= cartType.Core(cartty,ctype, tdType).apply(Help.UnStructure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
            System.out.println(je);
            JSONArray coupon=checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders..coupon_code",JSONArray.class);
            System.out.println("-00-"+coupon+coupon.get(0).toString());
            System.out.println("000"+coupon.get(0).toString()+cartCreation.getCouponCode());
            softAssert.assertEquals(cartCreation.getCouponCode(),coupon.get(0).toString(),"coupon code is not matching");
            System.out.println(checkoutHelper.getDataFromJson(je, "$..customer_info",String.class));
            //softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$.data..coupon_code",String.class),);
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);

        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "unstructure edit with new set of items in patch call",dataProvider = "getTdCouponNull")
    public void unStructureEditCouponNull(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        CartCreation cartCreation= cartType.Core(cartty,null, CART.PERCENTAGE).apply(Help.UnStructure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
            JSONArray coupon=checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders..coupon_code",JSONArray.class);
         /*   System.out.println("jk"+coupon);
            System.out.println("kj");
            softAssert.assertEquals(coupon,"[null]","coupon code is present");*/
           // System.out.println(checkoutHelper.getDataFromJson(je, "$..customer_info",String.class));
            //softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$.data..coupon_code",String.class),);
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);

        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }


    @Test(description = "structure edit addition new set of items in patch call",dataProvider = "getTdCouponStruct")
    public void structureAddItems(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        CartCreation cartCreation = cartType.Core(cartty,ctype, tdType).apply(Help.Structure);
        String createCartRequest = checkoutHelper.serialize(cartType.Core(cartty,ctype, tdType).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.getItemPatch(cartCreation.getItems()));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "structure edit remove set of items in patch call",dataProvider = "getTdCouponStruct")
    public void structureRemoveItems(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        CartCreation cartCreation = cartType.Core(cartty,ctype, tdType).apply(Help.Structure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.getItemPatch(cartCreation.getItems()));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "structure edit remove set of items in patch call",dataProvider = "getTdCouponStruct")
    public void structureRemove(String cartty, String ctype, String tdTy){
        softAssert = new SoftAssert();
        CartCreation cartCreation = cartType.Core(cartty,ctype, tdTy).apply(Help.Structure);
       /* Coupon coup=couponHelper.Entity(ctype).apply(ctype);
        TdView tdView =tdType.Entity(tdTy).apply(tdTy);*/
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.getItemPatch(cartCreation.getItems()));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponStructure")
    public void structureMapCoupon(String cartty, String ctype, int enable, int minAmount,String tdType) {
        softAssert = new SoftAssert();
        CartCreation cartCreation = cartType.Cores(cartty,ctype,enable,minAmount,tdType).apply(Help.Structure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.getItemPatch(cartCreation.getItems()));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponUnStructure")
    public void unStructureMapCoupon(String cartty, String ctype, int enable,int minAmount, String tdType) {
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Cores(cartty,ctype,enable,minAmount, tdType).apply(Help.UnStructure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }


    @Test(description = "Map a coupon with store id and verify its there or not and cart amount is less than coupon min amount",dataProvider = "getTdCouponUnStructureCartAmount")
    public void unStructureCouponAmount(String cartty, String ctype, int enable,int minAmount, String tdType){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(cartType.Cores(cartty,ctype,enable,minAmount, tdType).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"120", "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(90.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        String errorMsg=checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data.ctx.Coupon_ERROR", String.class);
        softAssert.assertTrue(!errorMsg.isEmpty(), "msg is not there");

        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "Map a coupon with store id and verify its there or not and cart amount is less than coupon min amount",dataProvider = "getTdCouponStructureCartAmount")
    public void structureCouponAmount(String ctype, int enable,int minAmount, String tdType){
        softAssert = new SoftAssert();
        Coupon coupon = couponHelper.Mapping(ctype,enable,minAmount);
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));
        checkoutHelper.couponMap(lis);
        if(tdType!=null){
            TdView tdView = td.Entity(tdType).apply(tdType);
            checkoutHelper.tdCreation(tdView);
        }
        CartCreation cartCreation = getStructuredCart(coupon.getCode());
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
            softAssert.assertNull(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon", String.class), "its not null");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.getItemPatch(cartCreation.getItems()));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(90.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }





    @Test(description = "structure edit with new set of items in patch call with coupons",dataProvider = "getTdCouponStruct")
    public void structureEditCoupon(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        String createCartRequest = checkoutHelper.serialize(partial(cartType::Core,cartty).apply(ctype, tdType).apply(Help.Structure));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        HttpResponse paymentLink = checkoutHelper.invokePostWithStringJson("paymentlink", null,null,checkoutHelper.payment(hashMap));
        if (paymentLink.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + paymentLink.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(paymentLink.getResponseMsg(), "$.statusCode", Integer.class), true);
        }
        Double amount=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.bill.toPay", Double.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(String.valueOf(amount)).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.PLACED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 01");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }


    @Test(description = "unstructure edit with coupon call",dataProvider = "getTdCouponUnStruct")
    public void unStructureCoupon(String cartty, String ctype, String tdType){
        softAssert = new SoftAssert();
        CartCreation cartCreation= cartType.Core(cartty,ctype, tdType).apply(Help.UnStructure);

        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),"0", "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.Structure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
            //softAssert.assertEquals(cartCreation.getCouponCode(),checkoutHelper.getDataFromJson(je, "$..coupon_code",String.class),"coupon code is not matching");
            System.out.println(checkoutHelper.getDataFromJson(je, "$..customer_info",String.class));
            //softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$.data..coupon_code",String.class),);
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);

        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }


    @Test(description = "Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponStructure")
    public void structureDiffAddress(String cartty) {
        softAssert = new SoftAssert();
        CartCreation cartCreation = cartType.Entity(cartty).apply(Help.Structure);
        String createCartRequest = checkoutHelper.serialize(cartCreation);
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        } else {
            Reporter.log("ID " + checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), true);
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class), "0", "statuscode is not 0");
        }
    }

    @Test(description = "Cart with combination of structured and ustructured items, map a coupon & td and verify its response ",dataProvider = "getTdCouponUnStruct")
    public void hybridCart(String cartty,String cType, String tdType) {
        CartCreation cartCreation=partial(cartType::Core,cartty).apply(cType, tdType).apply(cartty);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String coupon = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
        Optional<String> optCoupon = Optional.ofNullable(coupon);
        String td=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.tradeDiscount.type", String.class);
        Optional<String> opt= Optional.ofNullable(td);
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(CART.UNSTRUCTURE).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.header(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        Object orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0);
            System.out.println(orderJobId);
            softAssert.assertTrue(!orderJobId.toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        Assert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.itemList("41288"));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", String.class), CART.STATUS,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        if(opt.isPresent() && optCoupon.isPresent()){
            checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data.", String.class);
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null,new String[]{orderJobId.toString(),cloneCartId},hashMap);
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
    }

    @Test(description = "Cart with combination of structured and ustructured items, map a coupon & td & remove items during item update & verify its response ",dataProvider = "getTdCouponUnStruct")
    public void hybridCartRemoveItems(String cartty,String cType, String tdType) {
        CartCreation cartCreation=partial(cartType::Core,cartty).apply(cType, tdType).apply(cartty);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String coupon = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
        Optional<String> optCoupon = Optional.ofNullable(coupon);
        String td=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.tradeDiscount.type", String.class);
        Optional<String> opt= Optional.ofNullable(td);
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(CART.UNSTRUCTURE).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.header(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        Object orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0);
            System.out.println(orderJobId);
            softAssert.assertTrue(!orderJobId.toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        Assert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", String.class), CART.STATUS,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(120.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        if(opt.isPresent() && optCoupon.isPresent()){
            checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data.", String.class);
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null,new String[]{orderJobId.toString(),cloneCartId},hashMap);
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
    }

    @Test(description = "Cart with combination of structured and ustructured items, map a coupon & td & remove items(unstructure) during item update & verify its response ",dataProvider = "getTdCouponUnStruct")
    public void hybridCartRemove(String cartty,String cType, String tdType) {
        CartCreation cartCreation=partial(cartType::Core,cartty).apply(cType, tdType).apply(cartty);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create cart is not 200 OK\nReason :\n" + response.getResponseMsg());
        }
        String coupon = checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.coupon.coupon", String.class);
        Optional<String> optCoupon = Optional.ofNullable(coupon);
        String td=checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.tradeDiscount.type", String.class);
        Optional<String> opt= Optional.ofNullable(td);
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        String orderRequest = checkoutHelper.serialize(orderType.Entity(CART.UNSTRUCTURE).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.header(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        Object orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0);
            System.out.println(orderJobId);
            softAssert.assertTrue(!orderJobId.toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        Assert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(new HashMap<String, String>()));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.addItemPatch(checkoutHelper.itemListUpdate("41288")));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHead());
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", String.class), CART.STATUS,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(120.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        if(opt.isPresent() && optCoupon.isPresent()){
            checkoutHelper.getDataFromJson(forceResp.getResponseMsg(), "$.data.", String.class);
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null,new String[]{orderJobId.toString(),cloneCartId},hashMap);
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
    }


    @Test(description = "hybrid cart with Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponUnStructure")
    public void hybridCartCouponMap(String cartty, String ctype, int enable,int minAmount, String tdType) {
        softAssert = new SoftAssert();
        CartCreation cartCreation=cartType.Cores(cartty,ctype,enable,minAmount, tdType).apply(Help.UnStructure);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            softAssert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),CART.STATUS, "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "hybrid cart with Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponUnStructure")
    public void hybridCartCouponMapRemove(String cartty, String ctype, int enable,int minAmount, String tdType) {
        softAssert = new SoftAssert();
        CartCreation cartCreation=cartType.Cores(cartty,ctype,enable,minAmount, tdType).apply(Help.UnStructure);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Assert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),CART.STATUS, "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutHelper.addItemPatch(checkoutHelper.itemListUpdate("41288")));
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(320.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    @Test(description = "hybrid cart with Map a coupon with store id and verify its there or not",dataProvider = "getTdCouponUnStructureCartAmount")
    public void hybridCartCouponAmount(String cartty, String ctype, int enable,int minAmount, String tdType) {
        softAssert = new SoftAssert();
        CartCreation cartCreation=cartType.Cores(cartty,ctype,enable,minAmount, tdType).apply(Help.UnStructure);
        String createCartRequest = checkoutHelper.serialize(cartCreation.setItems(checkoutHelper.itemList("41288")));
        HttpResponse response = checkoutHelper.invokePostWithStringJson("create", createCartRequest, null, checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + response.getResponseMsg());
        }else{
            Assert.assertEquals(checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.statusCode", String.class),CART.STATUS120, "statuscode is not 0");
        }
        String orderRequest = checkoutHelper.serialize(orderType.Entity(cartty).apply(Help.UnStructure));
        HttpResponse orderResponse = checkoutHelper.invokePostWithStringJson("order", orderRequest,null,checkoutHelper.commonHeaders(hashMap));
        JSONArray orderJob= checkoutHelper.getDataFromJson(orderResponse.getResponseMsg(), "$.data.orders", JSONArray.class);

        String orderJobId="";
        for(Object je : orderJob){
            orderJobId=checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString();
            softAssert.assertTrue(!checkoutHelper.getDataFromJson(je, "$..order_job_id", JSONArray.class).get(0).toString().isEmpty(), "order job is not present");
            softAssert.assertEquals(checkoutHelper.getDataFromJson(je, "$..status", JSONArray.class).get(0).toString(),CART.CONFIRMED, "status is not place or Confirmed ");
        }
        softAssert.assertTrue(!orderJob.isEmpty(), "order job is not present");
        String cartId= checkoutHelper.getDataFromJson(response.getResponseMsg(), "$.data.cartId", String.class);
        HttpResponse cloneCartResp = checkoutHelper.invokePostWithStringJson("clonecart", null,new String[] {cartId},checkoutHelper.commonHeaders(hashMap));
        String cloneCartId= checkoutHelper.getDataFromJson(cloneCartResp.getResponseMsg(), "$.data.cartId", String.class);
        String getPatch = checkoutHelper.serialize(checkoutDp.getItemPatch());
        HttpResponse patchResponse = checkoutHelper.invokePostWithStringJson("itemupdate", getPatch, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (patchResponse.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + patchResponse.getResponseMsg());
        } else {
            softAssert.assertEquals(checkoutHelper.getDataFromJson(patchResponse.getResponseMsg(), "$.statusCode", Integer.class), CART.status0,"status is not 0");
        }
        //parameter for forceupdate
        String forceUpdate = checkoutHelper.serialize(checkoutDp.forceUpdate(20.21));
        HttpResponse forceResp = checkoutHelper.invokePostWithStringJson("forceupdate", forceUpdate, new String[]{cloneCartId},checkoutHelper.commonHeaders(hashMap));
        if (response.getResponseCode() != HttpStatus.SC_OK) {
            softAssert.fail("Response of create Test Store is not 200 OK\nReason :\n" + forceResp.getResponseMsg());
        }
       // System.out.println(JsonPath.read(forceResp.getResponseMsg(),"$.data.ctx..Coupon_ERROR").toString());
        //System.out.println(checkoutHelper.getDataFromJson(forceResp.getResponseMsg(),"$.data.ctx..Coupon_ERROR",JSONArray.class).get(0).toString());
        Assert.assertEquals(checkoutHelper.getDataFromJson(forceResp.getResponseMsg(),"$.data.ctx..Coupon_ERROR",JSONArray.class).get(0).toString(),CART.COUPONERROR, "no error in coupon");
        HttpResponse patchOrder = checkoutHelper.invokePostWithStringJson("patchorder",null, new String[]{orderJobId,cloneCartId},checkoutHelper.commonHeaders(hashMap));
        String patchOrderId= checkoutHelper.getDataFromJson(patchOrder.getResponseMsg(), "$.data.order_job_id", String.class);
        softAssert.assertEquals(patchOrderId, orderJobId, "order id is not same");
        softAssert.assertAll();
    }

    public static <T, U, V, R> BiFunction<U, V, R> partial(TriFunctional<T, U, V, R> f, T x) {
        return (y, z) -> f.apply(x, y, z);
    }

    public <T> Optional func(T Object){
        return Optional.of(Object);
   }

    public <T, U> List<U> convertList(List<T> from, Function<T, U> func) {
        return from.stream().map(func).collect(Collectors.toList());
    }

}
