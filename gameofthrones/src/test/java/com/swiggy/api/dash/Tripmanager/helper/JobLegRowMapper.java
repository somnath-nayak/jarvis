package com.swiggy.api.dash.Tripmanager.helper;

import com.swiggy.api.dash.Tripmanager.models.JoblegBo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class JobLegRowMapper implements RowMapper {
    public JoblegBo mapRow(ResultSet rs, int rowNum) throws SQLException {
        JoblegBo joblegBo=new JoblegBo();
        joblegBo.setAckTime(rs.getTime("ack_time"));
        joblegBo.setJobId(rs.getLong("job_id"));
        joblegBo.setId(rs.getLong("id"));
        joblegBo.setStatus(JobLegStatus.values()[rs.getInt("status")]);
        return joblegBo;
    }


    }
