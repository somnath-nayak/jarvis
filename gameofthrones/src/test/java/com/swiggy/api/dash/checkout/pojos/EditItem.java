package com.swiggy.api.dash.checkout.pojos;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "serviceLine",
        "storeId",
        "itemTag",
        "quantity",
        "itemId",
        "name",
        "variant",
        "imageIds",
        "comment"
})
public class EditItem {

    @JsonProperty("type")
    private String type;
    @JsonProperty("serviceLine")
    private String serviceLine;
    @JsonProperty("storeId")
    private Integer storeId;
    @JsonProperty("itemTag")
    private Object itemTag;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("itemId")
    private String itemId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("variant")
    private Object variant;
    @JsonProperty("imageIds")
    private List<String> imageIds = null;
    @JsonProperty("comment")
    private Object comment;

    /**
     * No args constructor for use in serialization
     *
     */
    private List<EditItem> listOfItems = new ArrayList<>();
    public EditItem() {
    }

    /**
     *
     * @param serviceLine
     * @param name
     * @param quantity
     * @param comment
     * @param itemId
     * @param type
     * @param variant
     * @param itemTag
     * @param storeId
     * @param imageIds
     */
    public EditItem(String type, String serviceLine, Integer storeId, Object itemTag, Integer quantity, String itemId, String name, Object variant, List<String> imageIds, Object comment) {
        super();
        this.type = type;
        this.serviceLine = serviceLine;
        this.storeId = storeId;
        this.itemTag = itemTag;
        this.quantity = quantity;
        this.itemId = itemId;
        this.name = name;
        this.variant = variant;
        this.imageIds = imageIds;
        this.comment = comment;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public EditItem withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("serviceLine")
    public String getServiceLine() {
        return serviceLine;
    }

    @JsonProperty("serviceLine")
    public void setServiceLine(String serviceLine) {
        this.serviceLine = serviceLine;
    }

    public EditItem withServiceLine(String serviceLine) {
        this.serviceLine = serviceLine;
        return this;
    }

    @JsonProperty("storeId")
    public Integer getStoreId() {
        return storeId;
    }

    @JsonProperty("storeId")
    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public EditItem withStoreId(Integer storeId) {
        this.storeId = storeId;
        return this;
    }

    @JsonProperty("itemTag")
    public Object getItemTag() {
        return itemTag;
    }

    @JsonProperty("itemTag")
    public void setItemTag(Object itemTag) {
        this.itemTag = itemTag;
    }

    public EditItem withItemTag(Object itemTag) {
        this.itemTag = itemTag;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public EditItem withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("itemId")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public EditItem withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public EditItem withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("variant")
    public Object getVariant() {
        return variant;
    }

    @JsonProperty("variant")
    public void setVariant(Object variant) {
        this.variant = variant;
    }

    public EditItem withVariant(Object variant) {
        this.variant = variant;
        return this;
    }

    @JsonProperty("imageIds")
    public List<String> getImageIds() {
        return imageIds;
    }

    @JsonProperty("imageIds")
    public void setImageIds(List<String> imageIds) {
        this.imageIds = imageIds;
    }

    public EditItem withImageIds(List<String> imageIds) {
        this.imageIds = imageIds;
        return this;
    }

    @JsonProperty("comment")
    public Object getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(Object comment) {
        this.comment = comment;
    }

    public EditItem withComment(Object comment) {
        this.comment = comment;
        return this;
    }

    public EditItem addItem(EditItem itemlist) {
        listOfItems.add(itemlist);
        return this;
    }
    public List<EditItem> returnListOfItems(){
        return listOfItems;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("serviceLine", serviceLine).append("storeId", storeId).append("itemTag", itemTag).append("quantity", quantity).append("itemId", itemId).append("name", name).append("variant", variant).append("imageIds", imageIds).append("comment", comment).toString();
    }

}
