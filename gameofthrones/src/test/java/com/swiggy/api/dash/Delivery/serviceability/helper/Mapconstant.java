package com.swiggy.api.dash.Delivery.serviceability.helper;

import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;

import java.util.HashMap;

public class Mapconstant {
    public static String LAT1="28.45243032";
    public static String LNG1="77.00496384";
    public static String CITY_ID="1";
    public static String TAG_1="1";
    public static String DEFAULT_FROM="0";
    public static String DEFAULT_SIZE="10";
    public static String DEFAULT_MAXDISTANCE="10000";
    public static String MAP_SCHEMA_PATH = "/../Data/SchemaSet/Json/Dash/Map/";

    public static String AND="AND";
    public static String OR="OR";
    public static String NAME_CATEGORY="categoryId";
    public static String NAME_SUBCATEGORY="subCategoryId";
    public static String NAME_TAGID="areaId";

    public static HashMap<String,Integer> getCategoryIds(){
        HashMap<String,Integer> categorymapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        categorymapping.put("Bakery",dashAvailabilityHelper.createCategory("Bakery"));
        categorymapping.put("Book store",dashAvailabilityHelper.createCategory("Book store"));
        categorymapping.put("Vegetables shop",dashAvailabilityHelper.createCategory("Vegetables shop"));
        categorymapping.put("Grocery store",dashAvailabilityHelper.createCategory("Grocery store"));
        categorymapping.put("Liquor stores",dashAvailabilityHelper.createCategory("Liquor stores"));
        categorymapping.put("Grocery store",dashAvailabilityHelper.createCategory("Grocery store"));
        categorymapping.put("Pharmacy",dashAvailabilityHelper.createCategory("Pharmacy"));
        categorymapping.put("Supermarkets",dashAvailabilityHelper.createCategory("Supermarkets"));
        categorymapping.put("Fruit shops",dashAvailabilityHelper.createCategory("Fruit shops"));


        return categorymapping;
    }

    public static HashMap<String,Integer> getSubCategoryIds(){
        HashMap<String,Integer> subcategorymapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        subcategorymapping.put("Whisky",dashAvailabilityHelper.createSubCategory("Whisky"));
        subcategorymapping.put("medicine",dashAvailabilityHelper.createSubCategory("medicine"));
        subcategorymapping.put("fruit",dashAvailabilityHelper.createSubCategory("fruit"));
        subcategorymapping.put("cigarette",dashAvailabilityHelper.createSubCategory("cigarette"));
        subcategorymapping.put("Birthdaygift",dashAvailabilityHelper.createSubCategory("Birthdaygift"));
        subcategorymapping.put("Bread",dashAvailabilityHelper.createSubCategory("Bread"));
        subcategorymapping.put("Samsung phones",dashAvailabilityHelper.createSubCategory("Samsung phones"));


        return subcategorymapping;
    }

    public static HashMap<String,Integer> getTagsId(){
        HashMap<String,Integer> tagMapping=new HashMap<>();
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();

        tagMapping.put("50% off on samsungphone",dashAvailabilityHelper.createTag("50% off on samsungphone"));


        return tagMapping;
    }






}
