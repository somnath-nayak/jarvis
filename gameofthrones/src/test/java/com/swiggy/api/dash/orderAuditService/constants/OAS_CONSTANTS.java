package com.swiggy.api.dash.orderAuditService.constants;

public interface OAS_CONSTANTS {
    /** API Details **/
    String LOGS_BY_ORDER_ID = "order_logs_by_id";
    String LOGS_BY_ORDER_ID_UPDATETYPE = "order_logs_by_id_with_updatetype";
    String LOGS_BY_GROUPORDER_ID = "order_logs_by_group";
    String LOGS_BY_GROUPORDER_UPDATETYPE = "order_logs_by_group_with_updatetype";
    String ACTIVITY = "activity_logs";

    /**Chaos Functional Tests **/
    String SERVICE_NAME_CHAOS = "order_audit_service_chaos";
    String MYSQL = "mysql";

    /** Service NAME **/
    String SERVICE_NAME = "order_audit_service";

    /** Array of status types **/
    String[] STATUS_LIST = {
            "UPDATE_A", "UPDATE_B" , "UPDATE_C", "UPDATE_D", "UPDATE_E",
            "UPDATE_F", "UPDATE_G" , "UPDATE_H", "UPDATE_I", "UPDATE_J",
            "UPDATE_K", "UPDATE_L" , "UPDATE_M", "UPDATE_N", "UPDATE_O",
            "UPDATE_P", "UPDATE_Q" , "UPDATE_R", "UPDATE_S", "UPDATE_T",
            "UPDATE_U", "UPDATE_V" , "UPDATE_W", "UPDATE_X", "UPDATE_Y"
    };

    /** Array of updater types **/
    String[] UPDATER_TYPES = {
            "USER_A", "USER_B" , "USER_C", "USER_D", "USER_E",
            "USER_F", "USER_G" , "USER_H", "USER_I", "USER_J"
    };

    /** kafka  name **/
    String AUDIT_KAFKA = "kmskafka";
    String AUDIT_TOPIC_NAME = "order-update";
    String AUDIT_RETRY_QUEUE = "retryQueue";

    /** MySql **/
    String MYSQL_AUDIT_TEMPLATE = "order_audit_service";

    /** Mysql Actions **/
    String MYSQL_START = "start";
    String MYSQL_STOP = "stop";
    String MYSQL_RESTART = "restart";
}


