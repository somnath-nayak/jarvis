
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.squareup.moshi.Json;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.JobType;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "areaId",
        "cityId",
        "id",
        "jobType",
        "metaData",
        "orderId",
        "serviceLineId",
        "slaInMins",
        "tasks",
        "metaData",


})
public class UpdateJob {

    @JsonProperty("jobType")
    private JobType jobType;
    @JsonProperty("slaInMins")
    private Integer slaInMins;
    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("id")
    private int id;
    @JsonProperty("cityId")
    private Integer cityId;
    @JsonProperty("tasks")
    private List<Task> tasks = null;
    @JsonProperty("metaData")
    private MetaDataforJob metaData;
    @JsonProperty("serviceLineId")
    private Integer serviceLineId;
    @JsonProperty("areaId")
    private Integer areaId;

    @JsonProperty("jobType")
    public JobType getJobType() {
        return jobType;
    }

    @JsonProperty("jobType")
    public void setJobType(JobType jobType) {
        this.jobType = jobType;
    }

    @JsonProperty("slaInMins")
    public Integer getSlaInMins() {
        return slaInMins;
    }

    @JsonProperty("slaInMins")
    public void setSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
    }



    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("tasks")
    public List<Task> getTasks() {
        return tasks;
    }

    @JsonProperty("tasks")
    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }


    @JsonProperty("metaData")
    public MetaDataforJob getMetaData() {
        return metaData;
    }

    @JsonProperty("metaData")
    public void setMetaData(MetaDataforJob metaData) {
        this.metaData = metaData;
    }



    @JsonProperty("cityId")
    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }


    public Integer getCityId() {
        return cityId;
    }

    @JsonProperty("serviceLineId")

    public void setServiceLineId(Integer serviceLineId) {
        this.serviceLineId = serviceLineId;
    }

    @JsonProperty("serviceLineId")
    public Integer getServiceLineId()
    {
        return serviceLineId;
    }


    @JsonProperty("areaId")
    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    @JsonProperty("areaId")

    public Integer getAreaId() {
        return areaId;
    }

    public static UpdateJob setDefaultUpdateJobData(String orderId)
    {

        UpdateJob updateJob = new UpdateJob();
        updateJob.setCityId(DashDeliveryConstant.CITY_ID_GURGOAN);
        updateJob.setAreaId(DashDeliveryConstant.AREA_ID);
        updateJob.setSlaInMins(DashDeliveryConstant.DEFAULT_SLA);
        updateJob.setTasks(Task.setDefaultTask());
        updateJob.setOrderId(orderId);
        updateJob.setMetaData(MetaDataforJob.defaultMetaDataForJob());
        updateJob.setServiceLineId(DashDeliveryConstant.SERVICE_LINE_ID);
        updateJob.setOrderId(orderId);
        updateJob.setJobType(JobType.BUY);

        return updateJob;

    }




}
