package com.swiggy.api.dash.viewservice.test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.lms.constants.LMS;
import com.swiggy.api.dash.viewservice.helper.ErrorMessage;
import com.swiggy.api.dash.viewservice.helper.StoreIdentifier;
import com.swiggy.api.dash.viewservice.helper.Viewserviceconstant;
import com.swiggy.api.dash.viewservice.helper.Viewservicehelper;
import com.swiggy.api.dash.viewservice.models.Createviewpojo;
import com.swiggy.api.dash.viewservice.models.Filter;
import com.swiggy.api.dash.viewservice.models.TradeDiscountInfo;
import com.swiggy.api.erp.finance.pojo.BlockUnblockDe;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import kafka.security.auth.Create;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.http.HTTP;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

public class Viewservicetest {

    JsonHelper helper = new JsonHelper();
    HttpCore core = new HttpCore(Viewserviceconstant.VIEWSERVICE);
    JsonHelper jsonHelper=new JsonHelper();
    Viewservicehelper viewservicehelper=new Viewservicehelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();



    @Test(description = "create dashview", dataProviderClass = Viewservicedp.class, dataProvider = "createviewdata",priority = 0)
    public void createview(Createviewpojo view, int responseCode, ErrorMessage errorMessage) throws Exception{
        String apiname="createdashview";
        String payload=jsonHelper.getObjectToJSON(view);
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        Assert.assertEquals(response.getResponseCode(),responseCode);
        Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.mainMessage",String.class), Viewserviceconstant.hm.get(errorMessage));
        if(errorMessage.equals(ErrorMessage.MININUM_STORE_NOT_AVAILABLE)){
            Assert.assertNotNull(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data",Integer.class));
        }
    }
    @Test(description = "update dashview", dataProviderClass = Viewservicedp.class, dataProvider = "updateDashView",priority = 0)
    public void updateDashView(int viewId, String whatToUpdate, Createviewpojo createviewpojo) throws Exception{
        HttpResponse response;
        HttpResponse response_detailview;
        String apiname="updatedashview";
        switch (whatToUpdate){
            case "VIEWNAME":
                String updatedViewName="UpdateViewName";
                createviewpojo.setId(viewId);
                createviewpojo.setViewName(updatedViewName);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.name",String.class),updatedViewName);
                break;
            case "VIEWTYPE":
                String updatedViewType="UpdatedViewType";
                createviewpojo.setId(viewId);
                createviewpojo.setViewType(updatedViewType);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.type",String.class),updatedViewType);
                break;
            case "VIEWTITLE":
                String updatedViewTitle="UpdatedViewTitle";
                createviewpojo.setId(viewId);
                createviewpojo.setViewTitle(updatedViewTitle);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.title",String.class),updatedViewTitle);
                break;
            case "VISIBILITY":
                boolean visibility=false;
                createviewpojo.setId(viewId);
                createviewpojo.setVisible(visibility);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.visible",Boolean.class),Boolean.valueOf(visibility));
                break;
            case "STOREIDENTIFIER_TAG":
                createviewpojo.setId(viewId);
                createviewpojo.setStoreIdentifier(StoreIdentifier.TAG.toString());
                createviewpojo.setTags(asList(1,2));
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.storeIdentifier",String.class),StoreIdentifier.TAG.toString());
                break;

            case "STOREIDENTIFIER_STORE":
                ArrayList<Integer> storeList=new ArrayList<>(asList(1,2));

                createviewpojo.setId(viewId);
                createviewpojo.setStoreIdentifier(StoreIdentifier.STORE.toString());
                createviewpojo.setStoreIds(storeList);

                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.storeIdentifier",String.class),StoreIdentifier.STORE.toString());
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.stores.length()",Integer.class),Integer.valueOf(storeList.size()));
                Assert.assertTrue(CollectionUtils.isEqualCollection(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.stores[*].storeId",ArrayList.class),storeList));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.stores[0].dashViewId",Integer.class),Integer.valueOf(viewId));

            case "ADDCATEGORIES":
                createviewpojo.setId(viewId);
                List<Integer> categories=createviewpojo.getCategories();
                categories.add(10);
                categories.add(11);
                createviewpojo.setStoreIdentifier(StoreIdentifier.CATEGORY.toString());
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                System.out.println(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.categories",String.class));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.categories",String.class), categories.stream().map(Object::toString).collect(Collectors.joining(",")));
                break;

            case "REDUCECATEGORIES":
                createviewpojo.setId(viewId);
                List categoriespresent=createviewpojo.getCategories();
                categoriespresent.remove(0);
                createviewpojo.setStoreIdentifier(StoreIdentifier.CATEGORY.toString());
                createviewpojo.setStoreIds(categoriespresent);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.storeIdentifier",String.class),StoreIdentifier.CATEGORY.toString());
                break;
            case "ADDSUBCATEGORIES" :
                createviewpojo.setId(viewId);
                List subcategory=createviewpojo.getSubCategories();
                if(subcategory==null) {
                    subcategory=new ArrayList();
                }
                subcategory.add(10);
                subcategory.add(11);
                createviewpojo.setStoreIdentifier(StoreIdentifier.CATEGORY.toString());
                createviewpojo.setSubCategories(subcategory);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.storeIdentifier",String.class),StoreIdentifier.CATEGORY.toString());
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.subCategories",String.class), subcategory.stream().map(Object::toString).collect(Collectors.joining(",")));
                break;
            case "ADDTAGS":
                createviewpojo.setId(viewId);
                List tags=createviewpojo.getSubCategories();
                if(tags==null) {
                    tags=new ArrayList();
                }
                tags.add(10);
                tags.add(11);
                createviewpojo.setStoreIdentifier(StoreIdentifier.TAG.toString());
                createviewpojo.setTags(tags);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.storeIdentifier",String.class),StoreIdentifier.TAG.toString());
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.tags",String.class), tags.stream().map(Object::toString).collect(Collectors.joining(",")));
                break;
            case "ADDFILTER":
                createviewpojo.setId(viewId);
                List<Filter> filters=createviewpojo.getFilters();
                if(filters==null) {
                    filters=new ArrayList();
                }
                filters.add(new Filter().withName("test").withTagId(12));
                filters.add(new Filter().withName("testhello").withTagId(13));
                createviewpojo.setFilters(filters);
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.filterEntities.length()",Integer.class),Integer.valueOf(filters.size()));
                Assert.assertTrue(CollectionUtils.isEqualCollection(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.filterEntities[*].tagId",ArrayList.class),filters.stream().map(Filter::getTagId).collect(Collectors.toList())));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.filterEntities[0].dashViewId",Integer.class),Integer.valueOf(viewId));
                break;
            case "ADDTRADEDISCOUNT":
                String updateDiscounType="Freebie";
                createviewpojo.setId(viewId);
                createviewpojo.setTradeDiscountInfo(new TradeDiscountInfo().withDiscountType(updateDiscounType).withDiscountValue("1"));
                response=core.invokePostWithStringJson(apiname,null,jsonHelper.getObjectToJSON(createviewpojo));
                response_detailview=viewservicehelper.getDashViewDetail(String.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.tradeDiscountEntities.length()",Integer.class),Integer.valueOf(1));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.tradeDiscountEntities[0].dashViewId",Integer.class),Integer.valueOf(viewId));
                Assert.assertEquals(AutomationHelper.getDataFromJson(response_detailview.getResponseMsg(),"$.data.tradeDiscountEntities[0].tradeDiscountType",String.class),updateDiscounType);

        }


    }
    @Test(description = "get all dash view ",priority = 1)
    public void getAllDashView() throws Exception{
        String apiname="getalldashview";
        HttpResponse response=core.invoker(apiname,null);
        Assert.assertEquals(response.getResponseCode(),200);
        Assert.assertTrue( schema.validateSchema(viewservicehelper.getAbsolutePathOfSchema("getalldashview"),response.getResponseMsg() ), "Schema validation failed");
    }
    @Test(description = "Get dashview by Id", dataProviderClass = Viewservicedp.class, dataProvider = "getDashViewById",priority = 2)
    public void getDashViewById(String id,int internalStatusCode,Enum code) throws Exception{
        String apiname="getdashviewbyid";
        HttpResponse response=core.invoker(apiname,null,new String[]{id});
        Assert.assertEquals(internalStatusCode,AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.internalStatusCode",Integer.class),internalStatusCode);
        Assert.assertEquals(Viewserviceconstant.hm.get(code),AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.mainMessage",String.class));
    }
    @Test(description = "get dashview using pagination", dataProviderClass = Viewservicedp.class, dataProvider = "DashViewPaginationData",priority = 3)
    public void getDashViewPagination(int pagenumber,int pagesize,int total) throws Exception{
        int totalPage=getTotalPage(pagesize,total);
        String apiname="getdashviewpage";
        if(pagenumber<totalPage) {
            for (int i = pagenumber; i < totalPage; i++) {
                HttpResponse response = core.invoker(apiname, null, new String[]{String.valueOf(i), String.valueOf(pagesize)});
                Assert.assertEquals(response.getResponseCode(),200);
                ArrayList<Integer> al=AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.dashViewEntities.[*].id",ArrayList.class);

            }
        }
        else{
            HttpResponse response = core.invoker(apiname, null, new String[]{String.valueOf(pagenumber), String.valueOf(pagesize)});
            System.out.println(response);
            Assert.assertEquals(response.getResponseCode(),200);
            Assert.assertEquals(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.dashViewEntities.[*]",ArrayList.class).size(),0);
        }
    }

    @Test(description = "get catgory list by view id",dataProviderClass = Viewservicedp.class,dataProvider = "dashviewcategory")
    public void verifyGetCategoryListById(int id,ArrayList<String> categories){
        System.out.println(categories);
        String apiname="getviewcategorybyviewid";
        HttpResponse response=core.invoker(apiname,null,new String[]{String.valueOf(id)});
        System.out.println(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data."+id,ArrayList.class));
        Assert.assertTrue(CollectionUtils.isEqualCollection(AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data."+id,ArrayList.class),categories));

    }


    public int getTotalPage(int pagesize,int total){
        int totalPage=0;
        totalPage=totalPage+total/pagesize+(total%pagesize>0?1:0);
        System.out.println(totalPage);
        return totalPage;
    }

    public boolean hasDuplicate(ArrayList<Integer> list) {
        Set<Integer> appeared = new HashSet<>();
        for (int item : list) {
            if (!appeared.add(item)) {
                return true;
            }
        }
        return false;
    }











}
