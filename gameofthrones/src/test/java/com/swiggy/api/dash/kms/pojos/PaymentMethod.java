
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name"
})
public class PaymentMethod {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    private List<PaymentMethod> listOfPaymentMethods = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public PaymentMethod() {
    }

    /**
     * 
     * @param id
     * @param name
     */
    public PaymentMethod(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public PaymentMethod setId(Integer id) {
        this.id = id;
        return this;
    }

    public PaymentMethod withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public PaymentMethod setName(String name) {
        this.name = name;
        return this;
    }

    public PaymentMethod withName(String name) {
        this.name = name;
        return this;
    }

    public PaymentMethod addPaymentMethod(PaymentMethod newMethod){
        listOfPaymentMethods.add(newMethod);
        return this;
    }

    public List<PaymentMethod> returnListOfPaymentMethods(){
        return listOfPaymentMethods;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }

}
