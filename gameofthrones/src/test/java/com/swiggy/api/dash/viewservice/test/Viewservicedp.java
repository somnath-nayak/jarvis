package com.swiggy.api.dash.viewservice.test;

import com.swiggy.api.dash.Availability.helper.CreateStoresForAvailablity;
import com.swiggy.api.dash.Availability.helper.DashAvailabilityHelper;
import com.swiggy.api.dash.DashContant;
import com.swiggy.api.dash.Delivery.serviceability.helper.Mapconstant;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.kms.pojos.CategoryBuilder;
import com.swiggy.api.dash.kms.pojos.Tags;
import com.swiggy.api.dash.viewservice.helper.*;
import com.swiggy.api.dash.viewservice.models.Createviewpojo;
import com.swiggy.api.dash.viewservice.models.Filter;
import com.swiggy.api.dash.viewservice.models.TradeDiscountInfo;
import com.swiggy.api.sf.snd.pojo.Tag;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import javax.swing.text.View;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public class Viewservicedp {

    DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
    CreateStoresForAvailablity createStoresForAvailablity=new CreateStoresForAvailablity();
    Viewservicehelper viewservicehelper=new Viewservicehelper();
    HttpCore core = new HttpCore(Viewserviceconstant.VIEWSERVICE);
    JsonHelper jsonHelper=new JsonHelper();

    HashMap<String,Integer> nameIdMapping=new HashMap<>();
    @DataProvider(name = "createviewdata", parallel = false)
    public Object[][] availabilitydata() throws Exception{
        ArrayList<String> storeList = Stream.of("Store1","Store2","Store3","Store4","Store5","Store6","Store7","Store8","Store9","Store10").collect(Collectors.toCollection(ArrayList::new));
        HashMap<String,Integer> categoriesId= DashContant.getCategoryIds();
        HashMap<String,Integer> subCategoriesId= DashContant.getSubCategoryIds();
        HashMap<String,Integer> tagId= DashContant.getTagIds();


        List<CategoryBuilder> categories_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(DashContant.getCategoryIds().get(DashContant.CATEGORY_BAKERY) , DashContant.CATEGORY_BAKERY)).returnListOfCategories();
        List<Tags> subcategory=new Tags().addNewTag(new Tags(subCategoriesId.get(DashContant.SUBCATEGORY_BIRTHDAYGIFT),DashContant.SUBCATEGORY_BIRTHDAYGIFT)).returnListOfTags();
        List<Tags> tags = new Tags().addNewTag(new Tags(tagId.get(DashContant.TAG_KIDSWEAR),DashContant.TAG_KIDSWEAR)).returnListOfTags();

        ArrayList<Integer> listfilterId=new ArrayList<>(asList(DashContant.getTagIds().get(DashContant.TAG_DIWALI)));

        HashMap<String,Integer> storeAvailibility=dashAvailabilityHelper.isStoreAvailable(storeList);
        createStoresForAvailablity.createStoreForMaps(storeList.get(0),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(1),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(2),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(3),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(4),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(5),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(6),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(7),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(8),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));
        createStoresForAvailablity.createStoreForMaps(storeList.get(9),DashContant.LATLONG_1,DashContant.AREAID_1,DashContant.AREANAME_1,DashContant.CITYID_1,DashContant.CITYNAME_1,1,categories_1,subcategory,tags,storeAvailibility.get(storeList.get(0)));


        ArrayList categoryArrayList_1=new ArrayList<Integer>(asList(categoriesId.get(DashContant.CATEGORY_BAKERY)));
        ArrayList categoryArrayList_2=new ArrayList<Integer>(asList(categoriesId.get(DashContant.CATEGORY_BAKERY),categoriesId.get(DashContant.CATEGORY_BOOKSTORE)));
        ArrayList categoryArrayList_3=new ArrayList<Integer>(asList(categoriesId.get(DashContant.CATEGORY_BAKERY),categoriesId.get(DashContant.CATEGORY_BOOKSTORE),categoriesId.get(DashContant.CATEGORY_SUPERMARKET)));

        ArrayList subcategoryArrayList_1=new ArrayList<Integer>(asList(subCategoriesId.get(DashContant.SUBCATEGORY_BIRTHDAYGIFT)));
        ArrayList subcategoryArrayList_2=new ArrayList<Integer>(asList(subCategoriesId.get(DashContant.SUBCATEGORY_MEDICINE),subCategoriesId.get(DashContant.CATEGORY_BOOKSTORE)));
        ArrayList subcategoryArrayList_3=new ArrayList<Integer>(asList(subCategoriesId.get(DashContant.SUBCATEGORY_BREAD),subCategoriesId.get(DashContant.CATEGORY_BOOKSTORE),categoriesId.get(DashContant.CATEGORY_SUPERMARKET)));

        ArrayList tagArrayList_1=new ArrayList<Integer>(asList(tagId.get(DashContant.TAG_DIWALI)));
        ArrayList tagArrayList_2=new ArrayList<Integer>(asList(tagId.get(DashContant.TAG_DIWALI),tagId.get(DashContant.TAG_FRUITSPECIAL)));
        ArrayList tagArrayList_3=new ArrayList<Integer>(asList(tagId.get(DashContant.TAG_DIWALI),tagId.get(DashContant.TAG_FRUITSPECIAL),tagId.get(DashContant.TAG_KIDSWEAR)));


        return new Object[][]{

                //Combination of categoy,subcategory,tags
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,subcategoryArrayList_1,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,null,subcategoryArrayList_1,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_3,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,null,subcategoryArrayList_2,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,null,subcategoryArrayList_3,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,subcategoryArrayList_2,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_3,subcategoryArrayList_3,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.TAG,null,null,tagArrayList_1,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.TAG,null,null,tagArrayList_2,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.TAG,null,null,tagArrayList_3,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_1,subcategoryArrayList_1,tagArrayList_1,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_1,subcategoryArrayList_1,tagArrayList_1,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},

                // Error for store identifier wrong
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,null,null,tagArrayList_1,listfilterId,null),200, ErrorMessage.STORE_IDENTIFIER_WRONG},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.TAG,categoryArrayList_1,subcategoryArrayList_1,null,listfilterId,null),200, ErrorMessage.STORE_IDENTIFIER_WRONG},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.TAG,null,subcategoryArrayList_1,null,listfilterId,null),200, ErrorMessage.STORE_IDENTIFIER_WRONG},
                { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.STORE,null,subcategoryArrayList_1,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},


                  //variout discount type
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,false,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Freebie.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Flat.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.BXGY.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.FREE_DELIVERY.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},

                  // viewname or type or title not present
                  { createView("",Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_NAME_NOT_AVAILABLE},
                  { createView(Viewserviceconstant.VIEW_NAME,"",Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_TITLE_NOT_AVAILABLE},
                  { createView(Viewserviceconstant.VIEW_NAME, Viewserviceconstant.VIEW_TITLE,"",true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,listfilterId,null),200, ErrorMessage.VIEW_TYPE_NOT_AVAILABEL},

                  //2 filter
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,false,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.CATEGORY,categoryArrayList_2,null,null,new ArrayList<>(asList(1,2)),null),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},
                //  { createView("viewname","viewtitle","viewType",false,"Percentage","mukesh.kabra@swiggy.in","category",new ArrayList<Integer>(asList(category1,category2)),null,null,null),200, ErrorMessage.VIEW_TYPE_NOT_AVAILABEL},

                   // storeIds
                  { createView(Viewserviceconstant.VIEW_NAME,Viewserviceconstant.VIEW_TITLE,Viewserviceconstant.VIEW_TYPE,true,DiscountType.Percentage.toString(),Viewserviceconstant.VIEW_CREATEDBY,StoreIdentifier.STORE,null,null,null,listfilterId,new ArrayList<Integer>(asList(1,2,3))),200, ErrorMessage.VIEW_CREATED_SUCCESSFULLY},





        };
    }

    @DataProvider(name = "getDashViewById", parallel = false)
    public Object[][] DashViewById() throws Exception{
        return new Object[][]{
                {"1",0,ErrorMessage.SUCCESSFULLY_DONE},
                {"13213112",1,ErrorMessage.VIEW_NOT_FOUND}
        };
    }
    @DataProvider(name = "DashViewPaginationData", parallel = false)
    public Object[][] DashViewByPagination() throws Exception{
        int totalView=viewservicehelper.getTotalNumberOfView();
        System.out.println(totalView);
        return new Object[][]{
                {0,20,totalView},
                {100,20,totalView},
                {0,totalView,totalView}

        };
    }

    @DataProvider(name = "updateDashView", parallel = false)
    public Object[][] updateDashView() throws Exception{
        ArrayList<Integer> listfilterId=new ArrayList<>(asList(DashContant.getTagIds().get(DashContant.TAG_DIWALI)));
        Createviewpojo createviewpojo = createView("viewname","viewtitle","viewType",true,"Percentage","mukesh.kabra@swiggy.in",StoreIdentifier.CATEGORY,new ArrayList<Integer>(asList(DashContant.getCategoryIds().get(DashContant.CATEGORY_BAKERY))),null,null,listfilterId,null);
        int id=createDashViewForUpdate(createviewpojo);
        return new Object[][]{
                {id,"VIEWNAME",createviewpojo},
                {id,"ADDCATEGORIES",createviewpojo},
                {id,"ADDSUBCATEGORIES",createviewpojo},
                {id,"ADDTAGS",createviewpojo},
                {id,"ADDFILTER",createviewpojo},
                {id,"ADDTRADEDISCOUNT",createviewpojo},
                {id,"VIEWTYPE",createviewpojo},
                {id,"VIEWTITLE",createviewpojo},
                {id,"VISIBILITY",createviewpojo},
                {id,"STOREIDENTIFIER_TAG",createviewpojo},
                {id,"STOREIDENTIFIER_STORE",createviewpojo},
                {id,"REDUCECATEGORIES",createviewpojo}




        };
    }

    @DataProvider(name = "dashviewcategory", parallel = false)
    public Object[][] dashviewcategorydata() throws Exception {
        int tag_1 = dashAvailabilityHelper.createTag("Diwali");
        ArrayList<Integer> listfilterId = new ArrayList<>(asList(DashContant.getTagIds().get(DashContant.TAG_DIWALI)));
        Createviewpojo createviewpojo = createView(Viewserviceconstant.VIEW_NAME, Viewserviceconstant.VIEW_TITLE, Viewserviceconstant.VIEW_TYPE, true, DiscountType.Percentage.toString(), Viewserviceconstant.VIEW_CREATEDBY, StoreIdentifier.CATEGORY, new ArrayList<Integer>(asList(DashContant.getCategoryIds().get(DashContant.CATEGORY_SUPERMARKET))), null, null, listfilterId, null);
        return new Object[][]{
                {createDashViewForUpdate(createviewpojo), createviewpojo.getCategories().stream().map(n -> n.toString()).collect(Collectors.toList())},
                {createDashViewForUpdate(createviewpojo.withCategories(asList(1))),new ArrayList<String>(Arrays.asList("1"))},
                {createDashViewForUpdate(createviewpojo.withCategories(null).withSubCategories(asList(1))),new ArrayList<String>(Arrays.asList(""))},
                {createDashViewForUpdate(createviewpojo.withCategories(null).withTags(asList(1))),new ArrayList<String>(Arrays.asList(""))},
                {createDashViewForUpdate(createviewpojo.withstoreIds(asList(1,2))),new ArrayList<String>(Arrays.asList(""))},
                {createDashViewForUpdate(createviewpojo.withCategories(asList(1,2)).withSubCategories(asList(3,4))),new ArrayList<String>(Arrays.asList("1","2"))},



        };
    }





    public Createviewpojo createView(String viewName, String viewTitle, String viewType, boolean visible, String discountType, String createdBy,StoreIdentifier identifier, ArrayList<Integer> category, ArrayList<Integer> subcategory, ArrayList<Integer> tag,ArrayList<Integer> filterId,ArrayList<Integer> storeIds){
        return new Createviewpojo().withCategories(category)
                .withSubCategories(subcategory).withTags(tag)
                .withCityId(3)
                .withTradeDiscountInfo(new TradeDiscountInfo().withDiscountType(discountType).withDiscountValue("1"))
                .withViewName(viewName)
                .withViewTitle(viewTitle)
                .withViewType(viewType)
                .withVisible(visible)
                .withCreatedBy(createdBy)
                .withFilters(filterId.stream().map(id->new Filter().withName("test").withTagId(id)).collect(Collectors.toList()))
                .withStoreIdentifier(identifier.toString())
                .withMinStoreCount("0").withOnboardingMessages(new ArrayList<>(Arrays.asList("Helo")))
                .withstoreIds(storeIds);
    }

    public Integer createDashViewForUpdate(Createviewpojo createviewpojo) throws Exception{

        String apiname="createdashview";
        String payload=jsonHelper.getObjectToJSONallowNullValues(createviewpojo);
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data",Integer.class);
    }

















}
