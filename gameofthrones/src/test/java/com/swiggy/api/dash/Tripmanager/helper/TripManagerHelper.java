package com.swiggy.api.dash.Tripmanager.helper;

import com.swiggy.api.dash.SuperAppDeliveryService.models.Location;
import com.swiggy.api.dash.Tripmanager.models.*;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripManagerHelper {
    HttpCore core = new HttpCore(TripManagerConstant.TRIPMANAGER);
    JsonHelper jsonHelper = new JsonHelper();


    public CreateJob createJobObject(JobType jobType, String orderId, int sla,List<Task> task,MetaDataforJob metadata){
        if(jobType!=null) {
            return new CreateJob().withJobType(jobType.toString()).withOrderId(orderId).withSlaInMins(sla).withMetaData(metadata).withTasks(task);
        }
        else{
            return new CreateJob().withJobType(null).withOrderId(orderId).withSlaInMins(sla).withMetaData(metadata).withTasks(task);

        }

    }

    public CreateJob editJobObject(String jobId,JobType jobType, String orderId, int sla,List<Task> task,MetaDataforJob metadata){
            return new CreateJob().withJobType(jobType.toString()).withOrderId(orderId).withSlaInMins(sla).withMetaData(metadata).withTasks(task).withId(jobId);


    }


    public Task createTaskObject(TaskType taskType, Address address, int pay, int billvalue, int collect, MetaData metaData){
        Task task=new Task().withType(taskType.toString()).withAddress(address).withPay(pay).withBill(billvalue).withCollect(collect).withMetaData(metaData);
        return task;
    }
    public Task createTaskObject(TaskType taskType, Address address, int pay, int billvalue, int collect, MetaData metaData,int taskId){
        Task task=new Task().withType(taskType.toString()).withAddress(address).withPay(pay).withBill(billvalue).withCollect(collect).withMetaData(metaData).withId(String.valueOf(taskId));
        return task;
    }

    public Address createAddressObject(String name, String primaryContact, String landmark, String flatno , String  addressLine, boolean isVerified,String latlong){
        Address address=new Address().withName(name).withPrimaryContact(primaryContact)
                .withLandmark(landmark)
                .withFlatNo(flatno)
                .withAddressLine(addressLine)
                .withIsVerfied(isVerified)
                .withLocation(latlong);
        return address;
    }

    public MetaData createMetaDataForTask(int quantity){

        List<Addon> addons=new ArrayList<>();
        addons.add(new Addon().withName("addon").withPrice(1).withQuantity(1));

        List<Variant> variant=new ArrayList<>();
        variant.add(new Variant().withName("variant 1").withPrice(1));

        Item item=new Item().withName("Biryani").withQuantity(quantity)
                .withPrice(1).withUrl("www.google.com").withAddons(addons).withVariants(variant);
        List<Item> items=new ArrayList<>();
        items.add(item);


        MetaData metaData =  new MetaData().withBillTolerance(20)
                            .withPayTolerance(20)
                            .withCollectTolerance(20)
                .withQRVerficationRequired(true)
                .withBillMismatchTimeoutInSec(20)
                .withNoBillBlockerTimeoutInSec(20)
                .withVendorIssueConfig("NO_BLOCKER")
                .withInstruction("Take care")
                .withItems(items);

        return metaData;

    }



    public int getTripIdForJobLeg(int jobLegId){
        String apiname="getjoblegbulk";
        HttpResponse response=core.invoker(apiname,null,new String[]{String.valueOf(jobLegId)});
        int tripId= AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data[0].tasks[0].tripId",Integer.class);
        return tripId;

    }

    public HttpResponse assigntripToDe(int tripId,int deId) throws Exception{
        String apiname="assigntriptode";
        AssignTrip assignTrip=new AssignTrip().withTripId(tripId).withDeId(deId).withSource("app");
        String payload=jsonHelper.getObjectToJSON(assignTrip);
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return response;
    }
    public HttpResponse assigntripToDeByJobId(int jobId,int deId) throws Exception{
        String apiname="assigntripToDeByJobId";
        JobAssign assignJob=new JobAssign().withJobId(jobId).withDeId(deId);
        String payload=jsonHelper.getObjectToJSON(assignJob);
        HttpResponse response=core.invokePostWithStringJson(apiname,getTripManagerHeader(JobType.BUY),payload);
        return response;
    }
    public HttpResponse unAssignTripToDeByJobId(int jobId,int deId) throws Exception{
        String apiname="unassigntripToDeByJobId";
        JobAssign assignJob=new JobAssign().withJobId(jobId).withDeId(deId);
        String payload=jsonHelper.getObjectToJSON(assignJob);
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return response;
    }
    public HttpResponse unAssignTripToDe(int tripId,int deId) throws Exception{
        String apiname="unassigntrip";
        AssignTrip assignTrip=new AssignTrip().withTripId(tripId).withDeId(deId).withSource("app");
        String payload=jsonHelper.getObjectToJSON(assignTrip);
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return response;
    }

    public HttpResponse getJobLegBulk(int jobLegId){
        String apiname="getjoblegbulk";
        HttpResponse response=core.invoker(apiname,null,new String[]{String.valueOf(jobLegId)});
        return response;
    }

    public HttpResponse getAssignedJob(int deId,Long lastupdatedTime){
        String apiname="getassignedtrip";
        HttpResponse response=core.invoker(apiname,null,new String[]{String.valueOf(deId),String.valueOf(lastupdatedTime)});
        return response;
    }

    public HttpResponse createJob(JobType jobType,String orderId,int sla,ArrayList<Task> tasks,MetaDataforJob metaData) throws Exception{
        String apiname="createjob";
        String payload=jsonHelper.getObjectToJSONallowNullValues(createJobObject(jobType,orderId,sla,tasks,metaData));
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return response;
    }
    public HttpResponse editJob(String jobId,JobType jobType,String orderId,int sla,ArrayList<Task> tasks,MetaDataforJob metaData) throws Exception{
        String apiname="editjob";
        String payload=jsonHelper.getObjectToJSONallowNullValues(editJobObject(jobId,jobType,orderId,sla,tasks,metaData));
        HttpResponse response=core.invokePostWithStringJson(apiname,null,payload);
        return response;
    }

    public HttpResponse ackForJobLeg(int deId,int joblegId){


        return null;
    }

    public HttpResponse updatejobLegStatus(JobLegStatus jobLegStatus,int joblegId,int deId,JobType jobType) throws Exception{
        String apiname="updatejoblegstatus";
        Location location=new Location().withLng("12.93").withLng("88.9");
        Joblegstatusupdate joblegstatusupdate=new Joblegstatusupdate().withDeId(deId).withBattery(0).withStatus(jobLegStatus.toString())
                .withId(joblegId).withLocation(location);
        HttpResponse httpResponse=core.invokePostWithStringJson(apiname,getTripManagerHeader(jobType),jsonHelper.getObjectToJSONallowNullValues(joblegstatusupdate));
        return httpResponse;
    }
    public HttpResponse updatejobLegStatusByJobId(JobLegStatus jobLegStatus,int jobId,int deId,JobType jobType) throws Exception{
        String apiname="updatejoblegstatus";
        int jobLegId=(int)getJobLegIdByJobId(jobId);
        Location location=new Location().withLat("12.93").withLng("88.9");

        Joblegstatusupdate joblegstatusupdate=new Joblegstatusupdate().withDeId(deId).withBattery(0).withStatus(jobLegStatus.toString())
                .withId(jobLegId).withLocation(location);
        HttpResponse httpResponse=core.invokePostWithStringJson(apiname,getTripManagerHeader(jobType),jsonHelper.getObjectToJSONallowNullValues(joblegstatusupdate));
        return httpResponse;
    }
    public HttpResponse updateTaskStatus(int taskId,String taskstatus,int pay,int bill,int collect,int deId,JobType jobType ) throws Exception{
        String apiname="taskstatusupdate";
        Location location=new Location().withLng("12.93").withLng("88.9");
        TaskStatusUpdate taskStatusUpdate=new TaskStatusUpdate().withStatus(taskstatus).withBattery(10).withBill(bill).withLocation(location)
                .withCollect(collect).withDeId(deId).withGeoFenceBreached(false).withPay(pay).withId(taskId);
        HttpResponse response=core.invokePostWithStringJson(apiname,getTripManagerHeader(jobType),jsonHelper.getObjectToJSONallowNullValues(taskStatusUpdate));
        return response;
    }


    public HttpResponse cancelJob(int jobId) throws Exception{
        String apiname="canceljob";
        CancelJob cancelJob=new CancelJob().withJobId(jobId).withCancellationReason("").withSource("Automation");
        HttpResponse response=core.invokePostWithStringJson(apiname,getTripManagerHeader(JobType.BUY),jsonHelper.getObjectToJSONallowNullValues(cancelJob));
        return response;
    }

    public int getNextTaskId(int headTaskId,HttpResponse response){
        int taskCount=AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.trips[0].tasks.length()",Integer.class);
        int taskId=0;
        for(int j=0;j<taskCount;j++){
            taskId=AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.trips[0].tasks.["+j+"].id",Integer.class);
            if(taskId==headTaskId){
                try {
                    taskId = AutomationHelper.getDataFromJson(response.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].nextTaskId", Integer.class);
                }
                catch (Exception e){
                    taskId=0;
                }
                finally {
                    break;

                }
            }
        }

        return taskId;
    }

    public HashMap<String,Integer> getTaskBasedOnType(HttpResponse response) {
        HashMap<String, Integer> hm = new HashMap<>();

        int taskCount = AutomationHelper.getDataFromJson(response.getResponseMsg(), "$.data.trips[0].tasks.length()", Integer.class);
        int taskId = 0;
        for (int j = 0; j < taskCount; j++) {
            taskId = AutomationHelper.getDataFromJson(response.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].id", Integer.class);
            String taskType = AutomationHelper.getDataFromJson(response.getResponseMsg(), "$.data.trips[0].tasks.[" + j + "].type", String.class);
            hm.put(taskType, taskId);

        }
        return hm;
    }

    public HttpResponse getEligibleDes(int jobId){
        String apiname="getEligibleDes";
        HttpResponse response=core.invoker(apiname,getTripManagerHeader(JobType.BUY),new String[]{String.valueOf(jobId)});
        return response;

    }
    public HttpResponse getDELocation(int deId){
        String apiname="getDELocation";
        HttpResponse response=core.invoker(apiname,getTripManagerHeader(JobType.BUY),new String[]{String.valueOf(deId)});
        return response;
    }
    public HttpResponse getJobTrackingInfo(int jobId){
        String apiname="jobTracking";
        HttpResponse response=core.invoker(apiname,getTripManagerHeader(JobType.BUY),new String[]{String.valueOf(jobId)});
        return response;
    }




    public void updateJobStatus(int jobId,int deId,int pay,int bill,int collect,Jobstatus jobstatus) throws Exception{
        String apiname="jobstatusupdate";
        Jobstatusupdate jobstatusupdate=new Jobstatusupdate().
                withBill(bill)
                .withPay(pay)
                .withCollect(collect)
                .withStatus(jobstatus)
                .withId(jobId);
        HttpResponse response=core.invokePostWithStringJson(apiname,getTripManagerHeader(JobType.BUY),jsonHelper.getObjectToJSON(jobstatusupdate));
    }



    public HashMap<String,String> getTripManagerHeader(JobType jobType){

        HashMap<String,String> header=new HashMap<>();
        switch (jobType){
            case BUY:
                header.put("Job-Type","BUY");
                header.put("Authorization","Basic YWRtaW46YWRtaW4=");
                break;
            case FOOD:
                header.put("Job-Type","FOOD");
                header.put("Authorization","Basic YWRtaW46YWRtaW4=");
                break;
        }
        return header;
    }





    public List<Long> getTaskIdBasedOnJobId(int jobId){
        String query="select id from task where job_leg_id in (select id from job_leg where job_id='"+jobId+"') ORDER BY id desc";
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("tripmanagerdb");
        Object[] params = new Object[] {jobId};
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        List<Long> l=new ArrayList<>();
        list.stream().forEach(e->l.add((Long) e.get("id")));
        //System.out.println(l);
        return l;

    }

    public Long getDEIdBasedOnJobId(int jobId){
        String query="select trip.de_id from job_leg join task on job_leg.id = task.`job_leg_id` join trip on task.id = trip.head_task_id where job_leg.job_id = "+jobId;
        System.out.println(query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("tripmanagerdb");
        Object[] params = new Object[] {jobId};
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return (Long)list.get(0).get("de_id");

    }

    public long getJobLegIdByJobId(int jobId) {
        String query = "select id from job_leg where job_id=" + jobId;
        System.out.println(query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("tripmanagerdb");
        Object[] params = new Object[]{jobId};
        List<Map<String, Object>> list = sqlTemplate.queryForList(query);
        return (Long) list.get(0).get("id");
    }

    public void getJobLegBoBasedOnClientOrderId(String clientOrderId){
        String query = "select * from job_leg where client_order_id= "+clientOrderId;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("tripmanagerdb");
        Object[] params = new Object[]{clientOrderId};
        JoblegBo object=(JoblegBo)sqlTemplate.queryForObjectRowMapper(query,clientOrderId,new JobLegRowMapper());
        System.out.println(object.getStatus());



    }
    public void getTaskLegBoBasedOnClientOrderId(String clientOrderId){
        String query = "select * from task where job_leg_id= "+clientOrderId;
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("tripmanagerdb");
        Object[] params = new Object[]{clientOrderId};
        List<TaskBo> object= (List<TaskBo>)(Object)  sqlTemplate.queryForObjectRowMapper(query,clientOrderId,new TaskRowMapper());
        System.out.println(object.get(0).getMetaData());
    }


}
