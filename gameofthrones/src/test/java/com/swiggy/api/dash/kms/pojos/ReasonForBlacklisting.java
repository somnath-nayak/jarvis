
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "reason"
})
public class ReasonForBlacklisting {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("reason")
    private String reason;

    List<ReasonForBlacklisting> listAllReason = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public ReasonForBlacklisting() {
    }

    /**
     * 
     * @param id
     * @param reason
     */
    public ReasonForBlacklisting(Integer id, String reason) {
        super();
        this.id = id;
        this.reason = reason;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public ReasonForBlacklisting setId(Integer id) {
        this.id = id;
        return this;
    }

    public ReasonForBlacklisting withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("reason")
    public String getReason() {
        return reason;
    }

    @JsonProperty("reason")
    public ReasonForBlacklisting setReason(String reason) {
        this.reason = reason;
        return this;
    }

    public ReasonForBlacklisting withReason(String reason) {
        this.reason = reason;
        return this;
    }

    public ReasonForBlacklisting addNewReason(ReasonForBlacklisting reason){
        listAllReason.add(reason);
        return this;
    }

    public List<ReasonForBlacklisting> returnAllReason() {
        return listAllReason.subList(0, new Random().nextInt(listAllReason.size()));
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("reason", reason).toString();
    }

}
