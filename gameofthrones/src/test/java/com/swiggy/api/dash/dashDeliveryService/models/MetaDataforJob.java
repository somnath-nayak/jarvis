
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryHelper;
import com.swiggy.automation.common.utils.DateTimeUtils;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({

    "customerNumber",
    "orderedTimeInSec",

})
public class MetaDataforJob {


    @JsonProperty("customerNumber")
    private String customerNumber;

    @JsonProperty("orderedTimeInSec")
    private Long orderedTimeInSec;


    @JsonProperty("customerNumber")
    public String getCustomerNumber() {
        return customerNumber;
    }

    @JsonProperty("customerNumber")
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public MetaDataforJob withCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
        return this;
    }


    @JsonProperty("orderedTimeInSec")
    public Long getOrderedTimeInSec() {
        return orderedTimeInSec;
    }

    @JsonProperty("orderedTimeInSec")
    public void setOrderedTimeInSec(Long orderedTimeInSec) {
        this.orderedTimeInSec = orderedTimeInSec;
    }

    public MetaDataforJob withOrderedTimeInSec(Long orderedTimeInSec) {
        this.orderedTimeInSec = orderedTimeInSec;
        return this;
    }

    public static MetaDataforJob defaultMetaDataForJob()
    {
        MetaDataforJob metaDataforJob = new MetaDataforJob();

        metaDataforJob.setCustomerNumber(DashDeliveryConstant.PHONE);
        metaDataforJob.setOrderedTimeInSec(DateTimeUtils.generateEPOCTime());
        return metaDataforJob;

    }



}
