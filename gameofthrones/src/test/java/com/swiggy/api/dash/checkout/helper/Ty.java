package com.swiggy.api.dash.checkout.helper;

import java.util.function.Function;

@FunctionalInterface
public interface Ty<BO, E>{
    public Function<BO, E> Core(String cartType, String coupontype, String tdType);
}
