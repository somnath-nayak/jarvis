package com.swiggy.api.dash.checkout.pojos;


public class ForceUpdate {


    private String type;

    private Double amount;

    /**
     * No args constructor for use in serialization
     *
     */
    public ForceUpdate() {
    }

    /**
     *
     * @param amount
     * @param type
     */
    public ForceUpdate(String type, Double amount) {
        super();
        this.type = type;
        this.amount = amount;
    }


    public String getType() {
        return type;
    }


    public ForceUpdate setType(String type) {
        this.type = type;
        return this;
    }

    public ForceUpdate withType(String type) {
        this.type = type;
        return this;
    }

    public Double getAmount() {
        return amount;
    }


    public ForceUpdate setAmount(Double amount) {
        this.amount = amount;
        return this;
    }

    public ForceUpdate withAmount(Double amount) {
        this.amount = amount;
        return this;
    }



}
