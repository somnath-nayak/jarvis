package com.swiggy.api.dash.lms.pojos;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class BusinessHours {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("day_id")
    private Integer dayId;

    @JsonProperty("open_24_hours")
    private Integer open24Hours;

    @JsonProperty("open_time")
    private String openTime;

    @JsonProperty("close_time")
    private String closeTime;

    @JsonProperty("lead")
    private Integer lead;

    /**
     * No args constructor for use in serialization
     *
     */
    public BusinessHours() {
    }

    /**
     *
     * @param id
     * @param closeTime
     * @param dayId
     * @param lead
     * @param open24Hours
     * @param openTime
     */
    public BusinessHours(Integer id, Integer dayId, Integer open24Hours, String openTime, String closeTime, Integer lead) {
        super();
        this.id = id;
        this.dayId = dayId;
        this.open24Hours = open24Hours;
        this.openTime = openTime;
        this.closeTime = closeTime;
        this.lead = lead;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public BusinessHours setId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("day_id")
    public Integer getDayId() {
        return dayId;
    }

    @JsonProperty("day_id")
    public BusinessHours setDayId(Integer dayId) {
        this.dayId = dayId;
        return this;
    }

    @JsonProperty("open_24_hours")
    public Integer getOpen24Hours() {
        return open24Hours;
    }

    @JsonProperty("open_24_hours")
    public BusinessHours setOpen24Hours(Integer open24Hours) {
        this.open24Hours = open24Hours;
        return this;
    }

    @JsonProperty("open_time")
    public String getOpenTime() {
        return openTime;
    }

    @JsonProperty("open_time")
    public BusinessHours setOpenTime(String openTime) {
        this.openTime = openTime;
        return this;
    }

    @JsonProperty("close_time")
    public String getCloseTime() {
        return closeTime;
    }

    @JsonProperty("close_time")
    public BusinessHours setCloseTime(String closeTime) {
        this.closeTime = closeTime;
        return this;
    }

    @JsonProperty("lead")
    public Integer getLead() {
        return lead;
    }

    @JsonProperty("lead")
    public BusinessHours setLead(Integer lead) {
        this.lead = lead;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("dayId", dayId).append("open24Hours", open24Hours).append("openTime", openTime).append("closeTime", closeTime).append("lead", lead).toString();
    }
}
