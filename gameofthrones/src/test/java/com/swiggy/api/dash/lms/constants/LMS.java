package com.swiggy.api.dash.lms.constants;

public class LMS {
    public static String LMS_TEMPLATE = "lms";

    public static String SERVICE_NAME = "lms";
    public static String LAT_SOURCE = "12.9325";
    public static String LONG_SOURCE = "77.6037";

    public static String LAT_SOURCE_50M_FROM_SOURCE = "12.9324";
    public static String LONG_SOURCE_50M_FROM_SOURCE = "77.6041";

    public static String LAT_SOURCE_LESS_250M_FROM_SOURCE = "12.9335";
    public static String LONG_SOURCE_LESS_250M_FROM_SOURCE = "77.6047";

    public static String LAT_SOURCE_1000M_FROM_SOURCE = "12.9395";
    public static String LONG_SOURCE_1000M_FROM_SOURCE = "77.6094";

    public static String P_100_NAME_MATCH_1 = "Anand Sweets";
    public static String P_100_NAME_MATCH_2 = "Anand Sweets";

    public static String P_90_NAME_MATCH_1 = "Anand Sweets";
    public static String P_90_NAME_MATCH_2 = "Anand Sweet";

    public static String P_50_NAME_MATCH_1 = "abc rivate limited";
    public static String P_50_NAME_MATCH_2 = "abc pvt ld";

    public static String P_90_TO_98_NAME_MATCH_1 = "pappu halwai";
    public static String P_90_TO_98_NAME_MATCH_2 = "pappu halwayi";

    public static String P_95_ABOVE_NAME_MATCH_1 = "pappu halwai bulanshehr wale";
    public static String P_95_ABOVE_NAME_MATCH_2 = "pappu halwai bulanshehar wale";

    public static String P_100_PHONE_NO_MATCH_1 = "1111111111";
    public static String P_100_PHONE_NO_MATCH_2 = "1111111111";

    public static String KMS_SCHEMA_PATH = "/../Data/SchemaSet/Json/Dash/Lms/";

    public static String USER_NAME = "lms";
    public static String PASSWORD = "Swiggy@123";
    public static String LMS_TOKEN = "eb794915f70920321fce829a87957c769380c199";

    public static String DOWNLOAD_CSV_FIRST_LINE = "id," +
            "source," +
            "name," +
            "phone_number," +
            "lat," +
            "lng," +
            "address," +
            "city," +
            "area," +
            "is_duplicate," +
            "parent_id," +
            "landmark," +
            "email," +
            "website," +
            "category," +
            "subcategory," +
            "popular_category," +
            "popular_subcategory," +
            "suggested_items," +
            "list_category," +
            "tags," +
            "images," +
            "chain," +
            "id_from_source," +
            "source_link," +
            "source_rating," +
            "source_votes," +
            "type," +
            "partner_intent," +
            "holidays," +
            "score,onground_rating," +
            "onground_verified," +
            "tele_verified," +
            "blacklisted," +
            "available_jd," +
            "available_google," +
            "available_swiggy," +
            "available_dunzo," +
            "wait_time," +
            "home_delivery," +
            "payment_methods," +
            "commission," +
            "dedup_status" +
            "sent_to_kms" +
            "id_in_kms";



    /** API Names **/
    public static String CREATE_LEAD = "create_lead";
    public static String UPDATE_LEAD = "update_lead";
    public static String GET_ALL_LEADS = "get_all_leads";
    public static String GET_LEAD_BY_ID = "get_lead_by_id";
    public static String DELETE_LEAD_BY_ID = "delete_lead_by_id";
    public static String GET_DUPLICATE_LEAD_BY_ID = "get_duplicate_lead_by_id";
    public static String GET_ALL_DUPLICATE_LEADS = "get_all_duplicate_leads";
    public static String DELETE_DUPLICATE_LEAD_BY_ID = "delete_duplicate_lead_by_id";
    public static String DOWNLOAD_ALL_DUPLICATE_LEADS = "download_duplicate_leads";
    public static String MARK_DUPLICATE = "mark_duplicate";
    public static String SAMPLE_UPLOAD_FILE = "get_sample_upload_file";
    public static String BULK_UPLOAD = "lms_bulk_upload";
    public static String TASKS_BY_ID = "task_by_id";

    /** Bulk Upload */
    public static String BULK_DUPLICATE_LEADS_NAME = "'A'";
    public static int BULK_DUPLICATE_LEADS_COUNT = 5;

    public static String BULK_UNIQUE_LEADS_NAME = "'B', 'C', 'D', 'E', 'F'";
    public static int BULK_UNIQUE_LEADS_COUNT = 5;

    public static String FEW_INVALID_LEADS_NAME = "'N'";
    public static int FEW_INVALID_LEADS_COUNT = 1;

    /** Request Validation */
    public static int DEFAULT_WAIT_TIME = 5;
}
