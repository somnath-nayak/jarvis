package com.swiggy.api.dash.cms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "key",
        "inventory_construct",
        "meta"
})
public class InventoryBuilder{

    @JsonProperty("key")
    private String key;
    @JsonProperty("inventory_construct")
    private InventoryConstruct inventoryConstruct;
    @JsonProperty("meta")
    private Meta meta;

    /**
     * No args constructor for use in serialization
     *
     */
    public InventoryBuilder() {
    }

    /**
     *
     * @param inventoryConstruct
     * @param meta
     * @param key
     */
    public InventoryBuilder(String key, InventoryConstruct inventoryConstruct, Meta meta) {
        super();
        this.key = key;
        this.inventoryConstruct = inventoryConstruct;
        this.meta = meta;
    }

    @JsonProperty("key")
    public String getKey() {
        return key;
    }

    @JsonProperty("key")
    public InventoryBuilder setKey(String key) {
        this.key = key;
        return this;
    }

    public InventoryBuilder withKey(String key) {
        this.key = key;
        return this;
    }

    @JsonProperty("inventory_construct")
    public InventoryConstruct getInventoryConstruct() {
        return inventoryConstruct;
    }

    @JsonProperty("inventory_construct")
    public InventoryBuilder setInventoryConstruct(InventoryConstruct inventoryConstruct) {
        this.inventoryConstruct = inventoryConstruct;
        return this;
    }

    public InventoryBuilder withInventoryConstruct(InventoryConstruct inventoryConstruct) {
        this.inventoryConstruct = inventoryConstruct;
        return this;
    }

    @JsonProperty("meta")
    public Meta getMeta() {
        return meta;
    }

    @JsonProperty("meta")
    public InventoryBuilder setMeta(Meta meta) {
        this.meta = meta;
        return this;
    }

    public InventoryBuilder withMeta(Meta meta) {
        this.meta = meta;
        return this;
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "inventories",
            "attributes"
    })
    public static class InventoryConstruct extends InventoryBuilder{

        @JsonProperty("inventories")
        private Inventories inventories;
        @JsonProperty("attributes")
        private InventoryConstruct.Attributes attributes;

        /**
         * No args constructor for use in serialization
         *
         */
        public InventoryConstruct() {
        }

        /**
         *
         * @param inventories
         * @param attributes
         */
        public InventoryConstruct(Inventories inventories, InventoryConstruct.Attributes attributes) {
            super();
            this.inventories = inventories;
            this.attributes = attributes;
        }

        @JsonProperty("inventories")
        public Inventories getInventories() {
            return inventories;
        }

        @JsonProperty("inventories")
        public InventoryConstruct setInventories(Inventories inventories) {
            this.inventories = inventories;
            return this;
        }

        public InventoryConstruct withInventories(Inventories inventories) {
            this.inventories = inventories;
            return this;
        }

        @JsonProperty("attributes")
        public InventoryConstruct.Attributes getAttributes() {
            return attributes;
        }

        @JsonProperty("attributes")
        public InventoryConstruct setAttributes(InventoryConstruct.Attributes attributes) {
            this.attributes = attributes;
            return this;
        }

        public InventoryConstruct withAttributes(InventoryConstruct.Attributes attributes) {
            this.attributes = attributes;
            return this;
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Attributes extends InventoryConstruct{
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "quantity"
    })
    public static class Inventories extends InventoryConstruct{

        @JsonProperty("quantity")
        private Integer quantity;

        /**
         * No args constructor for use in serialization
         *
         */
        public Inventories() {
        }

        /**
         *
         * @param quantity
         */
        public Inventories(Integer quantity) {
            super();
            this.quantity = quantity;
        }

        @JsonProperty("quantity")
        public Integer getQuantity() {
            return quantity;
        }

        @JsonProperty("quantity")
        public Inventories setQuantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

        public Inventories withQuantity(Integer quantity) {
            this.quantity = quantity;
            return this;
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "store-id"
    })
    public static class Meta extends InventoryBuilder{

        @JsonProperty("store-id")
        private String storeId;

        /**
         * No args constructor for use in serialization
         *
         */
        public Meta() {
        }

        /**
         *
         * @param storeId
         */
        public Meta(String storeId) {
            super();
            this.storeId = storeId;
        }

        @JsonProperty("store-id")
        public String getStoreId() {
            return storeId;
        }

        @JsonProperty("store-id")
        public Meta setStoreId(String storeId) {
            this.storeId = storeId;
            return this;
        }

        public Meta withStoreId(String storeId) {
            this.storeId = storeId;
            return this;
        }

    }


}