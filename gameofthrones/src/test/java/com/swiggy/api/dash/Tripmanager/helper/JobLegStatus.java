package com.swiggy.api.dash.Tripmanager.helper;

public enum JobLegStatus {
    CREATED,
    ASSIGNED,
    CONFIRMED,
    REJECTED,
    COMPLETED,
    AUTO_REJECTED,

}
