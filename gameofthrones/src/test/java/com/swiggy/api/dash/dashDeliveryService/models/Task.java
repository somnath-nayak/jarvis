
package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.helper.TaskType;

import java.util.ArrayList;

import static java.util.Arrays.asList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "address",
    "metaData",
    "pay",
    "bill",
    "waitTime"
})
public class Task {

    @JsonProperty("type")
    private TaskType type;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("metaData")
    private MetaData metaData;
    @JsonProperty("pay")
    private Double pay;
    @JsonProperty("bill")
    private Double bill;
    @JsonProperty("waitTime")
    private Integer waitTime;


    @JsonProperty("type")
    public TaskType getType()
    {
        return type;
    }

    @JsonProperty("type")
    public void setType(TaskType type) {
        this.type = type;
    }


    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    public Task withAddress(Address address) {
        this.address = address;
        return this;
    }

    @JsonProperty("metaData")
    public MetaData getMetaData() {
        return metaData;
    }

    @JsonProperty("metaData")
    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public Task withMetaData(MetaData metaData) {
        this.metaData = metaData;
        return this;
    }


    @JsonProperty("pay")
    public Double getPay() {
        return pay;
    }

    @JsonProperty("pay")
    public void setPay(Double pay) {
        this.pay = pay;
    }



    @JsonProperty("bill")
    public Double getBill() {
        return bill;
    }

    @JsonProperty("bill")
    public void setBill(Double bill) {
        this.bill = bill;
    }


    @JsonProperty("waitTime")
    public Integer getWaitTime() {
        return waitTime;
    }

    @JsonProperty("waitTime")
    public void setWaitTime(Integer waitTime) {
        this.waitTime = waitTime;
    }


    public static ArrayList<Task> setDefaultTask()
    {

        Task task1 = new Task();
        task1.setWaitTime(DashDeliveryConstant.WAIT_TIME);
        task1.setBill((DashDeliveryConstant.SETBILL));
        task1.setPay((DashDeliveryConstant.SETPAY));
        task1.setMetaData(MetaData.defaultMetaDataForJob());
        task1.setAddress(Address.setDefaultAddress());
        task1.setType(TaskType.PICK_UP);

        Task task2 = new Task();
        task2.setWaitTime(DashDeliveryConstant.WAIT_TIME);
        task1.setBill((DashDeliveryConstant.SETBILL));
        task1.setPay((DashDeliveryConstant.SETPAY));
        task2.setMetaData(MetaData.defaultMetaDataForJob());
        task2.setAddress(Address.setDefaultAddress());
        task2.setType(TaskType.DROP);

        ArrayList<Task> tasks = new ArrayList<>(asList(task1,task2));
        return tasks;


    }
}
