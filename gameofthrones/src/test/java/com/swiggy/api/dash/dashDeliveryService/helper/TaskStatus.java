package com.swiggy.api.dash.dashDeliveryService.helper;

public enum
TaskStatus {
    ARRIVED,
    COMPLETED,
    CREATED,
    NA,
}
