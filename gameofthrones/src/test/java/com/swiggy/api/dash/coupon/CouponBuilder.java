package com.swiggy.api.dash.coupon;

import com.swiggy.api.dash.coupon.pojo.Coupon;
import com.swiggy.api.dash.coupon.pojo.MinAmount;
import com.swiggy.api.dash.coupon.pojo.MinQuantity;
import com.swiggy.api.dash.coupon.pojo.TimeSlot;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class CouponBuilder {

    public Coupon flatCouponBuilder(){
        List<TimeSlot> timeSlot= new ArrayList<>();
        timeSlot.add(new TimeSlot(1000,2359,"ALL"));
        List<String> tnc= new ArrayList<>();
     return new Coupon().setName("test").setCode(UUID.randomUUID().toString().replace("-", "")).setDescription("").setTimeSlot(timeSlot).setImage(null).setTitle("jk").setTnc(tnc).
                setCappingPercentage(-1).setApplicableOn(0).setCouponType("Discount").setIsPrivate(0).setValidFrom("2018-12-23T09:55:36.000Z").setValidTill("2019-12-24T09:55:36.000Z")
                .setUpdatedOn("2019-01-02T09:55:36.000Z").setCreatedOn("2019-01-02T09:55:36.000Z").setTotalAvailable(100).setTotalPerUser(25).setMinAmount(new MinAmount(0))
                .setImage(null).setGlobalOffsetDays(0).setCustomerRestriction(0).setAreaRestriction(0).setCityRestriction(0).setRestaurantRestriction(0).setCategoryRestriction(0).setItemRestriction(0)
                .setDiscountPercentage(0).setDiscountAmount(50).setFreeShipping(0).setFreeGifts(0).setFirstOrderRestriction(0).setPreferredPaymentMethod("AmazonPay,Sodexo")
                .setUserClient(0).setSlotRestriction(0).setCouponBucket("Internal").setCuisineRestriction(0).setDiscountItem("0").setUsageCount(0).setExpiryOffset(-1).setApplicableWithSwiggyMoney(false)
                .setIsBankDiscount(0).setRefundSource("").setPgMessage("").setSupportedIosVersion(1).setCreatedBy("user@swiggy.in").setLogoId("lbx02iaashsnhea5fpwa").setCouponUserTypeId(2).setCouponCoolingTypeId(0)
                .setNumTxnPaymentType(0).setMinQuantity(new MinQuantity()).setCouponDomain("dash");
    }

    public Coupon percentCouponBuilder(){
        List<TimeSlot> timeSlot= new ArrayList<>();
        timeSlot.add(new TimeSlot(1000,2359,"ALL"));
        List<String> tnc= new ArrayList<>();
        return new Coupon().setName("test").setCode(UUID.randomUUID().toString().replace("-", "")).setDescription("Swiggy79").setTimeSlot(timeSlot).setImage(null).setTitle("jk").setTnc(tnc).
                setCappingPercentage(-1).setApplicableOn(0).setCouponType("Discount").setIsPrivate(0).setValidFrom("2018-12-23T09:55:36.000Z").setValidTill("2019-12-24T09:55:36.000Z")
                .setUpdatedOn("2019-01-02T09:55:36.000Z").setCreatedOn("2019-01-02T09:55:36.000Z").setTotalAvailable(100).setTotalPerUser(25).setMinAmount(new MinAmount(0))
                .setImage(null).setGlobalOffsetDays(0).setCustomerRestriction(0).setAreaRestriction(0).setCityRestriction(0).setRestaurantRestriction(0).setCategoryRestriction(0).setItemRestriction(0)
                .setDiscountPercentage(20).setDiscountAmount(50).setFreeShipping(0).setFreeGifts(0).setFirstOrderRestriction(0).setPreferredPaymentMethod("AmazonPay,Sodexo")
                .setUserClient(0).setSlotRestriction(0).setCouponBucket("Internal").setCuisineRestriction(0).setDiscountItem("0").setUsageCount(0).setExpiryOffset(-1).setApplicableWithSwiggyMoney(false)
                .setIsBankDiscount(0).setRefundSource("").setPgMessage("").setSupportedIosVersion(1).setCreatedBy("user@swiggy.in").setLogoId("lbx02iaashsnhea5fpwa").setCouponUserTypeId(0).setCouponCoolingTypeId(0)
                .setNumTxnPaymentType(0).setMinQuantity(new MinQuantity()).setCouponDomain("dash");
    }

    public Coupon flatCouponBuilderUn(int enable, int amount){
        List<TimeSlot> timeSlot= new ArrayList<>();
        timeSlot.add(new TimeSlot(1000,2359,"ALL"));
        List<String> tnc= new ArrayList<>();
        return new Coupon().setName("test").setCode(UUID.randomUUID().toString().replace("-", "")).setDescription("").setTimeSlot(timeSlot).setImage(null).setTitle("jk").setTnc(tnc).
                setCappingPercentage(-1).setApplicableOn(0).setCouponType("Discount").setIsPrivate(0).setValidFrom("2018-12-23T09:55:36.000Z").setValidTill("2019-12-24T09:55:36.000Z")
                .setUpdatedOn("2019-01-02T09:55:36.000Z").setCreatedOn("2019-01-02T09:55:36.000Z").setTotalAvailable(100).setTotalPerUser(25).setMinAmount(new MinAmount(amount))
                .setImage(null).setGlobalOffsetDays(0).setCustomerRestriction(0).setAreaRestriction(0).setCityRestriction(0).setRestaurantRestriction(enable).setCategoryRestriction(0).setItemRestriction(0)
                .setDiscountPercentage(0).setDiscountAmount(50).setFreeShipping(0).setFreeGifts(0).setFirstOrderRestriction(0).setPreferredPaymentMethod("AmazonPay,Sodexo")
                .setUserClient(0).setSlotRestriction(0).setCouponBucket("Internal").setCuisineRestriction(0).setDiscountItem("0").setUsageCount(0).setExpiryOffset(-1).setApplicableWithSwiggyMoney(false)
                .setIsBankDiscount(0).setRefundSource("").setPgMessage("").setSupportedIosVersion(1).setCreatedBy("user@swiggy.in").setLogoId("lbx02iaashsnhea5fpwa").setCouponUserTypeId(2).setCouponCoolingTypeId(0)
                .setNumTxnPaymentType(0).setMinQuantity(new MinQuantity()).setCouponDomain("dash");
    }

    public Coupon percentCouponBuilderUn(int enable, int amount){
        List<TimeSlot> timeSlot= new ArrayList<>();
        timeSlot.add(new TimeSlot(1000,2359,"ALL"));
        List<String> tnc= new ArrayList<>();
        return new Coupon().setName("test").setCode(UUID.randomUUID().toString().replace("-", "")).setDescription("Swiggy79").setTimeSlot(timeSlot).setImage(null).setTitle("jk").setTnc(tnc).
                setCappingPercentage(-1).setApplicableOn(0).setCouponType("Discount").setIsPrivate(0).setValidFrom("2018-12-23T09:55:36.000Z").setValidTill("2019-12-24T09:55:36.000Z")
                .setUpdatedOn("2019-01-02T09:55:36.000Z").setCreatedOn("2019-01-02T09:55:36.000Z").setTotalAvailable(100).setTotalPerUser(25).setMinAmount(new MinAmount(amount))
                .setImage(null).setGlobalOffsetDays(0).setCustomerRestriction(0).setAreaRestriction(0).setCityRestriction(0).setRestaurantRestriction(enable).setCategoryRestriction(0).setItemRestriction(0)
                .setDiscountPercentage(20).setDiscountAmount(50).setFreeShipping(0).setFreeGifts(0).setFirstOrderRestriction(0).setPreferredPaymentMethod("AmazonPay,Sodexo")
                .setUserClient(0).setSlotRestriction(0).setCouponBucket("Internal").setCuisineRestriction(0).setDiscountItem("0").setUsageCount(0).setExpiryOffset(-1).setApplicableWithSwiggyMoney(false)
                .setIsBankDiscount(0).setRefundSource("").setPgMessage("").setSupportedIosVersion(1).setCreatedBy("user@swiggy.in").setLogoId("lbx02iaashsnhea5fpwa").setCouponUserTypeId(0).setCouponCoolingTypeId(0)
                .setNumTxnPaymentType(0).setMinQuantity(new MinQuantity()).setCouponDomain("dash");
    }

}
