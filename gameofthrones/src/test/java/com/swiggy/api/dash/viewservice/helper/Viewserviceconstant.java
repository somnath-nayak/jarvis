package com.swiggy.api.dash.viewservice.helper;

import net.sf.saxon.trans.Err;

import java.util.HashMap;

public class Viewserviceconstant {

        public static HashMap<Enum,String> hm=new HashMap<>();
        static {
            hm.put(ErrorMessage.VIEW_TYPE_NOT_AVAILABEL,"DashView Type is empty");
            hm.put(ErrorMessage.VIEW_NAME_NOT_AVAILABLE,"DashView Name is empty");
            hm.put(ErrorMessage.SUCCESSFULLY_DONE,"successfully done");
            hm.put(ErrorMessage.VIEW_NOT_FOUND,"Dash View not found");
            hm.put(ErrorMessage.VIEW_CREATED_SUCCESSFULLY,"Dash View created successfully!");
            hm.put(ErrorMessage.STORE_IDENTIFIER_WRONG,"Store Identifier is wrong");
            hm.put(ErrorMessage.VIEW_TITLE_NOT_AVAILABLE,"DashView Title is empty");

        }
        public static String VIEWSERVICE="dashviewservice";
        public static String VIEW_SCHEMA_PATH = "/../Data/SchemaSet/Json/Dash/Viewservice/";


        public static String VIEW_NAME="SupermarketView";
        public static String VIEW_TITLE="Supermarket";
        public static String VIEW_TYPE="super";
        public static String VIEW_CREATEDBY="mukesh.kabra@swiggy.in";

}
