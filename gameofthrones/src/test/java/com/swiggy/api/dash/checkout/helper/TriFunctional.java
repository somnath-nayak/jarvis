package com.swiggy.api.dash.checkout.helper;

@FunctionalInterface
public interface TriFunctional<A, B, C, R> {
    R apply(A a, B b, C c);
   /* default <V> TriFunctional<A, B, C, V> andThen( Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (A a, B b, C c) -> after.apply(apply(a, b, c));
    }*/
}