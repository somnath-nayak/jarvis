
package com.swiggy.api.dash.Tripmanager.models;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "instruction",
    "customerNumber",
    "totalDistanceInKm",
    "canReject",
    "isPlaced",
    "withDe",
    "orderedTimeInSec",
    "rejectTimerInSec",
    "incentiveData"
})
public class MetaDataforJob {

    @JsonProperty("instruction")
    private String instruction;
    @JsonProperty("customerNumber")
    private String customerNumber;
    @JsonProperty("totalDistanceInKm")
    private Double totalDistanceInKm;
    @JsonProperty("canReject")
    private Boolean canReject;
    @JsonProperty("isPlaced")
    private Boolean isPlaced;
    @JsonProperty("withDe")
    private Boolean withDe;
    @JsonProperty("orderedTimeInSec")
    private Integer orderedTimeInSec;
    @JsonProperty("rejectTimerInSec")
    private Integer rejectTimerInSec;
    @JsonProperty("incentiveData")
    private List<IncentiveDatum> incentiveData = null;

    @JsonProperty("instruction")
    public String getInstruction() {
        return instruction;
    }

    @JsonProperty("instruction")
    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public MetaDataforJob withInstruction(String instruction) {
        this.instruction = instruction;
        return this;
    }

    @JsonProperty("customerNumber")
    public String getCustomerNumber() {
        return customerNumber;
    }

    @JsonProperty("customerNumber")
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public MetaDataforJob withCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
        return this;
    }

    @JsonProperty("totalDistanceInKm")
    public Double getTotalDistanceInKm() {
        return totalDistanceInKm;
    }

    @JsonProperty("totalDistanceInKm")
    public void setTotalDistanceInKm(Double totalDistanceInKm) {
        this.totalDistanceInKm = totalDistanceInKm;
    }

    public MetaDataforJob withTotalDistanceInKm(Double totalDistanceInKm) {
        this.totalDistanceInKm = totalDistanceInKm;
        return this;
    }

    @JsonProperty("canReject")
    public Boolean getCanReject() {
        return canReject;
    }

    @JsonProperty("canReject")
    public void setCanReject(Boolean canReject) {
        this.canReject = canReject;
    }

    public MetaDataforJob withCanReject(Boolean canReject) {
        this.canReject = canReject;
        return this;
    }

    @JsonProperty("isPlaced")
    public Boolean getIsPlaced() {
        return isPlaced;
    }

    @JsonProperty("isPlaced")
    public void setIsPlaced(Boolean isPlaced) {
        this.isPlaced = isPlaced;
    }

    public MetaDataforJob withIsPlaced(Boolean isPlaced) {
        this.isPlaced = isPlaced;
        return this;
    }

    @JsonProperty("withDe")
    public Boolean getWithDe() {
        return withDe;
    }

    @JsonProperty("withDe")
    public void setWithDe(Boolean withDe) {
        this.withDe = withDe;
    }

    public MetaDataforJob withWithDe(Boolean withDe) {
        this.withDe = withDe;
        return this;
    }

    @JsonProperty("orderedTimeInSec")
    public Integer getOrderedTimeInSec() {
        return orderedTimeInSec;
    }

    @JsonProperty("orderedTimeInSec")
    public void setOrderedTimeInSec(Integer orderedTimeInSec) {
        this.orderedTimeInSec = orderedTimeInSec;
    }

    public MetaDataforJob withOrderedTimeInSec(Integer orderedTimeInSec) {
        this.orderedTimeInSec = orderedTimeInSec;
        return this;
    }

    @JsonProperty("rejectTimerInSec")
    public Integer getRejectTimerInSec() {
        return rejectTimerInSec;
    }

    @JsonProperty("rejectTimerInSec")
    public void setRejectTimerInSec(Integer rejectTimerInSec) {
        this.rejectTimerInSec = rejectTimerInSec;
    }

    public MetaDataforJob withRejectTimerInSec(Integer rejectTimerInSec) {
        this.rejectTimerInSec = rejectTimerInSec;
        return this;
    }

    @JsonProperty("incentiveData")
    public List<IncentiveDatum> getIncentiveData() {
        return incentiveData;
    }

    @JsonProperty("incentiveData")
    public void setIncentiveData(List<IncentiveDatum> incentiveData) {
        this.incentiveData = incentiveData;
    }

    public MetaDataforJob withIncentiveData(List<IncentiveDatum> incentiveData) {
        this.incentiveData = incentiveData;
        return this;
    }

}
