package com.swiggy.api.dash.kms.constants;

public class KMS {
    public static String TOKEN = "c89a239f8a466fd24742a2f18d53cdca3c0dbc2b";
    public static String HEADER_CONTENT_TYPE = "Content-Type";
    public static String HEADER_ACCEPT = "Accept";
    public static String HEADER_AUTH = "Authorization";
    public static String KMS_SCHEMA_PATH = "/../Data/SchemaSet/Json/Dash/Kms/";
    public static String KAFKA_NAME = "dash_kms_kafka";
    public static String STORE_UPDATE_TOPIC = "kmsupdates.store";
    public static String HOLIDAY_SLOT_TOPIC = "kmsupdates.holidayslot";
    public static String CITY_ITEM_BLACKZONE_TOPIC = "kmsupdates.cityitemblackzone";
    public static String AREA_ITEM_BLACKZONE_TOPIC = "kmsupdates.areaitemblackzone";

    public static String SOURCE_SW = "SW";
    public static String SOURCE_JD = "JD";
    public static String SOURCE_GP = "GP";

    public static String SOURCE_SW_PLACE_ID = "AAAA";
    public static String SOURCE_JD_PLACE_ID = "BBBB";
    public static String SOURCE_GP_PLACE_ID = "CCCC";

    public static String WITHDATA_LIST_SOURCE_TO_SWIGGY = "withdata_listSourceToSwiggyMapping";
    public static String WITHOUTDATA_LIST_SOURCE_TO_SWIGGY = "withoutdata_listSourceToSwiggyMapping.json";


}
