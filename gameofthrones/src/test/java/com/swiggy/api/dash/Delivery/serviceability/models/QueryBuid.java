package com.swiggy.api.dash.Delivery.serviceability.models;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;

import java.util.ArrayList;

public class QueryBuid {
    private QueryBuilder filterfield;
    private QueryBuilder shouldfiled;
    private ArrayList<QueryBuilder> list_filterField;
    private ArrayList<QueryBuilder> list_shouldField;
    private QueryBuilder q;

    public QueryBuid(QueryBuilder filterfield,QueryBuilder shouldfiled){
        this.filterfield=filterfield;
        this.shouldfiled=shouldfiled;
    }
    public QueryBuid(ArrayList<QueryBuilder> filterfield, ArrayList<QueryBuilder> shouldfiled){
        System.out.println(filterfield);
        this.list_filterField=filterfield;
        this.list_shouldField=shouldfiled;
    }
    public QueryBuid(QueryBuilder q){
       this.q=q;
    }


    public BoolQueryBuilder setField(BoolQueryBuilder qb){
        qb.filter(filterfield);
       // qb.should(shouldfiled);
        return qb;
    }

    public BoolQueryBuilder setShould(BoolQueryBuilder qb){
        //qb.filter(filterfield);
        qb.should(shouldfiled);
        return qb;
    }
    public BoolQueryBuilder setFieldInList(BoolQueryBuilder qb){
       for(int i=0;i<list_filterField.size();i++){
           qb.filter(list_filterField.get(i));
       }
       return qb;
    }
    public BoolQueryBuilder setShouldInList(BoolQueryBuilder qb){
        for(int i=0;i<list_shouldField.size();i++){
            qb.should(list_shouldField.get(i));
        }
        return qb;
    }
    public void setFilternew(BoolQueryBuilder qb){
        qb.filter(q);
    }





}
