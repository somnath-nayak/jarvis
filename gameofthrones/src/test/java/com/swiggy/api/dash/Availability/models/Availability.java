package com.swiggy.api.dash.Availability.models;

public class Availability {

        private String to;

        private String from;

        private int[] stores;
    private int[] categories;


    public Availability(String from,String to,int[] stores,int[] categories){
            this.from=from;
            this.to=to;
            this.stores=stores;
            this.categories=categories;
        }


    public int[] getCategories() {
        return categories;
    }

    public void setCategories(int[] categories) {
        this.categories = categories;
    }



    public String getTo ()
        {
            return to;
        }

        public void setTo (String to)
        {
            this.to = to;
        }

        public String getFrom ()
        {
            return from;
        }

        public void setFrom (String from)
        {
            this.from = from;
        }

        public int[] getStores ()
        {
            return stores;
        }

        public void setStores (int[] stores)
        {
            this.stores = stores;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [to = "+to+", from = "+from+", stores = "+stores+"]";
        }
    }


