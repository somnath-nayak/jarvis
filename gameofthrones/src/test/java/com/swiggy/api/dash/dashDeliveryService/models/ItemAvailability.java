package com.swiggy.api.dash.dashDeliveryService.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.swiggy.api.dash.dashDeliveryService.helper.DashDeliveryConstant;
import com.swiggy.api.dash.dashDeliveryService.models.ItemsToConfirm;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

public class ItemAvailability {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "id",
            "name",
            "isAvailable",
            "isRemoved",
            "alternatives"
    })
    public class ItemsAvailability {

        @JsonProperty("id")
        private int id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("isAvailable")
        private boolean isAvailable;
        @JsonProperty("isRemoved")
        private boolean isRemoved;
        @JsonProperty("alternatives")
        private List<ItemAlternative> alternatives;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isAvailable() {
            return isAvailable;
        }

        public void setAvailable(boolean available) {
            isAvailable = available;
        }

        public boolean isRemoved() {
            return isRemoved;
        }

        public void setRemoved(boolean removed) {
            isRemoved = removed;
        }

        public List<ItemAlternative> getAlternatives() {
            return alternatives;
        }

        public void setAlternatives(List<ItemAlternative> alternatives) {
            this.alternatives = alternatives;
        }

        public List<ItemsToConfirm> setDefaultItemsToConfirm() {
            ItemsToConfirm itemsToConfirm = new com.swiggy.api.dash.dashDeliveryService.models.ItemsToConfirm();
            itemsToConfirm.setAlternatives(ItemAlternative.setDefaultItemAlternative());
            itemsToConfirm.setId(DashDeliveryConstant.ALTERNATIVE_ITEM_ID);
            itemsToConfirm.setAvailable(false);
            itemsToConfirm.setRemoved(false);
            itemsToConfirm.setName(DashDeliveryConstant.ALTERNATIVE_ITEM_NAME);
            ArrayList<ItemsToConfirm> itemsToConfirmArrayList = new ArrayList<>(asList(itemsToConfirm));
            return itemsToConfirmArrayList;


        }

    }
}