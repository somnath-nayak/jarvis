package com.swiggy.api.dash.Tripmanager.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.swiggy.api.dash.Tripmanager.helper.JobLegStatus;
import com.swiggy.api.dash.Tripmanager.helper.JobType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class JoblegBo {
    private Long id;
    private boolean active;
    private JobLegStatus status = JobLegStatus.CREATED;
    private String createdBy;
    private Date confirmedTime;
    private Date assignedTime;
    private Date createdAt;
    private Date updatedAt;
    private Date ackTime;
    private Integer slaInMins;
    private Long zoneId;
    private Long jobId;
    private JsonNode metaData;
    private String clientOrderId;
    private JobType type;
    private Long cityId;
    private Long areaId;
    private Integer serviceLineId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public JobLegStatus getStatus() {
        return status;
    }

    public void setStatus(JobLegStatus status) {
        this.status = status;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getConfirmedTime() {
        return confirmedTime;
    }

    public void setConfirmedTime(Date confirmedTime) {
        this.confirmedTime = confirmedTime;
    }

    public Date getAssignedTime() {
        return assignedTime;
    }

    public void setAssignedTime(Date assignedTime) {
        this.assignedTime = assignedTime;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getAckTime() {
        return ackTime;
    }

    public void setAckTime(Date ackTime) {
        this.ackTime = ackTime;
    }

    public Integer getSlaInMins() {
        return slaInMins;
    }

    public void setSlaInMins(Integer slaInMins) {
        this.slaInMins = slaInMins;
    }

    public Long getZoneId() {
        return zoneId;
    }

    public void setZoneId(Long zoneId) {
        this.zoneId = zoneId;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public JsonNode getMetaData() {
        return metaData;
    }

    public void setMetaData(JsonNode metaData) {
        this.metaData = metaData;
    }

    public String getClientOrderId() {
        return clientOrderId;
    }

    public void setClientOrderId(String clientOrderId) {
        this.clientOrderId = clientOrderId;
    }

    public JobType getType() {
        return type;
    }

    public void setType(JobType type) {
        this.type = type;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public Long getAreaId() {
        return areaId;
    }

    public void setAreaId(Long areaId) {
        this.areaId = areaId;
    }

    public Integer getServiceLineId() {
        return serviceLineId;
    }

    public void setServiceLineId(Integer serviceLineId) {
        this.serviceLineId = serviceLineId;
    }
}
