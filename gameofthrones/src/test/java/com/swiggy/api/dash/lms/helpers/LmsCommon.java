package com.swiggy.api.dash.lms.helpers;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.lms.constants.LMS;
import com.swiggy.api.dash.lms.dp.LmsDP;
import com.swiggy.api.dash.lms.pojos.CreateLeadPojo;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.*;

public class LmsCommon {
    HttpCore core =  new HttpCore(LMS.SERVICE_NAME);
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    LmsDBManager db = new LmsDBManager();
    LmsDP dp = new LmsDP();

    public static String getAbsolutePathOfSchema(String apiname){
        return LMS.KMS_SCHEMA_PATH + apiname + ".json";
    }

    /**
     *
     * @param duplicateLeadId
     * @param child
     * @param parent
     */
    public void getDuplicateLeadById(int duplicateLeadId, int child, int parent){
        HttpResponse respGetDuplicateLeadById = core.invoker(LMS.GET_DUPLICATE_LEAD_BY_ID, getLmsTokens(), new String[] { String.valueOf( duplicateLeadId ) });
        Assert.assertEquals(respGetDuplicateLeadById.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK");
        Assert.assertEquals(AutomationHelper.getDataFromJson(respGetDuplicateLeadById.getResponseMsg(), "$.child.id", Integer.class), Integer.valueOf(child), "Child ID is not as expected");
        Assert.assertEquals(AutomationHelper.getDataFromJson(respGetDuplicateLeadById.getResponseMsg(), "$.parent.id", Integer.class), Integer.valueOf(parent), "Parent ID is not as expected");
    }

    /**
     *
     * @param duplicateLeadId
     */
    public void getAllDuplicateLeads(int duplicateLeadId){
        HttpResponse respGetDuplicateLeadById = core.invoker(LMS.GET_ALL_DUPLICATE_LEADS, getLmsTokens(), new String[] { String.valueOf( duplicateLeadId ) });
        Assert.assertEquals(respGetDuplicateLeadById.getResponseCode(), HttpStatus.SC_OK, "Status is not 200 OK");
        Assert.assertTrue(schema.validateSchema(LmsCommon.getAbsolutePathOfSchema(LMS.GET_ALL_DUPLICATE_LEADS), respGetDuplicateLeadById.getResponseMsg()), "Schema validation failed");
    }

    /**
     *
     * @param child
     * @param parent
     */
    public void markLeadAsDuplicate(int child, int parent) {
        HttpResponse resp = core.invoker(LMS.MARK_DUPLICATE, getLmsTokens(), new String[] { String.valueOf(child), String.valueOf(parent) }, null);
        Assert.assertEquals(resp.getResponseCode(), HttpStatus.SC_ACCEPTED, "Status is not 202 ACCEPTED");
        Assert.assertEquals(parent, db.getParentFromId(child), "mis match in parent id");
        Assert.assertEquals("d", db.getDeDupeStausFromLead(child), "mis match in dedup_status. leadId :"+child);
        Assert.assertEquals("d", db.getDeDupeStausFromLead(parent), "mis match in dedup_status. lead id: "+parent);
    }

    /**
     *
     * @param dbDetails
     * @param request
     */
    public void verifyDBWithRequest(Map<String, Object> dbDetails, CreateLeadPojo request) {
        SoftAssert AssertSoft = new SoftAssert();
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.NAME), request.getName(), "Mismatch in name");
        if(dbDetails.get(LmsDBManager.APILead.PHONE_NUMBER) == null || dbDetails.get(LmsDBManager.APILead.PHONE_NUMBER).toString().isEmpty())
            AssertSoft.assertTrue(request.getPhoneNumber().isEmpty(), "Mismatch in phone number");
        else
            AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.PHONE_NUMBER), request.getPhoneNumber(), "Mismatch in phone number");
        AssertSoft.assertTrue(dbDetails.get(LmsDBManager.APILead.LAT).toString().contains(request.getLat()), "Mismatch in LAT");
        AssertSoft.assertTrue(dbDetails.get(LmsDBManager.APILead.LNG).toString().contains(request.getLng()), "Mismatch in LNG");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.ADDRESS), request.getAddress(), "Mismatch in address");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.LANDMARK), request.getLandmark(), "Mismatch in Landmark");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.EMAIL), request.getEmail(), "Mismatch in Email");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.WEBSITE), request.getWebsite(), "Mismatch in WebSite");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.CATEGORY), request.getCategory(), "Mismatch in category");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SUB_CATEGORY), request.getSubcategory(), "Mismatch in sub category");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.POPULAR_CATEGORY), request.getPopularCategory(), "Mismatch in getPopularCategory");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.POPULAR_SUB_CATEGORY), request.getPopularSubcategory(), "Mismatch in getPopularSubcategory");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SUGGESTED_ITEMS), request.getSuggestedItems(), "Mismatch in getSuggestedItems");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.LIST_CATEGORY), request.getListCategory(), "Mismatch in getListCategory");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.TAGS), request.getTags(), "Mismatch in getTags");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.IMAGES), request.getImages(), "Mismatch in getImages");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.CHAIN), request.getChain(), "Mismatch in getChain");
        if(dbDetails.get(LmsDBManager.APILead.ID_FROM_SOURCE) != null )
            AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.ID_FROM_SOURCE), request.getIdFromSource(), "Mismatch in getIdFromSource");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SOURCE_LINK), request.getSourceLink(), "Mismatch in getSourceLink");
        AssertSoft.assertEquals((dbDetails.get(LmsDBManager.APILead.SOURCE_RATING)), Double.valueOf(request.getSourceRating()), "Mismatch in getSourceRating");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SOURCE_VOTES), request.getSourceVotes(), "Mismatch in getSourceVotes");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.TYPE), request.getType(), "Mismatch in getType");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.PARTNER_INTENT), request.getPartnerIntent(), "Mismatch in getPartnerIntent");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.HOLIDAYS), request.getHolidays(), "Mismatch in getHolidays");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SCORE).toString(), request.getScore().toString(), "Mismatch in getScore");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.ONGROUND_RATING).toString(), request.getOngroundRating().toString(), "Mismatch in getOngroundRating");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.ONGROUND_VERIFIED), request.getOngroundVerified(), "Mismatch in getOngroundVerified");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.TELE_VERIFIED), request.getTeleVerified(), "Mismatch in getTeleVerified");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.BLACKLISTED), request.getBlacklisted(), "Mismatch in getBlacklisted");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.AVAILABLE_JD), request.getAvailableJd(), "Mismatch in getAvailableJd");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.AVAILABLE_GOOLE), request.getAvailableGoogle(), "Mismatch in getAvailableGoogle");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.AVAILABLE_SWIGGY), request.getAvailableSwiggy(), "Mismatch in getAvailableSwiggy");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.AVAILABLE_DUNZO), request.getAvailableDunzo(), "Mismatch in getAvailableDunzo");
        if(dbDetails.get(LmsDBManager.APILead.WAIT_TIME) != null )
            AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.WAIT_TIME), request.getWaitTime(), "Mismatch in getWaitTime");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.HOME_DELIVERY), request.getHomeDelivery(), "Mismatch in getHomeDelivery");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.PAYMENT_METHODS), request.getPaymentMethods(), "Mismatch in getPaymentMethods");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.COMMISSION).toString(), request.getCommission().toString(), "Mismatch in getCommission");
        AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.CITY_ID), request.getCity(), "Mismatch in getCity");
        if(dbDetails.get(LmsDBManager.APILead.SOURCE_ID) != null )
            AssertSoft.assertEquals(dbDetails.get(LmsDBManager.APILead.SOURCE_ID), request.getSource(), "Mismatch in getSource");
        AssertSoft.assertAll();
    }

    /**
     *
     * @param dbDetails
     * @param isDuplicate
     */
    public void verifyIsDuplicateStatus(Map<String, Object> dbDetails, int isDuplicate){
        boolean matched = DBHelper.pollDB(LMS.LMS_TEMPLATE, "SELECT * from api_lead where id=" + dbDetails.get(LmsDBManager.APILead.ID), LmsDBManager.APILead.IS_DUPLICATE, String.valueOf(isDuplicate), 2, 10);
        Assert.assertTrue(matched, "Mismatch in is_duplicate. Expected :"+isDuplicate + ", Found :" + db.getDbInstance().queryForMap("SELECT * from api_lead where id=" + dbDetails.get(LmsDBManager.APILead.ID)).get(LmsDBManager.APILead.IS_DUPLICATE));
    }

    /**
     *
     * @param dbDetails
     * @param id
     * @param parentId
     */
    public void verifyParentId(Map<String, Object> dbDetails, int id, int parentId){
        boolean matched = DBHelper.pollDB(LMS.LMS_TEMPLATE, "SELECT * from api_lead where id=" + id, LmsDBManager.APILead.PARENT_ID, String.valueOf(parentId), 5, 60);
        Assert.assertTrue(matched, "Mismatch in parentId. Expected="+parentId);
    }

    /**
     *
     * @param dbDetails
     * @param dedupStaus
     */
    public void verifyDedupStatus(Map<String, Object> dbDetails, String dedupStaus){
        String statusFromDB = db.leadDetails((Integer) dbDetails.get(LmsDBManager.APILead.ID)).get(LmsDBManager.APILead.DEDUP_STATUS).toString();
        if ( dedupStaus =="n" || dedupStaus =="r" || dedupStaus =="d" ) {
            Assert.assertEquals(dedupStaus, statusFromDB, "Mismatch in DeDup Status");
        }else if(dedupStaus =="n/r") {
            if (statusFromDB == "n" || statusFromDB == "r")
                Assert.assertEquals(dedupStaus, statusFromDB, "Mismatch in DeDup Status");
        }else if(dedupStaus =="n/d") {
            if (statusFromDB == "n" || statusFromDB == "d")
                Assert.assertEquals(dedupStaus, statusFromDB, "Mismatch in DeDup Status");
        }
    }

    /**
     *
     * @param line
     * @return
     */
    public CreateLeadPojo csvParser(String line){
        String [] allFields = LMS.DOWNLOAD_CSV_FIRST_LINE.split(",");
        String [] values = line.split(",");
        CreateLeadPojo blankLead = dp.giveMeRandomPayloadForCreateLead();
        for(int i=0; i< allFields.length;i++) {
            System.out.println(allFields[i] + "---->" + values[i]);
            switch(allFields[i]) {
                case "source" :
                    blankLead.setSource(values[i]);
                    break;
                case "name" :
                    blankLead.setName(values[i]);
                    break;
                case "phone_number":
                    blankLead.setPhoneNumber(values[i]);
                    break;
                case "lat" :
                    blankLead.setLat(values[i]);
                    break;
                case "lng" :
                    blankLead.setLng(values[i]);
                    break;
                case "address" :
                    blankLead.setAddress(values[i]);
                    break;
                case "city" :
                    blankLead.setCity(Integer.valueOf(values[i]));
                    break;
                case "area" :
                    break;
                case "is_duplicate" :
                    break;
                case "parent_id" :
                    blankLead.setParentId(Integer.valueOf(values[i]));
                    break;
                case "landmark" :
                    blankLead.setLandmark(values[i]);
                    break;
                case "email" :
                    blankLead.setEmail(values[i]);
                    break;
                case "website" :
                    blankLead.setWebsite(values[i]);
                    break;
                case "category" :
                    blankLead.setCategory(values[i]);
                    break;
                case "subcategory" :
                    blankLead.setSubcategory(values[i]);
                    break;
                case "popular_category" :
                    blankLead.setPopularCategory(values[i]);
                    break;
                case "popular_subcategory" :
                    blankLead.setPopularSubcategory(values[i]);
                    break;
                case "suggested_items" :
                    blankLead.setSuggestedItems(values[i]);
                    break;
                case "list_category" :
                    blankLead.setListCategory(values[i]);
                    break;
                case "tags" :
                    blankLead.setTags(values[i]);
                    break;
                case "images" :
                    blankLead.setImages(values[i]);
                    break;
                case "chain" :
                    blankLead.setChain(values[i]);
                    break;
                case "id_from_source" :
                    blankLead.setIdFromSource(values[i]);
                    break;
                case "source_link" :
                    blankLead.setSourceLink(values[i]);
                    break;
                case "source_rating" :
                    if(!values[i].isEmpty())
                        blankLead.setSourceRating(Float.valueOf(values[i]));
                    break;
                case "source_votes" :
                    if(!values[i].isEmpty())
                        blankLead.setSourceVotes(Integer.valueOf(values[i]));
                    break;
                case "type" :
                    blankLead.setType(values[i]);
                    break;
                case "partner_intent" :
                    blankLead.setPartnerIntent(values[i]);
                    break;
                case "holidays" :
                    blankLead.setHolidays(values[i]);
                    break;
                case "score" :
                    blankLead.setScore(Float.valueOf(values[i]));
                    break;
                case "onground_rating" :
                    blankLead.setOngroundRating(Float.valueOf(values[i]));
                    break;
                case "onground_verified" :
                    blankLead.setOngroundVerified(Boolean.valueOf(values[i]));
                    break;
                case "tele_verified" :
                    blankLead.setTeleVerified(Boolean.valueOf(values[i]));
                    break;
                case "blacklisted" :
                    blankLead.setBlacklisted(Boolean.valueOf(values[i]));
                    break;
                case "available_jd" :
                    blankLead.setAvailableJd(Boolean.valueOf(values[i]));
                    break;
                case "available_google" :
                    blankLead.setAvailableGoogle(Boolean.valueOf(values[i]));
                    break;
                case "available_swiggy" :
                    blankLead.setAvailableSwiggy(Boolean.valueOf(values[i]));
                    break;
                case "available_dunzo" :
                    blankLead.setAvailableDunzo(Boolean.valueOf(values[i]));
                    break;
                case "time_slots" :
                    blankLead.setTimeSlots(values[i]);
                    break;
                case "wait_time" :
                    blankLead.setWaitTime(values[i]);
                    break;
                case "home_delivery" :
                    blankLead.setHomeDelivery(Boolean.valueOf(values[i]));
                    break;
                case "payment_methods" :
                    blankLead.setPaymentMethods(values[i]);
                    break;
                case "commission" :
                    blankLead.setCommission(Float.valueOf(values[i]));
                    break;
                case "dedup_status" :
                    break;
                case "sent_to_kms" :
                    break;
                case "id_in_kms" :
                    break;

            }
        }
        System.out.println(blankLead);
        return blankLead;
    }

    /**
     *
     * @param line
     * @return
     */
    public int returnLeadIdFromCSV(String line){
        return Integer.valueOf(line.split(",")[0]);
    }

    /**
     *
     * @param fileName
     * @return
     */
    public HashMap<String, String> formDataForBulkUpload(String fileName){
        HashMap<String, String> formParam = new HashMap<>();
        formParam.put("document", fileName);
        return formParam;
    }

    /**
     *
     * @param allLeads
     */
    public void deleteAllLeads(ArrayList<Integer> allLeads){
        String listOfIds = "";
        if(allLeads.size() > 0) {
            for (int id : allLeads) {
                listOfIds = listOfIds + id + ",";
            }
            listOfIds = listOfIds.substring(0, (listOfIds.length() - 1));
            db.deleteDuplicateLeadsFromTable(listOfIds);
            db.deleteLeadsFromTable(listOfIds);
        }
    }

    /**
     *
     * @param taskId
     * @return
     */
    public boolean isProcessingDone(String taskId){
        boolean status = false;
        int count = 0;
        while ( status == false && count < 10) {
            try {
                Thread.sleep( 2 * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            HttpResponse respGetTask =  core.invoker(LMS.TASKS_BY_ID, getLmsTokens(), new String[] { taskId } );
            Assert.assertEquals(respGetTask.getResponseCode(), HttpStatus.SC_OK, "Response is not 200 OK\n"+respGetTask.getResponseMsg());
            String message = AutomationHelper.getDataFromJson(respGetTask.getResponseMsg(), "$.message", String.class);
            status = !message.contains("pending");
            ++count;
        }
        return status;
    }

    /**
     *
     * @return
     */
    public HashMap<String, String> getLmsTokens() {
        HashMap<String, String> map = core.getBasicHeaders();
        map.put("Authorization", "Token " + LMS.LMS_TOKEN);
        return map;
    }
}
