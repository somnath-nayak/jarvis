package com.swiggy.api.dash.orderAuditService.tests;
import com.swiggy.api.dash.orderAuditService.constants.OAS_CONSTANTS;
import com.swiggy.api.dash.orderAuditService.helpers.OASCommon;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OrderAuditChaos {
    OASCommon oas = new OASCommon();

    @Test
    public void stopMysql(){
        int COUNT = 2;

        /** Stop Mysql **/
        oas.mysqlActions(OAS_CONSTANTS.MYSQL_STOP);

        /** Size of the retryQueue Before **/
        int sizeBefore = oas.consumerCount(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_RETRY_QUEUE);

        /** Publish messages in kafka **/
        oas.publishMessageInKafka(COUNT);

        /** Size of the retryQueue After **/
        int sizeAfter = oas.consumerCount(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_RETRY_QUEUE);

        /** Retry Queue count should get increased **/
        Assert.assertEquals(sizeAfter, (sizeBefore + COUNT), "Not pushing to retryQueue if mysql DB is down");

        /** start Mysql again **/
        oas.mysqlActions(OAS_CONSTANTS.MYSQL_START);

        /** Publish messages in kafka **/
        oas.publishMessageInKafka(COUNT);

        /** Size of the retryQueue should not get increased **/
        int sizeAfterMysqlRestart = oas.consumerCount(OAS_CONSTANTS.AUDIT_KAFKA, OAS_CONSTANTS.AUDIT_RETRY_QUEUE);

        /** Retry Queue count should not get increased **/
        Assert.assertEquals(sizeAfterMysqlRestart, 0, "retryQueue count should not get increased");

    }
}
