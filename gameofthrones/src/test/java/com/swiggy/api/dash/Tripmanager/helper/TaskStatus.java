package com.swiggy.api.dash.Tripmanager.helper;

public enum TaskStatus {
    CREATED,
    ARRIVED,
    COMPLETED,
}
