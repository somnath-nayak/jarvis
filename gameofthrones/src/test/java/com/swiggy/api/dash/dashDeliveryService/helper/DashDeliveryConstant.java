package com.swiggy.api.dash.dashDeliveryService.helper;

import com.swiggy.automation.common.utils.DateTimeUtils;

public class DashDeliveryConstant {

        public static String DASHDELIVERY="dash";
        public static String SERVICE_NAME="dashdeliveryservice";
        public static String CONTENT_TYPE_JSON =  "application/json";
        public static String BASIC_AUTHORIZATION = "Basic YWRtaW46YWRtaW4=";
        public static String NO_CACHE_CONTROL = "no-cache";

        public static int TEST_DE_ID=88652;
        public static int TEST_DE_ID_1=88651;
        public static String ORDER_ID = "13183338510";
        public static int AREA_ID = 88;
        public static int CITY_ID_GURGOAN = 2;
        public static int PAY_TOLERANCE = 200;
        public static int COLLECT_TOLERANCE = 200;
        public static int BILL_TOLERANCE = 20;
        public static int QTY = 5;
        public static String ITEM_ID = "7";
        public static int ALTERNATIVE_ITEM_ID = 12;
        public static int WAIT_TIME = 2;
        public static double SETBILL = 266;
        public static double SETPAY = 266;
        public static int ZONE_ID_87 = 87;
        public static String FF_URL_DELIVERY_JOB = "http://dash-fulfilment.u1.swiggyops.de/api/v1/dash/ff/delivery_job/";
        public static String VARIANT_QTY = "250g";
        public static String STORE_NAME="Meghna biryani";
        public static String PHONE="0802323422";
        public static String STORE_ADDRESS="Meghna Biryani ,2nd main";
        public static String LATLONG="28.4302214, 77.09474790000002";
        public static String ITEM = "Godrej Real Good Yummiez Chicken Chilli Sausages";
        public static String IMAGE_URL1 = "Swiggy_Super_Dash\\/pmbjkopisoqtswet9ju2";
        public static String IMAGE_UR2 = "Swiggy_Super_Dash\\/avavaadsisoqtswet9ju2";
        public static int DEFAULT_SLA=40;
        public static int SERVICE_LINE_ID = ServiceLine.DASHCOMPLEX.getVal();
        public static  String ALTERNATIVE_ITEM_NAME = "Alternate Item";
        public static String ITEM_DESC = "description";
        public static int PRICE = 1000;
        public static String STATUS_NA="NA";
        public static String CART_CONFIRMATION_ENDPOINT = "/cart_confirmation_request";
        public static String ITEM_CONFIRMATION_ENDPOINT = "/item_confirmation_request";
        public static long ORDERED_TIME = DateTimeUtils.generateEPOCTime();
        public static String TRIP_MANAGER_DB = "tripmanagerdb";
        public static int STATUSCODE_BUSY=0;
        public static int STATUSCODE_FREE=3;
        public static int STATUSCODE_NOUPDATE=5;
        public static String STRUCTURED_CART_TYPE = "structure";
        public static String UNSTRUCTURED_CART_TYPE = "unstructure";
        public static String COUT_TYPE = "flat";
        public static String TD_TYPE = "percentage";





        }
