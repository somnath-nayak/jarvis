package com.swiggy.api.dash.kms.tests;
import com.swiggy.api.dash.kms.dp.KmsDP;
import com.swiggy.api.dash.kms.helper.HttpResponse;
import com.swiggy.api.dash.kms.helper.kmsHelper;
import com.swiggy.api.dash.kms.pojos.*;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import net.minidev.json.JSONArray;
import org.apache.http.HttpStatus;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class KmsTests {
    SoftAssert softAssert;
    kmsHelper kms = new kmsHelper();
    SchemaValidatorUtils schema = new SchemaValidatorUtils();
    HashMap<Integer, StoreBuilder> listOfAllStoreCreated = new HashMap<>();
    KmsDP kmsDP = new KmsDP();

    @Test(description = "List All Categories from KMS DB")
    public void listCategories() {
        String apiName = "list_categories";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_OK,  "Response of list categories is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "get details of specific store")
    public void getDetailsOfSpecificStore(){
        String apiName = "get_specific_store";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName, new String[] {"1"} );
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of details of store is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all cities in KMS")
    public void listCities(){
        String apiName = "list_cities";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of list_all_cities is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all areas in KMS")
    public void listAreas(){
        String apiName = "list_areas";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of list_all_areas is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all business_hours in KMS")
    public void listBusinessHours(){
        String apiName = "list_business_hours";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of list_business_hours is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    /**
     *
     * @param listOfStores comma separated list of stores
     */
    @Test(description = "Get details of provided stores in KMS", dataProviderClass = KmsDP.class, dataProvider = "listOfStores")
    public void getDetailsOfProvidedStores(String listOfStores){
        String apiName = "get_provided_stores";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName, new String[] { listOfStores } );
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of get_provided_stores is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get details of all stores in KMS")
    public void listAllStores(){
        String apiName = "get_all_stores";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of get_all_stores is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get details of all stores in KMS")
    public void listPaymentMethods(){
        String apiName = "list_payment_methods";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of list_payment_method is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get details area category blackzone in KMS")
    public void listAreaCategotyBlackZone(){
        String apiName = "area_category_blackzone";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of area_category_blackzone is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get details city category blackzone in KMS")
    public void listCityCategotyBlackZone(){
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("city_category_blackzone");
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of city_category_blackzone is not 200 OK");
        softAssert.assertAll();
    }

    @Test(description = "Get all sources in  KMS")
    public void listSources(){
        String apiName = "get_source";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of get sources is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get source_mapping_by_store from  KMS")
    public void listSourceMappingByStore(){
        String apiName = "source_mapping_by_store";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("source_mapping_by_store", new String[] { "1" });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of source_mapping_by_store is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get source to swiggy mapping from  KMS", dataProviderClass = KmsDP.class, dataProvider = "listSourceToSwiggyMappingDP")
    public void listSourceToSwiggyMapping(String placeIds, String[] queryParam, String schemaFileName){
        String apiName = "source_to_swiggy_mapping";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName, new String[] { placeIds }, queryParam);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of source_to_swiggy_mapping is not 200 OK");
        if (schemaFileName != null )
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(schemaFileName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get source to swiggy mapping with source and id from  KMS", dataProviderClass = KmsDP.class, dataProvider = "listSourceToSwiggyMappingWithSourceAndIDDP")
    public void listSourceToSwiggyMappingWithSourceAndID(String source, String sourceId, int expectedStatus){
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("source_to_swiggy_mapping_with_source_id", new String[] { source, sourceId });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), expectedStatus, "Response of source_to_swiggy_mapping_with_source_id with source and ID is not what is expected");
        softAssert.assertAll();
    }

    /**
     *
     * @param areaId
     */
    @Test(description = "Get category BlackZone By Area from KMS", dataProviderClass = KmsDP.class, dataProvider = "getAreaId")
    public void categoryBlackZoneByArea(String areaId){
        String apiName = "category_blackzone_by_area";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker(apiName, new String[] { String.valueOf(areaId) });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of category_blackzone_by_area is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get category BlackZone By CityBuilder from KMS")
    public void categoryBlackZoneByCity(){
        String apiName = "category_blackzone_by_city";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("category_blackzone_by_city", new String[] { "1" });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of category_blackzone_by_city is not 200 OK");
        //schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg());
        softAssert.assertAll();
    }

    /**
     *
     * @param storeId
     */
    @Test(description = "Get holiday Slots By StoreBuilder from  KMS", dataProviderClass = KmsDP.class, dataProvider = "getStoreId")
    public void holidaySlotsByStore(String storeId){
        String apiName = "holiday_slots_by_store";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("holiday_slots_by_store", new String[] { String.valueOf(storeId) });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of holidaySlotsByStore is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    /**
     *
     * @param storeId
     */
    @Test(description = "Get business Slots By StoreBuilder from  KMS", dataProviderClass = KmsDP.class, dataProvider = "getStoreId")
    public void businessSlotsByStore(String storeId){
        String apiName = "business_hours_by_store";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("business_hours_by_store", new String[] { String.valueOf(storeId) });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of businessSlotsByStore is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get all blacklist Reasons from KMS")
    public void blacklistReasons(){
        String apiName = "blacklist_reasons";
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("blacklist_reasons");
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of blacklist_reasons is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "Get republish events from KMS")
    public void republishEvents(){
        softAssert  = new SoftAssert();
        HttpResponse response =  kms.invoker("republish_events");
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(), HttpStatus.SC_OK, "Response of republish_events is not 201 CREATED");
        softAssert.assertAll();
    }

    @Test(description = "Create a test store in KMS", dataProviderClass = KmsDP.class, dataProvider = "createStore")
    public void createTestStore(StoreBuilder store){
        softAssert =  new SoftAssert();
        JsonHelper helper = new JsonHelper();
        try {
            String createStoreRequest = helper.getObjectToJSON(store);
            System.out.println("Payload \n"+ createStoreRequest);
            HttpResponse response = kms.invokePostWithStringJson("create_store", createStoreRequest);
            if(response.getResponseCode() != HttpStatus.SC_CREATED) {softAssert.fail("Response of create Test Store in not 200 OK\nReason :\n"+response.getResponseMsg());}
            else {
                Reporter.log("ID " + kms.getDataFromJson(response.getResponseMsg(), "$.data.id", Integer.class), true);
                listOfAllStoreCreated.put(kms.getDataFromJson(response.getResponseMsg(), "$.data.id", Integer.class), store);
            }
            softAssert.assertAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("InstanceVariableMayNotBeInitialized")
    @Test(description = "update store", dataProvider = "updateStore", dataProviderClass = KmsDP.class, dependsOnMethods = "createTestStore")
    public void updateStore(String whatToUpdate) throws IOException {
        JsonHelper helper = new JsonHelper();
        int key = kms.sendMeRandomKeyFromHashMap(listOfAllStoreCreated);
        StoreBuilder store = listOfAllStoreCreated.get(key);
        StoreBuilder storeB = store;
        switch (whatToUpdate) {
            case "NAME" :
                SoftAssert softAssertN =  new SoftAssert();
                StoreBuilder storeN = store;
                String updateName = "UPDATED NAME";
                storeN.setName(updateName);
                String updateStoreRequest = helper.getObjectToJSON(storeN);

                /*** Update Store call */
                HttpResponse responseUpdate = kms.invokePostWithStringJson("update_store", updateStoreRequest, new String[] { String.valueOf(key) });
                /*
                    KafkaConsumerResponse resp = kafka.runConsumer(KMS.KAFKA_NAME, KMS.STORE_UPDATE_TOPIC);
                    for (KafkaRecords record: resp.getkafkaRecordsList()) {
                        System.out.println(record.getKey() + " : " + record.getValue());
                    }
                */
                softAssertN.assertEquals(responseUpdate.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details call */
                HttpResponse responseGet  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                softAssertN.assertEquals(kms.getDataFromJson(responseGet.getResponseMsg(), "$.data.name", String.class), updateName, "[ Update Store ], Name is not getting updated.");
                softAssertN.assertAll();

                break;

            case "AREA" :
                SoftAssert softAssertA =  new SoftAssert();
                StoreBuilder storeA = store;
                AreaBuilder updatedArea = new AreaBuilder()
                        .setId(1)
                        .setName("Koramangala")
                        .setEnabled(0);
                storeA.setArea(updatedArea);
                String updateStoreArea = helper.getObjectToJSON(storeA);

                /*** Update Store call */
                HttpResponse updateArea = kms.invokePostWithStringJson("update_store", updateStoreArea, new String[] { String.valueOf(key) });
                softAssertA.assertEquals(updateArea.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details call */
                HttpResponse responseGetArea  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                System.out.println(responseGetArea.getResponseMsg());

                /*** Validations */
                softAssertA.assertEquals(String.valueOf(kms.getDataFromJson(responseGetArea.getResponseMsg(), "$.data.area.id", Integer.class)), String.valueOf(updatedArea.getId()), "[ Update Store --> Area ] id is not matching.");
                softAssertA.assertEquals(kms.getDataFromJson(responseGetArea.getResponseMsg(), "$.data.area.name", String.class), updatedArea.getName(), "[ Update Store --> Area ] Name is not matching.");
                //softAssertA.assertEquals(String.valueOf(kms.getDataFromJson(responseGetArea.getResponseMsg(), "$.data.area.enabled", Integer.class)), updatedArea.getEnabled(), "[ Update Store --> Area ] \"enable\" is not matching.");
                softAssertA.assertAll();
                break;
            case "CITY":
                SoftAssert softAssertC =  new SoftAssert();
                StoreBuilder storeC = store;
                AreaBuilder updatedAreaCity = new AreaBuilder()
                        .setId(3)
                        .setName("Preet Vihar")
                        .setEnabled(1);

                CityBuilder updatedCity = new CityBuilder()
                        .setId(2)
                        .setName("Delhi");

                storeC.setArea(updatedAreaCity);
                storeC.setCityBuilder(updatedCity);
                String updateStoreCity = helper.getObjectToJSON(storeC);

                /*** Update Store call */
                HttpResponse updateCity = kms.invokePostWithStringJson("update_store", updateStoreCity, new String[] { String.valueOf(key) });
                softAssertC.assertEquals(updateCity.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details call */
                HttpResponse responseGetCity  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                System.out.println(responseGetCity.getResponseMsg());

                /*** Validations */
                softAssertC.assertEquals(String.valueOf(kms.getDataFromJson(responseGetCity.getResponseMsg(), "$.data.city.id", Integer.class)), String.valueOf(updatedCity.getId()), "[ Update Store --> City ] id is not matching.");
                softAssertC.assertEquals(kms.getDataFromJson(responseGetCity.getResponseMsg(), "$.data.city.name", String.class), updatedCity.getName(), "[ Update Store --> City ] Name is not matching.");
                softAssertC.assertAll();
                break;
            case "STORE_ADDRESS":
                SoftAssert softAssertSA =  new SoftAssert();
                StoreBuilder storeSA = store;
                String updatedAddress = "NEW UPDATED ADDRESS";
                String updatedLandmark = "NEW UPDATED LANDMARK";
                String updatedDesc = "NEW UPDATED Desc";
                String latLong = "12.45,76.60";
                String phoneNumbers = "1111111111,2222222222";
                String imgageId = "ZZZZZZ";

                storeSA.setAddress(updatedAddress)
                        .setLandmark(updatedLandmark)
                        .setDescription(updatedDesc)
                        .setLatLong(latLong)
                        .setPhoneNumbers(phoneNumbers)
                        .setImageId(imgageId);


                String updatedStoreAddress = helper.getObjectToJSON(storeSA);

                /*** Update Store call */
                HttpResponse responseUpdateStoreAdd = kms.invokePostWithStringJson("update_store", updatedStoreAddress, new String[] { String.valueOf(key) });
                softAssertSA.assertEquals(responseUpdateStoreAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details call */
                HttpResponse responseGetStoreAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });

                /*** Validate response with updated values */
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.address", String.class), updatedAddress, "[ Update Store's Address ], value is not matching");
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.landmark", String.class), updatedLandmark, "[ Update Store's landmark ], value is not matching");
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.description", String.class), updatedDesc, "[ Update Store's description ], value is not matching");
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.lat_long", String.class), latLong, "[ Update Store's lat_long ], value is not matching");
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.phone_numbers", String.class), phoneNumbers, "[ Update Store's phone_numbers ], value is not matching");
                softAssertSA.assertEquals(kms.getDataFromJson(responseGetStoreAdd.getResponseMsg(), "$.data.image_id", String.class), imgageId, "[ Update Store's image_id ], value is not matching");
                softAssertSA.assertAll();
                break;
            case "STORE_MISC" :
                SoftAssert softAssertSM =  new SoftAssert();
                StoreBuilder storeSM = store;
                int updatedActive = 0;
                int updatedQyality = 3;
                int updatedWaitTime = 10;
                int updatedCloseDay = 1;
                int updatedVerified = 1;
                String updatedCommission = "[bill]*0.02";

                storeSM.setActive(updatedActive)
                    .setQuality(updatedQyality)
                    .setWaitTime(updatedWaitTime)
                    .setCloseDay(updatedCloseDay)
                    .setOnGroundVerified(updatedVerified)
                    .setCommission(updatedCommission);

                String updatedStoreMisc = helper.getObjectToJSON(storeSM);

                /*** Update Store call */
                HttpResponse responseUpdateStoreMisc = kms.invokePostWithStringJson("update_store", updatedStoreMisc, new String[] { String.valueOf(key) });
                softAssertSM.assertEquals(responseUpdateStoreMisc.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details call */
                HttpResponse responseGetStoreMisc  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });

                /*** Validate response with updated values */
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.active", Integer.class)), String.valueOf(updatedActive), "[ Update Store's active ], value is not matching");
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.quality", Integer.class)), String.valueOf(updatedQyality), "[ Update Store's quantity ], value is not matching");
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.on_ground_verified", Integer.class)), String.valueOf(updatedVerified), "[ Update Store's on_ground_verified ], value is not matching");
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.wait_time", Integer.class)), String.valueOf(updatedWaitTime), "[ Update Store's wait time ], value is not matching");
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.close_day", Integer.class)), String.valueOf(updatedCloseDay), "[ Update Store's Close day ], value is not matching");
                softAssertSM.assertEquals(String.valueOf(kms.getDataFromJson(responseGetStoreMisc.getResponseMsg(), "$.data.commission", String.class)), String.valueOf(updatedCommission), "[ Update Store's commission ], value is not matching");
                softAssertSM.assertAll();
                break;
            case "REMOVE_PAYMENT_MATHODS" :
                SoftAssert softAssertRPM =  new SoftAssert();
                StoreBuilder storeRPM = store;
                List<PaymentMethod> paymentRemove = new PaymentMethod()
                        .addPaymentMethod( new PaymentMethod(1, "Cash"))
                        .returnListOfPaymentMethods();

                storeRPM.setPaymentMethods(paymentRemove);
                String updatedStorePayment = helper.getObjectToJSON(storeRPM);

                /*** Update Store call */
                HttpResponse responseUpdatePayment = kms.invokePostWithStringJson("update_store", updatedStorePayment, new String[] { String.valueOf(key) });
                softAssertRPM.assertEquals(responseUpdatePayment.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details before update */
                HttpResponse responseGetStoreAfterPayment  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray paymentMethods = kms.getDataFromJson(responseGetStoreAfterPayment.getResponseMsg(), "$.data.payment_methods", JSONArray.class);
                softAssertRPM.assertEquals(paymentMethods.size(), 1, "[ Update Store's Payment Method ]");
                softAssertRPM.assertEquals(kms.getDataFromJson(paymentMethods.get(0), "$.name", String.class), "Cash", "[ Update Store's Payment Method ]");
                softAssertRPM.assertAll();
                break;
            case "ADD_PAYMENT_MATHODS" :
                SoftAssert softAssertAPM =  new SoftAssert();
                StoreBuilder storeAPM = store;
                boolean contains = false;
                List<PaymentMethod> paymentAdd = new PaymentMethod()
                        .addPaymentMethod( new PaymentMethod(1, "Cash"))
                        .addPaymentMethod( new PaymentMethod(2, "Credit Card/Debit Card"))
                        .addPaymentMethod( new PaymentMethod(3, "Paytm"))
                        .returnListOfPaymentMethods();

                storeAPM.setPaymentMethods(paymentAdd);
                String updatedStorePaymentAdd = helper.getObjectToJSON(storeAPM);

                /*** Update Store call */
                HttpResponse responseUpdatePaymentAdd = kms.invokePostWithStringJson("update_store", updatedStorePaymentAdd, new String[] { String.valueOf(key) });
                softAssertAPM.assertEquals(responseUpdatePaymentAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details before update */
                HttpResponse responseGetStoreAfterPaymentAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray paymentMethodsAdd = kms.getDataFromJson(responseGetStoreAfterPaymentAdd.getResponseMsg(), "$.data.payment_methods", JSONArray.class);
                softAssertAPM.assertEquals(paymentMethodsAdd.size(), 3, "[ Update Store's Payment Method ]");
                for(Object je : paymentMethodsAdd) {
                    String paymentM = kms.getDataFromJson(je, "$.name", String.class);
                    if(paymentM.contains("Paytm"))
                        contains = true;
                }
                softAssertAPM.assertTrue(contains, "Payment_method is not getting added");
                softAssertAPM.assertAll();
                break;
            case "ADD_CATEGORIES" :
                SoftAssert softAssertAC =  new SoftAssert();
                StoreBuilder storeAC = store;
                boolean addCategory = false;
                List<CategoryBuilder> categories = new CategoryBuilder()
                        .addNewCategory( new CategoryBuilder(3 , "Organic Store"))
                        .addNewCategory( new CategoryBuilder(8 , "Alocohal"))
                        .addNewCategory( new CategoryBuilder(5 , "Bakery"))
                        .returnListOfCategories();

                storeAC.setCategories(categories);
                String updatedStoreCategoriesAdd = helper.getObjectToJSON(storeAC);

                /*** Update Store call */
                HttpResponse responseUpdateCategoriesAdd = kms.invokePostWithStringJson("update_store", updatedStoreCategoriesAdd, new String[] { String.valueOf(key) });
                softAssertAC.assertEquals(responseUpdateCategoriesAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreAfterCategoriesAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray paymentCategoriesAdd = kms.getDataFromJson(responseGetStoreAfterCategoriesAdd.getResponseMsg(), "$.data.categories", JSONArray.class);
                softAssertAC.assertEquals(paymentCategoriesAdd.size(), 3, "[ Update Store's Categories Type ]");
                for(Object je : paymentCategoriesAdd) {
                    String paymentM = kms.getDataFromJson(je, "$.name", String.class);
                    if(paymentM.contains("Bakery"))
                        addCategory = true;
                }
                softAssertAC.assertTrue(addCategory, "New Category is not getting added");
                softAssertAC.assertAll();
                break;
            case "REMOVE_CATEGORIES" :
                SoftAssert softAssertRC =  new SoftAssert();
                StoreBuilder storeRC = store;
                List<CategoryBuilder> categoriesRemove = new CategoryBuilder()
                        .addNewCategory( new CategoryBuilder(8 , "Alocohal"))
                        .returnListOfCategories();

                storeRC.setCategories(categoriesRemove);
                String updatedStoreRemoveCat = helper.getObjectToJSON(storeRC);

                /*** Update Store call */
                HttpResponse responseUpdateRemoveCat = kms.invokePostWithStringJson("update_store", updatedStoreRemoveCat, new String[] { String.valueOf(key) });
                softAssertRC.assertEquals(responseUpdateRemoveCat.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details after update */
                HttpResponse responseGetStoreRemoveCat  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray categoriesAfterRemove = kms.getDataFromJson(responseGetStoreRemoveCat.getResponseMsg(), "$.data.categories", JSONArray.class);
                softAssertRC.assertEquals(categoriesAfterRemove.size(), 1, "[ Update Store's Categories ]");
                softAssertRC.assertEquals("Alocohal", kms.getDataFromJson(categoriesAfterRemove.get(0), "$.name", String.class), "[ Update Store's categories ]");
                softAssertRC.assertAll();
                break;
            case "ADD_SUB_CATEGORIES":
                SoftAssert softAssertASC =  new SoftAssert();
                StoreBuilder storeASC = store;
                boolean addSubCategory = false;
                List<Tags> subTagsAdd = new Tags()
                        .addNewTag( new Tags(28 , "fruit"))
                        .addNewTag( new Tags(30 , "thandi beer"))
                        .addNewTag( new Tags(29 , "cigarette"))
                        .returnListOfTags();

                storeASC.setSubCategories(subTagsAdd);
                String updatedStoreSubCatAdd = helper.getObjectToJSON(storeASC);

                /*** Update Store call */
                HttpResponse responseUpdateSubCatAdd = kms.invokePostWithStringJson("update_store", updatedStoreSubCatAdd, new String[] { String.valueOf(key) });
                softAssertASC.assertEquals(responseUpdateSubCatAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreAfterSubCategoriesAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray subCategoriesAdd = kms.getDataFromJson(responseGetStoreAfterSubCategoriesAdd.getResponseMsg(), "$.data.sub_categories", JSONArray.class);
                softAssertASC.assertEquals(subCategoriesAdd.size(), 3, "[ Update Store's Sub Categories Type ]");
                for(Object je : subCategoriesAdd) {
                    String paymentM = kms.getDataFromJson(je, "$.name", String.class);
                    if(paymentM.contains("cigarette"))
                        addSubCategory = true;
                }
                softAssertASC.assertTrue(addSubCategory, "New Sub Category is not getting added");
                softAssertASC.assertAll();
                break;
            case "REMOVE_SUB_CATEGORIES":
                SoftAssert softAssertRSC =  new SoftAssert();
                StoreBuilder storeRSC = store;
                List<Tags> subTagsRemove = new Tags()
                        .addNewTag( new Tags(28 , "fruit"))
                        .returnListOfTags();
                storeRSC.setSubCategories(subTagsRemove);
                String updatedStoreRemoveSubCat = helper.getObjectToJSON(storeRSC);

                /*** Update Store call */
                HttpResponse responseUpdateRemoveSubCat = kms.invokePostWithStringJson("update_store", updatedStoreRemoveSubCat, new String[] { String.valueOf(key) });
                softAssertRSC.assertEquals(responseUpdateRemoveSubCat.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details after update */
                HttpResponse responseGetStoreRemoveSubCat  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray subCategoriesAfterRemove = kms.getDataFromJson(responseGetStoreRemoveSubCat.getResponseMsg(), "$.data.sub_categories", JSONArray.class);
                softAssertRSC.assertEquals(subCategoriesAfterRemove.size(), 1, "[ Update Store's Sub Categories ]");
                softAssertRSC.assertEquals(kms.getDataFromJson(subCategoriesAfterRemove.get(0), "$.name", String.class), "Whiskey", "[ Update Store's sub categories ]");
                softAssertRSC.assertAll();
                break;
            case "ADD_TAGS":
                SoftAssert softAssertAT =  new SoftAssert();
                StoreBuilder storeAT = store;
                boolean addTag = false;
                List<Tags> tags = new Tags()
                        .addNewTag( new Tags(1 , "Diwali"))
                        .addNewTag( new Tags(9 , "Tobbaco"))
                        .addNewTag( new Tags(3 , "holiday"))
                        .returnListOfTags();

                storeAT.setTags(tags);
                String updatedStoreTagAdd = helper.getObjectToJSON(storeAT);

                /*** Update Store call */
                HttpResponse responseUpdateTagAdd = kms.invokePostWithStringJson("update_store", updatedStoreTagAdd, new String[] { String.valueOf(key) });
                softAssertAT.assertEquals(responseUpdateTagAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreAfterTagAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray tagsAdd = kms.getDataFromJson(responseGetStoreAfterTagAdd.getResponseMsg(), "$.data.tags", JSONArray.class);
                softAssertAT.assertEquals(tagsAdd.size(), 3, "[ Update Store's Tags]");
                for(Object je : tagsAdd) {
                    String paymentM = kms.getDataFromJson(je, "$.name", String.class);
                    if(paymentM.contains("holiday"))
                        addTag = true;
                }
                softAssertAT.assertTrue(addTag, "New Tag is not getting added");
                softAssertAT.assertAll();
                break;
            case "REMOVE_TAGS":
                SoftAssert softAssertRT =  new SoftAssert();
                StoreBuilder storeRT = store;
                List<Tags> tagsRemove = new Tags()
                        .addNewTag( new Tags(1 , "Diwali"))
                        .returnListOfTags();

                storeRT.setTags(tagsRemove);
                String updatedStoreTagRemove = helper.getObjectToJSON(storeRT);

                /*** Update Store call */
                HttpResponse responseUpdateRemoveTag = kms.invokePostWithStringJson("update_store", updatedStoreTagRemove, new String[] { String.valueOf(key) });
                softAssertRT.assertEquals(responseUpdateRemoveTag.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details after update */
                HttpResponse responseGetStoreRemoveTag  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray tagsAfterRemove = kms.getDataFromJson(responseGetStoreRemoveTag.getResponseMsg(), "$.data.tags", JSONArray.class);
                softAssertRT.assertEquals(tagsAfterRemove.size(), 1, "[ Update Store's Tag ]");
                softAssertRT.assertEquals(kms.getDataFromJson(tagsAfterRemove.get(0), "$.name", String.class), "Diwali", "[ Update Store's Tag ]");
                softAssertRT.assertAll();
                break;
            case "ADD_BUSINESS_HOURS":
                SoftAssert softAssertABH =  new SoftAssert();
                StoreBuilder storeABH = store;
                boolean businessHour = false;
                List<BusinessHourBuilder> bHours = new BusinessHourBuilder()
                        .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 0, "10:00:00"))
                        .addBusinessHour( new BusinessHourBuilder(2, "22:30:00", 0, "11:00:00"))
                        .addBusinessHour( new BusinessHourBuilder(3, "23:30:00", 1, "10:30:00"))
                        .returnBusinessHourList();

                storeABH.setBusinessHourBuilders(bHours);
                String updatedStoreBHAdd = helper.getObjectToJSON(storeABH);

                /*** Update Store call */
                HttpResponse responseUpdateBHAdd = kms.invokePostWithStringJson("update_store", updatedStoreBHAdd, new String[] { String.valueOf(key) });
                softAssertABH.assertEquals(responseUpdateBHAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreAfterBHAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray bhAdd = kms.getDataFromJson(responseGetStoreAfterBHAdd.getResponseMsg(), "$.data.business_hours", JSONArray.class);
                softAssertABH.assertEquals(bhAdd.size(), 3, "[ Update Store's Business Hours]");
                for(Object je : bhAdd) {
                    int bH = kms.getDataFromJson(je, "$.day_id", Integer.class);
                    if(bH == 3) {
                        businessHour = true;
                        softAssertABH.assertEquals(kms.getDataFromJson(je, "$.open_time", String.class),"10:30:00" ,"Newly added BH's open time is not matching");
                        softAssertABH.assertEquals(kms.getDataFromJson(je, "$.close_time", String.class),"23:30:00" ,"Newly added BH's close time is not matching");
                        softAssertABH.assertEquals(kms.getDataFromJson(je, "$.open_24_hours", Integer.class), Integer.valueOf(1) ,"Newly added BH's open_24_hours is not matching");
                    }
                }
                softAssertABH.assertTrue(businessHour, "New BH is not getting added");
                softAssertABH.assertAll();
                break;
            case "REMOVE_BUSINESS_HOURS":
                SoftAssert softAssertRBH =  new SoftAssert();
                StoreBuilder storeRBH = store;
                List<BusinessHourBuilder> bHoursRemove = new BusinessHourBuilder()
                        .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 0, "10:00:00"))
                        .returnBusinessHourList();

                storeRBH.setBusinessHourBuilders(bHoursRemove);
                String updatedStoreBHRemove = helper.getObjectToJSON(storeRBH);

                /*** Update Store call */
                HttpResponse responseUpdateRemoveBH = kms.invokePostWithStringJson("update_store", updatedStoreBHRemove, new String[] { String.valueOf(key) });
                softAssertRBH.assertEquals(responseUpdateRemoveBH.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details after update */
                HttpResponse responseGetStoreRemoveBH  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray bhAfterRemove = kms.getDataFromJson(responseGetStoreRemoveBH.getResponseMsg(), "$.data.business_hours", JSONArray.class);
                softAssertRBH.assertEquals(bhAfterRemove.size(), 1, "[ Update Store's BH ]");
                softAssertRBH.assertEquals(kms.getDataFromJson(bhAfterRemove.get(0), "$.day_id", Integer.class), Integer.valueOf(1), "[ Update Store's BH ]");
                softAssertRBH.assertAll();
                break;
            case "ADD_SOURCE":
                SoftAssert softAssertAS =  new SoftAssert();
                StoreBuilder storeAS = store;
                boolean sourceAdd = false;
                List<Source> storeSources = new Source()
                        .addSource( new Source(1, "SW"))
                        .addSource( new Source(2, "JD"))
                        .addSource( new Source(3, "GP"))
                        .returnListOfSources();
                storeAS.setSource(storeSources);
                String updatedStoreSourceAdd = helper.getObjectToJSON(storeAS);

                /*** Update Store call */
                HttpResponse responseUpdateSourceAdd = kms.invokePostWithStringJson("update_store", updatedStoreSourceAdd, new String[] { String.valueOf(key) });
                softAssertAS.assertEquals(responseUpdateSourceAdd.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreAfterSourceAdd  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray sourceAddNew = kms.getDataFromJson(responseGetStoreAfterSourceAdd.getResponseMsg(), "$.data.source", JSONArray.class);
                softAssertAS.assertEquals(sourceAddNew.size(), 3, "[ Update Store's Source]");
                for(Object je : sourceAddNew) {
                    String source = kms.getDataFromJson(je, "$.name", String.class);
                    if(source.equalsIgnoreCase("GP")) {
                        sourceAdd = true;
                    }
                }
                softAssertAS.assertTrue(sourceAdd, "New source is not getting added");
                softAssertAS.assertAll();
                break;
            case "REMOVE_SOURCE":
                SoftAssert softAssertRS =  new SoftAssert();
                StoreBuilder storeRS = store;
                List<Source> storeSourcesRemoved = new Source()
                        .addSource( new Source(1, "SW"))
                        .returnListOfSources();

                storeRS.setSource(storeSourcesRemoved);
                String updatedStoreSourceRemoved = helper.getObjectToJSON(storeRS);

                /*** Update Store call */
                HttpResponse responseUpdateRemoveSource = kms.invokePostWithStringJson("update_store", updatedStoreSourceRemoved, new String[] { String.valueOf(key) });
                softAssertRS.assertEquals(responseUpdateRemoveSource.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                /*** Get Store Details after update */
                HttpResponse responseGetStoreRemoveSource  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                JSONArray sourcesAfterRemove = kms.getDataFromJson(responseGetStoreRemoveSource.getResponseMsg(), "$.data.source", JSONArray.class);
                softAssertRS.assertEquals(sourcesAfterRemove.size(), 1, "[ Update Store's Sources ]");
                softAssertRS.assertEquals(kms.getDataFromJson(sourcesAfterRemove.get(0), "$.name", String.class), String.valueOf("SW"), "[ Update Store's sources ]");
                softAssertRS.assertAll();
                break;
            case "BLACKLISTED":
                SoftAssert softAssertB =  new SoftAssert();
                ReasonForBlacklisting blockReason = new ReasonForBlacklisting(2 , "Chhutti");
                store.setIsBlacklisted(1);
                storeB.setReasonForBlacklisting(blockReason);
                String markStoreBlackListed = helper.getObjectToJSON(storeB);

                /*** Update Store call */
                HttpResponse responseMarkStoreBlacklist = kms.invokePostWithStringJson("update_store", markStoreBlackListed, new String[] { String.valueOf(key) });
                softAssertB.assertEquals(responseMarkStoreBlacklist.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreBlacklisted  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                int blacklisted = kms.getDataFromJson(responseGetStoreBlacklisted.getResponseMsg(), "$.data.is_blacklisted", Integer.class);
                softAssertB.assertEquals(blacklisted, 1, "[ Update Store's is_blacklisted to 1]");
                softAssertB.assertEquals(kms.getDataFromJson(responseGetStoreBlacklisted.getResponseMsg(), "$.data.reason_for_blacklisting.id", Integer.class), Integer.valueOf(2), "[ Update Store's reason_for_blacklisting.id to 2]");
                softAssertB.assertEquals(kms.getDataFromJson(responseGetStoreBlacklisted.getResponseMsg(), "$.data.reason_for_blacklisting.reason", String.class), String.valueOf("Chhutti"), "[ Update Store's reason_for_blacklisting.reason to chhutti]");
                softAssertB.assertAll();
                break;
            case "NOT_BLACKLISTED":
                SoftAssert softAssertNB =  new SoftAssert();
                StoreBuilder storeNB = storeB;
                ReasonForBlacklisting unBlockReason = new ReasonForBlacklisting(0 , "Not blacklisted");
                storeNB.setIsBlacklisted(0);
                storeNB.setReasonForBlacklisting(unBlockReason);
                String storeNotBlackListed = helper.getObjectToJSON(storeB);

                /*** Update Store call */
                HttpResponse storeNotBlackListedUpdate = kms.invokePostWithStringJson("update_store", storeNotBlackListed, new String[] { String.valueOf(key) });
                softAssertNB.assertEquals(storeNotBlackListedUpdate.getResponseCode(), HttpStatus.SC_ACCEPTED, "Response of update store is not 200 OK");

                HttpResponse responseGetStoreNotBlacklisted  = kms.invoker("get_specific_store", new String[] { String.valueOf(key) });
                int notBlacklisted = kms.getDataFromJson(responseGetStoreNotBlacklisted.getResponseMsg(), "$.data.is_blacklisted", Integer.class);
                softAssertNB.assertEquals(notBlacklisted, 0, "[ Update Store's is_blacklisted to 0 ]");
                softAssertNB.assertEquals(kms.getDataFromJson(responseGetStoreNotBlacklisted.getResponseMsg(), "$.data.reason_for_blacklisting.id", Integer.class), Integer.valueOf(0), "[ Update Store's reason_for_blacklisting.id to 0 ]");
                softAssertNB.assertEquals(kms.getDataFromJson(responseGetStoreNotBlacklisted.getResponseMsg(), "$.data.reason_for_blacklisting.reason", String.class), String.valueOf("Not blacklisted"), "[ Update Store's reason_for_blacklisting.reason to Not blacklisted]");
                softAssertNB.assertAll();
                break;
        }
    }

    @Test(description = "Create store with invalid names", dataProviderClass = KmsDP.class, dataProvider = "createStoreInvalidNames")
    public void createStoreWithNameInvalidValues(String name){
        softAssert =  new SoftAssert();
        StoreBuilder store = kmsDP.giveMeValidRandomStoreObject();
        store.setName(name);
        JsonHelper helper = new JsonHelper();
        try {
            String createStoreRequest = helper.getObjectToJSON(store);
            HttpResponse response = kms.invokePostWithStringJson("create_store", createStoreRequest);
            softAssert.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getResponseCode(), "Response code should be 400 Bad request");
            softAssert.assertAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(description = "Create store with invalid lat long", dataProviderClass = KmsDP.class, dataProvider = "createStoreInvalidLatLong")
    public void createStoreWithLatLongInvalidValues(String latLong){
        softAssert =  new SoftAssert();
        StoreBuilder store = kmsDP.giveMeValidRandomStoreObject();
        store.setLatLong(latLong);
        JsonHelper helper = new JsonHelper();
        try {
            String createStoreRequest = helper.getObjectToJSON(store);
            HttpResponse response = kms.invokePostWithStringJson("create_store", createStoreRequest);
            softAssert.assertEquals(HttpStatus.SC_BAD_REQUEST, response.getResponseCode(), "Response code should be 400 Bad request");
            softAssert.assertAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(description = "Create store with valid combination of phone numbers", dataProviderClass = KmsDP.class, dataProvider = "createStoreWithValidPhone")
    public void createStoreWithValidPhone(String phoneNumbers){
        StoreBuilder store = kmsDP.giveMeValidRandomStoreObject();
        store.setPhoneNumbers(phoneNumbers);
        JsonHelper helper = new JsonHelper();
        try {
            String createStoreRequest = helper.getObjectToJSON(store);
            HttpResponse response = kms.invokePostWithStringJson("create_store", createStoreRequest);
            Assert.assertEquals(response.getResponseCode(), HttpStatus.SC_CREATED, "Response code should be 202 ACCEPTED");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test(description = "List All Tags")
    public void listAllTags(){
        String apiName = "list_all_tags";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName);
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_OK,  "Response of list tags is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all sub categories of a category", dataProviderClass = KmsDP.class, dataProvider = "subCategoriesByCategoryDP")
    public void subCategoriesByCategory(String categotyId){
        String apiName = "sub_categories_by_category";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName, new String[] {categotyId});
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_OK,  "Response of sub_categories_by_category is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all sub categories of a category", dataProviderClass = KmsDP.class, dataProvider = "subCategoriesByCategoryDPInvalid")
    public void subCategoriesByCategoryInvalidCategory(String categotyId){
        String apiName = "sub_categories_by_category";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName, new String[] { categotyId });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_BAD_REQUEST,  "Response of sub_categories_by_category should be 400");
        softAssert.assertAll();
    }

    @Test(description = "List all sub categories of a category", dataProviderClass = KmsDP.class, dataProvider = "parentCategoryForSubCategoryDP")
    public void parentCategoryForSubCategory(String categotyId){
        String apiName = "parent_category_for_sub_category";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName, new String[] {categotyId});
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_OK,  "Response of parentCategoryForSubCategory is not 200 OK");
        softAssert.assertTrue( schema.validateSchema(kms.getAbsolutePathOfSchema(apiName), response.getResponseMsg()), "Schema validation failed");
        softAssert.assertAll();
    }

    @Test(description = "List all sub categories of a category", dataProviderClass = KmsDP.class, dataProvider = "parentCategoryForSubCategoryDPInvalid")
    public void parentCategoryForSubCategoryInvalid(String subCategotyId){
        String apiName = "parent_category_for_sub_category";
        softAssert  = new SoftAssert();
        HttpResponse response = kms.invoker(apiName, new String[] { subCategotyId });
        Reporter.log(response.getResponseMsg(), true);
        softAssert.assertEquals(response.getResponseCode(),HttpStatus.SC_BAD_REQUEST,  "Response of parentCategoryForSubCategory should be 400");
        softAssert.assertAll();
    }
}
