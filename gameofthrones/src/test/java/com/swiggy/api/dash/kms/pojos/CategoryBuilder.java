
package com.swiggy.api.dash.kms.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name"
})
public class CategoryBuilder {
    @JsonProperty("id")
    private Integer id;

    @JsonProperty("name")
    private String name;

    List<CategoryBuilder> listOfCategories = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     * 
     */
    public CategoryBuilder() {
    }

    /**
     * 
     * @param id
     * @param name
     */
    public CategoryBuilder(Integer id, String name) {
        super();
        this.id = id;
        this.name = name;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public CategoryBuilder setId(Integer id) {
        this.id = id;
        return this;
    }

    public CategoryBuilder withId(Integer id) {
        this.id = id;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public CategoryBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public CategoryBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public CategoryBuilder addNewCategory(CategoryBuilder newCategory){
        listOfCategories.add(newCategory);
        return this;
    }

    public List<CategoryBuilder> returnListOfCategories() {
        return listOfCategories;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).toString();
    }

}
