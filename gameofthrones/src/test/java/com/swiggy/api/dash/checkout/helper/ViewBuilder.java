package com.swiggy.api.dash.checkout.helper;

import com.swiggy.api.dash.checkout.constants.CART;
import com.swiggy.api.dash.checkout.pojos.*;
import com.swiggy.api.dash.coupon.CouponHelper;
import com.swiggy.api.dash.coupon.pojo.Coupon;
import com.swiggy.api.dash.coupon.pojo.CouponMap;
import com.swiggy.api.dash.td.TdType;
import com.swiggy.api.dash.td.pojo.TdView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ViewBuilder extends CartCommandExecutor<CartCreation,Coupon,CouponHelper>  {

    CheckoutHelper checkoutHelper= new CheckoutHelper();
    CouponHelper couponHelper = new CouponHelper();
    TdType td = new TdType();

    /*
    coupon  and td call should be added
     */
    public CartCreation buildStructure(CartCreation cart, String couponType, String tdType){
        if(couponType != null && tdType !=null)
            return buildStructured(cart,couponHelper.Entity(couponType).apply(couponType),td.Entity(tdType).apply(tdType));
        if(tdType !=null)
            return buildStructure(cart, td.Entity(tdType).apply(tdType));
        if(couponType!= null)
            return buildStructure(cart, couponHelper.Entity(couponType).apply(couponType));
        return buildStructure(cart);
    }


    public CartCreation buildUnStructure(CartCreation cart, String couponType, String tdType){
        if(couponType != null && tdType !=null)
            return buildUnStructured(cart,couponHelper.Entity(couponType).apply(couponType),td.Entity(tdType).apply(tdType));
        if(tdType!= null)
            return buildUnStructure(cart, td.Entity(tdType).apply(tdType));
        if(couponType!= null)
            return buildUnStructure(cart, couponHelper.Entity(couponType).apply(couponType));
        return buildUnStructure(cart);
    }


    public Coupon buildCouponMapp(String cType,int enable, int amount){
        return partial(couponHelper::Mapping,cType).apply(enable, amount);
    }


    public static <T, U, R> Function<U, R> partial(BiFunction<T, U, R> f, T x) {
        return (y) -> f.apply(x, y);
    }

    public static <T, U, V, R> Function<V, R> partial(TriFunctional<T, U, V, R> f, T x, U y) {
        return (z) -> f.apply(x, y, z);
    }

    public static <T, U, V, R> BiFunction<U, V, R> partial(TriFunctional<T, U, V, R> f, T x) {
        return (y, z) -> f.apply(x, y, z);
    }

    public  TdView buildTd(String tdType, String storeId, String end){
        return partial(td::Cores,tdType).apply(storeId,end);

    }


//    public static <T, U, V, R> BiFunction<U, V, R> partial(BiFunction<T, U, V> f, T x) {
//        return (y, z) -> f.apply(x, y, z);
//    }

    public CartCreation buildStructureMap(CartCreation cart, String couponType, int enable,int amount, String tdType){
        if(couponType != null && tdType !=null)
            return buildStructuredMap(cart,buildCouponMapp(couponType,enable, amount),td.Entity(tdType).apply(tdType));
        if(tdType !=null)
            return buildStructure(cart, td.Entity(tdType).apply(tdType));
        if(couponType!= null)
            return buildStructure(cart, buildCouponMapp(couponType,enable,amount));
        return buildStructure(cart);
    }

    public CartCreation buildUnStructureMap(CartCreation cart, String couponType, int enable,int amount, String tdType){
        if(couponType != null && tdType !=null)
            return buildUnStructuredMap(cart,buildCouponMapp(couponType,enable, amount),td.Entity(tdType).apply(tdType));
        if(tdType !=null)
            return buildUnStructure(cart, td.Entity(tdType).apply(tdType));
        if(couponType!= null)
            return buildUnStructure(cart, buildCouponMapp(couponType,enable,amount));
        return buildUnStructure(cart);
    }


    public CartCreation buildUnStructureTd(CartCreation cart, String couponType, int enable,int amount, String tdType){
        if(couponType != null && tdType !=null)
            return buildUnStructuredMap(cart,buildCouponMapp(couponType,enable, amount),td.Entity(tdType).apply(tdType));
        if(tdType !=null)
            return buildUnStructure(cart, td.Entity(tdType).apply(tdType));
        if(couponType!= null)
            return buildUnStructure(cart, buildCouponMapp(couponType,enable,amount));
        return buildUnStructure(cart);
    }




    /*
    menu and listing call should be added, item and quantity should be dynamic acc. to stores config
     */
    public CartCreation buildStructure(CartCreation cart){
        List<String> iteml=checkoutHelper.getItems("41288","28.430327","77.096015");
        String itemId="";

        List<Item> item = new Item()
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, iteml.get(6), ""))
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, iteml.get(4), ""))
                .returnListOfItems();
        //iteml.size()<=item.size()
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }

    public CartCreation buildStructure(CartCreation cart, TdView td){
        checkoutHelper.tdCreation(td);
        List<Item> item = new Item()
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "11", ""))
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "7", ""))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }

    public CartCreation buildStructured(CartCreation cart, Coupon coupon, TdView td){
        // List<String> iteml=checkoutHelper.getItems("41288","28.430327","77.096015");
        String storeId="";
        String lat="";
        String lng="";
        String id=checkoutHelper.couponCreation(coupon);
        checkoutHelper.tdCreation(td);
        List<Item> item = new Item()
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "11", ""))
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "7", ""))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setCouponCode(coupon.getCode())
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }


    public CartCreation buildStructuredMap(CartCreation cart, Coupon coupon, TdView td){
        // List<String> iteml=checkoutHelper.getItems("41288","28.430327","77.096015");
        String storeId="";
        String lat="";
        String lng="";
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));
        checkoutHelper.couponMap(lis);
        checkoutHelper.tdCreation(td);
        List<Item> item = new Item()
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "11", ""))
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "7", ""))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setCouponCode(coupon.getCode())
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }

    public EditItemPatch getItemPatch(Item items){
        List<Object> images = new ArrayList<>();
        List<Item> item = new Item()
                .addItem(new Item("Beer","41288", 1,"unstructure","750ml",images,"comment","dash"))
                .addItem(items)
                .returnListOfItems();
        Map<String, Object> ctx = new HashMap<>();
        return new EditItemPatch().setItems(item)
                .setCtx(ctx)
                .setVerifyItemAvailability(true)
                .setVerifyItemPricing(true)
                .setStoreAvailability(true)
                .setVerifyServiceability(true);
    }

    CouponMap map= new CouponMap();
    public CouponMap map(String code, String id, String store){
       return map.setCode(code).setCouponId(id).setRestaurantId(store);

    }

    public CartCreation buildUnStructuredMap(CartCreation cart, Coupon coupon, TdView td){
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));

        checkoutHelper.couponMap(lis);
        checkoutHelper.tdCreation(td);
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","41288", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","41288", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        Location location1 = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment1 = new Attachment()
                .addItem(new Attachment("Description for the file","Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setCouponCode(coupon.getCode())
                .setLocation(location1)
                .setItems(item1)
                .setAttachments(attachment1);
        return cart;
    }


    public CartCreation buildUnStructured(CartCreation cart, Coupon coupon, TdView td){
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));

        checkoutHelper.couponMap(lis);
        checkoutHelper.tdCreation(td);
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","41288", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","41288", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        Location location1 = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment1 = new Attachment()
                .addItem(new Attachment("Description for the file","Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setCouponCode(coupon.getCode())
                .setLocation(location1)
                .setItems(item1)
                .setAttachments(attachment1);
        return cart;
    }

    public CartCreation buildStructure(CartCreation cart, Coupon coupon){
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));

        checkoutHelper.couponMap(lis);
        List<Item> item = new Item()
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "11", ""))
                .addItem(new Item("Item", "41288", 1, CART.STRUCTURE, "7", ""))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

                cart
                .setAddressId("37667531")
                .setCouponCode(coupon.getCode())
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }

    public CartCreation buildUnStructure(CartCreation cart){
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","41288", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","41288", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        Location location1 = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment1 = new Attachment()
                .addItem(new Attachment("Description for the file","Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setLocation(location1)
                .setItems(item1)
                .setAttachments(attachment1);
        return cart;
    }

    public CartCreation buildUnStructure(CartCreation cart, TdView td){
        checkoutHelper.tdCreation(td);
        List<Object> images1 = new ArrayList<>();
        List<Item> item = new Item()
                .addItem(new Item("Milk","41288", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","41288", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        Location location = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment = new Attachment()
                .addItem(new Attachment("Description for the file", "Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667531")
                .setLocation(location)
                .setItems(item)
                .setAttachments(attachment);
        return cart;
    }


    public CartCreation buildUnStructure(CartCreation cart, Coupon coupon){
        String id=checkoutHelper.couponCreation(coupon);
        List<CouponMap> lis = new ArrayList<>();
        lis.add(new CouponMap(coupon.getCode(),id,"41288"));

        checkoutHelper.couponMap(lis);
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","41288", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","41288", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        Location location1 = new Location()
                .setLongitude(77.096015)
                .setLatitude(28.430327);

        List<Attachment> attachment1 = new Attachment()
                .addItem(new Attachment("Description for the file","Field Id"))
                .getListOfAttachments();

        cart
                .setAddressId("37667539")
                .setCouponCode(coupon.getCode())
                .setLocation(location1)
                .setItems(item1)
                .setAttachments(attachment1);
        return cart;
    }


    public OrderCreation buildStructureOrder(String amount){
        return new OrderCreation()
                .setAddressId(37667539)
                .setPaymentInfo(new PaymentInfo(CART.PREPAYMENT, Double.valueOf(amount),CART.ORDERJOB, CART.CASH))
                .setPaymentType(CART.PREPAYMENT);
    }


    public OrderCreation buildStructureOrder(Double amount, String address){
        return new OrderCreation()
                .setAddressId(Integer.valueOf(address))
                .setPaymentInfo(new PaymentInfo(CART.PREPAYMENT, amount,CART.ORDERJOB, CART.CASH))
                .setPaymentType(CART.PREPAYMENT);
    }

    public OrderCreation buildUnStructureOrder(){
        return new OrderCreation()
                .setAddressId(37667539)
                .setPaymentType(CART.POSTPAYMENT);
    }
    public OrderCreation buildUnStructureOrder(String address){
        return new OrderCreation()
                .setAddressId(Integer.valueOf(address))
                .setPaymentType(CART.POSTPAYMENT);
    }

    public OrderCreation buildCustomOrder(){
        List<Object> images1 = new ArrayList<>();
        List<Item> item1 = new Item()
                .addItem(new Item("Milk","", 1,CART.UNSTRUCTURE,"500ml", images1,"comment", "dash"))
                .addItem(new Item("Beer","", 1,CART.UNSTRUCTURE,"750ml",images1,"comment","dash"))
                .returnListOfItems();
        return new OrderCreation().setItems(item1)
                .setAddressId(37667539)
                .setPaymentType(CART.POSTPAYMENT);
    }






}
