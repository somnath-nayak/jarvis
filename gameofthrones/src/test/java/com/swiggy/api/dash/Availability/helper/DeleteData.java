package com.swiggy.api.dash.Availability.helper;

import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.kms.helper.kmsHelper;
import com.swiggy.api.dash.kms.pojos.AreaBuilder;
import com.swiggy.api.dash.kms.pojos.CategoryBuilder;
import com.swiggy.api.dash.kms.pojos.StoreBuilder;
import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeleteData {

    public static ArrayList<Integer> listcategoryId=new ArrayList<>();
    public static ArrayList<Integer> listCityid=new ArrayList<>();
    public static ArrayList<Integer> listAreaid=new ArrayList<>();
    public static ArrayList<Integer> listSubCatId=new ArrayList<>();
    public static HashMap<Integer,StoreBuilder> listStore=new HashMap<>();
    public static HashMap<Integer,Integer> listAreaBlackZone=new HashMap<>();
    public static HashMap<Integer,Integer> listCityBlackZone=new HashMap<>();
    public static void AddCategoryForDelete(int categoryId){
        listcategoryId.add(categoryId);
    }
    public static void AddCityForDelete(int cityId){
        listCityid.add(cityId);
    }
    public static void AddAreaForDelete(int areaId){
        listAreaid.add(areaId);
    }
    public static void AddSubCategoryForDelete(int subcategoryId){
            listSubCatId.add(subcategoryId);
    }
    public static void AddStoreForDelete(int storeId,StoreBuilder store){
        listStore.put(storeId,store);
    }
    public static void AddMappingIdForAreaBlackzone(int areaId,int categoryId){
            listAreaBlackZone.put(categoryId,areaId);
    }
    public static void AddMappingIdForCityBlackzone(int cityId,int categoryId){
        listCityBlackZone.put(categoryId,cityId);
    }

    public static void deleteAllData(){
        DashAvailabilityHelper dashAvailabilityHelper=new DashAvailabilityHelper();
        HttpCore core=new HttpCore(AvailabilityConstant.KMS_SERVICE_NAME);
        System.out.println("Deleting the data");
        Object[] keysArea = listAreaBlackZone.keySet().toArray();
        Object[] keysCity = listCityBlackZone.keySet().toArray();
        for(int i=0;i<keysArea.length;i++){
            dashAvailabilityHelper.deleteAreaBlackZone(listAreaBlackZone.get(keysArea[i]),(Integer)keysArea[i]);
        }
        for(int i=0;i<keysCity.length;i++){
            dashAvailabilityHelper.deleteCityBlackzone(listCityBlackZone.get(keysCity[i]),(Integer)keysCity[i]);
        }

        //listCityid.forEach(id -> dashAvailabilityHelper.deleteCity(id));



        //dashAvailabilityHelper.deleteCity();
        Object[] storeIds = listStore.keySet().toArray();
        JsonHelper jsonHelper = new JsonHelper();
        int storeAreaCategory_1=dashAvailabilityHelper.createCategory("Organic Store");
        List<CategoryBuilder> categories_1 = new CategoryBuilder().addNewCategory( new CategoryBuilder(storeAreaCategory_1 , "Organic Store")).returnListOfCategories();
        int areadID_1=dashAvailabilityHelper.createArea(3,"Hudacity");
        AreaBuilder area = new AreaBuilder().setId(areadID_1).setName("Hudacity").setEnabled(1);


        try {
            for (int i = 0; i < storeIds.length; i++) {
                core.invokePostWithStringJson("update_store", HeaderUtils.getHeaderForKms(KMS.TOKEN),jsonHelper.getObjectToJSON(listStore.get(storeIds[i]).setActive(0).setCategories(categories_1).setArea(area)), new String[]{String.valueOf(storeIds[i])});
            }
        }
        catch (Exception e){
            e.printStackTrace();
            }
        finally {
            listAreaid.forEach(id -> dashAvailabilityHelper.deleteArea(id));
            listcategoryId.forEach(id -> dashAvailabilityHelper.deleteCategory(id));
        }


    }



}
