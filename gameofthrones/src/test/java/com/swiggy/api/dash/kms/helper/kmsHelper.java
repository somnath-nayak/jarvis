package com.swiggy.api.dash.kms.helper;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.kms.pojos.StoreBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Reporter;

import javax.ws.rs.core.MediaType;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class kmsHelper {
    Initialize gameofthrones = new Initialize();

    /**
     *
     * @param apiName
     * @return
     */
    public HttpResponse invoker(String apiName){
        return invoker(apiName, null);
    }

    /**
     *
     * @param apiName
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, String[] urlParam){
        return invoker(apiName, null, urlParam);
    }

    /**
     *
     * @param apiName
     * @param payloadParam
     * @param urlParam
     * @return
     */
    public HttpResponse invoker(String apiName, String[] payloadParam, String[] urlParam){
        GameOfThronesService service = new GameOfThronesService("kms",apiName, gameofthrones);
        Processor proc = new Processor(service, getAllKmsHeaders(), payloadParam, urlParam);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(),  proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     *
     * @param apiName Name of the API
     * @param payloadParam Array of payload params
     * @return
     */
    public HttpResponse invokerPayloadParam(String apiName, String[] payloadParam){
        return invoker(apiName, payloadParam, null);
    }

    public HttpResponse invokePostWithStringJson(String apiName, String jsonAsStr){
        return invokePostWithStringJson(apiName, jsonAsStr, null);
    }

    public HttpResponse invokePostWithStringJson(String apiName, String jsonAsStr, String[] urlParams){
        GameOfThronesService service = new GameOfThronesService("kms",apiName, gameofthrones);
        Processor proc = new Processor(service, getAllKmsHeaders(), jsonAsStr, urlParams, 0);
        return new HttpResponse(proc.ResponseValidator.GetResponseCode(),  proc.ResponseValidator.GetBodyAsText(), proc.ResponseValidator.GetHeaders());
    }

    /**
     * get all headers
     * @return
     */
    public HashMap getAllKmsHeaders(){
        HashMap<String, String> headers = new HashMap<>();
        headers.put(KMS.HEADER_CONTENT_TYPE, MediaType.APPLICATION_JSON);
        headers.put(KMS.HEADER_ACCEPT, MediaType.APPLICATION_JSON);
        headers.put(KMS.HEADER_AUTH, "Token "+KMS.TOKEN);
        return headers;
    }

    public String getAbsolutePathOfSchema(String apiname){
        return KMS.KMS_SCHEMA_PATH + apiname + ".json";
    }

    /**
     *
     * @param min Start index
     * @param max End Index
     * @param separator value of separator e.g , # etc
     * @param size number of records
     * @return
     */
    public String randomNumbersWithSeperator(int min, int max, String separator, int size){
        StringBuffer stringToReturn = new StringBuffer();
        HashSet<Integer> uniqueInts = new HashSet<>();
        while(uniqueInts.size() != size) {
            uniqueInts.add(ThreadLocalRandom.current().nextInt(min, max + 1));
        }
        uniqueInts.forEach(number -> stringToReturn.append(String.valueOf(number)).append(separator));
        return separator.length() > 0 ? stringToReturn.substring(0, (stringToReturn.length() - 1 ) ) : stringToReturn.toString();
    }

    /**
     *
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T getDataFromJson(String jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    /**
     *
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public <T> T getDataFromJson(Object jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    public int sendMeRandomKeyFromHashMap(HashMap<Integer, StoreBuilder> allStores){
        List asList = new ArrayList(allStores.keySet());
        Collections.shuffle(asList);
        return (Integer)asList.get(0);
    }
}
