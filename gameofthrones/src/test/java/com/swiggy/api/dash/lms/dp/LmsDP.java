package com.swiggy.api.dash.lms.dp;

import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.lms.pojos.BusinessHours;
import com.swiggy.api.dash.lms.pojos.CreateLeadPojo;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LmsDP {
    CreateLeadPojo createleadPaylod = new CreateLeadPojo();
    String[] categories = new String[] { "Bakery","Book store","Clothing store","Grocery store","Electronics store","Flower shop","Hardware store","Jewelry shop","Laundry shop",
            "Library","Liquor stores","Keymakers","Restaurants","Pet shops","Pharmacy","Post office & Couriers","Shoe stores","Supermarkets","Paan Shops","Gourmet stores",
            "Organic stores","Meat store","Stationary shops","Gift shops","Fancy stores","Toy stores","Baby stores","Home appliances","Sports stores","Supplement stores",
            "Computer stores","Fruit shops","Vegetables shop","Mobile stores","Personal care","Sweetshops"
    };
    @DataProvider
    public Object[][] createLeadPayload(){
        return new Object[][] {
                { giveMeRandomPayloadForCreateLead() }
        };
    }

    @DataProvider Object[][] bulkUpload(){
        return new Object[][] {
                { "UNIQUE", System.getProperty("user.dir") + "/src/test/java/com/swiggy/api/dash/lms/uploaddata/unique_leads.csv" },
                { "DUPLICATE", System.getProperty("user.dir") + "/src/test/java/com/swiggy/api/dash/lms/uploaddata/duplicate_leads.csv" },
                { "FEW_INVALID", System.getProperty("user.dir") + "/src/test/java/com/swiggy/api/dash/lms/uploaddata/few_invalid_leads.csv" }
        };
    }

    @DataProvider
    public Object[][] requestValidationDP(){
        return new Object[][] {
                {"PHONE"},
                {"EMAIL"},
                {"WEBSITE"},
                {"LEAD_SOURCE_DEFAULT"},
                {"LEAD_TYPE_DEFAULT"},
                {"MIN_CATEGOTY"},
                {"WAIT_TIME"}
        };
    }

    public CreateLeadPojo giveMeRandomPayloadForCreateLead(){
        CreateLeadPojo newLead = createleadPaylod;
        newLead.setName(UUID.randomUUID().toString())
                .setPhoneNumber(AutomationHelper.GetRandomPhone("+91"))
                .setLat("12.949537")
                .setLng("77.642397")
                .setAddress(UUID.randomUUID().toString())
                //.setIsDuplicate(false)
                .setPaymentMethods("NA")
                .setCity(1)
                .setLandmark(UUID.randomUUID().toString().substring(0, 30))
                .setEmail(UUID.randomUUID().toString().substring(0, 10)+"@gmail.com")
                .setWebsite("http://"+UUID.randomUUID().toString().substring(0, 20)+".com")
                .setCategory(AutomationHelper.randomSubArrayWithSeperator(categories, ":", 2))
                .setSubcategory("Any comma() separated sub cats")
                .setPopularCategory("Popular Cat")
                .setPopularSubcategory("Popular Sub Cat")
                .setSuggestedItems("abc")
                .setListCategory("def")
                .setTags("ABC DEF")
                .setImages("ABCBCSJadasd212")
                .setChain("chain")
                .setIdFromSource(null)
                .setSourceLink("http://www.validsourcelink.com")
                .setSourceRating(2.00f)
                .setSourceVotes(1000)
                .setType("pharmacy store")
                .setPartnerIntent("partnerintent")
                .setHolidays("Monday Tuesday")
                .setSource(null)
                .setHomeDelivery(false)
                .setAvailableGoogle(true)
                .setAvailableDunzo(false)
                .setAvailableJd(true)
                .setAvailableSwiggy(false)
                .setTimeSlots(UUID.randomUUID().toString().substring(0, 20) + ":" + UUID.randomUUID().toString().substring(0, 20))
                .setScore(2.0f)
                .setOngroundRating(2.0f)
                .setOngroundVerified(false)
                .setTeleVerified(false)
                .setBlacklisted(false)
                .setCommission(2.0f);

        List<BusinessHours> hours = new ArrayList<>();
        BusinessHours singleDay = new BusinessHours();
        singleDay.setId(1).setDayId(1).setOpenTime("12:00:00").setCloseTime("23:00:00").setOpen24Hours(0);
        hours.add(singleDay);

        //Add business hours
        newLead.setBusinessHoursList(hours);
        return newLead;
    }
}
