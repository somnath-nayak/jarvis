package com.swiggy.api.dash.kms.dp;

import com.swiggy.api.dash.kms.constants.KMS;
import com.swiggy.api.dash.kms.helper.kmsHelper;
import com.swiggy.api.dash.kms.pojos.*;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;

import java.util.List;
import java.util.UUID;

public class KmsDP {
    kmsHelper kms = new kmsHelper();

    @DataProvider(parallel = false)
    public Object[][] getStoreId(){
        return new Object[][] {
                { "1" },
                { "5" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] getAreaId(){
        return new Object[][] {
                { "1" },
                { "5" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] listOfStores(){
        return new Object[][] {
                { kms.randomNumbersWithSeperator(1, 10, ",", 2) },
                { kms.randomNumbersWithSeperator(1, 1000, ",", 100) }
        };
    }

    @DataProvider
    public Object[][] createStore(){
        return new Object[][] {
                { giveMeValidRandomStoreObject() },
                { giveMeValidRandomStoreObject() },
                { giveMeValidRandomStoreObject() }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] updateStore(){
        return new Object[][] {
                { "NAME" },
                { "AREA" },
                { "CITY" },
                { "STORE_ADDRESS" },
                { "STORE_MISC" },
                { "REMOVE_PAYMENT_MATHODS" },
                { "ADD_PAYMENT_MATHODS" },
                { "ADD_CATEGORIES" },
                { "REMOVE_CATEGORIES" },
                { "ADD_TAGS" },
                { "REMOVE_TAGS" },
                { "ADD_BUSINESS_HOURS" },
                { "REMOVE_BUSINESS_HOURS" },
                { "ADD_SOURCE" },
                { "REMOVE_SOURCE" },
                { "BLACKLISTED" },
                { "NOT_BLACKLISTED" }
        };
    }
    @DataProvider (parallel = false)
    public Object[][] createStoreInvalidNames(){
        return new Object[][] {
                { "" },
                { UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString() + UUID.randomUUID().toString()},
                { null }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] createStoreInvalidLatLong(){
        return new Object[][] {
                { "" },
                { "0,0" },
                { null },
                { "0,0,10,12" }
        };
    }

    @DataProvider(parallel = true)
    public Object[][] createStoreWithValidPhone(){
        return new Object[][] {
                { "1212121212, 2222222222" },
                { "1212121212 ,2222222222" },
                { "1212121212,2222222222,0801212123,6767676767" },
                { "" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] subCategoriesByCategoryDP(){
        return new Object[][] {
                { "3" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] subCategoriesByCategoryDPInvalid(){
        return new Object[][] {
                { "100" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] parentCategoryForSubCategoryDP(){
        return new Object[][] {
                { "30" }
        };
    }

    @DataProvider(parallel = false)
    public Object[][] parentCategoryForSubCategoryDPInvalid(){
        return new Object[][] {
                { "100" }
        };
    }

    @DataProvider
    public Object[][] listSourceToSwiggyMappingDP(){
        return new Object[][] {
                { KMS.SOURCE_SW_PLACE_ID, new String[] { KMS.SOURCE_SW, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, KMS.WITHDATA_LIST_SOURCE_TO_SWIGGY },
                { KMS.SOURCE_SW_PLACE_ID, new String[] { KMS.SOURCE_JD, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_SW_PLACE_ID, new String[] { KMS.SOURCE_GP, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_JD_PLACE_ID, new String[] { KMS.SOURCE_SW, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_JD_PLACE_ID, new String[] { KMS.SOURCE_JD, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_JD_PLACE_ID, new String[] { KMS.SOURCE_GP, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_SW, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_JD, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_GP, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_SW_PLACE_ID + "\",\"" + KMS.SOURCE_JD_PLACE_ID + "\",\"" + KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_GP, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_SW_PLACE_ID + "\",\"" + KMS.SOURCE_JD_PLACE_ID + "\",\"" + KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_JD, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
                { KMS.SOURCE_SW_PLACE_ID + "\",\"" + KMS.SOURCE_JD_PLACE_ID + "\",\"" + KMS.SOURCE_GP_PLACE_ID, new String[] { KMS.SOURCE_SW, "id,name,address,area,landmark,description,lat_long,phone_numbers,image_id,active,quality,on_ground_verified,payment_methods,categories,sub_categories,business_hours,tags,wait_time,close_day,is_blacklisted,commission,city,reason_for_blacklisting"}, null },
        };
    }

    @DataProvider
    public Object[][] listSourceToSwiggyMappingWithSourceAndIDDP(){
        return new Object[][] {
                { KMS.SOURCE_SW, KMS.SOURCE_SW_PLACE_ID, HttpStatus.SC_OK },
                { KMS.SOURCE_GP, KMS.SOURCE_SW_PLACE_ID, HttpStatus.SC_NOT_FOUND },
                { KMS.SOURCE_SW, KMS.SOURCE_JD_PLACE_ID, HttpStatus.SC_NOT_FOUND },
        };
    }

    public StoreBuilder giveMeValidRandomStoreObject(){
        List<Source> storeSources = new Source()
                .addSource( new Source(1, KMS.SOURCE_SW))
                .addSource( new Source(2, KMS.SOURCE_JD))
                .returnListOfSources();

        AreaBuilder area = new AreaBuilder()
                .setId(2)
                .setName("Sarjapur Road")
                .setEnabled(1);

        CityBuilder city = new CityBuilder()
                .setId(1)
                .setName("Bangalore");


        List<PaymentMethod> payment = new PaymentMethod()
                .addPaymentMethod( new PaymentMethod(1, "Cash"))
                .addPaymentMethod( new PaymentMethod(2, "Credit Card/Debit Card"))
                .returnListOfPaymentMethods();

        List<BusinessHourBuilder> bHours = new BusinessHourBuilder()
                .addBusinessHour( new BusinessHourBuilder(1, "22:30:00", 0, "10:00:00"))
                .addBusinessHour( new BusinessHourBuilder(2, "22:30:00", 0, "11:00:00"))
                .returnBusinessHourList();

        List<CategoryBuilder> categories = new CategoryBuilder()
                .addNewCategory( new CategoryBuilder(3 , "Organic Store"))
                .addNewCategory( new CategoryBuilder(8 , "Alocohal"))
                .returnListOfCategories();

        List<Tags> tags = new Tags()
                .addNewTag( new Tags(1 , "Diwali"))
                .addNewTag( new Tags(3 , "holiday"))
                .returnListOfTags();

        List<Tags> subTags = new Tags()
                .addNewTag( new Tags(28 , "fruit"))
                .addNewTag( new Tags(30 , "thandi beer"))
                .returnListOfTags();

        ReasonForBlacklisting blockReason = new ReasonForBlacklisting(0 , "Not blacklisted");

        StoreBuilder store =  new StoreBuilder()
                .setName(UUID.randomUUID().toString())
                .setLatLong("12.93,77.60")
                .setPhoneNumbers("1212121212")
                .setImageId("ABCDEFGH")
                .setActive(1)
                .setQuality(1)
                .setDescription("Automation Desc with special chars ~!@#$%^&*()_+")
                .setAddress("Automation Address with special chars ~!@#$%^&*()_+")
                .setLandmark("Automation Landmark with special chars ~!@#$%^&*()_+")
                .setWaitTime(3)
                .setCloseDay(2)
                .setOnGroundVerified(0)
                .setCommission("[bill]*0.01")
                .setSource(storeSources)
                .setArea(area)
                .setCityBuilder(city)
                .setPaymentMethods(payment)
                .setBusinessHourBuilders(bHours)
                .setCategories(categories)
                .setTags(tags)
                .setSubCategories(subTags)
                .setIsBlacklisted(0)
                .setReasonForBlacklisting(blockReason);
        return store;
    }




}
