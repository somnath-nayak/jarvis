package com.swiggy.api.dash.Tripmanager.helper;

public enum  Jobstatus {
    ARRIVED,
    IN_TRANSIT,
    REACHED,
    COMPLETED
}
