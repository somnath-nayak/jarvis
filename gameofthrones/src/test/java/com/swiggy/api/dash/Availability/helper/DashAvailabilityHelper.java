package com.swiggy.api.dash.Availability.helper;

import com.swiggy.api.dash.Availability.models.Availability;
import com.swiggy.api.dash.Availability.models.AvailableCategory;
import com.swiggy.api.dash.Availability.models.KafkaMessage;
import com.swiggy.api.dash.kms.helper.HttpResponse;
import com.swiggy.api.dash.kms.helper.kmsHelper;
import com.swiggy.api.dash.kms.pojos.StoreBuilder;
import com.swiggy.automation.utils.HeaderUtils;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.*;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DashAvailabilityHelper {


    Initialize gameofthrones=new Initialize();
    JsonHelper jsonHelper;
    KafkaHelper kafkaHelper = new KafkaHelper();
    kmsHelper kms = new kmsHelper();
    CreateStoresForAvailablity createStoresForAvailablity=new CreateStoresForAvailablity();

    DBHelper db = new DBHelper();

    public DashAvailabilityHelper(){
        this.jsonHelper= new JsonHelper();
    }

    public Processor getAvailability(String fromTime, String toTime, int[] listOfStore,int[] listOfCategory){

        Processor processor=null;
        try {
            GameOfThronesService gots = new GameOfThronesService("dashavailability", "availability", gameofthrones);
            Availability availability=new Availability(fromTime,toTime,listOfStore,listOfCategory);
            String payload=jsonHelper.getObjectToJSON(availability);
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }
    public Processor getAvailabilityOfCategory(int cityId, int areaId, int[] categories){

        Processor processor=null;
        try {
            GameOfThronesService gots = new GameOfThronesService("dashavailability", "availablecategory", gameofthrones);
            AvailableCategory availableCategory=new AvailableCategory(cityId,areaId,categories);
            String payload=jsonHelper.getObjectToJSON(availableCategory);
            processor = new Processor(gots, HeaderUtils.getDefaultMapSearchHeader(), payload,0);
        }
        catch(Exception e){
            return null;
        }
        return processor;
    }

    public void sendAreaCategoryBlackzoneEvent(int areaId,String action,String blackzoneFor){
        try {
            KafkaMessage message = new KafkaMessage(areaId, action,blackzoneFor);
            System.out.println(jsonHelper.getObjectToJSON(message));
            kafkaHelper.runProducer("kmskafka", AvailabilityConstant.kafkatopic.get(blackzoneFor), jsonHelper.getObjectToJSON(message));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public void insertCity(String cityName){
        //SqlTemplate template= db.getMySqlTemplate("kms");
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { cityName,1};
        template.update("INSERT INTO `city` (`name`, `enabled`) VALUES (?, ?)",params);
    }
    public List<Map<String, Object>> getCityListFromDb(){
        ArrayList cityList=new ArrayList();
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        List<Map<String, Object>> list=template.queryForList("SELECT * from city");
        return list;

    }

    public List<Map<String, Object>> getAreaListForCity(int cityId){
        ArrayList cityList=new ArrayList();
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { cityId};
        List<Map<String, Object>> list=template.queryForList("SELECT * from area where city_id=?",params);
        return list;
    }

    public void createNewAreaInDb(String name,int enabled,int cityId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { name, enabled, cityId};
        template.update("INSERT IGNORE INTO area(name, enabled, city_id) VALUES (?,?,?)",params);
    }
    public void createNewCity(String cityName,int enabled){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { cityName, enabled};
        template.update("INSERT IGNORE INTO city(name, enabled) VALUES (?,?)",params);
    }

    public void createNewCategory(String categoryName){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { categoryName};
        template.update("INSERT IGNORE INTO category(name) VALUES (?)",params);
    }
    public void createNewSubcategory(String subCategoryName){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { subCategoryName};
        template.update("INSERT IGNORE INTO sun_category(name) VALUES (?)",params);
    }
    public void getCategory(){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        List<Map<String, Object>> referral=template.queryForList("SELECT * from category");
        System.out.println(referral.get(0).get("name"));
    }
    public void createAreaBlackZone(int areaId,int categoryId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { areaId,categoryId};
        template.update("INSERT IGNORE INTO area_category_blackzone(area_id,category_id) VALUES (?,?)",params);
        sendAreaCategoryBlackzoneEvent(areaId,"create","area");
        DeleteData.AddMappingIdForAreaBlackzone(areaId,categoryId);

    }

    public void createCityBlackzone(int cityId,int categoryId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { categoryId,cityId};
        template.update("INSERT IGNORE INTO city_category_blackzone(category_id,city_id) VALUES (?,?)",params);
        sendAreaCategoryBlackzoneEvent(cityId,"create","city");
        DeleteData.AddMappingIdForCityBlackzone(cityId,categoryId);
    }

    public void deleteCityBlackzone(int cityId,int categoryId){

        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { cityId,categoryId};
        template.update("DELETE FROM city_category_blackzone Where city_id=? AND category_id=? ",params);
        sendAreaCategoryBlackzoneEvent(cityId,"delete","city");

    }
    public void deleteAreaBlackZone(int areaId,int categoryId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] { areaId,categoryId};
        template.update("DELETE FROM area_category_blackzone Where area_id=? AND category_id=? ",params);
        sendAreaCategoryBlackzoneEvent(areaId,"delete","area");

    }
    public void deleteCity(int cityName){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {cityName};
        template.update("DELETE FROM city Where name=?",params);
    }
    public void createTagInDb(String tagName){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {tagName};
        template.update("INSERT IGNORE INTO tag(name) VALUES (?)",params);
    }
    public void deleteCategory(int categoryId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {categoryId};
        try {
            template.update("DELETE FROM category Where id=?", params);
        }
        catch (Exception e){
            System.out.println("Category Id"+categoryId);
        }
    }
    public void deleteArea(int areaId){
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {areaId};
        try {
            template.update("DELETE FROM area Where id=?", params);
        }
        catch(Exception e){
            System.out.println("Area Id"+areaId);
        }
    }



    public int createCity(String cityName){
        int cityId=-1;
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {cityName};
        String orderCount = "select id from city where name=?";
        try {
            Integer abc = template.queryForObjectInteger(orderCount, params, String.class);
            cityId=abc;
        }
        catch(EmptyResultDataAccessException e) {
            insertCity(cityName);
            cityId=createCity(cityName);

        }
        DeleteData.AddCityForDelete(cityId);
        return cityId;
    }

    public int createArea(int cityId,String areaName){
        int areaId=-1;
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {areaName,cityId};
        String orderCount = "select id from area Where name=? AND city_id=?";
        try {
            Integer abc = template.queryForObjectInteger(orderCount, params, String.class);
            System.out.println(abc);
            areaId=abc;

        }
        catch (Exception e){
            System.out.println("Exception");
            createNewAreaInDb(areaName,1,cityId);
            areaId=createArea(cityId,areaName);
        }
        DeleteData.AddAreaForDelete(areaId);
        return areaId;
    }

    public int createCategory(String categoryName){
        int categoryId=-1;
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {categoryName};
        String orderCount = "select id from category Where name=?";
        try {
            Integer abc = template.queryForObjectInteger(orderCount, params, String.class);
            System.out.println(abc);
            categoryId=abc;

        }
        catch (Exception e){
            System.out.println("Exception");
            createNewCategory(categoryName);
            categoryId=createCategory(categoryName);
        }
        DeleteData.AddCategoryForDelete(categoryId);
        return categoryId;
    }

    public int createSubCategory(String subCategory){
        int subCategoryId=-1;
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {subCategory};
        String orderCount = "select id from sub_category Where name=?";
        try {
            Integer abc = template.queryForObjectInteger(orderCount, params, String.class);
            System.out.println(abc);
            subCategoryId=abc;

        }
        catch (Exception e){
            System.out.println("Exception");
            createNewSubcategory(subCategory);
            subCategoryId=createSubCategory(subCategory);
        }
        DeleteData.AddCategoryForDelete(subCategoryId);
        return subCategoryId;
    }




    public int createTag(String tagName){
        int tagId=-1;
        SqlTemplate template= SystemConfigProvider.getTemplate("kms");
        Object[] params = new Object[] {tagName};
        String orderCount = "select id from tag Where name=?";
        try {
            Integer abc = template.queryForObjectInteger(orderCount, params, String.class);
            System.out.println("Hello" +abc);
            tagId=abc;

        }
        catch (Exception e){
            System.out.println("Exception");
            createTagInDb(tagName);
            tagId=createTag(tagName);
        }

        return tagId;
    }

    public HashMap<String,Integer> isStoreAvailable(ArrayList<String> arrayList){

        HashMap<String,Integer> hm=new HashMap<>();
        NamedGenericTemplate template= db.getMySqlTemplateNamed("kms");
        String sql = "SELECT id ,name from stores where name IN (:names)";
        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("names", arrayList);
        List<Map<String, Object>> list=template.query(sql,parameters);
        System.out.println(list.size());
        for(int i=0;i<arrayList.size();i++){
            boolean available=false;
            for(int j=0;j<list.size();j++){
                if(arrayList.get(i).equalsIgnoreCase(list.get(j).get("name").toString())){
                    hm.put(arrayList.get(i),Integer.parseInt(list.get(j).get("id").toString()));
                    //createStoresForAvailablity.createStore_1_ForAvailability();
                    available=true;

                    break;
                }
            }
            if(!available){
                hm.put(arrayList.get(i),-1);
            }
        }
        System.out.println(hm.size());
        return hm;

    }


    public void inactiveStore(StoreBuilder store, int storeId) throws Exception{
        store.setActive(0);
        HttpResponse responseUpdate = kms.invokePostWithStringJson("update_store", jsonHelper.getObjectToJSON(store), new String[] { String.valueOf(storeId) });

    }









}
