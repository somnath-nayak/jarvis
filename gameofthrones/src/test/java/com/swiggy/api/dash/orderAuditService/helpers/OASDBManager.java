package com.swiggy.api.dash.orderAuditService.helpers;
import com.swiggy.api.dash.orderAuditService.constants.OAS_CONSTANTS;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import java.util.List;
import java.util.Map;

public class OASDBManager {
    DBHelper db = new DBHelper();

    public interface ORDER_UPDATE {
        String ID = "id";
        String UPDATE_JSON = "update_json";
        String UPDATE_TYPE = "update_type";
        String ORDER_VERSION = "order_version";
        String ORDER_ID = "order_id";
        String ORDER_GROUP_ID = "order_group_id";
        String UPDATE_TIME = "update_time";
        String UPDATED_BY = "updated_by";
    }

    /**
     *
     * @return SQl Template
     */
    public SqlTemplate getDbInstance(){
        return SystemConfigProvider.getTemplate(OAS_CONSTANTS.MYSQL_AUDIT_TEMPLATE);
    }

    /**
     *
     * @param orderId
     * @param orderGroupId
     * @param orderVersion
     * @param updateType
     * @return
     */
    public Map<String, Object> getOrderUpdateDetailsUnique(String orderId, String orderGroupId, int orderVersion, String updateType) {
        return getDbInstance().queryForMap("SELECT * FROM order_update WHERE order_id=? AND order_group_id=? AND order_version=? AND update_type=?", orderId, orderGroupId, orderVersion, updateType);
    }

    /**
     *
     * @param orderId
     * @return
     */
    public int getOrderUpdateCount(String orderId) {
        return getOrderUpdateDetails(orderId).size();
    }

    /**
     *
     * @param orderId
     * @return
     */
    public List<Map<String, Object>> getOrderUpdateDetails(String orderId) {
        return getDbInstance().queryForList("SELECT * FROM order_update WHERE order_id='"+orderId+"'");
    }

    /**
     *
     * @param orderGroupId
     * @return
     */
    public int getOrderUpdateCountG(String orderGroupId) {
        return getOrderUpdateDetailsG(orderGroupId).size();
    }

    /**
     *
     * @param orderGroupId
     * @return
     */
    public List<Map<String, Object>> getOrderUpdateDetailsG(String orderGroupId) {
        return getDbInstance().queryForList("SELECT * FROM order_update WHERE order_group_id='"+orderGroupId+"'");
    }

    /**
     *
     * @param agentId
     * @param agentType
     * @return
     */
    public int getOrderUpdateCountAgent(String agentId, String agentType) {
        return getOrderUpdateDetailsAgent(agentId, agentType).size();
    }

    /**
     *
     * @param agentId
     * @param agentType
     * @return
     */
    public List<Map<String, Object>> getOrderUpdateDetailsAgent(String agentId, String agentType) {
        return getDbInstance().queryForList("SELECT * FROM order_update WHERE updated_by='" + agentId + "^"+ agentType +"'");
    }

    /**
     *
     * @param orderId
     * @param count
     * @return
     */
    public boolean isCountAsExpected(String orderId, int count) {
        return DBHelper.pollDB(OAS_CONSTANTS.MYSQL_AUDIT_TEMPLATE, "SELECT count(*) AS count from order_update where order_id='" + orderId + "'", "count", String.valueOf(count), 5, 60);
    }

    /**
     *
     * @param orderId
     * @param orderGroupId
     * @param orderVersion
     * @param updateType
     * @return
     */
    public boolean isCountAsExpected(String orderId, String orderGroupId, int orderVersion, String updateType) {
        return DBHelper.pollDB(OAS_CONSTANTS.MYSQL_AUDIT_TEMPLATE, "SELECT count(*) AS count from order_update where order_id='" + orderId +
                "' AND order_group_id='"+ orderGroupId +
                "' AND order_version='"+ orderVersion +
                "' AND update_type='"+ updateType +"'", "count", String.valueOf(1), 5, 60);
    }


}