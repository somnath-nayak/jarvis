package com.swiggy.api.dash.checkout.pojos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "payment_type",
        "transaction_amount",
        "order_context",
        "payment_method"
})
public class PaymentInfo {

    @JsonProperty("payment_type")
    private String paymentType;
    @JsonProperty("transaction_amount")
    private Double transactionAmount;
    @JsonProperty("order_context")
    private String orderContext;
    @JsonProperty("payment_method")
    private String paymentMethod;

    /**
     * No args constructor for use in serialization
     *
     */
    public PaymentInfo() {
    }

    /**
     *
     * @param transactionAmount
     * @param paymentType
     * @param orderContext
     * @param paymentMethod
     */
    public PaymentInfo(String paymentType, Double transactionAmount, String orderContext, String paymentMethod) {
        super();
        this.paymentType = paymentType;
        this.transactionAmount = transactionAmount;
        this.orderContext = orderContext;
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty("payment_type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment_type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PaymentInfo withPaymentType(String paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    @JsonProperty("transaction_amount")
    public Double getTransactionAmount() {
        return transactionAmount;
    }

    @JsonProperty("transaction_amount")
    public void setTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public PaymentInfo withTransactionAmount(Double transactionAmount) {
        this.transactionAmount = transactionAmount;
        return this;
    }

    @JsonProperty("order_context")
    public String getOrderContext() {
        return orderContext;
    }

    @JsonProperty("order_context")
    public void setOrderContext(String orderContext) {
        this.orderContext = orderContext;
    }

    public PaymentInfo withOrderContext(String orderContext) {
        this.orderContext = orderContext;
        return this;
    }

    @JsonProperty("payment_method")
    public String getPaymentMethod() {
        return paymentMethod;
    }

    @JsonProperty("payment_method")
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public PaymentInfo withPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("payment_type", paymentType).append("transaction_amount", transactionAmount).append("order_context", orderContext).append("payment_method", paymentMethod).toString();
    }

}
