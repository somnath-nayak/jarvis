package com.swiggy.api.dash.viewservice.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;
import com.swiggy.api.dash.Delivery.serviceability.helper.Mapconstant;
import com.swiggy.api.dash.common.AutomationHelper;
import com.swiggy.api.dash.common.HttpCore;
import com.swiggy.api.dash.common.HttpResponse;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.util.ArrayList;

public class Viewservicehelper {
    HttpCore core = new HttpCore(Viewserviceconstant.VIEWSERVICE);



    public void createView(String viewName, String viewTitle, String viewType, boolean visible, String discountType, String storeIdentifier, ArrayList<Integer> category,ArrayList<Integer> subcategory,ArrayList<Integer> tag){

    }

    public String getAbsolutePathOfSchema(String apiname){
        return Viewserviceconstant.VIEW_SCHEMA_PATH + apiname + ".json";
    }

    public int getTotalNumberOfView(){
        String apiname="getalldashview";
        HttpResponse response=core.invoker(apiname,null);
        ReadContext ctx = JsonPath.parse(response);
        return AutomationHelper.getDataFromJson(response.getResponseMsg(),"$.data.length()",Integer.class);
    }

    public HttpResponse getDashViewDetail(String id) {
        String apiname="getdashviewbyid";
        HttpResponse response=core.invoker(apiname,null,new String[]{id});
        return response;
    }





}
