package com.swiggy.api.sf.rng.tests;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

import org.mortbay.util.ajax.JSON;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.SuperConstants;
import com.swiggy.api.sf.rng.dp.SuperDP;
import com.swiggy.api.sf.rng.helper.SuperDbHelper;
import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.CreateIncentive;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.CreatePlan;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Createbenefit;

import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.DBHelper;
import framework.gameofthrones.Tyrion.RedisHelper;

import static com.swiggy.api.sf.rng.constants.SuperConstants.subDuplicateStatusCode;

public class SuperTest {

	HashMap<String, String> requestHeaders = new HashMap<String, String>();
	GameOfThronesService service;
	Processor processor;
	DBHelper dbHelper = new DBHelper();
	RedisHelper redisHelper = new RedisHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	SoftAssert softAssert = new SoftAssert();
	SuperHelper superHelper = new SuperHelper();
	CreatePlan createPlan = new CreatePlan();
	Createbenefit createBenefit = new Createbenefit();
	CreateIncentive createIncentive = new CreateIncentive();

	@Test(dataProvider = "publicUserSubsCriptionWithoutIncentiveData", description = "", dataProviderClass = SuperDP.class)
	public void publicUserSubsCriptionWithoutIncentiveTest(String planPayload, String numOfPlans,
														   String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// System.out.println( "palnid" +planId);
		Assert.assertEquals(statusCode, 1);
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));

		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		softAssert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		softAssert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		softAssert.assertEquals(benefitDetails.isEmpty(), false);
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		softAssert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		softAssert.assertEquals(verifyMessage, SuperConstants.successCheck);

		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		softAssert.assertEquals(validPlanVerify, true);
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		softAssert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		softAssert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		softAssert.assertEquals(benefitDetailsAfterSubs, "[]");
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		softAssert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		softAssert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			softAssert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "publicUserSubsCriptionWithIncentiveData", description = "", dataProviderClass = SuperDP.class)
	public void publicUserSubsCriptionWithIncentiveTest(String planPayload, String numOfPlans, String benefitPayload,
														HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid" + planId);
		Assert.assertEquals(statusCode, 1);
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create incentive(tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by userId
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");

		// plan by user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}

		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
		System.out.println("palnid --" + planId);
	}

	@Test(dataProvider = "mappedUserSubsCriptionWithIncentiveData", description = "", dataProviderClass = SuperDP.class)
	public void mappedUserSubsCriptionWithIncentiveTest(String planPayload, String numOfPlans, String benefitPayload,
														HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid" + planId);
		Assert.assertEquals(statusCode, 1);
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create Incentive (tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// User id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping for mapped user only
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), publicUserId, incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by mapped userId before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan by mapped user id
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription for mapped user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");

		// plan by mapped user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get mapped User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "mappedUserSubsCriptionWithoutIncentiveData", description = "", dataProviderClass = SuperDP.class)
	public void mappedUserSubsCriptionWithoutIncentiveTest(String planPayload, String numOfPlans,
														   String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan (1-month tenure)
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		// create benefit- FREE DEL
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// Plan-Benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		// user id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		// Plan-user-incentive mapping (incentive as null, means no incentive mapped)
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);
		// plan by user mapped user id befor taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		// verify plan for mapped user
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);
		// Create subs. for mapped user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		// get plans by mapped user id after taking subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");
		// get user subs by mapped user id
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		// get mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "publicAndMappedUserSubsCriptionWithoutIncentiveData", description = "", dataProviderClass = SuperDP.class)
	public void publicAndMappedUserSubsCriptionWithoutIncentiveData(String planPayload, String numOfPlans,
																	String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan (1-month tenure)
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid-->  " + planId);
		Assert.assertEquals(statusCode, 1);
		// create benefit- FREE DEL
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// Plan-Benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// user id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(59000, 190000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));
		String mappedUserOrderId = Integer.toString(Utility.getRandom(59000, 190000));

		// Plan-user-incentive mapping (incentive as null, means no incentive mapped)
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", "null").toString();
		String planUserIncentiveMapFoMappedUserPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), mappedUserId, "null").toString();

		// for public user plan-user-incentive-mapping
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// for mapped user plan-user-incentive-mapping
		Processor planUserIncentiveMappingForMapUserResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapFoMappedUserPayload);
		int statusCodePlanUserIncentiveForMapUser = planUserIncentiveMappingForMapUserResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMapForMap = planUserIncentiveMappingForMapUserResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// plan by user mapped user id before taking subs.
		Processor planRespByMappedUserId = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsForMappedUserId = planRespByMappedUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsForMappedUserId.isEmpty(), false);

		// plan by public user id before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);

		// verify plan for public user
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);

		// verify plan for mapped user
		String verifyPlanForMappedPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId),
				mappedUserId);
		Processor verifyPlanForMappedResponse = superHelper.verifyPlan(verifyPlanForMappedPayload);
		int verifyMappedUserStatus = verifyPlanForMappedResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyMappedUserStatus, 1);
		String verifyMappedUserMessage = verifyPlanForMappedResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMappedUserMessage, SuperConstants.successCheck);

		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		boolean validPlanForMappedUserVerify = verifyPlanForMappedResponse.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanForMappedUserVerify, true);

		// Create subs. for public user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));

		// Create subs. for Mapped user
		String subscriptionPayloadForMappedUser = superHelper
				.populateCreateSubscriptionPayload(Integer.toString(planId), mappedUserId, mappedUserOrderId);
		Processor subsResponseOfMappedUser = superHelper.createSubscription(subscriptionPayloadForMappedUser);
		int statusCodeOfSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespOfMappedUser, 1);
		String userSubsIDOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValue("$.data.subscription_id");
		int userIDInSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsRespOfMappedUser, Integer.parseInt(mappedUserId));

		// get plans by public user id after taking subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// get plans by mapped user id after taking subs.
		Processor planRespByMappedUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsOfMappedUserAfterSubs = planRespByUserIdAfterSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsOfMappedUserAfterSubs, "[]");

		// get user subs by public user id
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);

		// get user subs by mapped user id
		Processor mappedUserSubscResponse = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserMappedUserPlanId = mappedUserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserMappedUserPlanId, planId);
		String subsUserMappedUserBenefitId = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserMappedUserBenefitId.isEmpty(), false);

		// get public user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}

		// get mapped user subs. history
		Processor mappedUserSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsForMappedUser = JsonPath.read(mappedUserSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionOfMappedUser = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsIDOfMappedUser,
				Integer.toString(planId), mappedUserOrderId);
		Assert.assertNotEquals(idInSubscriptionOfMappedUser,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsForMappedUser) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsIDOfMappedUser), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "publicAndMappedUserSubsCriptionWithIncentiveToMappedUserData", description = "", dataProviderClass = SuperDP.class)
	public void publicAndMappedUserSubsCriptionWithIncentiveToMappedUserTest(String planPayload, String numOfPlans,
																			 String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid --" + planId);
		Assert.assertEquals(statusCode, 1);

		// Create benefit
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create Incentive (tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// User id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));
		String mappedUserOrderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), publicUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// plan user incentive mapping for mapped user with incentive tenure zero
		String planUserIncentiveMapForMappedUserPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), mappedUserId, incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapForMappedUserResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapForMappedUserPayload);
		int statusCodePlanUserIncentiveMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapForMappedUser, 1);
		String dataPlanUserIncenMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapForMappedUser, SuperConstants.successCheck);

		// get plan by mapped userId before taking subs.
		Processor planRespByUserIdbyMappedUser = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsOfMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountAppliedForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfoForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfoForMappedUser, "null");
		Assert.assertEquals(benefitDetailsOfMappedUser.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountAppliedForMappedUser, "[true]");

		// get plan by public userId before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertEquals(incentiveDiscountInfo, "[null]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[false]");

		// verify plan by mapped user id
		String verifyPlanPayloadForMappedUser = superHelper.populateVerifyPlanPayload(Integer.toString(planId),
				mappedUserId);
		Processor verifyPlanResponseOfMappedUser = superHelper.verifyPlan(verifyPlanPayloadForMappedUser);
		int verifyStatusOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusOfMappedUser, 1);
		String verifyMessageOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageOfMappedUser, SuperConstants.successCheck);
		boolean validPlanVerifyForMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyForMappedUser, true);

		// verify plan by public user id
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription for mapped user
		String subscriptionPayloadOfMappedUser = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				mappedUserId, mappedUserOrderId);
		Processor subsResponseOfMappedUser = superHelper.createSubscription(subscriptionPayloadOfMappedUser);
		int statusCodeOfSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespOfMappedUser, 1);
		String mappedUserSubsID = subsResponseOfMappedUser.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int mappedUserIDInSubsResp = subsResponseOfMappedUser.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(mappedUserIDInSubsResp, Integer.parseInt(mappedUserId));

		// create subscription for public user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));

		// get plan by public user id after subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by mapped user id after subs.
		Processor planRespByMappedUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsAfterSubsOfMappedUser = planRespByMappedUserIdAfterSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubsOfMappedUser, "[]");

		// plan by user id and plan id after subs.

		// get mapped User current Subs. details
		Processor userSubscResponseOfMappedUser = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserOfMappedUserPlanId = userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserOfMappedUserPlanId, planId);
		String subsUserMappedUserBenefitId = JSON.toString(userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserMappedUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubsForMappedUser = JsonPath
				.read(userSubscResponseOfMappedUser.ResponseValidator.GetBodyAsText(), ("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubsForMappedUser.contains(incentiveId), true);

		// get public User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// mapped user subs. history
		Processor mappedUserSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsOfMappedUser = JsonPath.read(mappedUserSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionForMappedUser = SuperDbHelper.getIdFromSubscriptionForUserSubs(mappedUserSubsID,
				Integer.toString(planId), mappedUserOrderId);
		Assert.assertNotEquals(idInSubscriptionForMappedUser,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsOfMappedUser) {
			Assert.assertEquals(id.equalsIgnoreCase(mappedUserSubsID), true);
		}

		// mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "publicAndMappedUserSubsCriptionWithIncentiveToPublicUserData", description = "", dataProviderClass = SuperDP.class)
	public void publicAndMappedUserSubsCriptionWithIncentiveToPublicUserTest(String planPayload, String numOfPlans,
																			 String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid --" + planId);
		Assert.assertEquals(statusCode, 1);

		// Create benefit
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create Incentive (tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// User id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String mappedUserOrderId = Integer.toString(Utility.getRandom(1, 10000000));

		// plan user incentive mapping for public user with incentive tenure zero
		String planUserIncentiveMapPayload = superHelper.populatePlanUserIncentiveMappingPayload(
				Integer.toString(planId), publicUserId, Integer.toString(incentiveId)).toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// plan user incentive mapping for mapped user is null mean no incentive
		String planUserIncentiveMapForMappedUserPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), mappedUserId, "null").toString();
		Processor planUserIncentiveMapForMappedUserResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapForMappedUserPayload);
		int statusCodePlanUserIncentiveMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapForMappedUser, 1);
		String dataPlanUserIncenMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapForMappedUser, SuperConstants.successCheck);

		// get plan by mapped userId before taking subs.
		Processor planRespByUserIdbyMappedUser = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsOfMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountAppliedForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfoForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertEquals(incentiveDiscountInfoForMappedUser, "[null]");
		Assert.assertEquals(benefitDetailsOfMappedUser.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountAppliedForMappedUser, "[false]");

		// get plan by public userId before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "[null]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan by mapped user id
		String verifyPlanPayloadForMappedUser = superHelper.populateVerifyPlanPayload(Integer.toString(planId),
				mappedUserId);
		Processor verifyPlanResponseOfMappedUser = superHelper.verifyPlan(verifyPlanPayloadForMappedUser);
		int verifyStatusOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusOfMappedUser, 1);
		String verifyMessageOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageOfMappedUser, SuperConstants.successCheck);
		boolean validPlanVerifyForMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyForMappedUser, true);

		// verify plan by public user id
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription for mapped user
		String subscriptionPayloadOfMappedUser = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				mappedUserId, mappedUserOrderId);
		Processor subsResponseOfMappedUser = superHelper.createSubscription(subscriptionPayloadOfMappedUser);
		int statusCodeOfSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespOfMappedUser, 1);
		String mappedUserSubsID = subsResponseOfMappedUser.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int mappedUserIDInSubsResp = subsResponseOfMappedUser.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(mappedUserIDInSubsResp, Integer.parseInt(mappedUserId));

		// create subscription for public user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));

		// get plan by public user id after subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by mapped user id after subs.
		Processor planRespByMappedUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsAfterSubsOfMappedUser = planRespByMappedUserIdAfterSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubsOfMappedUser, "[]");

		// plan by user id and plan id after subs.

		// get mapped User current Subs. details
		Processor userSubscResponseOfMappedUser = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserOfMappedUserPlanId = userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserOfMappedUserPlanId, planId);
		String subsUserMappedUserBenefitId = JSON.toString(userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserMappedUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubsForMappedUser = JsonPath
				.read(userSubscResponseOfMappedUser.ResponseValidator.GetBodyAsText(), ("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubsForMappedUser.contains(incentiveId), true);

		// get public User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// mapped user subs. history
		Processor mappedUserSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsOfMappedUser = JsonPath.read(mappedUserSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionForMappedUser = SuperDbHelper.getIdFromSubscriptionForUserSubs(mappedUserSubsID,
				Integer.toString(planId), mappedUserOrderId);
		Assert.assertNotEquals(idInSubscriptionForMappedUser,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsOfMappedUser) {
			Assert.assertEquals(id.equalsIgnoreCase(mappedUserSubsID), true);
		}

		// mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "publicAndMappedUserSubsCriptionWithIncentiveToBothPublicandMappedUserData", description = "", dataProviderClass = SuperDP.class)
	public void publicAndMappedUserSubsCriptionWithIncentiveToBothPublicandMappedUserTest(String planPayload,
																						  String numOfPlans, String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid --" + planId);
		Assert.assertEquals(statusCode, 1);

		// Create benefit
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create Incentive (tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// User id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));
		String mappedUserOrderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping for public user with incentive tenure zero
		String planUserIncentiveMapPayload = superHelper.populatePlanUserIncentiveMappingPayload(
				Integer.toString(planId), publicUserId, Integer.toString(incentiveId)).toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// plan user incentive mapping for mapped user with incentive as tenure zero
		String planUserIncentiveMapForMappedUserPayload = superHelper.populatePlanUserIncentiveMappingPayload(
				Integer.toString(planId), mappedUserId, Integer.toString(incentiveId)).toString();
		Processor planUserIncentiveMapForMappedUserResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapForMappedUserPayload);
		int statusCodePlanUserIncentiveMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapForMappedUser, 1);
		String dataPlanUserIncenMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapForMappedUser, SuperConstants.successCheck);

		// get plan by mapped userId before taking subs.
		Processor planRespByUserIdbyMappedUser = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsOfMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountAppliedForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfoForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfoForMappedUser, "[null]");
		Assert.assertEquals(benefitDetailsOfMappedUser.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountAppliedForMappedUser, "[true]");

		// get plan by public userId before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "[null]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan by mapped user id
		String verifyPlanPayloadForMappedUser = superHelper.populateVerifyPlanPayload(Integer.toString(planId),
				mappedUserId);
		Processor verifyPlanResponseOfMappedUser = superHelper.verifyPlan(verifyPlanPayloadForMappedUser);
		int verifyStatusOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusOfMappedUser, 1);
		String verifyMessageOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageOfMappedUser, SuperConstants.successCheck);
		boolean validPlanVerifyForMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyForMappedUser, true);

		// verify plan by public user id
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription for mapped user
		String subscriptionPayloadOfMappedUser = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				mappedUserId, mappedUserOrderId);
		Processor subsResponseOfMappedUser = superHelper.createSubscription(subscriptionPayloadOfMappedUser);
		int statusCodeOfSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespOfMappedUser, 1);
		String mappedUserSubsID = subsResponseOfMappedUser.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int mappedUserIDInSubsResp = subsResponseOfMappedUser.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(mappedUserIDInSubsResp, Integer.parseInt(mappedUserId));

		// create subscription for public user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));

		// get plan by public user id after subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by mapped user id after subs.
		Processor planRespByMappedUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsAfterSubsOfMappedUser = planRespByMappedUserIdAfterSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubsOfMappedUser, "[]");

		// plan by user id and plan id after subs.

		// get mapped User current Subs. details
		Processor userSubscResponseOfMappedUser = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserOfMappedUserPlanId = userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserOfMappedUserPlanId, planId);
		String subsUserMappedUserBenefitId = JSON.toString(userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserMappedUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubsForMappedUser = JsonPath
				.read(userSubscResponseOfMappedUser.ResponseValidator.GetBodyAsText(), ("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubsForMappedUser.contains(incentiveId), true);

		// get public User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// mapped user subs. history
		Processor mappedUserSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsOfMappedUser = JsonPath.read(mappedUserSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionForMappedUser = SuperDbHelper.getIdFromSubscriptionForUserSubs(mappedUserSubsID,
				Integer.toString(planId), mappedUserOrderId);
		Assert.assertNotEquals(idInSubscriptionForMappedUser,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsOfMappedUser) {
			Assert.assertEquals(id.equalsIgnoreCase(mappedUserSubsID), true);
		}

		// mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		System.out.println("palnid --" + planId);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "mappedUserTwoPlansWhereOneMappedUserNotGetPlanOfAnotherMappedUserData", description = "", dataProviderClass = SuperDP.class)
	public void mappedUserTwoPlansWhereOneMappedUserNotGetPlanOfAnotherMappedUserData(String planOnePayload,
																					  String numOfPlans, String benefitPayload, HashMap<String, String> createIncentiveData,
																					  String planTwoPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create Plan one
		Processor planResponse = superHelper.createPlan(planOnePayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// System.out.println( "palnid --" +planId);
		Assert.assertEquals(statusCode, 1);

		// create Plan two
		Processor planTwoResponse = superHelper.createPlan(planTwoPayload);
		int statusCodePlanTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planIdOfPlanTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		// System.out.println( "palnid --" +planId);
		Assert.assertEquals(statusCode, 1);
		// Create benefit
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create Incentive (tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// create plan two benefit mapping
		String planBenefitMapPayloadOfPlanTwo = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planIdOfPlanTwo), Integer.toString(benefitId))
				.toString();
		Processor planBenefitMapResponseOfPlanTwo = superHelper
				.createPlanBenefitMapping(planBenefitMapPayloadOfPlanTwo);
		int statusCodeOfPlanBenefitMappingOfPlanTwo = planBenefitMapResponseOfPlanTwo.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMappingOfPlanTwo, 1);
		String dataPlanBenefitMappingRespOfPlanTwo = planBenefitMapResponseOfPlanTwo.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingRespOfPlanTwo, SuperConstants.successCheck);

		// User id and order id
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String mappedUserOrderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping for public user with no incentive
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), publicUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// plan user incentive mapping for mapped user with incentive tenure zero
		String planUserIncentiveMapForMappedUserPayload = superHelper.populatePlanUserIncentiveMappingPayload(
				Integer.toString(planIdOfPlanTwo), mappedUserId, Integer.toString(incentiveId)).toString();
		Processor planUserIncentiveMapForMappedUserResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapForMappedUserPayload);
		int statusCodePlanUserIncentiveMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapForMappedUser, 1);
		String dataPlanUserIncenMapForMappedUser = planUserIncentiveMapForMappedUserResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapForMappedUser, SuperConstants.successCheck);

		// get plan by mapped userId before taking subs.
		Processor planRespByUserIdbyMappedUser = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsOfMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdOfPlanTwo
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountAppliedForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.plan_list.[?(@.plan_id==" + planIdOfPlanTwo + ")].discount_applied");
		String incentiveDiscountInfoForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.plan_list.[?(@.plan_id==" + planIdOfPlanTwo + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfoForMappedUser, "[null]");
		Assert.assertEquals(benefitDetailsOfMappedUser.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountAppliedForMappedUser, "[true]");
		List<String> listOfPlanIdsByMappedUserId = JsonPath
				.read(planRespByUserIdbyMappedUser.ResponseValidator.GetBodyAsText(), ("$.data.plan_list..plan_id"));
		Assert.assertEquals(listOfPlanIdsByMappedUserId.contains(planId), false);

		// get plan by public userId before taking subs.
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertEquals(incentiveDiscountInfo, "[null]");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[false]");

		List<String> listOfPlanIdsByPublicUserId = JsonPath.read(planRespByUserId.ResponseValidator.GetBodyAsText(),
				("$.data.plan_list..plan_id"));
		Assert.assertEquals(listOfPlanIdsByPublicUserId.contains(planIdOfPlanTwo), false);

		// verify plan by mapped user id
		String verifyPlanPayloadForMappedUser = superHelper.populateVerifyPlanPayload(Integer.toString(planId),
				mappedUserId);
		Processor verifyPlanResponseOfMappedUser = superHelper.verifyPlan(verifyPlanPayloadForMappedUser);
		// int
		// verifyStatusOfMappedUser=verifyPlanResponseOfMappedUser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertEquals(verifyStatusOfMappedUser,false);
		String verifyMessageOfMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageOfMappedUser, "Invalid plan id or user mapping doesn't exist");
		boolean validPlanVerifyForMappedUser = verifyPlanResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertNotEquals(validPlanVerifyForMappedUser, true);

		// verify plan by public user id
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planIdOfPlanTwo),
				publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		// int
		// verifyStatus=verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertNotEquals(verifyStatus,1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertNotEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertNotEquals(validPlanVerify, true);

		// create subscription for mapped user
		String subscriptionPayloadOfMappedUser = superHelper
				.populateCreateSubscriptionPayload(Integer.toString(planIdOfPlanTwo), mappedUserId, mappedUserOrderId);
		Processor subsResponseOfMappedUser = superHelper.createSubscription(subscriptionPayloadOfMappedUser);
		int statusCodeOfSubsRespOfMappedUser = subsResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespOfMappedUser, 1);
		String mappedUserSubsID = subsResponseOfMappedUser.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int mappedUserIDInSubsResp = subsResponseOfMappedUser.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(mappedUserIDInSubsResp, Integer.parseInt(mappedUserId));

		// create subscription for public user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));

		// get plan by public user id after subs.
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by mapped user id after subs.
		Processor planRespByMappedUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetailsAfterSubsOfMappedUser = planRespByMappedUserIdAfterSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubsOfMappedUser, "[]");

		// plan by user id and plan id after subs.

		// get mapped User current Subs. details
		Processor userSubscResponseOfMappedUser = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserOfMappedUserPlanId = userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserOfMappedUserPlanId, planIdOfPlanTwo);
		String subsUserMappedUserBenefitId = JSON.toString(userSubscResponseOfMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserMappedUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubsForMappedUser = JsonPath
				.read(userSubscResponseOfMappedUser.ResponseValidator.GetBodyAsText(), ("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubsForMappedUser.contains(incentiveId), true);

		// get public User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// mapped user subs. history
		Processor mappedUserSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsOfMappedUser = JsonPath.read(mappedUserSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionForMappedUser = SuperDbHelper.getIdFromSubscriptionForUserSubs(mappedUserSubsID,
				Integer.toString(planIdOfPlanTwo), mappedUserOrderId);
		Assert.assertNotEquals(idInSubscriptionForMappedUser,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsOfMappedUser) {
			Assert.assertEquals(id.equalsIgnoreCase(mappedUserSubsID), true);
		}

		// mapped user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "cancelPublicUserSubsCriptionWithOrderIdData", description = "", dataProviderClass = SuperDP.class)
	public void cancelPublicUserSubsCriptionWithOrderIdTest(String planPayload, String numOfPlans,
															String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		// create benefit

		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create incentive(tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));

		// plan user incentive mapping
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by userId
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");

		// plan by user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		// cancel subs. by order id
		String cancelSubsByOrderPayload = superHelper.populateCancelSubscriptionByOrderIdAndUserIdPayload(orderId,
				publicUserId);
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsByOrderPayload);
		String dataMessage = cancelResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataMessage, SuperConstants.successCheck);
		int status = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(status, 1);

		// check recently canceled in susb. user, it should not be visible
		// get User current Subs. details
		Processor canceledSubsInuserSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int statusCodeAfterCancel = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeAfterCancel, 202);
		String statusMessage = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, "No subscription found for user");

		// user subs. history after cancel order
		Processor userSubsHistoryAfterCancel = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIdsAfterCancel = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionAfterCancel = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID,
				Integer.toString(planId), orderId);
		Assert.assertNotEquals(idInSubscriptionAfterCancel,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsAfterCancel) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		String statusOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].status");
		Assert.assertEquals(statusOfSubsAfterCancel, SuperConstants.cancelledCheck);

		String activeFlagOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].active");
		Assert.assertEquals(activeFlagOfSubsAfterCancel, "[false]");
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "cancelPublicUserSubsCriptionWithSubscriptionIdData", description = "", dataProviderClass = SuperDP.class)
	public void cancelPublicUserSubsCriptionWithSubscriptionIdTest(String planPayload, String numOfPlans,
																   String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		// create benefit

		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create incentive(tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), "USER", incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by userId
		Processor planRespByUserId = superHelper.getPlansByUserId(publicUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), publicUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				publicUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(publicUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(publicUserId, "true");

		// plan by user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		// cancel subs. by order id
		String cancelSubsByOrderPayload = superHelper.populateCancelSubscriptionIdPayload(userSubsID);
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsByOrderPayload);
		String dataMessage = cancelResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataMessage, SuperConstants.successCheck);
		int status = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(status, 1);

		// check recently canceled in susb. user, it should not be visible
		// get User current Subs. details
		Processor canceledSubsInuserSubscResponse = superHelper.getUserSubscription(publicUserId, "true");
		int statusCodeAfterCancel = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeAfterCancel, 202);
		String statusMessage = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, "No subscription found for user");

		// user subs. history after cancel order
		Processor userSubsHistoryAfterCancel = superHelper.getSubscriptionHistoryOfUser(publicUserId);
		List<String> listOfSubsIdsAfterCancel = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionAfterCancel = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID,
				Integer.toString(planId), orderId);
		Assert.assertNotEquals(idInSubscriptionAfterCancel,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsAfterCancel) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		String statusOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].status");
		Assert.assertEquals(statusOfSubsAfterCancel, SuperConstants.cancelledCheck);

		String activeFlagOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].active");
		Assert.assertEquals(activeFlagOfSubsAfterCancel, "[false]");
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "cancelMappedUserSubsCriptionWithSubscriptionIdData", description = "", dataProviderClass = SuperDP.class)
	public void cancelMappedUserSubsCriptionWithSubscriptionIdTest(String planPayload, String numOfPlans,
																   String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		System.out.println("palnid" + planId);
		Assert.assertEquals(statusCode, 1);
		// create benefit

		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create incentive(tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), mappedUserId, incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by userId
		Processor planRespByUserId = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), mappedUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				mappedUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(mappedUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");

		// plan by user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		// cancel subs. by order id
		String cancelSubsByOrderPayload = superHelper.populateCancelSubscriptionIdPayload(userSubsID);
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsByOrderPayload);
		String dataMessage = cancelResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataMessage, SuperConstants.successCheck);
		int status = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(status, 1);

		// check recently canceled in susb. user, it should not be visible
		// get User current Subs. details
		Processor canceledSubsInuserSubscResponse = superHelper.getUserSubscription(mappedUserId, "true");
		int statusCodeAfterCancel = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeAfterCancel, 202);
		String statusMessage = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, "No subscription found for user");

		// user subs. history after cancel order
		Processor userSubsHistoryAfterCancel = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsAfterCancel = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionAfterCancel = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID,
				Integer.toString(planId), orderId);
		Assert.assertNotEquals(idInSubscriptionAfterCancel,
				"no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIdsAfterCancel) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		String statusOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].status");
		Assert.assertEquals(statusOfSubsAfterCancel, SuperConstants.cancelledCheck);

		String activeFlagOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].active");
		Assert.assertEquals(activeFlagOfSubsAfterCancel, "[false]");
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "cancelMappedUserSubsCriptionWithOrderIdData", description = "", dataProviderClass = SuperDP.class)
	public void cancelMappedUserSubsCriptionWithOrderIdTest(String planPayload, String numOfPlans,
															String benefitPayload, HashMap<String, String> createIncentiveData) {
		SuperDbHelper.deleteFreeDelBenefit();
		// create plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(statusCode, 1);
		// create benefit

		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// create incentive(tenure zero only)
		String incentivePayload = superHelper.populateIncentivePayload(createIncentiveData);
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper
				.populatePlanBenefitMappingPayload(Integer.toString(planId), Integer.toString(benefitId)).toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 1);
		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanBenefitMappingResp, SuperConstants.successCheck);

		// user and order id
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan user incentive mapping
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(Integer.toString(planId), mappedUserId, incentiveId.toString())
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// get plan by userId
		Processor planRespByUserId = superHelper.getPlansByUserId(mappedUserId, "true");
		String benefitDetails = planRespByUserId.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountApplied = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfo = planRespByUserId.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertNotEquals(incentiveDiscountInfo, "null");
		Assert.assertEquals(benefitDetails.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountApplied, "[true]");

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(Integer.toString(planId), mappedUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(Integer.toString(planId),
				mappedUserId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(userIDInSubsResp, Integer.parseInt(mappedUserId));
		Processor planRespByUserIdAfterSubs = superHelper.getPlansByUserId(mappedUserId, "true");

		// plan by user id after subs.
		String benefitDetailsAfterSubs = planRespByUserIdAfterSubs.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planId + ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		Assert.assertEquals(benefitDetailsAfterSubs, "[]");

		// plan by user id and plan id after subs.

		// get User current Subs. details
		Processor userSubscResponse = superHelper.getUserSubscription(mappedUserId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		List listOfBenefitsInUserSubs = JsonPath.read(userSubscResponse.ResponseValidator.GetBodyAsText(),
				("$.data.benefits..benefit_id"));
		Assert.assertNotEquals(listOfBenefitsInUserSubs.contains(incentiveId), true);

		// user subs. history
		Processor userSubsHistory = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIds = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscription = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, Integer.toString(planId),
				orderId);
		Assert.assertNotEquals(idInSubscription, "no id created in subscription for user subscription is DB");
		for (String id : listOfSubsIds) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		// cancel subs. by order id
		String cancelSubsByOrderPayload = superHelper.populateCancelSubscriptionByOrderIdAndUserIdPayload(orderId,
				mappedUserId);
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsByOrderPayload);
		String dataMessage = cancelResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataMessage, SuperConstants.successCheck);
		int status = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(status, 1);

		// check recently canceled in susb. user, it should not be visible
		// get User current Subs. details
		Processor canceledSubsInuserSubscResponse = superHelper.getUserSubscription(mappedUserId, "true");
		int statusCodeAfterCancel = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeAfterCancel, 202);
		String statusMessage = canceledSubsInuserSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, "No subscription found for user");

		// user subs. history after cancel order
		Processor userSubsHistoryAfterCancel = superHelper.getSubscriptionHistoryOfUser(mappedUserId);
		List<String> listOfSubsIdsAfterCancel = JsonPath.read(userSubsHistory.ResponseValidator.GetBodyAsText(),
				("$.data..subscription_id"));
		String idInSubscriptionAfterCancel = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID,
				Integer.toString(planId), orderId);
		Assert.assertNotEquals(idInSubscriptionAfterCancel,
				"no id created in subscription for user subscription in DB");
		for (String id : listOfSubsIdsAfterCancel) {
			Assert.assertEquals(id.equalsIgnoreCase(userSubsID), true);
		}
		String statusOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].status");
		Assert.assertEquals(statusOfSubsAfterCancel, SuperConstants.cancelledCheck);

		String activeFlagOfSubsAfterCancel = userSubsHistoryAfterCancel.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idInSubscriptionAfterCancel + ")].active");
		Assert.assertEquals(activeFlagOfSubsAfterCancel, "[false]");
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "checkGetSubscriptionHistoryOfNonSubscribedUserData", description = "", dataProviderClass = SuperDP.class)
	public void checkGetSubscriptionHistoryOfNonSubscribedUser(String planPayload, String benefitPayload,
															   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
		String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataOfUserHistory, "[]");
		int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkGetSubscriptionHistoryWhereUserIdIsNullData", description = "", dataProviderClass = SuperDP.class)
	public void checkGetSubscriptionHistoryWhereUserIdIsNullTest(String planPayload, String benefitPayload,
																 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload, incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		// String userId=Integer.toString(Utility.getRandom(1,100000));
		// String orderId=Integer.toString(Utility.getRandom(5000,10000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser("null");
		int dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetResponseCode();
		Assert.assertEquals(dataOfUserHistory, 400);
		// int
		// statusCode=getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertEquals(statusCode, 1);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "checkGetSubscriptionHistoryWhereUserIdAsZeroData", description = "", dataProviderClass = SuperDP.class)
	public void checkGetSubscriptionHistoryWhereUserIdAsZeroTest(String planPayload, String benefitPayload,
																 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser("0");
		String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(dataOfUserHistory, "[]");
		int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForPastSubsOfUserData", description = "", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForPastSubsOfUserTest(String planPayload, String benefitPayload,
													  String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		if (!(subsId.equalsIgnoreCase("Subscription not created")))
			;
		{
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
			superHelper.setPresentSubscriptionToPastSubsByValidFromValidTillDate(subsId, userId, orderId, planId,
					Integer.toString(1));
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);
			String statusOfSubs = getSubsHistoryResp.ResponseValidator
					.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase(SuperConstants.expiredCheck), true);
		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForExpiredSubsOfUserData", description = "Checking expired subs. history-Created Subs. then in DB update it as expired", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForExpiredSubsOfUserTest(String planPayload, String benefitPayload,
														 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		if (!(subsId.equalsIgnoreCase("Subscription not created")))
			;
		{
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
			superHelper.setPresentSubscriptionToPastSubsByValidFromValidTillDate(subsId, userId, orderId, planId,
					Integer.toString(1));
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);

			String statusOfSubs = getSubsHistoryResp.ResponseValidator
					.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase(SuperConstants.expiredCheck), true);
		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForUserWhoseSubsIsAboutToExpireData", description = "Checking subs. history-for user whose Subs. is about to expire", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForUserWhoseSubsIsAboutToExpireTest(String planPayload, String benefitPayload,
																	String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		if (!(subsId.equalsIgnoreCase("Subscription not created")))
		// get subs history
		{
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
			superHelper.setPresentSubscriptionToAboutToExpireByValidTillDate(subsId, orderId, userId, planId);
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);

			String statusOfSubs = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase(SuperConstants.successMessgae), true);
			// Assert.assertEquals(statusOfSubs,"[\"SUCCESS\"]");
		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForUserWhoIsHavingMultipleSubsData", description = "Checking subs. history-for user whose Subs. is about to expire", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForUserWhoIsHavingMultipleSubsTest(String planPayload, String benefitPayload,
																   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdThree = Integer.toString(Utility.getRandom(1, 30000000));
		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		String id=  SuperDbHelper.getIdFromSubscriptionForUserSubs( subsId,  planId,  orderId);
		SuperDbHelper.updateValidTillByIdInSusbscription(Utility.getPastTime(1), id,  userId);
		String subsIdTwo = superHelper.createSubscripionResponse(planId, userId, orderIdTwo);
		SuperDbHelper.updateRenewalOffsetOfPlan( planId,"300");
		String subsIdThree = superHelper.createSubscripionResponse(planId, userId, orderIdThree);
		if (!(subsId.equalsIgnoreCase("Subscription not created")
				&& subsIdThree.equalsIgnoreCase("Subscription not created"))) {
			// get subs history
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
			String idTwoForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsIdTwo, planId, orderIdTwo);
			String idThreeForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsIdThree, planId, orderIdThree);
//                superHelper.setPresentSubscriptionToAboutToExpireByValidTillDate(subsId, orderId, userId, planId);
			String statusOfSubs = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			String statusOfSubsTwo = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idTwoForSubs + ")].status");
			String statusOfSubsThree = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idThreeForSubs + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase("[" + "\"EXPIRED\"" + "]"), true);
			Assert.assertEquals(statusOfSubsTwo.equalsIgnoreCase("[" + "\"SUCCESS\"" + "]"), true);
			Assert.assertEquals(statusOfSubsThree.equalsIgnoreCase("[" + "\"YET_TO_START\"" + "]"), true);
		}
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryWhenPlanIsHavingFlatTypIncentiveForUserData", description = "Checking subs. history-for user whose Subs. is about to expire", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryWhenPlanIsHavingFlatTypIncentiveForUserTest(String planPayload, String benefitPayload,
																			String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		if (!(subsId.equalsIgnoreCase("Subscription not created")))
			;
		// get subs history
		{
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
			superHelper.setPresentSubscriptionToAboutToExpireByValidTillDate(subsId, orderId, userId, planId);
			String statusOfSubs = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			System.out.print("status---->>> " + statusOfSubs);
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase("[" + "\"SUCCESS\"" + "]"), true);
		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForUserWhoTookDifferentTypeOfSubsData", description = "Checking subs. history-for user whose Subs. is about to expire", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForUserWhoTookDifferentTypeOfSubsTest(String planOneMonthTenurePayload,
																	  String planThreeMonthTenurePayload, String renewalOffSetDay, String benefitPayload,
																	  String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planOneMonthTenurePayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];
//
//        String[] allIdsOfThreeMonthTenruePlan = superHelper
//                .subscriptionPlanBenefitIncentiveIDs(planThreeMonthTenurePayload, benefitPayload, incentivePayload);
//        String threeMonthTenurePlanId = allIds[0];
//        String threeMonthTenureBenefitId = allIds[1];
//        String threeMonthTenureIncentiveId = allIds[2];
		// create plan
		Processor planResponse = superHelper.createPlan(planOneMonthTenurePayload);
		int status = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String threeMonthTenurePlanId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(status, 1);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);
		// String orderIdThree = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// plan benefit mapping
		boolean mappingStatusPlanTwo = superHelper.mapPlanAndBenefit(threeMonthTenurePlanId, benefitId);
		Assert.assertEquals(mappingStatusPlanTwo, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatusOfPlanTwo = superHelper.mapPlanIncentiveAndUserMapping(threeMonthTenurePlanId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapStatusOfPlanTwo, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		// String subsIdTwo=superHelper.createSubscripionResponse( planId, userId,
		// orderIdTwo);
		// String subsIdThree=superHelper.createSubscripionResponse( planId, userId,
		// orderIdThree);

		if (!(subsId.equalsIgnoreCase("Subscription not created"))) {
			// get subs history

			superHelper.setPresentSubscriptionToAboutToExpireByValidTillDate(subsId, orderId,userId , planId);
			superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(subsId, orderId, userId, planId,
					renewalOffSetDay);
			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);

			String subsIdTwo = superHelper.createSubscripionResponse(threeMonthTenurePlanId, userId, orderIdTwo);
			String idForSubsTwo = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsIdTwo, threeMonthTenurePlanId,
					orderIdTwo);

			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);

			String statusOfSubs = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			String statusOfSubsTwo = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubsTwo + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase("[" + "\"SUCCESS\"" + "]"), true);
			Assert.assertEquals(statusOfSubsTwo.equalsIgnoreCase("[" + "\"YET_TO_START\"" + "]"), true);

		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "checkSubsHistoryForValidTillAndValidFromOfPlanData", description = "Checking subs. history-for user whose Subs. is about to expire", dataProviderClass = SuperDP.class)
	public void checkSubsHistoryForValidTillAndValidFromOfPlanTest(String planPayload, String benefitPayload,
																   String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// plan incentive mapping
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subsId = superHelper.createSubscripionResponse(planId, userId, orderId);
		if (!(subsId.equalsIgnoreCase("Subscription not created")))
		// get subs history
		{
			Processor getSubsHistoryResp = superHelper.getSubscriptionHistoryOfUser(userId);
			String dataOfUserHistory = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
			Assert.assertNotEquals(dataOfUserHistory, "[]");
			int statusCode = getSubsHistoryResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
			Assert.assertEquals(statusCode, 1);

			String idForSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(subsId, planId, orderId);
//            superHelper.setPresentSubscriptionToAboutToExpireByValidTillDate(subsId, orderId,userId , planId);
			String statusOfSubs = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + idForSubs + ")].status");
			Assert.assertEquals(statusOfSubs.equalsIgnoreCase("[\"SUCCESS\"]"), true);
			String validFrom = SuperDbHelper.getValidFromOfPlan(planId, orderId, userId);
			String from = Utility.getDateInToMilliseconds(validFrom);
			String validTill = SuperDbHelper.getValidTillOfPlan(planId, orderId, userId);
			String till = Utility.getDateInToMilliseconds(validTill);
			String subsValidFrom = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id=="+idForSubs +")].valid_from");
			String subsValidTill = getSubsHistoryResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id=="+idForSubs +")].valid_till");
			Assert.assertEquals(subsValidFrom, "["+from+"]");
			Assert.assertEquals(subsValidTill, "["+till+"]");
		}

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "getPlanBenefitsData", description = "check all benefits attached with the enabled plan", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsTest(String planPayload, String freeDelBenefitPayload, String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(description = "Checking non-existing plan", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsWithNonExistingPlanIdTest() {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String planId = Integer.toString(Utility.getRandom(1, 100000));
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
	}

	@Test(dataProvider = "getPlanBenefitsOfDisabledPlanData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsOfDisabledPlanTest(String planPayload, String freeDelBenefitPayload,
												  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		String patchPlanPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planId, false);
		System.out.println(patchPlanPayload + "payload forpatch");

		Processor patchPlanResp = superHelper.patchPlan(patchPlanPayload);
		int statusCodeOfPatch = patchPlanResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPatch, 1);

		int dataInPatchPlan = patchPlanResp.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertEquals(Integer.toString(dataInPatchPlan), planId);

		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlanBenefitsOfExpiredPlanData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsOfExpiredPlanTest(String planPayload, String freeDelBenefitPayload,
												 String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);
		// set past date to expire plan
		superHelper.setActivePlanToDisableByValidTillDate(planId);
		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "[]");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlanBenefitsOfTenureThreeData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsOfTenureThreeTest(String planPayload, String freeDelBenefitPayload,
												 String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);
		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlanForDeletedBenefitsData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanForDeletedBenefitsTest(String planPayload, String freeDelBenefitPayload,
											  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		SuperDbHelper.deleteFreeDelBenefit();

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[]");
		Assert.assertEquals(freebieBenefitIdInResp, "["+freebieBenefitId+"]");
		Assert.assertEquals(benefitTypeInResp, "[]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlansWithMultipleBenefitsAndIncentivesData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlansWithMultipleBenefitsAndIncentivesTest(String planPayload, String freeDelBenefitPayload,
															  String freebieBenefitPayload, String incentivePayload, String precentageIncentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// create Incentive (tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		Processor percentageIncectiveResp = superHelper.createIncentive(precentageIncentivePayload);
		Integer precentageIncentiveId = new Integer(
				percentageIncectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(precentageIncentiveId, null);

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping
		String planUserIncetMap = superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER",
				Integer.toString(precentageIncentiveId));
		String planUserPercentIncetMap = superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER",
				Integer.toString(incentiveId));
		Processor planUserIncentiveMapResponse = superHelper.createPlanUserIncentiveMapping(planUserIncetMap);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		Processor planUserPercentageIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserPercentIncetMap);
		int statusCodePlanUserPercentIncentiveMap = planUserPercentageIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserPercentIncentiveMap, 1);
		String dataPlanUserPercentIncenMap = planUserPercentageIncentiveMapResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserPercentIncenMap, SuperConstants.successCheck);

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlansBenefitsWhereSameIncentivesInMappedTwiceData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlansBenefitsWhereSameIncentivesInMappedTwiceTest(String planPayload, String freeDelBenefitPayload,
																	 String freebieBenefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// create Incentive (tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping

		String planUserPercentIncetMap = superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER",
				Integer.toString(incentiveId));

		Processor planUserPercentageIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserPercentIncetMap);
		Processor planUserIncentMapping = superHelper.createPlanUserIncentiveMapping(planUserPercentIncetMap);
		int statusCodePlanUserPercentIncentiveMap = planUserPercentageIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserPercentIncentiveMap, 1);
		String dataPlanUserPercentIncenMap = planUserPercentageIncentiveMapResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserPercentIncenMap, SuperConstants.successCheck);

		int statusCodePlanUserPercentIncentiveMapTwo = planUserIncentMapping.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserPercentIncentiveMap, 1);
		String dataPlanUserPercentIncenMapTwo = planUserIncentMapping.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserPercentIncenMap, SuperConstants.successCheck);

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "[" + benefitId + "]");
		Assert.assertEquals(freebieBenefitIdInResp, "[" + freebieBenefitId + "]");
		Assert.assertEquals(benefitTypeInResp, "[\"FREE_DELIVERY\"]");
		Assert.assertEquals(freebieBenefitTypeResp, "[\"Freebie\"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlansBenefitsWhereNoBenefitIsMappedWithPlanData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlansBenefitsWhereNoBenefitIsMappedWithPlanTest(String planPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(data, "[]");

	}

	@Test(dataProvider = "getPlanBenefitsOfPlanWhichWilBeActiveInFutureData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsOfPlanWhichWilBeActiveInFutureTest(String planPayload, String freeDelBenefitPayload,
																  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(data, "null");
		String benefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].benefit_id");
		String freebieBenefitIdInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].benefit_id");
		String benefitTypeInResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + benefitId + ")].type");
		String freebieBenefitTypeResp = getbenefitResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.benefit_id==" + freebieBenefitId + ")].type");

		Assert.assertEquals(benefitIdInResp, "["+benefitId+"]");
		Assert.assertEquals(freebieBenefitIdInResp, "["+freebieBenefitId+"]");
		Assert.assertEquals(benefitTypeInResp, "["+"\"FREE_DELIVERY\""+"]");
		Assert.assertEquals(freebieBenefitTypeResp, "["+"\"Freebie\""+"]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "getPlanBenefitsOfFuturePlanAndDeletedBenefitsData", description = "Checking disabled plan denefits", dataProviderClass = SuperDP.class)
	public void getPlanBenefitsOfFuturePlanAndDeletedBenefitsTest(String planPayload, String freeDelBenefitPayload,
																  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		// get plan benefits
		Processor getbenefitResp = superHelper.getPlanbenefits(planId);
		String data = getbenefitResp.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertEquals(data, "[]");
		String statusMessage = getbenefitResp.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(data, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "", description = "Verify plan with the plan id as null", dataProviderClass = SuperDP.class)
	public void VerifyPlanTest() {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String userId = Integer.toString(Utility.getRandom(1, 100000));

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(null, userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

	}

	@Test(dataProvider = "verifyPlanWhenPlanIsNotMappedWithUserIncentiveData", description = "verifying plan when plan-user-incentive mapping in not done", dataProviderClass = SuperDP.class)
	public void verifyPlanWhenPlanIsNotMappedWithUserIncentiveTest(String planPayload, String freeDelBenefitPayload,
																   String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);
		superHelper.setActivePlanToDisableByValidTillDate(planId);

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanWithTheExpiredPlanIdData", description = "verifying expired plan", dataProviderClass = SuperDP.class)
	public void verifyPlanWithTheExpiredPlanIdTest(String planPayload, String freeDelBenefitPayload,
												   String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER", "null")
				.toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// expire plan
		superHelper.setActivePlanToDisableByValidTillDate(planId);

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.expiredPlan);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanNonMappedUserIdData", description = "verifying plan with the user id which is not mapped with the plan", dataProviderClass = SuperDP.class)
	public void verifyPlanNonMappedUserIdTest(String planPayload, String freeDelBenefitPayload,
											  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, mappedUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanForMappedUserIdData", description = "verifying plan with the user id which is mapped with the plan", dataProviderClass = SuperDP.class)
	public void verifyPlanForMappedUserIdTest(String planPayload, String freeDelBenefitPayload,
											  String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, mappedUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planId, mappedUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPublicPlanWithDifferentUserIdsData", description = "verifying public plan with the user id zero, random user id and user id which is mapped with some other plan", dataProviderClass = SuperDP.class)
	public void verifyPublicPlanWithDifferentUserIdsTest(String planPayload, String freeDelBenefitPayload,
														 String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		Processor planTwoResponse = superHelper.createPlan(planPayload);
		int planTwoStatusCode = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planTwoplanId = Integer.toString(planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(planTwoStatusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planTwoplanId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, mappedUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planTwoplanId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		// verify plan with the user id which is mapped with some other plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planTwoplanId, mappedUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);
		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planTwoplanId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.successCheck);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, true);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planTwoplanId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.successCheck);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, true);

		// verify plan which is for mapped user only and hit with the public user id
		String verifyPlanMappedPayload = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyMappedPlanResponse = superHelper.verifyPlan(verifyPlanMappedPayload);
		int verifyMappedStatus = verifyMappedPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyMappedStatus, 1);
		String verifyMappedMessage = verifyMappedPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMappedMessage, SuperConstants.planIdInValid);
		boolean validMappedPlanVerify = verifyMappedPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validMappedPlanVerify, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPublicPlanWhichIsMappedWithBothPubliUserAndMappedUserIdsData", description = "verifying public plan which is mapped with both public and mapped user id", dataProviderClass = SuperDP.class)
	public void verifyPublicPlanWhichIsMappedWithBothPubliUserAndMappedUserIdsTest(String planPayload,
																				   String freeDelBenefitPayload, String freebieBenefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null
		String planUserIncentiveMapPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, mappedUserId, "null").toString();
		Processor planUserIncentiveMapResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPayload);
		int statusCodePlanUserIncentiveMap = planUserIncentiveMapResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMap, 1);
		String dataPlanUserIncenMap = planUserIncentiveMapResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMap, SuperConstants.successCheck);

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		// verify plan with the user id which is mapped with some other plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planId, mappedUserId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.successCheck);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, true);
		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.successCheck);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, true);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.successCheck);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, true);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPublicPlanWhichWillBeExpireInFewDaysData", description = "verifying public plan which will be expired in few days", dataProviderClass = SuperDP.class)
	public void verifyPublicPlanWhichWillBeExpireInFewDaysTest(String planPayload, String freeDelBenefitPayload,
															   String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String validTillDays = "4";

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		superHelper.updateValidTillOfThePlanByDays(planId, validTillDays);

		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.invalidPlanValidity);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.invalidPlanValidity);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "", description = "Verify plan with the plan id is empty", dataProviderClass = SuperDP.class)
	public void VerifyPlanWherePlanIdIsEmptyTest() {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String userId = Integer.toString(Utility.getRandom(1, 100000));

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload("\"\"", userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

	}

	@Test(dataProvider = "", description = "Verify plan with the plan id is zero", dataProviderClass = SuperDP.class)
	public void VerifyPlanWherePlanIdIsZeroTest() {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String userId = Integer.toString(Utility.getRandom(1, 100000));

		// verify plan
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload("0", userId);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

	}

	@Test(dataProvider = "verifyPublicPlanWhenPlanTotalAvailableIsZeroData", description = "verifying public plan when total available count is zero", dataProviderClass = SuperDP.class)
	public void verifyPublicPlanWhenPlanTotalAvailableIsZeroTest(String planPayload, String freeDelBenefitPayload,
																 String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String totalCount = "0";

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);

		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.planUsageLimitReached);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.planUsageLimitReached);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPublicPlanWhenNoBenefitIsMappedWithPlanData", description = "verifying public plan when no benefit is mapped with the plan", dataProviderClass = SuperDP.class)
	public void verifyPublicPlanWhenNoBenefitIsMappedWithPlanTest(String planPayload, String freeDelBenefitPayload,
																  String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String totalCount = "0";

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		// Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		// int benefitStatusCode =
		// benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertEquals(benefitStatusCode, 1);
		// String benefitId =
		// Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		//
		// Processor freebieBenefitResponse =
		// superHelper.createBenefit(freebieBenefitPayload);
		// int freebieBenefitStatusCode =
		// freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertEquals(benefitStatusCode, 1);
		// String freebieBenefitId =
		// Integer.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// //plan benefit mapping
		// boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingStatus, true);
		//
		// boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId,
		// freebieBenefitId);
		// Assert.assertEquals(freebieMappingStatus, true);
		//
		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		// SuperDbHelper.updateTotalAvailableOfPlan(planId,totalCount);

		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.benefitIsNotMappedWithPlan);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.benefitIsNotMappedWithPlan);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

		// SuperDbHelper.deleteBenefitFromDB(benefitId);
		// SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanWhenPlanUserIncentiveMappingIsNotPresentData", description = "verifying plan when plan-user-incentive mapping is not done", dataProviderClass = SuperDP.class)
	public void verifyPlanWhenPlanUserIncentiveMappingIsNotPresentTest(String planPayload, String freeDelBenefitPayload,
																	   String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String totalCount = "0";

		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// //plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		// String planUserIncentiveMapPlanTwoPayload =
		// superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER",
		// "null").toString();
		// Processor planUserIncentiveMapPlanTwoResponse =
		// superHelper.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		// int statusCodePlanUserIncentiveMapPlanTwo =
		// planUserIncentiveMapPlanTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		// String dataPlanUserIncenMapPlanTwo =
		// planUserIncentiveMapPlanTwoResponse.ResponseValidator.GetNodeValue("$.data");
		// Assert.assertEquals(dataPlanUserIncenMapPlanTwo,
		// SuperConstants.successCheck);

		// SuperDbHelper.updateTotalAvailableOfPlan(planId,totalCount);

		// verify plan with the user id zero
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.planUserMappingMissing);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with the random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.planUserMappingMissing);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanWhereUserIdIsInvalidData", description = "verifying plan with the invalid user id like null, empty", dataProviderClass = SuperDP.class)
	public void verifyPlanWhereUserIdIsInvalidTest(String planPayload, String freeDelBenefitPayload,
												   String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String totalCount = "0";
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		Processor planTwoResponse = superHelper.createPlan(planPayload);
		int planTwoStatusCode = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planTwoPlanId = Integer.toString(planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(planTwoStatusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// //plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planTwoPlanId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		// SuperDbHelper.updateTotalAvailableOfPlan(planId,totalCount);

		// verify plan with the user id as empty string
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "\"\"");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.successCheck);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, true);
		// verify plan with user id as null
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, null);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.successCheck);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, true);

		// verify mapped plan with the user id as empty string
		String verifyMappedPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planTwoPlanId, "\"\"");
		Processor verifyMappedPlanResponseWithUserZero = superHelper.verifyPlan(verifyMappedPlanPayloadWithUserZero);
		int verifyMappedPlanStatusWithUserZero = verifyMappedPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyMappedPlanStatusWithUserZero, 1);
		String verifyMappedPlanMessageWithUserZero = verifyMappedPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMappedPlanMessageWithUserZero, SuperConstants.planIdInValid);
		boolean validMappedPlanVerifyWithUserZero = verifyMappedPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validMappedPlanVerifyWithUserZero, false);
		// verify plan with user id as null
		String verifyPlanPayload = superHelper.populateVerifyPlanPayload(planTwoPlanId, null);
		Processor verifyPlanResponse = superHelper.verifyPlan(verifyPlanPayload);
		int verifyStatus = verifyPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatus, 1);
		String verifyMessage = verifyPlanResponse.ResponseValidator.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessage, SuperConstants.planIdInValid);
		boolean validPlanVerify = verifyPlanResponse.ResponseValidator.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerify, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanWhereUsageCountEqualsToTOtalAvailableData", description = "verifying plan where usage count is equal to total available", dataProviderClass = SuperDP.class)
	public void verifyPlanWhereUsageCountEqualsToTOtalAvailableTest(String planPayload, String freeDelBenefitPayload,
																	String freebieBenefitPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String usageCount = "2000";
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create benefit
		Processor benefitResponse = superHelper.createBenefit(freeDelBenefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String benefitId = Integer.toString(benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		Processor freebieBenefitResponse = superHelper.createBenefit(freebieBenefitPayload);
		int freebieBenefitStatusCode = freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		String freebieBenefitId = Integer
				.toString(freebieBenefitResponse.ResponseValidator.GetNodeValueAsInt("$.data"));

		// //plan benefit mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean freebieMappingStatus = superHelper.mapPlanAndBenefit(planId, freebieBenefitId);
		Assert.assertEquals(freebieMappingStatus, true);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper
				.populatePlanUserIncentiveMappingPayload(planId, "USER", "null").toString();
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		SuperDbHelper.updateUsageCountOfPlan(planId, usageCount);

		// verify plan with the user id as zero string
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.planUsageLimitReached);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.planUsageLimitReached);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(freebieBenefitId);
	}

	@Test(dataProvider = "verifyPlanWherNoBenefitMappedButIncentiveIsMappedData", description = "verifying plan when no benefit is mapped with the plan but incentive is mapped", dataProviderClass = SuperDP.class)
	public void verifyPlanWherNoBenefitMappedButIncentiveIsMappedTest(String planPayload, String incentivePayload)
			throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planId = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String mappedUserId = Integer.toString(Utility.getRandom(1, 100000));

		// create Incentive (tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		Integer incentiveId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);

		// plan user incentive mapping for public user without incentive or incentive as
		// null

		String planUserIncentiveMapPlanTwoPayload = superHelper.populatePlanUserIncentiveMappingPayload(planId, "USER",
				Integer.toString(incentiveId));
		Processor planUserIncentiveMapPlanTwoResponse = superHelper
				.createPlanUserIncentiveMapping(planUserIncentiveMapPlanTwoPayload);
		int statusCodePlanUserIncentiveMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodePlanUserIncentiveMapPlanTwo, 1);
		String dataPlanUserIncenMapPlanTwo = planUserIncentiveMapPlanTwoResponse.ResponseValidator
				.GetNodeValue("$.data");
		Assert.assertEquals(dataPlanUserIncenMapPlanTwo, SuperConstants.successCheck);

		// verify plan with the user id as zero string
		String verifyPlanPayloadWithUserZero = superHelper.populateVerifyPlanPayload(planId, "0");
		Processor verifyPlanResponseWithUserZero = superHelper.verifyPlan(verifyPlanPayloadWithUserZero);
		int verifyStatusWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithUserZero, 1);
		String verifyMessageWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithUserZero, SuperConstants.benefitIsNotMappedWithPlan);
		boolean validPlanVerifyWithUserZero = verifyPlanResponseWithUserZero.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithUserZero, false);
		// verify plan with random user id
		String verifyPlanPayloadWithRandomUser = superHelper.populateVerifyPlanPayload(planId, userId);
		Processor verifyPlanResponseWithRandomUser = superHelper.verifyPlan(verifyPlanPayloadWithRandomUser);
		int verifyStatusWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(verifyStatusWithRandomUser, 1);
		String verifyMessageWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValue("$.data.message");
		Assert.assertEquals(verifyMessageWithRandomUser, SuperConstants.benefitIsNotMappedWithPlan);
		boolean validPlanVerifyWithRandomUser = verifyPlanResponseWithRandomUser.ResponseValidator
				.GetNodeValueAsBool("$.data.valid");
		Assert.assertEquals(validPlanVerifyWithRandomUser, false);

	}

	@Test(dataProvider = "createSubscriptionWithUserIdAsZeroData", description = "add subscription where user id zero", dataProviderClass = SuperDP.class)
	public void createSubscriptionWithUserIdZeroTest(String planPayload, String benefitPayload,
													 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, "0", orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 0);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.userInvalid);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, null, orderId);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 0);
		String dataTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataTwo, null);
		String statusMessageTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessageTwo, SuperConstants.userInvalid);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionWithPlanIdAsZeroData", description = "add subscription with plan id zero", dataProviderClass = SuperDP.class)
	public void createSubscriptionWithPlanIdAsZeroTest(String planPayload, String benefitPayload,
													   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload("0", userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 0);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planIdInvalid);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(null, userId, orderId);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 0);
		String dataTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataTwo, null);
		String statusMessageTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessageTwo, SuperConstants.planIdInvalid);
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionWithOrderIdAsZeroData", description = "add subscription with order id zero and null", dataProviderClass = SuperDP.class)
	public void createSubscriptionWithOrderIdAsZeroTest(String planPayload, String benefitPayload,
														String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, "0");
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 0);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.orderidInvalid);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, null);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 0);
		String dataTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataTwo, null);
		String statusMessageTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessageTwo, SuperConstants.orderidInvalid);
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionForUserForFuturePlansData", description = "add subscription with order id zero and null", dataProviderClass = SuperDP.class)
	public void createSubscriptionForUserForFuturePlansTest(String planPayload, String benefitPayload,
															String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode208);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiredPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionForFuturePlansToWhichNoBenefitIncenticeMappedData", description = "add subscription for future plan to which no benefit and incentive are mapped", dataProviderClass = SuperDP.class)
	public void createSubscriptionForFuturePlansToWhichNoBenefitIncenticeMappedTest(String planPayload,
																					String benefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		// boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingStatus, true);
		// boolean planUserIncentiveMapStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		// Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode205);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionForFuturePlansToWhichOnlyIncentiveMappedData", description = "add subscription for future plan to which only incentive is mapped", dataProviderClass = SuperDP.class)
	public void createSubscriptionForFuturePlansToWhichOnlyIncentiveMappedTest(String planPayload,
																			   String benefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		// boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode208);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiredPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createSubscriptionForExpiredPlansData", description = "add subscription with expired plan id", dataProviderClass = SuperDP.class)
	public void createSubscriptionForExpiredPlansTest(String planPayload, String benefitPayload,
													  String incentivePayload, String valid_till) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		System.out.println("valid till date --->>  " + valid_till);
		SuperDbHelper.updateValidTillOfPlanByValidTill( valid_till,planId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode208);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiredPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createOneMoreSubscriptionForUserAlreadyHaveSubsData", description = "try to create subs. for user who is already having active subs.", dataProviderClass = SuperDP.class)
	public void createOneMoreSubscriptionForUserAlreadyHaveSubsTest(String planPayload, String benefitPayload,
																	String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, SuperConstants.statusCode210);
		String data = subsResponseTwo.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponseTwo.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.userCantRenewPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "createOneMoreSubscriptionFutureSubscriptionWithInRenewalOffsetData", description = "try to create one more subs. with renewal offset or future subscription", dataProviderClass = SuperDP.class)
	public void createOneMoreSubscriptionFutureSubscriptionWithInRenewalOffsetTest(String planPayload,
																				   String benefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsTwoID = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "cancelCurrentSubsWithOrderIdWhenFutureSubsIsAlsoThereData", description = "cancel active subs with order id will not cancel the subs. because future subs is also taken by user", dataProviderClass = SuperDP.class)
	public void cancelCurrentSubsWithOrderIdWhenFutureSubsIsAlsoThereTest(String planPayload, String benefitPayload,
																		  String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsTwoID = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		String cancelSubsPayload = superHelper.populateCancelSubscriptionByOrderIdAndUserIdPayload(orderId, userId);
		// cancel 1st subs. by order id
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsPayload);
		int statusCodeOfCancel = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfCancel, 221);
		String statusMessageOfCancel = cancelResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessageOfCancel, SuperConstants.invalidOrderCancelRequest);
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsTwoID, planId, orderIdTwo);
		// check in subs history
		Processor historyCheck = superHelper.getSubscriptionHistoryOfUser(userId);
		String idInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + id + ")].id");
		Assert.assertEquals(idInHistory, "[" + id + "]");
		String statusMessageInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + id + ")].status");
		Assert.assertEquals(statusMessageInHistory, SuperConstants.subsYetToStart);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "buyOneSubsThenCancelAndAgainBuySamePlanData", description = "buy one subs. cancel active subs  then again buy same subs.", dataProviderClass = SuperDP.class)
	public void buyOneSubsThenCancelAndAgainBuySamePlanTest(String planPayload, String benefitPayload,
															String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// cancel 1st subs. by order id
		String cancelSubsPayload = superHelper.populateCancelSubscriptionByOrderIdAndUserIdPayload(orderId, userId);
		Processor cancelResponse = superHelper.cancelSubscription(cancelSubsPayload);
		int statusCodeOfCancel = cancelResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfCancel, 1);

		// superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(
		// userSubsID, orderId, userId, planId, "27");

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsTwoID = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);

		// check in subs history
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsTwoID, planId, orderIdTwo);
		Processor historyCheck = superHelper.getSubscriptionHistoryOfUser(userId);
		String idInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + id + ")].id");
		Assert.assertEquals(idInHistory, "[" + id + "]");
		String statusMessageInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + id + ")].status");
		Assert.assertEquals(statusMessageInHistory, SuperConstants.successMessgae);

		// check in subs history
		String idFirstSubs = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, planId, orderId);
		Processor historyCheckFirstSubs = superHelper.getSubscriptionHistoryOfUser(userId);
		String firstSubsIdInHistory = historyCheckFirstSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + idFirstSubs + ")].id");
		Assert.assertEquals(firstSubsIdInHistory, "[" + idFirstSubs + "]");
		String statusMessageOfFirstSubsInHistory = historyCheckFirstSubs.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + idFirstSubs + ")].status");
		Assert.assertEquals(statusMessageOfFirstSubsInHistory, SuperConstants.cancelledCheck);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "buySubsWithSameUserIdPlanIdOrderIdMultipleTimesData", description = "buy one subs. with same order id user id and plan id", dataProviderClass = SuperDP.class)
	public void buySubsWithSameUserIdPlanIdOrderIdMultipleTimesTest(String planPayload, String benefitPayload,
																	String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsTwoID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);

		// check in subs history
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, planId, orderId);
		Processor historyCheck = superHelper.getSubscriptionHistoryOfUser(userId);
		// String
		// idInHistory=historyCheck.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id=="+id+")].id");

		System.out.println("data 1 " + "$.data.[?(@.id==" + id + ")].id");
		System.out.println("data 2"
				+ historyCheck.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + id + ")].id"));
		String idInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.[?(@.id==" + id + ")].id");
		Assert.assertEquals(idInHistory, "[" + id + "]");
		String statusMessageInHistory = historyCheck.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data..[?(@.id==" + id + ")].status");
		Assert.assertEquals(statusMessageInHistory, SuperConstants.successMessgae);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "buyPublicPlanForUserWHoAlreadyHaveMappedPlanSubsData", description = "buy public plan subs. for user who already have mapped plan subs. OR buy mapped plan subs. for user already have public plan subs", dataProviderClass = SuperDP.class)
	public void buyPublicPlanForUserWHoAlreadyHaveMappedPlanSubsTest(String planPayload, String benefitPayload,
																	 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planIdTwo = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdThree = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdFour = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		boolean mappingStatusTwo = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingStatusTwo, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription where plan is public
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, SuperConstants.statusCode205);
		String data = subsResponseTwo.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponseTwo.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);

		// create subscription for public user
		String subscriptionPayloadThree = superHelper.populateCreateSubscriptionPayload(planIdTwo, publicUserId,
				orderIdThree);
		Processor subsResponseThree = superHelper.createSubscription(subscriptionPayloadThree);
		int statusCodeOfSubsRespThree = subsResponseThree.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespThree, 1);
		String userSubsIDThree = subsResponseThree.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsRespThree = subsResponseThree.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsRespThree), publicUserId);

		// create subscription where plan is for only mapped user
		String subscriptionPayloadFour = superHelper.populateCreateSubscriptionPayload(planId, publicUserId,
				orderIdFour);
		Processor subsResponseFour = superHelper.createSubscription(subscriptionPayloadFour);
		int statusCodeOfSubsRespFour = subsResponseFour.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespFour, SuperConstants.statusCode205);
		String dataFour = subsResponseFour.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(dataFour, null);
		String statusMessageFour = subsResponseFour.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessageFour, SuperConstants.planUserMappingMissing);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWhenTenurePeriodNotLyingUnderValidTillData", description = "buy subs. when present date plus tenure date is greater than valid till date of plan", dataProviderClass = SuperDP.class)
	public void buySubscriptionWhenTenurePeriodNotLyingUnderValidTillTest(String planPayload, String benefitPayload,
																		  String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		superHelper.updateValidTillOfThePlanByDays(planId, "5");

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.status218);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidPlanValidity);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWhenPlanUsageCountIsEqualToTotalAvailableData", description = "buy subs. when usage count of plan is equal to total available", dataProviderClass = SuperDP.class)
	public void buySubscriptionWhenPlanUsageCountIsEqualToTotalAvailableTest(String planPayload, String benefitPayload,
																			 String incentivePayload, String usageCount) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		SuperDbHelper.updateUsageCountOfPlan(planId, usageCount);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWhenPlanUsageCountIsAboutToreachItsMaxLimitOrTotalAvailIsZeroData", description = "buy subs. when usage count of plan is about to reach its max limit and when total available is 0", dataProviderClass = SuperDP.class)
	public void buySubscriptionWhenPlanUsageCountIsAboutToreachItsMaxLimitOrTotalAvailIsZeroTest(String planPayload,
																								 String benefitPayload, String incentivePayload, String usageCount) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		SuperDbHelper.updateUsageCountOfPlan(planId, usageCount);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, "0");

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userIdTwo, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsIDTwo = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsRespTwo), userIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWhenPlanIsDisabledData", description = "buy subs. when plan is disabled", dataProviderClass = SuperDP.class)
	public void buySubscriptionWhenPlanIsDisabledTest(String planPayload, String benefitPayload,
													  String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		superHelper.setActivePlanToDisableByValidTillDate(planId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userIdTwo, orderIdTwo);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode208);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiredPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWhenPatchPlanForTotalCountOfPlanData", description = "buy subs. when total count is 0 then patch the plan count and again buy the plan", dataProviderClass = SuperDP.class)
	public void buySubscriptionWhenPatchPlanForTotalCountOfPlanTest(String planPayload, String benefitPayload,
																	String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		String patchPlanPayload = superHelper.populatePatchPlanPayloadForCount(planId, "100");
		System.out.println("patch plan payload-->>>" + patchPlanPayload);
		superHelper.patchPlan(patchPlanPayload);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userIdTwo, orderIdTwo);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsIDTwo = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsRespTwo), userIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionAfterPatchPlanWithValidTillData", description = "buy subs. when plan is expired then patch the plan with its validtill and again try to buy subs", dataProviderClass = SuperDP.class)
	public void buySubscriptionAfterPatchPlanWithValidTillTest(String planPayload, String benefitPayload,
															   String incentivePayload, String patchValidTill) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 2000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		superHelper.setActivePlanToDisableByValidTillDate(planId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode208);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiredPlan);

		String patchPlanPayload = superHelper.populatePatchPlanPayloadByValidityCount(planId, patchValidTill, "100");
		System.out.println("patch plan payload-->>>" + patchPlanPayload);
		superHelper.patchPlan(patchPlanPayload);

		// create subscription
		String subscriptionPayloadTwo = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponseTwo = superHelper.createSubscription(subscriptionPayloadTwo);
		int statusCodeOfSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsRespTwo, 1);
		String userSubsIDTwo = subsResponseTwo.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsRespTwo = subsResponseTwo.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsRespTwo), userId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionWithNonExistingPlanIdData", description = "buy subs. when plan is expired then patch the plan with its validtill and again try to buy subs", dataProviderClass = SuperDP.class)
	public void buySubscriptionWithNonExistingPlanIdTest(String planPayload, String benefitPayload,
														 String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// delete plan
		SuperDbHelper.deletePlan(planId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode205);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buySubscriptionForDifferentPlanWhenCurrentPlanRenewOffsetIsNotStartedData", description = "buy different plan subs. when user already have one subs. and its renewaloff set is not yet started ", dataProviderClass = SuperDP.class)
	public void buySubscriptionForDifferentPlanWhenCurrentPlanRenewOffsetIsNotStartedTest(String planPayload,
																						  String benefitPayload, String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planIdTwo = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 2000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, SuperConstants.statusCode210);
		String data = subsTwoResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.userCantRenewPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionToWhichNoBenefitIsAttahcedData", description = "buy plan subs. when no benefit is attached with plan ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionToWhichNoBenefitIsAttahcedTest(String planPayload, String benefitPayload,
																  String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create Plan
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String planIdTwo = Integer.toString(planResponse.ResponseValidator.GetNodeValueAsInt("$.data"));
		Assert.assertEquals(statusCode, 1);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode210);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.benefitIsNotMappedWithPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionToWhichIncentiveTypeZeroAndMinusOneIsMappedData", description = "buy plan subs.to which both type incentives are mapped 0 and -1 ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionToWhichIncentiveTypeZeroAndMinusOneIsMappedTest(String planPayload,
																				   String benefitPayload, String incentivePayload, String incentiveTenureMinusOne) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create Incentive (tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentiveTenureMinusOne);
		Integer incentiveTwoId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveTwoId, null);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveTwoMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId,
				Integer.toString(incentiveTwoId), "USER");
		Assert.assertEquals(planUserIncentiveTwoMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhichIsForBothMappedUserAndPublicData", description = "buy plan subs.which is for both mapped and public users", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhichIsForBothMappedUserAndPublicTest(String planPayload, String benefitPayload,
																		 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 1000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveTwoMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveTwoMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserIdTwo,
				orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsTwoID = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhichIsForBothMappedUserAndPublicButForMappedIncentiveIsAlsoMappedData", description = "buy plan subs.which is for both mapped and public users but for mapped user incentive is also maped", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhichIsForBothMappedUserAndPublicButForMappedIncentiveIsAlsoMappedTest(
			String planPayload, String benefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, null, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveTwoMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveTwoMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserIdTwo,
				orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsTwoID = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserIdTwo);

		Processor userSubscResponse = superHelper.getUserSubscription(publicUserIdTwo, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		softAssert.assertEquals(subsUserPlanId, planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);

		Processor mappedUserSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = mappedUserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		softAssert.assertEquals(subsUserTwoPlanId, planId);
		String subsUserBenefitIdTwo = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		softAssert.assertEquals(subsUserBenefitIdTwo.isEmpty(), false);
		String subsUserIncentiveCheck = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveId + ")].user_offer"));

		Assert.assertEquals(subsUserIncentiveCheck, "\"[true]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhereNoPlanUserIncentiveMappingData", description = "buy plan subs.where plan-user-incentive mapping is not done only beneift is mapped", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhereNoPlanUserIncentiveMappingTest(String planPayload, String benefitPayload,
																	   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		// boolean planUserIncentiveMapStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planId, null, "USER");
		// Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode205);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhereNoPlanBenefitMappingData", description = "buy plan subs.where plan-benefit mapping is not done", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhereNoPlanBenefitMappingTest(String planPayload, String benefitPayload,
																 String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		// boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, null, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode210);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.benefitIsNotMappedWithPlan);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhereNoPlanBenefitAndPlanUserIncentiveMappingData", description = "buy plan subs.where plan-benefit mapping and plan-user-incnetive mapping is not done", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhereNoPlanBenefitAndPlanUserIncentiveMappingTest(String planPayload,
																					 String benefitPayload, String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		// boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingStatus, true);
		// boolean planUserIncentiveMapStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planId, null, "USER");
		// Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, SuperConstants.statusCode205);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWithUserIdZeroData", description = "buy plan subs. user id zero", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWithUserIdZeroTest(String planPayload, String benefitPayload,
													  String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, "0", orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 0);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.userInvalid);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWithOrderIdZeroData", description = "buy plan subs. order id zero", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWithOrderIdZeroTest(String planPayload, String benefitPayload,
													   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, "0");
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 0);
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.orderidInvalid);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWithTheDuplicateOrderIdData", description = "buy plan subs. with duplicate order id ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWithTheDuplicateOrderIdTest(String planPayload, String benefitPayload,
															   String incentivePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsTwoID = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userIdTwo, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, subDuplicateStatusCode, "allowing to create subs. with duplicate order id");
		String data = subsResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusMessage = subsResponse.ResponseValidator.GetNodeValue("$.statusMessage");
//        Assert.assertEquals(statusMessage, SuperConstants.orderidInvalid);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionWhereMultipleBenefitsAreMappedData", description = "buy plan subs. where multiple benefits are attached with the plan ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionWhereMultipleBenefitsAreMappedTest(String planPayload, String benefitPayload,
																	  String incentivePayload, String benefitTwoPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create benefit- FREE DEL
		Processor benefitResponse = superHelper.createBenefit(benefitTwoPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitTwoId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, Integer.toString(benefitTwoId));
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsTwoID = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);

		Processor mappedUserSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = mappedUserSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		softAssert.assertEquals(subsUserTwoPlanId, planId);
		String subsUserBenefitId = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		softAssert.assertEquals(subsUserBenefitId.isEmpty(), false);
		String subsUserBenefitIdTwo = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitTwoId + ")]"));
		String subsUserIncentiveCheck = JSON.toString(mappedUserSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveId + ")].user_offer"));

		Assert.assertEquals(subsUserIncentiveCheck, "\"[true]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckSubscriptionRemainSameData", description = "buy plan subs. check subs id remain same for user  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckSubscriptionRemainSameTest(String planPayload, String benefitPayload,
																   String incentivePayload, String benefitTwoPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create benefit- FREE DEL
		Processor benefitResponse = superHelper.createBenefit(benefitTwoPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitTwoId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, Integer.toString(benefitTwoId));
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);

		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		Assert.assertEquals(userSubsID, userSubsIDTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckPlanIdInResponseData", description = "buy plan subs. check plan id in response  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckPlanIdInResponseTest(String planPayload, String benefitPayload,
															 String incentivePayload, String benefitTwoPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create benefit- FREE DEL
		Processor benefitResponse = superHelper.createBenefit(benefitTwoPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitTwoId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, Integer.toString(benefitTwoId));
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckValidTillAndValidFromResponseData", description = "buy plan subs. check validTill and ValidFrom in response  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckValidTillAndValidFromResponseTest(String planPayload, String benefitPayload,
																		  String incentivePayload, String benefitTwoPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create benefit- FREEbie
		Processor benefitResponse = superHelper.createBenefit(benefitTwoPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitTwoId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 1000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, Integer.toString(benefitTwoId));
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		String validFromInSubsResp = subsResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.valid_from");
		String validFrom = Utility.getDateInToMilliseconds(
				SuperDbHelper.getValidfromFromSubscriptionForUserSubs(userSubsID, planId, orderId));
		String validTill = Utility.getDateInToMilliseconds(
				SuperDbHelper.getValidtillFromSubscriptionForUserSubs(userSubsID, planId, orderId));

		Assert.assertEquals(validFromInSubsResp, validFrom);
		String validTillInSubsResp = subsResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.valid_till");
		Assert.assertEquals(validTillInSubsResp, validTill);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckMappedUserNotAbleToBuyPublicPlanInRenewalPeriodData", description = "check mapped user not able to buy public plan in renewal period  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckMappedUserNotAbleToBuyPublicPlanInRenewalPeriodTest(String planPayload,
																							String benefitPayload, String incentivePayload, String benefitTwoPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitTwoPayload, incentivePayload);
		String publicPlanId = planBenefitIncentiveIds[0];
		String publicBenefitId = planBenefitIncentiveIds[1];
		String publicIncentiveId = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(publicPlanId, publicBenefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(publicPlanId,
				publicIncentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(publicPlanId, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, SuperConstants.statusCode205);
		String statusMessage = subsTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planUserMappingMissing);
		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckWhenBenfitDeletedData", description = "check mapped user not able to buy public plan in renewal period  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckWhenBenfitDeletedTest(String planPayload, String benefitPayload,
															  String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		SuperDbHelper.deleteBenefitFromDB(benefitId);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckForDisabledIncentiveData", description = "check mapped user not able to buy public plan in renewal period  ", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckForDisabledIncentiveTest(String planPayload, String benefitPayload,
																 String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// incentive_id, tenure, enabled
		patchIncentive.put("0", incentiveId);
		patchIncentive.put("1", "0");
		patchIncentive.put("2", "0");
		String patchPayload = superHelper.populatePatchIncentiveByEnableDisableTenurePayload(patchIncentive);
		superHelper.patchIncentive(patchPayload);

		Processor planRespByUserIdbyMappedUser = superHelper.getPlansByUserId(userId, "true");
		String benefitDetailsOfMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId
						+ ")].benefits.[?(@.benefit_id==" + benefitId + ")]");
		String incentiveDiscountAppliedForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_applied");
		String incentiveDiscountInfoForMappedUser = planRespByUserIdbyMappedUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planId + ")].discount_info");
		Assert.assertEquals(incentiveDiscountInfoForMappedUser, "[null]");
		Assert.assertEquals(benefitDetailsOfMappedUser.isEmpty(), false);
		Assert.assertEquals(incentiveDiscountAppliedForMappedUser, "[false]");

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "buyPlanSubscriptionCheckAfterBuyingSubsDisabledIncentiveData", description = "check when incentive got disabled after then user subs that plan,user should get tha incentive", dataProviderClass = SuperDP.class)
	public void buyPlanSubscriptionCheckAfterBuyingSubsDisabledIncentiveTest(String planPayload, String benefitPayload,
																			 String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// incentive_id, tenure, enabled
		patchIncentive.put("0", incentiveId);
		patchIncentive.put("1", "0");
		patchIncentive.put("2", "0");
		String patchPayload = superHelper.populatePatchIncentiveByEnableDisableTenurePayload(patchIncentive);
		superHelper.patchIncentive(patchPayload);

		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		String subsUserIncentiveId = JSON.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId,"\""+ "[]"+"\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getuserSubscriptionCheckCanceledSubsData", description = "get user subscription when user canceled took subs and then canceled it. ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckCanceledSubsTest(String planPayload, String benefitPayload,
														 String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];
		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		String cancelSubsPayload = superHelper
				.populateCancelSubscriptionBySubscriptionIdOrderIdAndUserIdPayload(userSubsID, orderId, userId);
		superHelper.cancelSubscription(cancelSubsPayload);

		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		String statusMessage = userSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.noSubsFound);
		int statusCode = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.statusCode202);
		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getuserSubscriptionCheckCurrentSubsDetailsWhenUserHaveRecentlyCanceledOneSubsData",
			description = "get user current subscription when before this user cancelled one subs. ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckCurrentSubsDetailsWhenUserHaveRecentlyCanceledOneSubsTest(String planPayload,
																								  String benefitPayload, String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];
		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);


		String cancelSubsPayload = superHelper
				.populateCancelSubscriptionBySubscriptionIdOrderIdAndUserIdPayload(userSubsID, orderId, userId);
		superHelper.cancelSubscription(cancelSubsPayload);

		// create subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId + "1");
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsTwoID = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planId);

		Processor subscriptionDetails = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = subscriptionDetails.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(subscriptionDetails.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		String subsUserIncentiveId = JSON
				.toString(subscriptionDetails.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "", description = "get user subscription for non super user or non existing user. ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckSubsForNonSuperUserOrNonExistingUserTest() throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		// non existing user id
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		String statusMessage = userSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.noSubsFound);
		int statusCode = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.statusCode202);
	}

	@Test(dataProvider = "", description = "get user subscription for user id zero. ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckSubsForUserIdZeroTest() throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		// non existing user id
		Processor userSubscResponse = superHelper.getUserSubscription("0", "true");
		String statusMessage = userSubscResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.noSubsFound);
		int statusCode = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.statusCode202);
	}

	@Test(dataProvider = "", description = "get user subscription for user id as blank space ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckSubsForUserIdBlankSpaceTest() throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		// non existing user id
		Processor userSubscResponse = superHelper.getUserSubscription("   ", "true");
		int statusCode = userSubscResponse.ResponseValidator.GetResponseCode();
		Assert.assertEquals(statusCode, 404);

	}

	@Test(dataProvider = "getuserSubscriptionCheckWhenBenefitParamIsFalseData", description = "get user subs. when benefit URL param is false should not get benefits ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckWhenBenefitParamIsFalseTest(String planPayload, String benefitPayload,
																	String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];
		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		Processor userSubscResponse = superHelper.getUserSubscription(userId, "false");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.benefits"));
		Assert.assertEquals(subsUserBenefitId, "\"[]\"");
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "getuserSubscriptionCheckTenureZeroIncentiveIsNotVisibleData", description = "get user subscription and 0-tenure incentive should not visible ", dataProviderClass = SuperDP.class)
	public void getuserSubscriptionCheckTenureZeroIncentiveIsNotVisibleTest(String planPayload, String benefitPayload,
																			String incentivePayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];
		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[]\"");
		SuperDbHelper.deleteBenefitFromDB(benefitId);

	}

	@Test(dataProvider = "getUserSubscriptionCheckAfterBuyingPlanWhenMoreIncentivesAddedToPlanData", description = "check user subscription when after buying plan more incentives added so new incentive should not get to user who already buyed plan", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckAfterBuyingPlanWhenMoreIncentivesAddedToPlanTest(String planPayload,
																						 String benefitPayload, String incentivePayload, String incentiveFlatPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentiveFlatPayload);
		Integer incentiveTwoId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveTwoId, null);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		// get user subs.
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitId.isEmpty(), false);
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId,
				Integer.toString(incentiveTwoId), userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitTwoId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitTwoId.isEmpty(), false);
		String subsUserIncentiveOneId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveOneId, "\"[true]\"");

		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveTwoId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckWhenMultipleIncentiveAddedThenOnlyOneWillGetBasedOnPriorityData", description = "check user subscription when multiple incentives added to plan then only one incentive should be visible based on priority", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckWhenMultipleIncentiveAddedThenOnlyOneWillGetBasedOnPriorityTest(
			String planPayload, String benefitPayload, String incentivePayload, String incentiveFlatPayload)
			throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		HashMap<String, String> patchIncentive = new HashMap<String, String>();
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentiveFlatPayload);
		Integer incentiveTwoId = new Integer(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveTwoId, null);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId,
				Integer.toString(incentiveTwoId), userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitTwoId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitTwoId.isEmpty(), false);
		String subsUserIncentiveOneId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveOneId, "\"[]\"");

		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveTwoId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckWhenSubscriptionGotExpiredData", description = "check user subscription expired subs. should not be visible", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckWhenSubscriptionGotExpiredTest(String planPayload, String benefitPayload,
																	   String incentivePayload, String incentiveFlatPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// create incentive(tenure zero only)
		Processor incentiveResp = superHelper.createIncentive(incentiveFlatPayload);
		Integer incentiveTwoId = incentiveResp.ResponseValidator.GetNodeValueAsInt(" $.data");
		Assert.assertNotEquals(incentiveTwoId, null);

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertTrue(mappingStatus);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertTrue(planUserIncentiveMapStatus);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId,
				Integer.toString(incentiveTwoId), userId);
		Assert.assertTrue(planUserIncentiveMapStatus);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		superHelper.setPresentSubscriptionToExpiredSubsByValidTillDate(userSubsID, orderId, userId, planId);
		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		String statusMessage = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.noSubsFound);
		int statusCode = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.statusCode202);
		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckFutureSubsNotVisibleOnlyCurrentSubsVisibleData", description = "check user subscription future subs should not be visible only current subs should be visible", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckFutureSubsNotVisibleOnlyCurrentSubsVisibleTest(String planPayload,
																					   String benefitPayload, String incentivePayload, String benefitFreebiePayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentivePayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 1000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create future subscription
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planIdTwo);

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitTwoId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")]"));
		Assert.assertEquals(subsUserBenefitTwoId.isEmpty(), false);
		String subsUserIncentiveOneId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveOneId, "\"[true]\"");
		int subsUserPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserBenefitId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckBenefitsAppearsOnTheBasesOfTheirPriorityData", description = "check user subscription future subs should not be visible only current subs should be visible", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckBenefitsAppearsOnTheBasesOfTheirPriorityTest(String planPayload,
																					 String benefitPayload, String incentivePayload, String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitTwoId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitTwoId, "\"[false]\"");
		String subsUserIncentiveOneId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveOneId, "\"[true]\"");
		int subsUserPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserBenefitId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckMappedUserIncentiveIsNotVisibleToPublicData", description = "check user subscription when same plan is for mapped and public user then mapped user incentive should not be visible to public", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckMappedUserIncentiveIsNotVisibleToPublicTest(String planPayload,
																					String benefitPayload, String incentivePayload, String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderId = Integer.toString(Utility.getRandom(200, 1000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// get user subs. mapped user
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");
		int subsUserPlanIdOne = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdOne), planIdTwo);
		String subsUserBenefitTwoId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitTwoId, "\"[false]\"");

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planId);

		// get user subs. of public user
		Processor userSubscTwoResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitOneId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitOneId, "\"[false]\"");
		String subsUserIncentiveTwoId = JSON.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[]\"");
		int subsUserPlanIdTwo = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdTwo), planIdTwo);
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckPublicUserIncentiveIsNotVisibleToMappedData", description = "check user subscription when same plan is for mapped and public user then public user incentive should not be visible to mapped user", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckPublicUserIncentiveIsNotVisibleToMappedTest(String planPayload,
																					String benefitPayload, String incentivePayload, String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, null, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// get user subs. mapped user
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[]\"");
		int subsUserPlanIdOne = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdOne), planIdTwo);
		String subsUserBenefitTwoId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitTwoId, "\"[false]\"");

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planId);

		// get user subs. of public user
		Processor userSubscTwoResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitOneId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitOneId, "\"[false]\"");
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		int subsUserPlanIdTwo = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdTwo), planIdTwo);
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider="getUserSubscriptionCheckIncentiveIsVisibleToBothMappedAndPublicUserData", description = "check user subscription when same plan is for mapped and public user then to " +
			"public user and mapped incentive should be visible because incentive mapped for both type users",
			dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckIncentiveIsVisibleToBothMappedAndPublicUserTest(String planPayload,
																						String benefitPayload,
																						String incentivePayload,
																						String benefitFreebiePayload,
																						String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 1000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// get user subs. mapped user
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");
		int subsUserPlanIdOne = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdOne), planIdTwo);
		String subsUserBenefitTwoId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitTwoId, "\"[false]\"");

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planId);

		// get user subs. of public user
		Processor userSubscTwoResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitOneId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitOneId, "\"[false]\"");
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		int subsUserPlanIdTwo = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdTwo), planIdTwo);
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckWhenPlanGotDisableAfterTakingSubscriptionData", description = "check user subscription when after taking subs. plan got disabled then plan and benefits should get for the subscribed user", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckWhenPlanGotDisableAfterTakingSubscriptionTest(String planPayload,
																					  String benefitPayload, String incentivePayload, String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1, 1000000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1, 1000000));

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planId, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription for mapped user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planId, publicUserId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), publicUserId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planId);

		String planPatchPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planId, false);
		superHelper.patchPlan(planPatchPayload);

		// get user subs. mapped user
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");
		int subsUserPlanIdOne = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdOne), planIdTwo);
		String subsUserBenefitTwoId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitTwoId, "\"[false]\"");

		// get user subs. of public user
		Processor userSubscTwoResponse = superHelper.getUserSubscription(publicUserId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planId);
		String subsUserBenefitOneId = JSON.toString(userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitOneId, "\"[false]\"");
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		int subsUserPlanIdTwo = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertNotEquals(Integer.toString(subsUserPlanIdTwo), planIdTwo);
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckWhenOnePlanIsExpiredAndAnothPlanIsAboutToStartData", description = "check user subscription when after taking subs. plan got disabled then plan and benefits should get for the subscribed user", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckWhenOnePlanIsExpiredAndAnothPlanIsAboutToStartTest(String planPayload,
																						   String benefitPayload, String incentivePayload, String benefitFreebiePayload, String incentiveFlatPayload,
																						   String validTill, String validFrom) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitPayload,
				incentivePayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));
		String orderId = Integer.toString(Utility.getRandom(1,10000000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);

		boolean mappingStatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription for mapped user
		String subscriptionPayload = superHelper.populateCreateSubscriptionPayload(planId, userId, orderId);
		Processor subsResponse = superHelper.createSubscription(subscriptionPayload);
		int statusCodeOfSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsResp, 1);
		String userSubsID = subsResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsResp), userId);
		int planIdInSubsResp = subsResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsResp), planId);
		superHelper.setPresentSubscriptionToRenewalSubsPeriodByValidFromDate(userSubsID, orderId, userId, planId, "27");

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planIdTwo);

		// get user subs.
		Processor userSubscResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserPlanId = userSubscResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserPlanId), planId);
		String subsUserBenefitId = JSON.toString(userSubscResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits.[?(@.benefit_id==" + benefitId + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitId, "\"[false]\"");
		String subsUserIncentiveId = JSON
				.toString(userSubscResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveId + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveId, "\"[true]\"");

		// expire present subs.
		superHelper.setPresentSubscriptionToExpiredSubsByValidTillDate(userSubsID, orderId, userId, planId);
		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsID, planIdTwo, orderIdTwo);

		// set future subscription to current subs.
		SuperDbHelper.updateValidFromAndValidTillByIdInSusbscription(validFrom, validTill, id,
				Integer.toString(userIDInSubsResp));

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");
		Assert.assertEquals(userSubsID, userSubsIDTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
	}

	@Test(dataProvider = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsPlanData", description = "check all the details of current active subs. plan of user in user-subscription", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsPlanData(String planPayload,
																				String benefitFreebiePayload, String incentiveFlatPayload, String validTill, String validFrom) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);
		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planIdTwo);

		String id = SuperDbHelper.getIdFromSubscriptionForUserSubs(userSubsIDTwo, planIdTwo, orderIdTwo);
		// set future subscription to current subs.
		SuperDbHelper.updateValidFromAndValidTillByIdInSusbscription(validFrom, validTill, id,
				Integer.toString(userIDInSubsTwoResp));

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");
		String subsIdInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		Assert.assertEquals(subsIdInUserSubsResp, userSubsIDTwo);
		int planTenureInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_tenure");
		Assert.assertEquals(planTenureInUserSubsResp, 1);
		String planName = createPlan.getName();
		String planNameInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.plan_name");
		Assert.assertEquals(planNameInUserSubsResp, planName);
		String planTitle = createPlan.getTitle();
		String planTitleInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.plan_title");
		Assert.assertEquals(planTitleInUserSubsResp, planTitle);
		String planDescription = createPlan.getDescription();
		String planDescriptionInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValue("$.data.plan_description");
		Assert.assertEquals(planDescriptionInUserSubsResp, planDescription);
		String planLogoId = createPlan.getLogoId();
		String planLogoIdInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.logo_id");
		Assert.assertEquals(planLogoIdInUserSubsResp, planLogoId);
		String planTnc = createPlan.getTnc();
		String planTncInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValue("$.data.terms_and_condition");
		Assert.assertEquals(planTncInUserSubsResp, planTnc);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsIncentiveData", description = "check all the details of current active subs. incentive of user in user-subscription", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsIncentiveTest(String planPayload,
																					 String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String orderId = Integer.toString(Utility.getRandom(1,10000000));

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderIdTwo = String.valueOf(Long.valueOf(orderId) + 1);
		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planIdTwo);

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");
		String subsIdInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		Assert.assertEquals(subsIdInUserSubsResp, userSubsIDTwo);
		int planTenureInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_tenure");
		Assert.assertEquals(planTenureInUserSubsResp, 1);
		String benefitName = createIncentive.getName();
		String benefitNameInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].name");
		Assert.assertEquals(benefitNameInUserSubsResp, "[\"" + benefitName + "\"]");
		String benefitTitle = createIncentive.getTitle();
		String benefitTitleInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].title");
		Assert.assertEquals(benefitTitleInUserSubsResp, "[\"" + benefitTitle + "\"]");
		String benefitDescription = createIncentive.getDescription();
		String benefitDescriptionInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].description");
		Assert.assertEquals(benefitDescriptionInUserSubsResp, "[\"" + benefitDescription + "\"]");
		String benefitLogoId = createIncentive.getLogo_id();
		String benefitLogoIdInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].logo_id");
		Assert.assertEquals(benefitLogoIdInUserSubsResp, "[\"" + benefitLogoId + "\"]");
		String benefitTypeInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].type");
		Assert.assertEquals(benefitTypeInUserSubsResp, "[\"NONE\"]");
		String benefitMetaTypeInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].meta.type");
		Assert.assertEquals(benefitMetaTypeInUserSubsResp, "[\"FLAT\"]");
		String benefitMetaValueInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].meta.value");
		Assert.assertEquals(benefitMetaValueInUserSubsResp, "[10.0]");
		String benefitMetaCapInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].meta.cap");
		Assert.assertEquals(benefitMetaCapInUserSubsResp, "[-1]");
		String benefitMetavalidPaymentMethodsInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].meta.valid_payment_methods");
		Assert.assertEquals(benefitMetavalidPaymentMethodsInUserSubsResp, "[" + null + "]");
		String benefitUserOfferUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.benefits[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer");
		Assert.assertEquals(benefitUserOfferUserSubsResp, "[true]");
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsBenefitData", description = "check all the details of current active subs. benefit of user in user-subscription", dataProviderClass = SuperDP.class)
	public void getUserSubscriptionCheckAllDetailsOfCurrentlyActiveSubsBenefitTest(String planPayload,
																				   String benefitFreebiePayload, String incentiveFlatPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload,
				benefitFreebiePayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String orderIdTwo = Integer.toString(Utility.getRandom(1,10000000));
		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// create subscription for public user
		String subscriptionTwoPayload = superHelper.populateCreateSubscriptionPayload(planIdTwo, userId, orderIdTwo);
		Processor subsTwoResponse = superHelper.createSubscription(subscriptionTwoPayload);
		int statusCodeOfSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfSubsTwoResp, 1);
		String userSubsIDTwo = subsTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		int userIDInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.user_id");
		Assert.assertEquals(Integer.toString(userIDInSubsTwoResp), userId);
		int planIdInSubsTwoResp = subsTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(planIdInSubsTwoResp), planIdTwo);

		// get user subs.
		Processor userSubscTwoResponse = superHelper.getUserSubscription(userId, "true");
		int subsUserTwoPlanId = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_id");
		Assert.assertEquals(Integer.toString(subsUserTwoPlanId), planIdTwo);
		String subsUserIncentiveTwoId = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + incentiveIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserIncentiveTwoId, "\"[true]\"");
		String subsUserBenefitIdTwo = JSON
				.toString(userSubscTwoResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits.[?(@.benefit_id==" + benefitIdTwo + ")].user_offer"));
		Assert.assertEquals(subsUserBenefitIdTwo, "\"[false]\"");
		String subsIdInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValue("$.data.subscription_id");
		Assert.assertEquals(subsIdInUserSubsResp, userSubsIDTwo);
		int planTenureInUserSubsResp = userSubscTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data.plan_tenure");
		Assert.assertEquals(planTenureInUserSubsResp, 1);
		String benefitName = createBenefit.getName();
		String benefitNameInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].name");
		Assert.assertEquals(benefitNameInUserSubsResp, "[\"" + benefitName + "\"]");
		String benefitTitle = createBenefit.getTitle();
		String benefitTitleInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].title");
		Assert.assertEquals(benefitTitleInUserSubsResp, "[\"" + benefitTitle + "\"]");
		String benefitDescription = createBenefit.getDescription();
		String benefitDescriptionInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray(
						"$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].description");
		Assert.assertEquals(benefitDescriptionInUserSubsResp, "[\"" + benefitDescription + "\"]");
		String benefitLogoId = createBenefit.getLogoId();
		String benefitLogoIdInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].logo_id");
		Assert.assertEquals(benefitLogoIdInUserSubsResp, "[\"" + benefitLogoId + "\"]");
		String benefitTypeInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].type");
		Assert.assertEquals(benefitTypeInUserSubsResp, "[\"Freebie\"]");
		String benefitMetaInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].meta");
		Assert.assertEquals(benefitMetaInUserSubsResp, "[" + null + "]");
		String benefitUserOfferInUserSubsResp = userSubscTwoResponse.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.benefits[?(@.benefit_id==" + benefitIdTwo + ")].user_offer");
		Assert.assertEquals(benefitUserOfferInUserSubsResp, "[false]");
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserAsNonLoggedPublicAndMappedUserData", description = "check plan-user as a nonlogged, public and mapped user, where only public plans should be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserAsNonLoggedPublicAndMappedUserTest(String planPayload, String benefitFreebiePayload,
																String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// get plan by user id with user id as zero
		Processor planUserResp = superHelper.getPlansByUserId("0", "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List<String> listOfPlanIds = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				("$.data.plan_list..plan_id"));
		Assert.assertEquals(listOfPlanIds.contains("[" + planId + "]"), false);

		// get plan by user id with random user id (public)
		Processor planUserRespForPublicUser = superHelper.getPlansByUserId(publicUserId, "true");
		int statusCodeplanUserRespForPublicUser = planUserRespForPublicUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeplanUserRespForPublicUser, 1);
		String planIdInPlanUserResplanUserRespForPublicUserp = planUserRespForPublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResplanUserRespForPublicUserp, "[" + planIdTwo + "]");
		List<String> listOfPlanIdsplanUserRespForPublicUser = JsonPath
				.read(planUserRespForPublicUser.ResponseValidator.GetBodyAsText(), ("$.data.plan_list..plan_id"));
		Assert.assertEquals(listOfPlanIdsplanUserRespForPublicUser.contains("[" + planId + "]"), false);

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userId, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		List<String> listOfPlanIdsFroMappedUser = JsonPath
				.read(planUserRespForMappeduser.ResponseValidator.GetBodyAsText(), ("$.data.plan_list..plan_id"));
		Assert.assertEquals(listOfPlanIdsFroMappedUser.contains("[" + planIdTwo + "]"), false);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckAllActivePublicPlansForPublicUserData", description = "check plan-user for all active public plans", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckAllActivePublicPlansForPublicUserTest(String planPayload,
																		String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																		String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String publicUserId = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// get plan by user id with user id as zero
		Processor planUserResp = superHelper.getPlansByUserId("0", "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserOneResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserOneResp, "[" + planId + "]");
		// List<String> listOfPlanIds =
		// JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),("$.data.plan_list..plan_id"));
		// Assert.assertEquals(listOfPlanIds.contains("["+planId+"]"),true);

		// get plan by user id with random user id (public)
		Processor planUserRespForPublicUser = superHelper.getPlansByUserId(publicUserId, "true");
		int statusCodeplanUserRespForPublicUser = planUserRespForPublicUser.ResponseValidator
				.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeplanUserRespForPublicUser, 1);
		String planIdInPlanUserResplanUserRespForPublicUserp = planUserRespForPublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResplanUserRespForPublicUserp, "[" + planIdTwo + "]");
		String planIdTwoInPlanUserResplanUserRespForPublicUserp = planUserRespForPublicUser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdTwoInPlanUserResplanUserRespForPublicUserp, "[" + planId + "]");
		// List<String> listOfPlanIdsplanUserRespForPublicUser =
		// JsonPath.read(planUserRespForPublicUser.ResponseValidator.GetBodyAsText(),("$.data.plan_list..plan_id"));
		// Assert.assertEquals(listOfPlanIdsplanUserRespForPublicUser.contains("["+planId+"]"),true);

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userId, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		// List<String> listOfPlanIdsFroMappedUser =
		// JsonPath.read(planUserRespForMappeduser.ResponseValidator.GetBodyAsText(),("$.data.plan_list..plan_id"));
		// Assert.assertEquals(listOfPlanIdsFroMappedUser.contains("["+planIdTwo+"]"),true);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckAllActiveMappedUserPlansForMappedUserData", description = "check plan-user for all active mapped user plans for mapped user", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckAllActiveMappedUserPlansForMappedUserTest(String planPayload,
																			String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																			String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckExpiredMappedUserPlanData", description = "check plan-user for expired mapped user plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckExpiredMappedUserPlanTest(String planPayload, String benefitFreebiePayload,
															String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 1000000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 2000000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		// Expire plan
		superHelper.setActivePlanToDisableByValidTillDate(planId);
		superHelper.setActivePlanToDisableByValidTillDate(planIdTwo);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list");
		Assert.assertEquals(planIdInPlanUserResp, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckExpiredPublicUserPlanData", description = "check plan-user for expired public plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckExpiredPublicUserPlanTest(String planPayload, String benefitFreebiePayload,
															String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		// Expire plan
		superHelper.setActivePlanToDisableByValidTillDate(planId);
		superHelper.setActivePlanToDisableByValidTillDate(planIdTwo);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckDisablePublicUserPlanData", description = "check plan-user for disable public plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckDisablePublicUserPlanTest(String planPayload, String benefitFreebiePayload,
															String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		String patchPlanPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planId, false);
		superHelper.patchPlan(patchPlanPayload);
		String patchPlanTwoPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planIdTwo, false);
		superHelper.patchPlan(patchPlanTwoPayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckDisableMappedUserPlanData", description = "check plan-user for disable mapped plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckDisableMappedUserPlanTest(String planPayload, String benefitFreebiePayload,
															String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		String patchPlanPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planId, false);
		superHelper.patchPlan(patchPlanPayload);
		String patchPlanTwoPayload = superHelper.populatePatchPlanPayloadByPlanIdEnabled(planIdTwo, false);
		superHelper.patchPlan(patchPlanTwoPayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhereMappedUserPlanTenureIsGreaterThanValidTillOfPlanData", description = "check plan-user for disable mapped plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhereMappedUserPlanTenureIsGreaterThanValidTillOfPlanTest(String planPayload,
																							String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																							String benefitFreeDelPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		superHelper.updateValidTillOfThePlanByDays(planId, "6");
		superHelper.updateValidTillOfThePlanByDays(planIdTwo, "6");

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWherePublicPlanTenureIsGreaterThanValidTillOfPlanData", description = "check plan-user for disable mapped plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWherePublicPlanTenureIsGreaterThanValidTillOfPlanTest(String planPayload,
																						String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																						String benefitFreeDelPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		superHelper.updateValidTillOfThePlanByDays(planId, "6");
		superHelper.updateValidTillOfThePlanByDays(planIdTwo, "6");

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + incentiveId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckFuturePublicPlanData", description = "check plan-user for disable mapped plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckFuturePublicPlanTest(String planPayload, String benefitFreebiePayload,
													   String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		superHelper.updateValidTillOfThePlanByDays(planId, "6");
		superHelper.updateValidTillOfThePlanByDays(planIdTwo, "6");

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + incentiveId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckFutureMappedUserPlanData", description = "check plan-user for disable mapped plans, expired plan should not be visible", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckFutureMappedUserPlanTest(String planPayload, String benefitFreebiePayload,
														   String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload) throws ParseException {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);

		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		superHelper.updateValidTillOfThePlanByDays(planId, "6");
		superHelper.updateValidTillOfThePlanByDays(planIdTwo, "6");

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserRespOneUser = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + incentiveId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespTwoUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwoUser, "[]");
		String incentiveIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + incentiveId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespOneUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckTotalAvailableOfMappedUserPlanData", description = "check plan-user for all active mapped user plans for mapped user", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckTotalAvailableOfMappedUserPlanTest(String planPayload, String benefitFreebiePayload,
																	 String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckTotalAvailableOfPublicPlanData", description = "check plan-user for all active mapped user plans for mapped user", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckTotalAvailableOfPublicPlanTest(String planPayload, String benefitFreebiePayload,
																 String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenTotalAvailableReachedItsMaxLimitPublicPlanData", description = "check public plan-user when total available reached to its max limit", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenTotalAvailableReachedItsMaxLimitPublicPlanTest(String planPayload,
																				String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																				String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenTotalAvailableReachedItsMaxLimitMappedUserPlanData", description = "check mapped user plan-user when total available reached to its max limit", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenTotalAvailableReachedItsMaxLimitMappedUserPlanTest(String planPayload,
																					String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																					String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenTotalAvailableIsZeroMappedUserPlanData", description = "check mapped user plan-user when total available reached to its max limit", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenTotalAvailableIsZeroMappedUserPlanTest(String planPayload,
																		String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																		String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenTotalAvailableIsZeroPublicPlanData", description = "check public plan-user when total available reached to its max limit", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenTotalAvailableIsZeroPublicPlanTest(String planPayload, String benefitFreebiePayload,
																	String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload, String totalCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// boolean planUserIncentiveMapStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,null , userIdTwo);
		// Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		// boolean planUserIncentiveMapThreeStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planId,incentiveId , userId);
		// Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		SuperDbHelper.updateTotalAvailableOfPlan(planId, totalCount);
		SuperDbHelper.updateTotalAvailableOfPlan(planIdTwo, totalCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenUsageCountIsEqualToTotalAvailablePublicPlanData", description = "check public plan-user when usage count is total available", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenUsageCountIsEqualToTotalAvailablePublicPlanTest(String planPayload,
																				 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																				 String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// boolean planUserIncentiveMapStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,null , userIdTwo);
		// Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		// boolean planUserIncentiveMapThreeStatus =
		// superHelper.mapPlanIncentiveAndUserMapping(planId,incentiveId , userId);
		// Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		SuperDbHelper.updateUsageCountOfPlan(planId, usageCount);
		SuperDbHelper.updateUsageCountOfPlan(planIdTwo, usageCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenUsageCountIsEqualToTotalAvailableMappedUserPlanData", description = "check mapped user plan-user when usage count is total available", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenUsageCountIsEqualToTotalAvailableMappedUserPlanTest(String planPayload,
																					 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																					 String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		SuperDbHelper.updateUsageCountOfPlan(planId, usageCount);
		SuperDbHelper.updateUsageCountOfPlan(planIdTwo, usageCount);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[" + planId + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenNoBenefitIsMappedWithMappedUserPlanData", description = "check mapped user plan-user when no benefit is mapped with the mapped user plan", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenNoBenefitIsMappedWithMappedUserPlaTest(String planPayload,
																		String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																		String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		// boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo,
		// benefitIdTwo);
		// Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userIdTwo);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		// boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userId);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenNoBenefitIsMappedWithPublicUserPlanData", description = "check mapped user plan-user when no benefit is mapped wit the public plan", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenNoBenefitIsMappedWithPublicUserPlanTest(String planPayload,
																		 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																		 String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		// boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo,
		// benefitIdTwo);
		// Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		// boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		// Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");
		String planIdInPlanUserRespUserThree = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUserThree, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenMappedPlanIsMappedTwiceWithUserData", description = "check mapped user plan-user when plan is mapped twice with user", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenMappedPlanIsMappedTwiceWithUserTest(String planPayload, String benefitFreebiePayload,
																	 String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, userId);
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				userIdTwo);
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenPublicPlanIsMappedTwiceWithUserData", description = "check mapped user plan-user when plan is mapped twice with user", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenPublicPlanIsMappedTwiceWithUserTest(String planPayload, String benefitFreebiePayload,
																	 String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveMapStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, null, "USER");
		Assert.assertEquals(planUserIncentiveMapStatus, true);
		// mapping
		boolean mappingtatus = superHelper.mapPlanAndBenefit(planId, benefitId);
		Assert.assertEquals(mappingtatus, true);
		boolean planUserIncentiveMapFourStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapFourStatus, true);
		boolean planUserIncentiveMapThreeStatus = superHelper.mapPlanIncentiveAndUserMapping(planId, incentiveId,
				"USER");
		Assert.assertEquals(planUserIncentiveMapThreeStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String planIdInPlanUserRespTwo = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespTwo, "[" + planId + "]");

		// get plan by user id with mapped userId
		Processor planUserRespForMappeduser = superHelper.getPlansByUserId(userIdTwo, "true");
		int statusCodeForMappedUser = planUserRespForMappeduser.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeForMappedUser, 1);
		String planIdInPlanUserRespForMappedUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planId + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespForMappedUser, "[" + planId + "]");
		String planIdInPlanUserRespUser = planUserRespForMappeduser.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserRespUser, "[" + planIdTwo + "]");

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserWhenFreeDelBenefitIsMappedTwiceWithPublicPlanData", description = "check mapped user plan-user when plan is mapped twice with user", dataProviderClass = SuperDP.class)
	public void getPlanByUserWhenFreeDelBenefitIsMappedTwiceWithPublicPlanTest(String planPayload,
																			   String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																			   String benefitFreeDelPayload, String usageCount) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdFreeDel = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];
		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdFreeDel);
		Assert.assertEquals(mappingTwoStatus, true);

		// create plan benefit mapping
		String planBenefitMapPayload = superHelper.populatePlanBenefitMappingPayload(planIdTwo, benefitIdFreeDel)
				.toString();
		Processor planBenefitMapResponse = superHelper.createPlanBenefitMapping(planBenefitMapPayload);
		int statusCodeOfPlanBenefitMapping = planBenefitMapResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");

		String dataPlanBenefitMappingResp = planBenefitMapResponse.ResponseValidator.GetNodeValue("$.data");
		String dataPlanBenefitMappingstatusMessage = planBenefitMapResponse.ResponseValidator
				.GetNodeValue("$.statusMessage");
		Assert.assertEquals(dataPlanBenefitMappingResp, null);
		Assert.assertEquals(
				dataPlanBenefitMappingstatusMessage.contains("Plan id: {} is already mapped with benefit id:"), true);
		Assert.assertEquals(statusCodeOfPlanBenefitMapping, 0);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdFreeDel);
	}

	@Test(dataProvider = "getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithPlanData", description = "check mapped user plan-user when plan is mapped twice with user", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithPlanTest(String planPayload,
																			  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																			  String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		String secondBenefit = benefitsList.get(1).toString();
		String thirdBenefit = benefitsList.get(2).toString();
		Assert.assertEquals(firstBenefit, incentiveIdTwo);
		Assert.assertEquals(secondBenefit, benefitIdTwo);
		Assert.assertEquals(thirdBenefit, benefitId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithMappedUserPlanData", description = "check mapped user plan-user when plan is mapped twice with user", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckPriorityOrderOfBenefitsAttachedWithMappedUserPlanTest(String planPayload,
																						String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																						String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		String secondBenefit = benefitsList.get(1).toString();
		String thirdBenefit = benefitsList.get(2).toString();
		Assert.assertEquals(firstBenefit, incentiveIdTwo);
		Assert.assertEquals(secondBenefit, benefitIdTwo);
		Assert.assertEquals(thirdBenefit, benefitId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityMappedUserPlanData", description = "check mapped user plan-user when two diff. type of benefits of same priority are mapped with mapped-user-incentive", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityMappedUserPlanTest(
			String planPayload, String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
			String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		String secondBenefit = benefitsList.get(1).toString();
		String thirdBenefit = benefitsList.get(2).toString();
		Assert.assertEquals(firstBenefit, incentiveIdTwo);
		Assert.assertEquals(secondBenefit, benefitIdTwo);
		Assert.assertEquals(thirdBenefit, benefitId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityPublicPlanData", description = "check mapped user plan-user when two diff. type of benefits of same priority are mapped with mapped-user-incentive", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckPriorityOrderOfBenefitsOfDiffTypeWithSamePriorityPublicPlanTest(String planPayload,
																								  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																								  String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		String secondBenefit = benefitsList.get(1).toString();
		String thirdBenefit = benefitsList.get(2).toString();
		Assert.assertEquals(firstBenefit, incentiveIdTwo);
		Assert.assertEquals(secondBenefit, benefitIdTwo);
		Assert.assertEquals(thirdBenefit, benefitId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenDisableIncentivePublicPlanData", description = "check mapped user plan-user when after mapping plan-user-incentive, disable the incentive ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenDisableIncentivePublicPlanTest(String planPayload, String benefitFreebiePayload,
																	 String incentiveFlatPayload, String planTwoPayload, String benefitFreeDelPayload,
																	 HashMap<String, String> patchIncentive) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// disable incentive
		patchIncentive.put("0", incentiveIdTwo);
		String patchIncentivePayload = superHelper.populatePatchIncentiveDisableEnabledPayload(patchIncentive);
		superHelper.patchIncentive(patchIncentivePayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertNotEquals(firstBenefit, incentiveIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenDisableIncentiveMappedUserPlanData", description = "check mapped user plan-user when after mapping public plan with incentive and user, disable the incentive ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenDisableIncentiveMappedUserPlanTest(String planPayload,
																		 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																		 String benefitFreeDelPayload, HashMap<String, String> patchIncentive) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		// disable incentive
		patchIncentive.put("0", incentiveIdTwo);
		String patchIncentivePayload = superHelper.populatePatchIncentiveDisableEnabledPayload(patchIncentive);
		superHelper.patchIncentive(patchIncentivePayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertNotEquals(firstBenefit, incentiveIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfTenureZeroMappedUserPlanData", description = "check mapped user plan-user when incentive of tenure zero ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfTenureZeroMappedUserPlanTest(String planPayload,
																			  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																			  String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + true + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[\"FLAT\"]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfTenureZeroPublicUserPlanData", description = "check public user plan-user when public plan with incentive zero ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfTenureZeroPublicUserPlanTest(String planPayload,
																			  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																			  String benefitFreeDelPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + true + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[\"FLAT\"]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenDisableIncentiveOfTenureZeroPublicUserPlanData", description = "check public user plan-user when public plan with disable incentive zero ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenDisableIncentiveOfTenureZeroPublicUserPlanTest(String planPayload,
																					 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																					 String benefitFreeDelPayload, HashMap<String, String> patchIncentive) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// disable incentive
		patchIncentive.put("0", incentiveIdTwo);
		String patchIncentivePayload = superHelper.populatePatchIncentiveDisableEnabledPayload(patchIncentive);
		superHelper.patchIncentive(patchIncentivePayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + false + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenDisableIncentiveOfTenureZeroMappedUserPlanData", description = "check public user plan-user when public plan with disable incentive zero ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenDisableIncentiveOfTenureZeroMappedUserPlanTest(String planPayload,
																					 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																					 String benefitFreeDelPayload, HashMap<String, String> patchIncentive) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveFlatPayload);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);

		// disable incentive
		patchIncentive.put("0", incentiveIdTwo);
		String patchIncentivePayload = superHelper.populatePatchIncentiveDisableEnabledPayload(patchIncentive);
		superHelper.patchIncentive(patchIncentivePayload);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + false + "]");
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOneMappedUserPlanData", description = "check plan by user ID when -1 and 0 tenure incentives are mapped with the mapped user plan ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOneMappedUserPlanTest(String planPayload,
																						 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																						 String benefitFreeDelPayload, HashMap<String, String> patchIncentive, String incentiveTenureMinusOne) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveTenureMinusOne);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				userId);
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveTenureMinueOneStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,
				incentiveId, userId);
		Assert.assertEquals(planUserIncentiveTenureMinueOneStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + true + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[\"FLAT\"]");
		String freeDelBenefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitIdTwo + ")].benefit_id");
		Assert.assertEquals(freeDelBenefitIdInfoResp, "[" + benefitIdTwo + "]");
		String benefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitId + ")].benefit_id");
		Assert.assertEquals(benefitIdInfoResp, benefitIdInfoResp);
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, incentiveId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOnePublicUserPlanData", description = "check plan by user ID when -1 and 0 tenure incentives are mapped with the public user plan ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfTenureZeroAndMinusOnePublicUserPlanTest(String planPayload,
																						 String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																						 String benefitFreeDelPayload, HashMap<String, String> patchIncentive, String incentiveTenureMinusOne) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveTenureMinusOne);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveTenureMinueOneStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,
				incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveTenureMinueOneStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + true + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[\"FLAT\"]");
		String freeDelBenefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitIdTwo + ")].benefit_id");
		Assert.assertEquals(freeDelBenefitIdInfoResp, "[" + benefitIdTwo + "]");
		String benefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitId + ")].benefit_id");
		Assert.assertEquals(benefitIdInfoResp, benefitIdInfoResp);
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, incentiveId);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfSameTenureZeroPublicUserPlanData", description = "check plan by user ID when 2 same type of incentives- 0 tenure are mapped with the public user plan ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfSameTenureZeroPublicUserPlanTest(String planPayload,
																				  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																				  String benefitFreeDelPayload, HashMap<String, String> patchIncentive, String incentiveTenureMinusOne) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveTenureMinusOne);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveTenureMinueOneStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,
				incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveTenureMinueOneStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + true + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[\"FLAT\"]");
		String freeDelBenefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitIdTwo + ")].benefit_id");
		Assert.assertEquals(freeDelBenefitIdInfoResp, "[" + benefitIdTwo + "]");
		String benefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitId + ")].benefit_id");
		Assert.assertEquals(benefitIdInfoResp, benefitIdInfoResp);
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "getPlanByUserCheckWhenIncentiveOfSameTenureMinusOnePublicUserPlanData", description = "check plan by user ID when 2 same type of incentives- -1 tenure are mapped with the public user plan ", dataProviderClass = SuperDP.class)
	public void getPlanByUserCheckWhenIncentiveOfSameTenureMinusOnePublicUserPlanTest(String planPayload,
																					  String benefitFreebiePayload, String incentiveFlatPayload, String planTwoPayload,
																					  String benefitFreeDelPayload, HashMap<String, String> patchIncentive, String incentiveTenureMinusOne) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();

		String[] planBenefitIncentiveIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planTwoPayload,
				benefitFreeDelPayload, incentiveFlatPayload);
		String planIdTwo = planBenefitIncentiveIds[0];
		String benefitIdTwo = planBenefitIncentiveIds[1];
		String incentiveIdTwo = planBenefitIncentiveIds[2];

		String[] allIds = superHelper.subscriptionPlanBenefitIncentiveIDs(planPayload, benefitFreebiePayload,
				incentiveTenureMinusOne);
		String planId = allIds[0];
		String benefitId = allIds[1];
		String incentiveId = allIds[2];

		// user and order id
		String userId = Integer.toString(Utility.getRandom(1, 100000));
		String userIdTwo = Integer.toString(Utility.getRandom(1, 100000));

		// mapping
		boolean mappingStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitIdTwo);
		Assert.assertEquals(mappingStatus, true);
		boolean mappingTwoStatus = superHelper.mapPlanAndBenefit(planIdTwo, benefitId);
		Assert.assertEquals(mappingTwoStatus, true);
		boolean planUserIncentiveMapTwoStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo, incentiveIdTwo,
				"USER");
		Assert.assertEquals(planUserIncentiveMapTwoStatus, true);
		boolean planUserIncentiveTenureMinueOneStatus = superHelper.mapPlanIncentiveAndUserMapping(planIdTwo,
				incentiveId, "USER");
		Assert.assertEquals(planUserIncentiveTenureMinueOneStatus, true);

		// get plan by user id with mapped user id
		Processor planUserResp = superHelper.getPlansByUserId(userId, "true");
		int statusCode = planUserResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String planIdInPlanUserResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list[?(@.plan_id==" + planIdTwo + ")].plan_id");
		Assert.assertEquals(planIdInPlanUserResp, "[" + planIdTwo + "]");
		String incentiveTenureZeroPlanUserResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_applied");
		Assert.assertEquals(incentiveTenureZeroPlanUserResp, "[" + false + "]");
		String incentiveTenureZeroDiscInfoResp = planUserResp.ResponseValidator.GetNodeValueAsStringFromJsonArray(
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].discount_info.type");
		Assert.assertEquals(incentiveTenureZeroDiscInfoResp, "[]");
		String freeDelBenefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitIdTwo + ")].benefit_id");
		Assert.assertEquals(freeDelBenefitIdInfoResp, "[" + benefitIdTwo + "]");
		String benefitIdInfoResp = planUserResp.ResponseValidator
				.GetNodeValueAsStringFromJsonArray("$.data.plan_list.[?(@.plan_id==" + planIdTwo
						+ ")].benefits[?(@.benefit_id==" + benefitId + ")].benefit_id");
		Assert.assertEquals(benefitIdInfoResp, benefitIdInfoResp);
		List benefitsList = JsonPath.read(planUserResp.ResponseValidator.GetBodyAsText(),
				"$.data.plan_list.[?(@.plan_id==" + planIdTwo + ")].benefits..benefit_id");
		String firstBenefit = benefitsList.get(0).toString();
		Assert.assertEquals(firstBenefit, incentiveId);
		String SecondBenefit = benefitsList.get(1).toString();
		Assert.assertEquals(SecondBenefit, benefitIdTwo);

		SuperDbHelper.deleteBenefitFromDB(benefitId);
		SuperDbHelper.deleteBenefitFromDB(benefitIdTwo);
	}

	@Test(dataProvider = "createPlanWhereTitleIsEmptyData", description = "createPlan where title is empty", dataProviderClass = SuperDP.class)
	public void createPlanWhereTitleIsEmptyTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		// String data =
		// planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidTitle);
		Assert.assertEquals(statusCode, 0);
		// Assert.assertEquals(data, null);
	}

	@Test(dataProvider = "createPlanWhereDescriptionIsEmptyData", description = "createPlan where description is empty ", dataProviderClass = SuperDP.class)
	public void createPlanWhereDescriptionIsEmpty(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidDescription);
		Assert.assertEquals(statusCode, 0);
	}

	@Test(dataProvider = "createPlanWhereNameIsEmptyData", description = "createPlan where name is empty ", dataProviderClass = SuperDP.class)
	public void createPlanWhereNameIsEmpty(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidName);
		Assert.assertEquals(statusCode, 0);
	}

	@Test(dataProvider = "createPlanWhereDuplicatePlanNameData", description = "createPlan where plan name is duplicate ", dataProviderClass = SuperDP.class)
	public void createPlanWhereDuplicatePlanNameTest(String planPayload, String planTwoPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		Processor planTwoResponse = superHelper.createPlan(planTwoPayload);
		int statusCodeOfPlanTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 1);
		int planIdTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planIdTwo, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
		SuperDbHelper.deletePlan(Integer.toString(planIdTwo));
	}

	@Test(dataProvider = "createPlanWhereValidFromIsGreaterThanValidTillData", description = "createPlan where plan valid from is greater than valid till ", dataProviderClass = SuperDP.class)
	public void createPlanWhereValidFromIsGreaterThanValidTillTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCode = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.status218);
		//int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		//Assert.assertEquals(planId, null);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidPlanValidity);

		//SuperDbHelper.deletePlan(Integer.toString(planId));

	}

	@Test(dataProvider = "createPlanWhereTenureOtherThanThreeTwoAndOneValueData", description = "createPlan where plan tenure is other than three, two or one value ", dataProviderClass = SuperDP.class)
	public void createPlanWhereTenureOtherThanThreeTwoAndOneValueTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.tenureInvalid);

	}

	@Test(dataProvider = "createPlanWhereTenureIsZeroData", description = "createPlan where plan tenure is zero value ", dataProviderClass = SuperDP.class)
	public void createPlanWhereTenureIsZeroTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.tenureInvalid);
	}

	@Test(dataProvider = "createPlanWhereTenureIsMinusOneData", description = "createPlan where plan tenure is minus one value ", dataProviderClass = SuperDP.class)
	public void createPlanWhereTenureIsMinusOneTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.tenureInvalid);
	}

	@Test(dataProvider = "createPlanWhereTenureIsGreaterThanPlanPeriodData", description = "createPlan where plan tenure is greater than plan validTill and plan validFrom ", dataProviderClass = SuperDP.class)
	public void createPlanWhereTenureIsGreaterThanPlanPeriodTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.tenureInvalid);
	}

	@Test(dataProvider = "createPlanWhereTenureIsEqualToPlanPeriodData", description = "createPlan where plan tenure is equal to plan validTill and plan validFrom ", dataProviderClass = SuperDP.class)
	public void createPlanWhereTenureIsEqualToPlanPeriodTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.tenureInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanPriceIsZeroData", description = "createPlan where plan price is zero ", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanPriceIsZeroTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.priceInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanPriceIsNegativeValueData", description = "createPlan where plan price is negative value ", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanPriceIsNegativeValueTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.priceInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanEnabledIsFalseData", description = "createPlan where plan enabled is set to false ", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanEnabledIsFalseTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
	}

	@Test(dataProvider = "createPlanWherePlanOffsetDaysGreaterThanTwentySevenData", description = "createPlan where plan offset days is greater than 27", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanOffsetDaysGreaterThanTwentySevenTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlanTwo = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, SuperConstants.statusCode216);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidRenewalOffset);
	}

	@Test(dataProvider = "createPlanWherePlanCreatedByIsEmptyStringData", description = "createPlan where created by is empty ", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanCreatedByIsEmptyStringTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.createdByInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanTotalAvailableIsZeroData", description = "createPlan where plan total available count is zero", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanTotalAvailableIsZeroTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.totalAvailableInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanTotalAvailableIsNullData", description = "createPlan where plan total available count is null", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanTotalAvailableIsNull(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.totalAvailableInvalid);
	}

	@Test(dataProvider = "createPlanWherePlanTncIsEmptyData", description = "createPlan where plan tnc is empty", dataProviderClass = SuperDP.class)
	public void createPlanWherePlanTncIsEmptyTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
	}

	@Test(dataProvider = "createMultiplePlanWithInSameTimeTenurePriceData", description = "create multiple plans with in same time slot, price and tenure", dataProviderClass = SuperDP.class)
	public void createMultiplePlanWithInSameTimeTenurePriceTest(String planPayload, String planPayloadTwo) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		Processor planTwoResponse = superHelper.createPlan(planPayloadTwo);
		int statusCodeOfPlanTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlanTwo, 1);
		int planIdTwo = planTwoResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planIdTwo, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
		SuperDbHelper.deletePlan(Integer.toString(planIdTwo));
	}

	@Test(dataProvider = "createPlanWhereTitleIsBlankData", description = "createPlan where plan title is blank", dataProviderClass = SuperDP.class)
	public void createPlanWhereTitleIsBlankTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidTitle);
	}

	@Test(dataProvider = "createPlanWhereDescriptionIsBlankData", description = "createPlan where plan description is blank", dataProviderClass = SuperDP.class)
	public void createPlanWhereDescriptionIsBlankTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidDescription);
	}

	@Test(dataProvider = "createPlanWhereNameIsBlankData", description = "createPlan where plan name is blank", dataProviderClass = SuperDP.class)
	public void createPlanWhereNameIsBlankTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidName);
	}


	@Test(dataProvider = "createPlanWhereCreatedByIsBlankData", description = "createPlan where plan name is blank", dataProviderClass = SuperDP.class)
	public void createPlanWhereCreatedByIsBlankTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.createdByInvalid);
	}

	@Test(dataProvider = "createPlanWhereIsRecommendedIsTrueData", description = "createPlan where isRecommanded is true", dataProviderClass = SuperDP.class)
	public void createPlanWhereIsRecommendedIsTrueTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
	}

	@Test(dataProvider = "createPlanWhereIsRecommendedIsFalseData", description = "createPlan where isRecommanded is false", dataProviderClass = SuperDP.class)
	public void createPlanWhereIsRecommendedIsFalseTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 1);
		int planId = planResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(planId, null);

		SuperDbHelper.deletePlan(Integer.toString(planId));
	}


	@Test(dataProvider = "createPlanWhereExpiryOffsetIsNullData", description = "createPlan where isRecommanded is false", dataProviderClass = SuperDP.class)
	public void createPlanWhereExpiryOffsetIsNullTest(String planPayload) {
		Processor planResponse = superHelper.createPlan(planPayload);
		int statusCodeOfPlan = planResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCodeOfPlan, 0);
		String statusMessage = planResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.expiryOffsetInvalid);
	}


	@Test(dataProvider = "patchPlanWithNonExistingPlanIdData", description = "patchPlan with the no exisiting plan id", dataProviderClass = SuperDP.class)
	public void patchPlanWithNonExistingPlanIdTest(String patchPayload) {

		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);

		String data = patchPlanResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(data, null);
		String statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.statusCode204);
		String statusMessage = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.planNotExist);
	}

	@Test(dataProvider = "patchPlanWithCountAsZeroData", description = "patchPlan with count as zero", dataProviderClass = SuperDP.class)
	public void patchPlanWithCountAsZeroTest(String patchPayload) {
		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);
		String planId = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(planId, null);
		int statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String totalCountInDB = SuperDbHelper.getTotalAvailableOfPlan(planId);
		Assert.assertEquals(totalCountInDB, "2000");
		SuperDbHelper.deletePlan(planId);
	}

	@Test(dataProvider = "patchPlanDisablePlanData", description = "patchPlan, disable plan", dataProviderClass = SuperDP.class)
	public void patchPlanDisablePlanTest(String patchPayload) {
		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);
		String planId = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(planId, null);
		int statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		SuperDbHelper.deletePlan(planId);
	}

	@Test(dataProvider = "patchPlanEnablePlanData", description = "patchPlan, enable plan", dataProviderClass = SuperDP.class)
	public void patchPlanEnablePlanTest(String patchPayload) {
		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);
		String planId = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(planId, null);
		int statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		SuperDbHelper.deletePlan(planId);
	}

	@Test(dataProvider = "patchPlanUpdateTotalAvailableCountData", description = "patchPlan, enable plan", dataProviderClass = SuperDP.class)
	public void patchPlanUpdateTotalAvailableCountTest(String patchPayload) {
		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);
		String planId = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
		Assert.assertNotEquals(planId, null);
		int statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String countInDb = SuperDbHelper.getTotalAvailableOfPlan(planId);
		Assert.assertEquals(countInDb, "2100");
		SuperDbHelper.deletePlan(planId);
	}

	@Test(dataProvider = "patchPlanValidTillSmallerThanValidTillData", description = "patchPlan, enable plan", dataProviderClass = SuperDP.class)
	public void patchPlanValidTillSmallerThanValidTillTest(String patchPayload) {
		Processor patchPlanResponse = superHelper.patchPlan(patchPayload);
		int statusCode = patchPlanResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, SuperConstants.status218);
		String statusMessage = patchPlanResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidPlanValidity);
	}

	@Test(dataProvider = "createBenefitCheckTitleData", description = "create benfit, check title empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCheckTitleTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 0);
		String statusMessage = benefitResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidTitle);
	}

	@Test(dataProvider = "createBenefitCheckDescriptionData", description = "create benfit, check description empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCheckDescriptionTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 0);
		String statusMessage = benefitResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidDescription);
	}

	@Test(dataProvider = "createBenefitCheckNameData", description = "create benfit, check name empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCheckNameTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 0);
		String statusMessage = benefitResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.invalidName);
	}

	@Test(dataProvider = "createBenefitCheckLogoIdData", description = "create benfit, check logo id empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCheckLogoIdTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 0);
		String statusMessage = benefitResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.logoIdInvalid);
	}

	@Test(dataProvider = "createBenefitCreatedByData", description = "create benfit, check created by empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCreatedByTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 0);
		String statusMessage = benefitResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.statusMessage");
		Assert.assertEquals(statusMessage, SuperConstants.createdByInvalid);
	}

	@Test(dataProvider = "createBenefitCheckPriorityData", description = "create benfit, check priority empty string, null or blank space", dataProviderClass = SuperDP.class)
	public void createBenefitCheckPriorityTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(benefitId, null);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "createBenefitCheckBenefitTypeData", description = "create benfit, check benefit types:- freeDel, freebie or cc priority", dataProviderClass = SuperDP.class)
	public void createBenefitCheckBenefitTypeTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(benefitId, null);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "createSameBenefitData", description = "create same benfits, check benefit types:- freeDel, freebie or cc priority", dataProviderClass = SuperDP.class)
	public void createSameBenefitTest(String benefitPayload) {
		SuperDbHelper.deleteFreeDelBenefit();
		SuperDbHelper.deleteFreebieBenefit();
		Processor benefitResponse = superHelper.createBenefit(benefitPayload);
		int benefitStatusCode = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitStatusCode, 1);
		int benefitId = benefitResponse.ResponseValidator.GetNodeValueAsInt("$.data");
		Assert.assertNotEquals(benefitId, null);

		Processor benefitTwoResponse = superHelper.createBenefit(benefitPayload);
		int benefitTwoStatusCode = benefitTwoResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(benefitTwoStatusCode, 0);
		String benefitTwoId = benefitTwoResponse.ResponseValidator.GetNodeValue("$.data");
		Assert.assertEquals(benefitTwoId, null);
		SuperDbHelper.deleteBenefitFromDB(Integer.toString(benefitId));
	}

	@Test(dataProvider = "createIncentiveCheckTitleDescriptionNameData", description = "create incentive, check title, description and name", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckTitleDescriptionNameTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertEquals(incentiveId, null);
	}


	@Test(dataProvider = "createIncentiveCheckMetaTypeFlatData", description = "create incentive, check meta-type", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckMetaTypeFlatTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		int incentiveId = incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data");
		Assert.assertNotEquals(incentiveId, null);
	}

	@Test(dataProvider = "createIncentiveCheckMetaValueData", description = "create incentive, check meta-value", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckMetaValueTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertEquals(incentiveId, null);
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveTenureData", description = "create incentive, check incentive tenure 0 and -1", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveTenureTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String incentiveId = Integer.toString(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveTenureOtherThanZeroMinusOneData", description = "create incentive, check incentive tenure other than 0 and -1 ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveTenureOtherThanZeroMinusOneTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertEquals(incentiveId, null);
	}


	@Test(dataProvider = "createIncentiveCheckIncentivePriorityData", description = "create incentive, check incentive priority 100, 0 and -1 ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentivePriorityTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String incentiveId = Integer.toString(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveEnabledData", description = "create incentive, check incentive enabled true or false ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveEnabledTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String incentiveId = Integer.toString(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveCreatedByData", description = "create incentive, check incentive created by is empty or blank space  ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveCreatedByTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckTitleDescriptionNameAsBlankData", description = "create incentive, check incentive title, description and name as blank space  ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckTitleDescriptionNameAsBlankTest(String incentivePayload) {

		// create incentive(tenure zero only)
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckIncentivePercentageGreaterThanHunderedData", description = "create incentive, check incentive type percentage value greater than 100 ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentivePercentageGreaterThanHunderedTest(String incentivePayload) {

		// create incentive
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertNotEquals(incentiveId, "null");
	}

	@Test(dataProvider = "createIncentiveCheckIncentivePercentageCappingData", description = "create incentive, check incentive-percentage cap as -1 or zero ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentivePercentageCappingTest(String incentivePayload) {

		// create incentive
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String incentiveId = Integer.toString(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);
	}

	@Test(dataProvider = "createIncentiveCheckIncentivePercentageCappingAsNullData", description = "create incentive, check incentive-percentage cap as null ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentivePercentageCappingAsNullTet(String incentivePayload) {

		// create incentive
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertEquals(incentiveId, null);
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveFlatCappingAsNullData", description = "create incentive, check incentive-flat cap as null ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveFlatCappingAsNullTest(String incentivePayload) {

		// create incentive
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 0);
		String incentiveId = incectiveResp.ResponseValidator.GetNodeValue(" $.data");
		Assert.assertEquals(incentiveId, null);
	}

	@Test(dataProvider = "createIncentiveCheckIncentiveFlatCappingData", description = "create incentive, check incentive-flat cap as -1 or zero ", dataProviderClass = SuperDP.class)
	public void createIncentiveCheckIncentiveFlatCappingTest(String incentivePayload) {

		// create incentive
		Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		int statusCode = incectiveResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		Assert.assertEquals(statusCode, 1);
		String incentiveId = Integer.toString(incectiveResp.ResponseValidator.GetNodeValueAsInt(" $.data"));
		Assert.assertNotEquals(incentiveId, null);
	}

//	@Test
//	public void testSuper(String incentivePayload) {
//superHelper.createSuperUserWithPublicPlan("000030");
//}

	@Test(dataProvider = "", description = "create incentive, check incentive-flat cap as -1 or zero ", dataProviderClass = SuperDP.class)
	public void createTest() {

		// create incentive
		//Processor incectiveResp = superHelper.createIncentive(incentivePayload);
		superHelper.createSuperUserWithPublicPlan("3001");
	}
}