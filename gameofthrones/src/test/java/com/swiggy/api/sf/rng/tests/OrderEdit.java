package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.delivery.constants.EndToEndConstants;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.AddressPOJO;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.helper.EDVOHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinQuantity;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOCartMealItemRequest;
import com.swiggy.api.sf.snd.helper.HeaderHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class OrderEdit {
	SANDHelper sandHelper = new SANDHelper();
	RngHelper rngHelper = new RngHelper();
	Initialize gameofthrones = new Initialize();
	AddressHelper addressHelper = new AddressHelper();
//	OrderPlace orderPlaceHelper = new OrderPlace();
	JsonHelper jsonHelper = new JsonHelper();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	HeaderHelper headerHelper = new HeaderHelper();
	EDVOHelper edvoHelper = new EDVOHelper();

	public HashMap<String, String> signupAndLoginUser(HashMap<String, Object> userData) {
		HashMap<String, String> signUpDetails = sandHelper.createOrderWithSignUp((String) userData.get("name"),
				(String) userData.get("mobile"), (String) userData.get("email"), (String) userData.get("password"));
//			 user login
		Processor loginResponse = sandHelper.login1((String) userData.get("mobile"), (String) userData.get("password"));
		return signUpDetails;
	}

	public Integer addNewAddress(HashMap<String, String> signUpDetails, AddressPOJO address) {

		Processor addressResponse = addressHelper.addNewAddress(signUpDetails.get("tid"), signUpDetails.get("token"),
				address);
		return JsonPath.read(addressResponse.ResponseValidator.GetBodyAsText(), "$.data.address_id");
	}

	public HashMap<String, String> getMenuItem(AddressPOJO address, Integer minAmt) {
		List<String> restList = sandHelper.aggregatorRestList(new String[] { address.getLat(), address.getLng() });
		HashMap<String, String> menuItemRest = rngHelper.getMenuItemWithoutVariantWithMinAmt(restList, minAmt);
		Assert.assertTrue(menuItemRest.get("menuItemID") != null,
				"No item found in the given location, whose price is >= min coupon");
		return menuItemRest;
	}

//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a new user, flat td for first order then place order, edit the same order and confirm it")
	public void orderEditE2EFlatTDWithFirstOrderConstraintTest(HashMap<String, Object> userData)
			throws NumberFormatException, IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		rest listing
		HashMap<String, String> menuItemRest = getMenuItem(address, Integer.parseInt((String) userData.get("minCart")));
//		 Create a flat td for that restro
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"),
				menuItemRest.get("restID")).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create cart
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());

		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
//		 assert if td is applied
		Assert.assertNotNull(JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders"),
				"No TD applied on the cart");
		Assert.assertTrue(
				JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders[0].campaignId").equals(campaignId),
				"Assertion failed, applied td is not same as expected td");
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.discount_total"),
				Double.valueOf((String) userData.get("flat")), "Total discount amount not equal");

//		place order
		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());
		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");
//		add assertion on order
//		assert if td is applied
//		assert if same td is getting applied
		Assert.assertTrue(
				JsonPath.read(order, "$.data.trade_discount")
						.equals(Double.parseDouble((String) userData.get("flat"))),
				"Assertion Failed, discount amount is not same");
//		 edit the order
		String[] editOrderPayload = new String[] { order_id, (String) userData.get("initSource"),
				menuItemRest.get("menuItemID"), (String) userData.get("editOrderQuantity"),
				menuItemRest.get("restID") };
		checkoutHelper.editOrderCheck(editOrderPayload);
		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(editOrderPayload).ResponseValidator
				.GetBodyAsText();
//		 assert before and after cart edit
		List<Integer> appliedCampaignID = JsonPath.read(editOrderConfirmResponse,
				"$.data.trade_discount_meta[*].campaignId");
		Assert.assertTrue(appliedCampaignID.contains(campaignId), "expected TD is not applied on the order");
		Assert.assertTrue(
				JsonPath.read(editOrderConfirmResponse, "$.data.trade_discount")
						.equals(Double.parseDouble((String) userData.get("flat"))),
				"Assertion Failed, discount amount is not same");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create flat TD first order user restricted, then call cart evaluate API to validate TD")
	public void orderEditFlatTDWithFirstOrderConstraintUserInitTest(HashMap<String, Object> userData) {
		int orderEditSource = 2;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.valueOf((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		System.out.println(cartEvaluateResponse);
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	public void orderEditFlatTDWithFirstOrderConstraintVendorInitTest(HashMap<String, Object> userData) {
		int orderEditSource = 1;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.valueOf((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		System.out.println(cartEvaluateResponse);
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	// @Test(dataProvider = "orderEditWithPercentageTD", dataProviderClass =
	// DP.class, description = "create a new user, % td for first order then place
	// order, edit the same order and confirm it")

	public void orderEditE2EPercentageWithFirstOrderConstraint(HashMap<String, Object> userData)
			throws NumberFormatException, IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		rest listing
		HashMap<String, String> menuItemRest = getMenuItem(address, Integer.parseInt((String) userData.get("minCart")));
		Double percentDiscountAmount = (Double.valueOf(menuItemRest.get("itemPrice"))
				* Double.valueOf((String) userData.get("percentage"))) / 100;
//		create TD
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), menuItemRest.get("restID")).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create cart
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());

		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
//		 assert if td is applied
		Assert.assertNotNull(JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders"),
				"No TD applied on the cart");
		Assert.assertTrue(
				JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders[0].campaignId").equals(campaignId),
				"Assertion failed, applied td is not same as expected td");
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.discount_total"), percentDiscountAmount,
				"Assertion failed on cart, discount not equal as expected");

//		place order
		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());
		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");
		Assert.assertTrue(JsonPath.read(order, "$.data.trade_discount").equals(percentDiscountAmount),
				"Assertion Failed, discount amount is not same");
//		add assertion on order
//		assert if td is applied
//		assert if same td is getting applied
		Double expectedEditPercentDiscountAmount = ((Double.valueOf(menuItemRest.get("itemPrice"))
				* Double.valueOf((String) userData.get("editOrderQuantity")))
				* Double.valueOf((String) userData.get("percentage"))) / 100;

//		 edit the order
		String[] editOrderPayload = new String[] { order_id, (String) userData.get("initSource"),
				menuItemRest.get("menuItemID"), (String) userData.get("editOrderQuantity"),
				menuItemRest.get("restID") };
		checkoutHelper.editOrderCheck(editOrderPayload);
		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(editOrderPayload).ResponseValidator
				.GetBodyAsText();
//		 assert before and after cart edit
		List<Integer> appliedCampaignID = JsonPath.read(editOrderConfirmResponse,
				"$.data.trade_discount_meta[*].campaignId");
		Assert.assertTrue(appliedCampaignID.contains(campaignId), "expected TD is not applied on the order");
		Assert.assertTrue(JsonPath.read(editOrderConfirmResponse, "$.data.trade_discount")
				.equals(expectedEditPercentDiscountAmount), "Assertion Failed, discount amount is not same");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithPercentageTD", dataProviderClass = DP.class, description = "create TD with discount type as % with first order restriction, then call cart evaluate API on order edit to validate TD")
	public void orderEditPercentageWithFirstOrderConstraintUserInit(HashMap<String, Object> userData) {
		int orderEditSource = 2;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true;
//		Create a percentage td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Double expectedEditPercentDiscountAmount = ((Double.valueOf((String) userData.get("minCart")))
				* Double.valueOf((String) userData.get("percentage"))) / 100;
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate((String) userData.get("minCart"), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				expectedEditPercentDiscountAmount,
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithPercentageTD", dataProviderClass = DP.class, description = "create TD with discount type as % with first order restriction, then call cart evaluate API on order edit to validate TD")
	public void orderEditPercentageWithFirstOrderConstraintVendorInit(HashMap<String, Object> userData) {
		int orderEditSource = 1;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true;
//		Create a percentage td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Double expectedEditPercentDiscountAmount = ((Double.valueOf((String) userData.get("minCart")))
				* Double.valueOf((String) userData.get("percentage"))) / 100;
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate((String) userData.get("minCart"), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				expectedEditPercentDiscountAmount,
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a new user, free delivery td for first order then place order, edit the same order and confirm it")
	public void orderEditE2EFreeDeliveryWithFirstOrderConstraint(HashMap<String, Object> userData)
			throws NumberFormatException, IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		rest listing
		HashMap<String, String> menuItemRest = getMenuItem(address, Integer.parseInt((String) userData.get("minCart")));
		String createTDResponse = rngHelper.createFreeDeliveryTD(menuItemRest.get("restID"), false, true, true, false);
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create cart
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());
		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
//		 assert if td is applied
		Assert.assertNotNull(JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders"),
				"No TD applied on the cart");
		Assert.assertTrue(
				JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders[0].campaignId").equals(campaignId),
				"Assertion failed, applied td is not same as expected td");
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.delivery_charges"), 0.0,
				"Assertion failed on cart, delivery fee not zero");

//		add free del assert

//		place order
		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());
		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");
		Assert.assertEquals(JsonPath.read(order, "$.data.order_delivery_charge"), 0.0,
				"Assertion failed on order, delivery fee not zero");
//		add free delivery assertion
		String[] editOrderPayload = new String[] { order_id, (String) userData.get("initSource"),
				menuItemRest.get("menuItemID"), (String) userData.get("editOrderQuantity"),
				menuItemRest.get("restID") };
		checkoutHelper.editOrderCheck(editOrderPayload);
		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(editOrderPayload).ResponseValidator
				.GetBodyAsText();
		List<Integer> appliedCampaignID = JsonPath.read(editOrderConfirmResponse,
				"$.data.trade_discount_meta[*].campaignId");
		Assert.assertTrue(appliedCampaignID.contains(campaignId), "expected TD is not applied on the order");
		Assert.assertEquals(JsonPath.read(editOrderConfirmResponse, "$.data.order_delivery_charge"), 0.0,
				"Assertion failed on order, delivery fee not zero");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create free del TD with first order user restriction, then call cart evaluate API to validate TD")
	public void orderEditFreeDeliveryWithFirstOrderConstraintUserInit(HashMap<String, Object> userData) {
		int orderEditSource = 2;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true, userRestrcited = false;
//		Create a flat td for that restro
		String createTDResponse = rngHelper.createFreeDeliveryTD(restID, false, firstOrder, restFirstOrder,
				userRestrcited);
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.valueOf((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
	}

	public void orderEditFreeDeliveryWithFirstOrderConstraintVendorInit(HashMap<String, Object> userData) {
		int orderEditSource = 1;
		String restID = "483";
		Boolean firstOrder = true, restFirstOrder = true, userRestrcited = false;
//		Create a flat td for that restro
		String createTDResponse = rngHelper.createFreeDeliveryTD(restID, false, firstOrder, restFirstOrder,
				userRestrcited);
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.valueOf((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
	}

//	@Test(dataProvider = "orderEditPercentageWithDiscountCapTD", dataProviderClass = DP.class, description = "create a new user, % td with discount cap for first order then place order, edit the same order and confirm it")
	public void orderEditE2EPercentageWithDiscountCapFirstOrder(HashMap<String, Object> userData)
			throws NumberFormatException, IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		rest listing
		HashMap<String, String> menuItemRest = getMenuItem(address, Integer.parseInt((String) userData.get("minCart")));
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), menuItemRest.get("restID")).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
			int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create cart
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());

		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
//		 assert if td is applied
		Assert.assertNotNull(JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders"),
				"No TD applied on the cart");
		Assert.assertTrue(
				JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders[0].campaignId").equals(campaignId),
				"Assertion failed, applied td is not same as expected td");
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.discount_total"),
				Double.valueOf((String) userData.get("discountCap")),
				"Assertion failed on cart, discount not equal as expected");
//		place order

		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());
		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");
		Assert.assertTrue(
				JsonPath.read(order, "$.data.trade_discount")
						.equals(Double.valueOf((String) userData.get("discountCap"))),
				"Assertion Failed, discount amount is not same");
//		add assertion on order
//		assert if td is applied
//		assert if same td is getting applied

//		 edit the order
		String[] editOrderPayload = new String[] { order_id, (String) userData.get("initSource"),
				menuItemRest.get("menuItemID"), (String) userData.get("editOrderQuantity"),
				menuItemRest.get("restID") };
		checkoutHelper.editOrderCheck(editOrderPayload);
		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(editOrderPayload).ResponseValidator
				.GetBodyAsText();
//		 assert before and after cart edit
		List<Integer> appliedCampaignID = JsonPath.read(editOrderConfirmResponse,
				"$.data.trade_discount_meta[*].campaignId");
		Assert.assertTrue(appliedCampaignID.contains(campaignId), "expected TD is not applied on the order");
		Assert.assertTrue(
				JsonPath.read(editOrderConfirmResponse, "$.data.trade_discount")
						.equals(Double.valueOf((String) userData.get("discountCap"))),
				"Assertion Failed, discount amount is not same");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditPercentageWithDiscountCapTD", dataProviderClass = DP.class, description = "create TD, then update TD's upper cap, then call cart evaluate API on order edit to validate TD")
	public void orderEditPercentageWithDiscountCapFirstOrderUserInit(HashMap<String, Object> userData) {
		int orderEditSource = 2;
		String restID = "583";
		Boolean firstOrder = true, restFirstOrder = true;
		// Create a percentage td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate((String) userData.get("minCart"), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("discountCap")),
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditPercentageWithDiscountCapTD", dataProviderClass = DP.class, description = "create TD, then update TD's upper cap, then call cart evaluate API on order edit to validate TD")
	public void orderEditPercentageWithDiscountCapFirstOrderVendorInit(HashMap<String, Object> userData) {
		int orderEditSource = 1;
		String restID = "583";
		Boolean firstOrder = true, restFirstOrder = true;
		// Create a percentage td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate((String) userData.get("minCart"), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("discountCap")),
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a new user, flat td for first order then place order, edit the same order and confirm it")
	public void orderEditE2EExpiredTDWhileEdit(HashMap<String, Object> userData)
			throws NumberFormatException, IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		rest listing
		HashMap<String, String> menuItemRest = getMenuItem(address, Integer.parseInt((String) userData.get("minCart")));
//		 Create a flat td for that restro
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"),
				menuItemRest.get("restID")).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create cart
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());

		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
//		 assert if td is applied
		Assert.assertNotNull(JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders"),
				"No TD applied on the cart");
		Assert.assertTrue(
				JsonPath.read(getCart1, "$.data.appliedTradeCampaignHeaders[0].campaignId").equals(campaignId),
				"Assertion failed, applied td is not same as expected td");
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.discount_total"),
				Double.valueOf((String) userData.get("flat")), "Total discount amount not equal");
//		place order
		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());
		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");
//		add assertion on order
//		assert if td is applied
//		assert if same td is getting applied
		Assert.assertTrue(
				JsonPath.read(order, "$.data.trade_discount")
						.equals(Double.parseDouble((String) userData.get("flat"))),
				"Assertion Failed, discount amount is not same");
//		edit td make it expired
		String editTDResponse = rngHelper.updateExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), menuItemRest.get("restID"),
				campaignId).ResponseValidator.GetBodyAsText();
		Assert.assertEquals(Integer.parseInt(JsonPath.read(editTDResponse, "$.data")), campaignId,
				"TD updation failed. Didn't received any id in the response");
//		edit order check
		String[] editOrderPayload = new String[] { order_id, (String) userData.get("initSource"),
				menuItemRest.get("menuItemID"), (String) userData.get("editOrderQuantity"),
				menuItemRest.get("restID") };
		checkoutHelper.editOrderCheck(editOrderPayload);
		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(editOrderPayload).ResponseValidator
				.GetBodyAsText();
//		 assert before and after cart edit
		List<Integer> appliedCampaignID = JsonPath.read(editOrderConfirmResponse,
				"$.data.trade_discount_meta[*].campaignId");
		Assert.assertTrue(appliedCampaignID.contains(campaignId), "expected TD is not applied on the order");
		Assert.assertTrue(
				JsonPath.read(editOrderConfirmResponse, "$.data.trade_discount")
						.equals(Double.parseDouble((String) userData.get("flat"))),
				"Assertion Failed, discount amount is not same");

	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create TD with time slot restriction, then call cart evaluate API to validate TD")
	public void orderEditAfterTimeSlotUserInit(HashMap<String, Object> userData) {
		int orderEditSource = 2;
		String restID = "683";
		Boolean firstOrder = false, restFirstOrder = false;
		// Create a flat td for that restro
		String createTDResponse = rngHelper.createExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	public void orderEditAfterTimeSlotVendorInit(HashMap<String, Object> userData) {
		int orderEditSource = 1;
		String restID = "783";
		Boolean firstOrder = true, restFirstOrder = true;
		// Create a flat td for that restro
		String createTDResponse = rngHelper.createExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create TD with min cart amount, edit order below min cart amount and validate it")
	public void orderEditUpdateCartBelowMinCartUserInit(HashMap<String, Object> userData) {
		int minCartAndItemPriceDiff = 20, orderEditSource = 2;
		String restID = "783";
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart")) - minCartAndItemPriceDiff), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
				"Assertion failed, TD shouldn't get applied");
	}

	public void orderEditUpdateCartBelowMinCartVendorInit(HashMap<String, Object> userData) {
		int minCartAndItemPriceDiff = 20, orderEditSource = 1;
		String restID = "783";
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart")) - minCartAndItemPriceDiff), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD shouldn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

//14737
//	Not applicable
	public void applyTDonOrderEdit() {

	}

	@Test(groups = {"TD"}, dataProvider = "orderEditPercentageWithDiscountCapTD", dataProviderClass = DP.class, description = "create TD, then update TD's upper cap, then call cart evaluate API on order edit to validate TD")
	public void updateTDUpperCapThenOrderEditUserInit(HashMap<String, Object> userData) {
		String restID = "783";
		int orderEditSource = 2;
		Boolean firstOrder = true, restFirstOrder = true;
		Double updatedCap = Double.valueOf((String) userData.get("discountCap")) + 100;
		Double updatedCartAmount = (updatedCap * 2) + 20;
//		 Create a flat td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String editTDResponse = rngHelper.updateTDFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"), String.valueOf(updatedCap),
				restID, campaignId).ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(editTDResponse, "$.data"), campaignId,
				"TD updation failed. Didn't received any id in the response");
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(String.valueOf(updatedCartAmount), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"), updatedCap,
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditPercentageWithDiscountCapTD", dataProviderClass = DP.class, description = "create TD, then update TD's upper cap, then call cart evaluate API on order edit to validate TD")
	public void updateTDUpperCapThenOrderEditVendorInit(HashMap<String, Object> userData) {
		String restID = "782";
		int orderEditSource = 1;
		Boolean firstOrder = true, restFirstOrder = true;
		Double updatedCap = Double.valueOf((String) userData.get("discountCap")) + 100;
		Double updatedCartAmount = (updatedCap * 2) + 20;
//		 Create a flat td for that restro
		String createTDResponse = rngHelper.createPercentageWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"),
				(String) userData.get("discountCap"), restID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");

		String editTDResponse = rngHelper.updateTDFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("percentage"), (String) userData.get("minCart"), String.valueOf(updatedCap),
				restID, campaignId).ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(editTDResponse, "$.data"), campaignId,
				"TD updation failed. Didn't received any id in the response");
		String cartEvaluateResponse = rngHelper.orderEditCartEvaluate(String.valueOf(updatedCartAmount), restID,
				firstOrder, restFirstOrder, orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");

		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"), updatedCap,
				"Assertion Failed, trade discount on cart is not equal to discount cap");
	}

//	public type dormant campaign 
	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create 30 days dormant td, edit the order and validated it")
	public void thirtyDaysDormantUserTypeUserInitTD(HashMap<String, Object> userData) throws IOException {
		String userID = "999", dormantDays = "30", restID = "883";
		int orderEditSource = 2;
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID, false).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
//		since, test is deactivating all the active td's before creating campaign, that's why index is hard code to 0
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create 30 days dormant td, edit the order and validated it")
	public void thirtyDaysDormantUserTypeVendorInitTD(HashMap<String, Object> userData) {
		String userID = "999", dormantDays = "30", restID = "883";
		int orderEditSource = 1;
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID, false).ResponseValidator
						.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
//		since, test is deactivating all the active td's before creating campaign, that's why index is hard code to 0
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

//	test dormant, campaign type user restricted
	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create 30 days dormant with user restricted td, edit the order and validated it")
	public void thirtyDaysDormantUserRestrictedTypeUserInitTD(HashMap<String, Object> userData) {
		String userID = "999", dormantDays = "30", restID = "883";
		int orderEditSource = 2;
		Boolean firstOrder = true, restFirstOrder = true, userRestricted = true;
		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID,
				userRestricted).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		rngHelper.userMappingTD(userID, String.valueOf(campaignId));
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	public void thirtyDaysDormantUserRestrictedTypeVendorInitTD(HashMap<String, Object> userData) {
		String userID = "999", dormantDays = "30", restID = "883";
		int orderEditSource = 1;
		Boolean firstOrder = true, restFirstOrder = true, userRestricted = true;
		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID,
				userRestricted).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		rngHelper.userMappingTD(userID, String.valueOf(campaignId));
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create expired TD, then call cart evaluate API to validate TD")
//	public void thirtyDaysDormantInvalidUserRestrictedTypeTD(HashMap<String, Object> userData) {
//		String userID = "999", dormantDays = "30", restID = "883";
//		int orderEditSource = 2;
//		Boolean firstOrder = false, restFirstOrder = false, userRestricted = true;
//		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevel(
//				(String) userData.get("minCart"), (String) userData.get("flat"), restID, userRestricted).ResponseValidator
//						.GetBodyAsText();
//		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
//				"TD creation failed. Didn't received any id in the response");
//		int campaignId = JsonPath.read(createTDResponse, "$.data");
//		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
//				"TD creation failed. StatusCode is not zero");
////		create a dormant user
//		rngHelper.DormantUser(userID, restID, dormantDays);
//		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
//				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
//				orderEditSource, new ArrayList<Integer>() {
//					{
//						add(campaignId);
//					}
//				}, Integer.valueOf(userID)+20).ResponseValidator.GetBodyAsText();
//		rngHelper.deleteDormantUserRestMapping(userID);
//		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
//		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
//				"Assertion failed, TD isn't get applied");
//	}
//	NA as order edit isn't responsible for customer dormant validity check
//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "negative test case, create a 30 days dormant user, 60 days td")
//	public void sixtyDaysDormantThirtyDaysUserInitTD(HashMap<String, Object> userData) {
//		String userID = "999", dormantDays = "30", restID = "885";
//		int orderEditSource = 2;
//		Boolean firstOrder =  true, restFirstOrder = true;
//		String createTDResponse = rngHelper.createFlatWithSixtyDayDormantAtRestaurantLevel(
//				(String) userData.get("minCart"), (String) userData.get("flat"), restID, false).ResponseValidator
//						.GetBodyAsText();
//		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),"TD creation failed. Didn't received any id in the response");
//		int campaignId = JsonPath.read(createTDResponse, "$.data");
//		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),"TD creation failed. StatusCode is not zero");
////		create a dormant user
//		rngHelper.DormantUser(userID, restID, dormantDays);
//		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
//		String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
//				orderEditSource, new ArrayList<Integer>() {{add(campaignId);}}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
//		rngHelper.deleteDormantUserRestMapping(userID);
//		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
//		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
//				"Assertion failed, TD isn't get applied");
//	}

//	@Test(dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "negative test case, create a 30 days dormant user, 60 days td")
//	public void sixtyDaysDormantThirtyDaysCustomerVendorInitTD(HashMap<String, Object> userData) {
//		String userID = "999", dormantDays = "30", restID = "875";
//		int orderEditSource = 1;
//		Boolean firstOrder = true, restFirstOrder = true;
//		String createTDResponse = rngHelper.createFlatWithSixtyDayDormantAtRestaurantLevel(
//				(String) userData.get("minCart"), (String) userData.get("flat"), restID, false).ResponseValidator
//						.GetBodyAsText();
//		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
//				"TD creation failed. Didn't received any id in the response");
//		int campaignId = JsonPath.read(createTDResponse, "$.data");
//		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
//				"TD creation failed. StatusCode is not zero");
////		create a dormant user
//		rngHelper.DormantUser(userID, restID, dormantDays);
//		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
//				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
//				orderEditSource, new ArrayList<Integer>() {
//					{
//						add(campaignId);
//					}
//				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
//		rngHelper.deleteDormantUserRestMapping(userID);
//		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
//		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
//				"Assertion failed, TD isn't get applied");
//	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a 30 days dormant user, 60 days user inactivity td")
	public void thirtyDaysDormantSixtyDaysUserInitTD(HashMap<String, Object> userData) {
		String userID = "999", dormantDays = "60", restID = "883";
		int orderEditSource = 2;
		Boolean firstOrder = true, restFirstOrder = true, userRestricted = true;
		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID,
				userRestricted).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//		create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a 30 days dormant user, 60 days user inactivity td")
	public void thirtyDaysDormantSixtyDaysVendorInitTD(HashMap<String, Object> userData) {
		String userID = "999", dormantDays = "60", restID = "883";
		int orderEditSource = 1;
		Boolean firstOrder = true, restFirstOrder = true, userRestricted = true;

		String createTDResponse = rngHelper.createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(
				(String) userData.get("minCart"), (String) userData.get("flat"), restID,
				userRestricted).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
//			create a dormant user
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.cartTradeDiscount"),
				Double.valueOf((String) userData.get("flat")),
				"Assertion Failed, trade discount on cart is not equal to flat discount");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with first order restristion order, here cart amount is zero")
	public void freebieWithFirstOrderRestricitionUserInit(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "883";
		int orderEditSource = 2;
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with first order restristion order, here cart amount is zero")
	public void freebieWithFirstOrderRestricitionVendorInit(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "883";
		int orderEditSource = 1;
		Boolean firstOrder = true, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with no minimum order, here cart amount is zero")
	public void freebieWithNoMinimumOrderUserInitTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "885";
		int orderEditSource = 2;
		Boolean firstOrder = false, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel("0", restID,
				freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate("0", restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				0.0, "Excpected min cart amount is not zero.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with no minimum order, here cart amount is zero")
	public void freebieWithNoMinimumOrderVendorInitTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "885";
		int orderEditSource = 1;
		Boolean firstOrder = false, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel("0", restID,
				freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate("0", restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				0.0, "Excpected min cart amount is not zero.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with minimum amount and no user restriction")
	public void freebieWithMinAmountOrderUserInitTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "133", restID = "884";
		int orderEditSource = 2;
		Boolean firstOrder = false, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				Double.valueOf((String) userData.get("minCart")), "Excpected min cart amount is not same.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with minimum amount and no user restriction")
	public void freebieWithMinAmountOrderVendorInitTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "133", restID = "884";
		int orderEditSource = 1;
		Boolean firstOrder = false, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				Double.valueOf((String) userData.get("minCart")), "Excpected min cart amount is not same.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with minimum amount and thirty days dormant user")
	public void freebieWithThirtyDaysDormantUserInitTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "133", dormantDays = "30", restID = "66766";
		int orderEditSource = 2;
		Boolean firstOrder = false, restFirstOrder = true;
		String createTDResponse = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				Double.valueOf((String) userData.get("minCart")), "Excpected min cart amount is not same.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie with minimum amount and thirty days dormant user")
	public void freebieWithThirtyDaysDormantVendorTD(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "133", dormantDays = "30", restID = "66786";
		int orderEditSource = 1;
		Boolean firstOrder = false, restFirstOrder = false;
		String createTDResponse = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		rngHelper.DormantUser(userID, restID, dormantDays);
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate(
				String.valueOf(Integer.parseInt((String) userData.get("minCart"))), restID, firstOrder, restFirstOrder,
				orderEditSource, new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteDormantUserRestMapping(userID);
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");
		Assert.assertEquals(
				(int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].campaignId"),
				campaignId, "Excpected campaign is not same as applied one.");
		Assert.assertEquals(
				JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].tradeDiscountInfo[0].discountType"),
				"Freebie", "Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardType"), "Freebie",
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].itemId"),
				freebieItemID, "Excpected discount type is not same as applied one.");
		Assert.assertEquals((int) JsonPath.read(cartEvaluateResponse, "$.data.restaurants[0].rewardList[0].count"), 1,
				"Excpected discount type is not same as applied one.");
		Assert.assertEquals(
                JsonPath.read(cartEvaluateResponse,
                        "$.data.restaurants[0].tradeDiscountInfo[0].minCartAmount"),
				Double.valueOf((String) userData.get("minCart")), "Excpected min cart amount is not same.");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie TD, edit order send some other rest id, and validate it")
	public void freebieWithFirstOrderRestricitionRestMismatchUserInit(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "66776";
		Boolean firstOrder = true, restFirstOrder = true;
		int orderEditedSource = 2;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate((String) userData.get("minCart"),
				String.valueOf(Integer.valueOf(restID) + 20), firstOrder, restFirstOrder, orderEditedSource,
				new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
//		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
				"Assertion failed, TD shouldn't get applied");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create a freebie TD, edit order send some other rest id, and validate it")
	public void freebieWithFirstOrderRestricitionRestMismatchVendorInit(HashMap<String, Object> userData) {
		String freebieItemID = "1234", userID = "999", restID = "66776";
		Boolean firstOrder = true, restFirstOrder = true;
		int orderEditedSource = 1;
		String createTDResponse = rngHelper.createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel(
				(String) userData.get("minCart"), restID, freebieItemID).ResponseValidator.GetBodyAsText();
		Assert.assertNotNull(JsonPath.read(createTDResponse, "$.data"),
				"TD creation failed. Didn't received any id in the response");
		Assert.assertTrue(JsonPath.read(createTDResponse, "$.statusCode").equals(0),
				"TD creation failed. StatusCode is not zero");
		int campaignId = JsonPath.read(createTDResponse, "$.data");
		String cartEvaluateResponse = rngHelper.orderEditTDCartEvaluate((String) userData.get("minCart"),
				String.valueOf(Integer.valueOf(restID) + 20), firstOrder, restFirstOrder, orderEditedSource,
				new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID)).ResponseValidator.GetBodyAsText();
		rngHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), false,
				"Assertion failed, TD shouldn't get applied");
	}

	@Test(groups = {"TD"}, dataProvider = "orderEditWithFlatTD", dataProviderClass = DP.class, description = "create final off EDVo campaign, call order edit and validate it")
	public void edvoOrderEditFinalOff(HashMap<String, Object> userData) throws IOException {
		String restID = "7777", mealID = "33744", userID = "999", price = "500", count = "1", groupID = "104";
		String itemID = "1117771";
		int campaignId = edvoHelper.createEDVOCamp(restID, mealID);
		EDVOCartMealItemRequest eDVOCartMealItem = new EDVOCartMealItemRequest();
		eDVOCartMealItem.setMealId(mealID);
		eDVOCartMealItem.setItemId(itemID);
		eDVOCartMealItem.setRestaurantId(restID);
		eDVOCartMealItem.setPrice(price);
		eDVOCartMealItem.setCount(count);
		eDVOCartMealItem.setGroupId(groupID);
		List<EDVOCartMealItemRequest> edvoCartMealItemRequest = new ArrayList();
		edvoCartMealItemRequest.add(eDVOCartMealItem);
		String cartEvaluateResponse = rngHelper.edvoOrderEditCartEvaluate(String.valueOf(10.0),
				String.valueOf(Integer.valueOf(restID)), true, true,
				Integer.valueOf((String) userData.get("initSource")), new ArrayList<Integer>() {
					{
						add(campaignId);
					}
				}, Integer.valueOf(userID), edvoCartMealItemRequest).ResponseValidator.GetBodyAsText();
		System.out.println(cartEvaluateResponse);
		edvoHelper.deleteEDVOCampaignFromDB(String.valueOf(campaignId));
		Assert.assertEquals((boolean) JsonPath.read(cartEvaluateResponse, "$.data.tradeDiscountApplied"), true,
				"Assertion failed, TD isn't get applied");

	}

	@Test(dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create a new user, coupon then place order, edit the same order and confirm it")
	public void orderEditE2ECouponFirstOrderRestrictionTest(HashMap<String, Object> userData) throws IOException {
		CreateMenuEntry payload = null;
		String cartPayload = null;
		HashMap<String, String> orderDetails;
//		user signup and verify otp
		HashMap<String, String> signUpDetails = signupAndLoginUser(userData);
//		create coupon
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
//		 create address
		AddressPOJO address = (AddressPOJO) userData.get("address");
		Integer addressId = addNewAddress(signUpDetails, address);
		Assert.assertTrue(null != addressId, "Address not added");
//		 create cart

		HashMap<String, String> menuItemRest = getMenuItem(address, createCouponPayload.getMinAmount().getCart());
		List<Cart> cart = new ArrayList<>();
		cart.add(new Cart(null, null, menuItemRest.get("menuItemID").toString(), 1));
		payload = new CreateOrderBuilder().cart(cart).restaurant(Integer.valueOf(menuItemRest.get("restID")))
				.paymentMethod("Cash").orderComments("Test-Order").buildPayment();
		cartPayload = jsonHelper.getObjectToJSON(payload.getCartItems());
		System.out.println(checkoutHelper.CreateCartAddon(signUpDetails.get("tid"), signUpDetails.get("token"),
				cartPayload, payload.getRestaurantId()).ResponseValidator.GetBodyAsText());

//		 apply coupon
		String applyCouponResponse = checkoutHelper.ApplyCouponOnCart(signUpDetails.get("tid"),
				signUpDetails.get("token"), createCouponPayload.getCode()).ResponseValidator.GetBodyAsText();

		/*
		 * add assertion on cart that discount applied is correct
		 */
		String getCart1 = checkoutHelper.GetCart(signUpDetails.get("tid"), signUpDetails.get("token")).ResponseValidator
				.GetBodyAsText();
		Assert.assertEquals(JsonPath.read(getCart1, "$.data.discount_total"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Total discount amount not equal");

//		place order
		Processor processor = checkoutHelper.orderPlace(signUpDetails.get("tid"), signUpDetails.get("token"), addressId,
				payload.getpayment_cod_method(), payload.getorder_comments());

		String order = processor.ResponseValidator.GetBodyAsText();
		Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.order_id"), "order is not generated");
		String order_id = headerHelper.JsonString(order, "$.data.order_id");
		Assert.assertEquals(EndToEndConstants.statusCode, headerHelper.JsonString(order, "$.statusCode"),
				"statusCode is not 0");
		Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200, "ResponseCode is not 200");
		Assert.assertNotNull(order_id, "Order generation failed");

		System.out.println("final response ::\n" + order);
		/*
		 * assert if order is placed and coupon is applied discount amount is applied
		 */
		orderDetails = new HashMap<String, String>() {
			{
				put("couponCode", createCouponPayload.getCode());
				put("orderID", order_id);
				put("restID", menuItemRest.get("restID"));
				put("initSource", "2");
				put("menuID", menuItemRest.get("menuItemID"));
				put("quantity", "3");
			}
		};
		// edit order
//		String editOrderCheckResponse= checkoutHelper.editOrderCheck(new String[] {order_id,"2",menuItemRest.get("menuItemID"),"3",menuItemRest.get("restID")}).ResponseValidator.GetBodyAsText();
		String editOrderCheckResponse = checkoutHelper.editOrderCheck(orderDetails).ResponseValidator.GetBodyAsText();

		System.out.println("editOrderCheckResponse ::\n" + editOrderCheckResponse);
		/*
		 * assert if coupon is applied and coupon amount is deducted from the mrp
		 */

		// place order

		String editOrderConfirmResponse = checkoutHelper.editOrderConfirm(orderDetails).ResponseValidator
				.GetBodyAsText();
		// assert before and after cart edit
		System.out.println("editOrderConfirmResponse ::\n" + editOrderConfirmResponse);
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon flat discount, edit the order and validate it")
	public void orderEditFlatCouponFirstOrderRestrictionUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Coupon discount is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon flat discount, edit the order and validate it")
	public void orderEditFlatCouponFirstOrderRestrictionzVendorInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 1;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Coupon discount is not as excpected");

	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon % with discount cap, edit the order send cart amount below discount cap  and validate it")
	public void orderEditPercentageCouponFirstOrderRestrictionUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1).withDiscountPercentage(20).withDiscountAmount(90);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(
						createCouponPayload.getDiscountPercentage() * createCouponPayload.getMinAmount().getCart())
						/ 100,
				"Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon % with discount cap, edit the order send cart amount below discount cap  and validate it")
	public void orderEditPercentageCouponFirstOrderRestrictionVendorInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 1;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1).withDiscountPercentage(20).withDiscountAmount(90);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(
						createCouponPayload.getDiscountPercentage() * createCouponPayload.getMinAmount().getCart())
						/ 100,
				"Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon % with discount cap, edit the order send cart amount above discount cap  and validate it")
	public void percentageWithDiscountCapCouponFirstOrderRestrictionUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1).withDiscountPercentage(50).withDiscountAmount(90);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getDiscountAmount() * 2) + 20,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon % with discount cap, edit the order send cart amount above discount cap  and validate it")
	public void percentageWithDiscountCapCouponFirstOrderRestrictionVendorInitTest(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333, userID = 90, editInitSource = 1;
		Boolean firstOrder = true;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(1).withDiscountPercentage(50).withDiscountAmount(90);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getDiscountAmount() * 2) + 20,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with free del, user restricted, map user; edit the order send same user id and validate it")
	public void freeDeliveryCouponFirstOrderRestrictionUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 88, editInitSource = 2, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1)
				.withFreeShipping(1);
		String createCouponResponse = rngHelper.createCoupon(createCouponPayload).ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		Assert.assertTrue(
				JsonPath.read(RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(),
						couponID).ResponseValidator.GetBodyAsText(), "$.statusCode").equals(1),
				"user mapping was not successfull");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.free_shipping"), Boolean.TRUE,
				"Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with free del, user restricted, map user; edit the order send soem other user id and validate it")
	public void invalidfreeDeliveryCouponCustomerRestrictionUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 88, editInitSource = 2, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1)
				.withFreeShipping(1);
		String createCouponResponse = rngHelper.createCoupon(createCouponPayload).ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		Assert.assertTrue(
				JsonPath.read(RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(),
						couponID).ResponseValidator.GetBodyAsText(), "$.statusCode").equals(1),
				"user mapping was not successfull");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID + 33, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.free_shipping"), Boolean.FALSE,
				"Applied coupon is not as excpected");

	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with free del; edit the order and validate it")
	public void freeDeliveryCouponFirstOrderRestrictionVendorInitTest(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 88, editInitSource = 1, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1)
				.withFreeShipping(1);
		String createCouponResponse = rngHelper.createCoupon(createCouponPayload).ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		Assert.assertTrue(
				JsonPath.read(RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(),
						couponID).ResponseValidator.GetBodyAsText(), "$.statusCode").equals(1),
				"user mapping was not successfull");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.free_shipping"), Boolean.TRUE,
				"Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Applied coupon is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with 5mins date and time expiry; edit the order, send current time and validate it")
	public void timeSlotCouponOrderEditUserInit(HashMap<String, Object> userData) {
		String restID = "1145";
		Integer menuItemID = 233;
		Integer userID = 30, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withSlotRestriction(1)
				.withValidFrom(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000));
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditWithCurrentTimeCartEvaluate(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"), 0.0,
				"Discount amount is not zero");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with expired date and time; edit the order, send current time and validate it")
	public void expiredCouponWhileOrderEditUserInit(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333;
		Integer userID = 90, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withSlotRestriction(1)
				.withValidFrom(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
				.withValidTill(
						String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000));
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditWithCurrentTimeCartEvaluate(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"), 0.0,
				"Discount amount is not zero");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with expired date and time; edit the order, send current time and validate it")
	public void expiredCouponWhileOrderEditVendorInit(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333;
		Integer userID = 90, editInitSource = 1;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withSlotRestriction(1)
				.withValidFrom(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
				.withValidTill(
						String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000));

		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditWithCurrentTimeCartEvaluate(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not zero");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min cart restriction; edit the order, send amount below min cart amount and validate it")
	public void editOrderbelowMinCartAmountCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333;
		Integer userID = 90, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()) - 20,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Coupon shouldn't be applied, actual coupon code is not null");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"), 0.0,
				"Discount amount is not zero");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min cart restriction; edit the order, send amount below min cart amount and validate it")
	public void editOrderbelowMinCartAmountCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6645";
		Integer menuItemID = 333;
		Integer userID = 90, editInitSource = 1;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()) - 20,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with area and city restriction, do area and city mapping; edit the order, send same area and city and validate it")
	public void areaAndCityRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 2, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withAreaRestriction(1).withCityRestriction(1).withIsPrivate(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponAreaMap(createCouponPayload.getCode(), areaEntityID.toString(), couponID);
		RngHelper.couponCityMap(createCouponPayload.getCode(), cityEntityID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(),
				"Vendor init coupon should get applied, expected coupon code is not same");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with area and city restriction, do area and city mapping; edit the order, send same area and city and validate it")
	public void areaAndCityRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 1, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withAreaRestriction(1).withCityRestriction(1).withIsPrivate(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponAreaMap(createCouponPayload.getCode(), areaEntityID.toString(), couponID);
		RngHelper.couponCityMap(createCouponPayload.getCode(), cityEntityID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(),
				"Vendor init coupon should get applied, expected coupon code is not same");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with area and city restriction, do area and city mapping; edit the order, send some other area and city and validate it")
	public void invalidAreaRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 2, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withAreaRestriction(1).withCityRestriction(1).withIsPrivate(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponAreaMap(createCouponPayload.getCode(), areaEntityID.toString(), couponID);
		RngHelper.couponCityMap(createCouponPayload.getCode(), cityEntityID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID * 2, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Coupon shouldn't get applied");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"), 0.0,
				"Discount amount is not zero");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with area and city restriction, do area and city mapping; edit the order, send some other area and city and validate it")
	public void invalidAreaRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6645", couponID;
		Integer menuItemID = 333, userID = 93, editInitSource = 1, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withAreaRestriction(1).withCityRestriction(1).withIsPrivate(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponAreaMap(createCouponPayload.getCode(), areaEntityID.toString(), couponID);
		RngHelper.couponCityMap(createCouponPayload.getCode(), cityEntityID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID * 2, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(),
				"Vendor init coupon should get applied, expected coupon code is not same");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with user restriction, do user coupon mapping; edit the order, send same user and validate it")
	public void userRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6646", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		Assert.assertTrue(
				JsonPath.read(RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(),
						couponID).ResponseValidator.GetBodyAsText(), "$.statusCode").equals(1),
				"user mapping was not successfull");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Coupon applied is not as expected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");

	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with user restriction, do user coupon mapping; edit the order, send same user and validate it")
	public void userRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6646", couponID;
		Integer menuItemID = 333, userID = 91, editInitSource = 1;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponUsageCount(createCouponPayload.getCode(), userID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Coupon applied is not as expected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");

	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with user restriction, do user coupon mapping; edit the order, send some other user and validate it")
	public void invalidUserRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6646", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 2, areaEntityID = 21, cityEntityID = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithAreaListing(
				createCouponPayload.getCode(), menuItemID, restID, userID + 10, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), areaEntityID, cityEntityID,
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Coupon shouldn't get applied");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with user restriction, do user coupon mapping; edit the order, send some other user and validate it")
	public void invalidUserRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6646", couponID;
		Integer menuItemID = 333, userID = 90, editInitSource = 1;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(1).withCustomerRestriction(1);
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		couponID = String.valueOf((int) JsonPath.read(createCouponResponse, "$.data.id"));
		processor = RngHelper.couponUserMap(createCouponPayload.getCode(), userID.toString(), couponID);
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID + 10, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart()).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Expected Coupon isn't applied");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min quantity as 3; edit the order, send quantity as 3 and validate it")
	public void quantityRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6646";
		Integer menuItemID = 333, userID = 901, editInitSource = 2, editQuantity = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(0)
				.withMinQuantity(new MinQuantity().withCart(editQuantity));
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), editQuantity).ResponseValidator
						.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Expected Coupon isn't applied");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min quantity as 3; edit the order, send quantity as 3 and validate it")
	public void quantityRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6646";
		Integer menuItemID = 333, userID = 901, editInitSource = 1, editQuantity = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(0)
				.withMinQuantity(new MinQuantity().withCart(editQuantity));
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), editQuantity).ResponseValidator
						.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Expected Coupon isn't applied");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min quantity as 3; edit the order, send quantity as 2 and validate it")
	public void invalidQuantityRestrictionCouponUserInit(HashMap<String, Object> userData) {
		String restID = "6646";
		Integer menuItemID = 333, userID = 902, editInitSource = 2, editQuantity = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(0)
				.withMinQuantity(new MinQuantity().withCart(editQuantity));
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), editQuantity - 1).ResponseValidator
						.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Coupon shouldn't get applied");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = "create coupon with min quantity as 3; edit the order, send quantity as 2 and validate it")
	public void invalidQuantityRestrictionCouponVendorInit(HashMap<String, Object> userData) {
		String restID = "6646";
		Integer menuItemID = 333, userID = 902, editInitSource = 1, editQuantity = 3;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withIsPrivate(0)
				.withMinQuantity(new MinQuantity().withCart(editQuantity));
		Processor processor = rngHelper.createCoupon(createCouponPayload);
		String createCouponResponse = processor.ResponseValidator.GetBodyAsText();
		Assert.assertEquals((int) JsonPath.read(createCouponResponse, "$.statusCode"), 1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluate(createCouponPayload.getCode(),
				menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()), editQuantity - 1).ResponseValidator
						.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Coupon shouldn't get applied");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Discount amount is not as expected");
	}

	/*
	 * <option value="0">All</option> <option value="1">Web</option> <option
	 * value="2">App</option> <option value="3">Android</option> <option
	 * value="4">IOS</option>
	 */
	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = " create coupon user client as all; edit the order, send web as user client and validate it")
	public void allUserClientFlatCouponUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645", userClient = "WEB";
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withUserClient(0);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithUserClient(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart(), userClient).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"),
				createCouponPayload.getCode(), "Applied coupon is not as excpected");
		Assert.assertEquals(JsonPath.read(couponCartEvaluateResponse, "$.data.discount_amount"),
				Double.valueOf(createCouponPayload.getDiscountAmount()), "Coupon discount is not as excpected");
	}

	@Test(groups = {"Coupons"}, dataProvider = "orderEdit", dataProviderClass = DP.class, description = " create coupon with ios user client; edit the order, send android as user client and validate it")
	public void userClientMismatchFlatCouponUserInitTest(HashMap<String, Object> userData) {
		String restID = "6645", userClient = "ANDROID";
		Integer menuItemID = 333, userID = 90, editInitSource = 2;
		Boolean firstOrder = false;
		CreateCouponPOJO createCouponPayload = (CreateCouponPOJO) userData.get("createCouponPayload");
		createCouponPayload.withFirstOrderRestriction(0).withUserClient(4);
		Processor createCouponResponse = rngHelper.createCoupon(createCouponPayload);
		Assert.assertEquals((int) JsonPath.read(createCouponResponse.ResponseValidator.GetBodyAsText(), "$.statusCode"),
				1, "coupon creation failed");
		String couponCartEvaluateResponse = rngHelper.couponOrderEditCartEvaluateWithUserClient(
				createCouponPayload.getCode(), menuItemID, restID, userID, firstOrder, editInitSource,
				Double.valueOf(createCouponPayload.getMinAmount().getCart()),
				createCouponPayload.getMinQuantity().getCart(), userClient).ResponseValidator.GetBodyAsText();
		RngHelper.deleteCoupon(createCouponPayload.getCode());
		Assert.assertEquals((String) JsonPath.read(couponCartEvaluateResponse, "$.data.couponCode"), null,
				"Applied coupon is not as excpected");
	}
// item mapping

//	td and coupon both applied on cacrt
//	min cart min quantity
//	 NA -- if coupon was not applied then should not get coupon discount for any active coupon at the time  of Order Edit
//	Order edit is still in progress and in mean time user place one more order, how we will treat it? will it be new user if its user first order ?

}
