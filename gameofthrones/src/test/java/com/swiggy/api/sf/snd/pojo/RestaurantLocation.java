package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RestaurantLocation {

    private String type;
    private Point point;
    private Double distance;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantLocation() {
    }

    /**
     *
     * @param point
     * @param distance
     * @param type
     */
    public RestaurantLocation(String type, Point point, Double distance) {
        super();
        this.type = type;
        this.point = point;
        this.distance = distance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("point", point).append("distance", distance).toString();
    }

}
