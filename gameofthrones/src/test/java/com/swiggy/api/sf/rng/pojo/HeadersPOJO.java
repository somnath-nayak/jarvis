package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "versionCode",
        "userAgent"
})
public class HeadersPOJO {

    @JsonProperty("versionCode")
    private Integer versionCode;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public HeadersPOJO() {
    }

    /**
     *
     * @param userAgent
     * @param versionCode
     */
    public HeadersPOJO(Integer versionCode, String userAgent) {
        super();
        this.versionCode = versionCode;
        this.userAgent = userAgent;
    }

    @JsonProperty("versionCode")
    public Integer getVersionCode() {
        return versionCode;
    }

    @JsonProperty("versionCode")
    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public HeadersPOJO withVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
        return this;
    }

    @JsonProperty("userAgent")
    public String getUserAgent() {
        return userAgent;
    }

    @JsonProperty("userAgent")
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public HeadersPOJO withUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public HeadersPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public HeadersPOJO setDefault() {
        return this.withVersionCode(302)
                .withUserAgent("ANDROID");
    }

}
