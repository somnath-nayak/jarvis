package com.swiggy.api.sf.rng.pojo.CopyResolver;

import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.RuleDiscount;
import com.swiggy.api.sf.rng.pojo.Slot;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CreateTdWithCopy {

    @JsonProperty("namespace")
    private String namespace = "Automation";
    @JsonProperty("header")
    private String header = "Default";
    @JsonProperty("description")
    private String description = "Resolver-Automation-Default-Description";
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType = "Percentage";
    @JsonProperty("restaurant_hit")
    private String restaurantHit = "40";
    @JsonProperty("enabled")
    private Boolean enabled = true;
    @JsonProperty("swiggy_hit")
    private String swiggyHit = "60";
    @JsonProperty("createdBy")
    private String createdBy = "Resolver-Automation";
    @JsonProperty("discountLevel")
    private String discountLevel = "Restaurant";
    @JsonProperty("restaurantList")
    private List<RestaurantList> restaurantList = null;
    @JsonProperty("ruleDiscount")
    private RuleDiscount ruleDiscount;
    @JsonProperty("slots")
    private List<Slot> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill = false;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill = false;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction = false;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction = false;
    @JsonProperty("userRestriction")
    private Boolean userRestriction = false;
    @JsonProperty("dormant_user_type")
    private String dormantUserType = "ZERO_DAYS_DORMANT";
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder = false;
    @JsonProperty("updatedBy")
    private String updatedBy = "Resolver-Automation";
    @JsonProperty("isSuper")
    private Boolean isSuper = false;
    @JsonProperty("copyOverridden")
    private Boolean copyOverridden = false;
    @JsonProperty("shortDescription")
    private String shortDescription = "Default";

    public CreateTdWithCopy() {

    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public CreateTdWithCopy withNameSpace(String nameSpace) {
        this.namespace = nameSpace;
        return this;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public CreateTdWithCopy withHeader(String header) {
        this.header = header;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CreateTdWithCopy withDescription(String description) {
        this.description = description;
        return this;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public CreateTdWithCopy withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public CreateTdWithCopy withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public CreateTdWithCopy withCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    public String getRestaurantHit() {
        return restaurantHit;
    }

    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    public CreateTdWithCopy withRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
        return this;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public CreateTdWithCopy withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public String getSwiggyHit() {
        return swiggyHit;
    }

    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    public CreateTdWithCopy withSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
        return this;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CreateTdWithCopy withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public String getDiscountLevel() {
        return discountLevel;
    }

    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public CreateTdWithCopy withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    public List<RestaurantList> getRestaurantList() {
        return restaurantList;
    }

    public void setRestaurantList(List<RestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public CreateTdWithCopy withRestaurantList(List<RestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
        return this;
    }

    public RuleDiscount getRuleDiscount() {
        return ruleDiscount;
    }

    public void setRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
    }

    public CreateTdWithCopy withRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
        return this;
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public void setSlots(List<Slot> slots) {
        this.slots = slots;
    }

    public CreateTdWithCopy withSlots(List<Slot> slots) {
        this.slots = slots;
        return this;
    }

    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    public CreateTdWithCopy withCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
        return this;
    }

    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    public CreateTdWithCopy withTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        return this;
    }

    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public CreateTdWithCopy withFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    public CreateTdWithCopy withTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
        return this;
    }

    public Boolean getUserRestriction() {
        return userRestriction;
    }

    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    public CreateTdWithCopy withUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    public String getDormantUserType() {
        return dormantUserType;
    }

    public void setDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
    }

    public CreateTdWithCopy withDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
        return this;
    }

    public Boolean getRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    public CreateTdWithCopy withRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
        return this;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public CreateTdWithCopy withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public Boolean getSuper() {
        return isSuper;
    }

    public void setSuper(Boolean aSuper) {
        isSuper = aSuper;
    }

    public CreateTdWithCopy withSuper(Boolean isSuper) {
        this.isSuper = isSuper;
        return this;
    }

    public Boolean getCopyOverridden() {
        return copyOverridden;
    }

    public void setCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
    }

    public CreateTdWithCopy withCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
        return this;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public CreateTdWithCopy withShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
        return this;
    }

    public CreateTdWithCopy withDefaultData() {
        this.namespace = "Automation";
        this.header = "Default";
      this.description = "Resolver-Automation-Default-Description";

//      this.validFrom = ;
//        @JsonProperty("valid_till")
//        private String validTill;
//        @JsonProperty("campaign_type")
//        private String campaignType = "Percentage";
//        @JsonProperty("restaurant_hit")
//        private String restaurantHit = "40";
//        @JsonProperty("enabled")
//        private Boolean enabled = true;
//        @JsonProperty("swiggy_hit")
//        private String swiggyHit = "60";
//        @JsonProperty("createdBy")
//        private String createdBy = "Resolver-Automation";
//        @JsonProperty("discountLevel")
//        private String discountLevel = "Restaurant";
//        @JsonProperty("restaurantList")
//        private List<RestaurantList> restaurantList = null;
//        @JsonProperty("ruleDiscount")
//        private RuleDiscount ruleDiscount;
//        @JsonProperty("slots")
//        private List<Slot> slots = null;
//        @JsonProperty("commissionOnFullBill")
//        private Boolean commissionOnFullBill = false;
//        @JsonProperty("taxesOnDiscountedBill")
//        private Boolean taxesOnDiscountedBill = false;
//        @JsonProperty("firstOrderRestriction")
//        private Boolean firstOrderRestriction = false;
//        @JsonProperty("timeSlotRestriction")
//        private Boolean timeSlotRestriction = false;
//        @JsonProperty("userRestriction")
//        private Boolean userRestriction = false;
//        @JsonProperty("dormant_user_type")
//        private String dormantUserType = "ZERO_DAYS_DORMANT";
//        @JsonProperty("restaurantFirstOrder")
//        private Boolean restaurantFirstOrder = false;
//        @JsonProperty("updatedBy")
//        private String updatedBy = "Resolver-Automation";
//        @JsonProperty("isSuper")
//        private Boolean isSuper = false;
//        @JsonProperty("copyOverridden")
//        private Boolean copyOverridden = false;
//        @JsonProperty("shortDescription")
//        private String shortDescription = "Default";


        return null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("namespace", namespace).append("header", header).append("description", description).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("restaurantHit", restaurantHit).append("enabled", enabled).append("swiggyHit", swiggyHit).append("createdBy", createdBy).append("discountLevel", discountLevel).append("restaurantList", restaurantList).append("ruleDiscount", ruleDiscount).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("userRestriction", userRestriction).append("dormantUserType", dormantUserType).append("restaurantFirstOrder", restaurantFirstOrder).toString();
    }
}
