package com.swiggy.api.sf.checkout.helper.edvo.util;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.testng.Reporter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {
    public static <T> T jsonDecode(String s, Class<?> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        JavaType classType = objectMapper.getTypeFactory().constructType(clazz);
        T cartV2Response = null;
        try {
            return objectMapper.reader(classType).readValue(s);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Reporter.log("Failed to decode Json String to pojo class", true);
        return null;
    }

    public static String jsonEncode(Object object){
        JsonHelper jh = new JsonHelper();
        String payload = null;
        try {
            return jh.getObjectToJSON(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean doesJsonPathExist(Processor processor, String jsonPath) {
        try {
            Object object = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), jsonPath);
            Reporter.log("JSON PATH FOUND == '" + jsonPath + "'", true);
        }catch (PathNotFoundException e){
            Reporter.log("JSON PATH DOES NOT EXIST == '" + jsonPath + "'", true);
            return false;
        }
        return true;
    }

    public static void wait(int milliSecondsToWait){
        try { Thread.sleep(milliSecondsToWait);}
        catch (InterruptedException e) {e.printStackTrace();}
    }

    public static String getCurrentTimeStamp(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = simpleDateFormat.format(now);
        return strDate;
    }

    public static String getTimeStampPlusOneYear(){
        String currentTimeStamp = getCurrentTimeStamp();
        String year = currentTimeStamp.substring(0,4);
        Integer intYear = Integer.parseInt(year);
        intYear++;
        return currentTimeStamp.replace(year, String.valueOf(intYear));
    }

    public static Long getCurrentTimeStampInEpoch(){
        Long epoch = null;
        try {
            epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(getCurrentTimeStamp()).getTime();
        }catch (ParseException e){}
        return epoch;
    }

    public static Long getTimeStampPlusOneYearInEpoch(){
        Long epoch = null;
        try {
            epoch = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(getTimeStampPlusOneYear()).getTime();
        }catch (ParseException e){}
        return epoch;
    }
}
