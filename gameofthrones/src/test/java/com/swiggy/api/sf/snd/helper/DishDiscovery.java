package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.snd.constants.IStatusConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.util.*;

public class DishDiscovery {

    Initialize gameofthrones = new Initialize();
    RngHelper rngHelper= new RngHelper();
    JsonHelper jsonHelper= new JsonHelper();
    RedisHelper redisHelper= new RedisHelper();


    public Processor search(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanddev", "conan", gameofthrones);
        String[] payloadparams = {payload };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor searchDebug(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanddev", "conandebug", gameofthrones);
        String[] payloadparams = {payload };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor campaignSlots(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanddev", "collectioncampaign", gameofthrones);
        String[] payloadparams = {payload };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor collectionSchema(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanddev", "collectionschemas", gameofthrones);
        String[] payloadparams = {payload};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor collectionLayout(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sanddev", "collectionlayout", gameofthrones);
        String[] payloadparams = {payload };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor collectionAggregator(String payload)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "collaggregator", gameofthrones);
        String[] payloadparamas= {payload};
        Processor processor = new Processor(service, requestheaders, payloadparamas);
        return processor;
    }

    public void generateTd(List<String> restId) throws IOException{
        for(int i= 0; i<restId.size(); i++) {

           //List<Slot> slots = new ArrayList<>();
           // slots.add(new Slot("1430", "ALL", "1419"));
            List<RestaurantList> restaurantLists1 = new ArrayList<>();
            restaurantLists1.add(new RestaurantList(restId.get(i).replace("\"",""), "Test", new ArrayList<>()));

            CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
                    .header("RestauratLevelDiscount")
                    .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                    .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 3600).toInstant().getEpochSecond() * 1000))
                    .campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Jitender")
                    .discountLevel("Restaurant").restaurantList(restaurantLists1)
                    //.slots(slots)
                    .ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").userRestriction(false)
                    .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                    .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
            rngHelper.createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
            System.out.println("rest id"+restId);
            redisHelper.sAdd("sandredisstage","trade_discount_enabled_rest_set",restId.get(i).replace("\"",""));
        }
    }


    public List<Map<String, Object>> selectCollection(){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(IStatusConstants.collectionTable);
        List<Map<String, Object>> list = sqlTemplate.queryForList(IStatusConstants.query);
        return list;
    }

    public int updateCOllection(){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(IStatusConstants.collectionTable);
        int update = sqlTemplate.update(IStatusConstants.query);
        return update;
    }

    public Processor postHolidayTimeSlots(String restId, String fromTime, String toTime) {
        HashMap<String, String> requestheaders = requestHeaders();
        GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "holidayslot", gameofthrones);
        String[] payloadparams = {restId, fromTime, toTime};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public HashMap<String, String> requestHeaders()
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("user_meta","{\"source\": \"CMS\", \"user\":\"cms-tester\"}");
        return requestheaders;
    }


    public Processor SingleCollectionVerify(String latlng, String collectionId){
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sandrasmalai", "collectionrasmalai", gameofthrones);
        String[] payloadparamas= {latlng,collectionId};
        Processor processor = new Processor(service, requestheaders, payloadparamas);
        return processor;
    }


}
