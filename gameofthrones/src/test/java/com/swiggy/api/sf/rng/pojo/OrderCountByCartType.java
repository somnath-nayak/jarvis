package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class OrderCountByCartType {
    @JsonProperty("Pop")
    private long pop;

    @JsonProperty("Pre")
    private long pre;

    @JsonProperty("Cafe")
    private long cafe;

    @JsonProperty("Promo")
    private long promo;

    @JsonProperty("SwiggyExpress")
    private long swiggyExpress;

    @JsonProperty("Group")
    private long group;

    public long getPop() {
        return pop;
    }

    public void setPop(long pop) {
        this.pop = pop;
    }

    public OrderCountByCartType withPop(long Pop){
        this.pop = Pop;
        return this;
    }
    public long getPre() {
        return pre;
    }

    public void setPre(long pre) {
        this.pre = pre;
    }

    public OrderCountByCartType withPre(long Pre){
        this.pre = Pre;
        return this;
    }
    public long getCafe() {
        return cafe;
    }

    public void setCafe(long cafe) {
        this.cafe = this.cafe;
    }
    public OrderCountByCartType withCafe(long Cafe){
        this.cafe = Cafe;
        return this;
    }
    public long getPromo() {
        return promo;
    }

    public void setPromo(long promo) {

        this.promo = promo;
    }

    public OrderCountByCartType withPromo(long Promo){
        this.promo = Promo;
        return this;
    }
    public long getSwiggyExpress() {
        return swiggyExpress;
    }

    public void setSwiggyExpress(long SwiggyExpress) {
        this.swiggyExpress = SwiggyExpress;
    }

    public OrderCountByCartType withSwiggyExpress(long SwiggyExpress){
        this.swiggyExpress = SwiggyExpress;
        return this;
    }
    public long getGroup() {
        return group;
    }

    public void setGroup(long Group) {
        this.group = Group;
    }

    public OrderCountByCartType withGroup(long Group){
        this.group = Group;
        return this;
    }
}
