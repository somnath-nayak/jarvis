package com.swiggy.api.sf.rng.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RuleDiscount {

    private String type;
    private String discountLevel;
    private String percentDiscount;
    private String minCartAmount;
    private String percentDiscountAbsoluteCap;
   
	/**
     * No args constructor for use in serialization
     *
     */
    public RuleDiscount() {
    }

    /**
     *
     * @param percentDiscountAbsoluteCap
     * @param percentDiscount
     * @param minCartAmount
     * @param discountLevel
     * @param type
     */

    public RuleDiscount(String type, String discountLevel, String percentDiscount, String minCartAmount,String percentDiscountAbsoluteCap) {
		super();
		this.type = type;
		this.discountLevel = discountLevel;
		this.percentDiscount = percentDiscount;
		this.minCartAmount = minCartAmount;
		this.percentDiscountAbsoluteCap = percentDiscountAbsoluteCap;
	}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDiscountLevel() {
        return discountLevel;
    }

    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public String getPercentDiscount() {
        return percentDiscount;
    }

    public void setPercentDiscount(String percentDiscount) {
        this.percentDiscount = percentDiscount;
    }

    public String getMinCartAmount() {
        return minCartAmount;
    }

    public void setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
    }
    
    public void setPercentDiscountAbsoluteCap(String type) {
        this.type = type;
    }

    public String getPercentDiscountAbsoluteCap() {
        return percentDiscountAbsoluteCap;
    }

    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("discountLevel", discountLevel).append("percentDiscount", percentDiscount).append("minCartAmount",minCartAmount).append("percentDiscountAbsoluteCap", percentDiscountAbsoluteCap).toString();
    }

}