package com.swiggy.api.sf.rng.tests;


import com.swiggy.api.sf.rng.dp.DP;
import com.swiggy.api.sf.rng.dp.TdCachingDP;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.tdcaching.OperationModel;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.swiggy.api.sf.rng.constants.CacheConstants.*;
import static com.swiggy.api.sf.rng.constants.RngConstants.DATABASE;
import static com.swiggy.api.sf.rng.helper.TdCachingHelper.*;

public class TdCachingTest extends DP {

    private RngHelper rngHelper = new RngHelper();

    private TDRedisHelper tdRedisHelper = new TDRedisHelper();

    private TdCachingHelper tdCachingHelper = new TdCachingHelper() ;

    private EDVOHelper edvoHelper = new EDVOHelper();




//     ==================================== Restaurant Only for Guava ======================================

    @Test(description="test data is not coming from guava when data is not present in guava" )
    public void guavaEnabledForCampaignTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";

        String testRestaurantId = getRestaurantIds();


        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, userAgent, versionCode)
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because -> data is present in guava");

        tdCachingHelper.evaluateMenu(testRestaurantId, testUserId, userAgent, versionCode, "[]");
//        tdCachingHelper.evaluateCart(testRestaurantId);
    }


    @Test(description="test data is coming from guava, when data is present in guava" )
    public void guavaEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId);
//        Assert.assertEquals(header, testHeader);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));
        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
        Assert.assertEquals(result1, campaignId,"Failure because-> data is not present in guava");
//        Assert.assertEquals(header1, testHeader);

        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        tdCachingHelper.evaluateMenu(testRestaurantId, "9465",userAgent, versionCode, testHeader);
    }

    @Test(description="Test rest level data is not coming from guava when data is not present in guava" )
    public void guavaEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");


        Assert.assertEquals(result, "[]","data is present in guava");
    }

    @Test(description="Rest TD data is not coming from guava")
    public void guavaEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
        tdCachingHelper.evictKeyFromGuava();

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"data is present in guava");
    }

    @Test(description="")
    public void guavaEnabledForTdOperationTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent() {

    }

    @Test(description="")
    public void guavaEnabledForTdOperationTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresent() {

    }

    @Test(description="Test user mapped camp data is not coming from guava when data is not present in guava")
    public void guavaEnabledForUserTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        String testUserId = getUserId();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because :- user mapped camp. data is present in guava");
    }


    @Test(description="Test user mapped camp data is coming from guava ")
    public void guavaEnabledForUserTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        String testUserId = getUserId();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);


        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because-> data is not present in guava");
    }


    //     ==================================== Restaurant Only for Redis  ======================================
    @Test(description="Test data should not come from redis when data is not present")
    public void redisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldNotComeFromRedisIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"Failure because -> data is not present in DB");
//        Assert.assertEquals(header, DATABASE);
    }

    @Test (description="Test data is coming from redis when data is present in redis")
    public void redisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId);
//        Assert.assertEquals(header, testHeader);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdCachingHelper.evictKeyFromGuava();
        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        header = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"Failure because -> data is not present in redis ");
//        Assert.assertEquals(header, testHeader);
    }

    @Test(description="Test data is not coming from redis when data is not present")
    public void redisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldNotComeFromRedisIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");


        Assert.assertEquals(result, "[]","Failure because -> data is present in DB");
    }

    @Test(description="Test data is coming from redis when data is present in redis")
    public void redisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        String replacedRestaurantId = testRestaurantId + "1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because -> data is not present in redis");

        tdCachingHelper.checkRedisData(RESTAURANT_REDIS_KEY, campaignId, restaurantId);
    }

    @Test(description="Test data is not coming from redis when data is not present")
    public void redisEnabledForUserTestCreateRestaurantLevelTdDataShouldNotComeFromRedisIfDataIsNotPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", replaceUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"data is present in DB");
    }

    @Test(description="Test data is coming from redis when data is present in redis")
    public void redisEnabledForUserTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsPresent() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);


        tdCachingHelper.evictKeyFromGuava();

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because-> data is not present in Redis");
    }

//     ==================================== Restaurant Both Guava And Redis  ======================================

    @Test(description="Test data is not coming when data is not present in Guava, Redis and DB")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because-> data is present in DB");

    }

    @Test(description="Test data is not coming from guava and redis when data is present in DB only ")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String header = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"data is not present in DB");
//        Assert.assertEquals(header, DATABASE);
    }

    @Test(description="Test data  is Coming From Guava if Data Is Present in Guava Redis And Db ")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {

        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdRedisHelper.setKeyValue(getKey(CAMPAIGN_REDIS_KEY, campaignId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
//
//        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"failure because -> data is not present in guava");
    }

    @Test(description="Test data is Coming From Guava if Data is present in Guava And Db Only ")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

//
// ert.assertEquals(resultHeaderAfterEvict, testHeader);
    }

    @Test(description="Test data is coming from guava when data is present in guava and redis")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        TDCachingDbHelper.deleteCampaignById(campaignId);
        tdRedisHelper.setKeyValue(getKey(CAMPAIGN_REDIS_KEY, campaignId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

//        String resultHeaderAfterEvict = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
//
//        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because-> data is not coming from guava");
    }

    @Test(description="Test Data is Coming From Redis if Data is not present in Guava but present in Redis And Db ")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaButPresentInRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        tdCachingHelper.evictKeyFromGuava();
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

//        String resultHeaderAfterEvict = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
//
//        Assert.assertEquals(resultHeaderAfterEvict, testHeader);
    }

    @Test(description="Test data is coming from redis only when data is not present in guava and DB")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaAndDbButPresentInRedisOnly() throws IOException {

        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        tdCachingHelper.evictKeyFromGuava();
        TDCachingDbHelper.deleteCampaignById(campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

//        String resultHeaderAfterEvict = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
//
//        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because-> data is not coming from redis");
    }

    @Test(description="Test Data is Coming From Db if Data is not present in Redis And Guava ")
    public void guavaAndRedisEnabledForCampaignTestCreateRestaurantLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava() throws IOException {
        String testHeader = "caching_test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].name");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);


        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

//        String resultHeaderAfterEvict = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].name");
//
//        Assert.assertEquals(resultHeaderAfterEvict, testHeader);
    }

    //Restaurant

    @Test(description="Test data is not Coming if Data is not present in Guava, Redis And Db ")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws Exception {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");


        Assert.assertEquals(result, "[]");
    }

    @Test(description="Test data is not coming from guava and redis when data is present only in DB")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
        tdCachingHelper.evictKeyFromGuava();

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"Failure because -> data is not present in DB");
    }

    @Test(description="Test data is coming from guava when  data data is present in guava, redis and DB")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        String replacedRestaurantId = testRestaurantId + "1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdRedisHelper.setKeyValue(getKey(RESTAURANT_REDIS_KEY, restaurantId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because -> data is present in guava");
    }

    @Test(description="Test data is coming from guava when data is present in guava and DB")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        String replacedRestaurantId = testRestaurantId + "1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, restaurantId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because-> data is not coming from guava");

    }

    @Test(description="Test data is coming from guava when data is present in guava and redis when td is restaurant level")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        TDCachingDbHelper.deleteRestaurantMap(restaurantId, campaignId);
        tdRedisHelper.setKeyValue(getKey(RESTAURANT_REDIS_KEY, restaurantId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"failure because ->> data is not coming from guava ");
    }

    @Test(description="Test data is coming from redis when data is present in redis and DB AND TD is restaurant level")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaButPresentInRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
//
//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);

        String replacedRestaurantId = testRestaurantId + "1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"failure because ->> data is not present in Redis ");

        tdCachingHelper.checkRedisData(RESTAURANT_REDIS_KEY, campaignId, restaurantId);

    }

    @Test(description="Test data is coming from guava when data is present in redis not in DB and guava and TD is restaurant level")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaAndDbButPresentInRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(resultCampaignId, campaignId);

        TDCachingDbHelper.deleteRestaurantMap(restaurantId, campaignId);
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"failure because ->> data is not present in redis");

//        tdCachingHelper.checkRedisData(RESTAURANT_REDIS_KEY, restaurantId, campaignId);
    }

    @Test(description="Test data is coming from DB when data is not present in Redis & Guava and TD is restaurant level")
    public void guavaAndRedisEnabledForRestaurantTestCreateRestaurantLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

//        String resultHeader = listingResponse.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId);
//        Assert.assertEquals(resultHeader, testHeader);


        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, restaurantId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because ->> data is not coming from DB");
    }

    // User map

    @Test (description="Test data is coming when data is not present in Guava, Redis and DB ")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because->> data in present in DB, Redis or Guava");
    }

    @Test(description="Test data is coming from DB when data is not persent in Redis and Guava and TD is restaurant level ")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", replaceUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because ->> data is not coming from DB");
    }

    @Test(description="Test data is coming from guava when data is present in all Guava, Redis & DB when td is restaurant level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId);


        tdRedisHelper.setKeyValue(getKey(TD_USER_MAP_REDIS_KEY, testUserId), "");

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because ->> data is not coming from guava");
    }

    @Test(description="Test data is coming from Guava when data is present in Guava & DB when TD is restaurant level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);


        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        tdRedisHelper.delete(getKey(TD_USER_MAP_REDIS_KEY, testUserId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because ->> data is not coming from Guava");

    }

    @Test(description="Test data is coming from Guava when data is present in Guava & Redis when TD is restaurant level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);
        tdRedisHelper.setKeyValue(getKey(TD_USER_MAP_REDIS_KEY, testUserId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because ->> data is not coming from Guava");
    }

    @Test(description="Test data is coming from Redis when data is present in Redis and DB ")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaButPresentInRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);


        tdCachingHelper.evictKeyFromGuava();

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because ->> data is not coming from Redis");
    }

    @Test(description="Test data is coming from Redis when data is not present in Guava and DB when TD is restaurant level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaAndDbButPresentInRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId);
    }

    @Test(description="Test data is coming from DB when data is not present in Guava & Redis when TD is restaurant level with user restirction")
    public void guavaAndRedisEnabledForUserMapTestCreateRestaurantLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava() throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader, discountAmount);

        String campaignId = map.get("campaign_id");

        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId);

        tdRedisHelper.delete(getKey(TD_USER_MAP_REDIS_KEY, testUserId));
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from DB");
    }


//         ==================================== Meal level Only for Guava ======================================


    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class,description="Test data is not coming from Guava when data is not present " )
    public void guavaEnabledForCampaignTestCreateMealLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent(HashMap<String,String> cartV3Payload,String testRestaurantId, String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
//        String testRestaurantId = getRestaurantIds();
//        String mealId = getRestaurantIds();
//        String groupId = getRestaurantIds();


        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because --> data is present in DB");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
        Assert.assertEquals(cartResponse, "[]","Failure because --> data is present in DB");


    }

    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class ,description="Test data is coming from Guava when data is present and TD is meal level" )
    public void guavaEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresent(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();
//        String testRestaurantId = getRestaurantIds();
//        String mealId = getRestaurantIds();
//        String groupId = getRestaurantIds();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        //TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");


        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"campaign is not present for the testRestaurantId");
        Assert.assertEquals(header, testHeader,"campaign is not present for the testRestaurantId");
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String headerInListing = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not present in Guava ");
        Assert.assertEquals(headerInListing, testHeader,"Failure because --> data is not present in Guava ");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> data is not present in Guava ");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> data is not present in Guava ");


    }

    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class,description="Test data is not coming from  Guava when data is not present and TD is MEAL type "  )
    public void guavaEnabledForRestaurantTestCreateMealLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");


        Assert.assertEquals(result, "[]","Failure because --> data is coming from DB");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
        Assert.assertEquals(cartResponse, "[]","Failure because --> data is coming from DB");
    }

    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class ,description="Test data is not coming from Guava when data is present only in DB " )
    public void guavaEnabledForRestaurantTestCreateMealLevelTdDataShouldNotComeFromGuavaIfDataIsPresent(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

//        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
//        tdCachingHelper.evictKeyFromGuava();

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"Failure because --> Data is not present in DB");

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
        tdCachingHelper.evictKeyFromGuava();

        //cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> Data is not coming from");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> Data is not coming from");

    }

    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class ,description="Test data is not coming when data is not present and TD is MEAL type ")
    public void guavaEnabledForUserTestCreateMealLevelTdDataShouldNotComeFromGuavaIfDataIsNotPresent(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,true, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","test data is present for the user");


        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
        Assert.assertEquals(cartResponse, "[]","test data is present for the user");
    }


    @Test(dataProvider = "guavaEnabledForCampaignTestData", dataProviderClass = TdCachingDP.class ,description="Test data is coming from Guava when test data is present and TD is MEAL type")
    public void guavaEnabledForUserTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresent(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,true, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        // TDCachingDbHelper.updateUserRestrictionOfCampaign(campaignId, "1");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        //  String replaceUserId = testUserId + "1";
        //TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because --> campaign is not present for the testRestaurantId");
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Campaign id is not present in listing response");
        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

//        Assert.assertEquals(resultHeader, testHeader);


        // cart evaluate
//        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
//        String cartResponse = cartV3Response.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");
//
//        String headerInCart = cartV3Response.ResponseValidator
//                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");
//
//        Assert.assertEquals(cartResponse, campaignId);
//        Assert.assertEquals(headerInCart, testHeader);
    }


//     ==================================== Meal Both Guava And Redis  ======================================


    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class ,description="Test data is not coming when data is not present in Guava, Redis and DB and TD is MEAL type")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because --> data is present in DB");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
        Assert.assertEquals(cartResponse, "[]","Fialure because --> data is present in DB ");

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class ,description="Test data is not coming from Guava and Redis when data is present only in DB and TD is MEAL type ")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(header, DATABASE,"Failure because --> campaign is not present for testRestaurantId");

        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        TDCachingDbHelper.updateHeaderOfCampaign(testHeader, campaignId);
        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> data is not coming from DB");


    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class ,description="Test data is coming from Guava when data is present in all Guava,Redis & DB and TD is MEAL type")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"campaign is not present for the testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"campaign is not present for the testRestaurantId");

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdRedisHelper.setKeyValue(getKey(CAMPAIGN_REDIS_KEY, campaignId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because data is not coming from Guava ");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(headerInCart, testHeader, "Failure because --> data is not coming from Guava");
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Guava when data is present in Guava and DB and TD is Meal type ")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because  --> campaign is not present for the testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"Failure because --> test header is different");

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because --> data is not coming from Guava");

        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> Campaign is not present for the testRestaurantId");
        Assert.assertEquals(headerInCart, testHeader,"failure because --> data is not coming from Guava ");
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Guava when data is present in Redis and Guava and TD is Meal level  ")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId, "Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader, "Failure because --> test header is different ");

//        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

//        TDCachingDbHelper.deleteCampaignById(campaignId);
//        tdRedisHelper.setKeyValue(getKey(CAMPAIGN_REDIS_KEY, campaignId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because --> data is not coming from Guava");

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> data is not present ");
//        Assert.assertEquals(headerInCart, testHeader);

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from redis when data is present in Redis & DB")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaButPresentInRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();
        cartV3Payload.put("4",testUserId);

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"campaign is not present for the testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"Failure because test header is different");

        tdCachingHelper.evictKeyFromGuava();
        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because --> data is not present in Redis");

        tdCachingHelper.evictKeyFromGuava();
        // cart evaluate
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Campaign is not present for the testRestaurantId");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> data is not coming from Redis ");
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from redis when data is present in redis only")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaAndDbButPresentInRedisOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campaign is not present for testRestaurantId");
//        Assert.assertEquals(resultHeader, testHeader);

        tdCachingHelper.evictKeyFromGuava();
//        TDCachingDbHelper.deleteCampaignById(campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

//        Assert.assertEquals(resultHeaderAfterEvict, testHeader);

        tdCachingHelper.evictKeyFromGuava();
        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campagin is not present for testRestaurantId");
//        Assert.assertEquals(headerInCart, testHeader);

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from DB when data is not present in Redis & Guava and TD is MEAL Type")
    public void guavaAndRedisEnabledForCampaignTestCreateMealLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"Failure because --> campaign test header is different");


        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because --> data is not coming from DB");

        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(CAMPAIGN_REDIS_KEY, campaignId));

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> data is not coming from DB");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> data is not coming from DB");

    }



    //Meal

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is not coming when data is not present in DB,Redis & Guava")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws Exception {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");


        Assert.assertEquals(result, "[]","Failure because --> Campaign is present for testRestaurantId ");

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo");
        Assert.assertEquals(cartResponse, "[]","Failure because --> Campaign is present for testRestaurantId ");
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class, description="Test data is not coming from Redis and Guava when data is only present in DB and TD is MEAL type")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {

        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");
        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"Failure because --> campaign is not present for testRestaurantId ");

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
        tdCachingHelper.evictKeyFromGuava();

         listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");
        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        String resultHeaderAfterEvict = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderAfterEvict, testHeader,"Failure because --> test header is different");
        Assert.assertEquals(result1, campaignId,"Failure because --> campaign is not present for testRestaurantId in DB ");

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));
        tdCachingHelper.evictKeyFromGuava();
        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId , "Failure because --> campaign is not present for testRestaurantId in DB ");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> test header is different");

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Guava when data is present in Guava, Redis & DB and TD is MEAL type")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campagin is not present for testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"Failure because --> test header is different");

        String replacedRestaurantId = testRestaurantId+"1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdRedisHelper.setKeyValue(getKey(RESTAURANT_REDIS_KEY, restaurantId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        String resultHeaderListing = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultHeaderListing, testHeader,"Failure because --> data is not coming from Guava");
        Assert.assertEquals(result, campaignId,"Failure because --> campaign is not present for testRestaurantId");

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campaign is not present for testRestaurantId");
        Assert.assertEquals(headerInCart, testHeader,"Failure because --> data is not coming from Guava");

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from guava when data is present in Guava & Redis and TD is MEAL type")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId)throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"");
        Assert.assertEquals(resultHeader, testHeader);

        String replacedRestaurantId = testRestaurantId+"1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, restaurantId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        String resultHeaderListing = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
        Assert.assertEquals(resultHeaderListing, testHeader,"Failure because --> Test header is different");
        Assert.assertEquals(result, campaignId, "Failure because --> campaign is not present for testRestaurantId ");

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId, "Failure because --> data is not coming from Guava");
        Assert.assertEquals(headerInCart, testHeader, "Failure because --> data is not coming fron Guava");


    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Guava when data is present in Guava & Redis and TD is MEAL level ")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campaign is not present the testRestaurantId");
//        Assert.assertEquals(resultHeader, testHeader);

//        TDCachingDbHelper.deleteRestaurantMap(testRestaurantId, campaignId);
        tdRedisHelper.setKeyValue(getKey(RESTAURANT_REDIS_KEY, testRestaurantId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId);

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campaign is not present for testRestaurantId");
//        Assert.assertEquals(headerInCart, testHeader);
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Redis when data is present in Redis & DB only and TD is MEAL level")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaButPresentInRedisAndDb(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");

        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campaign is not present for the testRestaurantId");
        Assert.assertEquals(resultHeader, testHeader,"Failure because --> test header is different");

        String replacedRestaurantId = testRestaurantId+"1";
        TDCachingDbHelper.updateCampaignRestaurantMap(replacedRestaurantId, campaignId);

        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because --> data is not coming from Redis");

        tdCachingHelper.checkRedisData(RESTAURANT_REDIS_KEY, campaignId, testRestaurantId);

        tdCachingHelper.evictKeyFromGuava();
        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> data is not coming from Redis");
        Assert.assertEquals(headerInCart, testHeader,"Failure because test header is different");

    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class , description="Test data is coming from Redis when data is not present in Guava & DB and TD is MEAL level ")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaAndDbButPresentInRedisOnly(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");


        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> data is not present for testRestaurantId");

//        TDCachingDbHelper.deleteRestaurantMap(testRestaurantId, campaignId);
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because --> campaign is not present for the testRestaurantId");
        tdCachingHelper.evictKeyFromGuava();
        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> campaign is not present the testRestaurantId ");
//        Assert.assertEquals(headerInCart, testHeader);

//        tdCachingHelper.checkRedisData(RESTAURANT_REDIS_KEY, restaurantId, campaignId);
    }

    @Test(dataProvider = "guavaAndRedisEnabledForCampaignData", dataProviderClass = TdCachingDP.class,  description="Test data is coming from DB when data is not present in redis and Guava and TD is Meal levelO")
    public void guavaAndRedisEnabledForRestaurantTestCreateMealLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava(HashMap<String,String> cartV3Payload, String testRestaurantId
            ,String mealId, String groupId) throws IOException {
        String testHeader = "caching test";
        String testUserId = getUserId();

        Map<String, String> map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");


        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String resultCampaignId = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String resultHeader = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(resultCampaignId, campaignId,"Failure because --> campaign is not present for the testRestaurantId");
//        Assert.assertEquals(resultHeader, testHeader);


        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);
        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        String resultHeaderListing = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");
//        Assert.assertEquals(resultHeaderListing, DATABASE);
        Assert.assertEquals(result, campaignId,"Failure because --> data is not coming from DB");

        tdCachingHelper.evictKeyFromGuava();
        tdRedisHelper.delete(getKey(RESTAURANT_REDIS_KEY, testRestaurantId));

        // cart evaluate
        cartV3Payload.put("4",testUserId);
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String cartResponse = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].campaignId");

        String headerInCart = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].tradeDiscountInfo[0].header");

        Assert.assertEquals(cartResponse, campaignId,"Failure because --> data is not coming from DB");
//        Assert.assertEquals(headerInCart, DATABASE);
    }

//     User map

    @Test(description="Test data is not come when data is not present in Guava, Redis and DB and TD is MEAL level with user mapping ")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();

        String discountAmount = "10";
        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because --> data is present in DB ");
    }

    @Test(description="Test data is not coming from Guava & Redis when data is only present in DB and TD is MEAL level with user restriction ")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);
        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", replaceUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because -->  campaign is not present for testRestaurantId in DB");
    }

    @Test(description="Test data is coming from Guava when data is present in Redis, Guava and DB and TD is MEAL level with user restriction ")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);


        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"Failure because --> Campaign is not present to the testRestaurantId");


        tdRedisHelper.setKeyValue(getKey(TD_USER_MAP_REDIS_KEY, testUserId), "");

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", replaceUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from Guava");
    }

    @Test(description="Test data is  coming from Guava when data is present in Guava and DB only ")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();

        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because --> Campaign is not present for testRestaurantID");


        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        tdRedisHelper.delete(getKey(TD_USER_MAP_REDIS_KEY, testUserId));

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", replaceUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because -->  data is not coming from Guava ");

    }

    @Test(description="Test data is coming from Guava when data is present in Guava and Redis and TS is MEAL level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId, "Failure because --> Campaign is not present for testRestaurantId");
        Assert.assertEquals(header, DATABASE,"Failure because --> test header is different");


        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);
        tdRedisHelper.setKeyValue(getKey(TD_USER_MAP_REDIS_KEY, testUserId), "");

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from Guava");
    }

    @Test(description="Test data is coming from Redis when data is present in Redis and DB and TD is Meal level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaButPresentInRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        TDCachingDbHelper.updateHeaderOfCampaign(DATABASE, campaignId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        String header = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].header");

        Assert.assertEquals(result, campaignId,"Failure because --> Campaign is not present for testRestaurantId");
        Assert.assertEquals(header, DATABASE,"Failure because --> testHeader is different");

        tdCachingHelper.evictKeyFromGuava();

        String replaceUserId = testUserId + "1";
        TDCachingDbHelper.updateCampaignUserMap(replaceUserId, campaignId);

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from Redis");
    }

    @Test(description="Test data is coming from Redis when data is only present in Redis and TD is MEAL level with user restriction " )
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromRedisIfDataIsNotPresentInGuavaAndDbButPresentInRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result, campaignId,"Failure because --> Campaign is not present for testRestaurantId");

        TDCachingDbHelper.deleteCampaignUserMap(campaignId, testUserId);
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from Redis");
    }

    @Test(description="Test data is coming from DB when data is not present in Redis & Guava and TD is MEAL level with user restriction")
    public void guavaAndRedisEnabledForUserMapTestCreateMealLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava() throws IOException {

        String testHeader = "caching test";
        String testRestaurantId = getRestaurantIds();
        String testUserId = getUserId();
        String discountAmount = "10";

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(testRestaurantId, testHeader,discountAmount);
        String campaignId = map.get("campaign_id");
        String restaurantId = map.get("restaurant_id");

        tdCachingHelper.createUserMapping(campaignId, testUserId);

        Processor listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");
        Assert.assertEquals(result, campaignId,"Campaign is not present for testRestaurantID");

        tdRedisHelper.delete(getKey(TD_USER_MAP_REDIS_KEY, testUserId));
        tdCachingHelper.evictKeyFromGuava();

        listingResponse = rngHelper.listEvalulate(testRestaurantId, "false", testUserId, "ANDROID", "299");

        String result1 = listingResponse.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo[0].campaignId");

        Assert.assertEquals(result1, campaignId,"Failure because --> data is not coming from DB");
    }


    // operational


    @Test(description="Test data is coming from DB only when data is not present in Guava & Redis")
    public void guavaAndRedisEnabledForOperationTestCreateMealLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, notDiscountAmount);
    }

    @Test(description="Test data is coming from guava when data is present in Guava, Redis and DB")
    public void guavaAndRedisEnabledForOperationTestCreateMealLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
//        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is not coming when data is not present in Guava, Redis and DB")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]","Failure because --> campaign is present in DB");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "0.0");
    }

    @Test(description="Test data is not coming from Guava and Redis when data is only present in Db")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldNotComeFromGuavaAndRedisIfDataIsPresentOnlyInDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
        tdRedisHelper.delete(getKey(TD_OPERATION_REDIS_KEY, campaignId));

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, notDiscountAmount);
    }

    @Test(description="Test data is coming from Guava when data is present in Guava, Redis and DB")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInAllGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

        TDCachingDbHelper.updateCampaignRuleMap(campaignId, dummyRuleId);
        tdRedisHelper.setKeyValue(getKey(TD_OPERATION_REDIS_KEY, campaignId), "");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is coming from Guava when data is present in Guava and DB only")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndDbOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

        TDCachingDbHelper.updateCampaignRuleMap(campaignId, dummyRuleId);
        tdRedisHelper.delete(getKey(TD_OPERATION_REDIS_KEY, campaignId));

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is coming from Guava when data is present in Guava & Redis ")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromGuavaIfDataIsPresentInGuavaAndRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

//        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
//        tdRedisHelper.setKeyValue(getKey(TD_OPERATION_REDIS_KEY, campaignId), "");

        //TODO
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is coming from Redis when data is present in Redis and DB")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaButPresentInRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId =  getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
//        tdRedisHelper.setKeyValue(getKey(TD_OPERATION_REDIS_KEY, campaignId), "");
        tdCachingHelper.evictKeyFromGuava();

        //TODO
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, notDiscountAmount );

    }

    @Test(description="Test data is not coming from Guava & DB when data is only present in Redis ")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromRedisIfDataIsNotPresentIfGuavaAndDbButPresentInRedisOnly() throws IOException {
        String testHeader = "caching test";
        String testUserId =   getUserId();
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

//        TDCachingDbHelper.deleteCampaignRuleMap(campaignId);
//        tdRedisHelper.setKeyValue(getKey(TD_OPERATION_REDIS_KEY, campaignId), "");
//        tdCachingHelper.evictKeyFromGuava();

        //TODO need to check with dev redis key is not setting
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is not coming from Guava & Redis when data is only present in DB")
    public void guavaAndRedisEnabledForOperationTestCreateRestaurantLevelTdDataShouldComeFromDbIfDataIsNotPresentInRedisAndGuava() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10.0";
        String notDiscountAmount = "0.0";
        String dummyRuleId = "0";

        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);

        tdRedisHelper.setKeyValue(getKey(TD_OPERATION_REDIS_KEY, campaignId), "");
        tdCachingHelper.evictKeyFromGuava();

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, discountAmount);
    }

    @Test(description="Test data is not coming when data is not present in Guava, Redis & DB")
    public void guavaAndRedisEnabledForOperationTestCreateLevelTdDataShouldNotComeIfDataIsNotPresentInGuavaRedisAndDb() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "10";
        String testRestaurantId = getRestaurantIds();

        Map<String, String> map = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(testRestaurantId, testHeader,discountAmount);

        String campaignId = map.get("campaign_id");

        TDCachingDbHelper.deleteCampaignRestaurantMapping(campaignId, testRestaurantId);

        String result = rngHelper.listEvalulate(testRestaurantId, "false", "9465", "ANDROID", "299")
                .ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo");

        Assert.assertEquals(result, "[]");

        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "0.0");

    }

// ================= Test cases for Caching of Rule pogo in Redis ====================

@Test (description="Data is coming from Redis if Data is present in redis And DB")
    public void redisEnabledForRuleTestCreateRestaurantTypeCampaignDataShouldComeFromRedisIfDataIsPresentInRedisAndDB() throws IOException {
    String testHeader = "caching test";
    String testUserId = "9465";
    String userAgent = "ANDROID";
    String versionCode = "299";
    String discountAmount = "100";
    String testRestaurantId = getRestaurantIds();
    String groupId = getRestaurantIds();
    String updatedRewardValue = "10";

    Map<String,String> createCamp= rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount( testRestaurantId,  testHeader,  discountAmount);
    String campaign_id= createCamp.get("campaign_id");
    tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "100.0");
    TDCachingDbHelper.updatCampaignRewarValue(updatedRewardValue, campaign_id);
    tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "100.0");

}

@Test (dataProvider = "redisEnabledForRulesTestData", dataProviderClass = TdCachingDP.class,description = "Data is Coming from redis if Data is present in Redis And DB")
    public void redisEnabledForRuleTestCreateMealTypeCampaignDataShouldComeFromRedisIfDataIsPresentInRedisAndDB(HashMap<String,String> cartV3Payload,String testRestaurantId, String mealId, String groupId){
    String testHeader = "caching test";
    String testUserId = "9465";
    String userAgent = "ANDROID";
    String versionCode = "299";
    String discountAmount = "100";

    String updatedRewardValue = "10";
    Map<String, String> map;
    map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
            groupId, "NONE","FinalPrice", "100","1");
    String campaignId = map.get("campaign_id");
    Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
    String rewardType = cartV3Response.ResponseValidator
            .GetNodeValueAsStringFromJsonArray("$.data.meals[0].rewardType");
    String resp = cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount");
    Assert.assertEquals(resp, "100.0");
    Assert.assertEquals(rewardType,"BXGY");
    //TDCachingDbHelper.updatCampaignRewarValue(updatedRewardValue, campaignId);
    //String  reward= new CreateReward( "NONE", "FinalPrice", "10", "1").toStringJson();
    TDCachingDbHelper. updateMealRewardValue( updatedRewardValue, campaignId );
    Processor cartV3 = edvoHelper.cartV3Evaluate(cartV3Payload);
    String rewardTypeInRedis = cartV3.ResponseValidator
            .GetNodeValueAsStringFromJsonArray("$.data.meals[0].rewardType");
    String cartDiscount = cartV3.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount");
    Assert.assertEquals(cartDiscount, "100.0");
    Assert.assertEquals(rewardTypeInRedis,"BXGY");
    }

    @Test (description="Data is Coming From DB If Data Is Present Only in DB")
    public void redisEnabledForRuleTestCreateRestaurantTypeCampaignDataShouldComeFromDBIfDataIsPresentOnlyInDB() throws IOException {
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "100";
        String testRestaurantId = getRestaurantIds();
        String groupId = getRestaurantIds();
        String updatedRewardValue = "10";

        Map<String,String> createCamp= rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount( testRestaurantId,  testHeader,  discountAmount);
        String campaign_id= createCamp.get("campaign_id");
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "100.0");
        TDCachingDbHelper.updatCampaignRewarValue(updatedRewardValue, campaign_id);
        List<String> ruleIds = tdCachingHelper.getRuleIdsByCampaignId(campaign_id);
        for(int i = 0; i < ruleIds.size(); i++) {
            System.out.print("rule ids-- >>" + ruleIds.get(i));
            tdRedisHelper.deleteRuleKeys(getKey(RULE_REDIS_KEY, ruleIds.get(i)));
        }
        tdCachingHelper.evaluateCart(testRestaurantId, testUserId, userAgent, versionCode, "10.0");

    }

    @Test (dataProvider = "redisEnabledForRulesTestData", dataProviderClass = TdCachingDP.class,description = "Data is Coming From DB if Data is present only in DB")
    public void redisEnabledForRuleTestCreateMealTypeCampaignDataShouldComeFromDBIfDataIsPresentOnlyInDB(HashMap<String,String> cartV3Payload,String testRestaurantId, String mealId, String groupId){
        String testHeader = "caching test";
        String testUserId = "9465";
        String userAgent = "ANDROID";
        String versionCode = "299";
        String discountAmount = "100";
        String updatedRewardValue = "10";
        Map<String, String> map;
        map = edvoHelper.createMealLevelCampaign( testHeader,false, false, false, "ZERO_DAYS_DORMANT", testRestaurantId, mealId,
                groupId, "NONE","FinalPrice", "100","1");
        String campaignId = map.get("campaign_id");
        Processor cartV3Response = edvoHelper.cartV3Evaluate(cartV3Payload);
        String rewardType = cartV3Response.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].rewardType");
        String resp = cartV3Response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount");
        Assert.assertEquals(resp, "100.0");
        Assert.assertEquals(rewardType,"BXGY");

        //String  reward= new CreateReward( "NONE", "FinalPrice", "10", "1").toStringJson();
        TDCachingDbHelper. updateMealRewardValue( updatedRewardValue, campaignId );
        List<String> ruleIds = tdCachingHelper.getRuleIdsByCampaignId(campaignId);
        for(int i = 0; i < ruleIds.size(); i++) {
            System.out.print("rule ids-- >>" + ruleIds.get(i));
            tdRedisHelper.deleteRuleKeys(getKey(RULE_REDIS_KEY, ruleIds.get(i)));
        }
        Processor cartV3 = edvoHelper.cartV3Evaluate(cartV3Payload);
        String rewardTypeInRedis = cartV3.ResponseValidator
                .GetNodeValueAsStringFromJsonArray("$.data.meals[0].rewardType");
        String cartDiscount = cartV3.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.cartTradeDiscount");
        Assert.assertEquals(cartDiscount, "10.0");
        Assert.assertEquals(rewardTypeInRedis,"BXGY");
    }

}
