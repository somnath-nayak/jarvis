package com.swiggy.api.sf.checkout.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import com.swiggy.api.sf.checkout.dp.EDVODP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.CartValidator;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOMealHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.OrderValidator;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getLastOrderResponse.GetLastOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getSingleOrderResponse.GetSingleOrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class EDVOTest extends EDVODP{
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
    EDVOMealHelper edvoMealHelper = new EDVOMealHelper();
    CartValidator cartValidator =new CartValidator();
    OrderValidator orderValidator = new OrderValidator();
    CheckoutHelper checkoutHelper = new CheckoutHelper();

    @BeforeClass
    public void createMeal(){
        MEAL_ID_2_SCREEN = edvoMealHelper.createMealAndReturnID(EDVOConstants.RESTAURANT_ID, EDVOConstants.SCREEN_COUNT_02);
        edvoMealHelper.twoScreenMeal = MEAL_ID_2_SCREEN;
        MEAL_ID_3_SCREEN = edvoMealHelper.createMealAndReturnID(EDVOConstants.RESTAURANT_ID, EDVOConstants.SCREEN_COUNT_03);
        edvoMealHelper.getMeal(MEAL_ID_2_SCREEN, EDVOConstants.RESTAURANT_ID);
        edvoMealHelper.getMeal(MEAL_ID_3_SCREEN, EDVOConstants.RESTAURANT_ID);
    }

    @Test(dataProvider = "updateCartForOneMealTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Create/Update TD then, create cart and verify discount")
    public void updateCartForOneMealTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                         boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                         String discountLevel, String discountType, String discount, String tdType){
        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.createEDVOCartForMeals(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response cartV2Response = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode, statusMessage,processor);
        cartValidator.validateMealCart(cartV2Response, tdType);
        cartValidator.checkItemsAddedCorrectly(processor, edvoCartHelper.getCartObject());
    }

    @Test(dataProvider = "getCartForOneMealTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Get cart and verify discount.")
    public void getCartTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                            boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                            String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor updateCartProcessor = edvoCartHelper.createEDVOCartForMeals(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        cartValidator.smokeCheck(statusCode, statusMessage,updateCartProcessor);
        Processor getCartProcessor = edvoCartHelper.getCart();
        CartV2Response getCartResponse = Utility.jsonDecode(getCartProcessor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode, statusMessage,getCartProcessor);
        cartValidator.validateMealCart(getCartResponse, tdType);
        cartValidator.checkItemsAddedCorrectly(getCartProcessor, edvoCartHelper.getCartObject());
    }

    @Test (dataProvider = "flushCartForOneMealTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Flush the cart.")
    public void flushCartTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                              boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                              String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor updateCartProcessor = edvoCartHelper.createEDVOCartForMeals(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response cartV2Response = Utility.jsonDecode(updateCartProcessor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(0, "CART_UPDATED_SUCCESSFULLY", updateCartProcessor);
        cartValidator.validateMealCart(cartV2Response, tdType);
        Processor flushCartProcessor = edvoCartHelper.flushCart(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        cartValidator.smokeCheck(0, "CART_DELETED_SUCCESSFULLY", flushCartProcessor);
        Assert.assertEquals(flushCartProcessor.ResponseValidator.GetNodeValue("$.data.phoneNo"), EDVOConstants.MOBILE);
    }

    @Test (dataProvider = "getCartMinimalTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Flush the cart")
    public void getCartMinimalTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                              boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                              String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor minimalCart = edvoCartHelper.getCartMinimal(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response minimalCartResponse = Utility.jsonDecode(minimalCart.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode, statusMessage, minimalCart);
        cartValidator.checkItemsAddedCorrectly(minimalCart, edvoCartHelper.getCartObject());
        cartValidator.validateMealCart(minimalCartResponse, tdType);
    }

    @Test (dataProvider = "getCheckTotalTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Create Check Total cart for non-logged in user")
    public void checkTotalTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                               boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                               String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getCheckTotalCart(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response checkTotalsResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        cartValidator.checkItemsAddedCorrectly(processor, edvoCartHelper.getCartObject());
        cartValidator.validateMealCart(checkTotalsResponse, tdType);
    }

    @Test (dataProvider = "getPlaceOrderTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Place Order")
    public void placeOrderTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                               boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                               String discountLevel, String discountType, String discount, String tdType){
        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getPlaceOrder(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        edvoCartHelper.cancelOrder(orderResponse.getData().getOrderId().toString());
        orderValidator.validateMealOrder(orderResponse, tdType);
    }

    @Test (dataProvider = "getEditOrderCheckTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Edit Order Check")
    public void editOrderCheckTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                   boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                   String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getEditOrderCheck(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response editOrderCheck = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode, statusMessage,processor);
        cartValidator.checkItemsAddedCorrectly(processor, edvoCartHelper.getCartObject());
        orderValidator.validateMealCart(editOrderCheck, tdType);
    }

    @Test (dataProvider = "getEditOrderConfirmTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Edit Order Confirm")
    public void editOrderConfirmTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                   boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                   String discountLevel, String discountType, String discount, String tdType) {

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getEditOrderConfirm(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);

        OrderResponse orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        edvoCartHelper.cancelOrder(orderResponse.getData().getOrderId().toString());
        orderValidator.checkAllItemsAddedFromCart(processor, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(orderResponse, tdType);
    }

    @Test (dataProvider = "getCancelOrderTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Cancel Order Check")
    public void cancelOrderTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                     boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                     String discountLevel, String discountType, String discount, String tdType) {

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getCancelOrder(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse cancelOrder = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        cartValidator.smokeCheck(statusCode, statusMessage,processor);
        orderValidator.checkAllItemsAddedFromCart(processor, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(cancelOrder, tdType);
    }

    @Test (dataProvider = "getCloneOrderCheckTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Clone Order Check")
    public void cloneOrderCheckTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                String discountLevel, String discountType, String discount, String tdType) {

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getCloneOrderCheck(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response orderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        cartValidator.smokeCheck(statusCode,statusMessage, processor );
        cartValidator.checkItemsAddedCorrectly(processor, edvoCartHelper.getCartObject());
        orderValidator.validateMealCart(orderResponse, tdType);
    }

    @Test (dataProvider = "getCloneOrderConfirmTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Clone Order Confirm")
    public void cloneOrderConfirmTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                      boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                      String discountLevel, String discountType, String discount, String tdType) {

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor processor = edvoCartHelper.getCloneOrderConfirm(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse cloneOrder = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        orderValidator.checkAllItemsAddedFromCart(processor, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(cloneOrder, tdType);
    }

    @Test(dataProvider = "getSingleOrderTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Get Single Order")
    public void getSingleOrderTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                      boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                      String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor placeOrder = edvoCartHelper.getPlaceOrder(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(placeOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        long expectedOrderId = orderResponse.getData().getOrderId();
        Processor processor = edvoCartHelper.singleOrder(String.valueOf(expectedOrderId));
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        GetSingleOrderResponse getSingleOrderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetSingleOrderResponse.class);
        long actualOrderId = getSingleOrderResponse.getData().getOrders().get(0).getOrderId();
        Assert.assertEquals(actualOrderId, expectedOrderId, "Actual Order Id does not match with expected Order Id");

        orderResponse = new OrderResponse();
        orderResponse.setStatusCode(getSingleOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getSingleOrderResponse.getStatusMessage());
        orderResponse.setTid(getSingleOrderResponse.getTid());
        orderResponse.setSid(getSingleOrderResponse.getSid());
        orderResponse.setDeviceId(getSingleOrderResponse.getDeviceId());
        orderResponse.setData(getSingleOrderResponse.getData().getOrders().get(0));

        orderValidator.checkAllItemsAddedFromCart(orderResponse, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(orderResponse, tdType);
    }

    @Test (dataProvider = "getAllOrderTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Get All Order")
    public void getAllOrderTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                   boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                   String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor placeOrder = edvoCartHelper.getPlaceOrder(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(placeOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        long expectedOrderId = orderResponse.getData().getOrderId();

        Processor processor = edvoCartHelper.getAllOrders();
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        GetSingleOrderResponse getSingleOrderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetSingleOrderResponse.class);
        orderResponse = new OrderResponse();
        orderResponse.setStatusCode(getSingleOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getSingleOrderResponse.getStatusMessage());
        orderResponse.setTid(getSingleOrderResponse.getTid());
        orderResponse.setSid(getSingleOrderResponse.getSid());
        orderResponse.setDeviceId(getSingleOrderResponse.getDeviceId());
        orderResponse.setData(getSingleOrderResponse.getData().getOrders().get(0));
        long actualOrderId = orderResponse.getData().getOrderId();

        Assert.assertEquals(expectedOrderId, actualOrderId, "Actual Order Id does not match with expected Order Id");
        orderValidator.checkAllItemsAddedFromCart(orderResponse, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(orderResponse, tdType);
    }

    @Test (dataProvider = "getLastOrderTestData", groups = {"shaswat", "smoke", "sanity", "regression"}, description = "Get Last Order")
    public void getLastOrderTest(String[] mealIds, String restaurantId, boolean isAddonRequired,
                                boolean isMealItemRequired, boolean isCartItemRequired, int statusCode, String statusMessage, String rewardType,
                                String discountLevel, String discountType, String discount, String tdType){

        edvoMealHelper.createOrUpdateMealTD(Integer.valueOf(restaurantId), Integer.valueOf(mealIds[0]), tdType);
        Processor placeOrder = edvoCartHelper.getPlaceOrder(mealIds, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(placeOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        long expectedOrderId = orderResponse.getData().getOrderId();

        Processor processor = edvoCartHelper.getLastOrder();
        cartValidator.smokeCheck(statusCode, statusMessage, processor);
        GetLastOrderResponse getLastOrderResponse = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetLastOrderResponse.class);
        orderResponse = new OrderResponse();
        orderResponse.setStatusCode(getLastOrderResponse.getStatusCode());
        orderResponse.setStatusMessage(getLastOrderResponse.getStatusMessage());
        orderResponse.setTid(getLastOrderResponse.getTid());
        orderResponse.setSid(getLastOrderResponse.getSid());
        orderResponse.setDeviceId(getLastOrderResponse.getDeviceId());
        orderResponse.setData(getLastOrderResponse.getData().getLastOrder());
        long actualOrderId = orderResponse.getData().getOrderId();

        Assert.assertEquals(expectedOrderId, actualOrderId, "Actual Order Id does not match with expected Order Id");
        orderValidator.checkAllItemsAddedFromCart(orderResponse, edvoCartHelper.getCartObject());
        orderValidator.validateMealOrder(orderResponse, tdType);
    }

    @AfterMethod
    public void cancelAllOrders(ITestResult iTestResult){
        if(!iTestResult.getMethod().getMethodName().equals("updateCartForOneMealTest")
                && !iTestResult.getMethod().getMethodName().equals("getCartTest")
                && !iTestResult.getMethod().getMethodName().equals("flushCartTest")
                && !iTestResult.getMethod().getMethodName().equals("getCartMinimalTest")
                && !iTestResult.getMethod().getMethodName().equals("checkTotalTest")) {
            edvoCartHelper.cancelAllOrders();
        }
        Utility.wait(EDVOConstants.WAIT_TIME);
    }


}
