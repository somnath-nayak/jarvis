package com.swiggy.api.sf.checkout.constants;

public interface CafeConstant {
    String CART_UPDATED_SUCCESSFULLY="CART_UPDATED_SUCCESSFULLY";
    String EXPECTED_CAFE_ORDER_TYPE="cafe";
    String EXPECTED_CAFE_RESTAURANT_TYPE= "MADETOORDER";
    String ORDER_REDEEMED_MESSAGE="Order redeemed successfully!";
    String AWAITING_CONFIRMATION_MESSAGE ="Awaiting confirmation from the counter";
    String AWAITING_MARKED_READY_MESSAGE="We will inform you once the order is marked ready by the counter";
    String AWAITING_REDEEM_MESSAGE="Redeem the above order token at the counter to pick up your food";
    String REDEEM_MESSAGE="Enjoy your food";
    String VERSION_CODE="554";
    String ORDER_ACTION_CODE="111";
    int DEFAULT_COUNT=3;
}