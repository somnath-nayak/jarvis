package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.helper.RngHelper;

import framework.gameofthrones.JonSnow.Processor;

public class CreateCampaign{
	int i =0;
    RngHelper rngHelper = new RngHelper();
	CreateTDDataCreation cc = new CreateTDDataCreation();
	public void createTradeDiscount(int CampaignCount) throws IOException {
		
		List<String> list = cc.CreateTD(CampaignCount);
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();/*;rngHelper.TDstatusChecker(response);
			 Assert.assertEquals(JsonPath.rssssead(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");*/
			 i++;
		}
		System.out.println(i+"============================");
	}


    public void createTradeDiscount(String CampaignType, int CampaignCount) throws IOException
    {
    	List<String> list = cc.CreateTD(CampaignType,CampaignCount);
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();/*rngHelper.TDstatusChecker(response);
			 Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");*/
		}	
    }

/*
    public void createTradeDiscount(List CampaignType,List Level,int CampaignCount) throws IOException
    {
    	List<String> list = cc.CreateTD(CampaignType,Level,CampaignCount);
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();rngHelper.TDstatusChecker(response);
			 Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
		}	
    }
    
    
    */
    public void createTradeDiscount(List CampaignType,List Level,int CampaignCount) throws IOException
    {
    	int i =0;
    	List<String> list = cc.CreateTD(CampaignType,Level,CampaignCount);
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();
			 /*rngHelper.TDstatusChecker(response);
			 *//*Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");*/
	      i++;
		}	
    System.out.println("total No of Campaign =" +i);
    }
    

    public void createTradeDiscount(List CampaignType,List Level,int CampaignCount,boolean isMultiTD) throws IOException
    {
    	List<String> list = cc.CreateTD(CampaignType,Level,CampaignCount,isMultiTD);
		Iterator it = list.iterator();
		while(it.hasNext())
		{
			Processor processor=rngHelper.createTD(it.next().toString());
			 String response =	processor.ResponseValidator.GetBodyAsText();rngHelper.TDstatusChecker(response);
			 Assert.assertEquals(JsonPath.read(response,"$.statusMessage").toString(), "done");
			 Assert.assertNotNull(JsonPath.read(response,"$.data").toString(), "td id is null");
		}	
    }


}
