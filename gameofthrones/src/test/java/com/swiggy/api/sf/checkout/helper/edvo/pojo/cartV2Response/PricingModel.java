package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "price",
        "variations",
        "addon_combinations"
})
public class PricingModel {

    @JsonProperty("price")
    private Integer price;
    @JsonProperty("variations")
    private List<Variation_> variations = null;
    @JsonProperty("addon_combinations")
    private List<AddonCombination> addonCombinations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     */
    public PricingModel() {
    }

    /**
     * @param price
     * @param addonCombinations
     * @param variations
     */
    public PricingModel(Integer price, List<Variation_> variations, List<AddonCombination> addonCombinations) {
        super();
        this.price = price;
        this.variations = variations;
        this.addonCombinations = addonCombinations;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("variations")
    public List<Variation_> getVariations() {
        return variations;
    }

    @JsonProperty("variations")
    public void setVariations(List<Variation_> variations) {
        this.variations = variations;
    }

    @JsonProperty("addon_combinations")
    public List<AddonCombination> getAddonCombinations() {
        return addonCombinations;
    }

    @JsonProperty("addon_combinations")
    public void setAddonCombinations(List<AddonCombination> addonCombinations) {
        this.addonCombinations = addonCombinations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("price", price).append("variations", variations).append("addonCombinations", addonCombinations).append("additionalProperties", additionalProperties).toString();
    }
}
