package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartItem {

	@JsonProperty("itemKey")
	private Object itemKey;
	@JsonProperty("menu_item_id")
	private Integer menuItemId;
	@JsonProperty("quantity")
	private Integer quantity;

	@JsonProperty("itemKey")
	public Object getItemKey() {
		return itemKey;
	}

	@JsonProperty("itemKey")
	public void setItemKey(Object itemKey) {
		this.itemKey = itemKey;
	}

	public CartItem withItemKey(Object itemKey) {
		this.itemKey = itemKey;
		return this;
	}

	@JsonProperty("menu_item_id")
	public Integer getMenuItemId() {
		return menuItemId;
	}

	@JsonProperty("menu_item_id")
	public void setMenuItemId(Integer menuItemId) {
		this.menuItemId = menuItemId;
	}

	public CartItem withMenuItemId(Integer menuItemId) {
		this.menuItemId = menuItemId;
		return this;
	}

	@JsonProperty("quantity")
	public Integer getQuantity() {
		return quantity;
	}

	@JsonProperty("quantity")
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public CartItem withQuantity(Integer quantity) {
		this.quantity = quantity;
		return this;
	}
}
