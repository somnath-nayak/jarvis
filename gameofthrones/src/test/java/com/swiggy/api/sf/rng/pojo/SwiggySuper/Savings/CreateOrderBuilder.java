package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateOrderBuilder {


    public CreateOrder createOrder;

    public CreateOrderBuilder() {
        createOrder = new CreateOrder();
    }


    public CreateOrderBuilder withOrderId(Integer orderId) {
        createOrder.setOrderId(orderId);
        return this;
    }

    public CreateOrderBuilder withCustomerId(String customerId) {
        createOrder.setCustomerId(customerId);
        return this;
    }

    public CreateOrderBuilder withOrderTime(String orderTime) {
        createOrder.setOrderTime(orderTime);
        return this;
    }

    public CreateOrderBuilder withTradeDiscountBreakup(List<TradeDiscountBreakup> tradeDiscountBreakup) {
        createOrder.setTradeDiscountBreakup(tradeDiscountBreakup);
        return this;
    }

    public CreateOrderBuilder withOrderType(String orderType) {
        createOrder.setOrderType(orderType);
        return this;
    }

    public CreateOrderBuilder withCouponCode(String couponCode) {
        createOrder.setCouponCode(couponCode);
        return this;
    }

    public CreateOrderBuilder withCouponDiscount(Integer couponDiscount) {
        createOrder.setCouponDiscount(couponDiscount);
        return this;
    }

    public CreateOrderBuilder withOrderTags(List<String> orderTags) {
        createOrder.setOrderTags(orderTags);
        return this;
    }

    public CreateOrder build() {
        getDefaultValue();
        return createOrder;
    }

    private void getDefaultValue() {
        if (createOrder.getOrderId() == null) {
            createOrder.setOrderId(1);
        }
        if (createOrder.getCustomerId() == null) {
            createOrder.setCustomerId("1");
        }
        if (createOrder.getOrderTime() == null) {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date dateobj = new Date();
            createOrder.setOrderTime(df.format(dateobj));
        }
        if (createOrder.getTradeDiscountBreakup() == null) {
            List<TradeDiscountBreakup> tdlist = new ArrayList<>();
            tdlist.add(new TradeDiscountBreakupBuilder().build());
            createOrder.setTradeDiscountBreakup(tdlist);
        }
        if (createOrder.getOrderType() == null) {
            createOrder.setOrderType("regular");
        }
        if (createOrder.getCouponCode() == null) {
            createOrder.setCouponCode("COUPON");
        }
        if (createOrder.getCouponDiscount() == null) {
            createOrder.setCouponDiscount(2);
        }
        if (createOrder.getOrderTags() == null) {
            ArrayList<String> arrayList = new ArrayList<>();
            arrayList.add("SUPER");
            createOrder.setOrderTags(arrayList);
        }
    }

}