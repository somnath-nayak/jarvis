package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "service_tax",
        "vat",
        "service_charges",
        "GST"
})
public class ItemTaxes {

    @JsonProperty("service_tax")
    private Integer serviceTax;
    @JsonProperty("vat")
    private Integer vat;
    @JsonProperty("service_charges")
    private Integer serviceCharges;
    @JsonProperty("GST")
    private Integer gST;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemTaxes() {
    }

    /**
     *
     * @param serviceCharges
     * @param serviceTax
     * @param vat
     * @param gST
     */
    public ItemTaxes(Integer serviceTax, Integer vat, Integer serviceCharges, Integer gST) {
        super();
        this.serviceTax = serviceTax;
        this.vat = vat;
        this.serviceCharges = serviceCharges;
        this.gST = gST;
    }

    @JsonProperty("service_tax")
    public Integer getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("service_tax")
    public void setServiceTax(Integer serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("vat")
    public Integer getVat() {
        return vat;
    }

    @JsonProperty("vat")
    public void setVat(Integer vat) {
        this.vat = vat;
    }

    @JsonProperty("service_charges")
    public Integer getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("service_charges")
    public void setServiceCharges(Integer serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    @JsonProperty("GST")
    public Integer getGST() {
        return gST;
    }

    @JsonProperty("GST")
    public void setGST(Integer gST) {
        this.gST = gST;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("serviceTax", serviceTax).append("vat", vat).append("serviceCharges", serviceCharges).append("gST", gST).append("additionalProperties", additionalProperties).toString();
    }

}