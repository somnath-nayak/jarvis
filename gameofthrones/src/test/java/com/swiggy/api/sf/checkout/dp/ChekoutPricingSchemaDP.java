package com.swiggy.api.sf.checkout.dp;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

public class ChekoutPricingSchemaDP {

    EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
    CheckoutHelper helper = new CheckoutHelper();
    SANDHelper sandHelper = new SANDHelper();
    static String restId;
    String[] latLog;
    static Cart cartPayload;

    @BeforeClass
    public void getRestData() {


        latLog=new String[]{"27.8374445","76.17257740000002","0"};

        String listingRes=helper.getRestList(latLog).ResponseValidator.GetBodyAsText();
        String[] restList=JsonPath.read(listingRes, "$.data..id").toString()
                .replace("[","").replace("]","").replace("\"","").split(",");
        String[] unServiceabilityList=JsonPath.read(listingRes, "$.data..unserviceable").toString()
                .replace("[","").replace("]","").replace("\"","").split(",");
        String[] isOpenedList=JsonPath.read(listingRes, "$.data..availability..opened").toString()
                .replace("[","").replace("]","").replace("\"","").split(",");

        for(int i=0;i<restList.length;i++){
            if (isOpenedList[i].equalsIgnoreCase("true") && unServiceabilityList[i].equalsIgnoreCase("false")) {
                restId=restList[i];
                String statuscode=cartValidation();
                if (statuscode.equals("0")){
                    break;}
            }
            if(i == restList.length - 1){
                Reporter.log("[ERROR -- No serviceable restaurant found in given lat-lng = '" + latLog[0] + "," + latLog[1] + "']", true);
                Assert.fail("[ERROR -- No serviceable restaurant found in given lat-lng = '" + latLog[0] + "," + latLog[1] + "']");
            }
        }
    }

    public String cartValidation(){
        String getStatusCode;
        try{
            cartPayload=eDVOCartHelper.getCartPayload1(null, restId, false, false, true);
            String response=helper.createCartCheckTotals(Utility.jsonEncode(cartPayload)).ResponseValidator.GetBodyAsText();
            getStatusCode = JsonPath.read(response, "$.statusCode").toString().replace("[", "").replace("]", "");
        }catch (Exception e){
            return "97";
        }
        return getStatusCode;
    }

    public boolean isRestPresent(String[] prodResList,String restId){
        for(int j=0;j<prodResList.length;j++){
            if(prodResList[j].equals(restId)){
                return true;
            }
        }
        return false;
    }

    @DataProvider(name = "FetchPricingSchemaValidator")
    public static Object[][] FetchPricingValidatorData() {
        return new Object[][]{
                {"Swiggy-Android", "300","1",restId,"4","0","fetchPricingAndroid.txt"},
                {"Swiggy-iOS", "300","1",restId,"4","0","fetchPricingIos.txt"},
                {"Web", "300","1",restId,"4","0","fetchPricingWeb.txt"}

        };
    }

    @DataProvider(name = "thresholdFee")
    public static Object[][] thresholdFee() {
        return new Object[][]{
                //Bangalore

                {"Swiggy-Android", "300", restId,"0000","2300","11.0","0","0","5000","12.0","thresholdFeeUpdateAndroid.txt"},
                {"Swiggy-Ios", "300", restId,"0000","2300","11.0","0","0","5000","12.0","thresholdFeeUpdateIos.txt"},
                {"Web", "300", restId,"0000","2300","11.0","0","0","5000","12.0","thresholdFeeUpdateWeb.txt"}



        };
    }

    @DataProvider(name = "distanceFee")
    public static Object[][] distanceFee() {
        return new Object[][]{
                {"Swiggy-Android", "300",restId,"0000","2359","0","0","1000","41.0","distanceFeeUpdateAndroid.txt"},
                {"Swiggy-Ios", "300",restId,"0000","2359","0","0","1000","41.0","distanceFeeUpdateIos.txt"},
                {"Web", "300",restId,"0000","2359","0","0","1000","41.0","distanceFeeUpdateWeb.txt"},

        };
    }



}



