package com.swiggy.api.sf.checkout.helper.edvo.pojo.cmsAttributes;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "portionSize",
        "spiceLevel",
        "vegClassifier",
        "accompaniments"
})
public class Attributes {

    @JsonProperty("portionSize")
    private String portionSize;
    @JsonProperty("spiceLevel")
    private String spiceLevel;
    @JsonProperty("vegClassifier")
    private String vegClassifier;
    @JsonProperty("accompaniments")
    private String accompaniments;

    /**
     * No args constructor for use in serialization
     *
     */
    public Attributes() {
    }

    /**
     *
     * @param portionSize
     * @param accompaniments
     * @param vegClassifier
     * @param spiceLevel
     */
    public Attributes(String portionSize, String spiceLevel, String vegClassifier, String accompaniments) {
        super();
        this.portionSize = portionSize;
        this.spiceLevel = spiceLevel;
        this.vegClassifier = vegClassifier;
        this.accompaniments = accompaniments;
    }

    @JsonProperty("portionSize")
    public String getPortionSize() {
        return portionSize;
    }

    @JsonProperty("portionSize")
    public void setPortionSize(String portionSize) {
        this.portionSize = portionSize;
    }

    @JsonProperty("spiceLevel")
    public String getSpiceLevel() {
        return spiceLevel;
    }

    @JsonProperty("spiceLevel")
    public void setSpiceLevel(String spiceLevel) {
        this.spiceLevel = spiceLevel;
    }

    @JsonProperty("vegClassifier")
    public String getVegClassifier() {
        return vegClassifier;
    }

    @JsonProperty("vegClassifier")
    public void setVegClassifier(String vegClassifier) {
        this.vegClassifier = vegClassifier;
    }

    @JsonProperty("accompaniments")
    public String getAccompaniments() {
        return accompaniments;
    }

    @JsonProperty("accompaniments")
    public void setAccompaniments(String accompaniments) {
        this.accompaniments = accompaniments;
    }

}