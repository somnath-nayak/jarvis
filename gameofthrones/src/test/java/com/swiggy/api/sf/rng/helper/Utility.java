package com.swiggy.api.sf.rng.helper;

import edu.emory.mathcs.backport.java.util.Collections;
import org.apache.commons.lang.RandomStringUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Utility {

	public static String getRandomFromList(List restIds) {
		Random rand = new Random();
		return String.valueOf(restIds.get(rand.nextInt(restIds.size())));
	}

	public static int getRandom(int l, int h) {
		Random rand = new Random();
		return rand.nextInt((h - l) + 1) + l;
	}

	public static List getRandomElementsFromList(List restIds, int restIdsRange) {
		Collections.shuffle(restIds);
		return restIds.subList(0, restIdsRange);
	}

	public static String getCurrrentTime() {
		LocalDateTime current = LocalDateTime.now();

		DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
		String formatted = current.format(formatter);
		return formatted;
	}

	public static String convertDateToString(Date date) {
		LocalDateTime current = LocalDateTime.now();

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return formatter.format(date);
	}

	public static String getPresentTimeInMilliSeconds() {
		Calendar calendar = Calendar.getInstance();
		long time = calendar.getTimeInMillis();
		return Long.toString(time);
	}

	public static String getFuturetimeInMilliSeconds(int a) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, a);
		calendar.getTimeInMillis();
		return Long.toString(calendar.getTimeInMillis());
	}

	public static String getPasttimeInMilliSeconds(int a) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.MILLISECOND, -a);
		calendar.getTimeInMillis();
		return Long.toString(calendar.getTimeInMillis());
	}

	public static String getPastMonthDate(int a) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -a);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		// System.out.println(date);
		//calendar.getTimeInMillis();
		//return Long.toString(calendar.getTimeInMillis());

		return date;
	}

	public static String getFutureMonthInMillisecond(int a) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, a);
		// calendar.getTimeInMillis();
		return Long.toString(calendar.getTimeInMillis());
	}

	public static String getPastTime(int month) {
		Calendar calendar = Calendar.getInstance();

		System.out.println("month " + month);
		System.out.println("Curr Date " + new Date());

		calendar.setTime(new Date());
		calendar.add(Calendar.MILLISECOND, -30000);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		System.out.println("Check date for past---->>>  " + date);
		return date;
	}

	public static String getPastTimeForPlan(int month) {
		Calendar calendar = Calendar.getInstance();

		System.out.println("month " + month);
		System.out.println("Curr Date " + new Date());

		calendar.setTime(new Date());
		calendar.add(Calendar.MILLISECOND, month);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		System.out.println("Check date for past---->>>  " + date);
		return date;
	}

	public static String getFutureMonthDate(int month) {
		Calendar calendar = Calendar.getInstance();

		System.out.println("month " + month);
		System.out.println("Curr Date " + new Date());

		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, month);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		// System.out.println(date);
		return date;
	}


	public static String getDateInToMilliseconds(String validDate) throws ParseException {
		String myDate = validDate;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		Date date = sdf.parse(myDate);
		long millis = date.getTime();

		return Long.toString(millis);
	}

	public static Date getDatePlusDays(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, days);
		return calendar.getTime();
	}

	public static String getCustomDaysInDateFormat(int days) {
		Calendar calendar = Calendar.getInstance();

		System.out.println("days--> " + days);
		System.out.println("Curr Date " + new Date());

		calendar.setTime(new Date());
		calendar.add(Calendar.DAY_OF_WEEK, days);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		// System.out.println(date);
		return date;
	}

	public static String getPresentDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MILLISECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String date = sdf.format(calendar.getTime());
		System.out.println(date);
		return date;
	}

	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String getFutureDate() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}
    
    public static String pastTimeDateAndGetDateInGivenFormat(int milliseconds,String format) {
    
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MILLISECOND, -milliseconds);
        SimpleDateFormat sdf = new SimpleDateFormat(format);
//        sdf.setTimeZone(TimeZone.getTimeZone("IST"));
        String date = sdf.format(calendar.getTime());
        System.out.println(date);
        return date;
    }
    public static String getCurrentDateInGivenFormat(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        Date date = new Date();
        return dateFormat.format(date);
    }
    public static String getFutureDateInGivenFormat(int day, String format) {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, day);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(format);
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        return dateFormat.format(tomorrow);
    }
	
	public static String getPreviousDate() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, -33);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}


	public static String getYesterDayDate() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, -1);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}

	public static  String getFutureDateThreeMonth() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.MONTH, 3);
		Date futureMonth = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(futureMonth);
	}




	public static String getRandomCodePrefix() {
		int codeLength = 4;
		// TODO Auto-generated method stub
		RandomStringUtils.randomAlphabetic(codeLength);
		return RandomStringUtils.randomAlphabetic(codeLength);
	}
	public  static String getRandomPostfix() {
		int codeLength = 6;
		// TODO Auto-generated method stub
		return RandomStringUtils.randomAlphanumeric(codeLength).toUpperCase();
	}

	public static String getDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MILLISECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = sdf.format(calendar.getTime());
		System.out.println(date);
		return date;
	}

	public static String getDate(String format) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MILLISECOND, 0);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(calendar.getTime());
		System.out.println(date);
		return date;
	}

	public static String getFutureDate(int daysFromToday, String pattern){
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_YEAR, daysFromToday);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}
	
	//------------------- Daily ---------------------------------
    public static String getCurrentDateInHHMMDD() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    
    public static String addMinutesCurrentDateInHHMMDD(long minutes) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        return dateFormat.format(date);
    }
    
    public static String getCurrentDateInHHMM() {
       // SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("HHmm");
        System.out.println("Date --->>  " + dateFormat.format(calendar.getTime()));
        return dateFormat.format(calendar.getTime());
    }
    
    public static String addMinutesToTheGivenHHMM(String hhmm, int interval) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
        Calendar calendar = Calendar.getInstance();
        while (hhmm.length() < 4) {
            hhmm = "0" + hhmm;
        }
        String mm = hhmm.substring(2,4);
        String hrs = hhmm.substring(0, 2);
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(hrs));
        calendar.set(Calendar.MINUTE, Integer.valueOf(mm));
        calendar.add(Calendar.MINUTE, interval);
        String data = sdf.format((calendar.getTime().getTime()));
        if(data.length()==2){
            return "00"+ data;
        }
        return sdf.format((calendar.getTime().getTime()));
    }
    
}

