package com.swiggy.api.sf.snd.pojo;


public class Data {
    private String menu_id;

    private String sub_category_desc;

    private String eligible_for_long_distance;

    private String in_stock;

    private String is_veg;

    private String service_charges;

    private String vat;

    private String recommended;

    private String service_tax;

    private String type;

    private String s3_image_url;

    private Meta meta;

    private String version;

    private String event_timestamp;

    private String serves_how_many;

    private String timestamp;

    private String id;

    private String sub_category_order;

    private String order;

    private String addon_limit;

    private String description;

    private Item_suggestion[] item_suggestion;

    private String name;

    private Variants variants;

    private String crop_choices;

    private String sub_category;

    private String packing_slab_count;

    private Catalog_attributes catalog_attributes;

    private String enabled;

    private String is_perishable;

    private String third_party_category_name;

    private String third_party_sub_category_name;

    private String addon_free_limit;

    private String restaurant_id;

    private String category_id;

    private String image_id;

    private String third_party_id;

    private String category;

    private String price;

    private String sub_category_id;

    private String is_spicy;

    private String category_order;

    private String is_discoverable;

    private String preparation_style;

    private String category_desc;

    public String getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(String menu_id) {
        this.menu_id = menu_id;
    }

    public String getSub_category_desc() {
        return sub_category_desc;
    }

    public void setSub_category_desc(String sub_category_desc) {
        this.sub_category_desc = sub_category_desc;
    }

    public String getEligible_for_long_distance() {
        return eligible_for_long_distance;
    }

    public void setEligible_for_long_distance(String eligible_for_long_distance) {
        this.eligible_for_long_distance = eligible_for_long_distance;
    }

    public String getIn_stock() {
        return in_stock;
    }

    public void setIn_stock(String in_stock) {
        this.in_stock = in_stock;
    }

    public String getIs_veg() {
        return is_veg;
    }

    public void setIs_veg(String is_veg) {
        this.is_veg = is_veg;
    }

    public String getService_charges() {
        return service_charges;
    }

    public void setService_charges(String service_charges) {
        this.service_charges = service_charges;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getRecommended() {
        return recommended;
    }

    public void setRecommended(String recommended) {
        this.recommended = recommended;
    }

    public String getService_tax() {
        return service_tax;
    }

    public void setService_tax(String service_tax) {
        this.service_tax = service_tax;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getS3_image_url() {
        return s3_image_url;
    }

    public void setS3_image_url(String s3_image_url) {
        this.s3_image_url = s3_image_url;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getEvent_timestamp() {
        return event_timestamp;
    }

    public void setEvent_timestamp(String event_timestamp) {
        this.event_timestamp = event_timestamp;
    }

    public String getServes_how_many() {
        return serves_how_many;
    }

    public void setServes_how_many(String serves_how_many) {
        this.serves_how_many = serves_how_many;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSub_category_order() {
        return sub_category_order;
    }

    public void setSub_category_order(String sub_category_order) {
        this.sub_category_order = sub_category_order;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getAddon_limit() {
        return addon_limit;
    }

    public void setAddon_limit(String addon_limit) {
        this.addon_limit = addon_limit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item_suggestion[] getItem_suggestion() {
        return item_suggestion;
    }

    public void setItem_suggestion(Item_suggestion[] item_suggestion) {
        this.item_suggestion = item_suggestion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Variants getVariants() {
        return variants;
    }

    public void setVariants(Variants variants) {
        this.variants = variants;
    }

    public String getCrop_choices() {
        return crop_choices;
    }

    public void setCrop_choices(String crop_choices) {
        this.crop_choices = crop_choices;
    }

    public String getSub_category() {
        return sub_category;
    }

    public void setSub_category(String sub_category) {
        this.sub_category = sub_category;
    }

    public String getPacking_slab_count() {
        return packing_slab_count;
    }

    public void setPacking_slab_count(String packing_slab_count) {
        this.packing_slab_count = packing_slab_count;
    }

    public Catalog_attributes getCatalog_attributes() {
        return catalog_attributes;
    }

    public void setCatalog_attributes(Catalog_attributes catalog_attributes) {
        this.catalog_attributes = catalog_attributes;
    }

    public String getEnabled() {
        return enabled;
    }

    public void setEnabled(String enabled) {
        this.enabled = enabled;
    }

    public String getIs_perishable() {
        return is_perishable;
    }

    public void setIs_perishable(String is_perishable) {
        this.is_perishable = is_perishable;
    }

    public String getThird_party_category_name() {
        return third_party_category_name;
    }

    public void setThird_party_category_name(String third_party_category_name) {
        this.third_party_category_name = third_party_category_name;
    }

    public String getThird_party_sub_category_name() {
        return third_party_sub_category_name;
    }

    public void setThird_party_sub_category_name(String third_party_sub_category_name) {
        this.third_party_sub_category_name = third_party_sub_category_name;
    }

    public String getAddon_free_limit() {
        return addon_free_limit;
    }

    public void setAddon_free_limit(String addon_free_limit) {
        this.addon_free_limit = addon_free_limit;
    }

    public String getRestaurant_id() {
        return restaurant_id;
    }

    public void setRestaurant_id(String restaurant_id) {
        this.restaurant_id = restaurant_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getThird_party_id() {
        return third_party_id;
    }

    public void setThird_party_id(String third_party_id) {
        this.third_party_id = third_party_id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getIs_spicy() {
        return is_spicy;
    }

    public void setIs_spicy(String is_spicy) {
        this.is_spicy = is_spicy;
    }

    public String getCategory_order() {
        return category_order;
    }

    public void setCategory_order(String category_order) {
        this.category_order = category_order;
    }

    public String getIs_discoverable() {
        return is_discoverable;
    }

    public void setIs_discoverable(String is_discoverable) {
        this.is_discoverable = is_discoverable;
    }

    public String getPreparation_style() {
        return preparation_style;
    }

    public void setPreparation_style(String preparation_style) {
        this.preparation_style = preparation_style;
    }

    public String getCategory_desc() {
        return category_desc;
    }

    public void setCategory_desc(String category_desc) {
        this.category_desc = category_desc;
    }

    public Data build() {
        setDefaultValues();
        return this;
    }

    public void setDefaultValues() {
        Meta meta = new Meta();
        meta.build();
        Item_suggestion item_suggestion = new Item_suggestion();
        item_suggestion.build();
        Variants variants = new Variants();
        variants.build();
        Catalog_attributes catalog_attributes = new Catalog_attributes();
        catalog_attributes.build();

        if (this.getMenu_id() == null) {
            this.setMenu_id("3245");
        }
        if (this.getSub_category_desc() == null) {
            this.setSub_category_desc("Anjappar - Starters - Non-Veg");
        }

        if (this.getEligible_for_long_distance() == null) {
            this.setEligible_for_long_distance("1");
        }
        if (this.getIn_stock() == null) {
            this.setIn_stock("1");
        }

        if (this.getIs_veg() == null) {
            this.setIs_veg("0");
        }

        if (this.getService_charges() == null) {
            this.setService_charges("0");
        }

        if (this.getVat() == null) {
            this.setVat("0");
        }
        if (this.getRecommended() == null) {
            this.setRecommended("true");

        }
        if (this.getService_tax() == null) {
            this.setService_tax("0");
        }

        if (this.getType() == null) {
            this.setType("REGULAR_ITEM");
        }

        if (this.getS3_image_url() == null) {
            this.setS3_image_url("https://swiggy-rest-item-image-upload.s3.amazonaws.com/item_image/cfd471cc-6c45-11e7-b791-06b53d688da3Wednesday_19_July_2017__11_17id132335.JPG");
        }

        if (this.getVersion() == null) {
            this.setVersion("2607");
        }

        if (this.getEvent_timestamp() == null) {
            this.setEvent_timestamp("1535222900146");
        }
        if (this.getServes_how_many() == null) {
            this.setServes_how_many("1");
        }

        if (this.getTimestamp() == null) {
            this.setTimestamp("1535222900148");
        }

        if (this.getId() == null) {
            this.setId("132335");
        }

        if (this.getSub_category_order() == null) {
            this.setSub_category_order("0");
        }

        if (this.getOrder() == null) {
            this.setOrder("0");
        }

        if (this.getAddon_limit() == null) {
            this.setAddon_limit("-1");
        }

        if (this.getDescription() == null) {
            this.setDescription("");
        }

        if (this.getName() == null) {
            this.setName("Tandoori Chicken");
        }

        if (this.getCrop_choices() == null) {
            this.setCrop_choices("-1");
        }

        if (this.getSub_category() == null) {
            this.setSub_category("Non-Veg");
        }

        if (this.getPacking_slab_count() == null) {
            this.setPacking_slab_count("1");
        }

        if (this.getEnabled() == null) {
            this.setEnabled("1");
        }

        if (this.getIs_perishable() == null) {
            this.setIs_perishable("0");
        }

        if (this.getThird_party_category_name() == null) {
            this.setThird_party_category_name("MAIN COURSE");
        }

        if (this.getThird_party_sub_category_name() == null) {
            this.setThird_party_sub_category_name("INDIAN GRAVIES");
        }

        if (this.getAddon_free_limit() == null) {
            this.setAddon_free_limit("-1");
        }

        if (this.getRestaurant_id() == null) {
            this.setRestaurant_id("219");
        }
        if (this.getCategory_id() == null) {
            this.setCategory_id("9174");
        }

        if (this.getImage_id() == null) {
            this.setImage_id("c9esrkv93cefrwew9mpt");
        }

        if (this.getThird_party_id() == null) {
            this.setThird_party_id("");
        }

        if (this.getCategory() == null) {
            this.setCategory("");
        }

        if (this.getPrice() == null) {
            this.setPrice("301.0");
        }

        if (this.getSub_category() == null) {
            this.setSub_category("Starters");
        }

        if (this.getIs_spicy() == null) {
            this.setIs_spicy("0");
        }

        if (this.getCategory_order() == null) {
            this.setCategory_order("6");
        }
        if (this.getIs_discoverable() == null) {
            this.setIs_discoverable("true");
        }
        if (this.getPreparation_style() == null) {
            this.setPreparation_style("");
        }
        if (this.getCategory_desc() == null) {
            this.setCategory_desc("Anjappar - Starters");
        }
    }

    @Override
    public String toString() {
        return " [enabled = " + enabled + ", type = " + type + ", name = " + name + ", is_veg = " + is_veg + ", serves_how_many = " + serves_how_many + ",third_party_category_name = " + third_party_category_name + ",in_stock = " + in_stock + ",packing_slab_count = " + packing_slab_count + ", event_timestamp = " + event_timestamp + ", price = " + price + ",category_order = " + category_order + ",sub_category_order = " + sub_category_order + ",vat = " + vat + ", menu_id = " + menu_id + ",description = " + description + ",image_id = " + image_id + ",preparation_style = " + preparation_style + ", is_perishable = " + is_perishable + ",third_party_sub_category_name = " + third_party_sub_category_name + ",variants = " + variants + ",eligible_for_long_distance = " + eligible_for_long_distance + ",s3_image_url = " + s3_image_url + ", sub_category_desc = " + sub_category_desc + ", category_desc = " + category_desc + " ,third_party_id = " + third_party_id + ",  service_charges = " + service_charges + ",version = " + version + ",meta = " + meta + ",id = " + id + ",timestamp = " + timestamp + ",catalog_attributes = " + catalog_attributes + ",recommended = " + recommended + ",category = " + category + " ,restaurant_id = " + restaurant_id + ",crop_choices = " + crop_choices + ",order = " + order + ",sub_category_id = " + sub_category_id + ",item_suggestion = " + item_suggestion + ",is_discoverable = " + is_discoverable + ", category_id = " + category_id + ",addon_free_limit = " + addon_free_limit + ",sub_category = " + sub_category + ",service_tax = " + service_tax + ",addon_limit = " + addon_limit + ",is_spicy = " + is_spicy + "]";
    }
}
