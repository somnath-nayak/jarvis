package com.swiggy.api.sf.rng.constants;

public interface SuperConstants {

	String successCheck="success";
	String cancelledCheck="[\"CANCELLED\"]";
	String expiredCheck="[\"EXPIRED\"]";
	String successMessgae="[\"SUCCESS\"]";
	String planIdInValid="Invalid plan id or user mapping doesn't exist";
	String expiredPlan="Plan expired";
	String invalidPlanValidity="Invalid plan validity";
	String planUsageLimitReached="Plan usage limit reached";
	String benefitIsNotMappedWithPlan="Invalid plan configuration, benefits are not mapped with plan";
	String planUserMappingMissing="Invalid plan id or user mapping doesn't exist";
	String userInvalid="userId is Invalid";
	String planIdInvalid="planId is Invalid";
	String orderidInvalid="orderId is Invalid";
	int statusCode208=208;
	int statusCode205=205;
	int statusCode210=210;
	int status218=218;
	int subDuplicateStatusCode=0;
	String userCantRenewPlan="User can't renew the plan";
	String subsYetToStart="[\"YET_TO_START\"]";
	String invalidOrderCancelRequest="Invalid order cancel request";
	int statusCode202=202;
	String noSubsFound="No subscription found for user";
	String invalidTitle="title is Invalid";
	String invalidDescription="description is Invalid";
	String invalidName="name is Invalid";
	String tenureInvalid="tenure is Invalid";
	String priceInvalid="price is Invalid";
	String invalidRenewalOffset="Invalid plan configuration, renewal days can't be greater than 27 days";
	int statusCode216=216;
	String createdByInvalid="createdBy is Invalid";
	String totalAvailableInvalid="totalAvailable is Invalid";
	String expiryOffsetInvalid="expiryOffsetDays is Invalid";
	String statusCode204="204";
	String planNotExist="Plan doesn't exist";
	String logoIdInvalid="logoId is Invalid";

	String restaurant = "RESTAURANT";
	int commissionLevy=0;
	String percentage="Percentage";
	int percentagevalue=100;
	String delivered = "delivered";
}
