package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class SearchRequest {

    private String entityType;
    private Filters filters;
    private Integer maxDocs;

    /**
     * No args constructor for use in serialization
     *
     */
    public SearchRequest() {
    }

    /**
     *
     * @param maxDocs
     * @param entityType
     * @param filters
     */
    public SearchRequest(String entityType, Filters filters, Integer maxDocs) {
        super();
        this.entityType = entityType;
        this.filters = filters;
        this.maxDocs = maxDocs;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Filters getFilters() {
        return filters;
    }

    public void setFilters(Filters filters) {
        this.filters = filters;
    }

    public Integer getMaxDocs() {
        return maxDocs;
    }

    public void setMaxDocs(Integer maxDocs) {
        this.maxDocs = maxDocs;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("entityType", entityType).append("filters", filters).append("maxDocs", maxDocs).toString();
    }

}
