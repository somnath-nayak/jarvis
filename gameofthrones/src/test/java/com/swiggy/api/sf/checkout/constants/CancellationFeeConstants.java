package com.swiggy.api.sf.checkout.constants;

public interface CancellationFeeConstants {
    String RESTAURANT_ID = "223";
    int STATUS_CODE = 0;
    String STATUS_MESSAGE_ORDER_CANCEL_CHECK = "Done Successfully";
    String STATUS_MESSAGE_ORDER_CANCEL = "done successfully";

    //Order Cancellation Details
    String REASON = "Test Order";
    String CANCELLATION_FEE_APPLICABILITY = "true";
    String CANCELLATION_FEE = "70";

}
