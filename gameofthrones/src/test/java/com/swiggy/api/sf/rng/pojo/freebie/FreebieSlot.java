package com.swiggy.api.sf.rng.pojo.freebie;

import org.apache.commons.lang.builder.ToStringBuilder;

public class FreebieSlot {

	private String closeTime;
	private String day;
	private String openTime;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public FreebieSlot() {
	}

	/**
	 * 
	 * @param closeTime
	 * @param openTime
	 * @param day
	 */
	public FreebieSlot(String closeTime, String day, String openTime) {
		super();
		this.closeTime = closeTime;
		this.day = day;
		this.openTime = openTime;
	}

	public String getCloseTime() {
		return closeTime;
	}

	public void setCloseTime(String closeTime) {
		this.closeTime = closeTime;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getOpenTime() {
		return openTime;
	}

	public void setOpenTime(String openTime) {
		this.openTime = openTime;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("closeTime", closeTime).append("day", day).append("openTime", openTime)
				.toString();
	}

}
