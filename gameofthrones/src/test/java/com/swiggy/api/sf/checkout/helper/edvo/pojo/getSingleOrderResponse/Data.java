package com.swiggy.api.sf.checkout.helper.edvo.pojo.getSingleOrderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "customer_care_number",
        "orders",
        "is_first_order_delivered"
})
public class Data {

    @JsonProperty("customer_care_number")
    private Object customerCareNumber;
    @JsonProperty("orders")
    private List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> orders = null;
    @JsonProperty("is_first_order_delivered")
    private Integer isFirstOrderDelivered;

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    /**
     *
     * @param orders
     * @param customerCareNumber
     * @param isFirstOrderDelivered
     */
    public Data(Object customerCareNumber, List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> orders, Integer isFirstOrderDelivered) {
        super();
        this.customerCareNumber = customerCareNumber;
        this.orders = orders;
        this.isFirstOrderDelivered = isFirstOrderDelivered;
    }

    @JsonProperty("customer_care_number")
    public Object getCustomerCareNumber() {
        return customerCareNumber;
    }

    @JsonProperty("customer_care_number")
    public void setCustomerCareNumber(Object customerCareNumber) {
        this.customerCareNumber = customerCareNumber;
    }

    @JsonProperty("orders")
    public List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> orders) {
        this.orders = orders;
    }

    @JsonProperty("is_first_order_delivered")
    public Integer getIsFirstOrderDelivered() {
        return isFirstOrderDelivered;
    }

    @JsonProperty("is_first_order_delivered")
    public void setIsFirstOrderDelivered(Integer isFirstOrderDelivered) {
        this.isFirstOrderDelivered = isFirstOrderDelivered;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("customerCareNumber", customerCareNumber).append("orders", orders).append("isFirstOrderDelivered", isFirstOrderDelivered).toString();
    }

}