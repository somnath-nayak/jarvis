package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

public class SubscriptionItemBuilder {
    private SubscriptionItem subscriptionItem;

    public SubscriptionItemBuilder(){
    	subscriptionItem = new SubscriptionItem();
    }

    public SubscriptionItemBuilder plan_id(Integer plan_id){
    	subscriptionItem.setplan_id(plan_id);
        return this;
    }

    public SubscriptionItemBuilder quantity(Integer quantity){
    	subscriptionItem.setQuantity(quantity);
        return this;
    }
    
    public SubscriptionItem build(){
        return subscriptionItem;
    }
}
