package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

public class ItemSuggestion {

    @JsonProperty("item_id")
    private Integer itemId;
    private Double score;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Double getScore() {
        return score;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("item_id", itemId).append("score", score).toString();
    }

}