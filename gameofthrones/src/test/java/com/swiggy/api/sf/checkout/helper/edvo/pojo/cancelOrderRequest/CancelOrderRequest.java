package com.swiggy.api.sf.checkout.helper.edvo.pojo.cancelOrderRequest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "cancellation_reason",
        "cancellation_fee_applicability",
        "cancellation_fee",
        "cancellation_reason_text"
})
public class CancelOrderRequest {

    @JsonProperty("cancellation_reason")
    private String cancellationReason;
    @JsonProperty("cancellation_fee_applicability")
    private boolean cancellationFeeApplicability;
    @JsonProperty("cancellation_fee")
    private float cancellationFee;
    @JsonProperty("cancellation_reason_text")
    private String cancellationReasonText;

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public boolean isCancellationFeeApplicability() {
        return cancellationFeeApplicability;
    }

    public void setCancellationFeeApplicability(boolean cancellationFeeApplicability) {
        this.cancellationFeeApplicability = cancellationFeeApplicability;
    }

    public float getCancellationFee() {
        return cancellationFee;
    }

    public void setCancellationFee(float cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    public String getCancellationReasonText() {
        return cancellationReasonText;
    }

    public void setCancellationReasonText(String cancellationReasonText) {
        this.cancellationReasonText = cancellationReasonText;
    }
}