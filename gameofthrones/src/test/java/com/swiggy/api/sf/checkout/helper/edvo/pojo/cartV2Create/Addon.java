package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "choice_id",
        "group_id",
        "name",
        "price"
})
public class Addon {

    @JsonProperty("choice_id")
    private String choiceId;
    @JsonProperty("group_id")
    private String groupId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;

    /**
     * No args constructor for use in serialization
     *
     */
    public Addon() {
    }

    /**
     *
     * @param groupId
     * @param price
     * @param choiceId
     * @param name
     */
    public Addon(String choiceId, String groupId, String name, Integer price) {
        super();
        this.choiceId = choiceId;
        this.groupId = groupId;
        this.name = name;
        this.price = price;
    }

    @JsonProperty("choice_id")
    public String getChoiceId() {
        return choiceId;
    }

    @JsonProperty("choice_id")
    public void setChoiceId(String choiceId) {
        this.choiceId = choiceId;
    }

    public Addon withChoiceId(String choiceId) {
        this.choiceId = choiceId;
        return this;
    }

    @JsonProperty("group_id")
    public String getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Addon withGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public Addon withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public Addon withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("choiceId", choiceId).append("groupId", groupId).append("name", name).append("price", price).toString();
    }

}
