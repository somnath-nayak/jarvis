package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import com.swiggy.api.sf.snd.constants.SANDConstants;


import com.swiggy.api.sf.rng.tests.RandomNumber;

import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.PropertiesHandler;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.Tyrion.DBHelper;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class SnDDp implements SANDConstants {

	String Env;
	PropertiesHandler properties = new PropertiesHandler();
	Random random = new Random();
	DBHelper db = new DBHelper();
	SANDHelper helper = new SANDHelper();
	RandomNumber rm = new RandomNumber(2000, 3000);

	public SnDDp() {
		if (System.getenv("ENVIRONMENT") == null)
			Env = properties.propertiesMap.get("environment");
		else
			Env = System.getenv("ENVIRONMENT");
	}

	@DataProvider(name = "getLatLong")
	public Object[][] getLatLong(ITestContext testContext) {
		String[] latlng1 = {"stage1", "12.934696", "77.616530"};
		String[] latlng2 = {"stage1", "13.934696", "77.616530"};
		String[] latlng3 = {"stage2", "12.934696", "77.616530"};
		String[] latlng4 = {"stage3", "12.934696", "77.616530"};
		String[] latlng5 = {"prod", "12.934696", "77.616530"};
		String[] latlng6 = {"stage1", "12.934696", "77.616530"};

		Object[][] dataSet = new Object[][]{latlng1, latlng2, latlng3, latlng4, latlng5, latlng6};

		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}

	@DataProvider(name = "validateAggregator")
	public Object[][] validateAggregator() {

		return new Object[][]{
				{"12.933", "77.601", 1, 5, true, "Test Msg", 0, 0, 2590},
				{"12.933", "77.601", 1, 5, false, "Test Msg", 0, 0, 2590}
		};
	}

	public String dateTime() {
		Date date = new Date();
		String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
		String DT = modifiedDate + "T" + formatTime;
		return DT;
	}


	public String extendedTime(int i) {
		Date date = new Date();
		String modifiedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
		LocalDateTime d = LocalDateTime.now();
		String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
		String DT = modifiedDate + "T" + dateFormat;
		return DT;
	}


	@DataProvider(name = "popListingValid")
	public Object[][] getpopListingValidLatLong() {
		return new Object[][]{
				{"12.934696", "77.616530", 1, "done successfully", 180, 0, "PopMetaConfig", "bn6orh4twq0i5zbxc9ag", "Introducing Swiggy Pop", "Single meals. All inclusive prices.",
						"extendedMessageCard", "VERTICAL", "extendedMessageCard", "extendedMessageCardWithImageAndButton", "Check back later to see Pop items.", "Meanwhile, don't let hunger get the best of you. Keep Swiggying from restaurants near you.",
						null, "Pop_All_OOS_gd7wpg", null, ""}
		};
	}


	/**
	 * Shaswat Start
	 * ***************************************************************************
	 */

	@DataProvider(name = "searchV2SmokeTestData")
	public Object[][] getsearchV2SmokeTestData() {
		return new Object[][]{
				{"28.5335197", "77.21088570000006", "tunday", 0, "done successfully"}
		};
	}

	@DataProvider(name = "searchV2RestaurantLevel")
	public Object[][] getsearchV2RestaurantLevelValidTestData() {
		return new Object[][]{
				{"12.936515", "77.605567", "Tarb"}
		};
	}

	@DataProvider(name = "searchV2CuisineLevel")
	public Object[][] getsearchV2CuisineLevelValidTestData() {
		return new Object[][]{
				{"12.936515", "77.605567", "Arab"}
		};
	}

	@DataProvider(name = "searchV2ItemLevel")
	public Object[][] getSearchV2ItemLevelValidTestData() {
		return new Object[][]{
				{"12.936515", "77.605567", "Chicken", "ITEM"}
		};
	}

	@DataProvider(name = "searchV2EmptyParamCombinationTestData")
	public Object[][] getsearchV2EmptyParamCombinationTestData() {
		return new Object[][]{
				{"28.5335197", "77.21088570000006", "", "", 1, new String[]{"error occured", "Search string can't be empty"}},
				{"", "77.21088570000006", "Ita", "", 1, new String[]{"error occured", "Latitude is missing.", "Latitude must be of type float"}},
				{"17.37401", "", "Ita", "", 1, new String[]{"error occured", "Longitude is missing.", "Longitude must be of type float"}},
				{"", "", "Ita", "", 1, new String[]{"error occured", "Latitude is missing.", "Longitude is missing.", "Latitude must be of type float", "Longitude must be of type float"}},
				{"", "", "", "", 1, new String[]{"error occured", "Latitude is missing.", "Longitude is missing.", "Latitude must be of type float", "Longitude must be of type float", "Search string can't be empty"}}
		};
	}

	@DataProvider(name = "searchV2InvalidRestaurant")
	public Object[][] getSearchV2InvalidSearchkeyTest() {
		return new Object[][]{
				{"12.936515", "77.605567", "pokemon"}
		};
	}

	@DataProvider(name = "searchV2InvalidCuisine")
	public Object[][] getSearchV2InvalidSeaech() {
		return new Object[][]{
				{"12.936515", "77.605567", "pokemon"}
		};
	}

	@DataProvider(name = "searchV2InvalidItem")
	public Object[][] getSearchV2InvalidSearchkeyItemLevelTest() {
		return new Object[][]{
				{"12.936515", "77.605567", "pokemon", "ITEM"}
		};
	}

	@DataProvider(name = "SearchV2FreeDeliverAtRestaurantLevel")
	public Object[][] SearchV2FreeDeliverAtRestaurantLevel() {
		return new Object[][]{
				{"Tarbouche", "12.936515", "77.605567", "100", true, false, false, "ZERO_DAYS_DORMANT", false}
		};
	}

	@DataProvider(name = "SearchV2DiscountAtRestaurantLevel")
	public Object[][] SearchV2DiscountAtRestaurantLevel() {
		return new Object[][]{
				{"12.92", "77.62", percent}
		};
	}

	@DataProvider(name = "searchV2FreeDeliver")
	public Object[][] searchV2FreeDeliver(ITestContext testContext) {
		String[] latLng = {"stage3", "12.9166", "77.6101", freeDel};
		Object[][] dataSet = new Object[][]{latLng};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}


	@DataProvider(name = "searchV2Dish")
	public Object[][] searchV2Dish(ITestContext testContext) {
		String[] latLng = {"stage3", "12.9166", "77.6101", "Pizza"};
		String[] latLng1 = {"stage3", "12.9166", "77.6101", "Biryani"};
		String[] latLng2 = {"stage3", "12.9166", "77.6101", "Rice"};
		Object[][] dataSet = new Object[][]{latLng, latLng1, latLng2};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}

	@DataProvider(name = "searchV2RestName")
	public Object[][] searchV2RestName(ITestContext testContext) {
		String[] latLng = {"stage3", "12.9166", "77.6101"};
		Object[][] dataSet = new Object[][]{latLng};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}


	@DataProvider(name = "searchByCuisine")
	public Object[][] searchByCuisine(ITestContext testContext) {
		return new Object[][]{{"12.9326", "77.6036", "Chettinad"}};
	}

	@DataProvider(name = "searchV2Page")
	public Object[][] searchV2Page(ITestContext testContext) {
		String[] latLng = {"stage1", "12.9326", "77.6036", "Pizza", "&page", "=ITEM"};
		Object[][] dataSet = new Object[][]{latLng};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}


	@DataProvider(name = "searchV2Percentage")
	public Object[][] searchV2Percentage(ITestContext testContext) {
		String[] latLng = {"stage3", "12.9166", "77.6101", "100", tr, fal, fal, "ZERO_DAYS_DORMANT", fal, percent};
		Object[][] dataSet = new Object[][]{latLng};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}

	@DataProvider(name = "searchV2Flat")
	public Object[][] searchV2Flat(ITestContext testContext) {
		String[] latLng = {"stage3", "12.9166", "77.6101", "100", tr, fal, fal, "ZERO_DAYS_DORMANT", fal, flat};
		Object[][] dataSet = new Object[][]{latLng};
		return ToolBox.returnReducedDataSet(Env, dataSet, testContext.getIncludedGroups(), dataSet.length, dataSet.length);
	}


	/*@DataProvider(name = "SearchV2DiscountAtRestaurantLevel")
	public Object[][] SearchV2DiscountAtRestaurantLevel(){
		return new Object[][]{
				{"Tarbouche","12.9166", "77.6101"}
		};
	}*/

	@DataProvider(name = "SearchV2FlatDiscountAtRestaurantLevel")
	public Object[][] SearchV2FlatDiscountAtRestaurantLevel() {
		return new Object[][]{
				{"0", "200", "12.9166", "77.6101"}
		};
	}


	@DataProvider(name = "SearchV2FeebieTDAtRestaurantLevel")
	public Object[][] SearchV2FeebieTDAtRestaurantLevel() {
		String freebieitemId = new Integer(rm.nextInt()).toString();
		return new Object[][]{
				{"0", "12.9326", "77.6036", freebieitemId}
		};
	}

	@DataProvider(name = "searchV2SchemaValidationTestData")
	public Object[][] getSearchV2SchemaValidationTestData() {
		return new Object[][]{
				{"28.5335197", "77.21088570000006", "tunday", 0, "done successfully"}
		};
	}

	@DataProvider(name = "samaySmokeTestData")
	public Object[][] getSamaySmokeTestData() {
		return new Object[][]{
				{"7130288", "9912", "POP_ITEM", "true", 200}
		};
	}

	@DataProvider(name = "samaySchemaValidationTestData")
	public Object[][] getSamaySchemaValidationTestData() {
		return new Object[][]{
				{"7130288", "9912", "POP_ITEM", "true"}
		};
	}

	@DataProvider(name = "similarRestaurantSmokeTestData")
	public Object[][] getSimilarRestaurantSmokeTestData() {
		return new Object[][]{
				{"12.9327745", "77.6161323", "Truffles", 0, "done successfully"}
		};
	}

	@DataProvider(name = "similarRestaurantServiceabilityOpenedTestData")
	public Object[][] getSimilarRestaurantServiceabilityOpenedTestData() {
		return new Object[][]{
				{"12.9327745", "77.6161323", "Truffles", 0, "done successfully", "SERVICEABLE", true}
		};
	}

	@DataProvider(name = "similarRestaurantSchemaTestData")
	public Object[][] getSimilarRestaurantSchemaTestData() {
		return new Object[][]{
				{"12.9327745", "77.6161323", "Truffles"}
		};
	}

	@DataProvider(name = "similarRestaurantsVsMenuV3TestData")
	public Object[][] getSimilarRestaurantsVsMenuV3TestData() {
		return new Object[][]{
				{"12.9327745", "77.6161323", "Truffles", "8884292249", "welcome123", "SERVICEABLE", true}
		};
	}


	@DataProvider(name = "setFavoriteSmokeTestData")
	public Object[][] getSetFavoriteSmokeTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", "true", "1", "12.936515", "77.605567", "false", 0, "done successfully"}
		};
	}

	@DataProvider(name = "setFavorite")
	public Object[][] getSetFavoriteSanityTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", "true", "1", "12.936515", "77.605567", 0, "done successfully"}
		};
	}

	@DataProvider(name = "removeFavorite")
	public Object[][] getRemoveFavoriteTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", "1", "false", "12.936515", "77.605567", 0, "done successfully"}
		};
	}

	@DataProvider(name = "getFavoriteSmokeTestData")
	public Object[][] getGetFavoriteSmokeTestData() {
		return new Object[][]{
				{"12.936515", "77.605567", "8884292249", "welcome123", 0, "done successfully"}
		};
	}

	@DataProvider(name = "setFavoriteSchemaTestData")
	public Object[][] setSetFavoriteSchemaTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", "true", "1", "Truffles", "false"}
		};
	}

	@DataProvider(name = "getFavoriteSchemaTestData")
	public Object[][] getSetFavoriteSchemaTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", "true", "1", "Truffles", "false", "12.936515", "77.605567"}
		};
	}

	@DataProvider(name = "getQuickMenuSmokeTestData")
	public Object[][] getQuickMenuSmokeTestData() {
		return new Object[][]{
				{"Truffles", "true", 0, "done successfully"},
				{"Truffles", "false", 0, "done successfully"}
		};
	}

	@DataProvider(name = "getQuickMenuSchemaTestData")
	public Object[][] getQuickMenuSchemaTestData() {
		return new Object[][]{
				{"Truffles", "true"},
				{"Truffles", "false"}
		};
	}

	@DataProvider(name = "quickMenuVsMenuV4")
	public Object[][] getQuickMenuVsMenuV3TestData() {
		return new Object[][]{
				{"true", "12.936515", "77.605567"}
		};
	}

	@DataProvider(name = "presentationControllerSmokeTestData")
	public Object[][] getPresentationControllerSmokeTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", new String[]{"12.9325585", "77.6013893"}, 0, "done successfully"}
		};
	}

	@DataProvider(name = "presentationControllerValidTestData")
	public Object[][] getPresentationControllerValidTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", new String[]{"12.9325585", "77.6013893"}, 1}
		};
	}

	@DataProvider(name = "presentationControllerWithNoPolygonTestData")
	public Object[][] getPresentationControllerWithNoPolygonTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", new String[]{"99.111", "77.6013893"}, 0}
		};
	}

	@DataProvider(name = "presentationControllerSchemaTestData")
	public Object[][] getPresentationControllerSchemaTestData() {
		return new Object[][]{
				{"8884292249", "welcome123", new String[]{"12.9325585", "77.6013893"}}
		};
	}

	@DataProvider(name = "listingTagPromotedRestaurantValidTestData")
	public Object[][] getListingTagPromotedRestaurantValidTestData() {
		return new Object[][]{
				{1, true}
		};
	}

	@DataProvider(name = "listingTagNonPromotedRestaurantTestData")
	public Object[][] getListingTagNonPromotedRestaurantTestData() {
		return new Object[][]{
				{1, false}
		};
	}

	@DataProvider(name = "listingTagPromotedRestaurantPositionTestData")
	public Object[][] getListingTagPromotedRestaurantPositionTestData() {
		return new Object[][]{
				{1, true},
				{1, false}
		};
	}

	@DataProvider(name = "crossSellingSmokeTestData")
	public Object[][] getCcrossSellingSmokeTestData() {
		return new Object[][]{
				{"545428", "1", "A", "B", "223", 0, "Success!"}
		};
	}

	@DataProvider(name = "crossSellingSchemaTestData")
	public Object[][] getCrossSellingSchemaTestData() {
		return new Object[][]{
				{"545428", "1", "A", "B", "223"}
		};
	}

	@DataProvider(name = "crossSellingValidTestData")
	public Object[][] getCrossSellingValidTestData() {
		return new Object[][]{
				{"545428", "1", "A", "B", "223", "12.933416243159748", "77.61433422338291"}
		};
	}

	@DataProvider(name = "crossSellingAddItemFromResponseTestData")
	public Object[][] getCrossSellingAddItemFromResponseTestData() {
		return new Object[][]{
				{"545428", "1", "A", "B", "223"}
		};
	}

	@DataProvider(name = "crossSellingAvailibilityOfItemsTestData")
	public Object[][] getCrossSellingAvailibilityOfItemsTestData() {
		return new Object[][]{
				{"545428", "1", "A", "B", "223"}
		};
	}

	/**
	 * Shaswat End
	 * #######################################################################
	 */


	@DataProvider(name = "signUpV2")
	public Object[][] singUpv2() {
		return new Object[][]{
				{"TestUser", SANDConstants.consumerSessionTstMobile, "swiggyqa14234234@gmail.com", SANDConstants.consumerSessionTstPwd}
		};
	}

	@DataProvider(name = "signUpV2InvalidDataSets")
	public Object[][] signUpV2InvalidDataSets() {
		return new Object[][]{
				{null, CheckoutConstants.mobile, "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid name"}},
				{"", CheckoutConstants.mobile, "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid name"}},
				{" ", CheckoutConstants.mobile, "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid name"}},
				{"   myNameWithSpaces   ", CheckoutConstants.mobile, "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid name"}},
				{"Simple User", "1234", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", null, "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "0", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "+919986379321", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "998637932", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "99863793211", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", " 9986379321  ", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "          ", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "asdfghjklp", "swiggyqa12342@gmail.com", CheckoutConstants.password, new Object[]{8, "invalid mobile number"}},
				{"Simple User", "1232123211", "swiggyqa12342", CheckoutConstants.password, new Object[]{8, "invalid email"}},
				{"Simple User", "1232123211", "", CheckoutConstants.password, new Object[]{8, "invalid email"}},
				{"Simple User", "1232123211", null, CheckoutConstants.password, new Object[]{8, "invalid email"}},
				{"Simple User", "1232123211", "   ", CheckoutConstants.password, new Object[]{8, "invalid email"}},
				{"Simple User", "1232123211", "swiggyqa12342@gmail.com", "", new Object[]{8, "Password length is too short"}},
				{"Simple User", "1232123211", "swiggyqa12342@gmail.com", null, new Object[]{8, "Password length is too short"}},
				{"Simple User", "1232123211", "swiggyqa12342@gmail.com", "12345", new Object[]{8, "Password length is too short"}}
		};
	}


	@DataProvider(name = "messagingv1")
	public Object[][] messagingv1() {
		return new Object[][]{
				{"1"}
		};
	}

	@DataProvider(name = "messagingv1Invalid")
	public Object[][] messagingv1Invalid() {
		return new Object[][]{
				{"2", new Object[]{0, "done successfully", 0.0, null}}

		};
	}


	@DataProvider(name = "sendOTP")
	public Object[][] sendOTP() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile}
		};
	}


	@DataProvider(name = "verifyOTP")
	public Object[][] verifyOTP() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile}
		};
	}

	@DataProvider(name = "logincheck")
	public Object[][] logincheck() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile}
		};
	}

	@DataProvider(name = "logincheckInvalidDataSets")
	public Object[][] logincheckInvalidDataSets() {
		return new Object[][]{
				{"", new Object[]{999, "Invalid mobile number."}},
				{"123", new Object[]{999, "Invalid mobile number."}},
				{"988637932", new Object[]{999, "Invalid mobile number."}},
				{"98863793212", new Object[]{999, "Invalid mobile number."}},
				{"abcdeee", new Object[]{999, "Invalid mobile number."}}
		};
	}

	@DataProvider(name = "callauth")
	public Object[][] callauth() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile}
		};
	}


	@DataProvider(name = "callauthInvalidDataSets")
	public Object[][] callauthInvalidDataSets() {
		return new Object[][]{
				{"", new Object[]{999, "Invalid mobile number."}},
				{null, new Object[]{999, "Invalid mobile number."}},
				{"123", new Object[]{999, "Invalid mobile number."}},
				{"988637932", new Object[]{999, "Invalid mobile number."}},
				{"98863793212", new Object[]{999, "Invalid mobile number."}},
				{"+9198863793212", new Object[]{999, "Invalid mobile number."}},
				{"abcdeee", new Object[]{999, "Invalid mobile number."}}
		};
	}


	@DataProvider(name = "loginV2")
	public Object[][] loginV2() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile, SANDConstants.password}
		};
	}

	@DataProvider(name = "loginV2InvalidDataSets")
	public Object[][] loginV2InvalidDataSets() {
		return new Object[][]{
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{null, SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
				{"aasdasd", SANDConstants.password, new Object[]{1, "Invalid password"}},
		};
	}

	@DataProvider(name = "callFlow")
	public Object[][] callFlow() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile, SANDConstants.password}
		};
	}


	@DataProvider(name = "setpassword")
	public static Object[][] setpassword() {
		return new Object[][]{
				{SANDConstants.consumerSessionTstMobile, SANDConstants.password, "rkonowhere"},
		};
	}

	@DataProvider(name = "passwordInvalidDataSets")
	public static Object[][] setpasswordInvalidDataSets() {
		return new Object[][]{
				{SANDConstants.mobile, SANDConstants.password, "qw", new Object[]{999, "cannot"}},
				{SANDConstants.mobile, SANDConstants.password, "", new Object[]{999, "cannot"}},
				{SANDConstants.mobile, SANDConstants.password, "ssdsd", new Object[]{999, "cannot"}},
				{SANDConstants.mobile, SANDConstants.password, "21333", new Object[]{999, "cannot"}},
		};
	}

	/**
	 * Srishty Start
	 * ****************************************************************************
	 */
	@DataProvider(name = "locationBasedPOPEnabled")
	public Object[][] getPOPEnabled() {
		return new Object[][]{
				{"12.933810", "77.614735", SANDConstants.LocationFeature[0]}
		};
	}

	@DataProvider(name = "locationBasedPOPDisabled")
	public Object[][] getPOPDisabled() {

		return new Object[][]{
				{"17.933810", "77.614735", SANDConstants.LocationFeature[0]}
		};
	}

	@DataProvider(name = "locationBasedCafeEnabled")
	public Object[][] getCafeEnabled() {

		return new Object[][]{
				{"12.933810", "77.614735", SANDConstants.LocationFeature[2]}
		};
	}

	@DataProvider(name = "locationBasedCafeDisabled")
	public Object[][] getCafeDisabled() {

		return new Object[][]{
				{"12.933810", "77.614735", SANDConstants.LocationFeature[2]}
		};
	}

	@DataProvider(name = "locationBasedPOPAssured")
	public Object[][] getLocBasedPOPAssured() {
		String feature = SANDConstants.LocationFeature[0] + "," + SANDConstants.LocationFeature[1];
		return new Object[][]{
				{"12.933810", "77.614735", feature}
		};
	}


	@DataProvider(name = "locationBasedAssuredEnabled")
	public Object[][] getAssuredEnabled() {

		return new Object[][]{
				{"12.933810", "77.614735", SANDConstants.LocationFeature[1]}
		};
	}

	@DataProvider(name = "locationBasedAssuredDisabled")
	public Object[][] getAssuredDisabled() {

		return new Object[][]{
				{"17.933810", "77.614735", SANDConstants.LocationFeature[1]}
		};
	}


	@DataProvider(name = "locationBasedAllEnabled")
	public Object[][] getAllEnabled() {
		String feature = SANDConstants.LocationFeature[0] + "," + SANDConstants.LocationFeature[1];
		return new Object[][]{
				{"12.933810", "77.614735", feature}
		};
	}

	@DataProvider(name = "locationBasedAllDisabled")
	public Object[][] getAllDisabled() {
		String feature = SANDConstants.LocationFeature[0] + "," + SANDConstants.LocationFeature[1];
		return new Object[][]{
				{"27.933810", "77.614735", feature}
		};
	}

	@DataProvider(name = "launch")
	public Object[][] getlaunch() {

		String experimentname = SANDConstants.experimentName;
		return new Object[][]{
//			{ experimentname, "8431137043", "srishty16","990514","5934","1"}
				{experimentname, "7406734416", "rkonowhere", "294987", "223", "1"}
		};
	}

	@DataProvider(name = "settings")
	public Object[][] getSettings() {

		return new Object[][]{
				{SANDConstants.options[0], SANDConstants.values[0], SANDConstants.options[1], SANDConstants.values[1]}
		};
	}

	@DataProvider(name = "areaListingPostive")
	public Object[][] getAreaListing() {

		return new Object[][]{
				{"koramangala"},
				{"rohini"}
		};
	}

	@DataProvider(name = "areaListingNegative")
	public Object[][] getAreaListingNegative() {

		return new Object[][]{
				{"invalid"}

		};
	}

	@DataProvider(name = "areaListingSchema")
	public Object[][] getAreaListngSchema() {

		return new Object[][]{
				{"koramangala"}
		};
	}


	@DataProvider(name = "menuTags")
	public Object[][] getRestId() {

		return new Object[][]{
				{"12.934847", "77.61612300000002"}
		};
	}

	@DataProvider(name = "authenticateCorporate")
	public Object[][] getAuthenticateCorporate() {

		return new Object[][]{
				{"8431137043", "srishty16", "1", "com.swiggy.poc.test1"}
		};
	}

	@DataProvider(name = "authenticateCorporateNegative")
	public Object[][] getAuthenticateCorporateNegative() {

		return new Object[][]{
				{"8431137043", "srishty16", "178", "com.swiggy.poc.test1"}
		};
	}

	@DataProvider(name = "CorporateByLatLong")
	public Object[][] getCorporateByLatLong() {

		return new Object[][]{
				{"8431137043", "srishty16", "12.9279232", "77.62710779999998"}
		};
	}

	@DataProvider(name = "CorporateByLatLongNegative")
	public Object[][] getCorporateByLatLongNegative() {

		return new Object[][]{
				{"8431137043", "srishty16", "15.9279232", "77.62710779999998"}
		};
	}

	@DataProvider(name = "cafeListing")
	public Object[][] getCafeListing() {

		return new Object[][]{
				{"3", "test3", "srishty16", "8431137043"}
		};
	}

	@DataProvider(name = "cafeListingNegative")
	public Object[][] getCafeListingNegative() {

		return new Object[][]{
				{"30", "test3", "srishty16", "8431137043"}
		};
	}

	@DataProvider(name = "menuV4")
	public Object[][] getMenuV4() {

		return new Object[][]{
				{"229", "15.9279232", "77.62710779999998"}
		};
	}

	@DataProvider(name = "menuV4Negative")
	public Object[][] getMenuV4Negative() {

		return new Object[][]{
				{"223", "15.9279232", "77.62710779999998"}
		};
	}

	@DataProvider(name = "accessibleCorporates")
	public Object[][] getAccessibleCorporates() {

		return new Object[][]{
				{"201966"}
		};
	}

	@DataProvider(name = "accessibleCorporatesNegative")
	public Object[][] getAccessibleCorporatesNegative() {

		return new Object[][]{
				{"266"}
		};
	}

	@DataProvider(name = "authenticate")
	public Object[][] authenticate() {

		return new Object[][]{
				{"4", "test4", ""},
				{"4", "test4", "201920"},
				{"4", "te", "201920"}

		};
	}

	@DataProvider(name = "authenticateNegative")
	public Object[][] authenticateNegative() {

		return new Object[][]{
				{"4", "te4", "2019034"},
				{"4", "", "2019034"}

		};
	}

	@DataProvider(name = "accessToCafe")
	public Object[][] accessToCafe() {

		return new Object[][]{
				{"2", "201966", ""},
				{"2", "201966", "test4"},

		};
	}

	@DataProvider(name = "accessToCafeNegative")
	public Object[][] accessToCafeNegative() {

		return new Object[][]{
				{"4", "2016", ""},
				{"4", "", "ts4"},
				{"4", "2019", ""}

		};
	}

	@DataProvider(name = "accessToCorporate")
	public Object[][] accessToCorporate() {

		return new Object[][]{
				{"4", "201966", ""},
				{"4", "", "test4"},

		};
	}

	@DataProvider(name = "accessToCorporateNegative")
	public Object[][] accessToCorporateNegative() {

		return new Object[][]{
				{"4", "2016", ""},
				{"4", "", "ts4"},
				{"4", "2019", ""}

		};
	}

	@DataProvider(name = "accessToRestaurant")
	public Object[][] accessToRestaurant() {

		return new Object[][]{
				{"229", "201966", ""},
				{"229", "", "test4"},
				{"229", "201966", "testoi"}

		};
	}

	@DataProvider(name = "accessToRestaurantNegative")
	public Object[][] accessToRestaurantNegative() {

		return new Object[][]{
				{"229", "2019", ""},
				{"229", "", "te454"},
				{"229", "2019", "testoi"}

		};
	}

	@DataProvider(name = "getCafeListing")
	public Object[][] getCafes() {

		return new Object[][]{
				{"4", "201966", ""},
				{"4", "", "test4"},

		};
	}

	@DataProvider(name = "getCorporateByLatLng")
	public Object[][] getCorporateByLatLng() {

		return new Object[][]{
				{"12.9279232,77.62710779999998"}
		};
	}

	@DataProvider(name = "getRestaurantList")
	public Object[][] getRestaurantList() {

		return new Object[][]{
				{"2", "201966", ""},
				{"2", "201966", "test4"},
				{"2", "201966", "tes6556"}

		};
	}

	/**
	 * Srishty End
	 * ############################################################################
	 */


	@DataProvider(name = "menuV3TestData")
	public static Object[][] menuV3TestDataObject() {
		return new Object[][]{
				{"8884292249", "welcome123", "12.936515", "77.605567"}
		};
	}

	@DataProvider(name = "menuV3TestDataBasic")
	public static Object[][] menuV3TestDataBasic() {
		return new Object[][]{
				{"12.936515", "77.605567"}
		};
	}

	@DataProvider(name = "locationBasedRain")
	public static Object[][] locationBasedRain() {

		return new Object[][]{
				{"12.933810", "77.614735", SANDConstants.LocationFeature[0]}
		};
	}

	@DataProvider(name = "rainAggregator")
	public static Object[][] rainAggregator() {

		return new Object[][]{
				{"12.933810", "77.614735"}
		};
	}

	@DataProvider(name = "userId")
	public Object[][] userId() {
		//Map<String, Object> m=helper.query(SANDConstants.userId);
		return new Object[][]{{SANDConstants.id},
				{SANDConstants.mobileId},
				{SANDConstants.email},
				{SANDConstants.referal_code}};
	}

	@DataProvider(name = "userIdNegative")
	public Object[][] userIdNegative() {
		return new Object[][]{{SANDConstants.id, userIDD()},
				{SANDConstants.mobileId, getMobile()},
				{SANDConstants.email, getemailString()},
				{SANDConstants.referal_code, randomAlpha()}};
	}

	@DataProvider(name = "userProfile")
	public Object[][] userProfile() {
		return new Object[][]{{SANDConstants.tr, SANDConstants.tr},
				{SANDConstants.tr, SANDConstants.fal},
				{SANDConstants.fal, SANDConstants.tr},
				{SANDConstants.fal, SANDConstants.fal}};
	}


	@DataProvider(name = "signUpProfile")
	public Object[][] signUpProfile() {
		return new Object[][]{{getMobile(), getemailString(), SANDConstants.name, SANDConstants.tr, SANDConstants.tr},
				{getMobile(), getemailString(), SANDConstants.name, SANDConstants.tr, SANDConstants.fal},
				{getMobile(), getemailString(), SANDConstants.name, SANDConstants.fal, SANDConstants.tr},
				{getMobile(), getemailString(), SANDConstants.name, SANDConstants.fal, SANDConstants.fal}};
	}

	@DataProvider(name = "userProfileAdmin")
	public Object[][] userProfileAdmin() {
		return new Object[][]{{SANDConstants.user_id}, {SANDConstants.userEmail}, {SANDConstants.userMobile}};
	}

	@DataProvider(name = "userProfileAdminNegative")
	public Object[][] userProfileAdminNegative() {
		return new Object[][]{{SANDConstants.user_id, userIDD()}, {SANDConstants.userEmail, getemailString()}, {SANDConstants.userMobile, getMobile()}};
	}

	@DataProvider(name = "signUpV1")
	public Object[][] signUpV1() {
		return new Object[][]{{getMobile(), getemailString(), SANDConstants.name}, {getMobile(), getemailString(), SANDConstants.name}};
	}


	@DataProvider(name = "updateMobileNumber")
	public Object[][] updateMobileNumber() {
		return new Object[][]{{SANDConstants.name, getMobile(), getemailString(), SANDConstants.password}, {SANDConstants.name, getMobile(), getemailString(), SANDConstants.password}};
	}

	@DataProvider(name = "loginDetails")
	public Object[][] loginDetails() {
		return new Object[][]{{SANDConstants.mobile, SANDConstants.password}, {SANDConstants.mobile, SANDConstants.password}};
	}

	@DataProvider(name = "otpMobile")
	public Object[][] otpMobile() {
		return new Object[][]{{SANDConstants.mobile}};
	}

	@DataProvider(name = "login")
	public Object[][] login() {
		return new Object[][]{{SANDConstants.mobile}};
	}

	@DataProvider(name = "latLong")
	public Object[][] latLong() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng}};
	}

	@DataProvider(name = "loginDetail")
	public Object[][] loginDetail() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng}};
	}

	@DataProvider(name = "freebieMenu")
	public Object[][] freebieMenu() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng, SANDConstants.freebie}};
	}

	@DataProvider(name = "percentageMenu")
	public Object[][] percentageMenu() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng, SANDConstants.percent}};
	}

	@DataProvider(name = "flatMenu")
	public Object[][] flatMenu() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng, SANDConstants.flat}};
	}

	@DataProvider(name = "freeDelMenu")
	public Object[][] freeDelMenu() {
		return new Object[][]{{SANDConstants.lat, SANDConstants.lng, SANDConstants.freeDel}};
	}

	@DataProvider(name = "flatAggregator")
	public Object[][] flatAggregator() {
		return new Object[][]{{lat, lng, flat, rest}};
	}

	@DataProvider(name = "percentageAggregator")
	public Object[][] percentageAggregator() {
		return new Object[][]{{lat, lng, percent, rest}};
	}

	@DataProvider(name = "freebieAggregator")
	public Object[][] freebieAggregator() {
		return new Object[][]{{"12.9166", "77.6101", freebie}};
	}

	@DataProvider(name = "freeDelAggregator")
	public Object[][] freeDelAggregator() {
		return new Object[][]{{lat, lng, freeDel, rest}};
	}

	@DataProvider(name = "rainMode")
	public Object[][] rainMode() {
		return new Object[][]{{lat, lng, "1", "3", "HEAVY"},
				{lat, lng, "2", "3", "LIGHT"}};
	}

	@DataProvider(name = "slug")
	public Object[][] slug() {
		return new Object[][]{{lat, lng}};
	}


	private String getMobile() {
		return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
	}

	private String userIDD() {
		return String.valueOf((long) Math.floor(Math.random() * 9_000_000_00L) + 1_000_000_00L);
	}

	private String getemailString() {
		String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) {
			int index = (int) (rnd.nextFloat() * CHARS.length());
			salt.append(CHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr + "@gmail.com";
	}

	private String randomAlpha() {
		int count = 6;
		String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * alphabet.length());
			builder.append(alphabet.charAt(character));
		}
		return builder.toString();
	}

	@DataProvider(name = "signUp")
	public Object[][] signUp() {
		return new Object[][]{
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password}
		};
	}

	@DataProvider(name = "userInternalPatch")
	public Object[][] userInternalPatch() {
		return new Object[][]{
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, mo, getMobile()},
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, usedEmail, getemailString()},
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, lastOrderCity, "1"},
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, emailVerificationFlag, tr},
		};
	}

	@DataProvider(name = "userInternalReferralPatch")
	public Object[][] userInternalReferralPatch() {
		return new Object[][]{
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, referralUsed},
		};
	}

	@DataProvider(name = "setPassword")
	public Object[][] setPassword() {
		return new Object[][]{
				{getMobile(), getemailString()},
				{getMobile(), getemailString()},
		};
	}

	@DataProvider(name = "referCode")
	public Object[][] referCode() {
		return new Object[][]{
				{getMobile(), getemailString()},
				{getMobile(), getemailString()},
		};
	}


	@DataProvider(name = "loginD")
	public Object[][] loginD() {
		return new Object[][]{{"7899772315", "swiggy"}};
	}

	@DataProvider(name = "logintest")
	public Object[][] logintest() {
		return new Object[][]{{"swiggy", "9861224884", "user with this mobile number is not registered"}, {"", "", "Invalid mobile number."}, {"swiggy", "", "Invalid mobile number.",}, {"", "7406734416", "Invalid Password"}};
	}

	@DataProvider(name = "checkMenuV4")
	public Object[][] checkMenuV4() {
		return new Object[][]{{"223", "28.4190511", "77.05194699999993"}};
	}

	@DataProvider(name = "checkRest")
	public Object[][] checkRest() {
		return new Object[][]{{"28.4190511", "77.05194699999993"}};
	}


	@DataProvider(name = "catalogAttributesVariants")
	public Object[][] catalogAttributesVariants() {
		return new Object[][]{{"219", "12.9334162315974", "77.61433422338", SANDConstants.data}};
	}

	@DataProvider(name = "catalogAttributesRest")
	public Object[][] catalogAttributesRest() {
		return new Object[][]{{"223", "12.9334162315974", "77.61433422338"}};
	}

	@DataProvider(name = "catalogAttributesItem")
	public Object[][] catalogAttributesItem() {
		return new Object[][]{{"219", "12.9334162315974", "77.61433422338", SANDConstants.data}};
	}

	@DataProvider(name = "rainServiceability")
	public Object[][] rainServiceability() {
		return new Object[][]{{"12.933810", "77.614735", "1"}};
	}


	@DataProvider(name = "rainModeType")
	public Object[][] rainModeType() {
		return new Object[][]{{"12.933810", "77.614735", "1"}};
	}


	@DataProvider(name = "rainCity")
	public Object[][] rainCity() {
		return new Object[][]{{"12.93440905476166,77.61608090062907"}};
	}

	@DataProvider(name = "rainArea")
	public Object[][] rainArea() {
		return new Object[][]{{"12.93440905476166,77.61608090062907"}};
	}

	@DataProvider(name = "rainRestaurant")
	public Object[][] rainRestaurant() {
		return new Object[][]{{"12.93440905476166,77.61608090062907"}};
	}

	@DataProvider(name = "rainMenu")
	public Object[][] rainMenu() {
		return new Object[][]{{"12.93440905476166,77.61608090062907"}};
	}

	@DataProvider(name = "rainItem")
	public Object[][] rainItem() {
		return new Object[][]{{"12.93440905476166,77.61608090062907"}};
	}


	@DataProvider(name = "exclusiveTag")
	public Object[][] exclusiveTag() {
		return new Object[][]{{1, "28.590173721313477", "77.3077163696289"}};
	}

	@DataProvider(name = "nonExclusiveTag")
	public Object[][] nonExclusiveTag() {
		return new Object[][]{{0, "28.590173721313477", "77.3077163696289"}};
	}

	@DataProvider(name = "newTag")
	public Object[][] newTag() {
		return new Object[][]{{helper.dateTime(-1), "28.590173721313477", "77.3077163696289"}};
	}


	@DataProvider(name = "notNewTag")
	public Object[][] notNewTag() {
		return new Object[][]{{helper.dateTime(-15), "28.590173721313477", "77.3077163696289"}};
	}


	@DataProvider(name = "popularTag")
	public Object[][] popularTag() {
		return new Object[][]{{"12.934847", "77.61612300000002"}};
	}

	@DataProvider(name = "nonPopularTag")
	public Object[][] nonPopularTag() {
		return new Object[][]{{"12.934847", "77.61612300000002"}};
	}


	@DataProvider(name = "premiumTag")
	public Object[][] premiumTag() {
		return new Object[][]{{"12.934847", "77.61612300000002"}};
	}


	@DataProvider(name = "nonPremiumTag")
	public Object[][] nonPremiumTag() {
		return new Object[][]{{"12.933416243159748", "77.61433422338291"}};
	}


	@DataProvider(name = "MenuV4")
	public Object[][] MenuV4() {
		return new Object[][]{{"12.92", "77.62"}};
	}

	@DataProvider(name = "LatLong")
	public Object[][] LatLong() {
		return new Object[][]{{"12.92", "77.62"}};
	}

	@DataProvider(name="latlongthroughjenkins")
	public Object[][]latLongThroughParameter(){
		return new Object[][]{{System.getenv("day"),System.getenv("restaurant_id"),System.getenv("open_time"),System.getenv("close_time")}};

	}

	@DataProvider(name = "menuReorder")
	public Object[][] MenuReorder() {
		return new Object[][]{{"10832", "12.960719", "77.63852999999995", "314"}};
	}


	@DataProvider(name = "latLong1")
	public Object[][] latLong1() {
		return new Object[][]{
				{"12.92", "77.62"}
		};
	}

	@DataProvider(name = "latLongDel")
	public Object[][] latLongDel() {
		return new Object[][]{
				{"12.92", "77.62"
						, "free_delivery"}
		};
	}

	@DataProvider(name = "latLongDelPer")
	public Object[][] latLongDelPer() {
		return new Object[][]{
				{"12.92", "77.62"
						, "free_delivery", "discount_percent_greater_than"}
		};
	}

	@DataProvider(name = "latLongDelFlat")
	public Object[][] latLongDelFlat() {
		return new Object[][]{
				{"12.92", "77.62"
						, "free_delivery", "flat"}
		};
	}


	@DataProvider(name = "percentageCol")
	public Object[][] percentageCol() {
		return new Object[][]{
				{"12.933810", "77.614735", "discount_percent_greater_than"}
		};
	}

	@DataProvider(name = "percentageLessThanCol")
	public Object[][] percentageLessThanCol() {
		return new Object[][]{
				{"12.933810", "77.614735", "discount_percent_less_than"}
		};
	}


	@DataProvider(name = "percentGreaterFirstCol")
	public Object[][] percentGreaterFirstCol() {
		return new Object[][]{
				{"12.9279", "77.6271", "discount_percent_greater_than_first_user"}
		};
	}

	@DataProvider(name = "slaCollection")
	public Object[][] slaCollection() {
		return new Object[][]{
				{"12.9279", "77.6271", "delivery_time"}
		};
	}

	@DataProvider(name = "longDistance")
	public Object[][] longDistance() {
		return new Object[][]{
				{"12.9279", "77.6271", "long_distance"}
		};
	}

	@DataProvider(name = "offers")
	public Object[][] offers() {
		return new Object[][]{
				{"12.933810", "77.614735", "offers"}
		};
	}

	@DataProvider(name = "trending")
	public Object[][] trending() {
		return new Object[][]{
				{"12.9279", "77.6271", "trending"}
		};
	}

	@DataProvider(name = "vegOnly")
	public Object[][] vegOnly() {
		return new Object[][]{
				{"12.933810", "77.614735", "veg_only"}
		};
	}

	@DataProvider(name = "ratings")
	public Object[][] ratings() {
		return new Object[][]{
				{"12.933810", "77.614735", "ratings_aov"}
		};
	}

	@DataProvider(name = "costForTwo")
	public Object[][] costForTwo() {
		return new Object[][]{
				{"12.933810", "77.614735", "cost_for_two"}
		};
	}

	@DataProvider(name = "distanceFee")
	public Object[][] distanceFee() {
		return new Object[][]{
				{"12.9326", "77.6036", fromTime, toTime, defaultFee, fromDistance, toDistance, cartFe}
		};
	}

	@DataProvider(name = "timeFee")
	public Object[][] timeFee() {
		return new Object[][]{
				{"12.9279", "77.6271", fromTime, toTime, "2"}
		};
	}

	@DataProvider(name = "specialFee")
	public Object[][] specialFee() {
		return new Object[][]{
				{"12.9279", "77.6271", fal, "3", priority, 0, 2}
		};
	}

	@DataProvider(name = "multi")
	public Object[][] multi() {
		return new Object[][]{
				{"12.9326", "77.6036"}
		};
	}


	@DataProvider(name = "favourite")
	public Object[][] favourite() {
		return new Object[][]{
				{name, password, tr, "12.92", "77.62"}
		};
	}

	@DataProvider(name = "favouritePercentage")
	public Object[][] favouritePercentage() {
		return new Object[][]{
				{name, password, tr, "12.9326", "77.6036", percent}
		};
	}

	@DataProvider(name = "favouriteFlat")
	public Object[][] favouriteFlat() {
		return new Object[][]{
				{name, password, tr, "12.92", "77.62", flat}
		};
	}

	@DataProvider(name = "favouriteFreeDel")
	public Object[][] favouriteFreeDel() {
		return new Object[][]{
				{name, password, tr, "12.92", "77.62", freeDel, minAmount}
		};
	}

	@DataProvider(name = "favouriteFreeBie")
	public Object[][] favouriteFreeBie() {
		return new Object[][]{
				{name, password, tr, "12.9166", "77.6101", freebie, "10"}
		};
	}

	@DataProvider(name = "unFavourite")
	public Object[][] unFavourite() {
		return new Object[][]{
				{name, password, tr, fal, "12.92", "77.62"}
		};
	}

	@DataProvider(name = "SearchFlatDiscount")
	public Object[][] SearchFlatDiscount() {
		return new Object[][]{
				{lat, lng, flat}
		};
	}

	@DataProvider(name = "searchFreeBie")
	public Object[][] searchFreeBie() {
		return new Object[][]{
				{"Pizza", lat, lng, freebie}
		};
	}


	@DataProvider(name = "cross")
	public Object[][] getCross() {
		return new Object[][]{
				{"223", "Main Course"}
		};
	}

	@DataProvider(name = "crossSell")
	public Object[][] crossSell() {
		List<String> cat = new ArrayList<>();
		cat.add("Desserts");
		return new Object[][]{
				{"Main Course", "223", cat}
		};
	}


	@DataProvider(name = "freebie")
	public Object[][] freebie() {
		return new Object[][]{
				{"12.92", "77.62"
						, "super_freebie"}
		};
	}

	@DataProvider(name = "free_del_discount_percent_greater_than")
	public Object[][] free_del_discount_percent_greater_than() {
		return new Object[][]{
				{"12.933810", "77.614735", "free_del_discount_percent_greater_than"}
		};
	}

	@DataProvider(name = "30MinsOrFree")
	public Object[][] thirtyMinsOrFree() {
		return new Object[][]{
				{"12.932", "77.614"}
		};
	}

	@DataProvider(name = "30MinsOrFreeNegative")
	public Object[][] thirtyMinsOrFreeNegative() {
		return new Object[][]{
				{"12.9326", "77.6036"}
		};
	}


	@DataProvider(name = "freeDelSearch")
	public Object[][] freeDelSearch() {
		return new Object[][]{
				{"12.9326", "77.6036", freeDel}
		};
	}

	@DataProvider(name = "perSearch")
	public Object[][] perSearch() {
		return new Object[][]{
				{"12.9326", "77.6036", percent}
		};
	}

	@DataProvider(name = "flatSearch")
	public Object[][] flatSearch() {
		return new Object[][]{
				{"12.9326", "77.6036", flat}
		};
	}

	@DataProvider(name = "freebieSearch")
	public Object[][] freebieSearch() {
		return new Object[][]{
				{"12.9326", "77.6036", freebie}
		};
	}

	@DataProvider(name = "menuv4")
	public Object[][] menuv4() {

		return new Object[][]{
				{"12.91489", "77.61267"}
		};

	}

	@DataProvider(name = "userArchival")
	public Object[][] userArchival() {
		return new Object[][]{
				{randomAlpha(), getMobile(), getemailString(), SANDConstants.password, "user-archival", "Nevia"}
		};
	}

	@DataProvider(name = "userArchivalSmoke")
	public Object[][] userArchivalSmoke() {
		return new Object[][]{
				{getMobile(), "user-archival", "Nevia"}
		};
	}

	@DataProvider(name = "userArchivalInvalid")
	public Object[][] userArchivalInvalid() {
		return new Object[][]{
				{"98989808", "user-archival", "Nevia"}
		};
	}

	@DataProvider(name = "menuV4WithUA")
	public Object[][] getMenuV4WithUA() {

		return new Object[][]{
				{"12.9326", "77.6036", "436", "Swiggy-Android"},
				{"12.9326", "77.6036", "255", "Swiggy-IOS"}
		};
	}

	@DataProvider(name = "menuV4WithUAOnVersion")
	public Object[][] getMenuV4WithUAOnVersion() {

		return new Object[][]{
				{"12.9326", "77.6036", "435", "Swiggy-Android"},
				{"12.9326", "77.6036", "250", "Swiggy-IOS"}
		};
	}

	@DataProvider(name = "menuV4WithUABelowVersion")
	public Object[][] getMenuV4WithUABelowVersion() {

		return new Object[][]{
				{"12.9326", "77.6036", "434", "Swiggy-Android"},
				{"12.9326", "77.6036", "244", "Swiggy-IOS"}
		};
	}



}

