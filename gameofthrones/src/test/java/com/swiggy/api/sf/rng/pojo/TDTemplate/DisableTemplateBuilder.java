package com.swiggy.api.sf.rng.pojo.TDTemplate;

import java.util.ArrayList;
import java.util.List;

public class DisableTemplateBuilder {

    public DisableTemplate disableTemplate;

    public DisableTemplateBuilder() {
        disableTemplate = new DisableTemplate();
    }

    public DisableTemplateBuilder withIds(List<Integer>  idlList) {  //comma separated ids


        disableTemplate.setIds(idlList);
        return this;
    }

    public DisableTemplate build() {
        getDefaultValue();
        return disableTemplate;
    }

    private void getDefaultValue() {
        if (disableTemplate.getIds() == null) {
            List<Integer> ids = new ArrayList<>();
            ids.add(1);
            disableTemplate.setIds(ids);
        }
    }

}
