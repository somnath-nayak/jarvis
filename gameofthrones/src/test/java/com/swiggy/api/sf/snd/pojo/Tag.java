package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Tag {

    private String type;
    private String tagGroup;
    private String tagValue;

    /**
     * No args constructor for use in serialization
     *
     */
    public Tag() {
    }

    /**
     *
     * @param tagValue
     * @param tagGroup
     * @param type
     */
    public Tag(String type, String tagGroup, String tagValue) {
        super();
        this.type = type;
        this.tagGroup = tagGroup;
        this.tagValue = tagValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTagGroup() {
        return tagGroup;
    }

    public void setTagGroup(String tagGroup) {
        this.tagGroup = tagGroup;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("tagGroup", tagGroup).append("tagValue", tagValue).toString();
    }

}