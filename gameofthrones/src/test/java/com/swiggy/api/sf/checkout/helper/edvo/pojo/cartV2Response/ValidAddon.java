package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "group_id",
        "group_name",
        "choices",
        "maxAddons",
        "maxFreeAddons",
        "minAddons",
        "order"
})
public class ValidAddon {

    @JsonProperty("group_id")
    private Integer groupId;
    @JsonProperty("group_name")
    private String groupName;
    @JsonProperty("choices")
    private List<Choice> choices = null;
    @JsonProperty("maxAddons")
    private Integer maxAddons;
    @JsonProperty("maxFreeAddons")
    private Integer maxFreeAddons;
    @JsonProperty("minAddons")
    private Integer minAddons;
    @JsonProperty("order")
    private Integer order;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public ValidAddon() {
    }

    /**
     *
     * @param minAddons
     * @param groupId
     * @param groupName
     * @param order
     * @param maxAddons
     * @param choices
     * @param maxFreeAddons
     */
    public ValidAddon(Integer groupId, String groupName, List<Choice> choices, Integer maxAddons, Integer maxFreeAddons, Integer minAddons, Integer order) {
        super();
        this.groupId = groupId;
        this.groupName = groupName;
        this.choices = choices;
        this.maxAddons = maxAddons;
        this.maxFreeAddons = maxFreeAddons;
        this.minAddons = minAddons;
        this.order = order;
    }

    @JsonProperty("group_id")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("group_id")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("group_name")
    public String getGroupName() {
        return groupName;
    }

    @JsonProperty("group_name")
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @JsonProperty("choices")
    public List<Choice> getChoices() {
        return choices;
    }

    @JsonProperty("choices")
    public void setChoices(List<Choice> choices) {
        this.choices = choices;
    }

    @JsonProperty("maxAddons")
    public Integer getMaxAddons() {
        return maxAddons;
    }

    @JsonProperty("maxAddons")
    public void setMaxAddons(Integer maxAddons) {
        this.maxAddons = maxAddons;
    }

    @JsonProperty("maxFreeAddons")
    public Integer getMaxFreeAddons() {
        return maxFreeAddons;
    }

    @JsonProperty("maxFreeAddons")
    public void setMaxFreeAddons(Integer maxFreeAddons) {
        this.maxFreeAddons = maxFreeAddons;
    }

    @JsonProperty("minAddons")
    public Integer getMinAddons() {
        return minAddons;
    }

    @JsonProperty("minAddons")
    public void setMinAddons(Integer minAddons) {
        this.minAddons = minAddons;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("groupId", groupId).append("groupName", groupName).append("choices", choices).append("maxAddons", maxAddons).append("maxFreeAddons", maxFreeAddons).append("minAddons", minAddons).append("order", order).append("additionalProperties", additionalProperties).toString();
    }

}