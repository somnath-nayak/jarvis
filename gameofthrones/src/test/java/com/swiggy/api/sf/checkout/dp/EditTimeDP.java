package com.swiggy.api.sf.checkout.dp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.sf.checkout.helper.EditTimeHelper;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.checkout.constants.EditTimeConstants;

public class EditTimeDP {
	public static List<Integer> itemIds = new ArrayList<>();

	@DataProvider(name = "editTimeCartUpdate")
	public static Object[][] editTimeCheck() throws IOException{

		return new Object[][]{
				{EditTimeConstants.restInitSource,String.valueOf(String.valueOf(itemIds.get(0))),"1",EditTimeConstants.REST_ID,String.valueOf(String.valueOf(itemIds.get(1))),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},
				{EditTimeConstants.restInitSource,String.valueOf(itemIds.get(0)),"3",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"1",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},

				{EditTimeConstants.custInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},
				{EditTimeConstants.custInitSource,String.valueOf(itemIds.get(0)),"3",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"1",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},

				{EditTimeConstants.defaultInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},
				{EditTimeConstants.defaultInitSource,String.valueOf(itemIds.get(0)),"3",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"1",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},

				{EditTimeConstants.restInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},
				{EditTimeConstants.custInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE},
				{EditTimeConstants.defaultInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"2",EditTimeConstants.STATUS_CODE,EditTimeConstants.CART_UPDATED_SUCCESSFULLY,EditTimeConstants.STATUS_CODE,EditTimeConstants.STATUS_MESSAGE}


		};
	}


	@DataProvider(name = "editTimeCartUpdateNegative")
	public static Object[][] editTimeCheckNegative() throws IOException{

		return new Object[][]{
				{EditTimeConstants.restInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"0",EditTimeConstants.ERROR_CODE,EditTimeConstants.ERROR_MSG+" 1257725",EditTimeConstants.ERROR_CODE,EditTimeConstants.CONFIRM_ERROR_MSG},
				{EditTimeConstants.custInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"0",EditTimeConstants.ERROR_CODE,EditTimeConstants.ERROR_MSG+" 1257725",EditTimeConstants.ERROR_CODE,EditTimeConstants.CONFIRM_ERROR_MSG},
				{EditTimeConstants.defaultInitSource,String.valueOf(itemIds.get(0)),"1",EditTimeConstants.REST_ID,String.valueOf(itemIds.get(1)),"0",EditTimeConstants.ERROR_CODE,EditTimeConstants.ERROR_MSG+" 1257725",EditTimeConstants.ERROR_CODE,EditTimeConstants.CONFIRM_ERROR_MSG},
		};


	}

}
