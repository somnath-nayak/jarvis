package com.swiggy.api.sf.rng.pojo.freedelivery;

import com.swiggy.api.sf.rng.helper.Utility;
import org.apache.commons.lang.time.DateUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CreateFreeDeliveryTdBuilder {

    public CreateFreeDeliveryEntry createFreeDeliveryEntry;

    public CreateFreeDeliveryTdBuilder() {
    	createFreeDeliveryEntry = new CreateFreeDeliveryEntry();
    }

    public CreateFreeDeliveryTdBuilder nameSpace(String nameSpace) {
    	createFreeDeliveryEntry.setNamespace(nameSpace);
        return this;
    }

    public CreateFreeDeliveryTdBuilder header(String header) {
    	createFreeDeliveryEntry.setHeader(header);
        return this;
    }

    public CreateFreeDeliveryTdBuilder description(String description) {
    	createFreeDeliveryEntry.setDescription(description);
        return this;
    }

    public CreateFreeDeliveryTdBuilder valid_from(String valid_from) {
    	createFreeDeliveryEntry.setValidFrom(valid_from);
        return this;
    }

    public CreateFreeDeliveryTdBuilder valid_till(String valid_till) {
    	createFreeDeliveryEntry.setValidTill(valid_till);
        return this;
    }

    public CreateFreeDeliveryTdBuilder campaign_type(String campaign_type) {
    	createFreeDeliveryEntry.setCampaignType(campaign_type);
        return this;
    }

    public CreateFreeDeliveryTdBuilder restaurant_hit(String restaurant_hit) {
    	createFreeDeliveryEntry.setRestaurantHit(restaurant_hit);
        return this;
    }

    public CreateFreeDeliveryTdBuilder enabled(boolean enabled) {
    	createFreeDeliveryEntry.setEnabled(enabled);
        return this;
    }

    public CreateFreeDeliveryTdBuilder swiggy_hit(String swiggy_hit) {
    	createFreeDeliveryEntry.setSwiggyHit(swiggy_hit);
        return this;
    }

    public CreateFreeDeliveryTdBuilder createdBy(String createdBy) {
    	createFreeDeliveryEntry.setCreatedBy(createdBy);
        return this;
    }

    public CreateFreeDeliveryTdBuilder discountLevel(String discountLevel) {
    	createFreeDeliveryEntry.setDiscountLevel(discountLevel);
        return this;
    }

    public CreateFreeDeliveryTdBuilder commissionOnFullBill(boolean commissionOnFullBill) {
    	createFreeDeliveryEntry.setCommissionOnFullBill(commissionOnFullBill);
        return this;
    }

    public CreateFreeDeliveryTdBuilder taxesOnDiscountedBill(boolean taxesOnDiscountedBill) {
    	createFreeDeliveryEntry.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
        return this;
    }

    public CreateFreeDeliveryTdBuilder firstOrderRestriction(boolean firstOrderRestriction) {
    	createFreeDeliveryEntry.setFirstOrderRestriction(firstOrderRestriction);
        return this;
    }

    public CreateFreeDeliveryTdBuilder timeSlotRestriction(boolean timeSlotRestriction) {
    	createFreeDeliveryEntry.setTimeSlotRestriction(timeSlotRestriction);
        return this;
    }

    public CreateFreeDeliveryTdBuilder userRestriction(boolean userRestriction) {
    	createFreeDeliveryEntry.setUserRestriction(userRestriction);
        return this;
    }

    public CreateFreeDeliveryTdBuilder restaurantFirstOrder(boolean restaurantFirstOrder) {
        createFreeDeliveryEntry.setRestaurantFirstOrder(restaurantFirstOrder);
        return this;
    }


    public CreateFreeDeliveryTdBuilder dormant_user_type(String dormant_user_type) {
    	createFreeDeliveryEntry.setDormantUserType(dormant_user_type);
        return this;
    }

    public CreateFreeDeliveryTdBuilder is_surge(Boolean is_surge) {
        createFreeDeliveryEntry.setIs_surge(is_surge);
        return this;
    }
    public CreateFreeDeliveryTdBuilder slots(List<FreeDeliverySlot> slotsDetails) {
        ArrayList<FreeDeliverySlot> slotEntries = new ArrayList<>();
        for (FreeDeliverySlot slotEntry : slotsDetails) {
            slotEntries.add(slotEntry);
        }
        createFreeDeliveryEntry.setSlots(slotEntries);
        return this;
    }

    public CreateFreeDeliveryTdBuilder ruleDiscount(String type, String discountLevel, String minCartAmount) {
    	createFreeDeliveryEntry.setRuleDiscount(new FreeDeliveryRuleDiscount(type, discountLevel, minCartAmount));
        return this;
    }

    public CreateFreeDeliveryTdBuilder restaurantList(List<FreeDeliveryRestaurantList> restaurantLists) {
    	createFreeDeliveryEntry.setRestaurantList(restaurantLists);
        return this;
    }


    public CreateFreeDeliveryEntry build() throws IOException {
        defaultData();
        return createFreeDeliveryEntry;
    }

    public CreateFreeDeliveryTdBuilder withShortDescription(String shortDescription){
        createFreeDeliveryEntry.setShortDescription(shortDescription);
        return this;
    }

   public CreateFreeDeliveryTdBuilder withCopyOverridden(Boolean copyOverridden){
        createFreeDeliveryEntry.setCopyOverridden(copyOverridden);
        return this;
   }

    public CreateFreeDeliveryTdBuilder isSuper(Boolean isSuper) {
        createFreeDeliveryEntry.setIsSuper(isSuper);
        return this;
    }


    private void defaultData() throws IOException {
        if(createFreeDeliveryEntry.getNamespace()==null){
        	createFreeDeliveryEntry.setNamespace("DefaultNamespace");
        }
        if(createFreeDeliveryEntry.getDescription()==null){
        	createFreeDeliveryEntry.setDescription("Default Description");
        }
        if(createFreeDeliveryEntry.getCampaignType()==null){
        	createFreeDeliveryEntry.setCampaignType("FREE_DELIVERY");
        }
        if(createFreeDeliveryEntry.getDiscountLevel()==null){
        	createFreeDeliveryEntry.setDiscountLevel("Restaurant");
        }
        if(createFreeDeliveryEntry.getSlots()==null){
        	createFreeDeliveryEntry.setSlots(new ArrayList<>());
        }
        if(createFreeDeliveryEntry.getHeader()==null){
        	createFreeDeliveryEntry.setHeader("Default Header");
        }
        if(createFreeDeliveryEntry.getCreatedBy()==null){
        	createFreeDeliveryEntry.setCreatedBy( "Default Created By-Manu");
        }
        if(createFreeDeliveryEntry.getUserRestriction()==null){
        	createFreeDeliveryEntry.setUserRestriction(false);
        }
        if(createFreeDeliveryEntry.getTimeSlotRestriction()==null){
        	createFreeDeliveryEntry.setTimeSlotRestriction(false);
        }
        if(createFreeDeliveryEntry.getFirstOrderRestriction()==null){
        	createFreeDeliveryEntry.setFirstOrderRestriction(false);
        }
        if(createFreeDeliveryEntry.getTaxesOnDiscountedBill()==null){
        	createFreeDeliveryEntry.setTaxesOnDiscountedBill(false);
        }
        if(createFreeDeliveryEntry.getCommissionOnFullBill()==null){
        	createFreeDeliveryEntry.setCommissionOnFullBill(false);
        }
        if(createFreeDeliveryEntry.getDormantUserType()==null){
        	createFreeDeliveryEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }
        if(createFreeDeliveryEntry.getValidTill()==null){
          createFreeDeliveryEntry.setValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000));
        }
        if(createFreeDeliveryEntry.getValidFrom()==null){
            createFreeDeliveryEntry.setValidFrom(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000));
          }
        if(createFreeDeliveryEntry.getRestaurantHit()==null)
        {
        	createFreeDeliveryEntry.setRestaurantHit("100");
        }

        if(createFreeDeliveryEntry.getSwiggyHit()==null)
        {
        	createFreeDeliveryEntry.setSwiggyHit("0");
        }

        if(createFreeDeliveryEntry.getEnabled()==null)
        {
        	createFreeDeliveryEntry.setEnabled(true);
        }
        //set values if required...
    }

    public CreateFreeDeliveryTdBuilder createFreedelWithCopy() throws IOException {
        if(createFreeDeliveryEntry.getNamespace()==null){
            createFreeDeliveryEntry.setNamespace("DefaultNamespace");
        }
        if(createFreeDeliveryEntry.getDescription()==null){
            createFreeDeliveryEntry.setDescription("Default Description");
        }
        if(createFreeDeliveryEntry.getCampaignType()==null){
            createFreeDeliveryEntry.setCampaignType("FREE_DELIVERY");
        }
        if(createFreeDeliveryEntry.getDiscountLevel()==null){
            createFreeDeliveryEntry.setDiscountLevel("Restaurant");
        }
        if(createFreeDeliveryEntry.getSlots()==null){
            createFreeDeliveryEntry.setSlots(new ArrayList<>());
        }
        if(createFreeDeliveryEntry.getHeader()==null){
            createFreeDeliveryEntry.setHeader("Default Header");
        }
        if(createFreeDeliveryEntry.getCreatedBy()==null){
            createFreeDeliveryEntry.setCreatedBy( "Ankita-Copy Resolver Automation");
        }
        if(createFreeDeliveryEntry.getUserRestriction()==null){
            createFreeDeliveryEntry.setUserRestriction(false);
        }
        if(createFreeDeliveryEntry.getTimeSlotRestriction()==null){
            createFreeDeliveryEntry.setTimeSlotRestriction(false);
        }
        if(createFreeDeliveryEntry.getFirstOrderRestriction()==null){
            createFreeDeliveryEntry.setFirstOrderRestriction(false);
        }
        if(createFreeDeliveryEntry.getTaxesOnDiscountedBill()==null){
            createFreeDeliveryEntry.setTaxesOnDiscountedBill(false);
        }
        if(createFreeDeliveryEntry.getCommissionOnFullBill()==null){
            createFreeDeliveryEntry.setCommissionOnFullBill(false);
        }
        if(createFreeDeliveryEntry.getDormantUserType()==null){
            createFreeDeliveryEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }
        if(createFreeDeliveryEntry.getValidTill()==null){
            createFreeDeliveryEntry.setValidTill(Utility.getFutureDate());
        }
        if(createFreeDeliveryEntry.getValidFrom()==null){
            createFreeDeliveryEntry.setValidFrom(Utility.getCurrentDate());
        }
        if(createFreeDeliveryEntry.getRestaurantHit()==null)
        {
            createFreeDeliveryEntry.setRestaurantHit("100");
        }

        if(createFreeDeliveryEntry.getSwiggyHit()==null)
        {
            createFreeDeliveryEntry.setSwiggyHit("0");
        }

        if(createFreeDeliveryEntry.getEnabled()==null)
        {
            createFreeDeliveryEntry.setEnabled(true);
        }
        if(createFreeDeliveryEntry.getCopyOverridden()==null)
        {
            createFreeDeliveryEntry.setCopyOverridden(true);
        }
        if(createFreeDeliveryEntry.getIsSuper()==null)
        {
            createFreeDeliveryEntry.setIsSuper(false);
        }


        return this; //set values if required...
    }
}