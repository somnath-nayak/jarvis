package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

public class AddonBuilder {
    private Addon addon;

    public AddonBuilder(){
        addon = new Addon();
    }

    public AddonBuilder choiceId(String choiceId){
        addon.setChoiceId(choiceId);
        return this;
    }

    public AddonBuilder groupId(String groupId){
        addon.setGroupId(groupId);
        return this;
    }

    public AddonBuilder name(String name){
        addon.setName(name);
        return this;
    }

    public AddonBuilder price(Integer price){
        addon.setPrice(price);
        return this;
    }

    public Addon build(){
        return addon;
    }
}
