package com.swiggy.api.sf.rng.pojo.ads.Segmentation;

public class UserSegment {
    String marketingSegment;
    Integer premiumSegment;
    String userId;
    String valueSegment;
    String igccSegment;
    
    public UserSegment(String marketingSegment, String premiumSegment, String userId , String valueSegment ,String igccSegment){
        this.setMarketingSegment(marketingSegment);
        this.setPremiumSegment(Integer.parseInt(premiumSegment));
        this.setUserId(userId);
        this.setValueSegment(valueSegment);
        this.setIgccSegment(igccSegment);
    }
    
    public String getMarketingSegment() {
        return marketingSegment;
    }
    
    public void setMarketingSegment(String marketingSegment) {
        this.marketingSegment = marketingSegment;
    }
    
    public int getPremiumSegment() {
        return premiumSegment;
    }
    
    public void setPremiumSegment(int premiumSegment) {
        this.premiumSegment = premiumSegment;
    }
    
    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getValueSegment() {
        return valueSegment;
    }
    
    public void setValueSegment(String valueSegment) {
        this.valueSegment = valueSegment;
    }
    
    public String getIgccSegment() {
        return igccSegment;
    }
    
    public void setIgccSegment(String igccSegment) {
        this.igccSegment = igccSegment;
    }
    
   
}
