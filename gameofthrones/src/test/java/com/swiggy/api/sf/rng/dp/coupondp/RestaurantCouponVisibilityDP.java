package com.swiggy.api.sf.rng.dp.coupondp;

import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class RestaurantCouponVisibilityDP {


    @DataProvider(name = "createPrivateCouponWithRestaurantMapping")
    public static Object[][] createPrivateCouponWithRestaurantMapping() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withMinAmount(new MinAmount().withCart(1))
                .withCouponUserTypeId(2)
                .withIsPrivate(1)
                .withRestaurantRestriction(1)
                .withCustomerRestriction(1)
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPublicCouponWithWithRestaurantMapping")
    public static Object[][] createPublicCouponWithWithRestaurantMapping() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withMinAmount(new MinAmount().withCart(1))
                .withCouponUserTypeId(0)
                .withIsPrivate(0)
                .withRestaurantRestriction(1)
                .withCustomerRestriction(0)
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }

    @DataProvider(name = "createPublicCouponWithWithRestaurantMappingWithIsPrivate")
    public static Object[][] createPublicCouponWithWithRestaurantMappingWithIsPrivate() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withMinAmount(new MinAmount().withCart(1))
                .withCouponUserTypeId(0)
                .withIsPrivate(1)
                .withRestaurantRestriction(1)
                .withCustomerRestriction(1)
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }
    @DataProvider(name = "createCouponWithoutCustomerRestrictionWithRestaurantMappingWithIsPrivate")
    public static Object[][] createCouponWithoutCustomerRestrictionWithRestaurantMappingWithIsPrivate() {

        HashMap<String, Object> data = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withCityRestriction(0)
                .withMinAmount(new MinAmount().withCart(1))
                .withCouponUserTypeId(0)
                .withIsPrivate(1)
                .withRestaurantRestriction(1)
                .withCustomerRestriction(0)
                .withCouponUserTypeId(0)
                .withDescription("Test123")
                .withCode("VerifyNUX" + Utility.getRandomPostfix());
        data.put("createCouponPOJO", createCouponPOJO);
        return new Object[][]{{data}};
    }
}