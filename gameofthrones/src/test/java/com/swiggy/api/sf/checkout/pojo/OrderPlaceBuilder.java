package com.swiggy.api.sf.checkout.pojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OrderPlaceBuilder {
    private OrderPlace orderPlace;
    CreateMenuEntry createMenuEntry;
    public OrderPlaceBuilder() {

        orderPlace = new OrderPlace();
        createMenuEntry= new CreateMenuEntry();

    }

    public OrderPlaceBuilder cart(List<Cart> cartDetails) {
        ArrayList<Cart> cartEntries = new ArrayList<>();
        for (Cart cartEntry : cartDetails) {
            cartEntries.add(cartEntry);
        }
        createMenuEntry.setCartItems(cartEntries);
        return this;
    }

    public OrderPlaceBuilder restaurant(Integer restaurant) {
        createMenuEntry.setRestaurantId(restaurant);
        return this;
    }

    public OrderPlaceBuilder Address(Integer Address) {
        orderPlace.setAddressId(Address);
        return this;
    }

    public OrderPlaceBuilder paymentMethod(String paymentCodMethod) {
        orderPlace.setPaymentCodMethod(paymentCodMethod);
        return this;
    }

    public OrderPlaceBuilder orderComments(String orderComments) {
        orderPlace.setOrderComments(orderComments);
        return this;
    }


    public OrderPlace build() throws IOException {

        return orderPlace;
    }




}
