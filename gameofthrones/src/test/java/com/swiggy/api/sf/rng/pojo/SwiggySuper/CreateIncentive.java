package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import java.util.HashMap;

public class CreateIncentive {
	
	
	private String title="Automated_Incentive";
	
	private String description="Automated_Incentive_Description_Test";
	
	private String name="Auto";
	
	private String logo_id="z1d3z4ws42";
	
	private String type="PERCENTAGE";
	
	private String value="10";
	
	private String cap="15";
	
	private String valid_payment_methods=null;
	
	private String tenure="-1";
	
	private String priority="10";
	
	private String enabled="1";
	
	private String created_by="By-Super-Atomation";
	
	public CreateIncentive() {
		
	}
	
	public CreateIncentive(String type, String value, String cap, String paymentMethods, String tenure,
			String priority, String  enabled) {
	
		this.type= type;
		this.value= value;
		this.cap= cap;
		this.valid_payment_methods= paymentMethods;
		this.tenure= tenure;
		this.priority= priority;
		this.enabled= enabled;
		
	}
	
	public CreateIncentive(HashMap<String, String> incentivePayload) {
		this. title=incentivePayload.get("0");
		 this.description=incentivePayload.get("1");
		 this.name=incentivePayload.get("2");
		 this.logo_id=incentivePayload.get("3");
		this. type=incentivePayload.get("4");
		 this.value=incentivePayload.get("5");
		 this.cap=incentivePayload.get("6");
		 this.valid_payment_methods=incentivePayload.get("7");
		 this.tenure=incentivePayload.get("8");
		 this.priority=incentivePayload.get("9");
		this. enabled=incentivePayload.get("10");
		 this.created_by=incentivePayload.get("11");
	}
	
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title= title;
	}
	public String getDescription() {
		return this.description;
	}
	public void setdescription(String description) {
		this.description= description;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name= name;
	}
	public String getLogo_id() {
		return this.logo_id;
	}
	public void setLogo_id(String logo_id) {
		this.logo_id= logo_id;
	}
	public String getType() {
		return this.type;
	}
	public void setType(String type) {
		this.type= type;
	}
	public String getValue() {
		return this.value;
	}
	public void setValue(String value) {
		this.value= value;
	}
	public String getCap() {
		return this.cap;
	}
	public void setCap(String cap) {
		this.cap= cap;
	}
	public String getValid_payment_methods() {
		return this.valid_payment_methods;
	}
	public void setValid_payment_methods(String valid_payment_methods) {
		this.valid_payment_methods= valid_payment_methods;
	}
	public String getTenure() {
		return this.tenure;
	}
	public void setTenure(String tenure) {
		this.tenure= tenure;
	}
	public String getPriority() {
		return this.priority;
	}
	public void setPriority(String priority) {
		this.priority= priority;
	}
	public String getEnabled() {
		return this.enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled= enabled;
	}
	public String getCreated_by() {
		return this.created_by;
	}
	public void setCreated_by(String created_by) {
		this.created_by= created_by;
	}
	
	@Override
	public String toString() {
		
		return "{" 
		 
				 + "\"title\"" + ":" + "\"" + title + "\"" + ","
				 + "\"description\"" + ":" + "\"" +description + "\"" + ","
				 + "\"name\"" + ":" + "\"" + name + "\"" + ","
				 + "\"logo_id\"" + ":" + "\"" + logo_id + "\"" + ","
				 + "\"meta\"" + ":" 
				 +  "{"
				 + "\"type\"" + ":" + "\"" + type + "\"" + ","
				 + "\"value\"" + ":" + value + ","
				 + "\"cap\""  + ":"+ cap  + ","
				 + "\"valid_payment_methods\"" + ":" + valid_payment_methods  
				
				 + "},"
                 + "\"tenure\"" + ":" + tenure + ","
                 + "\"priority\"" + ":" + priority + ","
                 + "\"enabled\"" + ":" + enabled + ","
                  + "\"created_by\"" + ":" + "\"" + created_by+ "\"" 
                  
                  + "}"
				
				
				
				
				
				
				
				;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	