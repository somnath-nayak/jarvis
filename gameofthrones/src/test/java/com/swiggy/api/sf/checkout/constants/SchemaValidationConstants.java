package com.swiggy.api.sf.checkout.constants;

public interface SchemaValidationConstants {

    String REST_ID = "19669";
    String CART_TYPE_REGULAR = "REGULAR";

    String MOBILE = "8197982351";
    String PASSWORD = "welcome123";
    String SCHEMA_PATH="/../Data/SchemaSet/Json/Checkout/";
    String PAAS_SCHEMA_PATH="/../Data/SchemaSet/Json/Paas/";

    
    String UPDATE_CART_V2_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/regularCartWithoutTDCouponAndroid.txt"; //For cart without td and without coupon
    String UPDATE_CART_V2_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/regularCartWithoutTDCouponWeb.txt"; //For cart without td and without coupon
    String UPDATE_CART_V2_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/regularCartWithoutTDCouponIOS.txt"; //For cart without td and without coupon

    String CHECKTOTALS_V2_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkTotalsWithoutTDCouponAndroid.txt"; //For cart without td and without coupon
    String CHECKTOTALS_V2_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkTotalsWithoutTDCouponWeb.txt"; //For cart without td and without coupon
    String CHECKTOTALS_V2_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkTotalsWithoutTDCouponIOS.txt"; //For cart without td and without coupon

    String FETCHPRICING_V2_RESPONSE_ANDROID_SCHEMA_01 = "SCHEMA_PATH/fetchPricingAndroid.txt"; //For cart

    String CHECKBANKLISTALL_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkBankListAllAndroid.txt"; //For cart without td and without coupon
    String CHECKBANKLISTALL_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkBankListAllWeb.txt"; //For cart without td and without coupon
    String CHECKBANKLISTALL_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkBankListAllIOS.txt"; //For cart without td and without coupon

    String CHECKGETINVENTORY_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetInventoryAndroid.txt"; //For cart without td and without coupon
    String CHECKGETINVENTORY_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetInventoryWeb.txt"; //For cart without td and without coupon
    String CHECKGETINVENTORY_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetInventoryIOS.txt"; //For cart without td and without coupon

    String CHECKCARDSJUSPAY_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCardsJuspayAndroid.txt"; //For cart without td and without coupon
    String CHECKCARDSJUSPAY_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCardsJuspayWeb.txt"; //For cart without td and without coupon
    String CHECKCARDSJUSPAY_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCardsJuspayIOS.txt"; //For cart without td and without coupon

    String CHECKAPPLYCOUPON_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkApplyCouponAndroid.txt"; //For cart without td and without coupon
    String CHECKAPPLYCOUPON_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkApplyCouponWeb.txt"; //For cart without td and without coupon
    String CHECKAPPLYCOUPON_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkApplyCouponIOS.txt"; //For cart without td and without coupon

    String CHECKREMOVECOUPON_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkRemoveCouponAndroid.txt"; //For cart without td and without coupon
    String CHECKREMOVECOUPON_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkRemoveCouponWeb.txt"; //For cart without td and without coupon
    String CHECKREMOVECOUPON_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkRemoveCouponIOS.txt"; //For cart without td and without coupon


    String CHECKCANCELLATIONFEEREVERT_V1_RESPONSE_SCHEMA_01="/Data/SchemaSet/Json/Checkout/checkCancellationFeeRevert.txt";

    String CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkSettingsWalletEmailUpdateURLAndroid.txt"; //For cart without td and without coupon
    String CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkSettingsWalletEmailUpdateURLWeb.txt"; //For cart without td and without coupon
    String CHECKSETTINGSWALLETEMAILUPDATEURL_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkSettingsWalletEmailUpdateURLIOS.txt"; //For cart without td and without coupon

    String CHECKGETALLPAYMENT_V1_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetAllPaymentAndroid.txt"; //For cart without td and without coupon
    String CHECKGETALLPAYMENT_V1_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetAllPaymentWeb.txt"; //For cart without td and without coupon
    String CHECKGETALLPAYMENT_V1_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkGetAllPaymentIOS.txt"; //For cart without td and without coupon

    String CHECKCAFEUPDATECART_V2_RESPONSE_ANDROID_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCafeUpdateCartAndroid.txt"; //For cafe cart
    String CHECKCAFEUPDATECART_V2_RESPONSE_WEB_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCafeUpdateCartWeb.txt"; //For cafe cart
    String CHECKCAFEUPDATECART_V2_RESPONSE_IOS_SCHEMA_01 = "/Data/SchemaSet/Json/Checkout/checkCafeUpdateCartIOS.txt"; //For cafe cart

 }
