package com.swiggy.api.sf.snd.dp;


import framework.gameofthrones.Aegon.PropertiesHandler;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.snd.constants.SANDConstants;

public class EditOrderDp implements SANDConstants {

    String Env;
    PropertiesHandler properties = new PropertiesHandler();


    public EditOrderDp() {
        if (System.getenv("ENVIRONMENT") == null)
            Env = properties.propertiesMap.get("environment");
        else
            Env = System.getenv("ENVIRONMENT");
    }

    @DataProvider(name = "EditOrderMenu")
    public Object[][] EditOrderMenu(){
        return new Object[][] {
                {"223", "12.92", "77.62","1001"}
        };
    }

    @DataProvider(name = "EditOrderMenuInvalidRest")
    public Object[][] EditOrderMenuInvalidRest(){
        return new Object[][] {
                {"00", "12.92", "77.62","1001"}
        };
    }

    @DataProvider(name = "EditOrderMenuInvalidOrder")
    public Object[][] EditOrderMenuInvalidOrder(){
        return new Object[][] {
                {"223", "12.92", "77.62","00"}
        };
    }

    @DataProvider(name = "EditOrderMenuvalidOrder")
    public Object[][] EditOrderMenuvalidOrder(){
        return new Object[][] {
                {"223", "12.92", "77.62","1001"}
        };
    }

    @DataProvider(name = "EditOrderMenuNonServiceable")
    public Object[][] EditOrderMenuNonServiceable(){
        return new Object[][] {
                {"223", "28.714321", "77.076856","1001"}
        };
    }

    @DataProvider(name = "SmartSuggestions")
    public Object[][] SmartSuggestions(){

        String items = " 545197";

        return new Object[][] {
                {"12.92", "77.62","REGULAR_MENU","223",items}
        };
    }

    @DataProvider(name = "SmartSuggestionsInvalid")
    public Object[][] SmartSuggestionsInvalid(){

        String items = "545197";
        String items1 = "545197111111";

        return new Object[][] {
                {"12.92", "77.62","REGULAR_MENU","2231111",items},{"12.92", "77.62","REGULAR_MENU","2231111",items1}
        };
    }

    @DataProvider(name = "SmartSuggestionsCache")
    public Object[][] SmartSuggestionsCache(){

        String items1 = "545197111111";

        return new Object[][] {
                {"12.92", "77.62","REGULAR_MENU","223",items1}
        };
    }

    @DataProvider(name = "SmartSuggestionsInvalidMenuType")
    public Object[][] SmartSuggestionsInvalidMenuType(){

        String items = "545197";

        return new Object[][] {
                {"12.92", "77.62","xyz","223",items}
        };
    }

}

