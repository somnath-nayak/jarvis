package com.swiggy.api.sf.snd.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class OrderabilityInfoHelper {

    Initialize gameofthrones = new Initialize();

    SnDHelper snDHelper = new SnDHelper();

    public Processor getOrderabilityInfo(String tid, String token, String lat, String lng)
    {

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token", token);
        GameOfThronesService service = new GameOfThronesService("sand", "orderability", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { lat,lng});
        return processor;
    }

    public HashMap<String,String> getTidToken(String mobile, String password){

        Processor processor = snDHelper.consumerLogin(mobile, password);
        String tid = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..tid");
        String token = processor.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.token");


        HashMap<String,String> hashMap = new HashMap<>();
        hashMap.put("tid",tid);
        hashMap.put("token",token);
        return hashMap;
    }

    public Processor getOrderabilityInfo(String lat, String lng)
    {

        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "orderability", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { lat,lng});
        return processor;
    }



}
