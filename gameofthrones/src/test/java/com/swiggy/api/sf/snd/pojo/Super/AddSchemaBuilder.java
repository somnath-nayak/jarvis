package com.swiggy.api.sf.snd.pojo.Super;

public class AddSchemaBuilder {

    private AddSc addSc;
    public AddSchemaBuilder()
    {
        addSc=new AddSc();
    }

    public AddSchemaBuilder key(String key) {
        addSc.setKey(key);
        return this;
    }

    public AddSchemaBuilder jsonSchema(String schema) {
        addSc.setJsonSchema(schema);
        return this;
    }

    public AddSchemaBuilder author(String auth) {
        addSc.setAuthor(auth);
        return this;
    }

    public AddSchemaBuilder schmeaVersion(String version) {
        addSc.setSchemaVersion(version);
        return this;
    }
    public AddSchemaBuilder defaultValue(String value) {
        addSc.setDefaultValue(value);
        return this;
    }

    public AddSc build() {
        return addSc;
    }
}
