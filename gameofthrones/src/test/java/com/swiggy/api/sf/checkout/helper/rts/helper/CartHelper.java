package com.swiggy.api.sf.checkout.helper.rts.helper;

import com.swiggy.api.sf.checkout.constants.RTSConstants;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOMealHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartFromRedis.CartFromRedis;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.CartItem;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.Address;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.checkout.helper.rts.validator.GenericValidator;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class CartHelper {
    String TID = null;
    String TOKEN = null;
    String mobile = null;
    boolean isSessionActive = false;
    HashMap<String, String> headers = new HashMap<>();
    GenericValidator genericValidator = new GenericValidator();
    RngHelper rngHelper = new RngHelper();

    // Using EDVOHelper to create or reuse the API helpers. Also, to avoid multiple calls to Initialize class
    EDVOCartHelper edvoCartHelper = new EDVOCartHelper();


    /**
     * Returns the cart payload based on the parameters provided
     *
     * @param restaurantId
     * @param isCartItemRequired
     * @param cartItemQuantity
     * @param isAddonRequiredForCartItems
     * @param isMealItemRequired
     * @param mealItemQuantity
     * @param isAddonRequiredForMealItems
     * @param mealIds
     * @param cartType
     * @return
     */
    public Cart getCartPayload(String restaurantId,
                               boolean isCartItemRequired,
                               int cartItemQuantity,
                               boolean isAddonRequiredForCartItems,
                               boolean isMealItemRequired,
                               int mealItemQuantity,
                               boolean isAddonRequiredForMealItems,
                               String[] mealIds,
                               String cartType) {

        return edvoCartHelper.getCartPayload(restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
    }

    //Update cart with string payload and get the response as processor
    public Processor updateCartV2(String payload, HashMap<String, String> headers) {
        return edvoCartHelper.createCartV2(payload, headers);
    }

    //Update cart with string payload and get the response as CartV2Response pojo object
    public CartV2Response updateCart(String payload, HashMap<String, String> headers) {
        Processor processor = edvoCartHelper.createCartV2(payload, headers);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), CartV2Response.class);
    }

    //Returns serviceable Address instance from Cart-update response instance
    public Address getServiceableAddressFromCartResponse(CartV2Response cartV2Response){
        List<Address> addresses = cartV2Response.getData().getAddresses();
        for(Address address : addresses) {
            if(address.getDeliveryValid()==1 && address.getRainMode()==0){
                return address;
            }
        }
        return null;
    }

    //Reserve Cart internally
    public Processor reserveCart(CartV2Response cartV2Response){
        Address address = getServiceableAddressFromCartResponse(cartV2Response);
        if(address==null){
            edvoCartHelper.addNewAddressFromCartV2Response(cartV2Response);
        }
        return reserveCart(address.getId(), String.valueOf(address.getEstimatedSla()), headers);
    }

    //Reserve cart (Does not support Group Orders)
    public Processor reserveCart(String addressId, String sla, HashMap<String, String> headers) {
        return edvoCartHelper.reserveCart(addressId, sla, headers);
    }

    //Unreserves the reserved cart with provided headers
    public Processor unreserveCart(HashMap<String, String> headers) {
        return edvoCartHelper.unreserveCart(headers);
    }

    //Unreserves the reserved cart for default user
    public Processor unreserverCart(){
        return unreserveCart(headers);
    }

    //Login as consumer and get the response as processor
    public Processor loginV2(String mobile, String password) {
        SANDHelper sandHelper = new SANDHelper();
        return sandHelper.login1(mobile, password);
    }

    //Getting headers for new session
    public HashMap<String, String> getHeaders(String mobile, String password) {
        Processor processor = loginV2(mobile, password);

        //Checking if login attempt is success or not
        genericValidator.smokeCheck(0, "done successfully",
                processor, "LOGIN V2 FAILED...!!");

        //Initializing TID and TOKEN with new session
        TOKEN = processor.ResponseValidator.GetNodeValue("$.data.token");
        TID = processor.ResponseValidator.GetNodeValue("$.tid");
        headers.put("tid", TID);
        headers.put("token", TOKEN);

        headers.put("Authorization", RTSConstants.AUTHORIZATION);
        headers.put("Content-Type", RTSConstants.CONTENT_TYPE);
        headers.put("version-code", RTSConstants.VERSION_CODE);
        headers.put("User-Agent", RTSConstants.USER_AGENT);
        headers.put("swuid", RTSConstants.SWUID);

        //Setting isSessionActive value as true and this.mobile as mobile,
        // so it can be reused in getHeadersSingleton to avoid multiple login attempts for same user
        isSessionActive = true;
        this.mobile = mobile;

        return headers;
    }

    //Getting headers for existing session if user is already in session, else getting headers for new session
    public HashMap<String, String> getHeadersSingleton(String mobile, String password) {
        if (isSessionActive && this.mobile.equals(mobile)) {
            //If isSessionActive is true for same mobile which means the user is already logged in,
            // then returning the previous session headers to avoid multiple login attempts
            return headers;
        } else {
            //If isSessionActive is not true, then attempting a new login to create a new session and returning the headers
            return getHeaders(mobile, password);
        }
    }

    //Update cart and get the CartV2Response processor
    public Processor updateCartV2(String mobile,
                                  String password,
                                  String restaurantId,
                                  boolean isCartItemRequired,
                                  int cartItemQuantity,
                                  boolean isAddonRequiredForCartItems,
                                  boolean isMealItemRequired,
                                  int mealItemQuantity,
                                  boolean isAddonRequiredForMealItems,
                                  String[] mealIds,
                                  String cartType){
        Cart cart = getCartPayload(restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);

        String payload = Utility.jsonEncode(cart);
        return updateCartV2(payload, getHeadersSingleton(mobile, password));
    }

    //Update Cart and get the CartV2Response pojo object
    public CartV2Response updateCart(String mobile,
                                     String password,
                                     String restaurantId,
                                     boolean isCartItemRequired,
                                     int cartItemQuantity,
                                     boolean isAddonRequiredForCartItems,
                                     boolean isMealItemRequired,
                                     int mealItemQuantity,
                                     boolean isAddonRequiredForMealItems,
                                     String[] mealIds,
                                     String cartType){
        Processor processor = updateCartV2(mobile,
                password,
                restaurantId,
                isCartItemRequired,
                cartItemQuantity,
                isAddonRequiredForCartItems,
                isMealItemRequired,
                mealItemQuantity,
                isAddonRequiredForMealItems,
                mealIds,
                cartType);
        return Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(),
                CartV2Response.class);
    }

    //Flush/Delete cart for current user
    public Processor flushCart(){
        return edvoCartHelper.flushCart(headers);
    }

    //Returns the cart data from redis for the key 'user_cart_MOBILE'
    public String getRedisData(String keyInitial, String entity, int redisDB){
        RedisHelper redisHelper =new RedisHelper();
        String redisKey = keyInitial.concat(entity);
        return (String) redisHelper.getValue("checkoutredisstage", redisDB, redisKey);
    }

    //Returns the Order-Id from redis key "user_cart_MOBILE"
    public Long getOrderIdFromRedisCart(String mobile){
        CartFromRedis cartFromRedis = Utility.jsonDecode(getRedisData("user_cart_", mobile, 0), CartFromRedis.class);
        return cartFromRedis.getOrderId();
    }

    //Returns the Cart-Id from redis key "user_cart_MOBILE"
    public Integer getCartIdFromRedisCart(String mobile){
        CartFromRedis cartFromRedis = Utility.jsonDecode(getRedisData("user_cart_", mobile, 0), CartFromRedis.class);
        return cartFromRedis.getId();
    }

    //Creates Free Delivery Trade Discount for provided Restaurant ID
    public HashMap<String, String> createFreeDelTD(String minCartAmount, String restId, boolean firstOrderRestriction,
                                                   boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type, boolean isSurge){
        HashMap<String, String> keys = new HashMap<>();
        try {
            keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, firstOrderRestriction,
                    restaurantFirstOrder, userRestriction, dormant_user_type, isSurge);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keys;
    }

    //Returns the Item-Id from a restaurant to create a "FREEBIE" TD
    public String getItemIdForFreebie(String restaurantId){
        List<CartItem> cartItems = edvoCartHelper.getCartItems(restaurantId, 1, false, true);
        return cartItems.get(0).getMenuItemId();
    }

    //Creates Freebie Trade Discount for provided Restaurant ID and provided Item ID
    public HashMap<String, String> createFreebieTD(String minCartAmount, String restId,
                                                   String freebieItemId){
        HashMap<String, String> keys = new HashMap<>();
        try {
            keys = rngHelper.createFeebieTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, freebieItemId);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return keys;
    }

    //Creates Trade Discount based on the parameter provided
    public HashMap<String, String> createOrUpdateTradeDiscountForRTS(String restaurantId, String tdType){
        HashMap<String, String> keys = new HashMap<>();
        rngHelper.disabledActiveTD(restaurantId);
        if(tdType.equals(RTSConstants.FREE_DELIVERY)) {
            keys = createFreeDelTD(RTSConstants.MIN_CART_AMOUNT, restaurantId, RTSConstants.FIRST_ORDER_RESTRICTION,
                    RTSConstants.RESTAURANT_FIRST_ORDER, RTSConstants.USER_RESTRICTION, RTSConstants.DORMANT_USER_TYPE,
                    RTSConstants.SURGE);
        }else if(tdType.equals(RTSConstants.FREEBIE)){
            keys = createFreebieTD(RTSConstants.MIN_CART_AMOUNT, restaurantId, getItemIdForFreebie(restaurantId));
            Utility.wait(RTSConstants.FREEBIE_CACHE_DELAY);
            System.out.println("FREEBIE Created Successfully...");
        }
        return keys;
    }

    public Processor createOrUpdateMealTD(String restaurantId, String mealId, String tdType){
        EDVOMealHelper edvoMealHelper = new EDVOMealHelper();
        return edvoMealHelper.createOrUpdateMealTD(Integer.parseInt(restaurantId),Integer.parseInt(mealId),tdType);
    }

}
