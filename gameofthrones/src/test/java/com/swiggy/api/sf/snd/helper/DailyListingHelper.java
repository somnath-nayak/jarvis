package com.swiggy.api.sf.snd.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.snd.constants.DailySandConstants;
import com.swiggy.api.sf.snd.constants.POPConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.tomcat.jni.Proc;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.helper
 **/
public class DailyListingHelper {

    public static Initialize init = new Initialize();
   // WireMockHelper wireMockHelper = new WireMockHelper();
    DateHelper dateHelper = new DateHelper();
    RedisHelper redisHelper =  new RedisHelper();
    DailyGeoHashUtilityHelper dailyGeoHashUtilityHelper = new DailyGeoHashUtilityHelper();

    public HashMap<String, String> defaultheaders() {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("tid", DailySandConstants.dummy_tid);
        hm.put("user-agent",DailySandConstants.userAgent);
        return hm;
    }

    //Overload
    public HashMap<String, String> defaultheaders(String sid, String tid) {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type", "application/json");
        hm.put("sid", sid);
        hm.put("tid",tid);
        hm.put("user-agent",DailySandConstants.userAgent);
        return hm;
    }

    public HashMap<String, String> catalogHeaders() {
        HashMap<String, String> hm = new HashMap<>();
        hm.put("Content-Type","application/json");
        hm.put("Authorization",DailySandConstants.authentication);
        hm.put("tenant-id",DailySandConstants.tenantId);
        return hm;
    }

    public Processor login(String mobile, String password) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] payload = {mobile, password};
        GameOfThronesService gots = new GameOfThronesService("sanduserdaily", "loginV2", init);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }

    public Processor createDailyMealSlotsHelper (String cityId, String slotName, String startTime, String endTime) {
        String[] payload = {cityId,slotName,startTime,endTime};
        GameOfThronesService gots = new GameOfThronesService("mealslotservice", "createMealSlotService", init);
        Processor p = new Processor(gots, defaultheaders(), payload);
        return p;
    }

    public Processor deleteMealSlotByCityId(String cityId) {
        String[] params = {cityId};
        GameOfThronesService gots = new GameOfThronesService("mealslotservice", "deleteMealSlotByCityId", init);
        Processor p = new Processor(gots, defaultheaders(), null,params);
        return p;
    }

    public Processor dailyMealAggregator(String lat, String lng, String slot, String offset, String sid, String tid ) {
        HashMap<String, String> hm = new HashMap<>();
        if (tid.equals("") || sid.equals("")) {
            hm = defaultheaders();
        } else
            hm = defaultheaders(sid,tid);
        if (slot.equals("") && offset.equals("")){
            String[] params = {lat, lng};
            GameOfThronesService gots = new GameOfThronesService("sanddaily", "mealListingWithoutSlotOffset", init);
            Processor p = new Processor(gots, hm, null,params);
            return p;
        } else {
            String[] params = {lat, lng, slot, offset};
            GameOfThronesService gots = new GameOfThronesService("sanddaily", "mealListingWithSlotOffset", init);
            Processor p = new Processor(gots, hm, null,params);
            return p;
        }
    }

    public Processor dailyPlanAggregator(String lat, String lng, String slot, String offset, String sid, String tid ) {
        HashMap<String, String> hm = new HashMap<>();
        if (tid.equals("") || sid.equals("")) {
            hm = defaultheaders();
        } else
            hm = defaultheaders(sid,tid);
        if (slot.equals("") && offset.equals("")){
            String[] params = {lat, lng};
            GameOfThronesService gots = new GameOfThronesService("sanddaily", "planListingWithoutSlotOffset", init);
            Processor p = new Processor(gots, hm, null,params);
            return p;
        } else {
            String[] params = {lat, lng, slot, offset};
            GameOfThronesService gots = new GameOfThronesService("sanddaily", "planListingWithSlotOffset", init);
            Processor p = new Processor(gots, hm, null,params);
            return p;
        }
    }

    public Processor getDailyMealSlotsHelper(String cityId, String time) {
        String[] params = {cityId, time};
        GameOfThronesService gots = new GameOfThronesService("mealslotservice", "getMealSlotByCityId", init);
        Processor p = new Processor(gots, defaultheaders(), null, params);
        return p;
    }

    public HashMap<String, String> getDefaultMealSlot(String cityId, String time) {
        String dailyMeals = getDailyMealSlotsHelper(cityId, time).ResponseValidator.GetBodyAsText();
        HashMap<String, String> hm = new HashMap<>();
        int status = JsonPath.read(dailyMeals,"$.statusCode");
        if(status == DailySandConstants.statusOne) {
            String default_slotName = JsonPath.read(dailyMeals, "$.data[0].slot").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String start_time = JsonPath.read(dailyMeals, "$.data[0].start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String end_time = JsonPath.read(dailyMeals, "$.data[0].end_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String delivery_start_time = JsonPath.read(dailyMeals, "$.data[0].delivery_start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String day = JsonPath.read(dailyMeals, "$.data[0].day").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_date = JsonPath.read(dailyMeals, "$.data[0].slot_date").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");

            hm.put("slot", default_slotName);
            hm.put("start_time", start_time);
            hm.put("end_time", end_time);
            hm.put("delivery_start_time", delivery_start_time);
            hm.put("day", day);
            hm.put("slot_date",slot_date);

        }
        return hm;
    }

    //To be used when the default slot does not have any other delivery slots - flipping scenario
    public HashMap<String, String> getNextDefaultMealSlot(String cityId, String time) {
        String dailyMeals = getDailyMealSlotsHelper(cityId, time).ResponseValidator.GetBodyAsText();
        HashMap<String, String> hm = new HashMap<>();
        int status = JsonPath.read(dailyMeals,"$.statusCode");
        if(status == DailySandConstants.statusOne) {
            String default_slotName = JsonPath.read(dailyMeals, "$.data[1].slot").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String start_time = JsonPath.read(dailyMeals, "$.data[1].start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String end_time = JsonPath.read(dailyMeals, "$.data[1].end_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String delivery_start_time = JsonPath.read(dailyMeals, "$.data[1].delivery_start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String day = JsonPath.read(dailyMeals, "$.data[1].slot_date").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_date = JsonPath.read(dailyMeals, "$.data[1].slot_date").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            hm.put("slot", default_slotName);
            hm.put("start_time", start_time);
            hm.put("end_time", end_time);
            hm.put("delivery_start_time", delivery_start_time);
            hm.put("day", day);
            hm.put("slot_date",slot_date);
        }
        return hm;
    }



    public HashMap<String, String> getSlotDetailOfGivenSlotIfExists(String cityId, String epochtime, String slotName) {
        String dailyMeals = getDailyMealSlotsHelper(cityId, epochtime).ResponseValidator.GetBodyAsText();
        HashMap<String, String> hm = new HashMap<>();
        int status = JsonPath.read(dailyMeals,"$.statusCode");
        String[] get_all_available_slots = JsonPath.read(dailyMeals, "$.data[*].slot").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        Arrays.sort(get_all_available_slots);
        int if_exists = Arrays.binarySearch(get_all_available_slots,slotName);
        if(status == DailySandConstants.statusOne && if_exists >= 0) {
            String slot_day = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].day").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String start_time = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String end_time = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].end_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String delivery_start_time = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].delivery_start_time").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_city_id = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].city_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_id = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_display_name = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].slot_display_name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            String slot_date = JsonPath.read(dailyMeals, "$.data[?(@.slot=='"+slotName+"')].slot_date").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
            hm.put("day", slot_day);
            hm.put("start_time", start_time);
            hm.put("end_time", end_time);
            hm.put("delivery_start_time", delivery_start_time);
            hm.put("city_id", slot_city_id);
            hm.put("id",slot_id);
            hm.put("slot_display_name",slot_display_name);
            hm.put("slot_date",slot_date);

        }
        return hm;
    }

    //Mocking Stuff
   /* public Processor addMapping(String uri, String method, String body){
        HashMap<String,String> headers = new HashMap<String,String>(){{put("Content-Type","application/json");}};
        GameOfThronesService service = new GameOfThronesService("mock", "addmapping", init);
        body = StringEscapeUtils.escapeJava(body);
        String[] payloadparams = new String[] { uri,method,body };
        return  new Processor(service,headers , payloadparams);

    }

    public void setMockRequest(String uri, String fileName) {
        File file = new File("../Data/MockAPI/ResponseBody/rng/gandalf/" + fileName);
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
//			wireMockHelper.setupStubPost(uri,200,"application/json",body,0);
            Processor processor = addMapping("/" + uri, "POST", body);
            System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setMockGetRequest(String uri, String fileName) {
        File file = new File("../Data/MockAPI/ResponseBody/rng/gandalf/" + fileName);
        String body = null;
        try {
            body = FileUtils.readFileToString(file);
            wireMockHelper.setupStub(uri, 200, "application/json", body, 0);
            Processor processor = addMapping("/" + uri, "GET", body);
            System.out.println("mocking uri ::" + uri + "\n with this response body :: " + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

   public Processor deliveryListingHelper (String json) {
       String[] payload = {json};
       GameOfThronesService gots = new GameOfThronesService("cerebrodaily", "deliveryListing", init);
       Processor p = new Processor(gots, defaultheaders(), payload);
       return p;
   }

   public Processor updateSlotForExistingBlackZoneHelper(String id, String zoneId, String startTime, String endTime, String enabled) {
       String[] params = {id};
       String[] payload = {zoneId,startTime,endTime,enabled};
       GameOfThronesService gots = new GameOfThronesService("deliveryservicedaily", "updateZoneSlot", init);
       Processor p = new Processor(gots,defaultheaders(),payload,params);
       return p;
   }

   public HashMap<String, String> getDailyBlackZoneDetailsFromZoneId(String zoneId) {
       HashMap<String, String> hm = new HashMap();
       String[] params = {zoneId};
       GameOfThronesService gots = new GameOfThronesService("deliveryservicedaily", "getBlackZoneDetails", init);
       Processor p = new Processor(gots, defaultheaders(), null,params);
       int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
       if(status == 0) {
           String id = p.ResponseValidator.GetNodeValue("$.data.id");
           String startTime = p.ResponseValidator.GetNodeValue("$.data.startTime");
           String endTime = p.ResponseValidator.GetNodeValue("$.data.endTime");
           String enabled = p.ResponseValidator.GetNodeValue("$.data.enabled");
           hm.put("id",id);
           hm.put("startTime",startTime);
           hm.put("endTime",endTime);
           hm.put("enabled",enabled);
           return hm;
       }
       else return hm;
   }

   public HashMap<String, String> setDeliveryBlackZoneTimeSlots(String bz_lat, String bz_lng, String zoneId) {
       HashMap<String, String> hm;
       HashMap<String, String> hm_response = new HashMap<>();
       //currentTime = currentTime+2 min
       String currentTime = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(2);
       String update_startTime = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(-1);
       String update_endTime = dateHelper.getMinutesPlusMinusCurrentTimeInHundredHours(29);
       String update_enabled = "1";
       hm = getDailyBlackZoneDetailsFromZoneId(zoneId);
       if (hm.size() > 0 && hm.get("enabled").equals(update_enabled)) {
           String get_startTime = hm.get("startTime");
           String get_endTime = hm.get("endTime");
           // if in slot
           if (Integer.parseInt(currentTime) >= Integer.parseInt(get_startTime) && Integer.parseInt(currentTime) <= Integer.parseInt(get_endTime)) {
               hm_response.put("startTime", get_startTime);
               hm_response.put("endTime", get_endTime);
               hm_response.put("enabled", hm.get("enabled"));
               return hm_response;
           } else {
               Processor p = updateSlotForExistingBlackZoneHelper(hm.get("id"), zoneId, update_startTime, update_endTime, update_enabled);
               int status = p.ResponseValidator.GetNodeValueAsInt("$.statusCode");
               if (status == 0) {
                   hm_response.put("startTime", update_startTime);
                   hm_response.put("endTime", update_endTime);
                   hm_response.put("enabled", update_enabled);
                   //sleep is required when updating as there is 10 sec cache in delivery
                   try {
                       Thread.sleep(10000);
                   } catch (InterruptedException e) {
                       e.printStackTrace();
                   }
                   return hm_response;
               } else {
                   hm_response.put("startTime", "");
                   hm_response.put("endTime", "");
                   hm_response.put("enabled", "");
               }
           }
       }
       return hm_response;
   }

   public Processor getDeliverySlotsHelper(String lat, String lng, String time_slot_begin, String start_time, String end_time) {
       String[] payload = {lat, lng,time_slot_begin,start_time,end_time};
       GameOfThronesService gots = new GameOfThronesService("deliveryservicedaily", "getDeliverySlots", init);
       Processor p = new Processor(gots,defaultheaders(),payload);
       return p;
   }

   public Processor dailyMealAggregatorWithOptionalParams(String lat, String lng, HashMap<String, String> optional_params, String sid, String tid) {
        String opt_params = "";
        if(!optional_params.isEmpty()) {
            for (Map.Entry<String, String> entry:optional_params.entrySet()) {
                opt_params = opt_params +"&amp;" + entry.getKey().replace(" ","%20") + "=" + entry.getValue().replace(" ","%20");
            }
        } else
            System.out.println("Optional params empty");
        System.out.println("-- OPT_PARAMS STRING ::: " +opt_params);

        String[] params = {lat,lng,opt_params};
        GameOfThronesService gots = new GameOfThronesService("sanddaily", "mealListingWithOptionalParams", init);
        Processor p = new Processor(gots, defaultheaders(sid,tid), null,params);
        return p;
    }

    public Processor dailyPlanAggregatorWithOptionalParams(String lat, String lng, HashMap<String, String> optional_params, String sid, String tid) {
        String opt_params = "";
        if(!optional_params.isEmpty()) {
            for (Map.Entry<String, String> entry:optional_params.entrySet()) {
                opt_params = opt_params +"&amp;" + entry.getKey().replace(" ","%20") + "=" + entry.getValue().replace(" ", "%20");
            }
        } else
            System.out.println("Optional params empty");
        System.out.println("-- OPT_PARAMS STRING ::: " +opt_params);

        String[] params = {lat,lng,opt_params};
        GameOfThronesService gots = new GameOfThronesService("sanddaily", "planListingWithOptionalParams", init);
        Processor p = new Processor(gots, defaultheaders(sid,tid), null,params);
        return p;
    }

    public String redisKeyGenerator (String tid, String meal_plan, String slotName, String lat, String lng, HashMap<String, List<String>> hm_filter, HashMap<String, String> hm_sort) {
        String returnVal = "";
        String geoHashKey = dailyGeoHashUtilityHelper.encodeGeohashWithPrecision(Double.parseDouble(lat), Double.parseDouble(lng),null);
        returnVal = returnVal + tid + "_"+ geoHashKey + "_" + meal_plan + "_" + slotName + "_";
        if (hm_filter.size() > 0) {
            for (String key : hm_filter.keySet()) {
                returnVal = returnVal + key + "_";
                Collections.sort(hm_filter.get(key));
                for (int i = 0; i < hm_filter.get(key).size(); i++) {
                    String temp = hm_filter.get(key).get(i).trim().replace(" ", "_");
                    returnVal = returnVal + temp + "_";
                }
            }
        }
        if (hm_sort.size() > 0) {
            for (String key : hm_sort.keySet()) {
                String temp = hm_sort.get(key).trim().replace(" ", "_");
                returnVal = returnVal + key + "_" + temp + "_";
            }
    }
        System.out.println("-- REDIS KEY GENERATOR ::: "+returnVal);
       return returnVal;
    }

    public String redisKeyGeneratorForSwapMeal(String tid,String mealId,String mealType, String lat, String lon){
        String returnVal;
        String geoHashKey = "s000000";//dailyGeoHashUtilityHelper.encodeGeohashWithPrecision(Double.parseDouble(lat), Double.parseDouble(lon),null);
        returnVal = tid + "_"+ geoHashKey + "_" + mealType + "_" + mealId + "_";
        return returnVal;
    }

    public String redisKeyGeneratorForMeal(String tid,String mealType,String slotType, String lat, String lon){
        String returnVal;
        String geoHashKey = dailyGeoHashUtilityHelper.encodeGeohashWithPrecision(Double.parseDouble(lat), Double.parseDouble(lon),null);
        returnVal = tid + "_"+ geoHashKey + "_" + mealType + "_" + slotType + "_";
        return returnVal;
    }



    public String optionalFilterParamGenerator(HashMap<String, List<String>> hm_filters) {
       String returnVal = "";
        for (String key: hm_filters.keySet()) {
//            if (!=0)
//                returnVal = returnVal + ",";
            returnVal = returnVal+ key+":";
            for (int j = 0; j < hm_filters.get(key).size() ; j++) {
                if (j==0)
                    returnVal = returnVal + hm_filters.get(key).get(j);
                else
                    returnVal = returnVal +";" + hm_filters.get(key).get(j);
            }
            returnVal = returnVal + ",";
        }
        returnVal = returnVal.substring(0,returnVal.length()-1);
        System.out.println("-- Optional Params: Filters ::: "+returnVal);
        return returnVal;
    }

    public String optionalSortParamGenerator(HashMap<String, String> hm_sort) {
       String returnVal = "";
        for (String key: hm_sort.keySet()) {
            returnVal = returnVal + key + ":"+hm_sort.get(key)+",";
        }
        returnVal =  returnVal.replace(" ","%20");
        returnVal = returnVal.substring(0,returnVal.length()-1);
        System.out.println("-- Optional Params: Sorting ::: "+returnVal);
        return returnVal;
    }

    //returns string array = {pagination_blockSize , userAgent_pageSize}
    public String[] getPaginationConfigFromConsul(String userAgent) {
       String[] blockSize_PageSize = new String[2];
        String[] params_2 = new String[1];
        GameOfThronesService gots = new GameOfThronesService("consuldaily", "consulConfigDailyDiscoveryEngine", init);
        String[] params_1 = {DailySandConstants.redis_block_size};
        Processor p = new Processor(gots, defaultheaders(), null,params_1);
        blockSize_PageSize[0] = p.ResponseValidator.GetBodyAsText();
       if(userAgent.equals("ANDROID")){
           params_2[0] = DailySandConstants.redis_android_page_size;
       } else if(userAgent.equals("IOS")){
           params_2[0] = DailySandConstants.redis_android_page_size;
       }
        Processor p1 = new Processor(gots, defaultheaders(), null,params_2);
        blockSize_PageSize[1] = p1.ResponseValidator.GetBodyAsText();
        System.out.println("--CONSUL REDIS BLOCK SIZE ::: "+blockSize_PageSize[0]);
        System.out.println("--CONSUL REDIS USER AGENT PAGE SIZE ::: "+blockSize_PageSize[1]);
        return blockSize_PageSize;
    }

    public int totalLengthOfRedisDataInPagination(String hashKey, int blockSize) {
       int total = 0;
        long hlen = redisHelper.getHLen(DailySandConstants.sandRedis,0,hashKey);
        System.out.println("-- Length of the HashMap: "+hashKey+ "    is (hlen):::"+hlen);
        String get_hlen = String.valueOf(((int) hlen) - 1);
        if (Integer.parseInt(get_hlen) < 0)
            get_hlen = String.valueOf(0);
        System.out.println("--Last Redis Block ::: "+get_hlen);
        String getLastBlockData = redisHelper.getHashValue(DailySandConstants.sandRedis,0,hashKey,get_hlen);
        String[] all_meal_ids_last_block = JsonPath.read(getLastBlockData,"$.records[*].storeId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        total = total+(Integer.parseInt(get_hlen)*blockSize) + (all_meal_ids_last_block.length);
        return total;
    }

    //return ArrayList<Integer> of size 2 showing starting and ending block -
    // if both are same then same block has all the data of offset and offset+pagesize
    public ArrayList<Integer> identifyRedisBlockId(String offset, String hashKey) {
       ArrayList<Integer> returnVal = new ArrayList<>();
       int offst;
       String[] blockSize_PageSize = getPaginationConfigFromConsul(DailySandConstants.userAgent);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (offset.equals(""))
           offst = 0;
       else
           offst = Integer.parseInt(offset);
       int blockSize = Integer.parseInt(blockSize_PageSize[0]);
       int total_data_size = totalLengthOfRedisDataInPagination(hashKey,blockSize);
        System.out.println("--TOTAL DATA LENGTH IN REDIS ( "+hashKey+" ) IS ::: "+total_data_size);
       if (offst < 0) {
           offst = 0;
           returnVal.add(offst);
           returnVal.add(offst);
           System.out.println("-- identifyRedisBlockId returnVal ::: "+returnVal);
           return returnVal;
       }
        if(offst >= total_data_size) {
            System.out.println("-- Offset is greater than total data in cache ... so assert should have empty set of data");
            System.out.println("-- identifyRedisBlockId returnVal ::: "+returnVal);
            return returnVal;
        }
       else if (offst >= 0 && offst <= blockSize) {
           returnVal.add(0);
           int div = (offst+Integer.parseInt(blockSize_PageSize[1])) / blockSize;
           returnVal.add(div);
            System.out.println("-- identifyRedisBlockId returnVal ::: "+returnVal);
            return returnVal;
       }
       else if (offst > blockSize && offst < total_data_size) {
           int div1 = offst / blockSize;
           int div2 = (offst + Integer.parseInt(blockSize_PageSize[1])) / blockSize;
           returnVal.add(div1);
           returnVal.add(div2);
            System.out.println("-- identifyRedisBlockId returnVal ::: "+returnVal);
            return returnVal;
       }
        System.out.println("-- identifyRedisBlockId returnVal ::: "+returnVal);
       return returnVal;
    }

    public int getNextOffset(String currentOffset, String totalOffset, String paginationSize) {
        //String[] consul_prop = getPaginationConfigFromConsul(DailySandConstants.userAgent);
        int nextOffset = Integer.parseInt(currentOffset)+ Integer.parseInt(paginationSize);
        if(nextOffset < Integer.parseInt(totalOffset))
            return nextOffset;
        else
            return Integer.parseInt(totalOffset);
    }

    public HashMap<String, List<String>> getAllSortOptions(String slotType, String slotName, String sid, String tid) {
        Processor p_default = null;
        HashMap<String, List<String>> hm_sortOptions =  new HashMap<>();
        if (slotType.equals(DailySandConstants.slotType_meal)) {
            p_default = dailyMealAggregator(DailySandConstants.lat, DailySandConstants.lng, slotName, "",sid,tid);
        } else if (slotType.equals(DailySandConstants.slotType_plan)) {
            p_default = dailyPlanAggregator(DailySandConstants.lat, DailySandConstants.lng, slotName, "",sid,tid);
        } else
            System.out.println("INVALID MEAL/SLOT TYPE - Value given is other than MEAL/SLOT");
        int get_status_1 = p_default.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(get_status_1, DailySandConstants.statusZero);
        String[] sort_keys = p_default.ResponseValidator.GetNodeValueAsJsonArray("$.data.sort[*].key").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        for (int i = 0; i < sort_keys.length; i++) {
            System.out.println("-- SORT_KEY ::: " + sort_keys[i]);
            String[] sort_options = p_default.ResponseValidator.GetNodeValueAsJsonArray("$.data.sort[?(@.key=='" + sort_keys[i] + "')].options[*].option").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            System.out.println("-- SORT OPTIONS ::: " + Arrays.asList(sort_options));
            hm_sortOptions.put(sort_keys[i], Arrays.asList(sort_options));
        }
        return hm_sortOptions;
    }

    public HashMap<String, List<String>> getAllFilterOptions(String slotType, String slotName, String sid, String tid) {
        Processor p_default = null;
        HashMap<String, List<String>> hm_filterOptions =  new HashMap<>();
        if (slotType.equals(DailySandConstants.slotType_meal)) {
            p_default = dailyMealAggregator(DailySandConstants.lat, DailySandConstants.lng, slotName, "",sid,tid);
        } else if (slotType.equals(DailySandConstants.slotType_plan)) {
            p_default = dailyPlanAggregator(DailySandConstants.lat, DailySandConstants.lng, slotName, "",sid,tid);
        } else
            System.out.println("INVALID MEAL/SLOT TYPE - Value given is other than MEAL/SLOT");
        int get_status_1 = p_default.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(get_status_1, DailySandConstants.statusZero);
        String[] filter_keys = p_default.ResponseValidator.GetNodeValueAsJsonArray("$.data.filters[*].key").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        for (int i = 0; i < filter_keys.length; i++) {
            System.out.println("-- FILTER_KEY ::: "+filter_keys[i]);
            String[] filter_options = p_default.ResponseValidator.GetNodeValueAsJsonArray("$.data.filters[?(@.key=='"+filter_keys[i]+"')].options[*].option").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            System.out.println("-- FILTER OPTIONS ::: "+Arrays.asList(filter_options));
            hm_filterOptions.put(filter_keys[i],Arrays.asList(filter_options));
        }
        return hm_filterOptions;
    }

    //returns array elements not contained in the keys array
    public List<String> validateKeysEquatesAllValuesInArray(String[] arr, String[] keys) {
       List<String> list = new ArrayList<>();
        for (int i = 0; i < arr.length ; i++) {
                if (!ArrayUtils.contains(keys,arr[i]));
                list.add(arr[i]);
        }
        return list;
    }

    public boolean isSorted(String[] arr, String sorting) {
        double[] array = Arrays.stream(arr)
                .mapToDouble(Double::parseDouble).toArray();

       if (sorting.equals("Low To High"))
           return IntStream.range(0, array.length - 1).noneMatch(i -> array[i] > array[i + 1]);
       else if (sorting.equals("High To Low"))
           return IntStream.range(0, array.length - 1).noneMatch(i -> array[i] < array[i + 1]);
       else return false;
    }

    public Processor dailySubscriptionListingHelper(String customerId, String slotStartTime, String slotEndTime){
       String[] params = {customerId,slotStartTime,slotEndTime};
        GameOfThronesService gots = new GameOfThronesService("dailysubscription", "getSubscriptionListing", init);
        Processor p = new Processor(gots,defaultheaders(),null,params);
        return p;
    }

    public Processor pauseDailySubscriptionHelper(String subscriptionId, String startDate, String endDate) {
       String[] params =  {subscriptionId,startDate,endDate};
        GameOfThronesService gots = new GameOfThronesService("dailysubscription", "pausesubscription", init);
        Processor p = new Processor(gots,defaultheaders(),null,params);
        return p;
    }

    public Processor resumeDailySubscriptionHelper(String subscriptionId) {
       String[] params = {subscriptionId};
        GameOfThronesService gots = new GameOfThronesService("dailysubscription", "resumesubscription", init);
        Processor p = new Processor(gots,defaultheaders(),null,params);
        return p;
    }

    public boolean isSlotNameValid(String slotName) {
       if (slotName.equals(""))
           return true;
       String currentTime = String.valueOf(dateHelper.getCurrentEpochTimeInMillis());
       Processor p = getDailyMealSlotsHelper(DailySandConstants.cityId,currentTime);
       String[] get_all_available_slots = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data[*].slot").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
       if (ArrayUtils.contains(get_all_available_slots,slotName))
           return true;
       return false;
    }

    public Processor availabilityPlanListingHelper(String json, String includes_only, String start_date, String start_time, String end_time, String base_variant) {
       String[] params = {includes_only, start_date,start_time, end_time,base_variant};
       String[] payload = {json};
       GameOfThronesService gots = new GameOfThronesService("cmsdaily", "availabilityPlanListing", init);
       Processor p = new Processor(gots,catalogHeaders(),payload,params);
       return p;
    }

    public Processor availabilityMealListingHelper(String json,String includes_only, String start_time_epoch, String end_time_epoch) {
        String[] params = {includes_only,start_time_epoch, end_time_epoch};
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("cmsdaily", "availabilityMealListing", init);
        Processor p = new Processor(gots,catalogHeaders(),payload,params);
        return p;
    }

    public Processor availabilityMealMetaHelper(String json, String includes_only) {
        String[] params = {includes_only};
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("cmsdaily", "availabilityMealMeta", init);
        Processor p = new Processor(gots,catalogHeaders(),payload,params);
        return p;
    }

    public Processor availabilityPlanMetaHelper(String json, String includes_only, String base_variant) {
        String[] params = {includes_only,base_variant};
        String[] payload = {json};
        GameOfThronesService gots = new GameOfThronesService("cmsdaily", "availabilityPlanMeta", init);
        Processor p = new Processor(gots,catalogHeaders(),payload,params);
        return p;
    }

    public boolean isOutOfStock(String inventory) {
       int inv = Integer.parseInt(inventory);
       if (inv > 0)
           return false;
       else return true;
    }

    //@Overload
    public boolean isOutOfStock(String[] inventory) {
        Integer[] inventory_int = Stream.of(inventory).map(Integer::valueOf).toArray(Integer[]::new);
        for (int i = 0; i < inventory_int.length; i++) {
            if(inventory_int[i] != 0)
                return false;
        }
        return true;
    }

    public boolean isAvailable(String[] is_avail) {
        for (int i = 0; i < is_avail.length; i++) {
            boolean avail = Boolean.parseBoolean(is_avail[i]);
            if (avail)
                return true;
        }
        return false;
    }

    public String[] returnArrayWithUniqueData(String[] arr) {
        Set<String> set = new HashSet<>(Arrays.asList(arr));
        System.out.println("--UNIQUE Data ::: "+set);
        String[] returnVal = set.toArray(new String[set.size()]);
        return returnVal;
    }

    public HashMap<String,String> getMetaDetailsForAGivenSpin(String json, String spinId) {
       int location = 0;
       int i=0;
       HashMap<String, String> hm = new HashMap<>();
       boolean flag = false;
       String[] data_productId = JsonPath.read(json,"$.data[*].product_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
        do {
            String[] spins = JsonPath.read(json, "$.data["+i+"].variations[*].spin").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
            if(ArrayUtils.contains(spins,spinId)) {
                flag = true;
                location = i;
            } else
                ++i;
        } while(flag == false && i < data_productId.length);

        String store_id = JsonPath.read(json,"$.data["+location+"].store_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String product_id = JsonPath.read(json,"$.data["+location+"].product_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String display_name = JsonPath.read(json,"$.data["+location+"].meta.display_name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String is_veg = JsonPath.read(json,"$.data["+location+"].meta.is_veg").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String images = JsonPath.read(json,"$.data["+location+"].variations[?(@.spin=='"+spinId+"')].images[0]").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        hm.put("store_id",store_id);
        hm.put("product_id",product_id);
        hm.put("display_name",display_name);
        hm.put("is_veg",is_veg);
        hm.put("images",images);
        return  hm;
   }

    public HashMap<String,String> getSpinMetaFromSearchSKU(String json, String spinId) {
        HashMap<String, String> hm = new HashMap<>();
// TODO: add imageurl once fixed for bug: https://swiggy.atlassian.net/browse/DAIL-720
        String product_id = JsonPath.read(json,"$.data[?(@.spin=='"+spinId+"')].product_id").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String display_name = JsonPath.read(json,"$.data[?(@.spin=='"+spinId+"')].meta.display_name").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String is_veg = JsonPath.read(json,"$.data[?(@.spin=='"+spinId+"')].meta.is_veg").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String images = JsonPath.read(json,"$.data[?(@.spin=='"+spinId+"')].components[0].images").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        hm.put("product_id",product_id);
        hm.put("display_name",display_name);
        hm.put("is_veg",is_veg);
       hm.put("images",images);
        System.out.println("Subscription meta:::"+hm);
        return  hm;
    }

   public String[] getServiceableRestaurantsArray(String json) {
       List<Integer> location = new ArrayList<>();
       // invalid lat lng - no serviceable or unserviceable restaurant scenario
       String serve = JsonPath.read(json,"$.data.serviceableRestaurants").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
       if (serve.equals(""))
           return new String[0];
       String[] serviceability = JsonPath.read(json,"$.data.serviceableRestaurants[*]..serviceability").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").split(",");
       for (int i = 0; i < serviceability.length; i++) {
           if(Integer.parseInt(serviceability[i]) == 2)
               location.add(i);
       }
       if (location.size() == 0)
           return new String[0];
       else {
           String[] serv_restIds = new String[location.size()];
           for (int i = 0; i < location.size(); i++) {
               serv_restIds[i] = JsonPath.read(json,"$.data.serviceableRestaurants["+ location.get(i) +"].restaurantId").toString().replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "").trim();
           }
           return serv_restIds;
       }
   }

   public Processor TDEnrichmentHelper(String json, String meal_plan) {
       String[] payload = {json};
       GameOfThronesService gots;
       if (meal_plan.equals(DailySandConstants.slotType_meal))
           gots = new GameOfThronesService("promotion", "mealTDEnrichment", init);
       else
           gots = new GameOfThronesService("promotion", "planTDEnrichment", init);
       Processor p = new Processor(gots, defaultheaders(), payload);
       return p;
   }

   public Processor vendorEnrichment(String json) {
       String[] payload = {json};
       GameOfThronesService gots = new GameOfThronesService("srs", "vendorEnrichment", init);
       Processor p = new Processor(gots, defaultheaders(), payload);
       return p;
   }

   public Processor enrichmentServiceHelper(String json, String meal_plan) {
       String[] payload = {json};
       GameOfThronesService gots;
       if (meal_plan.equals(DailySandConstants.slotType_meal))
           gots = new GameOfThronesService("sndenrichment", "mealEnrichment", init);
       else
           gots = new GameOfThronesService("sndenrichment", "planEnrichment", init);
       Processor p = new Processor(gots, defaultheaders(), payload);
       return p;
   }

   public Processor searchBySpinIdHelper(String json) {
       String[] payload = {json};
       GameOfThronesService gots = new GameOfThronesService("swiggylistingservice", "searchBySpin", init);
       Processor p = new Processor(gots, catalogHeaders(), payload);
       return p;

   }


   public Processor metaValuesForSwappableMealId(String priceDetails, String editType, String mealDetails, String mealId){
       Processor processor;

       HashMap headers = new HashMap();
       headers.put("Content-Type","application/json");
       headers.put("cache-control","no-cache");

       String urlParams [] = {mealId,priceDetails,editType,mealDetails};
       GameOfThronesService gameOfThronesService = new GameOfThronesService("swappablemeanmeta","swapmealmeta",init);
       processor = new Processor(gameOfThronesService,headers,null,urlParams);
return processor;
   }


   public Processor swapListingCall(String tid, String subscriptionMealId){

       HashMap headers = new HashMap();
       headers.put("Content-Type","application/json");
       headers.put("tid",tid);
       String [] urlParams = {subscriptionMealId};
       GameOfThronesService gameOfThronesService = new GameOfThronesService("swaplisting","swapmeallisting",init);
       Processor processor = new Processor(gameOfThronesService,headers,null,urlParams);

       return processor;

   }


   public Processor mealListingWithSlot(String tid , String sid, String lat, String lon, String slotType){

       String[] urlParams = {lat, lon, slotType};
       GameOfThronesService gots = new GameOfThronesService("sanddaily", "mealListingWithSlot", init);
       Processor processor = new Processor(gots, defaultheaders(tid,sid), null,urlParams);
       return processor;
   }

//@Test
//    public void a()
//    {
//        String json = "{\"statusCode\":0,\"statusMessage\":\"Success\",\"data\":{\"blackZoneResponse\":{\"illustration_details\":{\"title\":\"\",\"description\":\"Sorry, we are currently not serviceable in your area..\"},\"restaurants\":[],\"black_zone_title\":\"Out of delivery area\",\"black_zone_message\":\"Sorry, this location is out of our delivery area!\"},\"serviceableRestaurants\":[{\"restaurantId\":\"19533\",\"cityId\":\"75\",\"listingServiceabilityResponse\":{\"restaurant_id\":19533,\"last_mile_travel\":0.381,\"distance_calculation_method\":\"HAVERSINE_LUCIFER\",\"serviceability\":0,\"non_serviceable_reason\":9}},{\"restaurantId\":\"74360\",\"cityId\":\"75\",\"listingServiceabilityResponse\":{\"restaurant_id\":74360,\"last_mile_travel\":0.381,\"distance_calculation_method\":\"HAVERSINE_LUCIFER\",\"serviceability\":0,\"non_serviceable_reason\":9}},{\"restaurantId\":\"74359\",\"cityId\":\"75\",\"listingServiceabilityResponse\":{\"restaurant_id\":74359,\"last_mile_travel\":0.381,\"distance_calculation_method\":\"HAVERSINE_LUCIFER\",\"serviceability\":0,\"non_serviceable_reason\":9}},{\"restaurantId\":\"74361\",\"cityId\":\"75\",\"listingServiceabilityResponse\":{\"restaurant_id\":74361,\"last_mile_travel\":0.381,\"distance_calculation_method\":\"HAVERSINE_LUCIFER\",\"serviceability\":0,\"non_serviceable_reason\":9}}],\"customerInfo\":{\"customer_zone_id\":683,\"customer_zone_raining\":false}}}";
//        System.out.println(Arrays.asList(getServiceableRestaurantsArray(json)));
//    }

    public Processor getCityIdFromLatLng(String lat, String lng) {
        String[] params = {lat, lng};
        GameOfThronesService gots = new GameOfThronesService("sandappconfigservicedaily", "getCityId", init);
        Processor p = new Processor(gots, defaultheaders(), null,params);
        return p;
    }

    public HashMap<String,HashMap<String,Object>> getSpinStoreMetaMap(String swapMealDataFromRedis) throws Exception{

        HashMap<String,HashMap<String,Object>> spinStoreMetaMap = new HashMap<>();
        HashMap<String,Object> eachRecordMap = new HashMap<>();

        JSONObject objectOfSwapMealResponse = new JSONObject(swapMealDataFromRedis);
        JSONArray arrayOfRecords = objectOfSwapMealResponse.getJSONArray("records");
        for(int i =0; i<arrayOfRecords.length(); i++){
            JSONObject eachObject = arrayOfRecords.getJSONObject(i);
            eachRecordMap.put("storeId",eachObject.get("storeId"));
            eachRecordMap.put("spin",eachObject.get("spin"));

            JSONObject pricingDetailsOfEachObject = eachObject.getJSONObject("priceDetails");
            eachRecordMap.put("price",pricingDetailsOfEachObject.get("price"));
            eachRecordMap.put("discountedPrice",pricingDetailsOfEachObject.get("discountedPrice"));

            spinStoreMetaMap.put(eachRecordMap.get("storeId")+"_"+eachRecordMap.get("spin"),eachRecordMap);


        }

        return spinStoreMetaMap;
    }

    public HashMap<String,Object> getSpinPriceForASpinId(String newSpinId, String responseOfMealListing) throws Exception {

        HashMap<String, Object> eachRecordMap = new HashMap<>();
        JSONObject objectOfMealResponse = new JSONObject(responseOfMealListing);
        JSONArray arrayOfRecords = objectOfMealResponse.getJSONArray("records");
        for (int i = 0; i < arrayOfRecords.length(); i++) {
            JSONObject eachObject = arrayOfRecords.getJSONObject(i);
            if (eachObject.get("spin").toString().equalsIgnoreCase(newSpinId)){
                eachRecordMap.put("spin", eachObject.get("spin"));

                JSONObject pricingDetailsOfEachObject = eachObject.getJSONObject("priceDetails");

                eachRecordMap.put("price", pricingDetailsOfEachObject.get("price"));
                eachRecordMap.put("discountedPrice", pricingDetailsOfEachObject.get("discountedPrice"));
            }
        }

return eachRecordMap;
    }

    public Processor getMealId(String orderId){
       HashMap headers = new HashMap();
       String [] urlPramas = {orderId};
       GameOfThronesService gameOfThronesService = new GameOfThronesService("dailysubscription","detailssubscription",init);
       Processor processor = new Processor(gameOfThronesService,headers,null,urlPramas);
       return processor;
    }



}
