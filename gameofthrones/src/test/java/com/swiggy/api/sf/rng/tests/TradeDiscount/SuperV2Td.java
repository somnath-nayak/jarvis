package com.swiggy.api.sf.rng.tests.TradeDiscount;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.TradeDiscount.SuperV2Dp;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.SuperDbHelper;
import com.swiggy.api.sf.rng.helper.TDHelper.SuperV2TdHelper;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.tests.RandomNumber;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SuperV2Td extends SuperV2Dp {

    SoftAssert softAssert = new SoftAssert();
    JsonHelper jsonHelper = new JsonHelper();
    Processor processor;
    RngHelper rngHelper = new RngHelper();
    SuperV2TdHelper superV2TdHelper = new SuperV2TdHelper();
    RandomNumber rm = new RandomNumber(2000, 3000);
    RngConstants rngConstants;




    @Test(dataProvider = "FreeDeliveryTD_EvaluationAtRestaurantLevel", description = "FreeDelivery Evaluation With NoMinCartAmount for NON-SUPER USER", enabled = true)
    public void FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevel(String type, ArrayList al, String tid, boolean check) {
        String TDType = "FREE_DELIVERY";
        String minCartAmount = "99.0";
        String cartTD = "0.0";
        String rewardList = "[]";
        superV2TdHelper.validationForSuperUser(type, al, tid, TDType, minCartAmount, cartTD, rewardList, check);
    }

    @Test(dataProvider = "superSubscriptionFreeDel", description = "FreeDelivery Evaluation With NoMinCartAmount for SUPER USER", enabled = true)
    public void FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevelSuperuser(String planPayload, String numOfPlans,
                                                                                 String benefitPayload, String type, ArrayList al, String tid, boolean check) {
        SuperDbHelper.deleteAllBenefit();
        HashMap<String, String> hmap = superV2TdHelper.publicUserSubsCription(planPayload, numOfPlans,
                benefitPayload);
        String userid = hmap.get("userid");

        al.set(2, userid); //update the existing user id to

        String TDType = "FREE_DELIVERY";
        String minCartAmount = "99.0";
        String cartTD = "0.0";
        String rewardList = "[]";
        superV2TdHelper.validationForSuperUser(type, al, tid, TDType, minCartAmount, cartTD, rewardList, check);
    }

    @Test(dataProvider = "superPlanBenefitsFreeDelMapped", description = "FreeDelivery Evaluation With NoMinCartAmount for NON SUPER USER with Plan benefits mapped")
    public void FreeDeliveryTD_EvaluationWithNoMinCartAtRestaurantLevelNonSuperUser(String planPayload, String numOfPlans,
                                                                                    String benefitPayload, String type, ArrayList al, String tid, boolean check) {
        SuperDbHelper.deleteAllBenefit();
        HashMap<String, String> hmap = superV2TdHelper.publicUserPlanBenefitsMapped(planPayload, numOfPlans,
                benefitPayload);
        String userid = hmap.get("userid");

        al.set(2, userid); //update the existing user id to

        String TDType = "FREE_DELIVERY";
        String minCartAmount = "99.0";
        String cartTD = "0.0";
        String rewardList = "[]";
        superV2TdHelper.validationForSuperUser(type, al, tid, TDType, minCartAmount, cartTD, rewardList, check);
    }


    @Test(description = "CreateMultiTdOnSameRestaurant")
    public void CreateMultiTdOnSameRestaurant() throws IOException {
        String TDType = "FREE_DELIVERY";
        String cartTD = "0.0";
        String rewardList = "[]";

        String price = "500";
        String cartAmount = "500";
        String minCartAmount = "99.0";

        String restId = new Integer(rm.nextInt()).toString();// rngHelper.getRandomRestaurant("12.9719", "77.6412");
        String itemId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(0).toString();
        String categoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(1).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();// rngHelper.getItemDetails(restId).get(2).toString();
        String count = "1";// rngHelper.getItemDetails(restId).get(3).toString();
        HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);

        String userId = new Integer(rm.nextInt()).toString();// checkoutHelper.TokenData(rngConstants.mobile1,
        String tid = (String) Keys.get("TDID");

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);

        //Validation
        superV2TdHelper.validationForSuperUser("list", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("list", a2, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("menu", a3, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("menu", a4, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("cart", a5, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("cart", a6, tid, TDType, minCartAmount, cartTD, rewardList, true);

        //FreeDel- Creation on rest already Free-Del available
        HashMap Keys1 = superV2TdHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);
        String status_message = RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid;
        softAssert.assertEquals(Keys1.get("statusCode"), "2");
        softAssert.assertEquals(Keys1.get("statusMessage"), status_message);

        //FreeBie- Creation on rest already Free-Del available
        String TDType_Freebie = "Freebie";
        String freebieitemId = new Integer(rm.nextInt()).toString();
        String rewardList_Freebie = "[" + "\"" + freebieitemId + "\"" + "]";
        HashMap Keys2 = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("100", restId, freebieitemId);
        softAssert.assertEquals(Keys2.get("statusCode"), "0");
        softAssert.assertEquals(Keys2.get("statusMessage"), RngConstants.TD_SUCCESS_CREATION_STATUS);
        rngHelper.disableCampaign(Keys2.get("TDID").toString()); //disable after successfull creation for other types of TD creation


        //Flat
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(restId.toString(), "Test", new ArrayList<>()));
        String payload = superV2TdHelper.createFlat(restaurantLists1);
        Processor processor = rngHelper.createTD(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        rngHelper.TDstatusChecker(response);
        String campaign_id = JsonPath.read(response, "$.data").toString();
        softAssert.assertEquals(JsonPath.read(response, "$.statusMessage").toString(), "done");
        softAssert.assertNotNull(campaign_id, "td id is null");
        rngHelper.disableCampaign(campaign_id);//disable after successfull creation for other types of TD creation


        //Percentage
        String td_percentage_response = superV2TdHelper.createPercentageWithMinCartAtItemLevel(restId);
        String campaign_id_percentage_td = JsonPath.read(td_percentage_response, "$.data").toString();

        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusMessage").toString(), "done");
        softAssert.assertNotNull(campaign_id_percentage_td, "td id is null");
        rngHelper.disableCampaign(campaign_id_percentage_td);//disable after successfull creation for other types of TD creation
        softAssert.assertAll();
    }


    @Test(dataProvider = "superSubscriptionFlatDp", description = "Flat Evaluation With NoMinCartAmount for SUPER USER")
    public void FlatTD_EvaluationWithNoMinCartAtRestaurantLevelSuperuser(String planPayload, String numOfPlans,
                                                                         String benefitPayload) throws IOException {
        SuperDbHelper.deleteAllBenefit();
        HashMap<String, String> hmap = superV2TdHelper.publicUserSubsCription(planPayload, numOfPlans,
                benefitPayload);
        String userid = hmap.get("userid");
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String count = "1";
        String TDType = "FLAT";
        String minCartAmount = "0.0";
        String price = "500";
        String cartTD = "0.0";
        String rewardList = "[]";
        String restId = new Integer(rm.nextInt()).toString();
        String discountAmount = "10.0";
        String testHeader = "test header";

        rngHelper.disableCampaign(restId);
        //TD - FLAT CREATION
        HashMap Keys = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountForSuperUser(restId, testHeader, discountAmount);
        String tid = (String) Keys.get("campaign_id");

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("listsuper", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("listsuper", a2, tid, TDType, minCartAmount, cartTD, rewardList, true);

        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("menu", a3, tid, TDType, minCartAmount, cartTD, rewardList, true);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("menu", a4, tid, TDType, minCartAmount, cartTD, rewardList, true);

        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userid, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("cartsuper", a5, tid, TDType, minCartAmount, discountAmount, rewardList, true);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userid, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("cartsuper", a6, tid, TDType, minCartAmount, discountAmount, rewardList, true);

        //FreeDel- Creation (Will be able to create)
        HashMap Keys1 = superV2TdHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);
        String status_message = RngConstants.TD_SUCCESS_CREATION_STATUS;
        softAssert.assertEquals(Keys1.get("statusCode"), "0");
        softAssert.assertEquals(Keys1.get("statusMessage"), status_message);
        rngHelper.disableCampaign(Keys1.get("TDID").toString()); //disable after successfull creation for other types of TD creation

        //FreeBie- Creation(Will not be able to create)
        String TDType_Freebie = "Freebie";
        String freebieitemId = new Integer(rm.nextInt()).toString();
        String rewardList_Freebie = "[" + "\"" + freebieitemId + "\"" + "]";
        HashMap Keys2 = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("100", restId, freebieitemId);
        softAssert.assertEquals(Keys2.get("statusCode"), "2");
        softAssert.assertEquals(Keys2.get("statusMessage"), RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid);

        //Percentage(Will not be able to create)
        String td_percentage_response = superV2TdHelper.createPercentageWithMinCartAtItemLevel(restId);
        String campaign_id_percentage_td = JsonPath.read(td_percentage_response, "$.data").toString();
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusMessage").toString(), RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid);
        softAssert.assertEquals(campaign_id_percentage_td, RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid);
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusCode").toString(), "2");
        softAssert.assertAll();
    }

    @Test(dataProvider = "superSubscriptionFreebieDp", description = "FreeBie Evaluation With NoMinCartAmount for SUPER USER")
    public void Freebie_EvaluationWithNoMinCartAtRestaurantLevelSuperuser(String planPayload, String numOfPlans,
                                                                          String benefitPayload) throws IOException {
        SuperDbHelper.deleteAllBenefit();
        HashMap<String, String> hmap = superV2TdHelper.publicUserSubsCription(planPayload, numOfPlans,
                benefitPayload);
        String userid = hmap.get("userid");
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String count = "1";
        String TDType = "Freebie";
        String minCartAmount = "0.0";
        String price = "500";
        String cartTD = "0.0";
        String rewardList = "[]";
        String restId = new Integer(rm.nextInt()).toString();
        String discountAmount = "10.0";
        String testHeader = "test header";

        rngHelper.disableCampaign(restId);
        //TD - CREATION
        String response = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevelForSuperUser(minCartAmount, restId, "123").ResponseValidator.GetBodyAsText();
        String tid = jsonHelper.getJSONToMap(response).get("data").toString();

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("listsuper", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "true", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("listsuper", a2, tid, TDType, minCartAmount, cartTD, rewardList, true);

        //MENU
        ArrayList a3 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "false", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("menu", a3, tid, TDType, minCartAmount, cartTD, rewardList, true);
        ArrayList a4 = superV2TdHelper.getListForMenu(String.valueOf(rngConstants.minCartAmount), restId, "true", userid, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("menu", a4, tid, TDType, minCartAmount, cartTD, rewardList, true);

        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userid, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("cart", a5, tid, TDType, minCartAmount, minCartAmount, rewardList, true);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, "99", userid, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("cart", a6, tid, TDType, minCartAmount, minCartAmount, rewardList, true);

        //FreeDel- Creation (Will be able to create)
        HashMap Keys1 = superV2TdHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);
        String status_message = RngConstants.TD_SUCCESS_CREATION_STATUS;
        softAssert.assertEquals(Keys1.get("statusCode"), "0");
        softAssert.assertEquals(Keys1.get("statusMessage"), status_message);
        rngHelper.disableCampaign(Keys1.get("TDID").toString()); //disable after successfull creation for other types of TD creation

        //FreeBie- Creation(Will not be able to create)
        String TDType_Freebie = "Freebie";
        String freebieitemId = new Integer(rm.nextInt()).toString();
        String rewardList_Freebie = "[" + "\"" + freebieitemId + "\"" + "]";
        HashMap Keys2 = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("100", restId, freebieitemId);
        softAssert.assertEquals(Keys2.get("statusCode"), "2");
        softAssert.assertEquals(Keys2.get("statusMessage"), RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid);

        //Percentage(Will be able to create)
        String td_percentage_response = superV2TdHelper.createPercentageWithMinCartAtItemLevel(restId);
        String campaign_id_percentage_td = JsonPath.read(td_percentage_response, "$.data").toString();
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusMessage").toString(), RngConstants.TD_SUCCESS_CREATION_STATUS);
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusCode").toString(), "0");
        rngHelper.disableCampaign(campaign_id_percentage_td);


        //FLAT (able to create)
        HashMap Keys = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(restId, testHeader, discountAmount);
        String statusMessage = (String) Keys.get("statusMessage");
        softAssert.assertEquals(statusMessage, RngConstants.TD_SUCCESS_CREATION_STATUS);
        rngHelper.disableCampaign(Keys.get("campaign_id").toString());
        softAssert.assertAll();

    }

    @Test(dataProvider = "superSubscriptionPercentageDp", description = "FreeBie Evaluation With NoMinCartAmount for SUPER USER")
    public void Percentage_EvaluationWithNoMinCartAtRestaurantLevelSuperuser(String planPayload, String numOfPlans,
                                                                             String benefitPayload) throws IOException {
        SuperDbHelper.deleteAllBenefit();
        HashMap<String, String> hmap = superV2TdHelper.publicUserSubsCription(planPayload, numOfPlans,
                benefitPayload);

        String TDType = "Percentage";
        String minCartAmount = "0.0";

        String restId = new Integer(rm.nextInt()).toString();
        String discountAmount = "10.0";
        String testHeader = "test header";

        rngHelper.disableCampaign(restId);
        //TD - CREATION
        String response = superV2TdHelper.createPercentageWithMinCartAtItemLevelForSuperUser(restId);
        String tid = jsonHelper.getJSONToMap(response).get("data").toString();
        String statusMessage = jsonHelper.getJSONToMap(response).get("statusMessage").toString();
        softAssert.assertEquals(statusMessage, RngConstants.TD_SUCCESS_CREATION_STATUS);


        //FreeDel- Creation (Will be able to create)
        HashMap Keys1 = superV2TdHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);
        String status_message = RngConstants.TD_SUCCESS_CREATION_STATUS;
        softAssert.assertEquals(Keys1.get("statusCode"), "0");
        softAssert.assertEquals(Keys1.get("statusMessage"), status_message);
        rngHelper.disableCampaign(Keys1.get("TDID").toString()); //disable after successfull creation for other types of TD creation

        //FreeBie- Creation(Will not be able to create)
        String freebieitemId = new Integer(rm.nextInt()).toString();
        HashMap Keys2 = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("100", restId, freebieitemId);
        softAssert.assertEquals(Keys2.get("statusCode"), "2");
        softAssert.assertEquals(Keys2.get("statusMessage"), RngConstants.TD_REST_ALREADY_HAS_ACTIVE_CAMPAIGN + tid);

        //Percentage(Will be able to create)
        String td_percentage_response = superV2TdHelper.createPercentageWithMinCartAtItemLevel(restId);
        String campaign_id_percentage_td = JsonPath.read(td_percentage_response, "$.data").toString();
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusMessage").toString(), RngConstants.TD_SUCCESS_CREATION_STATUS);
        softAssert.assertEquals(JsonPath.read(td_percentage_response, "$.statusCode").toString(), "0");
        rngHelper.disableCampaign(campaign_id_percentage_td);


        //FLAT (able to create)
        HashMap Keys = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(restId, testHeader, discountAmount);
        String statusMessage1 = (String) Keys.get("statusMessage");
        softAssert.assertEquals(statusMessage1, RngConstants.TD_SUCCESS_CREATION_STATUS);
        rngHelper.disableCampaign(Keys.get("campaign_id").toString());
        softAssert.assertAll();

    }


    @Test(dataProvider = "superPlanBenefitsFreeDelMappedDp", description = "FreeDelivery Evaluation With NoMinCartAmount for NON SUPER USER with Plan benefits mapped -- > NUDGE DISCOUNT INFO")
    public void FreeDeliveryTD_AtRestaurantLevelNonSuperUser(String planPayload, String numOfPlans,
                                                             String benefitPayload) throws IOException {

        SuperDbHelper.deleteAllBenefit();

        HashMap<String, String> hmap = superV2TdHelper.publicUserPlanBenefitsMapped(planPayload, numOfPlans,
                benefitPayload);
        String userId = hmap.get("userid");

        String TDType = "FREE_DELIVERY";
        String cartTD = "0.0";
        String rewardList = "[]";

        String price = "500";
        String cartAmount = "500";
        String minCartAmount = "99.0";

        String restId = new Integer(rm.nextInt()).toString();
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String count = "1";
        rngHelper.disabledActiveTD(restId);
        HashMap Keys = rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevelSuperUser(minCartAmount, restId, false, false, false,
                "ZERO_DAYS_DORMANT", false);


        String tid1 = (String) Keys.get("TDID");

        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "true", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("list_nudge", a1, tid1, TDType, minCartAmount, cartTD, rewardList, true);
        ArrayList a2 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("list_nudge", a2, tid1, TDType, minCartAmount, cartTD, rewardList, true);

        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);

        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a5, tid1, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a6, tid1, TDType, minCartAmount, cartTD, rewardList, true);



    }

    @Test(dataProvider = "superPlanBenefitsFreeBieMappedDp", description = "FreeBie Evaluation With NoMinCartAmount for NON SUPER USER with Plan benefits mapped -- > NUDGE DISCOUNT INFO")
    public void FreeBieTD_AtRestaurantLevelNonSuperUser(String planPayload, String numOfPlans,
                                                        String benefitPayload) throws IOException {

//        SuperDbHelper.deleteAllBenefit();

        String TDType = "Freebie";
        String cartTD = "0.0";
        String rewardList = "[]";
        String minCartAmount = "99.0";
        String restId = new Integer(rm.nextInt()).toString();
        String freebieitemId = new Integer(rm.nextInt()).toString();


        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String price = "500";
        String cartAmount = "500";

        String count = "1";

        HashMap<String, String> hmap = superV2TdHelper.publicUserPlanBenefitsMapped(planPayload, numOfPlans,
                benefitPayload);
        String userId = hmap.get("userid");

        rngHelper.disabledActiveTD(restId);
        String response = rngHelper.createFreebieTDWithMinAmountAtRestaurantLevelForSuperUser(minCartAmount, restId, freebieitemId).ResponseValidator.GetBodyAsText();
        String tid = jsonHelper.getJSONToMap(response).get("data").toString();
        String statusCode = jsonHelper.getJSONToMap(response).get("statusCode").toString();
        softAssert.assertEquals(statusCode, "0");


        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
       superV2TdHelper.validationForSuperUser("list_nudge", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);


        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a5, tid, TDType, minCartAmount, cartTD, rewardList, true);
        softAssert.assertAll();
    }

    @Test(dataProvider = "superPlanBenefitsFlatMappedDp", description = "FreeBie Evaluation With NoMinCartAmount for NON SUPER USER with Plan benefits mapped -- > NUDGE DISCOUNT INFO")
    public void FlatTD_AtRestaurantLevelNonSuperUser(String planPayload, String numOfPlans,
                                                        String benefitPayload) throws IOException {

        SuperDbHelper.deleteAllBenefit();

        String TDType = "Flat";
        String cartTD = "0.0";
        String rewardList = "[]";
        String minCartAmount = "99.0";
        String restId = new Integer(rm.nextInt()).toString();

        String discountAmount = "10.0";
        String testHeader = "test header";
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String price = "500";
        String cartAmount = "500";

        String count = "1";

        HashMap<String, String> hmap = superV2TdHelper.publicUserPlanBenefitsMapped(planPayload, numOfPlans,
                benefitPayload);
        String userId = hmap.get("userid");
        System.out.println(userId);

        HashMap Keys = rngHelper.createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountForSuperUser(restId, testHeader, discountAmount);
        String statusMessage = (String) Keys.get("statusMessage");
        softAssert.assertEquals(statusMessage, RngConstants.TD_SUCCESS_CREATION_STATUS);
        String tid=Keys.get("campaign_id").toString();


        //LISTING
        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
        superV2TdHelper.validationForSuperUser("list_nudge", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);


        //CART
        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
        ArrayList a6 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "true", rngConstants.AndroidOS, rngConstants.versionCode229);

        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a5, tid, TDType, minCartAmount, cartTD, rewardList, true);
        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a6, tid, TDType, minCartAmount, cartTD, rewardList, true);

        softAssert.assertAll();
//

    }


    @Test(dataProvider = "superPlanBenefitsPercenatageMappedDp", description = "FreeBie Evaluation With NoMinCartAmount for NON SUPER USER with Plan benefits mapped -- > NUDGE DISCOUNT INFO")
    public void PercentageTD_AtRestaurantLevelNonSuperUser(String planPayload, String numOfPlans,
                                                     String benefitPayload) throws IOException {
        //SuperDbHelper.deleteAllBenefit();

        String TDType = "Percentage";
        String cartTD = "0.0";
        String rewardList = "[]";
        String minCartAmount = "99.0";
        String restId = new Integer(rm.nextInt()).toString();

        String discountAmount = "10.0";
        String testHeader = "test header";
        String itemId = new Integer(rm.nextInt()).toString();
        String categoryId = new Integer(rm.nextInt()).toString();
        String subCategoryId = new Integer(rm.nextInt()).toString();
        String price = "500";
        String cartAmount = "500";

        String count = "1";

        HashMap<String, String> hmap = superV2TdHelper.publicUserPlanBenefitsMapped(planPayload, numOfPlans,
                benefitPayload);
        String userId = hmap.get("userid");

        String response = superV2TdHelper.createPercentageWithMinCartAtItemLevelForSuperUser(restId);

//        String tid = jsonHelper.getJSONToMap(response).get("data").toString();
//        String statusMessage = jsonHelper.getJSONToMap(response).get("statusMessage").toString();
//        softAssert.assertEquals(statusMessage, RngConstants.TD_SUCCESS_CREATION_STATUS);
//
//        //LISTING
//        ArrayList a1 = superV2TdHelper.getArrayList(restId, "false", userId, rngConstants.AndroidOS, rngConstants.versionCode229);
//        superV2TdHelper.validationForSuperUser("list_nudge", a1, tid, TDType, minCartAmount, cartTD, rewardList, true);
//
//        //CART
//        ArrayList a5 = superV2TdHelper.getListForCart(restId, categoryId, subCategoryId, itemId, count, price, cartAmount, userId, "false", rngConstants.AndroidOS, rngConstants.versionCode229);
//        superV2TdHelper.validationForSuperUser("cartsuper_freedel", a5, tid, TDType, minCartAmount, cartTD, rewardList, true);
//        softAssert.assertAll();
    }


}
