package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "bgColor",
        "textColor",
        "id",
        "lat",
        "lng",
        "phone_no",
        "address",
        "name",
        "third_party_vendor_type",
        "cloudinary_image_id",
        "slug",
        "city",
        "area_name"
})
public class RestaurantDetails {

    @JsonProperty("bgColor")
    private String bgColor;
    @JsonProperty("textColor")
    private String textColor;
    @JsonProperty("id")
    private String id;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("phone_no")
    private String phoneNo;
    @JsonProperty("address")
    private String address;
    @JsonProperty("name")
    private String name;
    @JsonProperty("third_party_vendor_type")
    private Object thirdPartyVendorType;
    @JsonProperty("cloudinary_image_id")
    private String cloudinaryImageId;
    @JsonProperty("slug")
    private String slug;
    @JsonProperty("city")
    private String city;
    @JsonProperty("area_name")
    private String areaName;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantDetails() {
    }

    /**
     *
     * @param phoneNo
     * @param id
     * @param bgColor
     * @param thirdPartyVendorType
     * @param textColor
     * @param areaName
     * @param address
     * @param name
     * @param slug
     * @param lng
     * @param cloudinaryImageId
     * @param lat
     * @param city
     */
    public RestaurantDetails(String bgColor, String textColor, String id, String lat, String lng, String phoneNo, String address, String name, Object thirdPartyVendorType, String cloudinaryImageId, String slug, String city, String areaName) {
        super();
        this.bgColor = bgColor;
        this.textColor = textColor;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.phoneNo = phoneNo;
        this.address = address;
        this.name = name;
        this.thirdPartyVendorType = thirdPartyVendorType;
        this.cloudinaryImageId = cloudinaryImageId;
        this.slug = slug;
        this.city = city;
        this.areaName = areaName;
    }

    @JsonProperty("bgColor")
    public String getBgColor() {
        return bgColor;
    }

    @JsonProperty("bgColor")
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    @JsonProperty("textColor")
    public String getTextColor() {
        return textColor;
    }

    @JsonProperty("textColor")
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("phone_no")
    public String getPhoneNo() {
        return phoneNo;
    }

    @JsonProperty("phone_no")
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("third_party_vendor_type")
    public Object getThirdPartyVendorType() {
        return thirdPartyVendorType;
    }

    @JsonProperty("third_party_vendor_type")
    public void setThirdPartyVendorType(Object thirdPartyVendorType) {
        this.thirdPartyVendorType = thirdPartyVendorType;
    }

    @JsonProperty("cloudinary_image_id")
    public String getCloudinaryImageId() {
        return cloudinaryImageId;
    }

    @JsonProperty("cloudinary_image_id")
    public void setCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
    }

    @JsonProperty("slug")
    public String getSlug() {
        return slug;
    }

    @JsonProperty("slug")
    public void setSlug(String slug) {
        this.slug = slug;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("area_name")
    public String getAreaName() {
        return areaName;
    }

    @JsonProperty("area_name")
    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("bgColor", bgColor).append("textColor", textColor).append("id", id).append("lat", lat).append("lng", lng).append("phoneNo", phoneNo).append("address", address).append("name", name).append("thirdPartyVendorType", thirdPartyVendorType).append("cloudinaryImageId", cloudinaryImageId).append("slug", slug).append("city", city).append("areaName", areaName).append("additionalProperties", additionalProperties).toString();
    }

}