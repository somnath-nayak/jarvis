package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "RAIN_NUDGE_CARD"
})
public class Rain1 {

    @JsonProperty("RAIN_NUDGE_CARD")
    private RAIN_NUDGE_CARD RAIN_NUDGE_CARD;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RAIN_NUDGE_CARD")
    public RAIN_NUDGE_CARD getRAIN_NUDGE_CARD() {
        return RAIN_NUDGE_CARD;
    }

    @JsonProperty("RAIN_NUDGE_CARD")
    public void setRAIN_NUDGE_CARD(RAIN_NUDGE_CARD RAIN_NUDGE_CARD) {
        this.RAIN_NUDGE_CARD = RAIN_NUDGE_CARD;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
