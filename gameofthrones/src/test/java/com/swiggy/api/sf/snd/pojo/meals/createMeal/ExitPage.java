package com.swiggy.api.sf.snd.pojo.meals.createMeal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "image",
        "bgColor",
        "textColor",
        "mainText",
        "subText"
})
public class ExitPage {

    @JsonProperty("image")
    private String image;
    @JsonProperty("bgColor")
    private String bgColor;
    @JsonProperty("textColor")
    private String textColor;
    @JsonProperty("mainText")
    private String mainText;
    @JsonProperty("subText")
    private String subText;

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("bgColor")
    public String getBgColor() {
        return bgColor;
    }

    @JsonProperty("bgColor")
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    @JsonProperty("textColor")
    public String getTextColor() {
        return textColor;
    }

    @JsonProperty("textColor")
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    @JsonProperty("mainText")
    public String getMainText() {
        return mainText;
    }

    @JsonProperty("mainText")
    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    @JsonProperty("subText")
    public String getSubText() {
        return subText;
    }

    @JsonProperty("subText")
    public void setSubText(String subText) {
        this.subText = subText;
    }

}