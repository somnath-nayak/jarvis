package com.swiggy.api.sf.checkout.constants;

public interface EDVOConstants {

    String RESTAURANT_ID = "226";
    String MOBILE = "8197982351";
    String PASSWORD = "swiggy123";
    Integer ADDRESS_ID = 11126798; //14076467;//11130556;
    String TID = "9c1c3296-392f-4e13-b6b3-fede612f7bca";//"7e9b02f9-e0f4-493e-be1b-f63d9c6b6c70";//"ee1a868e-5238-488e-94c3-c1355dc1bd70";
    String TOKEN = "3dcc12ab-2cde-47f4-9021-aea87e90aa6d48183cae-c6e2-4760-b887-b9bd67bd1e13";//"ac423ecf-9a36-4e01-b881-642746aea94a7e5fce86-dd4f-4509-9177-659a2db26e0a";//"9d529b27-adfc-4347-b5ac-4820343e5846a3ea4d57-9c15-4f81-82c1-134f301eb06b";
    String AUTHORIZATION = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
    String PAYMENT_METHOD_CASH = "Cash";
    String ORDER_COMMENTS = "Automation Test Order";
    String VERSION_CODE = "300";
    String USER_AGENT = "Swiggy-Android";
    String SWUID = "Shaswat";

    //Cancel Order Data
    String CANCELLATION_REASON = "Automation Testing";
    String CANCELLATION_FEE_APPLICABILITY = "true";
    String CANCELLATION_FEE = "0";

    //Wait Time for Test
    int WAIT_TIME = 2000;

    /**
     * Meal TD Constants
     * Will be used to create EDVO Meal Trade Discount of different types
     */
    String NAMESPACE = "EDVO_Checkout";
    String HEADER = "EDVO_Checkout";
    String DESCRIPTION = "EDVO_Checkout";
    String CAMPAIGN_TYPE = "BXGY";
    String RESTAURANT_HIT = "100";
    boolean ENABLED = true;
    String SWIGGY_HIT = "0";
    String CREATED_BY = "SP";
    Integer DISCOUNT_CAP = 2000;
    String OPERATION_TYPE = "MEAL";
    Integer GROUP_ID_01 = 1;
    Integer GROUP_ID_02 = 2;
    Integer COUNT = 1;
    String REWARD_FUNCTION_MIN = "MIN";
    String REWARD_FUNCTION_MAX = "MAX";
    String REWARD_TYPE_FLAT = "Flat";
    String REWARD_TYPE_PERCENTAGE = "Percentage";
    String REWARD_TYPE_FINALPRICE = "FinalPrice";
    boolean COMMISION_ON_FULL_BILL = false;
    boolean TAXES_ON_DISCOUNTED_BILL = false;
    boolean FIRST_ORDER_RESTRICTION = false;
    boolean TIME_SLOT_RESTRICTION = false;
    boolean USER_RESTRICTION = false;

    String TD_MEAL_LEVEL = "mealLevel";
    String TD_ITEM_LEVEL = "itemLevel";
    Integer REWARD_VALUE_FLAT = 50;
    Integer REWARD_VALUE_FINAL_PRICE = 50;
    Integer REWARD_VALUE_PERCENTAGE = 50;
    Integer REWARD_VALUE_BOGO = 100;


    /**
     * Meal TD Selectors
     */
    String MEAL_TD_TYPE_01 = "MEAL_TD_TYPE_01"; // (buy x and y items, at Price P)
    String MEAL_TD_TYPE_02 = "MEAL_TD_TYPE_02"; // (buy x,y with %P off)
    String MEAL_TD_TYPE_03 = "MEAL_TD_TYPE_03"; // (buy x,y with  flat P off)
    String MEAL_TD_TYPE_04 = "MEAL_TD_TYPE_04"; // (buy x and y items at Price P each)
    String MEAL_TD_TYPE_05 = "MEAL_TD_TYPE_05"; // (buy x and y items with %P discount each)
    String MEAL_TD_TYPE_06 = "MEAL_TD_TYPE_06"; // (buy x and y items with flat P discount each)
    String MEAL_TD_TYPE_07 = "MEAL_TD_TYPE_07"; // (buy x,y , get y item free(y is min amoung both)
    String MEAL_TD_TYPE_08 = "MEAL_TD_TYPE_08"; // (buy x, y and z, get x at %P1, y at %P2, z at %P3)

    /**
     * Add New Address Data
     */
    String ADDRESS_NAME = "EDVO AUTOMATION";
    String ADDRESS_MOBILE = MOBILE;
    String ADDRESS_ADDRESS = "EDVO";
    String ADDRESS_LANDMARK = "EDVO";
    String ADDRESS_AREA = "EDVO";
    String ADDRESS_FLAT_NO = "EDVO";
    String ADDRESS_CITY = "EDVO";
    String ADDRESS_ANNOTATION = "WORK";

    /**
     * Create Meal Constants
     */
    String SCREEN_TITLE = "Breakfast item";
    String SCREEN_DESCRIPTION = "Choose your breakfast item";
    String TYPE = "m_group_k_choices";
    String NAME_1 = "Everyday Deal- ";
    String NAME_2 = " groups";

    //MEAL Launch Page
    String LP_IMAGE = "";
    String LP_BGCOLOR = "#0000e5";
    String LP_MAIN_TEXT = " Buy 3 & get 50 Rs off ";
    String LP_SUB_TEXT = "Buy 3 & get 50 Rs off ";
    String LP_TEXT_COLOR  = "#ffffff";
    String LP_TAG_TEXT = "Buy 3 & get 50 Rs off ";
    String LP_TAG_COLOR = "#ff0000";
    String LP_COMMUNICATION_TEXT = "Valid on a min. of 3 items";

    //MEAL Exit Page
    String EP_IMAGE = "qswwkdqklyk7s7bvpryk";
    String EP_BGCOLOR = "#0000e5";
    String EP_MAIN_TEXT = " Buy 3 & get 50 Rs off ";
    String EP_SUB_TEXT = "Buy 3 & get 50 Rs off ";
    String EP_TEXT_COLOR  = null;

    //Meal Screem Count
    int SCREEN_COUNT_02 = 2;
    int SCREEN_COUNT_03 = 3;
}


