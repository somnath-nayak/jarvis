package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.snd.constants.EDVOConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EDVOHelper implements EDVOConstants{

    Initialize gameofthrones = new Initialize();
    RedisHelper redisHelper= new RedisHelper();
    SnDHelper sndHelper = new SnDHelper();
    com.swiggy.api.sf.rng.helper.EDVOHelper rngEDVOHelper = new com.swiggy.api.sf.rng.helper.EDVOHelper();
    WireMockHelper wireMockHelper= new WireMockHelper();

    public Processor getMenuV4(String Id,String lat, String lng)
    {
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("version-code", "400");
        requestHeaders.put("user-agent", "Swiggy-Android");
        GameOfThronesService service = new GameOfThronesService("sand", "menuV4", gameofthrones);
        Processor processor = new Processor(service, requestHeaders, null,new String[] { Id,lat,lng});
        return processor;
    }

    public Processor getMealById(String mealId, String ItemId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "getMealById", gameofthrones);
        String[] payloadparams = {ItemId};
        Processor processor = new Processor(service, requestheaders, payloadparams,new String[] { mealId});
        return processor;
    }

    public Processor addMeal(String restId,String itemId1,String itemId2,String itemId3,String itemId4,String itemId5 )
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "addMeal", gameofthrones);
        String[] payloadparams = {restId,itemId1,itemId2,itemId3,itemId4,itemId5};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor getBulkMeals(String mealIds)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "getBulkMeals", gameofthrones);
        String[] payloadparams = new String[] { mealIds };
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor validateMeal(String group1, String item1, String quantity1,String group2, String item2, String quantity2,String mealId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "validateMeal", gameofthrones);
        String[] payloadparams = {group1,item1,quantity1,group2,item2,quantity2,mealId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor mealAvailable(String itemsIds,String restId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "mealAvailable", gameofthrones);
        String[] payloadparams = {itemsIds,restId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public String unescapeJSON(String jsonString){
        String jsonStringnew = jsonString.replaceAll("\\\\\"", "\"");
        return jsonStringnew;
    }

    public String escapeJSON(String jsonString){
        String escapedJsonString = jsonString.replaceAll("\"", "\\\"");
        return escapedJsonString;
    }

    public void clearEDVORedis(String restId, String mealId) throws InterruptedException {

        redisHelper.deleteKey("sandredisstage",0,SANDConstants.edvoRestMealKey+restId);
        redisHelper.deleteKey("sandredisstage",0,SANDConstants.edvoMealDefKey+mealId);
        redisHelper.deleteKey("sandredisstage",0,SANDConstants.edvoMealUiKey+mealId);
        Thread.sleep(4000);
    }

    public void clearEDVORedisWithoutRestId(String mealId) throws InterruptedException {

        redisHelper.deleteKey("sandredisstage",0,SANDConstants.edvoMealDefKey+mealId);
        redisHelper.deleteKey("sandredisstage",0,SANDConstants.edvoMealUiKey+mealId);
        Thread.sleep(4000);
    }
    public Processor validateMealMinMax(String group1, String item1, String quantity1,String item2, String quantity2, String group2, String item3, String quantity3,String mealId)
    {
        HashMap<String, String> requestheaders = new HashMap<>();
        requestheaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("sand", "validateMealMinMax", gameofthrones);
        String[] payloadparams = {group1,item1,quantity1,item2,quantity2,group2, item3,quantity3,mealId};
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public void deleteMealData(String restId){

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal1 + restId + EDVOConstants.deleteEDVOMeal7);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal2 + restId + EDVOConstants.deleteEDVOMeal8);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal3 + restId + EDVOConstants.deleteEDVOMeal8);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal4 + restId + EDVOConstants.deleteEDVOMeal8);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal5 + restId + EDVOConstants.deleteEDVOMeal8);
        sqlTemplate.update(EDVOConstants.deleteEDVOMeal6 + restId );

        com.swiggy.api.sf.rng.helper.EDVOHelper.deleteUserByMobile(restId);
    }

    public String createNegativeMealData(){

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        return (String.valueOf(sqlTemplate.queryForList(EDVOConstants.negativeMealData1).get(0).get("id")));

    }

    public List<Map<String, Object>> getEDVOGroupIds(String mealId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        return sqlTemplate.queryForList(EDVOConstants.getGroup1 + mealId);
    }

    public void mockServiceabilityforRest(String serviceability) throws IOException {
        File file = new File("../Data/MockAPI/ResponseBody/sand/MockServiceabilityForMeal.json");
        String resBody = FileUtils.readFileToString(file);
        resBody = resBody.replace("${serviceability}",serviceability);
        System.out.println(resBody);
        wireMockHelper.setupStub(mockAPI,mockstatusCode, mockcontentType,resBody);
    }

    public List<Map<String, Object>> getEDVOMealIds(String restId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return sqlTemplate.queryForList(CmsConstants.mealIdsbyRestId + restId);
    }

    public String getEDVOImage(String mealID) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.EDVOImage + mealID).get(0).get("image_id"));
    }

    public String getEDVOText(long campaignId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.rngDB);
        List<Map<String, Object>> list = sqlTemplate.queryForList(EDVOConstants.getEDVOText + campaignId);
        return (list.size() > 0) ? (list.get(0).get("operation_meta")).toString() : "Data not found";

    }

    public String getEDVOSubText(String mealID){

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(EDVOConstants.EDVOSubText + mealID).get(0).get("sub_text"));
    }

    public void setEDVOImage(String imageId, String mealId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.updateEDVOImage+"\'"+imageId+"\'"+EDVOConstants.updateEDVOmeal+mealId);
    }

    public void setEDVOText(String text, String mealId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        System.out.println(CmsConstants.updateEDVOText+text+CmsConstants.updateEDVOmeal+mealId);
        sqlTemplate.update(EDVOConstants.updateEDVOText+"\""+text+"\""+EDVOConstants.updateEDVOmeal+mealId);
    }

    public void setEDVOSubText(String subtext, String mealId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.rngDB);
        sqlTemplate.update(EDVOConstants.updateEDVOSubText+"\'"+subtext+"\'"+EDVOConstants.updateEDVOmeal+mealId);
    }


    public Processor updateMeals(String CronExpression, String restIds, String message, String headersAuthorization){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("cmsEdvoMealCron", "updateMeals", gameofthrones);
        Processor processor = new Processor(gots, requestHeader,  new String[]{CronExpression, restIds,message});
        return processor;
    }

    public Processor createMeals(String CronExpression, String restIds, String headersAuthorization){
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization", headersAuthorization);
        GameOfThronesService gots = new GameOfThronesService("cmsEdvoMealCron", "createMeals", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, new String[]{CronExpression, restIds});
        return processor;
    }

    public String getLaunchMainText(String mealId) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.launchMainText + mealId).get(0).get("main_text"));
    }

    public String getLaunchSubText( String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.launchSubText + mealId).get(0).get("sub_text"));
    }

    public String getExitMainText(String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.exitMainText + mealId).get(0).get("main_text"));
    }

    public String getExitSubText(String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.exitSubText + mealId).get(0).get("sub_text"));
    }

    public String getScreenTitle(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.screenTitle + groupId).get(0).get("screen_title"));
    }

    public String getScreenDescription(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.screenDescription + groupId).get(0).get("screen_description"));
    }

    public void setLaunchMainText(String mainText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updatelaunchMainText+"\""+mainText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setLaunchSubText(String subText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updatelaunchSubText+"\""+subText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setExitMainText(String mainText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateExitMainText+"\""+mainText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setExitSubText(String subText, String mealId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateExitSubText+"\""+subText+"\""+CmsConstants.updateEDVOMealId+mealId);
    }

    public void setScreenTitle(String title, String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenTitle+"\""+title+"\""+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setScreenDescription(String description, String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenDescription+"\""+description+"\""+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setScreenNumber(String groupId, int screenNumber){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateScreenNumber+screenNumber+CmsConstants.updateEDVOGroup+groupId);
    }

    public void setMinTotal(String groupId, int count){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.updateMinTotal+count+EDVOConstants.updateEDVOmeal+groupId);
    }

    public void setMaxTotal(String groupId, String count){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.updateMaxTotal+count+EDVOConstants.updateEDVOmeal+groupId);
    }

    public void setMinChoice(String groupId, String minChoice){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.updateMinChoice+minChoice+EDVOConstants.updateEDVOmeal+groupId);

    }

    public void setMaxChoice(String groupId, String maxChoice){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        sqlTemplate.update(EDVOConstants.updateMaxChoice+maxChoice+EDVOConstants.updateEDVOmeal+groupId);
    }

    public void setMinTotalForGroup(String groupId, String minTotal){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMinTotal+minTotal+CmsConstants.updateEDVOmeal+groupId);
    }

    public void setMaxTotalForGroup(String groupId, String maxTotal){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.updateMaxTotal+maxTotal+CmsConstants.updateEDVOmeal+groupId);
    }

    public String getMinChoice(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMinChoice+CmsConstants.updateEDVOmeal+ groupId).get(0).get("min_choices"));
    }

    public String getMaxChoice(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getMaxChoice+CmsConstants.updateEDVOmeal+ groupId).get(0).get("max_choices"));
    }

    public String getMinTotal(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(EDVOConstants.getMinQuantity+EDVOConstants.updateEDVOmeal+ groupId).get(0).get("min_total"));
    }

    public String getMaxTotal(String groupId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(EDVOConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(EDVOConstants.getMaxQuantity+EDVOConstants.updateEDVOmeal+ groupId).get(0).get("max_total"));
    }

    public String getItemMaxQuantity(String groupId, String itemId){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        return String.valueOf(sqlTemplate.queryForList(CmsConstants.getItemMaxQuantity+CmsConstants.updateEDVOGroup+ groupId+CmsConstants.getItemMaxQuantity1+itemId).get(0).get("max_quantity"));
    }

    public void setItemMaxQuantity(String count, String groupId, String itemId) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
        sqlTemplate.update(CmsConstants.setItemMaxQuantity + count + CmsConstants.updateEDVOGroup + groupId + CmsConstants.setItemMaxQuantity1 + itemId);

    }


    /*
     * Shaswat +++++
     */
    //Creates meal
    public Processor createMeal(String payload){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("sand", "createmeal", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{payload});
    }
    /*
     * Shaswat #####
     */
}
