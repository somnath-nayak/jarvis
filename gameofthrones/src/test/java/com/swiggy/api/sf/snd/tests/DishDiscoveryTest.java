package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.TimeSlotsHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.IStatusConstants;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.DishDiscoveryDP;
import com.swiggy.api.sf.snd.helper.DishDiscovery;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import io.gatling.core.json.Json;
import org.apache.commons.lang.time.DateUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class DishDiscoveryTest extends DishDiscoveryDP implements IStatusConstants {

    DishDiscovery dishDiscovery= new DishDiscovery();
    TimeSlotsHelper timeSlotsHelper= new TimeSlotsHelper();
    RedisHelper redisHelper= new RedisHelper();
    SANDHelper sandHelper= new SANDHelper();
    RngHelper rngHelper = new RngHelper();
    OrderPlace orderPlace = new OrderPlace();



    /*@Test(dataProvider = "getItemName")
    public void searchItemName(String payload, String item)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], item),"item is not matching");
        }
    }

    @Test(dataProvider = "getItemId")
    public void searchItemId(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");

    }

    @Test(dataProvider = "getItemPrice")
    public void searchItemPrice(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
    }

    @Test(dataProvider = "getItemEnabled")
    public void searchItemEnabled(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
    }

    @Test(dataProvider = "getItemLongDistance")
    public void searchItemLongDistance(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
    }

    @Test(dataProvider = "getRestId")
    public void searchRestId(String payload, String rest)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<restId.length; i++){
            Assert.assertTrue(containsIgnoreCase(restId[i], rest),"rest is not matching");
        }
    }


    @Test(dataProvider = "getRestName")
    public void searchRestName(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        //String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");
        *//*for(int i =0; i<restId.length; i++){
            Assert.assertTrue(containsIgnoreCase(restId[i], rest),"rest is not matching");
        }*//*
    }

    @Test(dataProvider = "getRestLoc")
    public void searchRestLoc(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        //String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");

    }

    @Test(dataProvider = "getRestEnabled")
    public void searchRestEnabled(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
    }

    @Test(dataProvider = "getRestLongDistance")
    public void searchRestLongDistance(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
    }

    @Test(dataProvider = "getCityId")
    public void searchCityId(String payload, String rest)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<restId.length; i++){
            Assert.assertTrue(containsIgnoreCase(restId[i], rest),"rest is not matching");
        }
    }

    @Test(dataProvider = "getCityName")
    public void searchCityName(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");

    }

    @Test(dataProvider = "getAreaName")
    public void searchAreaName(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        //String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");
        *//*for(int i =0; i<restId.length; i++){
            Assert.assertTrue(containsIgnoreCase(restId[i], rest),"rest is not matching");
        }*//*
    }

    @Test(dataProvider = "getAreaId")
    public void searchAreaId(String payload)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] restId= JsonPath.read(responseDebug, "$.data..entities[*].restaurantId").toString().replace("[","").replace("]","").replace("\"","").split(",");
        *//*for(int i =0; i<restId.length; i++){
            Assert.assertTrue(containsIgnoreCase(restId[i], rest),"rest is not matching");
        }*//*
    }

    @Test(dataProvider = "getItemComb")
    public void searchItemCOmb(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemRestComb")
    public void searchItemRestCOmb(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemRestTagComb")
    public void searchItemRestTagCombo(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }


    @Test(dataProvider = "getItemRestTagOrdersCount")
    public void searchItemRestTagOrder(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemRestTagAreaOrdersCount")
    public void searchItemRestTagAreaOrder(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,"ITEM", "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,"ITEM", "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemIdTagAreaOrdersCount")
    public void searchItemRestTagAreaOrdersCount(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,SANDConstants.item, "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,SANDConstants.item, "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemIdRestIDTagArea")
    public void searchItemRestTagArea(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,SANDConstants.item, "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,SANDConstants.item, "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }

    @Test(dataProvider = "getItemIdPrice")
    public void searchItemRelatedData(String payload, String dish)
    {
        String response= dishDiscovery.search(payload).ResponseValidator.GetBodyAsText();
        status(response);
        String entity= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity,SANDConstants.item, "Entity type is not ITEM");
        String responseDebug= dishDiscovery.searchDebug(payload).ResponseValidator.GetBodyAsText();
        status(responseDebug);
        String entity2= JsonPath.read(response,"$.data..entities[0]..type").toString().replace("[","").replace("]","").replace("\"","");
        Assert.assertEquals(entity2,SANDConstants.item, "Entity type is not ITEM");
        String[] name= JsonPath.read(responseDebug, "$.data..entities[*].name").toString().replace("[","").replace("]","").replace("\"","").split(",");
        for(int i =0; i<name.length; i++){
            Assert.assertTrue(containsIgnoreCase(name[i], dish),"rest is not matching");
        }
    }
*/

    @Test(dataProvider = "collectionLatLng",description = "Check collection V2 data")
    public void collectionAggregator(String payload)
    {
        sandHelper.sndD();
        String response= dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        String collection= JsonPath.read(response,"$.data.collectionsV2.collectionOfCollections").toString().replace("[","").replace("]","");

    }

    @Test(dataProvider = "collectionLatLng", description = "check CollectionV2 node exists")
    public void collectionAggregatorNode(String payload)
    {
        Processor processor= dishDiscovery.collectionAggregator(payload);
       if(processor.ResponseValidator.DoesNodeExists("data.collectionsV2"))
       {
           String resp= processor.ResponseValidator.GetBodyAsText();
           List<String> simpleCollections=Arrays.asList(JsonPath.read(resp,"$.data.collectionsV2.simpleCollections").toString().replace("[","").replace("]","").split(","));
           if(processor.ResponseValidator.DoesNodeExists("data.collectionsV2.simpleCollections"))
           {
               //List<String> simpleCollections=Arrays.asList(JsonPath.read(resp,"$.data.collectionsV2.simpleCollections").toString().replace("[","").replace("]","").split(","))
               Assert.assertNotNull(simpleCollections);
           }
           if(processor.ResponseValidator.DoesNodeExists("data.collectionsV2.collectionOfCollections"))
           {
               String[] collectionOfCollections=JsonPath.read(resp,"$.data.collectionsV2.collectionOfCollections").toString().replace("[","").replace("]","").split(",");
               Assert.assertNotNull(collectionOfCollections);
               List<String> childCollection= Arrays.asList(JsonPath.read(resp,"$.data.collectionsV2.collectionOfCollections..collectionIds").toString().replace("[","")
                       .replace("]","").split(","));
                   Set<String> intersect = new HashSet<String>(simpleCollections);
                   intersect.retainAll(childCollection);
                   for(int i=0; i<intersect.size(); i++){
                       Assert.assertTrue(intersect.contains(childCollection.get(i)));
               }
           }
       }
    }

    @Test(dataProvider = "collectionLatLng", description = "check Collectionv2 node is not empty")
    public void collectionAggregatorisEmpty(String payload)
    {
        String response= dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        Assert.assertNotNull(sandHelper.JsonString(response, "$.data.collectionsV2"), "is null");
    }

    @Test(dataProvider = "collectionLatLng", description = "NOrmal collection ")
    public void verifyNormalCollection(String payload)
    {
        Processor processor= dishDiscovery.collectionAggregator(payload);
        String response=processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[","").replace("]","").split(","));
        for(String id:collectionIds) {
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists("data.collectionsV2.collectionsData."+id.replace("\"","")));
        }
    }
//apply holday slot on rest and item
    @Test(dataProvider = "collectionLatLng", description = "Check rest availability and servicibilty")
    public void collectionAggregatorRestAvailabilityAndServicibility(String payload)
    {
        Processor processor= dishDiscovery.collectionAggregator(payload);
        String response=processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[","").replace("]","").split(","));
        for(String id:collectionIds){
            String r="data.collectionsV2.collectionsData."+id.replace("\"","")+".menuletData.restId";
            Assert.assertFalse(processor.ResponseValidator.DoesNodeExists(r));
            List<String> restId= Arrays.asList(JsonPath.read(response,"$.data.collectionsV2.collectionsData."+id.replace("\"","")+".menuletData..restId").toString().replace("[","").
                    replace("]","").split(","));
            for(String rest:restId){
                String restaurantServicibility= JsonPath.read(response,"$.data.restaurants."+rest.replace("\"","")+".sla.serviceability").toString().
                        replace("[","").replace("]","").replace("\"","");

                //System.out.println("rtestata"+ rest);
                if(!restaurantServicibility.contains("SERVICABLE")){
                    Assert.assertTrue(restaurantServicibility.contains("SERVICEABLE_WITH_BANNER"));

                }else Assert.assertTrue(restaurantServicibility.contains("SERVICEABLE"));
                String availabilty= JsonPath.read(response,"$.data.restaurants."+rest.replace("\"","")+".availability.opened").toString().
                        replace("[","").replace("]","");
                Assert.assertTrue(availabilty.contains("true"), "false");
            }
        }
    }


    @Test(dataProvider = "collectionLatLng", description = "Check Item availability and servicibilty")
    public void collectionAggregatorItemAvailabilityAndServicibility(String payload)
    {
        Processor processor= dishDiscovery.collectionAggregator(payload);
        String response=processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[","").replace("]","").split(","));
        for(String id:collectionIds){
            String r="data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            List<String> itemsId= Arrays.asList(JsonPath.read(response,"$.data.collectionsV2.collectionsData."+id.replace("\"","")+".menuletData..itemIds").toString().replace("[","").
                    replace("]","").split(","));
            for(String item:itemsId){
                String itemStock= JsonPath.read(response,"$.data.items."+item.replace("\"","")+".inStock").toString().
                        replace("[","").replace("]","").replace("\"","");
                String itemEnabled= JsonPath.read(response,"$.data.items."+item.replace("\"","")+".enabled").toString().
                        replace("[","").replace("]","").replace("\"","");
                Assert.assertNotEquals(itemEnabled,"1","item is not enabled");
                Assert.assertNotEquals(itemStock,"1","item is not in stock");

            }
        }
    }

    @Test(dataProvider = "collectionLatLng2", description = "Check Item availability and servicibilty")
    public void collectionAggregatorMultiTD(String payload, String lat, String lng) throws InterruptedException
    {
        sandHelper.sndD();
    List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
        freeBie(listOfRestIds.get(1), "100");
        rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(1), false, false, false, "ZERO_DAYS_DORMANT", false, false);
    } catch (Exception e) {
        Assert.assertTrue(false, "discount not created");
    }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(1));
    String aggAfterDiscount = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(1) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel)){
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(1) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));
        }else Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(1) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(1) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.freebie));
    }



    @Test(dataProvider = "collectionLatLng1", description = "Check Item availability and servicibilty")
    public void collectionAggregatorFF(String payload, String lat, String lng) throws InterruptedException
    {
        sandHelper.sndD();
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            freeBie(listOfRestIds.get(3), "100");
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(3), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(3));
        String aggAfterDiscount = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType"));
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".aggregatedDiscountInfo.shortDescriptionList..discountType"));
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel)){
           Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));
        }else Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.freebie));

    }

    public void freeBie(String restId,String minAmount){
        HashMap<String, String> hMap;
        sandHelper.TMenu(restId);
        String item = orderPlace.getItemFromMenu(restId, null, null);
        try {
            hMap = rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel(minAmount, restId, item);
            String getTid = RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(sandHelper.JsonString(getTid, "$.data.ruleDiscount.type"), SANDConstants.freebie, "No discounts present or different type of discount is there");
        } catch (Exception e) {
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
    }


    /**
     * Check item availability(elastic cache)
     * @param payload
     */
    @Test(dataProvider = "collectionLatLng1",description = "Check item out of stock")
    public void collectionAggregatorFreeDelOnly(String payload, String lat, String lng) throws InterruptedException{
        sandHelper.sndD();
        System.out.println(payload);
        List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
        try {
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(4),"100" ,"10", false);
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(4), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        }
        catch(Exception e){
            Assert.assertTrue(false,"discount not created");
        }
        Thread.sleep(4000);
        String response = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        String r="$.data.collectionsV2.collectionOfCollections";
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.."+listOfRestIds.get(4)+".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel));
        Assert.assertTrue(sandHelper.JsonString(response, "$.data.."+listOfRestIds.get(4)+".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.freeDel));

    }

    @Test(dataProvider = "collectionLatLng1", description = "TD on Restaurant")
    public void collectionAggregatorTDRest(String payload, String lat, String lng) throws InterruptedException{
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
           //List<String> restId = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionsData." + id.replace("\"", "") + ".menuletData..restId").toString().replace("[", "").
                  //  replace("]", "").split(","));
            List<String> listOfRestIds=orderPlace.aggregatorTD(new String[]{lat,lng}, null, null);
            try {
                rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(4),"100" ,"10", false);
                //rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(4), false, false, false, "ZERO_DAYS_DORMANT", false, false);
            }
            catch(Exception e){
                Assert.assertTrue(false,"discount not created");
            }
            Thread.sleep(4000);
            String response1 = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
            System.out.println(sandHelper.JsonString(response1, "$.data.." + listOfRestIds.get(4) + ".tradeDiscountInfo..discountType").contains(SANDConstants.flat));
            Assert.assertTrue(sandHelper.JsonString(response1, "$.data.." + listOfRestIds.get(4) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.flat));
        }
    }


    @Test(dataProvider = "collectionLatLngTag", description = "TD on Restaurant")
    public void collectionAggregatorTag(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            Assert.assertTrue(!collectionIds.contains(sandHelper.id()));
        }
    }


    @Test(dataProvider = "collectionLatLngTagEquals", description = "TD on Restaurant")
    public void collectionAggregatorTagEq(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            Assert.assertTrue(!collectionIds.contains(sandHelper.id()));
        }
    }


    @Test(dataProvider = "collectionLatLngSla", description = "TD on Restaurant")
    public void collectionAggregatorTagSla(String payload, String lat, String lng, String tag) throws InterruptedException{
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            Assert.assertTrue(!collectionIds.contains(sandHelper.id()));
        }
    }


    @Test(dataProvider = "collectionLatLngItem", description = "if item is present in particular lat lng, collection will come")
    public void collectionAggregatorTagItem(String payload, String lat, String lng, String tag) throws InterruptedException{
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            String colId= sandHelper.id();
            if(!collectionIds.contains(sandHelper.id())) {
                Assert.assertTrue(!collectionIds.contains(colId));
            }else
                Assert.assertTrue(collectionIds.contains(colId));
        }
    }


    @Test(dataProvider = "collectionLatLngLong", description = "if item is present in particular lat lng, collection will come")
    public void collectionAggregatorTagLong(String payload, String lat, String lng, String tag) throws InterruptedException{
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            if(!collectionIds.contains(sandHelper.id())) {
                Assert.assertTrue(!collectionIds.contains(sandHelper.id()));
            }else
                Assert.assertTrue(collectionIds.contains(sandHelper.id()));
        }
    }


    @Test(dataProvider = "collectionLatLngMultiBTM", description = "")
    public void collectionAggregatorMultiFF(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            freeBie(listOfRestIds.get(3), "100");
            rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", listOfRestIds.get(3), false, false, false, "ZERO_DAYS_DORMANT", false, false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(3));
        String aggAfterDiscount = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType"));
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".aggregatedDiscountInfo.shortDescriptionList..discountType"));
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel)){
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));
        }else Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freeDel));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(3) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.freebie));

    }


    @Test(dataProvider = "collectionLatLngMultiBTM", description = "")
    public void collectionAggregatorMultiFlatF(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        System.out.println("-->"+listOfRestIds.size());
        try {
            freeBie(listOfRestIds.get(2), "100");
            rngHelper.createFlatWithMinCartAmountAtRestaurantLevel(listOfRestIds.get(2),"100" ,"10", false);
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(2));
        String aggAfterDiscount = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".tradeDiscountInfo..discountType"));
        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".aggregatedDiscountInfo.shortDescriptionList..discountType"));
        if(!sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".tradeDiscountInfo..discountType").contains(SANDConstants.flat)){
            Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));
        }else Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".tradeDiscountInfo..discountType").contains(SANDConstants.flat));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(2) + ".aggregatedDiscountInfo.shortDescriptionList..discountType").contains(SANDConstants.freebie));
    }



    @Test(dataProvider = "collectionLatLngMultiBTM99", description = "")
    public void collectionAggregatoPrice99(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        Processor processor = dishDiscovery.collectionAggregator(payload);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
        for (String id : collectionIds) {
            String r = "data.collectionsV2.collectionsData.";
            Assert.assertTrue(processor.ResponseValidator.DoesNodeExists(r));
            Assert.assertTrue(!collectionIds.contains(sandHelper.id()));
        }
    }




    @Test(dataProvider = "collectionLatLngMulti", description = "")
    public void collectionAggregatorFreebie(String payload, String lat, String lng, String tag) throws InterruptedException{
        sandHelper.sndCol(tag);
        sandHelper.sndDD();
        List<String> listOfRestIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            freeBie(listOfRestIds.get(4), "100");
        } catch (Exception e) {
            Assert.assertTrue(false, "discount not created");
        }
        Thread.sleep(4000);
        System.out.println(listOfRestIds.get(4));
        String aggAfterDiscount = dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
        List<String> collectionIds = Arrays.asList(JsonPath.read(aggAfterDiscount, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[", "").replace("]", "").split(","));
            Assert.assertTrue(!collectionIds.contains(sandHelper.id()));

        System.out.println(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(4) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));
        Assert.assertTrue(sandHelper.JsonString(aggAfterDiscount, "$.data.." + listOfRestIds.get(4) + ".tradeDiscountInfo..discountType").contains(SANDConstants.freebie));

    }








    @Test(dataProvider = "collectionHolidaySlot", description = "TD on Restaurant")
    public void collectionAggregatorHolidaySlotRest(String payload){

        Processor processor= dishDiscovery.collectionAggregator(payload);
        String response=processor.ResponseValidator.GetBodyAsText();
        List<String> collectionIds= Arrays.asList(JsonPath.read(response, "$.data.collectionsV2.collectionOfCollections..collectionIds").toString().
                replace("[","").replace("]","").split(","));
        for(String id:collectionIds){
            List<String> restId= Arrays.asList(JsonPath.read(response,"$.data.collectionsV2.collectionsData."+id.replace("\"","")+".menuletData..restId").toString().replace("[","").replace("]","").split(","));
            for(String rest:restId){
                String slotTiming=dishDiscovery.postHolidayTimeSlots(rest,extendedTime(5),extendedTime(10)).ResponseValidator.GetBodyAsText();
                redisHelper.publish("sandredisstage","rest_holidayslots_change",publishMessageRest(rest.replace("\"","")));
                String response2=dishDiscovery.collectionAggregator(payload).ResponseValidator.GetBodyAsText();
                String restaurantServicibility= JsonPath.read(response2,"$.data.restaurants."+rest.replace("\"","")+".availability.nextCloseTime").toString().
                        replace("[","").replace("]","").replace("\"","");
                System.out.println("rko"+restaurantServicibility);
                break;
            }
            break;
            }
        }

    public void status(String response)
    {
        String statusCode= JsonPath.read(response, "$.internalStatusCode").toString().replace("[","").replace("]","");
        String message= JsonPath.read(response, "$.mainMessage").toString().replace("[","").replace("]","");
        Assert.assertEquals(statusCode, SANDConstants.statusCode, "Status code is not 0");
        Assert.assertEquals(message, SANDConstants.message, "failed ");

    }

    public boolean containsIgnoreCase(String str, String searchStr)     {
        if(str == null || searchStr == null) return false;
        final int length = searchStr.length();
        if (length == 0)
            return true;
        for (int i = str.length() - length; i >= 0; i--) {
            if (str.regionMatches(true, i, searchStr, 0, length))
                return true;
        }
        return false;
    }

    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime;
        return DT;
    }
    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+"T"+dateFormat;
        return DT;
    }

}
