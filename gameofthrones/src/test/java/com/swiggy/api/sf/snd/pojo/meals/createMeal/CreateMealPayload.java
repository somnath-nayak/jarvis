package com.swiggy.api.sf.snd.pojo.meals.createMeal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "restId",
        "type",
        "name",
        "minTotal",
        "maxTotal",
        "tdNecessary",
        "launchPage",
        "exitPage",
        "screens",
        "externalId"
})
public class CreateMealPayload {

    @JsonProperty("restId")
    private Integer restId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name = "Automation Test Meal";
    @JsonProperty("minTotal")
    private Integer minTotal;
    @JsonProperty("maxTotal")
    private Integer maxTotal;
    @JsonProperty("tdNecessary")
    private Boolean tdNecessary = false;
    @JsonProperty("launchPage")
    private LaunchPage launchPage;
    @JsonProperty("exitPage")
    private ExitPage exitPage;
    @JsonProperty("screens")
    private List<Screen> screens = null;
    @JsonProperty("externalId")
    private Integer externalId;

    @JsonProperty("restId")
    public Integer getRestId() {
        return restId;
    }

    @JsonProperty("restId")
    public void setRestId(Integer restId) {
        this.restId = restId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("minTotal")
    public Integer getMinTotal() {
        return minTotal;
    }

    @JsonProperty("minTotal")
    public void setMinTotal(Integer minTotal) {
        this.minTotal = minTotal;
    }

    @JsonProperty("maxTotal")
    public Integer getMaxTotal() {
        return maxTotal;
    }

    @JsonProperty("maxTotal")
    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    @JsonProperty("tdNecessary")
    public Boolean getTdNecessary() {
        return tdNecessary;
    }

    @JsonProperty("tdNecessary")
    public void setTdNecessary(Boolean tdNecessary) {
        this.tdNecessary = tdNecessary;
    }

    @JsonProperty("launchPage")
    public LaunchPage getLaunchPage() {
        return launchPage;
    }

    @JsonProperty("launchPage")
    public void setLaunchPage(LaunchPage launchPage) {
        this.launchPage = launchPage;
    }

    @JsonProperty("exitPage")
    public ExitPage getExitPage() {
        return exitPage;
    }

    @JsonProperty("exitPage")
    public void setExitPage(ExitPage exitPage) {
        this.exitPage = exitPage;
    }

    @JsonProperty("screens")
    public List<Screen> getScreens() {
        return screens;
    }

    @JsonProperty("screens")
    public void setScreens(List<Screen> screens) {
        this.screens = screens;
    }

    @JsonProperty("externalId")
    public Integer getExternalId() {
        return externalId;
    }

    @JsonProperty("externalId")
    public void setExternalId(Integer externalId) {
        this.externalId = externalId;
    }

}