package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Slot {

    private String day;
    private Integer openTime;
    private Integer closeTime;

    /**
     * No args constructor for use in serialization
     *
     */
    public Slot() {
    }

    /**
     *
     * @param closeTime
     * @param openTime
     * @param day
     */
    public Slot(String day, Integer openTime, Integer closeTime) {
        super();
        this.day = day;
        this.openTime = openTime;
        this.closeTime = closeTime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public Integer getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Integer openTime) {
        this.openTime = openTime;
    }

    public Integer getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("day", day).append("openTime", openTime).append("closeTime", closeTime).toString();
    }

}