package com.swiggy.api.sf.rng.constants;

public interface SegmentationConstants {
    String segmentationRMQUser = "guest";
    String getSegmentationRMQPwd = "guest";
    String[] paymentMethod = {"PayTM-SSO", "Cash", "Freecharge-SSO", "Juspay", "Juspay-NB", "Sodexo", "Mobikwik", "UPICollect", "PhonePe", "AmazonPay"};
    String[] cartType = {"shared", "pre_order", "POP", "CAFE", "regular", "swiggy_express", "PREORDER", "promo"};
    String segmentation = "segmentationDB";
    String[] igccSegment = {"H", "L", "M", "NA", "NULL"};
    String[] valueSegment = {"H", "L", "M", "NA", "NULL"};
    String[] premiumSegment = {"0", "1", "NULL"};
    String[] marketingSegment = {"A", "B", "C", "D", "NULL"};
    String[] orderStatus = {"processing", "cancel"};
    String[] paymentTxnStatus = {"success", "refund-initiated", "failed"};


//Cart type Group(“shared”), Pre(“pre_order”), Pop(“POP”), Cafe(“cafe”),
//   Regular(“regular”), SwiggyExpress(“swiggy_express”), CloudKitchen(“cloud_kitchen”),Promo(“promo”);

}
