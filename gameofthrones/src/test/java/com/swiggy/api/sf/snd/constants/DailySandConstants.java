package com.swiggy.api.sf.snd.constants;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.constants
 **/
public interface DailySandConstants {
    String sandRedis="dailysandredis";
    String userAgent="ANDROID";
    String mobile = "9899072197";
    String password = "rkonowhere";
    String user_id="user_id";
    String lat="17.9689";
    String lng="79.5941";
    String blackzone_lat = "17.968928";
    String blackzone_lng = "79.597699";
    String blackzone_zoneId = "12346";
    String invalid_lat = "7.107545";
    String invalid_lng = "80.997771";
    String invalid_city_id = "99";
    String dummy_sid = "123-123-123-123-123";
    String dummy_tid = "41d32168-1482-4713-a309-0b8ec1fb604c";
    String cityId = "75";
    int statusOne = 1;
    int statusZero = 0;
    String slotType_meal = "MEAL";
    String slotType_plan = "PLAN";
    String get_delivery_blackzone_slots = "SELECT * FROM daily_timeslots WHERE zone=";
    String redis_block_size = "PAGINATION_BLOCK_SIZE";
    String redis_ios_page_size = "PAGINATION_BLOCK_SIZE";
    String redis_android_page_size = "PAGINATION_ANDROID_PAGE_SIZE";
    String[] SlotKeys = {"LUNCH", "DINNER"};
    int min_number_of_meals_for_plan = 3;
    String dummyCustomerId = "33665082";
    String cms_base_variant = "Tenure";
    String cms_listing_include_only = "AVAILABILITY,INVENTORY";
    String cms_meta_include_only = "LIST-META";
    String customerId_with_no_subscriptions = "1601908";
    String tenantId = "9012d90sdck3o48";
    String authentication = "Basic dXNlcjpjaGVjaw==";

}
