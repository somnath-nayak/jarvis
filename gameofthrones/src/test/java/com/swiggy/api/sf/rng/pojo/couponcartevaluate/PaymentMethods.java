package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Arrays;
import java.util.List;

public class PaymentMethods {
	@JsonProperty("payment_codes")
	private List<String> paymentCodes = null;

	@JsonProperty("payment_codes")
	public List<String> getPaymentCodes() {
		return paymentCodes;
	}

	@JsonProperty("payment_codes")
	public void setPaymentCodes(List<String> paymentCodes) {
		this.paymentCodes = paymentCodes;
	}

	public PaymentMethods withPaymentCodes(List<String> paymentCodes) {
		this.paymentCodes = paymentCodes;
		return this;
	}
	public PaymentMethods setDefaultData() {
		return this.withPaymentCodes(
				Arrays.asList("PayTM", "PhonePe", "Mobikwik", "Freecharge", "NEW_CARD", "Sodexo", "NB_HDFC", "NB_SBI",
						"NB_ICICI", "NB_AXIS", "NB_KOTAK", "NB_YESB", "NB_CITI", "PayLater_Lazypay", "cod"));

	}
}
