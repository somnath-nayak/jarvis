package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class DataDistance {

    private Point point;
    private String distance;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataDistance() {
    }

    /**
     *
     * @param point
     * @param distance
     */
    public DataDistance(Point point, String distance) {
        super();
        this.point = point;
        this.distance = distance;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("point", point).append("distance", distance).toString();
    }

}