package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "apiKey",
        "authToken"
})
public class MySmsGetAllconversations {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsGetAllconversations() {
    }

    /**
     *
     * @param authToken
     * @param apiKey
     */
    public MySmsGetAllconversations(String apiKey, String authToken) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).toString();
    }

}