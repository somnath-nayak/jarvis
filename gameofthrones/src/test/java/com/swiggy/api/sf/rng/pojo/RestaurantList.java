package com.swiggy.api.sf.rng.pojo;

import com.redis.S;
import com.swiggy.api.sf.rng.helper.Utility;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class RestaurantList {

    private String id;
    private String name;
    private List<Category> categories = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantList() {
    }

    /**
     *
     * @param id
     * @param name
     * @param categories
     */
    public RestaurantList(String id, String name, List<Category> categories) {
        super();
        this.id = id;
        this.name = name;
        this.categories = categories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public RestaurantList withRestIdAndName(String restId, String restName){
       this.id = restId;
       this.name = restName;
       return this;
    }

    public RestaurantList withCategory(String restId, String categoryId){
        Category category = new Category();
        List<Category> list = new ArrayList<Category>();
        list.add(0,category.withCategory(categoryId));
        this.id = restId;
        this.name = "testRest";
        this.categories = list;
        return this;
    }

    public RestaurantList withRestList(String restId,String categoryId, String subCategoryId,List<Menu> menuList){
        Category category = new Category();
        List<Category> list = new ArrayList<Category>();
        list.add(0,category.withSubCategory(categoryId,subCategoryId,menuList));
        this.id = restId;
        this.name = "testRest";
        this.categories = list;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("categories", categories).toString();
    }

}