package com.swiggy.api.sf.rng.dp.Segmentation;

import com.swiggy.api.sf.rng.constants.SegmentationConstants;
import com.swiggy.api.sf.rng.helper.SegmentationDbHelper;
import com.swiggy.api.sf.rng.helper.SegmentationHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.ads.Segmentation.*;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;

public class SegmentationDP {
    OrderCreateMessageBuilder orderCreateMessageBuilder = new OrderCreateMessageBuilder();
    SegmentationHelper segmentationHelper = new SegmentationHelper();
    OrderCancelMessageBuilder orderCancelMessageBuilder = new OrderCancelMessageBuilder();
    
    @DataProvider(name = "checkDataInDBForOrderStatsAndOrderCancelDP")
    public Object[][] checkDataInDBForOrderStatsAndOrderCancel() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[2], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "success" ,SegmentationConstants.orderStatus[0]);

        OrderCreateMessage payload = orderCreateMessageBuilder.orderCreateMessageByCartTypeUserId(userId, SegmentationConstants.cartType[2], orderId, restId, SegmentationHelper.getDate());
        //yyyy-MM-dd HH:mm:ss
    
        OrderCancelMessage payloadTwo = orderCancelMessageBuilder.orderCancelByCartTypeUserId(userId, SegmentationConstants.cartType[2], orderId, SegmentationConstants.orderStatus[1], SegmentationConstants.paymentMethod[0],false);
        
        return new Object[][]{{payload,payloadTwo ,userId,orderId, restId}};
    }
    
    @DataProvider(name = "getCartTypePOPOrderCountByUserIdDP")
    public Object[][] getCartTypePOPOrderCountByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[2], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(),SegmentationConstants.paymentTxnStatus[0], SegmentationConstants.orderStatus[0]);
        
        OrderCreateMessage payload = orderCreateMessageBuilder.orderCreateMessageByCartTypeUserId(userId, SegmentationConstants.cartType[2], orderId, restId, SegmentationHelper.getDate());
        //yyyy-MM-dd HH:mm:ss
        
        return new Object[][]{{payload, userId}};
    }
    
    @DataProvider(name = "getMultipleCartTypeCAFEAndPOPOrderCountByUserIdDP")
    public Object[][] getMultipleCartTypeCAFEAndPOPOrderCountByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        //Mocking checkout response for Cart type CAFE
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), SegmentationConstants.paymentTxnStatus[0],SegmentationConstants.orderStatus[0]);
    
        // Payload to push in queue for Cart type CAFE
        OrderCreateMessage OrderCreatePayloadOne = orderCreateMessageBuilder.orderCreateMessageByCartTypeUserId(userId, SegmentationConstants.cartType[3], orderId, restId, SegmentationHelper.getDate());

//        segmentationHelper.stubCheckoutOrder(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "processing");
    
        // Payload two to push in queue for Cart type POP
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByCartTypeUserId(userId, SegmentationConstants.cartType[2], orderSecondId, restId, SegmentationHelper.getPastTime(1));
        //yyyy-MM-dd HH:mm:ss
        
        return new Object[][]{{OrderCreatePayloadOne, OrderCreatePayloadTwo, userId, orderSecondId, restId}};
    }
    
    
    @DataProvider(name = "getCartTypeOrderCountForNewUserIdDP")
    public Object[][] getCartTypeOrderCountForNewUserId() throws IOException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getOrderCountByPaymentMethodByUserIdDP")
    public Object[][] getOrderCountByPaymentMethodByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String currentTime =SegmentationHelper.getDate();
      
        //Mocking checkout response for Payment type PayTM
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), SegmentationConstants.paymentTxnStatus[0],SegmentationConstants.orderStatus[0]);
        
        // Payload to push in queue for Payment type PayTM
        OrderCreateMessage OrderCreatePayloadOne = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[0], orderId,  restId,currentTime );

//        segmentationHelper.stubCheckoutOrder(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "processing");
        
        // Payload two to push in queue for Payment type PhonePe
        //yyyy-MM-dd HH:mm:ss
    
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, SegmentationHelper.getPastTime(1));
        
        return new Object[][]{{OrderCreatePayloadOne, OrderCreatePayloadTwo, userId, orderSecondId, restId}};
    }
    
    @DataProvider(name = "getOrderCountWhenPaymentMethodIsFalseByUserIdDP")
    public Object[][] getOrderCountWhenPaymentMethodIsFalseByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String currentTime =SegmentationHelper.getDate();
        
        //Mocking checkout response for Payment type PayTM
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(),SegmentationConstants.paymentTxnStatus[0] ,SegmentationConstants.orderStatus[0]);
        
        // Payload to push in queue for Payment type PayTM
        OrderCreateMessage OrderCreatePayloadOne = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[0], orderId,  restId,currentTime );

//        segmentationHelper.stubCheckoutOrder(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "processing");
        
        // Payload two to push in queue for Payment type PhonePe
        //yyyy-MM-dd HH:mm:ss
        
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, SegmentationHelper.getPastTime(1));
        
        return new Object[][]{{OrderCreatePayloadOne, OrderCreatePayloadTwo, userId, orderSecondId, restId}};
    }
    
    @DataProvider(name = "getUserRestaurantLastOrderByUserIdDP")
    public Object[][] getUserRestaurantLastOrderByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String[] allRestId = {restId};
        //String currentTime =SegmentationHelper.getDate();
        String[] currentTime = SegmentationHelper.getDateInFormatAndInMillisecond();
        //Mocking checkout response for Payment type PayTM
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), SegmentationConstants.paymentTxnStatus[0],SegmentationConstants.orderStatus[0]);
        
        // Payload to push in queue for Payment type PayTM
        OrderCreateMessage OrderCreatePayloadOne = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[0], orderId,  restId,currentTime[0] );
        
        // Payload two to push in queue for Payment type PhonePe
        //yyyy-MM-dd HH:mm:ss
        
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, SegmentationHelper.getPastTime(1));
        
        return new Object[][]{{OrderCreatePayloadOne, OrderCreatePayloadTwo, userId, orderSecondId, allRestId, currentTime[1]}};
    }
    
    @DataProvider(name = "getUserRestaurant30daysOrderByUserIdDP")
    public Object[][] getUserRestaurant30daysOrderByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String[] allRestId = {restId};
         String[] dateValues= SegmentationHelper.getDatePlusDays( -35);
         
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, dateValues[0]);
        
        return new Object[][]{{ OrderCreatePayloadTwo, userId, orderSecondId, allRestId, dateValues[1]}};
    }
    
    @DataProvider(name = "getUserRestaurant60daysOrderByUserIdDP")
    public Object[][] getUserRestaurant60daysOrderByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String[] allRestId = {restId};
        String[] dateValues= SegmentationHelper.getDatePlusDays( -65);
        
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, dateValues[0]);
        
        return new Object[][]{{ OrderCreatePayloadTwo, userId, orderSecondId, allRestId, dateValues[1]}};
    }
    
    @DataProvider(name = "getUserRestaurant90daysOrderByUserIdDP")
    public Object[][] getUserRestaurant90daysOrderByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String[] allRestId = {restId};
        String[] dateValues= SegmentationHelper.getDatePlusDays( -93);
        
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, dateValues[0]);
        
        return new Object[][]{{ OrderCreatePayloadTwo, userId, orderSecondId, allRestId, dateValues[1]}};
    }
    
    @DataProvider(name = "getUserRestaurantLastOrderByUserIdAsZeroDP")
    public Object[][] getUserRestaurantLastOrderByUserIdAsZero() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        return new Object[][]{{ "0", restId}};
    }
    
    @DataProvider(name = "getUserRestaurantLastOrderByRestaurantIdAsZeroDP")
    public Object[][] getUserRestaurantLastOrderByRestaurantIdAsZero() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        return new Object[][]{{ userId, "0"}};
    }
    
    @DataProvider(name = "getUserRestaurantLastOrderByRestaurantIdAndUserIdBothAsZeroDP")
    public Object[][] getUserRestaurantLastOrderByRestaurantIdAndUserIdBothAsZero() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        return new Object[][]{{ "0", "0"}};
    }
    
    @DataProvider(name = "getNewUserRestaurantLastOrderByUserIdDP")
    public Object[][] getNewUserRestaurantLastOrderByUserId() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        
        return new Object[][]{{ userId, restId}};
    }
    
    @DataProvider(name = "getUserSegmentDP")
    public Object[][] getUserSegment() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
      
        
        return new Object[][]{{ userId}};
    }
    
    @DataProvider(name = "getUserIGCCSegmentMediumDP")
    public Object[][] getUserIGCCSegmentMedium() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
       UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[0],userId,SegmentationConstants.valueSegment[0],SegmentationConstants.igccSegment[2]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
//        SegmentationDbHelper.setUserSegmentDataInDB(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[0],userId,SegmentationConstants.valueSegment[0],SegmentationConstants.igccSegment[2]);
        
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserIGCCSegmentNADP")
    public Object[][] getUserIGCCSegmentNA() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
    
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[0],userId,SegmentationConstants.valueSegment[0],SegmentationConstants.igccSegment[3]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserIGCCSegmentNULLDP")
    public Object[][] getUserIGCCSegmentNULL() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[0],userId,SegmentationConstants.valueSegment[0],SegmentationConstants.igccSegment[4]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserIGCCSegmentAsEmptyStringDP")
    public Object[][] getUserIGCCSegmentAsEmptyString() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[0],userId,SegmentationConstants.valueSegment[0],"");
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserMarketingSegmentAsPremiumSegmentAsOneDP")
    public Object[][] getUserMarketingSegmentAsPremiumSegmentAsOne() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[2],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[0],SegmentationConstants.igccSegment[0]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    

    
    @DataProvider(name = "getUserMarketingSegmentAsBDP")
    public Object[][] getUserMarketingSegmentAsB() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[1],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserMarketingSegmentAsNULLDP")
    public Object[][] getUserMarketingSegmentAsNULL() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[4],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserMarketingSegmentAsEmptyStringDP")
    public Object[][] getUserMarketingSegmentAsEmptyString() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment("",SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[0], SegmentationConstants.igccSegment[4]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserValueSegmentAsMDP")
    public Object[][]     getUserValueSegmentAsMDP() throws IOException, ParseException {
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[2],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[2], SegmentationConstants.igccSegment[1]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
   
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserValueSegmentAsNADP")
    public Object[][]     getUserValueSegmentAsNA() throws IOException, ParseException {
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[2],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[3], SegmentationConstants.igccSegment[1]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);
        return new Object[][]{{userId}};
    }
    
    @DataProvider(name = "getUserValueSegmentAsNULLDP")
    public Object[][]     getUserValueSegmentAsNULL() throws IOException, ParseException {
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        UserSegment userSegment= new UserSegment(SegmentationConstants.marketingSegment[2],SegmentationConstants.premiumSegment[1],userId,SegmentationConstants.valueSegment[4], SegmentationConstants.igccSegment[1]);
        SegmentationDbHelper.setUserSegmentDataInDB(userSegment);

        return new Object[][]{{userId}};
    }
    @DataProvider(name = "getUserOrdersDP")
    public Object[][] getUserOrdersDP() throws IOException, ParseException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        String orderId = userId;
        String orderSecondId = Integer.toString(Utility.getRandom(1, 8999121));
        String restId = Integer.toString(Utility.getRandom(9000, 2200000));
        String[] allRestId = {restId};
        String currentTime =SegmentationHelper.getDate();
        
        //Mocking checkout response for Payment type PayTM
        segmentationHelper.stubCheckoutOrderResponse(userId, orderId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), SegmentationConstants.paymentTxnStatus[0],SegmentationConstants.orderStatus[0]);
        
        // Payload to push in queue for Payment type PayTM
        OrderCreateMessage OrderCreatePayloadOne = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[0], orderId,  restId,currentTime );

//        segmentationHelper.stubCheckoutOrder(userId, orderSecondId, restId, SegmentationConstants.cartType[3], SegmentationConstants.paymentMethod[0], SegmentationHelper.getDate(), "processing");
        
        // Payload two to push in queue for Payment type PhonePe
        //yyyy-MM-dd HH:mm:ss
        
        OrderCreateMessage OrderCreatePayloadTwo = orderCreateMessageBuilder.orderCreateMessageByPaymentMethodUserId(userId, SegmentationConstants.paymentMethod[8], orderSecondId,  restId, SegmentationHelper.getPastTime(1));
        
        return new Object[][]{{OrderCreatePayloadOne, OrderCreatePayloadTwo, userId, orderSecondId, allRestId}};
    }
    
    @DataProvider(name = "getUserOrdersCountWhenNoOrderPresentForUserDP")
    public Object[][] getUserOrdersCountWhenNoOrderPresentForUser() throws IOException {
        
        String userId = Integer.toString(Utility.getRandom(1, 99990000));
        
        return new Object[][]{{ userId}};
    }
    
    @DataProvider(name = "getUserDeviceIdDP")
    public Object[][] getUserDeviceId() throws IOException, ParseException {
        
        String deviceId = Integer.toString(Utility.getRandom(1, 99990000));
        String userId = deviceId;
        // Insert data in to DB for device
        SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        return new Object[][]{{ deviceId}};
    }
    
    @DataProvider(name = "getNewUserDeviceIdDP")
    public Object[][] getNewUserDeviceId() throws IOException, ParseException {
        
        String deviceId = Integer.toString(Utility.getRandom(1, 99990000));
        String userId = deviceId;
        // Insert data in to DB for device
       // SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        return new Object[][]{{ deviceId}};
    }
    
    @DataProvider(name = "getNewUserDeviceIdForAUserIdWithMultipleDeviceIdDP")
    public Object[][] getNewUserDeviceIdForAUserIdWithMultipleDeviceId() throws IOException, ParseException {
        
        String deviceId = Integer.toString(Utility.getRandom(1, 99990000));
        String deviceIdTwo = Integer.toString(Utility.getRandom(1, 10000000));
        String userId = deviceId;
        // Insert data in to DB for device
         SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        SegmentationDbHelper.setDeviceId(deviceIdTwo, userId, SegmentationHelper.getDate());
        return new Object[][]{{ deviceId, deviceIdTwo}};
    }
    
    @DataProvider(name = "getNewUserDeviceIdForADeviceIdWithMultipleUserIdDP")
    public Object[][] getNewUserDeviceIdForADeviceIdWithMultipleUserId() throws IOException, ParseException {
        
        String deviceId = Integer.toString(Utility.getRandom(1, 99990000));
        String userIdTwo = Integer.toString(Utility.getRandom(1, 10000000));
        String userIdThree = Integer.toString(Utility.getRandom(1, 18888888));
        String userId = deviceId;
        // Insert data in to DB for device
        SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        SegmentationDbHelper.setDeviceId(deviceId, userIdTwo, SegmentationHelper.getDate());
        
        SegmentationDbHelper.setDeviceId(deviceId, userIdThree, SegmentationHelper.getDate());
        return new Object[][]{{ deviceId}};
    }
    
    @DataProvider(name = "testForDuplicateDeviceDetailEntryInDBDP")
    public Object[][] testForDuplicateDeviceDetailEntryInDB() throws IOException, ParseException {
        
        String deviceId = Integer.toString(Utility.getRandom(1, 99990000));
        String userId = deviceId;
        // Insert data in to DB for device
        SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        SegmentationDbHelper.setDeviceId(deviceId, userId, SegmentationHelper.getDate());
        return new Object[][]{{ deviceId}};
    }
}
