package com.swiggy.api.sf.rng.pojo.SwiggySuper;

public class PatchIncentive {
	
	
	private String tenure="0";
	private String priority="10";
	private boolean enabled=true;
	private String updated_by="By-Super-Atomation";
	private String id;
	public PatchIncentive() {
		
	}
	
	public PatchIncentive(String incentive_id,String tenure, boolean  enabled) {
		this.id= incentive_id;
		this.tenure= tenure;
		this.priority= priority;
		this.enabled= enabled;
		
	}
	public PatchIncentive(String incentive_id, boolean enabled) {
		this.id= incentive_id;
		this.enabled= enabled;
		
	}
	public PatchIncentive(String incentive_id, String tenure) {
		this.id= incentive_id;
		this.tenure= tenure;
		
	}


	public String getTenure() {
		return this.tenure;
	}
	public void setTenure(String tenure) {
		this.tenure= tenure;
	}
	public String getPriority() {
		return this.priority;
	}
	public void setPriority(String priority) {
		this.priority= priority;
	}
	public boolean getEnabled() {
		return this.enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled= enabled;
	}
	public String getUpdated_by() {
		return this.updated_by;
	}
	public void setUpdated_by(String updated_by) {
		this.updated_by= updated_by;
	}
	
	@Override
	public String toString() {
		
		return "{" 
		 
                 + "\"id\"" + ":" + id + ","
                 + "\"tenure\"" + ":" + tenure + ","
                 + "\"enabled\"" + ":" + enabled + ","
                  + "\"updated_by\"" + ":" + "\"" + updated_by + "\"" 
                  
                  + "}"			;
	}
	
public String disableEnableIncentive() {
		
		return "{" 
		 
                 + "\"id\"" + ":" + id + ","
                 + "\"enabled\"" + ":" + enabled + ","
                  + "\"updated_by\"" + ":" + "\"" + updated_by + "\"" 
                  
                  + "}"			;
	}
	
public String updateIncentiveTenure() {
	
	return "{" 
	 
             + "\"id\"" + ":" + id + ","
             + "\"tenure\"" + ":" + tenure + ","
              + "\"updated_by\"" + ":" + "\"" + updated_by + "\"" 
              
              + "}"			;
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	