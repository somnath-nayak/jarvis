package com.swiggy.api.sf.checkout.dp;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.superConstant;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.snd.helper.SANDHelper;

public class SuperDP {
    
	EDVOCartHelper eDVOCartHelper=new EDVOCartHelper();
	SANDHelper sANDHelper=new SANDHelper();
	static String restId;
	static String menuItemId;
	static String quantity=superConstant.SUB_QUANTITY;
	
	
	@BeforeClass
    public void setCartDetails() {
		restId=sANDHelper.Aggregator(new String[]{CheckoutConstants.lat,CheckoutConstants.lng});
		Cart processor=eDVOCartHelper.getCartPayload(new String[0], restId, false, false, true);
		menuItemId=processor.getCartItems().get(0).getMenuItemId();
    }
	
	@DataProvider(name = "super") 
	 public static Object[][] validateSuperCase() { 
		
		 return new Object[][] {{menuItemId,quantity,restId}};
		 }
}