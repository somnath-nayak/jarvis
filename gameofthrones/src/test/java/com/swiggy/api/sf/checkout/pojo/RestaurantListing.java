package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RestaurantListing {

    private String latlng;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestaurantListing() {
    }

    /**
     *
     * @param latlng
     */
    public RestaurantListing(String latlng) {
        super();
        this.latlng = latlng;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setLatlng(String latlng) {
        this.latlng = latlng;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("latlng", latlng).toString();
    }

}

