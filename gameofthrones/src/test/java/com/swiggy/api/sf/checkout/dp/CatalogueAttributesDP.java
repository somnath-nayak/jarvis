package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.CatalogueAttributesConstants;
import org.testng.annotations.DataProvider;

public class CatalogueAttributesDP {

    @DataProvider(name = "cartPayload")
    public static Object[][] getCartPayload() {
        return new Object[][]{
                //Restaurant Id, Items Array, Is Addon Required, Is Item Level Check Required
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR},
//                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR}
        };
    }

    @DataProvider(name = "cartPayloadForOrder")
    public static Object[][] getCartPayloadgetCartPayload() {
        return new Object[][]{
                //Restaurant Id, Items Array, Is Addon Required, Is Item Level Check Required
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_V_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.NO_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FREE_DELIVERY, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FREEBIE, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.FLAT_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT},
                {CatalogueAttributesConstants.REST_ID_01, new String[]{CatalogueAttributesConstants.ITEM_ID_I_V_01}, false, true, CatalogueAttributesConstants.PERCENTAGE_TD, CatalogueAttributesConstants.CART_TYPE_REGULAR, CatalogueAttributesConstants.PAYMENT_METHOD, CatalogueAttributesConstants.ORDER_COMMENT}
        };
    }
}
