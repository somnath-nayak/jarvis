package com.swiggy.api.sf.rng.pojo.pitara;


import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "count",
        "modes"
})
public class RAIN_NUDGE_CARD {

    @JsonProperty("type")
    private String type;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("modes")
    private List<Mode> modes = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("modes")
    public List<Mode> getModes() {
        return modes;
    }

    @JsonProperty("modes")
    public void setModes(List<Mode> modes) {
        this.modes = modes;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
