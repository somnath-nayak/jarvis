/**
 * @author manu.chadha
 */

package com.swiggy.api.sf.rng.pojo.freedelivery;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class CreateFreeDeliveryEntry {

    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("restaurant_hit")
    private String restaurantHit;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("restaurantList")
    private List<FreeDeliveryRestaurantList> restaurantList = null;
    @JsonProperty("ruleDiscount")
    private FreeDeliveryRuleDiscount ruleDiscount;
    @JsonProperty("slots")
    private List<FreeDeliverySlot> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;
    @JsonProperty("dormant_user_type")
    private String dormantUserType;
    @JsonProperty("surgeApplicable")
    private Boolean surgeApplicable;
    @JsonProperty("shortDescription")
    private String shortDescription = "Default";
    @JsonProperty("copyOverridden")
    private Boolean copyOverridden = false;

    @JsonProperty("isSuper")
    private Boolean isSuper = false;

    /**
     * No args constructor for use in serialization
     *
     */
    public CreateFreeDeliveryEntry() {
    }

    /**
     *
     * @param ruleDiscount
     * @param timeSlotRestriction
     * @param enabled
     * @param dormantUserType
     * @param campaignType
     * @param restaurantList
     * @param commissionOnFullBill
     * @param userRestriction
     * @param swiggyHit
     * @param restaurantHit
     * @param validTill
     * @param header
     * @param namespace
     * @param firstOrderRestriction
     * @param createdBy
     * @param slots
     * @param description
     * @param validFrom
     * @param discountLevel
     * @param taxesOnDiscountedBill
     */
    public CreateFreeDeliveryEntry(String namespace, String header, String description, String validFrom, String validTill, String campaignType, String restaurantHit, Boolean enabled, String swiggyHit, String discountLevel, String createdBy, List<FreeDeliveryRestaurantList> restaurantList, FreeDeliveryRuleDiscount ruleDiscount, List<FreeDeliverySlot> slots, Boolean commissionOnFullBill, Boolean taxesOnDiscountedBill, Boolean firstOrderRestriction, Boolean timeSlotRestriction, Boolean userRestriction, String dormantUserType,Boolean is_surge,Boolean isSuper) {
        super();
        this.namespace = namespace;
        this.header = header;
        this.description = description;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.campaignType = campaignType;
        this.restaurantHit = restaurantHit;
        this.enabled = enabled;
        this.swiggyHit = swiggyHit;
        this.discountLevel = discountLevel;
        this.createdBy = createdBy;
        this.restaurantList = restaurantList;
        this.ruleDiscount = ruleDiscount;
        this.slots = slots;
        this.commissionOnFullBill = commissionOnFullBill;
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        this.firstOrderRestriction = firstOrderRestriction;
        this.timeSlotRestriction = timeSlotRestriction;
        this.userRestriction = userRestriction;
        this.dormantUserType = dormantUserType;
        this.surgeApplicable = is_surge;
        this.isSuper=isSuper;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTill() {
        return validTill;
    }

    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public String getCampaignType() {
        return campaignType;
    }

    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public String getRestaurantHit() {
        return restaurantHit;
    }

    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("swiggy_hit")
    public String getSwiggyHit() {
        return swiggyHit;
    }

    @JsonProperty("swiggy_hit")
    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @JsonProperty("restaurantList")
    public List<FreeDeliveryRestaurantList> getRestaurantList() {
        return restaurantList;
    }

    @JsonProperty("restaurantList")
    public void setRestaurantList(List<FreeDeliveryRestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
    }

    @JsonProperty("ruleDiscount")
    public FreeDeliveryRuleDiscount getRuleDiscount() {
        return ruleDiscount;
    }

    @JsonProperty("ruleDiscount")
    public void setRuleDiscount(FreeDeliveryRuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
    }

    @JsonProperty("slots")
    public List<FreeDeliverySlot> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public void setSlots(List<FreeDeliverySlot> slots) {
        this.slots = slots;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    @JsonProperty("restaurantFirstOrder")
    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }
    @JsonProperty("is_surge")
    public void setIs_surge(Boolean is_surge) {
        this.surgeApplicable = is_surge;
    }
    @JsonProperty("is_surge")
    public Boolean getis_surge() {
        return surgeApplicable;
    }

    public String getDormantUserType() {
        return dormantUserType;
    }

    public void setDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
    }
    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Boolean getCopyOverridden() {
        return copyOverridden;
    }

    public void setCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
    }


    public Boolean getIsSuper() {
        return isSuper;
    }

    public void setIsSuper(Boolean isSuper) {
        this.isSuper = isSuper;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this).append("namespace", namespace).append("header", header).append("description", description).append("validFrom", validFrom).append("validTill", validTill).append("campaignType", campaignType).append("restaurantHit", restaurantHit).append("enabled", enabled).append("swiggyHit", swiggyHit).append("discountLevel", discountLevel).append("createdBy", createdBy).append("restaurantList", restaurantList).append("ruleDiscount", ruleDiscount).append("slots", slots).append("commissionOnFullBill", commissionOnFullBill).append("taxesOnDiscountedBill", taxesOnDiscountedBill).append("firstOrderRestriction", firstOrderRestriction).append("timeSlotRestriction", timeSlotRestriction).append("userRestriction", userRestriction).append("dormantUserType", dormantUserType).append("is_surge",surgeApplicable).append("isSuper",isSuper).toString();
    }


}