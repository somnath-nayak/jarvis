package com.swiggy.api.sf.rng.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.Utility.GenerateRandomUtils;
import com.swiggy.api.sf.rng.constants.RngConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.annotations.Test;

import java.math.BigInteger;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.HashMap;
import java.util.List;

public class PitaraHelper {

    static Initialize gameofthrones = new Initialize();
    RngHelper rngHelper= new RngHelper();
    static HashMap<String, String> requestHeaders = new HashMap<String, String>();
    static GameOfThronesService service;
    static Processor processor;
    RedisHelper rh = new RedisHelper();

    public Processor getNuxCollectionCard(String body,String tid,String token)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token", token);
        String[] param = {"12.9279232","77.62710779999998","1"};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardonlisting", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }

    public Processor getNuxCollectionCardWithOrderId(String body,String tid,String token, String orderId)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token", token);
        String[] param = {"12.9279232","77.62710779999998","1",orderId};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardonlistingwithorderid", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }

    public Processor getSuperCard(String body,String userId)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] param = {"12.9279232","77.62710779999998","1",userId};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardonlistingsuper", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }

    public Processor getTrackSuperCard(String body,String userId)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] param = {"12.9279232","77.62710779999998","1","0","1","12486049",userId};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardontracksuper", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }
    public Processor getTrackScreenCardProcessor(String body,String userId,String orderId,String tid,String token,String isPreOrder )
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token",token);
        String[] param = {"12.9279232","77.62710779999998","1","0",isPreOrder,orderId,userId};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardontracksuper", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }
    public Processor getTrackScreenCardProcessor(String body,String userId,String orderId,String tid,String token )
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token",token);
        String[] param = {"12.9279232","77.62710779999998","1","0","1",orderId,userId};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardontracksuper", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }
    public Processor getTrackScreenCardProcessor(String body,String showRateApp)
    {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        String[] param = {"12.9279232","77.62710779999998","1",showRateApp,"1","12486049"};
        GameOfThronesService service = new GameOfThronesService("pitara", "fetchcardontrack", gameofthrones);
        return processor = new Processor(service, requestHeaders, new String[]{body},param);
    }



    public void disableAllContext() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(RngConstants.disableContext);
    }

    public void deleteCards(String id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String query1 = String.format(RngConstants.deleteContext,id);
        sqlTemplate.update(query1);
        query1 = String.format(RngConstants.deleteValidation,id);
        sqlTemplate.update(query1);
        query1 = String.format(RngConstants.deleteCard,id);
        sqlTemplate.update(query1);
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
    @Test()
    public void test(){
        deleteCards("999298");
        cacheEvict();
        deleteCards("999229");
        cacheEvict();
    }

    public void cacheEvict() {
        GameOfThronesService service = new GameOfThronesService("pitara", "cacheEvict", gameofthrones);
         processor = new Processor(service, requestHeaders,null);
         processor.ResponseValidator.GetBodyAsText();
    }


    public void disableAllCards() {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(RngConstants.disableCards);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void disableCard(String id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String query = String.format(RngConstants.disableCard,id);
        sqlTemplate.update(query);
    }

    public String createContext() {

        String tableName = getTableName(RngConstants.context);
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query = String.format(RngConstants.context,id);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
        return id;
    }
    public String createTrackContext() {

        String tableName = getTableName(RngConstants.tackcontext);
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query = String.format(RngConstants.tackcontext,id);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
        return id;
    }



    public enum CardType {
        nuxcard(RngConstants.nuxCard),
        nuxcard2(RngConstants.nuxCard2),
        lcard(RngConstants.lCard),
        scard(RngConstants.sCard),
        xlcard(RngConstants.xlCard),
        scardv2(RngConstants.sCardV2),
        rain(RngConstants.rainCard),
        launchcard(RngConstants.launchCard),
        smallnudgecard(RngConstants.smallNudgeCard),
        bannercard(RngConstants.bannerCard),
        superCard(RngConstants.superCard),
        popCard(RngConstants.popCard);
        public final String name;

        CardType(String card) {
            this.name = card;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }


    public String createCard(CardType type) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String tableName = getTableName(type.toString());
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query = String.format(type.toString(),id);
        System.out.println(query);
        int i = sqlTemplate.update(query);
        System.out.println(i);
        return id;
    }

    public HashMap<String, String> createAllCards() {
        HashMap<String, String> allCards = new HashMap<>();
        for (CardType status : CardType.values()) {
            allCards.put(status.name(), createCard(status));
        }
        return allCards;
    }

    public enum ValidationType {
        collection(RngConstants.collectionCreation),
        coupon(RngConstants.couponCreation),
        showrate(RngConstants.showRateValidation),
        rain(RngConstants.rainCardValidation),
        lcard_isPreOrderEnabled(RngConstants.lcardValiadation_isPreOrderEnabled),
        lcard_hasPreOrder(RngConstants.lcardValiadation_hasPreOrder),
        lcard_isPreOrder(RngConstants.lcardValiadation_isPreOrder),
        xlcard_isLongDistanceOrder(RngConstants.xlcardValiadation_IS_LONG_DISTANCE_ORDER),
        xlcard_hasLongDistanceOrder(RngConstants.xlcardValiadation_HAS_PLACED_LONG_DISTANCE_ORDER),
        launchCard(RngConstants.launchCardValiadation),
        superCard(RngConstants.superCardValidation),
        superCardMin(RngConstants.superCardValidationWithMinAsTen),
        superCardMax(RngConstants.superCardValidationWithMaxAsTen),
        orderCount(RngConstants.orderCountValidation),
        orderCountMin(RngConstants.orderCountValidationWithMinAsTen),
        clickCount(RngConstants.clickCountValidation),
        seenCount(RngConstants.seenCountValidation),
        seenCountMinMax(RngConstants.seenCountValidationWithMinMax),
        isSuperOrder(RngConstants.isSuperOrderCardValidation),
        launchCorporateCafeCard(RngConstants.launchCorporateCafeCardValiadation),
        isSwiggyAssured(RngConstants.isSwiggyAssuredValidation),
        hasSwiggyAssured(RngConstants.hasSwiggyAssuredValidation);
        
        private final String name;

        ValidationType(String card) {
            this.name = card;
        }

        public boolean equalsName(String otherName) {
            // (otherName == null) check is not needed because name.equals(null) returns false
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }

    public String createValidation(ValidationType type) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String tableName = getTableName(type.toString());
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String str = String.format(type.toString(),id);
        System.out.println(str);
        sqlTemplate.update(str);
        return id;
    }

    public String createValidation(ValidationType type,String enable) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String tableName = getTableName(type.toString());
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String str = String.format(type.toString(),id,enable);
        System.out.println(str);
        sqlTemplate.update(str);
        return id;
    }

//    public HashMap<String, String> createAllValidation() {
//        HashMap<String, String> allCards = new HashMap<>();
//        for (ValidationType status : ValidationType.values()) {
//            allCards.put(status.name(), createValidation(status));
//        }
//        return allCards;
//    }

    public String createCardContextMap(String contextid ,String cardid ) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String tableName = getTableName(RngConstants.cardContextMap);
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query = String.format(RngConstants.cardContextMap,id,contextid,cardid);
        System.out.println(query);
       // String query = String.format(RngConstants.cardContextMap,id,contextid,cardid);
        sqlTemplate.update(query);
//        System.out.println(id);
//        try
//        {
//            Thread.sleep(5000);
//        }catch(Exception e)
//        {
//            e.printStackTrace();
//        }
        return id;
    }
    public String createCardValidationMap(String cardid,String validationId) {
        String tableName = getTableName(RngConstants.cardValidationMap);
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query= String.format(RngConstants.cardValidationMap,id,cardid,validationId);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
        System.out.println(id);
//        try
//        {
//            Thread.sleep(5000);
//        }catch(Exception e)
//        {
//            e.printStackTrace();
//        }
        return id;
    }
    public String createContextKeynMap(String contextId) {
        String tableName = getTableName(RngConstants.contextKeyMap);
        String id = getAutoIncrementId(RngConstants.pitaraDBName,tableName);
        String query= String.format(RngConstants.contextKeyMap,id,contextId);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
        return id;
    }
    public void createKeyValue(String contextId) {
        String query= String.format(RngConstants.contextValue,contextId);
        System.out.println(query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
    }
    public void trackCreateKeyValue(String contextId) {
        String query= String.format(RngConstants.trackContextValue,contextId);
        System.out.println(query);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        sqlTemplate.update(query);
    }


    public void couponUsermap(String couponId,String userId,String CouponCode)
    {


        String query= String.format(RngConstants.couponUsermap,CouponCode,couponId,userId);
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rng_couponDB);
        sqlTemplate.update(query);

    }
    //    public long generateRandom()
//    {
//        Random rand = new Random();
//        int value = rand.nextInt(9999);
//        return value;
//    }
    public String createCopuonForPitara(String isprivate)
    {

        Processor processor= RngHelper.createCoupon2("\"Discount\"", "TestAutomation", "AUTOMAT123", "Test Automation coupon Gaurav", isprivate,
                "3", "3", "10", "1", "0", "0", "0",
                "0", "0","0","0", "10", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL", "Internal", "1",
                "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
                "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa");

        String response = processor.ResponseValidator.GetBodyAsText();
        if(isprivate.equalsIgnoreCase("1"))
        {
            List couponIds = JsonPath.read(response,"$.data..id");
            String couponId = couponIds.get(0).toString();
            String couponCode = JsonPath.read(response,"$.data.code");


            couponUsermap(couponId,RngConstants.userId,couponCode);
        }
        return processor.ResponseValidator.GetNodeValue("$.data.code");
    }

    public String createCopuonForPitara(String isprivate, String userID)
    {

        Processor processor= RngHelper.createCoupon2("\"Discount\"", "TestAutomation", "AUTOMAT123", "Test Automation coupon Gaurav", isprivate,
                "3", "3", "10", "1", "0", "1", "0",
                "0", "0","0","0", "10", "50", "0", "0", "0", "\"Juspay-NB\"", "0", "0", "", "NULL", "Internal", "1",
                "0", "-1", "0", "1", "true", "0", "", "", "1", "user@swiggy.in", "testing",
                "\"testTNC\",\"test\"", "lbx02iaashsnhea5fpwa");

        String response = processor.ResponseValidator.GetBodyAsText();
        if(isprivate.equalsIgnoreCase("1"))
        {
            List couponIds = JsonPath.read(response,"$.data..id");
            String couponId = couponIds.get(0).toString();
            String couponCode = JsonPath.read(response,"$.data.code");


            couponUsermap(couponId,userID,couponCode);
        }
        return processor.ResponseValidator.GetNodeValue("$.data.code");
    }

//    public String getRestauranListing()
//    {
//
//        Processor processor = rngHelper.getListing(RngConstants.iBCLat,RngConstants.iBCLng,"0","","");
//
//        String response = processor.ResponseValidator.GetBodyAsText();
//
//
//
//    }


    public void preOrderEnableHelper(String restId,String areaId)
    {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.cmsDB);
        String query1 = String.format(RngConstants.areaSchedule,areaId);
        String query2 = String.format(RngConstants.restauratSchedule,restId,restId,restId,restId,restId,restId,restId);
        sqlTemplate.update(query1); sqlTemplate.update(query2);

        rh.setValueJson("sandredisstage",2,"METADATA_RESTAURANT_"+restId,"{\\\"delivery\\\":false ,\\\"is_buffet\\\":false,\\\"self_pick_up\\\":false,\\\"preorder_enabled\\\":true}");

    }

    public void setCardMetaDataSCardV2(String cardID){

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String query = String.format(RngConstants.cardMetaData,cardID);
        sqlTemplate.update(query);

    }

    public void deleteCardMetaData(String cardID){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.pitaraDBName);
        String query = String.format(RngConstants.deleteCardMetaData,cardID);
        sqlTemplate.update(query);
    }

    public String getTableName(String query){
        String pattern ="insert into\\s*`*(.*?)`*\\s*\\(";
        Pattern p = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(query);
        if(m.find()){
            if(m.group(1).contains("."))
                return m.group(1).trim().split(".")[1];
            else
                return m.group(1);
        }
        return null;

    }

    public String getAutoIncrementId(String schema, String table){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(schema);
        String query = String.format(RngConstants.getAutoIncrementId,schema,table);
        Map<String, Object> map = sqlTemplate.queryForMap(query);
        return String.valueOf(map.get("AUTO_INCREMENT"));
    }

}
