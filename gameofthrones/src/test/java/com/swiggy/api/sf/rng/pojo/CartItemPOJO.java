package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "itemKey",
        "menu_item_id",
        "quantity"
})
public class CartItemPOJO {

    @JsonProperty("itemKey")
    private String itemKey;
    @JsonProperty("menu_item_id")
    private Integer menuItemId;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartItemPOJO() {
    }

    /**
     *
     * @param menuItemId
     * @param itemKey
     * @param quantity
     */
    public CartItemPOJO(String itemKey, Integer menuItemId, Integer quantity) {
        super();
        this.itemKey = itemKey;
        this.menuItemId = menuItemId;
        this.quantity = quantity;
    }

    @JsonProperty("itemKey")
    public String getItemKey() {
        return itemKey;
    }

    @JsonProperty("itemKey")
    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    public CartItemPOJO withItemKey(String itemKey) {
        this.itemKey = itemKey;
        return this;
    }

    @JsonProperty("menu_item_id")
    public Integer getMenuItemId() {
        return menuItemId;
    }

    @JsonProperty("menu_item_id")
    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    public CartItemPOJO withMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CartItemPOJO withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartItemPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CartItemPOJO setDefault() {
        return this.withItemKey("null")
                .withMenuItemId(11122233)
                .withQuantity(1);
    }
    
}
