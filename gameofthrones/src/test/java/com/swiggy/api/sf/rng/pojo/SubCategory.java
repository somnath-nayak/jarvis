package com.swiggy.api.sf.rng.pojo;


import com.swiggy.api.sf.rng.helper.Utility;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class SubCategory {

    private String id;
    private String name;
    private List<Menu> menu = null;
    /**
     * No args constructor for use in serialization
     *
     */
    public SubCategory() {
    }

    /**
     *
     * @param id
     * @param menu
     * @param name
     */
    public SubCategory(String id, String name, List<Menu> menu) {
        super();
        this.id = id;
        this.name = name;
        this.menu = menu;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Menu> getMenu() {
        return menu;
    }

    public void setMenu(List<Menu> menu) {
        this.menu = menu;
    }

    public SubCategory withItem(String subCategoryId,List<Menu> menuList){
        Menu menu = new Menu();
        this.id = subCategoryId;
        this.name = "TestSubCategory";
        this.menu = menuList;
        return this;
    }



//    public Category withSubCategory( String subCategoryId){
//        SubCategory subcategory = new SubCategory();
//        this.id = subCategoryId;
//        this.name = "testSubCategory";
//        return null;
//    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("menu", menu).toString();
    }


}
