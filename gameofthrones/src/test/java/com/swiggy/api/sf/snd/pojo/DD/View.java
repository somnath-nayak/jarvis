package com.swiggy.api.sf.snd.pojo.DD;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class View {

    private String page;
    @JsonProperty("view_type")
    private String viewType;
    private Integer id;
    @JsonProperty("view_data")
    private ViewData viewData;
    private List<String> viewVariables = null;

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ViewData getViewData() {
        return viewData;
    }

    public void setViewData(ViewData viewData) {
        this.viewData = viewData;
    }

    public List<String> getViewVariables() {
        return viewVariables;
    }

    public void setViewVariables(List<String> viewVariables) {
        this.viewVariables = viewVariables;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("page", page).append("viewType", viewType).append("id", id).append("viewData", viewData).append("viewVariables", viewVariables).toString();
    }

}