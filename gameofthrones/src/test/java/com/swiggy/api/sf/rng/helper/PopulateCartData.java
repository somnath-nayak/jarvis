package com.swiggy.api.sf.rng.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PopulateCartData {

	private static int payLoadLimit = 100000;
	private static final String filePath ="D:\\perf\\cart_perf.csv";

	private static final int itemPayLoadMinValue=3;
	private static final int itemPayLoadMaxValue=20;
	
	private static final int itemIdMinValue=2345543;
	private static final int itemIdMaxValue=99999999;
	
	private String menuPayload = "{\"minCartAmount\": 100,\"userId\": 5769085, \"isFirstOrder\": false, \"itemRequests\": [<itemPayload>]}";
	private String itemPayload = "{ \"subCategoryId\": 1,  \"itemId\": <itemId>, \"restaurantId\":<restId>,\"price\": 5000,\"count\": 1, \"categoryId\": 1}";
	
	public void writeDataToFile(String filePath, Map<Integer, String> map) {

		File listingFile = new File(filePath);
		FileWriter fw;
		try {
			fw = new FileWriter(listingFile);

			System.out.println("size " + map.size());
			for (int i = 0; i < map.size(); i++) {

				String payload = map.get(i);
				System.out.println("payload " + payload);
				fw.write(payload);
				fw.write("\n");

			}
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		}

	
	public void createCartPayload() {
		TDPerfTest tdPerfTest = new TDPerfTest();
		
		List restIds = tdPerfTest.getTdEnableRestIds();
		Map<String, List<String>> restIdToItemMap = TDPerfTest.getTDEnabledItemIds(restIds);
		
		Map<Integer, String> restIdToCartMap = new HashMap();
		List validRestIds = new ArrayList<>(restIdToItemMap.keySet());
		
		for(int i = 0; i < payLoadLimit; i++) {
			String restId1 = Utility.getRandomFromList(restIds);	
			String cartPayLoad = createCartPayLoad(restId1, restIdToItemMap);
			restIdToCartMap.put(i, cartPayLoad);
		}
		writeDataToFile(filePath,restIdToCartMap);
	}
	
	
	private String createCartPayLoad(String restId, Map<String, List<String>> restIdToItemMap) {
		String itemRequest = "";
		if(restIdToItemMap.get(restId) != null) {
			itemRequest = createItemRequest(restId, restIdToItemMap);
		} else {
			itemRequest = createItemRequestForRandomRest(restId);
		}
		return menuPayload.replace("<itemPayload>", itemRequest);

	}

	private String createItemRequestForRandomRest(String restId) {
		
		int itemLimit = Utility.getRandom(itemPayLoadMinValue, itemPayLoadMaxValue);
		List<String> itemList = new ArrayList<String>();
		
		for(int i = 0; i < itemLimit; i++) {
			String itemId = String.valueOf(Utility.getRandom(itemIdMinValue, itemIdMaxValue));
			itemList.add(itemPayload.replace("<restId>", restId).replace("<itemId>", itemId));
		}
		return String.join(",", itemList);
	}


	private String createItemRequest(String restId, Map<String, List<String>> restIdToItemMap) {

		int itemLimit = Utility.getRandom(itemPayLoadMinValue, itemPayLoadMaxValue);
		List<String> itemList = new ArrayList<String>();
		
		for(int i = 0; i < itemLimit; i++) {
			String itemId = Utility.getRandomFromList(restIdToItemMap.get(restId));
			itemList.add(itemPayload.replace("<restId>", restId).replace("<itemId>", itemId));
		}
		return String.join(",", itemList);
	}

}
