package com.swiggy.api.sf.snd.tests;

import org.testng.annotations.Test;

import com.swiggy.api.sf.snd.helper.POPHelper;

import framework.gameofthrones.JonSnow.Processor;


public class POPSmokeTest {
	
	@Test (priority = 1)
	public void popAggregator(){
		String[] latlng = {"12.934696", "77.616530"};
		int statusCode = 0;
		String statusMessage = "done successfully";
		POPHelper ph = new POPHelper();
		Processor processor = ph.popAggregator(latlng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 2)
	public void popItemMenu(){
		String itemID = "1765567";
		String restID = "29627";
		int statusCode = 0;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popItemMenu(itemID,restID);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 3)
	public void popListing(){
		String lat = "12.934696";
		String lng = "77.616530";
		int statusCode = 1;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popListing(lat, lng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 4)
	public void popForDelivery(){
		
		String[] values = {"\"223\"","\"1200\"","\"9990\""};
		String delimitor=",";
		String finalValue=String.join(delimitor, values);
		System.out.println(finalValue);
		String[] payload = {finalValue};
		int statusCode = 0;
		String statusMessage = "done successfully";
		System.out.println(payload);		
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popForDelivery(payload);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 5)
	public void popAggregatorHyderabad(){
		String[] latlng = {"17.436472", "78.405139"};
		int statusCode = 0;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popAggregator(latlng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 6)
	public void popAggregatorDelhi(){
		String[] latlng = {"28.533850", "77.207671"};
		int statusCode = 0;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popAggregator(latlng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 7)
	public void popListingKoramangala(){
		String lat = "12.935248";
		String lng = "77.615884";
		int statusCode = 1;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popListing(lat, lng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

	@Test (priority = 8)
	public void popListingHyderabad(){
		String lat = "17.436472";
		String lng = "78.405139";
		int statusCode = 1;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popListing(lat, lng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}
	
	@Test (priority = 9)
	public void popListingDelhi(){
		String lat = "28.533850";
		String lng = "77.207671";
		int statusCode = 1;
		String statusMessage = "done successfully";
		
		POPHelper ph = new POPHelper();
		Processor processor = ph.popListing(lat, lng);
		ph.smokeCheck(statusCode, statusMessage, processor);
		System.out.println("+++++++++++++++++++#############" + processor.ResponseValidator.GetBodyAsText());
	}

}
