package com.swiggy.api.sf.snd.pojo.DDAggregator;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ViewTypeRequest {

    private String viewType;
    private Integer maxCount;

    public String getViewType() {
        return viewType;
    }

    public void setViewType(String viewType) {
        this.viewType = viewType;
    }

    public Integer getMaxCount() {
        return maxCount;
    }

    public void setMaxCount(Integer maxCount) {
        this.maxCount = maxCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("viewType", viewType).append("maxCount", maxCount).toString();
    }

}