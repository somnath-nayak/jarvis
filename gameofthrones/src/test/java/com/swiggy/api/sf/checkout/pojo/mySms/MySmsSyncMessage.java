package com.swiggy.api.sf.checkout.pojo.mySms;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonPropertyOrder({
        "apiKey",
        "authToken",
        "deviceId",
        "syncLimit"
})
public class MySmsSyncMessage {

    @JsonProperty("apiKey")
    private String apiKey;
    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("deviceId")
    private Integer deviceId;
    @JsonProperty("syncLimit")
    private Integer syncLimit;

    /**
     * No args constructor for use in serialization
     *
     */
    public MySmsSyncMessage() {
    }

    /**
     *
     * @param authToken
     * @param syncLimit
     * @param deviceId
     * @param apiKey
     */
    public MySmsSyncMessage(String apiKey, String authToken, Integer deviceId, Integer syncLimit) {
        super();
        this.apiKey = apiKey;
        this.authToken = authToken;
        this.deviceId = deviceId;
        this.syncLimit = syncLimit;
    }

    @JsonProperty("apiKey")
    public String getApiKey() {
        return apiKey;
    }

    @JsonProperty("apiKey")
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("deviceId")
    public Integer getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("syncLimit")
    public Integer getSyncLimit() {
        return syncLimit;
    }

    @JsonProperty("syncLimit")
    public void setSyncLimit(Integer syncLimit) {
        this.syncLimit = syncLimit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("apiKey", apiKey).append("authToken", authToken).append("deviceId", deviceId).append("syncLimit", syncLimit).toString();
    }

}