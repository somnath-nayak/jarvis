package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class FilterTags {

    private String type;
    private List<DataTag> data = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public FilterTags() {
    }

    /**
     *
     * @param data
     * @param type
     */
    public FilterTags(String type, List<DataTag> data) {
        super();
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DataTag> getData() {
        return data;
    }

    public void setData(List<DataTag> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("data", data).toString();
    }

}