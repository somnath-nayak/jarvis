package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.redis.S;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;

import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.*;


public class menuV4 extends SnDDp {
        SANDHelper helper = new SANDHelper();
        CMSHelper cmsHelper = new CMSHelper();
        OrderPlace orderPlace= new OrderPlace();
        RngHelper rngHelper = new RngHelper();


        @Test(dataProvider = "menuv4", description = "Verify menu V4 smoke")
        public void menuV4smoke( String lat, String lng) {

            /*
             * Smoke Test
             */
            String restId = helper.Aggregator(new String[]{lat, lng});
            Processor processor = helper.menuV4RestId(restId, lat, lng);
            String resp = processor.ResponseValidator.GetBodyAsText();
            int statusCode = JsonPath.read(resp, "$.statusCode");
            String statusMessage = JsonPath.read(resp, "$.statusMessage");
            Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
            Assert.assertEquals(statusCode, 0);
            Assert.assertEquals(statusMessage, "done successfully");
        }

        @Test(dataProvider = "menuv4", description = "Verify by adding new items")
        public void menuV4AddItems( String lat, String lng) {

            String restId = helper.Aggregator(new String[]{lat, lng});
            Processor processor = helper.menuV4RestId(restId, lat, lng);
            String resp = processor.ResponseValidator.GetBodyAsText();
            String restName = JsonPath.read(resp,"$.data.name");
            Processor processor1 = cmsHelper.createItem(restName);
            String newItem = processor1.ResponseValidator.GetBodyAsText();
            System.out.println(newItem);
            //need to push item events from cms
            //then verify new item added in menu

        }

    @Test(dataProvider = "menuv4", description = "Verify if items are enabled or in stock for all items")
    public void menuV4AInStockEnabled( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();

         List<String> enabled = Arrays.asList(JsonPath.read(response, "$.data.menu.items..enabled").toString().replace("[", "").replace("]", "").split(","));
         List<String> enabledList = new ArrayList<>();
         List<String> inStock = Arrays.asList(JsonPath.read(response, "$.data.menu.items..inStock").toString().replace("[", "").replace("]", "").split(","));
         List<String> inStockList = new ArrayList<>();
		    	for (int i = 0; i < enabled.size(); i++) {
                     if (enabled.get(i).equals("1")) {
                     enabledList.add(enabled.get(i));
                      Assert.assertEquals(enabledList.get(i), "1", "not enabled");
                     }
                     if (inStock.get(i).equals("1")) {
                     inStockList.add(inStock.get(i));
                     Assert.assertEquals(inStockList.get(i), "1", "out of stock");
                      } else if (inStock.get(i).equals("0")) {
                     inStockList.add(inStock.get(i));
                     Assert.assertEquals(inStockList.get(i), "0", "in stock");
                     }
             }
        }

    @Test(dataProvider = "menuv4", description = "Verify prices are in paise")
    public void menuV4Price( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> price = JsonPath.read(response,"$.data.menu.items..price");
        for (int i = 0; i < price.size(); i++) {
            if(price.indexOf(i) % 100 == 0){
                Assert.assertEquals(price.indexOf(i),"Price is in paise","Price is not in paise");
            }

        }
    }

    @Test(dataProvider = "menuv4", description = "Verify availabilty fields when restaurant is open or close")
    public void menuV4Avail( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        Boolean open = JsonPath.read(response,"$.data.availability.opened");
        if(open= true){
            Assert.assertNotNull(JsonPath.read(response,"$.data.availability.nextCloseTime"));
        }
        else if (open = false){
            Assert.assertNotNull(JsonPath.read(response,"$.data.availability.nextOpenTime"));
        }
    }

    @Test(dataProvider = "menuv4", description = "Verify if items are veg or not")
    public void menuV4Veg( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        List<String> veg = JsonPath.read(response, "$.data.menu.items..isVeg");
        for (int i = 0; i < veg.size(); i++) {
            if (veg.get(i)=="1"){
                Assert.assertEquals(veg.get(i),"1","Item is not veg");
            }
            else if (veg.get(i)=="0"){
                Assert.assertEquals(veg.get(i),"0","Item is veg");
            }
        }
    }

    @Test(dataProvider = "menuv4", description = "Verify if items are veg or not")
    public void menuV4Sla( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        int minTime = JsonPath.read(response, "$.data.sla.minDeliveryTime");
        int maxTime = JsonPath.read(response, "$.data.sla.maxDeliveryTime");
        String slaString = JsonPath.read(response, "$.data.sla.slaString");
        // Assert.assertEquals(slaString, + minTime  );
         Assert.assertEquals(slaString,minTime+"-"+maxTime+" "+"MINS");
    }

    @Test(dataProvider = "menuv4", description = "Verify if irestaurant slug exists and its data are correct")
    public void menuV4Slug( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        String restaurantSlugNode = JsonPath.read(response, "$.data.restaurantSlug");
        Assert.assertNotNull(restaurantSlugNode, "Restaurant slug node is null");
        Assert.assertEquals("$.data.restaurantSlug.restaurant", "$.data.slug");
        Assert.assertEquals("$.data.restaurantSlug.city", "$.data.city");
    }

    @Test(dataProvider = "menuv4", description = "Verify if irestaurant slug exists and its data are correct")
    public void menuV4Favorite( String lat, String lng) {

        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        //setfavorite helper from cms
        String response = processor.ResponseValidator.GetBodyAsText();
        Boolean favorite = JsonPath.read(response, "$.data.favorite");
        Assert.assertFalse(favorite,"Favorite is true");
    }

    @Test(dataProvider = "menuTags", description = "check for Must Try")
    public void must_try(String lat, String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        String ribbonData = null;
        Processor menuResponse = helper.menuV3(restId);
        List menuItemIds = JsonPath.read(menuResponse.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");

        LocalDateTime date1 = LocalDateTime.now();
        int time = helper.getTime(date1);
        int slotId = helper.getSlotIdforItem(time);

        String keyInitial = SANDConstants.keyInitial;
        String entity = restId;
        int redisdb = SANDConstants.db;
        String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);


        HashMap<String, String> mustTry = JsonPath.read(redisInfo, "$.item_ratings");
        List<String> itemIdsMustTry = new ArrayList<String>(mustTry.keySet());
        boolean flag = false;
        for (int j = 0; j < itemIdsMustTry.size(); j++) {
            boolean itemMatched = menuItemIds.contains(Integer.parseInt(itemIdsMustTry.get(j).trim().toString()));
            System.out.println(itemMatched + itemIdsMustTry.get(j).trim().toString());
            if (itemMatched == true) {
                flag = true;
                //System.out.println(itemMatched);
                String itemIdMatched = itemIdsMustTry.get(j);
                //System.out.println("ItemId matched :" + itemIdMatched);
                ribbonData = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..menu.items." + itemIdMatched + ".ribbon.text");
                Assert.assertEquals(ribbonData, "[\"MUST TRY\"]", "must try not present");
                break;
            }
        }
        if (!flag) {
            Assert.fail("assertion failed");
        }
    }


    @Test(dataProvider = "menuTags", description = "check for Bestseller")
    public void bestseller(String lat, String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});

        String ribbonData = null;
        Processor menuResponse = helper.menuV3(restId);
        List menuItemIds = JsonPath.read(menuResponse.ResponseValidator.GetBodyAsText(), "$.data.menu.items[*].id");

        LocalDateTime date1 = LocalDateTime.now();
        int time = helper.getTime(date1);
        int slotId = helper.getSlotIdforItem(time);

        String keyInitial = SANDConstants.keyInitial;
        String entity = restId;
        int redisdb = SANDConstants.db;
        String redisInfo = helper.redisconnectget(keyInitial, entity, redisdb);
        HashMap<String, String> bestseller = JsonPath.read(redisInfo, "$.most_ordered." + slotId);
        List<String> itemIdsbestseller = new ArrayList<String>(bestseller.keySet());
        boolean flag = false;
        for (int j = 0; j < itemIdsbestseller.size(); j++) {
            boolean itemMatched = menuItemIds.contains(Integer.parseInt(itemIdsbestseller.get(j).trim().toString()));
            System.out.println(itemMatched + itemIdsbestseller.get(j).trim().toString());
            if (itemMatched == true) {
                flag = true;
                String itemIdMatched = itemIdsbestseller.get(j);
                ribbonData = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$..menu.items." + itemIdMatched + ".ribbon.text");
                Assert.assertEquals(ribbonData, "[\"BESTSELLER\"]", "ribbon data not present");
                break;
            }
            if (!flag) {
                Assert.fail(" Bestseller assertion failed");
            }
        }
    }


    @Test(dataProvider = "menuv4", description = "Verify menuV4 reponse by passing slug and uuid")
    public void menuV4WithUuidSlug( String lat, String lng) {
        String restId = helper.Aggregator(new String[]{lat, lng});
        Processor processor = helper.menuV4RestId(restId, lat, lng);
        String response = processor.ResponseValidator.GetBodyAsText();
        String slug = JsonPath.read(response, "$.data.slug");
        String uuid = JsonPath.read(response, "$.data.uuid");
        Processor processor1 = helper.menuV4UuidSlug (restId, lat, lng, uuid, slug);
        String response1 = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(response1, "$.statusCode");
        String statusMessage = JsonPath.read(response1, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), 200);
        Assert.assertEquals(statusCode, 0);
        Assert.assertEquals(statusMessage, "done successfully");


    }

    @Test(dataProvider = "freebieMenu", description = "apply freebie discount and verify the same on menu" )
    public void menuFreebie(String lat, String lng, String freebie){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        helper.TMenu(restIds.get(0));
        String item=orderPlace.getItemFromMenu(restIds.get(0),null, null);
        try {
            System.out.println("restId="+restIds.get(0));
            hMap=rngHelper.createFeebieTDWithMinAmountAtRestaurantLevel("10", restIds.get(0), item);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid, "$.data.ruleDiscount.type"), freebie, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=helper.menuV4RestId(restIds.get(0),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.aggregatedDiscountInfo.descriptionList..discountType").split(",")).contains(freebie), "no free bie on this restaurant");
    }

    @Test(dataProvider = "percentageMenu", description = "apply percentage discount and verify the same on menu" )
    public void menuPercentage(String lat, String lng, String percentage){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createPercentageWithNoMinAmountAtRestaurantLevel(restIds.get(1));
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=helper.menuV4RestId(restIds.get(1),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(percentage), "no percentage on this restaurant");
    }

    @Test(dataProvider = "freeDelMenu", description = "apply free del and verify the same on menu" )
    public void menuFreeDelivery(String lat, String lng, String freeDel){
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        HashMap<String, String> hMap;
        try {
            hMap=rngHelper.createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel("99.0", restIds.get(2), false, false, false, "ZERO_DAYS_DORMANT", false);
            String getTid= RngHelper.getTradeDiscount(hMap.get("TDID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid, "$.data.ruleDiscount.type"), freeDel, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=helper.menuV4RestId(restIds.get(2),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(freeDel), "no free del on this restaurant");
    }

    @Test(dataProvider = "flatMenu", description = "apply flat del and verify the same on menu" )
    public void menuFlat(String lat, String lng, String flat){
        HashMap<String, String> hMap;
        List<String> restIds = orderPlace.aggregatorTD(new String[]{lat, lng}, null, null);
        try {
            hMap=rngHelper.createFlatWithMinCartAmountAtRestaurantLevel("10", restIds.get(3),"10");
            String getTid= RngHelper.getTradeDiscount(hMap.get("TID")).ResponseValidator.GetBodyAsText();
            Assert.assertEquals(helper.JsonString(getTid, "$.data.ruleDiscount.type"), flat, "No discounts present or different type of discount is there");
        }
        catch(Exception e){
            e.printStackTrace();
            Assert.assertTrue(false, "No discounts present or different type of discount is there");
        }
        String response=helper.menuV4RestId(restIds.get(3),lat, lng).ResponseValidator.GetBodyAsText();
        Assert.assertTrue(Arrays.asList(helper.JsonString(response,"$.data.tradeCampaignHeaders..discountType").split(",")).contains(flat), "no flat on this restaurant");
    }

    @Test(dataProvider = "rainMode", description = "apply a rain mode and verify the rain mode on aggregator")
    public void menuRainMode(String lat, String lng,String rainMode, String removeRainMode, String heavy) throws InterruptedException{
        String response=helper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(helper.JsonString(response, "$.data.restaurants..id").split(","));
        List<String> zoneId = Arrays.asList(helper.JsonString(response, "$.data.restaurants..areaId").split(","));
        helper.rainMode(rainMode, zoneId.get(0));
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<zoneId.size(); i++){
            if(zoneId.get(0).equals(zoneId.get(i))){
                list.add(i);
            }
        }
        Thread.sleep(10000);
        String menuResponse=helper.menuV4RestId(restIds.get(list.get(0)),lat,lng).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(helper.JsonString(menuResponse, "$.data..rainMode"), heavy, "rain mode is not heavy");
        helper.rainMode(removeRainMode, zoneId.get(0));
    }

    @Test(dataProvider = "slug", description = "verify costfortwo on menu")
    public void costForTwoMenu(String lat, String lng){
        String response=helper.aggregator(new String[]{lat,lng}).ResponseValidator.GetBodyAsText();
        List<String> restIds= Arrays.asList(helper.JsonString(response, "$.data.restaurants..id").split(","));
        for(int i=0; i<10; i++){
            String menuResponse = helper.menuV4RestId(restIds.get(i), lat,lng).ResponseValidator.GetBodyAsText();
            String costForTwo = helper.JsonString(menuResponse, "$.data.costForTwo");
            Map<String, Object> hMap=helper.getSlug(restIds.get(i));
            Assert.assertEquals(Integer.parseInt(costForTwo), (int) (Double.parseDouble(hMap.get("cost_for_two").toString())) * 100, "cost for two is not present");
        }
    }

    }


