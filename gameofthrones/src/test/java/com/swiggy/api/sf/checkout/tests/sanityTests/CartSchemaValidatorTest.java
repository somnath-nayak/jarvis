package com.swiggy.api.sf.checkout.tests.sanityTests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.dp.CheckoutCartSchemaValidationDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import com.swiggy.api.sf.checkout.helper.edvo.helper.EDVOCartHelper;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class CartSchemaValidatorTest extends CheckoutCartSchemaValidationDP {

    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    CheckoutHelper helper=new CheckoutHelper();
    CheckoutPricingHelper pricingHelper = new CheckoutPricingHelper();



    String tid;
    String token;
    HashMap<String, String> hashMap;
    String schemaPath = "Data/SchemaSet/Json/Checkout/";

    @BeforeClass
    public void login() {
        hashMap = helper.TokenData("8197982351", "welcome123");
        tid = hashMap.get("Tid");
        token = hashMap.get("Token");
    }


    @Test(dataProvider = "DeleteSchemaValidator", groups = {"schemaValidator"})
    public void DeleteSchemaValidatorTest(String userAgent, String versionCode, String fileName)
            throws IOException, ProcessingException {

        String deleteCart=deleteCart(userAgent,versionCode);
        System.out.println(deleteCart);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(deleteCart,expectedSchema,"Flush Cart");
    }

    @Test(dataProvider = "minimalCartV2",priority = 0,groups = {"schemaValidator"})
    public void UpdateMinimalSchemaValidatorTest(String userAgent,String versionCode,int n,String restId,String fileName)
            throws IOException, ProcessingException {

        EDVOCartHelper edvoCartHelper = new EDVOCartHelper();
        HashMap<String, String> headers =  hashMap;
        headers.put("Content-Type", "application/json");
        headers.put("User-Agent", userAgent);
        headers.put("version-code", versionCode);

        String cartMinimalPayload = Utility.jsonEncode(edvoCartHelper.getCartPayload(restId,
                true,
                1,
                false,
                false,
                1,
                false,
                null,
                "REGULAR"));
        Processor processor = edvoCartHelper.createMinimalCart(cartMinimalPayload, headers);
        String updateMinimalCart = processor.ResponseValidator.GetBodyAsText();
        System.out.println(updateMinimalCart);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        validateSchema(updateMinimalCart,expectedSchema,"Update Minimal Cart");

    }

//    @Test(dataProvider = "cartCheckTotalV2", groups = {"schemaValidator"})
    public void CheckCartTotalsV2(String userAgent,String versionCode,int n,String restId,String fileName)
            throws IOException, ProcessingException {

        String Checktotals=CartCheckTotals(userAgent,versionCode,n,restId);
        System.out.println(Checktotals);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(Checktotals,expectedSchema,"Check Totals");

    }

    @Test(dataProvider = "getCart", groups = {"schemaValidator"})
    public void Getcart(String userAgent,String versionCode,String fileName)
            throws IOException, ProcessingException {

        String getcart=getCart(userAgent,versionCode);
        System.out.println(getcart);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(getcart,expectedSchema,"Get Cart");

    }





    public String deleteCart(String userAgent, String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  helper.DeleteCart(header).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }

    public String updateMinimalCart(String userAgent, String versionCode,int n,String restId){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  helper.cartMinimalV2(header,n,restId).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }


    public String  CartCheckTotals(String userAgent, String versionCode,int n,String restId)
    {

        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  helper. CreateCartCheckTotals(header,n,restId).ResponseValidator.GetBodyAsText();

        String restId1= JsonPath.read(cartResponse, "$.data.restaurant_details.id").toString().replace("[","").replace("]","");
        String menuItem=JsonPath.read(cartResponse, "$.data.cart_menu_items..menu_item_id").toString().replace("[","").replace("]","");

        return cartResponse;

    }

    public String getCart(String userAgent, String versionCode){
        HashMap<String, String> header=helper.setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  helper.getCart(header).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }








    public void validateSchema(String response,String expectedSchema,String message) throws IOException, ProcessingException{
//        System.out.println("Expected jsonschema   $$$$" + expectedSchema);
        List missingNodeList = schemaValidatorUtils.validateServiceSchema(expectedSchema, response);
        Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For "+message+" API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes=>" + isEmpty);
        Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }




    public String getSchemafromFile(String schemaPath,String fileName) throws IOException, ProcessingException{
        String userDir = System.getProperty("user.dir");
        userDir = userDir.substring(0, userDir.indexOf("gameofthrones"));
        Reporter.log("[FILE PATH] " + userDir + schemaPath+fileName, true);
        return new ToolBox().readFileAsString(userDir + schemaPath+fileName);    }


}







