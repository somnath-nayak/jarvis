package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderCancelCheck;

import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "statusCode",
        "statusMessage",
        "data",
        "tid",
        "sid",
        "deviceId",
        "successful"
})
public class OrderCancelCheckResponse {

    @JsonProperty("statusCode")
    private Integer statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("data")
    private Data data;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("sid")
    private String sid;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("successful")
    private Boolean successful;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderCancelCheckResponse() {
    }

    /**
     *
     * @param statusCode
     * @param sid
     * @param data
     * @param successful
     * @param tid
     * @param deviceId
     * @param statusMessage
     */
    public OrderCancelCheckResponse(Integer statusCode, String statusMessage, Data data, String tid, String sid, String deviceId, Boolean successful) {
        super();
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.data = data;
        this.tid = tid;
        this.sid = sid;
        this.deviceId = deviceId;
        this.successful = successful;
    }

    @JsonProperty("statusCode")
    public Integer getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("tid")
    public String getTid() {
        return tid;
    }

    @JsonProperty("tid")
    public void setTid(String tid) {
        this.tid = tid;
    }

    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("successful")
    public Boolean getSuccessful() {
        return successful;
    }

    @JsonProperty("successful")
    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}