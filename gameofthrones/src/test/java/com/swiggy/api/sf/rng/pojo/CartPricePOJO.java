package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "quantity",
        "cartTotal",
        "itemLevelPrice",
        "preferredPaymentMethod",
        "referralPresence",
        "swiggyMoneyApplicable"
})
public class CartPricePOJO {

    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("cartTotal")
    private Integer cartTotal;
    @JsonProperty("itemLevelPrice")
    private ItemLevelPricePOJO itemLevelPrice;
    @JsonProperty("preferredPaymentMethod")
    private String preferredPaymentMethod;
    @JsonProperty("referralPresence")
    private Boolean referralPresence;
    @JsonProperty("swiggyMoneyApplicable")
    private Boolean swiggyMoneyApplicable;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CartPricePOJO() {
    }

    /**
     *
     * @param cartTotal
     * @param referralPresence
     * @param itemLevelPrice
     * @param quantity
     * @param swiggyMoneyApplicable
     * @param preferredPaymentMethod
     */
    public CartPricePOJO(Integer quantity, Integer cartTotal, ItemLevelPricePOJO itemLevelPrice, String preferredPaymentMethod, Boolean referralPresence, Boolean swiggyMoneyApplicable) {
        super();
        this.quantity = quantity;
        this.cartTotal = cartTotal;
        this.itemLevelPrice = itemLevelPrice;
        this.preferredPaymentMethod = preferredPaymentMethod;
        this.referralPresence = referralPresence;
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public CartPricePOJO withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("cartTotal")
    public Integer getCartTotal() {
        return cartTotal;
    }

    @JsonProperty("cartTotal")
    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    public CartPricePOJO withCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
        return this;
    }

    @JsonProperty("itemLevelPrice")
    public ItemLevelPricePOJO getItemLevelPrice() {
        return itemLevelPrice;
    }

    @JsonProperty("itemLevelPrice")
    public void setItemLevelPrice(ItemLevelPricePOJO itemLevelPrice) {
        this.itemLevelPrice = itemLevelPrice;
    }

    public CartPricePOJO withItemLevelPrice(ItemLevelPricePOJO itemLevelPrice) {
        this.itemLevelPrice = itemLevelPrice;
        return this;
    }

    @JsonProperty("preferredPaymentMethod")
    public String getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    @JsonProperty("preferredPaymentMethod")
    public void setPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
    }

    public CartPricePOJO withPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
        return this;
    }

    @JsonProperty("referralPresence")
    public Boolean getReferralPresence() {
        return referralPresence;
    }

    @JsonProperty("referralPresence")
    public void setReferralPresence(Boolean referralPresence) {
        this.referralPresence = referralPresence;
    }

    public CartPricePOJO withReferralPresence(Boolean referralPresence) {
        this.referralPresence = referralPresence;
        return this;
    }

    @JsonProperty("swiggyMoneyApplicable")
    public Boolean getSwiggyMoneyApplicable() {
        return swiggyMoneyApplicable;
    }

    @JsonProperty("swiggyMoneyApplicable")
    public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    public CartPricePOJO withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CartPricePOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CartPricePOJO setDefault() {
//        ItemLevelPricePOJO itemLevelPricePOJO = new ItemLevelPricePOJO().setDefault();
        return this.withCartTotal(105)
//            .withItemLevelPrice(itemLevelPricePOJO)
            .withPreferredPaymentMethod("PhonePe")
            .withQuantity(2)
            .withReferralPresence(false)
            .withSwiggyMoneyApplicable(false);
    }

}
