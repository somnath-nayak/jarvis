package com.swiggy.api.sf.rng.tests.TradeDiscount;

import com.swiggy.api.sf.rng.dp.EDVODp.EDVOMultiTdDP;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateListingV3.EvaluateListingV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.EvaluateMenu;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOCartV3Builder;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOMealPOJO;
import com.swiggy.api.sf.rng.pojo.edvo.ItemRequestCartV3Builder;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;


public class EDVOMultiTdTest {
    EDVOMultiTdHelper edvoMultiTd = new EDVOMultiTdHelper();
    EDVOHelper edvoHelper = new EDVOHelper();
    SuperHelper superHelper = new SuperHelper();
    CopyResolverHelper copyResolverHelper = new CopyResolverHelper();

    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    JsonHelper jsonHelper = new JsonHelper();

    // EDVO TD & Restaurant level Freedel Td With MOV

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3")
    public void createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superUserId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(tdCreation.contains("Restaurant"), true);

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // EDVO TD & Restaurant level Freedel Td With No MOV

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3")
    public void createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superUserId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(tdCreation.contains("Restaurant"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in MEAL");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");

        softAssert.assertAll();
        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }


    // Super Freedel TD, EDVO TD & Restaurant level Freedel Td With No MOV As Super user

    @Test(dataProvider = "createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as Super user")
    public void createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superUserId) throws IOException, InterruptedException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(tdCreation.contains("Restaurant"), true);
        Thread.sleep(1000);
        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible to super user in Listing");
        softAssert.assertEquals(tdInListingResp, "[]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible to super user in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]", "EDVO public campagin is not visible in Menu");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]", "--->>> Meal Cart edvo campaign missing");
        softAssert.assertEquals(superFreedelCampIdInMealCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(superFreedelCampInItemCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }


    //non- Super Freedel TD, EDVO TD & Restaurant level Freedel Td With MOV

    @Test(dataProvider = "createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsNonSuperDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsNonSuperTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(tdCreation.contains("Restaurant"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO campaign is not visible in listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Restaurant level Freedel is not visible in listing ");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]", "Restaurant level Freedel is visible in Meal");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Freedel is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        //Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Restaurant level Freedel is not visible in MealItem CARTV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Restaurant level Freedel is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //
//    // Super Freebie TD, EDVO TD & Restaurant level Freebie Td With MOV
//
    @Test(dataProvider = "createSuperFreebieTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreebieTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVTest(String tdCreation, String edvoCampaignId, String superFreebieCamp, String tdSameItemFreebieCreation) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreebieCamp.contains("Restaurant"), true);
        softAssert.assertEquals(tdCreation.contains("fail"), true);
        softAssert.assertEquals(tdSameItemFreebieCreation.contains("fail"), true);
        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as non-super user
    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Rest. level freebie is not visible in listing ");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in MENU request");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in Meal Cart request");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Rest. level freebie is not visible in item request cartV3 ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With No MOV as non-super user
    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException, InterruptedException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);
        Thread.sleep(1000);
        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in Restaurant listing");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in Restaurant Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in Meal cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Rest. Freebie is not visible in Restaurant cartV3");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as Super user
    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With No-MOV as Super user
    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as Super user & evaluate with MOV Zero

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserButEvalauteWithMOVZeroDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserButEvalauteWithMOVZeroTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.supe.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }


    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as Non Super user & evaluate with MOV Zero

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserButEvalauteWithMOVZeroDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserButEvalauteWithMOVZeroTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With No-MOV as Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With MOV as Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for Super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With No-MOV as Non-Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "%age td is not visible in Listing");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]", "super freedel td is not visible in Listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With No-MOV as Non-Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        //Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as  Non-Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in Listing");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in MENU");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in MEAL cart");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }
    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With No-MOV as Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        // softAssert.assertEquals(tdInInMealCartResp, "[]","Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With MOV as Super user

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException, InterruptedException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);
        Thread.sleep(1000);
        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3 of SUper user");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Flat Td With No-MOV as Non-Super user

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreedelWithoutAnySuperTdWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreedelWithoutAnySuperTdWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        //softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        // String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp +"]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        // String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        // String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        // String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Fail- Need to Fix because restaurant level Freedel td is not getting in MealItemRequest of cartV3");
        //softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithoutSuperTdWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithoutSuperTdWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        //softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId=="+superFreedelCamp+")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible is listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation +")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");
        //softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        // softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        // softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        // softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Freebie Td With No-MOV as Non-Super user

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreebieWithoutAnySuperTdWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreebieWithoutAnySuperTdWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        //softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        // String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp +"]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        // String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in MEAL request");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        // String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Menu response");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        // String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]", "EDVO public td is not visible in MealItem Cart response");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Fail- Need to Fix because restaurant level Freedel td is not getting in MealItemRequest of cartV3");
        //softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Freebie Td With MOV as  Non-Super user

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithoutSuperTdWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithoutSuperTdWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        //softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId=="+superFreedelCamp+")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + tdCreation +")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]");
        //softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    //  EDVO TD & Restaurant level Freebie Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        // softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO pubic campaign is not visible in listing for non-super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        // softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        //softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        //softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        //String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level freebie td is getting in MealItemRequest of cartV3");
        // softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        //String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        // softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Flat Td With No-MOV as Super user

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampInListingResp, "[" + superFreebieCamp + "]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[" + superFreebieCamp + "]");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[" + superFreebieCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[" + superFreebieCamp + "]", "Frebiee camp. is not getting in restaurant CartV3");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user
//=====
    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[]", "Need to fix - Super freebie is visible in Listing request when freebie benefit is not mapped with the super user plan");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Meal for super user");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Need to fix - Super freebie is visible in MEAL request");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Menu for super user");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]", "Need to fix - Super freebie is visible in MENU request when freebie benefit is not mapped with the super user plan");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Meal Cart for super user");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]", "Super freebie is visible in mealItem cartV3 request");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Super freebie is visible in Item cartV3 request");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1
//====
    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO td is not visible in listing for Super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in Meal for Super user");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level %age Td With No-MOV as Super user

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampInListingResp, "[" + superFreebieCamp + "]");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[" + superFreebieCamp + "]");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[" + superFreebieCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant %age td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[" + superFreebieCamp + "]", "Frebiee camp. is not getting in restaurant CartV3");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[" + superFreebieCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[" + superFreebieCamp + "]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[" + superFreebieCamp + "]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[" + superFreebieCamp + "]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[" + superFreedelCamp + "]");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[" + superFreebieCamp + "]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Freedel td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level %age Td With No-MOV as Non-Super user

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");
        softAssert.assertEquals(superFreebieCampInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Need to fix - Fail because in ItemRequest cartV3 restaurant %age td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible in listing for non super user");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible to non super user in Listing ");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]", "Need to fix - SUper freedel is visible in listing to non-super user");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[]", "Need to fix - Super freebie is visible in trade discount info list for non super user");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]", "EDVO public campaign is not visible to non super user in Meal ");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]", "Rest. %age td is visible in item Request of cartV3");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Flat Td With No-MOV as Non-Super user

    @Test(dataProvider = "createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");
        softAssert.assertEquals(superFreebieCampInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");

        // Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");

        // Evaluate Meal Cart
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Fail because in ItemRequest cartV3 restaurant flat td is not getting");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td With MOV as  Non-Super user

    @Test(dataProvider = "createEvalauteSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvalauteSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreedbieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");
        softAssert.assertEquals(superFreedbieCampInListingResp, "[]", "Fail- Need to Fix Super freebie is visible to non-Super user in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @Test(dataProvider = "createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO with MOV and Freedel with MOV & evaluate Listing, Menu, Meal, Item request CartV3 and MealItem Request CartV3 as non-Super user")
    public void createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneTest(String tdCreation, String edvoCampaignId, String superFreedelCamp, String superFreebieCamp, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder edvoCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String userId) throws IOException {

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertNotEquals(edvoCampaignId.contains("Restaurant"), true);
        softAssert.assertNotEquals(superFreedelCamp.contains("Restaurant"), true);
        softAssert.assertNotEquals(tdCreation.contains("fail"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoCampaignId + "]", "EDVO Campaign is not visible in Listing");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(superFreedelCampInListingResp, "[]");
        softAssert.assertEquals(superFreebieCampInListingResp, "[]", "Super freebie is visible in mealItem Request listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoCampaignId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoCampaignId + "]");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(superFreedelCampInMealResp, "[]");
        softAssert.assertEquals(superFreebieCampInMealResp, "[]", "Super freebie is visible in mealItem Request Meal ");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]", "Super freebie is visible in mealItem Request Menu ");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInMenuResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(edvoCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String superFreedelTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        String superFreebieTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "Fail- Need to Fix because restaurant level Freedel td is not getting in MealItemRequest of cartV3");
        softAssert.assertEquals(superFreedelTdInInMealCartResp, "[]");
        softAssert.assertEquals(superFreebieTdInInMealCartResp, "[]", "Super freebie is visible in mealItem Request cartV3 ");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoCampaignId + ")].campaignId");
        String superFreedelCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        String superFreebieCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreedelCamp + ")].campaignId");
        softAssert.assertEquals(tdCampInItemCartResp, "[]");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreedelCampIdInCartResp, "[]");
        softAssert.assertEquals(superFreebieCampIdInCartResp, "[]", "Super freebie is visible in item Request cartV3 ");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // EDVO TD, Restaurant level %age & Flat Td with MOV
    @Test(dataProvider = "createEDVORestaurantPercentageAndFlatCampaignsWithMOVDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO, %age and Flat campaigns with MOV on same restaurant, should not allow to create Flat or %age at a same time ")
    public void createEDVORestaurantPercentageAndFlatCampaignsWithMOVTest(String tdCreation, String edvoCampaignId, String percentCampaignId) throws IOException {
        String multiTdCreateStatus = "FAIL";
        if (tdCreation.equalsIgnoreCase("fail") && !(edvoCampaignId.equalsIgnoreCase("fail")) && (!(percentCampaignId.equalsIgnoreCase("fail")))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + tdCreation);
        Assert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);
    }

    //  Create EDVO TD, Restaurant level  Flat Td & %age with MOV
    @Test(dataProvider = "createEDVORestaurantFlatAndPercentageCampaignsWithMOVDP", dataProviderClass = EDVOMultiTdDP.class, description = "Create EDVO, Flat & %age campaigns with MOV on same restaurant, should not allow to create Flat or %age at a same time ")
    public void createEDVORestaurantFlatAndPercentageCampaignsWithMOVTest(String tdCreation, String edvoCampaignId, String percentCampaignId) throws IOException {
        String multiTdCreateStatus = "FAIL";
        if (!(tdCreation.equalsIgnoreCase("fail")) && !(edvoCampaignId.equalsIgnoreCase("fail")) && percentCampaignId.equalsIgnoreCase("fail")) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + tdCreation);
        Assert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);


    }

    // EDVO public , 1st order, rest 1st order, user restriction & dormant (thirty days) with MOV
    @Test(dataProvider = "createEDVOForAllUserSegmentDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createEDVOForAllUserSegmentTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId) throws IOException {
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        Assert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);
    }

    // EDVO public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create  with MOV
    @Test(dataProvider = "createEDVOWithUserSegmentAndTwoOfDormantTypeDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createEDVOWithUserSegmentAndTwoOfDormantTypeTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId, String edvoSixtyDormantCampId) throws IOException {
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail")) && edvoSixtyDormantCampId.equalsIgnoreCase("fail")) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        Assert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);
    }

    // Create EDVO user segment td and restaurant level public type Flat td and Evaluate
    @Test(dataProvider = "createFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoFirstCampId + "]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoFirstCampId + "]", "EDVO 1st order TD is not visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]", "Restaurant 1st order EDVO td is visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoFirstCampId + "]", "EDVO 1st order type is not visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoFirstCampId + "]", "EDVO 1st order Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create & Evaluate EDVO user segment td and restaurant level public type Flat td and Evaluate As public non-super user

    @Test(dataProvider = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is  visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level Flat td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type %age td and Evaluate
    @Test(dataProvider = "createPercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createPercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoFirstCampId + "]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoFirstCampId + "]", "EDVO 1st order TD is not visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]", "EDVO Rest. 1st order TD is visible in Meal");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoFirstCampId + "]", "EDVO 1st order type is not visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoFirstCampId + "]", "EDVO 1st order Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level %age Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type %age td and Evaluate As public non-Super user
    @Test(dataProvider = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Fail- Need to Fix because restaurant level %age td is getting in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level %age Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freedel td and Evaluate

    @Test(dataProvider = "createFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoFirstCampId + "]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoFirstCampId + "]", "EDVO 1st order TD is not visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]", "EDVO Rest. 1st td is visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Freedel TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoFirstCampId + "]", "EDVO 1st order type is not visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]", "EDVO Rest. 1st td is visible in MENU ");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoFirstCampId + "]", "EDVO 1st order Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "restaurant level Freedel td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Freedel Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freedel td and Evaluate As public non-Super user

    @Test(dataProvider = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in Meal");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Freedel TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "restaurant level Freedel td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Freedel Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freebie td and Evaluate

    @Test(dataProvider = "createFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[" + edvoFirstCampId + "]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[" + edvoFirstCampId + "]", "EDVO 1st order TD is not visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]", "EDVO Rest. 1st order TD is visible in Meal");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is not visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Freebie TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[" + edvoFirstCampId + "]", "EDVO 1st order type is not visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public type td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]", "EDVO rest. 1st order td is visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[" + edvoFirstCampId + "]", "EDVO 1st order Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "restaurant level Freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Freebie Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freebie td and Evaluate As Public Non Super User

    @Test(dataProvider = "createAndEvaluateFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "public type EDVO is not visible in Meal");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is not visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Freebie TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "public type EDVO is not visible in Menu");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "restaurant level Freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Freebie Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freedel td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type freedel TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[" + tdCreation + "]", "restaurant level freedel td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Freedel Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and restaurant level public type Freedel td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[]", "Restaurant level Public type freedel TD is visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "restaurant level freedel td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[]", "Public type Restaurant level Freedel Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Cateogry level %age td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "category level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level %age Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - restaurant level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level %age Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Cateogry level %age td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "category level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level %age Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Sub-Category level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level %age Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }


    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Item level %age td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Item level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Item level %age Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type %age TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Item level %age td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type item level %age Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]", "Super freedel is visible in listing for non super user");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Cateogry level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        Assert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        Assert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        Assert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        Assert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        Assert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Flat TD is not visible in Menu");
        Assert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        Assert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        Assert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - restaurant level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

//    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Sub-Cateogry level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Sub-category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        System.out.println("edvoCampInMealResp 1st order--------->>" + edvoCampInMealResp);
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        System.out.println("edvoPublicCamp --------->>" + edvoPublicCamp);
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "--EDVO public td is not visible in MENU   ");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]", "EDVO rest. 1st order td is visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Sub-Category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }


    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @Test(dataProvider = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie is visible in trade discount info list of listing for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Item level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie td is visible in MealItemRequest of cartV3 for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Item level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie Td is visible in CartV3 itemRequest for non super user ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Item level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as public Super user

    @Test(dataProvider = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[" + edvoPublicCamp + "]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[]");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[]");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[]");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Item level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[" + edvoPublicCamp + "]", "EDVO public td is not visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[]", "EDVO rest 1st order td is visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type item level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }
    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Cateogry level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Meal for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie td is visible in MealItemRequest of cartV3 for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie Td is visible in CartV3 itemRequest for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - restaurant level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Sub-Cateogry level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Sub-category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in lsiting");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Sub-Category level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie td is visible in trade discount info list for non-Super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Item level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Listing Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Item level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Item level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visiible in Listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in Menu");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Item level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type item level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Restaurant level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Super freebie campaign is visible in trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Restaurant level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Restaurant level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Super freebie is visible in Restaurant Cart trade discount info list for non super user ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Restaurant level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]", "Super freebie td is visible in trade discount info list for non-Super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visiible in Listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Flat TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in Menu");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Restaurant level Flat td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Flat Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type %age td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Cateogry level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "category level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Percentage Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Category level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Category level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - restaurant level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Category level Percentage Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Sub-Cateogry level Percentage td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Sub-category level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Percentage Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Sub-Category level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in lsiting");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Sub-Category level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Sub-Category level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Sub-Category level Percentage Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Item level Percentage td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Item level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Item level Percentage Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Item level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visiible in Listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Item level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in Menu");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Item level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type item level Percentage Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Restaurant level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @Test(dataProvider = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[]");
        softAssert.assertEquals(superFreebieInListingResp, "[]", "Need to fix - Super freebie is visible in Listing trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]", "Restaurant level Flat td is not visible in listing");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in listing");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]", "Need to fix - Super freebie is visible in Meal trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[]", "Super freedel Td is visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[]", "Need to fix - Super freebie is visible in Menu trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in MENU");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[]", "Super freedel Td is visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[]", "Need to fix - Super freebie is visible in Meal Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Restaurant level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[]", "Super freedel td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[]", "Need to fix - Super freebie is visible in Restaurant Cart trade discount info list for non super user");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Flat Td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }

    // Create EDVO user segment td and Restaurant level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @Test(dataProvider = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP", dataProviderClass = EDVOMultiTdDP.class, description = "Super public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create two dormant types at a time ")
    public void createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserTest(String edvoPublicCamp, String edvoRestFirstCamp, String edvoFirstCampId, String edvoThirtyDormantCampId, String edvoUserRestrictedCampId
            , String tdCreation, EvaluateListingV3 listingPayload, EDVOMealPOJO mealPayload, EvaluateMenu evaluateMenu, EDVOCartV3Builder mealCartV3Payload, ItemRequestCartV3Builder restaurantCartV3payload, String superFreebieCamp, String superFreeDelCamp) throws IOException {
        SoftAssert softAssert = new SoftAssert();
        String multiTdCreateStatus = "FAIL";
        if (!(edvoPublicCamp.equalsIgnoreCase("fail")) && !(edvoRestFirstCamp.equalsIgnoreCase("fail")) && !(edvoFirstCampId.equalsIgnoreCase("fail")) && !(edvoThirtyDormantCampId.equalsIgnoreCase("fail")) && !(edvoUserRestrictedCampId.equalsIgnoreCase("fail"))) {
            multiTdCreateStatus = "PASS";
        }
        System.out.println("---->>>>>>>>>>>> " + edvoPublicCamp + "  edvo rest 1st " + edvoRestFirstCamp);
        softAssert.assertEquals(multiTdCreateStatus.equalsIgnoreCase("PASS"), true);

        // Evaluate listing
        Processor listingResponse = superMultiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(listingPayload));
        String tdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInListingResp = listingResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.discountList..tradeDiscountInfoList.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInListingResp, "[" + superFreeDelCamp + "]");
        softAssert.assertEquals(superFreebieInListingResp, "[" + superFreebieCamp + "]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInListingResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampInListingResp, "[]");
        softAssert.assertEquals(edvoCampInListingResp, "[]");
        softAssert.assertEquals(tdInListingResp, "[" + tdCreation + "]");
        softAssert.assertEquals(edvoPublicCampInListingResp, "[]");
        softAssert.assertEquals(edvoRestFirstCampInListingResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visiible in Listing ");

        //Evaluate Meal
        Processor mealResponse = superMultiTDHelper.evaluateMealHelper(jsonHelper.getObjectToJSON(mealPayload));
        String edvoCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoFirstCampId + ")].campaignId");
        String tdInInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInMealResp = mealResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealResp, "[]");
        softAssert.assertEquals(superFreebieInMealResp, "[]");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoUserRestrictedCampIdInMealResp, "[]");
        softAssert.assertEquals(edvoCampInMealResp, "[]", "EDVO 1st order TD is visible in Meal");
        softAssert.assertEquals(tdInInMealResp, "[]");
        softAssert.assertEquals(edvoPublicCampInMealResp, "[]", "EDVO public td is visible in MEAL");
        softAssert.assertEquals(edvoRestFirstCampInMealResp, "[" + edvoRestFirstCamp + "]", "EDVO is not visible in MEAL");

        //Evaluate Menu
        Processor menuResponse = superMultiTDHelper.evaluateMenuHelper(jsonHelper.getObjectToJSON(evaluateMenu));
        String tdCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + tdCreation + ")].campaignId");
        String edvoCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdIdInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInMenuResp = menuResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMenuResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Menu request");
        softAssert.assertEquals(superFreebieInMenuResp, "[" + superFreebieCamp + "]", "Superr freebie td is not visible in Menu");
        softAssert.assertEquals(edvoThirtyDormantCampIdInMenuResp, "[]", "EDVO 30days dormant Td is visible in Menu request");
        softAssert.assertEquals(edvoUserRestrictedCampIdIdInMenuResp, "[]", "EDVO user restricted td is visible in Menu");
        softAssert.assertEquals(tdCampIdInMenuResp, "[" + tdCreation + "]", "Restaurant level Public type Percentage TD is not visible in Menu");
        softAssert.assertEquals(edvoCampIdInMenuResp, "[]", "EDVO 1st order type is visible in Menu");
        softAssert.assertEquals(edvoPublicCampIdInMenuResp, "[]", "EDVO public td is visible in MENU");
        softAssert.assertEquals(edvoRestFirstCampIdInMenuResp, "[" + edvoRestFirstCamp + "]", "EDVO RFO is not visible in Menu");

        // Evaluate Meal Cart
        //superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(cartPayload));
        Processor mealItemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(mealCartV3Payload.edvoCartV3Pogo));
        String edvoCampIdInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String tdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoPublicTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstTdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId== " + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreeDelCamp + ")].campaignId");
        String superFreebieInInMealCartResp = mealItemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId== " + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelInMealCartResp, "[" + superFreeDelCamp + "]", "Super freedel Td is not visible in Meal CartV3 request");
        softAssert.assertEquals(superFreebieInInMealCartResp, "[" + superFreebieCamp + "]", "Super freebie td is not visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoThirtyDormantCampIdInInMealCartResp, "[]", "EDVO 30days dormant Td is visible in Meal CartV3 request");
        softAssert.assertEquals(edvoUserRestrictedCampIdInInMealCartResp, "[]", "EDVO user restricted td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoCampIdInMealCartResp, "[]", "EDVO 1st order Td is visible in Meal CartV3 request");
        softAssert.assertEquals(tdInInMealCartResp, "[]", "Need to fix - Restaurant level Percentage td is visible in MealItemRequest of cartV3");
        softAssert.assertEquals(edvoPublicTdInInMealCartResp, "[]", "EDVO public td is visible in CartV3 mealItemRequest");
        softAssert.assertEquals(edvoRestFirstTdInInMealCartResp, "[" + edvoRestFirstCamp + "]", "EDVO rest 1st order td is not visible in CartV3 mealItemRequest");

        //Evaluate Restaurant Cart
        Processor itemRequestCartResponse = superMultiTDHelper.evaluateCartHelper(jsonHelper.getObjectToJSON(restaurantCartV3payload.edvoCartV3Pogo));
        String tdCampInItemCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.restaurants..tradeDiscountInfo.[?(@.campaignId== " + tdCreation + ")].campaignId");
        String edvoCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoFirstCampId + ")].campaignId");
        String edvoPublicCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoPublicCamp + ")].campaignId");
        String edvoRestFirstCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoRestFirstCamp + ")].campaignId");
        String edvoThirtyDormantCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoThirtyDormantCampId + ")].campaignId");
        String edvoUserRestrictedCampIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.meals..tradeDiscountInfo.[?(@.campaignId==" + edvoUserRestrictedCampId + ")].campaignId");
        String superFreedelIdInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreeDelCamp + ")].campaignId");
        String superFreebieInCartResp = itemRequestCartResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.super..tradeDiscountInfo.[?(@.campaignId==" + superFreebieCamp + ")].campaignId");
        softAssert.assertEquals(superFreedelIdInCartResp, "[" + superFreeDelCamp + "]", "Super freedel td is not visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(superFreebieInCartResp, "[" + superFreebieCamp + "]", "Super freebie Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoThirtyDormantCampIdInCartResp, "[]", "EDVO 30day dormant td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoUserRestrictedCampIdInCartResp, "[]", "EDVO user restricted Td is visible in CartV3 itemRequest ");
        softAssert.assertEquals(tdCampInItemCartResp, "[" + tdCreation + "]", "Public type Restaurant level Percentage Td is visible in Restaurant Cart request- itemRequest ");
        softAssert.assertEquals(edvoCampIdInCartResp, "[]", "EDVO 1st order Td is not visible in CartV3 itemRequest ");
        softAssert.assertEquals(edvoPublicCampIdInCartResp, "[]", "EDVO public td is not visible in CartV3 itemRequest");
        softAssert.assertEquals(edvoRestFirstCampIdInCartResp, "[]", "EDVO Rest 1st order td is visible in CartV3 itemRequest");

        softAssert.assertAll();

        // delete campaigns created in automation
        //edvoHelper.deleteEDVOCampaignFromDB(edvoCampaignId);
        //edvoHelper.deleteEDVOCampaignFromDB(tdCreation);
    }
}
