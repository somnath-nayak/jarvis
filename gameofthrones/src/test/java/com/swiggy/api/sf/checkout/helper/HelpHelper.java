package com.swiggy.api.sf.checkout.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;

public class HelpHelper {
    Initialize gameofthrones = new Initialize();
    //CheckoutHelper checkoutHelper= new CheckoutHelper();

    public Processor help(int n, String tid, String token, String restId)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        GameOfThronesService service = new GameOfThronesService("checkout", "createcartv2nitems", gameofthrones);
        String[] payloadparams = new String[2];
        payloadparams[0] =
        payloadparams[1] = restId;
        Processor processor = new Processor(service, requestheaders, payloadparams);
        return processor;
    }

    public Processor track(String tid, String token, String orderId)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkout", "track", gameofthrones);
        String[] urlParams = {orderId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor trackMinimalOrder(String tid, String token, String orderKey)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkout", "trackminimalorder", gameofthrones);
        String[] urlParams = {orderKey};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor track1(String tid, String token, String orderId)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkout1", "track", gameofthrones);
        String[] urlParams = {orderId};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }

    public Processor trackMinimalOrder1(String tid, String token, String orderKey)
    {
        HashMap<String, String> requestheaders = CheckoutHelper.requestHeader(tid, token);
        requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkout1", "trackminimalorder", gameofthrones);
        String[] urlParams = {orderKey};
        Processor processor = new Processor(service, requestheaders, null, urlParams);
        return processor;
    }
}
