package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import java.util.List;

public class ItemBuilder {
    private Item item;

    public ItemBuilder(){
        item = new Item();
    }

    public ItemBuilder menuItemId(Integer menuItemId){
        item.setMenuItemId(menuItemId);
        return this;
    }

    public ItemBuilder quantity(Integer quantity){
        item.setQuantity(quantity);
        return this;
    }

    public ItemBuilder addons(List<Addon> addons){
        item.setAddons(addons);
        return this;
    }

    public ItemBuilder variants(List<Variant> variants){
        item.setVariants(variants);
        return this;
    }

    public Item build(){
        return item;
    }
}
