package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class Priority {

    private String type;
    private List<String> targettedTags = null;
    private Integer hardPriority;

    /**
     * No args constructor for use in serialization
     *
     */
    public Priority() {
    }

    /**
     *
     * @param hardPriority
     * @param targettedTags
     * @param type
     */
    public Priority(String type, List<String> targettedTags, Integer hardPriority) {
        super();
        this.type = type;
        this.targettedTags = targettedTags;
        this.hardPriority = hardPriority;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getTargettedTags() {
        return targettedTags;
    }

    public void setTargettedTags(List<String> targettedTags) {
        this.targettedTags = targettedTags;
    }

    public Integer getHardPriority() {
        return hardPriority;
    }

    public void setHardPriority(Integer hardPriority) {
        this.hardPriority = hardPriority;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("targettedTags", targettedTags).append("hardPriority", hardPriority).toString();
    }

}