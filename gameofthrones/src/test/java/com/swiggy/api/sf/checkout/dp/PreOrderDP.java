package com.swiggy.api.sf.checkout.dp;


import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.helper.CartPayloadHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.Cart;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.MinAmount;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class PreOrderDP {

    CartPayloadHelper CartPayloadHelper=new CartPayloadHelper();
    CheckoutHelper helper = new CheckoutHelper();

    Cart cartpayload;

    String environment= System.getenv("environment");
    String latLng= System.getenv("latLng");

    @BeforeClass
    public void setCartPayload() {

        String[] latLog=null;

        if (environment==null || latLog==null){
            latLog=new String[]{"12.9352","77.6199"};
        } else if (environment.equalsIgnoreCase("prod")){
            latLog=new String[]{"27.8374445","76.17257740000002"};
        } else {
            latLog=latLng.split(",");
        }
            cartpayload=CartPayloadHelper.getObjectCartPayloadRegular(latLog[0], latLog[1],false);
           try {
                 cartpayload.getCartItems().get(0).getMenuItemId();
           } catch (Exception e){
               throw new SkipException("Skipping test, NO serviceable restaurant, Contact SAND/Delivery Team");
           }
    }


    @DataProvider(name = "preorder")
    public Object[][] CartData() {
        return new Object[][]{
                {cartpayload}
        };
    }

}