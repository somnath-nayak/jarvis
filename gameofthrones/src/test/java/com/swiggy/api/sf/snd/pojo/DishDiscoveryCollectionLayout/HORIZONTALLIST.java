package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class HORIZONTALLIST {

    private String type;
    private String title;
    private String subTitle;
    private Integer minElements;
    private Integer maxElements;

    /**
     * No args constructor for use in serialization
     *
     */
    public HORIZONTALLIST() {
    }

    /**
     *
     * @param subTitle
     * @param title
     * @param maxElements
     * @param minElements
     * @param type
     */
    public HORIZONTALLIST(String type, String title, String subTitle, Integer minElements, Integer maxElements) {
        super();
        this.type = type;
        this.title = title;
        this.subTitle = subTitle;
        this.minElements = minElements;
        this.maxElements = maxElements;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public Integer getMinElements() {
        return minElements;
    }

    public void setMinElements(Integer minElements) {
        this.minElements = minElements;
    }

    public Integer getMaxElements() {
        return maxElements;
    }

    public void setMaxElements(Integer maxElements) {
        this.maxElements = maxElements;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("title", title).append("subTitle", subTitle).append("minElements", minElements).append("maxElements", maxElements).toString();
    }

}