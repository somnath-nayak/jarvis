package com.swiggy.api.sf.snd.helper;

import com.jayway.jsonpath.JsonPath;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class HeaderHelper {

    public HashMap<String, String> contentType(){
        HashMap<String, String> requestheaders = new HashMap<String, String>(){{put("content-type", "application/json");}};
        return requestheaders;
    }

    public String JsonString(String response, String jsonPath){
        return JsonPath.read(response, jsonPath).toString().replace("[","").replace("]","").replace("\"","");
    }

    public HashMap<String, String> requestHeader(String tid, String token) {
        HashMap<String, String> requestheaders = new HashMap<String, String>(){{put("tid", tid);put("token", token); contentType();}};
        return requestheaders;
    }

    public String dateTime()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        String formatTime = new SimpleDateFormat("HH:mm:ss").format(date);
        String DT=modifiedDate+"T"+formatTime;
        return DT;
    }


    public String extendedTime(int i)
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        LocalDateTime d = LocalDateTime.now();
        String dateFormat = DateTimeFormatter.ofPattern("HH:mm:ss").format(d.plusMinutes(i));
        String DT=modifiedDate+"T"+dateFormat;
        return DT;
    }

    public String date()
    {
        Date date = new Date();
        String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        return modifiedDate+"T";
    }

    public String futureDate()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        //String modifiedDate= new SimpleDateFormat("yyyy-MM-dd").format(date);
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, 1);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne)+"T";
    }

    public String currentTime()
    {
        Date date=new Date();
        String modifiedDate=new SimpleDateFormat("HHmm").format(date);
        return modifiedDate;
    }

    public String futureTime()
    {
        DateFormat dateFormat=new SimpleDateFormat("HHmm");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MINUTE, 5);
        Date currentDatePlusOne = c.getTime();
        return dateFormat.format(currentDatePlusOne);
    }
}
