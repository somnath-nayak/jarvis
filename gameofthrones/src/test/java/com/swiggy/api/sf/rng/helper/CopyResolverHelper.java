package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.rng.constants.CopyConstants;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.CreateSuperCampaignV2;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTDEntryNewPojo;
import com.swiggy.api.sf.rng.pojo.freebie.RestIdList;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryEntry;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryTdBuilder;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CopyResolverHelper {

    TdCachingHelper tdCachingHelper = new TdCachingHelper();
    RngHelper rngHelper = new RngHelper();
    CreateSuperCampaignV2 createSuperCampaignV2 = new CreateSuperCampaignV2();
    SuperDbHelper superDbHelper = new SuperDbHelper();
    SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
    JsonHelper jsonHelper = new JsonHelper();
    SuperHelper superHelper = new SuperHelper();
    CopyResolverDBHelper copyResolverDBHelper = new CopyResolverDBHelper();
    CreateFreeDeliveryEntry createFreeDeliveryEntry = new CreateFreeDeliveryEntry();
    CreateFreeDeliveryTdBuilder createFreeDeliveryTdBuilder = new CreateFreeDeliveryTdBuilder();

    public String createFreedelCampaginWithCopyTrue(String minCartAmount, String level, List<String> restaurantId) throws IOException {
        CreateFreeDeliveryTdBuilder createFreeDeliveryTdBuilder = new CreateFreeDeliveryTdBuilder()
                .createFreedelWithCopy()
                .withCopyOverridden(true)
                .withShortDescription("Copy True")
                .description("Copy Over-ridden True")
                .header("CopyTrue")
                .ruleDiscount("FREE_DELIVERY", level, minCartAmount)
                .restaurantList(freeDeliveryRestaurantLists(restaurantId));
        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createFreeDeliveryTdBuilder.createFreeDeliveryEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return "success";
        }
        return CopyConstants.statusFail;
    }

    public String createFreedelCampaginWithCopyFalse(String minCartAmount, String level, List<String> restaurantId) throws IOException {
        CreateFreeDeliveryTdBuilder createFreeDeliveryTdBuilder = new CreateFreeDeliveryTdBuilder()
                .createFreedelWithCopy()
                .withCopyOverridden(false)
                .withShortDescription("Copy False")
                .description("Copy Over-ridden False")
                .header("CopyFalse")
                .ruleDiscount("FREE_DELIVERY", level, minCartAmount)
                .restaurantList(freeDeliveryRestaurantLists(restaurantId));
        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createFreeDeliveryTdBuilder.createFreeDeliveryEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return "success";
        }
        return CopyConstants.statusFail;
    }

    public List<FreeDeliveryRestaurantList> freeDeliveryRestaurantLists(List<String> restaurantId) {
        List<FreeDeliveryRestaurantList> freeDelRestList = new ArrayList<>();
        for (int i = 0; i < restaurantId.size(); i++) {
            freeDelRestList.add(new FreeDeliveryRestaurantList(restaurantId.get(i)));
        }
        return freeDelRestList;
    }

    public String createFlatTDWithCopyResolverTrue(Boolean isSuper, String discountLevel, String minCartAmount, List<String> allRestIds,
                                                   List<List<String>> categoriesId, List<String> subCategoriesId, List<String> menuId) throws IOException {
        RestaurantList restaurantList = new RestaurantList();
        CreateFlatTdBuilder createFlatTdBuilder = new CreateFlatTdBuilder()
                .createFlatTdWithCopy().campaign_type("Flat")
                .discountLevel(discountLevel)
                .ruleDiscount("Flat", discountLevel, "20", minCartAmount)
                .valid_from(Utility.getCurrentDate())
                .valid_till(Utility.getFutureDate())
                .header("CopyTrue")
                .description("Copy Over-ridden true")
                .withShortDescription("Copy True")
                .withCopyOverridden(true)
                .withSuper(isSuper)
                .restaurantList(restaurantListHelper(allRestIds, categoriesId, subCategoriesId, menuId))
                .withLevelPlaceholder("Automation-Default-CopyTrue");

        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createFlatTdBuilder.createFlatTdEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String id = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return id;
        }
        return CopyConstants.statusFail;
    }

    public String createFlatTDWithCopyResolverFalse(Boolean isSuper, String discountLevel, String minCartAmount, List<String> allRestIds,
                                                    List<List<String>> categoriesId, List<String> subCategoriesId, List<String> menuId) throws IOException {
        RestaurantList restaurantList = new RestaurantList();
        CreateFlatTdBuilder createFlatTdBuilder = new CreateFlatTdBuilder()
                .createFlatTdWithCopy().campaign_type("Flat")
                .discountLevel(discountLevel)
                .ruleDiscount("Flat", discountLevel, "20", minCartAmount)
                .valid_from(Utility.getCurrentDate())
                .valid_till(Utility.getFutureDate())
                .header("CopyFalse")
                .description("Copy Over-ridden false")
                .withShortDescription("Copy False")
                .withCopyOverridden(false)
                .withSuper(isSuper)
                .restaurantList(restaurantListHelper(allRestIds, categoriesId, subCategoriesId, menuId))
                .withLevelPlaceholder("Automation-Default-CopyFalse");

        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createFlatTdBuilder.createFlatTdEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String id = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return id;
        }
        return CopyConstants.statusFail;
    }


    public String createTDWithCopyResolverFalse(Boolean isSuper, String type, String discountLevel, String minCartAmount, String discountCap, List<String> allRestIds,
                                                List<List<String>> categoriesId, List<String> subCategoriesId, List<String> menuId) throws IOException {
        RestaurantList restaurantList = new RestaurantList();
        CreateTdBuilder createTdBuilder = new CreateTdBuilder()
                .createTDWithCopyOverridden()
                .campaign_type(type)
                .discountLevel(discountLevel)
                .ruleDiscount(type, discountLevel, "10", minCartAmount, discountCap)
                .valid_from(Utility.getCurrentDate())
                .valid_till(Utility.getFutureDate())
                .header("CopyFalse")
                .description("Copy Over-ridden false")
                .withShortDescription("Copy False")
                .withCopyOverridden(false)
                .withIsSuper(isSuper)
                .restaurantList(restaurantListHelper(allRestIds, categoriesId, subCategoriesId, menuId))
                .withLevelPlaceHolder("Automation-Default-CopyFalse");
        //.restaurantList(restaurantList.defaultData("99990", "NA", "NA", "NA"))

        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createTdBuilder.createTdEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return "success";
        }
        return CopyConstants.statusFail;
    }

    public String createTDWithCopyResolverTrue(Boolean isSuper, String type, String discountLevel, String minCartAmount, String discountCap, List<String> allRestIds,
                                               List<List<String>> categoriesId, List<String> subCategoriesId, List<String> menuId) throws IOException {
        RestaurantList restaurantList = new RestaurantList();
        CreateTdBuilder createTdBuilder = new CreateTdBuilder()
                .createTDWithCopyOverridden()
                .campaign_type(type)
                .discountLevel(discountLevel)
                .ruleDiscount(type, discountLevel, "10", minCartAmount, discountCap)
                .valid_from(Utility.getCurrentDate())
                .valid_till(Utility.getFutureDate())
                .header("CopyTrue")
                .description("Copy Over-ridden true")
                .withShortDescription("Copy True")
                .withCopyOverridden(true)
                .withIsSuper(isSuper)
                .restaurantList(restaurantListHelper(allRestIds, categoriesId, subCategoriesId, menuId))
                .withLevelPlaceHolder("Automation-Default-CopyTrue");
        //.restaurantList(restaurantList.defaultData("99990", "NA", "NA", "NA"))

        Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(createTdBuilder.createTdEntry));
        String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
        String id = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
        String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
        if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
            return id;
        }
        return CopyConstants.statusFail;
    }

    public String createSuperFreeDelTDForResolver(String benefitType, Boolean copyOverridden, List<String> restID, String minCartAmount) throws IOException {
        //List<Integer> restID1 = new ArrayList<Integer>();
        //restID.add(0, "-1");
        String id = "";
        List<Integer> restIdList = new ArrayList<>();
        if (benefitType.equalsIgnoreCase("FREE_DELIVERY")) {
            id = superDbHelper.getActiveBenefitsFromDb(benefitType);
            if (id.equalsIgnoreCase("No active Free Delivery benefit found in DB")) {
                String benefitId = superHelper.createSuperBenefit(benefitType);
                if (!benefitId.equalsIgnoreCase("fail")) {
                    id = "" + benefitId;
                }
            }
        }
        for (int i = 0; i < restID.size(); i++) {
            restIdList.add(i, Integer.parseInt(restID.get(0)));
        }

        //restIdList.add(0,-1);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        String campId = "";
        if (copyOverridden.toString().equalsIgnoreCase("true")) {
            CreateSuperCampaignV2 className = createSuperCampaignV2.createFreeDelTdWithCopy(copyOverridden, id, restIdList, minCartAmount).withHeader("CopyTrue")
                    .withShortDescription("Copy True").withDescription("Copy Over-ridden true");
            Processor tdResponse = superMultiTDHelper.createSuperCampaign(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("success")) {
                return campId;
            }
        } else {
            CreateSuperCampaignV2 className = createSuperCampaignV2.createFreeDelTdWithCopy(copyOverridden, id, restIdList, minCartAmount).withHeader("CopyFalse")
                    .withShortDescription("Copy False").withDescription("Copy Over-ridden False");
            Processor tdResponse = superMultiTDHelper.createSuperCampaign(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("success")) {
                return campId;
            }
        }
        return CopyConstants.statusFail;
    }


    public String createSuperFreebieTDForResolver(String benefitType, Boolean copyOverridden, List<String> restID, String minCartAmount, String itemId) throws IOException {
        //List<Integer> restID1 = new ArrayList<Integer>();
        //restID.add(0, "-1");
        String id = "";
        CreateFreebieTDEntryNewPojo createFreebieTDEntryNewPojo = new CreateFreebieTDEntryNewPojo();
        List<Integer> restIdList = new ArrayList<>();
        if (benefitType.equalsIgnoreCase("Freebie")) {
            id = superDbHelper.getActiveFreebieBenefitsFromDb(benefitType);
            if (id.equalsIgnoreCase("No active Freebie benefit found in DB")) {
                String benefitId = superHelper.createSuperBenefit(benefitType);
                if (!benefitId.equalsIgnoreCase("fail")) {
                    id = "" + benefitId;
                }
            }
        }
        for (int i = 0; i < restID.size(); i++) {
            restIdList.add(i, Integer.parseInt(restID.get(0)));
        }

        //restIdList.add(0,-1);
        copyResolverDBHelper.disableFreebieSuperCampaign(restID.get(0));
        String campId = "";
        if (copyOverridden.toString().equalsIgnoreCase("true")) {
            CreateFreebieTDEntryNewPojo className = createFreebieTDEntryNewPojo.createFreebieTdWithCopyOverridden(createFreebieRestIdList(restID),
                    copyOverridden, true, minCartAmount, itemId, "Automation-Default-CopyTrue", "CopyTrue", "Copy Over-ridden true", "Copy True");
            Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
                return campId;
            }
        } else {
            CreateFreebieTDEntryNewPojo className = createFreebieTDEntryNewPojo.createFreebieTdWithCopyOverridden(createFreebieRestIdList(restID),
                    copyOverridden, true, minCartAmount, itemId, "Automation-Default-CopyFalse", "CopyFalse", "Copy Over-ridden false", "Copy false");
            Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
                return campId;
            }
        }
        return CopyConstants.statusFail;
    }

    public String createFreebieWithCopy(List<String> allRestIds, Boolean copyOverridden, Boolean isSuper,
                                        String minCartAmount, String itemId) throws IOException {
        CreateFreebieTDEntryNewPojo createFreebieTDEntryNewPojo = new CreateFreebieTDEntryNewPojo();
        String campId = "";
        if (copyOverridden) {

            CreateFreebieTDEntryNewPojo className = createFreebieTDEntryNewPojo.createFreebieTdWithCopyOverridden(createFreebieRestIdList(allRestIds),
                    copyOverridden, isSuper, minCartAmount, itemId, "Automation-Default-CopyTrue", "CopyTrue", "Copy Over-ridden true", "Copy True");
            Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
                return campId;
            }
        } else {
            CreateFreebieTDEntryNewPojo className = createFreebieTDEntryNewPojo.createFreebieTdWithCopyOverridden(createFreebieRestIdList(allRestIds),
                    copyOverridden, isSuper, minCartAmount, itemId, "Automation-Default-CopyFalse", "CopyFalse", "Copy Over-ridden false", "Copy false");
            Processor tdResponse = rngHelper.createTD(jsonHelper.getObjectToJSON(className));
            String statusMessage = tdResponse.ResponseValidator.GetNodeValue("$.statusMessage");
            String statusCode = Integer.toString(tdResponse.ResponseValidator.GetNodeValueAsInt("$.statusCode"));
            campId = tdResponse.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data");
            if (statusCode.equals("0") && statusMessage.equalsIgnoreCase("done")) {
                return campId;
            }
        }
        return CopyConstants.statusFail;
    }

    public List<RestaurantList> restaurantListHelper(List<String> allRestIds, List<List<String>> categoriesIds, List<String> subCategorieIds, List<String> menuIds) {

        List<String> restIds = new ArrayList<>();
        restIds.add(allRestIds.get(0));
        List<Menu> menu = new ArrayList<>();
        menu.add(0, new Menu(menuIds.get(0), "testMenu"));
        List<SubCategory> subCategories = new ArrayList<>();
        subCategories.add(0, new SubCategory(subCategorieIds.get(0), "testSubCategory", menu));
        List<List<Category>> restaurantCategoryList = new ArrayList<>();
        List<Category> categoryList = new ArrayList<>();
        categoryList.add(0, new Category(categoriesIds.get(0).get(0), "testCategory", subCategories));
        restaurantCategoryList.add(categoryList);
        List<RestaurantList> restaurantList = populateRestaurantList(restIds, restaurantCategoryList);
        return restaurantList;

    }

    public List<RestaurantList> populateRestaurantList(List<String> restIds, List<List<Category>> categoryList) {
        List<RestaurantList> restList = new ArrayList<>();
        for (int i = 0; i < restIds.size(); i++) {
            RestaurantList restaurantList = new RestaurantList(restIds.get(i), "testRest", categoryList.get(i));
            restList.add(restaurantList);
        }

        return restList;
    }

    public List<RestIdList> createFreebieRestIdList(List<String> restId) {
        RestIdList allRestId = new RestIdList();
        List<RestIdList> listRestId = new ArrayList<>();
        List<RestIdList> freebieRestId = new ArrayList<>();
        for (int i = 0; i < restId.size(); i++) {
            listRestId.add(i, allRestId.withId(restId.get(i)));
        }
        return listRestId;
    }

    public void deleteDataFromCache(String restaurantId) {
        tdCachingHelper.deleteDataFromCacheForRestaurant(restaurantId);
    }

    public String getBenefitId(String benefitType) {
        String id = superDbHelper.getActiveBenefitsFromDb(benefitType);

        if (id.equalsIgnoreCase("No active Free Delivery benefit found in DB")) {
            return null;
        }
        return id;

    }
}
