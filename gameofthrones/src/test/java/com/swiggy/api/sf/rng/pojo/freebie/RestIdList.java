package com.swiggy.api.sf.rng.pojo.freebie;

import org.codehaus.jackson.annotate.JsonProperty;

public class RestIdList {

    @JsonProperty("id")
    private String id;

    /**
     * No args constructor for use in serialization
     *
     */
    public RestIdList() {
    }

    /**
     *
     * @param id
     */
    public RestIdList(String id) {
        super();
        this.id = id;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    public RestIdList withId(String id) {
        this.id = id;
        return this;
    }

}