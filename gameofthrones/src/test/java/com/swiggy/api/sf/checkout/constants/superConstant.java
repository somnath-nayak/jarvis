package com.swiggy.api.sf.checkout.constants;

public interface superConstant {
    String NUDGE_CTA_MESSAGE="GET SUPER NOW";
    String SUB_QUANTITY="1";
    int INT_ZERO=0;
    String EXPECTED_FALSE_FLAG="false";
    String EXPECTED_TRUE_FLAG="true";
    String ZERO_AS_STRING="0.0";
    String CART_TYPE_REGULAR="REGULAR";
    String CART_TYPE_SUPER="SUPER";
    String PAYMENT_METHOD_CASH="cash";
    String DEF_STRING="Test";
    String MOBIKWIK_SSO="Mobikwik-SSO";
    String DISCOUNT_FREE_DELIVERY="FREE_DELIVERY";
    String REST_ID="8725";
    String ORDER_TYPE="SUBSCRIPTION";
    String CANCEL_STATUS="cancelled";
}