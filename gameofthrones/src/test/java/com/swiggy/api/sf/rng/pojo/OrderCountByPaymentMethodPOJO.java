package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.helper.Utility;
import io.gatling.commons.stats.assertion.In;

public class OrderCountByPaymentMethodPOJO {

    private OrderCountByCartType orderCountByCartType;
    private UserOrderCountByPaymentMethod orderCountByPaymentMethod;
    private Integer userId;
    private Integer orderCount;

    public OrderCountByCartType getOrderCountByCartType() {
        return orderCountByCartType;
    }

    public void setOrderCountByCartType(OrderCountByCartType orderCountByCartType) {
        this.orderCountByCartType = orderCountByCartType;
    }

    public OrderCountByPaymentMethodPOJO withOrderCountByCartType(String cartType, long orderCount) {
        this.orderCountByCartType = orderCountByCartType(cartType, orderCount);
        return this;
    }

    public UserOrderCountByPaymentMethod getOrderCountByPaymentMethod() {
        return orderCountByPaymentMethod;
    }

    public void setOrderCountByPaymentMethod(UserOrderCountByPaymentMethod orderCountByPaymentMethod) {
        this.orderCountByPaymentMethod = orderCountByPaymentMethod;
    }

    public OrderCountByPaymentMethodPOJO withOrderCountByPaymentMethod(String paymentMethod, long orderCount) {
        this.orderCountByPaymentMethod = countByPaymentMethod(paymentMethod, orderCount);
        return this;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }


    public OrderCountByCartType orderCountByCartType(String cartType, long orderCount) {
        OrderCountByCartType orderCountByCartType = new OrderCountByCartType();
        if (cartType.equalsIgnoreCase("Pop")) {
            orderCountByCartType.withPop(orderCount);
        }
        if (cartType.equalsIgnoreCase("Pre")) {
            orderCountByCartType.withPre(orderCount);
        }
        if (cartType.equalsIgnoreCase("Cafe")) {
            orderCountByCartType.withCafe(orderCount);
        }
        if (cartType.equalsIgnoreCase("Promo")) {
            orderCountByCartType.withPromo(orderCount);
        }
        if (cartType.equalsIgnoreCase("SwiggyExpress")) {
            orderCountByCartType.withSwiggyExpress(orderCount);
        }
        if (cartType.equalsIgnoreCase("Group")) {
            orderCountByCartType.withGroup(orderCount);
        }

        return orderCountByCartType;
    }

    public UserOrderCountByPaymentMethod countByPaymentMethod(String paymentMethod, long orderCount) {
        UserOrderCountByPaymentMethod userOrderCountByPaymentMethod = new UserOrderCountByPaymentMethod();

        if (paymentMethod.equalsIgnoreCase("PayTM")) {
            userOrderCountByPaymentMethod.withPayTM(orderCount);
        }
        if (paymentMethod.equalsIgnoreCase("Sodexo")) {
            userOrderCountByPaymentMethod.withSodexo(orderCount);
        }
        if (paymentMethod.equalsIgnoreCase("creditDebitCard")) {
            userOrderCountByPaymentMethod.withCreditDebitCard(orderCount);
        }
        if (paymentMethod.equalsIgnoreCase("amazonPay")) {
            userOrderCountByPaymentMethod.withAmazonPay(orderCount);
        }
        if (paymentMethod.equalsIgnoreCase("phonePe")) {
            userOrderCountByPaymentMethod.withPhonePe(orderCount);
        }
        if (paymentMethod.equalsIgnoreCase("payLaterLazypay")) {
            userOrderCountByPaymentMethod.withPayLaterLazypay(orderCount);
        }

        return userOrderCountByPaymentMethod;
    }

    public OrderCountByPaymentMethodPOJO setDefaultData(){
        //this.orderCountByCartType = orderCountByCartType("POP",1);
       // this.orderCountByPaymentMethod = countByPaymentMethod("amazonPay",1);
        this.userId = Utility.getRandom(0,1000000);
        this.orderCount = 1;
        return this;
    }


}