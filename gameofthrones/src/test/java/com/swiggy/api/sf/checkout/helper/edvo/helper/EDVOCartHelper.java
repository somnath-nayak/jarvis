package com.swiggy.api.sf.checkout.helper.edvo.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cancelOrderRequest.CancelOrderRequest;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create.*;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response.CartV2Response;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.editOrderRequest.EditOrderRequest;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.getAllOrders.GetAllOrders;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.OrderResponse;
import com.swiggy.api.sf.checkout.helper.edvo.pojo.placeOrder.PlaceOrderRequest;
import com.swiggy.api.sf.checkout.helper.edvo.util.Utility;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EDVOCartHelper {
    static boolean alreadyLogin = false;
    static int countLogin = 0;
    static HashMap<String, String> headers = new HashMap<>();
    Initialize gameofthrones = new Initialize();
    Cart cart = null;
    SANDHelper sandHelper = new SANDHelper();
    EditOrderRequest cloneOrderRequest;
    public static HashMap<String, Processor> menuResponses = new HashMap<>();
    public static HashMap<String, Processor> loginResponse = new HashMap<>();


    public Cart getCartPayload(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        List<MealItem> mealItems = new ArrayList<>();
        Processor menuV4 = getMenuV4(restaurantId);
        CartBuilder cartBuilder = new CartBuilder()
                .cartItems(getCartItems(restaurantId, 1, isAddonRequired, isCartItemRequired))
                .mealItems(getMealItems(restaurantId, mealId, 1, isAddonRequired, isMealItemRequired))
                .cartType("REGULAR")
                .restaurantId(restaurantId);
        if(menuV4.ResponseValidator.GetNodeValue("$.data.name").contains("Domino")) {
            cartBuilder.address(EDVOConstants.ADDRESS_ID);
        }
        return cartBuilder.build();
    }

    public Cart getCartPayload(String restaurantId,
                            boolean isCartItemRequired,
                            int cartItemQuantity,
                            boolean isAddonRequiredForCartItems,
                            boolean isMealItemRequired,
                            int mealItemQuantity,
                            boolean isAddonRequiredForMealItems,
                            String[] mealIds,
                            String cartType){
        List<MealItem> mealItems = new ArrayList();
        Processor menuV4 = getMenuV4(restaurantId);
        CartBuilder cartBuilder = new CartBuilder()
                .cartItems(getCartItems(restaurantId, cartItemQuantity, isAddonRequiredForCartItems, isCartItemRequired))
                .mealItems(getMealItems(restaurantId, mealIds, mealItemQuantity, isAddonRequiredForMealItems,isMealItemRequired))
                .cartType(cartType)
                .restaurantId(restaurantId);

//        ToDo - Address Impl need to be created for restaurants like Domino's where address is mandatory

        return cartBuilder.build();
    }

    public Processor createCartV2(String payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    /**
     *
     * @param mealId
     * @param restaurantId
     * @return
     */
    public Processor createEDVOCartForMeals(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        cart = getCartPayload(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        String payload = null;
        payload = Utility.jsonEncode(cart);
        return createCartV2(payload);
    }

    public Processor createCartV2(String payload){
        HashMap<String, String> requestHeader = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "createcartv2edvo", gameofthrones);
        return new Processor(gots, requestHeader, new String[]{payload}, null);
    }

    public List<CartItem> getCartItems(String restaurantId, Integer quantity, boolean isAddonRequired, boolean isCartItemRequired){
        List<CartItem> cartItems = new ArrayList<>();
        if(!isCartItemRequired){
            return cartItems;
        }
        Processor processor = getMenuV4(restaurantId);
        List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.inStock");
        List<Integer> itemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");

        for(int i=0;i<inStocks.size();i++) {
            CartItem cartItem = null;
            if (inStocks.get(i).intValue() == 1) {
                if (isAddonRequired && Utility.doesJsonPathExist(processor, "data.menu.items." + itemIds.get(i).intValue() + ".addons")) {
                        cartItem = new CartItemBuilder()
                                .menuItemId(String.valueOf(itemIds.get(i).intValue()))
                                .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
                                .quantity(quantity)
                                .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
                                .build();
                        cartItems.add(cartItem);
                        break;
                } else {
                    cartItem = new CartItemBuilder()
                            .menuItemId(String.valueOf(itemIds.get(i).intValue()))
                            .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
                            .quantity(quantity)
                            .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
                            .build();
                    cartItems.add(cartItem);
                    break;
                }
            }
        }
        return cartItems;
    }

    public List<Addon> getAddonsForCartMenuItem(Processor processor, Integer itemId, boolean isAddonRequired){
        List<Addon> addons = new ArrayList<>();
        if(!isAddonRequired){
            return addons;
        }
        if (Utility.doesJsonPathExist(processor, "$.data.menu.items." + itemId + ".addons.[*].group_id")) {
            List<Integer> addonGroupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                    "$.data.menu.items." + itemId + ".addons.[*].group_id");
            for (int i = 0; i < addonGroupIds.size(); i++) {
                List<Integer> choiceIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.menu.items." + itemId + ".addons.[" + i + "].choices.[*].id");
                List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.menu.items." + itemId + ".addons.[" + i + "].choices.[*].inStock");
                for (int j = 0; j < choiceIds.size(); j++) {
                    System.out.println(i + "   " + j);
                    if (inStocks.get(j).intValue() > 0) {
                        Addon addon = new AddonBuilder()
                                .groupId(String.valueOf(addonGroupIds.get(i).intValue()))
                                .choiceId(String.valueOf(choiceIds.get(j).intValue()))
                                .build();
                        addons.add(addon);
                        break;
                    }
                }
            }
        }
        return addons;
    }

    /**
     *
     * @param mobile
     * @param password
     * @return
     */

    public HashMap<String,String> getHeaders(String mobile, String password){
        Processor processor = null;
        if(loginResponse.containsKey(mobile)){
            processor = loginResponse.get(mobile);
        }else {
            processor = sandHelper.login(new String[]{mobile, password});
            smokeCheck(0, "done successfully", processor);
            loginResponse.put(mobile, processor);
        }
        String token = processor.ResponseValidator.GetNodeValue("$.data.token");
        String tid = processor.ResponseValidator.GetNodeValue("$.tid");

        headers.put("tid", tid);
        headers.put("token", token);
        headers.put("Authorization", EDVOConstants.AUTHORIZATION);
        headers.put("Content-Type", "application/json");
        headers.put("version-code", EDVOConstants.VERSION_CODE);
        headers.put("User-Agent", EDVOConstants.USER_AGENT);
        headers.put("swuid", EDVOConstants.SWUID);
        return headers;
    }

    /**
     *
     * @param
     * @param mealId
     * @param quantity
     * @return
     */
    public List<MealItem> getMealItems(String restaurantId, String[] mealId, int quantity, boolean isAddonRequired, boolean isMealItemRequired){
        List<MealItem> mealItems = new ArrayList<>();
        if(!isMealItemRequired){
            return mealItems;
        }
        for(int i=0;i<mealId.length;i++) {
            Processor getMeals = getMeals(mealId[i], restaurantId);
            smokeCheck(0, "done successfully", getMeals, "Get Meals API Failed");
            MealItem mealItem = new MealItemBuilder()
                    .mealId(Integer.valueOf(mealId[i]))
                    .quantity(quantity)
                    .groups(getGroups(getMeals, isAddonRequired))
                    .build();
            mealItems.add(mealItem);
        }
        return mealItems;
    }

    /**
     *
     * @param processor
     * @return
     */
    public List<Group> getGroups(Processor processor, boolean isAddonRequired){
        List<Group> groups = new ArrayList<>();
        List<String> screens = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.screens.*");
        List<Integer> groupIds = new ArrayList<>();
        Integer groupId = null;
        for(int i=0;i<screens.size();i++) {
            //int i is the screen position in getMeals response, which becomes the group position in mealitems
            groupId = JsonPath.read(processor.ResponseValidator.GetBodyAsText()
                    , "$.data.screens.[" + i + "].group.id");
            Group group = new GroupBuilder()
                    .groupId(groupId)
                    .items(getItems(processor, i, isAddonRequired))
                    .build();
            groups.add(group);
        }
        return groups;
    }

    /**
     * Get Items for meal cart
     * @param processor
     * @param screenPosition
     * @return
     */
    public List<Item> getItems(Processor processor, int screenPosition, boolean isAddonRequired){
        List<Item> items = new ArrayList<>();
        List<String> itemList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.screens.["+ screenPosition +"].group.items.*");
        List<Integer> itemsInStockList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.screens.["+ screenPosition +"].group.items.*.inStock");
        List<Integer> itemIdList = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.screens.["+ screenPosition +"].group.items.*.id");

        for(int i=0;i<itemList.size();i++){
            if(Integer.valueOf(itemsInStockList.get(i)).intValue()>0){
                Item item = new ItemBuilder()
                        .menuItemId(itemIdList.get(i))
                        .quantity(1)
                        .variants(getVariants(processor, screenPosition, i, 0))
                        .addons(getAddons(processor, screenPosition, i, 0, isAddonRequired))
                        .build();
                items.add(item);
                return items;
            }
        }
        Assert.fail("All items are out of stock for Group ID(Screen Position) " + screenPosition);
        return null;
    }

    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @return
     */
    public List<Addon> getAddons(Processor processor, int screenPosition, int itemPosition, int variationPosition, boolean isAddonRequired){
        List<Addon> addons = new ArrayList<>();
        try {
            if (isAddonRequired) {
                Addon addon = new AddonBuilder()
                        .groupId(getGroupId(processor, screenPosition, itemPosition, variationPosition))
                        .choiceId(getChoiceId(processor, screenPosition, itemPosition, variationPosition))
                        .build();
                addons.add(addon);
            }
        }catch (PathNotFoundException e){}
        return addons;

    }

    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @return
     */
    public String getGroupId(Processor processor, int screenPosition, int itemPosition, int variationPosition){
        return String.valueOf(processor.ResponseValidator.GetNodeValueAsInt("$.data.screens.["
                + screenPosition + "].group.items[" + itemPosition + "].addons[0].group_id"));
    }

    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @return
     */
    public String getChoiceId(Processor processor, int screenPosition, int itemPosition, int variationPosition){
        List<Integer> choicesCount = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.screens.[" + screenPosition + "].group.items[" + itemPosition
                        + "].addons[0].choices[*].inStock");
        List<Integer> choiceIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.screens.[" + screenPosition + "].group.items[" + itemPosition
                        + "].addons[0].choices[*].id");
        for(int i=0;i<choicesCount.size();i++){
            if(choicesCount.get(i).intValue()>0){
                return String.valueOf(choiceIds.get(i));
            }
        }
        Assert.fail("All choice Ids are'out of stock");
        return null;

    }

    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @return
     */
    public List<Variant> getVariants(Processor processor, int screenPosition, int itemPosition, int variationPosition){
        List<Variant> variants = new ArrayList<>();
        try{
            List<String> variantsCount = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                    "$.data.screens.[" + screenPosition + "].group.items[" + itemPosition
                            + "].variantsV2.pricing_models[" + variationPosition + "].variations.*");
            for (int i = 0; i < variantsCount.size(); i++) {
                Variant variant = new VariantBuilder()
                        .groupId(Integer.parseInt(getGroupId(processor, screenPosition, itemPosition, variationPosition, i)))
                        .variationId(Integer.parseInt(getVariationId(processor, screenPosition, itemPosition, variationPosition, i)))
                        .build();
                variants.add(variant);
            }
        }catch (PathNotFoundException e){}

        try{
            if(!Utility.doesJsonPathExist(processor, "$.data.screens[" + screenPosition + "].group.items[" + itemPosition + "].variants_new")){
                return variants;
            }
            Variant variant = null;
            List<String> groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                    "$.data.screens[" + screenPosition + "].group.items[" + itemPosition + "].variants_new.variant_groups.[*].group_id");

            for(int j=0; j<groupIds.size();j++) {
                List<String> variationIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.screens[" + screenPosition + "].group.items[" + itemPosition + "].variants_new.variant_groups.[" + j + "].variations.[*].id");
                variant = new VariantBuilder()
                        .variationId(Integer.parseInt(variationIds.get(0)))
                        .groupId(Integer.parseInt(groupIds.get(j)))
                        .build();
                variants.add(variant);
            }
        }catch (PathNotFoundException e){}
        return  variants;
    }

    public List<Variant> getVariantsForNormalItem(Processor processor, int itemPosition, int variationPosition, int itemId){
        List<Variant> variants = new ArrayList<>();
        if(!processor.ResponseValidator.GetNodeValue("$.data.name").contains("Domino")){
            if(!Utility.doesJsonPathExist(processor, "data.menu.items." + itemId + ".variants_new.variant_groups")){
                return variants;
            }
            Variant variant = null;
            List<String> groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                    "$.data.menu.items." + itemId + ".variants_new.variant_groups.[*].group_id");

            for(int j=0; j<groupIds.size();j++) {
                List<String> variationIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.menu.items." + itemId + ".variants_new.variant_groups.[" + j + "].variations.[*].id");
                variant = new VariantBuilder()
                        .variationId(Integer.parseInt(variationIds.get(0)))
                        .groupId(Integer.parseInt(groupIds.get(j)))
                        .build();
                variants.add(variant);
            }
        }
        else {
            if (Utility.doesJsonPathExist(processor, "data.menu.items." + itemId + ".variantsV2")){
                Variant variant = null;
                List<String> groupIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.menu.items." + itemId + ".variantsV2.pricing_models.[0].variations.[*].group_id");
                List<String> variationIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                        "$.data.menu.items." + itemId + ".variantsV2.pricing_models.[0].variations.[*].variation_id");

                for(int i=0;i<groupIds.size();i++){
                    variant = new VariantBuilder()
                            .variationId(Integer.parseInt(variationIds.get(i)))
                            .groupId(Integer.parseInt(groupIds.get(i)))
                            .build();
                    variants.add(variant);
                }
            }
        }
        return variants;
    }


    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @param groupIdPosition
     * @return
     */
    public String getGroupId(Processor processor,  int screenPosition, int itemPosition,
                             int variationPosition, int groupIdPosition){
        return processor.ResponseValidator.GetNodeValue("$.data.screens.[" + screenPosition
                + "].group.items[" + itemPosition + "].variantsV2.pricing_models[" + variationPosition
                + "].variations.[" + groupIdPosition + "].group_id");
    }

    /**
     *
     * @param processor
     * @param screenPosition
     * @param itemPosition
     * @param variationPosition
     * @param variationIdPosition
     * @return
     */
    public String getVariationId(Processor processor,  int screenPosition, int itemPosition,
                                 int variationPosition, int variationIdPosition){
        return processor.ResponseValidator.GetNodeValue("$.data.screens.[" + screenPosition
                + "].group.items[" + itemPosition + "].variantsV2.pricing_models[" + variationPosition
                + "].variations.[" + variationIdPosition + "].variation_id");

    }

    /**
     *
     * @param mealId
     * @param restaurantId
     * @return
     */
    public Processor getMeals(String mealId, String restaurantId){
        String[] queryParam = new String[]{mealId, restaurantId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "getmeals", gameofthrones);
        return new Processor(gots, requestHeader, null, queryParam);
    }

    /**
     *
     * @param restaurantId
     * @return
     */
    public Processor getMenuv4(String restaurantId){
        String[] queryParam = {restaurantId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("checkout", "menuv4withoutlatlng", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    public Processor getMenuV4(String restaurantId){
        if(menuResponses.containsKey(restaurantId)){
            return menuResponses.get(restaurantId);
        }else {
            Processor menu = getMenuv4(restaurantId);
            if (menu.ResponseValidator.GetNodeValueAsInt("$.statusCode") == 0){
                menuResponses.put(restaurantId, menu);
                return menu;
            }else {
                Reporter.log("[Menu V4 Call Failed]", true);
                Reporter.log("[Menu V4 Response] \n ==> " + menu.ResponseValidator.GetBodyAsText(), true);
            }
        }
        return null;
    }

    /**
     * Returns MenuV4 response for particular Lat and Lng
     * @param restaurantId
     * @param lat
     * @param lng
     * @return
     */
    public Processor getMenuV4WithLatLng(String restaurantId, String lat, String lng){
        String[] queryParam = {restaurantId, lat, lng};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("sand", "menuv4", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    /**
     *
     * @param restaurantId
     * @return
     */
    public List<Integer> getMealIdsFromRestaurant(String restaurantId){
        Processor processor = getMenuV4(restaurantId);
        List<String> allItemTypes = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.menu.widgets..entities..type");
        System.out.println("Count of All Item Types : " + allItemTypes.size());
        List<Integer> allIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.menu.widgets..entities..id");
        System.out.println("Count of All Item Ids : " + allItemTypes.size());
        List<Integer> mealIds = new ArrayList<>();
        int count = 0;

        for (int i = 0; i<allItemTypes.size(); i++) {
            if(allItemTypes.get(i).equals("meal")){
                mealIds.add(count,allIds.get(i));
                count++;
                System.out.println("Count : " + mealIds.get(count));
            }
        }
        return mealIds;
    }

    public Processor flushCart(String mobile, String password){
        HashMap<String, String> headers = getHeaders(mobile, password);
        return flushCart(headers);
    }

    public Processor flushCart(HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "deletecart", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor getCartMinimal(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        cart = getCartPayload(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        JsonHelper jh = new JsonHelper();
        String payload = null;
        try {
            payload = jh.getObjectToJSON(cart);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return createMinimalCart(payload);
    }

    public Processor createMinimalCart(String payload){
        HashMap<String, String> requestHeader = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "edvominimalcartv2", gameofthrones);
        return new Processor(gots, requestHeader, new String[]{payload}, null);
    }

    public Processor createMinimalCart(String payload, HashMap<String, String> requestHeader){
        GameOfThronesService gots = new GameOfThronesService("checkout", "edvominimalcartv2", gameofthrones);
        return new Processor(gots, requestHeader, new String[]{payload}, null);
    }

    public Processor getCart(HashMap<String, String> header){
        GameOfThronesService service = new GameOfThronesService("checkout", "getcart", gameofthrones);
        Processor processor = new Processor(service, header, null, null);
        return processor;
    }

    public Processor getCart(){
        HashMap<String, String> rheaders = new HashMap<>();
        if(alreadyLogin) {
            rheaders = headers;
        }else {
            rheaders = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        }
        GameOfThronesService service = new GameOfThronesService("checkout", "getcart", gameofthrones);
        HashMap<String, String> headers = new HashMap<String, String>();
        Processor processor = new Processor(service, rheaders, null, null);
        return processor;
    }


    public Cart getCartObject(){
        return cart;
    }

    public Processor checkTotalCart(String payload){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "checktotalv2edvo", gameofthrones);
        return new Processor(gots, headers, new String[]{payload}, null);
    }

    public Processor getCheckTotalCart(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        cart = getCartPayload(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        JsonHelper jh = new JsonHelper();
        String payload = null;
        try {
            payload = jh.getObjectToJSON(cart);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return checkTotalCart(payload);
    }

    public Processor placeOrder(String addressId, String paymentMethod, String orderComments){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        return placeOrder(addressId, paymentMethod, orderComments, headers);
    }

    public Processor placeOrder(String addressId, String paymentMethod, String orderComments, HashMap<String, String> headers){
        if(addressId == null || addressId.equals("")){
            addressId = String.valueOf(EDVOConstants.ADDRESS_ID);
        }
        if(paymentMethod == null || paymentMethod.equals("")){
            paymentMethod = EDVOConstants.PAYMENT_METHOD_CASH;
        }
        if(orderComments == null || orderComments.equals("")){
            paymentMethod = EDVOConstants.ORDER_COMMENTS;
        }

        PlaceOrderRequest placeOrderRequest = new PlaceOrderRequest();
        placeOrderRequest.setAddressId(Integer.parseInt(addressId));
        placeOrderRequest.setPaymentMenthod(paymentMethod);
        placeOrderRequest.setOrderComments(orderComments);
        String payload = Utility.jsonEncode(placeOrderRequest);
        GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
        return new Processor(service, headers, new String[] {payload});
    }

    public Processor getPlaceOrder(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        Processor cartV2 = createEDVOCartForMeals(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        smokeCheck(0, "CART_UPDATED_SUCCESSFULLY", cartV2, "Create Cart Failed...!!");
        CartV2Response cartV2Response = Utility.jsonDecode(cartV2.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        String addressId = getValidAddress(cartV2Response);
        if(addressId==null){
            addressId = addNewAddressFromCartV2Response(cartV2Response);
        }
        return placeOrder(addressId, EDVOConstants.PAYMENT_METHOD_CASH, EDVOConstants.ORDER_COMMENTS);
    }

    public String getValidAddress(CartV2Response cartV2Response){
        if(cartV2Response.getData().getAddresses()==null){
            return null;
        }
        for(int i=0; i<cartV2Response.getData().getAddresses().size(); i++){
            if(cartV2Response.getData().getAddresses().get(i).getDeliveryValid()==1){
                return cartV2Response.getData().getAddresses().get(i).getId();
            }
        }
        return null;
    }

    public String addNewAddressFromCartV2Response(CartV2Response cartV2Response){
        HashMap<String, String> addressData = new HashMap<>();
        addressData.put("name", EDVOConstants.ADDRESS_NAME);
        addressData.put("mobile", EDVOConstants.ADDRESS_MOBILE);
        addressData.put("address", EDVOConstants.ADDRESS_ADDRESS);
        addressData.put("landmark", EDVOConstants.ADDRESS_LANDMARK);
        addressData.put("area", EDVOConstants.ADDRESS_AREA);
        addressData.put("lat", cartV2Response.getData().getRestaurantDetails().getLat());
        addressData.put("lng", cartV2Response.getData().getRestaurantDetails().getLng());
        addressData.put("flat_no", EDVOConstants.ADDRESS_FLAT_NO);
        addressData.put("city", EDVOConstants.ADDRESS_CITY);
        addressData.put("annotation", EDVOConstants.ADDRESS_ANNOTATION);
        HashMap<String, String>  headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        String tid = headers.get("tid");
        String token = headers.get("token");
        Processor processor = CheckoutHelper.addNewAddress(addressData, tid, token);
        int addressId = processor.ResponseValidator.GetNodeValueAsInt("$.data.address_id");
        return String.valueOf(addressId);
    }

    public String[] getEditOrderPayload(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        Processor cartV2 = createEDVOCartForMeals(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        CartV2Response cartV2Response = Utility.jsonDecode(cartV2.ResponseValidator.GetBodyAsText(), CartV2Response.class);
        String addressId = getValidAddress(cartV2Response);
        if(addressId==null){
            addressId = addNewAddressFromCartV2Response(cartV2Response);
        }
        Processor placeOrder = placeOrder(addressId, EDVOConstants.PAYMENT_METHOD_CASH, EDVOConstants.ORDER_COMMENTS);
        OrderResponse orderResponse = Utility.jsonDecode(placeOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        cart = getCartObject();
        Cart cart1 = new CartBuilder().restaurantId(cart.getRestaurantId()).build();

        if(isCartItemRequired){
            List<CartItem> cartItems =new ArrayList<>();
            for(int i=0; i<cart.getCartItems().size(); i++){
                CartItem cartItem = cart.getCartItems().get(i);
                cartItem.setQuantity(cartItem.getQuantity() + 1);
                cartItems.add(cartItem);
            }
            cart1.setCartItems(cartItems);
        }

        if(isMealItemRequired){
            List<MealItem> mealItems = new ArrayList<>();
            for(int i=0; i<cart.getMealItems().size(); i++){
                MealItem mealItem = cart.getMealItems().get(i);
                mealItem.setQuantity(mealItem.getQuantity() + 1);
                mealItems.add(mealItem);
            }
            cart1.setMealItems(mealItems);
        }

        this.cart.setCartItems(cart1.getCartItems());
        this.cart.setMealItems(cart1.getMealItems());

        EditOrderRequest editOrderRequest = new EditOrderRequest();
        editOrderRequest.setCartItems(cart1.getCartItems());
        editOrderRequest.setMealItems(cart1.getMealItems());
        editOrderRequest.setRestaurantId(cart1.getRestaurantId());
        editOrderRequest.setCartType(cart1.getCartType());
        editOrderRequest.setOrderId(String.valueOf(orderResponse.getData().getOrderId()));
        editOrderRequest.setInitiationSource(2);
        String payload = Utility.jsonEncode(editOrderRequest);
        return new String[]{payload};
    }

    public Processor getEditOrderCheck(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        String[] payload = getEditOrderPayload(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        Processor processor = editOrderCheck(payload);
        return processor;
    }

    public Processor editOrderCheck(String[] payload){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "editordercheckv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor editOrderCheck(String[] payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "editordercheckv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor getEditOrderConfirm(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        String[] payload = getEditOrderPayload(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        editOrderCheck(payload);
        Processor processor = editOrderConfirm(payload);
        return processor;
    }

    public Processor editOrderConfirm(String[] payload){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "editorderconfirmv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor editOrderConfirm(String[] payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "editorderconfirmv2", gameofthrones);
        return new Processor(gots, headers, payload, null);
    }

    public Processor cancelOrder(String orderId, String reason, String cancellationFeeApplicability, String cancellationFee){
        HashMap<String, String>  headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        String[] payloadparams = {reason , cancellationFeeApplicability, cancellationFee, reason};
        String[] urlParams = new String[]{orderId};
        GameOfThronesService gots = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
        Processor processor = new Processor(gots, headers, payloadparams, urlParams);
        return  processor;
    }

    public Processor cancelOrder(String orderId){
        HashMap<String, String>  headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
        cancelOrderRequest.setCancellationReason(EDVOConstants.CANCELLATION_REASON);
        cancelOrderRequest.setCancellationFeeApplicability(Boolean.parseBoolean(EDVOConstants.CANCELLATION_FEE_APPLICABILITY));
        cancelOrderRequest.setCancellationFee(Float.valueOf(EDVOConstants.CANCELLATION_FEE));
        cancelOrderRequest.setCancellationReasonText(EDVOConstants.CANCELLATION_REASON);
        String[] urlParams = new String[]{orderId};
        String payload = Utility.jsonEncode(cancelOrderRequest);
        GameOfThronesService gots = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
        Processor processor = new Processor(gots, headers, new String[]{payload}, urlParams);
        return  processor;
    }

    public Processor cancelOrder(String orderId, HashMap<String, String> headers){
        String[] urlParams = new String[]{orderId};
        CancelOrderRequest cancelOrderRequest = new CancelOrderRequest();
        cancelOrderRequest.setCancellationReasonText(EDVOConstants.CANCELLATION_REASON);
        cancelOrderRequest.setCancellationReason(EDVOConstants.CANCELLATION_REASON);
        cancelOrderRequest.setCancellationFee(0);
        cancelOrderRequest.setCancellationFeeApplicability(true);
        String[] payloadparams = new String[]{Utility.jsonEncode(cancelOrderRequest)};
        GameOfThronesService gots = new GameOfThronesService("checkout", "ordercancel", gameofthrones);
        Processor processor = new Processor(gots, headers, payloadparams, urlParams);
        return  processor;
    }

    public Processor getCancelOrder(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        Processor placeOrder = getPlaceOrder(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(placeOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        smokeCheck(0, "done successfully", placeOrder, "Place Order API Failed...!!");
        return cancelOrder(String.valueOf(orderResponse.getData().getOrderId()));
    }

    public Processor getAllOrders(){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "getallorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor getAllOrders(HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "getallorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public List<Integer> getAllOrderId(){
        Processor processor = getAllOrders();
        List<Integer> orderIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.orders.[*].order_id");
        return orderIds;
    }

    public List<Integer> getAllActiveOrders(){
        Processor processor = getAllOrders();
        List<Integer> orderIds = new ArrayList<>();
        GetAllOrders getAllOrders = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetAllOrders.class);
        for(Data data : getAllOrders.getData().getOrders()){
            if (!data.getOrderStatus().equals("cancelled") && !data.getOrderStatus().equals("failed") && !data.getOrderStatus().equals("completed")){
                orderIds.add(data.getOrderId().intValue());
            }
        }
        return orderIds;
    }

    public List<Integer> getAllActiveOrders(HashMap<String, String> headers){
        Processor processor = getAllOrders(headers);
        List<Integer> orderIds = new ArrayList<>();
        GetAllOrders getAllOrders = Utility.jsonDecode(processor.ResponseValidator.GetBodyAsText(), GetAllOrders.class);
        for(Data data : getAllOrders.getData().getOrders()){
            if (!data.getOrderStatus().equals("cancelled") && !data.getOrderStatus().equals("failed") && !data.getOrderStatus().equals("completed")){
                orderIds.add(data.getOrderId().intValue());
            }
        }
        return orderIds;
    }

    public List<Integer> getAllOrderId(HashMap<String, String> headers){
        Processor processor = getAllOrders(headers);
        List<Integer> orderIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),
                "$.data.orders.[*].order_id");
        return orderIds;
    }

    public void cancelAllOrders(){
        List<Integer> orderIds = getAllActiveOrders();
        for(int i=0; i<orderIds.size(); i++){
            cancelOrder(String.valueOf(orderIds.get(i)));
        }
    }

    public void cancelAllOrders(HashMap<String, String> headers){
        List<Integer> orderIds = getAllActiveOrders(headers);
        for(int i=0; i<orderIds.size(); i++){
            cancelOrder(String.valueOf(orderIds.get(i)), headers);
        }
    }

    public Processor getCloneOrderCheck(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        Processor cancelOrder = getCancelOrder(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(cancelOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        Cart cart = getCloneOrderPayload1(this.cart, isCartItemRequired, isMealItemRequired);
        EditOrderRequest cloneOrderRequest = new EditOrderRequest();
        cloneOrderRequest.setOrderId(orderResponse.getData().getOrderId().toString());
        cloneOrderRequest.setInitiationSource(2);
        cloneOrderRequest.setIdentifier("A unique string for this order edit operation");
        cloneOrderRequest.setSwiggyMoneyApplicable("true");
        cloneOrderRequest.setReasonForCloning("Testing");
        cloneOrderRequest.setRestaurantBearAmount("0");
        cloneOrderRequest.setCartItems(cart.getCartItems());
        cloneOrderRequest.setMealItems(cart.getMealItems());
        cloneOrderRequest.setRestaurantId(restaurantId);

        String payload = Utility.jsonEncode(cloneOrderRequest);
        Processor processor = cloneOrderCheck(new String[]{payload});

        return processor;
    }

    public String getCloneOrderCheckOrderId(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        Processor cancelOrder = getCancelOrder(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        OrderResponse orderResponse = Utility.jsonDecode(cancelOrder.ResponseValidator.GetBodyAsText(), OrderResponse.class);
        String orderId = orderResponse.getData().getOrderId().toString();

        Cart cart = getCloneOrderPayload1(this.cart, isCartItemRequired, isMealItemRequired);
        cloneOrderRequest = new EditOrderRequest();
        cloneOrderRequest.setOrderId(orderResponse.getData().getOrderId().toString());
        cloneOrderRequest.setInitiationSource(2);
        cloneOrderRequest.setIdentifier("A unique string for this order edit operation");
        cloneOrderRequest.setSwiggyMoneyApplicable("true");
        cloneOrderRequest.setReasonForCloning("Testing");
        cloneOrderRequest.setRestaurantBearAmount("0");
        cloneOrderRequest.setCartItems(cart.getCartItems());
        cloneOrderRequest.setMealItems(cart.getMealItems());
        cloneOrderRequest.setRestaurantId(restaurantId);

        String payload = Utility.jsonEncode(cloneOrderRequest);
        Processor processor = cloneOrderCheck(new String[]{payload});

        return orderId;
    }

    public Processor cloneOrderCheck(String[] payload){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Processor cloneOrderCheck(String[] payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneordercheck", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Cart getCloneOrderPayload1(Cart cart, boolean isCartMenuItemRequired, boolean isMealItemRequired){
        Cart cart1;
        CartBuilder cartEntityBuilder = new CartBuilder();
        cartEntityBuilder.restaurantId(cart.getRestaurantId());

        if(isCartMenuItemRequired){
            cartEntityBuilder.cartItems(cart.getCartItems());
        }

        if(isMealItemRequired){
            cartEntityBuilder.mealItems(cart.getMealItems());
        }

        cart1 = cartEntityBuilder.build();
        return cart1;
    }

    public String getCloneOrderPayload(Cart cart, boolean isCartMenuItemRequired, boolean isMealItemRequired){
        Cart cart1 = null;
        CartBuilder cartEntityBuilder = new CartBuilder();
        cartEntityBuilder.restaurantId(cart.getRestaurantId());

        if(isCartMenuItemRequired){
            cartEntityBuilder.cartItems(cart.getCartItems());
        }

        if(isMealItemRequired){
            cartEntityBuilder.mealItems(cart.getMealItems());
        }

        cart1 = cartEntityBuilder.build();
        String cartEntityString = Utility.jsonEncode(cart1);
        cartEntityString = cartEntityString.substring(1, cartEntityString.length()-1);
        return cartEntityString;
    }

    public Processor getCloneOrderConfirm(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        String orderId = getCloneOrderCheckOrderId(mealId, restaurantId, isAddonRequired, isMealItemRequired, isCartItemRequired);
        String payload = Utility.jsonEncode(cloneOrderRequest);
        Processor processor = cloneOrderConfirm(new String[]{payload});

        return processor;
    }

    public Processor cloneOrderConfirm(String[] payload){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneorderconfirm", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Processor cloneOrderConfirm(String[] payload, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "cloneorderconfirm", gameofthrones);
        return new Processor(gots, headers, payload, null);

    }

    public Processor singleOrder(String orderId){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "getsingleorder", gameofthrones);
        return new Processor(gots, headers, null, new String[]{orderId});

    }

    public Processor singleOrder(String orderId, HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "getsingleorder", gameofthrones);
        return new Processor(gots, headers, null, new String[]{orderId});

    }

    public Processor getLastOrder(){
        HashMap<String, String> headers = getHeaders(EDVOConstants.MOBILE, EDVOConstants.PASSWORD);
        GameOfThronesService gots = new GameOfThronesService("checkout", "getlastorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public Processor getLastOrder(HashMap<String, String> headers){
        GameOfThronesService gots = new GameOfThronesService("checkout", "getlastorder", gameofthrones);
        return new Processor(gots, headers, null, null);
    }

    public void smokeCheck(int statusCode, String statusMessage, Processor processor, String errorMessage){
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, errorMessage);
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, errorMessage);
        softAssert.assertAll();
    }

    public void smokeCheck(int statusCode, String statusMessage, Processor processor){
        SoftAssert softAssert = new SoftAssert();
        Reporter.log("Expected StatusCode = '" + statusCode + "' <==> Actual StatusCode = '" + processor.ResponseValidator.GetNodeValueAsInt("statusCode") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValueAsInt("statusCode"), statusCode, "StatusCode mismatch");
        Reporter.log("Expected StatusMessage = '" + statusMessage + "' <==> Actual StatusMessage = '" + processor.ResponseValidator.GetNodeValue("statusMessage") + "'", true);
        softAssert.assertEquals(processor.ResponseValidator.GetNodeValue("statusMessage"), statusMessage, "StatusMessage mismatch");
        softAssert.assertAll();
    }

    public Processor reserveCart(String address, String sla, HashMap<String, String> headers){
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "reservecart", gameofthrones);
        return new Processor(gameOfThronesService, headers, null, new String[]{address, sla});
    }

    public Processor unreserveCart(HashMap<String, String> headers){
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "unreservecart", gameofthrones);
        return new Processor(gameOfThronesService, headers, null, null);
    }

    public Processor orderCancelCheck(String payload){
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "cancelcheck", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{payload}, null);
    }

    public Processor revertCancellationFee(String payload){
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "revertcancellationfee", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{payload}, null);
    }

    /**
     * This method checks if the restaurant is serviceable or not.
     * Returns true for serviceable and false if the restaurant is not serviceable.
     * @param restaurantId
     * @return
     */
    public boolean restaurantServiceabilityCheck(String restaurantId){
        Processor processor = getMenuV4(restaurantId);
        String latLng = processor.ResponseValidator.GetNodeValue("$.data.latLong");
        String lat =latLng.substring(0, latLng.indexOf(","));
        String lng = latLng.substring(latLng.indexOf(",")+1);
        Reporter.log("Lat = ["+lat+"]", true);
        Reporter.log("Lng = ["+lng+"]", true);
        processor = getMenuV4WithLatLng(restaurantId, lat, lng);
        //Checks if sla node is present in the Menu V4 response
        if(Utility.doesJsonPathExist(processor, "$.data.sla")){
            //Checks if serviceability node is present inside sla of Menu v4 response
            if(Utility.doesJsonPathExist(processor, "$.data.sla.serviceability")){
                String serviceability = processor.ResponseValidator.GetNodeValue("$.data.sla.serviceability");
                String rainMode = processor.ResponseValidator.GetNodeValue("$.data.sla.rainMode");
                if(!serviceability.equals("SERVICEABLE")){
                    Reporter.log("Restaurant ID [" + restaurantId + "] serviceability = '" + serviceability + "'", true);
                    Reporter.log("Restaurant ID [" + restaurantId + "] rainMode = '" + rainMode + "'", true);
                    Reporter.log("Restaurant ID [" + restaurantId + "] is not serviceable..!!!", true);
                    return false;
                }
            }else {
                Reporter.log("SLA is not found in MenuV4 Response..!!!", true);
                return false;
            }
        }
        return true;
    }

    /**
     * This method Gets the item delails with Catalogue Attributes from CMS
     * @param itemIds
     * @param isAvailable
     * @return
     */
    public Processor getItemFromCMS(String[] itemIds, boolean isAvailable){
        String mergedItemIds = "";
        if(itemIds != null & itemIds.length > 0) {
            mergedItemIds = itemIds[0];
            if(itemIds.length>1) {
                for (int i = 1; i < itemIds.length; i++) {
                    mergedItemIds = mergedItemIds.concat("," + itemIds[i]);
                }
            }
        }else {
            Reporter.log("[NO ITEM ID FOUND] to get the item details with Catalogue Attributes..!!", true);
            return null;
        }
        GameOfThronesService gameOfThronesService = new GameOfThronesService("cmscatalogueattributes",
                "getitemswithattributes", gameofthrones);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return new Processor(gameOfThronesService, headers, null, new String[]{mergedItemIds, String.valueOf(isAvailable)});
    }

    /**
     * This method Creates a Meal for provided payload
     * @param payload
     * @return
     */
    public Processor createMeal(String payload){
        HashMap<String, String> headers = new HashMap<>();
        GameOfThronesService gameOfThronesService = new GameOfThronesService("sand", "createmeal", gameofthrones);
        headers.put("Content-Type", "application/json");
        return new Processor(gameOfThronesService, headers, new String[]{payload}, null);
    }
    
    public Cart getCartPayload1(String[] mealId, String restaurantId, boolean isAddonRequired, boolean isMealItemRequired, boolean isCartItemRequired){
        List<MealItem> mealItems = new ArrayList<>();
        Processor menuV4 = sandHelper.getRestaurantMenu(restaurantId);
        CartBuilder cartBuilder = new CartBuilder()
                .cartItems(getCartItems1(menuV4, 1, isAddonRequired, isCartItemRequired))
                .cartType("REGULAR")
                .restaurantId(restaurantId);
        if(menuV4.ResponseValidator.GetNodeValue("$.data.name").contains("Domino")) {
            cartBuilder.address(EDVOConstants.ADDRESS_ID);
        }
        return cartBuilder.build();
    }
    
    public Cart getCartPayloadSuper(String[] mealId, String restaurantId, boolean isAddonRequired, 
    		                                boolean isMealItemRequired, boolean isCartItemRequired,
    		                                String couponCode,Integer plan_id){
        Processor menuV4 = sandHelper.getRestaurantMenu(restaurantId);
        CartBuilder cartBuilder = new CartBuilder()
                .cartItems(getCartItems1(menuV4, 1, isAddonRequired, isCartItemRequired))
                .cartType("REGULAR")
                .restaurantId(restaurantId)
                .couponCode(couponCode)
                .subscriptionItems(getSubscriptionItems(plan_id));
        if(menuV4.ResponseValidator.GetNodeValue("$.data.name").contains("Domino")) {
            cartBuilder.address(EDVOConstants.ADDRESS_ID);
        }
        return cartBuilder.build();
    }
    
    
   public Cart getCartPayloadPreOrder(String restaurantId, boolean isAddonRequired,
		                                    boolean isCartItemRequired,String couponCode){
	   Processor menuV4 = sandHelper.getRestaurantMenu(restaurantId);  
	   String menuRes=menuV4.ResponseValidator.GetBodyAsText();
	   String preorderable=JsonPath.read(menuRes, "$.data.preorderable").toString().replace("[", "").replace("]", "");
	   if (preorderable.equals("false")){
		   return null;
	   }
	   Processor preOrderSlotListing = getPreOrderSlot(restaurantId);
           CartBuilder cartBuilder = new CartBuilder()
           .cartItems(getCartItems1(menuV4, 1, isAddonRequired, isCartItemRequired))
           .cartType("PREORDER")
           .preorderSlot(getPreorderSlot(preOrderSlotListing))
           .couponCode(couponCode)
           .restaurantId(restaurantId);
    return cartBuilder.build();
  }
   
   public Cart getCafeCartPayload(String restaurantId, boolean isAddonRequired, 
                                  boolean isMealItemRequired, boolean isCartItemRequired,
                                  String couponCode){
      Processor menuV4 = sandHelper.getRestaurantMenu(restaurantId);
      CartBuilder cartBuilder = new CartBuilder()
                                .cartItems(getCartItems1(menuV4, 1, isAddonRequired, isCartItemRequired))
                                .cartType("CAFE")
                                .restaurantId(restaurantId)
                                .couponCode(couponCode);
     return cartBuilder.build();
}
   
   public Cart getCartPayloadPopOrder(String[] mealId, String restaurantId, boolean isAddonRequired, 
           boolean isMealItemRequired, boolean isCartItemRequired){
	   Processor menuV4 = sandHelper.getRestaurantMenu(restaurantId);
          CartBuilder cartBuilder = new CartBuilder()
          .cartItems(getCartItems1(menuV4, 1, isAddonRequired, isCartItemRequired))
          .cartType("POP")
          .restaurantId(restaurantId);
   return cartBuilder.build();
 }
   
   
   public PreorderSlot getPreorderSlot(Processor preOrderSlotListing){
	   
	   if(Utility.doesJsonPathExist(preOrderSlotListing, "$.data.[0].date")){
		   String response=preOrderSlotListing.ResponseValidator.GetBodyAsText();
		   Long date = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
                                               .toString().replace("]",""));
           PreorderSlot preorderSlot=new PreorderSlot();
           Slot slot=new Slot();

           int lastIndex = (JsonPath.read(response,"$.data[0]..slots..startTime").toString().replace("[","")
                                    .toString().replace("]","").split(",").length)-1;
           Long startTime = Long.parseLong(JsonPath.read(response,"$.data[0].slots["+lastIndex+"].startTime")
                                .toString().replace("[","").toString().replace("]",""));
           Long endTime = Long.parseLong(JsonPath.read(response,"$.data.[0].date").toString().replace("[","")
                              .toString().replace("]",""));

           slot.setStartTime(startTime);
           slot.setEndTime(endTime);		   
           preorderSlot.setDate(date);
           preorderSlot.setSlot(slot);

       return preorderSlot;
	   }
return null;  
   }
   
    public List<CartItem> getCartItems1(Processor processor, Integer quantity, boolean isAddonRequired, boolean isCartItemRequired){
        List<CartItem> cartItems = new ArrayList<>();
        if(!isCartItemRequired){
            return cartItems;
        }
        //Processor processor = getMenuV4(restaurantId);
        List<Integer> inStocks = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.inStock");
        List<Integer> itemIds = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.menu.items.*.id");

        for(int i=0;i<inStocks.size();i++) {
            CartItem cartItem = null;
            if (inStocks.get(i).intValue() == 1) {
                if (isAddonRequired && Utility.doesJsonPathExist(processor, "data.menu.items." + itemIds.get(i).intValue() + ".addons")) {
                        cartItem = new CartItemBuilder()
                                .menuItemId(String.valueOf(itemIds.get(i).intValue()))
                                .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
                                .quantity(quantity)
                                .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
                                .build();
                        cartItems.add(cartItem);
                        break;
                } else {
                    cartItem = new CartItemBuilder()
                            .menuItemId(String.valueOf(itemIds.get(i).intValue()))
                            .variants(getVariantsForNormalItem(processor, i, 1, itemIds.get(i).intValue()))
                            .quantity(quantity)
                            .addons(getAddonsForCartMenuItem(processor, itemIds.get(i), isAddonRequired))
                            .build();
                    cartItems.add(cartItem);
                    break;
                }
            }
        }
        return cartItems;
    }
    
    public List<SubscriptionItem> getSubscriptionItems(Integer plan_id){
        List<SubscriptionItem> subscriptionItems = new ArrayList<>();
        if(plan_id==0){
            return subscriptionItems;
        }
        SubscriptionItem subscriptionItem = new SubscriptionItemBuilder()
        		                                .plan_id(plan_id)
        		                                .quantity(1).build();
        subscriptionItems.add(subscriptionItem);
        return subscriptionItems;
    }

    public Processor getPreOrderSlot(String restID) {
        String[] queryParam = {restID};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService gots = new GameOfThronesService("sand", "preOrderSlot", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    public Processor getRestaurantMenu(String restID) {
    	Processor menuV4 = sandHelper.getRestaurantMenu(restID);
    	return menuV4;
    }

    public HashMap<String, String> getTokenDataCheckout(String mobile, String password) {
        String resp = loginFromCheckout(mobile, password).ResponseValidator.GetBodyAsText();
        String tid = JsonPath.read(resp, "$.tid").toString().replace("[", "").replace("]", "");
        String token = JsonPath.read(resp, "$.data.token").toString().replace("[", "").replace("]", "");
        String customerId = JsonPath.read(resp, "$.data.customer_id").toString().replace("[", "").replace("]", "");
        String deviceId = JsonPath.read(resp, "$.deviceId").toString().replace("[", "").replace("]", "");
        String sid= JsonPath.read(resp,"$.sid").toString().replace("[", "").replace("]", "");
        String userId = JsonPath.read(resp, "$.data.addresses[0]..user_id").toString().replace("[", "").replace("]", "");
        HashMap<String, String> hashMap= new HashMap<>();
        hashMap.put("Tid", tid);
        hashMap.put("Token", token);
        hashMap.put("CustomerId", customerId);
        hashMap.put("userId", userId);
        hashMap.put("DeviceId", deviceId);
        hashMap.put("sid",sid);
        hashMap.put("content-type", "application/json");
        hashMap.put("Authorization", EditTimeConstants.AUTHORIZATION);
        return hashMap;
    }

    public Processor loginFromCheckout(String mobile, String password){
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        GameOfThronesService gameOfThronesService = new GameOfThronesService("checkout", "logincheckout", gameofthrones);
        return new Processor(gameOfThronesService, headers, new String[]{mobile, password});
    }
}
