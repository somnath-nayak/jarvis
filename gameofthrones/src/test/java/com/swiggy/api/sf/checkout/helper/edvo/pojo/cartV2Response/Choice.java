package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "price",
        "inStock",
        "order",
        "default"
})
public class Choice {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("inStock")
    private Integer inStock;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("default")
    private Integer _default;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Choice() {
    }

    /**
     *
     * @param id
     * @param price
     * @param order
     * @param _default
     * @param name
     * @param inStock
     */
    public Choice(Integer id, String name, Integer price, Integer inStock, Integer order, Integer _default) {
        super();
        this.id = id;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.order = order;
        this._default = _default;
    }

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("inStock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("price", price).append("inStock", inStock).append("order", order).append("_default", _default).append("additionalProperties", additionalProperties).toString();
    }

}