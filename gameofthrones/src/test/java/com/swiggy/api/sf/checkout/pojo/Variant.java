package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class Variant {

    private Integer variation_id;
    private Integer group_id;
    //private String name;
    private Double price;

    /**
     * No args constructor for use in serialization
     *
     */
    public Variant() {
    }

    /**
     *
     * @param group_id
     * @param variation_id
     * @param price
     *
     */
    public Variant(Integer variation_id, Integer group_id,  Double price) {
        super();
        this.variation_id = variation_id;
        this.group_id = group_id;
        this.price = price;
    }

    public Integer getvariation_id() {
        return variation_id;
    }

    public void setvariation_id(Integer variation_id) {
        this.variation_id = variation_id;
    }

    public Integer getgroup_id() {
        return group_id;
    }

    public void setgroup_id(Integer group_id) {
        this.group_id = group_id;
    }

   /* public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }*/

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variation_id", variation_id).append("group_id", group_id).append("price", price).toString();
    }

}
