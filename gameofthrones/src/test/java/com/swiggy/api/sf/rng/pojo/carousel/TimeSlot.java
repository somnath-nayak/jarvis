package com.swiggy.api.sf.rng.pojo.carousel;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class TimeSlot {

    @JsonProperty("day")
    private String day;
    @JsonProperty("startTime")
    private Integer startTime;
    @JsonProperty("closeTime")
    private Integer closeTime;

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public TimeSlot withDay(String day) {
        this.day = day;
        return this;
    }

    @JsonProperty("startTime")
    public Integer getStartTime() {
        return startTime;
    }

    @JsonProperty("startTime")
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public TimeSlot withStartTime(Integer startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("closeTime")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public TimeSlot withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }
}
