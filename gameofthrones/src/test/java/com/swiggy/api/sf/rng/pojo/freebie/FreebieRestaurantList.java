package com.swiggy.api.sf.rng.pojo.freebie;

import org.apache.commons.lang.builder.ToStringBuilder;

public class FreebieRestaurantList {

	private Integer id;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public FreebieRestaurantList() {
	}

	/**
	 * 
	 * @param id
	 */
	public FreebieRestaurantList(Integer id) {
		super();
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id:", id).toString();
	}

}