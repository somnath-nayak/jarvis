package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.*;
import com.swiggy.api.sf.rng.helper.RngHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.Slot;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;

import java.util.List;

public class HelpDP {
    @DataProvider(name="userSingleOrder")
    public Object[][] userSingleOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "1007285", 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("rkonowhere")
                .mobile(7406734416l)
                .cart(cart)
                .restaurant(6131)
                .Address(782234)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createOrder,CheckoutConstants.trackUnassigned}
        };
    }

    @DataProvider(name="MultipleOrder")
    public Object[][] MultipleOrder() throws IOException {
        CheckoutHelper helper = new CheckoutHelper();
        List<String> list = helper.pickNRandom("6131");
        List<Addon> addon = new ArrayList<>();
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, list.get(0), 1));
        cart.add(new Cart(addon, variant, list.get(1), 1));
        cart.add(new Cart(addon, variant, list.get(2), 1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("swiggy")
                .mobile(7899772315l)
                .cart(cart)
                .restaurant(6131)
                .Address(777204)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createOrder,CheckoutConstants.trackUnassigned}
        };
    }

    @DataProvider(name="MultipleAddonOrder")
    public Object[][] MultipleAddonOrder() throws IOException {
        List<Addon> addon = new ArrayList<>();
        addon.add(new Addon("961936", "162687", "Boneless Chicken ", 10));
        List<Addon> addons = new ArrayList<>();
        addons.add(new Addon("961922","162686","Chilli Garlic",10));
        List<Variant> variant = new ArrayList<>();
        List<Cart> cart = new ArrayList<>();
        cart.add(new Cart(addon, variant, "1007195", 1));
        cart.add(new Cart(addons, variant, "1008523",1));
        CreateMenuEntry createOrder = new CreateOrderBuilder()
                .password("swiggy")
                .mobile(7899772315l)
                .cart(cart)
                .restaurant(6131)
                .Address(13172334)
                .paymentMethod("Cash")
                .orderComments("Test-Order")
                .build();
        return new Object[][]{
                {createOrder,CheckoutConstants.trackUnassigned}
        };
    }
}
