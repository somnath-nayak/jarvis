package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "address_line1",
        "address",
        "landmark",
        "area",
        "mobile",
        "annotation",
        "email",
        "flat_no",
        "city",
        "lat",
        "lng",
        "reverse_geo_code_failed"
})
public class DeliveryAddress {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("address_line1")
    private String addressLine1;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("email")
    private String email;
    @JsonProperty("flat_no")
    private String flatNo;
    @JsonProperty("city")
    private String city;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("reverse_geo_code_failed")
    private Boolean reverseGeoCodeFailed;

    /**
     * No args constructor for use in serialization
     *
     */
    public DeliveryAddress() {
    }

    /**
     *
     * @param annotation
     * @param id
     * @param landmark
     * @param area
     * @param email
     * @param address
     * @param name
     * @param lng
     * @param reverseGeoCodeFailed
     * @param lat
     * @param flatNo
     * @param addressLine1
     * @param city
     * @param mobile
     */
    public DeliveryAddress(String id, String name, String addressLine1, String address, String landmark, String area, String mobile, String annotation, String email, String flatNo, String city, String lat, String lng, Boolean reverseGeoCodeFailed) {
        super();
        this.id = id;
        this.name = name;
        this.addressLine1 = addressLine1;
        this.address = address;
        this.landmark = landmark;
        this.area = area;
        this.mobile = mobile;
        this.annotation = annotation;
        this.email = email;
        this.flatNo = flatNo;
        this.city = city;
        this.lat = lat;
        this.lng = lng;
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("address_line1")
    public String getAddressLine1() {
        return addressLine1;
    }

    @JsonProperty("address_line1")
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("flat_no")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flat_no")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("reverse_geo_code_failed")
    public Boolean getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    @JsonProperty("reverse_geo_code_failed")
    public void setReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("addressLine1", addressLine1).append("address", address).append("landmark", landmark).append("area", area).append("mobile", mobile).append("annotation", annotation).append("email", email).append("flatNo", flatNo).append("city", city).append("lat", lat).append("lng", lng).append("reverseGeoCodeFailed", reverseGeoCodeFailed).toString();
    }

}