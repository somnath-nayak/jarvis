package com.swiggy.api.sf.rng.pojo;


import org.codehaus.jackson.annotate.JsonProperty;

public class ApplyCouponTimeSlotPOJO {

    @JsonProperty("code")
    private String code;
    @JsonProperty("areaId")
    private int areaId;
    @JsonProperty("cityId")
    private int cityId;
    @JsonProperty("swiggyTradeDiscount")
    private Double swiggyTradeDiscount;
    @JsonProperty("restaurantTradeDiscount")
    private Double restaurantTradeDiscount;
    @JsonProperty("itemKey")
    private  Long itemKey;
    @JsonProperty("menuItemId")
    private Integer menuItemId;
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("swiggyMoney")
    private Double swiggyMoney;
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonProperty("cartPrice_Quantity")
    private Integer cartPrice_Quantity;
    @JsonProperty("cartTotal")
    private Double cartTotal;
    @JsonProperty("itemLevel")
    private String itemLevel;
    @JsonProperty("itemLevelPriceQuantity")
    private Integer itemLevelPriceQuantity;
    @JsonProperty("itemLevelPriceSubTotal")
    private Double  itemLevelPriceSubTotal;
    @JsonProperty("preferredPaymentMethod")
    private String preferredPaymentMethod;
    @JsonProperty("referralPresence")
    private Boolean referralPresence;
    @JsonProperty("swiggyMoneyApplicable")
    private Boolean swiggyMoneyApplicable;
    @JsonProperty("superUser")
    private Boolean superUser;
    @JsonProperty("typeOfPartner")
    private String typeOfPartner;
    @JsonProperty("versionCode")
    private  Integer versionCode;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonProperty("payment_codes")
    private String payment_codes;
    @JsonProperty("time")
    private String time;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public Double getSwiggyTradeDiscount() {
        return swiggyTradeDiscount;
    }

    public void setSwiggyTradeDiscount(Double swiggyTradeDiscount) {
        this.swiggyTradeDiscount = swiggyTradeDiscount;
    }

    public Double getRestaurantTradeDiscount() {
        return restaurantTradeDiscount;
    }

    public void setRestaurantTradeDiscount(Double restaurantTradeDiscount) {
        this.restaurantTradeDiscount = restaurantTradeDiscount;
    }

    public Long getItemKey() {
        return itemKey;
    }

    public void setItemKey(Long itemKey) {
        this.itemKey = itemKey;
    }

    public Integer getMenuItemId() {
        return menuItemId;
    }

    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Double getSwiggyMoney() {
        return swiggyMoney;
    }

    public void setSwiggyMoney(Double swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    public Boolean getFirstOrder() {
        return firstOrder;
    }

    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public Integer getCartPrice_Quantity() {
        return cartPrice_Quantity;
    }

    public void setCartPrice_Quantity(Integer cartPrice_Quantity) {
        this.cartPrice_Quantity = cartPrice_Quantity;
    }

    public Double getCartTotal() {
        return cartTotal;
    }

    public void setCartTotal(Double cartTotal) {
        this.cartTotal = cartTotal;
    }

    public String getItemLevel() {
        return itemLevel;
    }

    public void setItemLevel(String itemLevel) {
        this.itemLevel = itemLevel;
    }

    public Integer getItemLevelPriceQuantity() {
        return itemLevelPriceQuantity;
    }

    public void setItemLevelPriceQuantity(Integer itemLevelPriceQuantity) {
        this.itemLevelPriceQuantity = itemLevelPriceQuantity;
    }

    public Double getItemLevelPriceSubTotal() {
        return itemLevelPriceSubTotal;
    }

    public void setItemLevelPriceSubTotal(Double itemLevelPriceSubTotal) {
        this.itemLevelPriceSubTotal = itemLevelPriceSubTotal;
    }

    public String getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    public void setPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
    }

    public Boolean getReferralPresence() {
        return referralPresence;
    }

    public void setReferralPresence(Boolean referralPresence) {
        this.referralPresence = referralPresence;
    }

    public Boolean getSwiggyMoneyApplicable() {
        return swiggyMoneyApplicable;
    }

    public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    public Boolean getSuperUser() {
        return superUser;
    }

    public void setSuperUser(Boolean superUser) {
        this.superUser = superUser;
    }

    public String getTypeOfPartner() {
        return typeOfPartner;
    }

    public void setTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getPayment_codes() {
        return payment_codes;
    }

    public void setPayment_codes(String payment_codes) {
        this.payment_codes = payment_codes;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ApplyCouponTimeSlotPOJO withCode(String code) {
        this.code = code;
        return this;
    }
    public ApplyCouponTimeSlotPOJO withAreaId(Integer areaId) {
        this.areaId=areaId;
        return this;
    }
    public ApplyCouponTimeSlotPOJO withCityId(Integer cityId) {
        this.cityId=cityId;
        return this;
    }
    public ApplyCouponTimeSlotPOJO withSwiggyTradeDiscount(Double swiggyTradeDiscount) {
        this.swiggyTradeDiscount=swiggyTradeDiscount;
        return this;
    }
    public ApplyCouponTimeSlotPOJO withRestaurantTradeDiscount(Double restaurantTradeDiscount) {
        this.restaurantTradeDiscount=restaurantTradeDiscount;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withItemKey(Long itemKey) {
        this.itemKey=itemKey;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withMenuItemId(Integer menuItemId) {
        this.menuItemId=menuItemId;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withQuantity(Integer quantity) {
        this.quantity=quantity;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withRestaurantId(String restaurantId) {
        this.restaurantId=restaurantId;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withUserId(String userId) {
        this.userId=userId;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withSwiggyMoney(Double swiggyMoney) {
        this.swiggyMoney=swiggyMoney;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withFirstOrder(Boolean firstOrder) {
        this.firstOrder=firstOrder;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withCartPrice_Quantity(Integer cartPrice_Quantity) {
        this.cartPrice_Quantity=cartPrice_Quantity;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withCartTotal(Double cartTotal) {
        this.cartTotal=cartTotal;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withItemLevel(String itemLevel) {
        this.itemLevel=itemLevel;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withItemLevelPriceQuantity(Integer itemLevelPriceQuantity) {
        this.itemLevelPriceQuantity=itemLevelPriceQuantity;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withItemLevelPriceSubTotal(Double itemLevelPriceSubTotal) {
        this.itemLevelPriceSubTotal=itemLevelPriceSubTotal;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod=preferredPaymentMethod;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withReferralPresence(Boolean referralPresence) {
        this.referralPresence=referralPresence;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable=swiggyMoneyApplicable;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withSuperUser(Boolean superUser) {
        this.superUser=superUser;
        return this;
    }

    public ApplyCouponTimeSlotPOJO witTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner=typeOfPartner;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withVersionCode(Integer versionCode) {
        this.versionCode=versionCode;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withUserAgent(String userAgent) {
        this.userAgent=userAgent;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withPayment_codes(String payment_codes) {
        this.payment_codes=payment_codes;
        return this;
    }

    public ApplyCouponTimeSlotPOJO withTime(String time) {
        this.time=time;
        return this;
    }


    public ApplyCouponTimeSlotPOJO setDefaultData(String couponCode) {

        return this.withCode(couponCode)
                .withAreaId(1)
                .withCityId(1)
                .withSwiggyTradeDiscount(0.0)
                .withRestaurantTradeDiscount(12.0)
                .withItemKey(null)
                .withMenuItemId(5282696)
                .withQuantity(1)
                .withRestaurantId("28157")
                .withUserId("5660904")
                .withSwiggyMoney(0.0)
                .withFirstOrder(false)
                .withCartPrice_Quantity(2)
                .withCartTotal(200.0)
                .withItemLevel("1")
                .withItemLevelPriceQuantity(1)
                .withItemLevelPriceSubTotal(70.0)
                .withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo")
                .withReferralPresence(false)
                .withSwiggyMoneyApplicable(true)
                .withSuperUser(false)
                .witTypeOfPartner(null)
                .withVersionCode(245)
                .withUserAgent("ANDROID")
                .withPayment_codes(null)
                .withTime(null);


    }

}
