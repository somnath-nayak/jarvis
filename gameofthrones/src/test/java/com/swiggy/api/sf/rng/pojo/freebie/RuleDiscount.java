package com.swiggy.api.sf.rng.pojo.freebie;

import org.codehaus.jackson.annotate.JsonProperty;

public class RuleDiscount {

    @JsonProperty("type")
    private String type;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("minCartAmount")
    private String minCartAmount;
    @JsonProperty("itemId")
    private String itemId;

    /**
     * No args constructor for use in serialization
     *
     */
    public RuleDiscount() {
    }

    /**
     *
     * @param minCartAmount
     * @param itemId
     * @param discountLevel
     * @param type
     */
    public RuleDiscount(String type, String discountLevel, String minCartAmount, String itemId) {
        super();
        this.type = type;
        this.discountLevel = discountLevel;
        this.minCartAmount = minCartAmount;
        this.itemId = itemId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public RuleDiscount withType(String type) {
        this.type = type;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public RuleDiscount withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("minCartAmount")
    public String getMinCartAmount() {
        return minCartAmount;
    }

    @JsonProperty("minCartAmount")
    public void setMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    public RuleDiscount withMinCartAmount(String minCartAmount) {
        this.minCartAmount = minCartAmount;
        return this;
    }

    @JsonProperty("itemId")
    public String getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public RuleDiscount withItemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

}