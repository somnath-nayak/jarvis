package com.swiggy.api.sf.snd.pojo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchBuilder {
    private SearchRequests searchRequests;

    public SearchBuilder()
    {
        searchRequests = new SearchRequests();
    }

    public SearchBuilder searchrequest(List<SearchRequest> search) {
        ArrayList<SearchRequest> s= new ArrayList<>();
        for(SearchRequest searchR: search)
        {
            s.add(searchR);
        }
        searchRequests.setSearchRequests(s);
        return this;
    }

    public SearchBuilder requestContext(RequestContext request) {
        searchRequests.setRequestContext(request);
        return this;
    }

    private void defaultSearchConan() throws IOException {
        List <SearchRequests> e= new ArrayList<>();
        SearchRequests search=new SearchRequests();
        if(searchRequests.getSearchRequests()==null){
            search.setSearchRequests(new ArrayList<>());
            e.add(search);
        }
        if(search.getRequestContext()==null)
            search.setRequestContext(new RequestContext());
    }

    public SearchRequests buildSearchConan() throws IOException {
        defaultSearchConan();
        return searchRequests;
    }


}
