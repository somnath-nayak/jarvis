package com.swiggy.api.sf.rng.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.dp.CarouselBannerDP;
import com.swiggy.api.sf.rng.helper.RngHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import io.gatling.core.json.Json;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import com.swiggy.api.sf.rng.constants.RngConstants;

import java.util.HashMap;

public class CarouselBannerCreate extends CarouselBannerDP {

    Initialize gameofthrones = new Initialize();
    RngHelper rngHelper = new RngHelper();

    @Test(dataProvider = "carouselWidgetsDP")
    public void testCarouselWidgets(String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID){
        String restID = "15";
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID,restID).ResponseValidator.GetBodyAsText();
        //System.out.println(response);
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String tid = JsonPath.read(response,"$..tid").toString().replace("[","").replace("]","").trim();
        String sid = JsonPath.read(response,"$..sid").toString().replace("[","").replace("]","").trim();
        String deviceID = JsonPath.read(response,"$..deviceId").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(tid);
        Assert.assertNotNull(sid);
        Assert.assertNotNull(deviceID);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");
    }

    @Test(dataProvider = "channelDP")
    public void testChannelEnums(String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID){
        String restID = "15";
        /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, //RedirectLink(RestaurantID) */
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID, restID).ResponseValidator.GetBodyAsText();
        //System.out.println(response);
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String tid = JsonPath.read(response,"$..tid").toString().replace("[","").replace("]","").trim();
        String sid = JsonPath.read(response,"$..sid").toString().replace("[","").replace("]","").trim();
        String deviceID = JsonPath.read(response,"$..deviceId").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(tid);
        Assert.assertNotNull(sid);
        Assert.assertNotNull(deviceID);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");

    }

    @Test(dataProvider = "typeDP")
    public void testBannerTypes(String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID){
        String restID = "15";
        /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, RedirectLink(RestaurantID) */
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID,restID).ResponseValidator.GetBodyAsText();
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String tid = JsonPath.read(response,"$..tid").toString().replace("[","").replace("]","").trim();
        String sid = JsonPath.read(response,"$..sid").toString().replace("[","").replace("]","").trim();
        String deviceID = JsonPath.read(response,"$..deviceId").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(tid);
        Assert.assertNotNull(sid);
        Assert.assertNotNull(deviceID);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");

    }

    @Test(dataProvider = "cityPriorityCityIDDP")
    public void checkCityPriorityCityID(String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID){
        String restID = "15";
        /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, //RedirectLink(RestaurantID) */
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID, restID).ResponseValidator.GetBodyAsText();
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String tid = JsonPath.read(response,"$..tid").toString().replace("[","").replace("]","").trim();
        String sid = JsonPath.read(response,"$..sid").toString().replace("[","").replace("]","").trim();
        String deviceID = JsonPath.read(response,"$..deviceId").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        System.out.println("Status:" + status);
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(tid);
        Assert.assertNotNull(sid);
        Assert.assertNotNull(deviceID);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");
    }

    @Test(dataProvider = "enabledRestVisibilityUserRestrictDP")
    public void checkEnabledRestVisibilityUserRestrictDP(String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID) {
        String restID = "15";
        /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, //RedirectLink(RestaurantID) */
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID, restID).ResponseValidator.GetBodyAsText();
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String tid = JsonPath.read(response,"$..tid").toString().replace("[","").replace("]","").trim();
        String sid = JsonPath.read(response,"$..sid").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        String deviceID = JsonPath.read(response,"$..deviceId").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(tid);
        Assert.assertNotNull(sid);
        Assert.assertNotNull(deviceID);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");

    }

    @Test(dataProvider = "validateCarouselWithAggregatorForRestaurantDP")
    public void validateCarouselWithAggregatorRestaurant(String lat, String lng,String startDT, String endDT, String priority , String channel, String carousel, String type, String isEnabled, String restVisible, String userRestrict, String cityID ) {
        //Longitude, Latitude, Channel
        String responseAgg1 = rngHelper.aggregatorHelp(lat,lng, channel).ResponseValidator.GetBodyAsText();
        String restId = JsonPath.read(responseAgg1,"$..sortedRestaurants[0]..restaurantId").toString().replace("[","").replace("]","").trim();
        /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, //RedirectLink(RestaurantID) */
        String response=rngHelper.carouselBannerHelp(startDT,endDT,priority,channel,carousel,type,isEnabled, restVisible,userRestrict,cityID, restId).ResponseValidator.GetBodyAsText();
        //validate carousal response
        String message = JsonPath.read(response, "$..statusMessage").toString().replace("[","").replace("]","").trim();
        String status = JsonPath.read(response,"$..statusCode").toString().replace("[","").replace("]","").trim();
        String data = JsonPath.read(response,"$..data").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(status,RngConstants.statusCodeZero, "Status Code is not Valid");
        Assert.assertEquals("\"Banner created successfully!\"",message);
        Assert.assertNotNull(data);
        Assert.assertNotEquals(data,RngConstants.valueZero,"Data field has Zero Value");
        String responseAgg2 = rngHelper.aggregatorHelp(lat,lng, channel).ResponseValidator.GetBodyAsText();
        String bannerId = JsonPath.read(responseAgg2, "$..carousels[0]..bannerId").toString().replace("[","").replace("]","").trim();
        String validateType = JsonPath.read(responseAgg2, "$..carousels[0]..type").toString().replace("[","").replace("]","").trim();
        String validateRestId = JsonPath.read(responseAgg2, "$..carousels[0]..link").toString().replace("[","").replace("]","").trim();
        String validateCityId = JsonPath.read(responseAgg2, "$..carousels[0]..cityId").toString().replace("[","").replace("]","").trim();
        String validatePriority = JsonPath.read(responseAgg2, "$..carousels[0]..priority").toString().replace("[","").replace("]","").trim();
        String validateCarouselType = JsonPath.read(responseAgg2, "$..carousels[0]..metaType").toString().replace("[","").replace("]","").trim();
        //String validatesid = JsonPath.read(responseAgg2, "$..carousels[0]..priority").toString().replace("[","").replace("]","").trim();
        Assert.assertEquals(data,bannerId,"Banner ID is different from the Data field");
        Assert.assertEquals(type, validateType, "Type Not Similar");
        Assert.assertEquals(cityID,validateCityId, "CityId Not Similar");
        Assert.assertEquals(carousel, validateCarouselType, "Carousel Type  Not Similar");
        Assert.assertEquals(priority, validatePriority,"Priority Value Not Similar");
        Assert.assertEquals(restId, validateRestId,"Restaurant ID Not Similar");
    }
}
