package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "mealId",
        "operationMeta",
        "rewards",
        "groups"
})
public class Meal {

    @JsonProperty("mealId")
    private Integer mealId;
    @JsonProperty("operationMeta")
    private OperationMeta operationMeta;
    @JsonProperty("rewards")
    private List<Reward> rewards = null;
    @JsonProperty("groups")
    private List<Group> groups = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Meal() {
    }

    /**
     *
     * @param operationMeta
     * @param mealId
     * @param rewards
     * @param groups
     */
    public Meal(Integer mealId, OperationMeta operationMeta, List<Reward> rewards, List<Group> groups) {
        super();
        this.mealId = mealId;
        this.operationMeta = operationMeta;
        this.rewards = rewards;
        this.groups = groups;
    }

    @JsonProperty("mealId")
    public Integer getMealId() {
        return mealId;
    }

    @JsonProperty("mealId")
    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("operationMeta")
    public OperationMeta getOperationMeta() {
        return operationMeta;
    }

    @JsonProperty("operationMeta")
    public void setOperationMeta(OperationMeta operationMeta) {
        this.operationMeta = operationMeta;
    }

    @JsonProperty("rewards")
    public List<Reward> getRewards() {
        return rewards;
    }

    @JsonProperty("rewards")
    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

    @JsonProperty("groups")
    public List<Group> getGroups() {
        return groups;
    }

    @JsonProperty("groups")
    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mealId", mealId).append("operationMeta", operationMeta).append("rewards", rewards).append("groups", groups).toString();
    }

}