package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.helper.Utility;
import io.advantageous.boon.core.Str;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

public class Category extends RestaurantList{

    private String id;
    private String name;
    private List<SubCategory> subCategories = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Category() {
    }

    /**
     *
     * @param id
     * @param subCategories
     * @param name
     */
    public Category(String id, String name, List<SubCategory> subCategories) {
        super();
        this.id = id;
        this.name = name;
        this.subCategories = subCategories;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public Category withCategory(String categoryId){
        this.id = categoryId;
        this.name = "testCategory";
        return this;
    }

    public Category withSubCategory(String categoryId ,String subCategoryId, List<Menu> menuList) {
        SubCategory subCategory = new SubCategory();
        List<SubCategory> list = new ArrayList<SubCategory>();
        list.add(0,subCategory.withItem(subCategoryId, menuList));
        this.id = categoryId;
        this.name = "testCategory";
        this.subCategories =list;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("subCategories", subCategories).toString();
    }

}