package com.swiggy.api.sf.rng.dp.SuperSaving;

import com.swiggy.api.sf.rng.helper.SuperHelper;
import com.swiggy.api.sf.rng.helper.SuperSavingHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.CreateCouponPOJO;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings.TradeDiscountBreakup;
import com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings.TradeDiscountBreakupBuilder;
import org.testng.annotations.DataProvider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SuperSavingDP {
    static SuperSavingHelper superSavinghelper = new SuperSavingHelper();
    static SuperHelper superhelper = new SuperHelper();



    @DataProvider(name = "publicUserSubscriptionFreeDelData")
    public static Object[][] publicUserSubscriptionFreeDel() {

        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(superSavinghelper.createPublicPlan("0","3","1","99","55"), numOfPlans);
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1=superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));



        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails}, {planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails}};
    }

    @DataProvider(name = "mappedUserSubsCriptionWithIncentivePercentageData")
    public static Object[][] mappedUserSubscriptionWithIncentivePercentage() {

        //TESTDATA -1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0","3","1","99","5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("PERCENTAGE","10","10","null","0","100","true");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        //Testdata 1 Free delivery & Percentage
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");


        //TESTDATA-2 FreeBie & Percentage
        HashMap<String, String> createBenefit1 = new HashMap<String, String>();
        createBenefit1.put("0", "Freebie");
        createBenefit1.put("1", "1");
        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder2 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder2.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup2 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder2.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList1 = new ArrayList<>();
        tdBuilderPercentageList1.add(tdbreakup2);
        tdBuilderPercentageList1.add(tdbreakup3);

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails},
                {planPayload[0], numOfPlans, benefitPayload1, createIncentive, tdBuilderPercentageList1, orderdetails},
        };
    }

    @DataProvider(name = "mappedUserSubscriptionWithIncentivePercentageMultipleOrdersData")
    public static Object[][] mappedUserSubscriptionWithIncentivePercentageMultipleOrders() {

        //TESTDATA-1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0","3","1","99","5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("PERCENTAGE","10","10","null","0","100","true");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails}};
    }


    @DataProvider(name = "mappedUserSubscriptionWithIncentiveFlatMultipleOrdersData")
    public static Object[][] mappedUserSubscriptionWithIncentiveFlatMultipleOrders() {

        //TESTDATA-1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0","3","1","99","5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("FLAT","10","10","null","0","100","true");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("FLAT").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);


        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");
        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails}};

    }


    @DataProvider(name = "mappedUserSubscriptionFreeBieWithIncentivePercentageMultipleOrdersData")
    public static Object[][] mappedUserSubscriptionFreeBieWithIncentivePercentageMultipleOrders() {
        //TESTDATA-1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0","3","1","99","5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> createIncentive =superSavinghelper.createIncentive("PERCENTAGE","10","10","null","0","100","true");
        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");
        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails}};
    }


    @DataProvider(name = "mappedUserSubscriptionFreeBieWithIncentiveFlatMultipleOrdersData")
    public static Object[][] mappedUserSubscriptionFreeBieWithIncentiveFlatMultipleOrders() {

        //TESTDATA-1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0","3","1","99","5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("FLAT","10","10","null","0","100","true");
        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("FLAT").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");
        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails}};

    }


    @DataProvider(name = "mappedUserSubsCriptionWithIncentiveFlatData")
    public static Object[][] mappedUserSubsCriptionWithIncentiveFlat() {

        //TESTDATA -1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan(   "0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("FLAT", "10", "10", "null", "0", "100", "true");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        //Testdata 1 Free delivery & Percentage
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("FLAT").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");

        //TESTDATA-2 FreeBie & Percentage
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder2 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder2.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup2 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder2.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("FLAT").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList1 = new ArrayList<>();
        tdBuilderPercentageList1.add(tdbreakup2);
        tdBuilderPercentageList1.add(tdbreakup3);

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails},
                {planPayload[0], numOfPlans, benefitPayload1, createIncentive, tdBuilderPercentageList1, orderdetails}
        };
    }


    @DataProvider(name = "nonIncentiveFreeDeliverOrFreebieForSuperUserWithCouponCodeData")
    public static Object[][] nonIncentiveFreeDeliverOrFreebieForSuperUserWithCouponCode() {

        HashMap<String, Object> coupon_map = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withIsPrivate(0)
                .withCustomerRestriction(0)
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withDiscountPercentage(0)
                .withDiscountAmount(20)
                .withCouponUserTypeId(1)
                .withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
                .withDescription("Test")
                .withCode("SUPER" + Utility.getRandomPostfix());
        coupon_map.put("createCouponPOJO", createCouponPOJO);

        String coupon_code= createCouponPOJO.getCode();
        String coupon_discountamout= String.valueOf(createCouponPOJO.getDiscountAmount());


        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 =  superSavinghelper.createBenefit("Freebie","1");


        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));



        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular",coupon_code, String.valueOf(createCouponPOJO.getDiscountAmount()), "SUPER");

        return new Object[][]{{coupon_map,planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,coupon_discountamout},
                {coupon_map,planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,coupon_discountamout}};

    }


    @DataProvider(name = "incentiveFreeDeliverOrFreebieForSuperUserWithCouponCodeData")
    public static Object[][] incentiveFreeDeliverOrFreebieForSuperUserWithCouponCode() {

        HashMap<String, Object> coupon_map = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withIsPrivate(0)
                .withCustomerRestriction(0)
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withDiscountPercentage(0)
                .withDiscountAmount(20)
                .withCouponUserTypeId(1)
                .withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
                .withDescription("Test")
                .withCode("SUPER" + Utility.getRandomPostfix());
        coupon_map.put("createCouponPOJO", createCouponPOJO);

        String coupon_code= createCouponPOJO.getCode();
        String coupon_discountamout= String.valueOf(createCouponPOJO.getDiscountAmount());



        // type, value, cap, paymentMethods, tenure, priority, enabled
        HashMap<String, String> createIncentive =superSavinghelper.createIncentive("PERCENTAGE", "10", "10", "null", "0", "100", "true");

        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");


        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
       // Percentage TD
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();

        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);
        tdBuilderList.add(tdbreakup3);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();


        tdBuilderList1.add(tdbreakup);
        tdBuilderList1.add(tdbreakup3);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular",coupon_code, String.valueOf(createCouponPOJO.getDiscountAmount()),"SUPER");

        return new Object[][]{{coupon_map,planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,coupon_discountamout,createIncentive},
                {coupon_map,planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,coupon_discountamout,createIncentive}};

    }


    @DataProvider(name = "incentiveFlatFreeDeliverOrFreebieForSuperUserWithCouponCodeFlat")
    public static Object[][] incentiveFlatFreeDeliverOrFreebieForSuperUserWithCouponCode() {

        HashMap<String, Object> coupon_map = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withIsPrivate(0)
                .withCustomerRestriction(0)
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withDiscountPercentage(0)
                .withDiscountAmount(20)
                .withCouponUserTypeId(1)
                .withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
                .withDescription("Test")
                .withCode("SUPER" + Utility.getRandomPostfix());
        coupon_map.put("createCouponPOJO", createCouponPOJO);

        String coupon_code= createCouponPOJO.getCode();
        String coupon_discountamout= String.valueOf(createCouponPOJO.getDiscountAmount());



        // type, value, cap, paymentMethods, tenure, priority, enabled
        HashMap<String, String> createIncentive =superSavinghelper.createIncentive("FLAT", "10", "10", "null", "0", "100", "true");

        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan( "0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");


        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        // Percentage TD
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();

        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);
        tdBuilderList.add(tdbreakup3);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();


        tdBuilderList1.add(tdbreakup);
        tdBuilderList1.add(tdbreakup3);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));
        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular",coupon_code , String.valueOf(createCouponPOJO.getDiscountAmount()), "SUPER");

        return new Object[][]{{coupon_map,planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,coupon_discountamout,createIncentive},
                {coupon_map,planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,coupon_discountamout,createIncentive}};

    }



    @DataProvider(name = "incentiveFlatFreeDeliverFreebieForSuperUserWithCouponCodeData")
    public static Object[][] incentiveFlatFreeDeliverFreebieForSuperUserWithCouponCode() {

        HashMap<String, Object> coupon_map = new HashMap<String, Object>();
        CreateCouponPOJO createCouponPOJO = new CreateCouponPOJO()
                .setDefaultData()
                .withIsPrivate(0)
                .withCustomerRestriction(0)
                .withValidFrom(Utility.getCurrentDate())
                .withValidTill(Utility.getFutureDate())
                .withCouponType("Discount")
                .withDiscountPercentage(0)
                .withDiscountAmount(20)
                .withCouponUserTypeId(1)
                .withPreferredPaymentMethod("Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb")
                .withDescription("Test")
                .withCode("SUPER" + Utility.getRandomPostfix());
        coupon_map.put("createCouponPOJO", createCouponPOJO);

        String coupon_code= createCouponPOJO.getCode();
        String coupon_discountamout= String.valueOf(createCouponPOJO.getDiscountAmount());


        // type, value, cap, paymentMethods, tenure, priority, enabled
        HashMap<String, String> createIncentive =superSavinghelper.createIncentive("FLAT", "10", "10", "null", "0", "100", "true");

        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");

        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

         String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();

        // FLAT TD
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Flat").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();

        //TESTDATA-1
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);
        tdBuilderList.add(tdbreakup1);
        tdBuilderList.add(tdbreakup3);

        //TESTDATA-2
        // Percentage TD
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder4 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder4.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup4 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder4.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);
        tdBuilderList1.add(tdbreakup1);
        tdBuilderList1.add(tdbreakup4);



        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular",coupon_code,String.valueOf(createCouponPOJO.getDiscountAmount()),"SUPER");

        return new Object[][]{{coupon_map,planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,coupon_discountamout,createIncentive},
                {coupon_map,planPayload[0], numOfPlans, benefitPayload, tdBuilderList1, orderdetails,coupon_discountamout,createIncentive}};

    }



    /***WAS SUPER***/
    @DataProvider(name = "wasSuperPublicUserSubscriptionFreeDelData")
    public static Object[][] wasSuperPublicUserSubscriptionFreeDel() {

        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> wasSuperPlan = superSavinghelper.createWasSuperPlan("true","1526807885966","1");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));



        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,wasSuperPlan}, {planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,wasSuperPlan}};
    }



    @DataProvider(name = "freeDelFreebieAndPercentageWasSuperTestData")
    public static Object[][] freeDelFreebieAndPercentageWasSuper() {

        //TESTDATA -1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan( "0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("PERCENTAGE", "10", "10", "null", "0", "100", "true");
        HashMap<String, String> wasSuperPlan = superSavinghelper.createWasSuperPlan("true","1526807885966","1");

        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));

        //Testdata 1 Free delivery & Percentage
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails =superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");


        //TESTDATA-2 FreeBie & Percentage
        HashMap<String, String> createBenefit1 =superSavinghelper.createBenefit("Freebie","1");

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder2 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder2.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup2 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder2.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Percentage").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList1 = new ArrayList<>();
        tdBuilderPercentageList1.add(tdbreakup2);
        tdBuilderPercentageList1.add(tdbreakup3);

        HashMap<String, String> orderdetails_wassuper = superSavinghelper.createOrderDetailsMap("regular","","0","");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails,wasSuperPlan,orderdetails_wassuper},
                {planPayload[0], numOfPlans, benefitPayload1, createIncentive, tdBuilderPercentageList1, orderdetails,wasSuperPlan,orderdetails_wassuper},
        };
    }



    @DataProvider(name = "freeDelFreebieAndFlatWasSuperTestData")
    public static Object[][] freeDelFreebieAndFlatWasSuperTest() {

        //TESTDATA -1
        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit =superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createIncentive = superSavinghelper.createIncentive("FLAT", "10", "10", "null", "0", "100", "true");
        HashMap<String, String> wasSuperPlan = superSavinghelper.createWasSuperPlan("true","1526807885966","1");

        String numOfPlans = "1";
        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);
        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));


        //Testdata 1 Free delivery & Percentage
        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder1.withCampaignId(9).withRewardType("Flat").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();


        List<TradeDiscountBreakup> tdBuilderPercentageList = new ArrayList<>();
        tdBuilderPercentageList.add(tdbreakup);
        tdBuilderPercentageList.add(tdbreakup1);

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");

        //TESTDATA-2 FreeBie & Percentage
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder2 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder2.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup2 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder2.build();

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder3 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder3.withCampaignId(9).withRewardType("Flat").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup3 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder3.build();

        List<TradeDiscountBreakup> tdBuilderPercentageList1 = new ArrayList<>();
        tdBuilderPercentageList1.add(tdbreakup2);
        tdBuilderPercentageList1.add(tdbreakup3);

        HashMap<String, String> orderdetails_wassuper = superSavinghelper.createOrderDetailsMap("regular","","0","");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, createIncentive, tdBuilderPercentageList, orderdetails,wasSuperPlan,orderdetails_wassuper},
                {planPayload[0], numOfPlans, benefitPayload1, createIncentive, tdBuilderPercentageList1, orderdetails,wasSuperPlan,orderdetails_wassuper},
        };
    }


    @DataProvider(name = "nonIncentiveFreeDeliverOrFreebieForSuperUserAndSubscriptionExpiredData")
    public static Object[][] nonIncentiveFreeDeliverOrFreebieForSuperUserAndSubscriptionExpired() {

        HashMap<String, String> createPublicPlan =superSavinghelper.createPublicPlan( "0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 =superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> wasSuperPlan = superSavinghelper.createWasSuperPlan("true","1526807885966","1");
        String numOfPlans = "1";


        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);

        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));

        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");
        HashMap<String, String> orderdetails_for_wassuper =superSavinghelper.createOrderDetailsMap("regular","","0","");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,wasSuperPlan,orderdetails_for_wassuper}, {planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,wasSuperPlan,orderdetails_for_wassuper}};
    }


    @DataProvider(name = "superWasSuperToSuperFreeDeliverOrFreebieData")
    public static Object[][] superWasSuperToSuperFreeDeliverOrFreebie() {

        HashMap<String, String> createPublicPlan = superSavinghelper.createPublicPlan("0", "3", "1", "99", "5");
        HashMap<String, String> createBenefit = superSavinghelper.createBenefit("FREE_DELIVERY","1");
        HashMap<String, String> createBenefit1 = superSavinghelper.createBenefit("Freebie","1");
        HashMap<String, String> wasSuperPlan = superSavinghelper.createWasSuperPlan("true","1526807885966","1");
        String numOfPlans = "1";

        String[] planPayload = superhelper.populateActivePlanPayloadWithDefaultValues(createPublicPlan, numOfPlans);


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("FREE_DELIVERY").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup = (TradeDiscountBreakup) tradeDiscountBreakupBuilder.build();
        List<TradeDiscountBreakup> tdBuilderList = new ArrayList<>();
        tdBuilderList.add(tdbreakup);

        String benefitPayload = superhelper.populateBenefitData(createBenefit.get(Integer.toString(0)),
                createBenefit.get(Integer.toString(1)));


        TradeDiscountBreakupBuilder tradeDiscountBreakupBuilder1 = new TradeDiscountBreakupBuilder();
        tradeDiscountBreakupBuilder.withCampaignId(9).withRewardType("Freebie").withDiscountAmount(10).withIsSuper(Boolean.TRUE).withDiscountBreakup(null);
        TradeDiscountBreakup tdbreakup1 = (TradeDiscountBreakup) tradeDiscountBreakupBuilder1.build();
        List<TradeDiscountBreakup> tdBuilderList1 = new ArrayList<>();
        tdBuilderList1.add(tdbreakup);

        String benefitPayload1 = superhelper.populateBenefitData(createBenefit1.get(Integer.toString(0)),
                createBenefit1.get(Integer.toString(1)));
        HashMap<String, String> orderdetails = superSavinghelper.createOrderDetailsMap("regular","","0","SUPER");

        return new Object[][]{{planPayload[0], numOfPlans, benefitPayload, tdBuilderList, orderdetails,wasSuperPlan},
                {planPayload[0], numOfPlans, benefitPayload1, tdBuilderList1, orderdetails,wasSuperPlan}};
    }

}