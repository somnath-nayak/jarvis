package com.swiggy.api.sf.rng.pojo.edvo;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class EDVOMealPOJO {

    @JsonProperty("meals")
    private List<EDVOMeal> meals;

    @JsonProperty("restaurantIds")
    private Integer[] restaurantIds;

    @JsonProperty("userAgent")
    private String userAgent = "ANDROID";

    @JsonProperty("versionCode")
    private String versionCode = "400";

    @JsonProperty("restaurantFirstOrder")
    private boolean restaurantFirstOrder;

    @JsonProperty("minCartAmount")
    private long minCartAmount;

    @JsonProperty("firstOrder")
    private boolean firstOrder = false;

    @JsonProperty("userId")
    private long userId;

    public EDVOMealPOJO() {
    }

    public List<EDVOMeal> getMeals() {
        return meals;
    }

    public void setMeals(List<EDVOMeal> meals) {
        this.meals = meals;
    }

    public Integer[] getRestaurantIds() {
        return restaurantIds;
    }

    public void setRestaurantIds(Integer[] restaurantIds) {
        this.restaurantIds = restaurantIds;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public boolean isFirstOrder() {
        return firstOrder;
    }

    public void setFirstOrder(boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public boolean isRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    public void setRestaurantFirstOrder(boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    public long getMinCartAmount() {
        return minCartAmount;
    }

    public void setMinCartAmount(long minCartAmount) {
        this.minCartAmount = minCartAmount;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public EDVOMealPOJO withMeals(List<EDVOMeal> mealList) {

        this.setMeals(mealList);
        return this;
    }

    public EDVOMealPOJO withRestaurantIds(Integer[] restaurant_ids) {
        this.setRestaurantIds(restaurant_ids);
        return this;
    }

    public EDVOMealPOJO withVersionCode(String versionCode) {
        this.setVersionCode(versionCode);
        return this;
    }

    public EDVOMealPOJO withUserAgent(String userAgent) {
        this.setUserAgent(userAgent);
        return this;
    }

    public EDVOMealPOJO withMincartAmount(long minCartAmount) {
        this.setMinCartAmount(minCartAmount);
        return this;
    }

    public EDVOMealPOJO withRestaurantFirstOrder(boolean restaurantFirstOrder) {
        this.setRestaurantFirstOrder(restaurantFirstOrder);
        return this;
    }

    public EDVOMealPOJO withUserId(long userId) {
        this.setUserId(userId);
        return this;
    }


    public EDVOMealPOJO setDefaultData(List<EDVOMeal> mealList, Integer[] restaurantIds, long minCartAmount, boolean firstOrder, boolean restaurantFirstOrder, long userId) {

        this.setMeals(mealList);
        this.setRestaurantIds(restaurantIds);
        this.setMinCartAmount(minCartAmount);
        this.setFirstOrder(firstOrder);
        this.setRestaurantFirstOrder(restaurantFirstOrder);
        this.setUserId(userId);
        this.setUserAgent("ANDROID");
        this.setVersionCode("400");
        return this;

    }

}