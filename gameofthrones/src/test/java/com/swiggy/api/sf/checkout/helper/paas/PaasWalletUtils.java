package com.swiggy.api.sf.checkout.helper.paas;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;

public class PaasWalletUtils {

    MySmsValidator mySmsValidator = new MySmsValidator();
    PaasWalletValidator paasWalletValidator = new PaasWalletValidator();
    PaasHelper paasHelper=new PaasHelper();

    public double linkAndCheckWalletBalance(String tid,String token,String paymentMethod){

        String authToken = mySmsValidator.getAuthToken();

        if(paymentMethod.toLowerCase().contains("mobikwik")){
             return getMobikwikBalance(tid,token,authToken);
            } else if(paymentMethod.toLowerCase().contains("freecharge")){
            return getFreechargeBalance(tid,token,authToken);
        } else if(paymentMethod.toLowerCase().contains("paytm")){
            return getPaytmBalance(tid,token,authToken);
        }  else if(paymentMethod.toLowerCase().contains("phonepe")){
            return getPhonepeBalance(tid,token,authToken);
        }
        return 0.0;
    }



    private void invokeMobikwikLinkApi(String tid,String token,String authToken) {

        int statusCode = paasWalletValidator.callMobikwikCheckBalance(tid, token);
        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);
            paasWalletValidator.validateMobikwikLink(tid, token);
        } else {
            paasWalletValidator.callMobikwikDelink(tid, token);
            invokeMobikwikLinkApi(tid,token,authToken);
        }
    }

    private double getMobikwikBalance(String tid,String token,String authToken){

        Processor processor=paasHelper.mobikwikCheckBalance(tid,token);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (statusCode==0){
            return Double.parseDouble(processor.ResponseValidator.GetNodeValue("$.data.balanceamount"));
        } else {

            invokeMobikwikLinkApi(tid, token, authToken);
            String otp = mySmsValidator.getOTPFromSMS(authToken, PaasConstant.MOBIKWIK_ADDRESS);

            int retry = 0;
            while (otp == null && retry < PaasConstant.MAX_RETRY) {
                invokeMobikwikLinkApi(tid, token, authToken);
                otp = mySmsValidator.getOTPFromSMS(authToken, PaasConstant.MOBIKWIK_ADDRESS);
                retry++;
            }
            paasWalletValidator.validatemobikwikOtp(tid, token, otp);
            Processor processor1 = paasWalletValidator.validateMobikwikCheckBalance(tid, token);
            return Double.parseDouble(processor1.ResponseValidator.GetNodeValue("$.data.balanceamount"));
        }
    }


    public String invokeFreeChargeLinkApi(String tid,String token,String authToken) {
        int statusCode = paasWalletValidator.callfreechargeCheckBalance(tid, token);
        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);

            Processor processor = paasWalletValidator.validatefreechargeLink(tid,token);
            String otpId =processor.ResponseValidator.GetNodeValue("$.data.otpId");
            Assert.assertNotNull(otpId,"OTP-ID is null in FreeChargeLinkApi response");
            return otpId;
        } else {
            paasWalletValidator.validatefreechargeDelink(tid, token);
            invokeFreeChargeLinkApi(tid,token,authToken);
        }
        return null;
    }


    private double getFreechargeBalance(String tid,String token,String authToken){

        Processor processor=paasHelper.freechargeCheckBalance(tid,token);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (statusCode==0){
            return Double.parseDouble(processor.ResponseValidator.GetNodeValue("$.data.walletBalance"));
        } else {

        String otpId=invokeFreeChargeLinkApi(tid,token,authToken);
        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
        int retry = 0;
        while (otp==null && retry < PaasConstant.MAX_RETRY){
            paasWalletValidator.validateFreechargeResendOtp(tid,token,otpId);
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatefreechargeOtp(tid, token, otpId,otp);

        Processor processor1=paasWalletValidator.validatefreechargeCheckBalance(tid, token);
        return Double.parseDouble(processor1.ResponseValidator.GetNodeValue("$.data.walletBalance"));
    }}


    private String invokePaytmLinkApi(String tid,String token,String authToken) {
        String state=null;
        int statusCode = paasWalletValidator.callPaytmCheckBalance(tid, token);
        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);

            Processor processor = paasWalletValidator.validatePaytmLink(tid,token);
            state =processor.ResponseValidator.GetNodeValue("$.data.state");
            Assert.assertNotNull(state);

           //paasWalletValidator.validatePaymentAPIBasicResponseData(processor,tid);
        } else {
           // paasWalletValidator.callPaytmDelink(tid, token);
            //invokePaytmLinkApi(tid,token,authToken);
        }
        return state;
    }


    private double getPaytmBalance(String tid,String token,String authToken){

        Processor processor=paasHelper.paytmCheckBalance(tid,token);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        if (statusCode == 0) {
        return Double.parseDouble(processor.ResponseValidator.GetNodeValue("$.data.walletBalance"));
        } else {
        String state=invokePaytmLinkApi(tid,token,authToken);

        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PAYTM_ADDRESS);
        int retry = 0;

        while ((otp==null || state==null) && retry < PaasConstant.MAX_RETRY){
            state=invokePaytmLinkApi(tid,token,authToken);
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PAYTM_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatePaytmOtp(tid, token, state,otp);

        Processor processor1=paasWalletValidator.validatePaytmCheckBalance(tid, token);
        return Double.parseDouble(processor1.ResponseValidator.GetNodeValue("$.data.walletBalance"));}
    }

    private void invokePhonePeLinkApi(String tid,String token,String authToken) {
        int statusCode  = paasWalletValidator.callPhonepeIsLink(tid, token);

        if (statusCode != 0) {
            mySmsValidator.deleteAllConversation(authToken);
            paasWalletValidator.validatePhonepeSendOtp(tid,token);
        } else {
            paasWalletValidator.callPhonepeDelink(tid, token);
            invokePhonePeLinkApi(tid,token,authToken);
        }
    }

    private double getPhonepeBalance(String tid,String token,String authToken){

        int statusCode  = paasWalletValidator.callPhonepeIsLink(tid, token);
        if (statusCode==0){
            Processor processor=paasHelper.phonepeCheckBalance(tid,token);
            return (double) processor.ResponseValidator.GetNodeValueAsInt("$.data.wallet.availableBalance");
        } else {
        invokePhonePeLinkApi(tid,token,authToken);

        String otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.PHONEPE_ADDRESS);
        int retry = 0;

        while (otp==null && retry < PaasConstant.MAX_RETRY){
            paasWalletValidator.validatePhonepeResendOtp(tid,token);
            otp = mySmsValidator.getOTPFromSMS(authToken,PaasConstant.FREECHARGE_ADDRESS);
            retry++;
        }
        paasWalletValidator.validatePhonepeOtp(tid,token,otp);
        paasWalletValidator.validatePhonepeIsLink(tid, token);

        Processor processor1=paasWalletValidator.validatePhonepeCheckBalance(tid, token);
        return (double) processor1.ResponseValidator.GetNodeValueAsInt("$.data.wallet.availableBalance");

    }}


}
