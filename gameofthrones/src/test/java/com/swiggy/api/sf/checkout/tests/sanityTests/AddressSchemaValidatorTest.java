package com.swiggy.api.sf.checkout.tests.sanityTests;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import com.swiggy.api.sf.checkout.dp.AddressSchemaValidationDP;
import com.swiggy.api.sf.checkout.helper.AddressHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CheckoutPricingHelper;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;


public class AddressSchemaValidatorTest extends AddressSchemaValidationDP{

    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    CheckoutHelper helper=new CheckoutHelper();
    CheckoutPricingHelper pricingHelper = new CheckoutPricingHelper();
    AddressHelper addressHelper=new AddressHelper();
    String AddressId;



    String tid;
    String token;
    HashMap<String, String> hashMap;
    String schemaPath = "Data/SchemaSet/Json/Checkout/";

    @BeforeClass
    public void login() {
        hashMap = helper.TokenData("8197982351", "welcome123");
        tid = hashMap.get("Tid");
        token = hashMap.get("Token");
    }


    @Test(dataProvider = "GetAllAddressSchemaValidator", groups = {"schemaValidator"},priority = 0)
    public void getAllAddressTest(String userAgent, String versionCode,
                                                String fileName)
            throws IOException, ProcessingException {

        String getAllAddress=getAllAddress(userAgent,versionCode);
        System.out.println(getAllAddress);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(getAllAddress,expectedSchema,"Get All Address");

    }

    @Test(dataProvider = "isAddressServiceableSchemaValidator", groups = {"schemaValidator"},priority = 4)
    public void isAddressServiceable(String userAgent, String versionCode,String lat, String lng,
                                     String fileName)
            throws IOException, ProcessingException {

        String ServiceableAddress=addressServiceable(userAgent,versionCode,lat,lng);
        System.out.println(ServiceableAddress);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println("expected Schema"+expectedSchema);
        validateSchema(ServiceableAddress,expectedSchema,"Check Serviceable address");

    }

    @Test(dataProvider = "AddNewAddressSchemaValidator", groups = {"schemaValidator"},priority = 1)
    public void addNewAddressTest(String userAgent,String versionCode,String mobile1,String password, String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode,String fileName)
            throws IOException, ProcessingException {



        String addNewAddress=AddNewAddress1(userAgent,versionCode, mobile1, password,name,mobile, address,  landmark,  area,  lat, lng,  flat_no,  city,annotation,  message,  statusCode);
        System.out.println("UAT-01"+addNewAddress);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(addNewAddress,expectedSchema,"Add New Address");

    }

    @Test(dataProvider = "updateAddressSchemaValidator", groups = {"schemaValidator"},priority = 2)
    public void updateAddress(String userAgent, String versionCode,String name, String mobile, String address, String landmark1, String area, String lat, String lng, String flat_no, String city, String annotation,String fileName)

            throws IOException, ProcessingException {
        if (AddressId != null) {

        String UpdateAddress=updateAddressfunc(userAgent,versionCode, AddressId, name, mobile, address, landmark1, area, lat, lng, flat_no,  city, annotation);
        System.out.println(UpdateAddress);
        String expectedSchema = getSchemafromFile(schemaPath,fileName);
        System.out.println(expectedSchema);
        validateSchema(UpdateAddress,expectedSchema,"Update address");}

    }




    @Test(dataProvider = "deleteAddressSchemaValidator", groups = {"schemaValidator"},priority = 3)
    public void deleteAddressTest(String userAgent, String versionCode,
                                     String fileName)
            throws IOException, ProcessingException {

        if (AddressId != null) {

            String DeleteAddress = deleteAddress(userAgent, versionCode, AddressId);
            System.out.println(DeleteAddress);
            String expectedSchema = getSchemafromFile(schemaPath, fileName);
            System.out.println(expectedSchema);
            validateSchema(DeleteAddress, expectedSchema, "Check Serviceable address");
        }

    }










    public String getAllAddress(String userAgent, String versionCode){
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  addressHelper.GetAllAdress(header).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }


    public String AddNewAddress1(String userAgent,String versionCode,String mobile1,String password,  String name, String mobile, String address, String landmark, String area, String lat, String lng, String flat_no, String city,String annotation, String message, int statusCode)
    {
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);
        String cartresponse= addressHelper.NewAddress(header,name, mobile, address,landmark,area ,lat, lng, flat_no,city,annotation).ResponseValidator.GetBodyAsText();
        AddressId= JsonPath.read(cartresponse, "$.data..address_id").toString().replace("[", "").replace("]","");
        String statusMessage= JsonPath.read(cartresponse, "$.statusMessage").toString().replace("[", "").replace("]","");
        String statCode= JsonPath.read(cartresponse, "$.statusCode").toString().replace("[", "").replace("]","");
        Assert.assertEquals(message,statusMessage);
        Assert.assertEquals(statusCode, Integer.parseInt(statCode));
        Assert.assertNotNull(AddressId);
        String getAllResponse= addressHelper.GetAllAdress(header).ResponseValidator.GetBodyAsText();
        String AddressIdList= JsonPath.read(getAllResponse, "$.data.addresses..id").toString().replace("[", "").replace("]","");
        //Assert.assertEquals(AddressId, AddressIdList.);
        Assert.assertTrue((AddressIdList).contains(AddressId));
        return cartresponse;
    }

    public String addressServiceable(String userAgent, String versionCode, String lat, String lng){
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  addressHelper.Addresservicable(header,lat,lng).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }



    public String deleteAddress(String userAgent, String versionCode, String addressId){
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  addressHelper.DeleteAddress(header,addressId).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }

    public String updateAddressfunc(String userAgent, String versionCode, String addressId,String name, String mobile, String address, String landmark1, String area, String lat, String lng, String flat_no, String city, String annotation){
        HashMap<String, String> header=setCompleteHeader(tid, token, userAgent, versionCode);

        String cartResponse=  addressHelper.UpdateAddress(header,addressId,name,mobile, address, landmark1, area, lat, lng, flat_no, city, annotation).ResponseValidator.GetBodyAsText();

        return cartResponse;
    }






    public HashMap<String, String> setCompleteHeader(String tid, String token,
                                                     String userAgent,String versionCode) {
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Authorization", CheckoutConstants.authorization);
        requestheaders.put("tid", tid);
        requestheaders.put("token", token);
        requestheaders.put("User-Agent", token);
        requestheaders.put("Version-Code", token);
        return requestheaders;
    }


    public void validateSchema(String response,String expectedSchema,String message) throws IOException, ProcessingException{
        System.out.println("Expected jsonschema   $$$$" + expectedSchema);
        List missingNodeList = schemaValidatorUtils.validateServiceSchema(expectedSchema, response);
        Assert.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For "+message+" API");
        boolean isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
        System.out.println("Contain empty nodes=>" + isEmpty);
        Assert.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }




    public String getSchemafromFile(String schemaPath,String fileName) throws IOException, ProcessingException{
        String userDir = System.getProperty("user.dir");
        userDir = userDir.substring(0, userDir.indexOf("gameofthrones"));
        Reporter.log("[FILE PATH] " + userDir + schemaPath+fileName, true);
        return new ToolBox().readFileAsString(userDir + schemaPath+fileName);
    }


}







