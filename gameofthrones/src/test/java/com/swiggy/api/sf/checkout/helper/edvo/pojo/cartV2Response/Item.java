package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import com.swiggy.api.sf.checkout.helper.edvo.pojo.cmsAttributes.Attributes;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "attributes",
        "rewardType",
        "mealQuantity",
        "menu_item_id",
        "name",
        "base_price",
        "variants_price",
        "addons_price",
        "quantity",
        "subtotal",
        "subtotal_trade_discount",
        "packing_charge",
        "total",
        "final_price",
        "is_customized",
        "id_hash",
        "valid_addons",
        "valid_variants",
        "valid_variants_v2",
        "is_veg",
        "addons",
        "variants",
        "item_taxes",
        "GST_details",
        "cloudinaryImageId",
        "in_stock",
        "inventory",
        "inventory_message",
        "inventory_insufficient_message",
        "added_by_user_id",
        "added_by_user_name"
})
public class Item {

    @JsonProperty("attributes")
    private Attributes attributes;
    @JsonProperty("rewardType")
    private Object rewardType;
    @JsonProperty("mealQuantity")
    private Integer mealQuantity;
    @JsonProperty("menu_item_id")
    private Integer menuItemId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("base_price")
    private Double basePrice;
    @JsonProperty("variants_price")
    private Double variantsPrice;
    @JsonProperty("addons_price")
    private Double addonsPrice;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("subtotal")
    private Integer subtotal;
    @JsonProperty("subtotal_trade_discount")
    private Integer subtotalTradeDiscount;
    @JsonProperty("packing_charge")
    private Double packingCharge;
    @JsonProperty("packing_charges")
    private Double packingCharges;
    @JsonProperty("total")
    private Double total;
    @JsonProperty("final_price")
    private Double finalPrice;
    @JsonProperty("is_customized")
    private Object isCustomized;
    @JsonProperty("id_hash")
    private String idHash;
    @JsonProperty("valid_addons")
    private List<ValidAddon> validAddons = null;
    @JsonProperty("valid_variants")
    private Object validVariants;
    @JsonProperty("valid_variants_v2")
    private ValidVariantsV2 validVariantsV2;
    @JsonProperty("is_veg")
    private String isVeg;
    @JsonProperty("addons")
    private List<Addon> addons = null;
    @JsonProperty("variants")
    private List<Variant> variants = null;
    @JsonProperty("item_taxes")
    private ItemTaxes itemTaxes;
    @JsonProperty("GST_details")
    private GSTDetails gSTDetails;
    @JsonProperty("cloudinaryImageId")
    private String cloudinaryImageId;
    @JsonProperty("in_stock")
    private Integer inStock;
    @JsonProperty("inventory")
    private Integer inventory;
    @JsonProperty("inventory_message")
    private Object inventoryMessage;
    @JsonProperty("inventory_insufficient_message")
    private Object inventoryInsufficientMessage;
    @JsonProperty("added_by_user_id")
    private Integer addedByUserId;
    @JsonProperty("added_by_user_name")
    private String addedByUserName;
    @JsonProperty("item_id")
    private String itemId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("attributes")
    public Attributes getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(Attributes attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("rewardType")
    public Object getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(Object rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("mealQuantity")
    public Integer getMealQuantity() {
        return mealQuantity;
    }

    @JsonProperty("mealQuantity")
    public void setMealQuantity(Integer mealQuantity) {
        this.mealQuantity = mealQuantity;
    }

    @JsonProperty("menu_item_id")
    public Integer getMenuItemId() {
        return menuItemId;
    }

    @JsonProperty("menu_item_id")
    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("base_price")
    public Double getBasePrice() {
        return basePrice;
    }

    @JsonProperty("base_price")
    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    @JsonProperty("variants_price")
    public Double getVariantsPrice() {
        return variantsPrice;
    }

    @JsonProperty("variants_price")
    public void setVariantsPrice(Double variantsPrice) {
        this.variantsPrice = variantsPrice;
    }

    @JsonProperty("addons_price")
    public Double getAddonsPrice() {
        return addonsPrice;
    }

    @JsonProperty("addons_price")
    public void setAddonsPrice(Double addonsPrice) {
        this.addonsPrice = addonsPrice;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("subtotal")
    public Integer getSubtotal() {
        return subtotal;
    }

    @JsonProperty("subtotal")
    public void setSubtotal(Integer subtotal) {
        this.subtotal = subtotal;
    }

    @JsonProperty("subtotal_trade_discount")
    public Integer getSubtotalTradeDiscount() {
        return subtotalTradeDiscount;
    }

    @JsonProperty("subtotal_trade_discount")
    public void setSubtotalTradeDiscount(Integer subtotalTradeDiscount) {
        this.subtotalTradeDiscount = subtotalTradeDiscount;
    }

    @JsonProperty("packing_charge")
    public Double getPackingCharge() {
        return packingCharge;
    }

    @JsonProperty("packing_charges")
    public Double getPackingCharges() {
        return packingCharges;
    }

    @JsonProperty("packing_charge")
    public void setPackingCharge(Double packingCharge) {
        this.packingCharge = packingCharge;
    }

    @JsonProperty("packing_charges")
    public void setPackingCharges(Double packingCharges) {
        this.packingCharges = packingCharges;
    }

    @JsonProperty("total")
    public Double getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Double total) {
        this.total = total;
    }

    @JsonProperty("final_price")
    public Double getFinalPrice() {
        return finalPrice;
    }

    @JsonProperty("final_price")
    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }

    @JsonProperty("is_customized")
    public Object getIsCustomized() {
        return isCustomized;
    }

    @JsonProperty("is_customized")
    public void setIsCustomized(Object isCustomized) {
        this.isCustomized = isCustomized;
    }

    @JsonProperty("id_hash")
    public String getIdHash() {
        return idHash;
    }

    @JsonProperty("id_hash")
    public void setIdHash(String idHash) {
        this.idHash = idHash;
    }

    @JsonProperty("valid_addons")
    public List<ValidAddon> getValidAddons() {
        return validAddons;
    }

    @JsonProperty("valid_addons")
    public void setValidAddons(List<ValidAddon> validAddons) {
        this.validAddons = validAddons;
    }

    @JsonProperty("valid_variants")
    public Object getValidVariants() {
        return validVariants;
    }

    @JsonProperty("valid_variants")
    public void setValidVariants(Object validVariants) {
        this.validVariants = validVariants;
    }

    @JsonProperty("valid_variants_v2")
    public ValidVariantsV2 getValidVariantsV2() {
        return validVariantsV2;
    }

    @JsonProperty("valid_variants_v2")
    public void setValidVariantsV2(ValidVariantsV2 validVariantsV2) {
        this.validVariantsV2 = validVariantsV2;
    }

    @JsonProperty("is_veg")
    public String getIsVeg() {
        return isVeg;
    }

    @JsonProperty("is_veg")
    public void setIsVeg(String isVeg) {
        this.isVeg = isVeg;
    }

    @JsonProperty("addons")
    public List<Addon> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    @JsonProperty("item_taxes")
    public ItemTaxes getItemTaxes() {
        return itemTaxes;
    }

    @JsonProperty("item_taxes")
    public void setItemTaxes(ItemTaxes itemTaxes) {
        this.itemTaxes = itemTaxes;
    }

    @JsonProperty("GST_details")
    public GSTDetails getGSTDetails() {
        return gSTDetails;
    }

    @JsonProperty("GST_details")
    public void setGSTDetails(GSTDetails gSTDetails) {
        this.gSTDetails = gSTDetails;
    }

    @JsonProperty("cloudinaryImageId")
    public String getCloudinaryImageId() {
        return cloudinaryImageId;
    }

    @JsonProperty("cloudinaryImageId")
    public void setCloudinaryImageId(String cloudinaryImageId) {
        this.cloudinaryImageId = cloudinaryImageId;
    }

    @JsonProperty("in_stock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("in_stock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("inventory")
    public Integer getInventory() {
        return inventory;
    }

    @JsonProperty("inventory")
    public void setInventory(Integer inventory) {
        this.inventory = inventory;
    }

    @JsonProperty("inventory_message")
    public Object getInventoryMessage() {
        return inventoryMessage;
    }

    @JsonProperty("inventory_message")
    public void setInventoryMessage(Object inventoryMessage) {
        this.inventoryMessage = inventoryMessage;
    }

    @JsonProperty("inventory_insufficient_message")
    public Object getInventoryInsufficientMessage() {
        return inventoryInsufficientMessage;
    }

    @JsonProperty("inventory_insufficient_message")
    public void setInventoryInsufficientMessage(Object inventoryInsufficientMessage) {
        this.inventoryInsufficientMessage = inventoryInsufficientMessage;
    }

    @JsonProperty("added_by_user_id")
    public Integer getAddedByUserId() {
        return addedByUserId;
    }

    @JsonProperty("added_by_user_id")
    public void setAddedByUserId(Integer addedByUserId) {
        this.addedByUserId = addedByUserId;
    }

    @JsonProperty("added_by_user_name")
    public String getAddedByUserName() {
        return addedByUserName;
    }

    @JsonProperty("added_by_user_name")
    public void setAddedByUserName(String addedByUserName) {
        this.addedByUserName = addedByUserName;
    }

    @JsonProperty("item_id")
    public String getItemId() { return itemId; }

    @JsonProperty("item_id")
    public void setItemId(String itemId) { this.itemId = itemId; }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
