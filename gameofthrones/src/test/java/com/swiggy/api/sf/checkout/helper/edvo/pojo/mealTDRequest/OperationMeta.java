package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "mealHeader",
        "mealDescription"
})
public class OperationMeta {

    @JsonProperty("mealHeader")
    private String mealHeader;
    @JsonProperty("mealDescription")
    private String mealDescription;

    /**
     * No args constructor for use in serialization
     *
     */
    public OperationMeta() {
    }

    /**
     *
     * @param mealDescription
     * @param mealHeader
     */
    public OperationMeta(String mealHeader, String mealDescription) {
        super();
        this.mealHeader = mealHeader;
        this.mealDescription = mealDescription;
    }

    @JsonProperty("mealHeader")
    public String getMealHeader() {
        return mealHeader;
    }

    @JsonProperty("mealHeader")
    public void setMealHeader(String mealHeader) {
        this.mealHeader = mealHeader;
    }

    @JsonProperty("mealDescription")
    public String getMealDescription() {
        return mealDescription;
    }

    @JsonProperty("mealDescription")
    public void setMealDescription(String mealDescription) {
        this.mealDescription = mealDescription;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("mealHeader", mealHeader).append("mealDescription", mealDescription).toString();
    }

}