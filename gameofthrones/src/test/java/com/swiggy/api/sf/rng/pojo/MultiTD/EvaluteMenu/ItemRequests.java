package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import scala.Int;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "subCategoryId",
        "itemId",
        "restaurantId",
        "price",
        "count",
        "categoryId"
})
public class ItemRequests {

    @JsonProperty("subCategoryId")
    private Integer subCategoryId;
    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("restaurantId")
    private Integer restaurantId;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("categoryId")
    private Integer categoryId;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemRequests() {
    }

    /**
     *
     * @param subCategoryId
     * @param price
     * @param count
     * @param categoryId
     * @param itemId
     * @param restaurantId
     */
    public ItemRequests(Integer subCategoryId, Integer itemId, Integer restaurantId, Integer price, Integer count, Integer categoryId) {
        super();
        this.subCategoryId = subCategoryId;
        this.itemId = itemId;
        this.restaurantId = restaurantId;
        this.price = price;
        this.count = count;
        this.categoryId = categoryId;
    }

    public ItemRequests(String restaurantId, String categoryId, String subCategoryId, String itemId, String count, String price) {
        this.restaurantId = Integer.parseInt(restaurantId);
        this.categoryId = Integer.parseInt(categoryId);
        this.subCategoryId = Integer.parseInt(subCategoryId);
        this.itemId = Integer.parseInt(itemId);
        this.count = Integer.parseInt(count);
        this.price = Integer.parseInt(price);
    }


    @JsonProperty("subCategoryId")
    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    @JsonProperty("subCategoryId")
    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public ItemRequests withSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
        return this;
    }

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public ItemRequests withItemId(Integer itemId) {
        this.itemId = itemId;
        return this;
    }

    @JsonProperty("restaurantId")
    public Integer getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    public ItemRequests withRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    public ItemRequests withPrice(Integer price) {
        this.price = price;
        return this;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public ItemRequests withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("categoryId")
    public Integer getCategoryId() {
        return categoryId;
    }

    @JsonProperty("categoryId")
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public ItemRequests withCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
        return this;
    }
    public void build(String restId)
    {
        withRestaurantId(Integer.parseInt(restId));
    }

}
