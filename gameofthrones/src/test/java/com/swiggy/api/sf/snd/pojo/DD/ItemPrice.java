package com.swiggy.api.sf.snd.pojo.DD;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemPrice {

    private String id;
    private String type;
    private Integer min;
    private Integer max;
    private String fieldType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("type", type).append("min", min).append("max", max).append("fieldType", fieldType).toString();
    }

}