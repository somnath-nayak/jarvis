package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "name",
        "price",
        "id",
        "inStock",
        "order",
        "default",
        "variant_group_id",
        "default_dependent_variant"
})
public class Variation {

    @JsonProperty("name")
    private String name;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("id")
    private String id;
    @JsonProperty("inStock")
    private Integer inStock;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("variant_group_id")
    private String variantGroupId;
    @JsonProperty("default_dependent_variant")
    private DefaultDependentVariant defaultDependentVariant;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Variation() {
    }

    /**
     *
     * @param id
     * @param variantGroupId
     * @param price
     * @param order
     * @param _default
     * @param name
     * @param inStock
     * @param defaultDependentVariant
     */
    public Variation(String name, Integer price, String id, Integer inStock, Integer order, Integer _default, String variantGroupId, DefaultDependentVariant defaultDependentVariant) {
        super();
        this.name = name;
        this.price = price;
        this.id = id;
        this.inStock = inStock;
        this.order = order;
        this._default = _default;
        this.variantGroupId = variantGroupId;
        this.defaultDependentVariant = defaultDependentVariant;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("inStock")
    public Integer getInStock() {
        return inStock;
    }

    @JsonProperty("inStock")
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("variant_group_id")
    public String getVariantGroupId() {
        return variantGroupId;
    }

    @JsonProperty("variant_group_id")
    public void setVariantGroupId(String variantGroupId) {
        this.variantGroupId = variantGroupId;
    }

    @JsonProperty("default_dependent_variant")
    public DefaultDependentVariant getDefaultDependentVariant() {
        return defaultDependentVariant;
    }

    @JsonProperty("default_dependent_variant")
    public void setDefaultDependentVariant(DefaultDependentVariant defaultDependentVariant) {
        this.defaultDependentVariant = defaultDependentVariant;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("price", price).append("id", id).append("inStock", inStock).append("order", order).append("_default", _default).append("variantGroupId", variantGroupId).append("defaultDependentVariant", defaultDependentVariant).append("additionalProperties", additionalProperties).toString();
    }

}