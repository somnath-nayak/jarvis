package com.swiggy.api.sf.rng.dp;

import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import org.testng.annotations.DataProvider;

public class CarouselBannerDP {

    @DataProvider(name="carouselWidgetsDP")
    public Object[][] carousalWidget () {
        return new Object[][]{
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Open Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Cuisine Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "20", "Swiggy-Android", "Legacy Carousels", "Collection", "true", "true", "false", "1"}
        };
    }


    @DataProvider(name="channelDP")
    public Object[][] channel () {
        return new Object[][]{
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Open Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-Android", "Cuisine Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-Android", "Legacy Carousels", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-iOS", "Open Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-iOS", "Cuisine Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-iOS", "Legacy Carousels", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "TopCarousel", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Web", "Open Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Web", "Cuisine Filter", "Collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Web", "Legacy Carousels", "Collection", "true", "true", "false", "1"}
                };
    }

    @DataProvider(name="typeDP")
    public Object[][] type () {
        return new Object[][]{
                //collection all combinations
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Open Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-Android", "Cuisine Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-Android", "Legacy Carousels", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-iOS", "Open Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-iOS", "Cuisine Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-iOS", "Legacy Carousels", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "TopCarousel", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Web", "Open Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Web", "Cuisine Filter", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Web", "Legacy Carousels", "collection", "true", "true", "false", "1"},
                //restaurant al combinations
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Open Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-Android", "Cuisine Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-Android", "Legacy Carousels", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-iOS", "Open Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-iOS", "Cuisine Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-iOS", "Legacy Carousels", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "TopCarousel", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Web", "Open Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Web", "Cuisine Filter", "Restaurant", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Web", "Legacy Carousels", "Restaurant", "true", "true", "false", "1"},
                //static all combinations
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-Android", "Open Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-Android", "Cuisine Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-Android", "Legacy Carousels", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Swiggy-iOS", "Open Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Swiggy-iOS", "Cuisine Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Swiggy-iOS", "Legacy Carousels", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "TopCarousel", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "2", "Web", "Open Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "3", "Web", "Cuisine Filter", "static", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "22", "Web", "Legacy Carousels", "static", "true", "true", "false", "1"}
        };
    }

    @DataProvider(name="cityPriorityCityIDDP")
    public Object[][] cityPriorityCityID () {
        return new Object[][]{
                /* StartTime, EndTime, Priority, Channel, MetaType(TopCarousel etc.), Type(Restaurant, Collection etc.), Enabled, RestaurantVisibility, UserRestriction, CityId, //RedirectLink(RestaurantID) */
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "collection", "true", "true", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "29", "Swiggy-iOS", "Open Filter", "static", "false", "true", "true", "2"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "30", "Swiggy-iOS", "Open Filter", "static", "false", "true", "true", "5"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "0", "Swiggy-Android", "Cuisine Filter", "Restaurant", "false", "true", "false", "6"}
        };
    }

    @DataProvider(name="enabledRestVisibilityUserRestrictDP")
    public Object[][] enabledRestVisibilityUserRestrict() {
        return new Object[][]{
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "true", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Open Filter", "static", "true", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Cuisine Filter", "restaurant", "true", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "false", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Open Filter", "static", "false", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Legacy Carousels", "restaurant", "false", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "true", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Open Filter", "static", "true", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Legacy Carousels", "restaurant", "true", "false", "false", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "false", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Open Filter", "static", "false", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Legacy Carousels", "restaurant", "false", "true", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "collection", "false", "false", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Cuisine Filter", "static", "false", "false", "true", "1"},
                {"2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Legacy Carousels", "restaurant", "false", "false", "true", "1"}
        };
    }

    /*@DataProvider(name = "aggregatorHelperChannel")
    public Object[][] aggregatorHelperChannel () {
        return new Object[][] {
                {"Swiggy-iOS"},
                {"Swiggy-Android"},
                {"Web"}
        };
    }*/


    //For BannerType: Restaurants
    @DataProvider(name = "validateCarouselWithAggregatorForRestaurantDP")
    public Object[][] validateCarouselWithAggregatorForRestaurant () {
        return new Object[][]
                {
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "TopCarousel", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "TopCarousel", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "TopCarousel", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "Open Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Open Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "Open Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "Cuisine Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Cuisine Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "Cuisine Filter", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-Android", "Legacy Carousels", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Swiggy-iOS", "Legacy Carousels", "restaurant", "true", "false", "false", "1"},
                        {"12.9326","77.6036", "2017-10-10 0:0:0", "2018-10-10 1:0:0", "1", "Web", "Legacy Carousels", "restaurant", "true", "false", "false", "1"},
                };
    }

    @DataProvider(name = "validateCarouselWithAggregatorStatic")
    public Object[][] validateCarouselWithAggregatorStatic () {
       return new Object[][] {
               {}
       };
    }

    @DataProvider(name="carouselWidgets")
    public Object[][] carouselWidgets(){
        return new Object[][]{
                {new CarouselPOJO().setDefault().withMetatypes("TopCarousel")},
                {new CarouselPOJO().setDefault().withMetatypes("OpenFilter")},
                {new CarouselPOJO().setDefault().withMetatypes("CategoryCarousel")},
                {new CarouselPOJO().setDefault().withMetatypes("NewUserOpenFilter")},
                {new CarouselPOJO().setDefault().withMetatypes("CuisineFilter")},
//      this is not been in use now aggregator wont send this        {new CarouselPOJO().setDefault().withMetatypes("LegacyCarousels")},
                {new CarouselPOJO().setDefault().withMetatypes("OffersTopBanner")}
        };
    }
//    {"WEB", "Swiggy-Android", "Swiggy-iOS"}
    @DataProvider(name="carouselWidgetsWithChannel")
    public Object[][] carouselWidgetsWithChannel(){
        return new Object[][]{
                {new CarouselPOJO().setDefault().withMetatypes("TopCarousel").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().withMetatypes("OpenFilter").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().withMetatypes("CategoryCarousel").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().withMetatypes("NewUserOpenFilter").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().withMetatypes("CuisineFilter").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().withMetatypes("OffersTopBanner").withChannel("Swiggy-iOS")},

                {new CarouselPOJO().setDefault().withMetatypes("TopCarousel").withChannel("WEB")},
                {new CarouselPOJO().setDefault().withMetatypes("OpenFilter").withChannel("WEB")},
                {new CarouselPOJO().setDefault().withMetatypes("CategoryCarousel").withChannel("WEB")},
                {new CarouselPOJO().setDefault().withMetatypes("NewUserOpenFilter").withChannel("WEB")},
                {new CarouselPOJO().setDefault().withMetatypes("CuisineFilter").withChannel("WEB")},
                {new CarouselPOJO().setDefault().withMetatypes("OffersTopBanner").withChannel("WEB")},

                {new CarouselPOJO().setDefault().withMetatypes("TopCarousel").withChannel("Swiggy-Android")},
                {new CarouselPOJO().setDefault().withMetatypes("OpenFilter").withChannel("Swiggy-Android")},
                {new CarouselPOJO().setDefault().withMetatypes("CategoryCarousel").withChannel("Swiggy-Android")},
                {new CarouselPOJO().setDefault().withMetatypes("NewUserOpenFilter").withChannel("Swiggy-Android")},
                {new CarouselPOJO().setDefault().withMetatypes("CuisineFilter").withChannel("Swiggy-Android")},
                {new CarouselPOJO().setDefault().withMetatypes("OffersTopBanner").withChannel("Swiggy-Android")}
        };
    }



    @DataProvider(name="menuMerchCarouselWidgetsWithChannel")
    public Object[][] menuMerchCarouselWidgetsWithChannel(){
        return new Object[][]{
                {new CarouselPOJO().setDefault().setColor().withType("item").withMetatypes("TopCarousel").withChannel("Swiggy-iOS")},
                {new CarouselPOJO().setDefault().setColor().withType("item").withMetatypes("TopCarousel").withChannel("Swiggy-Android")}
        };
    }
}
