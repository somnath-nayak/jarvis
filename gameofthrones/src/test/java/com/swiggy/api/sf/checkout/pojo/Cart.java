package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

public class Cart {

    private List<Addon> addons = null;
    private List<Variant> variants = null;
    private String menu_item_id;
    private Integer quantity;

    /**
     * No args constructor for use in serialization
     *
     */
    public Cart() {
    }

    /**
     *
     * @param menu_item_id
     * @param addons
     * @param variants
     * @param quantity
     */
    public Cart(List<Addon> addons, List<Variant> variants, String menu_item_id, Integer quantity) {
        super();
        this.addons = addons;
        this.variants = variants;
        this.menu_item_id = menu_item_id;
        this.quantity = quantity;
    }

    public List<Addon> getAddons() {
        return addons;
    }

    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public String getmenu_item_id() {
        return menu_item_id;
    }

    public void setmenu_item_id(String menu_item_id) {
        this.menu_item_id = menu_item_id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addons", addons).append("variants", variants).append("menu_item_id", menu_item_id).append("quantity", quantity).toString();
    }

}