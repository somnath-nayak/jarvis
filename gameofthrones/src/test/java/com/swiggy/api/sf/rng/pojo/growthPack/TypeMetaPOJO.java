package com.swiggy.api.sf.rng.pojo.growthPack;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "icon",
            "description",
            "header"
    })
    public class TypeMetaPOJO {

        @JsonProperty("icon")
        private String icon;
        @JsonProperty("description")
        private String description;
        @JsonProperty("header")
        private String header;

        /**
         * No args constructor for use in serialization
         *
         */
        public TypeMetaPOJO() {
        }

        /**
         *
         * @param icon
         * @param description
         * @param header
         */
        public TypeMetaPOJO(String icon, String description, String header) {
            super();
            this.icon = icon;
            this.description = description;
            this.header = header;
        }

        @JsonProperty("icon")
        public String getIcon() {
            return icon;
        }

        @JsonProperty("icon")
        public void setIcon(String icon) {
            this.icon = icon;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("header")
        public String getHeader() {
            return header;
        }

        @JsonProperty("header")
        public void setHeader(String header) {
            this.header = header;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("icon", icon).append("description", description).append("header", header).toString();
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(icon).append(description).append(header).toHashCode();
        }

        @Override
        public boolean equals(Object other) {
            if (other == this) {
                return true;
            }
            if ((other instanceof TypeMetaPOJO) == false) {
                return false;
            }
            TypeMetaPOJO rhs = ((TypeMetaPOJO) other);
            return new EqualsBuilder().append(icon, rhs.icon).append(description, rhs.description).append(header, rhs.header).isEquals();
        }


        public TypeMetaPOJO withIcon(String icon) {
            this.icon=icon;
            return this;
        }


        public TypeMetaPOJO withDescription(String description) {
            this.description=description;
            return this;
        }

        public TypeMetaPOJO withHeader(String header) {
            this.header=header;
            return this;
        }

        public TypeMetaPOJO setDefault() {
            if (getIcon()== null){
                withIcon("url");
            }
            if (getDescription() == null){
                withDescription("RapidGrowth");
            }
            if(getHeader() == null){
                withHeader("Turbo");
            }
            return this;
        }
    }


