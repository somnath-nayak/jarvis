package com.swiggy.api.sf.rng.pojo.MenuMerch;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import java.util.List;
import java.util.ArrayList;


public class CreateFlatTDItemLevelPOJO {

    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("restaurant_hit")
    private String restaurantHit;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("createdBy")
    private String createdBy;

    @JsonProperty("restaurantList")
    private List<RestaurantList> restaurantList = null;

    @JsonProperty("ruleDiscount")
    private RuleDiscount ruleDiscount;

    @JsonProperty("slots")
    private List<Slots> slots = null;

    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public CreateFlatTDItemLevelPOJO withNamespace(String namespace) {
        this.namespace = namespace;
        return this;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    public CreateFlatTDItemLevelPOJO withHeader(String header) {
        this.header = header;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public CreateFlatTDItemLevelPOJO withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public CreateFlatTDItemLevelPOJO withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public CreateFlatTDItemLevelPOJO withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CreateFlatTDItemLevelPOJO withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("restaurantList")
    public List<RestaurantList> getRestaurantList() {
        return restaurantList;
    }

    @JsonProperty("restaurantList")
    public void setRestaurantList(List<RestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public CreateFlatTDItemLevelPOJO withRestaurantList(List<RestaurantList> restaurantList) {
        this.restaurantList = restaurantList;
        return this;
    }

    @JsonProperty("ruleDiscount")
    public RuleDiscount getRuleDiscount() {
        return ruleDiscount;
    }

    @JsonProperty("ruleDiscount")
    public void setRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
    }

    public CreateFlatTDItemLevelPOJO withRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
        return this;
    }

    @JsonProperty("slots")
    public List<Slots> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public void setSlots(List<Slots> slots) {
        this.slots = slots;
    }

    public CreateFlatTDItemLevelPOJO withSlots(List<Slots> slots) {
        this.slots = slots;
        return this;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    public CreateFlatTDItemLevelPOJO withCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
        return this;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    public CreateFlatTDItemLevelPOJO withTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        return this;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public CreateFlatTDItemLevelPOJO withFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    public CreateFlatTDItemLevelPOJO withTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
        return this;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    public CreateFlatTDItemLevelPOJO withUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    @JsonProperty("valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("valid_from")
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public CreateFlatTDItemLevelPOJO withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    @JsonProperty("valid_till")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public CreateFlatTDItemLevelPOJO withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    @JsonProperty("campaign_type")
    public String getCampaignType() {
        return campaignType;
    }

    @JsonProperty("campaign_type")
    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public CreateFlatTDItemLevelPOJO withCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    @JsonProperty("restaurant_hit")
    public String getRestaurantHit() {
        return restaurantHit;
    }

    @JsonProperty("restaurant_hit")
    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    public CreateFlatTDItemLevelPOJO withRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
        return this;
    }

    @JsonProperty("swiggy_hit")
    public String getSwiggyHit() {
        return swiggyHit;
    }

    @JsonProperty("swiggy_hit")
    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    public CreateFlatTDItemLevelPOJO withSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
        return this;
    }

    public CreateFlatTDItemLevelPOJO setDefault(){
        return this.withNamespace("TestTD")
                .withHeader("TestTD")
                .withDescription("TestTD")
                .withValidFrom("1507643702000")
                .withValidTill("1633857836000")
                .withCampaignType("Flat")
                .withRestaurantHit("40")
                .withEnabled(true)
                .withSwiggyHit("60")
                .withCreatedBy("srishty.mathur")
                .withDiscountLevel("Item")
                .withRestaurantList(getRestaurantList())
                .withRuleDiscount(getRuleDiscount())
                .withSlots(getAllDaysTimeSlot())
                .withCommissionOnFullBill(false)
                .withTaxesOnDiscountedBill(false)
                .withFirstOrderRestriction(false)
                .withTimeSlotRestriction(false)
                .withUserRestriction(false);
    }

    @JsonIgnore
    public List<Slots> getAllDaysTimeSlot(){
        List<Slots> slots = new ArrayList<>();
        Slots slots1 = new Slots();
        String[] days = {"SUN","MON","TUE","WED","THU","FRI","SAT"};
        for(String day: days) {
            slots1 = new Slots();
            slots.add(slots1.withCloseTime("2359").withOpenTime("0").withDay(day));
        }
        return slots;
    }

    public List<RestaurantList> withRestaurantsList(List<String> restaurantsList,List<List<String>> categoryId, List<String> subCategoryId, List<String> itemId){
        List<String> restIds = new ArrayList<>();
        restIds.add(restaurantsList.get(0));
        List<Menu> menu = new ArrayList<>();
        menu.add(0, new Menu(itemId.get(0), "testMenu"));
        List<SubCategories> subCategories = new ArrayList<>();
        subCategories.add(0, new SubCategories(subCategoryId.get(0), "testSubCategory", menu));
        List<Categories> restaurantCategoryList = new ArrayList<>();
//        List<Categories> categoryList = new ArrayList<>();
        restaurantCategoryList.add(0, new Categories(categoryId.get(0).get(0), "testCategory", subCategories));
//        restaurantCategoryList.add(categoryList);
        List<RestaurantList> restaurantList = populateRestaurantList(restIds,restaurantCategoryList);
        return restaurantList;
    }

    public List<RestaurantList> populateRestaurantList(List<String> restIds, List<Categories> categoryList) {
        List<RestaurantList> restList = new ArrayList<>();
        for (int i = 0; i < restIds.size(); i++) {
            RestaurantList restaurantList = new RestaurantList(restIds.get(i), "testRest", categoryList);
            restList.add(restaurantList);
        }
        return restList;
    }
}


