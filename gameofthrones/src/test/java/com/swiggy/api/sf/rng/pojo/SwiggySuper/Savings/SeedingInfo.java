package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;


import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "merchantType",
        "commissionLevy",
        "shareType",
        "shareValue"
})
public class SeedingInfo {

    @JsonProperty("merchantType")
    private String merchantType;
    @JsonProperty("commissionLevy")
    private Integer commissionLevy;
    @JsonProperty("shareType")
    private String shareType;
    @JsonProperty("shareValue")
    private Integer shareValue;

    @JsonProperty("merchantType")
    public String getMerchantType() {
        return merchantType;
    }

    @JsonProperty("merchantType")
    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    @JsonProperty("commissionLevy")
    public Integer getCommissionLevy() {
        return commissionLevy;
    }

    @JsonProperty("commissionLevy")
    public void setCommissionLevy(Integer commissionLevy) {
        this.commissionLevy = commissionLevy;
    }

    @JsonProperty("shareType")
    public String getShareType() {
        return shareType;
    }

    @JsonProperty("shareType")
    public void setShareType(String shareType) {
        this.shareType = shareType;
    }

    @JsonProperty("shareValue")
    public Integer getShareValue() {
        return shareValue;
    }

    @JsonProperty("shareValue")
    public void setShareValue(Integer shareValue) {
        this.shareValue = shareValue;
    }

}