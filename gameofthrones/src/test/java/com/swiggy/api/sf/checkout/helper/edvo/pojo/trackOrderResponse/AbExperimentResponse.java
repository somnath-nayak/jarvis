package com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class AbExperimentResponse {

    private Map<String, AbExperimentResult> experiments;

    public Map<String, AbExperimentResult> getExperiments() {
        return experiments;
    }

    public void setExperiments(Map<String, AbExperimentResult> experiments) {
        this.experiments = experiments;
    }
}
