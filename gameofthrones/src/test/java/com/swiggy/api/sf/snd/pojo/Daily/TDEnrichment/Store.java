package com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment
 **/
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "storeId",
        "items"
})
public class Store {

    @JsonProperty("storeId")
    private String storeId;
    @JsonProperty("items")
    private List<Item> items = null;

    @JsonProperty("storeId")
    public String getStoreId() {
        return storeId;
    }

    @JsonProperty("storeId")
    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("storeId", storeId).append("items", items).toString();
    }

    private void setDefaultValues(String storeId, List<Item> ls) {
        if (this.getStoreId() == null)
            this.setStoreId(storeId);
        if (this.getItems() == null)
            this.setItems(ls);
    }

    public Store build(String storeId, List<Item> ls){
        setDefaultValues(storeId, ls);
        return this;
    }

}
