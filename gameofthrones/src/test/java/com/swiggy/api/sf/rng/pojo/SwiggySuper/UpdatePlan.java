package com.swiggy.api.sf.rng.pojo.SwiggySuper;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "plan_id",
        "enabled",
        "count",
        "updated_by",
        "valid_till"
})
public class UpdatePlan {

    @JsonProperty("plan_id")
    private Integer planId;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("updated_by")
    private String updatedBy;
    @JsonProperty("valid_till")
    private Integer validTill;

    /**
     * No args constructor for use in serialization
     *
     */
    public UpdatePlan() {
    }

    /**
     *
     * @param enabled
     * @param count
     * @param planId
     * @param validTill
     * @param updatedBy
     */
    public UpdatePlan(Integer planId, Boolean enabled, Integer count, String updatedBy, Integer validTill) {
        super();
        this.planId = planId;
        this.enabled = enabled;
        this.count = count;
        this.updatedBy = updatedBy;
        this.validTill = validTill;
    }

    @JsonProperty("plan_id")
    public Integer getPlanId() {
        return planId;
    }

    @JsonProperty("plan_id")
    public void setPlanId(Integer planId) {
        this.planId = planId;
    }

    public UpdatePlan withPlanId(Integer planId) {
        this.planId = planId;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public UpdatePlan withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    public UpdatePlan withCount(Integer count) {
        this.count = count;
        return this;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public UpdatePlan withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    @JsonProperty("valid_till")
    public Integer getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public void setValidTill(Integer validTill) {
        this.validTill = validTill;
    }

    public UpdatePlan withValidTill(Integer validTill) {
        this.validTill = validTill;
        return this;
    }

    public void setDefaultValues(String planId,Boolean enabled,String validTill) {

        if (this.getPlanId() == null)
            this.setPlanId(Integer.parseInt(planId));

        this.setEnabled(enabled);

        if (this.getUpdatedBy() == null)
            this.setUpdatedBy("system");
        if (this.getValidTill()==null)
            this.setValidTill(Integer.parseInt(validTill));
        this.setCount(1);

    }



}