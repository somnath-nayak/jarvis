package com.swiggy.api.sf.checkout.helper.edvo.pojo.trackOrderResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MessageHelp {
    private String description;
    private String title;
    @JsonProperty("ask_confirmation")
    private boolean askConfirmation;
    private long helpAfter;

    private String descriptionBefore;
    private String titleBefore;

    private boolean askConfirmationBefore;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isAskConfirmation() {
        return askConfirmation;
    }

    public void setAskConfirmation(boolean askConfirmation) {
        this.askConfirmation = askConfirmation;
    }

    public long getHelpAfter() {
        return helpAfter;
    }

    public void setHelpAfter(long helpAfter) {
        this.helpAfter = helpAfter;
    }

    public String getDescriptionBefore() {
        return descriptionBefore;
    }

    public void setDescriptionBefore(String descriptionBefore) {
        this.descriptionBefore = descriptionBefore;
    }

    public String getTitleBefore() {
        return titleBefore;
    }

    public void setTitleBefore(String titleBefore) {
        this.titleBefore = titleBefore;
    }

    public boolean isAskConfirmationBefore() {
        return askConfirmationBefore;
    }

    public void setAskConfirmationBefore(boolean askConfirmationBefore) {
        this.askConfirmationBefore = askConfirmationBefore;
    }
}
