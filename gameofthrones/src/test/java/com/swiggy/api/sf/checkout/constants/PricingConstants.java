package com.swiggy.api.sf.checkout.constants;

public interface PricingConstants {
    String INSERTED_SUCCESSFULLY_MESSAGE="Inserted Successfully!";
    String UPDATED_SUCCESSFULLY_MESSAGE="Updated Successfully!";
    String DONE_SUCCESSFULLY_MESSAGE="done successfully";
    String REFUND_MESSAGE="PG refund is not allowed for Cash order.";
    String CART_UPDATED_SUCCESSFULLY="CART_UPDATED_SUCCESSFULLY";
    String VALID_STATUS_CODE="0";
    String STATUS_CODE_ONE="1";
    int SUCCESS_CODE=200;
    String PRICING_ZERO_VALUE="0.0";
}
