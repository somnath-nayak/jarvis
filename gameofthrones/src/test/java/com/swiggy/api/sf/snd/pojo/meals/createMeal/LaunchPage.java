package com.swiggy.api.sf.snd.pojo.meals.createMeal;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "image",
        "bgColor",
        "mainText",
        "subText",
        "textColor",
        "tagText",
        "tagColor",
        "communicationText"
})
public class LaunchPage {

    @JsonProperty("image")
    private String image;
    @JsonProperty("bgColor")
    private String bgColor;
    @JsonProperty("mainText")
    private String mainText;
    @JsonProperty("subText")
    private String subText;
    @JsonProperty("textColor")
    private String textColor;
    @JsonProperty("tagText")
    private String tagText;
    @JsonProperty("tagColor")
    private String tagColor;
    @JsonProperty("communicationText")
    private String communicationText;

    @JsonProperty("image")
    public String getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(String image) {
        this.image = image;
    }

    @JsonProperty("bgColor")
    public String getBgColor() {
        return bgColor;
    }

    @JsonProperty("bgColor")
    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    @JsonProperty("mainText")
    public String getMainText() {
        return mainText;
    }

    @JsonProperty("mainText")
    public void setMainText(String mainText) {
        this.mainText = mainText;
    }

    @JsonProperty("subText")
    public String getSubText() {
        return subText;
    }

    @JsonProperty("subText")
    public void setSubText(String subText) {
        this.subText = subText;
    }

    @JsonProperty("textColor")
    public String getTextColor() {
        return textColor;
    }

    @JsonProperty("textColor")
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    @JsonProperty("tagText")
    public String getTagText() {
        return tagText;
    }

    @JsonProperty("tagText")
    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    @JsonProperty("tagColor")
    public String getTagColor() {
        return tagColor;
    }

    @JsonProperty("tagColor")
    public void setTagColor(String tagColor) {
        this.tagColor = tagColor;
    }

    @JsonProperty("communicationText")
    public String getCommunicationText() {
        return communicationText;
    }

    @JsonProperty("communicationText")
    public void setCommunicationText(String communicationText) {
        this.communicationText = communicationText;
    }

}