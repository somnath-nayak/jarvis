package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "payment_mode",
        "promotion_text"
})
public class Offer {

    @JsonProperty("payment_mode")
    private String paymentMode;
    @JsonProperty("promotion_text")
    private String promotionText;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Offer() {
    }

    /**
     *
     * @param paymentMode
     * @param promotionText
     */
    public Offer(String paymentMode, String promotionText) {
        super();
        this.paymentMode = paymentMode;
        this.promotionText = promotionText;
    }

    @JsonProperty("payment_mode")
    public String getPaymentMode() {
        return paymentMode;
    }

    @JsonProperty("payment_mode")
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @JsonProperty("promotion_text")
    public String getPromotionText() {
        return promotionText;
    }

    @JsonProperty("promotion_text")
    public void setPromotionText(String promotionText) {
        this.promotionText = promotionText;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("paymentMode", paymentMode).append("promotionText", promotionText).append("additionalProperties", additionalProperties).toString();
    }

}