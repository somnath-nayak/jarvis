package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemPrice {

    private String type;
    private Integer min;
    private Integer max;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemPrice() {
    }

    /**
     *
     * @param min
     * @param max
     * @param type
     */
    public ItemPrice(String type, Integer min, Integer max) {
        super();
        this.type = type;
        this.min = min;
        this.max = max;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("min", min).append("max", max).toString();
    }

}
