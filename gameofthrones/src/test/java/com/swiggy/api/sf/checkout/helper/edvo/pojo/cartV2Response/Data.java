package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rainMode",
        "select",
        "cartId",
        "result",
        "email_id",
        "phone_no",
        "user_swiggy_money",
        "cart_menu_items",
        "mealItems",
        "cart_subtotal_without_packing",
        "cart_subtotal",
        "delivery_charges",
        "restaurant_packing_charges",
        "cart_total",
        "order_total",
        "total_without_coupon_discount",
        "total_packing_charges",
        "discount_total",
        "coupon_discount_total",
        "trade_discount_total",
        "swiggy_money",
        "cart_total_count",
        "restaurant_details",
        "is_coupon_valid",
        "is_group_parent",
        "free_gifts",
        "free_shipping",
        "coupon_error_message",
        "is_sla_honored",
        "sla_time",
        "sla_range_min",
        "sla_range_max",
        "payment_banner_message",
        "cart_banner_message",
        "cod_enabled",
        "cod_disabled_message",
        "convenience_fee",
        "delivery_fee_type",
        "offers",
        "is_assured",
        "order_incoming",
        "order_spending",
        "rendering_details",
        "display_sequence",
        "addresses",
        "cancellation_fee",
        "total_tax",
        "configurations",
        "messages",
        "GST_details",
        "threshold_fee",
        "distance_fee",
        "time_fee",
        "special_fee",
        "threshold_fee_message",
        "distance_fee_message",
        "time_fee_message",
        "special_fee_message",
        "bill_total",
        "item_total",
        "cart_token"
})
public class Data {

    @JsonProperty("rainMode")
    private Boolean rainMode;
    @JsonProperty("select")
    private Boolean select;
    @JsonProperty("cartId")
    private Integer cartId;
    @JsonProperty("result")
    private String result;
    @JsonProperty("email_id")
    private String emailId;
    @JsonProperty("phone_no")
    private String phoneNo;
    @JsonProperty("user_swiggy_money")
    private Integer userSwiggyMoney;
    @JsonProperty("cart_menu_items")
    private List<CartMenuItem> cartMenuItems = null;
    @JsonProperty("mealItems")
    private List<MealItem> mealItems = null;
    @JsonProperty("cart_subtotal_without_packing")
    private Integer cartSubtotalWithoutPacking;
    @JsonProperty("cart_subtotal")
    private Integer cartSubtotal;
    @JsonProperty("delivery_charges")
    private Integer deliveryCharges;
    @JsonProperty("restaurant_packing_charges")
    private Integer restaurantPackingCharges;
    @JsonProperty("cart_total")
    private Integer cartTotal;
    @JsonProperty("order_total")
    private Integer orderTotal;
    @JsonProperty("total_without_coupon_discount")
    private Integer totalWithoutCouponDiscount;
    @JsonProperty("total_packing_charges")
    private Integer totalPackingCharges;
    @JsonProperty("discount_total")
    private Integer discountTotal;
    @JsonProperty("coupon_discount_total")
    private Integer couponDiscountTotal;
    @JsonProperty("trade_discount_total")
    private float tradeDiscountTotal;
    @JsonProperty("trade_discount")
    private Integer tradeDiscount;
    @JsonProperty("swiggy_money")
    private Integer swiggyMoney;
    @JsonProperty("cart_total_count")
    private Integer cartTotalCount;
    @JsonProperty("restaurant_details")
    private RestaurantDetails restaurantDetails;
    @JsonProperty("is_coupon_valid")
    private Integer isCouponValid;
    @JsonProperty("is_group_parent")
    private Boolean isGroupParent;
    @JsonProperty("free_gifts")
    private List<Object> freeGifts = null;
    @JsonProperty("free_shipping")
    private Integer freeShipping;
    @JsonProperty("coupon_error_message")
    private String couponErrorMessage;
    @JsonProperty("is_sla_honored")
    private Boolean isSlaHonored;
    @JsonProperty("sla_time")
    private Integer slaTime;
    @JsonProperty("sla_range_min")
    private Integer slaRangeMin;
    @JsonProperty("sla_range_max")
    private Integer slaRangeMax;
    @JsonProperty("payment_banner_message")
    private String paymentBannerMessage;
    @JsonProperty("cart_banner_message")
    private String cartBannerMessage;
    @JsonProperty("cod_enabled")
    private Boolean codEnabled;
    @JsonProperty("cod_disabled_message")
    private String codDisabledMessage;
    @JsonProperty("convenience_fee")
    private Integer convenienceFee;
    @JsonProperty("delivery_fee_type")
    private Integer deliveryFeeType;
    @JsonProperty("offers")
    private List<Offer> offers = null;
    @JsonProperty("is_assured")
    private Integer isAssured;
    @JsonProperty("order_incoming")
    private Integer orderIncoming;
    @JsonProperty("order_spending")
    private Integer orderSpending;
    @JsonProperty("rendering_details")
    private List<RenderingDetail> renderingDetails = null;
    @JsonProperty("display_sequence")
    private List<String> displaySequence = null;
    @JsonProperty("addresses")
    private List<Address> addresses = null;
    @JsonProperty("cancellation_fee")
    private Integer cancellationFee;
    @JsonProperty("total_tax")
    private Double totalTax;
    @JsonProperty("configurations")
    private Configurations configurations;
    @JsonProperty("messages")
    private List<Object> messages = null;
    @JsonProperty("GST_details")
    private GSTDetails_ gSTDetails;
    @JsonProperty("threshold_fee")
    private Integer thresholdFee;
    @JsonProperty("distance_fee")
    private Integer distanceFee;
    @JsonProperty("time_fee")
    private Integer timeFee;
    @JsonProperty("special_fee")
    private Integer specialFee;
    @JsonProperty("threshold_fee_message")
    private String thresholdFeeMessage;
    @JsonProperty("distance_fee_message")
    private String distanceFeeMessage;
    @JsonProperty("time_fee_message")
    private String timeFeeMessage;
    @JsonProperty("special_fee_message")
    private String specialFeeMessage;
    @JsonProperty("bill_total")
    private Double billTotal;
    @JsonProperty("item_total")
    private Integer itemTotal;
    @JsonProperty("cart_token")
    private String cartToken;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Data() {
    }

    @JsonProperty("rainMode")
    public Boolean getRainMode() {
        return rainMode;
    }

    @JsonProperty("rainMode")
    public void setRainMode(Boolean rainMode) {
        this.rainMode = rainMode;
    }

    @JsonProperty("select")
    public Boolean getSelect() {
        return select;
    }

    @JsonProperty("select")
    public void setSelect(Boolean select) {
        this.select = select;
    }

    @JsonProperty("cartId")
    public Integer getCartId() {
        return cartId;
    }

    @JsonProperty("cartId")
    public void setCartId(Integer cartId) {
        this.cartId = cartId;
    }

    @JsonProperty("result")
    public String getResult() {
        return result;
    }

    @JsonProperty("result")
    public void setResult(String result) {
        this.result = result;
    }

    @JsonProperty("email_id")
    public String getEmailId() {
        return emailId;
    }

    @JsonProperty("email_id")
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @JsonProperty("phone_no")
    public String getPhoneNo() {
        return phoneNo;
    }

    @JsonProperty("phone_no")
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @JsonProperty("user_swiggy_money")
    public Integer getUserSwiggyMoney() {
        return userSwiggyMoney;
    }

    @JsonProperty("user_swiggy_money")
    public void setUserSwiggyMoney(Integer userSwiggyMoney) {
        this.userSwiggyMoney = userSwiggyMoney;
    }

    @JsonProperty("cart_menu_items")
    public List<CartMenuItem> getCartMenuItems() {
        return cartMenuItems;
    }

    @JsonProperty("cart_menu_items")
    public void setCartMenuItems(List<CartMenuItem> cartMenuItems) {
        this.cartMenuItems = cartMenuItems;
    }

    @JsonProperty("mealItems")
    public List<MealItem> getMealItems() {
        return mealItems;
    }

    @JsonProperty("mealItems")
    public void setMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
    }

    @JsonProperty("cart_subtotal_without_packing")
    public Integer getCartSubtotalWithoutPacking() {
        return cartSubtotalWithoutPacking;
    }

    @JsonProperty("cart_subtotal_without_packing")
    public void setCartSubtotalWithoutPacking(Integer cartSubtotalWithoutPacking) {
        this.cartSubtotalWithoutPacking = cartSubtotalWithoutPacking;
    }

    @JsonProperty("cart_subtotal")
    public Integer getCartSubtotal() {
        return cartSubtotal;
    }

    @JsonProperty("cart_subtotal")
    public void setCartSubtotal(Integer cartSubtotal) {
        this.cartSubtotal = cartSubtotal;
    }

    @JsonProperty("delivery_charges")
    public Integer getDeliveryCharges() {
        return deliveryCharges;
    }

    @JsonProperty("delivery_charges")
    public void setDeliveryCharges(Integer deliveryCharges) {
        this.deliveryCharges = deliveryCharges;
    }

    @JsonProperty("restaurant_packing_charges")
    public Integer getRestaurantPackingCharges() {
        return restaurantPackingCharges;
    }

    @JsonProperty("restaurant_packing_charges")
    public void setRestaurantPackingCharges(Integer restaurantPackingCharges) {
        this.restaurantPackingCharges = restaurantPackingCharges;
    }

    @JsonProperty("cart_total")
    public Integer getCartTotal() {
        return cartTotal;
    }

    @JsonProperty("cart_total")
    public void setCartTotal(Integer cartTotal) {
        this.cartTotal = cartTotal;
    }

    @JsonProperty("order_total")
    public Integer getOrderTotal() {
        return orderTotal;
    }

    @JsonProperty("order_total")
    public void setOrderTotal(Integer orderTotal) {
        this.orderTotal = orderTotal;
    }

    @JsonProperty("total_without_coupon_discount")
    public Integer getTotalWithoutCouponDiscount() {
        return totalWithoutCouponDiscount;
    }

    @JsonProperty("total_without_coupon_discount")
    public void setTotalWithoutCouponDiscount(Integer totalWithoutCouponDiscount) {
        this.totalWithoutCouponDiscount = totalWithoutCouponDiscount;
    }

    @JsonProperty("total_packing_charges")
    public Integer getTotalPackingCharges() {
        return totalPackingCharges;
    }

    @JsonProperty("total_packing_charges")
    public void setTotalPackingCharges(Integer totalPackingCharges) {
        this.totalPackingCharges = totalPackingCharges;
    }

    @JsonProperty("discount_total")
    public Integer getDiscountTotal() {
        return discountTotal;
    }

    @JsonProperty("discount_total")
    public void setDiscountTotal(Integer discountTotal) {
        this.discountTotal = discountTotal;
    }

    @JsonProperty("coupon_discount_total")
    public Integer getCouponDiscountTotal() {
        return couponDiscountTotal;
    }

    @JsonProperty("coupon_discount_total")
    public void setCouponDiscountTotal(Integer couponDiscountTotal) {
        this.couponDiscountTotal = couponDiscountTotal;
    }

    @JsonProperty("trade_discount_total")
    public float getTradeDiscountTotal() {
        return tradeDiscountTotal;
    }

    @JsonProperty("trade_discount_total")
    public void setTradeDiscountTotal(float tradeDiscountTotal) {
        this.tradeDiscountTotal = tradeDiscountTotal;
    }

    @JsonProperty("trade_discount")
    public Integer getTradeDiscount() {
        return tradeDiscount;
    }

    @JsonProperty("trade_discount")
    public void setTradeDiscount(Integer tradeDiscount) {
        this.tradeDiscount = tradeDiscount;
    }

    @JsonProperty("swiggy_money")
    public Integer getSwiggyMoney() {
        return swiggyMoney;
    }

    @JsonProperty("swiggy_money")
    public void setSwiggyMoney(Integer swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    @JsonProperty("cart_total_count")
    public Integer getCartTotalCount() {
        return cartTotalCount;
    }

    @JsonProperty("cart_total_count")
    public void setCartTotalCount(Integer cartTotalCount) {
        this.cartTotalCount = cartTotalCount;
    }

    @JsonProperty("restaurant_details")
    public RestaurantDetails getRestaurantDetails() {
        return restaurantDetails;
    }

    @JsonProperty("restaurant_details")
    public void setRestaurantDetails(RestaurantDetails restaurantDetails) {
        this.restaurantDetails = restaurantDetails;
    }

    @JsonProperty("is_coupon_valid")
    public Integer getIsCouponValid() {
        return isCouponValid;
    }

    @JsonProperty("is_coupon_valid")
    public void setIsCouponValid(Integer isCouponValid) {
        this.isCouponValid = isCouponValid;
    }

    @JsonProperty("is_group_parent")
    public Boolean getIsGroupParent() {
        return isGroupParent;
    }

    @JsonProperty("is_group_parent")
    public void setIsGroupParent(Boolean isGroupParent) {
        this.isGroupParent = isGroupParent;
    }

    @JsonProperty("free_gifts")
    public List<Object> getFreeGifts() {
        return freeGifts;
    }

    @JsonProperty("free_gifts")
    public void setFreeGifts(List<Object> freeGifts) {
        this.freeGifts = freeGifts;
    }

    @JsonProperty("free_shipping")
    public Integer getFreeShipping() {
        return freeShipping;
    }

    @JsonProperty("free_shipping")
    public void setFreeShipping(Integer freeShipping) {
        this.freeShipping = freeShipping;
    }

    @JsonProperty("coupon_error_message")
    public String getCouponErrorMessage() {
        return couponErrorMessage;
    }

    @JsonProperty("coupon_error_message")
    public void setCouponErrorMessage(String couponErrorMessage) {
        this.couponErrorMessage = couponErrorMessage;
    }

    @JsonProperty("is_sla_honored")
    public Boolean getIsSlaHonored() {
        return isSlaHonored;
    }

    @JsonProperty("is_sla_honored")
    public void setIsSlaHonored(Boolean isSlaHonored) {
        this.isSlaHonored = isSlaHonored;
    }

    @JsonProperty("sla_time")
    public Integer getSlaTime() {
        return slaTime;
    }

    @JsonProperty("sla_time")
    public void setSlaTime(Integer slaTime) {
        this.slaTime = slaTime;
    }

    @JsonProperty("sla_range_min")
    public Integer getSlaRangeMin() {
        return slaRangeMin;
    }

    @JsonProperty("sla_range_min")
    public void setSlaRangeMin(Integer slaRangeMin) {
        this.slaRangeMin = slaRangeMin;
    }

    @JsonProperty("sla_range_max")
    public Integer getSlaRangeMax() {
        return slaRangeMax;
    }

    @JsonProperty("sla_range_max")
    public void setSlaRangeMax(Integer slaRangeMax) {
        this.slaRangeMax = slaRangeMax;
    }

    @JsonProperty("payment_banner_message")
    public String getPaymentBannerMessage() {
        return paymentBannerMessage;
    }

    @JsonProperty("payment_banner_message")
    public void setPaymentBannerMessage(String paymentBannerMessage) {
        this.paymentBannerMessage = paymentBannerMessage;
    }

    @JsonProperty("cart_banner_message")
    public String getCartBannerMessage() {
        return cartBannerMessage;
    }

    @JsonProperty("cart_banner_message")
    public void setCartBannerMessage(String cartBannerMessage) {
        this.cartBannerMessage = cartBannerMessage;
    }

    @JsonProperty("cod_enabled")
    public Boolean getCodEnabled() {
        return codEnabled;
    }

    @JsonProperty("cod_enabled")
    public void setCodEnabled(Boolean codEnabled) {
        this.codEnabled = codEnabled;
    }

    @JsonProperty("cod_disabled_message")
    public String getCodDisabledMessage() {
        return codDisabledMessage;
    }

    @JsonProperty("cod_disabled_message")
    public void setCodDisabledMessage(String codDisabledMessage) {
        this.codDisabledMessage = codDisabledMessage;
    }

    @JsonProperty("convenience_fee")
    public Integer getConvenienceFee() {
        return convenienceFee;
    }

    @JsonProperty("convenience_fee")
    public void setConvenienceFee(Integer convenienceFee) {
        this.convenienceFee = convenienceFee;
    }

    @JsonProperty("delivery_fee_type")
    public Integer getDeliveryFeeType() {
        return deliveryFeeType;
    }

    @JsonProperty("delivery_fee_type")
    public void setDeliveryFeeType(Integer deliveryFeeType) {
        this.deliveryFeeType = deliveryFeeType;
    }

    @JsonProperty("offers")
    public List<Offer> getOffers() {
        return offers;
    }

    @JsonProperty("offers")
    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    @JsonProperty("is_assured")
    public Integer getIsAssured() {
        return isAssured;
    }

    @JsonProperty("is_assured")
    public void setIsAssured(Integer isAssured) {
        this.isAssured = isAssured;
    }

    @JsonProperty("order_incoming")
    public Integer getOrderIncoming() {
        return orderIncoming;
    }

    @JsonProperty("order_incoming")
    public void setOrderIncoming(Integer orderIncoming) {
        this.orderIncoming = orderIncoming;
    }

    @JsonProperty("order_spending")
    public Integer getOrderSpending() {
        return orderSpending;
    }

    @JsonProperty("order_spending")
    public void setOrderSpending(Integer orderSpending) {
        this.orderSpending = orderSpending;
    }

    @JsonProperty("rendering_details")
    public List<RenderingDetail> getRenderingDetails() {
        return renderingDetails;
    }

    @JsonProperty("rendering_details")
    public void setRenderingDetails(List<RenderingDetail> renderingDetails) {
        this.renderingDetails = renderingDetails;
    }

    @JsonProperty("display_sequence")
    public List<String> getDisplaySequence() {
        return displaySequence;
    }

    @JsonProperty("display_sequence")
    public void setDisplaySequence(List<String> displaySequence) {
        this.displaySequence = displaySequence;
    }

    @JsonProperty("addresses")
    public List<Address> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    @JsonProperty("cancellation_fee")
    public Integer getCancellationFee() {
        return cancellationFee;
    }

    @JsonProperty("cancellation_fee")
    public void setCancellationFee(Integer cancellationFee) {
        this.cancellationFee = cancellationFee;
    }

    @JsonProperty("total_tax")
    public Double getTotalTax() {
        return totalTax;
    }

    @JsonProperty("total_tax")
    public void setTotalTax(Double totalTax) {
        this.totalTax = totalTax;
    }

    @JsonProperty("configurations")
    public Configurations getConfigurations() {
        return configurations;
    }

    @JsonProperty("configurations")
    public void setConfigurations(Configurations configurations) {
        this.configurations = configurations;
    }

    @JsonProperty("messages")
    public List<Object> getMessages() {
        return messages;
    }

    @JsonProperty("messages")
    public void setMessages(List<Object> messages) {
        this.messages = messages;
    }

    @JsonProperty("GST_details")
    public GSTDetails_ getGSTDetails() {
        return gSTDetails;
    }

    @JsonProperty("GST_details")
    public void setGSTDetails(GSTDetails_ gSTDetails) {
        this.gSTDetails = gSTDetails;
    }

    @JsonProperty("threshold_fee")
    public Integer getThresholdFee() {
        return thresholdFee;
    }

    @JsonProperty("threshold_fee")
    public void setThresholdFee(Integer thresholdFee) {
        this.thresholdFee = thresholdFee;
    }

    @JsonProperty("distance_fee")
    public Integer getDistanceFee() {
        return distanceFee;
    }

    @JsonProperty("distance_fee")
    public void setDistanceFee(Integer distanceFee) {
        this.distanceFee = distanceFee;
    }

    @JsonProperty("time_fee")
    public Integer getTimeFee() {
        return timeFee;
    }

    @JsonProperty("time_fee")
    public void setTimeFee(Integer timeFee) {
        this.timeFee = timeFee;
    }

    @JsonProperty("special_fee")
    public Integer getSpecialFee() {
        return specialFee;
    }

    @JsonProperty("special_fee")
    public void setSpecialFee(Integer specialFee) {
        this.specialFee = specialFee;
    }

    @JsonProperty("threshold_fee_message")
    public String getThresholdFeeMessage() {
        return thresholdFeeMessage;
    }

    @JsonProperty("threshold_fee_message")
    public void setThresholdFeeMessage(String thresholdFeeMessage) {
        this.thresholdFeeMessage = thresholdFeeMessage;
    }

    @JsonProperty("distance_fee_message")
    public String getDistanceFeeMessage() {
        return distanceFeeMessage;
    }

    @JsonProperty("distance_fee_message")
    public void setDistanceFeeMessage(String distanceFeeMessage) {
        this.distanceFeeMessage = distanceFeeMessage;
    }

    @JsonProperty("time_fee_message")
    public String getTimeFeeMessage() {
        return timeFeeMessage;
    }

    @JsonProperty("time_fee_message")
    public void setTimeFeeMessage(String timeFeeMessage) {
        this.timeFeeMessage = timeFeeMessage;
    }

    @JsonProperty("special_fee_message")
    public String getSpecialFeeMessage() {
        return specialFeeMessage;
    }

    @JsonProperty("special_fee_message")
    public void setSpecialFeeMessage(String specialFeeMessage) {
        this.specialFeeMessage = specialFeeMessage;
    }

    @JsonProperty("bill_total")
    public Double getBillTotal() {
        return billTotal;
    }

    @JsonProperty("bill_total")
    public void setBillTotal(Double billTotal) {
        this.billTotal = billTotal;
    }

    @JsonProperty("item_total")
    public Integer getItemTotal() {
        return itemTotal;
    }

    @JsonProperty("item_total")
    public void setItemTotal(Integer itemTotal) {
        this.itemTotal = itemTotal;
    }

    @JsonProperty("cart_token")
    public String getCartToken() {
        return cartToken;
    }

    @JsonProperty("cart_token")
    public void setCartToken(String cartToken) {
        this.cartToken = cartToken;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rainMode", rainMode).append("select", select).append("cartId", cartId).append("result", result).append("emailId", emailId).append("phoneNo", phoneNo).append("userSwiggyMoney", userSwiggyMoney).append("cartMenuItems", cartMenuItems).append("mealItems", mealItems).append("cartSubtotalWithoutPacking", cartSubtotalWithoutPacking).append("cartSubtotal", cartSubtotal).append("deliveryCharges", deliveryCharges).append("restaurantPackingCharges", restaurantPackingCharges).append("cartTotal", cartTotal).append("orderTotal", orderTotal).append("totalWithoutCouponDiscount", totalWithoutCouponDiscount).append("totalPackingCharges", totalPackingCharges).append("discountTotal", discountTotal).append("couponDiscountTotal", couponDiscountTotal).append("tradeDiscountTotal", tradeDiscountTotal).append("swiggyMoney", swiggyMoney).append("cartTotalCount", cartTotalCount).append("restaurantDetails", restaurantDetails).append("isCouponValid", isCouponValid).append("isGroupParent", isGroupParent).append("freeGifts", freeGifts).append("freeShipping", freeShipping).append("couponErrorMessage", couponErrorMessage).append("isSlaHonored", isSlaHonored).append("slaTime", slaTime).append("slaRangeMin", slaRangeMin).append("slaRangeMax", slaRangeMax).append("paymentBannerMessage", paymentBannerMessage).append("cartBannerMessage", cartBannerMessage).append("codEnabled", codEnabled).append("codDisabledMessage", codDisabledMessage).append("convenienceFee", convenienceFee).append("deliveryFeeType", deliveryFeeType).append("offers", offers).append("isAssured", isAssured).append("orderIncoming", orderIncoming).append("orderSpending", orderSpending).append("renderingDetails", renderingDetails).append("displaySequence", displaySequence).append("addresses", addresses).append("cancellationFee", cancellationFee).append("totalTax", totalTax).append("configurations", configurations).append("messages", messages).append("gSTDetails", gSTDetails).append("thresholdFee", thresholdFee).append("distanceFee", distanceFee).append("timeFee", timeFee).append("specialFee", specialFee).append("thresholdFeeMessage", thresholdFeeMessage).append("distanceFeeMessage", distanceFeeMessage).append("timeFeeMessage", timeFeeMessage).append("specialFeeMessage", specialFeeMessage).append("billTotal", billTotal).append("itemTotal", itemTotal).append("cartToken", cartToken).append("additionalProperties", additionalProperties).toString();
    }

}