package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Response;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rainMode",
        "delivery_valid",
        "estimated_sla",
        "estimated_sla_min",
        "estimated_sla_max",
        "recalculation_required",
        "id",
        "user_id",
        "default",
        "name",
        "mobile",
        "address",
        "landmark",
        "area",
        "lat",
        "lng",
        "created_on",
        "updated_on",
        "updated_by",
        "is_deleted",
        "is_edited",
        "city",
        "flat_no",
        "annotation",
        "reverse_geo_code_failed",
        "image_url",
        "assured",
        "delivery_charge_info"
})
public class Address {

    @JsonProperty("rainMode")
    private Integer rainMode;
    @JsonProperty("delivery_valid")
    private Integer deliveryValid;
    @JsonProperty("estimated_sla")
    private Integer estimatedSla;
    @JsonProperty("estimated_sla_min")
    private Integer estimatedSlaMin;
    @JsonProperty("estimated_sla_max")
    private Integer estimatedSlaMax;
    @JsonProperty("recalculation_required")
    private Boolean recalculationRequired;
    @JsonProperty("id")
    private String id;
    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("default")
    private Integer _default;
    @JsonProperty("name")
    private String name;
    @JsonProperty("mobile")
    private String mobile;
    @JsonProperty("address")
    private String address;
    @JsonProperty("landmark")
    private String landmark;
    @JsonProperty("area")
    private String area;
    @JsonProperty("lat")
    private String lat;
    @JsonProperty("lng")
    private String lng;
    @JsonProperty("created_on")
    private String createdOn;
    @JsonProperty("updated_on")
    private String updatedOn;
    @JsonProperty("updated_by")
    private String updatedBy;
    @JsonProperty("is_deleted")
    private String isDeleted;
    @JsonProperty("is_edited")
    private String isEdited;
    @JsonProperty("city")
    private String city;
    @JsonProperty("flat_no")
    private String flatNo;
    @JsonProperty("annotation")
    private String annotation;
    @JsonProperty("reverse_geo_code_failed")
    private String reverseGeoCodeFailed;
    @JsonProperty("image_url")
    private String imageUrl;
    @JsonProperty("assured")
    private Boolean assured;
    @JsonProperty("delivery_charge_info")
    private String deliveryChargeInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Address() {
    }

    /**
     *
     * @param isEdited
     * @param imageUrl
     * @param rainMode
     * @param lng
     * @param updatedBy
     * @param city
     * @param id
     * @param estimatedSlaMax
     * @param annotation
     * @param deliveryChargeInfo
     * @param area
     * @param name
     * @param _default
     * @param userId
     * @param estimatedSla
     * @param updatedOn
     * @param estimatedSlaMin
     * @param lat
     * @param flatNo
     * @param assured
     * @param isDeleted
     * @param createdOn
     * @param landmark
     * @param recalculationRequired
     * @param address
     * @param deliveryValid
     * @param reverseGeoCodeFailed
     * @param mobile
     */
    public Address(Integer rainMode, Integer deliveryValid, Integer estimatedSla, Integer estimatedSlaMin, Integer estimatedSlaMax, Boolean recalculationRequired, String id, String userId, Integer _default, String name, String mobile, String address, String landmark, String area, String lat, String lng, String createdOn, String updatedOn, String updatedBy, String isDeleted, String isEdited, String city, String flatNo, String annotation, String reverseGeoCodeFailed, String imageUrl, Boolean assured, String deliveryChargeInfo) {
        super();
        this.rainMode = rainMode;
        this.deliveryValid = deliveryValid;
        this.estimatedSla = estimatedSla;
        this.estimatedSlaMin = estimatedSlaMin;
        this.estimatedSlaMax = estimatedSlaMax;
        this.recalculationRequired = recalculationRequired;
        this.id = id;
        this.userId = userId;
        this._default = _default;
        this.name = name;
        this.mobile = mobile;
        this.address = address;
        this.landmark = landmark;
        this.area = area;
        this.lat = lat;
        this.lng = lng;
        this.createdOn = createdOn;
        this.updatedOn = updatedOn;
        this.updatedBy = updatedBy;
        this.isDeleted = isDeleted;
        this.isEdited = isEdited;
        this.city = city;
        this.flatNo = flatNo;
        this.annotation = annotation;
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
        this.imageUrl = imageUrl;
        this.assured = assured;
        this.deliveryChargeInfo = deliveryChargeInfo;
    }

    @JsonProperty("rainMode")
    public Integer getRainMode() {
        return rainMode;
    }

    @JsonProperty("rainMode")
    public void setRainMode(Integer rainMode) {
        this.rainMode = rainMode;
    }

    @JsonProperty("delivery_valid")
    public Integer getDeliveryValid() {
        return deliveryValid;
    }

    @JsonProperty("delivery_valid")
    public void setDeliveryValid(Integer deliveryValid) {
        this.deliveryValid = deliveryValid;
    }

    @JsonProperty("estimated_sla")
    public Integer getEstimatedSla() {
        return estimatedSla;
    }

    @JsonProperty("estimated_sla")
    public void setEstimatedSla(Integer estimatedSla) {
        this.estimatedSla = estimatedSla;
    }

    @JsonProperty("estimated_sla_min")
    public Integer getEstimatedSlaMin() {
        return estimatedSlaMin;
    }

    @JsonProperty("estimated_sla_min")
    public void setEstimatedSlaMin(Integer estimatedSlaMin) {
        this.estimatedSlaMin = estimatedSlaMin;
    }

    @JsonProperty("estimated_sla_max")
    public Integer getEstimatedSlaMax() {
        return estimatedSlaMax;
    }

    @JsonProperty("estimated_sla_max")
    public void setEstimatedSlaMax(Integer estimatedSlaMax) {
        this.estimatedSlaMax = estimatedSlaMax;
    }

    @JsonProperty("recalculation_required")
    public Boolean getRecalculationRequired() {
        return recalculationRequired;
    }

    @JsonProperty("recalculation_required")
    public void setRecalculationRequired(Boolean recalculationRequired) {
        this.recalculationRequired = recalculationRequired;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("default")
    public Integer getDefault() {
        return _default;
    }

    @JsonProperty("default")
    public void setDefault(Integer _default) {
        this._default = _default;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("mobile")
    public String getMobile() {
        return mobile;
    }

    @JsonProperty("mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("landmark")
    public String getLandmark() {
        return landmark;
    }

    @JsonProperty("landmark")
    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    @JsonProperty("area")
    public String getArea() {
        return area;
    }

    @JsonProperty("area")
    public void setArea(String area) {
        this.area = area;
    }

    @JsonProperty("lat")
    public String getLat() {
        return lat;
    }

    @JsonProperty("lat")
    public void setLat(String lat) {
        this.lat = lat;
    }

    @JsonProperty("lng")
    public String getLng() {
        return lng;
    }

    @JsonProperty("lng")
    public void setLng(String lng) {
        this.lng = lng;
    }

    @JsonProperty("created_on")
    public String getCreatedOn() {
        return createdOn;
    }

    @JsonProperty("created_on")
    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty("updated_on")
    public String getUpdatedOn() {
        return updatedOn;
    }

    @JsonProperty("updated_on")
    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    @JsonProperty("updated_by")
    public String getUpdatedBy() {
        return updatedBy;
    }

    @JsonProperty("updated_by")
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @JsonProperty("is_deleted")
    public String getIsDeleted() {
        return isDeleted;
    }

    @JsonProperty("is_deleted")
    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    @JsonProperty("is_edited")
    public String getIsEdited() {
        return isEdited;
    }

    @JsonProperty("is_edited")
    public void setIsEdited(String isEdited) {
        this.isEdited = isEdited;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("flat_no")
    public String getFlatNo() {
        return flatNo;
    }

    @JsonProperty("flat_no")
    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    @JsonProperty("annotation")
    public String getAnnotation() {
        return annotation;
    }

    @JsonProperty("annotation")
    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    @JsonProperty("reverse_geo_code_failed")
    public String getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    @JsonProperty("reverse_geo_code_failed")
    public void setReverseGeoCodeFailed(String reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    @JsonProperty("image_url")
    public String getImageUrl() {
        return imageUrl;
    }

    @JsonProperty("image_url")
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @JsonProperty("assured")
    public Boolean getAssured() {
        return assured;
    }

    @JsonProperty("assured")
    public void setAssured(Boolean assured) {
        this.assured = assured;
    }

    @JsonProperty("delivery_charge_info")
    public String getDeliveryChargeInfo() {
        return deliveryChargeInfo;
    }

    @JsonProperty("delivery_charge_info")
    public void setDeliveryChargeInfo(String deliveryChargeInfo) {
        this.deliveryChargeInfo = deliveryChargeInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rainMode", rainMode).append("deliveryValid", deliveryValid).append("estimatedSla", estimatedSla).append("estimatedSlaMin", estimatedSlaMin).append("estimatedSlaMax", estimatedSlaMax).append("recalculationRequired", recalculationRequired).append("id", id).append("userId", userId).append("_default", _default).append("name", name).append("mobile", mobile).append("address", address).append("landmark", landmark).append("area", area).append("lat", lat).append("lng", lng).append("createdOn", createdOn).append("updatedOn", updatedOn).append("updatedBy", updatedBy).append("isDeleted", isDeleted).append("isEdited", isEdited).append("city", city).append("flatNo", flatNo).append("annotation", annotation).append("reverseGeoCodeFailed", reverseGeoCodeFailed).append("imageUrl", imageUrl).append("assured", assured).append("deliveryChargeInfo", deliveryChargeInfo).append("additionalProperties", additionalProperties).toString();
    }

}