package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "menu_item_id",
        "quantity",
        "addons",
        "variants"
})
public class Item {

    @JsonProperty("menu_item_id")
    private Integer menuItemId;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonProperty("addons")
    private List<Addon> addons = null;
    @JsonProperty("variants")
    private List<Variant> variants = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Item() {
    }

    /**
     *
     * @param menuItemId
     * @param addons
     * @param variants
     * @param quantity
     */
    public Item(Integer menuItemId, Integer quantity, List<Addon> addons, List<Variant> variants) {
        super();
        this.menuItemId = menuItemId;
        this.quantity = quantity;
        this.addons = addons;
        this.variants = variants;
    }

    @JsonProperty("menu_item_id")
    public Integer getMenuItemId() {
        return menuItemId;
    }

    @JsonProperty("menu_item_id")
    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    public Item withMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Item withQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    @JsonProperty("addons")
    public List<Addon> getAddons() {
        return addons;
    }

    @JsonProperty("addons")
    public void setAddons(List<Addon> addons) {
        this.addons = addons;
    }

    public Item withAddons(List<Addon> addons) {
        this.addons = addons;
        return this;
    }

    @JsonProperty("variants")
    public List<Variant> getVariants() {
        return variants;
    }

    @JsonProperty("variants")
    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public Item withVariants(List<Variant> variants) {
        this.variants = variants;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("menuItemId", menuItemId).append("quantity", quantity).append("addons", addons).append("variants", variants).toString();
    }

}