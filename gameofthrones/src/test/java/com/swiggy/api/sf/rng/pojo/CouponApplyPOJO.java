package com.swiggy.api.sf.rng.pojo;

import java.util.HashMap;
import java.util.Map;

import com.swiggy.api.sf.rng.helper.Utility;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "code",
        "cart",
        "headers",
        "paymentMethods",
        "time"
})
public class CouponApplyPOJO {

    @JsonProperty("code")
    private String code;
    @JsonProperty("cart")
    private CartPOJO cart;
    @JsonProperty("headers")
    private HeadersPOJO headers;
    @JsonProperty("paymentMethods")
    private PaymentMethodsPOJO paymentMethods;
    @JsonProperty("time")
    private Object time;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public CouponApplyPOJO() {
    }

    /**
     *
     * @param headers
     * @param time
     * @param paymentMethods
     * @param cart
     * @param code
     */
    public CouponApplyPOJO(String code, CartPOJO cart, HeadersPOJO headers, PaymentMethodsPOJO paymentMethods, Object time) {
        super();
        this.code = code;
        this.cart = cart;
        this.headers = headers;
        this.paymentMethods = paymentMethods;
        this.time = time;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    public CouponApplyPOJO withCode(String code) {
        this.code = code;
        return this;
    }

    @JsonProperty("cart")
    public CartPOJO getCart() {
        return cart;
    }

    @JsonProperty("cart")
    public void setCart(CartPOJO cart) {
        this.cart = cart;
    }

    public CouponApplyPOJO withCart(CartPOJO cart) {
        this.cart = cart;
        return this;
    }

    @JsonProperty("headers")
    public HeadersPOJO getHeaders() {
        return headers;
    }

    @JsonProperty("headers")
    public void setHeaders(HeadersPOJO headers) {
        this.headers = headers;
    }

    public CouponApplyPOJO withHeaders(HeadersPOJO headers) {
        this.headers = headers;
        return this;
    }

    @JsonProperty("paymentMethods")
    public PaymentMethodsPOJO getPaymentMethods() {
        return paymentMethods;
    }

    @JsonProperty("paymentMethods")
    public void setPaymentMethods(PaymentMethodsPOJO paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    public CouponApplyPOJO withPaymentMethods(PaymentMethodsPOJO paymentMethods) {
        this.paymentMethods = paymentMethods;
        return this;
    }

    @JsonProperty("time")
    public Object getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(Object time) {
        this.time = time;
    }

    public CouponApplyPOJO withTime(Object time) {
        this.time = time;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CouponApplyPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public CouponApplyPOJO setDefault(String couponCode){

        CartPOJO cart = new CartPOJO().setDefault();
        HeadersPOJO headers = new HeadersPOJO().setDefault();
        PaymentMethodsPOJO paymentMethods = new PaymentMethodsPOJO().setDefault();
        return this.withCart(cart)
                .withCode(couponCode)
                .withHeaders(headers)
                .withPaymentMethods(paymentMethods)
                .withTime(Utility.getPresentTimeInMilliSeconds());
    }

}
