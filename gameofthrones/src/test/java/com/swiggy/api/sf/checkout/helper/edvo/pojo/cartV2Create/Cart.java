package com.swiggy.api.sf.checkout.helper.edvo.pojo.cartV2Create;


import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
	     "cartItems", 
         "mealItems",
         "cart_type",
         "preorderSlot",
         "address_id",
         "couponCode",
         "restaurantId",
         "subscriptionItems"
})
public class Cart {

    @JsonProperty("preorderSlot")
    private PreorderSlot preorderSlot;
    @JsonProperty("address_id")
    private Integer addressId;
    @JsonProperty("cartItems")
    private List<CartItem> cartItems = null;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("cart_type")
    private String cartType = null;
    @JsonProperty("mealItems")
    private List<MealItem> mealItems = null;
    @JsonProperty("couponCode")
    private String couponCode = null;
    @JsonProperty("subscriptionItems")
    private List<SubscriptionItem> subscriptionItems = null;
    
    /**
     * No args constructor for use in serialization
     *
     */
    public Cart() {
    }

    /**
     *
     * @param cartItems
     * @param cartType
     * @param mealItems
     * @param restaurantId
     * @param couponCode
     * @param addressId
     */
    public Cart(PreorderSlot preorderSlot, Integer addressId, List<CartItem> cartItems, String restaurantId, String cartType, List<MealItem> mealItems,
    		          String couponCode ,List<SubscriptionItem> subscriptionItems) {
        super();
        this.addressId = addressId;
        this.cartItems = cartItems;
        this.restaurantId = restaurantId;
        this.cartType = cartType;
        this.mealItems = mealItems;
        this.couponCode=couponCode;
        this.subscriptionItems=subscriptionItems;
    }

    @JsonProperty("preorderSlot")
    public PreorderSlot getPreorderSlot() {
        return preorderSlot;
    }

    @JsonProperty("preorderSlot")
    public void setPreorderSlot(PreorderSlot preorderSlot) {
        this.preorderSlot = preorderSlot;
    }



    @JsonProperty("address_id")
    public Integer getAddressId() {
        return addressId;
    }

    @JsonProperty("address_id")
    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public Cart withAddressId(Integer addressId) {
        this.addressId = addressId;
        return this;
    }

    @JsonProperty("cartItems")
    public List<CartItem> getCartItems() {
        return cartItems;
    }

    @JsonProperty("cartItems")
    public void setCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public Cart withCartItems(List<CartItem> cartItems) {
        this.cartItems = cartItems;
        return this;
    }

    @JsonProperty("restaurantId")
    public String getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public Cart withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    @JsonProperty("cart_type")
    public String getCartType() {
        return cartType;
    }

    @JsonProperty("cart_type")
    public void setCartType(String cartType) {
        this.cartType = cartType;
    }

    public Cart withCartType(String cartType) {
        this.cartType = cartType;
        return this;
    }

    @JsonProperty("mealItems")
    public List<MealItem> getMealItems() {
        return mealItems;
    }

    @JsonProperty("mealItems")
    public void setMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
    }

    public Cart withMealItems(List<MealItem> mealItems) {
        this.mealItems = mealItems;
        return this;
    }

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }
    
    @JsonProperty("subscriptionItems")
    public List<SubscriptionItem> getSubscriptionItems() {
        return subscriptionItems;
    }

    @JsonProperty("subscriptionItems")
    public void setSubscriptionItems(List<SubscriptionItem> subscriptionItems) {
        this.subscriptionItems = subscriptionItems;
    }
    
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("preorderSlot", preorderSlot).append("addressId", addressId).append("cartItems", cartItems)
        		                        .append("restaurantId", restaurantId).append("cartType", cartType).append("mealItems", mealItems)
        		                        .append("couponCode", couponCode).append("subscriptionItems",subscriptionItems).toString();
    }

}