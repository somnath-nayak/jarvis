package com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.pojo.Daily.TDEnrichment
 **/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "stores"
})
public class TDEnrichment {

    @JsonProperty("stores")
    private List<Store> stores = null;

    @JsonProperty("stores")
    public List<Store> getStores() {
        return stores;
    }

    @JsonProperty("stores")
    public void setStores(List<Store> stores) {
        this.stores = stores;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("stores", stores).toString();
    }

    private void setDefaultValues(HashMap<String, HashMap<String,String>> hm) {
        Item item;
        Store store;
        List<Store> ls_store = new ArrayList<>();
        List<Item> ls_item = new ArrayList<>();
        for (String storeId:hm.keySet()) {
            HashMap<String,String> temp_hm = new HashMap<>();
            temp_hm = hm.get(storeId);
            for (String id: temp_hm.keySet()) {
                item = new Item();
                item.build(id,temp_hm.get(id));
                ls_item.add(item);
            }
            store = new Store();
            store.build(storeId,ls_item);
            ls_store.add(store);
        }
        if (this.getStores() == null)
            this.setStores(ls_store);
    }

    public TDEnrichment build(HashMap<String, HashMap<String,String>> hm) {
        setDefaultValues(hm);
        return this;
    }

}
