package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class CollectionLayout {

    private Integer collectionId;
    private EnabledLayouts enabledLayouts;

    /**
     * No args constructor for use in serialization
     *
     */
    public CollectionLayout() {
    }

    /**
     *
     * @param collectionId
     * @param enabledLayouts
     */
    public CollectionLayout(Integer collectionId, EnabledLayouts enabledLayouts) {
        super();
        this.collectionId = collectionId;
        this.enabledLayouts = enabledLayouts;
    }

    public Integer getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Integer collectionId) {
        this.collectionId = collectionId;
    }

    public EnabledLayouts getEnabledLayouts() {
        return enabledLayouts;
    }

    public void setEnabledLayouts(EnabledLayouts enabledLayouts) {
        this.enabledLayouts = enabledLayouts;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("collectionId", collectionId).append("enabledLayouts", enabledLayouts).toString();
    }

}