package com.swiggy.api.sf.rng.pojo.freebie;


import com.swiggy.api.sf.rng.helper.Utility;
import io.advantageous.boon.core.Str;
import org.apache.xpath.operations.Bool;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.rmi.CORBA.Util;
import java.util.List;

public class CreateFreebieTDEntryNewPojo {

    @JsonProperty("namespace")
    private String namespace;
    @JsonProperty("header")
    private String header;
    @JsonProperty("description")
    private String description;
    @JsonProperty("enabled")
    private Boolean enabled;
    @JsonProperty("discountLevel")
    private String discountLevel;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("restaurantList")
    private List<RestIdList> restaurantList = null;
    @JsonProperty("ruleDiscount")
    private RuleDiscount ruleDiscount;
    @JsonProperty("slots")
    private List<Object> slots = null;
    @JsonProperty("commissionOnFullBill")
    private Boolean commissionOnFullBill;
    @JsonProperty("taxesOnDiscountedBill")
    private Boolean taxesOnDiscountedBill;
    @JsonProperty("isSuper")
    private Boolean isSuper;
    @JsonProperty("firstOrderRestriction")
    private Boolean firstOrderRestriction;
    @JsonProperty("timeSlotRestriction")
    private Boolean timeSlotRestriction;
    @JsonProperty("userRestriction")
    private Boolean userRestriction;
    @JsonProperty("restaurantFirstOrder")
    private Boolean restaurantFirstOrder;
    @JsonProperty("surgeApplicable")
    private Boolean surgeApplicable;
    @JsonProperty("is_surge")
    private Boolean isSurge;
    @JsonProperty("valid_from")
    private String validFrom;
    @JsonProperty("valid_till")
    private String validTill;
    @JsonProperty("campaign_type")
    private String campaignType;
    @JsonProperty("restaurant_hit")
    private String restaurantHit;
    @JsonProperty("swiggy_hit")
    private String swiggyHit;
    @JsonProperty("dormant_user_type")
    private String dormantUserType;
    @JsonProperty("copyOverridden")
    private Boolean copyOverridden = false;
    @JsonProperty("freebiePlaceHolder")
    private String freebiePlaceHolder = "Default";
    @JsonProperty("shortDescription")
    private String shortDescription = "Default";

    /**
     * No args constructor for use in serialization
     */
    public CreateFreebieTDEntryNewPojo() {
    }

    /**
     * @param surgeApplicable
     * @param ruleDiscount
     * @param timeSlotRestriction
     * @param enabled
     * @param dormantUserType
     * @param restaurantFirstOrder
     * @param campaignType
     * @param restaurantList
     * @param commissionOnFullBill
     * @param userRestriction
     * @param swiggyHit
     * @param restaurantHit
     * @param validTill
     * @param header
     * @param namespace
     * @param firstOrderRestriction
     * @param createdBy
     * @param isSurge
     * @param description
     * @param slots
     * @param validFrom
     * @param discountLevel
     * @param taxesOnDiscountedBill
     */
    public CreateFreebieTDEntryNewPojo(String namespace, String header, String description, Boolean enabled, String discountLevel, String createdBy, List<RestIdList> restaurantList, RuleDiscount ruleDiscount, List<Object> slots, Boolean commissionOnFullBill, Boolean taxesOnDiscountedBill, Boolean firstOrderRestriction, Boolean timeSlotRestriction, Boolean userRestriction, Boolean restaurantFirstOrder, Boolean surgeApplicable, Boolean isSurge, String validFrom, String validTill, String campaignType, String restaurantHit, String swiggyHit, String dormantUserType) {
        super();
        this.namespace = namespace;
        this.header = header;
        this.description = description;
        this.enabled = enabled;
        this.discountLevel = discountLevel;
        this.createdBy = createdBy;
        this.restaurantList = restaurantList;
        this.ruleDiscount = ruleDiscount;
        this.slots = slots;
        this.commissionOnFullBill = commissionOnFullBill;
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        this.firstOrderRestriction = firstOrderRestriction;
        this.timeSlotRestriction = timeSlotRestriction;
        this.userRestriction = userRestriction;
        this.restaurantFirstOrder = restaurantFirstOrder;
        this.surgeApplicable = surgeApplicable;
        this.isSurge = isSurge;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.campaignType = campaignType;
        this.restaurantHit = restaurantHit;
        this.swiggyHit = swiggyHit;
        this.dormantUserType = dormantUserType;
    }

    @JsonProperty("namespace")
    public String getNamespace() {
        return namespace;
    }

    @JsonProperty("namespace")
    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public CreateFreebieTDEntryNewPojo withNamespace(String namespace) {
        this.namespace = namespace;
        return this;
    }

    @JsonProperty("header")
    public String getHeader() {
        return header;
    }

    @JsonProperty("header")
    public void setHeader(String header) {
        this.header = header;
    }

    public CreateFreebieTDEntryNewPojo withHeader(String header) {
        this.header = header;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public CreateFreebieTDEntryNewPojo withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("enabled")
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonProperty("enabled")
    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public CreateFreebieTDEntryNewPojo withEnabled(Boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    @JsonProperty("discountLevel")
    public String getDiscountLevel() {
        return discountLevel;
    }

    @JsonProperty("discountLevel")
    public void setDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
    }

    public CreateFreebieTDEntryNewPojo withDiscountLevel(String discountLevel) {
        this.discountLevel = discountLevel;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public CreateFreebieTDEntryNewPojo withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    @JsonProperty("restaurantList")
    public List<RestIdList> getRestaurantList() {
        return restaurantList;
    }

    @JsonProperty("restaurantList")
    public void setRestaurantList(List<RestIdList> restaurantList) {
        this.restaurantList = restaurantList;
    }

    public CreateFreebieTDEntryNewPojo withRestaurantList(List<RestIdList> restaurantList) {
        this.restaurantList = restaurantList;
        return this;
    }

    @JsonProperty("ruleDiscount")
    public RuleDiscount getRuleDiscount() {
        return ruleDiscount;
    }

    @JsonProperty("ruleDiscount")
    public void setRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
    }

    public CreateFreebieTDEntryNewPojo withRuleDiscount(RuleDiscount ruleDiscount) {
        this.ruleDiscount = ruleDiscount;
        return this;
    }

    @JsonProperty("slots")
    public List<Object> getSlots() {
        return slots;
    }

    @JsonProperty("slots")
    public void setSlots(List<Object> slots) {
        this.slots = slots;
    }

    public CreateFreebieTDEntryNewPojo withSlots(List<Object> slots) {
        this.slots = slots;
        return this;
    }

    @JsonProperty("commissionOnFullBill")
    public Boolean getCommissionOnFullBill() {
        return commissionOnFullBill;
    }

    @JsonProperty("commissionOnFullBill")
    public void setCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
    }

    public CreateFreebieTDEntryNewPojo withCommissionOnFullBill(Boolean commissionOnFullBill) {
        this.commissionOnFullBill = commissionOnFullBill;
        return this;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public Boolean getTaxesOnDiscountedBill() {
        return taxesOnDiscountedBill;
    }

    @JsonProperty("taxesOnDiscountedBill")
    public void setTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
    }

    public CreateFreebieTDEntryNewPojo withTaxesOnDiscountedBill(Boolean taxesOnDiscountedBill) {
        this.taxesOnDiscountedBill = taxesOnDiscountedBill;
        return this;
    }


    @JsonProperty("isSuper")
    public Boolean getIsSuper() {
        return isSuper;
    }

    @JsonProperty("isSuper")
    public void setTIsSuper(Boolean isSuper) {
        this.isSuper = isSuper;
    }

    public CreateFreebieTDEntryNewPojo withIsSuper(Boolean isSuper) {
        this.isSuper = isSuper;
        return this;
    }

    @JsonProperty("firstOrderRestriction")
    public Boolean getFirstOrderRestriction() {
        return firstOrderRestriction;
    }

    @JsonProperty("firstOrderRestriction")
    public void setFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
    }

    public CreateFreebieTDEntryNewPojo withFirstOrderRestriction(Boolean firstOrderRestriction) {
        this.firstOrderRestriction = firstOrderRestriction;
        return this;
    }

    @JsonProperty("timeSlotRestriction")
    public Boolean getTimeSlotRestriction() {
        return timeSlotRestriction;
    }

    @JsonProperty("timeSlotRestriction")
    public void setTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
    }

    public CreateFreebieTDEntryNewPojo withTimeSlotRestriction(Boolean timeSlotRestriction) {
        this.timeSlotRestriction = timeSlotRestriction;
        return this;
    }

    @JsonProperty("userRestriction")
    public Boolean getUserRestriction() {
        return userRestriction;
    }

    @JsonProperty("userRestriction")
    public void setUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
    }

    public CreateFreebieTDEntryNewPojo withUserRestriction(Boolean userRestriction) {
        this.userRestriction = userRestriction;
        return this;
    }

    @JsonProperty("restaurantFirstOrder")
    public Boolean getRestaurantFirstOrder() {
        return restaurantFirstOrder;
    }

    @JsonProperty("restaurantFirstOrder")
    public void setRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
    }

    public CreateFreebieTDEntryNewPojo withRestaurantFirstOrder(Boolean restaurantFirstOrder) {
        this.restaurantFirstOrder = restaurantFirstOrder;
        return this;
    }

    @JsonProperty("surgeApplicable")
    public Boolean getSurgeApplicable() {
        return surgeApplicable;
    }

    @JsonProperty("surgeApplicable")
    public void setSurgeApplicable(Boolean surgeApplicable) {
        this.surgeApplicable = surgeApplicable;
    }

    public CreateFreebieTDEntryNewPojo withSurgeApplicable(Boolean surgeApplicable) {
        this.surgeApplicable = surgeApplicable;
        return this;
    }

    @JsonProperty("is_surge")
    public Boolean getIsSurge() {
        return isSurge;
    }

    @JsonProperty("is_surge")
    public void setIsSurge(Boolean isSurge) {
        this.isSurge = isSurge;
    }

    public CreateFreebieTDEntryNewPojo withIsSurge(Boolean isSurge) {
        this.isSurge = isSurge;
        return this;
    }

    @JsonProperty("valid_from")
    public String getValidFrom() {
        return validFrom;
    }

    @JsonProperty("valid_from")
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public CreateFreebieTDEntryNewPojo withValidFrom(String validFrom) {
        this.validFrom = validFrom;
        return this;
    }

    @JsonProperty("valid_till")
    public String getValidTill() {
        return validTill;
    }

    @JsonProperty("valid_till")
    public void setValidTill(String validTill) {
        this.validTill = validTill;
    }

    public CreateFreebieTDEntryNewPojo withValidTill(String validTill) {
        this.validTill = validTill;
        return this;
    }

    @JsonProperty("campaign_type")
    public String getCampaignType() {
        return campaignType;
    }

    @JsonProperty("campaign_type")
    public void setCampaignType(String campaignType) {
        this.campaignType = campaignType;
    }

    public CreateFreebieTDEntryNewPojo withCampaignType(String campaignType) {
        this.campaignType = campaignType;
        return this;
    }

    @JsonProperty("restaurant_hit")
    public String getRestaurantHit() {
        return restaurantHit;
    }

    @JsonProperty("restaurant_hit")
    public void setRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
    }

    public CreateFreebieTDEntryNewPojo withRestaurantHit(String restaurantHit) {
        this.restaurantHit = restaurantHit;
        return this;
    }

    @JsonProperty("swiggy_hit")
    public String getSwiggyHit() {
        return swiggyHit;
    }

    @JsonProperty("swiggy_hit")
    public void setSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
    }

    public CreateFreebieTDEntryNewPojo withSwiggyHit(String swiggyHit) {
        this.swiggyHit = swiggyHit;
        return this;
    }

    @JsonProperty("dormant_user_type")
    public String getDormantUserType() {
        return dormantUserType;
    }

    @JsonProperty("dormant_user_type")
    public void setDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
    }

    public Boolean getCopyOverridden() {
        return copyOverridden;
    }

    public void setCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
    }

    public String getFreebie_place_holder() {
        return freebiePlaceHolder;
    }

    public void setFreebie_place_holder(String freebie_place_holder) {
        this.freebiePlaceHolder = freebie_place_holder;
    }

    public CreateFreebieTDEntryNewPojo withCopyOverridden(Boolean copyOverridden) {
        this.copyOverridden = copyOverridden;
        return this;
    }

    public CreateFreebieTDEntryNewPojo withFreebiePlaceHolder(String freebiePlaceHolder) {
        this.freebiePlaceHolder = freebiePlaceHolder;
        return this;
    }

    public CreateFreebieTDEntryNewPojo withDormantUserType(String dormantUserType) {
        this.dormantUserType = dormantUserType;
        return this;
    }


    public CreateFreebieTDEntryNewPojo createFreebieTdWithCopyOverridden(List<RestIdList> restaurantList,
                                                                         Boolean copyOverridden, Boolean isSuper, String minCartAmount , String itemId, String freebiePlaceHolder, String header, String description, String shortDescription) {
        this.namespace = "copyTest";
        this.header = header;
        this.shortDescription = shortDescription;
        this.description = description;
        this.enabled = true;
        this.discountLevel = "Restaurant";
        this.createdBy = "Ankita_CopyResolver_Automation";
        this.restaurantList = restaurantList;
        this.ruleDiscount = new RuleDiscount( "Freebie", "Restaurant", minCartAmount,  itemId);
        this.slots = null;
        this.commissionOnFullBill = true;
        this.taxesOnDiscountedBill = true;
        this.firstOrderRestriction = false;
        this.timeSlotRestriction = false;
        this.userRestriction = false;
        this.restaurantFirstOrder = false;
        this.surgeApplicable = false;
        this.isSurge = false;
        this.validFrom = Utility.getCurrentDate();
        this.validTill = Utility.getFutureDate();
        this.campaignType = "Freebie";
        this.restaurantHit = "50";
        this.swiggyHit = "50";
        this.dormantUserType = "ZERO_DAYS_DORMANT";
        this.copyOverridden = copyOverridden;
        this.freebiePlaceHolder = freebiePlaceHolder;
        this.isSuper = isSuper;

        return this;
    }

}


