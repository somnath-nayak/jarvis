package com.swiggy.api.sf.rng.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class PopulateMealData {

    private static int payLoadLimit = 1;
    private static final int mealPayLoadMinValue = 1;
    private static final int mealPayLoadMaxValue = 10;
    private static final String filePath = "D:\\perf\\meal_perf.csv";

    String discoveryPayload = "{\"meals\":[<mealPayload>], \"restaurant_ids\": [],\"first_order\": true}";
    String mealPayload = "{\"meal_id\":<meal_id>,\"restaurant_id\":<rest_id>}";
    TDPerfTest tDPerfTest = new TDPerfTest();

    public void writeDataToFile(String filePath, Map<Integer, String> map) {

        File listingFile = new File(filePath);
        FileWriter fw;
        try {
            fw = new FileWriter(listingFile);

            for (int i = 0; i < map.size(); i++) {
                String payload = map.get(i);
                System.out.println("payload " + payload);
                fw.write(payload);
                fw.write("\n");

            }
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createMealPayload() {
        Map<Integer, String> restIdToMealMap = new HashMap();
        for (int i = 0; i < payLoadLimit; i++) {
            String mealList = createMeal();
            restIdToMealMap.put(i, mealList);

        }
        writeDataToFile(filePath, restIdToMealMap);
    }

    private String createMeal() {
        List<String> meal_Payload = new ArrayList<String>();
        Map<String, List<String>> restIdAndMealId1 = tDPerfTest.getEDVORestIdMealId();
        List<String> restIds = new ArrayList<>();
        String mealPayLoadList = createMealRequest(restIdAndMealId1, restIds);
        meal_Payload.add(discoveryPayload.replace("<mealPayload>", mealPayLoadList));

        return String.join(",", meal_Payload);
    }

    private String createMealRequest(Map<String, List<String>> restIdAndMealId, List<String> restIds) {
        int mealLimit = Utility.getRandom(mealPayLoadMinValue, mealPayLoadMaxValue);
        List<String> mealListPayLoad = new ArrayList<String>();
        List<String> restId =new ArrayList();

        Set<String> keysSet = restIdAndMealId.keySet();
        for (String key : keysSet) {
            restId.add(key);
        }

        for (int i = 0; i < mealLimit; i++) {
            String newRest =	 Utility.getRandomFromList(restId);
            String mealId = Utility.getRandomFromList(restIdAndMealId.get(newRest));
            mealListPayLoad.add(mealPayload.replace("<rest_id>", newRest).replace("<meal_id>", mealId));
            restIds.add(newRest);
        }

        return String.join(",", mealListPayLoad);
    }

}
