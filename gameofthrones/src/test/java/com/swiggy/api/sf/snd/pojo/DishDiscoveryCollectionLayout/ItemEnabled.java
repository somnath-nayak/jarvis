package com.swiggy.api.sf.snd.pojo.DishDiscoveryCollectionLayout;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ItemEnabled {

    private String type;
    private Data data;

    /**
     * No args constructor for use in serialization
     *
     */
    public ItemEnabled() {
    }

    /**
     *
     * @param data
     * @param type
     */
    public ItemEnabled(String type, Data data) {
        super();
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("data", data).toString();
    }

}