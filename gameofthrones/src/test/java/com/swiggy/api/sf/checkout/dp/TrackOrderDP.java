package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.erp.delivery.helper.OrderStatus;
import com.swiggy.api.sf.checkout.helper.TrackOrderHelper;
import org.testng.annotations.DataProvider;

public class TrackOrderDP {
    protected static String mobile = "8197982351";
    protected static String password = "swiggy123";
    private static String restId = "6118";
    protected static int zoneId = 4;
    private static String userAgentAndroid = "Swiggy-Android";
    private static String userAgentIOS = "Swiggy-iOS";
    private static String userAgentWeb = "Web";
    private static String versionCodeAndroid = "230";
    private static String versionCodeIOS = "230";
    private static String versionCodeWeb = "230";
    private static String paymentMethodCOD = "Cash";
    private static String orderComments = "Test Order";

    @DataProvider(name="trackOrderTestData")
    public static Object[][] getTrackOrderTestData() {
        return new Object[][]{
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "done successfully"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 8, "Your order has been delivered. Bon Appetit!"},

                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "done successfully"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 8, "Your order has been delivered. Bon Appetit!"},

                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "done successfully"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 8, "Your order has been delivered. Bon Appetit!"}
        };
    }

    @DataProvider(name="trackOrderMinimalTestData")
    public static Object[][] getTrackOrderMinimalTestData() {
        return new Object[][]{
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentAndroid, versionCodeAndroid, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 0, "ORDER_TRACKED_SUCCESSFULLY"},

                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentIOS, versionCodeIOS, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 0, "ORDER_TRACKED_SUCCESSFULLY"},

                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, TrackOrderHelper.UNASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.ASSIGNED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.CONFIRMED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.ARRIVED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.PICKEDUP, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.REACHED, 0, "ORDER_TRACKED_SUCCESSFULLY"},
                {restId, userAgentWeb, versionCodeWeb, paymentMethodCOD, orderComments, OrderStatus.DELIVERED, 0, "ORDER_TRACKED_SUCCESSFULLY"}
        };
    }
}
