package com.swiggy.api.sf.snd.pojo.DD;

import java.util.List;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Filters {

    private List<RestaurantDiscountType> restaurantDiscountType = null;
    private List<RestaurantDiscountOpType> restaurantDiscountOpType = null;

    public List<RestaurantDiscountType> getRestaurantDiscountType() {
        return restaurantDiscountType;
    }

    public void setRestaurantDiscountType(List<RestaurantDiscountType> restaurantDiscountType) {
        this.restaurantDiscountType = restaurantDiscountType;
    }

    public List<RestaurantDiscountOpType> getRestaurantDiscountOpType() {
        return restaurantDiscountOpType;
    }

    public void setRestaurantDiscountOpType(List<RestaurantDiscountOpType> restaurantDiscountOpType) {
        this.restaurantDiscountOpType = restaurantDiscountOpType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("restaurantDiscountType", restaurantDiscountType).append("restaurantDiscountOpType", restaurantDiscountOpType).toString();
    }

}