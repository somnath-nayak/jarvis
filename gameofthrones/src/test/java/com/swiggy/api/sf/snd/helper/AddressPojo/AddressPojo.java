package com.swiggy.api.sf.snd.helper.AddressPojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class AddressPojo {

    private Integer id;
    private Integer userId;
    private Boolean defaultAddress;
    private String name;
    private String mobile;
    private String address;
    private String landmark;
    private String area;
    private Double lat;
    private Double lng;
    private Boolean deleted;
    private Boolean edited;
    private String annotation;
    private String flatNo;
    private Object city;
    private Boolean reverseGeoCodeFailed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getEdited() {
        return edited;
    }

    public void setEdited(Boolean edited) {
        this.edited = edited;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

    public String getFlatNo() {
        return flatNo;
    }

    public void setFlatNo(String flatNo) {
        this.flatNo = flatNo;
    }

    public Object getCity() {
        return city;
    }

    public void setCity(Object city) {
        this.city = city;
    }

    public Boolean getReverseGeoCodeFailed() {
        return reverseGeoCodeFailed;
    }

    public void setReverseGeoCodeFailed(Boolean reverseGeoCodeFailed) {
        this.reverseGeoCodeFailed = reverseGeoCodeFailed;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("userId", userId).append("defaultAddress", defaultAddress).append("name", name).append("mobile", mobile).append("address", address).append("landmark", landmark).append("area", area).append("lat", lat).append("lng", lng).append("deleted", deleted).append("edited", edited).append("annotation", annotation).append("flatNo", flatNo).append("city", city).append("reverseGeoCodeFailed", reverseGeoCodeFailed).toString();
    }

}


