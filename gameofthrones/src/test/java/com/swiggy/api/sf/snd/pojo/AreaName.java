package com.swiggy.api.sf.snd.pojo;
import org.apache.commons.lang.builder.ToStringBuilder;

public class AreaName {

    private String type;
    private String value;

    /**
     * No args constructor for use in serialization
     */
    public AreaName() {
    }

    /**
     * @param value
     * @param type
     */
    public AreaName(String type, String value) {
        super();
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("value", value).toString();
    }
}
