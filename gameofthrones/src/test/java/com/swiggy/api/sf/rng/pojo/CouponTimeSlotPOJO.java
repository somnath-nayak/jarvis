package com.swiggy.api.sf.rng.pojo;

import com.swiggy.api.sf.rng.helper.RngHelper;
import org.codehaus.jackson.annotate.JsonProperty;

public class CouponTimeSlotPOJO {

    @JsonProperty("day")
    private String day;
    @JsonProperty("openTime")
    private String openTime;
    @JsonProperty("closeTime")
    private String closeTime;
    @JsonProperty("code")
    private String code;
    @JsonProperty("id")
    private String id;
    @JsonProperty("couponId")
    private Integer couponId;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("updatedOn")
    private String updatedOn;
    @JsonProperty("createdOn")
    private String createdOn;


    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getCouponId() {
        return couponId;
    }

    public void setCouponId(Integer couponId) {
        this.couponId = couponId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(String updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public CouponTimeSlotPOJO withDay(String day) {
        this.day=day;
        return this;
    }

    public CouponTimeSlotPOJO withOpenTime(String openTime) {
        this.openTime=openTime;
        return this;
    }

    public CouponTimeSlotPOJO withCloseTime(String closeTime) {
        this.closeTime=closeTime;
        return this;
    }
    public CouponTimeSlotPOJO withCode(String code) {
        this.code=code;
        return this;
    }
    public CouponTimeSlotPOJO withId(String id) {
        this.id=id;
        return this;
    }
    public CouponTimeSlotPOJO withCouponId(Integer couponId) {
        this.couponId=couponId;
        return this;
    }
    public CouponTimeSlotPOJO withCreatedBy(String createdBy) {
        this.createdBy=createdBy;
        return this;
    }
    public CouponTimeSlotPOJO withUpdatedBy(String updatedBy) {
        this.updatedBy=updatedBy;
        return this;
    }
    public CouponTimeSlotPOJO withUpdatedOn(String updatedOn) {
        this.updatedOn=updatedOn;
        return this;
    }
    public CouponTimeSlotPOJO withCreatedOn(String createdOn) {
        this.createdOn=createdOn;
        return this;
    }



    public CouponTimeSlotPOJO setDefaultData(String couponCode, Integer couponId) {

        return this.withDay("all")
                .withOpenTime("2000")
                .withCloseTime("300")
                .withCode(couponCode)
                .withId("1234456")
                .withCouponId(couponId)
                .withCreatedBy("Binit")
                .withUpdatedBy("BINIT")
                .withUpdatedOn(String.valueOf(RngHelper.getCurrentDate()))
                .withCreatedOn(String.valueOf(RngHelper.getCurrentDate()));

    }


}

