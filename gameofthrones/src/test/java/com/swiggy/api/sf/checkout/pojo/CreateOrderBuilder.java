package com.swiggy.api.sf.checkout.pojo;

import com.swiggy.api.sf.checkout.constants.CheckoutConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CreateOrderBuilder {
    private CreateMenuEntry createMenuEntry;
    private Cart cart;
    Integer adddressId;

    public CreateOrderBuilder()
    {
        createMenuEntry=new CreateMenuEntry();
    }

    public CreateOrderBuilder cart(List<Cart> cartDetails) {
        ArrayList<Cart> cartEntries = new ArrayList<>();
        for (Cart cartEntry : cartDetails) {
            cartEntries.add(cartEntry);
        }
        createMenuEntry.setCartItems(cartEntries);
        return this;
    }

    public CreateOrderBuilder restaurant(Integer restaurant) {
        createMenuEntry.setRestaurantId(restaurant);
        return this;
    }

    public CreateOrderBuilder Address(Integer Address) {
        createMenuEntry.setaddress_id(Address);
        return this;
    }

    public CreateOrderBuilder paymentMethod(String paymentCodMethod) {
        createMenuEntry.setpayment_cod_method(paymentCodMethod);
        return this;
    }

    public CreateOrderBuilder orderComments(String orderComments) {
        createMenuEntry.setorder_comments(orderComments);
        return this;
    }

    public CreateOrderBuilder mobile(Long mobile)
    {
        createMenuEntry.setmobile(mobile);
        return this;
    }

    public CreateOrderBuilder password(String password)
    {
        createMenuEntry.setpassword(password);
        return this;
    }

    public CreateOrderBuilder name(String name)
    {
        createMenuEntry.setName(name);
        return this;
    }

    public CreateOrderBuilder addressMobile(String mobile)
    {
        createMenuEntry.setMobile1(mobile);
        return this;
    }

    public CreateOrderBuilder address(String address)
    {
        createMenuEntry.setAddress(address);
        return this;
    }

    public CreateOrderBuilder landmark(String landmark)
    {
        createMenuEntry.setLandmark(landmark);
        return this;
    }

    public CreateOrderBuilder area(String area)
    {
        createMenuEntry.setArea(area);
        return this;
    }

    public CreateOrderBuilder flat_no(String flat_no)
    {
        createMenuEntry.setflat_no(flat_no);
        return this;
    }

    public CreateOrderBuilder lat(String lat)
    {
        createMenuEntry.setLat(lat);
        return this;
    }

    public CreateOrderBuilder lng(String lng)
    {
        createMenuEntry.setLng(lng);
        return this;
    }

    public CreateOrderBuilder city(String city)
    {
        createMenuEntry.setCity(city);
        return this;
    }

    public CreateOrderBuilder annotation(String annotation)
    {
        createMenuEntry.setAnnotation(annotation);
        return this;
    }

    private void defaultAddress() throws IOException {
        if(createMenuEntry.getName()==null){
            createMenuEntry.setName("Abhiskek");
        }
        if(createMenuEntry.getMobile1()==null){
            createMenuEntry.setMobile1("7406734416");
        }
        if(createMenuEntry.getAddress()==null)
            createMenuEntry.setAddress("3444");

        if(createMenuEntry.getLandmark()==null){
            createMenuEntry.setLandmark("Abhiskek");
        }
        if(createMenuEntry.getArea()==null){
            createMenuEntry.setArea("7406734416");
        }
        if(createMenuEntry.getLat()==null)
            createMenuEntry.setLat("3444");

        if(createMenuEntry.getLng()==null){
            createMenuEntry.setLng("7406734416");
        }
        if(createMenuEntry.getflat_no()==null)
            createMenuEntry.setflat_no("3444");

        if(createMenuEntry.getCity()==null){
            createMenuEntry.setCity("Abhiskek");
        }
        if(createMenuEntry.getAnnotation()==null){
            createMenuEntry.setAnnotation("7406734416");
        }
    }

    public CreateMenuEntry buildAddress() throws IOException {
        defaultAddress();
        return createMenuEntry;
    }

    public CreateOrderBuilder restaurantListing(RestaurantListing restaurantListing)
    {
        createMenuEntry.setRESTAURANTLISTING(restaurantListing);
        return this;
    }
    
    public CreateOrderBuilder COLLECTIONListing(COLLECTION COLLECTION)
    {
        createMenuEntry.setCOLLECTION(COLLECTION);
        return this;
    }
    public CreateOrderBuilder CAROUSELListing(CAROUSEL CAROUSEL)
    {
        createMenuEntry.setCAROUSEL(CAROUSEL);
        return this;
    }

    private void defaultAggregator(){
        if(createMenuEntry.getRESTAURANTLISTING()==null)
        {
            createMenuEntry.setRESTAURANTLISTING(new RestaurantListing("12.333,32.32"));
        }
        if(createMenuEntry.getCAROUSEL()==null)
        {
            createMenuEntry.setCAROUSEL(new CAROUSEL("67,999"));
        }
        if(createMenuEntry.getCOLLECTION()==null)
        {
            COLLECTION collection= new COLLECTION();
            if(collection.getLatlng()==null)
                collection.setLatlng("12.333,32.32");
            createMenuEntry.setCOLLECTION(collection);
        }
    }

    public CreateMenuEntry buildAggregator() {
        defaultAggregator();
        return createMenuEntry;
    }

    private void defaultCart() throws IOException {

        if(null == createMenuEntry.getCartItems()){
            List <Cart> cartList=new ArrayList<>();
            Cart cart=new Cart();
            if(cart.getAddons()==null)
                cart.setAddons(new ArrayList<>());
            if(cart.getVariants()==null)
                cart.setVariants(new ArrayList<>());
            if(cart.getmenu_item_id()==null)
                cart.setmenu_item_id("677793");
            if(cart.getQuantity()==null)
                cart.setQuantity(1);
            cartList.add(cart);
            createMenuEntry.setCartItems(cartList);
        }
        if(createMenuEntry.getRestaurantId()==null)
            createMenuEntry.setRestaurantId(273);
    }

    public CreateMenuEntry buildCart() throws IOException {
        defaultCart();
        return createMenuEntry;
    }



    private void defaultLogin() {
        if(createMenuEntry.getpassword()==null){
            createMenuEntry.setpassword(CheckoutConstants.password1);
        }
        if(createMenuEntry.getmobile()==null){
            createMenuEntry.setmobile(CheckoutConstants.loginMobile);
        }
    }

    public CreateMenuEntry buildLogin() {
        defaultLogin();
        return createMenuEntry;
    }

    private void defaultPayment() {
        if(createMenuEntry.getpayment_cod_method()==null){
            createMenuEntry.setpayment_cod_method(CheckoutConstants.paymentCod);
        }
        if(createMenuEntry.getorder_comments()==null){
            createMenuEntry.setorder_comments(CheckoutConstants.orderComments);
        }
        if(createMenuEntry.getaddress_id()==null)
            //
            createMenuEntry.setaddress_id(adddressId);
    }

    public CreateMenuEntry buildOrderPlace() {
        defaultPayment();
        return createMenuEntry;
    }

    public CreateMenuEntry build() throws  IOException{
        defaultLogin();
        defaultCart();
        defaultPayment();
        return createMenuEntry;
    }

    public CreateMenuEntry buildTillAddress() throws  IOException{
        defaultLogin();
        defaultCart();
        defaultAddress();
        return createMenuEntry;
    }

    public CreateMenuEntry buildAll() throws IOException{
        defaultLogin();
        defaultAggregator();
        defaultCart();
        defaultAddress();
        defaultPayment();
        return createMenuEntry;
    }

    public CreateMenuEntry buildPayment() throws IOException{
        defaultCart();
        defaultAddress();
        defaultPayment();
        return createMenuEntry;
    }
}
