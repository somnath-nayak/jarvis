package com.swiggy.api.sf.rng.pojo;

/**
 * Created by kiran.j on 1/4/18.
 */
public class ItemRequest {

    private String restaurantId;
    private String categoryId;
    private String subCategoryId;
    private String itemId;
    private String count;
    private String price;

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public ItemRequest(String restaurantId, String categoryId, String subCategoryId, String itemId, String count, String price) {
        this.restaurantId = restaurantId;
        this.categoryId = categoryId;
        this.subCategoryId = subCategoryId;
        this.itemId = itemId;
        this.count = count;
        this.price = price;
    }

    public ItemRequest(){}

    @Override
    public String toString() {
        return "{" +
                "restaurantId='" + restaurantId + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", subCategoryId='" + subCategoryId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", count='" + count + '\'' +
                ", price='" + price + '\'' +
                '}';
    }
}
