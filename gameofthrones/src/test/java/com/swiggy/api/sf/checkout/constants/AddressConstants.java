package com.swiggy.api.sf.checkout.constants;

public interface AddressConstants {
    String statusmessage="Address saved successfully";
    String deleteMessage="Address deleted successfully";
    String invalidAddressMessage="Invalid request Data";
    int invalidStatusCode=1;
    int validStatusCode=0;
    String inValidMessage="Address id not valid for user id";
    String updateAddressMesage="Address updated successfully";
    String mobile= "7406734416";
}
