package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.EditOrderDp;
import com.swiggy.api.sf.snd.helper.EditOrder;
import com.swiggy.api.sf.snd.helper.SANDHelper;

import framework.gameofthrones.JonSnow.Processor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;


public class EditOrderTest extends EditOrderDp  {
    EditOrder editOrderHelper = new EditOrder();
    CMSHelper cmsHelper = new CMSHelper();
    SANDHelper helper = new SANDHelper();


    @Test(dataProvider = "SmartSuggestions",description = "verify smoke test",groups = "smoke")
    public void SmartSuggestions(String lat, String lng,String menu_type, String restId, String itemId ) {
        String response = editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(response, "$.statusCode");
        String statusMessage = JsonPath.read(response, "$.statusMessage");
        Assert.assertEquals(statusCode, SANDConstants.StatusCode);
        Assert.assertEquals(statusMessage, SANDConstants.msg);

    }

    @Test(dataProvider = "SmartSuggestions",description = "Check for valid combination of restaurant id and item id",groups = {"functional", "regression"})
    public void verifySmartSuggestionsMenuValid(String lat, String lng,String menu_type, String restId, String itemId ) {
        boolean response =  editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.DoesNodeExists("data.response");
        Assert.assertTrue(response, "Invalid combination");
    }

    @Test(dataProvider = "SmartSuggestionsInvalid",description = "Check for invalid combination of restaurant id and item id",groups = {"functional", "regression"})
    public void verifySmartSuggestionsMenuInValid(String lat, String lng,String menu_type, String restId, String itemId ) {
        String response = editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.GetBodyAsText();
        String resp = JsonPath.read(response,"$.data.response");
        Assert.assertEquals(resp,null,"valid combination");
    }

    @Test(dataProvider = "SmartSuggestionsCache",description = "Verify the suggestions when the suggestions are not there in cache",groups = {"functional", "regression"})
    public void verifySmartSuggestionsCache(String lat, String lng,String menu_type, String restId, String itemId ) {
        String response = editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.GetBodyAsText();
        List<String> respo =Arrays.asList(JsonPath.read(response,"$.data.response").toString());
        Assert.assertEquals(respo.get(0),"[]","Response is not empty");
    }

    @Test(dataProvider = "SmartSuggestionsInvalidMenuType",description = "Check for invalid menu type(Menu Type is not a compulsory parameter)",groups = {"functional", "regression"})
    public void verifySmartSuggestionsMenuInValidMenuType(String lat, String lng,String menu_type, String restId, String itemId ) {
        boolean response =  editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.DoesNodeExists("data.response");
        Assert.assertFalse(response, "Menu Type missing");
    }

    @Test(dataProvider = "SmartSuggestions",description = "Verify if item suggested is long distance eligible",groups = {"functional", "regression"})
    public void SmartSuggestionsLongDistance(String lat, String lng,String menu_type, String restId, String itemId) {
        String response = editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.GetBodyAsText();
        List<String> smartSuggestedItems = Arrays.asList(JsonPath.read(response, "$.data.response..menuItems..id").toString().replace("[", "").replace("]", "").split(","));
        for (int i = 0; i < smartSuggestedItems.size(); i++) {
            Assert.assertTrue(cmsHelper.getItemEligibleForLongDistance(smartSuggestedItems.get(i)),"Item is not long distance eligible");
        }
    }

    @Test(dataProvider = "SmartSuggestions",description = "Verify if item suggested is in stock",groups = {"functional", "regression"})
    public void SmartSuggestionsInStock(String lat, String lng,String menu_type, String restId, String itemId) {
        String response = editOrderHelper.EditOrderSuggestion(new String[]{lat, lng, menu_type, restId, itemId}).ResponseValidator.GetBodyAsText();
        List<String> smartSuggestedItems = Arrays.asList(JsonPath.read(response, "$.data.response..menuItems..inStock").toString().replace("[", "").replace("]", "").split(","));
        for (int i = 0; i < smartSuggestedItems.size(); i++) {
            Assert.assertEquals(smartSuggestedItems.get(i), "1", "Item is out of stock");
        }
    }

    @Test(dataProvider = "EditOrderMenu",description = "Verify smoke test",groups = "smoke")
    public void verifyEditOrderMenu(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId,lat,lng,orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        int statusCode = JsonPath.read(resp, "$.statusCode");
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(processor.ResponseValidator.GetResponseCode(), SANDConstants.responseCode);
        Assert.assertEquals(statusCode, SANDConstants.StatusCode);
        Assert.assertEquals(statusMessage, SANDConstants.msg);
    }

    @Test(dataProvider = "EditOrderMenuInvalidRest",description = "Verify get menu for invalid rest id",groups = {"functional", "regression"})
    public void verifyEditOrderMenuInvalidRest(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String statusMessage = JsonPath.read(resp, "$.statusMessage");
        Assert.assertEquals(statusMessage,"Restaurant not found",SANDConstants.msg);
    }

    @Test(dataProvider = "EditOrderMenuInvalidOrder",description = "Verify get menu for invalid order id",groups = {"functional", "regression"})
    public void verifyEditOrderMenuInvalidorder(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        List<String> respo =Arrays.asList(JsonPath.read(resp,"$.data..widgets..[?(@.type=='vendor_suggestions')].entities").toString().replace("[","").replace("]",""));
        Assert.assertEquals(respo.get(0),"","Response is not empty");
       }

    @Test(dataProvider = "EditOrderMenuvalidOrder",description = "Verify get menu for valid order id and rest id",groups = {"functional", "regression"})
    public void verifyEditOrderMenuvalidorder(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        List<String> respo =Arrays.asList(JsonPath.read(resp,"$.data..widgets..[?(@.type=='vendor_suggestions')].entities..id").toString().replace("[","").replace("]",""));
        Assert.assertNotNull(respo,"Response is not empty");
    }

    @Test(dataProvider = "EditOrderMenu",description = "Verify get menu for non serviceable lat long",groups = {"functional", "regression"})
    public void verifyEditOrderMenuServiceable(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String serviceability = JsonPath.read(resp, "$.data.sla.serviceability");
        Assert.assertEquals(serviceability,"SERVICEABLE","Restaurant is not serviveable");
    }

    @Test(dataProvider = "EditOrderMenuNonServiceable",description = "Verify get menu for non serviceable lat long",groups = {"functional", "regression"})
    public void verifyEditOrderMenuNonServiceable(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        String serviceability = JsonPath.read(resp, "$.data.sla.serviceability");
        Assert.assertEquals(serviceability,"NON_SERVICEABLE","Restaurant is serviveable");
    }

    @Test(dataProvider = "EditOrderMenuvalidOrder",description = "Verify get menu for suggestions which are out of stock",groups = {"functional", "regression"})
    public void verifyEditOrderMenuvalidorderOOS(String restId, String lat, String lng,String orderId) {
        Processor processor = editOrderHelper.getEditOrderMenu(restId, lat, lng, orderId);
        String resp = processor.ResponseValidator.GetBodyAsText();
        List<String> respo =Arrays.asList(JsonPath.read(resp,"$.data..widgets..[?(@.type=='vendor_suggestions')].entities..id").toString().replace("[","").replace("]",""));
        for (int i = 0; i < respo.size(); i++) {
            cmsHelper.setItemInStock(respo.get(i), 0);
            Assert.assertEquals(respo.get(0), "", "Items out of stock is shown in vendor suggestion");
        }
    }


    }

