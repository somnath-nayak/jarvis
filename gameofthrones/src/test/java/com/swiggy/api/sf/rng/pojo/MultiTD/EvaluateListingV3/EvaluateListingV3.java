package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateListingV3;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "firstOrder",
        "minCartAmount",
        "restaurantFeeMap",
        "restaurantFirstOrderMap",
        "restaurantIds",
        "userAgent",
        "userId",
        "versionCode"
})
public class EvaluateListingV3 {
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
//    @JsonProperty("minCartAmount")
//    private Integer minCartAmount;
    @JsonProperty("restaurantFeeMap")
    private RestaurantFeeMap restaurantFeeMap;
    @JsonProperty("restaurantFirstOrderMap")
    private RestaurantFirstOrderMap restaurantFirstOrderMap;
    @JsonProperty("restaurantIds")
    private List<Integer> restaurantIds = null;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("versionCode")
    private Integer versionCode;

    @JsonProperty("firstOrder")
    public Boolean getFirstOrder() {
        return firstOrder;
    }

    @JsonProperty("firstOrder")
    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

 //   @JsonProperty("minCartAmount")
//    public Integer getMinCartAmount() {
//        return minCartAmount;
//    }
//
//    @JsonProperty("minCartAmount")
//    public void setMinCartAmount(Integer minCartAmount) {
//        this.minCartAmount = minCartAmount;
//    }

    @JsonProperty("restaurantFeeMap")
    public RestaurantFeeMap getRestaurantFeeMap() {
        return restaurantFeeMap;
    }

    @JsonProperty("restaurantFeeMap")
    public void setRestaurantFeeMap(RestaurantFeeMap restaurantFeeMap) {
        this.restaurantFeeMap = restaurantFeeMap;
    }

    @JsonProperty("restaurantFirstOrderMap")
    public RestaurantFirstOrderMap getRestaurantFirstOrderMap() {
        return restaurantFirstOrderMap;
    }

    @JsonProperty("restaurantFirstOrderMap")
    public void setRestaurantFirstOrderMap(RestaurantFirstOrderMap restaurantFirstOrderMap) {
        this.restaurantFirstOrderMap = restaurantFirstOrderMap;
    }

    @JsonProperty("restaurantIds")
    public List<Integer> getRestaurantIds() {
        return restaurantIds;
    }

    @JsonProperty("restaurantIds")
    public void setRestaurantIds(List<Integer> restaurantIds) {
        this.restaurantIds = restaurantIds;
    }

    @JsonProperty("userAgent")
    public String getUserAgent() {
        return userAgent;
    }

    @JsonProperty("userAgent")
    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return this.userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @JsonProperty("versionCode")
    public Integer getVersionCode() {
        return versionCode;
    }

    @JsonProperty("versionCode")
    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public void setDefaultValues(int UserID, boolean firstOrder) {
        if (this.getFirstOrder() == null)
            this.setFirstOrder(firstOrder);
//        if (this.getMinCartAmount() == null)
//            this.setMinCartAmount(199);
        if (this.getUserAgent() == null)
            this.setUserAgent("WEB");
        if (this.getUserId() == null)
            this.userId = UserID;
        if (this.getRestaurantIds() == null)
            this.setRestaurantIds(new ArrayList<>());
        if (this.getVersionCode() == null)
            this.setVersionCode(303);
    }
    public void setDefaultValues(int UserID, boolean firstOrder,List<Integer> data) {
        if (this.getFirstOrder() == null)
            this.setFirstOrder(firstOrder);
//        if (this.getMinCartAmount() == null)
//            this.setMinCartAmount(199);
        if (this.getUserAgent() == null)
            this.setUserAgent("WEB");
        if (this.getUserId() == null)
            this.setUserId(UserID);
        if (this.getRestaurantIds() == null)
            this.setRestaurantIds(data);
        if (this.getVersionCode() == null)
            this.setVersionCode(0);
    }

    public EvaluateListingV3 build(int userId, boolean firstOrder) {
        setDefaultValues(userId,firstOrder);
        return this;
    }
    public EvaluateListingV3 build(int userId, boolean firstOrder,List<Integer> restIds) {
        setDefaultValues(userId,firstOrder);
        setRestaurantIds(restIds);
        return this;
    }
//    @Override
//    public String toString() {
//        return new ToStringBuilder(this).append("firstOrder", firstOrder).append("minCartAmount", minCartAmount).append("restaurantFeeMap", restaurantFeeMap).append("restaurantFirstOrderMap", restaurantFirstOrderMap).append("restaurantIds", restaurantIds).append("userAgent", userAgent).append("userId", userId).append("versionCode", versionCode).toString();
//    }

}

