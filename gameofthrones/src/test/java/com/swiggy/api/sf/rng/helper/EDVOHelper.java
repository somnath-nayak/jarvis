package com.swiggy.api.sf.rng.helper;

import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.rng.constants.EDVOConstants;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.EvaluateMenu;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.ItemRequests;
import com.swiggy.api.sf.rng.pojo.edvo.*;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.apache.tomcat.util.buf.StringUtils;
import org.testng.Assert;
import wiremock.com.jayway.jsonpath.JsonPath;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EDVOHelper {

    static Initialize gameofthrones = new Initialize();
    static GameOfThronesService service;
    static Processor processor;
    static SchemaValidatorUtils schemaValidate = new SchemaValidatorUtils();
    CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
    static int mealNum = 0;
    static int groupNum = 0;
    EDVOMeal edvoMeal = new EDVOMeal();
    EDVOMealPOJO edvoMealPOJO = new EDVOMealPOJO();
    EDVOCartV3Builder edvoCartV3Builder = new EDVOCartV3Builder();
    ItemRequestCartV3Builder itemRequestCartV3Builder = new ItemRequestCartV3Builder();
    MealItemRequest mealItemRequest = new MealItemRequest();

    public String discountsCreate(ArrayList<String> allRestIds, ArrayList<String> campOperationType,
                                  ArrayList<String> restMealCount, ArrayList<String> mealIds, ArrayList<String> mealsOperationMeta,
                                  ArrayList<String> mealRewards, ArrayList<String> mealCount, ArrayList<String> mealIsParent,
                                  ArrayList<Integer> numberOfGroupsInMeal, ArrayList<String> allGroupCounts, ArrayList<String> mealGroupIds,
                                  ArrayList<String> groupRewards) {
        int restCount = allRestIds.size();
        String discounts = "<discountPayload>";
        String discountPayload = "";
        String discountRequest = "";
        String restId = "";
        String finalDiscountPayload = "";
        System.out.println("ankita " + restCount);
        for (int i = 0; i < restCount; i++) {
            // String count = Integer.
            if (!(allRestIds.get(i) == null)) {
                restId = allRestIds.get(i);
            } else {
                restId = "" + null;
            }
            String restaurantId = "\"restaurant\":{\"id\": <restId>},";
            discountPayload = discountPayload + restaurantId.replace("<restId>", restId);
            discountPayload = discountPayload + operationType(campOperationType);
            discountPayload = discountPayload + mealsPayload(restMealCount, mealIds, i, mealsOperationMeta, mealCount,
                    mealIsParent, numberOfGroupsInMeal, allGroupCounts, mealGroupIds, mealRewards, groupRewards);
            if (i == restCount - 1) {
                discountPayload = "{" + discountPayload + "}";
                System.out.println("at the end   " + discountPayload);
            } else {
                discountPayload = "{" + discountPayload + "},";
                System.out.println("at the begaining   " + discountPayload);
            }

            System.out.println("discount " + discountPayload);
            finalDiscountPayload = finalDiscountPayload + discountPayload;
            discountPayload = "";
        }

        discounts = discounts.replace("<discountPayload>", finalDiscountPayload);
        // discounts.replace("<discountPayload>", discountPayload);
        // System.out.println("discount123333333" + discountRequest);
        mealNum = 0;
        groupNum = 0;
        return discounts;

    }


    public String mealsPayload(ArrayList<String> restMealCount, ArrayList<String> mealIds, int restNo,
                               ArrayList<String> mealsOperationMeta, ArrayList<String> mealCount, ArrayList<String> mealIsParent,
                               ArrayList<Integer> groupCount, ArrayList<String> countInAGroup, ArrayList<String> mealGroupIds,
                               ArrayList<String> mealRewards, ArrayList<String> groupRewards) {
        String meals = "\"meals\": [ <mealsPayload> ]";
        String mealsPayload = "";
        String mealId = "\"mealId\": <id>,";
        String rewardsPayload = "";
        String groupsPayload = "";
        String mealRequest = "";
        String numberOfMealsInRest = restMealCount.get(restNo);

        for (int i = 0; i < Integer.parseInt(numberOfMealsInRest); i++) {
            if (!(mealIds.get(mealNum) == null)) {
                mealsPayload = mealsPayload + mealId.replace("<id>", mealIds.get(mealNum));
            } else {
                mealsPayload = mealsPayload + mealId.replace("<id>", "" + null);
            }
            mealsPayload = mealsPayload + mealOperationMeta(mealsOperationMeta, i);
            rewardsPayload = rewards(mealRewards, mealNum);
            mealsPayload = mealsPayload + rewardsPayload + ",";
            groupsPayload = groupsPayload(groupCount, mealGroupIds, i, countInAGroup, groupRewards);
            mealsPayload = mealsPayload + groupsPayload;
            mealsPayload = mealsPayload + mealCount(mealCount, mealNum);
            mealsPayload = mealsPayload + mealIsParent(mealIsParent, mealNum);
            mealNum++;
            if (i < Integer.parseInt(numberOfMealsInRest) - 1) {
                mealsPayload = mealsPayload + "},{";
            } else {
                mealsPayload = mealsPayload + "}";
            }
            mealRequest = meals.replace("<mealsPayload>", "{" + mealsPayload);
        }
        // meals.replace("[<mealsPayload>]", mealsPayload);
        return mealRequest;
    }


    public static void deleteUserByMobile(String rest_id) {
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
        // List<Map<String, Object>> list =
        // sqlTemplate.queryForList(RngConstants.selectUser+ mobile);
        sqlTemplate.execute(EDVOConstants.deleteEDVOWithRestId + rest_id + EDVOConstants.endQuote);
    }



    public EDVOMealPOJO evaluateMealPayload(List<String> mealIds, String restaurantId, long minCartAmount, boolean firstOrder, boolean restFirstOrder, long userId) {
        Integer[] restaurantIds = {Integer.parseInt(restaurantId)};
        List<EDVOMeal> mealList = createMealList(mealIds, restaurantId);
        EDVOMealPOJO mealsPayload = edvoMealPOJO.setDefaultData(mealList, restaurantIds, minCartAmount, firstOrder, restFirstOrder, userId);

        return mealsPayload;
    }

    public List<EDVOMeal> createMealList(List<String> mealIds, String restaurantId) {
        List<EDVOMeal> mealList = new ArrayList<>();
        for (int i = 0; i < mealIds.size(); i++) {
            mealList.add(i, new EDVOMeal(mealIds.get(0), restaurantId));
        }
        return mealList;
    }

    // Create evaluate meal cartV3 payload
    public EDVOCartV3Builder evaluateEDVOCartV3Payload(List<String> mealIds, List<String> itemId, List<Integer> restaurantId, List<Integer> price, List<Integer> count, List<String> groupId, int minCartAmount, boolean firstOrder, boolean restaurantFirstorder, String userid) {
        List<MealItemRequest> mealItemRequest = createMealItemRequest(mealIds, itemId, restaurantId, price, count, groupId);

        EDVOCartV3Builder evaluateEDVOCartV3Payload = edvoCartV3Builder.withMealRequestData(mealItemRequest, minCartAmount, firstOrder, restaurantFirstorder, Long.parseLong(userid), "ANDROID", "400");
        return evaluateEDVOCartV3Payload;
    }

    //Create list of MealItemRequest for CartV3 Meal payload
    public List<MealItemRequest> createMealItemRequest(List<String> mealIds, List<String> itemId, List<Integer> restaurantId, List<Integer> price, List<Integer> count, List<String> groupId) {
        List<MealItemRequest> mealItemRequest = new ArrayList<>();
        for (int i = 0; i < mealIds.size(); i++) {
            mealItemRequest.add(i, new MealItemRequest(mealIds.get(i), itemId.get(i), Integer.toString(restaurantId.get(i)), price.get(i), count.get(i), groupId.get(i)));
        }
        return mealItemRequest;
    }

    // Create Meal, Item, Group list for  MealItemRequest test data set
    public List<List<String>> createMealItemGroupLists(String mealId, String groupId, String itemId) {
        List<String> mealIds = new ArrayList<>();
        List<String> groupIds = new ArrayList<>();
        List<String> itemIds = new ArrayList<>();
        List<List<String>> lists = new ArrayList<>();

        mealIds.add(0, mealId);
        groupIds.add(0, groupId);
        itemIds.add(0, itemId);

        lists.add(0, mealIds);
        lists.add(1, groupIds);
        lists.add(2, itemIds);
        return lists;
    }

    // Create Restaruat, Price, Count list for  MealItemRequest test data set
    public List<List<Integer>> createRestaurantPriceCountLists(int restaurantId, int price, int count) {
        List<Integer> restaurantIds = new ArrayList<>();
        List<Integer> priceList = new ArrayList<>();
        List<Integer> countList = new ArrayList<>();
        List<List<Integer>> lists = new ArrayList<>();
        restaurantIds.add(0, restaurantId);
        priceList.add(0, price);
        countList.add(0, count);

        lists.add(0, restaurantIds);
        lists.add(1, priceList);
        lists.add(2, countList);
        return lists;
    }

    // Create Restaurant cartV3 request with ItemRequest
    public ItemRequestCartV3Builder evaluateItemCartV3Payload(List<String> restaurantIdList, List<String> categoryIdList, List<String> subCategoryIdList, List<String> itemIdList, List<Integer> countList, List<Integer> priceList, int minCartAmount, boolean firstOrder, boolean restaurantFirstorder, String userid) {

        List<ItemRequests> itemRequestList = createCartV3ItemRequest(restaurantIdList, categoryIdList, subCategoryIdList, itemIdList,
                countList, priceList);
        //List<ItemRequest> ItemRequests, int minCartAmount, boolean firstOrder, boolean restFirstOrder, long userId, String userAgent, String versionCode) {
        ItemRequestCartV3Builder evaluateCartV3Payload = itemRequestCartV3Builder.withItemRequestData(itemRequestList, minCartAmount, firstOrder, restaurantFirstorder, Long.parseLong(userid), "ANDROID", "400");
        return evaluateCartV3Payload;
    }

    // Create list of ItemRequest in cartV3  Restaurant request
    public List<ItemRequests> createCartV3ItemRequest(List<String> restaurantIdList, List<String> categoryIdList, List<String> subCategoryIdList, List<String> itemIdList,
                                                      List<Integer> countList, List<Integer> priceList) {
        List<ItemRequests> itemRequest = new ArrayList<>();

        for (int i = 0; i < itemIdList.size(); i++) {
            itemRequest.add(i, new ItemRequests(restaurantIdList.get(0), categoryIdList.get(0), subCategoryIdList.get(0), itemIdList.get(0), countList.get(0).toString(), priceList.get(0).toString()));
        }
        return itemRequest;
    }

    public EvaluateMenu evaluateMenuPayload(List<String> restaurantIdList, List<String> categoryIdList, List<String> subCategoryIdList, List<String> itemIdList,
                                            List<Integer> countList, List<Integer> priceList, int minCartAmount, boolean firstOrder, int userId) {

        List<ItemRequests> itemRequestList = createCartV3ItemRequest(restaurantIdList, categoryIdList, subCategoryIdList, itemIdList, countList, priceList);
        EvaluateMenu evaluateMenu = new EvaluateMenu(minCartAmount, firstOrder, userId, "ANDROID", "400", itemRequestList);

        return evaluateMenu;
    }



//	public String discountsCreate(ArrayList<String> allRestIds, ArrayList<String> campOperationType,
//								  ArrayList<String> restMealCount, ArrayList<String> mealIds, ArrayList<String> mealsOperationMeta,
//								  ArrayList<String> mealRewards, ArrayList<String> mealCount, ArrayList<String> mealIsParent,
//								  ArrayList<Integer> numberOfGroupsInMeal, ArrayList<String> allGroupCounts, ArrayList<String> mealGroupIds,
//								  ArrayList<String> groupRewards) {
//		int restCount = allRestIds.size();
//		String discounts = "<discountPayload>";
//		String discountPayload = "";
//		String discountRequest = "";
//		String restId = "";
//		String finalDiscountPayload = "";
//		System.out.println("ankita " + restCount);
//		for (int i = 0; i < restCount; i++) {
//			// String count = Integer.
//			if (!(allRestIds.get(i) == null)) {
//				restId = allRestIds.get(i);
//			} else {
//				restId = "" + null;
//			}
//			String restaurantId = "\"restaurant\":{\"id\": <restId>},";
//			discountPayload = discountPayload + restaurantId.replace("<restId>", restId);
//			discountPayload = discountPayload + operationType(campOperationType);
//			discountPayload = discountPayload + mealsPayload(restMealCount, mealIds, i, mealsOperationMeta, mealCount,
//					mealIsParent, numberOfGroupsInMeal, allGroupCounts, mealGroupIds, mealRewards, groupRewards);
//			if (i == restCount - 1) {
//				discountPayload = "{" + discountPayload + "}";
//				System.out.println("at the end   " + discountPayload);
//			} else {
//				discountPayload = "{" + discountPayload + "},";
//				System.out.println("at the begaining   " + discountPayload);
//			}
//
//			System.out.println("discount " + discountPayload);
//			finalDiscountPayload = finalDiscountPayload + discountPayload;
//			discountPayload = "";
//		}
//
//		discounts = discounts.replace("<discountPayload>", finalDiscountPayload);
//		// discounts.replace("<discountPayload>", discountPayload);
//		// System.out.println("discount123333333" + discountRequest);
//		mealNum = 0;
//		groupNum = 0;
//		return discounts;
//
//	}

	public String operationType(ArrayList<String> campOperationType) {
		String operationType = " \"operationType\": <operationTypePayload>,";
		if (!(campOperationType.get(mealNum) == null)) {
			String operationTypePayload = campOperationType.get(mealNum);
			return operationType.replace("<operationTypePayload>", operationTypePayload);
		} else
			return operationType.replace("<operationTypePayload>", null);
	}


	public String mealOperationMeta(ArrayList<String> mealsOperationMeta, int mealNumber) {

		String operationMeta = "  <operationMetaPayload> ";
		String operationRequest = "";
		String operationMetaPayload = mealsOperationMeta.get(mealNum);
		operationRequest = operationMeta.replace("<operationMetaPayload>", operationMetaPayload);

		return operationRequest;
	}

	public String mealCount(ArrayList<String> count, int i) {
		String numberCount = count.get(mealNum);
		String mealCount = " \"count\": <numberCount>,";
		return mealCount.replace("<numberCount>", numberCount);
	}

	public String mealIsParent(ArrayList<String> isParent, int i) {
		String mealIsParent = isParent.get(mealNum);
		String isParentRequest = "\"isParent\": <value>";
		return isParentRequest.replace("<value>", mealIsParent);
	}

	public String rewards(ArrayList<String> allRewardValues, int mealNumber) {

		String reward = "  <rewardPayload> ";
		String rewardRequest = "";
		String rewardPayload = allRewardValues.get(mealNumber);
		rewardRequest = reward.replace("<rewardPayload>", rewardPayload);

		return rewardRequest;
	}

	public String groupsPayload(ArrayList<Integer> groupCount, ArrayList<String> groupIds, int mealNumber,
								ArrayList<String> countInAGroup, ArrayList<String> groupRewards) {
		String groups = " \"groups\": [<groupsPayload>],";
		String groupsPayload = "";
		String groupsRequest = "";
		String groupId = "\"groupId\": <groupId>,";
		String rewards = "<rewardsPayload>";
		String count = "\"count\": <number>,";
		String finalPayload = "";
		int numberOfgroupsInAMeal = groupCount.get(mealNumber);
		if (!(groupIds.get(groupNum) == null)) {
			for (int i = 0; i < numberOfgroupsInAMeal; i++) {
				System.out.println("grp payload " + groupsPayload);
				groupsPayload = "{" + groupsPayload;
				System.out.println("grp payload " + groupsPayload);
				groupsPayload = groupsPayload + groupId.replace("<groupId>", groupIds.get(groupNum));
				groupsPayload = groupsPayload + count.replace("<number>", countInAGroup.get(groupNum));
				groupsPayload = groupsPayload + rewards.replace("<rewardsPayload>", rewards(groupRewards, groupNum));
				groupsPayload = groupsPayload + "}";
				groupNum++;
				if (i == numberOfgroupsInAMeal - 1) {
					groupsPayload = groupsPayload;
				} else {
					groupsPayload = groupsPayload + ",";
				}
				finalPayload = finalPayload + groupsPayload;

				groupsPayload = "";

				// groupsPayload = groupsPayload + rewards(groupRewards, mealNumber);
			}

			groupsRequest = groups.replace("<groupsPayload>", finalPayload);
		} else {
			groupsRequest = groups.replace("[<groupsPayload>]", "" + null);
		}
		return groupsRequest;
	}

	public Processor createEDVOTradeDiscount(HashMap<String, String> edvoData) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "createEDVOCampaign", gameofthrones);
		String[] edvoPayload = new String[edvoData.size()];
		for (int i = 0; i < edvoData.size(); i++) {
			edvoPayload[i] = edvoData.get(Integer.toString(i));
			System.out.println("" + edvoData.get(Integer.toString(i)));
		}
		Processor processor = new Processor(service, requestHeaders, edvoPayload);
		// System.out.println("In helper method");
		return processor;
	}

	public void deleteEDVOCampaignFromDB(String edvoCampaignId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		sqlTemplate.update(RngConstants.deleteEDVOCampaign + edvoCampaignId + RngConstants.endQuotes);

	}

	public HashMap<String, String> createEDVOWithDefaultValues(String discount, String day, int openTime, int closeTime,
															   int time) {

		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(time));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13", discount);
		if (!(day == null)) {
			createEDVOCamp.put("14", new EDVOTimeSlot(day, openTime, closeTime).toString());
		} else {
			createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		}
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", EDVOConstants.firstOrderRestriction);
		if (!(day == null)) {
			createEDVOCamp.put("18", "true");
		} else {
			createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		}
		createEDVOCamp.put("19", EDVOConstants.dormant_user_type);
		createEDVOCamp.put("20", EDVOConstants.restaurantFirstOrder);
		createEDVOCamp.put("21", EDVOConstants.userRestriction);

		return createEDVOCamp;

	}

	public String createMultipleTimeSlot(String[] days, int[] openTime, int[] closeTime) {
		List<String> list = new ArrayList();

		for (int i = 0; i < days.length; i++) {
			list.add(new EDVOTimeSlot(days[i], openTime[i], closeTime[i]).toPojo());
		}

		return "[" + StringUtils.join(list, ',') + "]";

	}

	public String checkEDVOCampaignShortDescriptionFromDB(String edvoCampaignId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.marketingDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(EDVOConstants.selectEDVOCampShortDescription + edvoCampaignId + EDVOConstants.endQuote);
		return (list.size() > 0) ? (list.get(0).get("short_description").toString())
				: "short Description is not persent in DB";
	}

	public HashMap<String, String> createUserCutMultiTD(boolean userRestriction, boolean firstOrderRestriction,
														boolean restaurantFirstOrder, String dormantType, String restId, String mealId) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", Boolean.toString(firstOrderRestriction));
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", Boolean.toString(restaurantFirstOrder));
		createEDVOCamp.put("21", Boolean.toString(userRestriction));

		return createEDVOCamp;
	}

	public String createMealData(ArrayList<String> listOfMealIds, ArrayList<String> listOfRestIds,
								 ArrayList<String> numberOfMealsInARest) {
		List<String> list = new ArrayList();
		for (int j = 0; j < listOfRestIds.size(); j++) {
			for (int i = 0; i < listOfMealIds.size(); i++) {
				list.add(new EDVOMeal(listOfMealIds.get(i), listOfRestIds.get(j)).toString());
			}
		}
		return StringUtils.join(list, ',');
	}

	public Processor mealEvaluate(HashMap<String, String> mealEvaluate) {

		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "meal", gameofthrones);
		String mealPayload[] = new String[mealEvaluate.size()];
		for (int i = 0; i < mealEvaluate.size(); i++) {
			mealPayload[i] = mealEvaluate.get(Integer.toString(i));
			System.out.println("size" + mealEvaluate.get(Integer.toString(i)));
		}

		// System.out.println("services" + service);
		// System.out.println("headers" + requestHeader);
		// System.out.println("size" + mealPayload.length);
		Processor processor = new Processor(service, requestHeader, mealPayload);

		return processor;
	}

	public String createCartMealItemRequest(ArrayList<String> numOfMealItemRequestInCart,
											ArrayList<String> listOfMealIds, ArrayList<String> listofItemIds, ArrayList<String> listofRestIds,
											ArrayList<String> listOfAllItemsPrice, ArrayList<String> listOfItemCountInMeal,
											ArrayList<String> listOfGroupIds) {

		List<String> mealCartItemRequests = new ArrayList();
		if (!(numOfMealItemRequestInCart.get(0).equalsIgnoreCase("0"))) {
			for (int i = 0; i < numOfMealItemRequestInCart.size(); i++) {
				mealCartItemRequests.add(
						new EDVOCartMealItemRequest(listOfMealIds.get(i), listofItemIds.get(i), listofRestIds.get(i),
								listOfAllItemsPrice.get(i), listOfItemCountInMeal.get(i), listOfGroupIds.get(i))
								.toString());
			}
		}
		if (!(numOfMealItemRequestInCart.get(0).equalsIgnoreCase("0"))) {
			return "" + StringUtils.join(mealCartItemRequests, ',');
		} else {
			return new EDVOCartMealItemRequest().toString();
		}
	}

	public Processor cartV3Evaluate(HashMap<String, String> cartPayload) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "cartV3Evaluate", gameofthrones);
		String[] cartRequest = new String[cartPayload.size()];
		for (int i = 0; i < cartPayload.size(); i++) {
			cartRequest[i] = cartPayload.get(Integer.toString(i));
		}
		Processor processor = new Processor(service, requestHeader, cartRequest);
		return processor;
	}

	public HashMap<String, String> createDormantTypeEDVOCampaign(String dormantType, String restId, String mealId,
																 String groupId) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId);

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");

		return createEDVOCamp;
	}

	public HashMap<String, String> createCampaignWithUserCut(boolean userRestriction, boolean firstOrderRestriction,
															 boolean restaurantFirstOrder, String dormantType, String mealReward, String groupReward, String restId,
															 String mealId, String groupId) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, mealReward);

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId);

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, groupReward);

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", Boolean.toString(firstOrderRestriction));
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", Boolean.toString(restaurantFirstOrder));
		createEDVOCamp.put("21", Boolean.toString(userRestriction));

		System.out.println("namespace" + createEDVOCamp.get("0"));
		return createEDVOCamp;
	}

	public HashMap<String, String> createCampaignWithUserCutAndTimeValidity(boolean userRestriction,
																			boolean firstOrderRestriction, boolean restaurantFirstOrder, String dormantType, String mealReward,
																			String groupReward, String restId, String mealId, String groupId, int validFrom, int validTill) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, mealReward);

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId);

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, groupReward);

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getFuturetimeInMilliSeconds(validFrom));
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(validTill));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", Boolean.toString(firstOrderRestriction));
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", Boolean.toString(restaurantFirstOrder));
		createEDVOCamp.put("21", Boolean.toString(userRestriction));

		System.out.println("namespace" + createEDVOCamp.get("0"));
		return createEDVOCamp;
	}

	public HashMap<String, String> createCampaignWithUserCutAndMinCartAmount(boolean userRestriction,
																			 boolean firstOrderRestriction, boolean restaurantFirstOrder, String dormantType, String mealReward,
																			 String groupReward, String restId, String mealId, String groupId, String minCartAmount) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, mealReward);

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId);

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, groupReward);

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", Boolean.toString(firstOrderRestriction));
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", Boolean.toString(restaurantFirstOrder));
		createEDVOCamp.put("21", Boolean.toString(userRestriction));

		System.out.println("namespace" + createEDVOCamp.get("0"));
		return createEDVOCamp;
	}

	public Processor getTradeDiscountV2(String campaignIds) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "V2getCampaigns", gameofthrones);
		String[] edvoPayload = new String[] { campaignIds };
		Processor processor = new Processor(service, requestHeaders, edvoPayload);
		// System.out.println("In helper method");
		return processor;

	}

	public String getMealId() {
		return Integer.toString(Utility.getRandom(500000, 10000000));
	}

	public String getRestId() {
		return Integer.toString(Utility.getRandom(50000000, 100000000));
	}

	public int createEDVOCamp(String restId, String mealId) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();
		deleteUserByMobile( restId);


		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0, new CreateReward("NONE", "FinalPrice", "120", "1").toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, Integer.toString(Utility.getRandom(1000, 40000)));

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", EDVOConstants.header);
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", "false");
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", "ZERO_DAYS_DORMANT");
		createEDVOCamp.put("20", "false");
		createEDVOCamp.put("21", "false");
		Processor EDVOCampResp = createEDVOTradeDiscount(createEDVOCamp);
		int statusCode = EDVOCampResp.ResponseValidator.GetNodeValueAsInt("$.statusCode");
		String statusMessage = EDVOCampResp.ResponseValidator.GetNodeValue("$.statusMessage");
//		int campaignId = EDVOCampResp.ResponseValidator.GetNodeValueAsInt("$.data");
		int campaignId =  (int)JsonPath.read(EDVOCampResp.ResponseValidator.GetBodyAsText(), "$.data");
		Assert.assertEquals(statusCode, 0);
		Assert.assertEquals(statusMessage, "success");

		return campaignId;
	}


	// method for TD-caching

	public HashMap<String, String> createMealLevelCampaign(String header,boolean userRestriction, boolean firstOrderRestriction,
														   boolean restaurantFirstOrder, String dormantType, String restId,
														   String mealId, String groupId,String mealRewardFunction , String mealRewardType,
														   String mealRewardValue , String mealRewardCount) {
		ArrayList<String> listOfRestIds = new ArrayList();
		ArrayList<String> operationTypeOfCamp = new ArrayList<String>();
		ArrayList<String> numberOfMealsInARest = new ArrayList();
		ArrayList<String> allMealIds = new ArrayList();
		ArrayList<String> allMealsOperationMeta = new ArrayList<String>();
		ArrayList<String> allMealRewards = new ArrayList<String>();
		ArrayList<String> mealCount = new ArrayList<String>();
		ArrayList<String> mealIsParent = new ArrayList<String>();
		ArrayList<Integer> numberOfGroupsInEachMeal = new ArrayList<Integer>();
		ArrayList<String> allGroupIdsOfMeals = new ArrayList<String>();
		ArrayList<String> allGroupCounts = new ArrayList<String>();
		ArrayList<String> allGroupsRewards = new ArrayList<String>();
		HashMap<String, String> createEDVOCamp = new HashMap<String, String>();

		// listOfAllRestaurantIds
		listOfRestIds.add(0, restId);

		// list of operationType of restaurants in campaign
		operationTypeOfCamp.add(0, "\"MEAL\"");

		// list of number of meals in restaurants
		numberOfMealsInARest.add(0, "1");

		// list of All MealIds
		allMealIds.add(0, mealId);

		// list of all Meals OperationMeta
		allMealsOperationMeta.add(0,
				new CreateEDVOOperationMeta("buy G1 at FINAL Price 120 ", "buy G1 at FINAL Price 120 ").toString());

		// list of all Meal rewards
		allMealRewards.add(0,  new CreateReward( mealRewardFunction, mealRewardType, mealRewardValue, mealRewardCount).toString());

		// list of number of "count" in a meal
		mealCount.add(0, "1");

		// isParent value of Meals
		mealIsParent.add(0, "true");

		// list of number of Groups in each meal
		numberOfGroupsInEachMeal.add(0, 1);

		// list of groupIds in Meal
		allGroupIdsOfMeals.add(0, groupId);

		// list of count in a group
		allGroupCounts.add(0, "1");

		// list of all GroupsReward
		allGroupsRewards.add(0, new CreateReward().toString());

		createEDVOCamp.put("0", EDVOConstants.namespace);
		createEDVOCamp.put("1", "\""+header+"\"");
		createEDVOCamp.put("2", EDVOConstants.description);
		createEDVOCamp.put("3", EDVOConstants.shortDescription);
		createEDVOCamp.put("4", Utility.getPresentTimeInMilliSeconds());
		createEDVOCamp.put("5", Utility.getFuturetimeInMilliSeconds(EDVOConstants.campValidTillTime));
		createEDVOCamp.put("6", EDVOConstants.campaign_type);
		createEDVOCamp.put("7", EDVOConstants.restaurant_hit);
		createEDVOCamp.put("8", EDVOConstants.enabled);
		createEDVOCamp.put("9", EDVOConstants.swiggy_hit);
		createEDVOCamp.put("10", EDVOConstants.createdBy);
		createEDVOCamp.put("11", EDVOConstants.discountCap);
		createEDVOCamp.put("12", EDVOConstants.minCartAmount);
		createEDVOCamp.put("13",
				discountsCreate(listOfRestIds, operationTypeOfCamp, numberOfMealsInARest, allMealIds,
						allMealsOperationMeta, allMealRewards, mealCount, mealIsParent, numberOfGroupsInEachMeal,
						allGroupCounts, allGroupIdsOfMeals, allGroupsRewards).toString());

		createEDVOCamp.put("14", new EDVOTimeSlot().toString());
		createEDVOCamp.put("15", EDVOConstants.commissionOnFullBill);
		createEDVOCamp.put("16", EDVOConstants.taxesOnDiscountedBill);
		createEDVOCamp.put("17", Boolean.toString(firstOrderRestriction));
		createEDVOCamp.put("18", EDVOConstants.timeSlotRestriction);
		createEDVOCamp.put("19", dormantType);
		createEDVOCamp.put("20", Boolean.toString(restaurantFirstOrder));
		createEDVOCamp.put("21", Boolean.toString(userRestriction));

		System.out.println("namespace" + createEDVOCamp.get("0"));
		//return createEDVOCamp;

		Processor processor=createEDVOTradeDiscount(createEDVOCamp);
		int campaignId = processor.ResponseValidator.GetNodeValueAsInt("$.data");
		HashMap Keys = new HashMap();
		Keys.put("campaign_id", Integer.toString(campaignId));
		return Keys;
	}
}
