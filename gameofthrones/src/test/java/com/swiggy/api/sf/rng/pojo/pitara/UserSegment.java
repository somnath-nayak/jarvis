
package com.swiggy.api.sf.rng.pojo.pitara;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "newUser"
})
public class UserSegment {

    @JsonProperty("userId")
    private Integer userId;
    @JsonProperty("newUser")
    private Integer newUser;

    /**
     * No args constructor for use in serialization
     * 
     */
    public UserSegment() {
    }

    /**
     * 
     * @param userId
     * @param newUser
     */
    public UserSegment(Integer userId, Integer newUser) {
        super();
        this.userId = userId;
        this.newUser = newUser;
    }

    @JsonProperty("userId")
    public Integer getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public UserSegment withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    @JsonProperty("newUser")
    public Integer getNewUser() {
        return newUser;
    }

    @JsonProperty("newUser")
    public void setNewUser(Integer newUser) {
        this.newUser = newUser;
    }

    public UserSegment withNewUser(Integer newUser) {
        this.newUser = newUser;
        return this;
    }

}
