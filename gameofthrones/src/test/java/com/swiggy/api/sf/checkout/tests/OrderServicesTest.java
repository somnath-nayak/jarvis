package com.swiggy.api.sf.checkout.tests;

import java.io.IOException;
import java.util.HashMap;

import org.codehaus.jackson.JsonNode;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.dp.CheckoutDP;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import com.swiggy.api.sf.snd.helper.SnDHelper;
import com.swiggy.automation.common.utils.APIUtils;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;

public class OrderServicesTest extends CheckoutDP {

	Initialize gameofthrones = new Initialize();
	CheckoutHelper helper = new CheckoutHelper();
	SnDHelper sndHelper = new SnDHelper();
	SANDHelper sandHelper = new SANDHelper();
	SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
	RngHelper rngHelper = new RngHelper();
	JsonHelper jsonHelper = new JsonHelper();
	HashMap<String, String> hashMap=null;
	String order_id = null;
	String order_key = null;
	String order = null;
	
	@Test(dataProvider = "orderplaceRegression")
	public void OrderWithoutAuth(CreateMenuEntry cartpayload,String tid,String token,int expectedCode,String message) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(cartpayload.getCartItems());
	  	HashMap<String, String> hashMap=null;
    	    hashMap = helper.createLogin(CheckoutConstants.password1, Long.parseLong(CheckoutConstants.mobile1));
		String response = helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload,
				cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		System.out.println(response);
		String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]",
				"");
		String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		String[] menuItems = menuItem.split(",");
		int noOfProducts = menuItems.length;
		helper.validateDataFromResponseNew(response, restId, menuItem, noOfProducts);
		order=helper.orderPlace1(tid, token, cartpayload.getaddress_id(),
				cartpayload.getpayment_cod_method(), cartpayload.getorder_comments()).ResponseValidator.GetBodyAsText();
        String statusMessage=JsonPath.read(order,"$.statusMessage");
        int statusCode=JsonPath.read(order,"$.statusCode");
		Assert.assertEquals(expectedCode,statusCode);	
        Assert.assertEquals(message,statusMessage);
	}
	
	
	@Test(dataProvider = "userSingleOrder", priority = 19)
	public void testOrder(CreateMenuEntry cartpayload) throws IOException {
		String cartPayload = jsonHelper.getObjectToJSON(cartpayload.getCartItems());
		hashMap = helper.createLogin(cartpayload.getpassword(), cartpayload.getmobile());
		String response = helper.CreateCartAddon(hashMap.get("Tid"), hashMap.get("Token"), cartPayload,
				cartpayload.getRestaurantId()).ResponseValidator.GetBodyAsText();
		
		String restId = JsonPath.read(response, "$.data.restaurant_details.id").toString().replace("[", "").replace("]",
				"");
		String menuItem = JsonPath.read(response, "$.data.cart_menu_items..menu_item_id").toString().replace("[", "")
				.replace("]", "");
		String[] menuItems = menuItem.split(",");
		int noOfProducts = menuItems.length;
		helper.validateDataFromResponseNew(response, restId, menuItem, noOfProducts);
		order = helper.orderPlace1(hashMap.get("Tid"), hashMap.get("Token"), cartpayload.getaddress_id(),
				cartpayload.getpayment_cod_method(), cartpayload.getorder_comments()).ResponseValidator.GetBodyAsText();

		order_id = JsonPath.read(order, "$.data.order_id").toString().replace("[", "").replace("]", "");
		order_key = JsonPath.read(order, "$.data.key").toString().replace("[", "").replace("]", "");
		System.out.println(order_id);
		Assert.assertNotNull(order_id);
	}

	@Test(dependsOnMethods = "testOrder", priority = 20, groups = { "Regression",
			"Smoke" }, description = " 1.place an order 2.Verify using get single order key")
	public void getOrder() {
		Processor p = helper.getOrder(hashMap.get("Tid"), hashMap.get("Token"));
		String getAllOrder = p.ResponseValidator.GetBodyAsText();
		try {
			JsonNode node = APIUtils.convertStringtoJSON(getAllOrder);
			Assert.assertEquals(node.get("statusCode").asInt(), 0);
			Assert.assertTrue(node.get("data").get("total_orders").asInt()>0);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@Test(dependsOnMethods="testOrder",priority=21, description = " 1.place an order 2.Verify using get single order key")
  public void getLastOrder()
  {
  	Processor p=helper.getLastOrder(hashMap.get("Tid"), hashMap.get("Token"));
  String getLastOrder=p.ResponseValidator.GetBodyAsText();
 try {
   JsonNode node=APIUtils.convertStringtoJSON(getLastOrder);
   Assert.assertEquals(node.get("statusCode").asInt(),0);
   
   String order_id1= JsonPath.read(getLastOrder,"$.data.last_order.order_id").toString().replace("[","").replace("]","");
   Assert.assertEquals(order_id1,order_id);
 } catch (Exception e) {
  e.printStackTrace();
   
}
  }
   
  
  @Test(dependsOnMethods="testOrder",priority=22,  description = " 1.place an order 2.Verify using get single order key")
    public void getSingleOrder1()
    {
    	Processor p=helper.getSingleOrder(order_id,hashMap.get("Tid"), hashMap.get("Token"));
    String singleorder=p.ResponseValidator.GetBodyAsText();
   try {
     JsonNode node=APIUtils.convertStringtoJSON(singleorder);
     Assert.assertEquals(node.get("statusCode").asInt(),0);
 } catch (Exception e) {
    e.printStackTrace();
     
 }
    }
  @Test(dependsOnMethods="testOrder",priority=24,  description = " 1.place an order 2.Verify using get single order key")
	public void getSingleOrderKey() {
		Processor p = helper.getSingleOrderKey(order_key,hashMap.get("Tid"), hashMap.get("Token"));
		String singleorder = p.ResponseValidator.GetBodyAsText();
		try {
			JsonNode node = APIUtils.convertStringtoJSON(singleorder);
			Assert.assertEquals(node.get("statusCode").asInt(), 0);
		} catch (Exception e) {
			e.printStackTrace();

		}
	}
	
	
	
}
