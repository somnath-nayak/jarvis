
package com.swiggy.api.sf.rng.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "cart"
})
public class MinQuantityObject {

    @JsonProperty("cart")
    private Integer cart;

    @JsonProperty("cart")
    public Integer getCart() {
        return cart;
    }

    @JsonProperty("cart")
    public void setCart(Integer cart) {
        this.cart = cart;
    }

}
