package com.swiggy.api.sf.checkout.helper.edvo.pojo.getAllOrders;

import java.util.List;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "total_orders",
        "customer_care_number",
        "orders"
})
public class Data {

    @JsonProperty("total_orders")
    private int totalOrders;
    @JsonProperty("customer_care_number")
    private String customerCareNumber;
    @JsonProperty("orders")
    private List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> orders = null;

    @JsonProperty("total_orders")
    public int getTotalOrders() {
        return totalOrders;
    }

    @JsonProperty("total_orders")
    public void setTotalOrders(int totalOrders) {
        this.totalOrders = totalOrders;
    }

    @JsonProperty("customer_care_number")
    public String getCustomerCareNumber() {
        return customerCareNumber;
    }

    @JsonProperty("customer_care_number")
    public void setCustomerCareNumber(String customerCareNumber) {
        this.customerCareNumber = customerCareNumber;
    }

    @JsonProperty("orders")
    public List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> getOrders() {
        return orders;
    }

    @JsonProperty("orders")
    public void setOrders(List<com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse.Data> orders) {
        this.orders = orders;
    }

}