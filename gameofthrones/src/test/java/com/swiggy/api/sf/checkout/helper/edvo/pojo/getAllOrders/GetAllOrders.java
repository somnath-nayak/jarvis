package com.swiggy.api.sf.checkout.helper.edvo.pojo.getAllOrders;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "statusCode",
        "statusMessage",
        "data",
        "tid",
        "sid",
        "deviceId",
        "successful"
})
public class GetAllOrders {

    @JsonProperty("statusCode")
    private int statusCode;
    @JsonProperty("statusMessage")
    private String statusMessage;
    @JsonProperty("data")
    private Data data;
    @JsonProperty("tid")
    private String tid;
    @JsonProperty("sid")
    private String sid;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("successful")
    private boolean successful;

    @JsonProperty("statusCode")
    public int getStatusCode() {
        return statusCode;
    }

    @JsonProperty("statusCode")
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    @JsonProperty("statusMessage")
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("data")
    public Data getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(Data data) {
        this.data = data;
    }

    @JsonProperty("tid")
    public String getTid() {
        return tid;
    }

    @JsonProperty("tid")
    public void setTid(String tid) {
        this.tid = tid;
    }

    @JsonProperty("sid")
    public String getSid() {
        return sid;
    }

    @JsonProperty("sid")
    public void setSid(String sid) {
        this.sid = sid;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonProperty("successful")
    public boolean getSuccessful() {
        return successful;
    }

    @JsonProperty("successful")
    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }
}