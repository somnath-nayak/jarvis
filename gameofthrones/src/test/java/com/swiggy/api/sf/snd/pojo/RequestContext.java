package com.swiggy.api.sf.snd.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

public class RequestContext {

    private LatLong latLong;

    /**
     * No args constructor for use in serialization
     *
     */
    public RequestContext() {
    }

    /**
     *
     * @param latLong
     */
    public RequestContext(LatLong latLong) {
        super();
        this.latLong = latLong;
    }

    public LatLong getLatLong() {
        return latLong;
    }

    public void setLatLong(LatLong latLong) {
        this.latLong = latLong;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("latLong", latLong).toString();
    }

}