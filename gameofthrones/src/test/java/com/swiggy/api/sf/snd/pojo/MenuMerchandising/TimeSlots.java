package com.swiggy.api.sf.snd.pojo.MenuMerchandising;

public class TimeSlots
{
    private String startTime;

    private String closeTime;

    private String day;

    public String getStartTime ()
    {
        return startTime;
    }

    public void setStartTime (String startTime)
    {
        this.startTime = startTime;
    }

    public String getCloseTime ()
    {
        return closeTime;
    }

    public void setCloseTime (String closeTime)
    {
        this.closeTime = closeTime;
    }

    public String getDay ()
    {
        return day;
    }

    public void setDay (String day)
    {
        this.day = day;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [startTime = "+startTime+", closeTime = "+closeTime+", day = "+day+"]";
    }
}