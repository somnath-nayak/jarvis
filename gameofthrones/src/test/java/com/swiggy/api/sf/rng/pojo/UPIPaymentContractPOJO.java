package com.swiggy.api.sf.rng.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "paymentCodeMappings",
        "createdBy"
})
public class UPIPaymentContractPOJO {
    @JsonProperty("paymentCodeMappings")
    private List<PaymentCodeMappingPOJO> paymentCodeMappings = null;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public UPIPaymentContractPOJO() {
    }

    /**
     *
     * @param createdBy
     * @param paymentCodeMappings
     */
    public UPIPaymentContractPOJO(List<PaymentCodeMappingPOJO> paymentCodeMappings, String createdBy) {
        super();
        this.paymentCodeMappings = paymentCodeMappings;
        this.createdBy = createdBy;
    }

    @JsonProperty("paymentCodeMappings")
    public List<PaymentCodeMappingPOJO> getPaymentCodeMappings() {
        return paymentCodeMappings;
    }

    @JsonProperty("paymentCodeMappings")
    public void setPaymentCodeMappings(List<PaymentCodeMappingPOJO> paymentCodeMappings) {
        this.paymentCodeMappings = paymentCodeMappings;
    }

    public UPIPaymentContractPOJO withPaymentCodeMappings(List<PaymentCodeMappingPOJO> paymentCodeMappings) {
        this.paymentCodeMappings = paymentCodeMappings;
        return this;
    }

    @JsonProperty("createdBy")
    public String getCreatedBy() {
        return createdBy;
    }

    @JsonProperty("createdBy")
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UPIPaymentContractPOJO withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public UPIPaymentContractPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public UPIPaymentContractPOJO setDefault(String couponCode,String paymentCode, String type){

            PaymentCodeMappingPOJO paymentObject = new PaymentCodeMappingPOJO();
            paymentObject.setDefault(couponCode,paymentCode, type);
            List<PaymentCodeMappingPOJO> listPayment = new ArrayList<>();
            listPayment.add(paymentObject);
            this.withCreatedBy("user@swiggy.in").setPaymentCodeMappings(listPayment);
            return  this;
    }
}
