package com.swiggy.api.sf.checkout.helper.edvo.pojo.mealTDRequest;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "rewardFunction",
        "rewardType",
        "rewardValue",
        "count"
})
public class Reward {

    @JsonProperty("rewardFunction")
    private String rewardFunction;
    @JsonProperty("rewardType")
    private String rewardType;
    @JsonProperty("rewardValue")
    private Integer rewardValue;
    @JsonProperty("count")
    private Integer count;

    /**
     * No args constructor for use in serialization
     *
     */
    public Reward() {
    }

    /**
     *
     * @param count
     * @param rewardValue
     * @param rewardFunction
     * @param rewardType
     */
    public Reward(String rewardFunction, String rewardType, Integer rewardValue, Integer count) {
        super();
        this.rewardFunction = rewardFunction;
        this.rewardType = rewardType;
        this.rewardValue = rewardValue;
        this.count = count;
    }

    @JsonProperty("rewardFunction")
    public String getRewardFunction() {
        return rewardFunction;
    }

    @JsonProperty("rewardFunction")
    public void setRewardFunction(String rewardFunction) {
        this.rewardFunction = rewardFunction;
    }

    @JsonProperty("rewardType")
    public String getRewardType() {
        return rewardType;
    }

    @JsonProperty("rewardType")
    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    @JsonProperty("rewardValue")
    public Integer getRewardValue() {
        return rewardValue;
    }

    @JsonProperty("rewardValue")
    public void setRewardValue(Integer rewardValue) {
        this.rewardValue = rewardValue;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("rewardFunction", rewardFunction).append("rewardType", rewardType).append("rewardValue", rewardValue).append("count", count).toString();
    }

}