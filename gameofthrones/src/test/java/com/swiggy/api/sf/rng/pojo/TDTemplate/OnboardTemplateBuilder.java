package com.swiggy.api.sf.rng.pojo.TDTemplate;

import java.util.List;

public class OnboardTemplateBuilder {

    public OnboardTemplate onboardTemplate;

    public OnboardTemplateBuilder() {
        onboardTemplate = new OnboardTemplate();
    }

    public OnboardTemplateBuilder withRestaurantIds(List<Integer> restaurantIds) {
        onboardTemplate.setRestaurantIds(restaurantIds);
        return this;
    }

    public OnboardTemplateBuilder withTemplateId(int templateId) {
        onboardTemplate.setTemplateId(templateId);
        return this;
    }
    public OnboardTemplateBuilder withStartDate(long startDate) {
        onboardTemplate.setStartDate(startDate);
        return this;
    }
    public OnboardTemplateBuilder withDuration(int duration)  {
        onboardTemplate.setDuration(duration);
        return this;
    }
    public OnboardTemplateBuilder withIngestionSource(String ingestionSource)  {
        onboardTemplate.setIngestionSource(ingestionSource);
        return this;
    }




    public OnboardTemplate build() {
        return onboardTemplate;
    }

}
