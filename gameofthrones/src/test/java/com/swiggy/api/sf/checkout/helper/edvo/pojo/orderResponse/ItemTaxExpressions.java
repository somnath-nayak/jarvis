package com.swiggy.api.sf.checkout.helper.edvo.pojo.orderResponse;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "Vat",
        "Service Charges",
        "Service Tax",
        "GST_inclusive",
        "SGST",
        "CGST",
        "IGST"
})
public class ItemTaxExpressions {

    @JsonProperty("Vat")
    private String vat;
    @JsonProperty("Service Charges")
    private String serviceCharges;
    @JsonProperty("Service Tax")
    private String serviceTax;
    @JsonProperty("GST_inclusive")
    private Boolean gSTInclusive;

    @JsonProperty("SGST")
    private String sgst;
    @JsonProperty("CGST")
    private String cgst;
    @JsonProperty("IGST")
    private String igst;


    @JsonProperty("SGST")
    public String getSgst() {
        return sgst;
    }

    @JsonProperty("SGST")
    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    @JsonProperty("CGST")
    public String getCgst() {
        return cgst;
    }

    @JsonProperty("CGST")
    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    @JsonProperty("IGST")
    public String getIgst() {
        return igst;
    }

    @JsonProperty("IGST")
    public void setIgst(String igst) {
        this.igst = igst;
    }

    @JsonProperty("Vat")
    public String getVat() {
        return vat;
    }

    @JsonProperty("Vat")
    public void setVat(String vat) {
        this.vat = vat;
    }

    @JsonProperty("Service Charges")
    public String getServiceCharges() {
        return serviceCharges;
    }

    @JsonProperty("Service Charges")
    public void setServiceCharges(String serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    @JsonProperty("Service Tax")
    public String getServiceTax() {
        return serviceTax;
    }

    @JsonProperty("Service Tax")
    public void setServiceTax(String serviceTax) {
        this.serviceTax = serviceTax;
    }

    @JsonProperty("GST_inclusive")
    public Boolean getGSTInclusive() {
        return gSTInclusive;
    }

    @JsonProperty("GST_inclusive")
    public void setGSTInclusive(Boolean gSTInclusive) {
        this.gSTInclusive = gSTInclusive;
    }

}