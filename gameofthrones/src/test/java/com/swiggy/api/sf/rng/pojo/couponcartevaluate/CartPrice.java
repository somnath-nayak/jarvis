package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class CartPrice {
	@JsonProperty("quantity")
	private Integer quantity;
	@JsonProperty("cartTotal")
	private Double cartTotal;
	@JsonProperty("itemLevelPrice")
	private ItemLevelPrice itemLevelPrice;
	@JsonProperty("preferredPaymentMethod")
	private String preferredPaymentMethod;
	@JsonProperty("referralPresence")
	private Boolean referralPresence;
	@JsonProperty("swiggyMoneyApplicable")
	private Boolean swiggyMoneyApplicable;

	@JsonProperty("quantity")
	public Integer getQuantity() {
	return quantity;
	}

	@JsonProperty("quantity")
	public void setQuantity(Integer quantity) {
	this.quantity = quantity;
	}

	public CartPrice withQuantity(Integer quantity) {
	this.quantity = quantity;
	return this;
	}

	@JsonProperty("cartTotal")
	public Double getCartTotal() {
	return cartTotal;
	}

	@JsonProperty("cartTotal")
	public void setCartTotal(Double cartTotal) {
	this.cartTotal = cartTotal;
	}

	public CartPrice withCartTotal(Double cartTotal) {
	this.cartTotal = cartTotal;
	return this;
	}

	@JsonProperty("itemLevelPrice")
	public ItemLevelPrice getItemLevelPrice() {
	return itemLevelPrice;
	}

	@JsonProperty("itemLevelPrice")
	public void setItemLevelPrice(ItemLevelPrice itemLevelPrice) {
	this.itemLevelPrice = itemLevelPrice;
	}

	public CartPrice withItemLevelPrice(ItemLevelPrice itemLevelPrice) {
	this.itemLevelPrice = itemLevelPrice;
	return this;
	}

	@JsonProperty("preferredPaymentMethod")
	public String getPreferredPaymentMethod() {
	return preferredPaymentMethod;
	}

	@JsonProperty("preferredPaymentMethod")
	public void setPreferredPaymentMethod(String preferredPaymentMethod) {
	this.preferredPaymentMethod = preferredPaymentMethod;
	}

	public CartPrice withPreferredPaymentMethod(String preferredPaymentMethod) {
	this.preferredPaymentMethod = preferredPaymentMethod;
	return this;
	}
	public CartPrice withDefaultPreferredPaymentMethod() {
		this.preferredPaymentMethod = preferredPaymentMethods();
		return this;
		}

	@JsonProperty("referralPresence")
	public Boolean getReferralPresence() {
	return referralPresence;
	}

	@JsonProperty("referralPresence")
	public void setReferralPresence(Boolean referralPresence) {
	this.referralPresence = referralPresence;
	}

	public CartPrice withReferralPresence(Boolean referralPresence) {
	this.referralPresence = referralPresence;
	return this;
	}

	@JsonProperty("swiggyMoneyApplicable")
	public Boolean getSwiggyMoneyApplicable() {
	return swiggyMoneyApplicable;
	}

	@JsonProperty("swiggyMoneyApplicable")
	public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
	this.swiggyMoneyApplicable = swiggyMoneyApplicable;
	}

	public CartPrice withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
	this.swiggyMoneyApplicable = swiggyMoneyApplicable;
	return this;
	}
	public String preferredPaymentMethods() {
		return "Juspay,PayTM,Cash,Juspay-NB,PayTM-SSO,Mobikwik,Mobikwik-SSO,Freecharge,Freecharge-SSO,third-party-cash,third-party-online,Sodexo,PayLater_Lazypay,PhonePe,AmazonPay,AmazonPayWallet,AmazonPayWeb";
	}
}
