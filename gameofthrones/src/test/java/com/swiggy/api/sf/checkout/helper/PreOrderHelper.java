package com.swiggy.api.sf.checkout.helper;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.sf.checkout.constants.CheckoutConstants;
import com.swiggy.api.sf.checkout.constants.PricingConstants;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PreOrderHelper {

	Initialize gameofthrones = new Initialize();
	RedisHelper redisHelper = new RedisHelper();

	public Processor preOrderAreaScheduleCreate(String areaId, String day,String openTime, String closeTime) {
		Processor processor = null;
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		sqlTemplate.execute("delete  from `area_schedule` where `area_id`=" + areaId + " and type='PRE_ORDER' and day='" + day + "'");

		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		headers.put("user_meta", "{\"source\":\"VENDOR\"}");
		String[] payload = new String[]{areaId, "PRE_ORDER", day, "PREORDER", openTime, closeTime};
		GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "preorderareaschedulecreate", gameofthrones);
		processor = new Processor(service, headers, payload, null);
		return processor;
	}

	public Processor preOrderEnable(String restId, String attribute, String value) {
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		String[] payload = new String[]{restId, attribute, value};
		GameOfThronesService service = new GameOfThronesService("cmsrestaurantservice", "preorderenable", gameofthrones);
		processor = new Processor(service, headers, payload, null);
		return processor;
	}

	public Processor setItemSechedule(String openTime, String closeTime, String dayOfWeek,String itemId) {
		Processor processor = null;
		HashMap<String, String> headers = new HashMap<>();
		headers.put("Content-Type", "application/json");
		String[] payload = new String[]{openTime,closeTime,dayOfWeek,itemId};
		GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "itemSechedule", gameofthrones);
		processor = new Processor(service, headers, payload, null);
		return processor;
	}

	public String getPreOrderDBConf() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String query="select option_value from wp_options where option_name='preorder_enabled' ";

		List<Map<String, Object>> payments = sqlTemplate.queryForList(query);
		return (String) payments.get(0).get("option_value");
	}

	public String getPreOrderCODEnabled() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String query="select option_value from wp_options where option_name='preorder_cod_enabled' ";

		List<Map<String, Object>> isCodEnabled = sqlTemplate.queryForList(query);
		return (String) isCodEnabled.get(0).get("option_value");
	}

	public void setResCache(String restId){
		String value="{\"delivery\":true ,\"is_buffet\":false,\"self_pick_up\":false,\"preorder_enabled\":true}";
		redisHelper.setValue("sandredisstage",0,"METADATA_RESTAURANT_"+restId,value);
	}

	public String getResCache(String restId){
		return (String) redisHelper.getValue("sandredisstage",0,"METADATA_RESTAURANT_"+restId);
	}


	public List<Map<String, Object>> getPreOrderAreaSchedule(String areaId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String day=CheckoutHelper.getDay();
		String query="select * from area_schedule where area_id='"+areaId+"'"+ "and day='"+day+"'"+"and slot_type='PREORDER'";
		List<Map<String, Object>> areaSchedule = sqlTemplate.queryForList(query);
		return areaSchedule;
	}

	public List<Map<String, Object>> getPreOrderRestSchedule(String restId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		String day=CheckoutHelper.getDay();
		String query="select * from restaurants_schedule where rest_id='"+restId+"'"+ "and day='"+day+"'"+"and type='PREORDER'";
		List<Map<String, Object>> restSchedule = sqlTemplate.queryForList(query);
		return restSchedule;
	}


	public void preorderSlots(String rest_id, String day, String openTime, String closeTime) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String current_time = dateFormat.format(date);
		sqlTemplate.execute("delete from `restaurants_schedule` where `rest_id`=" + rest_id + "");
		sqlTemplate.execute("INSERT INTO `restaurants_schedule` (`rest_id`,`day`,`open_time`,`close_time`,type,`created_on`,`updated_on`) VALUES ('" + rest_id + "','" + day + "','" + openTime + "','" + closeTime + "','PREORDER'" + ",'" + current_time + "','" + current_time + "')");

	}

	public int getAreaIDFromDB(String restId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate("cms");
		Object[] params = new Object[] {restId};
		String areaCodeQuery="select area_code from restaurants where id=?";
		return sqlTemplate.queryForObjectInteger(areaCodeQuery, params, String.class);
	}

	public static String getDay(){
		Format formatter = new SimpleDateFormat("EEE");
		String day = formatter.format(new Date());
		return day;
	}

}

