package com.swiggy.api.sf.rng.tests;

import java.util.ArrayList;
import java.util.List;

import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieRestaurantList;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;

public class getObject {

	public RestaurantList getRestaurantObject()
	{
		RandomNumber rm = new RandomNumber(2000000,3000000);
		return new RestaurantList(new Integer(rm.nextInt()).toString(), "Test", new ArrayList<>());
        
	}
	public FreebieRestaurantList getFreebieRestaurantObject()
	{
		RandomNumber rm = new RandomNumber(3000000,3100000);
		return new FreebieRestaurantList(rm.nextInt());
        
	}
	public FreeDeliveryRestaurantList getFreedeliveryRestaurantObject()
	{
		RandomNumber rm = new RandomNumber(3000000,3100000);
		return new FreeDeliveryRestaurantList(new Integer(rm.nextInt()).toString());
        
	}
	public RestaurantList getCategoryObject()
	{
		List<Category> category = new ArrayList<>();
		RandomNumber rm = new RandomNumber(3000000,3100000);
		category.add(new Category(new Integer(rm.nextInt()).toString(),"Test",new ArrayList<>()));
		
        return new RestaurantList(new Integer(rm.nextInt()).toString(), "Test", category);
        
	}
	public RestaurantList getsubCategoryObject() {
		RandomNumber rm =new RandomNumber(3000000,3100000);
		
		List<SubCategory> Subcategory = new ArrayList<>();
		Subcategory.add(new SubCategory(new Integer(rm.nextInt()).toString(),"Test",new ArrayList<>()));
		
		List<Category> category = new ArrayList<>();
		category.add(new Category(new Integer(rm.nextInt()).toString(),"Test",Subcategory));
		
		
        return new RestaurantList(new Integer(rm.nextInt()).toString(), "Test", category);
        
	} 
	
	public RestaurantList getItemObject() {
		
		RandomNumber rm = new RandomNumber(3000000,3100000);
		
		List<Menu> item = new ArrayList<>();
		item.add(new Menu(new Integer(rm.nextInt()).toString(),"Test"));
		
		
		List<SubCategory> Subcategory = new ArrayList<>();
		Subcategory.add(new SubCategory(new Integer(rm.nextInt()).toString(),"Test",item));
		
		List<Category> category = new ArrayList<>();
		category.add(new Category(new Integer(rm.nextInt()).toString(),"Test",Subcategory));
		
		
        return new RestaurantList(new Integer(rm.nextInt()).toString(), "Test", category);
        
	} 
	
	public String FrebieItemId()
	{
		RandomNumber rm = new RandomNumber(3000000,3100000);
		return new Integer(rm.nextInt()).toString();
		
	}
}
