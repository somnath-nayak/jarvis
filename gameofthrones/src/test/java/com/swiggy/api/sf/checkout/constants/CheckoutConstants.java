package com.swiggy.api.sf.checkout.constants;

public interface CheckoutConstants {

String mobile1="7406734416";
String password1="rkonowhere";

String mobile="7406734416";//"9886379321";
String password="rkonowhere";//"myTest12";

String deleteCartMessage="CART_DELETED_SUCCESSFULLY";
String STATUS_MESSAGE="done successfully";
int success=200;
String cancellationmessage= "";
String restClosed="Sorry, this restaurant is now closed.";
int status=1;
String statusMessage="Oops, something went wrong. Please clear your cart and try again.";
String authorization="Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
String trackUnassigned="unassigned";
String trackOnTimeStatus="true";
String lat="12.9166";
String lng= "77.6101";
Long loginMobile=7406734416l;
String paymentMethod="Cash";
String orderComments= "Test_Order";
String cmsDB= "cms";
String checkout= "checkout";
String addonQuery="select ram.rest_id, ag.`item_id`, ag.id groupid, ad.id choiceid, ad.name, ad.price from `restaurant_addon_map` ram inner join addons_new ad on ad.id=ram.`addon_id` inner join addon_groups ag on ag.`id`= ad.id where ram.rest_id=";
String cancellationFee="update swiggy.wp_users SET cancellation_fee = '0' WHERE mobile=";
String getAllPayments="select * from payment_method";
String getPaymentMeta="select * from payment_method where name=\"";
String testLat="27.8356292";
String testLng="76.17308939999998";

int restaurantId=1087;
int address_id=11132349;
int invalidAddress=1;
int unServicableAddress=13191408;
String paymentCod="CASH";
String paymentPaytm="PayTM-SSO";
String paymentFreecharge ="Freecharge-SSO";
String paymentCodIgnoreCases="CASH";
String paymentInvalidCase="CASH-00";
String paymentNullCase="NULL";
String address_invalid = null;
int foo = Integer.parseInt(address_invalid);
String ITEM_QUANTITY="2";
String q="select id as item_menu_id,addons from items where addons is not null and addons !='{}' and restaurant_id=";
String IS_SUCCESSFUL_FLAG="true";
String INITIATION_SOURCE="2";
String updateDelFee = "UPDATE swiggy.restaurants set delivery_charges='";
String updatePackingFee = "UPDATE swiggy.restaurants set packing_charges='";
String resetRestFee = "UPDATE swiggy.restaurants set delivery_charges='0',packing_charges='0',updated_on='";
//where id=";
String updateItemGST = "UPDATE swiggy.restaurant_gst_details set "
		+ "item_cgst='[CART_SUBTOTAL_WITHOUT_PACKING]*0.025',"
		+ "item_igst='[CART_SUBTOTAL_WITHOUT_PACKING]*0.025',"
		+ "item_sgst='[CART_SUBTOTAL_WITHOUT_PACKING]*0.025' "
		+ ",updated_at='";
String updatePackagingGST = "UPDATE swiggy.restaurant_gst_details set "
		+ "packaging_cgst='[TOTAL_PACKING_CHARGES]*0.025',"
		+ "packaging_sgst='[TOTAL_PACKING_CHARGES]*0.025',"
		+ "packaging_igst='[TOTAL_PACKING_CHARGES]*0.025' "
		+ ", updated_at='";

String resetGST="UPDATE swiggy.restaurant_gst_details set "
		+ "packaging_cgst='0',"
		+ "packaging_sgst='0',"
		+ "packaging_igst='0', "
		+ "item_cgst='0',"
		+ "item_igst='0',"
		+ "item_sgst='0', updated_at='";

String updateSwiggyMoney="update user_credits set swiggy_money='";
String updateCancellationFee="update user_credits set cancellation_fee='";
String resetUserCredit="update user_credits set swiggy_money='0',cancellation_fee='0' where user_id=";
String renderingVersionCode="select option_value from checkout_options where option_name=?";
String restAreaId="select area_code from restaurants where id=?";
String whereId = "' where id=";
String whereUserId = "' where user_id=";
String updatedOn="',updated_on='";
String end = "'";
String whereRestId = "' where restaurant_id=";

String fee="10.0";
String defaultZero="0.0";
String cartTotalRange="1000.0";
String defaultZeroHour="0000";
String default23Hour="2300";
String defaultDistanceTo="15";

}
