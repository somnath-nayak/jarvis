package com.swiggy.api.sf.rng.dp.EDVODp;


import com.swiggy.api.sf.rng.constants.CopyConstants;
import com.swiggy.api.sf.rng.constants.EDVOConstants;
import com.swiggy.api.sf.rng.helper.*;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateListingV3.EvaluateListingV3;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluteMenu.EvaluateMenu;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOCartV3Builder;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOMealPOJO;
import com.swiggy.api.sf.rng.pojo.edvo.ItemRequestCartV3Builder;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EDVOMultiTdDP {

    public static EDVOHelper edvoHelper = new EDVOHelper();
    public static CopyResolverHelper copyResolverHelper = new CopyResolverHelper();
    public static SuperHelper superHelper = new SuperHelper();
    public static SANDHelper sandHelper = new SANDHelper();
    public static CopyResolverDBHelper copyResolverDBHelper = new CopyResolverDBHelper();
    public static EDVOMultiTdHelper edvoMultiTdHelper = new EDVOMultiTdHelper();
    public static RngHelper rngHelper = new RngHelper();
    public EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();

    // EDVO TD & Restaurant level Freedel Td With MOV
    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId
        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", Long.toString(EDVOConstants.edvoMinCartAmount));

        String edvoCampaignId = Keys.get("campaign_id").toString();
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, EDVOConstants.edvoMinCartAmount, false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), EDVOConstants.edvoMinCartAmount, false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), EDVOConstants.edvoMinCartAmount, false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), EDVOConstants.edvoMinCartAmount, false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // EDVO TD & Restaurant level Freedel Td With No MOV

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperUserTest() throws IOException {
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");

        String edvoCampaignId = Keys.get("campaign_id").toString();
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    //Super Freedel TD, EDVO TD & Restaurant level Freedel Td With No MOV

    @DataProvider(name = "createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuperDP")
    public Object[][] createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithNoMOVAsSuper() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete suepr from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
//        sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");

        String edvoCampaignId = Keys.get("campaign_id").toString();
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Non-Super Freedel TD, EDVO TD & Restaurant level Freedel Td With MOV

    @DataProvider(name = "createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsNonSuperDP")
    public Object[][] createEvaluateSuperFreedelTDEDVOTdAndRestaurantLevelFreedelMultiTDWithMOVAsNonSuper() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        superHelper.cancelSubscriptionOfUser(userId);
//        sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        //Create Super Freedel Campaign
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "10", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");

        String edvoCampaignId = Keys.get("campaign_id").toString();
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

// Super Freebie TD, EDVO TD & Restaurant level Freebie Td With MOV

    @DataProvider(name = "createSuperFreebieTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVDP")
    public Object[][] createSuperFreebieTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOV() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //  sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        //Create Super Freebie Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create Freebie TD different item from Super freebie item
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as Super freebie
        String tdSameItemFreebieCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "0", listOfTDRestCategorySubCatItemId.get(3).get(0));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        return new Object[][]{{tdCreation, edvoCampaignId, superFreebieCamp, tdSameItemFreebieCreation}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), Integer.toString(EDVOConstants.edvoMinCartAmount));

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as Super freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", Integer.toString(EDVOConstants.edvoMinCartAmount));
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superFreedelCamp, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With  No MOV
    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsNonSuperUser() throws IOException, InterruptedException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as  freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "0", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(userId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), Integer.toString(EDVOConstants.edvoMinCartAmount));

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as  freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", Integer.toString(EDVOConstants.edvoMinCartAmount));
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("99"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("99"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With No MOV

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithNoMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item  freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "0", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as Super user & evaluate with MOV Zero
    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserButEvalauteWithMOVZeroDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsSuperUserButEvalauteWithMOVZero() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), Integer.toString(EDVOConstants.edvoMinCartAmount));

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as  freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Freebie Td With MOV as Non-Super user & evaluate with MOV Zero

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserButEvalauteWithMOVZeroDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFreebieMultiTDWithMOVAsNonSuperUserButEvalauteWithMOVZero() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), Integer.toString(EDVOConstants.edvoMinCartAmount));

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

//        // Create Freebie TD different item from Super freebie item
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "0", Integer.toString(Utility.getRandom(10000000, 88000000)));

        //  Create Freebie TD same item as Super freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With  No-MOV

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        sandHelper.superKeyDelFromCache(superUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With MOV as Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With  No-MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td With MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserDP() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Percentage Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //  superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "100", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(userId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With  No-MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[0], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        //allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String userId = Integer.toString(Utility.getRandom(1, 99900));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(userId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //  superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(userId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(userId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(userId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, userId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, userId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, userId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With  No-MOV as Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[0], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Super user

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        //allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        //String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With  No-MOV as Super user

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreedelWithoutAnySuperTdWithNoMOVAsNonSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreedelWithoutAnySuperTdWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        //        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");
        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        //String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithoutSuperTdWithMOVAsNonSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithoutSuperTdWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");
        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);


        // create super user
//        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreedelTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        //String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    //  EDVO TD & Restaurant level Freebie Td With  No-MOV as Non-Super user

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreebieWithoutAnySuperTdWithNoMOVAsNonSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreebieWithoutAnySuperTdWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        //        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");
        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        //String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        //String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD

        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "0", listOfTDRestCategorySubCatItemId.get(3).get(0));

//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[2], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        //Setting public user type
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithoutSuperTdWithMOVAsNonSuperUserDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithoutSuperTdWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");
        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);


        // create super user
//        superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        // String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createEvaluateEDVOTdAndRestaurantLevelFreebieTDWithOutSuperTdWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        //Clear or disable all SUper camapigns
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);

        //Create Super Freedel Campaign
        //String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super freebie, EDVO TD & Restaurant level Flat Td With  No-MOV as Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "0", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[0], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        // setting public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super freebie, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        //allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super Freebie, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level %age Td With  No-MOV as Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "0", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel & Freebie TD, EDVO TD & Restaurant level Flat Td With  No-MOV as Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "0", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);

        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelPercentageMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
//        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
//        List<String> allCategoriesId = new ArrayList<>();
//        allCategoriesId.add(0,category);
//        List<List<String>> categoriesId = new ArrayList<>();
//        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);
        // superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation =copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super freebie, EDVO TD & Restaurant level Flat Td With  No-MOV as Non-Super user

    @DataProvider(name = "createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUserDP")
    public Object[][] createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithNoMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);
        // superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "0", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "0");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[0], CopyConstants.discountLevel[0], "0", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], "0", listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "0");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("0"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("0"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("0"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super freebie, EDVO TD & Restaurant level Flat Td With MOV as Non-Super user

    @DataProvider(name = "createEvalauteSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserDP")
    public Object[][] createEvalauteSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUser() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        //allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        //superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // Super Freedel TD & Super Freebie, EDVO TD & Restaurant level Flat Td with MOV 100 as Non-Super user but evaluate with MOV 1

    @DataProvider(name = "createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOneDP")
    public Object[][] createEvaluateSuperFreedelAndFreebieTDEDVOTdAndRestaurantLevelFlatMultiTDWithMOVAsNonSuperUserButEvaluateWithMOVOne() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // Cancel Subscription for user and delete super from SAND-Redis
        superHelper.cancelSubscriptionOfUser(superUserId);
        //sandHelper.superKeyDelFromCache(CopyConstants.nonSuperUserId, "IS_SUPER");

        // create super user
        // superHelper.createSuperUserWithPublicPlan(superUserId);
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        //Create Super Freedel Campaign
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //(Boolean isSuper, String type, String discountLevel, String minCartAmount,String discountCap, List<String> allRestIds,List<List<String>> categoriesId,List<String> subCategoriesId, List<String> menuId

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("1"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("1"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("1"), false, false, superUserId);

        return new Object[][]{{tdCreation, edvoCampaignId, superFreedelCamp, superFreebieCamp, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superUserId}};
    }

    // EDVO TD, Restaurant level %age & Flat Td with MOV

    @DataProvider(name = "createEDVORestaurantPercentageAndFlatCampaignsWithMOVDP")
    public Object[][] createEDVORestaurantPercentageAndFlatCampaignsWithMOV() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava and clear redis
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        // Create TD
        String percentTdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Flat campaign
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        return new Object[][]{{tdCreation, edvoCampaignId, percentTdCreation}};
    }

    //  Create EDVO TD, Restaurant level  Flat Td & %age with MOV
    @DataProvider(name = "createEDVORestaurantFlatAndPercentageCampaignsWithMOVDP")
    public Object[][] createEDVORestaurantFlatAndPercentageCampaignsWithMOV() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava and clear redis
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        //create EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Flat campaign
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        // Create TD
        String percentTdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        return new Object[][]{{tdCreation, edvoCampaignId, percentTdCreation}};
    }

    // EDVO public , 1st order, rest 1st order, user restriction & dormant (thirty days) with MOV
    @DataProvider(name = "createEDVOForAllUserSegmentDP")
    public Object[][] createEDVOForAllUserSegment() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava and clear redis
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();


        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId}};
    }

    // EDVO public , 1st order, rest 1st order, user restriction , dormant (thirty days) & Sixty days dormant should not allow to create  with MOV
    @DataProvider(name = "createEDVOWithUserSegmentAndTwoOfDormantTypeDP")
    public Object[][] createEDVOWithUserSegmentAndTwoOfDormantType() throws IOException {

        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava and clear redis
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        //create SIXTY dormant EDVO campaign
        HashMap sixtyDormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "NINETY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoSixtyDormantCampId = sixtyDormantCamp.get("campaign_id").toString();

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, edvoSixtyDormantCampId}};
    }

    // Create EDVO with all user segment and Restaurant level Flat type td

    @DataProvider(name = "createFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeDP")
    public Object[][] createFlatRestaurantLevelTdAndEDVOWithUserSegmentType() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();


        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //rngHelper.DormantUser(superUserId,"1","10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), true, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), true, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), true, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create & Evaluate EDVO user segment td and restaurant level public type Flat td and Evaluate As public non-super user

    @DataProvider(name = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP")
    public Object[][] createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create EDVO user segment td and restaurant level public type %age td and Evaluate

    @DataProvider(name = "createPercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeDP")
    public Object[][] createPercentageRestaurantLevelTdAndEDVOWithUserSegmentType() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), true, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), true, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), true, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create EDVO user segment td and restaurant level public type %age td and Evaluate As Public NonSuper user

    @DataProvider(name = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP")
    public Object[][] createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // deleting dormant set
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "100", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create EDVO user segment td and restaurant level public type Freedel td and Evaluate

    @DataProvider(name = "createFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeDP")
    public Object[][] createFreedelRestaurantLevelTdAndEDVOWithUserSegmentType() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), true, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), true, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), true, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create & Evaluate EDVO user segment td and restaurant level public type Freedel td and Evaluate As Public NonSuper User

    @DataProvider(name = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP")
    public Object[][] createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }


    // Create EDVO user segment td and restaurant level public type Freebie td and Evaluate

    @DataProvider(name = "createFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeDP")
    public Object[][] createFreebieRestaurantLevelTdAndEDVOWithUserSegmentType() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[2], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), true, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), true, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), true, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), true, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create EDVO user segment td and restaurant level public type Freebie td and Evaluate As Public Non SuperUser

    @DataProvider(name = "createAndEvaluateFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUserDP")
    public Object[][] createAndEvaluateFreebieRestaurantLevelTdAndEDVOWithUserSegmentTypeAsPublicNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        //delete dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[2], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload}};
    }

    // Create EDVO user segment td and restaurant level public type Freedel td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and restaurant level public type Freedel td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluateFreedelRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[3], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserTest() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }


    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUser() throws IOException {
        // EvaluateListingV3 listingPayload = new EvaluateListingV3();
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);
        String superUserIdd = Integer.toString(Utility.getRandom(1000000, 9000000));
        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);
        // sandHelper.superKeyDelFromCache(superUserId, "IS_SUPER");
        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserIdd);
        superHelper.cancelSubscriptionOfUser(superUserIdd);
        //Create Super user
        //superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserIdd);
        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserIdd), false, listOfEDVORestIdPriceCount.get(0));
        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserIdd));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserIdd));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserIdd);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserIdd);
// setting user as Public
        rngHelper.DormantUser(superUserIdd, edvoRestId, "10");
        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }


    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as public non-Super user

    @DataProvider(name = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUserDP")
    public Object[][] createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsNonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as public Super user

    @DataProvider(name = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUserDP")
    public Object[][] createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, edvoRestId, "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as RFO
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluateFlatCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluateFlatSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluateFaltSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluateFlatItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }
    // Create EDVO user segment td and Restaurant level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Restaurant level public type %age td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluateFlatRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
//        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
//                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }
    // Create EDVO user segment td and Category level public type Percentage td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as RFO
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluatePercentageCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);
        copyResolverDBHelper.disableFreeDelSuperCampaign("-1");
        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[1], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[1], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));
        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, false, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, false, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, false, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Sub-Category level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluatePercentageSubCategoryLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[2], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[2], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Item level public type %age td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluatePercentageItemLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[3], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[3], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }
    // Create EDVO user segment td and Restaurant level public type Flat td with Super Freedel & Super Freebie and Evaluate as RFO non-Super user

    @DataProvider(name = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUserDP")
    public Object[][] createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFONonSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");

        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }

    // Create EDVO user segment td and Restaurant level public type %age td with Super Freedel & Super Freebie and Evaluate as RFO Super user

    @DataProvider(name = "createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUserDP")
    public Object[][] createAndEvaluatePercentageRestaurantLevelTdAndEDVOWithUserSegmentTypeWithSuperFreedelAndSuperFreebieAsRFOSuperUser() throws IOException {
        String superUserId = Integer.toString(Utility.getRandom(1000000, 9000000));
        String edvoRestId = edvoHelper.getRestId();
        List<List<String>> allCategoryId = new ArrayList<>();

        List<List<String>> listsOfEDVOMealGroupItem = edvoHelper.createMealItemGroupLists(edvoHelper.getMealId(), Integer.toString(Utility.getRandom(1000, 40000)), Integer.toString(Utility.getRandom(10, 2300)));
        List<List<Integer>> listOfEDVORestIdPriceCount = edvoHelper.createRestaurantPriceCountLists(Integer.parseInt(edvoRestId), 500, 1);

        List<List<String>> listOfTDRestCategorySubCatItemId = edvoMultiTdHelper.createRestCategorySubsCategoryItemLists(edvoRestId, Integer.toString(Utility.getRandom(200000, 70000000)), Integer.toString(Utility.getRandom(30000000, 99900000)), Integer.toString(Utility.getRandom(10000000, 88000000)));
        allCategoryId.add(0, listOfTDRestCategorySubCatItemId.get(1));
        String category = listOfTDRestCategorySubCatItemId.get(1).get(0);
        List<String> allCategoriesId = new ArrayList<>();
        allCategoriesId.add(0, category);
        List<List<String>> categoriesId = new ArrayList<>();
        categoriesId.add(allCategoriesId);

        //disable camaping & evict from guava
        copyResolverDBHelper.disableFreebieSuperCampaign(edvoRestId);

        // dormant set
        // rngHelper.DormantUser(userId, restId, dormantDays);
        rngHelper.deleteDormantUserRestMapping(superUserId);

        //Create Super user
        superHelper.createSuperUserWithFreedelAndFreebiePlan(superUserId);

        //Create Super Freedel & SUper Freebie
        String superFreebieCamp = copyResolverHelper.createSuperFreebieTDForResolver("Freebie", false, listOfTDRestCategorySubCatItemId.get(0), "99", listOfTDRestCategorySubCatItemId.get(3).get(0));
        String superFreedelCamp = copyResolverHelper.createSuperFreeDelTDForResolver(CopyConstants.benefits[0], false, listOfTDRestCategorySubCatItemId.get(0), "99");

        //create Public EDVO campaign
        HashMap Keys = edvoMultiTdHelper.createMealLevelPublicEDVOCampaign(edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoCampaignId = Keys.get("campaign_id").toString();

        //create Restaurant first EDVO campaign
        HashMap restFirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(true, false, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoWithRestFirstCampId = restFirstOrderCamp.get("campaign_id").toString();

        //create firstOrder EDVO campaign
        HashMap FirstOrderCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, true, false, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoFirstCampId = FirstOrderCamp.get("campaign_id").toString();

        //create THIRTY_DAYS dormant EDVO campaign
        HashMap dormantCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, false, "THIRTY_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoThirtyDormantCampId = dormantCamp.get("campaign_id").toString();

        //create  User Restriction EDVO campaign
        HashMap userRestrictedCamp = edvoMultiTdHelper.createMealLevelEDVOCampaignWithUserRestriction(false, false, true, "ZERO_DAYS_DORMANT", edvoRestId,
                listsOfEDVOMealGroupItem.get(0).get(0), listsOfEDVOMealGroupItem.get(1).get(0), "Flat", "99", "1", "100");
        String edvoUserRestrictedCampId = userRestrictedCamp.get("campaign_id").toString();

        // Create TD
        String tdCreation = edvoMultiTdHelper.createTD(false, CopyConstants.type[1], CopyConstants.discountLevel[0], "99", "0", listOfTDRestCategorySubCatItemId.get(0),
                allCategoryId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
//        String tdCreation = copyResolverHelper.createFlatTDWithCopyResolverFalse(false, CopyConstants.discountLevel[0], Integer.toString(EDVOConstants.edvoMinCartAmount), listOfTDRestCategorySubCatItemId.get(0),
//                categoriesId, listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3));
        //  Create Freebie TD same item as Super freebie
//        String tdCreation = copyResolverHelper.createFreebieWithCopy(listOfTDRestCategorySubCatItemId.get(0), false, false,
//                "100", listOfTDRestCategorySubCatItemId.get(3).get(0));

        // setting user as Public
        rngHelper.DormantUser(superUserId, "1", "10");
        //Evaluate listing payload
        EvaluateListingV3 listingPayload = evaluateListingV3.build(Integer.parseInt(superUserId), false, listOfEDVORestIdPriceCount.get(0));

        //Evaluate meal payload
        EDVOMealPOJO mealPayload = edvoHelper.evaluateMealPayload(listsOfEDVOMealGroupItem.get(0), edvoRestId, Long.parseLong("100"), false, true, Integer.parseInt(superUserId));

        // Evaluate Menu payload
        EvaluateMenu evaluateMenu = edvoHelper.evaluateMenuPayload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, Integer.parseInt(superUserId));

        //Evaluate Meal cartV3
        EDVOCartV3Builder mealCartV3Payload = edvoHelper.evaluateEDVOCartV3Payload(listsOfEDVOMealGroupItem.get(0), listsOfEDVOMealGroupItem.get(2), listOfEDVORestIdPriceCount.get(0), listOfEDVORestIdPriceCount.get(1), listOfEDVORestIdPriceCount.get(2), listsOfEDVOMealGroupItem.get(1), Integer.parseInt("100"), false, true, superUserId);

        //Evaluate Restaurant cartV3
        ItemRequestCartV3Builder restaurantCartV3payload = edvoHelper.evaluateItemCartV3Payload(listOfTDRestCategorySubCatItemId.get(0), listOfTDRestCategorySubCatItemId.get(1), listOfTDRestCategorySubCatItemId.get(2), listOfTDRestCategorySubCatItemId.get(3), listOfEDVORestIdPriceCount.get(2), listOfEDVORestIdPriceCount.get(1), Integer.parseInt("100"), false, true, superUserId);

        return new Object[][]{{edvoCampaignId, edvoWithRestFirstCampId, edvoFirstCampId, edvoThirtyDormantCampId, edvoUserRestrictedCampId, tdCreation, listingPayload, mealPayload, evaluateMenu, mealCartV3Payload, restaurantCartV3payload, superFreebieCamp, superFreedelCamp}};
    }
}
