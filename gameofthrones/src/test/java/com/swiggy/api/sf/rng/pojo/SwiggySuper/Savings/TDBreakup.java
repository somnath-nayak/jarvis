package com.swiggy.api.sf.rng.pojo.SwiggySuper.Savings;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "trade_discount_breakup"
})
public class TDBreakup {

    @JsonProperty("trade_discount_breakup")
    private List<TradeDiscountBreakup> tradeDiscountBreakup = null;

    @JsonProperty("trade_discount_breakup")
    public List<TradeDiscountBreakup> getTradeDiscountBreakup() {
        return tradeDiscountBreakup;
    }

    @JsonProperty("trade_discount_breakup")
    public void setTradeDiscountBreakup(List<TradeDiscountBreakup> tradeDiscountBreakup) {
        this.tradeDiscountBreakup = tradeDiscountBreakup;
    }

}