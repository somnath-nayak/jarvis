package com.swiggy.api.sf.rng.helper;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.swiggy.api.sf.rng.constants.SegmentationConstants;
import com.swiggy.api.sf.rng.pojo.OrderCountByPaymentMethodPOJO;
import com.swiggy.api.sf.rng.pojo.RestaurantIds;
import com.swiggy.api.sf.rng.pojo.ads.Segmentation.OrderCreateMessageBuilder;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.QueueData;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.WireMockHelper;
import org.apache.commons.io.FileUtils;
import org.testng.asserts.SoftAssert;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class SegmentationHelper {
    static Initialize gameOfThrones = new Initialize();
    JsonHelper jsonHelper = new JsonHelper();
    RestaurantIds restaurantIds = new RestaurantIds();
    Initialize init = new Initialize();
    OrderCreateMessageBuilder orderCreateMessageBuilder = new OrderCreateMessageBuilder();
    WireMockHelper wireMockHelper = new WireMockHelper();
    RngHelper rngHelper = new RngHelper();
    
    public Processor getUserFirstOrder(String userId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        String[] urlParam = new String[1];
        urlParam[0] = userId;
        GameOfThronesService gameOfThronesService = new GameOfThronesService("segmentation", "userFirstOrder", gameOfThrones);
        Processor processor = new Processor(gameOfThronesService, requestHeader, null, urlParam);
        return processor;
    }
    
    public Processor getNewUserByDeviceId(String deviceId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("segmentation", "newUserByDeviceId", gameOfThrones);
        String[] urlParam = new String[1];
        urlParam[0] = deviceId;
        Processor processor = new Processor(service, requestHeader, null, urlParam);
        return processor;
    }
    
    public Processor getCartTypeAllOrderCountsOfUser(String userId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("segmentation", "cartTypeAllOrderCountByUserId", gameOfThrones);
        String[] urlParam = new String[1];
        urlParam[0] = userId;
        Processor processor = new Processor(service, requestHeader, null, urlParam);
        return processor;
    }
    
    public Processor getUserOrderCountByPaymentMethod(String userId, Boolean isPaymentMethodOrderCountRequired) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("segmentation", "userOrderCountByPaymentMethod", gameOfThrones);
        String[] urlParam = new String[2];
        urlParam[0] = userId;
        urlParam[1] = Boolean.toString(isPaymentMethodOrderCountRequired);
        Processor processor = new Processor(service, requestHeader, null, urlParam);
        return processor;
    }
    
    public Processor updateOrderCountByPaymentMethod(List<OrderCountByPaymentMethodPOJO> payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("segmentation", "updateOrderCountByPaymentMethodTypePOJO", gameOfThrones);
            processor = new Processor(service, requestHeader, new String[]{jsonHelper.getObjectToJSON(payload)});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processor;
    }
    
    public Processor getUserRestaurantLastOrderMap(String userId, String[] restaurantIds) throws IOException {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        //String payload =populateRestaurantPayload(restaurantIds);
        GameOfThronesService service = new GameOfThronesService("segmentation", "userRestaurantLastOrderMap", gameOfThrones);
        String[] urlParam = {userId};
        String[] payloadParams = restaurantIds;
        Processor processor = new Processor(service, requestHeader, payloadParams, urlParam);
        return processor;
    }
    
    public Processor getUserSegment(String userId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("segmentation", "getUserSegment                                  ", gameOfThrones);
        String[] urlParam = new String[1];
        urlParam[0] = userId;
        Processor processor = new Processor(service, requestHeader, null, urlParam);
        return processor;
    }
    
    public Processor getUserOrderCount(String userId) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("segmentation", "getUserOrderCount", gameOfThrones);
        String[] urlParam = new String[1];
        urlParam[0] = userId;
        Processor processor = new Processor(service, requestHeader, null, urlParam);
        return processor;
    }
    
    public String populateRestaurantPayload(String[] restIdList) {
        String payload = "";
        payload = restaurantIds.createRestaurantList(restIdList);
        return payload;
    }
    
    public void stubCheckoutOrderResponse(String userId, String orderId, String restId, String orderType, String paymentMethod, String orderTime, String paymentTxnStatus, String order_status) throws IOException {
        File file = new File("../Data/MockAPI/ResponseBody/rng/segmentation/SegmentationMockResponse");
        String body = FileUtils.readFileToString(file)
                              .replace("${user_id}", userId)
                              .replace("${order_id}", orderId)
                              .replace("${restaurant_id}", restId)
                              .replace("${order_type}", orderType)
                              .replace("${order_time}", orderTime)
                              .replace("${payment_method}", paymentMethod)
                              .replace("${ordered_time_in_seconds}", String.valueOf(System.currentTimeMillis()))
                              .replace("${payment_txn_status}", paymentTxnStatus)
                              .replace("${order_status}", order_status);
        
        //  wireMockHelper.setupStubPut("/api/v1/order/get_order_details/"+orderId, 200, "application/json", body,0);
        
        setMockRequest("/api/v1/order/get_order_details?order_id=" + orderId, body);
    }
    
    public void setMockRequest(String uri, String request) {
//			wireMockHelper.setupStubPost(uri,200,"application/json",body,0);
        
        Processor processor = rngHelper.addMapping(uri, "GET", request);
        System.out.println("mocking uri ::" + uri + "\n with this response body :: " + request);
        
    }
    
    public static String getDate() throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String date = sdf.format(calendar.getTime());
        System.out.println(date);
        return date;
    }
    
    public static String[] getDateInFormatAndInMillisecond() throws ParseException {
        String[] dateValues = new String[2];
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND, 0);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String date = sdf.format(calendar.getTime());
        Date date1 = sdf.parse(date);
        long millis = date1.getTime();
        
        dateValues[0] = date;
        dateValues[1] = Long.toString(millis);
        System.out.println(date);
        return dateValues;
    }
    
    public static String getPastTime(int month) {
        Calendar calendar = Calendar.getInstance();
        
        System.out.println("month " + month);
        System.out.println("Curr Date " + new Date());
        
        calendar.setTime(new Date());
        calendar.add(Calendar.MILLISECOND, -30000);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00.000");
        String date = sdf.format(calendar.getTime());
        System.out.println("Check date for past---->>>  " + date);
        return date;
    }
    
    public static String getDateInToMilliseconds(String validDate) throws ParseException {
        String myDate = validDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00.000");
        Date date = sdf.parse(myDate);
        long millis = date.getTime();
        
        return Long.toString(millis);
    }
    
    public static String[] getDatePlusDays(int days) throws ParseException {
        String[] dateValues = new String[2];
        Calendar calendar = Calendar.getInstance();
        //calendar.setTime(date);
        calendar.setTime(new Date());
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, days);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String date = sdf.format(calendar.getTime());
        Date date1 = sdf.parse(date);
        long millis = date1.getTime();
        
        dateValues[0] = date;
        dateValues[1] = Long.toString(millis);
        System.out.println(date);
        return dateValues;
    }
    
    public void userSegmentAssertionCheck(Processor response , String userId , String valueSegment, String igccSegment, String marketingSegment, String premiumSegment){
        SoftAssert softAssert = new SoftAssert();
        String userIdInResponse = response.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.user_id");
        String value = response.ResponseValidator.GetNodeValue("$.data.value_segment");
        String igcc = response.ResponseValidator.GetNodeValue("$.data.igcc_segment");
        String marketing = response.ResponseValidator.GetNodeValue("$.data.marketing_segment");
        String premium = Integer.toString(response.ResponseValidator.GetNodeValueAsInt("$.data.premium_segment"));
        softAssert.assertEquals(marketing, marketingSegment);
        softAssert.assertEquals(premium, premiumSegment);
        softAssert.assertEquals(userIdInResponse, userId);
        softAssert.assertEquals(value, valueSegment);
        softAssert.assertEquals(igcc, igccSegment);
        softAssert.assertAll();
    }
    
    public void userOrderCountAssertionCheck(Processor response, String totalOrderCount, String userId) {
        int orderCount = response.ResponseValidator.GetNodeValueAsInt("$.data.orderCount");
        String customerId = Integer.toString(response.ResponseValidator.GetNodeValueAsInt("$.data.customerId"));
//        softAssert.assertEquals(orderCountByPaymentMethodInResponse,"{}");
        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(orderCount, Integer.parseInt(totalOrderCount));
        softAssert.assertEquals(customerId, userId);
        softAssert.assertAll();
    }
    
    public void newUserByDeviceAssertionCheck(Processor response , int newUser, int statusCode){
        SoftAssert softAssert = new SoftAssert();
        int user = response.ResponseValidator.GetNodeValueAsInt("$.data.new_user");
        int status = response.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        softAssert.assertEquals(user, newUser);
        softAssert.assertEquals(status, statusCode);
        softAssert.assertAll();
    }
    
    public void userRestaurantLastOrderAssertionCheck(Processor response, String restaurantId, String orderTime){
        SoftAssert softAssert = new SoftAssert();
        
        if (orderTime.equalsIgnoreCase("null")) {
            String restLastOrderTimeInResp =  response.ResponseValidator.GetNodeValue
                                                                                ("$.data.restaurantLastOrderMap." + restaurantId);
            System.out.println("actual --->> " + restLastOrderTimeInResp + "  Expected " + orderTime);
             softAssert.assertEquals(restLastOrderTimeInResp, null);
            softAssert.assertAll();
        }
    else {
            String  restLastOrderTimeInResp = response.ResponseValidator.GetNodeValueAsStringFromJsonArray
                                                                                ("$.data.restaurantLastOrderMap." + restaurantId);
            System.out.println("actual --->> " + restLastOrderTimeInResp + "  Expected " + orderTime);
             softAssert.assertEquals(restLastOrderTimeInResp, orderTime);
            softAssert.assertAll();
        }
        
       
    }
}

