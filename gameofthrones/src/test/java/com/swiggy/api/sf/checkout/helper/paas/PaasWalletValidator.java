package com.swiggy.api.sf.checkout.helper.paas;

import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.swiggy.api.sf.checkout.constants.PaasConstant;
import com.swiggy.api.sf.checkout.constants.SchemaValidationConstants;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class PaasWalletValidator {

    PaasHelper paasHelper=new PaasHelper();
    SoftAssert softAssertion= new SoftAssert();
    SchemaValidatorUtils schemaValidatorUtils = new SchemaValidatorUtils();
    String schemaPath=SchemaValidationConstants.PAAS_SCHEMA_PATH;
    boolean isEmpty;

    public void validateResponseCode(Processor processor){
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200,"HTTP response code is not 200");
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        Assert.assertEquals(statusCode, 0,"API response- status code is not 0");
    }

    public void validateHTTPCode(Processor processor){
        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200,"HTTP response code is not 200");
    }

    public static HashMap<String, String> requestHeader(String tid, String token) {
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("content-type", "application/json");
        requestHeaders.put("tid", tid);
        requestHeaders.put("token", token);
        return requestHeaders;
    }

    public HashMap<String, String> loginTokenData(String mobile, String password) {
        ///Processor processor = SnDHelper.consumerLogin(mobile, password);
        Processor processor = paasHelper.loginFromCheckout(mobile, password);
        String tid=processor.ResponseValidator.GetNodeValue("$.tid");
        String token = processor.ResponseValidator.GetNodeValue("$.data.token");
        String userId = processor.ResponseValidator.GetNodeValue("$.data.customer_id");
        String deviceId = processor.ResponseValidator.GetNodeValue("$.deviceId");
        String sid= processor.ResponseValidator.GetNodeValue("$.sid");

        HashMap<String, String> hashMap= new HashMap<>();
        hashMap.put("tid", tid);
        hashMap.put("token", token);
        hashMap.put("userId", userId);
        hashMap.put("deviceId", deviceId);
        hashMap.put("sid",sid);
        return hashMap;
    }


    public void validateSchema(String response,String fileName,String message)   {
        try {
            String expectedSchema = getSchemafromFile(schemaPath, fileName);
            List<String> missingNodeList = schemaValidatorUtils.validateServiceSchema(expectedSchema, response);
            softAssertion.assertTrue(missingNodeList.isEmpty(), missingNodeList + " Nodes Are Missing Or Not Matching For " + message + " API");
            isEmpty = schemaValidatorUtils.hasEmptyNodes(response);
        }catch (Exception e){
            e.printStackTrace();
        }
        Reporter.log("FileName: " + fileName,true);
        Reporter.log("Contain empty nodes=>" + isEmpty,true);
        softAssertion.assertEquals(false, isEmpty, "Found Empty Nodes in Response");
    }


    public String getSchemafromFile(String schemaPath,String fileName) throws IOException, ProcessingException{
        return new ToolBox().readFileAsString(System.getProperty("user.dir") + schemaPath+fileName);
    }


    public int callMobikwikCheckBalance(String tid, String token){
        Processor processor=paasHelper.mobikwikCheckBalance(tid,token);
        validateHTTPCode(processor);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        return statusCode;
    }

    public Processor callMobikwikDelink(String tid, String token){
        Processor processor=paasHelper.mobikwikDelink(tid,token);
        validateHTTPCode(processor);
        return processor;
    }

    public Processor validateMobikwikCheckBalance(String tid, String token){
        Processor processor=paasHelper.mobikwikCheckBalance(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Mobikwik/mobikwik_check_balance.txt","Mobikwiks Check balance API");
        //validatePaymentAPIBasicResponseData(processor,tid);

        String walletBalance =processor.ResponseValidator.GetNodeValue("$.data.balanceamount");
        Assert.assertNotNull(walletBalance,"walletBalance is null for FreeChargeCheckBalanceApi");
        return processor;
    }

    public Processor validateMobikwikLink(String tid, String token){
        Processor processor=paasHelper.mobikwikLink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Mobikwik/mobikwik_wallet_link.txt","Mobikwik Link API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatemobikwikOtp(String tid, String token,String otp){
        Processor processor=paasHelper.mobikwikValidateOtp(tid,token,otp);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Mobikwik/mobikwik_validate_otp.txt","Mobikwik validate otp API");
        //validatePaymentAPIBasicResponseData(processor,tid);

        String data =processor.ResponseValidator.GetNodeValue("$.data");
        softAssertion.assertNotNull(data,"Data is null for Mobikwik Validate-OTP Api");

        return processor;
    }

    public Processor validateMobikwikDelink(String tid, String token){
        Processor processor=paasHelper.mobikwikDelink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Mobikwik/mobikwik_delink.txt","Mobikwik delink API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validateMobikwikHasToken(String tid, String token){
        Processor processor=paasHelper.mobikwikHasToken(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Mobikwik/mobikwik_has_token.txt","Mobikwik has Token API");
        //validatePaymentAPIBasicResponseData(processor,tid);

        boolean has_token =processor.ResponseValidator.GetNodeValueAsBool("$.data.has_token");
        softAssertion.assertTrue(has_token, "Freecharge has-token is false");

        return processor;
    }


    public int callfreechargeCheckBalance(String tid, String token){
        Processor processor=paasHelper.freechargeCheckBalance(tid,token);
        validateHTTPCode(processor);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        return statusCode;
    }

    public Processor validatefreechargeCheckBalance(String tid, String token){
        Processor processor=paasHelper.freechargeCheckBalance(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "FreeCharge/freecharge_check_balance.txt","FreeCharge Check balance API");
       // validatePaymentAPIBasicResponseData(processor,tid);
        String walletBalance =processor.ResponseValidator.GetNodeValue("$.data.walletBalance");
        Assert.assertNotNull(walletBalance,"walletBalance is null for FreeChargeCheckBalanceApi");
        return processor;
    }

    public Processor validatefreechargeLink(String tid, String token){
        Processor processor=paasHelper.freechargeLink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "FreeCharge/freecharge_wallet_link.txt","FreeCharge Link API");
       // validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatefreechargeOtp(String tid, String token,String otpID,String otp){
        Processor processor=paasHelper.freechargeValidateOtp(tid,token,otpID,otp);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "FreeCharge/freecharge_validate_otp.txt","FreeCharge Validate OTP API");
        //validatePaymentAPIBasicResponseData(processor,tid);

        String data =processor.ResponseValidator.GetNodeValue("$.data");
        softAssertion.assertNotNull(data,"FreeCharge Validate OTP response is null for a field DATA");

        return processor;
    }

    public Processor validatefreechargeDelink(String tid, String token){
        Processor processor=paasHelper.freechargeDelink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "FreeCharge/freecharge_delink.txt","FreeCharge delink API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatefreechargeHasToken(String tid, String token){
        Processor processor=paasHelper.freechargeHasToken(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "FreeCharge/freecharge_has_token.txt","FreeCharge has Token API");
       // validatePaymentAPIBasicResponseData(processor,tid);

        boolean has_token =processor.ResponseValidator.GetNodeValueAsBool("$.data.has_token");
        softAssertion.assertTrue(has_token, "Freecharge has-token is false");

        return processor;
    }

    public Processor validateFreechargeResendOtp(String tid, String token,String otpID){
        Processor processor=paasHelper.freechargeResendOtp(tid,token,otpID);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
            "FreeCharge/freecharge_resend_otp.txt","FreeCharge Resend OTP API");
    return processor;
}

    /*public void validatePaymentAPIBasicResponseData(Processor processor, String responseTid){
        boolean isSuccessful =processor.ResponseValidator.GetNodeValueAsBool("$.successful");
        softAssertion.assertTrue(isSuccessful,"successful is false in  API response");

        String tid =processor.ResponseValidator.GetNodeValue("$.tid");
        softAssertion.assertEquals(tid,responseTid,"Mismatch in TID");

        String deviceId =processor.ResponseValidator.GetNodeValue("$.deviceId");
        softAssertion.assertEquals(deviceId, PaasConstant.DEVICE_ID,"Mismatch in Device ID");
    }*/


    public int callPaytmCheckBalance(String tid, String token){
        Processor processor=paasHelper.paytmCheckBalance(tid,token);
        validateHTTPCode(processor);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        return statusCode;
    }

    public Processor validatePaytmCheckBalance(String tid, String token){
        Processor processor=paasHelper.paytmCheckBalance(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Paytm/paytm_check_balance.txt","Paytm check balance API");
        //validatePaymentAPIBasicResponseData(processor,tid);

        String walletBalance =processor.ResponseValidator.GetNodeValue("$.data.walletBalance");
        Assert.assertNotNull(walletBalance,"walletBalance is null for Paytm CheckBalanceApi");

        return processor;
    }

    public Processor validatePaytmLink(String tid, String token){
        Processor processor=paasHelper.paytmLink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Paytm/paytm_wallet_link.txt","Paytm link wallet API");
        return processor;
    }

    public Processor validatePaytmOtp(String tid, String token,String state,String otp){
        Processor processor=paasHelper.paytmValidateOtp(tid,token,state,otp);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Paytm/paytm_validate_otp.txt","Paytm validate otp API");

        //validatePaymentAPIBasicResponseData(processor,tid);

        String accessToken =processor.ResponseValidator.GetNodeValue("$.data.accessToken");
        softAssertion.assertNotNull(accessToken,"Paytm Validate OTP response is null for a field accessToken");
        return processor;
    }

    public Processor callPaytmDelink(String tid, String token){
        Processor processor=paasHelper.paytmDelink(tid,token);
        validateHTTPCode(processor);
        return processor;
    }

    public Processor validatePaytmDelink(String tid, String token){
        Processor processor=paasHelper.paytmDelink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Paytm/paytm_delink.txt","Paytm delink API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatePaytmHasToken(String tid, String token){
        Processor processor=paasHelper.paytmHasToken(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Paytm/paytm_has_token.txt","Paytm has token API");

        //validatePaymentAPIBasicResponseData(processor,tid);
        String accessToken =processor.ResponseValidator.GetNodeValue("$.data.accessToken");
        softAssertion.assertNotNull(accessToken, "Paytm has-token is false");
        return processor;
    }

    public Processor validatePhonepeCheckBalance(String tid, String token){
        Processor processor=paasHelper.phonepeCheckBalance(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_check_balance.txt","phonepe check balance API");

        int balance =processor.ResponseValidator.GetNodeValueAsInt("$.data.wallet.availableBalance");
        Assert.assertNotNull(balance,"Balance is null in check balance API response");

        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public int callPhonepeIsLink(String tid, String token){
        Processor processor=paasHelper.phonepeIsLink(tid,token);
        validateHTTPCode(processor);
        int statusCode =processor.ResponseValidator.GetNodeValueAsInt("$.statusCode");
        return statusCode;
    }

    public Processor validatePhonepeIsLink(String tid, String token){
        Processor processor=paasHelper.phonepeIsLink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_islinked.txt","phonepe is-linked API");
       // validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatePhonepeOtp(String tid, String token,String otp){
        Processor processor=paasHelper.phonepeValidateOtp(tid,token,otp);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_validate_otp.txt","phonepe validate otp API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor callPhonepeDelink(String tid, String token){
        Processor processor=paasHelper.phonepeDelink(tid,token);
        validateHTTPCode(processor);
        return processor;
    }

    public Processor validatePhonepeDelink(String tid, String token){
        Processor processor=paasHelper.phonepeDelink(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_delink.txt","phonepe delink API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatePhonepeSendOtp(String tid, String token){
        Processor processor=paasHelper.phonepeSendOtp(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_send_otp.txt","phonepe send OTP API");
        //validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }

    public Processor validatePhonepeResendOtp(String tid, String token){
        Processor processor=paasHelper.phonepeResendOtp(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_resend_otp.txt","phonepe Resend OTP API");
        return processor;
    }

    public Processor validatePhonepeProfileChecksum(String tid, String token){
        Processor processor=paasHelper.phonepeProfileChecksum(tid,token);
        validateResponseCode(processor);
        validateSchema(processor.ResponseValidator.GetBodyAsText(),
                "Phonepe/phonepe_profile_checksum.txt","phonepe profile checksum API");
       // validatePaymentAPIBasicResponseData(processor,tid);
        return processor;
    }
}
