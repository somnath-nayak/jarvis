package com.swiggy.api.sf.snd.helper.AddressPojo;

import com.swiggy.api.sf.snd.constants.SANDConstants;

public class AddressBuilder {
    private AddressPojo addressPojo;

    public AddressBuilder()
    {
        addressPojo=new AddressPojo();
    }

    public AddressBuilder userId(Integer userId)
    {
        addressPojo.setUserId(userId);
        return this;
    }

    public AddressBuilder id(Integer id)
    {
        addressPojo.setId(id);
        return this;
    }

    public AddressBuilder defaultAddress(Boolean flag)
    {
        addressPojo.setDefaultAddress(flag);
        return this;
    }

    public AddressBuilder name(String name)
    {
        addressPojo.setName(name);
        return this;
    }

    public AddressBuilder mobile(String mobile)
    { addressPojo.setMobile(mobile);
        return this;
    }

    public AddressBuilder address(String address)
    { addressPojo.setAddress(address);
        return this;
    }

    public AddressBuilder landMark(String landMark)
    { addressPojo.setLandmark(landMark);
        return this;
    }

    public AddressBuilder area(String area)
    { addressPojo.setArea(area);
        return this;
    }

    public AddressBuilder lat(Double lat)
    { addressPojo.setLat(lat);
        return this;
    }

    public AddressBuilder lng(Double lng)
    { addressPojo.setLng(lng);
        return this;
    }

    public AddressBuilder deleted(Boolean flag)
    { addressPojo.setDeleted(flag);
        return this;
    }

    public AddressBuilder edited(Boolean flag)
    { addressPojo.setEdited(flag);
        return this;
    }

    public AddressBuilder annotation(String anno)
    { addressPojo.setAnnotation(anno);
        return this;
    }

    public AddressBuilder flatNo(String flat)
    { addressPojo.setFlatNo(flat);
        return this;
    }

    public AddressBuilder city(String city)
    { addressPojo.setCity(city);
        return this;
    }

    public AddressBuilder reverseLoc(Boolean reverse)
    { addressPojo.setReverseGeoCodeFailed(reverse);
        return this;
    }

    private void defaultAddress()  {
        if(addressPojo.getUserId()==null){
            addressPojo.setUserId(Integer.parseInt(SANDConstants.userId));
        }
        if(addressPojo.getName()==null){
            addressPojo.setName(SANDConstants.name);
        }

        if(addressPojo.getMobile()==null){
            addressPojo.setMobile(SANDConstants.mobile);
        }


        if(addressPojo.getAddress()==null)
            addressPojo.setAddress(SANDConstants.address);

        if(addressPojo.getLandmark()==null){
            addressPojo.setLandmark(SANDConstants.landmark);
        }
        if(addressPojo.getArea()==null){
            addressPojo.setArea(SANDConstants.area);
        }

        if(addressPojo.getEdited()==null){
            addressPojo.setEdited(Boolean.valueOf(SANDConstants.fal));
        }

        if(addressPojo.getDeleted()==null){
            addressPojo.setDeleted(Boolean.valueOf(SANDConstants.fal));
        }

        if(addressPojo.getLat()==null)
            addressPojo.setLat(Double.parseDouble(SANDConstants.lat));

        if(addressPojo.getDefaultAddress()==null)
            addressPojo.setDefaultAddress(Boolean.valueOf(SANDConstants.fal));

        if(addressPojo.getLng()==null){
            addressPojo.setLng(Double.parseDouble(SANDConstants.lng));
        }
        if(addressPojo.getFlatNo()==null)
            addressPojo.setFlatNo(SANDConstants.flatNo);

        if(addressPojo.getCity()==null){
            addressPojo.setCity(SANDConstants.city);
        }

        if(addressPojo.getAnnotation()==null){
            addressPojo.setAnnotation(SANDConstants.annotation);
        }

        if(addressPojo.getReverseGeoCodeFailed()==null){
            addressPojo.setReverseGeoCodeFailed(Boolean.valueOf(SANDConstants.fal));
        }
    }

    public AddressPojo buildAddress() {
        defaultAddress();
        return addressPojo;
    }

    private void defaultAddressId()  {
        if(addressPojo.getId()==null){
            addressPojo.setId(Integer.parseInt(SANDConstants.userId));
        }

        if(addressPojo.getName()==null){
            addressPojo.setName(SANDConstants.name);
        }

        if(addressPojo.getMobile()==null){
            addressPojo.setMobile(SANDConstants.mobile);
        }


        if(addressPojo.getAddress()==null)
            addressPojo.setAddress(SANDConstants.address);

        if(addressPojo.getLandmark()==null){
            addressPojo.setLandmark(SANDConstants.landmark);
        }
        if(addressPojo.getArea()==null){
            addressPojo.setArea(SANDConstants.area);
        }

        if(addressPojo.getEdited()==null){
            addressPojo.setEdited(Boolean.valueOf(SANDConstants.fal));
        }

        if(addressPojo.getDeleted()==null){
            addressPojo.setDeleted(Boolean.valueOf(SANDConstants.fal));
        }

        if(addressPojo.getLat()==null)
            addressPojo.setLat(Double.parseDouble(SANDConstants.lat));

        if(addressPojo.getDefaultAddress()==null)
            addressPojo.setDefaultAddress(Boolean.valueOf(SANDConstants.fal));

        if(addressPojo.getLng()==null){
            addressPojo.setLng(Double.parseDouble(SANDConstants.lng));
        }
        if(addressPojo.getFlatNo()==null)
            addressPojo.setFlatNo(SANDConstants.flatNo);

        if(addressPojo.getCity()==null){
            addressPojo.setCity(SANDConstants.city);
        }

        if(addressPojo.getAnnotation()==null){
            addressPojo.setAnnotation(SANDConstants.annotation);
        }

        if(addressPojo.getReverseGeoCodeFailed()==null){
            addressPojo.setReverseGeoCodeFailed(Boolean.valueOf(SANDConstants.fal));
        }
    }

    public AddressPojo buildAddressId() {
        defaultAddressId();
        return addressPojo;
    }


    }

