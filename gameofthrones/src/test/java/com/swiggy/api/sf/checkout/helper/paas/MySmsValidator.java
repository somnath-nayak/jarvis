package com.swiggy.api.sf.checkout.helper.paas;

import com.swiggy.api.sf.checkout.constants.PaasConstant;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.swiggy.api.erp.crm.dp.foodissues.RecommendationDP.processor;

public class MySmsValidator {

    MySmsHelper mySmsHelper = new MySmsHelper();
    SoftAssert softAssertion= new SoftAssert();

    public String getAuthToken() {

        Processor processor = mySmsHelper.mySmsLogin(PaasConstant.MSISDN, PaasConstant.MY_SMS_PASSWORD, PaasConstant.APIKEY);
        validateResponseCode(processor);

        String authToken = processor.ResponseValidator.GetNodeValue("$.authToken");
        Assert.assertNotNull(authToken);

        return authToken;
    }

    public String getAuthToken(String mSISDN,String mySmsPassword) {

        Processor processor = mySmsHelper.mySmsLogin(mSISDN,mySmsPassword,PaasConstant.APIKEY);
        validateResponseCode(processor);

        String authToken = processor.ResponseValidator.GetNodeValue("$.authToken");
        Assert.assertNotNull(authToken);

        return authToken;
    }

    public JSONArray getAllConversationId(String authToken) {

        Processor processor = mySmsHelper.mySmsGetAllConversation(PaasConstant.APIKEY, authToken);
        validateResponseCode(processor);

        JSONArray addresses = processor.ResponseValidator.GetNodeValueAsJsonArray("$.conversations..address");
        return addresses;
    }


    public void deleteAllConversation(String authToken) {

        JSONArray addresses = getAllConversationId(authToken);
        List<String> addressList=getAddressList(addresses);

        if(addressList.size()>0) {
            Processor processor = mySmsHelper.mySmsDeleteAllMessage(PaasConstant.APIKEY, authToken, addressList);
            validateResponseCode(processor);
        }
    }


    public String getOTPFromSMS(String authToken,String paymentMethod){
        String otpMessage = getOTPMessgae(authToken, paymentMethod);
        String otp = searchOTP(otpMessage);
        int retry = 0;
        while (otp==null && retry < PaasConstant.MAX_RETRY){
            invokeSyncSmsFromServer(authToken);
            otpMessage = getOTPMessgae(authToken, paymentMethod);
            otp = searchOTP(otpMessage);
            retry++;
        }
        /*if(otp==null){
            throw new RuntimeException("Error in Syncing SMS from MySMS server after " + PaasConstant.MAX_RETRY +" retries.");
        }*/
        return otp;

    }
    public String getOTPMessgae(String authToken,String paymentMethod) {

        JSONArray addresses=getAllConversationId(authToken);
        String address = getAddressId(addresses,paymentMethod);
        String otpMessage =null;

        if (address != null) {
            Processor processor = mySmsHelper.mySmsGetConversation(PaasConstant.APIKEY, authToken, address, 0, 20);
            validateResponseCode(processor);
            otpMessage = processor.ResponseValidator.GetNodeValueAsJsonArray("$.messages..message").toString();
        }
        return otpMessage;
    }



    public String searchOTP(String otpMessage) {
        String otp = null;
        if(otpMessage!=null){
        Pattern pattern = Pattern.compile("[0-9][0-9][0-9]+");
        Matcher matcher = pattern.matcher(otpMessage);
           while (matcher.find()) {
            otp = matcher.group();
           }
        }
        return otp;
    }


    public void validateResponseCode(Processor processor){

        int responseCode = processor.ResponseValidator.GetResponseCode();
        Assert.assertEquals(responseCode, 200);

        int errorCode = processor.ResponseValidator.GetNodeValueAsInt("$.errorCode");
        softAssertion.assertEquals(errorCode, 0);

    }

    public List<String> getAddressList(JSONArray addresses){
        List<String> addressList=new ArrayList<>();
        for (int i = 0; i < addresses.size(); i++) {
            addressList.add((String) addresses.get(i));
        }
        return addressList;
    }

    public String getAddressId(JSONArray addresses,String paymentMethod){
        String address = null;
        for(int i = 0; i<addresses.size();i++) {
            if (addresses.get(i).toString().toLowerCase().contains(paymentMethod)) {
                address = (String) addresses.get(i);
                break;
            }
        }
        return address;
    }


    public void invokeSyncSmsFromServer(String authToken){
        Processor processor =mySmsHelper.mySmsGetDeviceId(PaasConstant.APIKEY,authToken,PaasConstant.OS,PaasConstant.PUSH_REG_ID);
        int deviceId=processor.ResponseValidator.GetNodeValueAsInt("$.deviceId");
        validateResponseCode(processor);

        Processor syncProcessor =mySmsHelper.mySmsSyncMessage(PaasConstant.APIKEY,authToken,deviceId,3);
        validateResponseCode(syncProcessor);
    }
}
