package com.swiggy.api.sf.snd.helper;

import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.helper.AddressPojo.AddressBuilder;
import com.swiggy.api.sf.snd.helper.AddressPojo.AddressPojo;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class AddressHelper {

    Initialize initialize= new Initialize();
    SANDHelper sandHelper= new SANDHelper();
    JsonHelper jsonHelper= new JsonHelper();

    public Processor addAddress(String payload){
        GameOfThronesService service = new GameOfThronesService("sand", "addaddress", initialize);
        return new Processor(service, sandHelper.getHeader(), new String[]{payload});
    }

    public Processor getaddressByUserId(String userId){
        GameOfThronesService service = new GameOfThronesService("sand", "getaddressbyuserid", initialize);
        return new Processor(service, sandHelper.getHeader(), null, new String[]{userId});
    }

    public Processor addressUpdateById(String payload){
        GameOfThronesService service = new GameOfThronesService("sand", "updateaddressbyid", initialize);
        return new Processor(service, sandHelper.getHeader(),  new String[]{payload});
    }

    public Processor addressById(String addressId){
        GameOfThronesService service = new GameOfThronesService("sand", "addressbyaddressid", initialize);
        return new Processor(service, sandHelper.getHeader(), null,  new String[]{addressId});
    }

    public Processor addressDefault(String userId, String annotation){
        GameOfThronesService service = new GameOfThronesService("sand", "addressdefault", initialize);
        return new Processor(service, sandHelper.getHeader(), null,  new String[]{userId, annotation});
    }

    public Processor deleteAddress(String addressId){
        GameOfThronesService service = new GameOfThronesService("sand", "addresssoftdelete", initialize);
        return new Processor(service, sandHelper.getHeader(), null,  new String[]{addressId});
    }


    public String address(Integer id, String mobile, String lat, String lng, String annotation){
        String request="";
        AddressPojo addressPojo= new AddressBuilder().userId(id)
                .name(SANDConstants.name)
                .defaultAddress(false)
                .mobile(mobile)
                .address(SANDConstants.keyInitial)
                .landMark(SANDConstants.area)
                .area(SANDConstants.area)
                .lat(Double.valueOf(lat))
                .lng(Double.valueOf(lng))
                .deleted(Boolean.valueOf(SANDConstants.fal))
                .edited(Boolean.valueOf(SANDConstants.fal))
                .annotation(annotation)
                .city(SANDConstants.city)
                .flatNo(SANDConstants.flatNo)
                .reverseLoc(Boolean.valueOf(SANDConstants.fal))
                .buildAddress();
        try {
            request= jsonHelper.getObjectToJSON(addressPojo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return request;
    }

    public String address(Integer id, String defaultAddress,String mobile, String lat, String lng, String deleted, String edited,String annotation, String reverseLoc){
        String request="";
        AddressPojo addressPojo= new AddressBuilder().userId(id)
                .name(SANDConstants.name)
                .defaultAddress(Boolean.valueOf(defaultAddress))
                .mobile(mobile)
                .address(SANDConstants.keyInitial)
                .landMark(SANDConstants.area)
                .area(SANDConstants.area)
                .lat(Double.valueOf(lat))
                .lng(Double.valueOf(lng))
                .deleted(Boolean.valueOf(deleted))
                .edited(Boolean.valueOf(edited))
                .annotation(annotation)
                .city(SANDConstants.city)
                .flatNo(SANDConstants.flatNo)
                .reverseLoc(Boolean.valueOf(reverseLoc))
                .buildAddress();
        try {
             request= jsonHelper.getObjectToJSON(addressPojo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return request;
    }

    public String addressUpdate(Integer id, Integer userId ,String mobile, String lat, String lng, String annotation){
        String request="";
        AddressPojo addressPojo= new AddressBuilder()
                .id(id)
                .userId(userId)
                .name(SANDConstants.name)
                .defaultAddress(false)
                .mobile(mobile)
                .address(SANDConstants.keyInitial)
                .landMark(SANDConstants.area)
                .area(SANDConstants.area)
                .lat(Double.valueOf(lat))
                .lng(Double.valueOf(lng))
                .deleted(Boolean.valueOf(SANDConstants.fal))
                .edited(Boolean.valueOf(SANDConstants.fal))
                .annotation(annotation)
                .city(SANDConstants.city)
                .flatNo(SANDConstants.flatNo)
                .reverseLoc(Boolean.valueOf(SANDConstants.fal))
                .buildAddress();
        try {
            request=jsonHelper.getObjectToJSON(addressPojo);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return  request;
    }








}