package com.swiggy.api.sf.rng.dp.growthPackDP;

import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.growthPack.CreateTypePOJO;
import com.swiggy.api.sf.rng.pojo.growthPack.TypeMetaPOJO;
import org.testng.annotations.DataProvider;

import java.util.HashMap;

public class GrowthPackDP {


    @DataProvider(name = "createGrowthPackType")
    public static Object[][] createGrowthPackType() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO = new CreateTypePOJO()
                    .setDefault()
                    .withName("TurboType"+ Utility.getRandomPostfix())
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+ Utility.getRandomPostfix()).withDescription("RapidGrowth"+ Utility.getRandomPostfix()).withHeader("Turbo"+ Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO);
            return new Object[][]{{data}};
        }
    }

    @DataProvider(name = "createGrowthPackTypeWithEmptyStringName")
    public static Object[][] createGrowthPackTypeWithEmptyStringName() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO1 = new CreateTypePOJO()
                    .setDefault()
                    .withName("")
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("Turbo"+Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO1);
            return new Object[][]{{data}};
        }
    }
    @DataProvider(name = "createGrowthPackTypeWithSpecialCharAsName")
    public static Object[][] createGrowthPackTypeWithSpecialCharAsName() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO1 = new CreateTypePOJO()
                    .setDefault()
                    .withName("!@#$%^&")
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("Turbo"+Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO1);
            return new Object[][]{{data}};
        }
    }


    @DataProvider(name = "createGrowthPackTypeIconNullCheck")
    public static Object[][] createGrowthPackTypeIconNullCheck() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO = new CreateTypePOJO()
                    .setDefault()
                    .withName("TurboType"+Utility.getRandomPostfix())
                    .withTypeMeta(new TypeMetaPOJO().withIcon("").withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader("Turbo"+Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO);
            return new Object[][]{{data}};
        }
    }
    @DataProvider(name = "createGrowthPackTypeDescriptionNullCheck")
    public static Object[][] createGrowthPackTypeDescriptionNullCheck() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO = new CreateTypePOJO()
                    .setDefault()
                    .withName("TurboType"+Utility.getRandomPostfix())
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+Utility.getRandomPostfix()).withDescription("").withHeader("Turbo"+Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO);
            return new Object[][]{{data}};
        }
    }
    @DataProvider(name = "createGrowthPackTypeHeaderNullCheck")
    public static Object[][] createGrowthPackTypeHeaderNullCheck() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO = new CreateTypePOJO()
                    .setDefault()
                    .withName("TurboType"+Utility.getRandomPostfix())
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+Utility.getRandomPostfix()).withDescription("RapidGrowth"+Utility.getRandomPostfix()).withHeader(""));
            data.put("createTypePOJO", createTypePOJO);
            return new Object[][]{{data}};
        }
    }
    @DataProvider(name = "growthPackTypeDBValidation")
    public static Object[][] growthPackTypeDBValidation() {
        {
            HashMap<String, Object> data = new HashMap<String, Object>();
            CreateTypePOJO createTypePOJO = new CreateTypePOJO()
                    .setDefault()
                    .withName("TurboType"+ Utility.getRandomPostfix())
                    .withTypeMeta(new TypeMetaPOJO().withIcon("IconUrl"+ Utility.getRandomPostfix()).withDescription("RapidGrowth"+ Utility.getRandomPostfix()).withHeader("Turbo"+ Utility.getRandomPostfix()));
            data.put("createTypePOJO", createTypePOJO);
            return new Object[][]{{data}};
        }
    }
}
