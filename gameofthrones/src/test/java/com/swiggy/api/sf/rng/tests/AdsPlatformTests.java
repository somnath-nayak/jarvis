package com.swiggy.api.sf.rng.tests;

import com.jayway.jsonpath.JsonPath;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.helper.RngHelper;
import com.swiggy.api.sf.rng.helper.Utility;
import com.swiggy.api.sf.rng.pojo.ads.FetchChargesPOJO;
import com.swiggy.api.sf.rng.pojo.ads.PrioritySlotsPOJO;
import com.swiggy.api.sf.rng.pojo.ads.RestaurantDetails;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.JonSnow.Processor;
import net.minidev.json.JSONArray;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class AdsPlatformTests {

    public final HashMap<String,HashMap<String,Integer>> slotsStartEndTime = new HashMap<>();
    RngHelper rngHelper = new RngHelper();
    SANDHelper sandHelper = new SANDHelper();
    Processor processor;
    List<HashMap<String, Integer>> restIds, slots;
    List<HashMap<String, String>> restDetailsIds;
//    String[] latLng =  {"12.9326","77.6036"};
    String[] latLng =  {"12.9718915","77.64115449999997"};


    @BeforeClass
    public void fetchRest(){
        restIds = sandHelper.aggregatorRestAreaCityList(latLng);
        restDetailsIds = sandHelper.aggregatorRestNameAreaCityList(latLng);
        Assert.assertFalse(restIds.isEmpty(),"Assertion Failed :: No serviceable and open rest found.");
        setSlotsStartEndTime();
    }

    @AfterClass
    public void deleteTempDirectory(){
        File dir = new File(RngConstants.adsTempFileDir);

        if (dir.exists()) {
        String[]entries = dir.list();
        for(String s: entries){
            File currentFile = new File(dir.getPath(),s);
            currentFile.delete();
        }
            System.out.println("Deleting temp directory");
            dir.delete();
        }
    }

    /**
     * Summary: Validates campaign creation for a single restaurant
     * Steps:
     *      1. get rest details from aggregator for a restID.
     *      2. clear pricing for that rest for particular dates, so that fetch pricing calls returns default pricing.
     *      3. call create campaign which return's campaignID.
     *      4. search for this campaignID and assert if found
     *      5. stop the campaign. reset the state
     */
    @Test(dataProvider = "adsDP")
    public void testPrioritySlotsBooking(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {
        String campaignId;
        Double price;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));

        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();

        processor=  rngHelper.searchAdsCampaign(new HashMap<String, String>(){{put("rest-id",String.valueOf(rest.get("restID")));}});
        rngHelper.stopAdsCampaign(campaignId);
        JSONArray campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.live.*.campaignEntityId");
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)), "Assertion Failed: campaign not visible in search");
    }

    @Test(dataProvider = "adsDP")
    public void testOverlappingSlotsForSameRest(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {
        String campaignId = null, campaignIdExpected;
        Double price;
        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertFalse((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode")== 1, "Assertion Failed :: campaign slot booking didn't failed");
        campaignIdExpected = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusMessage").toString().toLowerCase().split(":")[1].trim().split(" ")[0].trim();
        rngHelper.stopAdsCampaign(campaignId);
        Assert.assertTrue(campaignId.equals(campaignIdExpected));

    }

    @Test(dataProvider = "adsDP")
    public void testOverlappingSlotsForDifferentRestWithDifferentAreaID(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {

            String campaignId;
            Double price;
            HashMap<String, Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//            rest.put("restID",15870);
//            rest.put("areaID",68);
//            rest.put("cityID",1);
            HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
            RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
            restDetails.put(String.valueOf(rest.get("restID")), restaurantDetails);
            prioritySlotsPOJO.setRestDetailsMap(restDetails);

            clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));
            processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
            price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
            prioritySlotsPOJO.setCharges(price.intValue());

            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
//      get another rest
            HashMap<String, Integer> rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
            while (rest2.get("areaID") == rest.get("areaID")) {
                rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
            }

            restDetails = new HashMap<>();
            restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest2.get("restID")).withCityId(rest2.get("cityID")).withAreaId(rest2.get("areaID"));
            restDetails.put(String.valueOf(rest2.get("restID")), restaurantDetails);
            prioritySlotsPOJO.setRestDetailsMap(restDetails);

            clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest2.get("cityID"), rest2.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest2.get("restID"))).getPosition(),rest.get("restID"));
            processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
            price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
            prioritySlotsPOJO.setCharges(price.intValue());

            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

//          assert first rest
            JSONArray campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{
                put("rest-id", String.valueOf(rest.get("restID")));
            }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
            rngHelper.stopAdsCampaign(campaignId);
            List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
            Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));

//          assert second rest
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
            HashMap<String, Integer> finalRest = rest2;
            campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{ put("rest-id", String.valueOf(finalRest.get("restID"))); }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
            rngHelper.stopAdsCampaign(campaignId);
            adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
            Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingAllDaysAllSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000";
//
        double actualPrice,expectedPrice = 28500;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),daysOfWeek.size(),rest.get("cityID"),rest.get("areaID"),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));
//        upload pricing


        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);
        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }


    @Test(dataProvider = "pricingDP")
    public void testPricingPartialDaysAllSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000";
        double actualPrice,expectedPrice = 21000;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),rest.get("cityID"),rest.get("areaID"),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"));
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);
        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingPartialDaysPartialSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000";
        double actualPrice,expectedPrice = 15000;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),rest.get("cityID"),rest.get("areaID"),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"));
        List<String> partialSlots = fetchChargesPOJO.setAllDaysTimeSlot();
        partialSlots.remove(0);
        partialSlots.remove(partialSlots.size()-1);
        String filePath =  generatePricingRuleData(partialSlots,daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);
        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingAllDaysPartialSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000";
        double actualPrice,expectedPrice = 19500;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),rest.get("cityID"),rest.get("areaID"),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        List<String> partialSlots = fetchChargesPOJO.setAllDaysTimeSlot();
        partialSlots.remove(0);
        partialSlots.remove(partialSlots.size()-1);
        String filePath =  generatePricingRuleData(partialSlots,daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);
        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingWithDiscount(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000", discountPercentage ="10";

        double actualPrice,expectedPrice = 25650;

//        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);

//        generate discount upload file
        String discountUploadFilePath = createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,false);
        rngHelper.uploadDiscount(discountUploadFilePath);
//        upload file

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingWithPartialDiscount(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000", discountPercentage ="10";

        double actualPrice,expectedPrice = 26650;

//        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);

//        generate discount upload file
        String discountUploadFilePath = createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"), discountPercentage,false);
        rngHelper.uploadDiscount(discountUploadFilePath);
//        upload file

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingDiscountWithPartialDaysAllSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000", discountPercentage ="10";
        double actualPrice,expectedPrice = 18900;

        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"));
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),Utility.getFutureDate(3,"yyyy-MM-dd"),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);
        rngHelper.uploadPricing(filePath);

        String discountUploadFilePath = createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,false);
        rngHelper.uploadDiscount(discountUploadFilePath);

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }


    @Test(dataProvider = "endToEndDP")
    public void testPrioritySlotsEndToEndBooing(PrioritySlotsPOJO prioritySlotsPOJO, FetchChargesPOJO fetchChargesPOJO) throws IOException, ParseException {
        String cost ="2000", discountPercentage ="10";
        Double actualPrice,expectedPrice = 25650.0;

        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();

        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);

//        generate discount upload file
        String discountUploadFilePath = createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,false);
        rngHelper.uploadDiscount(discountUploadFilePath);
//        upload file

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");

        prioritySlotsPOJO.setCharges(actualPrice.intValue());

        String campaignId = null, restID;
        try {
            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();
        }catch (AssertionError error){
//          eg message  "Time slots are overlapping with other campaign id: 16251 for Rest id: 213"
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusMessage").toString().toLowerCase().split(":")[1].trim().split(" ")[0].trim();
            processor = rngHelper.stopAdsCampaign(campaignId);
            Assert.assertEquals((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"),1,"Assertion Failed :: disable campaign failed");
            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();
        }

        processor = rngHelper.stopAdsCampaign(campaignId);
//16288

    }


//    @Test(dataProvider = "endToEndDP")
    public void testMultiRestPrioritySlotsEndToEndBooking(PrioritySlotsPOJO prioritySlotsPOJO, FetchChargesPOJO fetchChargesPOJO) throws IOException, ParseException {
        String cost ="2000", discountPercentage ="10";
        Double actualPrice,expectedPrice = 11542.5;

        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size())), rest2;
        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();

        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);

//        adding second rest
        rest2 = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
        while(rest2.get("areaID").equals(rest.get("areaID"))){
            rest2 = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
        }
        restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest2.get("restID"))).withCityId(Integer.valueOf(rest2.get("cityID"))).withAreaId(Integer.valueOf(rest2.get("areaID")));
        restDetails.put(String.valueOf(rest2.get("restID")),restaurantDetails);

        fetchChargesPOJO.setRestDetailsMap(restDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest2.get("cityID")),Integer.valueOf(rest2.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest2.get("restID"))).getPosition(), Integer.valueOf(rest2.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);
        String filePath = generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest2.get("cityID")),String.valueOf(rest2.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest2.get("restID"))).getPosition()),cost,true);

        rngHelper.uploadPricing(filePath);

//        generate discount upload file
        createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,false);
        String discountUploadFilePath = createDiscountUploadFile(rest2.get("restID"),rest2.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,true);
        rngHelper.uploadDiscount(discountUploadFilePath);
//        upload file

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");

        prioritySlotsPOJO.setCharges(actualPrice.intValue());

        String campaignId = null, restID;
        try {
            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
            Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();
        }catch (AssertionError error){
//          eg message  "Time slots are overlapping with other campaign id: 16251 for Rest id: 213"
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusMessage").toString().toLowerCase().split(":")[1].trim().split(" ")[0].trim();
            processor = rngHelper.stopAdsCampaign(campaignId);
            Assert.assertEquals((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"),1,"Assertion Failed :: disable campaign failed");
            processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();
        }

        processor = rngHelper.searchAdsCampaign(new HashMap<String, String>() {{
                                        put("rest-id", String.valueOf(rest.get("restID")));
                                    }});
        JSONArray campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");


        JSONArray actualRestIDs = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.live[0].restaurantDetails.*.restaurantId");
        rngHelper.stopAdsCampaign(campaignId);
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));
        Assert.assertTrue(rngHelper.jsonArrayToArrayList(actualRestIDs).contains(Integer.valueOf(rest.get("restID"))),"Assertion failed :: rest 1 not visibile in search response");
        Assert.assertTrue(rngHelper.jsonArrayToArrayList(actualRestIDs).contains(Integer.valueOf(rest2.get("restID"))),"Assertion failed :: rest 2 not visibile in search response");


//16288

    }
    @Test(dataProvider = "adsDP")
    public void testPrioritySlotsSearch(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {
            String campaignId = null, campaignIdExpected;
            Double actualPrice;
            boolean flag = true; int i =0;
            HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
            HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
            RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
            restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
            prioritySlotsPOJO.setRestDetailsMap(restDetails);

            processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));

//        fetch pricing
            actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
            prioritySlotsPOJO.setCharges(actualPrice.intValue());
            while (flag && i<5) {
                try {
                    processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
                    Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
                    Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
                    flag = false;
                } catch (AssertionError error) {
                    i++;
                }
            }
            campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();

            processor = rngHelper.searchAdsCampaign(new HashMap<String, String>() {{
            put("rest-id", String.valueOf(rest.get("restID")));
        }});
        JSONArray campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");


        JSONArray actualRestIDs = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.live[0].restaurantDetails.*.restaurantId");
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));
        Assert.assertTrue(rngHelper.jsonArrayToArrayList(actualRestIDs).contains(Integer.valueOf(rest.get("restID"))),"Assertion failed :: rest 1 not visibile in search response");

            processor = rngHelper.stopAdsCampaign(campaignId);

    }


    @Test(dataProvider = "adsDP")
    public void testPrioritySlotsSearchWithExpiredCampaigns(PrioritySlotsPOJO prioritySlotsPOJO) throws ParseException, IOException {
        String campaignId;
        Double price;

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();

        processor=  rngHelper.searchAdsCampaign(new HashMap<String, String>(){{put("rest-id",String.valueOf(rest.get("restID")));}});
        rngHelper.stopAdsCampaign(campaignId);
        JSONArray campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.live.*.campaignEntityId");
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)), "Assertion Failed: campaign not visible in search");

//        now the campaign should come in expired
        processor=  rngHelper.searchAdsCampaign(new HashMap<String, String>(){{put("rest-id",String.valueOf(rest.get("restID")));}});
        campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.expired.*.campaignEntityId");
        adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)), "Assertion Failed: campaign not visible in search");
    }


    @Test(dataProvider = "adsDP")
    public void testPrioritySlotsWithCron(PrioritySlotsPOJO prioritySlotsPOJO) throws ParseException, IOException {
        String campaignId;
        Double price;

        prioritySlotsPOJO.withStartTime(Utility.getDate("dd/MM/yyyy")).withEndTime(Utility.getFutureDate(1,"dd/MM/yyyy"));

        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data").toString().toLowerCase().split(":")[1].trim();

        processor=  rngHelper.searchAdsCampaign(new HashMap<String, String>(){{put("rest-id",String.valueOf(rest.get("restID")));}});

        JSONArray campaigns = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.live.*.campaignEntityId");
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)), "Assertion Failed: campaign not visible in search");

        rngHelper.executeAdsCron();

        processor = rngHelper.fetchAdsCampaigns(rest.get("restID"));


        rngHelper.stopAdsCampaign(campaignId);
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getDate("yyyy-MM-dd"),rest.get("restID"));
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getFutureDate(1,"yyyy-MM-dd"), rest.get("restID"));
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getFutureDate(2,"yyyy-MM-dd"), rest.get("restID"));

        Assert.assertTrue(JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data."+rest.get("restID")+".restId").toString().equals(rest.get("restID")),"Assertion Failed :: rest campaign not available in fetch campaings.");
    }


    @Test(dataProvider = "pricingDP")
    public void testPricingAllDaysLateNightSlots(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        HashMap<String, Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault()
                .withRestId(rest.get("restID"))
                .withCityId(rest.get("cityID"))
                .withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")), restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(), getDays(fetchChargesPOJO.getStartDate(), fetchChargesPOJO.getEndDate()).size(),
                rest.get("cityID"), rest.get("areaID"), fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), rest.get("restID"));

        //        upload pricing
        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(), fetchChargesPOJO.getEndDate());
        List<String> partialSlots = fetchChargesPOJO.setLateNightTimeSlot();

        String cost = "2000";

        String filePath = generatePricingRuleData(partialSlots, daysOfWeek, String.valueOf(rest.get("cityID")),
                String.valueOf(rest.get("areaID")), fetchChargesPOJO.getStartDate(), fetchChargesPOJO.getEndDate(),
                String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()), cost, false);

        rngHelper.uploadPricing(filePath);

        //  fetch pricing
        processor = rngHelper.fetchpricing(fetchChargesPOJO);

        double expectedPrice = 10500;
        double actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");

        //  assert pricing
        Assert.assertEquals(actualPrice, expectedPrice, "Actual price isnt as expected one");
    }

    @Test(dataProvider = "pricingDP")
    public void testPricingWithHundredPercentDiscount(FetchChargesPOJO fetchChargesPOJO) throws ParseException, IOException {
        String cost ="2000", discountPercentage ="100";

        double actualPrice,expectedPrice = 0.0;

//        HashMap<String,Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String,String> rest = restDetailsIds.get(rngHelper.getRandomNo(restDetailsIds.size()));
//        HashMap<String,Integer> rest = new HashMap<>();
//        rest.put("restID",212);
//        rest.put("cityID",1);
//        rest.put("areaID",1);

        HashMap<String, RestaurantDetails> restDetails = fetchChargesPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(Integer.valueOf(rest.get("restID"))).withCityId(Integer.valueOf(rest.get("cityID"))).withAreaId(Integer.valueOf(rest.get("areaID")));
        restDetails.put(String.valueOf(rest.get("restID")),restaurantDetails);
        fetchChargesPOJO.setRestDetailsMap(restDetails);

//        clear slots
        clearSlotsPricing(fetchChargesPOJO.getStartDate(),getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate()).size(),Integer.valueOf(rest.get("cityID")),Integer.valueOf(rest.get("areaID")),fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(), Integer.valueOf(rest.get("restID")));
//        upload pricing

        List<String[]> daysOfWeek = getDays(fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate());
        String filePath =  generatePricingRuleData(fetchChargesPOJO.setAllDaysTimeSlot(),daysOfWeek, String.valueOf(rest.get("cityID")),String.valueOf(rest.get("areaID")),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(),String.valueOf(fetchChargesPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition()),cost,false);

        rngHelper.uploadPricing(filePath);

//        generate discount upload file
        String discountUploadFilePath = createDiscountUploadFile(rest.get("restID"),rest.get("restName"),fetchChargesPOJO.getStartDate(),fetchChargesPOJO.getEndDate(), discountPercentage,false);
        rngHelper.uploadDiscount(discountUploadFilePath);
//        upload file

        processor = rngHelper.fetchpricing(fetchChargesPOJO);

//        fetch pricing
        actualPrice = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.applicableCharges");
        Assert.assertEquals(actualPrice,expectedPrice,"Actual price isnt as expected one");
//        assert pricing
    }

//    Area Name	Area ID	City Name	City ID	Start Date	End Date	Day	Time Slot	Position	Cost Per Unit	Instrument Type	Unit Type
    public String generatePricingRuleData(List<String> slots, List<String[]> daysOfWeek, String cityID, String areaID, String startDate, String endDate,  String position, String cost, boolean append ) throws ParseException {
        String filePath = null;
        if(null != slots.get(1)) {
            filePath = createPricingRuleFile(cityID, areaID, startDate, endDate, daysOfWeek.get(0)[0].toUpperCase(), slots.get(1), position, cost, append);
            for (int i = 2; i < slots.size(); i++) {
                filePath = createPricingRuleFile(cityID, areaID, startDate, endDate, daysOfWeek.get(0)[0].toUpperCase(), slots.get(i), position, cost, true);
            }
            append = true;
        } else{
            append = false;
        }

        for(int i=1;i<daysOfWeek.size();i++){
            for(int j =0;j<slots.size();j++){
                if(null!=slots.get(j)) {
                    filePath = createPricingRuleFile(cityID, areaID, startDate, endDate, daysOfWeek.get(i)[0].toUpperCase(), slots.get(j), position, cost, append);
                    append = true;
                }
            }
        }

        return filePath;
    }

    public List<String[]> getDays(String start, String end) throws ParseException {

        List<String[]> daysOfWeek = new ArrayList<>();
        Date startDate,endDate;
        SimpleDateFormat dateFormat;
        try {
             dateFormat = new SimpleDateFormat("yyyy-MM-dd");
             startDate = dateFormat.parse(start);
             endDate = dateFormat.parse(end);
        }catch (ParseException e){
             dateFormat = new SimpleDateFormat("dd/MM/yyyy");
             startDate = dateFormat.parse(start);
             endDate = dateFormat.parse(end);
        }
        long diff = endDate.getTime() - startDate.getTime();
        int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        for(int i=0;i<=days;i++){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(startDate);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            Date nextDate = calendar.getTime();
            daysOfWeek.add(new String[]{new SimpleDateFormat("EEEE", Locale.ENGLISH).format(nextDate),dateFormat.format(nextDate)});
        }
    return daysOfWeek;
    }


    public void clearSlotsPricing(String startDate, int days, int cityID, int areaID, int instrumentPos, int restID) throws ParseException {
        String nextDate = null;
        DateFormat formatBefore = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat formatAfter  = new SimpleDateFormat("yyyy-MM-dd");

        Date date;

        for(int i=0;i<=days;i++) {
            try{
                date = formatBefore.parse(startDate);
            } catch (ParseException e) {
                date = formatAfter.parse(startDate);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR, i);
            date = calendar.getTime();
            nextDate = formatAfter.format(date);
            for (HashMap<String, Integer> slot : slots) {
                rngHelper.deletePricing(nextDate, areaID, cityID, instrumentPos, slot.get("StartTime"), slot.get("EndTime"));
                rngHelper.deletePricingExpection(nextDate,slot.get("StartTime"), slot.get("EndTime"), restID);
            }

        }

    }

    public String createPricingRuleFile( String cityID, String areaID, String startDate, String endDate, String day, String timeSlot, String position, String cost, boolean append ) throws ParseException {
        List<String[]> data = new ArrayList<String[]>();

        DateFormat formatBefore  = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatBefore.parse(startDate);
        DateFormat formatAfter = new SimpleDateFormat("dd/MM/yyyy");
        startDate = formatAfter.format(date);

        date = formatBefore.parse(endDate);
        endDate = formatAfter.format(date);

        String fileName = "pricing_rules_upload.csv";
        if(append==false) {
            data.add(new String[]{"Area Name", "Area ID", "City Name", "City ID", "Start Date", "End Date", "Day", "Time Slot", "Position", "Cost Per Unit", "Instrument Type", "Unit Type"});
        }
        data.add(new String[]{"Koramangala",areaID,"Bangalore",cityID,startDate,endDate, day, timeSlot, position, cost, "HIGH_PRIORITY", "SLOT"});
        rngHelper.writeCSVData(data, RngConstants.adsTempFileDir, fileName, append);
        return RngConstants.adsTempFileDir+fileName;


    }



    public String createDiscountUploadFile(String restID, String restName, String startDate, String endDate, String discountPercent, boolean append ) throws ParseException {
        List<String[]> data = new ArrayList<String[]>();

        DateFormat formatBefore  = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatBefore.parse(startDate);
        DateFormat formatAfter = new SimpleDateFormat("dd/MM/yyyy");
        startDate = formatAfter.format(date);

        date = formatBefore.parse(endDate);
        endDate = formatAfter.format(date);

        String fileName = "discounts_upload.csv";
        if(append==false) {
//RID	Rest Name	Start date	End date	Discount(in %)
            data.add(new String[]{"RID","Rest Name","Start date","End date","Discount(in %)"});
        }
        data.add(new String[]{restID,restName,startDate,endDate, discountPercent});
        rngHelper.writeCSVData(data, RngConstants.adsTempFileDir, fileName, append);
        return RngConstants.adsTempFileDir+fileName;
    }

//    instrument position should be multiples of five
    public String createInventoryFile(String... instrumentPosition){
        String currentDate= Utility.getDate("dd/MM/yyyy");
        String nextDay = Utility.getFutureDate(1,"dd/MM/yyyy");
        List<String[]> data = new ArrayList<String[]>();
        data.add(new String[]{"Area ID","City ID","Instrument Type","Instrument Position","Start Date","End Date","Total Quantity"});
        for(String pos: instrumentPosition) {
            data.add(new String[]{"1", "1", "HIGH_PRIORITY", pos, currentDate, nextDay, "1"});
        }

        if(rngHelper.writeCSVData(data,"/Users/dev.karan/Desktop/temp/","inventory.csv", false)){
            return "/Users/dev.karan/Desktop/temp/insventory.csv";
        }
        return "";

    }


    public void setSlotsStartEndTime(){
        slotsStartEndTime.put("EARLY_MORNING", new HashMap<String, Integer>(){{put("StartTime",0); put("EndTime",359);put("Price",250);}});
        slotsStartEndTime.put("BREAKFAST", new HashMap<String, Integer>(){{put("StartTime",360); put("EndTime",659);put("Price",500);}});
        slotsStartEndTime.put("LUNCH", new HashMap<String, Integer>(){{put("StartTime",660); put("EndTime",959);put("Price",500);}});
        slotsStartEndTime.put("SNACKS", new HashMap<String, Integer>(){{put("StartTime",960); put("EndTime",1139);put("Price",500);}});
        slotsStartEndTime.put("DINNER", new HashMap<String, Integer>(){{put("StartTime",1140); put("EndTime",1379);put("Price",500);}});
        slotsStartEndTime.put("LATE_DINNER", new HashMap<String, Integer>(){{put("StartTime",1380); put("EndTime",1439);put("Price",250);}});

        slots = new ArrayList<>();
        slots.add(new HashMap<String, Integer>(){{put("StartTime",0); put("EndTime",359);}});
        slots.add( new HashMap<String, Integer>(){{put("StartTime",360); put("EndTime",659);}});
        slots.add( new HashMap<String, Integer>(){{put("StartTime",660); put("EndTime",959);}});
        slots.add( new HashMap<String, Integer>(){{put("StartTime",960); put("EndTime",1139);}});
        slots.add( new HashMap<String, Integer>(){{put("StartTime",1140); put("EndTime",1379);}});
        slots.add( new HashMap<String, Integer>(){{put("StartTime",1380); put("EndTime",1439);}});
    }



    @DataProvider(name="adsDP")
    public Object[][] dataProvider(){

        PrioritySlotsPOJO prioritySlotsPOJO = new PrioritySlotsPOJO().setDefault();
        Object[][]  object= new Object[][]{
                {prioritySlotsPOJO}
        };
        return object;
    }

    @DataProvider(name="pricingDP")
    public Object[][] pricingdataProvider(){

        FetchChargesPOJO fetchChargesPOJO = new FetchChargesPOJO().setDefault();
        Object[][]  object= new Object[][]{
                {fetchChargesPOJO}
        };
        return object;
    }

    @DataProvider(name="endToEndDP")
    public Object[][] endToEndDataProvider(){

        PrioritySlotsPOJO prioritySlotsPOJO = new PrioritySlotsPOJO().setDefault();
        FetchChargesPOJO fetchChargesPOJO = new FetchChargesPOJO().setDefault();
        Object[][]  object= new Object[][]{
                {prioritySlotsPOJO, fetchChargesPOJO}
        };
        return object;
    }

    public static FetchChargesPOJO convert(PrioritySlotsPOJO  prioritySlotsPOJO) throws ParseException {
        FetchChargesPOJO fetchChargesPOJO = new FetchChargesPOJO();
        fetchChargesPOJO.setRestDetailsMap(prioritySlotsPOJO.getRestaurantDetails());
        fetchChargesPOJO.setTimeSlots(prioritySlotsPOJO.getTimeSlots());
        fetchChargesPOJO.setInstrumentType("HIGH_PRIORITY");
        DateFormat formatBefore = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat formatAfter  = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate = formatBefore.parse(prioritySlotsPOJO.getStartTime());
        fetchChargesPOJO.setStartDate(formatAfter.format(startDate));
        Date endDate = formatBefore.parse(prioritySlotsPOJO.getEndTime());
        fetchChargesPOJO.setEndDate(formatAfter.format(endDate));

        return fetchChargesPOJO;
    }




//    @Test
    public void test(){
//        16375 for Rest id: 5583
        rngHelper.stopAdsCampaign("16407");
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getDate("yyyy-MM-dd"),589);
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getFutureDate(1,"yyyy-MM-dd"),589);
        rngHelper.stopAdsCurrentAdsCampaigns(Utility.getFutureDate(2,"yyyy-MM-dd"),589);
    }


    @Test(dataProvider = "adsDP")
    public void testDifferentSlotsCampaignForDifferentRestWithSameAreaID(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {

        String campaignId;
        Double price;
        HashMap<String, Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
//        rest.put("restID",15870);
//        rest.put("areaID",68);
//        rest.put("cityID",1);
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")), restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        List<String> partialSlots = prioritySlotsPOJO.getTimeSlots();
//        removing slots late night and dinner
        partialSlots.remove(0);
        partialSlots.remove(partialSlots.size()-1);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
//      get another rest
        HashMap<String, Integer> rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
        while ((rest2.get("areaID") != rest.get("areaID"))&&(rest2.get("restID") == rest.get("restID"))) {
            rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
        }


//      2nd rest
        restDetails = new HashMap<>();
        restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest2.get("restID")).withCityId(rest2.get("cityID")).withAreaId(rest2.get("areaID"));
        restDetails.put(String.valueOf(rest2.get("restID")), restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

//      set different non over lapping slots for rest 2
        partialSlots = prioritySlotsPOJO.setAllDaysTimeSlot();
        partialSlots.remove(1); //removing breakfast
        partialSlots.remove(1); //removing lunch
        partialSlots.remove(1); //removing snacks
        prioritySlotsPOJO.withTimeSlots(partialSlots);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest2.get("cityID"), rest2.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest2.get("restID"))).getPosition(),rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

//          assert first rest
        JSONArray campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{
            put("rest-id", String.valueOf(rest.get("restID")));
        }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
        rngHelper.stopAdsCampaign(campaignId);
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));

//          assert second rest
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
        HashMap<String, Integer> finalRest = rest2;
        campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{ put("rest-id", String.valueOf(finalRest.get("restID"))); }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
        rngHelper.stopAdsCampaign(campaignId);
        adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));
    }


    @Test(dataProvider = "adsDP")
    public void testOverlappingSlotsForDifferentRestWithDifferentAreaIDWithSameParentRest(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException, ParseException {
        String campaignId;
        Double price;
//       set same parent rest id for both rest

        HashMap<String, Integer> rest = restIds.get(rngHelper.getRandomNo(restIds.size()));
        HashMap<String, RestaurantDetails> restDetails = prioritySlotsPOJO.getRestaurantDetails();
        RestaurantDetails restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest.get("restID")).withCityId(rest.get("cityID")).withAreaId(rest.get("areaID"));
        restDetails.put(String.valueOf(rest.get("restID")), restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest.get("cityID"), rest.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest.get("restID"))).getPosition(),rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
//      get another rest
        HashMap<String, Integer> rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
        while (rest2.get("areaID") != rest.get("areaID")) {
            rest2 = restIds.get(rngHelper.getRandomNo(restIds.size()));
        }
        if(rest2.get("areaID") != rest.get("areaID")){
            Assert.fail("No two rest with same area id is available");
        }

        restDetails = new HashMap<>();
        restaurantDetails = new RestaurantDetails().setDefault().withRestId(rest2.get("restID")).withCityId(rest2.get("cityID")).withAreaId(rest2.get("areaID"));
        restDetails.put(String.valueOf(rest2.get("restID")), restaurantDetails);
        prioritySlotsPOJO.setRestDetailsMap(restDetails);

        clearSlotsPricing(prioritySlotsPOJO.getStartTime(), getDays(prioritySlotsPOJO.getStartTime(),prioritySlotsPOJO.getEndTime()).size(), rest2.get("cityID"), rest2.get("areaID"), prioritySlotsPOJO.getRestaurantDetails().get(String.valueOf(rest2.get("restID"))).getPosition(),rest.get("restID"));
        processor = rngHelper.fetchpricing(convert(prioritySlotsPOJO));
        price = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data.applicableCharges");
        prioritySlotsPOJO.setCharges(price.intValue());

        processor = rngHelper.doInventorySlotBooking(prioritySlotsPOJO);
//        Assert.assertEquals((int) JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.statusCode"), 1, "Assertion Failed :: campaign slot booking failed");
//        Assert.assertTrue(processor.ResponseValidator.GetNodeValue("$.data").toLowerCase().contains("creation done successfully"), "Assertion Failed :: campaign slot booking");

//          assert first rest
        JSONArray campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{
            put("rest-id", String.valueOf(rest.get("restID")));
        }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
        rngHelper.stopAdsCampaign(campaignId);
        List<Integer> adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));

//          assert second rest
        campaignId = JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString().toLowerCase().split(":")[1].trim();
        HashMap<String, Integer> finalRest = rest2;
        campaigns = JsonPath.read(rngHelper.searchAdsCampaign(new HashMap<String, String>() {{ put("rest-id", String.valueOf(finalRest.get("restID"))); }}).ResponseValidator.GetBodyAsText(), "$.data.live.*.campaignEntityId");
        rngHelper.stopAdsCampaign(campaignId);
        adsCampaigns = rngHelper.jsonArrayToArrayList(campaigns);
        Assert.assertTrue(adsCampaigns.contains(Integer.valueOf(campaignId)));
    }






}


//create csv