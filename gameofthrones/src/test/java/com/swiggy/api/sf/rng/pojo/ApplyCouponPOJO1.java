package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class ApplyCouponPOJO1 {

    @JsonProperty("code")
    private String code;
    @JsonProperty("areaId")
    private int areaId;
    @JsonProperty("cityId")
    private int cityId;
    @JsonProperty("swiggyTradeDiscount")
    private Double swiggyTradeDiscount;
    @JsonProperty("restaurantTradeDiscount")
    private Double restaurantTradeDiscount;
    @JsonProperty("itemKey")
    private  Long itemKey;
    @JsonProperty("menuItemId")
    private Integer menuItemId;
    @JsonProperty("quantity")
    private int quantity;
    @JsonProperty("restaurantId")
    private String restaurantId;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("swiggyMoney")
    private Double swiggyMoney;
    @JsonProperty("firstOrder")
    private Boolean firstOrder;
    @JsonProperty("cartPrice_Quantity")
    private Integer cartPrice_Quantity;
    @JsonProperty("cartTotal")
    private Double cartTotal;
    @JsonProperty("itemLevel")
    private String itemLevel;
    @JsonProperty("itemLevelPriceQuantity")
    private Integer itemLevelPriceQuantity;
    @JsonProperty("itemLevelPriceSubTotal")
    private Double  itemLevelPriceSubTotal;
    @JsonProperty("preferredPaymentMethod")
    private String preferredPaymentMethod;
    @JsonProperty("referralPresence")
    private Boolean referralPresence;
    @JsonProperty("swiggyMoneyApplicable")
    private Boolean swiggyMoneyApplicable;
    @JsonProperty("superUser")
    private Boolean superUser;
    @JsonProperty("typeOfPartner")
    private String typeOfPartner;
    @JsonProperty("versionCode")
    private  Integer versionCode;
    @JsonProperty("userAgent")
    private String userAgent;
    @JsonProperty("payment_codes")
    private String payment_codes;
    @JsonProperty("time")
    private String time;


    public String getCode() {
        return code;
    }

    public int getAreaId() {
        return areaId;
    }

    public int getCityId() {
        return cityId;
    }

    public Double getSwiggyTradeDiscount() {
        return swiggyTradeDiscount;
    }

    public Double getRestaurantTradeDiscount() {
        return restaurantTradeDiscount;
    }

    public Long getItemKey() {
        return itemKey;
    }

    public Integer getMenuItemId() {
        return menuItemId;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getUserId() {
        return userId;
    }

    public Double getSwiggyMoney() {
        return swiggyMoney;
    }

    public Boolean getFirstOrder() {
        return firstOrder;
    }

    public Integer getCartPrice_Quantity() {
        return cartPrice_Quantity;
    }

    public Double getCartTotal() {
        return cartTotal;
    }

    public String getItemLevel() {
        return itemLevel;
    }

    public Integer getItemLevelPriceQuantity() {
        return itemLevelPriceQuantity;
    }

    public Double getItemLevelPriceSubTotal() {
        return itemLevelPriceSubTotal;
    }

    public String getPreferredPaymentMethod() {
        return preferredPaymentMethod;
    }

    public Boolean getReferralPresence() {
        return referralPresence;
    }

    public Boolean getSwiggyMoneyApplicable() {
        return swiggyMoneyApplicable;
    }

    public Boolean getSuperUser() {
        return superUser;
    }

    public String getTypeOfPartner() {
        return typeOfPartner;
    }

    public Integer getVersionCode() {
        return versionCode;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getPayment_codes() {
        return payment_codes;
    }

    public String getTime() {
        return time;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public void setSwiggyTradeDiscount(Double swiggyTradeDiscount) {
        this.swiggyTradeDiscount = swiggyTradeDiscount;
    }

    public void setRestaurantTradeDiscount(Double restaurantTradeDiscount) {
        this.restaurantTradeDiscount = restaurantTradeDiscount;
    }

    public void setItemKey(Long itemKey) {
        this.itemKey = itemKey;
    }

    public void setMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setSwiggyMoney(Double swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
    }

    public void setFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
    }

    public void setCartPrice_Quantity(Integer cartPrice_Quantity) {
        this.cartPrice_Quantity = cartPrice_Quantity;
    }

    public void setCartTotal(Double cartTotal) {
        this.cartTotal = cartTotal;
    }

    public void setItemLevel(String itemLevel) {
        this.itemLevel = itemLevel;
    }

    public void setItemLevelPriceQuantity(Integer itemLevelPriceQuantity) {
        this.itemLevelPriceQuantity = itemLevelPriceQuantity;
    }

    public void setItemLevelPriceSubTotal(Double itemLevelPriceSubTotal) {
        this.itemLevelPriceSubTotal = itemLevelPriceSubTotal;
    }

    public void setPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
    }

    public void setReferralPresence(Boolean referralPresence) {
        this.referralPresence = referralPresence;
    }

    public void setSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
    }

    public void setSuperUser(Boolean superUser) {
        this.superUser = superUser;
    }

    public void setTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
    }

    public void setVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public void setPayment_codes(String payment_codes) {
        this.payment_codes = payment_codes;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ApplyCouponPOJO1 withCode(String code) {
        this.code = code;
        return this;
    }

    public ApplyCouponPOJO1 withAreaId(int areaId) {
        this.areaId = areaId;
        return this;
    }

    public ApplyCouponPOJO1 withCityId(int cityId) {
        this.cityId = cityId;
        return this;
    }

    public ApplyCouponPOJO1 withSwiggyTradeDiscount(Double swiggyTradeDiscount) {
        this.swiggyTradeDiscount = swiggyTradeDiscount;
        return this;
    }

    public ApplyCouponPOJO1 withRestaurantTradeDiscount(Double restaurantTradeDiscount) {
        this.restaurantTradeDiscount = restaurantTradeDiscount;
        return this;
    }

    public ApplyCouponPOJO1 withItemKey(Long itemKey) {
        this.itemKey = itemKey;
        return this;
    }

    public ApplyCouponPOJO1 withMenuItemId(Integer menuItemId) {
        this.menuItemId = menuItemId;
        return this;
    }

    public ApplyCouponPOJO1 withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public ApplyCouponPOJO1 withRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
        return this;
    }

    public ApplyCouponPOJO1 withUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public ApplyCouponPOJO1 withSwiggyMoney(Double swiggyMoney) {
        this.swiggyMoney = swiggyMoney;
        return this;
    }

    public ApplyCouponPOJO1 withFirstOrder(Boolean firstOrder) {
        this.firstOrder = firstOrder;
        return this;
    }

    public ApplyCouponPOJO1 withCartPrice_Quantity(int cartPrice_Quantity) {
        this.cartPrice_Quantity = cartPrice_Quantity;
        return this;
    }

    public ApplyCouponPOJO1 withCartTotal(Double cartTotal) {
        this.cartTotal = cartTotal;
        return this;
    }

    public ApplyCouponPOJO1 withItemLevel(String itemLevel) {
        this.itemLevel = itemLevel;
        return this;
    }

    public ApplyCouponPOJO1 withItemLevelPriceQuantity(Integer itemLevelPriceQuantity) {
        this.itemLevelPriceQuantity = itemLevelPriceQuantity;
        return this;
    }


    public ApplyCouponPOJO1 withItemLevelPriceSubTotal(Double itemLevelPriceSubTotal) {
        this.itemLevelPriceSubTotal = itemLevelPriceSubTotal;
        return this;
    }


    public ApplyCouponPOJO1 withPreferredPaymentMethod(String preferredPaymentMethod) {
        this.preferredPaymentMethod = preferredPaymentMethod;
        return this;
    }


    public ApplyCouponPOJO1 withReferralPresence(Boolean referralPresence) {
        this.referralPresence = referralPresence;
        return this;
    }


    public ApplyCouponPOJO1 withSwiggyMoneyApplicable(Boolean swiggyMoneyApplicable) {
        this.swiggyMoneyApplicable = swiggyMoneyApplicable;
        return this;
    }
    public ApplyCouponPOJO1 withSuperUser(Boolean superUser) {
        this.superUser = superUser;
        return this;
    }


    public ApplyCouponPOJO1 withTypeOfPartner(String typeOfPartner) {
        this.typeOfPartner = typeOfPartner;
        return this;
    }

    public ApplyCouponPOJO1 withVersionCode(Integer versionCode) {
        this.versionCode = versionCode;
        return this;
    }

    public ApplyCouponPOJO1 withUserAgent(String userAgent) {
        this.userAgent = userAgent;
        return this;
    }

    public ApplyCouponPOJO1 withPayment_codes(String payment_codes) {
        this.payment_codes = payment_codes;
        return this;
    }

    public ApplyCouponPOJO1 withTime(String time) {
        this.time = time;
        return this;
    }


    public ApplyCouponPOJO1 setDefaultData(String code) {

        return this.withCode(code)
                .withAreaId(2)
                .withCityId(1)
                .withSwiggyTradeDiscount(0.0)
                .withRestaurantTradeDiscount(21.0)
                .withItemKey(null)
                .withMenuItemId(5282696)
                .withQuantity(1)
                .withRestaurantId("28157")
                .withUserId("5660904")
                .withSwiggyMoney(0.0)
                .withFirstOrder(false)
                .withCartPrice_Quantity(2)
                .withCartTotal(189.0)
                .withItemLevel("qwerty")
                .withItemLevelPriceQuantity(2)
                .withItemLevelPriceSubTotal(2.0)
                .withPreferredPaymentMethod("Juspay")
                .withReferralPresence(false)
                .withSwiggyMoneyApplicable(true)
                .withSuperUser(true)
                .withTypeOfPartner(null)
                .withVersionCode(245)
                .withUserAgent("ANDROID")
                .withPayment_codes(null)
                .withTime(null);

    }



}
