package com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3
 **/

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "groupId",
        "itemId",
        "itemKey",
        "mealCount",
        "mealId",
        "mealKey",
        "price",
        "restaurantId"
})
public class MealItemRequest {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("groupId")
    private Integer groupId;
    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("itemKey")
    private String itemKey;
    @JsonProperty("mealCount")
    private Integer mealCount;
    @JsonProperty("mealId")
    private Integer mealId;
    @JsonProperty("mealKey")
    private String mealKey;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("restaurantId")
    private Integer restaurantId;

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("groupId")
    public Integer getGroupId() {
        return groupId;
    }

    @JsonProperty("groupId")
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("itemKey")
    public String getItemKey() {
        return itemKey;
    }

    @JsonProperty("itemKey")
    public void setItemKey(String itemKey) {
        this.itemKey = itemKey;
    }

    @JsonProperty("mealCount")
    public Integer getMealCount() {
        return mealCount;
    }

    @JsonProperty("mealCount")
    public void setMealCount(Integer mealCount) {
        this.mealCount = mealCount;
    }

    @JsonProperty("mealId")
    public Integer getMealId() {
        return mealId;
    }

    @JsonProperty("mealId")
    public void setMealId(Integer mealId) {
        this.mealId = mealId;
    }

    @JsonProperty("mealKey")
    public String getMealKey() {
        return mealKey;
    }

    @JsonProperty("mealKey")
    public void setMealKey(String mealKey) {
        this.mealKey = mealKey;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonProperty("restaurantId")
    public Integer getRestaurantId() {
        return restaurantId;
    }

    @JsonProperty("restaurantId")
    public void setRestaurantId(Integer restaurantId) {
        this.restaurantId = restaurantId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("count", count).append("groupId", groupId).append("itemId", itemId).append("itemKey", itemKey).append("mealCount", mealCount).append("mealId", mealId).append("mealKey", mealKey).append("price", price).append("restaurantId", restaurantId).toString();
    }

}