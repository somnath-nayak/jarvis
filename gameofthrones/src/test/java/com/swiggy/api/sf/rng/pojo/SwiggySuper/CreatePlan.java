package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import com.swiggy.api.sf.rng.helper.Utility;

import java.util.HashMap;

public class CreatePlan {	
	
	private String title="AUTOMATE_PLAN";
    private String name = "Super_Plan_NAME";
	private String description="TEST_PLAN_Automation_DESCRIPTION";
	private String logo_id="tdxctewyhd2gfd";
	private String tenure="1";
	private String cgst="9";
	private String sgst= "9.999";
	private String price="99";
	private String enabled ="true";
	private String renewal_offset_days = "27";
	private String created_by= "Super-Automation-Script";
	private String gstInclusive= "true";
	private String recommended= "false";
	private String total_available= "2000";
	private String expiry_offset_days= "5";
	private String tnc= "offer valid on... Payment types, test123";
	private String valid_from=Utility.getPresentTimeInMilliSeconds();
	private String valid_till=Utility.getFutureMonthInMillisecond(9);
	
	public CreatePlan() {}
	
	
	public CreatePlan(String validFromDate,String numOfMonthValidity,String planTenure,String planPrice, String renewalOffsetDays) {
  	if(validFromDate.equals("0")) {
			 this.valid_from= Utility.getPresentTimeInMilliSeconds();
		 }
		 else {
			this.valid_from= Utility.getPasttimeInMilliSeconds(Integer.parseInt(validFromDate));
		 }
  	
  		System.out.println("validFromDate " + validFromDate);
		 valid_till= Utility.getFutureMonthInMillisecond(Integer.parseInt(numOfMonthValidity));
		 tenure= planTenure;
		 price= planPrice;
		 renewal_offset_days = renewalOffsetDays;
	}
	
	public CreatePlan(String validFromDate,String numOfMonthValidity,String planTenure,String planPrice) {
	  	
				this.valid_from= validFromDate;
	  		System.out.println("validFromDate " + validFromDate);
			 valid_till= Utility.getFutureMonthInMillisecond(Integer.parseInt(numOfMonthValidity));
			 tenure= planTenure;
			 price= planPrice;
			
		}
	
	public CreatePlan(HashMap <String, String> createPlanData) {
	 title= createPlanData.get(Integer.toString(0));
	 description= createPlanData.get(Integer.toString(1));
	 name= createPlanData.get(Integer.toString(2));
	 logo_id= createPlanData.get(Integer.toString(3));
	 if(createPlanData.get(Integer.toString(4)).equals("0")) {
		 valid_from= Utility.getPresentTimeInMilliSeconds();
	 }
	 else {
		 valid_from= Utility.getPasttimeInMilliSeconds(Integer.parseInt(createPlanData.get(Integer.toString(4))));
	 }
	 valid_till= Utility.getFutureMonthInMillisecond(Integer.parseInt(createPlanData.get(Integer.toString(5))));
		
	tenure=createPlanData.get(Integer.toString(6));
	cgst=createPlanData.get(Integer.toString(7));
	sgst=createPlanData.get(Integer.toString(8));
	price=createPlanData.get(Integer.toString(9));
	enabled=createPlanData.get(Integer.toString(10));
	renewal_offset_days=createPlanData.get(Integer.toString(11));
	created_by=createPlanData.get(Integer.toString(12));
	gstInclusive=createPlanData.get(Integer.toString(13));
	recommended=createPlanData.get(Integer.toString(14));
	total_available=createPlanData.get(Integer.toString(15));
	expiry_offset_days=createPlanData.get(Integer.toString(16));
	tnc=createPlanData.get(Integer.toString(17));	
	}
	
	public String getTitle()
	{
		return this.title;
	}	
	
	public void setTitle(String title) {
		this.title=title;
	}
	
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description=description;
	}
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name=name;
	}
	public String getLogoId() {
		return this.logo_id;
	}
	
	public void set(String logo_id) {
		this.logo_id= logo_id;
	}
	
	public String getValidFromDate() {
		return this.valid_from;
	}
	public void setValidFromDate(String valid_from) {
		this.valid_from= valid_from;
	}
	public String getValidTillDate()
	{
		return this.valid_till;
	}	
	public void setValidTillDate(String valid_till) {
		this.valid_till= valid_till;
	}
	public String getTenure() {
		return this.tenure;
	}
	public void setTenure(String tenure )
	{
		this.tenure= tenure;
	}
	public String getCgst() {
		return this.cgst;
	}
	public void setCgst(String cgst) {
		this.cgst=cgst;
	}
	public String getSgst() {
		return this.sgst;
	}
	public void setSgst(String sgst) {
		this.sgst=sgst;
	}
	public String getPrice() {
		return this.price;
	}
	public void setPrice(String price) {
		this.price= price;
	}
	public String getEnabled() {
		return this.enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled= enabled;
	}
	public String getRenewalOffsetDays() {
		return this.renewal_offset_days;
	}
	public void setRenewalOffsetDays(String renewalOffsetDays) {
		this.renewal_offset_days= renewalOffsetDays;
	}
	public String getCreatedBy() {
		return this.created_by;
	}
	public void setCreatedBy(String createdBy) {
		this.created_by= createdBy;
	}
	public String getGstInclusive() {
		return this.gstInclusive;
	}
	public void setGstInclusive(String gstInclusive)
	{
		this.gstInclusive= gstInclusive;
	}
	public String setRecommended() {
		return this.recommended;
	}
	public void setRecommended(String recommended) {
		this.recommended= recommended;
	}
	public String getTotalAvailable() {
		return this.total_available;
	}
	public void setTotalAvailable(String totalAvailable) {
		this.total_available= totalAvailable;
	}
	public String getExpiryOffsetDays() {
		return this.expiry_offset_days;
	}
	public void setExpiryOffsetDays(String expiryOffsetDays) {
		this.expiry_offset_days= expiryOffsetDays;
	}
	public String getTnc() {
		return this.tnc;
	}
	public void setTnc(String tnc) {
		this.tnc= tnc;
	}
	
	@Override
	public String toString()
	{
		return "{" 
		              +"\"title\"" + ":" + "\"" +title + "\"" + ","
				     + "\"description\"" + ":" + "\"" +description +"\"" + ","
		              + "\"name\"" + ":" + "\"" + name + "\"" + ","
				     + "\"logo_id\"" + ":" + "\"" + logo_id + "\"" + ","
		              + "\"valid_from\"" + ":" +  valid_from  +","
				     +  "\"valid_till\"" + ":" +  valid_till  + ","
				     + "\"tenure\"" + ":" +  tenure  + ","
				     + "\"cgst\"" + ":" +  cgst  + ","
				     + "\"sgst\"" + ":" + sgst  + ","
				     + "\"price\"" + ":" +  price  + ","
				     + "\"enabled\"" + ":" + enabled  + ","
				     + "\"renewal_offset_days\"" + ":" + renewal_offset_days  + ","
				      + "\"created_by\"" + ":" + "\"" + created_by + "\"" + ","
				      + "\"gstInclusive\"" + ":" +  gstInclusive  +","
				      + "\"recommended\"" + ":" +  recommended  + ","
				      + "\"total_available\"" + ":" + total_available  + ","
				      + "\"expiry_offset_days\"" + ":" +  expiry_offset_days  + ","
				      + "\"tnc\"" + ": [" + "\"" + tnc + "\"" + "]"
				      +"}"
				
				;
	}
}

	