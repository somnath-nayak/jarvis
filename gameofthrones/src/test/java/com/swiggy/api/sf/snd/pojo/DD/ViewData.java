package com.swiggy.api.sf.snd.pojo.DD;

import org.apache.commons.lang.builder.ToStringBuilder;

public class ViewData {

    private String title;
    private String subTitle;
    private String mainPageDescription;
    private String minRestaurants;
    private String maxRestaurants;
    private String minItemsPerRestaurant;
    private String maxItemsPerRestaurant;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getMainPageDescription() {
        return mainPageDescription;
    }

    public void setMainPageDescription(String mainPageDescription) {
        this.mainPageDescription = mainPageDescription;
    }

    public String getMinRestaurants() {
        return minRestaurants;
    }

    public void setMinRestaurants(String minRestaurants) {
        this.minRestaurants = minRestaurants;
    }

    public String getMaxRestaurants() {
        return maxRestaurants;
    }

    public void setMaxRestaurants(String maxRestaurants) {
        this.maxRestaurants = maxRestaurants;
    }

    public String getMinItemsPerRestaurant() {
        return minItemsPerRestaurant;
    }

    public void setMinItemsPerRestaurant(String minItemsPerRestaurant) {
        this.minItemsPerRestaurant = minItemsPerRestaurant;
    }

    public String getMaxItemsPerRestaurant() {
        return maxItemsPerRestaurant;
    }

    public void setMaxItemsPerRestaurant(String maxItemsPerRestaurant) {
        this.maxItemsPerRestaurant = maxItemsPerRestaurant;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("title", title).append("subTitle", subTitle).append("mainPageDescription", mainPageDescription).append("minRestaurants", minRestaurants).append("maxRestaurants", maxRestaurants).append("minItemsPerRestaurant", minItemsPerRestaurant).append("maxItemsPerRestaurant", maxItemsPerRestaurant).toString();
    }

}
