package com.swiggy.api.sf.checkout.helper;

import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import java.util.*;

public class CheckoutMockHelper {

	Initialize gameofthrones =Initializer.getInitializer();
	
	public Processor invokeAPI(String serviceName,String apiName,HashMap<String, String> requestheaders,
    		String[] payloadParam, String[] urlParam){
        GameOfThronesService service = new GameOfThronesService(serviceName,apiName, gameofthrones);
        Processor processor = new Processor(service, requestheaders, payloadParam, urlParam);
        return processor ;
    }
	
	public Processor wiremockMappingNew(String[] payload){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
        GameOfThronesService service = new GameOfThronesService("checkout1", "WiremockCreate", gameofthrones);
        Processor processor = new Processor(service, requestheaders, payload, null);
        return processor;
    }
	
	public String[] createPaytmWithdrawMockRes(String urlPattern, String method, String statusCode, String resBody) {
		String[] payloadparams = new String[4];
		payloadparams[0] = urlPattern;
		payloadparams[1] = method;
		payloadparams[2] = statusCode;
		payloadparams[3] = resBody;
		return payloadparams;
	}
	
	public HashMap<String, String> requestHeader(){
		HashMap<String, String> requestheaders = new HashMap<String, String>();
		requestheaders.put("content-type", "application/json");
		return requestheaders;
	}
}