package com.swiggy.api.sf.snd.pojo.event;

public class EventBuilder {
   private Event event;
    public EventBuilder(){
        event = new Event();
    }

    public EventBuilder eventType(String type) {
        event.setEventType(type);
        return this;
    }

    public EventBuilder data(Data data ) {
        event.setData(data);
        return this;
    }

    public EventBuilder meta(Object meta) {
        event.setMeta(meta);
        return this;
    }

    public EventBuilder id(Integer id) {
        event.setId(id);
        return this;
    }

    public EventBuilder timeStamp(Integer timeStamp) {
        event.setEventTimestamp(timeStamp);
        return this;
    }
    public EventBuilder version(String version) {
        event.setVersion(version);
        return this;
    }
    public EventBuilder opType(String opType) {
        event.setOpType(opType);
        return this;
    }



    public Event build() {
        return event;
    }



}
