package com.swiggy.api.sf.rng.tests;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.testng.annotations.DataProvider;

import com.swiggy.api.sf.rng.pojo.Category;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateFlatTdEntry;
import com.swiggy.api.sf.rng.pojo.CreateTdBuilder;
import com.swiggy.api.sf.rng.pojo.CreateTdEntry;
import com.swiggy.api.sf.rng.pojo.Menu;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.SubCategory;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTDEntry;
import com.swiggy.api.sf.rng.pojo.freebie.CreateFreebieTdBuilder;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieRestaurantList;
import com.swiggy.api.sf.rng.pojo.freebie.FreebieSlot;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryEntry;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryTdBuilder;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliverySlot;

import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;

public class CreateFreedeliveryTDWithRestrictions {
	
	getObject go = new getObject();
	static 	int i =0;
	
	public String getFreeDevliveryTD(String restrictionType,String level) throws IOException
	{
		i++;
	    List<FreeDeliveryRestaurantList> restaurantList = new ArrayList<>();
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFreeDeliveryEntry freeDeliveryDiscount =null ;
	    switch(level)
		{
		case "Restaurant"  : restaurantList.add(go.getFreedeliveryRestaurantObject());break;
		}
		
		
	
	{
		switch(restrictionType)
		{
		
		    case "SFO":  freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
					          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
					          .enabled(true)
				              .restaurantList(restaurantList)
					          .ruleDiscount("FREE_DELIVERY",level, "0")
				              .firstOrderRestriction(true)
					          .build();break;

		    case "RFO": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          //.slots(slots)
		              .ruleDiscount("FREE_DELIVERY",level, "0")
		              .restaurantFirstOrder(true)
			          .build();break;


		    case "NR": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          .ruleDiscount("FREE_DELIVERY",level, "0")
		              .build();break;


		    case "UR": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          .ruleDiscount("FREE_DELIVERY",level, "0").userRestriction(true)
		              .build();break;
					
		    case "DU": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          .ruleDiscount("FREE_DELIVERY",level, "0").dormant_user_type("THIRTY_DAYS_DORMANT")
		              .build();break;
		              
		              
		    case "TR": List<FreeDeliverySlot> slots = new ArrayList<>();
                       slots.add(new FreeDeliverySlot(0005,2350,"ALL"));
		    	       freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true).restaurantFirstOrder(false).timeSlotRestriction(true).slots(slots)
		              .restaurantList(restaurantList)
			          .ruleDiscount("FREE_DELIVERY",level, "0").dormant_user_type("ZERO_DAYS_DORMANT")
		              .build();break;          
		         

		}     
		      return   jsonHelper.getObjectToJSON(freeDeliveryDiscount);
	}
	}

	
	public String getFreeDevliveryTD(String restrictionType,String level, List<FreeDeliveryRestaurantList> restaurantList) throws IOException
	{
		i++;
	   
	    JsonHelper jsonHelper = new JsonHelper();
	    CreateFreeDeliveryEntry freeDeliveryDiscount =null ;
	    
	{
		switch(restrictionType)
		{
		
		    case "SFO":  freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
					          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
					          .enabled(true)
				              .restaurantList(restaurantList)
					          //.slots(slots)
				              .ruleDiscount("FREE_DELIVERY",level, "0")
				              .firstOrderRestriction(true)
					          .build();break;

		    case "RFO": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          //.slots(slots)
		              .ruleDiscount("FREE_DELIVERY",level, "0")
		              .restaurantFirstOrder(true)
			          .build();break;


		    case "NR": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          //.slots(slots)
		              .ruleDiscount("FREE_DELIVERY",level, "0")
		              .build();break;


		    case "UR": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          //.slots(slots)
		              .ruleDiscount("FREE_DELIVERY",level, "0").userRestriction(true)
		              .build();break;
					
		    case "DU": freeDeliveryDiscount = new CreateFreeDeliveryTdBuilder()
			          .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
			          .valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
			          .enabled(true)
		              .restaurantList(restaurantList)
			          //.slots(slots)
		              .ruleDiscount("FREE_DELIVERY",level, "0").dormant_user_type("THIRTY_DAYS_DORMANT")
		              .build();break;

		}     
		      return   jsonHelper.getObjectToJSON(freeDeliveryDiscount);
	}
	}

}
