package com.swiggy.api.sf.checkout.helper.sanityHelper;

import com.swiggy.api.sf.checkout.constants.EditTimeConstants;
import com.swiggy.api.sf.checkout.pojo.OrderEditPOJO;
import com.swiggy.api.sf.checkout.pojo.OrderPlace;
import com.swiggy.api.sf.checkout.tests.EditOrderV2;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import org.codehaus.jackson.map.ObjectMapper;
import java.util.HashMap;

public class EditOrderHelper   {
    Processor processor = null;
    static Initialize gameofthrones = new Initialize();
    OrderPlace OP = new OrderPlace();
    OrderEditPOJO editOrderpojo = new OrderEditPOJO();
    ObjectMapper mp=new ObjectMapper();

    public  HashMap Menu_Headers(){
        HashMap<String, String> requestHeaders = new HashMap<>();
        requestHeaders.put("Content-Type", "application/json");
        return  requestHeaders;

    }

    public HashMap<String, String> createReqHeader(String tid, String token){
        HashMap<String, String> requestheaders = new HashMap<String, String>();
        requestheaders.put("content-type", "application/json");
        requestheaders.put("Tid", tid);
        requestheaders.put("token", token);
        requestheaders.put("version-code", EditTimeConstants.VERSION_CODE);
        requestheaders.put("User-Agent", EditTimeConstants.USER_AGENT);
        requestheaders.put("Authorization", EditTimeConstants.AUTHORIZATION);
        return requestheaders;
    }

    public Processor Menu()
    {
        String[] queryParam = {EditTimeConstants.REST_ID,EditTimeConstants.lat,EditTimeConstants.lng};
        GameOfThronesService service1 = new GameOfThronesService("checkout", "menuv4withoutlatlng", gameofthrones);
        processor = new Processor(service1, Menu_Headers(),null,queryParam);
        return processor;
    }

    public Processor orderPlace(String Tid,String Token,Integer addressId, String payment, String comments) throws Exception
    {
        OP.setData(addressId,comments,payment);
        HashMap<String, String> requestheaders=createReqHeader(Tid, Token);
        GameOfThronesService service = new GameOfThronesService("checkout", "orderplacev1", gameofthrones);
        Processor processor = new Processor(service, requestheaders, new String[]{mp.writeValueAsString(OP)});
        return processor;
        }

    public Processor orderEdit(String Tid,String Token,Integer restId,Integer quantity,String Menu_Item_Id,Long Order_Id, Integer Initiation_source) throws Exception
    {
        editOrderpojo.setData(restId,quantity,  Menu_Item_Id, Order_Id , Initiation_source);
        HashMap<String, String> requestheaders=createReqHeader(Tid, Token);
        GameOfThronesService service = new GameOfThronesService("checkout", "orderEditv2", gameofthrones);
        Processor processor = new Processor(service, requestheaders, new String[]{mp.writeValueAsString(editOrderpojo)});
        return processor;
    }

    public Processor orderConfirm(String Tid,String Token,Integer restId,Integer quantity,String Menu_Item_Id,Long Order_Id,Integer Initiation_source) throws Exception
    {
        editOrderpojo.setData(restId,quantity,Menu_Item_Id,Order_Id,Initiation_source);
        HashMap<String, String> requestheaders=createReqHeader(Tid, Token);
        GameOfThronesService service = new GameOfThronesService("checkout", "editorderconfirm1", gameofthrones);
        // String []payloadparams = {Integer.valueOf(addressId),payment,comments};
        Processor processor = new Processor(service, requestheaders, new String[]{mp.writeValueAsString(editOrderpojo)});
        return processor;
    }

}

