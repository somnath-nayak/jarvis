package com.swiggy.api.sf.rng.helper;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.opencsv.CSVWriter;
import com.swiggy.api.erp.cms.constants.CmsConstants;
import com.swiggy.api.erp.cms.helper.BaseServiceHelper;
import com.swiggy.api.erp.cms.helper.CMSHelper;
import com.swiggy.api.erp.cms.helper.CriticalAPI_Helper;
import com.swiggy.api.erp.delivery.constants.DeliveryConstant;
import com.swiggy.api.erp.delivery.helper.*;
import com.swiggy.api.sf.checkout.helper.CheckoutHelper;
import com.swiggy.api.sf.checkout.helper.CommonAPIHelper;
import com.swiggy.api.sf.checkout.pojo.Cart;
import com.swiggy.api.sf.checkout.pojo.CreateMenuEntry;
import com.swiggy.api.sf.checkout.pojo.CreateOrderBuilder;
import com.swiggy.api.sf.rng.constants.CouponConstants;
import com.swiggy.api.sf.rng.constants.MultiTDConstants;
import com.swiggy.api.sf.rng.constants.RngConstants;
import com.swiggy.api.sf.rng.pojo.*;
import com.swiggy.api.sf.rng.pojo.MenuMerch.CreateFlatTDItemLevelPOJO;
import com.swiggy.api.sf.rng.pojo.MultiTD.CreateSuperCampaignV2.RemoveFreebie;
import com.swiggy.api.sf.rng.pojo.RestaurantList;
import com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.EvaluateCartV3;
import com.swiggy.api.sf.rng.pojo.ads.FetchChargesPOJO;
import com.swiggy.api.sf.rng.pojo.ads.PrioritySlotsPOJO;
import com.swiggy.api.sf.rng.pojo.carousel.CarouselPOJO;
import com.swiggy.api.sf.rng.pojo.couponcartevaluate.OrderEditCouponEvaluatePOJO;
import com.swiggy.api.sf.rng.pojo.edvo.EDVOCartMealItemRequest;
import com.swiggy.api.sf.rng.pojo.freebie.RuleDiscount;
import com.swiggy.api.sf.rng.pojo.freebie.*;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryEntry;
import com.swiggy.api.sf.rng.pojo.freedelivery.CreateFreeDeliveryTdBuilder;
import com.swiggy.api.sf.rng.pojo.freedelivery.FreeDeliveryRestaurantList;
import com.swiggy.api.sf.rng.pojo.growthPack.CreateTypePOJO;
import com.swiggy.api.sf.rng.pojo.growthPack.TypeMetaPOJO;
import com.swiggy.api.sf.rng.pojo.growthPack.UpdateTypePOJO;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Aegon.SystemConfigProvider;
import framework.gameofthrones.Cersei.ToolBox;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.JonSnow.SchemaValidatorUtils;
import framework.gameofthrones.Tyrion.JsonHelper;
import framework.gameofthrones.Tyrion.RedisHelper;
import framework.gameofthrones.Tyrion.SqlTemplate;
import net.minidev.json.JSONArray;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.EmptyResultDataAccessException;
import org.testng.Assert;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.*;
import java.util.concurrent.TimeUnit;


//import com.swiggy.api.sf.rng.dp.DP;

public class RngHelper {

	static Initialize gameofthrones = new Initialize();
	static HashMap<String, String> requestHeaders = new HashMap<String, String>();
	static GameOfThronesService service;
	static Processor processor;
	static SchemaValidatorUtils schemaValidate = new SchemaValidatorUtils();
	CheckoutHelper checkoutHelper = new CheckoutHelper();
	CommonAPIHelper commonAPIHelper = new CommonAPIHelper();
	RngConstants rngConstants;
	CMSHelper cmsHelper = new CMSHelper();
	EndToEndHelp endToEndHelp = new EndToEndHelp();
	DeliveryServiceHelper deliveryServiceHelper = new DeliveryServiceHelper();
	RedisHelper redisHelper = new RedisHelper();
	JsonHelper jsonHelper = new JsonHelper();
	RandomNumber rm = new RandomNumber(2000000, 3000000);
	//SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
	DeliveryHelperMethods dhm = new DeliveryHelperMethods();
	DeliveryDataHelper ddh = new DeliveryDataHelper();
	SANDHelper snd = new SANDHelper();
	int mealNum = 0;
	int groupNum = 0;

	// public static Processor itemAvailibility(String restId) {
	// HashMap<String, String> requestHeaders = new HashMap<String, String>();
	// requestHeaders.put("Content-Type", "application/json");
	// GameOfThronesService service = new GameOfThronesService("tdv1",
	// "itemavailability", gameofthrones);
	// String[] urlParams = new String[] { restId };
	// Processor processor = new Processor(service, requestHeaders, null,
	// urlParams);
	// int statusCode = processor.ResponseValidator.GetResponseCode();
	//
	// return processor;
	// }

	public static String checkMultiApplyCoupon(String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectMultiApplyCoupon + code + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "code is not persent in DB";
	}

	public static String checkCouponAvailabilityCouponSlot(String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectDataFromCouponsSlots + code + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "code is not present in DB";
	}

	public static String checkCouponAvailabilityCouponSlotAud(String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectDataFromCouponsSlotsAud + code + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "code is not present in DB";
	}

	public static String checkCouponTimeSlotAvailability(String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectDataFromCouponsSlotsAud + code + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("close_time").toString()) : "code is not present in DB";
	}



	public static String getMultiApplyCouponId(String couponId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectMultiApplyCouponCode + couponId + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "code is not persent in DB";
	}

	public static void insertCouponUsedCount(String couponId, String user_id, String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		sqlTemplate.execute(RngConstants.insertMultiApplyCouponUsageCount + couponId + RngConstants.commaSeparation
				+ user_id + RngConstants.commaSeparation + code + RngConstants.endPart);

		return;
	}

	public static String getMultiApplyCouponUsageCount(String couponId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectMultiApplyCouponCode + couponId + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("usage_count").toString()) : "code is not persent in DB";
	}

	public static Processor disableEnableTD(String tdId, String isenabled) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "edabletd", gameofthrones);
		String[] payload = new String[] { tdId, isenabled };
		Processor processor = new Processor(service, requestHeaders, payload);
		return processor;
	}

	public static Processor disableEnableTDFromMarketingDashboard(String tdId, String isenabled) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("marketing", "edabletd", gameofthrones);
		String[] payload = new String[] { tdId, isenabled };
		Processor processor = new Processor(service, requestHeaders, payload);
		return processor;
	}

	public void userMappingTD(String userID, String tdId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
//		sqlTemplate.update(RngConstants.tD_User + tdId + "," + userID + "," + "'2018-07-23 19:29:00'" + ","
//				+ "'TD_AUTOMATION_USER'" + "," + "1" + RngConstants.endPart);
		sqlTemplate.update(RngConstants.tD_User + tdId + "," + userID + "," + "'"+getCurrentDate("yyyy-MM-dd HH:mm:ss")+"'"+ ","
				+ "'TD_AUTOMATION_USER'" + "," + "1" + RngConstants.endPart);
		

    }

   	public static Processor SnditemAvailibility(String itemId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");

		GameOfThronesService service = new GameOfThronesService("avaliablity", "itemavailability", gameofthrones);

		String[] payloadParams = new String[] { itemId };
		Processor processor = new Processor(service, requestHeaders, payloadParams);
		int statusCode = processor.ResponseValidator.GetResponseCode();

		return processor;
	}

	public static Processor getTradeDiscount(String tID) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "getTDv1", gameofthrones);
		String[] urlParams = new String[] { tID };
		Processor processor = new Processor(service, requestHeaders, null, urlParams);
		return processor;
	}

	public static Processor createTradeDiscount(String namespace, String header, String description,
												String shortDescription, String valid_from, String valid_till, String campaign_type, String dormantUserType,
												String restaurant_hit, String swiggy_hit, String discountLevel, String rId, String Discounttype,
												String minCartAmount, String itemId, String enabled, String createdBy, String updatedBy,
												String firstOrderRestriction, String timeSlotRestriction, String slots, String commissionOnFullBill,
												String taxesOnDiscountedBill, String userRestriction, String restaurantFirstOrder) {
		requestHeaders.put("Content-Type", "application/json");
		service = new GameOfThronesService("td", "createtdv1", gameofthrones);
		String[] payloadparams = new String[] { namespace, header, description, shortDescription, valid_from,
				valid_till, campaign_type, dormantUserType, restaurant_hit, swiggy_hit, discountLevel, rId,
				Discounttype, minCartAmount, itemId, enabled, createdBy, updatedBy, firstOrderRestriction,
				timeSlotRestriction, slots, commissionOnFullBill, taxesOnDiscountedBill, userRestriction,
				restaurantFirstOrder };
		processor = new Processor(service, requestHeaders, payloadparams);
		return processor;

	}

	public static Processor login(String mobile, String password) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");

		GameOfThronesService service = new GameOfThronesService("login", "loginv2", gameofthrones);

		String[] payloadparams = new String[] { mobile, password };
		processor = new Processor(service, requestHeaders, payloadparams);
		return processor;

	}

	/*
	 * TO-DO This method returns list of Restaurant Ids which are having Trade
	 * Discount
	 */
	public List<String> getRestaurantList() {

		List<String> list = Arrays.asList("281", "1725", "1889", "217", "289", "319", "1262");
		return list;
	}

	public static HashMap<String, String> getDefaultHeaders() {
		HashMap<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/json");
		return header;
	}

	// public static boolean validateStatusInResponse(Processor processor) {
	// boolean valid = false;
	// valid = (processor.ResponseValidator.GetNodeValueAsInt("statusCode") ==
	// RngConstants.statuscode) ? true : false;
	// valid =
	// (processor.ResponseValidator.GetNodeValue("statusMessage").equalsIgnoreCase(RngConstants.statusmessage))
	// ? true
	// : false;
	// return valid;
	// }

	public static Processor tdByRestId(String restIds) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders();
		GameOfThronesService service = new GameOfThronesService("td", "tdbyrestid", gameofthrones);
		String[] payloadparams = new String[] { restIds };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor tdServeMenu(String restId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders();
		GameOfThronesService service = new GameOfThronesService("td", "getservemenu", gameofthrones);
		String[] urlparams = new String[] { restId };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static boolean validateSchema(Processor processor, String schemaPath) {
		boolean valid = false;
		try {
			String jsonSchema = new ToolBox().readFileAsString(System.getProperty("user.dir") + schemaPath);
			List missingNodeList = schemaValidate.validateServiceSchema(jsonSchema,
					processor.ResponseValidator.GetBodyAsText());
			valid = missingNodeList.isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return valid;
	}

	public List getRestaurantListing(String lat, String lng, String page) {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		String listingResponse = processor.ResponseValidator.GetBodyAsText();
		String pages = JsonPath.read(listingResponse, "$.data.pages").toString();
		String tid = JsonPath.read(listingResponse, "$.tid").toString();
		requestHeaders.put("tid", tid);

		List rids = new ArrayList<>();
		/* ArrayList restId = new ArrayList(); */
		/* CommonAPIHelper common = new CommonAPIHelper(); */
		for (int i = 0; i <= Integer.parseInt(page); i++) {

			urlparams[0] = lat;
			urlparams[1] = lng;
			urlparams[2] = String.valueOf(i);
			processor = new Processor(service, requestHeaders, null, urlparams);

			String response = processor.ResponseValidator.GetBodyAsText();
			rids.addAll(JsonPath.read(response, "$.data..id"));
			/*
			 * if (rids.size() > 0) { break; }
			 */
		}

		return rids;

	}

	public Processor createTD(String payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders();
		GameOfThronesService service = new GameOfThronesService("td", "createtd", gameofthrones);
		String[] payloadparams = new String[] { payload };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor getAllTdAppliedRestaurants() {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders();
		GameOfThronesService service = new GameOfThronesService("td", "getAllTDRestaurants", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, null);
		return processor;

	}

	public Processor listEvalulate(String restaurantIds, String firstOrder, String userId, String userAgent,
								   String versionCode) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "listevaluatev3", gameofthrones);
		String[] payloadparams = new String[] { restaurantIds, firstOrder, userId, userAgent, versionCode };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor listEvalulateV3(String restaurantIds, String firstOrder, String userId, String userAgent,
								   String versionCode) {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "listevaluatev3", gameofthrones);
		String[] payloadparams = new String[] { restaurantIds, firstOrder, userId, userAgent, versionCode };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

//        SuperMultiTDHelper multiTDHelper = new SuperMultiTDHelper();
//        List<Integer> restid= new ArrayList<Integer>();
//        restid.add(Integer.parseInt(restaurantIds));
//        EvaluateListingV3 evaluateListingV3 = new EvaluateListingV3();
//		evaluateListingV3.setRestaurantIds(restid);
//		evaluateListingV3.setFirstOrder(Boolean.parseBoolean(firstOrder));
//		evaluateListingV3.setUserId(Integer.parseInt(userId));
//		evaluateListingV3.setUserAgent(userAgent);
//		evaluateListingV3.setVersionCode(Integer.parseInt(versionCode));
//        //evaluateListingV3.build(Integer.parseInt(userId),false,restid);
//
//        try {
//            return multiTDHelper.evaluateListingHelper(jsonHelper.getObjectToJSON(evaluateListingV3));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;
//	}


	public Processor freedeliverylistevaluate(String restaurantIds, String firstOrder, String userId, String userAgent,
			String versionCode, String rainFee, String specialFee, String threshold, String distance, String time,
			String totalfee) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "freedeliverylistevaluate", gameofthrones);
		String[] payloadparams = new String[] { restaurantIds, firstOrder, userId, userAgent, versionCode, rainFee,
				specialFee, threshold, distance, time, totalfee };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor freedeliverymenuevaluatev3(String minCartAmount, String restaurantIds, String firstOrder,
			String userId, String userAgent, String versionCode, String rainFee, String specialFee, String threshold,
			String distance, String time, String totalfee) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "freedeliverymenuevaluatev3", gameofthrones);
		String[] payloadparams = new String[] { minCartAmount, restaurantIds, firstOrder, userId, userAgent,
				versionCode, rainFee, specialFee, threshold, distance, time, totalfee };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor freedeliverycartevaluatev3(String restaurantID, String categoryId, String subCategoryId, String itemId, String count, String price, String CartAmount, String userId, String firstOrder,String userAgent, String versionCode, String rainFee, String specialFee,String threshold,String distance,String time,String totalfee) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "freedeliverycartevaluatev3", gameofthrones);
		String[] payloadparams = new String[] { restaurantID, categoryId, subCategoryId, itemId, count, price,
				CartAmount, userId, firstOrder, userAgent, versionCode, rainFee, specialFee, threshold, distance, time,
				totalfee };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor menuEvaluate(String minCartAmount, String restaurantIds, String firstOrder, String userId,
								  String userAgent, String versionCode) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		/*
		 * requestHeaders.put("tid",checkoutHelper.TokenData(rngConstants.mobile1,
		 * rngConstants.password1).get("Tid").toString());
		 */GameOfThronesService service = new GameOfThronesService("td", "menuevaluatev2", gameofthrones);
		String[] payloadparams = new String[] { minCartAmount, restaurantIds, firstOrder, userId, userAgent,
				versionCode };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}
	public Processor menuEvaluateV3(String minCartAmount, String restaurantIds, String firstOrder, String userId,
								  String userAgent, String versionCode) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		/*
		 * requestHeaders.put("tid",checkoutHelper.TokenData(rngConstants.mobile1,
		 * rngConstants.password1).get("Tid").toString());
		 */GameOfThronesService service = new GameOfThronesService("td", "menuevaluatev3", gameofthrones);
		String[] payloadparams = new String[] { minCartAmount, restaurantIds, firstOrder, userId, userAgent,
				versionCode };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor cartEvaluate(String restaurantID, String categoryId, String subCategoryId, String itemId,
								  String count, String price, String CartAmount, String userId, String firstOrder, String userAgent,
								  String versionCode) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		requestHeaders.put("tid", "9f11a216-7e0d-45fa-a2d3-01577c5c4174");// checkoutHelper.TokenData(rngConstants.mobile1,
		// rngConstants.password1).get("Tid").toString()
		GameOfThronesService service = new GameOfThronesService("td", "cartevaluatev2", gameofthrones);
		String[] payloadparams = new String[] { restaurantID, categoryId, subCategoryId, itemId, count, price,
				CartAmount, userId, firstOrder, userAgent, versionCode };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public Processor cartEvaluatev3(String restaurantID, String categoryId, String subCategoryId, String itemId,
								  String count, String price, String CartAmount, String userId, String firstOrder, String userAgent,
								  String versionCode) {
		EvaluateCartV3 evaluateCartV3 = new EvaluateCartV3();
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		List<Integer> restID = new ArrayList<Integer>();
		restID.add(Integer.parseInt(restaurantID));
		evaluateCartV3.setUserId(Integer.parseInt(userId));
		evaluateCartV3.setFirstOrder(Boolean.parseBoolean(firstOrder));
		evaluateCartV3.setUserAgent(userAgent);
		evaluateCartV3.setMinCartAmount(Integer.parseInt(CartAmount));
		evaluateCartV3.setVersionCode(Integer.parseInt(versionCode));

		com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.ItemRequest itemRequest = new com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.ItemRequest();
		itemRequest.build(restID.get(0),Integer.parseInt(categoryId),Integer.parseInt(subCategoryId),Integer.parseInt(itemId));
		itemRequest.setPrice(Integer.parseInt(price));
		itemRequest.setCount(Integer.parseInt(count));
		List<com.swiggy.api.sf.rng.pojo.MultiTD.EvaluateCartV3.ItemRequest> list = new ArrayList<>();
		list.add(itemRequest);
		evaluateCartV3.setItemRequests(list);
		Processor processor = null;
		try {
			processor = mhelper.evaluateCartHelper(jsonHelper.getObjectToJSON(evaluateCartV3));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor cartEvaluateV2(String restaurantID, String categoryId, String subCategoryId, String itemId,
								  String count, String price, String CartAmount, String userId, String firstOrder, String userAgent,
								  String versionCode,String mincartAmt,String restBillAmt) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		requestHeaders.put("tid", "9f11a216-7e0d-45fa-a2d3-01577c5c4174");// checkoutHelper.TokenData(rngConstants.mobile1,
		// rngConstants.password1).get("Tid").toString()
		GameOfThronesService service = new GameOfThronesService("td", "cartevaluatev2", gameofthrones);
		String[] payloadparams = new String[] { restaurantID, categoryId, subCategoryId, itemId, count, price,
				CartAmount, userId, firstOrder, userAgent, versionCode,mincartAmt,restBillAmt };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public List<Category> getCategoryObject(String restId, int noOfCategory) {

		List<Category> categories = new ArrayList<>();
		for (int i = 0; i < noOfCategory; i++) {
			// String catId= iterator.next().get("cat_id").toString();
			// categories.add(new Category(catId, "Main Course", new ArrayList<>()));
		}
		return categories;
	}

	public List<RestaurantList> getRestaurantListObject(String restId, List<Map<String, Object>> al, int noOfCategory) {

		List<RestaurantList> restaurantLists = new ArrayList<>();
		List<Category> categories = new ArrayList<>();
		Iterator<Map<String, Object>> it = al.iterator();
		while (it.hasNext()) {
			Map<String, Object> obj = it.next();
			System.out.println(obj.get("rest_id").toString());
			if (restId.equals(obj.get("rest_id").toString())) {
				System.out.println(obj.get("cat_id").toString() + "=====" + obj.get("category_name").toString());
				categories.add(new Category(obj.get("cat_id").toString(), obj.get("category_name").toString(),
						new ArrayList<>()));
				restaurantLists.add(new RestaurantList(obj.get("rest_id").toString(), "Test", categories));
				break;
			}
		}
		return restaurantLists;
	}

	public List<RestaurantList> getRestaurantList(String restId, List<Category> categories) {
		List<RestaurantList> restaurantLists = new ArrayList<>();
		restaurantLists.add(new RestaurantList(restId, "Test", categories));
		return restaurantLists;
	}

	public List<Category> getCategory(List<Map<String, Object>> map, List<SubCategory> subCategories) {
		List<Category> categories = new ArrayList<>();
		Iterator<Map<String, Object>> it = map.iterator();
		while (it.hasNext()) {
			Map<String, Object> val = it.next();
			categories.add(new Category(val.get(RngConstants.catid).toString(),
					val.get(RngConstants.catname).toString(), subCategories));
		}
		return categories;
	}

	public List<SubCategory> getSubCategory(List<Map<String, Object>> map, List<Menu> menulist) {
		List<SubCategory> subcategories = new ArrayList<>();
		Iterator<Map<String, Object>> it = map.iterator();
		while (it.hasNext()) {
			Map<String, Object> val = it.next();
			subcategories.add(new SubCategory(val.get(RngConstants.subcatid).toString(),
					val.get(RngConstants.subcatname).toString(), menulist));
		}
		return subcategories;
	}

	public List<Menu> getMenu(List<Map<String, Object>> map) {
		List<Menu> menu = new ArrayList<>();
		Iterator<Map<String, Object>> it = map.iterator();
		while (it.hasNext()) {
			Map<String, Object> val = it.next();
			menu.add(new Menu(val.get(RngConstants.itemid).toString(), val.get(RngConstants.itemname).toString()));
		}
		return menu;
	}

	public List<Map<String, Object>> getCategoryRestaurants(List rlist2) {
		CMSHelper CMSHelper = new CMSHelper();
		List<Map<String, Object>> al = new ArrayList<>();
		Iterator iterator = rlist2.iterator();
		while (iterator.hasNext()) {
			String resId = iterator.next().toString();
			List<Map<String, Object>> subList = CMSHelper.getItemDetails(resId.toString());
			if (subList.size() > 0) {
				al.addAll(subList);
			}
		}
		return al;
	}

	public HashSet<String> getUniqueRestaurants(List allList) {
		HashSet<String> allRest = new HashSet<>();
		ListIterator<Map<String, Object>> it = allList.listIterator();
		while (it.hasNext()) {
			allRest.add((it.next().get("rest_id").toString()));
		}
		return allRest;
	}

	public static List<String> getList(List<Map<String, Object>> map) {
		List<String> list = new ArrayList<>();
		for (Map<String, Object> m : map) {
			Iterator<String> it = m.keySet().iterator();
			while (it.hasNext()) {
				String key = it.next();
				list.add(m.get(key).toString());
			}
		}
		return list;
	}

	public List<Map<String, Object>> getCategoryForRestId(String rest_id) {
		CMSHelper cmshelper = new CMSHelper();
		return cmshelper.getCategories(rest_id);
	}

	public List<Map<String, Object>> getSubCategoryForRestId(String rest_id, String category_id) {
		CMSHelper cmshelper = new CMSHelper();
		return cmshelper.getSubcategory(rest_id, category_id);
	}

	public List<Map<String, Object>> getItemsForRestid(String rest_id, String category_id, String subcategory_id) {
		CMSHelper cmshelper = new CMSHelper();
		return cmshelper.getItem(rest_id, category_id, subcategory_id);
	}

	public List<RestaurantList> createRandomRestaurantList(String rest_id, boolean includecat, int cat_quanity,
														   boolean includesubcat, int subcat_quantity, boolean includeitem, int item_quantity) {
		List<RestaurantList> restaurantlist = new ArrayList<>();
		List<Category> categorieslist = new ArrayList<>();
		try {
			List<String> cat = new ArrayList<>();
			List<String> subcat = new ArrayList<>();
			List<Map<String, Object>> itemmap = new ArrayList<>();
			List<Map<String, Object>> subcatmap = new ArrayList<>();
			List<Map<String, Object>> catmap = new ArrayList<>();
			List<SubCategory> subcatlist = new ArrayList<>();
			List<Menu> menulist = new ArrayList<>();
			int ran = 0;
			Map<String, Object> mm, mm2;

			if (includecat && includesubcat && includeitem && cat_quanity >= 1 && subcat_quantity >= 1
					&& item_quantity >= 1) {
				List<Map<String, Object>> ll = getCategoryForRestId(rest_id);
				for (int i = 0; i < cat_quanity && i < ll.size(); i++) {
					List<SubCategory> sublist = new ArrayList<>();
					mm = ll.get(i);
					String cat_id = mm.get(RngConstants.catid).toString();
					List<Map<String, Object>> ll2 = getSubCategoryForRestId(rest_id, cat_id);
					for (int j = 0; j < subcat_quantity && j < ll2.size(); j++) {
						mm2 = ll2.get(j);
						String subcat_id = mm2.get(RngConstants.subcatid).toString();
						List<Map<String, Object>> ll3 = getItemsForRestid(rest_id, cat_id, subcat_id);
						List<Menu> menu = getMenu(new ArrayList(
								ll3.subList(0, (item_quantity < ll3.size()) ? item_quantity : ll3.size())));
						sublist.add(new SubCategory(mm2.get(RngConstants.subcatid).toString(),
								mm2.get(RngConstants.subcatname).toString(), menu));
					}
					categorieslist.add(new Category(mm.get(RngConstants.catid).toString(),
							mm.get(RngConstants.catname).toString(), sublist));
				}
			}

			else if (includecat && includesubcat && cat_quanity >= 1 && subcat_quantity >= 1) {
				List<Map<String, Object>> ll = getCategoryForRestId(rest_id);
				for (int i = 0; i < cat_quanity; i++) {
					mm = ll.get(i);
					String cat_id = mm.get(RngConstants.catid).toString();
					List<Map<String, Object>> ll2 = getSubCategoryForRestId(rest_id, cat_id);
					List<SubCategory> sub = getSubCategory(ll2, new ArrayList<>());
					sub = new ArrayList(sub.subList(0, (subcat_quantity < sub.size()) ? subcat_quantity : sub.size()));
					categorieslist.add(new Category(mm.get(RngConstants.catid).toString(), mm.get(RngConstants.catname).toString(), sub));
				}
			}

			else if (includecat && cat_quanity >= 1) {
				List<Map<String, Object>> ll = getCategoryForRestId(rest_id);
				ll = new ArrayList(ll.subList(0, (cat_quanity < ll.size()) ? cat_quanity : ll.size()));
				categorieslist = getCategory(ll, subcatlist);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		restaurantlist = getRestaurantList(rest_id, categorieslist);
		System.out.println(restaurantlist);
		return restaurantlist;
	}

	public void TDstatusChecker(String response) {
		int statusCode = JsonPath.read(response, "$.statusCode");

		if (statusCode == 0) {
			Assert.assertEquals(JsonPath.read(response, "$.statusCode").toString(), "0");
		}
		if (statusCode == 2) {
			Assert.assertEquals(JsonPath.read(response, "$.statusCode").toString(), "2", "Aleady have campaign");
		} else {
			Assert.assertFalse(false);
		}
	}

	public String getCurrentTime(int i) {
		Calendar cal = Calendar.getInstance();
		DayOfWeek dayOfWeek = DayOfWeek.of(cal.get(Calendar.DAY_OF_WEEK));
		String currTime = cal.get(Calendar.HOUR_OF_DAY) + "" + parseToTwo(cal.get(Calendar.MINUTE) + i);
		return currTime;
	}

	public static String getCurrentDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		return dateFormat.format(date);
	}
	public static String getCurrentDate(String pattern) {
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date date = new Date();
		return dateFormat.format(date);
	}

	public static String getFutureDate() {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		Date tomorrow = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'.000Z'");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(tomorrow);
	}
    public static String getFutureDate(String pattern) {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        DateFormat dateFormat = new SimpleDateFormat(pattern);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(tomorrow);
    }
	public static String getDateToSetInCouponUseCountDate(Integer amount) {
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR, amount);
		Date fut = calendar.getTime();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return dateFormat.format(fut);
	}
	public static String getOneMonthAhead(Integer amount) throws ParseException{
		Calendar calendar = Calendar.getInstance();
		Date today = calendar.getTime();
		calendar.add(Calendar.DAY_OF_YEAR,amount);
		long fut = calendar.getTimeInMillis();
		return String.valueOf(fut);
	}

	public static Processor createCoupon(String coupon_type, String name, String code, String description,
										 String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
										 String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
										 String restaurant_restriction, String category_restriction, String item_restriction,
										 String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
										 String first_order_restriction, String preferred_payment_method, String user_client,
										 String slot_restriction, String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
										 String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
										 String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
										 String supported_ios_version, String updated_by, String title, String tnc, String logo) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String unique_code = code + getRandomPostfix();
		String validFrom = getCurrentDate();
		System.out.println("Valid From date in Coupon" + validFrom);
		String validTill = getFutureDate();
		System.out.println("Valid From date in Coupon" + validTill);
		// tnc= new String[]
		GameOfThronesService service = new GameOfThronesService("cs", "createcoupon", gameofthrones);
		String[] payloadparams = { coupon_type, name, unique_code, description, is_private, validFrom, validTill,
				total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction, area_restriction,
				restaurant_restriction, category_restriction, item_restriction, discount_percentage, discount_amount,
				free_shipping, free_gifts, first_order_restriction, preferred_payment_method, user_client,
				slot_restriction, updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction, discount_item,
				usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
				supported_ios_version, updated_by, title, tnc, logo };
		Processor processor = new Processor(service, requestHeaders, payloadparams);

		return processor;
	}

	public static Processor createCouponVendor(String coupon_type, String name, String code, String description,
											   String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
											   String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
											   String restaurant_restriction, String category_restriction, String item_restriction,
											   String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
											   String first_order_restriction, String preferred_payment_method, String user_client,
											   String slot_restriction,String updatedOn, String createdOn, String image, String coupon_bucket, String minQuantityCart,
											   String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
											   String applicable_with_swiggy_money, String vendor, String is_bank_discount, String refund_source,
											   String pg_message, String supported_ios_version, String updated_by, String title, String tnc, String logo) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String unique_code = code + getRandomPostfix();
		String validFrom = getCurrentDate();
		System.out.println("Valid From date in Coupon" + validFrom);
		String validTill = getFutureDate();
		System.out.println("Valid From date in Coupon" + validTill);
		// tnc= new String[]
		GameOfThronesService service = new GameOfThronesService("cs", "createcouponVendor", gameofthrones);
		String[] payloadparams = { coupon_type, name, unique_code, description, is_private, validFrom, validTill,
				total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction, area_restriction,
				restaurant_restriction, category_restriction, item_restriction, discount_percentage, discount_amount,
				free_shipping, free_gifts, first_order_restriction, preferred_payment_method, user_client,
				slot_restriction,updatedOn, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction, discount_item,
				usage_count, expiry_offset, applicable_with_swiggy_money, vendor, is_bank_discount, refund_source,
				pg_message, supported_ios_version, updated_by, title, tnc, logo };
		Processor processor = new Processor(service, requestHeaders, payloadparams);

		return processor;

	}

	private static String getCurrentDay() {
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		return new SimpleDateFormat("E", Locale.ENGLISH).format(date.getTime());

	}

	private static String getRandomOrderId() {
		int codeLength = 7;
		// TODO Auto-generated method stub
		RandomStringUtils.randomNumeric(codeLength);
		return RandomStringUtils.randomNumeric(codeLength);
	}

	private static String getRandomCodePrefix() {
		int codeLength = 4;
		// TODO Auto-generated method stub
		RandomStringUtils.randomAlphabetic(codeLength);
		return RandomStringUtils.randomAlphabetic(codeLength);
	}

	private static String getRandomPostfix() {
		int codeLength = 6;
		// TODO Auto-generated method stub
		return RandomStringUtils.randomAlphanumeric(codeLength).toUpperCase();
	}

	public static String randomString(char[] characterSet, int length) {
		Random random = new SecureRandom();
		char[] result = new char[length];
		for (int i = 0; i < result.length; i++) {
			// picks a random index out of character set > random character
			int randomCharIndex = random.nextInt(characterSet.length);
			result[i] = characterSet[randomCharIndex];
		}
		return new String(result);
	}

	public static Processor couponUserMap(String code, String userId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUserMapping", gameofthrones);
		String[] payloadparams = new String[] { code, userId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor couponUserMapRemove(String code, String userId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUserMappingRemove", gameofthrones);
		String[] payloadparams = new String[] { code, userId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor couponCityMap(String code, String cityId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponCityMapping", gameofthrones);
		String[] payloadparams = new String[] { code, cityId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor couponItemMap(String code, String itemId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponItemMapping", gameofthrones);
		String[] payloadparams = new String[] { code, itemId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor couponRestaurantMap(String code, String restaurantId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponRestaurantMapping", gameofthrones);
		String[] payloadparams = new String[] { code, restaurantId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor couponAreaMap(String code, String restaurantId, String couponId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponAreaMapping", gameofthrones);
		String[] payloadparams = new String[] { code, restaurantId, couponId };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor getCouponUserCity(String userId, String cityId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponCity", gameofthrones);
		String[] urlparams = new String[] { userId, cityId };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static Processor getCouponUserCityOnCartMinValue(String userId, String cityId, String cart_value) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUserCityOnCartMinValue", gameofthrones);
		String[] urlparams = new String[] { userId, cityId, cart_value };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static Processor getCouponUserCityOnRestaurant(String userId, String cityId, String restaurantID) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "CouponUserCityOnRestaurant", gameofthrones);
		String[] urlparams = new String[] { userId, cityId, restaurantID };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static Processor getCouponUser(String userId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUser", gameofthrones);
		String[] urlparams = new String[] { userId };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static Processor applyCouponSingleItem(String code, String cart_areaId, String cart_cityId,
												  String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount, String cartItems_itemKey,
												  String cartItems_menu_item_id, String cartItems_quantity, String restaurantId, String userId,
												  String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
												  String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
												  String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
												  String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
												  String headers_userAgent, String payment_codes, String time) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponApply", gameofthrones);
		String[] payloadparams = new String[] { code, cart_areaId, cart_cityId, cartBlob_swiggyTradeDiscount,
				cartBlob_restaurantTradeDiscount, cartItems_itemKey, cartItems_menu_item_id, cartItems_quantity,
				restaurantId, userId, swiggyMoney, firstOrder, cartPrice_quantity, cartPrice_cartTotal,
				cartPrice_itemLevelPrice, itemLevelPrice_quantity, itemLevelPrice_subTotal, preferredPaymentMethod,
				referralPresence, cartPrice_swiggyMoneyApplicable, superUser, typeOfPartner, swiggyMoneyApplicable,
				headers_versionCode, headers_userAgent, payment_codes, time };
		String[] urlparams = new String[] { code };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}

	public String parseToTwo(int i) {
		if (i < 10)
			return "0" + i;
		return String.valueOf(i);
	}

	public Processor carouselBannerHelp(String startDT, String endDT, String priority, String channel, String carousel,
										String type, String isEnabled, String restVisible, String userRestrict, String cityID, String restId) {
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");

		GameOfThronesService got = new GameOfThronesService("sand", "carouselbanner", gameofthrones);
		String[] arr = { startDT, endDT, priority, channel, carousel, type, isEnabled, restVisible, userRestrict,
				cityID, restId };
		Processor processor = new Processor(got, hashmap, arr);
		System.out.println(processor.RequestValidator.GetBodyAsText());
		return processor;
	}

	public String createCarousel(String channel, String type, String carousel, String redirectLink, String polygonId) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String startDate = dateFormat.format(new Date());
		String endDate = dateFormat.format(DateUtils.addDays(new Date(), 1));
		String priority = "1";
		String isEnabled = "true";
		String restVisible = "true";
		String userRestrict = "false";
		String cityID = "1";

		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");

		GameOfThronesService got = new GameOfThronesService("sand", "carouselbanner", gameofthrones);
		String[] arr = { startDate, endDate, priority, channel, carousel, type, isEnabled, restVisible, userRestrict,
				cityID, polygonId, redirectLink };
		Processor processor = new Processor(got, hashmap, arr);
		String response = processor.ResponseValidator.GetBodyAsText();
		errorCodeHandler(processor, "carousel API");
		return JsonPath.read(processor.ResponseValidator.GetBodyAsText(), "$.data").toString();

	}

	public Processor aggregatorHelp(String lat, String lng, String channel) {
		GameOfThronesService service = new GameOfThronesService("sand", "aggregator", gameofthrones);
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("cache-control", "no-cache");
		headers.put("version-code", rngConstants.versionCode260);
		headers.put("user-agent", channel);
		Processor processer = new Processor(service, headers, new String[] { lat, lng });
		return processer;
	}

	public static Processor bulkCoupon(String code_prefix, String code_suffix_length, String total_coupons,
									   String coupon_type, String name, String description, String is_private, String valid_from,
									   String valid_till, String total_available, String totalPerUser, String min_amount_cart,
									   String customer_restriction, String city_restriction, String area_restriction,
									   String restaurant_restriction, String category_restriction, String item_restriction,
									   String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
									   String first_order_restriction, String preferred_payment_method, String user_client,
									   String slot_restriction, String createdOn, String image, String coupon_bucket, String min_quantity_cart,
									   String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
									   String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
									   String supported_ios_version, String updated_by, String title, String tnc, String logo_id, String openTime,
									   String closeTime, String day, String type) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		int length = 3;
		String unique_code1 = "";
		int count = 0;
		if (type.equalsIgnoreCase("createAllTypebulkCouponData")) {

			unique_code1 = "\"" + code_prefix + getRandomCodePrefix() + "\"";
		} else if (type.equalsIgnoreCase("EmptyString")) {
			unique_code1 = "\"\"";
		} else if (type.equalsIgnoreCase("Repeat")) {
			unique_code1 = "\"" + code_prefix + "\"";
		} else {
			unique_code1 = "" + code_prefix;
		}

		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("type", "BulkCoupon");
		GameOfThronesService service = new GameOfThronesService("cs", "bulkCoupon", gameofthrones);
		String[] payloadparams = new String[] { unique_code1, code_suffix_length, total_coupons, coupon_type, name,
				description, is_private, valid_from, valid_till, total_available, totalPerUser, min_amount_cart,
				customer_restriction, city_restriction, area_restriction, restaurant_restriction, category_restriction,
				item_restriction, discount_percentage, discount_amount, free_shipping, free_gifts,
				first_order_restriction, preferred_payment_method, user_client, slot_restriction, createdOn, image,
				coupon_bucket, min_quantity_cart, cuisine_restriction, discount_item, usage_count, expiry_offset,
				applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message, supported_ios_version,
				updated_by, title, tnc, logo_id, openTime, closeTime, day };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static Processor getCouponBulkAll() {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponBulkAll", gameofthrones);
		// String[] payloadparams = new String[] { };
		// String[] urlparams = new String[] {};
		Processor processor = new Processor(service, requestHeaders);
		return processor;
	}

	public static Processor signUpWithReferral(String name, String mobile, String email, String password,
											   String referral_code) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("sand", "signupReferral", gameofthrones);
		String[] payloadparams = new String[] { name, mobile, email, password, referral_code };
		// String[] urlparams = new String[] {userId};
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public static String getReferralCodeFromWpUsers() {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.getUsers);
		String referral_code = "";
		String userId = "";
		for (int i = 0; i <= 5; i++) {
			referral_code = list.get(3).get("referral_code").toString();
			userId = list.get(3).get("id").toString();

			if (!StringUtils.isEmpty(referral_code)) {
				break;
			}
		}

		// String referral_code=list.get(3).get("referral_code").toString();
		// String userId=list.get(3).get("id").toString();
		System.out.println(referral_code + "B" + userId);
		return referral_code + "," + userId;
	}

	public static void deleteUserByMobile(String mobile) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		// List<Map<String, Object>> list =
		// sqlTemplate.queryForList(RngConstants.selectUser+ mobile);
		sqlTemplate.execute(RngConstants.deleteUser + mobile + RngConstants.endQuotes);
	}

	public static String checkUserByMobile(String mobile) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectUser + mobile + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("mobile").toString()) : "no user with this mobile num. is DB";
	}

	public static String getUserByMobile(String mobile) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectUser + mobile + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("id").toString()) : "no user with this mobile num. is DB";
	}

	public static String getReferralCode(String mobile) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectUser + mobile + RngConstants.endQuotes);
		return (list.size() > 0) ? (list.get(0).get("referral_code").toString()) : "RC1 is not created in DB";
	}

	public static String checkRC1Coupon(String referral_Code) {
		System.out.println("referral code in checkRC1Coupon method is    " + referral_Code);
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.checkRC1 + referral_Code);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "RC1 is not created in DB";
	}

	public static String checkRC2Coupon(String referral_Code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.referral);
		List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.checkRC2 + referral_Code);
		return (list.size() > 0) ? (list.get(0).get("code").toString()) : "RC2 is not created in DB";
	}

	public static Processor getReferralUserId(String userId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "getReferralUserId", gameofthrones);
		// String[] payloadparams = new String[] { };
		String[] urlparams = new String[] { userId };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public static Processor getCity(String cityId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "referralUser", gameofthrones);
		// String[] payloadparams = new String[] { };
		String[] urlparams = new String[] { cityId };
		Processor processor = new Processor(service, requestHeaders, urlparams);
		return processor;
	}

	public static void deleteCoupon(String code) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		sqlTemplate.execute(RngConstants.deleteCoupon + code + RngConstants.endQuotes);

	}

	public static Processor applyCoupon(String code, String cart_areaId, String cart_cityId,
										String cartBlob_swiggyTradeDiscount, String cartBlob_restaurantTradeDiscount, String cartItems_itemKey,
										String cartItems_menu_item_id, String cartItems_quantity, String restaurantId, String userId,
										String swiggyMoney, String firstOrder, String cartPrice_quantity, String cartPrice_cartTotal,
										String cartPrice_itemLevelPrice, String itemLevelPrice_quantity, String itemLevelPrice_subTotal,
										String preferredPaymentMethod, String referralPresence, String cartPrice_swiggyMoneyApplicable,
										String superUser, String typeOfPartner, String swiggyMoneyApplicable, String headers_versionCode,
										String headers_userAgent, String payment_codes, String time) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponApply", gameofthrones);
		String[] urlparams = new String[] { code };
		String[] payloadparams = new String[] { code, cart_areaId, cart_cityId, cartBlob_swiggyTradeDiscount,
				cartBlob_restaurantTradeDiscount, cartItems_itemKey, cartItems_menu_item_id, cartItems_quantity,
				restaurantId, userId, swiggyMoney, firstOrder, cartPrice_quantity, cartPrice_cartTotal,
				cartPrice_itemLevelPrice, itemLevelPrice_quantity, itemLevelPrice_subTotal, preferredPaymentMethod,
				referralPresence, cartPrice_swiggyMoneyApplicable, superUser, typeOfPartner, swiggyMoneyApplicable,
				headers_versionCode, headers_userAgent, payment_codes, time };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}

	public static Processor couponUsageCount(String Coupon_code, String userId, String orderId) {
		String order_Id = getRandomOrderId();
		System.out.println("Order ID is   " + order_Id);

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUsageCount", gameofthrones);
		String[] urlparams = new String[] { Coupon_code };
		String[] payloadparams = new String[] { Coupon_code, userId, order_Id };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}
	public static Processor couponUsageCountGlobalOffset(String Coupon_code, String userId) {
		String order_Id = getRandomOrderId();
		System.out.println("Order ID is   " + order_Id);

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUsageCount", gameofthrones);
		String[] urlparams = new String[] { Coupon_code };
		String[] payloadparams = new String[] { Coupon_code, userId, order_Id };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}
	public static Processor couponUsageCount_remove(String Coupon_code, String userId, String orderId) {
		System.out.println("Order ID is   " + orderId);

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponUsageCount", gameofthrones);
		String[] urlparams = new String[] { Coupon_code };
		String[] payloadparams = new String[] { Coupon_code, userId, orderId };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}

	public static Processor couponTimeSlotOverLap(List<CouponTimeSlotPOJO> couponTimeSlotPOJO) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponTimeSlotPOJO", gameofthrones);
		String[] payloadparams = new String[] {jsonHelper.getObjectToJSON(couponTimeSlotPOJO)};
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}


	public static Processor destroyCouponTimeSlot(String couponId) throws IOException {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("email", "binit.anand@swiggy.in");
		GameOfThronesService service = new GameOfThronesService("cs", "destroyCouponTimeSlot", gameofthrones);
		String[] urlparams = new String[] { couponId };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}


	public static Processor verifyNux(String deviceID,String userAgent,NuxPOJO nuxPOJO) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("device", deviceID);
		requestHeaders.put("User-Agent",userAgent);
		GameOfThronesService service = new GameOfThronesService("cs", "verifyNux", gameofthrones);
		String[] payloadParams = new String[] {jsonHelper.getObjectToJSON(nuxPOJO)};
		Processor processor = new Processor(service, requestHeaders, payloadParams,null);
		return processor;
	}

//	String deviceID, String userAgent, int userId, int cityId, List<String> codes;
	public static Processor postRefferalUserId(String userId, String count, String updatedAt) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "postReferralUserId", gameofthrones);
		// String updatedAtSys=RngHelper.getCurrentDate();
		String[] payloadparams = new String[] { count, updatedAt };
		String[] urlparams = new String[] { userId };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}

	public static Processor postCouponTimeSlot(String day, String openTime, String closeTime, String code, String id,
											   String couponId, String createdBy, String updatedBy, String updatedOn, String createdOn) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String current_day = RngHelper.getCurrentDay();

		System.out.println("Current day passed in TimeSlot method is     " + current_day);
		GameOfThronesService service = new GameOfThronesService("cs", "couponTimeSlot", gameofthrones);
		String[] payloadparams = new String[] { current_day, openTime, closeTime, code, id, couponId, createdBy,
				updatedBy, updatedOn, createdOn };
		String[] urlparams = new String[] {};
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}

	public long getDayDifference(String date) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date orderedDate = formatter.parse(date);
		Date currentDate = formatter.parse(formatter.format(new Date()));
		long diff = (currentDate.getTime() - orderedDate.getTime());

		return (TimeUnit.DAYS.convert((currentDate.getTime() - orderedDate.getTime()), TimeUnit.MILLISECONDS));
	}

	public HashMap<String, String> createFlatWithNoMinAmountAtRestaurantLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = new Integer(rm.nextInt()).toString(); // rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithMinCartAmountAtRestaurantLevel(String minCartAmount,
																				String flatTDAmount) throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = new Integer(rm.nextInt()).toString(); // rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithMinCartAmountAtRestaurantLevel(String minCartAmount,String restId,
																				String flatTDAmount) throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
        // rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restId, "Test", new ArrayList<>()));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}


	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
																						String flatTDAmount) throws IOException {

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant =
		// rlist.get(commonAPIHelper.getRandomNo(0,(rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
																								  String flatTDAmount) throws IOException {

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithTimeSlotRestrictionAtRestaurantLevel(String minCartAmount,
																					  String flatTDAmount) throws IOException {

		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addHours(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addHours(new Date(), -1))));

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size()-1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		disabledActiveTD(randomRestaurant);

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1).slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isValidforCurrentTime", "true");

		return Keys;
	}

	public HashMap<String, String> createFlatWithUserRestrictionAtRestaurantLevel(String minCartAmount,
																				  String flatTDAmount) throws IOException {

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant =
		// rlist.get(commonAPIHelper.getRandomNo(0,(rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(true)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithThirtyDayDormantAtRestaurantLevel(String minCartAmount,
																				   String flatTDAmount) throws IOException {
		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size()-1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();

		disabledActiveTD(randomRestaurant);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public Processor createFlatWithSixtyDayDormantAtRestaurantLevel(String minCartAmount,
			String flatTDAmount, String restID,boolean userRestriction){
		disabledActiveTD(restID);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1;
		Processor processor = null;
		try {
			flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Dev")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					// .slots(slots)
					.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(userRestriction)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
					.build();


			processor =  createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}

	public HashMap<String, String> createFlatWithSixtyDayDormantAtRestaurantLevel(String minCartAmount,
																				  String flatTDAmount) throws IOException {
		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant =
		// rlist.get(commonAPIHelper.getRandomNo(0,(rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithNinetyDayDormantAtRestaurantLevel(String minCartAmount,
																				   String flatTDAmount) throws IOException {
		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant =
		// rlist.get(commonAPIHelper.getRandomNo(0,(rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithNoMinAmountAtCategoryLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();

		List allList = RngHelper.getList(cmsHelper.getRestListTD());
		Iterator it = allList.iterator();
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("categoryLevel")
				.header("categoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Category")
				.restaurantList(createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2))
				/** .slots(slots) **/
				.ruleDiscount("Flat", "Category", "200", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		System.out.println(createRandomRestaurantList(it.next().toString(), true, 2, false, 2, false, 2)
				+ "))))))))))))))))))))))))))");
		HashMap Keys = new HashMap();
		Keys.put("restId", "223");
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithMinCartAmountAtCategoryLevel(String minCartAmount, String flatTDAmount)
			throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtCategoryLevel(String minCartAmount,
																					  String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtCategoryLevel(String minCartAmount,
																								String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithTimeSlotRestrictionAtCategoryLevel(String minCartAmount,
																					String flatTDAmount, int from, int till) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1620", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), from).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isValidforCurrentTime", "false");

		return Keys;
	}

	public HashMap<String, String> createFlatWithUserRestrictionAtCategoryLevel(String minCartAmount,
																				String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(true).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithThirtyDayDormantAtCategoryLevel(String minCartAmount,
																				 String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithSixtyDayDormantAtCategoryLevel(String minCartAmount,
																				String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithNinetyDayDormantAtCategoryLevel(String minCartAmount,
																				 String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public String createFlatWithNoMinAmountAtSubCategoryLevel(List<RestaurantList> restaurantList, String minCartAmount,
															  String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("SubCategoryLevel")
				.header("SubCategoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 30).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Subcategory").restaurantList(restaurantList)
				.ruleDiscount("Flat", "Subcategory", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		return createdTD_Id;
	}

	public HashMap<String, String> createFlatWithMinCartAmountAtSubCategoryLevel(String minCartAmount,
																				 String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtSubCategoryLevel(String minCartAmount,
																						 String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtSubCategoryLevel(String minCartAmount,
																								   String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithTimeSlotRestrictionAtSubCategoryLevel(String minCartAmount,
																					   String flatTDAmount, int from, int till) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1620", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), from).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isValidforCurrentTime", "false");

		return Keys;
	}

	public HashMap<String, String> createFlatWithUserRestrictionAtSubCategoryLevel(String minCartAmount,
																				   String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(true).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithThirtyDayDormantAtSubCategoryLevel(String minCartAmount,
																					String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));

		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithSixtyDayDormantAtSubCategoryLevel(String minCartAmount,
																				   String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithNinetyDayDormantAtSubCategoryLevel(String minCartAmount,
																					String flatTDAmount) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "200", "100").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> getitem(RestaurantList restaurantList) {
		HashMap<String, String> item = new HashMap<>();
		item.put(RngConstants.catid, restaurantList.getCategories().get(0).getId());
		item.put(RngConstants.subcatid, restaurantList.getCategories().get(0).getSubCategories().get(0).getId());
		item.put(RngConstants.itemid,
				restaurantList.getCategories().get(0).getSubCategories().get(0).getMenu().get(0).getId());
		return item;
	}

	public HashMap<String, String> createFlatWithNoMinAmountAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		List<Map<String, Object>> items = cmsHelper.getItemDetails(randomRestaurant);
		List<RestaurantList> restaurantLists = createRandomRestaurantList(randomRestaurant, true, 1, true, 1, true, 2);
		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		HashMap<String, String> item_map = getitem(restaurantLists.get(0));
		List<Map<String, Object>> second_list = getItemsForRestid(randomRestaurant, item_map.get(RngConstants.catid),
				item_map.get(RngConstants.subcatid));
		String second_item = second_list.get(second_list.size() - 1).get(RngConstants.itemid).toString();

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant")
				// .restaurantList(restaurantLists1)
				.restaurantList(restaurantLists)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFlatWithMinCartAmountAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);
		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		HashMap<String, String> item_map = getitem(restaurantLists1.get(0));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put(RngConstants.catid, item_map.get(RngConstants.catid));
		Keys.put(RngConstants.subcatid, item_map.get(RngConstants.subcatid));
		Keys.put(RngConstants.itemid, item_map.get(RngConstants.itemid));

		return Keys;
	}

	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtItemLevel(String minCartAmount,
																				  String flatTDAmount) throws IOException {

		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		////
		// //List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = new
		// Integer(rm.nextInt()).toString();//rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "50", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithNoMinCartAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		////
		// //List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = new
		// Integer(rm.nextInt()).toString();//rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "20", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithTimeSlotRestrictionAtItemLevel(String minCartAmount,
																				String flatTDAmount) throws IOException {

		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		////
		// //List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = new
		// Integer(rm.nextInt()).toString();//rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addSeconds(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -1))));

		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList).slots(slots)
				.ruleDiscount("Flat", "Item", "50", "0").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithDisabledTDAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		////
		// //List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = new
		// Integer(rm.nextInt()).toString();//rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addSeconds(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -1))));

		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(false).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList).slots(slots)
				.ruleDiscount("Flat", "Item", "20", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithMinCartAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		////
		// //List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = new
		// Integer(rm.nextInt()).toString();//rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtItemLevel(String minCartAmount,
																							String flatTDAmount) throws IOException {
		//
		// List<Slot> slots = new ArrayList<>();
		// slots.add(new Slot("1320", "ALL", "1305"));
		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		// List<RestaurantList> restaurantLists1 =
		// createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);
		// //restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		// HashMap<String, String> item_map = getitem(restaurantLists1.get(0));

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "50", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithTimeSlotRestrictionAtItemLevel(String minCartAmount,
																				String flatTDAmount, int from, int till) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1620", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, (rlist.size() - 1))).toString();
		disabledActiveTD(randomRestaurant);
		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		List<RestaurantList> restaurantLists1 = createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);
		HashMap<String, String> item_map = getitem(restaurantLists1.get(0));

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel")
				.header("ItemLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), from).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "200", "100").userRestriction(false).timeSlotRestriction(true)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isValidforCurrentTime", "false");
		Keys.put(RngConstants.catid, item_map.get(RngConstants.catid));
		Keys.put(RngConstants.subcatid, item_map.get(RngConstants.subcatid));
		Keys.put(RngConstants.itemid, item_map.get(RngConstants.itemid));

		return Keys;
	}

	public HashMap<String, String> createFlatWithUserRestrictionAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel")
				.header("ItemLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Item", "20", "0").userRestriction(true).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createFlatWithThirtyDayDormantAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel")
				.header("ItemLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "20", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);

		return Keys;
	}

	public HashMap<String, String> createFlatWithSixtyDayDormantAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "20", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createFlatWithNinetyDayDormantAtItemLevel(String minCartAmount, String flatTDAmount)
			throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantList)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", "20", "0").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithNoMinAmountAtRestaurantLevel(String minCartAmount, String restId,
																				  String freebieItemId) throws IOException {
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 10000).toInstant().getEpochSecond() * 1000))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
				.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
				.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");
		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithMinAmountAtRestaurantLevel(String minCartAmount, String restId,
																				String freebieItemId) throws IOException {
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
				.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
				.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * String tdEnables ; String isCheck ;
		 *
		 * Processor getTDProcessor= getTradeDiscount(createdTD_Id); String
		 * getTDResponse = getTDProcessor.ResponseValidator.GetBodyAsText();
		 * System.out.println("*********************************************"+
		 * getTDProcessor.ResponseValidator.GetResponseCode());
		 *
		 *
		 * int status = getTDProcessor.ResponseValidator.GetResponseCode();
		 * if(status==500 ) { tdEnables = "true"; } else { tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString(); }
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); int status2 =
		 * availibilityProcessor.ResponseValidator.GetResponseCode();
		 *
		 * if(status2==500) { isCheck = "true"; } else { isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); }
		 */
		String statusCode = JsonPath.read(createTDResponse, "$.statusCode").toString();
		String statusMessage = JsonPath.read(createTDResponse, "$.statusMessage").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");
		Keys.put("statusCode",statusCode);
		Keys.put("statusMessage",statusMessage);
		return Keys;
	}
	public Processor createFreebieTDWithMinAmountAtRestaurantLevel(String minCartAmount, String restId,
			String freebieItemId) {
        disabledActiveTD(restId);
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1;
		Processor processor = null;
		try {
			FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(al)
					// .slots(slots)
					.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
					.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
					.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();
			 processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor createFreebieTDWithMinAmountAtRestaurantLevelForSuperUser(String minCartAmount, String restId,
																   String freebieItemId) {
		disabledActiveTD(restId);
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1;
		Processor processor = null;
		try {
			FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(al)
					// .slots(slots)
					.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
					.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
					.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").isSuper(Boolean.TRUE).build();
			processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public HashMap<String, String> createFeebieTDWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
																							String restId, String freebieItemId) throws IOException {
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))

				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
				.timeSlotRestriction("false").firstOrderRestriction("true").taxesOnDiscountedBill("false")
				.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");
		return Keys;
	}
	public Processor createFreebieTDWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
			String restId, String freebieItemId){
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		disabledActiveTD(restId);
		CreateFreebieTDEntry FreebieTradeDiscount1;
		Processor processor = null;
		try {
			FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))

					.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(al)
					// .slots(slots)
					.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
					.timeSlotRestriction("false").firstOrderRestriction("true").taxesOnDiscountedBill("false")
					.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();
			processor=  createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	public HashMap<String, String> createFeebieTDWithRestaurantFirstOrderAtRestaurantLevel(String minCartAmount,
																						   String restId, String freebieItemId) throws IOException {
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		disabledActiveTD(restId);
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantFirstOrder("true").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
				.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
				.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 *
		 */
		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithTimeSlotRestrictionAtRestaurantLevel(String minCartAmount,
																						  String restId, String freebieItemId) throws IOException {

		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		List<FreebieSlot> slots = new ArrayList<>();
		slots.add(new FreebieSlot(new SimpleDateFormat("HHmm").format(DateUtils.addSeconds(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -2))));

		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al).slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
				.userRestriction("false").timeSlotRestriction("true").firstOrderRestriction("false")
				.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT")
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithUserRestrictionAtRestaurantLevel(String minCartAmount,
																					  String restId, String freebieItemId) throws IOException {

		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));

		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
				.userRestriction("true").timeSlotRestriction("false").firstOrderRestriction("false")
				.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT")
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 *
		 */
		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(String minCartAmount,
			String restId, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction,
			String dormant_user_type, boolean is_surge) throws IOException {
		disabledActiveTD(restId);
		ArrayList al = new ArrayList();
		al.add(new FreeDeliveryRestaurantList(restId));
		CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
				.header("RestaurantLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
				.ruleDiscount("FREE_DELIVERY", "Restaurant", minCartAmount).userRestriction(userRestriction)
				.timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
				.dormant_user_type(dormant_user_type).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}


	public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevelSuperUser(String minCartAmount,
																						String restId, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction,
																						String dormant_user_type, boolean is_surge) throws IOException {
		disabledActiveTD(restId);
		ArrayList al = new ArrayList();
		al.add(new FreeDeliveryRestaurantList(restId));
		CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
				.header("RestaurantLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
				.ruleDiscount("FREE_DELIVERY", "Restaurant", minCartAmount).userRestriction(userRestriction)
				.timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
				.dormant_user_type(dormant_user_type).isSuper(Boolean.TRUE).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithThirtyDayDormantAtRestaurantLevel(String minCartAmount,
																					   String restId, String freebieItemId) throws IOException {

		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
				.userRestriction("false").timeSlotRestriction("false").firstOrderRestriction("false")
				.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("THIRTY_DAYS_DORMANT")
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * System.out.println(processor.RequestValidator.GetBodyAsText()+
		 * "****************");
		 * System.out.println(createTDResponse+"============================");
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public Processor createFreebieTDWithThirtyDayDormantAtRestaurantLevel(String minCartAmount,String restId, String freebieItemId){
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1;
		Processor processor = null;
		try {
			FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(al)
					// .slots(slots)
					.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
					.userRestriction("false").timeSlotRestriction("false").firstOrderRestriction("false")
					.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("THIRTY_DAYS_DORMANT")
					.build();
			processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}
	public Processor createFreebieTDWithThirtyDayDormantAtRestaurantLevelUserRestriction(String minCartAmount,String restId, String freebieItemId){
		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1;
		Processor processor = null;
		try {
			FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(al)
					// .slots(slots)
					.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
					.userRestriction("true").timeSlotRestriction("false").firstOrderRestriction("false")
					.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("THIRTY_DAYS_DORMANT")
					.build();
			processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}

	public HashMap<String, String> createFeebieTDWithSixtyDayDormantAtRestaurantLevel(String minCartAmount,
																					  String restId, String freebieItemId) throws IOException {

		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
				.userRestriction("false").timeSlotRestriction("false").firstOrderRestriction("false")
				.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("SIXTY_DAYS_DORMANT")
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public HashMap<String, String> createFeebieTDWithNinetyDayDormantAtRestaurantLevel(String minCartAmount,
																					   String restId, String freebieItemId) throws IOException {

		List<FreebieRestaurantList> al = new ArrayList();
		al.add(new FreebieRestaurantList(Integer.parseInt(restId)));
		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al)
				// .slots(slots)
				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).restaurantFirstOrder("false")
				.userRestriction("false").timeSlotRestriction("false").firstOrderRestriction("false")
				.taxesOnDiscountedBill("false").commissionOnFullBill("false").dormant_user_type("NINETY_DAYS_DORMANT")
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(FreebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}

	public String getRandomRestaurant(String lat, String lng) {
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		List rlist = getRestaurantListing(lat, lng, "0");
		return rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
	}

	public List getItemDetails(String restId) {
		List<Map<String, Object>> itemDetails = cmsHelper.getItemDetails(restId);
		String itemId = itemDetails.get(0).get("item_id").toString();
		String categoryId = itemDetails.get(0).get("cat_id").toString();
		String subCategoryId = itemDetails.get(0).get("subcat_id").toString();
		String count = "1";
		String price = "200";
		List ItemDetails = new ArrayList();

		ItemDetails.add(itemId);
		ItemDetails.add(categoryId);
		ItemDetails.add(subCategoryId);
		ItemDetails.add(count);
		ItemDetails.add(price);
		return ItemDetails;
	}

	public Processor cartEvaluateItemList(List<ItemRequest> itemRequest, String CartAmount, String userId,
										  String firstOrder, String userAgent, String versionCode) {
		JsonHelper jsonHelper = new JsonHelper();
		Processor processor = null;
		try {
			CartEvaluateRequest request = new CartEvaluateRequest(CartAmount, userId, firstOrder, userAgent,
					versionCode, itemRequest);
			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			requestHeaders.put("Authorization", RngConstants.basic_auth);
			requestHeaders.put("tid",
					checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1).get("Tid").toString());
			GameOfThronesService service = new GameOfThronesService("td", "listcartevaluatev2", gameofthrones);
			processor = new Processor(service, requestHeaders, new String[] { jsonHelper.getObjectToJSON(request) });
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor cartEvaluateItemList(CartEvaluateRequest cartEvaluateRequest) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		requestHeaders.put("tid", "1f55a4c2-9bbb-4bcd-85f9-aaa3d6de70ea");
		GameOfThronesService service = new GameOfThronesService("td", "listcartevaluatev3", gameofthrones);
		Processor processor = new Processor(service, requestHeaders,
				new String[] { jsonHelper.getObjectToJSON(cartEvaluateRequest) });
		return processor;
	}
	public Processor menuEvaluateItemList(MenuEvaluateRequest menuEvaluateRequest) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		requestHeaders.put("tid", "1f55a4c2-9bbb-4bcd-85f9-aaa3d6de70ea");
		GameOfThronesService service = new GameOfThronesService("td", "listmenuevaluatev2", gameofthrones);
		Processor processor = new Processor(service, requestHeaders,
				new String[] { jsonHelper.getObjectToJSON(menuEvaluateRequest) });
		return processor;
	}

	public static List<ItemRequest> itemList(ArrayList a) {
		List<ItemRequest> itemRequests = new ArrayList<>();
		itemRequests.add(new ItemRequest(a.get(0).toString(), a.get(1).toString(), a.get(2).toString(),
				a.get(3).toString(), a.get(4).toString(), a.get(5).toString()));
		itemRequests.add(new ItemRequest(a.get(0).toString(), a.get(1).toString(), a.get(2).toString(),
				a.get(3).toString(), a.get(11).toString(), a.get(5).toString()));
		return itemRequests;
	}

	public String createPercentageTDWithNoMinAmountAtSubCategoryLevel(List<RestaurantList> restaurantList,
																	  String minCartAmount, String priceDiscountCap) throws IOException {

		JsonHelper jsonHelper = new JsonHelper();

		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("SubCategoryLevel")
				.header("SubCategoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Gaurav")
				.discountLevel("Subcategory").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Subcategory", "10", minCartAmount, priceDiscountCap).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		return createdTD_Id;
	}

	public void disabledActiveTD(String restaurant_id) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		sqlTemplate.update(RngConstants.deactiveTID + restaurant_id + RngConstants.end);
		System.out.println(RngConstants.deactiveTID + restaurant_id + RngConstants.end);

	}

	public void updateCreatedDateCouponUseCount(String  orderId,String createdAt) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		sqlTemplate.update(RngConstants.updateMultiApplyCreationDate + createdAt +RngConstants.endQuotes+" "+ "where order_id="+RngConstants.endQuotes+orderId+RngConstants.endQuotes+ RngConstants.ending);
		System.out.println(RngConstants.updateMultiApplyCreationDate + createdAt +RngConstants.endQuotes+" "+ "where order_id="+RngConstants.endQuotes+orderId+RngConstants.endQuotes+ RngConstants.ending);

	}
	public void updateFirstTransactionCreatedDateCouponUseCount(String  orderId,String createdAt,String couponCode) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		sqlTemplate.update(RngConstants.updateMultiApplyCreationDate + createdAt +RngConstants.endQuotes+" "+ "where code="+RngConstants.endQuotes+""+couponCode+""+RngConstants.endQuotes+"and"+" "+"order_id"+" "+"NOT IN ("+orderId+RngConstants.endStatement);
		System.out.println(RngConstants.updateMultiApplyCreationDate + createdAt +RngConstants.endQuotes+" "+ "where code="+RngConstants.endQuotes+""+couponCode+""+RngConstants.endQuotes+"and"+" "+"order_id"+" "+"NOT IN ("+orderId+RngConstants.endStatement);

	}

    public void insertCustomerOrderCount(String id,String customer_id,int orderCount) {
			SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.segmentationDB);
			sqlTemplate.update(RngConstants.customerOrders + id + "," + customer_id + "," + "'"+getCurrentDate("yyyy-MM-dd HH:mm:ss")+"'"+ "," + "'"+getCurrentDate("yyyy-MM-dd HH:mm:ss")+"'"+","+ orderCount + ","+"12" + "," + "19" +","+"'"+getCurrentDate("yyyy-MM-dd HH:mm:ss")+"'"+ RngConstants.endPart);
    }
	public void insertCustomerDeviceMapping(int id,int deviceId,String customer_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.segmentationDB);
		sqlTemplate.update(RngConstants.customerDeviceMap + id + "," +deviceId+","+ customer_id + "," + "'"+getCurrentDate("yyyy-MM-dd HH:mm:ss")+"'"+ RngConstants.endPart);
	}
	public int getCustomerOrderId(String customer_id) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.segmentationDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.getOrderId + "'"+customer_id + RngConstants.endQuotes);
		return Integer.parseInt((list.size() > 0) ? (list.get(0).get("id").toString()) : "code is not persent in DB");


	}
	public void updateCustomerOrderCount(String customer_id,int orderCount) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.segmentationDB);
		sqlTemplate.update(RngConstants.updateCustomerOrderCount + orderCount +RngConstants.endQuotes+" "+ "where customer_id="+RngConstants.endQuotes+customer_id+RngConstants.endQuotes+ RngConstants.ending);
	}
	public String getCartMinValueAgainstCouponCode(String couponCode) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.getCartMinValue + "'"+couponCode + RngConstants.endQuotes);
		String value= String.valueOf((list.size() > 0) ? (list.get(0).get("min_amount").toString()) : "code is not persent in DB");
		String intValue= value.replaceAll("[^0-9.^0-9]", "");
		return intValue;
	}
	public String getRestaurantID(String couponCode) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.getRestaurantID + "'"+couponCode + RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("restaurant_id").toString()) : "code is not persent in DB");
	}

	public void disabledActiveTD(String restaurant_id, String campaignType) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		sqlTemplate.update(RngConstants.DeactiveTIDwithCampaignType + campaignType + " and cres.restaurant_id="
				+ restaurant_id + RngConstants.end);
		System.out.println(sqlTemplate.update(RngConstants.DeactiveTIDwithCampaignType + campaignType
				+ " and cres.restaurant_id=" + restaurant_id + RngConstants.end));

	}
public static int incrementValue(int number) {
		int count=0;
		int value= number+count;
		count++;
		return value;
}
	public String createPercentageTDWithNoMinAmountAtCategoryLevel(List<RestaurantList> restaurantList,
																   String minCartAmount, String priceDiscountCap) throws IOException {
		JsonHelper jsonHelper = new JsonHelper();
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("CategoryLevel")
				.header("CategoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Abhijit")
				.discountLevel("Category").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Category", "20", minCartAmount, priceDiscountCap).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		System.out.println(createTDResponse);
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		return createdTD_Id;
	}

	public String createFlatWithNoMinAmountAtCategoryLevel(List<RestaurantList> restaurantList, String minCartAmount,
														   String flatTDAmount) throws IOException {
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("CategoryLevel")
				.header("CategoryLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Category").restaurantList(restaurantList)
				.ruleDiscount("Flat", "Category", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		return createdTD_Id;
	}

	public HashMap<String, String> createPercentageWithFirstOrderRestrictionAtItemLevel() throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				/** .slots(slots) **/
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(true).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;

	}

	public HashMap<String, String> createPercentageWithRestaurantFirstOrderRestrictionAtItemLevel() throws IOException {
		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithTimeSlotRestrictionAtItemLevel(String minCartAmount,
																					  String flatTDAmount, int from, int till) throws IOException {
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1620", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size())).toString();
		// restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new
		// ArrayList<>()));
		List<RestaurantList> restaurantLists1 = createRandomRestaurantList(randomRestaurant, true, 1, true, 2, true, 2);
		HashMap<String, String> item_map = getitem(restaurantLists1.get(0));

		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount33 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantLists1).slots(slots)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount33));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isValidforCurrentTime", "false");
		Keys.put(RngConstants.catid, item_map.get(RngConstants.catid));
		Keys.put(RngConstants.subcatid, item_map.get(RngConstants.subcatid));
		Keys.put(RngConstants.itemid, item_map.get(RngConstants.itemid));

		return Keys;
	}

	public HashMap<String, String> createPercentageWithUserRestrictionAtItemLevel() throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).userRestriction(true)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithThirtyDayDormantAtItemLevel() throws IOException {
		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithSixtyDayDormantAtItemLevel() throws IOException {
		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("SIXTY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithNinetyDayDormantAtItemLevel() throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("NINETY_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithNoMinCartAtItemLevel() throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "0", "2000").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithMinCartAtItemLevel() throws IOException {

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}



	public HashMap<String, String> createPercentageWithTimeSlotRestrictionAtItemLevel() throws IOException {
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addSeconds(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -1))));

		Menu menu = new Menu();
		String itemId = new Integer(rm.nextInt()).toString();

		menu.setId(itemId);
		menu.setName("testItem");

		List<Menu> menus = new ArrayList<>();
		menus.add(menu);

		SubCategory subCategory = new SubCategory();
		String SubcategoryId = new Integer(rm.nextInt()).toString();
		subCategory.setId(SubcategoryId);
		subCategory.setName("test");
		subCategory.setMenu(menus);

		List<SubCategory> subCategories = new ArrayList<>();
		subCategories.add(subCategory);

		Category category = new Category();
		String categoryId = new Integer(rm.nextInt()).toString();
		category.setId(categoryId);
		category.setName("test");
		category.setSubCategories(subCategories);

		List<Category> categories = new ArrayList<>();
		categories.add(category);

		RestaurantList restaurant = new RestaurantList();
		String restId = new Integer(rm.nextInt()).toString();
		restaurant.setId(restId);
		restaurant.setName("test");
		restaurant.setCategories(categories);

		List<RestaurantList> restaurantList = new ArrayList<>();
		restaurantList.add(restaurant);

		disabledActiveTD(restId);

		CreateTdEntry percetageTradeDiscount = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantList)
				.ruleDiscount("Percentage", "Item", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(true).slots(slots).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(false).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", restId);
		Keys.put("TDID", createdTD_Id);
		Keys.put("catId", categoryId);
		Keys.put("subCatId", SubcategoryId);
		Keys.put("itemId", itemId);
		return Keys;
	}

	public HashMap<String, String> PercentageTD_EvaluationWithNoMinCartAt_ItemLevel() throws IOException {

		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		disabledActiveTD(randomRestaurant);
		List<RestaurantList> restaurantLists1 = createRandomRestaurantList(randomRestaurant, true, 1, true, 1, true, 2);
		HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		List<Map<String, Object>> second_list = getItemsForRestid(randomRestaurant, item_map.get(RngConstants.catid),
				item_map.get(RngConstants.subcatid));
		String second_item = second_list.get(second_list.size() - 1).get(RngConstants.itemid).toString();
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount31 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "0", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount31));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", "0");
		Keys.put("TDID", createdTD_Id);
		Keys.put(RngConstants.next_item, second_item);
		return Keys;
	}

	public HashMap<String, String> PercentageTD_EvaluationWithMinCartAt_ItemLevel() throws IOException {

		JsonHelper jsonHelper = new JsonHelper();
		CMSHelper cmsHelper = new CMSHelper();
		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot("1320", "ALL", "1305"));
		List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0, rlist.size() - 1)).toString();
		disabledActiveTD(randomRestaurant);
		List<RestaurantList> restaurantLists1 = createRandomRestaurantList(randomRestaurant, true, 1, true, 1, true, 2);
		HashMap<String, String> item_map = getitem(restaurantLists1.get(0));
		List<Map<String, Object>> second_list = getItemsForRestid(randomRestaurant, item_map.get(RngConstants.catid),
				item_map.get(RngConstants.subcatid));
		String second_item = second_list.get(second_list.size() - 1).get(RngConstants.itemid).toString();
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount31 = new CreateTdBuilder().nameSpace("ItemLevel").header("ItemLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("40").enabled(true).swiggy_hit("60").createdBy("Test")
				.discountLevel("Item").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Percentage", "Category", "10", "100", "2000").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount31));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", "100");
		Keys.put("TDID", createdTD_Id);
		Keys.put(RngConstants.next_item, second_item);
		return Keys;
	}

	public Processor getListingProcessor(String lat, String lng, String page, String collection,
			String popupBannerIds) {
		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "listing_v5", gameofthrones);
		String[] urlparams = new String[] { lat, lng, page, collection, popupBannerIds };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getSwiggyPopListing(String lat, String lng) {
		HashMap headers = new HashMap<String, String>();
		service = new GameOfThronesService("restaurantlisting", "swiggyPopListing", gameofthrones);
		String[] urlparams = new String[] { lat, lng };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getCollectionLisitngProcessor(String lat, String lng, String collection) {
		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "collection_listing", gameofthrones);
		String[] urlparams = new String[] { lat, lng, collection };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getCollectionLisitngPaginationProcessor(String lat, String lng, String collection, String offset,
			String tid, String token) {
		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", tid);
		headers.put("token", token);
		service = new GameOfThronesService("restaurantlisting", "collection_pagination_listing", gameofthrones);
		String[] urlparams = new String[] { lat, lng, collection, offset, tid, token };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getUserAgentRelatedAgent(String lat, String lng, String userAgent, boolean redirectToMock) {
        redirectToMock = false;
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("User-Agent", userAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		String[] urlparams = new String[] { lat, lng, "0" };
		processor = new Processor(service, headers, null, urlparams);

        int i=5;
        while(i!=0) {
            processor = new Processor(service, headers, null, urlparams);
            i--;
            if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
                i=0;
            }else {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
		return processor;
	}

	public Processor getUserAgentRelatedAgent(String lat, String lng, String userAgent, String versionCode) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("User-Agent", userAgent);
		headers.put("version-code", versionCode);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		String[] urlparams = new String[] { lat, lng, "0" };
		processor = new Processor(service, headers, null, urlparams);
		return processor;
	}

	public Processor getDwebListingProcessor(String lat, String lng, String pageType) {

		HashMap headers = new HashMap<String, String>();
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "listingpage", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getSeeAllProcessor(String lat, String lng, String pageType, String offset, String pagelimit,
			String tid, String token) {

		HashMap headers = new HashMap<String, String>();
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("token", token);
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("tid", tid);
		service = new GameOfThronesService("restaurantlisting", "seeall", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit };
		int i=5;
		do {
			processor = new Processor(service, headers, null, urlparams);
			i--;
			if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
				i=0;
			}
		}while(i!=0);



		return processor;

	}

	public Processor getSeeAllFilterProcessor(String lat, String lng, String pageType, String offset, String pagelimit,
			String filter) {

		HashMap headers = new HashMap<String, String>();

		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "seeallfilter", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit, filter };
		processor = new Processor(service, headers, null, urlparams);
		return processor;

	}

	public Processor getSeeAllSortProcessor(String lat, String lng, String pageType, String offset, String pagelimit,
			String sort) {

		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "seeallsort", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit, sort };
		processor = new Processor(service, headers, null, urlparams);

		return processor;

	}

	public Processor getSeeAllProcessor(String lat, String lng, String pageType, String offset, String pagelimit) {

		HashMap headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		service = new GameOfThronesService("restaurantlisting", "seeall", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit };
		processor = new Processor(service, headers, null, urlparams);

		return processor;

	}

	public Processor getSeeAllProcessorwithFilter(String lat, String lng, String colletionId, String pageType,
												  String offset, String pagelimit, String filter) {

		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "desktopcollectionFilter", gameofthrones);

		String[] urlparams = new String[] { lat, lng, colletionId, pageType, offset, pagelimit, filter };

        int i=5;
        do {
            processor = new Processor(service, headers, null, urlparams);
            i--;
            if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
                i=0;
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(i!=0);




		return processor;

	}

	public Processor getDwebProcessorWithFilter(String lat, String lng, String pageType, String offset,
												String pagelimit, String filter) {

		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("dweblisting", "dwebfilterlisting", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit, filter };
		int i=5;
		do {
			processor = new Processor(service, headers, null, urlparams);
			i--;
			if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
				i=0;
			}
		}while(i!=0);
		return processor;

	}

	public Processor getDwebProcessorWithSort(String lat, String lng, String pageType, String offset, String pagelimit,
											  String sort) {

		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("dweblisting", "dwebsortlisting", gameofthrones);
		String[] urlparams = new String[] { lat, lng, pageType, offset, pagelimit, sort };

        int i=5;
        do {
            processor = new Processor(service, headers, null, urlparams);
            i--;
            if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
                i=0;
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(i!=0);

		return processor;

	}

	public Processor getDesktopCollectionProcessor(String lat, String lng, String collection, String offset,
												   String pagelimit) {

		HashMap headers = new HashMap<String, String>();
		headers.put("User-Agent", RngConstants.iosAgent);
		headers.put("version-code", RngConstants.VersionCode229);
		headers.put("tid", RngConstants.constantTid);
		headers.put("token", RngConstants.constantToken);
		service = new GameOfThronesService("restaurantlisting", "desktopcollection", gameofthrones);
		String[] urlparams = new String[] { lat, lng, collection, offset, pagelimit };
		processor = new Processor(service, headers, null, urlparams);

		return processor;

	}

	public String delivered() throws Exception {
		List<Object> t = endtoend();
		String order_id = endToEndHelp.createOrder((CreateMenuEntry) t.get(0));
		deliveryServiceHelper.processOrderInDelivery(order_id, OrderStatus.DELIVERED, t.get(1).toString());
		List<Map<String, Object>> status = deliveryServiceHelper.delDB(order_id);
		Assert.assertNotNull(status.get(0).get("delivered_time"), "status is null");
		return RngConstants.del;
	}

	public List<Object> endtoend() throws Exception {
		EndToEndHelp endToEndHelp = new EndToEndHelp();
		String de = endToEndHelp.getDEId().toString();
		System.out.println("====>" + de);
		List<Cart> cart = new ArrayList<>();
		// cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("683"), 1));
		cart.add(new Cart(null, null, endToEndHelp.getRestaurantMenu("223"), 1));
		CreateMenuEntry createOrder = new CreateOrderBuilder().password("swiggy").mobile(7987663409l).cart(cart)
				.restaurant(223).paymentMethod("Cash").orderComments("Test-Order").buildAll();
		List<Object> list = new ArrayList<>();
		list.add(createOrder);
		list.add("216");
		return list;
	}

	public void DormantUser(String userId, String restId, String days) {

		System.out
				.println("+++++++++++++++++++++++++++++++++" + userId + "       " + restId + "  Dormant days  " + days);
		Calendar calender = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		calender.add(Calendar.DATE, -Integer.parseInt(days));
		String date = dateFormat.format(calender.getTime());
		// sqlTemplate = redisHelper.getRedisTemplate("rngredis", 0);
		// sqlTemplate.setKeySerializer(new StringRedisSerializer());
		// sqlTemplate.setHashKeySerializer(new StringRedisSerializer());
		// sqlTemplate.opsForSet().add("user_restaurant_last_order", userId+" "+restId+"
		// "+date);
		redisHelper.setHashValue("rngredis", 0, "user_restaurant_last_order:" + userId, restId, date);
		System.out.println("Hello+========================================================s");

	}
	public void deleteDormantUserRestMapping(String userId) {
		String key = "user_restaurant_last_order:" + userId;
		redisHelper.deleteKey("rngredis", 0, key);
		System.out.println("Dormant key deleted");
	}

	public int roundUp(int n, int rountNo) {
		return (n + (rountNo - 1)) / rountNo * rountNo;
	}

	public Processor getFavoriteProcessor(String lat, String lng, String restId) {
		RngHelper rngHelper = new RngHelper();
		HashMap hm = rngHelper.getMakeFavorite(restId);
		String tid = hm.get("tid").toString();

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);

		service = new GameOfThronesService("restaurantlisting", "favoritelisting", gameofthrones);
		String[] urlparams = new String[] { lat, lng };
		processor = new Processor(service, headers, null, urlparams);
		return processor;
	}

	public Processor favoriteListingWithWrongtid(String lat, String lng, String tid, String token) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("tid", token);

		service = new GameOfThronesService("restaurantlisting", "favoritelisting", gameofthrones);
		String[] urlparams = new String[] { lat, lng };
		processor = new Processor(service, headers, null, urlparams);
		return processor;
	}

	public HashMap getMakeFavorite(String restId) {
		HashMap<String, String> hmap = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1);
		String tid = hmap.get("Tid").toString();
		String token = hmap.get("Token").toString();

		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");

		service = new GameOfThronesService("sand", "makefavorite", gameofthrones);
		String[] payloadparams = new String[] { restId };
		processor = new Processor(service, headers, payloadparams);

		return headers;

	}

	public HashMap getHeader() {

		HashMap<String, String> hmap = checkoutHelper.TokenData(rngConstants.mobile1, rngConstants.password1);

		return hmap;
	}

	public Processor getListing(String lat, String lng, String page, String tid, String token) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");

		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		Processor processor;
		int i=5;
        do {
            processor  = new Processor(service, headers, null, urlparams);
            i--;
            if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
                i=0;
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }while(i!=0);



		return processor;
	}

    public Processor getListing(String lat, String lng, String page, String tid, String token, boolean redirectToMock) {
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("tid", tid);
        headers.put("token", token);
        headers.put("Content-Type", "application/json");

        String[] urlparams = new String[] { lat, lng, page };
        GameOfThronesService service = new GameOfThronesService(redirectToMock?"restaurantlistingmock":"restaurantlisting", "listingv5", gameofthrones);
        Processor processor = new Processor(service, headers, null, urlparams);

        return processor;
    }

	public List<String> getAllResturants(String lat, String lng, String page) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");

		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);

		Processor processor;
		int i=5;
		do {
			processor = new Processor(service, headers, null, urlparams);
			i--;
			if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))!=0){
				i=0;
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}while(i!=0);
		String response = processor.ResponseValidator.GetBodyAsText();

		List<String> list = JsonPath.read(response, "$.data.cards..cardType");
		List<String> allRest = new ArrayList<String>();
		String data="";
		i=0;
		for(String cardtype: list)
		{
			if(cardtype.equals("restaurant")) {
				data = JsonPath.read(response, "$.data.cards[" + i + "].data.data.id");
				allRest.add(data);
			}
			i++;
		}
		return allRest;
	}
	public void addOfferOnGivenResturant(List<String> rests)
	{
		for(String restId: rests) {
			try {
				createFlatWithNoMinAmountAtRestaurantLevel(restId);
				System.out.println("successfully applied TD");
			} catch (IOException e) {
				System.out.printf("Unable to creat TD on given resturant " + restId);
				e.printStackTrace();
			}
		}
	}

	public Processor getListing(String lat, String lng, String page, String userAgent, String tid, String token) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");
		headers.put("User-Agent", userAgent);

		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		Processor processor = new Processor(service, headers, null, urlparams);

		return processor;
	}

	public Processor getListing(String lat, String lng, String page, String userAgent, String tid, String token, boolean redirectToMock) {
        redirectToMock = false;
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");
		headers.put("User-Agent", userAgent);

		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService(redirectToMock?"restaurantlistingmock":"restaurantlisting", "listingv5", gameofthrones);
		Processor processor = new Processor(service, headers, null, urlparams);

		return processor;
	}

	public Processor getFilterProcessor(String filter, String lat, String lng, String page, String tid, String token)

	{
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");

		String[] urlparams = new String[] { lat, lng, filter };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "filterlisting", gameofthrones);
		Processor processor = new Processor(service, headers, null, urlparams);

		return processor;

	}

	public Processor getSortProcessor(String filter, String lat, String lng, String page, String tid, String token)

	{
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("tid", tid);
		headers.put("token", token);
		headers.put("Content-Type", "application/json");

		String[] urlparams = new String[] { lat, lng, filter };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "sortlisting", gameofthrones);
		Processor processor = new Processor(service, headers, null, urlparams);

		return processor;

	}

	public String createCollectionNew(String cityId) {


		return "";
	}


	public String  createCollection(String lat, String lng, String userAgent, int i1) {

		String collectionId = null;
		try {
			SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);
			if (i1 == 1) {
				sqlTemplate.execute(RngConstants.collectionCreation1 + RngConstants.ending);
			}
			if (i1 == 2) {
				sqlTemplate.execute(RngConstants.collectionWithBackGroundCreation + RngConstants.ending);
			}
			List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.collectionId);

			collectionId = list.get(0).get("id").toString();
			processor = getUserAgentRelatedAgent(lat, lng, userAgent,"false");
			String response = processor.ResponseValidator.GetBodyAsText();
			System.out.println(response);
			List<String> cardTypes = JsonPath.read(response, "$.data..cardType");
			List<String> restIds = new ArrayList<String>();
			for (int i = 0; i < cardTypes.size(); i++) {
				if (cardTypes.get(i).toString().equals("restaurant")) {

					sqlTemplate.execute(RngConstants.collection_user + "'" + collectionId + "'" + "," + "'"
							+ JsonPath.read(response, "$.data.cards[" + i + "].data.data.id").toString() + "'" + ")"
							+ RngConstants.ending);

				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		collectionRedis(collectionId);
		return collectionId;

	}

	public String  createCollection(String lat, String lng, String userAgent, List<String> restID) {

		String collectionId = null;
		try {
			SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);

			sqlTemplate.execute(RngConstants.collectionCreation1 + RngConstants.ending);

			List<Map<String, Object>> list = sqlTemplate.queryForList(RngConstants.collectionId);

			collectionId = list.get(0).get("id").toString();


			for (int i = 0; i < restID.size() && i<5; i++) {
					sqlTemplate.execute(RngConstants.collection_user + "'" + collectionId + "'" + "," + "'"
							+ restID.get(i) + "'" + ")"
							+ RngConstants.ending);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		collectionRedis(collectionId);
		return collectionId;

	}

	public void collectionRedis(String collectionId) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");
		String[] urlParam = new String[] { collectionId };
		service = new GameOfThronesService("sand", "collectionredis", gameofthrones);
		processor = new Processor(service, headers, null, urlParam);

	}

	public Processor seeMore(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		service = new GameOfThronesService("dweblisting", "desktopcollection", gameofthrones);
		String[] urlParams = new String[] { lat, lng, pageType, collectionId, offset, pageLimit };
		processor = new Processor(service, headers, null, urlParams);
		return processor;
	}

	public Processor seeMore(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, String tid, String token) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		headers.put("tid", tid);
		headers.put("token", token);
		service = new GameOfThronesService("dweblisting", "desktopcollection", gameofthrones);
		String[] urlParams = new String[] { lat, lng, pageType, collectionId, offset, pageLimit };
		processor = new Processor(service, headers, null, urlParams);
		return processor;

	}

	public Processor seeMore(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, String filter) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		service = new GameOfThronesService("dweblisting", "desktopcollectionfilter", gameofthrones);
		String[] urlParams = new String[] { lat, lng, pageType, collectionId, offset, pageLimit, filter };
		processor = new Processor(service, headers, null, urlParams);
		return processor;

	}

	public Processor seeMoresort(String lat, String lng, String pageType, String collectionId, String offset,
			String pageLimit, String sort) {
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		service = new GameOfThronesService("dweblisting", "desktopcollectionsort", gameofthrones);
		String[] urlParams = new String[] { lat, lng, pageType, collectionId, offset, pageLimit, sort };
		processor = new Processor(service, headers, null, urlParams);
		return processor;

	}

	public HashMap<String, String> createPercentageWithNoMinAmountAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createPercentageWithMinAmountAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = new Integer(rm.nextInt()).toString();// rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithSwiggyFirstOrderAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").firstOrderRestriction(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithSwiggyRestaurantOrderAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").restaurantFirstOrder(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithUserRestrictionAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").userRestriction(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithTimeSlotRestrictionAtRestaurantLevel() throws IOException {

		List<Slot> slots = new ArrayList<>();
		slots.add(new Slot(new SimpleDateFormat("HHmm").format(DateUtils.addSeconds(new Date(), 10)), "ALL",
				new SimpleDateFormat("HHmm").format(DateUtils.addMinutes(new Date(), -1))));

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1).slots(slots)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").timeSlotRestriction(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithThirtyDaysDormantAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").dormant_user_type("THIRTY_DAYS_DORMANT")
				.build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithSixtyDaysDormantAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").dormant_user_type("SIXTY_DAYS_DORMANT")
				.build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createPercentageWithNinetyDaysDormantAtRestaurantLevel() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").dormant_user_type("NINETY_DAYS_DORMANT")
				.build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public void createFYIBanner(String areaID) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CmsConstants.cmsDB);

		sqlTemplate.execute(rngConstants.fyiBanner);

		List<Map<String, Object>> list = sqlTemplate.queryForList(rngConstants.fyiId);

		String fyibannerID = list.get(0).get("id").toString();

		System.out.println(fyibannerID + "===========");

		sqlTemplate.execute(rngConstants.pop_area_map + fyibannerID + "," + areaID + rngConstants.endpart);

		System.out.println(rngConstants.pop_channel_map + fyibannerID + rngConstants.pop_channel_map_end
				+ "**************************");

		sqlTemplate.execute(rngConstants.pop_channel_map + fyibannerID + rngConstants.pop_channel_map_end);

	}

	public void serviceabilityWithScanner() {
		DeliveryHelperMethods dhm = new DeliveryHelperMethods();

		dhm.dbhelperupdate(rngConstants.crutonQuery);
		dhm.dbhelperupdate(rngConstants.crutonRainQuery);
	}

	public Processor getRestaurantListOnLocation(String lat, String lng, String page) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] urlparams = new String[] { lat, lng, page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listingv5", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;

	}

	public Processor getRestaurantListOnLocationFromCollection(String lat, String lng, String page,
															   String collectionId) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] urlparams = new String[] { lat, lng, page, collectionId, "0" };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listing_v5", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;

	}

	public Processor getRestaurantListOnLocationWithFilter(String lat, String lng, String page, String filter) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] urlparams = new String[] { lat, lng, page, "0", filter };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listing_withFilter",
				gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;

	}

	public Processor getFavoritesList(String tid, String token, String lat, String lng, String page) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Tid", tid);
		requestHeaders.put("token", token);
		String[] urlparams = new String[] { lat, lng, page, "0" };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listing_forFav", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;

	}

	public static Processor createMultiApplyCoupon(HashMap<String, String> a1, String type) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "createMultiApplyCoupon", gameofthrones);
		// validFrom = getCurrentDate(); System.out.println("Valid From date in
		// Coupon"+validFrom);
		// validTill = getFutureDate(); System.out.println("Valid From date in
		// Coupon"+validTill);

		if (!(type.equalsIgnoreCase("true"))) {
			String code = getRandomPostfix();
			System.out.println("code is ---- " + code);
			// a1.get(Integer.toString(0)).replaceAll("ALPHACODE", code);
			a1.put("0", code);
		}
		String[] payloadparams = new String[a1.size()];
		// System.out.println("this is the value----- " + a1.get("0"));
		for (int j = 0; j < a1.size(); j++) {
			payloadparams[j] = a1.get(Integer.toString(j));
			// System.out.println("this is the value----- " + a1.get(Integer.toString(j)));
		}
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;

	}
    public Processor createMutiCoupon(CreateMultiApplyCouponPOJO payload) {

        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        // tnc= new String[]
        Processor processor = null;
        try {
            GameOfThronesService service = new GameOfThronesService("cs", "createMultiApplyCouponPOJO", gameofthrones);
            processor = new Processor(service, requestHeaders, new String[] { jsonHelper.getObjectToJSON(payload) });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return processor;
    }
	public Processor createMultiApplyCouponGlobalOffset(GlobalOffsetCreateMultiApplyCouponPOJO payload) {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		Processor processor = null;
		try {
			GameOfThronesService service = new GameOfThronesService("cs", "GlobalOffsetCreateMultiApplyCouponPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders, new String[] { jsonHelper.getObjectToJSON(payload) });
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}


		public static Processor guavaEviction(){

			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			GameOfThronesService service = new GameOfThronesService("cs", "guavaEviction", gameofthrones);
			Processor processor = new Processor(service, requestHeaders);
			return processor;

		}


	public static Processor updateMultiApplyCoupon(HashMap<String, String> a1, String couponCode, String Key,
												   String valueUpdate) {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "updateMultiApplyCoupon", gameofthrones);
		String[] urlparams = new String[] { couponCode };

		// validFrom = getCurrentDate(); System.out.println("Valid From date in
		// Coupon"+validFrom);
		// validTill = getFutureDate(); System.out.println("Valid From date in
		// Coupon"+validTill);

		// String code = getRandomPostfix();
		// System.out.println("code is ---- " + code);
		// a1.put("0", couponCode);
		a1.put(Key, valueUpdate);

		String[] payloadparams = new String[a1.size()];
		// System.out.println("this is the value----- " + a1.get("43"));
		for (int j = 0; j < a1.size(); j++) {
			payloadparams[j] = a1.get(Integer.toString(j));
			System.out.println("this is the value----- " + a1.get(Integer.toString(j)));
		}
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;

	}

	public static Processor getMultiApplyCoupon(String couponCode) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "getMultiApplyCoupon", gameofthrones);
		String[] urlparams = new String[] { couponCode };
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;
	}

	public String getMealCampaignByMealId(String mealId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		return String.valueOf(
				sqlTemplate.queryForList(RngConstants.getactiveCampaign + mealId + RngConstants.getactiveCampaign1)
						.get(0).get("id"));
	}

	public String getOperationMetaData(long campaignId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		System.out.println();
		return String.valueOf(
				sqlTemplate.queryForList(RngConstants.getOperationMeta + campaignId).get(0).get("operation_meta"));
	}

	public void setOperationMetaData( long campaignId, String text) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		sqlTemplate.update(
				RngConstants.updateOperationMeta + "\'" + text + "\'" + rngConstants.updateOperationMeta1 + campaignId);
	}

	public String setHeaderMetaData(String text) {
		String str1 = "{\"mealHeader\":";
		String str2 = ",\"mealDescription\":\"Select any 2 Medium Pizzas @ 199 each\"}";
		return (str1 + text + str2);
	}

	public String setDescriptionMetaData(String text) {
		String str1 = "{\"mealHeader\":\"2 Pizzas at 299 each\",\"mealDescription\":";
		String str2 = "}";
		return (str1 + text + str2);
	}

	public Processor couponApply(HashMap<String, String> val) {
		HashMap<String, String> requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponApply", gameofthrones);
		String[] urlparams = new String[] { val.get(Integer.toString(0)) };
		String[] payloadparams = new String[val.size()];
		for (int i = 0; i < val.size(); i++) {
			// payload[i]= val.get(Integer.toString(i));
			payloadparams[i] = val.get(Integer.toString(i));
			System.out.println("payload is ---- > " + val.get(Integer.toString(i)));
		}

		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);

		return processor;
	}

	public String discountsCreate(ArrayList<String> allRestIds, ArrayList<String> campOperationType,
								  ArrayList<String> restMealCount, ArrayList<String> mealIds, ArrayList<String> mealsOperationMeta,
								  ArrayList<String> mealRewards, ArrayList<String> mealCount, ArrayList<String> mealIsParent,
								  ArrayList<Integer> numberOfGroupsInMeal, ArrayList<String> allGroupCounts, ArrayList<String> mealGroupIds,
								  ArrayList<String> groupRewards) {
		int restCount = allRestIds.size();
		String discounts = "<discountPayload>";
		String discountPayload = "";
		String discountRequest = "";
		String restId = "";
		// int mealNum=reset;
		for (int i = 0; i < restCount; i++) {
			// String count = Integer.
			restId = allRestIds.get(i);
			String restaurantId = "\"restaurant\":{\"id\": <restId>},";
			discountPayload = discountPayload + restaurantId.replace("<restId>", restId);
			discountPayload = discountPayload + operationType(campOperationType);
			discountPayload = discountPayload + mealsPayload(restMealCount, mealIds, i, mealsOperationMeta, mealCount,
					mealIsParent, numberOfGroupsInMeal, mealGroupIds, mealRewards, groupRewards);

		}

		discountRequest = discounts.replace("<discountPayload>", discountPayload);
		// discounts.replace("<discountPayload>", discountPayload);
		// System.out.println("discount123333333" + discountRequest);
		mealNum = 0;
		groupNum = 0;
		return discountRequest;

	}

	public String operationType(ArrayList<String> campOperationType) {
		String operationType = " \"operationType\": <operationTypePayload>,";

		String operationTypePayload = campOperationType.get(0);

		return operationType.replace("<operationTypePayload>", operationTypePayload);
	}

	public String mealsPayload(ArrayList<String> restMealCount, ArrayList<String> mealIds, int restNo,
							   ArrayList<String> mealsOperationMeta, ArrayList<String> mealCount, ArrayList<String> mealIsParent,
							   ArrayList<Integer> groupCount, ArrayList<String> mealGroupIds, ArrayList<String> mealRewards,
							   ArrayList<String> groupRewards) {
		String meals = "\"meals\": [ <mealsPayload> ]";
		String mealsPayload = "";
		String mealId = "\"mealId\": <id>,";
		String rewardsPayload = "";
		String groupsPayload = "";
		String mealRequest = "";
		String numberOfMealsInRest = restMealCount.get(restNo);

		for (int i = 0; i < Integer.parseInt(numberOfMealsInRest); i++) {
			mealsPayload = mealsPayload + mealId.replace("<id>", mealIds.get(mealNum));
			mealNum++;
			mealsPayload = mealsPayload + mealOperationMeta(mealsOperationMeta, i);
			rewardsPayload = rewards(mealRewards, i);
			mealsPayload = mealsPayload + rewardsPayload + ",";
			groupsPayload = groupsPayload(groupCount, mealGroupIds, i, groupRewards);
			mealsPayload = mealsPayload + groupsPayload;
			mealsPayload = mealsPayload + mealCount(mealCount, i);
			mealsPayload = mealsPayload + mealIsParent(mealIsParent, i);
			if (i < Integer.parseInt(numberOfMealsInRest) - 1) {
				mealsPayload = mealsPayload + "},{";
			} else {
				mealsPayload = mealsPayload + "}";
			}
			mealRequest = meals.replace("<mealsPayload>", "{" + mealsPayload);
		}
		// meals.replace("[<mealsPayload>]", mealsPayload);
		return mealRequest;
	}

	public String mealOperationMeta(ArrayList<String> mealsOperationMeta, int mealNumber) {

		String operationMeta = "  <operationMetaPayload> ";
		String operationRequest = "";
		String operationMetaPayload = mealsOperationMeta.get(mealNumber);
		operationRequest = operationMeta.replace("<operationMetaPayload>", operationMetaPayload);

		return operationRequest;
	}

	public String mealCount(ArrayList<String> count, int i) {
		String numberCount = count.get(i);
		String mealCount = " \"count\": <numberCount>,";
		return mealCount.replace("<numberCount>", numberCount);
	}

	public String mealIsParent(ArrayList<String> isParent, int i) {
		String mealIsParent = isParent.get(i);
		String isParentRequest = "\"isParent\": <value>";
		return isParentRequest.replace("<value>", mealIsParent);
	}

	public String rewards(ArrayList<String> allRewardValues, int mealNumber) {
		String reward = "  <rewardPayload> ";
		String rewardRequest = "";
		String rewardPayload = allRewardValues.get(mealNumber);
		rewardRequest = reward.replace("<rewardPayload>", rewardPayload);

		return rewardRequest;
	}

	public String groupsPayload(ArrayList<Integer> groupCount, ArrayList<String> groupIds, int mealNumber,
								ArrayList<String> groupRewards) {
		String groups = " \"groups\": [ <groupsPayload>],";
		String groupsPayload = "";
		String groupsRequest = "";
		String groupId = "\"groupId\": <groupId>,";
		String rewards = "<rewardsPayload>";
		String finalPayload = "";
		int count = 0;
		int numberOfgroupsInAMeal = groupCount.get(mealNumber);
		for (int i = 0; i < numberOfgroupsInAMeal; i++) {
			System.out.println("grp payload " + groupsPayload);
			groupsPayload = "{" + groupsPayload;
			System.out.println("grp payload " + groupsPayload);
			groupsPayload = groupsPayload + groupId.replace("<groupId>", groupIds.get(groupNum));
			groupNum++;
			groupsPayload = groupsPayload + rewards.replace("<rewardsPayload>", rewards(groupRewards, mealNumber));
			groupsPayload = groupsPayload + "}";
			count++;
			if (i == numberOfgroupsInAMeal - 1) {
				groupsPayload = groupsPayload;
			} else {
				groupsPayload = groupsPayload + ",";
			}
			finalPayload = finalPayload + groupsPayload;

			groupsPayload = "";

			// groupsPayload = groupsPayload + rewards(groupRewards, mealNumber);
		}

		groupsRequest = groups.replace("<groupsPayload>", finalPayload);

		return groupsRequest;
	}

	public Processor createEDVOTradeDiscount(HashMap<String, String> edvoData) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("td", "createEDVOCampaign", gameofthrones);
		String[] edvoPayload = new String[edvoData.size()];
		for (int i = 0; i < edvoData.size(); i++) {
			edvoPayload[i] = edvoData.get(Integer.toString(i));
			System.out.println("" + edvoData.get(Integer.toString(i)));
		}
		Processor processor = new Processor(service, requestHeaders, edvoPayload);
		System.out.println("In helper method");
		return processor;
	}

	public void deleteEDVOCampaignFromDB(String edvoCampaignId) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
		sqlTemplate.update(RngConstants.deleteEDVOCampaign + edvoCampaignId + RngConstants.endQuotes);

	}

	public Processor getRestaurantListOnLocationFromCollection(String lat, String lng, String page, String collectionId,
															   String header) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("User-Agent", header);
		String[] urlparams = new String[] { lat, lng, page, collectionId, "0" };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "listing_v5", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, null, urlparams);
		return processor;

	}

	public String createPercentageTD(String restId) throws IOException {

		String randomRestaurant = restId;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 30).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		return createTDResponse;
	}
    public String createPercentageTD(String restId, boolean clearActiveTD) throws IOException {

        String randomRestaurant = restId;
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
        if(clearActiveTD) {
            disabledActiveTD(randomRestaurant);
        }
        CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 30).toInstant().getEpochSecond() * 1000))
                .campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                .ruleDiscount("Percentage", "Restaurant", "20", "0", "2000").build();
        Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        return createTDResponse;
    }

	public String createPercentageTD(boolean clearActiveTD, String... restId) throws IOException {

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(String rest: restId) {
            if(clearActiveTD) {
                disabledActiveTD(rest);
            }
		    restaurantLists1.add(new RestaurantList(rest, "Test", new ArrayList<>()));
		}
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 30).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "20", "0", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		return  processor.ResponseValidator.GetBodyAsText();
	}

	public HashMap<String, String> createTDUpperCapping() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createTDWithUpperCapping() throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "50").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);

		return Keys;
	}

	public HashMap<String, String> createPercentageWithNoMinAmountAtRestaurantLevel(String restId) throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = restId;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithNoMinAmountAtRestaurantLevel(String restId) throws IOException {

		// List rlist = getRestaurantListing("12.9719", "77.6412", "0");
		String randomRestaurant = restId; // rlist.get(commonAPIHelper.getRandomNo(0,
		// rlist.size() - 1)).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addHours(new Date(), 5000).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", "100", "200").userRestriction(false).timeSlotRestriction(false)
				.firstOrderRestriction(false).taxesOnDiscountedBill(false).commissionOnFullBill(false)
				.dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TID", createdTD_Id);
		return Keys;
	}
	
	public Processor getOfferListingProcessor(String lat, String lng, String source_page, String tid) {
		HashMap<String, String> headers = new HashMap();
		headers.put("tid", tid);
		String[] urlParams = new String[] { lat, lng, source_page };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "offerlisting", gameofthrones);
		Processor processor = new Processor(service, headers, null, urlParams);
		return processor;

	}
	
	public Processor getSlugListingProcessor(String areaId) {
		String[] urlParams = new String[] { areaId };
		GameOfThronesService service = new GameOfThronesService("restaurantlisting", "sluglisting", gameofthrones);
		Processor processor = new Processor(service, null, null, urlParams);
		return processor;

	}
	
    public Processor getPopListingProcessor(String lat,String lng,String page_type, boolean redirectToMock)
    {
        redirectToMock = false;
        String[] urlParams = new String[]{lat,lng,page_type};
        HashMap<String,String> headers = new HashMap();
        headers.put("Content-Type","application/json");
        GameOfThronesService service = new GameOfThronesService(redirectToMock?"restaurantlistingmock":"restaurantlisting","poplisting",gameofthrones);
        return  new Processor(service,null,null,urlParams);
    }

	public Processor getPopListingProcessorMock(String lat,String lng,String page_type)
	{
		String[] urlParams = new String[]{lat,lng,page_type};
		HashMap<String,String> headers = new HashMap();
		headers.put("Content-Type","application/json");
		GameOfThronesService service = new GameOfThronesService("restaurantlistingmock","poplisting",gameofthrones);
		Processor processors = new Processor(service,null,null,urlParams);
		return processor;
	}

    public Processor getDishDiscoveryCollectionProcessor(String lat,String lng,String type,String parentCollectionId,String collecitonId)
    {
        String[] urlParams = new String[]{collecitonId,lat,lng,type,parentCollectionId};
        GameOfThronesService service = new GameOfThronesService("restaurantlisting","dishdiscovery",gameofthrones);
        Processor processor = new Processor(service,null,null,urlParams);
        return processor;
    }
    
    public Processor getBigCardProcessor(String file_url)
    {
        String payload[] = new String[]{};
        GameOfThronesService service = new GameOfThronesService("restaurantlisting","bigcardapi",gameofthrones);
        return new Processor(service,null, new String[]{file_url});
    }

    public Processor getPreOrderProcessor(String lat,String lng,String start_time,String end_time, boolean redirectToMock)
    {
        redirectToMock = false;
        String[] urlParams = new String[]{lat,lng,start_time,end_time};
        GameOfThronesService service = new GameOfThronesService(redirectToMock?"restaurantlistingmock":"restaurantlisting","preorder",gameofthrones);
        Processor processor = new Processor(service,null,null,urlParams);
        return processor;
    }

    public String createLayoutContextMap(String contextName,String k1,String v1,String k2,String v2,String k3,String v3, String isActive,String priority)
    {
        String contexId = createContext(contextName,k1,v1,k2,v2,k3,v3,isActive,priority);
        String layoutId = createLayout();
        String[] urlparams = new String[]{layoutId,contexId};
        HashMap<String,String> headers = new HashMap<>();
        headers.put("Cache-Control","no-cache");
        headers.put("content-type","application/json");
        GameOfThronesService service = new GameOfThronesService("restaurantlisting","layoutcontextmapping",gameofthrones);
        String response = new Processor(service,headers,null,urlparams).ResponseValidator.GetBodyAsText();
        return contexId;
    }
    
	public String createContext(String contextName,String k1,String v1,String k2,String v2,String k3,String v3, String isActive,String priority)
	{
		HashMap<String,String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		GameOfThronesService service = new GameOfThronesService("restaurantlisting","contextcreation",gameofthrones);
		String[] payload = new String[]{contextName,k1,v1,k2,v2,k3,v3,isActive,priority};

		String response = new Processor(service,headers,payload).ResponseValidator.GetBodyAsText();
		System.out.println("+++++++++++++createdLayoutId=============>>>>>>>>>>>>>>>"+JsonPath.read(response,"$.data.id").toString());
		return JsonPath.read(response,"$.data.id").toString();
	}


	public String createLayout111()
	{

		GameOfThronesService service = new GameOfThronesService("restaurantlisting","layoutcreation",gameofthrones);
		String response = new Processor(service,null,null).ResponseValidator.GetBodyAsText();
		return JsonPath.read(response,"$.data.id").toString();
    }



	public String createLayout()
	{
		HashMap<String,String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");

		GameOfThronesService service = new GameOfThronesService("restaurantlisting","layoutcreation",gameofthrones);
		String response = new Processor(service,headers).ResponseValidator.GetBodyAsText();
		return JsonPath.read(response,"$.data.id").toString();

	}


	public void layoutContextMap(String layoutId,String contextId)
    {
        HashMap<String,String> headers = new HashMap<>();
        headers.put("Cache-Control","no-cache");
        headers.put("Content-Type","application/json");

        String[] urlParam = new String[]{layoutId,contextId};

        GameOfThronesService service = new GameOfThronesService("restaurantlisting","contextlayoutmap",gameofthrones);
        String response = new Processor(service,headers,null,urlParam).ResponseValidator.GetBodyAsText();
    }

    public void disableContext(String contextId)
    {
        String[] payload = new String[]{contextId};
        HashMap<String,String> headers = new HashMap<>();
        headers.put("Content-Type","application/json");
        GameOfThronesService service = new GameOfThronesService("restaurantlisting","contextdeletion",gameofthrones);
        String response = new Processor(service,headers,payload).ResponseValidator.GetBodyAsText();
    }
//
//
//	public String createLayout1()
//	{
//		HashMap<String,String> headers = new HashMap<>();
//		headers.put("Content-Type","application/json");
//
//		GameOfThronesService service = new GameOfThronesService("restaurantlisting","layoutcreation",gameofthrones);
//		String response = new Processor(service,headers).ResponseValidator.GetBodyAsText();
//		return JsonPath.read(response,"$.data.id").toString();
//
//	}

	public int enableRestaurantAsServiceableWithBanner(String restId) {
		String query = "select * from restaurant r inner join area a on r.area_code=a.id where r.id=" + restId + ";";
		Object obj = dhm.dbhelperget(query, "zone_id");
		int zoneId = (int) obj;
		ddh.zoneOpenCloseSwitch(zoneId);
		return zoneId;
	}

    public Processor getContextMatchProcessor(String lat,String lng,String id)
    {
        HashMap<String,String>headers =  new HashMap<>();
        headers.put("Cache-Control","no-cache");
        headers.put("Content-Type","application/json");
        String[] urlParam = new String[]{lat,lng,id};
        String[] payload = new String[]{"RESTAURANT_LISTING","280","Swiggy-Android","1","1","nat_account","4"};

        GameOfThronesService service = new GameOfThronesService("restaurantlisting","contextmatch",gameofthrones);

        return new Processor(service,headers,payload,urlParam);
    }

    public List getRestaurantsList(String response)
    {
        ArrayList al = new ArrayList();
        List<String>cardTypes = JsonPath.read(response,"$.data..cardType");
        int i=0;
        for(String cardType:cardTypes)
        {
            if(cardType.equalsIgnoreCase("restaurant"))
            {
                al.add(JsonPath.read(response,"$.data.cards["+i+"].data.data.id").toString());
            }
            i++;
        }

        return al;
    }

	public String createFreeDeliveryTD(String restId, boolean is_surge, boolean firstOrderRestriction,
			boolean restaurantFirstOrder, boolean userRestriction) {

		String randomRestaurant = restId;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		disabledActiveTD(restId);
		ArrayList al = new ArrayList();
		al.add(new FreeDeliveryRestaurantList(restId));
		CreateFreeDeliveryEntry FreeDeliveryDiscount1;
		Processor processor = null;
		try {
			FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
					.header("RestaurantLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
					.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0")
					.createdBy("Automation").discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
					// .slots(slots)
					.ruleDiscount("FREE_DELIVERY", "Restaurant", "99.0").userRestriction(userRestriction)
					.timeSlotRestriction(false).firstOrderRestriction(firstOrderRestriction)
					.taxesOnDiscountedBill(false).commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
					.dormant_user_type("ZERO_DAYS_DORMANT").build();
			processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor.ResponseValidator.GetBodyAsText();
	}

	public String createFreeDeliveryTD(boolean is_surge, boolean firstOrderRestriction,
									   boolean restaurantFirstOrder, boolean userRestriction, boolean clearActiveTD,String... restId) {
        ArrayList al = new ArrayList();
	    for(String randomRestaurant: restId) {
            List<RestaurantList> restaurantLists1 = new ArrayList<>();
            restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
            if (clearActiveTD) {
                disabledActiveTD(randomRestaurant);
            }

            al.add(new FreeDeliveryRestaurantList(randomRestaurant));
        }
		CreateFreeDeliveryEntry FreeDeliveryDiscount1;
		Processor processor = null;
		try {
			FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
					.header("RestaurantLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addHours(new Date(), 15).toInstant().getEpochSecond() * 1000))
					.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0")
					.createdBy("Automation").discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
					// .slots(slots)
					.ruleDiscount("FREE_DELIVERY", "Restaurant", "99.0").userRestriction(userRestriction)
					.timeSlotRestriction(false).firstOrderRestriction(firstOrderRestriction)
					.taxesOnDiscountedBill(false).commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
					.dormant_user_type("ZERO_DAYS_DORMANT").build();
			processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor.ResponseValidator.GetBodyAsText();
	}

	public void errorCodeHandler(Processor processor, String api) {
		if (processor.ResponseValidator.GetResponseCode() == 500) {
			Assert.assertTrue(false, "Api is down=" + api);
		}
	}

	public Processor createCoupon(CreateCouponPOJO payload) {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		// tnc= new String[]
		Processor processor = null;
		try {
			GameOfThronesService service = new GameOfThronesService("cs", "createcouponPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders, new String[] { jsonHelper.getObjectToJSON(payload) });
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public String selectPaymentCode(String  CouponCode) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);

		String query = String.format(RngConstants.selectPaymentCode, CouponCode);
		System.out.println(query);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(query);
		System.out.println("----------"+list.get(0)+"----------");
		return list.toString().substring(15,21);
	}

//	public String selectDetailsForUPIHandleFromPaymentCodeMappingAudTable(String  CouponCode) {
//
//		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
//		List<Map<String, Object>> list = sqlTemplate
//				.queryForList(RngConstants.selectPaymentTypePCMA + RngConstants.endQuotes + CouponCode + RngConstants.endQuotes + RngConstants.ending);
//		System.out.println("----------"+list.get(0)+"----------");
//		return list.toString().substring(15,21);
//	}

	public String selectBrandForUPIHandleFromWPOptionsTable() {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.couponDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.selectBrands);
        System.out.println("========================="+list.get(0)+"");
		System.out.println("========================="+list.toString().substring(17,21)+"");
		return list.toString().substring(17,21);
	}


	public Processor mapCoupon(UPIPaymentContractPOJO payload) throws Exception {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		Processor processor = null;
		GameOfThronesService service = new GameOfThronesService("cs", "mapCoupon", gameofthrones);
		String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
		processor = new Processor(service, requestHeaders, payloadparams,null);
		return processor;

	}

	public Processor deletePayment(UPIPaymentContractPOJO payload) throws Exception {

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
		GameOfThronesService service = new GameOfThronesService("cs", "deletePayment", gameofthrones);
		Processor processor = new Processor(service, requestHeaders, payloadparams,null);
		return processor;

	}

	public Processor applyCouponPojo1(ApplyCouponPOJO1 payload) throws Exception{

            JsonHelper jsonHelper=new JsonHelper();
            HashMap<String, String> requestHeaders = new HashMap<String, String>();
            requestHeaders.put("Content-Type", "application/json");
            GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
            String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
            String[] urlparams = new String[] { payload.getCode() };
            Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
            return processor;

	}

    public Processor couponApply001(CouponApplyPOJO payload, String code) throws Exception{

        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService service = new GameOfThronesService("cs", "couponApplyMappingPOJO", gameofthrones);
        String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
        String[] urlparams = new String[] { code };
        Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
        return processor;

    }

    public Processor getDetailsForCoupons(String userId, String cityId) throws Exception {

	    HashMap<String, String> requestHeaders = new HashMap<>();
	    requestHeaders.put("Content-Type", "application/json");
	    GameOfThronesService service = new GameOfThronesService("cs", "getDetailsForCoupons", gameofthrones);
	    String[] urlparams = new String[] {userId,cityId};
	    Processor processor = new Processor(service, requestHeaders, null, urlparams);
	    return processor;
    }

	public  Processor removeCouponUsagesCount(CouponUsageCountRemovePOJO payload) throws Exception{

		JsonHelper jsonHelper=new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
			GameOfThronesService service = new GameOfThronesService("cs", "removeCouponUsageCount", gameofthrones);
			String[] payloadparams = new String[] { jsonHelper.getObjectToJSON(payload)};
			String[] pathParams = new String[] { payload.getCode()};
			Processor	processor = new Processor(service, requestHeaders,payloadparams,pathParams);

		return processor;
	}

	public Processor useCouponUsagesCount(CouponUsageCountUsePOJO payload) throws IOException{

		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		String[] pathParams = new String[] { payload.getCode()};
		Processor processor = null;
			GameOfThronesService service = new GameOfThronesService("cs", "useCouponUsageCount", gameofthrones);
			processor = new Processor(service, requestHeaders, pathParams,new String[] { jsonHelper.getObjectToJSON(payload) });

		return processor;
	}

	public HashMap<String, String> getMenuItemWithoutVariantWithMinAmt(List<String> restList, Integer minAmt) {
		String menuItem;
		HashMap<String, String> menuItemAndPrice = new HashMap<>();
		for (String rest : restList) {
			String restroMenuResponse = snd.getRestaurantMenu(rest).ResponseValidator.GetBodyAsText();
			// select a menu item
			menuItem = checkoutHelper.getMenuItemWithoutVariant(restroMenuResponse, minAmt);
			if (menuItem != "") {
				menuItemAndPrice.put("menuItemID", menuItem);
				menuItemAndPrice.put("restID", rest);
				menuItemAndPrice.put("itemPrice", String.valueOf(
						(int) JsonPath.read(restroMenuResponse, "$.data.menu.items." + menuItem + ".price") / 100));
				break;

			}
		}
		return menuItemAndPrice;
	}
//	public String getRandomItemWithoutVariantWithMinMaxAmt(String )

	public Processor createFlatWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount, String flatTDAmount,
			String restID) {
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
		disabledActiveTD(restID);
		CreateFlatTdEntry flatTradeDiscount1;
		Processor processor = null;
		try {
			flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel").header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

			processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
    }

    public Processor createFlatWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount, String flatTDAmount,
                                                                          String restID, boolean firstOrderRestriction, boolean clearActiveTD) {
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
        if(clearActiveTD) {
            disabledActiveTD(restID);
        }
        CreateFlatTdEntry flatTradeDiscount1;
        Processor processor = null;
        try {
            flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel").header("RestauratLevelDiscount")
                    .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                    .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                    .campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
                    .discountLevel("Restaurant").restaurantList(restaurantLists1)
                    .ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
                    .timeSlotRestriction(false).firstOrderRestriction(firstOrderRestriction).taxesOnDiscountedBill(false)
                    .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

            processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return processor;
    }

    public static Processor createCoupon2(String coupon_type, String name, String code, String description,
    		String is_private, String valid_from, String valid_till, String total_available, String totalPerUser,
    		String minAmountCart, String customer_restriction, String city_restriction, String area_restriction,
    		String restaurant_restriction, String category_restriction, String item_restriction,
    		String discount_percentage, String discount_amount, String free_shipping, String free_gifts,
    		String first_order_restriction, String preferred_payment_method, String user_client,
    		String slot_restriction, String createdOn, String image, String coupon_bucket, String minQuantityCart,
    		String cuisine_restriction, String discount_item, String usage_count, String expiry_offset,
    		String applicable_with_swiggy_money, String is_bank_discount, String refund_source, String pg_message,
    		String supported_ios_version, String updated_by, String title, String tnc, String logo) {
    	HashMap<String, String> requestHeaders = new HashMap<String, String>();
    	requestHeaders.put("Content-Type", "application/json");
    	String unique_code = code + getRandomPostfix();
    	String validFrom = getCurrentDate();
    	System.out.println("Valid From date in Coupon" + validFrom);
    	String validTill = getFutureDate();
    	System.out.println("Valid From date in Coupon" + validTill);
    	// tnc= new String[]
    	GameOfThronesService service = new GameOfThronesService("cs", "createcoupon_new", gameofthrones);
    	String[] payloadparams = { coupon_type, name, unique_code, description, is_private, validFrom, validTill,
    			total_available, totalPerUser, minAmountCart, customer_restriction, city_restriction, area_restriction,
    			restaurant_restriction, category_restriction, item_restriction, discount_percentage, discount_amount,
    			free_shipping, free_gifts, first_order_restriction, preferred_payment_method, user_client,
    			slot_restriction, createdOn, image, coupon_bucket, minQuantityCart, cuisine_restriction, discount_item,
    			usage_count, expiry_offset, applicable_with_swiggy_money, is_bank_discount, refund_source, pg_message,
    			supported_ios_version, updated_by, title, tnc, logo };
    	Processor processor = new Processor(service, requestHeaders, payloadparams);

    	return processor;
    }

    public String fyiContextPopup()
	{
		HashMap<String,String> headers = new HashMap<>();
		headers.put("Content-Type","application/json");
		GameOfThronesService service = new GameOfThronesService("sand","contextpopup",gameofthrones);

		String[] payload = new String[]{RngConstants.city,RngConstants.androidUserAgent,RngConstants.versionCode200,};

		String response = new Processor(service,headers,payload).ResponseValidator.GetBodyAsText();
		if(processor.ResponseValidator.GetResponseCode()==503)
		{
			Assert.assertFalse(true);
		}
		List<String> datalist = JsonPath.read(response,"$.data");
		if(datalist.size()==0)
		{
			Assert.assertTrue(false,"No FYI banner is coming for given context");

		}
		String fyiId = JsonPath.read(response,"$.data[1].id").toString();
		return fyiId;
	}

		

	public Processor createPercentageWithFirstOrderRestrictionAtRestaurantLevel(String percentage, String minCartAmount,
			String discountCap, String restID) {

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
		disabledActiveTD(restID);
		CreateTdEntry percetageTradeDiscount1;
		Processor processor = null;
		try {
			percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					.ruleDiscount("Percentage", "Restaurant", percentage, minCartAmount, discountCap)
					.firstOrderRestriction(true).build();
			processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}	

//	public String getRandomItemWithoutVariantWithMinMaxAmt(String )
	public Processor updateExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel(String minCartAmount, String flatTDAmount,
			String restID, Integer tdID) {
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
		CreateFlatTdEntry flatTradeDiscount1;
		Processor processor = null;
		try {
			flatTradeDiscount1 = new CreateFlatTdBuilder().id(tdID).nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(restaurantLists1).updatedBy("Dev")
					.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
					.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

			processor = editTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}
	public Processor updateTDFirstOrderRestrictionAtRestaurantLevel(String percentage, String minCartAmount,
			String discountCap, String restID,Integer tdID) {
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
		CreateTdEntry percentTradeDiscount;
		Processor processor = null;
		try {
			percentTradeDiscount =  new CreateTdBuilder().id(tdID).nameSpace("RestauratLevel").updatedBy("dev")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					.ruleDiscount("Percentage", "Restaurant", percentage, minCartAmount, discountCap)
					.firstOrderRestriction(true).build();

			processor = editTD(jsonHelper.getObjectToJSON(percentTradeDiscount));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}
	public Processor createExpiredTimeSlotFirstOrderRestrictionAtRestaurantLevel(String minCartAmount, String flatTDAmount, String restID) {
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
		CreateFlatTdEntry flatTradeDiscount1;
		Processor processor = null;
		try {
			flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), -5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
					.discountLevel("Restaurant").restaurantList(restaurantLists1).updatedBy("Dev")
					.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
					.timeSlotRestriction(true).firstOrderRestriction(true).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

			processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}
	
	public Processor  orderEditCartEvaluate(String price, String restaurantId, Boolean firstOrder,Boolean restFirstOrder, int orderEditedSource, List<Integer> campaignIds) {
		OrderEditCartV3EvaluatePOJO cartEvaluatePOJO = new OrderEditCartV3EvaluatePOJO()
				.setDefaultData(price, restaurantId, firstOrder, restFirstOrder, orderEditedSource, campaignIds);
		Processor processor = null;
		try {
			processor = cartEvaluate(jsonHelper.getObjectToJSON(cartEvaluatePOJO));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	
	public Processor  edvoOrderEditCartEvaluate(String price, String restaurantId, Boolean firstOrder,
			Boolean restFirstOrder, int orderEditedSource, List<Integer> campaignIds,
			Integer userID,List<EDVOCartMealItemRequest> mealItemRequest) {
		OrderEditCartV3EvaluatePOJO cartEvaluatePOJO = new OrderEditCartV3EvaluatePOJO()
				.setDefaultData(price, restaurantId, firstOrder, restFirstOrder, orderEditedSource, campaignIds)
				.withUserId(userID)
				.withItemRequests(null);
		cartEvaluatePOJO.setMealItemRequest(mealItemRequest);
		Processor processor = null;
		try {
			String payload = jsonHelper.getObjectToJSON(cartEvaluatePOJO);
			processor = cartEvaluate(payload);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor  orderEditTDCartEvaluate(String price, String restaurantId, Boolean firstOrder,Boolean restFirstOrder, int orderEditedSource, List<Integer> campaignIds, Integer userID) {
		OrderEditCartV3EvaluatePOJO cartEvaluatePOJO = new OrderEditCartV3EvaluatePOJO()
				.setDefaultData(price, restaurantId, firstOrder, restFirstOrder, orderEditedSource, campaignIds)
				.withUserId(userID);
		Processor processor = null;
		try {
			processor = cartEvaluate(jsonHelper.getObjectToJSON(cartEvaluatePOJO));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}

	public Processor couponOrderEditCartEvaluate(String couponCode, Integer menuItemID, String restID, Integer userID, Boolean firstOrder, Integer editInitSource, Double cartTotal, Integer quantity) {
		OrderEditCouponEvaluatePOJO cartEvaluatePOJO = new OrderEditCouponEvaluatePOJO().setDefaultData(couponCode, menuItemID, restID, userID, firstOrder, editInitSource, cartTotal);
		cartEvaluatePOJO.getCart().getCartPrice().withQuantity(quantity);
		Processor processor = null;
		try {
			String payload = jsonHelper.getObjectToJSON(cartEvaluatePOJO);
			JsonHelper jsonHelper = new JsonHelper();
			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			requestHeaders.put("Authorization", RngConstants.basic_auth);
			GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders,
					new String[] { payload }, new String[] {couponCode});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	public Processor couponOrderEditCartEvaluateWithUserClient(String couponCode, Integer menuItemID, String restID, Integer userID, Boolean firstOrder, Integer editInitSource, Double cartTotal, Integer quantity,String userAgent) {
		OrderEditCouponEvaluatePOJO cartEvaluatePOJO = new OrderEditCouponEvaluatePOJO().setDefaultData(couponCode, menuItemID, restID, userID, firstOrder, editInitSource, cartTotal);
		cartEvaluatePOJO.getCart().getCartPrice().withQuantity(quantity);
		cartEvaluatePOJO.getHeaders().setUserAgent(userAgent);
		Processor processor = null;
		try {
			String payload = jsonHelper.getObjectToJSON(cartEvaluatePOJO);
			JsonHelper jsonHelper = new JsonHelper();
			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			requestHeaders.put("Authorization", RngConstants.basic_auth);
			GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders,
					new String[] { payload }, new String[] {couponCode});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	public Processor couponOrderEditWithCurrentTimeCartEvaluate(String couponCode, Integer menuItemID, String restID, Integer userID, Boolean firstOrder, Integer editInitSource, Double cartTotal, Integer quantity) {
		OrderEditCouponEvaluatePOJO cartEvaluatePOJO = new OrderEditCouponEvaluatePOJO().setDefaultData(couponCode, menuItemID, restID, userID, firstOrder, editInitSource, cartTotal);
		cartEvaluatePOJO.withTime(System.currentTimeMillis()).getCart().getCartPrice().withQuantity(quantity);
		Processor processor = null;
		try {
			String payload = jsonHelper.getObjectToJSON(cartEvaluatePOJO);
			JsonHelper jsonHelper = new JsonHelper();
			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			requestHeaders.put("Authorization", RngConstants.basic_auth);
			GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders,
					new String[] { payload }, new String[] {couponCode});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	public Processor couponOrderEditCartEvaluateWithAreaListing(String couponCode, Integer menuItemID, String restID, Integer userID, Boolean firstOrder, Integer editInitSource, Double cartTotal, Integer areaEntityID, Integer cityEntityID, Integer quantity) {
		OrderEditCouponEvaluatePOJO cartEvaluatePOJO = new OrderEditCouponEvaluatePOJO().setDefaultData(couponCode, menuItemID, restID, userID, firstOrder, editInitSource, cartTotal);
				cartEvaluatePOJO.getCart().setAreaAndCityEntityID(areaEntityID, cityEntityID);
				cartEvaluatePOJO.getCart().getCartPrice().withQuantity(quantity);
		Processor processor = null;
		try {
			String payload = jsonHelper.getObjectToJSON(cartEvaluatePOJO);
			JsonHelper jsonHelper = new JsonHelper();
			HashMap<String, String> requestHeaders = new HashMap<String, String>();
			requestHeaders.put("Content-Type", "application/json");
			requestHeaders.put("Authorization", RngConstants.basic_auth);
			GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
			processor = new Processor(service, requestHeaders,
					new String[] { payload }, new String[] {couponCode});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}
	public Processor editTD(String payload) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders = getDefaultHeaders();
		GameOfThronesService service = new GameOfThronesService("td", "editTD", gameofthrones);
		String[] payloadparams = new String[] { payload };
		Processor processor = new Processor(service, requestHeaders, payloadparams);
		return processor;
	}
		
	public Processor cartEvaluate(String payload) {
		JsonHelper jsonHelper = new JsonHelper();
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		requestHeaders.put("Authorization", RngConstants.basic_auth);
		GameOfThronesService service = new GameOfThronesService("td", "cartV3EvaluatePOJO", gameofthrones);
		Processor processor = new Processor(service, requestHeaders,
				new String[] { payload });
		return processor;

	}
	
	public String createAndMapPolygon(){

		Processor processor =snd.cityPolygon("Test","Test", RngConstants.bangalorePolygons);
		String response = processor.ResponseValidator.GetBodyAsText();
		int polygonId = JsonPath.read(response,"$.data");
		String 	polygonID = polygonId+"";
		snd.mapCityPolygonWithCity(RngConstants.city,polygonID);
		return polygonID;
	}
	
    public String getMidNightTimeOfCurrentDay()
	{
		Calendar c = Calendar.getInstance();
		long now = c.getTimeInMillis();
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);

		return ""+c.getTimeInMillis();
	}
	//@Overload
	public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(String minCartAmount, String restID, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type,boolean is_surge, boolean disableAllTD) throws IOException {

		List<Integer> restIds=new ArrayList<>();
		restIds.add(Integer.parseInt(restID));
		//String.valueOf(restID.get(0))
		dormant_user_type = "ZERO_DAYS_DORMANT";


		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		ArrayList al = new ArrayList();
		if (disableAllTD) {
			for (Integer restId : restIds) {
				mhelper.disableCampaignByRestId(String.valueOf(restId));
			}
		}
		for (Integer restId : restIds) {
			al.add(new FreeDeliveryRestaurantList(String.valueOf(restId)));
		}



		CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
				.header("RestaurantLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
				// .slots(slots)
				.ruleDiscount("FREE_DELIVERY", "Restaurant", "99.0").userRestriction(userRestriction)
				.timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
				.dormant_user_type(dormant_user_type).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIds.size();i++) {
			Keys.put("restId", restIds.get(i));
			Keys.put("restId" + i, restIds.get(i));
		}
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	//@Overload
	public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevel(String minCartAmount, List<Integer> restIds, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type,boolean is_surge, boolean disableAllTD) throws IOException {

		//String.valueOf(restID.get(0))
		dormant_user_type = "ZERO_DAYS_DORMANT";


		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		ArrayList al = new ArrayList();
		if (disableAllTD) {
			for (Integer restId : restIds) {
				mhelper.disableCampaignByRestId(String.valueOf(restId));
			}
		}
		for (Integer restId : restIds) {
			al.add(new FreeDeliveryRestaurantList(String.valueOf(restId)));
		}



		CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
				.header("RestaurantLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 1).toInstant().getEpochSecond() * 1000))
				.campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
				// .slots(slots)
				.ruleDiscount("FREE_DELIVERY", "Restaurant", "99.0").userRestriction(userRestriction)
				.timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
				.dormant_user_type(dormant_user_type).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIds.size();i++) {
			Keys.put("restId", restIds.get(i));
			Keys.put("restId" + i, restIds.get(i));
		}
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}
    public HashMap<String, String> createFreeDeliveryTDWithNoMinAmountAtRestaurantLevelThrityDayDormant(String minCartAmount, List<Integer> restIds, boolean firstorderRestriction, boolean restaurantFirstOrder, boolean userRestriction, String dormant_user_type,boolean is_surge, boolean disableAllTD) throws IOException {

        //String.valueOf(restID.get(0))
        dormant_user_type = "THIRTY_DAYS_DORMANT";
        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        ArrayList al = new ArrayList();
        if (disableAllTD) {
            for (Integer restId : restIds) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        for (Integer restId : restIds) {
            al.add(new FreeDeliveryRestaurantList(String.valueOf(restId)));
        }
        CreateFreeDeliveryEntry FreeDeliveryDiscount1 = new CreateFreeDeliveryTdBuilder().nameSpace("RestaurantLevel")
                .header("RestaurantLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                .campaign_type("FREE_DELIVERY").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
                .discountLevel("Restaurant").restaurantList(al).is_surge(is_surge)
                // .slots(slots)
                .ruleDiscount("FREE_DELIVERY", "Restaurant", "99.0").userRestriction(userRestriction)
                .timeSlotRestriction(false).firstOrderRestriction(firstorderRestriction).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).restaurantFirstOrder(restaurantFirstOrder)
                .dormant_user_type(dormant_user_type).build();
        Processor processor = createTD(jsonHelper.getObjectToJSON(FreeDeliveryDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

        HashMap Keys = new HashMap();
        int get_status = JsonPath.read(createTDResponse, "$.statusCode");
        if( get_status == 0)
            Keys.put("status","success");
        else
            Keys.put("status","failed");
        for(int i=0;i<restIds.size();i++) {
            Keys.put("restId", restIds.get(i));
            Keys.put("restId" + i, restIds.get(i));
        }
        Keys.put("minCartAmount", minCartAmount);
        Keys.put("TDID", createdTD_Id);

        return Keys;
    }
	public HashMap<String, String> createFeebieTDWithNoMinAmountAtRestaurantLevel(String minCartAmount,  List<Integer> restIds,
																				  String freebieItemId,boolean disableAllTD) throws IOException {


//		List<FreebieRestaurantList> al1 = new ArrayList();
//		al1.add(new FreebieRestaurantList(Integer.parseInt(restId)));
//		CreateFreebieTDEntry FreebieTradeDiscount1 = new CreateFreebieTdBuilder().nameSpace("RestauratLevel")
//				.header("RestauratLevelDiscount")
//				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
//				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
//				.campaign_type("Freebie").restaurant_hit("100").enabled("true").swiggy_hit("0").createdBy("Automation")
//				.discountLevel("Restaurant").restaurantList(al1)
//				.ruleDiscount("Freebie", "Restaurant", minCartAmount, freebieItemId).userRestriction("false")
//				.timeSlotRestriction("false").firstOrderRestriction("false").taxesOnDiscountedBill("false")
//				.commissionOnFullBill("false").dormant_user_type("ZERO_DAYS_DORMANT").build();

        ArrayList al = new ArrayList();
        for (Integer restId : restIds) {
            al.add(new RestIdList(String.valueOf(restId)));
        }
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();

        for (Integer restId : restIds)
			mhelper.disableCampaignByRestId(String.valueOf(restId));


		CreateFreebieTDEntryNewPojo freebieTradeDiscount1= new CreateFreebieTDEntryNewPojo();
		RuleDiscount discount =new RuleDiscount();
		discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("0").withItemId("1283271");



		freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
				.withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(getMidNightTimeOfCurrentDay())
				.withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.withRestaurantList(al)
				.withRuleDiscount(discount)
				.withRestaurantHit("100")
				.withFirstOrderRestriction(false)
				.withIsSurge(true)
				.withSlots(new ArrayList<>())
				.withSurgeApplicable(true)
				.withTaxesOnDiscountedBill(false)
				.withTimeSlotRestriction(false)
				.withUserRestriction(false)
				.withIsSuper(false)
				.withRestaurantFirstOrder(false).withDormantUserType("ZERO_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");


		Processor processor = createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");

        for(int i=0;i<restIds.size();i++) {
            Keys.put("restId", restIds.get(i));
            Keys.put("restId" + i, restIds.get(i));
        }
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");
		return Keys;
	}

	//@Overload
	public HashMap<String, String> createPercentageWithMinAmountAtRestaurantLevel(List<Integer> restIDs, boolean disableAllActiveTD) throws IOException {



		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        if (disableAllActiveTD) {
            for (Integer restId : restIDs) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        for(int restid: restIDs) {
            restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
        }



		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	//@Overload
	public HashMap<String, String> createPercentageWithMinAmountAtRestaurantLevel(String restID, boolean disableAllActiveTD) throws IOException {



		List<Integer> restIDs=new ArrayList<>();
		restIDs.add(Integer.parseInt(restID));
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		if (disableAllActiveTD) {
			for (Integer restId : restIDs) {
				mhelper.disableCampaignByRestId(String.valueOf(restId));
			}
		}
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(int restid: restIDs) {
			restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
		}



		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "100", "2000").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIDs.size();i++) {
			Keys.put("restId", restIDs.get(i));
			Keys.put("restId" + i, restIDs.get(i));
		}
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}


	//@Overload
	public HashMap<String, String> createFlatWithMinCartAmountAtRestaurantLevel(String restID, String minCartAmount, String flatTDAmount,boolean disableAllTD) throws IOException {
		List<Integer> restIDs=new ArrayList<>();
		restIDs.add(Integer.parseInt(restID));
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		ArrayList<Integer> al = new ArrayList<Integer>();

		for (Integer restId : restIDs) {
			mhelper.disableCampaignByRestId(String.valueOf(restId));
			al.add(restId);
		}

		int k=0;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(int restid: al) {
			restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test"+k++, new ArrayList<>()));
		}

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 2).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIDs.size();i++) {
			Keys.put("restId", restIDs.get(i));
			Keys.put("restId" + i, restIDs.get(i));
		}
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}
	//@Overload
	public HashMap<String, String> createFlatWithMinCartAmountAtRestaurantLevel(List<Integer> restIDs, String minCartAmount, String flatTDAmount,boolean disableAllTD) throws IOException {
        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        ArrayList<Integer> al = new ArrayList<Integer>();

        for (Integer restId : restIDs) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
                al.add(restId);
        }

        int k=0;
	    List<RestaurantList> restaurantLists1 = new ArrayList<>();
        for(int restid: al) {
            restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test"+k++, new ArrayList<>()));
        }

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 1).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	//@Overload
	public HashMap<String, String> createPercentageWithThirtyDaysDormantAtRestaurantLevel(List<Integer> restIDs, boolean disableAllActiveTD) throws IOException {

		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		if (disableAllActiveTD) {
			for (Integer restId : restIDs) {
				mhelper.disableCampaignByRestId(String.valueOf(restId));
			}
		}
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(int restid: restIDs) {
			restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
		}
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// changed THIRTY
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").dormant_user_type("THIRTY_DAYS_DORMANT")
				.build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIDs.size();i++) {
			Keys.put("restId", restIDs.get(i));
			Keys.put("restId" + i, restIDs.get(i));
		}
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	//@Overload
	public HashMap<String, String> createFlatWithThirtyDayDormantAtRestaurantLevel(List<Integer> restIDs,String minCartAmount, String flatTDAmount, boolean disableAllActiveTD) throws IOException {

        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        ArrayList<Integer> al = new ArrayList<Integer>();
        if (disableAllActiveTD) {
            for (Integer restId : restIDs) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        for(int restid: restIDs) {
            restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
        }

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				// ZERO
				.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
				.build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}
	public HashMap<String, String> createFeebieTDWithThirtyDayDormantAtRestaurantLevel(String minCartAmount,
																					   List<Integer> restIDs, String freebieItemId, boolean disableAllTD) throws IOException {

		//List<RestIdList> al = new ArrayList();
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        if (disableAllTD) {
            for (Integer restId : restIDs) {

                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        List<RestIdList> restaurantLists1 = new ArrayList<RestIdList>();
        for(int restid: restIDs) {
            restaurantLists1.add(new RestIdList(String.valueOf(restid)));
        }


		CreateFreebieTDEntryNewPojo freebieTradeDiscount1= new CreateFreebieTDEntryNewPojo();
		RuleDiscount discount =new RuleDiscount();

		discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("99").withItemId("1283271");



		freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
				.withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.withRestaurantList(restaurantLists1)
				.withRuleDiscount(discount)
				.withRestaurantHit("100")
				.withRestaurantList(restaurantLists1)
				.withFirstOrderRestriction(false)
				.withIsSurge(true)
				.withSlots(new ArrayList<>())
				.withSurgeApplicable(true)
				.withTaxesOnDiscountedBill(false)
				.withTimeSlotRestriction(false)
				.withUserRestriction(false)
				.withIsSuper(false)
				// Change Thirty
				.withRestaurantFirstOrder(false).withDormantUserType("THIRTY_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");



		Processor processor = createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		/*
		 * System.out.println(processor.RequestValidator.GetBodyAsText()+
		 * "****************");
		 * System.out.println(createTDResponse+"============================");
		 * Processor availibilityProcessor = SnditemAvailibility(freebieItemId); String
		 * availibilityProcessorResponse =
		 * availibilityProcessor.ResponseValidator.GetBodyAsText(); String isCheck =
		 * JsonPath.read(availibilityProcessorResponse,
		 * "$.availabilityResponse..opened").toString(); Processor getTDProcessor=
		 * getTradeDiscount(createdTD_Id); String getTDResponse =
		 * getTDProcessor.ResponseValidator.GetBodyAsText(); String tdEnables =
		 * JsonPath.read(getTDResponse, "$.data..isCurrentlyActive").toString();
		 */
		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");

        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}
	//@Overload
	public HashMap<String, String> createPercentageWithSwiggyFirstOrderAtRestaurantLevel( List<Integer> restIDs, boolean disableAllActiveTD) throws IOException {


        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        if (disableAllActiveTD) {
            for (Integer restId : restIDs) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        for(int restid: restIDs) {
            restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
        }
		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").firstOrderRestriction(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");

        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }

		Keys.put("TDID", createdTD_Id);
		return Keys;
	}


	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
																						String flatTDAmount, boolean disableAllTD) throws IOException {

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant =
		// rlist.get(commonAPIHelper.getRandomNo(0,(rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		if (disableAllTD)
			disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		Keys.put("restId", randomRestaurant);
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithFirstOrderRestrictionAtRestaurantLevel(List<Integer> restIDs, String minCartAmount,
																						String flatTDAmount, Boolean disableAllActiveTD) throws IOException {

        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        if (disableAllActiveTD) {
            for (Integer restId : restIDs) {
                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        for(int restid: restIDs) {
            restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
        }

		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(true).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();

        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
        int get_status = JsonPath.read(createTDResponse, "$.statusCode");
        if( get_status == 0)
            Keys.put("status","success");
        else
            Keys.put("status","failed");
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}
	//@Overload
	public HashMap<String, String> createFeebieTDWithFirstOrderRestrictionAtRestaurantLevel( List<Integer> restIDs, String minCartAmount, String freebieItemId, boolean disableAllTD) throws IOException {

//		List<RestIdList> al = new ArrayList();
//		al.add(new RestIdList(restId));
//		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
//		if (disableAllTD)
//			mhelper.disableCampaignByRestId(restId);

        List<RestIdList> al = new ArrayList();
        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
        if (disableAllTD) {
            for (Integer restId : restIDs) {

                mhelper.disableCampaignByRestId(String.valueOf(restId));
            }
        }
        for(int restid: restIDs) {
            al.add(new RestIdList(String.valueOf(restid)));
        }


		CreateFreebieTDEntryNewPojo freebieTradeDiscount1= new CreateFreebieTDEntryNewPojo();
		RuleDiscount discount =new RuleDiscount();

		discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("99").withItemId("1283271");


		freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
				.withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
				.withRestaurantList(al)
				.withRuleDiscount(discount)
				.withRestaurantHit("100")
				.withFirstOrderRestriction(true)
				.withIsSurge(true)
				.withSlots(new ArrayList<>())
				.withSurgeApplicable(true)
				.withTaxesOnDiscountedBill(false)
				.withTimeSlotRestriction(false)
				.withUserRestriction(false)
				.withIsSuper(false)
				.withRestaurantFirstOrder(false).withDormantUserType("ZERO_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");

		Processor processor = createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");

        for(int i=0;i<restIDs.size();i++) {
            Keys.put("restId", restIDs.get(i));
            Keys.put("restId" + i, restIDs.get(i));
        }
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");
		return Keys;
	}
	//@Overload
	public HashMap<String, String> createPercentageWithSwiggyRestaurantOrderAtRestaurantLevel(List<Integer> restIDs, boolean disableAllActiveTD) throws IOException {

//		List<RestaurantList> restaurantLists1 = new ArrayList<>();
//		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));
//		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
//		if (disableAllActiveTD)
//			mhelper.disableCampaignByRestId(restID);

		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		if (disableAllActiveTD) {
			for (Integer restId : restIDs) {
				mhelper.disableCampaignByRestId(String.valueOf(restId));
			}
		}
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(int restid: restIDs) {
			restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
		}


		CreateTdEntry percetageTradeDiscount1 = new CreateTdBuilder().nameSpace("RestauratLevel")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Percentage", "Restaurant", "10", "0", "2000").restaurantFirstOrder(true).build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(percetageTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		for(int i=0;i<restIDs.size();i++) {
			Keys.put("restId", restIDs.get(i));
			Keys.put("restId" + i, restIDs.get(i));
		}
		Keys.put("TDID", createdTD_Id);
		return Keys;
	}

	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevel(String minCartAmount,
																								  String flatTDAmount, boolean disableAllTD) throws IOException {

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();
		String randomRestaurant = new Integer(rm.nextInt()).toString();
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		if (disableAllTD)
			disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");
		Keys.put("restId", randomRestaurant);
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("FlatTDAmount", flatTDAmount);
		Keys.put("TDID", createdTD_Id);

		return Keys;
	}

	//@overload
	public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevel(String minCartAmount, String flatTDAmount,List<Integer> restIDs)  throws Exception{

		// List rlist = getRestaurantListing("12.9719","77.6412","0");
		// String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
		// (rlist.size()-1))).toString();


		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for(int restid: restIDs) {
			restaurantLists1.add(new RestaurantList(String.valueOf(restid), "Test", new ArrayList<>()));
		}


		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header("RestauratLevelDiscount")
				.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				// .slots(slots)
				.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

        HashMap Keys = new HashMap();
        int get_status = JsonPath.read(createTDResponse, "$.statusCode");
        if( get_status == 0)
            Keys.put("status","success");
        else
            Keys.put("status","failed");

		for(int i=0;i<restIDs.size();i++) {
			Keys.put("restId", restIDs.get(i));
			Keys.put("restId" + i, restIDs.get(i));
		}
        Keys.put("minCartAmount", minCartAmount);
        Keys.put("TDID", createdTD_Id);
        Keys.put("isCheck", "true");
        Keys.put("tdEnables", "true");

        return Keys;
	}

    //@overload
    public HashMap<String, String> createFlatWithRestaurantFirstOrderRestrictionAtRestaurantLevelMultiTD(String s, String minCartAmount, String flatTDAmount, Boolean disableAllActiveTD)  throws Exception{

        // List rlist = getRestaurantListing("12.9719","77.6412","0");
        // String randomRestaurant = rlist.get(commonAPIHelper.getRandomNo(0,
        // (rlist.size()-1))).toString();
        String randomRestaurant = new Integer(rm.nextInt()).toString();
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));

        CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
                .header("RestauratLevelDiscount")
                .valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
                .campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Automation")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                // .slots(slots)
                .ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(false)
                .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").restaurantFirstOrder(true).build();

        Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

        HashMap Keys = new HashMap();

        Keys.put("restId", randomRestaurant);
        Keys.put("minCartAmount", minCartAmount);
        Keys.put("FlatTDAmount", flatTDAmount);
        Keys.put("TDID", createdTD_Id);

        return Keys;
    }


	//@Overload
	public HashMap<String, String> createFeebieTDWithRestaurantFirstOrderAtRestaurantLevel(String minCartAmount, List<Integer> restIds, String freebieItemId, boolean disableAllTD) throws IOException {




//		List<RestIdList> al = new ArrayList();
//		al.add(new RestIdList(restId));
//		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
//		if (disableAllTD)
//			mhelper.disableCampaignByRestId(restId);
//


		ArrayList al = new ArrayList();
		for (Integer restId : restIds) {
			al.add(new RestIdList(String.valueOf(restId)));
		}
		SuperMultiTDHelper mhelper = new SuperMultiTDHelper();
		if (disableAllTD) {
			for (Integer restId : restIds)
				mhelper.disableCampaignByRestId(String.valueOf(restId));
		}

		CreateFreebieTDEntryNewPojo freebieTradeDiscount1= new CreateFreebieTDEntryNewPojo();

		RuleDiscount discount =new RuleDiscount();
		discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("99").withItemId("1283271");



		freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
				.withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(getMidNightTimeOfCurrentDay())
				.withValidTill(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
				.withRestaurantList(al)
				.withRuleDiscount(discount)
				.withRestaurantHit("100")
				.withFirstOrderRestriction(false)
				.withIsSurge(true)
				.withSlots(new ArrayList<>())
				.withSurgeApplicable(true)
				.withTaxesOnDiscountedBill(false)
				.withTimeSlotRestriction(false)
				.withUserRestriction(false)
				.withIsSuper(false)
				.withRestaurantFirstOrder(true).withDormantUserType("ZERO_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");

		System.out.println(jsonHelper.getObjectToJSON(freebieTradeDiscount1));

		Processor processor = createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();

		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		HashMap Keys = new HashMap();
		int get_status = JsonPath.read(createTDResponse, "$.statusCode");
		if( get_status == 0)
			Keys.put("status","success");
		else
			Keys.put("status","failed");


		for(int i=0;i<restIds.size();i++) {
			Keys.put("restId", restIds.get(i));
			Keys.put("restId" + i, restIds.get(i));
		}
		Keys.put("minCartAmount", minCartAmount);
		Keys.put("TDID", createdTD_Id);
		Keys.put("isCheck", "true");
		Keys.put("tdEnables", "true");

		return Keys;
	}
	
	public Processor createFlatWithThirtyDayDormantAtRestaurantLevelProcessor(String minCartAmount,
			String flatTDAmount, String restID,boolean userRestriction){
		disabledActiveTD(restID);

		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(restID, "Test", new ArrayList<>()));

		CreateFlatTdEntry flatTradeDiscount1;
		Processor processor = null;
		try {
			flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
					.header("RestauratLevelDiscount")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 5).toInstant().getEpochSecond() * 1000))
					.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Dev")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					// .slots(slots)
					.ruleDiscount("Flat", "Restaurant", flatTDAmount, minCartAmount).userRestriction(userRestriction)
					.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
					.commissionOnFullBill(false).dormant_user_type("THIRTY_DAYS_DORMANT").restaurantFirstOrder(false)
					.build();


			processor =  createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return processor;
	}


	public Processor addMapping(String uri, String method, String body){
		HashMap<String,String> headers = new HashMap<String,String>(){{put("Content-Type","application/json");}};
		GameOfThronesService service = new GameOfThronesService("mock", "addmapping", gameofthrones);
		 body = StringEscapeUtils.escapeJava(body);
		String[] payloadparams = new String[] { uri,method,body };
		return  new Processor(service,headers , payloadparams);
	}

    public Processor addPatternMapping(String uri, String method, String body){
        HashMap<String,String> headers = new HashMap<String,String>(){{put("Content-Type","application/json");}};
        GameOfThronesService service = new GameOfThronesService("mock", "addpatternmapping", gameofthrones);
        body = StringEscapeUtils.escapeJava(body);
        String[] payloadparams = new String[] { uri,method,body };
        return  new Processor(service,headers , payloadparams);
    }
	// td-caching helper

	public HashMap<String, String> createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmount(String restId, String header, String discountAmount) throws IOException {
		String randomRestaurant = restId;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header(header)
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addHours(new Date(), 5000).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Ankita")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", discountAmount, "0").userRestriction(false)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		String statusMessage = JsonPath.read(createTDResponse, "$.statusMessage").toString();
		HashMap Keys = new HashMap();
		Keys.put("restaurant_id", randomRestaurant);
		Keys.put("campaign_id", createdTD_Id);
		Keys.put("statusMessage",statusMessage);
		return Keys;
	}


    public HashMap<String, String> createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountForSuperUser(String restId, String header, String discountAmount) throws IOException {
        String randomRestaurant = restId;
        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
        disabledActiveTD(randomRestaurant);
        CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
                .header(header)
                .valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
                .valid_till(String.valueOf(DateUtils.addHours(new Date(), 5000).toInstant().getEpochSecond() * 1000))
                .campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Ankita")
                .discountLevel("Restaurant").restaurantList(restaurantLists1)
                .ruleDiscount("Flat", "Restaurant", discountAmount, "0").userRestriction(false)
                .timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
                .commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").withSuper(Boolean.TRUE).build();
        Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
        String statusMessage = JsonPath.read(createTDResponse, "$.statusMessage").toString();
        HashMap Keys = new HashMap();
        Keys.put("restaurant_id", randomRestaurant);
        Keys.put("campaign_id", createdTD_Id);
		Keys.put("statusMessage", statusMessage);

        return Keys;
    }



    public HashMap<String, String> createFlatTdWithNoMinAmountAtRestaurantLevelWithGiveHeaderAndDiscountAmountAndUserRestriction(String restId, String header,String discountAmount) throws IOException {
		String randomRestaurant = restId;
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		restaurantLists1.add(new RestaurantList(randomRestaurant, "Test", new ArrayList<>()));
		disabledActiveTD(randomRestaurant);
		CreateFlatTdEntry flatTradeDiscount1 = new CreateFlatTdBuilder().nameSpace("RestauratLevel")
				.header(header)
				.valid_from(String.valueOf(DateUtils.addHours(new Date(), 0).toInstant().getEpochSecond() * 1000))
				.valid_till(String.valueOf(DateUtils.addHours(new Date(), 5000).toInstant().getEpochSecond() * 1000))
				.campaign_type("Flat").restaurant_hit("100").enabled(true).swiggy_hit("0").createdBy("Ankita")
				.discountLevel("Restaurant").restaurantList(restaurantLists1)
				.ruleDiscount("Flat", "Restaurant", discountAmount, "0").userRestriction(true)
				.timeSlotRestriction(false).firstOrderRestriction(false).taxesOnDiscountedBill(false)
				.commissionOnFullBill(false).dormant_user_type("ZERO_DAYS_DORMANT").build();
		Processor processor = createTD(jsonHelper.getObjectToJSON(flatTradeDiscount1));
		String createTDResponse = processor.ResponseValidator.GetBodyAsText();
		String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();
		HashMap Keys = new HashMap();
		Keys.put("restaurant_id", randomRestaurant);
		Keys.put("campaign_id", createdTD_Id);
		return Keys;
	}

	public Integer getPolygonID(String... restaurantID){
        String headersToken = "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==";
        int statusCode = 0;
        String statusMessage = "Polygon Id found";
        SANDHelper sh = new SANDHelper();
        boolean flag = true;
        int i = 0;
        do {
			Processor processor = sh.getPolygon(new String[]{restaurantID[i]}, headersToken);
			if(((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"))==0){
				flag = false;
			}
			i++;
		}while(flag && i<5);
        return JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.id");
    }

    public String createCarousel(CarouselPOJO carouselPOJO) throws IOException {
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");

        GameOfThronesService got = new GameOfThronesService("sand10213", "carouselbannerpojo", gameofthrones);

        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(carouselPOJO)});
        String response = processor.ResponseValidator.GetBodyAsText();
        errorCodeHandler(processor, "carousel API");
       return response;
    }

	public String disableCarousel(CarouselPOJO carouselPOJO) throws IOException {
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		hashmap.put("authorization", "Basic b3psUlpUdWVSMmtxdjdnbTpkMmYyYmQ1MzBhYjI1ZmVmNDdhYjIyMGQwZTBhYTEwOA==");

		GameOfThronesService got = new GameOfThronesService("sand10213", "disablecarouselbannerpojo", gameofthrones);

		Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(carouselPOJO)});
		String response = processor.ResponseValidator.GetBodyAsText();
		errorCodeHandler(processor, "carousel API");
		return response;
	}

	public void disableCampaign(String... disableCamp){
	    RemoveFreebie rm = new RemoveFreebie();
        for (String campId : disableCamp)
            rm.withEnabled(false).withId(Integer.parseInt(campId)).withUpdatedBy("Dev Karan");
        try {
            String[] payload = {jsonHelper.getObjectToJSON(rm)};
            GameOfThronesService gots = new GameOfThronesService("td", "removecampaign", gameofthrones);
            HashMap<String, String> headers = new HashMap<>();
            headers.put("Content-Type", "application/json");
            Processor processor = new Processor(gots, headers, payload);
            Assert.assertEquals((int)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.statusCode"),0,"Campaign was not disabled");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public Processor updateTDAtRestaurantLevel(String percentage, String minCartAmount,
																	String discountCap,Integer tdID, String... restID) {
		List<RestaurantList> restaurantLists1 = new ArrayList<>();
		for (String rest : restID) {
			restaurantLists1.add(new RestaurantList(rest, "Test", new ArrayList<>()));
		}
		CreateTdEntry percentTradeDiscount;
		Processor processor = null;
		try {
			percentTradeDiscount = new CreateTdBuilder().id(tdID).nameSpace("RestauratLevel").updatedBy("dev")
					.valid_from(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1000))
					.valid_till(String.valueOf(DateUtils.addMinutes(new Date(), 0).toInstant().getEpochSecond() * 1100))
					.campaign_type("Percentage").restaurant_hit("100").enabled(true).swiggy_hit("0")
					.discountLevel("Restaurant").restaurantList(restaurantLists1)
					.ruleDiscount("Percentage", "Restaurant", percentage, minCartAmount, discountCap)
					.firstOrderRestriction(false).build();

			processor = editTD(jsonHelper.getObjectToJSON(percentTradeDiscount));
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
		return processor;
	}

	public static Processor applyCoupon(String payload, String code) {
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
		GameOfThronesService service = new GameOfThronesService("cs", "couponApplyPOJO", gameofthrones);
		String[] payloadparams = new String[] { payload};
		String[] urlparams = new String[] { code };
		Processor processor = new Processor(service, requestHeaders, payloadparams, urlparams);
		return processor;
	}

	public Processor updateOrderCountByPaymentMethod(List<OrderCountByPaymentMethodPOJO> payload){
		HashMap<String, String>  requestHeader = new HashMap<String, String>();
		requestHeader.put("Content-Type", "application/json");
		Processor processor = null;
		try {
			GameOfThronesService service = new GameOfThronesService("segmentation", "updateOrderCountByPaymentMethodTypePOJO", gameofthrones);
			processor = new Processor(service, requestHeaders, new String[] { jsonHelper.getObjectToJSON(payload) });
		} catch (IOException e) {
			e.printStackTrace();
		}
		return processor;
	}


	public Boolean switchServiceabilityBanner(String restId, Boolean serviceableFlag) {
		String zoneId = SystemConfigProvider.getTemplate(DeliveryConstant.databaseName).
				queryForMap("select z.id as zone_id from restaurant r inner join zone z inner join area a on z.id=a.zone_id and r.area_code=a.id where r.id = " + restId).get("zone_id").toString();
		RedisHelper redisHelper = new RedisHelper();
		String upperBanner = "5";
		String lowerBanner = ".5";
		Object getBanner = redisHelper.getValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId);
		if (serviceableFlag == true) {
			redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId, lowerBanner);
			return true;
		} else {
			redisHelper.setValue("deliveryredis", 1, "BL_BANNER_FACTOR_ZONE_" + zoneId, upperBanner);
			return true;
		}
	}

	public static void insertOrderCountInCustomerOrderTable(String customerId, String orderCount, String firstOrderCity,String lastOrderCity) {
		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(CouponConstants.segmentation_DB);
		String presentTime=Utility.getDate();
		String insert=TDCachingDbHelper.replaceSevenWords(CouponConstants.insertCustomerOrders,customerId,presentTime,presentTime,orderCount,firstOrderCity,lastOrderCity,presentTime);
		System.out.print("insert querry-- >> " + CouponConstants.insertCustomerOrders);
		//String insert="" +CouponConstants.insertCustomerOrders;
		sqlTemplate.execute(insert);
		return;
	}

	public void enableHighTouch(String restID, String versionCode, String channel){
        int i=100;
	    String response;
	    SANDHelper sandHelper = new SANDHelper();
        sandHelper.setLayoutInRedis("high_touch",restID);
        do {
            processor = sandHelper.menuV4RestIdWithChannel(restID, null, null, versionCode, channel);
            response = processor.ResponseValidator.GetBodyAsText();
        }while(!JsonPath.read(response, "$.data.menu.layoutType").toString().equalsIgnoreCase("high_touch") && i!=0);
    }

	public Processor getRestConfig(String versionCode, String userAgent, String restID){
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		hashmap.put("User-Agent",userAgent);
		hashmap.put("version-code",versionCode);
		String[] payloadParams = new String[] { restID };
		GameOfThronesService service = new GameOfThronesService("sand9005", "getRestConfig", gameofthrones);
		Processor processor = new Processor(service, hashmap, null,payloadParams);
		Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200,"Assertion Failed :: high touch enable failed");
		return processor;
	}
//	getRestConfig
    public String[] createSuperUser()  {
        SuperMultiTDHelper superMultiTDHelper = new SuperMultiTDHelper();
        try {
            superMultiTDHelper.disableSuperSubscription();
			superMultiTDHelper.deactiveAllSubscription();
            superMultiTDHelper.createPlanBenifitWithMapingSuper();
            superMultiTDHelper.createSubscription();
            for(String mob: MultiTDConstants.mobile)
                System.out.println(mob);

            return MultiTDConstants.mobile;
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    public int getTodaysDay(){
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
    }

	public static String getDay() {
		Date now = new Date();
		SimpleDateFormat simpleDateformatslot1 = new SimpleDateFormat("E"); // the day of the week abbreviated
		return simpleDateformatslot1 .format(now);
	}


	public int getRandomNo(int size){
        Random rn = new Random();
        int randomNum =  rn.nextInt(size);
        return randomNum;
    }


    public void enableTradeDiscountInSAND(String restID){
        redisHelper.sAdd("sandredisstage","trade_discount_enabled_rest_set",restID);
    }

    public HashMap getItemCategoryAndSubCategoryID(String itemID){

        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        String[] payloadParams = new String[] { itemID };
        GameOfThronesService service = new GameOfThronesService("cmsbaseservice", "getItemInfo", gameofthrones);
        Processor processor = new Processor(service, hashmap, null,payloadParams);
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode() == 200,"Assertion Failed :: high touch enable failed");
        hashmap.put("CategoryID",String.valueOf((Integer)JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.data.category_id")));

        return hashmap;
    }

    public String createFlatSubcatagoryMenu(String restID,String catId,String subcatId,String itemId) throws IOException
    {
        String minCartAmount = "100";
        String flatTDAmount = "20";

        String randomRestaurant = restID;

        disabledActiveTD(randomRestaurant);
        String categoryId1 = catId;

        String subCategoryId1 = subcatId;

        List<Category> categories = new ArrayList<>();
        List<Menu> menus = new ArrayList<>();
            Menu menu = new Menu();
            menu.setId(itemId);
            menu.setName("test");
            menus.add(menu);

        Category category = new Category();
            List<SubCategory> subCategories = new ArrayList<>();
            subCategories.add(new SubCategory(subCategoryId1, new Integer(rm.nextInt()).toString(), menus));
            category.setSubCategories(new ArrayList<>(subCategories));

        category.setId(categoryId1);
        category.setName("Testdsds");
        categories.add(category);

        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(randomRestaurant, "Test SubCategory", categories));
        String TdId = createFlatWithNoMinAmountAtSubCategoryLevel(restaurantLists1, minCartAmount, flatTDAmount);

        System.out.println("TD ID: " + TdId);
        return TdId;
    }

    public String createFlatCategoryMenu(String restID,String catId, boolean clearActiveTD) throws IOException
    {
        String minCartAmount = "100";
        String flatTDAmount = "20";
        String randomRestaurant = restID;
        if(clearActiveTD) {
            disabledActiveTD(randomRestaurant);
        }
        String categoryId1 = catId;
        List<Category> categories = new ArrayList<>();

        Category category = new Category();
        category.setId(categoryId1);
        category.setName("Testdsds");
        categories.add(category);

        List<RestaurantList> restaurantLists1 = new ArrayList<>();
        restaurantLists1.add(new RestaurantList(randomRestaurant, "Test Category", categories));
        String TdId = createFlatWithNoMinAmountAtCategoryLevel(restaurantLists1, minCartAmount, flatTDAmount);

        System.out.println("TD ID: " + TdId);
        return TdId;
    }
    public String createFrebieValidTillAndValidFrom(String restId, String freebieItemId, String validtill, String validfrom) throws IOException {


        List<RestIdList> al = new ArrayList();
        al.add(new RestIdList(restId));
        SuperMultiTDHelper mhelper = new SuperMultiTDHelper();

        CreateFreebieTDEntryNewPojo freebieTradeDiscount1= new CreateFreebieTDEntryNewPojo();
        RuleDiscount discount =new RuleDiscount();
        discount.withDiscountLevel("Restaurant").withType("Freebie").withMinCartAmount("99").withItemId("1283271");



        freebieTradeDiscount1.withNamespace("RestaurantLevel").withHeader("RestaurantLevelDiscount").withDescription("created by automation").withEnabled(true)
                .withDiscountLevel("Restaurant").withCreatedBy("Ram").withCommissionOnFullBill(false).withValidFrom(validfrom)
                .withValidTill(validtill)
                .withRestaurantList(al)
                .withRuleDiscount(discount)
                .withRestaurantHit("100")
                .withFirstOrderRestriction(false)
                .withIsSurge(true)
                .withSlots(new ArrayList<>())
                .withSurgeApplicable(true)
                .withTaxesOnDiscountedBill(false)
                .withTimeSlotRestriction(false)
                .withUserRestriction(false)
                .withIsSuper(false)
                // Change Thirty
                .withRestaurantFirstOrder(false).withDormantUserType("ZERO_DAYS_DORMANT").withCampaignType("Freebie").withSwiggyHit("0");


        Processor processor = createTD(jsonHelper.getObjectToJSON(freebieTradeDiscount1));
        String createTDResponse = processor.ResponseValidator.GetBodyAsText();
        String createdTD_Id = JsonPath.read(createTDResponse, "$.data").toString();

        return createdTD_Id;
    }

    public Integer getNextPriority(String response){
        try {
            List<Integer> priorities = JsonPath.read(response, "$.data.menu.menuCarousels..priority");
            Integer priority = priorities
                    .stream()
                    .mapToInt(v -> v)
                    .min().orElseThrow(NoSuchElementException::new) - 1;
            return priority;
        } catch (NoSuchElementException e){
            return 100;
        }
    }


    public int[] getMinMaxCarouselCount(String versionCode, String userAgent, String restID){
        Processor processor;
        int i=100;
        do {
            processor = getRestConfig(versionCode, userAgent, restID);
            i--;
        }while(!JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.restaurantLayoutType").toString().equalsIgnoreCase("high_touch") && i!=0);
        JSONArray minArray =JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.sectionConfigList[?(@.name=='menuCarousels')].min");
        int min = (Integer)minArray.get(0);
        JSONArray maxArray = JsonPath.read(processor.ResponseValidator.GetBodyAsText(),"$.sectionConfigList[?(@.name=='menuCarousels')].max");
        int max = (Integer)maxArray.get(0);
        return new int[]{min,max};
    }

    public List jsonArrayToArrayList(JSONArray jsonArray){
        List listdata = new ArrayList();
        if (jsonArray != null) {
            for (int i=0;i<jsonArray.size();i++){
                listdata.add(jsonArray.get(i));
            }
        }
        return listdata;
    }


    public void setInOrOutStock(String item_id, String flag){
        new RedisHelper().setValueJson("sandredisstage", 0, "AV_ITEM_" + item_id, "{\"status\":"+flag+",\"nextChangeTime\":1636364740000}");
    }


    public HashMap<String, String> createItemCategoryAndSubCategory(int restID){
        BaseServiceHelper baseServiceHelper = new BaseServiceHelper();
        HashMap<String,String> itemInfo = new HashMap();
        int catID = baseServiceHelper.createCategoryIdAndReturnInt(restID);
        int subCatID = baseServiceHelper.createSubCategoryAndReturnInt(restID,catID);
        String itemID = baseServiceHelper.createItemIdAndReturnString(restID,catID,subCatID);
        itemInfo.put("Cat",String.valueOf(catID));
        itemInfo.put("Sub",String.valueOf(subCatID));
        itemInfo.put("Item", itemID);

        return itemInfo;

    }

    public HashMap getItemCategoryAndSubCategory(String restID, String menuResponse) {
		try {
			List<Integer> availabeItems = JsonPath.read(menuResponse, "$.data.menu.items..id");
			CriticalAPI_Helper criticalAPI_helper = new CriticalAPI_Helper();
			String respone = criticalAPI_helper.getRestaurantMenu(restID).ResponseValidator.GetBodyAsText();
			List<HashMap> data = new ArrayList<>();
			HashMap<String, String> itemInfo = new HashMap<>();

			List categories = JsonPath.read(respone, "$.data.categories");
			for (int i = 0; i < categories.size(); i++) {
				List subCategories = JsonPath.read(categories.get(i), "$.subCategories");
				for (int j = 0; j < subCategories.size(); j++) {

					if (null != JsonPath.read(subCategories.get(j), "$.menu")) {
						List<Integer> itemID = JsonPath.read(subCategories.get(j), "$.menu..id");
						for (int k : itemID) {
							if (availabeItems.contains(k)) {
								itemInfo.put("Item", String.valueOf(k));
								itemInfo.put("Cat", String.valueOf((Integer) JsonPath.read(categories.get(i), "$.id")));
								itemInfo.put("Sub", String.valueOf((Integer) JsonPath.read(subCategories.get(j), "$.id")));
								data.add(itemInfo);
								int index = availabeItems.indexOf(k);
								availabeItems.remove(index);
							}
						}
					}
				}
			}
			if (!data.isEmpty()) {
				return data.get(getRandomNo(data.size()));
			}
		} catch (PathNotFoundException e) {
		}
		return null;
	}
	/*
    item TD create helper
    * */

	public Processor createItemTD(CreateFlatTDItemLevelPOJO createFlatTDItemLevelPOJO) throws IOException {
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		hashmap.put("authorization", "Basic R0dZU1dJOjIwMTVTVyFHR1k=");
		GameOfThronesService got = new GameOfThronesService("td", "createItemLevelTDpojo", gameofthrones);
		Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(createFlatTDItemLevelPOJO)});
		errorCodeHandler(processor, "create Item Level TD");
		return processor;
	}

    public static void disableEnableTd(String tdID, boolean value) {
	    if (value == true) {
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
            sqlTemplate.execute(RngConstants.updateEnableDisableTD0 + 1 + RngConstants.updateEnableDisableTD0 + tdID);
        }
        else if (value == false){
            SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.rngDB);
            sqlTemplate.execute(RngConstants.updateEnableDisableTD0 + 0 + RngConstants.updateEnableDisableTD0 + tdID);
        }

    }

	public void addMockEndPoint(ProxyServerPOJO request) throws IOException{
		GameOfThronesService got = new GameOfThronesService("stormborn", "add", gameofthrones);
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(request)});
		Assert.assertTrue(((processor.ResponseValidator.GetResponseCode()==201)||(processor.ResponseValidator.GetResponseCode()==409)));

	}
    public void updateMockEndPoint(ProxyServerPOJO request) throws IOException{
        GameOfThronesService got = new GameOfThronesService("stormborn", "update", gameofthrones);
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(request)});
        Assert.assertTrue(processor.ResponseValidator.GetResponseCode()==200);

    }

	public boolean writeCSVData(List<String[]> data, String dirPath, String fileName, boolean append){
		File dir = new File(dirPath);
		try{
			if(!dir.exists()){
				System.out.println("Creating new directory");
				dir.mkdir();
			}
			File file = new File(dirPath+fileName);
			if(!file.exists() || append == false) {
                file.createNewFile();
            }
//			first time append needs to be false and then it should be true
			FileWriter fileWriter = new FileWriter(file,append);
			CSVWriter csvWriter = new CSVWriter(fileWriter);
			csvWriter.writeAll(data);
			csvWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Processor doInventorySlotBooking(PrioritySlotsPOJO prioritySlotsPOJO) throws IOException {
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		GameOfThronesService got = new GameOfThronesService("adsplatform", "priorityslots", gameofthrones);
		Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(prioritySlotsPOJO)});
		errorCodeHandler(processor, "create campaign for a rest");
		return processor;

	}

	public Processor executeAdsCron(){
	    for(int i=0;i<10;i++) {
            HashMap<String, String> hashmap = new HashMap<>();
            hashmap.put("Content-Type", "application/json");
            GameOfThronesService got = new GameOfThronesService("adscron", "executecron", gameofthrones);
            processor = new Processor(got, hashmap);
        }
	    errorCodeHandler(processor, "run cron for ads");
        return processor;
    }

    public Processor stopAdsCampaign(String camapignId){
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        String[] queryParams= new String[]{camapignId,"dev"};
        GameOfThronesService got = new GameOfThronesService("adsplatform", "stopcampaign", gameofthrones);
        Processor processor = new Processor(got,hashmap,null,queryParams);
        errorCodeHandler(processor, "disable a campaign");
        return processor;
    }

    public void stopAdsCurrentAdsCampaigns(String date, Integer restID){

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.adsDB);
		List<Map<String, Object>> inventories = sqlTemplate.queryForList(String.format(RngConstants.getSoldInventoryIDs, restID, date));
		if(!inventories.isEmpty()) {
            StringBuilder str = new StringBuilder();
            for (Map<String, Object> data : inventories) {
                str.append(data.get("id") + ",");
            }
            str.deleteCharAt(str.length() - 1);
            sqlTemplate.execute(String.format(RngConstants.setSoldQuantityToZero, str));
        }
	}

    public Processor fetchAdsCampaigns(int... restID) throws IOException {
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
	    HashMap<String,ArrayList<HashMap<String,Integer>>> restaurants = new HashMap<>();
        HashMap<String,Integer> restIds = null;
	    for(int rest: restID){
            restIds = new HashMap<>();
            restIds.put("id", rest);

        }
        HashMap<String, Integer> finalRestIds = restIds;
        restaurants.put("restaurants",new ArrayList<HashMap<String,Integer>>(){{add(finalRestIds);}});

	    for(int i=0;i<5;i++) {
			GameOfThronesService got = new GameOfThronesService("adsserver","fetchcampaigns", gameofthrones);
            processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(restaurants)});
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	    errorCodeHandler(processor,"fetch active campaigns for restaurants");
	    return processor;
    }

    public void deletePricing(String date, int areaID, int cityID, int instrumentPos, int startTime, int endTime){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.adsDB);
        try {
            Map<String, Object> rule = sqlTemplate.queryForMap(String.format(RngConstants.getPricingRule, date, areaID, cityID, instrumentPos, startTime, endTime));
            long ruleID = (long) rule.get("id");

            Map<String, Object> ruleUnitMap = sqlTemplate.queryForMap(String.format(RngConstants.getPricingRuleUnitMap, ruleID));
            long unitID = (long) ruleUnitMap.get("unit_id");
            long ruleUnitMapID = (long) ruleUnitMap.get("id");

            sqlTemplate.execute(String.format(RngConstants.deletePricingRuleUnitMap, ruleUnitMapID));
            sqlTemplate.execute(String.format(RngConstants.deletePriceRule, ruleID));
            sqlTemplate.execute(String.format(RngConstants.deletePriceUnit, unitID));

        }catch (EmptyResultDataAccessException e){
            System.out.println("No price rule found :: "+ date);
        }


    }
    public void deletePricingExpection(String date,int startTime, int endTime, int restID){
        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.adsDB);
        sqlTemplate.execute(String.format(RngConstants.deletePricingException, restID, date, startTime, endTime));
    }

    public  Processor uploadPricing(String filePath){
	    File file = new File(filePath);

        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        HashMap<String, String> formData = new HashMap<>();
        formData.put("file",file.getAbsolutePath());
        GameOfThronesService got = new GameOfThronesService("adsplatform","pricingupload", gameofthrones);
//        GameOfThronesService service, HashMap<String, String> headers, String[] payloadparams, String[] urlparams, HashMap<String, String> formdata
        Processor processor = new Processor(got,hashmap,null,null,formData);
        errorCodeHandler(processor,"fetch active campaigns for restaurants");
        return processor;
    }

	public  Processor uploadDiscount(String filePath){
        File file = new File(filePath);
		HashMap<String, String> hashmap = new HashMap<>();
		hashmap.put("Content-Type", "application/json");
		HashMap<String, String> formData = new HashMap<>();
		formData.put("file",file.getAbsolutePath());
		GameOfThronesService got = new GameOfThronesService("adsplatform","discountupload", gameofthrones);
		Processor processor = new Processor(got,hashmap,null,null,formData);
		errorCodeHandler(processor,"fetch active campaigns for restaurants");
		return processor;
	}

    public Processor fetchpricing(FetchChargesPOJO fetchChargesPOJO) throws IOException {
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        GameOfThronesService got = new GameOfThronesService("adsplatform", "fetchpricing", gameofthrones);
        Processor processor = new Processor(got, hashmap, new String[]{jsonHelper.getObjectToJSON(fetchChargesPOJO)});
        errorCodeHandler(processor, "create campaign for a rest");
        return processor;
    }

    public Processor searchAdsCampaign(HashMap<String,String> queryParams){
        HashMap<String, String> hashmap = new HashMap<>();
        hashmap.put("Content-Type", "application/json");
        GameOfThronesService got = new GameOfThronesService("adsplatform", "searchadscampaign", gameofthrones);
        StringBuilder queryParameter = new StringBuilder();
        for (Map.Entry<String,String> entry: queryParams.entrySet()){
            queryParameter.append(entry.getKey()+"="+entry.getValue()+"&");
        }
        queryParameter.deleteCharAt(queryParameter.length()-1);
        Processor processor = new Processor(got, hashmap, null, new String[]{queryParameter.toString()});
        errorCodeHandler(processor, "create campaign for a rest");
        return processor;
    }


	public Processor createGrowthPackType(CreateTypePOJO payload) throws IOException { //growthPack
		HashMap<String, String> requestHeaders = new HashMap<String, String>();
		requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService got = new GameOfThronesService("growthpack", "createGrowthPackType", gameofthrones);
        Processor processor  = new Processor(got, requestHeaders, new String[]{jsonHelper.getObjectToJSON(payload)});
		return processor;
	}


	public String getGrowthPackTypeName(int id) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("name").toString()) : "code is not present in DB");
	}

	public String getGrowthPackTypeCreatedBy(int id) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("created_by").toString()) : "code is not present in DB");
	}
	public String getGrowthPackTypeUpdatedBy(int id) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("updated_by").toString()) : "code is not present in DB");
	}
	public String getGrowthPackTypeMetaIcon(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
			JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("iconUrl"));
	}

	public String getGrowthPackTypeMetaDescription(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
		JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("description"));
	}

	public String getGrowthPackTypeMetaHeader(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeDbData + "'"+id + RngConstants.endQuotes);
		JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("header"));
	}

    public String getGrowthPackTypeAudName(int id) throws JSONException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        return String.valueOf((list.size() > 0) ? (list.get(0).get("name").toString()) : "code is not present in DB");

    }
	public String getGrowthPackTypeAudNameUpdated(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		System.out.println(list);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("name").toString()) : "code is not present in DB");

	}
    public String getGrowthPackTypeAudCreatedBy(int id) {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        return String.valueOf((list.size() > 0) ? (list.get(0).get("created_by").toString()) : "code is not present in DB");
    }

	public String getGrowthPackTypeAudUpdatedByUpdated(int id) {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("updated_by").toString()) : "code is not present in DB");
	}
    public String getGrowthPackTypeAudRevType(int id) throws JSONException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        return String.valueOf((list.size() > 0) ? (list.get(0).get("revtype").toString()) : "code is not present in DB");

    }
	public String getGrowthPackTypeAudRevTypeUpdated(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		return String.valueOf((list.size() > 0) ? (list.get(0).get("revtype").toString()) : "code is not present in DB");

	}
    public String getGrowthPackTypeAudMetaIcon(int id) throws JSONException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
        return String.valueOf(jsonObject.get("iconUrl"));
    }

	public String getGrowthPackTypeAudMetaIconUpdated(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("iconUrl"));
	}
    public String getGrowthPackTypeAudMetaDescription(int id) throws JSONException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
        return String.valueOf(jsonObject.get("description"));
    }
	public String getGrowthPackTypeAudMetaDescriptionUpdated(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("description"));
	}
    public String getGrowthPackTypeAudMetaHeader(int id) throws JSONException {

        SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
        List<Map<String, Object>> list = sqlTemplate
                .queryForList(RngConstants.growthPackTypeAudDbData + "'"+id + RngConstants.endQuotes);
        JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
        return String.valueOf(jsonObject.get("header"));
    }
	public String getGrowthPackTypeAudMetaHeaderUpdated(int id) throws JSONException {

		SqlTemplate sqlTemplate = SystemConfigProvider.getTemplate(RngConstants.growthPackDB);
		List<Map<String, Object>> list = sqlTemplate
				.queryForList(RngConstants.growthPackTypeAudDbData +"'"+id +"'"+"and revtype ="+"'"+"1"+ RngConstants.endQuotes);
		JSONObject jsonObject = new JSONObject(String.valueOf((list.size() > 0) ? (list.get(0).get("meta").toString()) : "code is not present in DB"));
		return String.valueOf(jsonObject.get("header"));
	}

    public Processor updateGrowthPackType(String id, String name, TypeMetaPOJO meta,String updatedBy) throws IOException ,JSONException{
        HashMap<String, String> requestHeaders = new HashMap<String, String>();
        requestHeaders.put("Content-Type", "application/json");
        GameOfThronesService got = new GameOfThronesService("growthpack", "updateGrowthPackType", gameofthrones);
		UpdateTypePOJO updateTypePOJO = new UpdateTypePOJO();
		updateTypePOJO.withName(name).withId(Integer.valueOf(id)).withTypeMeta(meta).withUpdated_by(updatedBy);
		String[] payloadparams =  {jsonHelper.getObjectToJSON(updateTypePOJO)};
		Processor processor  = new Processor(got, requestHeaders, payloadparams,null);
        return processor;
    }

}
