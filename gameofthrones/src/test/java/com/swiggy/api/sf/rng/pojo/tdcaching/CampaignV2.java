package com.swiggy.api.sf.rng.pojo.tdcaching;

import com.swiggy.api.sf.rng.helper.Utility;

import java.util.Date;

public class CampaignV2 {

    private Long id;

    private String name = "Test caching name";

    private String header = "Test caching header";

    private String description = "Test caching description";

    private String shortDescription = "Test caching short description";

    private Date validFrom = new Date();

    private Date validTill = Utility.getDatePlusDays(new Date(), 1);

    private String campaignType = "Flat";

    private String dormantUserType = "ZERO_DAYS_DORMANT";

    private double restaurantHit = 0.0;

    private double swiggyHit = 100;

    private boolean enabled = true;

    private String createdBy = "automation_script";

    private String updatedBy = "automation_script";

    private boolean isFirstOrder = false;

    private double minCartAmount = 0.0;

    private double discountCap = 100;

    private Boolean userRestriction = false;

    private Boolean timeSlotRestriction = false;

    private Boolean commissionOnFullBill = false;

    private Boolean taxesOnDiscountedBill = false;

    private boolean restaurantFirstOrder = false;

    private Date createdAt = new Date();

    private Date updatedAt = new Date();

    private Long templateId = null;

    private boolean isSurgeApplicable = false;

    private boolean isCopyOverridden = true;

    private String levelPlaceHolder = null;

    private String freebiePlaceHolder = null;

    private Double commissionLevy = 0d;

    private String shareType = "Percentage";

    private Double shareValue = 100d;

    private String campaignSlotEntities = null;

    @Override
    public String toString() {
        return "{"
                + "\"id\" : " + id + ";"
                + "\"name\" : " + name + ";"
                + "\"header\" : " + header + ";"
                + "\"description\" : " + description + ";"
                + "\"shortDescription\" : " + shortDescription + ";"
                + "\"validFrom\" : " + validFrom + ";"
                + "\"validTill\" : " + validTill + ";"
                + "\"campaignType\"" + campaignType + ";"
                + "\"dormantUserType\" : " + dormantUserType + ";"
                + "\"restaurantHit\" : " + restaurantHit + ";"
                + "\"swiggyHit\" : " + swiggyHit + ";"
                + "\"enabled\"" + enabled + ";"
                + "\"createdBy\" : " + createdBy + ";"
                + "\"updatedBy\" : " + updatedBy + ";"
                + "\"isFirstOrder\" : " + isFirstOrder + ";"
                + "\"minCartAmount\" : " + minCartAmount + ";"
                + "\"discountCap\" : " + discountCap + ";"
                + "\"userRestriction\" : " + userRestriction + ";"
                + "\timeSlotRestriction\" : " + timeSlotRestriction + ";"
                + "\"commissionOnFullBill\" : " + commissionOnFullBill + ";"
                + "\"taxesOnDiscountedBill\" : " + taxesOnDiscountedBill + ";"
                + "\"campaignSlotEntities\" : " + campaignSlotEntities + ";"
                + "\"taxesOnDiscountedBill\" : " + restaurantFirstOrder + ";"
                + "\"createdAt\" : " + createdAt + ";"
                + "\"updatedAt\" : " + updatedAt + ";"
                + "\"templateId\" : " + templateId + ";"
                + "\"isSurgeApplicable\" : " + isSurgeApplicable + ";"
                + "\"isCopyOverridden\" : " + isCopyOverridden + ";"
                + "\"levelPlaceHolder\" : " + levelPlaceHolder + ";"
                + "\"freebiePlaceHolder\" : " + freebiePlaceHolder + ";"
                + "\"commissionLevy\" : " + commissionLevy + ";"
                + "\"shareType\" : " + shareType + ";"
                + "\"shareValue\" : " + shareValue + ";"
                + "}";
    }

}