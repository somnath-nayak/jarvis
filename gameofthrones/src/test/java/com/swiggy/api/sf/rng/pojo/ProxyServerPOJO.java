package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class ProxyServerPOJO {

    @JsonProperty("apiEndPoint")
    private String apiEndPoint;
    @JsonProperty("mockUrl")
    private String mockUrl;
    @JsonProperty("serviceUrl")
    private String serviceUrl;
    @JsonProperty("doMock")
    private String doMock;
    @JsonProperty("ipAddress")
    private String ipAddress;

    @JsonProperty("apiEndPoint")
    public String getApiEndPoint() {
        return apiEndPoint;
    }

    @JsonProperty("apiEndPoint")
    public void setApiEndPoint(String apiEndPoint) {
        this.apiEndPoint = apiEndPoint;
    }

    public ProxyServerPOJO withApiEndPoint(String apiEndPoint) {
        this.apiEndPoint = apiEndPoint;
        return this;
    }

    @JsonProperty("mockUrl")
    public String getMockUrl() {
        return mockUrl;
    }

    @JsonProperty("mockUrl")
    public void setMockUrl(String mockUrl) {
        this.mockUrl = mockUrl;
    }

    public ProxyServerPOJO withMockUrl(String mockUrl) {
        this.mockUrl = mockUrl;
        return this;
    }

    @JsonProperty("serviceUrl")
    public String getServiceUrl() {
        return serviceUrl;
    }

    @JsonProperty("serviceUrl")
    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public ProxyServerPOJO withServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        return this;
    }

    @JsonProperty("doMock")
    public String getDoMock() {
        return doMock;
    }

    @JsonProperty("doMock")
    public void setDoMock(String doMock) {
        this.doMock = doMock;
    }

    public ProxyServerPOJO withDoMock(String doMock) {
        this.doMock = doMock;
        return this;
    }

    @JsonProperty("ipAddress")
    public String getIpAddress() {
        return ipAddress;
    }

    @JsonProperty("ipAddress")
    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public ProxyServerPOJO withIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
        return this;
    }
}
