
package com.swiggy.api.sf.rng.pojo.pitara;


import org.codehaus.jackson.annotate.JsonProperty;

public class Payload {

    @JsonProperty("NUX_LISTING")
    private NUX_LISTING nUXLISTING;
    @JsonProperty("S_CARD")
    private SCARD S_CARD;
    @JsonProperty("L_CARD")
    private LCARD lCARD;
    @JsonProperty("XL_CARD")
    private XLCARD xLCARD;
    @JsonProperty("LaunchCARD")
    private LaunchCARD LaunchCARD;

    @JsonProperty("RAIN_NUDGE_CARD")
    private RAIN_NUDGE_CARD RAIN_NUDGE_CARD;

    @JsonProperty("SUPER_LISTING_CARD")
    private SUPER_LISTING_CARD SUPER_LISTING_CARD;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Payload() {
    }

    /**
     * 
     * @param nUXLISTING
     */
    public Payload(NUX_LISTING nUXLISTING) {
        super();
        this.nUXLISTING = nUXLISTING;
    }
    public Payload(SCARD S_CARD) {
        this.S_CARD = S_CARD;
    }
    public Payload(LCARD lCARD) {
        this.lCARD = lCARD;
    }
    public Payload(XLCARD xlCARD) { this.xLCARD = xlCARD; }
    public Payload(RAIN_NUDGE_CARD RAIN_NUDGE_CARD) {
        this.RAIN_NUDGE_CARD = RAIN_NUDGE_CARD;
    }
    public Payload(SUPER_LISTING_CARD SUPER_LISTING_CARD) {
        this.SUPER_LISTING_CARD = SUPER_LISTING_CARD;
    }
    public Payload(LaunchCARD launchCARD) {
        this.LaunchCARD = launchCARD;
    }


    public Payload(NUX_LISTING nUXLISTING,SCARD S_CARD, LCARD lCARD, XLCARD xLCARD,RAIN_NUDGE_CARD RAIN_NUDGE_CARD,SUPER_LISTING_CARD SUPER_LISTING_CARD,LaunchCARD LaunchCARD) {
        this.nUXLISTING = nUXLISTING;
        this.S_CARD = S_CARD;
        this.lCARD = lCARD;
        this.xLCARD = xLCARD;
        this.RAIN_NUDGE_CARD = RAIN_NUDGE_CARD;
        this.SUPER_LISTING_CARD = SUPER_LISTING_CARD;
        this.LaunchCARD = LaunchCARD;
    }

    @JsonProperty("S_CARD")
    public SCARD getS_CARD() {
        return S_CARD;
    }

    @JsonProperty("S_CARD")
    public void setS_CARD(SCARD S_CARD) {
        this.S_CARD = S_CARD;
    }


    @JsonProperty("LaunchCARD")
    public LaunchCARD getLaunchCARD() {
        return LaunchCARD;
    }

    @JsonProperty("LaunchCARD")
    public void setLaunchCARD(LaunchCARD LaunchCARD) {
        this.LaunchCARD = LaunchCARD;
    }


    public Payload withS_CARD(SCARD S_CARD) {
        this.S_CARD = S_CARD;
        return this;
    }

    @JsonProperty("L_CARD")
    public LCARD getLCARD() {
        return lCARD;
    }

    @JsonProperty("L_CARD")
    public void setLCARD(LCARD lCARD) {
        this.lCARD = lCARD;
    }

    public Payload withLCARD(LCARD lCARD) {
        this.lCARD = lCARD;
        return this;
    }

    @JsonProperty("XL_CARD")
    public XLCARD getXLCARD() {
        return xLCARD;
    }

    @JsonProperty("XL_CARD")
    public void setXLCARD(XLCARD XL_CARD) {
        this.xLCARD = XL_CARD;
    }


    @JsonProperty("RAIN_NUDGE_CARD")
    public RAIN_NUDGE_CARD getRAIN_NUDGE_CARD() {
        return RAIN_NUDGE_CARD;
    }


    @JsonProperty("RAIN_NUDGE_CARD")
    public void setRAIN_NUDGE_CARD(RAIN_NUDGE_CARD RAIN_NUDGE_CARD) {
        this.RAIN_NUDGE_CARD = RAIN_NUDGE_CARD;
    }


    @JsonProperty("NUX_LISTING")
    public NUX_LISTING getNUXLISTING() {
        return nUXLISTING;
    }

    @JsonProperty("NUX_LISTING")
    public void setNUXLISTING(NUX_LISTING nUXLISTING) {
        this.nUXLISTING = nUXLISTING;
    }

    public Payload withNUXLISTING(NUX_LISTING nUXLISTING) {
        this.nUXLISTING = nUXLISTING;
        return this;
    }

    @JsonProperty("SUPER_LISTING_CARD")
    public SUPER_LISTING_CARD getSUPER_LISTING_CARD() {
        return SUPER_LISTING_CARD;
    }


    @JsonProperty("SUPER_LISTING_CARD")
    public void setSUPER_LISTING_CARD(SUPER_LISTING_CARD SUPER_LISTING_CARD) {
        this.SUPER_LISTING_CARD = SUPER_LISTING_CARD;
    }





}