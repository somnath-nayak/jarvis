package com.swiggy.api.sf.snd.pojo.meals.createMealResponse;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "id",
        "name",
        "items",
        "minTotal",
        "maxTotal",
        "minChoices",
        "maxChoices",
        "showPrice",
        "showImage",
        "externalGroupId"
})
public class Group {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private Object name;
    @JsonProperty("items")
    private List<Item> items = null;
    @JsonProperty("minTotal")
    private Integer minTotal;
    @JsonProperty("maxTotal")
    private Integer maxTotal;
    @JsonProperty("minChoices")
    private Integer minChoices;
    @JsonProperty("maxChoices")
    private Integer maxChoices;
    @JsonProperty("showPrice")
    private Boolean showPrice;
    @JsonProperty("showImage")
    private Boolean showImage;
    @JsonProperty("externalGroupId")
    private Integer externalGroupId;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public Object getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(Object name) {
        this.name = name;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonProperty("minTotal")
    public Integer getMinTotal() {
        return minTotal;
    }

    @JsonProperty("minTotal")
    public void setMinTotal(Integer minTotal) {
        this.minTotal = minTotal;
    }

    @JsonProperty("maxTotal")
    public Integer getMaxTotal() {
        return maxTotal;
    }

    @JsonProperty("maxTotal")
    public void setMaxTotal(Integer maxTotal) {
        this.maxTotal = maxTotal;
    }

    @JsonProperty("minChoices")
    public Integer getMinChoices() {
        return minChoices;
    }

    @JsonProperty("minChoices")
    public void setMinChoices(Integer minChoices) {
        this.minChoices = minChoices;
    }

    @JsonProperty("maxChoices")
    public Integer getMaxChoices() {
        return maxChoices;
    }

    @JsonProperty("maxChoices")
    public void setMaxChoices(Integer maxChoices) {
        this.maxChoices = maxChoices;
    }

    @JsonProperty("showPrice")
    public Boolean getShowPrice() {
        return showPrice;
    }

    @JsonProperty("showPrice")
    public void setShowPrice(Boolean showPrice) {
        this.showPrice = showPrice;
    }

    @JsonProperty("showImage")
    public Boolean getShowImage() {
        return showImage;
    }

    @JsonProperty("showImage")
    public void setShowImage(Boolean showImage) {
        this.showImage = showImage;
    }

    @JsonProperty("externalGroupId")
    public Integer getExternalGroupId() {
        return externalGroupId;
    }

    @JsonProperty("externalGroupId")
    public void setExternalGroupId(Integer externalGroupId) {
        this.externalGroupId = externalGroupId;
    }

}