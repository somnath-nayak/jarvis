package com.swiggy.api.sf.checkout.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "address_id",
        "order_comments",
        "payment_cod_method"
})

public class OrderPlace {

    private Integer address_id;
    private String payment_cod_method;
    private String order_comments;

    /**
     * No args constructor for use in serialization
     *
     */
    public OrderPlace() {
    }

    /**
     *
     * @param orderComments
     * @param addressId
     * @param paymentCodMethod
     */
    public OrderPlace(Integer addressId, String paymentCodMethod, String orderComments) {
        super();
        this.address_id = addressId;
        this.payment_cod_method = paymentCodMethod;
        this.order_comments = orderComments;
    }
    @JsonProperty("address_id")
    public Integer getAddressId() {
        return address_id;
    }


    public void setAddressId(Integer addressId) {
        this.address_id = addressId;
    }
    @JsonProperty("payment_cod_method")
    public String getPaymentCodMethod() {
        return payment_cod_method;
    }
    public void setPaymentCodMethod(String paymentCodMethod) {
        this.payment_cod_method = paymentCodMethod;
    }
    @JsonProperty("order_comments")
    public String getOrderComments() {
        return order_comments;
    }

    public void setOrderComments(String orderComments) {
        this.order_comments = orderComments;
    }

    public OrderPlace withAddressId(Integer addressId) {
        this.address_id = addressId;
        return this;
    }

    public OrderPlace withOrderComments(String orderComments) {
        this.order_comments = orderComments;
        return this;
    }

    public OrderPlace withPaymentCodMethod(String paymentCodMethod) {
        this.payment_cod_method = paymentCodMethod;
        return this;
    }

    public OrderPlace setData(Integer addressId, String orderComments, String paymentCodMethod){
        this.withAddressId(addressId).withOrderComments(orderComments).withPaymentCodMethod(paymentCodMethod);
        return this;

    }
    @Override
    public String toString() {
        return new ToStringBuilder(this).append("addressId", address_id).append("paymentCodMethod", payment_cod_method).append("orderComments", order_comments).toString();
    }

}