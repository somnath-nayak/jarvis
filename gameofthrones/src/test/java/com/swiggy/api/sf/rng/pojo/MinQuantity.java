package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

public class MinQuantity {

	@JsonProperty("cart")
	private Integer cart;

	@JsonProperty("cart")
	public Integer getCart() {
		return cart;
	}
	@JsonProperty("cart")
	public void setCart(Integer cart) {
		this.cart = cart;
	}

	public MinQuantity withCart(Integer cart) {
		this.cart = cart;
		return this;
	}
}