package com.swiggy.api.sf.rng.pojo;

import org.codehaus.jackson.annotate.JsonAutoDetect;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

//@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CreateTdBuilder  {

    public CreateTdEntry createTdEntry ;

    public CreateTdBuilder() {
        createTdEntry = new CreateTdEntry();
    }

    public CreateTdBuilder nameSpace(String nameSpace) {
        createTdEntry.setNamespace(nameSpace);
        return this;
    }

    public CreateTdBuilder header(String header) {
        createTdEntry.setHeader(header);
        return this;
    }

    public CreateTdBuilder description(String description) {
        createTdEntry.setDescription(description);
        return this;
    }

    public CreateTdBuilder valid_from(String valid_from) {
        createTdEntry.setValidFrom(valid_from);
        return this;
    }

    public CreateTdBuilder valid_till(String valid_till) {
        createTdEntry.setValidTill(valid_till);
        return this;
    }

    public CreateTdBuilder campaign_type(String campaign_type) {
        createTdEntry.setCampaignType(campaign_type);
        return this;
    }

    public CreateTdBuilder restaurant_hit(String restaurant_hit) {
        createTdEntry.setRestaurantHit(restaurant_hit);
        return this;
    }

    public CreateTdBuilder enabled(boolean enabled) {
        createTdEntry.setEnabled(enabled);
        return this;
    }

    public CreateTdBuilder swiggy_hit(String swiggy_hit) {
        createTdEntry.setSwiggyHit(swiggy_hit);
        return this;
    }

    public CreateTdBuilder createdBy(String createdBy) {
        createTdEntry.setCreatedBy(createdBy);
        return this;
    }

    public CreateTdBuilder discountLevel(String discountLevel) {
        createTdEntry.setDiscountLevel(discountLevel);
        return this;
    }

    public CreateTdBuilder commissionOnFullBill(boolean commissionOnFullBill) {
        createTdEntry.setCommissionOnFullBill(commissionOnFullBill);
        return this;
    }

    public CreateTdBuilder taxesOnDiscountedBill(boolean taxesOnDiscountedBill) {
        createTdEntry.setTaxesOnDiscountedBill(taxesOnDiscountedBill);
        return this;
    }

    public CreateTdBuilder firstOrderRestriction(boolean firstOrderRestriction) {
        createTdEntry.setFirstOrderRestriction(firstOrderRestriction);
        return this;
    }

    public CreateTdBuilder restaurantFirstOrder(boolean restaurantFirstOrder) {
        createTdEntry.setRestaurantFirstOrder(restaurantFirstOrder);
        return this;
    }

    public CreateTdBuilder timeSlotRestriction(boolean timeSlotRestriction) {
        createTdEntry.setTimeSlotRestriction(timeSlotRestriction);
        return this;
    }

    public CreateTdBuilder userRestriction(boolean userRestriction) {
        createTdEntry.setUserRestriction(userRestriction);
        return this;
    }

    public CreateTdBuilder dormant_user_type(String dormant_user_type) {
        createTdEntry.setDormantUserType(dormant_user_type);
        return this;
    }

    public CreateTdBuilder slots(List<Slot> slotsDetails) {
        ArrayList<Slot> slotEntries = new ArrayList<>();
        for (Slot slotEntry : slotsDetails) {
            slotEntries.add(slotEntry);
        }
        createTdEntry.setSlots(slotEntries);
        return this;
    }

    public CreateTdBuilder ruleDiscount(String type, String discountLevel, String percentDiscount, String minCartAmount, String percentDiscountAbsoluteCap) {
        createTdEntry.setRuleDiscount(new RuleDiscount(type, discountLevel, percentDiscount, minCartAmount, percentDiscountAbsoluteCap));
        return this;
    }


    public CreateTdBuilder restaurantList(List<RestaurantList> restaurantLists) {
        createTdEntry.setRestaurantList(restaurantLists);
        return this;
    }

    public CreateTdBuilder id(Integer id) {
        createTdEntry.setId(id);
        return this;
    }

    public CreateTdBuilder updatedBy(String name) {
        createTdEntry.setUpdatedBy(name);
        return this;
    }

    public CreateTdBuilder withCopyOverridden(Boolean copyOverridden) {
        createTdEntry.setCopyOverridden(copyOverridden);
        return this;
    }

    public CreateTdBuilder withIsSuper(Boolean isSuper) {
        createTdEntry.setSuper(isSuper);
        return this;
    }

    public CreateTdBuilder withLevelPlaceHolder(String levelPlaceHolder) {
        createTdEntry.setLevelPlaceHolder(levelPlaceHolder);
        return this;
    }
    public CreateTdEntry build() throws IOException {
        defaultData();
        return createTdEntry;
    }

    public CreateTdBuilder withShortDescription(String shortDescription){
        createTdEntry.setShortDescription(shortDescription);
        return this;
    }


    private void defaultData() throws IOException {
        if (createTdEntry.getNamespace() == null) {
            createTdEntry.setNamespace("Default Namespace");
        }
        if (createTdEntry.getShortDescription() == null) {
            createTdEntry.setShortDescription("Default ShortDesc.");
        }
        if (createTdEntry.getDescription() == null) {
            createTdEntry.setDescription("Default Description");
        }
        if (createTdEntry.getSlots() == null) {
            createTdEntry.setSlots(new ArrayList<>());
        }
        if (createTdEntry.getHeader() == null) {
            createTdEntry.setHeader("Default Header");
        }
        if ((createTdEntry.getCreatedBy() == null) && (createTdEntry.getUpdatedBy() == null)) {
            createTdEntry.setCreatedBy("Default Created By-Manu");
        }
        if (createTdEntry.getUserRestriction() == null) {
            createTdEntry.setUserRestriction(false);
        }
        if (createTdEntry.getTimeSlotRestriction() == null) {
            createTdEntry.setTimeSlotRestriction(false);
        }
        if (createTdEntry.getFirstOrderRestriction() == null) {
            createTdEntry.setFirstOrderRestriction(false);
        }
        if (createTdEntry.getTaxesOnDiscountedBill() == null) {
            createTdEntry.setTaxesOnDiscountedBill(false);
        }
        if (createTdEntry.getCommissionOnFullBill() == null) {
            createTdEntry.setCommissionOnFullBill(false);
        }
        if (createTdEntry.getRestaurantFirstOrder() == null) {
            createTdEntry.setRestaurantFirstOrder(false);
        }
        if (createTdEntry.getDormantUserType() == null) {
            createTdEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }
        //set values if required...
    }

    public CreateTdBuilder createTDWithCopyOverridden() throws IOException {

        if (createTdEntry.getNamespace() == null) {
            createTdEntry.setNamespace("Default");
        }
        if (createTdEntry.getDescription() == null) {
            createTdEntry.setDescription("Default Description");
        }
        if (createTdEntry.getSlots() == null) {
            createTdEntry.setSlots(new ArrayList<>());
        }
        if (createTdEntry.getHeader() == null) {
            createTdEntry.setHeader("Default Header");
        }
        if ((createTdEntry.getCreatedBy() == null) && (createTdEntry.getUpdatedBy() == null)) {
            createTdEntry.setCreatedBy("Ankita-Copy Automation");
        }
        if (createTdEntry.getUserRestriction() == null) {
            createTdEntry.setUserRestriction(false);
        }
        if (createTdEntry.getTimeSlotRestriction() == null) {
            createTdEntry.setTimeSlotRestriction(false);
        }
        if (createTdEntry.getFirstOrderRestriction() == null) {
            createTdEntry.setFirstOrderRestriction(false);
        }
        if (createTdEntry.getTaxesOnDiscountedBill() == null) {
            createTdEntry.setTaxesOnDiscountedBill(false);
        }
        if (createTdEntry.getCommissionOnFullBill() == null) {
            createTdEntry.setCommissionOnFullBill(false);
        }
        if (createTdEntry.getRestaurantFirstOrder() == null) {
            createTdEntry.setRestaurantFirstOrder(false);
        }
        if (createTdEntry.getDormantUserType() == null) {
            createTdEntry.setDormantUserType("ZERO_DAYS_DORMANT");
        }
        if (createTdEntry.getCopyOverridden() == null) {
            createTdEntry.setCopyOverridden(false);
        }
        if (createTdEntry.getEnabled() == null) {
            createTdEntry.setEnabled(true);
        }
        if (createTdEntry.getDiscountLevel() == null) {
            createTdEntry.setDiscountLevel("Restaurant");
        }
        if (createTdEntry.getCreatedBy() == null) {
            createTdEntry.setCreatedBy("Ankita-CopyAutomation");
        }
        if (createTdEntry.getCommissionOnFullBill() == null) {
            createTdEntry.setCommissionOnFullBill(false);
        }
        if (createTdEntry.getFirstOrderRestriction() == null) {
            createTdEntry.setFirstOrderRestriction(false);
        }
        if (createTdEntry.getRestaurantHit() == null) {
            createTdEntry.setRestaurantHit("50");
        }
        if (createTdEntry.getSwiggyHit() == null) {
            createTdEntry.setSwiggyHit("50");
        }

    return this;
    }

}