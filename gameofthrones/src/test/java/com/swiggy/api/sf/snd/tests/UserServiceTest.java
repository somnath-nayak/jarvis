package com.swiggy.api.sf.snd.tests;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;
import com.swiggy.api.sf.snd.constants.SANDConstants;
import com.swiggy.api.sf.snd.dp.SnDDp;
import com.swiggy.api.sf.snd.helper.OrderPlace;
import com.swiggy.api.sf.snd.helper.SANDHelper;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.*;

public class UserServiceTest extends SnDDp {
    SANDHelper helper = new SANDHelper();
    OrderPlace orderPlace= new OrderPlace();
    SoftAssert softAssert= new SoftAssert();

    @Test(dataProvider = "userId", description = "verify user data")
    public void userInternal(String param){
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile, email, SANDConstants.password);
        Map<String, Object> userData= helper.referralCode(hMap.get("userId"));
        HashMap<String, String> data= new HashMap<String, String>() {{put("id", hMap.get("userId"));
        put("mobile", mobile);
        put("email",email);
        put("referral_code",hMap.get("referral_code"));}};
        for(String key : data.keySet()) {
            if (param.equals(key)) {
                String response = helper.userInternal(param, data.get(key)).ResponseValidator.GetBodyAsText();
                Assert.assertEquals(JsonString(response, "$.statusCode"), "0", "statusCode is not same");
                Assert.assertEquals(JsonString(response, "$.data.customer_id"), hMap.get("userId"), "userId is not same");
                Assert.assertEquals(JsonString(response, "$.data.email"), userData.get("user_email").toString(), "userId is not same");
            }
        }

    }

    @Test(dataProvider = "userIdNegative", description = "verify user data on negative cases")
    public void userInternalNegative(String param, String value) {
        //Map<String, Object> userData= helper.referralCode(SANDConstants.userId);
        String response = helper.userInternal(param, value).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"), "1", "statusCode is not same");
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.userNot, "userId is not same");
    }

    @Test(dataProvider = "userId", description = "verify user data without auth.")
    public void userInternalWithoutAuth(String param) {
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile, email, SANDConstants.password);
        Map<String, Object> userData= helper.referralCode(hMap.get("userId"));
        HashMap<String, String> data= new HashMap<String, String>() {{put("id", hMap.get("userId"));
            put("mobile", mobile);
            put("email",email);
            put("referral_code",hMap.get("referral_code"));}};
        for(String key : data.keySet()) {
            if (param.equals(key)) {
                String response = helper.userInternalWithoutAuth(param, data.get(key)).ResponseValidator.GetBodyAsText();
                try {
                    if ((JsonString(response, "$.statusCode").equals(0))) {
                        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.userNot, "userId is not same");
                    }
                }
                catch(PathNotFoundException e){
                    Assert.assertFalse(false, "auth key is missing");
                }
            }
        }
    }

    @Test(dataProvider = "userProfileAdmin", description = "verify user ")
    public void userProfileAdmin(String param){
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile, email, SANDConstants.password);
        HashMap<String, String> data= new HashMap<String, String>() {{put("user_id", hMap.get("userId"));
            put("user_mobile", mobile);
            put("user_email",email);}};
        for(String key : data.keySet()) {
            if (param.equals(key)) {
                String response = helper.userProfileAdmin(param, data.get(key)).ResponseValidator.GetBodyAsText();
                Assert.assertEquals(JsonString(response, "$.statusCode"), "0", "statusCode is not same or not 0");
                Map<String, Object> userData = helper.referralCode(JsonString(response, "$.data.customer_id"));
                Assert.assertEquals(JsonString(response, "$.data.customer_id"), userData.get("ID").toString(), "userId is not same");
                Assert.assertEquals(JsonString(response, "$.data.email"), userData.get("user_email").toString(), "userId is not same");
            }
        }
    }

    @Test(dataProvider = "userProfileAdminNegative", description = "verify user profile when data is incorrect ")
    public void userProfileAdminNeg(String param, String key) {
        String response= helper.userProfileAdmin(param, key).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"), "1", "statusCode is not same or not 0");
        Assert.assertEquals(JsonString(response, "$.statusMessage"),SANDConstants.usernot, "userId is not same");
    }

    @Test(dataProvider = "userProfileAdmin", description = "verify address Node in User profile admin api ")
    public void userProfileAdminAddressNode(String param) {
        String mobile=helper.getMobile();
        String email=helper.getemailString();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile, email, SANDConstants.password);
        HashMap<String, String> data= new HashMap<String, String>() {{put("user_id", hMap.get("userId"));
            put("user_mobile", mobile);
            put("user_email",email);}};
        for(String key : data.keySet()) {
            if (param.equals(key)) {
        boolean response= helper.userProfileAdmin(param, data.get(key)).ResponseValidator.DoesNodeExists("data.addresses");
        Assert.assertTrue(response, "Node does not exists");}}
    }

    @Test(dataProvider = "userProfile", description = "verify user profile data")
    public void userProfile(String addressReqd, String cardDetailsReqd){
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        String response= helper.userProfileGet(hMap.get("tid"),hMap.get("token"),addressReqd, cardDetailsReqd).ResponseValidator.GetBodyAsText();
        Map<String, Object> userData= helper.referralCode(JsonString(response, "$.data.customer_id"));
        Assert.assertEquals(userData.get("referral_code").toString(), JsonString(response, "$.data.referral_code"), "referral code is not same");
        Assert.assertEquals(userData.get("user_email").toString(), JsonString(response, "$.data.email"), "email is not same");
    }

    @Test(dataProvider = "signUpProfile", description = "Create a user and verify its profile")
    public void SignUpWithProfile(String mobile, String email, String name, String addressReqd, String cardDetailsReqd){
        String response=helper.signUp1(mobile, email, name).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),"0", "status is not same");
        Map<String, Object> userData= helper.userSignUp(mobile);

        Assert.assertEquals(email, userData.get("user_email").toString(), "email is not same");
        Assert.assertEquals(name, userData.get("display_name").toString(), "name is not same");
        String otpResponse=helper.verifyOTP(JsonString(response, "$.tid"),helper.getOTP(mobile)).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(otpResponse, "$.statusCode"),"0", "status is not same");
        Assert.assertEquals(JsonString(otpResponse, "$.data.email"),email, "email is not same");

        String userProfileResponse= helper.userProfileGet(JsonString(otpResponse, "$.tid"), JsonString(otpResponse, "$.data.token"),addressReqd, cardDetailsReqd).ResponseValidator.GetBodyAsText();
        Map<String, Object> userProfileData= helper.referralCode(JsonString(otpResponse, "$.data.customer_id"));
        Assert.assertEquals(userProfileData.get("referral_code").toString(), JsonString(userProfileResponse, "$.data.referral_code"), "referral code is not same");
        Assert.assertEquals(userProfileData.get("user_email").toString(), JsonString(userProfileResponse, "$.data.email"), "email is not same");
    }

    @Test(dataProvider = "userProfile", description = "verify user address node")
    public void userProfileAddressNode(String addressReqd, String cardDetailsReqd) {
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        boolean response= helper.userProfileGet(hMap.get("tid"),hMap.get("token"),addressReqd, cardDetailsReqd).ResponseValidator.DoesNodeExists("data.addresses");
        Assert.assertTrue(response, "Address node is not present");
    }

    @Test(dataProvider = "signUpV1", description = "sign up a user using sign up v1")
    public void signUpV1(String mobile, String email, String name){
        String response=helper.signUp1(mobile, email, name).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),"0", "status is not same");
        Map<String, Object> userData= helper.userSignUp(mobile);
        Assert.assertEquals(email, userData.get("user_email").toString(), "email is not same");
        Assert.assertEquals(name, userData.get("display_name").toString(), "name is not same");
    }

    @Test(dataProvider = "signUpV1", description = "otp verification for a user using sign up1")
    public void signUpV1OtpVerify(String mobile, String email, String name){
        String response=helper.signUp1(mobile, email, name).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),"0", "status is not same");
        Map<String, Object> userData= helper.userSignUp(mobile);
        Assert.assertEquals(email, userData.get("user_email").toString(), "email is not same");
        Assert.assertEquals(name, userData.get("display_name").toString(), "name is not same");
        String otpResponse=helper.verifyOTP(JsonString(response, "$.tid"),helper.getOTP(mobile)).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(otpResponse, "$.statusCode"),"0", "status is not same");
        Assert.assertEquals(JsonString(otpResponse, "$.data.email"),email, "email is not same");
    }

    @Test(dataProvider = "signUpV1",description = "try to use same otp again, will be a failure")
    public void signUpV1OtpVerifyAgain(String mobile, String email, String name){
        String response=helper.signUp1(mobile, email, name).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),"0", "status is not same");
        Map<String, Object> userData= helper.userSignUp(mobile);
        Assert.assertEquals(email, userData.get("user_email").toString(), "email is not same");
        Assert.assertEquals(name, userData.get("display_name").toString(), "name is not same");
        String otp= helper.getOTP(mobile);
        String otpResponse=helper.verifyOTP(JsonString(response, "$.tid"),otp).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(otpResponse, "$.statusCode"),"0", "status is not same");
        Assert.assertEquals(JsonString(otpResponse, "$.data.email"),email, "email is not same");
        String otpAttemptAgain=helper.verifyOTP(JsonString(response, "$.tid"),otp).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(otpAttemptAgain, "$.statusCode"),"1", "status is not same");
        Assert.assertEquals(JsonString(otpAttemptAgain, "$.statusMessage"),SANDConstants.otpExpireMsg, "otp is still working again");
    }

    @Test(dataProvider = "updateMobileNumber", description = "update mobile no. of a user")
    public void updateMobile(String name, String mobileNo, String email,String password){
        HashMap<String, String> hMap=helper.createUser(name, mobileNo, email,password );
        Map<String, String> loginData= helper.loginData(mobileNo,password);
        String mobile= getMobile();
        String response=helper.updateMobile(mobile, loginData.get("Tid"), loginData.get("Token")).ResponseValidator.GetBodyAsText();
        String verifyResponse=helper.updateMobileOtp(loginData.get("Tid"), loginData.get("Token"),mobile,helper.updateMobileRedis(loginData.get("userId"),mobile)).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(verifyResponse, "$.statusCode"),"0", "status is not same");
        Assert.assertEquals(JsonString(verifyResponse, "$.data.customer_id"),hMap.get("userId"), "status is not same");
    }

    @Test(description = "update a mobile no. after sign up")
    public void updateMobileSignUp(){
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        String mobile= getMobile();
        String response=helper.updateMobile(mobile, hMap.get("tid"), hMap.get("token")).ResponseValidator.GetBodyAsText();
        String verifyResponse=helper.updateMobileOtp(hMap.get("tid"), hMap.get("token"),mobile,helper.updateMobileRedis(hMap.get("userId"),mobile)).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(hMap.get("userId"), JsonString(verifyResponse, "$.data.customer_id"), "userId is not same");
        Assert.assertEquals(JsonString(verifyResponse, "$.statusCode"),"0", "status code is not zero");
    }

    @Test(description = "login or sign up a user , click on forgot password")
    public void forgotPassword() {
        String mobile= getMobile();
        HashMap<String, String> hMap = helper.createUser(SANDConstants.name, mobile, helper.getemailString(), SANDConstants.password);
        String response= helper.sendOTP(mobile).ResponseValidator.GetBodyAsText();

        String verifyResponse=helper.verifyOTP1(JsonString(response, "$.tid")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(verifyResponse, "$.statusCode"),"0", "status code is not zero");

    }

    @Test(description = "login or sign up a user, click on forgot password and use the same password again")
    public void forgotPasswordAgain() {
        String mobile= getMobile();
        HashMap<String, String> hMap = helper.createUser(SANDConstants.name, mobile, helper.getemailString(), SANDConstants.password);
        String response= helper.sendOTP(mobile).ResponseValidator.GetBodyAsText();
        String verifyResponse = helper.verifyOTP1(JsonString(response, "$.tid")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(verifyResponse, "$.statusCode"), "0", "status code is not zero");
        String verifyResponse1 = helper.verifyOTP1(JsonString(response, "$.tid")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(verifyResponse1, "$.statusCode"), "1", "status code is not zero");
    }

    @Test
    public void otpCheck(){
        String mobile= getMobile();
        HashMap<String, String> hMap = helper.createUser(SANDConstants.name, mobile, helper.getemailString(), SANDConstants.password);
       String repsonse= helper.sendOTP(mobile).ResponseValidator.GetBodyAsText();
       String verifyResponse=helper.verifyOTP1(JsonString(repsonse, "$.tid")).ResponseValidator.GetBodyAsText();
       Assert.assertEquals(JsonString(verifyResponse, "$.statusCode"),"0", "status code is not zero");
    }

  /*  @Test(dataProvider = "latLong")
    public void signUpOrder(String lat, String lng){
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, helper.getMobile(), helper.getemailString(), SANDConstants.password);
        for(int i=0; i<2; i++)
        orderPlace.createOrder(hMap.get("tid"), hMap.get("token"),lat,lng);
    }

   @Test(dataProvider = "loginDetail")
    public void loginToOrder(String lat, String lng)  {
        String mobile= helper.getMobile();
        HashMap<String, String> hMap=helper.createUser(SANDConstants.name, mobile, helper.getemailString(), SANDConstants.password);
        HashMap<String, String> hashMapLogin = orderPlace.createLogin(SANDConstants.password,Long.parseLong(mobile));
        for(int i=0; i<2; i++)
            orderPlace.createOrder(hashMapLogin.get("Tid"),hashMapLogin.get("Token"),lat,lng);
    }*/

    @Test(dataProvider = "signUp")
    public void loginV2(String name,String mobile, String email, String password){
        String signUpResponse=helper.signUpV2(name,mobile,email,password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponse, "tid"));
        String response = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
    }

    @Test(dataProvider = "userInternalPatch")
    public void userInternalUpdate(String name,String mobile, String email, String password,String key, String newMobileNo){
        String signUpResponse=helper.signUpV2(name,mobile,email,password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponse, "tid"));
        String response = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        String userId= JsonString(response, "$.data.customer_id");
        Assert.assertEquals(JsonString(response, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        String userInternalResponse= helper.userInternalpatch(key,newMobileNo, userId).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(userInternalResponse, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(userInternalResponse, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
    }

    @Test(dataProvider = "userInternalReferralPatch", enabled=false)
    public void userInternalReferralUpdate(String name,String mobile, String email, String password,String key){
        String signUpResponse=helper.signUpV2(name,mobile,email,password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponse, "tid"));
        String response = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        String referralCode= JsonString(response, "$.data.referral_code");
        String userId= JsonString(response, "$.data.customer_id");
        Assert.assertEquals(JsonString(response, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        String userInternalResponse= helper.userInternalpatch(key,referralCode, userId).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(userInternalResponse, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(userInternalResponse, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
    }

    @Test(description ="check that user exist or not using login check")
    public void loginCheck(){
        String mobile= getMobile();
        String signUpResponse=helper.signUpV2(name,mobile,helper.getemailString(),password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponse, "tid"));
        String response = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        String loginCheckResponse=helper.loginCheck(mobile).ResponseValidator.GetBodyAsText();
        softAssert.assertEquals(JsonString(loginCheckResponse, "$.statusCode"),SANDConstants.statusCode, "status code is not 0");
        softAssert.assertEquals(JsonString(loginCheckResponse, "$.data.verified"),tr, "user not verified");
        softAssert.assertEquals(JsonString(loginCheckResponse, "$.data.registered"),tr, "user not registered");
        softAssert.assertEquals(JsonString(loginCheckResponse, "$.data.passwordEnabled"),tr, "paassword not eanbled");
        softAssert.assertEquals(JsonString(loginCheckResponse, "$.data.active"),tr, "user not active");
        softAssert.assertAll();
    }

    @Test(dataProvider = "setPassword", description = "set password for a user")
    public void setPassword(String mobile, String email) {
        String signUpResponse = helper.signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String response=helper.verifyOTP1(JsonString(signUpResponse, "tid")).ResponseValidator.GetBodyAsText();
        String setResponse = helper.setPassword(mobile, password,JsonString(response, "$.tid"),JsonString(response, "$.data.token")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(setResponse, "$.statusCode"),"0", "status code is not same");
    }

    @Test(dataProvider = "setPassword", description = "set password for a user, change mobile no.")
    public void setPasswordInvalidMobile(String mobile, String email) {
        String signUpResponse = helper.signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String response=helper.verifyOTP1(JsonString(signUpResponse, "tid")).ResponseValidator.GetBodyAsText();

        String setResponse = helper.setPassword(getMobile(), password,JsonString(response, "$.tid"),JsonString(response, "$.data.token")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(setResponse, "$.statusCode"),"0", "status code is not same");

    }

    @Test(dataProvider = "referCode",description = "check the referral code is applied for a user")
    public void referredToCode(String mobile, String email) {
        String signUpResponse = helper.signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String response=helper.verifyOTP1(JsonString(signUpResponse, "tid")).ResponseValidator.GetBodyAsText();
        String referResponse = helper.referredTo(JsonString(response, "$.data.referral_code")).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(referResponse, "$.statusCode"),"0", "status code is not same");
        Assert.assertEquals(JsonString(referResponse, "$.statusMessage"),msg, "status msg is not same");
    }


    @Test(dataProvider = "referCode",description = "check the referral code data, like email")
    public void referredByCode(String mobile, String email) {
        String signUpResponse = helper.signUpV2(name, mobile, email, password).ResponseValidator.GetBodyAsText();
        String response=helper.verifyOTP1(JsonString(signUpResponse, "tid")).ResponseValidator.GetBodyAsText();
        String referResponse = helper.referredBy(JsonString(response, "$.data.referral_code")).ResponseValidator.GetBodyAsText();
        softAssert.assertEquals(JsonString(referResponse, "$.statusCode"),"0", "status code is not same");
        softAssert.assertEquals(JsonString(referResponse, "$.statusMessage"),msg, "status msg is not same");
        softAssert.assertEquals(JsonString(referResponse, "$.data.user_email"),email, "email is not same");
        softAssert.assertEquals(JsonString(referResponse, "$.data.referral_code"),JsonString(response, "$.data.referral_code"), "referral code is not same");
        softAssert.assertAll();
    }

    @Test(description = "check wrong referral code")
    public void referredByWrongCode() {
        String referResponse = helper.referredBy(helper.randomAlpha()).ResponseValidator.GetBodyAsText();
        softAssert.assertEquals(JsonString(referResponse, "$.statusCode"),"0", "status code is not same");
        softAssert.assertEquals(JsonString(referResponse, "$.statusMessage"),msg, "status msg is not same");
        softAssert.assertNull(JsonPath.read(referResponse, "$.data"), "email is not same");
        softAssert.assertAll();
    }


    private String getMobile(){
        return String.valueOf((long) Math.floor(Math.random() * 9_000_000_000L) + 1_000_000_000L);
    }
    private String JsonString(String response, String jsonPath){
        return JsonPath.read(response, jsonPath).toString().replace("[","").replace("]","").replace("\"","");
    }

    @Test(dataProvider = "userArchival",description = "Signup and login an user, archive the user, check login, again verify signup and login for same user")
    public void userArchival(String name,String mobile, String email, String password, String disable_reason, String updated_by){
        String signUpResponse=helper.signUpV2(name,mobile,email,password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponse, "tid"));
        String response = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(response, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        String resp = helper.archiveUser(new String [] {mobile},disable_reason,updated_by).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(resp, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        String responseAgain = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(responseAgain, "$.statusMessage"), SANDConstants.msg, "status message is not successful");//expect failure
        String signUpResponseAgain=helper.signUpV2(name,mobile,email,password).ResponseValidator.GetBodyAsText();
        helper.verifyOTP1(JsonString(signUpResponseAgain, "tid"));
        String responseLogin = helper.login(new String[]{mobile,password}).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(responseLogin, "$.statusMessage"), SANDConstants.msg, "status message is not successful");

    }

    @Test(dataProvider = "userArchivalSmoke",description = "Smoke test")
    public void userArchivalSmoke(String mobile,String disable_reason, String updated_by){
        String resp = helper.archiveUser(new String [] {mobile},disable_reason,updated_by).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(resp, "$.statusMessage"), SANDConstants.msg, "status message is not successful");
        Assert.assertEquals(JsonString(resp, "$.statusCode"), "0", "statusCode is not same");

    }

    @Test(dataProvider = "userArchivalInvalid",description = "Invalid user details")
    public void userArchivalInvalid(String mobile,String disable_reason, String updated_by){
        String resp = helper.archiveUser(new String [] {mobile},disable_reason,updated_by).ResponseValidator.GetBodyAsText();
        Assert.assertEquals(JsonString(resp, "$.statusMessage"), "Done 0/1, Not able to Archive: "+mobile+"", "status message is successful");
        Assert.assertEquals(JsonString(resp, "$.statusCode"), "0", "statusCode is not same");

    }



}
