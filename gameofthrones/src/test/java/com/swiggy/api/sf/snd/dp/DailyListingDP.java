package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.sf.snd.constants.DailySandConstants;
import com.swiggy.api.sf.snd.helper.DailyListingHelper;
import com.swiggy.api.sf.snd.pojo.Daily.DeliveryListing.DeliveryListing_Cerebro;
import cucumber.api.java.it.Data;
import framework.gameofthrones.JonSnow.DateHelper;
import framework.gameofthrones.JonSnow.Processor;
import framework.gameofthrones.Tyrion.JsonHelper;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.util.*;

/**
 * @author : aviral.nigam
 * @package : com.swiggy.api.sf.snd.dp
 **/
public class DailyListingDP {

    DailyListingHelper helper = new DailyListingHelper();
    DateHelper dateHelper = new DateHelper();
    public static String tid, sid;

    @DataProvider(name = "validateMealPlanSlotDP")
    public Object[][] validateMealSlotDP() {
        HashMap<String, String> hm =helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        //recalculate random slot name if its the default slot as we have already passed it
        if(random_slotName.equals(hm.get("slot"))) {
            slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
            random_slotName = DailySandConstants.SlotKeys[slot_random];
        }
        return new Object[][] {
                {DailySandConstants.lat, DailySandConstants.lng, "", "", "MEAL"},
                {DailySandConstants.lat, DailySandConstants.lng, "", "", "PLAN"},
                //default
                {DailySandConstants.lat, DailySandConstants.lng, hm.get("slot"), "2", "MEAL"},
                {DailySandConstants.lat, DailySandConstants.lng, hm.get("slot"), "2", "PLAN"},
                //random
                {DailySandConstants.lat, DailySandConstants.lng, random_slotName, "1", "MEAL"},
                {DailySandConstants.lat, DailySandConstants.lng, random_slotName, "1", "PLAN"}
        };
    }

    @DataProvider(name = "validateMealSlotsGenericDP")
    public Object[][] validateMealSlotsGenericDP() {
        return new Object[][] {
                {DailySandConstants.lat, DailySandConstants.lng, "", "", "MEAL"},
                {DailySandConstants.lat, DailySandConstants.lng, "", "", "PLAN"}
        };
    }

    @DataProvider(name = "validateEmptyMealPlanSlotDP")
    public Object[][] validateEmptyMealPlanSlotDP() {
        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        return new Object[][] {
                {DailySandConstants.invalid_lat, DailySandConstants.invalid_lng, "", "", "MEAL"},
                {DailySandConstants.invalid_lat, DailySandConstants.invalid_lng, "", "", "PLAN"},
                {DailySandConstants.invalid_lat, DailySandConstants.invalid_lng, random_slotName, "2", "MEAL"},
                {DailySandConstants.invalid_lat, DailySandConstants.invalid_lng, random_slotName, "2", "PLAN"}

        };
    }

    @DataProvider(name = "validateServiceabilityDP")
    public Iterator<Object[]> validateServiceabilityDP() throws IOException {
        HashMap<String, String> hm;
        JsonHelper jsonHelper = new JsonHelper();
        List<Object[]> obj = new ArrayList<>();
        hm = helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        DeliveryListing_Cerebro deliveryListing_cerebro = new DeliveryListing_Cerebro();
        deliveryListing_cerebro.build(Long.parseLong(hm.get("start_time")),Long.parseLong(hm.get("end_time")), DailySandConstants.lat,DailySandConstants.lng,Integer.parseInt(DailySandConstants.cityId));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(deliveryListing_cerebro),DailySandConstants.lat, DailySandConstants.lng, "", "", "MEAL"});

        //Start-time/ end-time other than the default slot
        deliveryListing_cerebro = new DeliveryListing_Cerebro();
        deliveryListing_cerebro.build(dateHelper.getMinutesAddedToCurrentEpochInMillis(120),dateHelper.getMinutesAddedToCurrentEpochInMillis(400),DailySandConstants.lat,DailySandConstants.lng,Integer.parseInt(DailySandConstants.cityId));
        obj.add(new Object[] {jsonHelper.getObjectToJSON(deliveryListing_cerebro),DailySandConstants.lat, DailySandConstants.lng, "", "", "MEAL"});


        return obj.iterator();
    }

    @DataProvider(name = "validateServiceabilityFromBlackZoneDP")
    public Object[][] validateServiceabilityFromBlackZoneDP() {
        HashMap<String, String> hm;
        hm = helper.setDeliveryBlackZoneTimeSlots(DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng,DailySandConstants.blackzone_zoneId);
        return new Object[][] {
                {hm.get("startTime"), hm.get("endTime"),"", "", "MEAL"}
        };
    }

    @DataProvider(name = "validateDeliverySlotsDP")
    public Object[][] validateDeliverySlotsDP() {
        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        return new Object[][] {
                {DailySandConstants.lat, DailySandConstants.lng,"","","MEAL"},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"0","MEAL"},
                {DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng, "","", "MEAL"},
                {DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng, random_slotName,"0", "MEAL"}
        };
    }

    // lat, lng, slot, offset. slotType, isFirstCall
    @DataProvider(name = "validatePaginationDP")
    public Object[][] validatePaginationDP() {
        HashMap<String, String> hm;
        hm = helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        //to randomly pick any slot
        Random random = new Random();
        int randome_no = random.nextInt(DailySandConstants.SlotKeys.length -1)+1;
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String[] consulProps = helper.getPaginationConfigFromConsul(DailySandConstants.userAgent);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        //recalculate random slot name if its the default slot as we have already passed it
        if(random_slotName.equals(hm.get("slot"))) {
            slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
            random_slotName = DailySandConstants.SlotKeys[slot_random];
        }
        return new Object[][] {
                {DailySandConstants.lat, DailySandConstants.lng,"","", "MEAL",true},
                {DailySandConstants.lat, DailySandConstants.lng,"","", "PLAN",true},
                //default slot name
                {DailySandConstants.lat, DailySandConstants.lng,hm.get("slot"),"0", "MEAL",true},
                {DailySandConstants.lat, DailySandConstants.lng,hm.get("slot"),"0", "PLAN",true},
                //offset 9 - second page
                {DailySandConstants.lat, DailySandConstants.lng,hm.get("slot"),"9", "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,hm.get("slot"),"9", "MEAL",false},
                //random slot name
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"0", "PLAN",true},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"0", "MEAL",true},
                //random offset within first block
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,String.valueOf(Integer.parseInt(consulProps[0])/randome_no), "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,String.valueOf(Integer.parseInt(consulProps[0])/randome_no), "MEAL",false},
                //Invalid offset -  offset > total_data_size
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"4000", "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"4000", "MEAL",false},
                //Offset that access from 2 blocks in redis
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,String.valueOf(Integer.parseInt(consulProps[0])-(Integer.parseInt(consulProps[1])/2)), "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,String.valueOf(Integer.parseInt(consulProps[0])-(Integer.parseInt(consulProps[1])/2)), "MEAL",false},
                //Non-existing slot name - this falls back to default
                {DailySandConstants.lat, DailySandConstants.lng,"AVIRAL",String.valueOf(Integer.parseInt(consulProps[0])-(Integer.parseInt(consulProps[1])/2)), "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,"AVIRAL",String.valueOf(Integer.parseInt(consulProps[0])-(Integer.parseInt(consulProps[1])/2)), "MEAL",false},
                //negative offset
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"-2", "PLAN",false},
                {DailySandConstants.lat, DailySandConstants.lng,random_slotName,"-2", "MEAL",false},
        };
    }

    //lat,lng,slotType,FILTER/SORT, hm_filter<String, List<String>>, hm_sort<String, String>
    @DataProvider(name = "validateFilterSortDP")
    public Iterator<Object[]> validateFilterSortDP() {
        //temp_hm is for filters and temp_sort is for sort data
        List<Object[]> obj = new ArrayList<>();
        String lat = DailySandConstants.lat;
        String lng = DailySandConstants.lng;
        HashMap<String, List<String>> hm_filterOptions_meal =  new HashMap<>();
        HashMap<String, List<String>> hm_filterOptions_plan =  new HashMap<>();
        HashMap<String, List<String>> hm_sortOptions =  new HashMap<>();
        //HashMap<String, List<String>> hm_sortOptions_plan =  new HashMap<>();
        hm_filterOptions_meal = helper.getAllFilterOptions(DailySandConstants.slotType_meal,"",sid,tid);
        hm_filterOptions_plan = helper.getAllFilterOptions(DailySandConstants.slotType_plan,"",sid,tid);
        hm_sortOptions = helper.getAllSortOptions(DailySandConstants.slotType_meal,"",sid,tid);
        //hm_sortOptions_plan =  helper.getAllSortOptions(DailySandConstants.slotType_plan,"");
        // 1filter - meal
        for (String filterKey: hm_filterOptions_meal.keySet()) {
            List<String> temp_ls = new ArrayList<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            temp_ls = hm_filterOptions_meal.get(filterKey);
            for (int i = 0; i < temp_ls.size() ; i++) {
                String[] temp_st = {temp_ls.get(i)};
                temp_hm.put(filterKey,Arrays.asList(temp_st));
                obj.add(new Object[] {lat,lng,DailySandConstants.slotType_meal ,"FILTER",temp_hm, temp_sort});
            }
        }
        //1filter - plan
        for (String filterKey: hm_filterOptions_plan.keySet()) {
            List<String> temp_ls = new ArrayList<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            temp_ls = hm_filterOptions_plan.get(filterKey);
            for (int i = 0; i < temp_ls.size() ; i++) {
                String[] temp_st = {temp_ls.get(i)};
                temp_hm.put(filterKey,Arrays.asList(temp_st));
                obj.add(new Object[] {lat,lng,DailySandConstants.slotType_plan ,"FILTER",temp_hm,temp_sort});
            }
        }
        //filter - multiple filters - meal
        for (String filterKey: hm_filterOptions_meal.keySet()) {
            List<String> temp_ls = new ArrayList<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            temp_ls = hm_filterOptions_meal.get(filterKey);
            //apply 2 filters of same key
            if (temp_ls.size() >= 2) {
                for (int i = 0; i < temp_ls.size() - 1; i++) {
                    String[] temp_st = {temp_ls.get(i),temp_ls.get(++i)};
                    temp_hm.put(filterKey,Arrays.asList(temp_st));
                    obj.add(new Object[]{lat, lng, DailySandConstants.slotType_meal, "FILTER", temp_hm,temp_sort});
                }
            }
        }
        //filter - multiple filters - plan
        for (String filterKey: hm_filterOptions_plan.keySet()) {
            List<String> temp_ls = new ArrayList<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            temp_ls = hm_filterOptions_plan.get(filterKey);
            //apply 2 filters of same key
            if (temp_ls.size() >= 2) {
                for (int i = 0; i < temp_ls.size() - 1; i++) {
                    String[] temp_st = {temp_ls.get(i),temp_ls.get(++i)};
                    temp_hm.put(filterKey,Arrays.asList(temp_st));
                    obj.add(new Object[]{lat, lng, DailySandConstants.slotType_plan, "FILTER", temp_hm,temp_sort});
                }
            }
        }
        //apply multiple filters of different key - meal
        if(hm_filterOptions_meal.size()>=2){
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            Set<String> filterOptionKeys = hm_filterOptions_meal.keySet();
            //converts set to array
            String[] temp_filterKeys = filterOptionKeys.toArray(new String[filterOptionKeys.size()]);
            temp_hm.put(temp_filterKeys[0],Arrays.asList(hm_filterOptions_meal.get(temp_filterKeys[0]).get(0)));
            temp_hm.put(temp_filterKeys[1],Arrays.asList(hm_filterOptions_meal.get(temp_filterKeys[1]).get(0)));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_meal, "FILTER", temp_hm,temp_sort});
        }
        //apply multiple filters of different key - plan
        if(hm_filterOptions_plan.size()>=2){
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            Set<String> filterOptionKeys = hm_filterOptions_plan.keySet();
            String[] temp_filterKeys = filterOptionKeys.toArray(new String[filterOptionKeys.size()]);
            temp_hm.put(temp_filterKeys[0],Arrays.asList(hm_filterOptions_plan.get(temp_filterKeys[0]).get(0)));
            temp_hm.put(temp_filterKeys[1],Arrays.asList(hm_filterOptions_plan.get(temp_filterKeys[1]).get(0)));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_plan, "FILTER", temp_hm,temp_sort});
        }
        //sort - plan
        for (String sortKey:hm_sortOptions.keySet()) {
            HashMap<String, String> temp_sort = new HashMap<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            List<String> temp_ls = hm_sortOptions.get(sortKey);
            for (int i = 0; i < temp_ls.size() ; i++) {
                temp_sort.put(sortKey,temp_ls.get(i));
                obj.add(new Object[] {lat,lng,DailySandConstants.slotType_plan ,"SORT",temp_hm,temp_sort});
            }

        }
        //sort - meal
        for (String sortKey:hm_sortOptions.keySet()) {
            HashMap<String, String> temp_sort = new HashMap<>();
            HashMap<String, List<String>> temp_hm =  new HashMap<>();
            List<String> temp_ls = hm_sortOptions.get(sortKey);
            for (int i = 0; i < temp_ls.size() ; i++) {
                temp_sort.put(sortKey,temp_ls.get(i));
                obj.add(new Object[] {lat,lng,DailySandConstants.slotType_meal ,"SORT",temp_hm,temp_sort});
            }
        }
        // apply both filter and sort - meal
        if(hm_filterOptions_meal.size()>=2) {
            HashMap<String, List<String>> temp_hm = new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            Set<String> filterOptionKeys = hm_filterOptions_meal.keySet();
            String[] temp_filterKeys = filterOptionKeys.toArray(new String[filterOptionKeys.size()]);
            temp_hm.put(temp_filterKeys[0], Arrays.asList(hm_filterOptions_meal.get(temp_filterKeys[0]).get(0)));
            temp_hm.put(temp_filterKeys[1], Arrays.asList(hm_filterOptions_meal.get(temp_filterKeys[1]).get(0)));
            Set<String> sortOptionKeys = hm_sortOptions.keySet();
            String[] temp_sortKeys = sortOptionKeys.toArray(new String[sortOptionKeys.size()]);
            temp_sort.put(temp_sortKeys[0], hm_sortOptions.get(temp_sortKeys[0]).get(0));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_meal, "FILTER-SORT", temp_hm, temp_sort});

            temp_sort = new HashMap<>();
            temp_sort.put(temp_sortKeys[0], hm_sortOptions.get(temp_sortKeys[0]).get(1));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_meal, "FILTER-SORT", temp_hm, temp_sort});
        }
        // apply both filter and sort - plan
        if(hm_filterOptions_plan.size()>=2) {
            HashMap<String, List<String>> temp_hm = new HashMap<>();
            HashMap<String, String> temp_sort = new HashMap<>();
            Set<String> filterOptionKeys = hm_filterOptions_plan.keySet();
            String[] temp_filterKeys = filterOptionKeys.toArray(new String[filterOptionKeys.size()]);
            temp_hm.put(temp_filterKeys[0], Arrays.asList(hm_filterOptions_plan.get(temp_filterKeys[0]).get(0)));
            temp_hm.put(temp_filterKeys[1], Arrays.asList(hm_filterOptions_plan.get(temp_filterKeys[1]).get(0)));
            Set<String> sortOptionKeys = hm_sortOptions.keySet();
            String[] temp_sortKeys = sortOptionKeys.toArray(new String[sortOptionKeys.size()]);
            temp_sort.put(temp_sortKeys[0], hm_sortOptions.get(temp_sortKeys[0]).get(0));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_plan, "FILTER-SORT", temp_hm, temp_sort});

            temp_sort = new HashMap<>();
            temp_sort.put(temp_sortKeys[0], hm_sortOptions.get(temp_sortKeys[0]).get(1));
            obj.add(new Object[]{lat, lng, DailySandConstants.slotType_plan, "FILTER-SORT", temp_hm, temp_sort});

        }
        return obj.iterator();
    }

    @DataProvider(name = "validateSubscriptionDP")
    public Object[][] validateSubscriptionDP() {
        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        HashMap<String, String> hm = new HashMap<>();
        hm = helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        Processor p = helper.login("9008738199","aviralavi");
        String no_sub_user_tid = p.ResponseValidator.GetNodeValue("$.tid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String no_sub_user_sid = p.ResponseValidator.GetNodeValue("$.sid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");

        return new Object[][] {
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"), "0", DailySandConstants.dummyCustomerId,sid,tid,  DailySandConstants.cityId},
                {DailySandConstants.lat,DailySandConstants.lng,random_slotName, "0", DailySandConstants.dummyCustomerId,sid,tid, DailySandConstants.cityId},
                //invalid slot name
                {DailySandConstants.lat,DailySandConstants.lng,"AVIRAL", "0", DailySandConstants.dummyCustomerId,sid,tid, DailySandConstants.cityId},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"), "0", DailySandConstants.customerId_with_no_subscriptions,no_sub_user_sid,no_sub_user_tid, DailySandConstants.cityId},
                {DailySandConstants.lat,DailySandConstants.lng,random_slotName, "0", DailySandConstants.customerId_with_no_subscriptions,"","", DailySandConstants.cityId}


        };

    }

    // return: lat, lng, cityId, SlotName, Offset, Meal/Plan, boolean - hit_serviceability, "String"- scenario
    @DataProvider(name = "validateAvailabilityDP")
    public Iterator<Object[]> validateAvailabilityDP() {
        List<Object[]> obj = new ArrayList<>();
        String currentTime = String.valueOf(dateHelper.getCurrentEpochTimeInMillis());
        HashMap<String, String> hm = new HashMap<>();
        hm = helper.getDefaultMealSlot(DailySandConstants.cityId, currentTime);
        //check if no delivery-slot then choose next one
        Processor p = helper.getDeliverySlotsHelper(DailySandConstants.lat, DailySandConstants.lng, hm.get("start_time"), hm.get("delivery_start_time"), hm.get("end_time"));
        String[] del_enabled_start_time = p.ResponseValidator.GetNodeValueAsStringFromJsonArray("$.data.timeslotList[?(@.enabled==1)].start_time").replace("[", "").replace("]", "").split(",");
        if (del_enabled_start_time.length <= 0) {
            hm = helper.getNextDefaultMealSlot(DailySandConstants.cityId, currentTime);
        }
        String defaultSlot = hm.get("slot");
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng, DailySandConstants.cityId, defaultSlot,"0", "MEAL", true, "" });
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng, DailySandConstants.cityId, defaultSlot,"0", "PLAN", true, "" });

        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng,DailySandConstants.cityId,random_slotName,"0", "MEAL", true, "" });
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng,DailySandConstants.cityId,random_slotName,"0", "PLAN", true, "" });
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng, DailySandConstants.cityId, defaultSlot,"0", "MEAL", false, "" });
        obj.add(new Object[] {DailySandConstants.lat,DailySandConstants.lng, DailySandConstants.cityId, defaultSlot,"0", "PLAN", false, "" });
        obj.add(new Object[] {DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng, DailySandConstants.cityId, defaultSlot,"0", "MEAL", true, "" });
        obj.add(new Object[] {DailySandConstants.blackzone_lat,DailySandConstants.blackzone_lng, DailySandConstants.cityId, defaultSlot,"0", "PLAN", true, "" });
        obj.add(new Object[] {DailySandConstants.invalid_lat,DailySandConstants.invalid_lng, DailySandConstants.cityId, defaultSlot,"0", "PLAN", true, "" });


        return obj.iterator();
    }

    //String lat, String lng, String slotName, String offset, String meal_plan, String sid, String tid
    @DataProvider(name = "validateTDAndVendorEnrichmentDP")
    public Object[][] validateTDAndVendorEnrichmentDP() {
        Random random = new Random();
        int slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
        String random_slotName = DailySandConstants.SlotKeys[slot_random];
        HashMap<String, String> hm = new HashMap<>();
        hm = helper.getDefaultMealSlot(DailySandConstants.cityId,String.valueOf(dateHelper.getCurrentEpochTimeInMillis()));
        if(random_slotName.equals(hm.get("slot"))) {
            slot_random = random.nextInt(DailySandConstants.SlotKeys.length -1);
            random_slotName = DailySandConstants.SlotKeys[slot_random];
        }
        Processor p = helper.login("9008738199","aviralavi");
        String no_sub_user_tid = p.ResponseValidator.GetNodeValue("$.tid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        String no_sub_user_sid = p.ResponseValidator.GetNodeValue("$.sid").replace("[", "").replace("]", "").replace("{", "").replace("}", "").replace("\"", "");
        return new Object[][] {
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"0","MEAL",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"0","PLAN",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"10","MEAL",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"10","PLAN",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,random_slotName,"0","MEAL",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,random_slotName,"0","PLAN",sid,tid},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"0","MEAL",no_sub_user_sid,no_sub_user_tid},
                {DailySandConstants.lat,DailySandConstants.lng,hm.get("slot"),"0","PLAN",no_sub_user_sid,no_sub_user_tid},
                {DailySandConstants.lat,DailySandConstants.lng,"AVIRAL","0","MEAL",no_sub_user_sid,no_sub_user_tid},
                {DailySandConstants.lat,DailySandConstants.lng,"AVIRAL","0","PLAN",no_sub_user_sid,no_sub_user_tid}

        };

    }


    @DataProvider(name = "latlng")
    public Object[][] latlng() {
        return new Object[][] {
                {DailySandConstants.lat, DailySandConstants.lng}
        };
    }

    @DataProvider(name = "latlngInvalid")
    public Object[][] latlngInvalid() {
        return new Object[][]{
                {"12.474833", "75.881979"}
        };
    }


    @DataProvider(name = "latlong")
    public Object[][] latLongForMealListing() {
        return new Object[][]{{"17.9689","79.5941"}};
    }

}
