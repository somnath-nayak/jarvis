package com.swiggy.api.sf.rng.pojo;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.*;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.HashMap;
import java.util.Map;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonPropertyOrder({
        "couponCode",
        "paymentCode",
        "type"
})
public class PaymentCodeMappingPOJO {
    @JsonProperty("couponCode")
    private String couponCode;
    @JsonProperty("paymentCode")
    private String paymentCode;
    @JsonProperty("type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * No args constructor for use in serialization
     *
     */
    public PaymentCodeMappingPOJO() {
    }

    /**
     *
     * @param couponCode
     * @param type
     * @param paymentCode
     */
    public PaymentCodeMappingPOJO(String couponCode, String paymentCode, String type) {
        super();
        this.couponCode = couponCode;
        this.paymentCode = paymentCode;
        this.type = type;
    }

    @JsonProperty("couponCode")
    public String getCouponCode() {
        return couponCode;
    }

    @JsonProperty("couponCode")
    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public PaymentCodeMappingPOJO withCouponCode(String couponCode) {
        this.couponCode = couponCode;
        return this;
    }

    @JsonProperty("paymentCode")
    public String getPaymentCode() {
        return paymentCode;
    }

    @JsonProperty("paymentCode")
    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public PaymentCodeMappingPOJO withPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
        return this;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    public PaymentCodeMappingPOJO withType(String type) {
        this.type = type;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PaymentCodeMappingPOJO withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    public PaymentCodeMappingPOJO setDefault(String couponCode,String paymentCode, String type){
        return this.withCouponCode(couponCode)
                .withPaymentCode(paymentCode)
                .withType(type);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("couponCode", couponCode).append("paymentCode", paymentCode).append("type", type).append("additionalProperties", additionalProperties).toString();
    }
}
