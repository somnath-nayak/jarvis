package com.swiggy.api.sf.checkout.dp;

import com.swiggy.api.sf.checkout.constants.EDVOConstants;
import org.testng.annotations.DataProvider;

public class EDVODP {
    public static String MEAL_ID_2_SCREEN = null;
    public static String MEAL_ID_3_SCREEN = null;

    @DataProvider(name="updateCartForOneMealTestData")
    public static Object[][] getUpdateCartForOneMealTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCartForOneMealTestData")
    public static Object[][] getCartForOneMealTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="flushCartForOneMealTestData")
    public static Object[][] getFlushCartForOneMealTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCartMinimalTestData")
    public static Object[][] getCartMinimalTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCheckTotalTestData")
    public static Object[][] getCheckTotalTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getPlaceOrderTestData")
    public static Object[][] getPlaceOrderTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getEditOrderCheckTestData")
    public static Object[][] getEditOrderCheckTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getEditOrderConfirmTestData")
    public static Object[][] getEditOrderConfirmTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCancelOrderTestData")
    public static Object[][] getCancelOrderTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCloneOrderCheckTestData")
    public static Object[][] getCloneOrderCheckTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "CART_UPDATED_SUCCESSFULLY", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getCloneOrderConfirmTestData")
    public static Object[][] getCloneOrderConfirmTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getSingleOrderTestData")
    public static Object[][] getSingleOrderTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getAllOrderTestData")
    public static Object[][] getAllOrderTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }

    @DataProvider(name="getLastOrderTestData")
    public static Object[][] getLastOrderTestData(){
        return new Object[][]{
                //Addon Required = false, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = false, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = false, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, false, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = true
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, true, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, true, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08},

                //Addon Required = true, Meal Item Required = true, Cart Menu Item Required = false
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "130", EDVOConstants.MEAL_TD_TYPE_01},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "115", EDVOConstants.MEAL_TD_TYPE_02},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_03},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "260", EDVOConstants.MEAL_TD_TYPE_04},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_05},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FLAT, "200", EDVOConstants.MEAL_TD_TYPE_06},
                {new String[]{MEAL_ID_2_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_PERCENTAGE, "230", EDVOConstants.MEAL_TD_TYPE_07},
                {new String[]{MEAL_ID_3_SCREEN}, EDVOConstants.RESTAURANT_ID, false, true, false, 0, "done successfully", "BXGY", "itemLevel", EDVOConstants.REWARD_TYPE_FINALPRICE, "50", EDVOConstants.MEAL_TD_TYPE_08}
        };
    }
}

