package com.swiggy.api.sf.snd.helper;


import com.swiggy.api.sf.snd.constants.SANDConstants;
import framework.gameofthrones.Aegon.Initialize;
import framework.gameofthrones.Daenerys.Initializer;
import framework.gameofthrones.JonSnow.GameOfThronesService;
import framework.gameofthrones.JonSnow.Processor;

import java.util.HashMap;


public class EditOrder implements SANDConstants {
    Initialize gameofthrones =Initializer.getInitializer();

    SANDHelper helper = new SANDHelper();


    public Processor getEditOrderMenu(String restId, String lat, String lng, String orderId) {
        String[] queryParam = {restId,lat,lng,orderId};
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("customerId", "4489715");
        GameOfThronesService gots = new GameOfThronesService("sandl", "editOrder", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, null, queryParam);
        return processor;
    }

    public Processor EditOrderSuggestion(String[] payload) {
        HashMap<String, String> requestHeader = new HashMap<String, String>();
        requestHeader.put("Content-Type", "application/json");
        requestHeader.put("Authorization",helper.basic());
        GameOfThronesService gots = new GameOfThronesService("sandl", "smartSuggestion", gameofthrones);
        Processor processor = new Processor(gots, requestHeader, payload);
        return processor;
    }


}
