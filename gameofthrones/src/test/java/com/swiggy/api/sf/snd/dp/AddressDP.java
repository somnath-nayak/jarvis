package com.swiggy.api.sf.snd.dp;

import com.swiggy.api.sf.snd.constants.SANDConstants;
import org.testng.annotations.DataProvider;

public class AddressDP implements SANDConstants {
    @DataProvider(name = "addAddress" )
    public Object[][] addAddress() {
        return new Object[][] {
                {"12.9279", "77.6271", name, password, msg, other },
                {"12.9279", "77.6271", name, password, msg, home },
                {"12.9279", "77.6271", name, password, msg, work },

        };
    }

    @DataProvider(name = "invalidUser" )
    public Object[][] invalidUser() {
        return new Object[][] {
                {"12.9279", "77.6271", name, password, invalidUser, other},
                {"12.9279", "77.6271", name, password, invalidUser, home},
                {"12.9279", "77.6271", name, password, invalidUser, work},
        };
    }

    @DataProvider(name = "sameAnnotation" )
    public Object[][] sameAnnotation() {
        return new Object[][] {
                {"12.9279", "77.6271", name, password, msg, home},
                {"12.9279", "77.6271", name, password, msg, other},
                {"12.9279", "77.6271", name, password, msg, work}
        };
    }

    @DataProvider(name = "nonExistingAddress" )
    public Object[][] nonExistingAddress() {
        return new Object[][] {
                {"12.9279", "77.6271", name, password, msg, home,notFound},
        };
    }

    @DataProvider(name = "nullExisting" )
    public Object[][] nullExisting() {
        return new Object[][] {
                {"12.9279", "77.6271", name, password, msg, other, notFound },
                {"12.9279", "77.6271", name, password, msg, home, notFound },
                {"12.9279", "77.6271", name, password, msg, work, notFound },

        };
    }


}
