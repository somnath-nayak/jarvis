package com.swiggy.api.sf.rng.pojo.SwiggySuper;

import io.advantageous.boon.json.annotations.JsonProperty;

public class Verifyplan {
	
	@JsonProperty ("")
	private String plan_id;
	@JsonProperty ("")
	private String user_id;
	
	public Verifyplan() {
		
	}
	public Verifyplan(String plan_id, String user_id) {
		this.plan_id= plan_id;
		this.user_id= user_id;
	}
	
	public String getPlanId() {
		return this.plan_id;
	}
    public void setPlanId(String plan_id) {
    	this.plan_id= plan_id;
    }
    public String getUSerId() {
		return this.user_id;
	}
    public void setUserId(String user_id) {
    	this.user_id= user_id;
    }

    @Override
    public String toString() {
    	
    	return "{"
    			+ "\"plan_id\"" + ":" + plan_id +","
    			+ "\"user_id\"" + ":" + user_id 
    			+ "}"    			;
    }



}
	



	