package com.swiggy.api.sf.snd.pojo.UpdateCarousel;

import org.codehaus.jackson.annotate.JsonProperty;

public class TimeSlots {

    @JsonProperty("day")
    private String day;
    @JsonProperty("startTime")
    private Integer startTime;
    @JsonProperty("closeTime")
    private Integer closeTime;

    @JsonProperty("day")
    public String getDay() {
        return day;
    }

    @JsonProperty("day")
    public void setDay(String day) {
        this.day = day;
    }

    public TimeSlots withDay(String day) {
        this.day = day;
        return this;
    }

    @JsonProperty("startTime")
    public Integer getStartTime() {
        return startTime;
    }

    @JsonProperty("startTime")
    public void setStartTime(Integer startTime) {
        this.startTime = startTime;
    }

    public TimeSlots withStartTime(Integer startTime) {
        this.startTime = startTime;
        return this;
    }

    @JsonProperty("closeTime")
    public Integer getCloseTime() {
        return closeTime;
    }

    @JsonProperty("closeTime")
    public void setCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
    }

    public TimeSlots withCloseTime(Integer closeTime) {
        this.closeTime = closeTime;
        return this;
    }
}