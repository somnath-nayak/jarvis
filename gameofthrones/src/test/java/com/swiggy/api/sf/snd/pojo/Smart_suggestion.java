package com.swiggy.api.sf.snd.pojo;

public class Smart_suggestion {

        private String id;

        private String event_type;

        private String op_type;

        private Data data;

        private String event_timestamp;

        private String version;


        public String getId ()
        {
            return id;
        }

        public void setId (String id)
        {
            this.id = id;
        }

        public String getEvent_type ()
        {
            return event_type;
        }

        public void setEvent_type (String event_type)
        {
            this.event_type = event_type;
        }

        public String getOp_type ()
        {
            return op_type;
        }

        public void setOp_type (String op_type)
        {
            this.op_type = op_type;
        }

        public Data getData ()
        {
            return data;
        }

        public void setData (Data data)
        {
            this.data = data;
        }

        public String getEvent_timestamp ()
        {
            return event_timestamp;
        }

        public void setEvent_timestamp (String event_timestamp)
        {
            this.event_timestamp = event_timestamp;
        }

        public String getVersion ()
        {
            return version;
        }

        public void setVersion (String version)
        {
            this.version = version;
        }

        public com.swiggy.api.sf.snd.pojo.Smart_suggestion build(){
            setDefaultValues();
            return this;
        }

        public void setDefaultValues(){

            Data data = new Data();
            data.build();

            if (this.getId() == null) {
                this.setId("132335");
            }

            if (this.getEvent_type() == null) {
                this.setEvent_type("ITEM");
            }

            if (this.getOp_type() == null) {
                this.setOp_type("UPDATE");
            }

            if (this.getEvent_timestamp() == null) {
                this.setEvent_timestamp("1535222900148");
            }

            if (this.getVersion() == null) {
                this.setVersion("2607");
            }
        }

        @Override
        public String toString()
        {
            return " [ event_type : "+event_type+",data : "+data+",id : "+id+", op_type : "+op_type+", version : "+version+", event_timestamp : "+event_timestamp+"]";
        }
    }

