package com.swiggy.api.sf.snd.pojo.event;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class PricingModel {

    private List<Variation_> variations = null;
    private Double price;
    @JsonProperty("addon_combinations")
    private List<Object> addonCombinations = null;

    public List<Variation_> getVariations() {
        return variations;
    }

    public void setVariations(List<Variation_> variations) {
        this.variations = variations;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<Object> getAddonCombinations() {
        return addonCombinations;
    }

    public void setAddonCombinations(List<Object> addonCombinations) {
        this.addonCombinations = addonCombinations;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("variations", variations).append("price", price).append("addonCombinations", addonCombinations).toString();
    }

}
