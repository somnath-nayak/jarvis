package com.swiggy.api.sf.rng.pojo.couponcartevaluate;

import org.codehaus.jackson.annotate.JsonProperty;

public class _117253920VariantsAddons {
	@JsonProperty("quantity")
	private Integer quantity;
	@JsonProperty("subTotal")
	private Double subTotal;

	@JsonProperty("quantity")
	public Integer getQuantity() {
	return quantity;
	}

	@JsonProperty("quantity")
	public void setQuantity(Integer quantity) {
	this.quantity = quantity;
	}

	public _117253920VariantsAddons withQuantity(Integer quantity) {
	this.quantity = quantity;
	return this;
	}

	@JsonProperty("subTotal")
	public Double getSubTotal() {
	return subTotal;
	}

	@JsonProperty("subTotal")
	public void setSubTotal(Double subTotal) {
	this.subTotal = subTotal;
	}

	public _117253920VariantsAddons withSubTotal(Double subTotal) {
	this.subTotal = subTotal;
	return this;
	}
}
